package com.ncsts.view.bean;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.component.UISelectItems;
import javax.faces.component.ValueHolder;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;

import org.ajax4jsf.component.html.HtmlAjaxSupport;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.richfaces.component.html.HtmlCalendar;
import org.richfaces.component.html.HtmlDataTable;
import org.richfaces.model.selection.SimpleSelection;
import org.springframework.beans.factory.annotation.Autowired;

import com.ncsts.common.CacheManager;
import com.ncsts.common.Util;
import com.ncsts.domain.BatchMaintenance;
import com.ncsts.domain.BatchMetadata;
import com.ncsts.domain.BatchMetadata.Order;
import com.ncsts.domain.ListCodes;
import com.ncsts.domain.ListCodesPK;
import com.ncsts.domain.Option;
import com.ncsts.domain.OptionCodePK;
import com.ncsts.domain.User;
import com.ncsts.dto.BatchMaintenanceDTO;
import com.ncsts.dto.BatchMetadataDTO;
import com.ncsts.dto.BatchMetadataList;
import com.ncsts.jsf.model.BatchMaintenanceDataModel;
import com.ncsts.services.BatchMaintenanceService;
import com.ncsts.services.ListCodesService;
import com.ncsts.services.OptionService;
import com.ncsts.view.bean.BaseMatrixBackingBean.FindIdCallback;
import com.ncsts.view.util.BatchStatistics;
import com.ncsts.view.util.BatchTypeSpecific;
import com.ncsts.view.util.TogglePanelController;



/**
 * @author Muneer
 *
 */
public class BatchMaintenanceBackingBean /*extends JsfHelper*/ {
	
	private Logger logger = Logger.getLogger(BatchMaintenanceBackingBean.class);

	private HtmlCalendar batchStartTime = new HtmlCalendar();
	
	private Date myCalendarEntryDate ;
	private BatchStatistics batchStatistics;
  
	private HtmlSelectOneMenu batchTypeMenu; //= new HtmlSelectOneMenu();
	private HtmlSelectOneMenu batchTypeMenuForDefUserFields;


	private HtmlSelectOneMenu batchStatusMenu;
	private HtmlSelectOneMenu batchStatusMenuForUpdate;
	private HtmlSelectOneMenu batchErrorMenu;
	
	private CacheManager cacheManager;
	private BatchMaintenanceDTO batchMaintenanceDTO = new BatchMaintenanceDTO(); //this is declared for search
	private BatchMaintenance batchMaintenance = new BatchMaintenance();
	private BatchMaintenanceDTO batchMaintenanceDTONew = new BatchMaintenanceDTO(); //this is declared for updating location matrix line
	private List<BatchMaintenanceDTO> batchMaintenanceList = new ArrayList<BatchMaintenanceDTO>(); // this list is declared for displaying the table rows
	private List<BatchMaintenanceDTO> selectedBatchMaintenanceList = new ArrayList<BatchMaintenanceDTO>(); // this list is declared for capturing selected rows from the table
	private BatchMaintenanceDTO currentBatchMaintencanceDTO = null;




	private List<BatchMetadata> validBatchMaintenance;
	private BatchMetadataDTO currentBMDTO = new BatchMetadataDTO();

	private String selectedUserFieldsBmType = null;

	private BatchMaintenance batchMaintForStatistics = new BatchMaintenance();
	
    private BatchErrorsBackingBean batchErrorsBean;
	
	private BatchMaintenanceDataModel batchMaintenanceDataModel;
	private BatchMaintenance selBatchMaintenance = new BatchMaintenance();
	private BatchMetadataList batchMetadataList;
		
	private SimpleSelection selection = new SimpleSelection();
	private int selectedRowIndex = -1;
	private String currentAction;
   
	private FilePreferenceBackingBean filePreferenceBean;
	
	private BatchMaintenanceService batchMaintenanceService;
	private ListCodesService listCodesService;
	private OptionService optionService;
	@Autowired
	private loginBean lgnBean;
	
	
	private boolean batchIdSorted = true;
	private boolean totalBytesSorted = true;
	private boolean totalRowsSorted = true;
	
	private Date delSchedDate;
	
	private FindIdCallback findIdCallback = null;
	private String findIdAction;
	private String findIdContext;
	private String findDefaultBatchType="";
	private String findDefaultStatusType="";
	private String findDefaultErrorType = "";



	private Boolean textFieldFlag1;
	private Boolean textFieldFlag2;
	private Boolean textFieldFlag3;
	private Boolean textFieldFlag4;
	private Boolean textFieldFlag5;
	private Boolean textFieldFlag6;
	private Boolean textFieldFlag7;
	private Boolean textFieldFlag8;
	private Boolean textFieldFlag9;
	private Boolean textFieldFlag10;
	private Boolean numberFieldFlag1;
	private Boolean numberFieldFlag2;
	private Boolean numberFieldFlag3;
	private Boolean numberFieldFlag4;
	private Boolean numberFieldFlag5;
	private Boolean numberFieldFlag6;
	private Boolean numberFieldFlag7;
	private Boolean numberFieldFlag8;
	private Boolean numberFieldFlag9;
	private Boolean numberFieldFlag10;
	private Boolean dateFieldFlag1;
	private Boolean dateFieldFlag2;
	private Boolean dateFieldFlag3;
	private Boolean dateFieldFlag4;
	private Boolean dateFieldFlag5;
	private Boolean dateFieldFlag6;
	private Boolean dateFieldFlag7;
	private Boolean dateFieldFlag8;
	private Boolean dateFieldFlag9;
	private Boolean dateFieldFlag10;
	private List<BatchTypeSpecific> batchTypeSpecificList = null;
	private List<BatchTypeSpecific> searchBatchTypeSpecificList = null;
	private String batchTypeCode  = null;
	private BatchMaintenance batchSpecificBatchMaintenance = new BatchMaintenance();
	
    public static final String BILLING_TAX_TYPE = "Billing/Sales";
    public static final String PURCHASING_TAX_TYPE = "Purchasing/Use";
    
    private boolean isResetBatchAccordion = true;
    
    private boolean isCalledFromConfigImportExpMenu; 
    
    public static final String BATCH_KILL_PROCESS_STATUS_CODE = "KILLP";
    
    private static final String USER_STSCORP = "STSCORP";
    
    private Date killBatchScheduleTime;
    
    private boolean hasExtractedTransactions;
    
	public boolean getHasExtractedTransactions() {
		return hasExtractedTransactions;
	}

	public void setHasExtractedTransactions(boolean hasExtractedTransactions) {
		this.hasExtractedTransactions = hasExtractedTransactions;
	}

	public Date getKillBatchScheduleTime() {
		return killBatchScheduleTime;
	}

	public void setKillBatchScheduleTime(Date killBatchScheduleTime) {
		this.killBatchScheduleTime = killBatchScheduleTime;
	}

	public boolean isCalledFromConfigImportExpMenu() {
		return isCalledFromConfigImportExpMenu;
	}

	public void setCalledFromConfigImportExpMenu(
			boolean isCalledFromConfigImportExpMenu) {
		this.isCalledFromConfigImportExpMenu = isCalledFromConfigImportExpMenu;
	}

	public boolean isResetBatchAccordion() {
		return isResetBatchAccordion;
	}

	public void setResetBatchAccordion(boolean isResetBatchAccordion) {
		this.isResetBatchAccordion = isResetBatchAccordion;
	}

	public List<BatchTypeSpecific> getSearchBatchTypeSpecificList() {
		return searchBatchTypeSpecificList;
	}

	public void setSearchBatchTypeSpecificList(
			List<BatchTypeSpecific> searchBatchTypeSpecificList) {
		this.searchBatchTypeSpecificList = searchBatchTypeSpecificList;
	}

	public BatchMaintenance getBatchSpecificBatchMaintenance() {
		return batchSpecificBatchMaintenance;
	}

	public void setBatchSpecificBatchMaintenance(
			BatchMaintenance batchSpecificBatchMaintenance) {
		this.batchSpecificBatchMaintenance = batchSpecificBatchMaintenance;
	}

	public String getBatchTypeCode() {
		return batchTypeCode;
	}

	public void setBatchTypeCode(String batchTypeCode) {
		this.batchTypeCode = batchTypeCode;
	}

	public List<BatchTypeSpecific> getBatchTypeSpecificList() {
		return batchTypeSpecificList;
	}

	public void setBatchTypeSpecificsList(
			List<BatchTypeSpecific> batchTypeSpecificList) {
		this.batchTypeSpecificList = batchTypeSpecificList;
	}
	
	public void findId(FindIdCallback callback, String context, String action, String defaultBatchType) {
		findIdCallback = callback;
		findIdContext = context;
		findIdAction = action;
		findDefaultBatchType = defaultBatchType;
		findDefaultStatusType = "";
		
		resetFilterComboBox();
		getBatchTypeMenu();
		getBatchStatusMenu();
		getBatchErrorMenu();
		
		if(batchStatusMenu!=null) batchStatusMenu.setValue("");
		if(batchTypeMenu!=null) batchTypeMenu.setValue(findDefaultBatchType);
		
		myCalendarEntryDate = null;
		
		//Do a default search on findDefaultBatchType
		selectedRowIndex = -1;
		selBatchMaintenance = new BatchMaintenance();
		
		batchMaintenance = new BatchMaintenance();
		batchMaintenance.setBatchTypeCode(findDefaultBatchType);
		batchMaintenanceDataModel.setCriteria(batchMaintenance);
		batchMaintenanceDataModel.setDefaultSortOrder("batchId", BatchMaintenanceDataModel.SORT_DESCENDING);
	}
	
	public String findAction() {
		findIdCallback.findIdCallback(selBatchMaintenance.getbatchId(), findIdContext);
		exitFindMode();
		return findIdAction;
	}
	
	public String cancelFindAction() {
		exitFindMode();
		return findIdAction;
	}
	
	public boolean getFindMode() {
		return (findIdCallback != null);
	}
	
	public void exitFindMode() {
		if (findIdCallback != null) {
			findIdCallback = null;

			selBatchMaintenance = new BatchMaintenance();
			batchMaintenanceList = new ArrayList<BatchMaintenanceDTO>();
			selectedRowIndex = -1;
		}
	}
	
	public String navigateAction() {
		exitFindMode();
		batchMaintenance = null;
		batchMaintenanceDataModel.setCriteria(batchMaintenance);
		resetFilterComboBox();
		resetSearchAction();
		if(batchTypeSpecificList != null && batchTypeSpecificList.size() > 0) {
			  batchTypeSpecificList.clear();
		  }
	    setCalledFromConfigImportExpMenu(false);
		return "data_utility_batch";
	}

	public Boolean getDisableComponents() {
		return "".equalsIgnoreCase(selectedUserFieldsBmType);
	}


	public HtmlSelectOneMenu getBatchTypeMenuForDefUserFields() {

		currentBMDTO = new BatchMetadataDTO();
		batchTypeMenuForDefUserFields = new HtmlSelectOneMenu();
		final Collection<SelectItem> batchTypeList = new ArrayList<SelectItem>();

		batchTypeList.add(new SelectItem("", "Select Batch Type"));
		selectedUserFieldsBmType = "";


		List<ListCodes> listCodeList = cacheManager.getListCodesByType("BATCHTYPE");

		String listCodeMessage = (lgnBean.getIsPurchasingSelected()) ? "1" : "2";

		for (ListCodes listcode : listCodeList) {
			if (listcode != null && listcode.getCodeTypeCode() != null && listcode.getDescription() != null
					&& listcode.getMessage() != null) {			
				if (listCodeMessage.equalsIgnoreCase(listcode.getMessage()) || "3".equalsIgnoreCase(listcode.getMessage())) {
					batchTypeList.add(new SelectItem(listcode.getCodeCode(), listcode.getDescription()));
				}			
			}
		}
		final UISelectItems items = new UISelectItems();
		items.setValue(batchTypeList);
		items.setId("userFieldsBatchType_item");
		batchTypeMenuForDefUserFields.getChildren().add(items);

		batchTypeMenuForDefUserFields.setValue(batchTypeList);

		return batchTypeMenuForDefUserFields;
	}

	public void setBatchTypeMenuForDefUserFields(HtmlSelectOneMenu batchTypeMenuForDefUserFields) {
		this.batchTypeMenuForDefUserFields = batchTypeMenuForDefUserFields;
	}


	public HtmlSelectOneMenu getBatchTypeMenu() {
		// Fixed for Issue 0004490
		
		if(batchTypeMenu==null){
			batchTypeMenu = new HtmlSelectOneMenu();
			
			final Collection<SelectItem> batchTypeList = getBatchTypeItems();
			final UISelectItems items = new UISelectItems();
			items.setValue(batchTypeList);
			items.setId("batchTypeId_item");
			batchTypeMenu.getChildren().add(items);
	
			batchTypeMenu.setValue(findDefaultBatchType);
		}

		return batchTypeMenu;
	}
	
	public Collection<SelectItem> getBatchTypeItems() {
		final Collection<SelectItem> batchTypeList = new ArrayList<SelectItem>();
		boolean pcoEnabled = false;
		Option isPCO = optionService.findByPK(new OptionCodePK("PCO", "ADMIN", "ADMIN"));
		if (isPCO!=null && isPCO.getValue()!=null && "1".equals(isPCO.getValue()))
			pcoEnabled = true;
		
		if (!getFindMode()) {
			
			batchTypeList.add(new SelectItem("", "Select Batch Type"));
			
			List<ListCodes> listCodeList = cacheManager.getListCodesByType("BATCHTYPE");
			
			if(lgnBean.getIsPurchasingSelected()) {
				for (ListCodes listcode : listCodeList) {
					if (listcode != null && listcode.getCodeTypeCode() != null && listcode.getDescription() != null 
							&& listcode.getMessage() != null) {						
						if(listcode.getMessage().equals("1") || listcode.getMessage().equals("3")) {
							
							if(listcode.getCodeCode()!=null && listcode.getCodeCode().equals("PCO")){
								if(pcoEnabled) 
									batchTypeList.add(new SelectItem(listcode.getCodeCode(),listcode.getDescription()));
									  
							} else 
								batchTypeList.add(new SelectItem(listcode.getCodeCode(),listcode.getDescription()));
						}												
					}
				}
				
			} else if (lgnBean.getIsBillingSelected()) {
				for (ListCodes listcode : listCodeList) {
					if (listcode != null && listcode.getCodeTypeCode() != null && listcode.getDescription() != null 
							&& listcode.getMessage() != null) {						
						if(listcode.getMessage().equals("2") || listcode.getMessage().equals("3")) {
							
							if(listcode.getCodeCode()!=null && listcode.getCodeCode().equals("PCO")){
								if(pcoEnabled) 
									batchTypeList.add(new SelectItem(listcode.getCodeCode(),listcode.getDescription()));
									  
							} else 
								batchTypeList.add(new SelectItem(listcode.getCodeCode(),listcode.getDescription()));
						}						
					}
				}
			}
			
		} else {
			
			if (!findDefaultBatchType.equals("GE")) {
				if (pcoEnabled) 
					findDefaultBatchType = "PCO";
				
				if (findIdCallback instanceof TransactionDetailBean) {
					
					if (pcoEnabled) {
						batchTypeList.add(new SelectItem("_APB", "All Purchase Batch Types"));
						batchTypeList.add(new SelectItem("PCO", "PCo Combined Transactions"));
					}				
						batchTypeList.add(new SelectItem("IP","Purchasing Transactions"));
					
				} else if (findIdCallback instanceof TransactionDetailSaleBean) {
					
					if (pcoEnabled) {
						batchTypeList.add(new SelectItem("_ASB", "All Sales Batch Types"));
						batchTypeList.add(new SelectItem("PCO", "PCo Combined Transactions"));
					}
					
					batchTypeList.add(new SelectItem("IPS", "Sales Transactions"));
				}				
			} else {
				batchTypeList.add(new SelectItem("GE", "G/L Extract"));
			}
		}
		return batchTypeList;
	}
	
	public void setBatchTypeMenu(HtmlSelectOneMenu batchTypeMenu) {
		this.batchTypeMenu = batchTypeMenu;
	}
	

	public HtmlSelectOneMenu getBatchStatusMenuForUpdate() {
		batchStatusMenuForUpdate = new HtmlSelectOneMenu();
		final Collection<SelectItem> batchStatuslist = new ArrayList<SelectItem>();
		//List<ListCodes> listCodeList = listCodesService.getListCodesByCodeTypeCode("BATCHSTAT");
		List<ListCodes> listCodeList = cacheManager.getListCodesByType("BATCHSTAT");
		for (ListCodes listcodes : listCodeList) {
			if(listcodes != null && listcodes.getCodeTypeCode()!=null && listcodes.getDescription()!=null){
				batchStatuslist.add(new SelectItem(listcodes.getCodeCode(),listcodes.getDescription()));
			}
		}
		final UISelectItems items = new UISelectItems();
		items.setId("batchStatusUpdateitemsID");
		items.setValue(batchStatuslist);
		batchStatusMenuForUpdate.getChildren().add(items);
		
		return batchStatusMenuForUpdate;
	}
	
	public void batchStatusChanged(ActionEvent e) {
		String statusNew = (String) ((HtmlSelectOneMenu) ((HtmlAjaxSupport) e.getComponent()).getParent()).getSubmittedValue();
		String statusOld = selBatchMaintenance.getBatchStatusCode();
		
		selBatchMaintenance.setBatchStatusCode(statusNew);
		
		
		if(statusNew.equalsIgnoreCase("D")){
			
			FacesMessage message = new FacesMessage("Setting a batch to 'Deleted' does not remove the batch records.");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
			
			message = new FacesMessage("Use 'Flagged for Delete' to remove all records in the batch.");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
		}
	}

	public void setBatchStatusMenuForUpdate(HtmlSelectOneMenu batchStatusMenuForUpdate) {
		this.batchStatusMenuForUpdate = batchStatusMenuForUpdate;
	}

	public HtmlSelectOneMenu getBatchStatusMenu() {
		if(batchStatusMenu==null){
			batchStatusMenu = new HtmlSelectOneMenu();
			final Collection<SelectItem> batchStatuslist = new ArrayList<SelectItem>();
			batchStatuslist.add(new SelectItem("","Select Batch Status"));
			List<ListCodes> listCodeList = cacheManager.getListCodesByType("BATCHSTAT");
			for (ListCodes listcodes : listCodeList) {
				if(listcodes != null && listcodes.getCodeTypeCode()!=null && listcodes.getDescription()!=null){
					batchStatuslist.add(new SelectItem(listcodes.getCodeCode(),listcodes.getDescription()));
				}
			}
			final UISelectItems items = new UISelectItems();
			items.setId("batchStatus_ITEM");
			items.setValue(batchStatuslist);
			batchStatusMenu.getChildren().add(items);
			
			batchStatusMenu.setValue(findDefaultStatusType);
		}
		
		return batchStatusMenu;
	}
	
	public HtmlSelectOneMenu getBatchErrorMenu() {
		if(batchErrorMenu==null){
			batchErrorMenu = new HtmlSelectOneMenu();
			final Collection<SelectItem> errorStatuslist = new ArrayList<SelectItem>();
			errorStatuslist.add(new SelectItem("","Select Error Severity"));
			List<ListCodes> listCodeList = cacheManager.getListCodesByType("ERRORSEV");
			for (ListCodes listcodes : listCodeList) {
				if(listcodes != null && listcodes.getCodeTypeCode()!=null && listcodes.getDescription()!=null){
					errorStatuslist.add(new SelectItem(listcodes.getCodeCode(),listcodes.getDescription()));
				}
			}
			final UISelectItems items = new UISelectItems();
			items.setId("errorStatus_ITEM");
			items.setValue(errorStatuslist);
			batchErrorMenu.getChildren().add(items);
			
			batchErrorMenu.setValue(findDefaultErrorType);
		}
		
		return batchErrorMenu;
	}
	
	public void resetFilterComboBox(){
		batchTypeMenu = null;
		batchStatusMenu = null;
		batchErrorMenu = null;
	}
	
	public void retrieveBatchMaintenance() {
		batchMaintenance = null;
		batchMaintenanceDataModel.setCriteria(batchMaintenance);
		selectedRowIndex = -1;
		batchMaintenanceDTO.setYearPeriod(null); 
		selBatchMaintenance = new BatchMaintenance();
		batchMaintenance = new BatchMaintenance();
		prepareBatchMaintenance(batchMaintenance);
	}
	
	public BatchMaintenanceDataModel prepareBatchMaintenance(BatchMaintenance batchMaintenance)  {
		if(lgnBean.getIsPurchasingSelected()) {
			batchMaintenance.setLastModuelId(PURCHASING_TAX_TYPE);	
		}
		else {
			batchMaintenance.setLastModuelId(BILLING_TAX_TYPE); 
		}
		createBatchMaintenance();
		copyBatchSpecifFields(batchMaintenance); 
		logger.debug("Date from myCalandarEntryDate "+ myCalendarEntryDate);
		//batchMaintenanceDTO.setEntryTimestamp(myCalendarEntryDate);
		if(myCalendarEntryDate != null) {
			batchMaintenance.setEntryTimestamp(myCalendarEntryDate);
		}
		//batch id
		if(batchMaintenanceDTO.getBatchId() != null && batchMaintenanceDTO.getBatchId().longValue() > 0) {
			batchMaintenance.setBatchId(batchMaintenanceDTO.getBatchId()); 
		}
		if(batchStatusMenu!=null && batchStatusMenu.getValue()!=null && !"".equalsIgnoreCase(batchStatusMenu.getValue().toString())){
			logger.debug("Batch status Menu "+ batchStatusMenu.getValue().toString());			
		    batchMaintenance.setBatchStatusCode(batchStatusMenu.getValue().toString());
		}
		if(batchTypeMenu!=null && batchTypeMenu.getValue()!=null && !"".equalsIgnoreCase(batchTypeMenu.getValue().toString())){
			logger.debug("Batch Type Code "+ batchTypeMenu.getValue().toString());
			
			//check for syntetic code
			if (batchTypeMenu.getValue().toString().equalsIgnoreCase("_ASB")) {
				batchMaintenance.setBatchTypeCode("IPS");
				batchMaintenance.setAttachPcoToCriteria(true);
			} else if (batchTypeMenu.getValue().toString().equalsIgnoreCase("_APB")) {
				batchMaintenance.setBatchTypeCode("IP");
				batchMaintenance.setAttachPcoToCriteria(true);
			} else {			
				batchMaintenance.setBatchTypeCode(batchTypeMenu.getValue().toString());
			}
		}
		
		if("GM".equals(batchMaintenance.getBatchTypeCode())) {
			String fiscalyear = batchMaintenanceDTO.getYearPeriod();
			if(fiscalyear != null && !"".equals(fiscalyear)){
				batchMaintenance.setYearPeriod(fiscalyear);
			}
			
		}
		if(batchErrorMenu!=null && batchErrorMenu.getValue()!=null && !"".equalsIgnoreCase(batchErrorMenu.getValue().toString())){
			logger.debug("Batch Error Menu "+ batchErrorMenu.getValue().toString());			
		    batchMaintenance.setErrorSevCode(batchErrorMenu.getValue().toString());
		}
		
		if(batchMaintenanceDTO.getHeldCode()!= null) {
			batchMaintenance.setHeldFlag(batchMaintenanceDTO.getHeldCode()); 
		}
		
		//check which input values are entered and send only those to criteria. 
		batchMaintenance.setSearchBatchTypeSpecificList(searchBatchTypeSpecificList);   
		
		batchMaintenance.setCalledFromConfigImportExpMenu(isCalledFromConfigImportExpMenu);
		
		batchMaintenanceDataModel.setCriteria(batchMaintenance);
		batchMaintenanceDataModel.setDefaultSortOrder("batchId", BatchMaintenanceDataModel.SORT_DESCENDING);
		
		deleteSelectedBatchMap.clear();
		deleteSelectedBatchMap  = new LinkedHashMap<Long, BatchMaintenance>();

		validBatchMaintenance = batchMaintenanceService.loadValidMetadata(lgnBean.getIsPurchasingSelected());
		return batchMaintenanceDataModel;
		
	}
	
	private void copyBatchSpecifFields(BatchMaintenance batchMaintenance) {
		
		 searchBatchTypeSpecificList = new ArrayList<BatchTypeSpecific>(); // Contains actual entered search fields data 
	     try{
	    	 for(BatchTypeSpecific batchTypeSpecific : batchTypeSpecificList) {
		    	 String batchFieldName = batchTypeSpecific.getBatchFieldname();
		    	 String batchFieldType = batchTypeSpecific.getBatchFieldtype();
		    	 if("String".equals(batchFieldType)) {
		    		 String value = (String)getBatchFieldValue(batchTypeSpecific);
		    		 if(StringUtils.isNotEmpty(value)){ 
			    		 Util.setProperty(batchMaintenance, batchFieldName, value, String.class);
			    		 searchBatchTypeSpecificList.add(batchTypeSpecific);
		    		 }
		    	 }
		    	 if("Date".equals(batchFieldType)) {
		    		 Date value = (Date)getBatchFieldValue(batchTypeSpecific);
		    		 if(value != null && !"".equals(value)){
			    		 Util.setProperty(batchMaintenance, batchFieldName, value, Date.class);
			    		 searchBatchTypeSpecificList.add(batchTypeSpecific);
		    		 }
		    	 }
		    	 if("Double".equals(batchFieldType)) {
		    		 Double value = (Double)getBatchFieldValue(batchTypeSpecific);
		    		 //default Double value is taking as 0.0
		    		 if(value != null && !"".equals(value) &&  value != 0.0){ 
			    		 Util.setProperty(batchMaintenance, batchFieldName, value, Double.class);
			    		 searchBatchTypeSpecificList.add(batchTypeSpecific);
		    		 }
		    	 }
		    	 if("BigDecimal".equals(batchFieldType)) {
		    		 BigDecimal value = (BigDecimal)getBatchFieldValue(batchTypeSpecific);
		    		 if(value != null && !"".equals(value) && !BigDecimal.ZERO.equals(value)){
			    		 Util.setProperty(batchMaintenance, batchFieldName, value, BigDecimal.class);
			    		 searchBatchTypeSpecificList.add(batchTypeSpecific);
		    		 }
		    	 }
		     }
	     }catch(Exception e) {
			 System.out.println("Exception occured in class JPABatchMaintenance in method generateCriteriaBatch() :   "+e.getMessage());
		 }
	     batchMaintenance.setSearchBatchTypeSpecificList(searchBatchTypeSpecificList); 
		
	}
	
	private Object getBatchFieldValue(BatchTypeSpecific batchTypeSpecific) {
		String batchFieldName = batchTypeSpecific.getBatchFieldname();
		Object value = null;
		value = Util.getProperty(batchSpecificBatchMaintenance, batchFieldName);
		return value;
	}
	
	public void sortAction(ActionEvent e) {
		batchMaintenanceDataModel.toggleSortOrder(e.getComponent().getParent().getId());
	}


	public void selectedRowChanged(ActionEvent e) throws AbortProcessingException {
		HtmlDataTable table = (HtmlDataTable) ((HtmlAjaxSupport) e.getComponent()).getParent(); 
		setSelectedRowIndex(table.getRowIndex());
		selBatchMaintenance = new BatchMaintenance();
		selBatchMaintenance = (BatchMaintenance)table.getRowData();
	}

	public String viewAction() {
		BatchMaintenance viewdbatch = createBatchMetadataList();
		if(viewdbatch == null) {
			return null;
		}
		else {
			this.currentAction = "View";
			return "batch_maintenance_view";
		}
		
	}
	
	public String displayUpdateAction(){
		BatchMaintenance updatedbatch = createBatchMetadataList();
		if(updatedbatch == null) {
			return null;
		}
		else {
			this.currentAction = "Update";
			return "batch_maintenance_update";
		}
	}

	public BatchMaintenance createBatchMetadataList() {
		BatchMaintenance batchmaint = null;
		if(selBatchMaintenance != null){
			batchmaint = batchMaintenanceService.findById(selBatchMaintenance.getBatchId());
			if(batchmaint!=null){
				String selectedType = "";
				if(batchmaint.getBatchTypeCode().equalsIgnoreCase("PCO")){
					boolean ipSelected =  lgnBean.getIsPurchasingSelected();
					if(ipSelected){
						selectedType = "IP";
			        }
			        else{
			        	boolean ipsSelected =  lgnBean.getIsBillingSelected();
			        	if(ipsSelected){
			        		selectedType = "IPS";
			        	}
			        	else{
			        		logger.error("Module not selected");
							return null;
			        	}
			        }			
				}
				else{
					selectedType = batchmaint.getBatchTypeCode();
				}
				BatchMetadata batchMetadata = batchMaintenanceService.findBatchMetadataById(selectedType);
				batchMetadataList = new BatchMetadataList(batchMetadata, batchmaint);
			}
			else{
				logger.error("Batch ID: " + selBatchMaintenance.getBatchId() + " not found");
				return null;
			}
		}
		return batchmaint;
		
	}
	
	public String displayBatchView(Long batchNo,String view) {
		if(view.equals("viewFromLogHistory")){
			currentAction = "viewFromLogHistory";
		}
		else if(view.equals("viewFromLogEntry")){
			currentAction = "viewFromLogEntry";
		}
		else {
			currentAction = "viewFromTransaction";
		}
		
		this.currentAction = view;
		selBatchMaintenance = batchMaintenanceService.findById(batchNo);
		if(selBatchMaintenance != null){
			selBatchMaintenance.setBatchTypeDesc(((ListCodes)((Map)cacheManager.getListCodeMap().get("BATCHTYPE")).get(selBatchMaintenance.getBatchTypeCode())).getDescription());
			BatchMetadata batchMetadata=batchMaintenanceService.findBatchMetadataById(selBatchMaintenance.getBatchTypeCode());
			batchMetadataList = new BatchMetadataList(batchMetadata, selBatchMaintenance);
			return "batch_maintenance_view";
		}else{
			selectedRowIndex = -1;
			selBatchMaintenance = new BatchMaintenance();
			return null;
		}
	}
	
	public String okAction(){
		if("View".equals(this.currentAction)){
			return "batch_maintenance_main";
			
		}else if("viewFromTransaction".equals(this.currentAction)){
			return "trans_view";
		}else if("viewFromLogEntry".equals(this.currentAction)){
			return "trans_log_entry_view";
		}
		else {
			return "trans_history";
		}
	}
	
	public Boolean getIsViewAction() {
		return ("View".equalsIgnoreCase(currentAction) || "viewFromTransaction".equalsIgnoreCase(currentAction) || "viewFromLogHistory".equals(this.currentAction) || "viewFromLogEntry".equals(this.currentAction));
	}
	public Boolean getIsUpdateAction() {
		return "Update".equalsIgnoreCase(currentAction);
	}
	
	public String updateAction(){
		BatchMaintenance updateBatchMaintenance = batchMetadataList.update();
		updateBatchMaintenance.setBatchStatusCode(getSelBatchMaintenance().getBatchStatusCode());
		updateBatchMaintenance.setSchedStartTimestamp(getSelBatchMaintenance().getSchedStartTimestamp()); 
		batchMaintenanceService.update(updateBatchMaintenance);
		retrieveBatchMaintenance();
		return "batch_maintenance_main";
	}

	public String displayDefnUserFieldsAction() {
		selectedUserFieldsBmType = "";
		this.currentAction = "UserFields";
		return "batch_maintenance_defuserfields";
	}

	public String holdAction() {
		List<BatchMaintenance> batches = new ArrayList<BatchMaintenance>();
		
		BatchMaintenance bm  = null;
		for (Map.Entry<Long, Boolean> e : batchMaintenanceDataModel.getSelectionMap().entrySet()) {
			bm = batchMaintenanceDataModel.getById(e.getKey());		
			if(bm!=null && !bm.getBatchStatusCode().startsWith("X")) {
				batches.add(new BatchMaintenance(bm));
			}
		}	
		
		if (batches.size() == 0 && selBatchMaintenance!=null && (!selBatchMaintenance.getBatchStatusCode().startsWith("X"))){
			batches.add(new BatchMaintenance(selBatchMaintenance));
		}

		for(BatchMaintenance batchMaintenance : batches){
			if(batchMaintenance.getHeldFlag()==null || "0".equals(batchMaintenance.getHeldFlag()) || batchMaintenance.getHeldFlag()==""){
				batchMaintenance.setHeldFlag("1");
				batchMaintenanceService.update(batchMaintenance);
			} else {
				batchMaintenance.setHeldFlag("0");
				batchMaintenanceService.update(batchMaintenance);
			}
		}
		retrieveBatchMaintenance();
		
		return "batch_maintenance_main"; 
	}
		
	public String statisticsAction() {
		batchStatistics = new BatchStatistics(selBatchMaintenance, 
					batchMaintenanceDataModel.getBatchMaintenanceDAO(),
					filePreferenceBean.getBatchPreferenceDTO());
		batchStatistics.calcStatistics();
		return "batch_maintenance_statistics";
	}
	public void refreshStatistics() {
		batchStatistics.calcStatistics();
  }
  	
	
	public Boolean getDisplayDelete() {
		if ((selectedRowIndex != -1) && (selBatchMaintenance != null)){
  			if(selBatchMaintenance.canBeDeleted()){
				return true;
			}
		}
		
		return getHasDeleteSelection();
	}
	
	private boolean getHasDeleteSelection() {	
		BatchMaintenance bm  = null;
		for (Map.Entry<Long, Boolean> e : batchMaintenanceDataModel.getSelectionMap().entrySet()) {
			bm = batchMaintenanceDataModel.getById(e.getKey());		
			if(bm!=null && bm.canBeDeleted()) {
				return true;
			}
		}
		
		return false;
	}
	
	private String deleteBatchIds = "";
	private String deleteBatchTypeDesc = "";
	private Map<Long, BatchMaintenance> deleteSelectedBatchMap = new LinkedHashMap<Long, BatchMaintenance>();
	List<BatchMaintenance> deleteSelectedBatches = new ArrayList<BatchMaintenance>();
	
	public String getDeleteBatchIds(){
		return deleteBatchIds;
	}
	
	public String getDeleteBatchTypeDesc(){
		return deleteBatchTypeDesc;
	}
	
	public String deleteSelectedAction()  {		
		deleteBatchIds = "";
		deleteBatchTypeDesc = "";
		deleteSelectedBatches.clear();
		
		deleteSelectedBatches  = new ArrayList<BatchMaintenance>();

		BatchMaintenance bm  = null;
		for (Map.Entry<Long, Boolean> e : batchMaintenanceDataModel.getSelectionMap().entrySet()) {
			bm = batchMaintenanceDataModel.getById(e.getKey());		
			if(bm!=null && bm.canBeDeleted()) {
				deleteSelectedBatches.add(new BatchMaintenance(bm));
			}
		}
				
		//For highlighted if needed.
		if (deleteSelectedBatches.size() == 0 && selBatchMaintenance!=null && selBatchMaintenance.canBeDeleted()){
			deleteSelectedBatches.add(new BatchMaintenance(selBatchMaintenance));
		}
		
		if(deleteSelectedBatches.size()==0){
			return null;
		}
		
		if(deleteSelectedBatches.size()>1){
			deleteBatchIds = "Multiple";
		}
		else{
			bm = (BatchMaintenance)deleteSelectedBatches.get(0);
			deleteBatchIds = "" + bm.getBatchId();
		}
		
		//Is there more that one type
		Map<String, String> batchTypeMap = new LinkedHashMap<String, String>();
		for(BatchMaintenance batchMaintenance : deleteSelectedBatches){
			batchTypeMap.put(batchMaintenance.getBatchTypeDesc(), batchMaintenance.getBatchTypeDesc());
		}
		if(batchTypeMap.size()>1){
			deleteBatchTypeDesc = "Multiple";
		}
		else{
			bm = (BatchMaintenance)deleteSelectedBatches.get(0);
			deleteBatchTypeDesc = bm.getBatchTypeDesc();
		}
		
		//Warning: No reversals will be made. 
		List<Long> ids = new ArrayList<Long>();
		for(BatchMaintenance batchMaintenance : deleteSelectedBatches){
			if(batchMaintenance.getBatchTypeCode().equalsIgnoreCase("PCO") || batchMaintenance.getBatchTypeCode().equalsIgnoreCase("IP")){
				ids.add(batchMaintenance.getbatchId());
			}
		}
		boolean hasExtractedTransactions = false;	
		if(ids.size()>0){
			hasExtractedTransactions = batchMaintenanceService.hasExtractedTransactions(ids);	
		}
		
		if(hasExtractedTransactions){
			if(deleteSelectedBatches.size()>1){
				FacesMessage message = new FacesMessage("Warning: at least 1 batch has extracted transactions. No reversals will be made.");
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage(null, message);
			}
			else{
				FacesMessage message = new FacesMessage("Warning: batch has extracted transactions. No reversals will be made.");
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage(null, message);
			}
		}
		
	  	return "batch_maintenance_delete";
	}
		
	public String deleteAction()  {
		
		System.out.println("Delete Action");
		for (BatchMaintenance batchMaintenance : deleteSelectedBatches) {
			batchMaintenance.setBatchStatusCode("FD");
			batchMaintenance.setSchedStartTimestamp((Date)batchStartTime.getValue());
	  		batchMaintenanceService.update(batchMaintenance);
    	}
		
		deleteSelectedBatchMap.clear();
		deleteSelectedBatchMap  = new LinkedHashMap<Long, BatchMaintenance>();
		
		batchMaintenanceDataModel.refreshData(false);
  		return "batch_maintenance_main";		
	}
	
	public String deleteBatchAction(){
		System.out.println("Delete! Batch Action");
		if(selBatchMaintenance.getBatchStatusCode()!=null & selBatchMaintenance.getBatchStatusCode().equals("P")){
			System.out.println("Delete Batch Action");
		  selBatchMaintenance.setBatchStatusCode("D");
		  batchMaintenanceService.update(selBatchMaintenance);
		  
		  DownloadAddBean.deleteDownloadFile(selBatchMaintenance.getVc01());
		  
		  retrieveBatchMaintenance();
		  return null;
		}
	  
		return "batch_maintenance_delete";
	}

	public String errorsAction(){
	  batchErrorsBean.setBatchMaintenance(selBatchMaintenance);
	  batchErrorsBean.setReturnView("batch_maintenance_main");
	  
	  //0001701: Import errors make the system crash
	  batchErrorsBean.getBatchErrorDataModel().setBatchMaintenance(selBatchMaintenance);

	  return "batch_maintenance_errors";
	}
	
	public String cancelAction() {
		logger.debug("Inside Cancel Action");
		return "batch_maintenance_main";
	}

	public Boolean getDisplayButtons() {
		return selectedRowIndex != -1;
	}
	
	public Boolean getDisplayHoldButtons() {
		if ((selectedRowIndex != -1) && (selBatchMaintenance != null)){
  			if(!(selBatchMaintenance.getBatchStatusCode().startsWith("X"))){
				return true;
			}
		}
	  	
	  	return getHasHoldSelection();
	}
	
	private boolean getHasHoldSelection() {
		BatchMaintenance bm  = null;
		for (Map.Entry<Long, Boolean> e : batchMaintenanceDataModel.getSelectionMap().entrySet()) {
			bm = batchMaintenanceDataModel.getById(e.getKey());		
			if(bm!=null && !bm.getBatchStatusCode().startsWith("X")) {
				return true;
			}
		}
		return false;
	}

	public Boolean getDisplayError() {
		return (selectedRowIndex != -1) && (selBatchMaintenance.getErrorSevCode() != null) &&
		(selBatchMaintenance.getErrorSevCode().trim().length() > 0);
	}

	private void createBatchMaintenance(){
		if(batchMaintenance.getEntryTimestamp()!=null && "".equalsIgnoreCase(batchMaintenance.getEntryTimestamp().toString())){
			batchMaintenance.setEntryTimestamp(null);
			}
		if(batchMaintenance.getBatchStatusCode() == "" || "".equals(batchMaintenance.getBatchStatusCode())){
			batchMaintenance.setBatchStatusCode(null);
			}
		if(batchMaintenance.getBatchTypeCode() == "" || "".equals(batchMaintenance.getBatchTypeCode())){
			batchMaintenance.setBatchTypeCode(null);
			}
	}



  // *********************  Utility Methods to get BatchTypeCode Description and Batch Status Description in HashTables *************//
	
	public Hashtable<String,String> getBatchTypes(){
        Hashtable<String,String> ht = new Hashtable<String,String>();
		//List<ListCodes> listCodeList = listCodesService.getListCodesByCodeTypeCode("BATCHTYPE");
		List<ListCodes> listCodeList = cacheManager.getListCodesByType("BATCHTYPE");
		for (ListCodes listcodes : listCodeList) {
			if(listcodes != null && listcodes.getCodeTypeCode()!=null && listcodes.getDescription()!=null){
				ht.put(listcodes.getCodeCode().trim(), listcodes.getDescription().trim());				
			}
		}
		return ht;
	}

	public Hashtable<String, String> getBatchStatus() {
		Hashtable<String, String> ht = new Hashtable<String, String>();
		List<ListCodes> listCodeList = cacheManager.getListCodesByType("BATCHSTAT");
		//List<ListCodes> listCodeList = listCodesService.getListCodesByCodeTypeCode("BATCHSTAT");
		for (ListCodes listcodes : listCodeList) {
			if (listcodes != null && listcodes.getCodeTypeCode() != null && listcodes.getDescription() != null) {
				ht.put(listcodes.getCodeCode().trim(), listcodes.getDescription().trim());
			}
		}
		return ht;
	}

	public Hashtable<String, String> getErrorStatus() {
		Hashtable<String, String> ht = new Hashtable<String, String>();
		List<ListCodes> listCodeList = cacheManager.getListCodesByType("ERRORSEV");
		//List<ListCodes> listCodeList = listCodesService.getListCodesByCodeTypeCode("ERRORSEV");
		for (ListCodes listcodes : listCodeList) {
			if (listcodes != null && listcodes.getCodeTypeCode() != null && listcodes.getDescription() != null) {
				ht.put(listcodes.getCodeCode().trim(), listcodes.getDescription().trim());
			}
		}
		return ht;
	}
	
  /**
   * @return the batchErrorsBean
   */
  public BatchErrorsBackingBean getBatchErrorsBean() {
    return this.batchErrorsBean;
  }
  public String resetSearchAction(){
	  batchMaintenanceDTO.setBatchTypeCode("");
	  resetOtherBatchFields();
	  return null;
  }
  
  public void resetOtherBatchFields() {
	  batchMaintenance = new BatchMaintenance();
	  if(!"".equals(batchMaintenanceDTO.getYearPeriod())) {
		  batchMaintenanceDTO.setYearPeriod("");
	  }
      batchMaintenanceDTO.setYearPeriod("");
	  if(batchStatusMenu != null) {
		  batchStatusMenu.setValue("");
	  }
	  if(batchTypeMenu !=null){
		  batchTypeMenu.setValue("");
	  }
	  if(batchErrorMenu != null) {
		  batchErrorMenu.setValue("");
	  }
	  if(!"".equals(batchMaintenanceDTO.getHeldCode())) {
		  batchMaintenanceDTO.setHeldCode("");
	  }
	  myCalendarEntryDate = null;
	  batchMaintenanceDTO.setBatchId(new Long("0")); 
	  clearbatchTypeSpecificInputFields();
	  if(batchTypeSpecificList != null && batchTypeSpecificList.size() > 0 && isResetBatchAccordion()) {
		  batchTypeSpecificList.clear();
	  }
	  //Keep default search type
	  if(getFindMode()){
		  batchTypeMenu.setValue(findDefaultBatchType);
	  }
	  
  }
  
  private void clearbatchTypeSpecificInputFields() {
		if(batchTypeSpecificList !=null && batchTypeSpecificList.size() > 0) {
			for(BatchTypeSpecific batchTypeSpecific : batchTypeSpecificList) {
		    	 String batchFieldName = batchTypeSpecific.getBatchFieldname();
		    	 String batchFieldType = batchTypeSpecific.getBatchFieldtype();
		    	 if("String".equals(batchFieldType)) {
		    		 Util.setProperty(batchSpecificBatchMaintenance, batchFieldName, null, String.class);
		    	 }
		    	 if("Date".equals(batchFieldType)) {
		    		 Util.setProperty(batchSpecificBatchMaintenance, batchFieldName, null, Date.class);
		    		
		    	 }
		    	 if("Double".equals(batchFieldType)) {
	 		   		 Util.setProperty(batchSpecificBatchMaintenance, batchFieldName, 0.0, Double.class);
	 		   		 
		    	 }
		    	 if("BigDecimal".equals(batchFieldType)) {
			    	 Util.setProperty(batchSpecificBatchMaintenance, batchFieldName, BigDecimal.ZERO, BigDecimal.class);
			    	 
		    	 }
			}
		}
		
	}
	
	// Fix for Bug 2234
	public List<String> getSelectionsForBinaryWeight(int weight) {
        List<String> selections = new ArrayList<String>();
		List<Integer> list = getBinaryCodeForBinaryWeight(weight);
		ListCodesPK pk;		
		for (Iterator<Integer> it = list.iterator(); it.hasNext(); ) {
		    Integer value = it.next();
		    pk = new ListCodesPK();
			pk.setCodeTypeCode("MEASURETYP");
			pk.setCodeCode(value.toString());
			ListCodes listCodes = listCodesService.findByPK(pk);
			selections.add(listCodes.getDescription());
		}
		pk = new ListCodesPK();
		pk.setCodeTypeCode("MEASURETYP");
		pk.setCodeCode("0");
		ListCodes listCodes = listCodesService.findByPK(pk);
		selections.add(listCodes.getDescription());
		return selections;
	}
	   
	// Fix for Bug 2234	
	private List<Integer> getBinaryCodeForBinaryWeight(int weight) {
	    List<Integer> list = new ArrayList<Integer>();
		String bin = Integer.toBinaryString(weight);	
		char[] charr = bin.toCharArray();
		for (int i = 0; i < charr.length ; i++) {
	        if ("1".equalsIgnoreCase(""+charr[i])) {
	            int pos = 1;
	            for (int j = 0; j < ((charr.length-1) - i); j++ ) {
	                pos = 2 * pos;	
	            }
	            list.add(new Integer(pos));
	        }
		}
		return list;
	}

	/**
	 * @return the cacheManager
	 */
	public CacheManager getCacheManager() {
		return this.cacheManager;
	}

	/**
	 * @param cacheManager the cacheManager to set
	 */
	public void setCacheManager(CacheManager cacheManager) {
		this.cacheManager = cacheManager;
	}   
	private void addError(String s) {
		addMessage(s, FacesMessage.SEVERITY_ERROR);
	}
	private void addInfo(String s) {
		addMessage(s, FacesMessage.SEVERITY_INFO);
	}
	
	private void addMessage(String s, FacesMessage.Severity severity) {
		FacesContext.getCurrentInstance().addMessage(null, 
				new FacesMessage(severity, s, null));
	}
  public HtmlCalendar getBatchStartTime() {
	batchStartTime.resetValue();
    batchStartTime.setValue(filePreferenceBean.getBatchPreferenceDTO().getDefaultBatchStartTime());
    return this.batchStartTime;
  }

	public void setBatchStartTime(HtmlCalendar batchStartTime) {
		this.batchStartTime = batchStartTime;
	}

	public FilePreferenceBackingBean getFilePreferenceBean() {
		return this.filePreferenceBean;
	}

	public void setFilePreferenceBean(FilePreferenceBackingBean filePreferenceBean) {
		this.filePreferenceBean = filePreferenceBean;
	}

	public BatchStatistics getBatchStatistics() {
		return this.batchStatistics;
	}
	public BatchMetadataList getBatchMetadataList() {
		return this.batchMetadataList;
	}
	public void setBatchErrorsBean(BatchErrorsBackingBean batchErrorsBean) {
		this.batchErrorsBean = batchErrorsBean;
	}

	public BatchMaintenance getSelBatchMaintenance() {
		return selBatchMaintenance;
	}

	public void setSelBatchMaintenance(BatchMaintenance selBatchMaintenance) {
		this.selBatchMaintenance = selBatchMaintenance;
	}

	public BatchMaintenance getBatchMaintenance() {
		return batchMaintenance;
	}

	public void setBatchMaintenance(BatchMaintenance batchMaintenance) {
		this.batchMaintenance = batchMaintenance;
	}

	public BatchMaintenanceDataModel getBatchMaintenanceDataModel() {
		return batchMaintenanceDataModel;
	}

	public void setBatchMaintenanceDataModel(
			BatchMaintenanceDataModel batchMaintenanceDataModel) {
		this.batchMaintenanceDataModel = batchMaintenanceDataModel;
	}

	public boolean isBatchIdSorted() {
		return batchIdSorted;
	}

	public void setBatchIdSorted(boolean batchIdSorted) {
		this.batchIdSorted = batchIdSorted;
	}

	public boolean isTotalBytesSorted() {
		return totalBytesSorted;
	}

	public void setTotalBytesSorted(boolean totalBytesSorted) {
		this.totalBytesSorted = totalBytesSorted;
	}

	public boolean isTotalRowsSorted() {
		return totalRowsSorted;
	}

	public void setTotalRowsSorted(boolean totalRowsSorted) {
		this.totalRowsSorted = totalRowsSorted;
	}

	public BatchMaintenance getBatchMaintForStatistics() {
		return batchMaintForStatistics;
	}

	public void setBatchMaintForStatistics(BatchMaintenance batchMaintForStatistics) {
		this.batchMaintForStatistics = batchMaintForStatistics;
	}

	public int getSelectedRowIndex() {
		return selectedRowIndex;
	}

	public void setSelectedRowIndex(int selectedRowIndex) {
		this.selectedRowIndex = selectedRowIndex;
	}

	public Date getDelSchedDate() {
		return delSchedDate;
	}

	public void setDelSchedDate(Date delSchedDate) {
		this.delSchedDate = delSchedDate;
	}

	public OptionService getOptionService() {
		return optionService;
	}

	public void setOptionService(OptionService optionService) {
		this.optionService = optionService;
	}


	public BatchMaintenanceService getBatchMaintenanceService() {
		return batchMaintenanceService;
	}

	public void setBatchMaintenanceService(
			BatchMaintenanceService batchMaintenanceService) {
		this.batchMaintenanceService = batchMaintenanceService;
	}

	public ListCodesService getListCodesService() {
		return listCodesService;
	}

	public void setListCodesService(ListCodesService listCodesService) {
		this.listCodesService = listCodesService;
	}
	public void setBatchStatusMenu(HtmlSelectOneMenu batchStatusMenu) {
		this.batchStatusMenu = batchStatusMenu;
	}
	
	public void setBatchErrorMenu(HtmlSelectOneMenu batchErrorMenu) {
		this.batchErrorMenu = batchErrorMenu;
	}

	public Date getMyCalendarEntryDate() {
		return myCalendarEntryDate;
	}

	public void setMyCalendarEntryDate(Date myCalendarEntryDate) {
		this.myCalendarEntryDate = myCalendarEntryDate;
	}

	public List<BatchMaintenanceDTO> getBatchMaintenanceList() {
		return batchMaintenanceList;		
	}

	public void setBatchMaintenanceList(List<BatchMaintenanceDTO> batchMaintenanceList) {
		this.batchMaintenanceList = batchMaintenanceList;
	}
	
	public BatchMaintenanceDTO getBatchMaintenanceDTO() {
		return batchMaintenanceDTO;
	}

	public void setBatchMaintenanceDTO(BatchMaintenanceDTO batchMaintenanceDTO) {
		this.batchMaintenanceDTO = batchMaintenanceDTO;
	}
	public SimpleSelection getSelection() {
		return selection;
	}
	
	public void setSelection(SimpleSelection selection) {
		this.selection = selection;
	}
	
	public List<BatchMaintenanceDTO> getSelectedBatchMaintenanceList() {
		return selectedBatchMaintenanceList;
	}

	public void setSelectedBatchMaintenanceList(
			List<BatchMaintenanceDTO> selectedBatchMaintenanceList) {
		this.selectedBatchMaintenanceList = selectedBatchMaintenanceList;
	}
	
	public BatchMaintenanceDTO getBatchMaintenanceDTONew() {
		return batchMaintenanceDTONew;
	}

	public void setBatchMaintenanceDTONew(BatchMaintenanceDTO batchMaintenanceDTONew) {
		this.batchMaintenanceDTONew = batchMaintenanceDTONew;
	}

	public String getCurrentAction() {
		if("View".equals(this.currentAction) ||"viewFromTransaction".equals(this.currentAction) || "viewFromLogHistory".equals(this.currentAction) || "viewFromLogEntry".equals(this.currentAction)){
			return "View";
		}
		return this.currentAction;
	}
	
	public String downloadActionRedirect() {
		if (this.selBatchMaintenance == null) {
			return "";
		}
				
		String path = System.getProperty("download.home", "").trim();
		if(path == null || path.length() == 0) {
			path = System.getProperty("catalina.home", System.getProperty("java.io.tmpdir")) + "\\downloads\\";	
		}
		String downloadPath = path + this.selBatchMaintenance.getVc01();

		Util.downloadFileUrl(downloadPath);

		return "";
	}
	
	public boolean getDisplayDownload() {
		//batch.setBatchStatusCode("P");
		return selBatchMaintenance != null && "DL".equals(this.selBatchMaintenance.getBatchTypeCode()) && ("P".equals(this.selBatchMaintenance.getBatchStatusCode()) || "DL".equals(this.selBatchMaintenance.getBatchStatusCode())) && this.selBatchMaintenance.getTotalRows() != null && this.selBatchMaintenance.getTotalRows() > 0;
	}


	public String getSelectedUserFieldsBmType() {
		return selectedUserFieldsBmType;
	}

	public void setSelectedUserFieldsBmType(String selectedUserFieldsBmType) {
		this.selectedUserFieldsBmType = selectedUserFieldsBmType;
	}

	public void userFieldsTypeChanged(ValueChangeEvent e) {
		if (!"".equalsIgnoreCase(selectedUserFieldsBmType)) {
			batchMaintenanceService.saveBatchMetadata(BatchMetadataDTO.createBatchMetadata(currentBMDTO));
		}
		selectedUserFieldsBmType = (String)e.getNewValue();
	}


	public void loadCurrentDTO() {

		if ("".equalsIgnoreCase(selectedUserFieldsBmType)) {
			currentBMDTO = new BatchMetadataDTO();
		} else {
			BatchMetadata batchMetadata = batchMaintenanceService.findBatchMetadataById(selectedUserFieldsBmType);
			if (batchMetadata != null) {
				currentBMDTO = BatchMetadataDTO.createCopyFrom(batchMetadata);
			}
		}
		setFlags();

	}



	public BatchMetadataDTO getCurrentBMDTO() {
		return currentBMDTO;
	}

	public void setCurrentBMDTO(BatchMetadataDTO currentBMDTO) {
		this.currentBMDTO = currentBMDTO;
	}

	public String userFieldsOkAction() {
		batchMaintenanceService.saveBatchMetadata(BatchMetadataDTO.createBatchMetadata(currentBMDTO));
		selectedUserFieldsBmType = "";
		currentBMDTO = new BatchMetadataDTO();
		setFlags();
		return "batch_maintenance_main";
	}


	public Boolean getTextFieldFlag1() {
		return textFieldFlag1;
	}

	public void setTextFieldFlag1(Boolean textFieldFlag1) {
		this.textFieldFlag1 = textFieldFlag1;
	}

	public void resetUvc01Desc() {
		if (!textFieldFlag1)
			currentBMDTO.setUvc01Desc(null);
	}
	public void resetUvc02Desc() {
		if (!textFieldFlag2)
			currentBMDTO.setUvc02Desc(null);
	}
	public void resetUvc03Desc() {
		if (!textFieldFlag3)
			currentBMDTO.setUvc03Desc(null);
	}
	public void resetUvc04Desc() {
		if (!textFieldFlag4)
			currentBMDTO.setUvc04Desc(null);
	}
	public void resetUvc05Desc() {
		if (!textFieldFlag5)
			currentBMDTO.setUvc05Desc(null);
	}
	public void resetUvc06Desc() {
		if (!textFieldFlag6)
			currentBMDTO.setUvc06Desc(null);
	}
	public void resetUvc07Desc() {
		if (!textFieldFlag7)
			currentBMDTO.setUvc07Desc(null);
	}
	public void resetUvc08Desc() {
		if (!textFieldFlag8)
			currentBMDTO.setUvc08Desc(null);
	}
	public void resetUvc09Desc() {
		if (!textFieldFlag9)
			currentBMDTO.setUvc09Desc(null);
	}
	public void resetUvc10Desc() {
		if (!textFieldFlag10)
			currentBMDTO.setUvc10Desc(null);
	}

	public void resetNu01Desc() {
		if (!numberFieldFlag1)
			currentBMDTO.setUnu01Desc(null);
	}
	public void resetNu02Desc() {
		if (!numberFieldFlag2)
			currentBMDTO.setUnu02Desc(null);
	}
	public void resetNu03Desc() {
		if (!numberFieldFlag3)
			currentBMDTO.setUnu03Desc(null);
	}
	public void resetNu04Desc() {
		if (!numberFieldFlag4)
			currentBMDTO.setUnu04Desc(null);
	}
	public void resetNu05Desc() {
		if (!numberFieldFlag5)
			currentBMDTO.setUnu05Desc(null);
	}
	public void resetNu06Desc() {
		if (!numberFieldFlag6)
			currentBMDTO.setUnu06Desc(null);
	}
	public void resetNu07Desc() {
		if (!numberFieldFlag7)
			currentBMDTO.setUnu07Desc(null);
	}
	public void resetNu08Desc() {
		if (!numberFieldFlag8)
			currentBMDTO.setUnu08Desc(null);
	}
	public void resetNu09Desc() {
		if (!numberFieldFlag9)
			currentBMDTO.setUnu09Desc(null);
	}
	public void resetNu10Desc() {
		if (!numberFieldFlag10)
			currentBMDTO.setUnu10Desc(null);
	}

	public void resetUts01Desc() {
		if (!dateFieldFlag1)
			currentBMDTO.setUts01Desc(null);
	}
	public void resetUts02Desc() {
		if (!dateFieldFlag2)
			currentBMDTO.setUts02Desc(null);
	}
	public void resetUts03Desc() {
		if (!dateFieldFlag3)
			currentBMDTO.setUts03Desc(null);
	}
	public void resetUts04Desc() {
		if (!dateFieldFlag4)
			currentBMDTO.setUts04Desc(null);
	}
	public void resetUts05Desc() {
		if (!dateFieldFlag5)
			currentBMDTO.setUts05Desc(null);
	}
	public void resetUts06Desc() {
		if (!dateFieldFlag6)
			currentBMDTO.setUts06Desc(null);
	}
	public void resetUts07Desc() {
		if (!dateFieldFlag7)
			currentBMDTO.setUts07Desc(null);
	}
	public void resetUts08Desc() {
		if (!dateFieldFlag8)
			currentBMDTO.setUts08Desc(null);
	}
	public void resetUts09Desc() {
		if (!dateFieldFlag9)
			currentBMDTO.setUts09Desc(null);
	}
	public void resetUts10Desc() {
		if (!dateFieldFlag10)
			currentBMDTO.setUts10Desc(null);
	}


	public Boolean getTextFieldFlag2() {
		return textFieldFlag2;
	}

	public void setTextFieldFlag2(Boolean textFieldFlag2) {
		this.textFieldFlag2 = textFieldFlag2;
	}

	public Boolean getTextFieldFlag3() {
		return textFieldFlag3;
	}

	public void setTextFieldFlag3(Boolean textFieldFlag3) {
		this.textFieldFlag3 = textFieldFlag3;
	}

	public Boolean getTextFieldFlag4() {
		return textFieldFlag4;
	}

	public void setTextFieldFlag4(Boolean textFieldFlag4) {
		this.textFieldFlag4 = textFieldFlag4;
	}

	public Boolean getTextFieldFlag5() {
		return textFieldFlag5;
	}

	public void setTextFieldFlag5(Boolean textFieldFlag5) {
		this.textFieldFlag5 = textFieldFlag5;
	}

	public Boolean getTextFieldFlag6() {
		return textFieldFlag6;
	}

	public void setTextFieldFlag6(Boolean textFieldFlag6) {
		this.textFieldFlag6 = textFieldFlag6;
	}

	public Boolean getTextFieldFlag7() {
		return textFieldFlag7;
	}

	public void setTextFieldFlag7(Boolean textFieldFlag7) {
		this.textFieldFlag7 = textFieldFlag7;
	}

	public Boolean getTextFieldFlag8() {
		return textFieldFlag8;
	}

	public void setTextFieldFlag8(Boolean textFieldFlag8) {
		this.textFieldFlag8 = textFieldFlag8;
	}

	public Boolean getTextFieldFlag9() {
		return textFieldFlag9;
	}

	public void setTextFieldFlag9(Boolean textFieldFlag9) {
		this.textFieldFlag9 = textFieldFlag9;
	}

	public Boolean getTextFieldFlag10() {
		return textFieldFlag10;
	}

	public void setTextFieldFlag10(Boolean textFieldFlag10) {
		this.textFieldFlag10 = textFieldFlag10;
	}

	public Boolean getNumberFieldFlag1() {
		return numberFieldFlag1;
	}

	public void setNumberFieldFlag1(Boolean numberFieldFlag1) {
		this.numberFieldFlag1 = numberFieldFlag1;
	}

	public Boolean getNumberFieldFlag2() {
		return numberFieldFlag2;
	}

	public void setNumberFieldFlag2(Boolean numberFieldFlag2) {
		this.numberFieldFlag2 = numberFieldFlag2;
	}

	public Boolean getNumberFieldFlag3() {
		return numberFieldFlag3;
	}

	public void setNumberFieldFlag3(Boolean numberFieldFlag3) {
		this.numberFieldFlag3 = numberFieldFlag3;
	}

	public Boolean getNumberFieldFlag4() {
		return numberFieldFlag4;
	}

	public void setNumberFieldFlag4(Boolean numberFieldFlag4) {
		this.numberFieldFlag4 = numberFieldFlag4;
	}

	public Boolean getNumberFieldFlag5() {
		return numberFieldFlag5;
	}

	public void setNumberFieldFlag5(Boolean numberFieldFlag5) {
		this.numberFieldFlag5 = numberFieldFlag5;
	}

	public Boolean getNumberFieldFlag6() {
		return numberFieldFlag6;
	}

	public void setNumberFieldFlag6(Boolean numberFieldFlag6) {
		this.numberFieldFlag6 = numberFieldFlag6;
	}

	public Boolean getNumberFieldFlag7() {
		return numberFieldFlag7;
	}

	public void setNumberFieldFlag7(Boolean numberFieldFlag7) {
		this.numberFieldFlag7 = numberFieldFlag7;
	}

	public Boolean getNumberFieldFlag8() {
		return numberFieldFlag8;
	}

	public void setNumberFieldFlag8(Boolean numberFieldFlag8) {
		this.numberFieldFlag8 = numberFieldFlag8;
	}

	public Boolean getNumberFieldFlag9() {
		return numberFieldFlag9;
	}

	public void setNumberFieldFlag9(Boolean numberFieldFlag9) {
		this.numberFieldFlag9 = numberFieldFlag9;
	}

	public Boolean getNumberFieldFlag10() {
		return numberFieldFlag10;
	}

	public void setNumberFieldFlag10(Boolean numberFieldFlag10) {
		this.numberFieldFlag10 = numberFieldFlag10;
	}

	public Boolean getDateFieldFlag1() {
		return dateFieldFlag1;
	}

	public void setDateFieldFlag1(Boolean dateFieldFlag1) {
		this.dateFieldFlag1 = dateFieldFlag1;
	}

	public Boolean getDateFieldFlag2() {
		return dateFieldFlag2;
	}

	public void setDateFieldFlag2(Boolean dateFieldFlag2) {
		this.dateFieldFlag2 = dateFieldFlag2;
	}

	public Boolean getDateFieldFlag3() {
		return dateFieldFlag3;
	}

	public void setDateFieldFlag3(Boolean dateFieldFlag3) {
		this.dateFieldFlag3 = dateFieldFlag3;
	}

	public Boolean getDateFieldFlag4() {
		return dateFieldFlag4;
	}

	public void setDateFieldFlag4(Boolean dateFieldFlag4) {
		this.dateFieldFlag4 = dateFieldFlag4;
	}

	public Boolean getDateFieldFlag5() {
		return dateFieldFlag5;
	}

	public void setDateFieldFlag5(Boolean dateFieldFlag5) {
		this.dateFieldFlag5 = dateFieldFlag5;
	}

	public Boolean getDateFieldFlag6() {
		return dateFieldFlag6;
	}

	public void setDateFieldFlag6(Boolean dateFieldFlag6) {
		this.dateFieldFlag6 = dateFieldFlag6;
	}

	public Boolean getDateFieldFlag7() {
		return dateFieldFlag7;
	}

	public void setDateFieldFlag7(Boolean dateFieldFlag7) {
		this.dateFieldFlag7 = dateFieldFlag7;
	}

	public Boolean getDateFieldFlag8() {
		return dateFieldFlag8;
	}

	public void setDateFieldFlag8(Boolean dateFieldFlag8) {
		this.dateFieldFlag8 = dateFieldFlag8;
	}

	public Boolean getDateFieldFlag9() {
		return dateFieldFlag9;
	}

	public void setDateFieldFlag9(Boolean dateFieldFlag9) {
		this.dateFieldFlag9 = dateFieldFlag9;
	}

	public Boolean getDateFieldFlag10() {
		return dateFieldFlag10;
	}

	public void setDateFieldFlag10(Boolean dateFieldFlag10) {
		this.dateFieldFlag10 = dateFieldFlag10;
	}

	private void setFlags() {

		if(currentBMDTO.getUvc01Desc()==null || "".equalsIgnoreCase(currentBMDTO.getUvc01Desc())) {
			textFieldFlag1 = false;
		} else
			textFieldFlag1 = true;

		if(currentBMDTO.getUvc02Desc()==null || "".equalsIgnoreCase(currentBMDTO.getUvc02Desc())) {
			textFieldFlag2 = false;
		} else
			textFieldFlag2 = true;

		if(currentBMDTO.getUvc03Desc()==null || "".equalsIgnoreCase(currentBMDTO.getUvc03Desc())) {
			textFieldFlag3 = false;
		} else
			textFieldFlag3 = true;

		if(currentBMDTO.getUvc04Desc()==null || "".equalsIgnoreCase(currentBMDTO.getUvc04Desc())) {
			textFieldFlag4 = false;
		} else
			textFieldFlag4 = true;

		if(currentBMDTO.getUvc05Desc()==null || "".equalsIgnoreCase(currentBMDTO.getUvc05Desc())) {
			textFieldFlag5 = false;
		} else
			textFieldFlag5 = true;

		if(currentBMDTO.getUvc06Desc()==null || "".equalsIgnoreCase(currentBMDTO.getUvc06Desc())) {
			textFieldFlag6 = false;
		} else
			textFieldFlag6 = true;

		if(currentBMDTO.getUvc07Desc()==null || "".equalsIgnoreCase(currentBMDTO.getUvc07Desc())) {
			textFieldFlag7 = false;
		} else
			textFieldFlag7 = true;

		if(currentBMDTO.getUvc08Desc()==null || "".equalsIgnoreCase(currentBMDTO.getUvc08Desc())) {
			textFieldFlag8 = false;
		} else
			textFieldFlag8 = true;

		if(currentBMDTO.getUvc09Desc()==null || "".equalsIgnoreCase(currentBMDTO.getUvc09Desc())) {
			textFieldFlag9 = false;
		} else
			textFieldFlag9 = true;

		if(currentBMDTO.getUvc10Desc()==null || "".equalsIgnoreCase(currentBMDTO.getUvc10Desc())) {
			textFieldFlag10 = false;
		} else
			textFieldFlag10 = true;

		if(currentBMDTO.getUnu01Desc()==null || "".equalsIgnoreCase(currentBMDTO.getUnu01Desc())) {
			numberFieldFlag1 = false;
		} else
			numberFieldFlag1 = true;

		if(currentBMDTO.getUnu02Desc()==null || "".equalsIgnoreCase(currentBMDTO.getUnu02Desc())) {
			numberFieldFlag2 = false;
		} else
			numberFieldFlag2 = true;

		if(currentBMDTO.getUnu03Desc()==null || "".equalsIgnoreCase(currentBMDTO.getUnu03Desc())) {
			numberFieldFlag3 = false;
		} else
			numberFieldFlag3 = true;

		if(currentBMDTO.getUnu04Desc()==null || "".equalsIgnoreCase(currentBMDTO.getUnu04Desc())) {
			numberFieldFlag4 = false;
		} else
			numberFieldFlag4 = true;

		if(currentBMDTO.getUnu05Desc()==null || "".equalsIgnoreCase(currentBMDTO.getUnu05Desc())) {
			numberFieldFlag5 = false;
		} else
			numberFieldFlag5 = true;

		if(currentBMDTO.getUnu06Desc()==null || "".equalsIgnoreCase(currentBMDTO.getUnu06Desc())) {
			numberFieldFlag6 = false;
		} else
			numberFieldFlag6 = true;

		if(currentBMDTO.getUnu07Desc()==null || "".equalsIgnoreCase(currentBMDTO.getUnu07Desc())) {
			numberFieldFlag7 = false;
		} else
			numberFieldFlag7 = true;

		if(currentBMDTO.getUnu08Desc()==null || "".equalsIgnoreCase(currentBMDTO.getUnu08Desc())) {
			numberFieldFlag8 = false;
		} else
			numberFieldFlag8 = true;

		if(currentBMDTO.getUnu09Desc()==null || "".equalsIgnoreCase(currentBMDTO.getUnu09Desc())) {
			numberFieldFlag9 = false;
		} else
			numberFieldFlag9 = true;

		if(currentBMDTO.getUnu10Desc()==null || "".equalsIgnoreCase(currentBMDTO.getUnu10Desc())) {
			numberFieldFlag10 = false;
		} else
			numberFieldFlag10 = true;

		if(currentBMDTO.getUts01Desc()==null || "".equalsIgnoreCase(currentBMDTO.getUts01Desc())) {
			dateFieldFlag1 = false;
		} else
			dateFieldFlag1 = true;

		if(currentBMDTO.getUts02Desc()==null || "".equalsIgnoreCase(currentBMDTO.getUts02Desc())) {
			dateFieldFlag2 = false;
		} else
			dateFieldFlag2 = true;

		if(currentBMDTO.getUts03Desc()==null || "".equalsIgnoreCase(currentBMDTO.getUts03Desc())) {
			dateFieldFlag3 = false;
		} else
			dateFieldFlag3 = true;

		if(currentBMDTO.getUts04Desc()==null || "".equalsIgnoreCase(currentBMDTO.getUts04Desc())) {
			dateFieldFlag4 = false;
		} else
			dateFieldFlag4 = true;

		if(currentBMDTO.getUts05Desc()==null || "".equalsIgnoreCase(currentBMDTO.getUts05Desc())) {
			dateFieldFlag5 = false;
		} else
			dateFieldFlag5 = true;

		if(currentBMDTO.getUts06Desc()==null || "".equalsIgnoreCase(currentBMDTO.getUts06Desc())) {
			dateFieldFlag6 = false;
		} else
			dateFieldFlag6 = true;

		if(currentBMDTO.getUts07Desc()==null || "".equalsIgnoreCase(currentBMDTO.getUts07Desc())) {
			dateFieldFlag7 = false;
		} else
			dateFieldFlag7 = true;

		if(currentBMDTO.getUts08Desc()==null || "".equalsIgnoreCase(currentBMDTO.getUts08Desc())) {
			dateFieldFlag8 = false;
		} else
			dateFieldFlag8 = true;

		if(currentBMDTO.getUts09Desc()==null || "".equalsIgnoreCase(currentBMDTO.getUts09Desc())) {
			dateFieldFlag9 = false;
		} else
			dateFieldFlag9 = true;

		if(currentBMDTO.getUts10Desc()==null || "".equalsIgnoreCase(currentBMDTO.getUts10Desc())) {
			dateFieldFlag10 = false;
		} else
			dateFieldFlag10 = true;
	}

	public List<BatchMetadata> getValidBatchMaintenance() {
		return validBatchMaintenance;
	}

	public void setValidBatchMaintenance(List<BatchMetadata> validBatchMaintenance) {
		this.validBatchMaintenance = validBatchMaintenance;
	}
	
	public boolean getViewAction() {
		return "Update".equalsIgnoreCase(currentAction);
	}
	
	public boolean getViewOKAction() {
		return "View".equals(currentAction) || "viewFromTransaction".equals(this.currentAction) || "viewFromLogHistory".equals(this.currentAction) || "viewFromLogEntry".equals(this.currentAction);
		
	}
	
	private TogglePanelController togglePanelController = null;
	public  TogglePanelController getTogglePanelController(){
		if(togglePanelController==null){
			togglePanelController = new TogglePanelController();
			togglePanelController.addTogglePanel("Batch Information", true);
			togglePanelController.addTogglePanel("Batch Type Specifics", false);
		}
		return togglePanelController;
	}
		
	public void updateBatchTypeCode(ActionEvent event) {
		ValueHolder comboBox = (ValueHolder) event.getComponent().getParent();      
		batchTypeCode =  (String) comboBox .getValue();
		prepareBatchTypeSpecificsFields();
	}
	
	public void prepareBatchTypeSpecificsFields() {
		batchMaintenanceDTO.setBatchTypeCode(batchTypeCode);
		clearbatchTypeSpecificInputFields();
		if(batchTypeCode !=null){
			processBatchTypeSpecificsFields(batchTypeCode);
		}
	}
	
	private void processBatchTypeSpecificsFields(String batchTypeCode) {
		batchTypeSpecificList = new ArrayList<BatchTypeSpecific>();
		BatchMetadata batchMetadata = null;
		batchMetadata = batchMaintenanceService.findBatchMetadataById(batchTypeCode);
		createBatchTypeSpecificList(batchMetadata);
	}
	
	private Method[] orderMethods() {
		Method[] methods =  BatchMetadata.class.getDeclaredMethods();
        Arrays.sort(methods, new Comparator<Method>() {
            @Override
            public int compare(Method o1, Method o2) {
                Order or1 = o1.getAnnotation(Order.class);
                Order or2 = o2.getAnnotation(Order.class);
                if (or1 != null && or2 != null) {
                    return or1.value() - or2.value();
                } else
                if (or1 != null && or2 == null) {
                    return -1;
                } else
                if (or1 == null && or2 != null) {
                    return 1;
                }
                return o1.getName().compareTo(o2.getName());
            }
        });
       return methods;
	}
	
	private void createBatchTypeSpecificList(BatchMetadata batchMetadata) {
		Object paramsObj[] = {};
		Method[] methods = orderMethods();
		int index = 0;
		for (Method m :  methods) {
			String name = m.getName();
			if(name.startsWith("get") && (m.getParameterTypes().length == 0) && name.endsWith("Desc")) {
				try{
					String value = (String)m.invoke(batchMetadata, paramsObj);
					if(value != null) {
						index  = index + 1 ;
						BatchTypeSpecific batchTypeSpecific = new BatchTypeSpecific();
						String batchFieldname;
						String methodName = m.getName();
						
						batchFieldname = methodName.substring(3, methodName.length()-4);
						//if config imp/exp menu then do not display VC01 field as this field is already included in the first accordion : "Configuration Field name "
						if(isCalledFromConfigImportExpMenu()){
							if("vc01".equalsIgnoreCase(batchFieldname)) {
								continue; //do not add it.
							}
						}
						String concatbatchFieldname =  methodName.substring(3, methodName.length()-6);
						String batchFieldtype = null;
						if(concatbatchFieldname.equalsIgnoreCase("vc") || concatbatchFieldname.equalsIgnoreCase("Uvc")) {
							batchFieldtype = "String";
						}
						if(concatbatchFieldname.equalsIgnoreCase("ts") || concatbatchFieldname.equalsIgnoreCase("Uts")) {
							batchFieldtype = "Date";
						}
						if(concatbatchFieldname.equalsIgnoreCase("nu")) {
							batchFieldtype = "Double";
						}
						if(concatbatchFieldname.equalsIgnoreCase("unu")) {
							batchFieldtype= "BigDecimal";
							
						}
						batchTypeSpecific.setId(new StringBuffer(batchFieldname).append("_").append(batchTypeCode).toString());
						
						batchTypeSpecific.setObjectHeading(value);
						batchTypeSpecific.setBatchFieldname(batchFieldname);
						batchTypeSpecific.setBatchFieldtype(batchFieldtype); 
						batchTypeSpecificList.add(batchTypeSpecific);
					}
					
				}catch(Exception e) {
					System.out.println("Exception occured.."+e.getMessage());
					
				}
			}
		}
	}
	public String batchSpecificAction() {
		return "data_utility_batch";
	}
	public String resetBatchFields(){
		resetSearchAction();
		return null;
	}	
	
	public User getCurrentUser() {
		return lgnBean.getUser();
   }
	
	public Boolean getDisplayKillProcessButton() {
		int currUserAdminFlag =  Integer.parseInt(getCurrentUser().getAdminFlag());
		if(USER_STSCORP.equals(getCurrentUser().getUserCode())) { 
			if(selBatchMaintenance.getBatchStatusCode().equals("XP") &&( selBatchMaintenance.getBatchTypeCode().equals("PCO") || 
					selBatchMaintenance.getBatchTypeCode().equals("IP"))){
				return false;
			}
			else {
				return true;
			}
		}
		else if(currUserAdminFlag != 2) {
			return true;
		}
		else if(selBatchMaintenance.getBatchStatusCode().equals("XP") &&( selBatchMaintenance.getBatchTypeCode().equals("PCO") || 
				selBatchMaintenance.getBatchTypeCode().equals("IP"))){
			return false;
		} 
		else {
			return true;
		}
	}
	
	
	public String displayKillProcessAction(){
		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm a");
		Date date = new Date();
		try{
			setKillBatchScheduleTime(dateFormat.parse(dateFormat.format(date))); //setting current date
		}catch(Exception pe){
			logger.error("exception occiured while setting Kill proceessing start date time");
		}
		
		return "batch_kill_process";
	}
	
	public String killBatchProcessAction() {
		selBatchMaintenance.setBatchStatusCode(BATCH_KILL_PROCESS_STATUS_CODE);
		batchMaintenanceService.update(selBatchMaintenance);
		retrieveBatchMaintenance();
		return "batch_maintenance_main";
	}
	
	public Boolean getDisableUpdateAction() {
		if(getIsUpdateAction()) {
			if(USER_STSCORP.equals(getCurrentUser().getUserCode())) { 
				return false;
			}
			return true;
		}
		return true;
	}
	 public boolean isNavigatedFlag() {
	        return true;
	    }

     public void setNavigatedFlag(boolean navigatedFlag) {}
	    
	
}
