package com.ncsts.view.bean;

import java.util.ArrayList;
import java.util.List;

import javax.faces.component.UIInput;
import javax.faces.component.UISelectItems;
import javax.faces.component.html.HtmlInputHidden;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.ajax4jsf.component.html.HtmlAjaxSupport;
import org.apache.log4j.Logger;
import org.richfaces.component.html.HtmlDataTable;

import com.ncsts.common.CacheManager;
import com.ncsts.common.LoggerFactory;
import com.ncsts.domain.Jurisdiction;
import com.ncsts.domain.JurisdictionTaxrate;
import com.ncsts.dto.TaxCodeStateDTO;
import com.ncsts.services.JurisdictionService;

public class SearchJurisdictionHandler implements BaseMatrixBackingBean.FindIdCallback {
	
	public static String GEOCODE = "GeoCode";
	public static String CITY = "City";
	public static String COUNTY = "County";
	public static String STATE = "State";
	public static String COUNTRY = "Country";
	public static String ZIPCODE = "ZipCode";
	public static String PLUS4 = "PLUS4";
	public static String ALL = "All";
	

	public interface SearchJurisdictionCallback {
		void searchJurisdictionCallback();
	}
	
	public interface SearchJurisdictionCallbackSTJ {
		void searchJurisdictionCallbackSTJ();
	}
	
	public interface SearchJurisdictionCallbackExt {
		void searchJurisdictionCallbackExt(SearchJurisdictionHandler searchJurisdictionHandler);
	}
	
	public interface SearchJurisdictionCallbackLocation {
		void searchJurisdictionCallbackLocation(SearchJurisdictionHandler searchJurisdictionHandler, String componentName);
	}
	
	public interface SearchJurisdictionIdCallback {
		Long searchJurisdictionIdCallback(SearchJurisdictionHandler searchJurisdictionHandler);
	}

   	static private Logger logger = LoggerFactory.getInstance().getLogger(SearchJurisdictionHandler.class);
   	
   	private HtmlInputText geocodeInput = new HtmlInputText();
   	private HtmlInputText cityInput = new HtmlInputText();
   	private HtmlInputText countyInput = new HtmlInputText();
   	private HtmlSelectOneMenu stateInput = new HtmlSelectOneMenu();
   	private HtmlSelectOneMenu countryInput = new HtmlSelectOneMenu();
   	private HtmlInputText zipInput = new HtmlInputText();
   	private HtmlInputText zipPlus4Input = new HtmlInputText();
   	private HtmlInputText jurisdictionIdInput = new HtmlInputText();
   	private HtmlInputText stj1NameInput = new HtmlInputText();
   	private HtmlInputText stj2NameInput = new HtmlInputText();
   	private HtmlInputText stj3NameInput = new HtmlInputText();
   	private HtmlInputText stj4NameInput = new HtmlInputText();
   	private HtmlInputText stj5NameInput = new HtmlInputText();
   	private HtmlSelectOneMenu entityCodeInput =  new HtmlSelectOneMenu();
   	
   	private String geocode;
   	private String city;
   	private String county;
   	private String state;
   	private String country;
   	private String zip;
   	private String zipPlus4;
   	private String jurisdictionId;
   	private String stj1Name;
   	private String stj2Name;
   	private String stj3Name;
   	private String stj4Name;
   	private String stj5Name;
   	private String entityCode;
   	
   	
	private Long extJurisdictionId;
   	
   	private boolean zipPlus4Enabled = false;
   	
	private List<JurisdictionTaxrate> taxRateList = new ArrayList<JurisdictionTaxrate>();
    private HtmlInputHidden taxRatePanel;

	private JurisdictionService jurisdictionService;
	private CacheManager cacheManager;
	private TaxJurisdictionBackingBean taxJurisdictionBean;
	
	private Long searchJurisdictionResultId;
	private List<Jurisdiction> jurisdictionList;
	
	private SearchJurisdictionHandler popupHandler;
	private SearchJurisdictionCallback callback;
	private SearchJurisdictionCallbackExt callbackExt;
	private SearchJurisdictionCallbackLocation callbackLocation;
	private SearchJurisdictionIdCallback callbackId;
	private SearchJurisdictionCallbackSTJ callbackSTJ;
	
	private boolean refreshState = false;
	private Jurisdiction selectedJurisdiction;
	private boolean isTxnLocnEditScreen = false;
	private boolean isBillingTestToolScreen = false;

	public HtmlSelectOneMenu getEntityCodeInput() {
		return entityCodeInput;
	}

	public void setEntityCodeInput(HtmlSelectOneMenu entityCodeInput) {
		this.entityCodeInput = entityCodeInput;
	}

	public String getEntityCode() {
		return entityCode;
	}

	public void setEntityCode(String entityCode) {
		this.entityCode = entityCode;
		setInputValue(entityCodeInput, entityCode);
	}

	public boolean isTxnLocnEditScreen() {
		return isTxnLocnEditScreen;
	}

	public void setTxnLocnEditScreen(boolean isTxnLocnEditScreen) {
		this.isTxnLocnEditScreen = isTxnLocnEditScreen;
	}
	
	public boolean isBillingTestToolScreen() {
		return isBillingTestToolScreen;
	}
	
	public void setBillingTestToolScreen(boolean isBillingTestToolScreen) {
		this.isBillingTestToolScreen = isBillingTestToolScreen;
	}

	public SearchJurisdictionHandler(){
		this.setJurisdiction(null);
	}

	public SearchJurisdictionHandler(boolean zipPlus4Enabled) {
		this.setZipPlus4Enabled(zipPlus4Enabled);
		this.setJurisdiction(null);
	} 

	public void setChanged(){
		refreshState = true;
		
		if (popupHandler != null){
			popupHandler.setChanged();
		}
	}
	
	public SearchJurisdictionCallback getCallback() {
		return callback;
	}
	
	public SearchJurisdictionCallbackExt getCallbackExt() {
		return callbackExt;
	}

	public void setCallbackExt(SearchJurisdictionCallbackExt callbackExt) {
		this.callbackExt = callbackExt;
	}
	
	public SearchJurisdictionCallbackLocation getCallbackLocation() {
		return callbackLocation;
	}

	public void setCallbackLocation(SearchJurisdictionCallbackLocation callbackLocation) {
		this.callbackLocation = callbackLocation;
	}
	
	public void setCallback(SearchJurisdictionCallback callback) {
		this.callback = callback;
	}

	public void setCallbackId(SearchJurisdictionIdCallback callbackId) {
		this.callbackId = callbackId;
	}
	
	public void setCallbackSTJ(SearchJurisdictionCallbackSTJ callbackSTJ) {
		this.callbackSTJ = callbackSTJ;
	}

	public SearchJurisdictionHandler getPopupHandler() {
		if (popupHandler == null) {
			popupHandler = new SearchJurisdictionHandler(zipPlus4Enabled);
			popupHandler.setJurisdictionService(jurisdictionService);
			popupHandler.setCacheManager(cacheManager);
		}
		return popupHandler;
	}

	public HtmlInputText getGeocodeInput() {
		return geocodeInput;
	}

	public void setGeocodeInput(HtmlInputText geocodeInput) {
		this.geocodeInput = geocodeInput;
	}

	public HtmlInputText getCityInput() {
		return cityInput;
	}

	public void setCityInput(HtmlInputText cityInput) {
		this.cityInput = cityInput;
	}

	public HtmlInputText getCountyInput() {
		return countyInput;
	}

	public void setCountyInput(HtmlInputText countyInput) {
		this.countyInput = countyInput;
	}
	
	public void plus4Changed(ActionEvent e) {
		String newPlus4 = (String) ((HtmlInputText) ((HtmlAjaxSupport) e.getComponent()).getParent()).getSubmittedValue();
		
		setZipPlus4(newPlus4);
		
		if (callbackLocation != null) {
			callbackLocation.searchJurisdictionCallbackLocation(this, SearchJurisdictionHandler.PLUS4);
		}
	}
	
	public void zipChanged(ActionEvent e) {
		String newZip = (String) ((HtmlInputText) ((HtmlAjaxSupport) e.getComponent()).getParent()).getSubmittedValue();
		
		setZip(newZip);
		
		if (callbackLocation != null) {
			callbackLocation.searchJurisdictionCallbackLocation(this, SearchJurisdictionHandler.ZIPCODE);
		}
	}
	
	public void geocodeChanged(ActionEvent e) {
		String newGeocode = (String) ((HtmlInputText) ((HtmlAjaxSupport) e.getComponent()).getParent()).getSubmittedValue();
		
		setGeocode(newGeocode);
		
		if (callbackLocation != null) {
			callbackLocation.searchJurisdictionCallbackLocation(this, SearchJurisdictionHandler.GEOCODE);
		}
	}
	
	public void cityChanged(ActionEvent e) {
		String newCity = (String) ((HtmlInputText) ((HtmlAjaxSupport) e.getComponent()).getParent()).getSubmittedValue();
		
		setCity(newCity);
		
		if (callbackLocation != null) {
			callbackLocation.searchJurisdictionCallbackLocation(this, SearchJurisdictionHandler.CITY);
		}
	}
	
	public void countyChanged(ActionEvent e) {
		String newCounty = (String) ((HtmlInputText) ((HtmlAjaxSupport) e.getComponent()).getParent()).getSubmittedValue();
		
		setCounty(newCounty);
		
		if (callbackLocation != null) {
			callbackLocation.searchJurisdictionCallbackLocation(this, SearchJurisdictionHandler.COUNTY);
		}
	}

	public void stateSelected(ActionEvent e) {
		String newState = (String) ((HtmlSelectOneMenu) ((HtmlAjaxSupport) e.getComponent()).getParent()).getSubmittedValue();
		
		setState(newState);
		
		if (callbackLocation != null) {
			callbackLocation.searchJurisdictionCallbackLocation(this, SearchJurisdictionHandler.STATE);
		}
	}
	
	public void countrySelected(ActionEvent e) {
		String newCountry = (String) ((HtmlSelectOneMenu) ((HtmlAjaxSupport) e.getComponent()).getParent()).getSubmittedValue();
		
		refreshState=true;
		state="";
		
		setCountry(newCountry);
		setState("");
		
		if (callback != null) {
			callback.searchJurisdictionCallback();
		}
		
		if (callbackLocation != null) {
			callbackLocation.searchJurisdictionCallbackLocation(this, SearchJurisdictionHandler.COUNTRY);
		}
	}
	
	/*
	public List<SelectItem> getStateItems() {
		//if(stateMenuItems==null){
		//	stateMenuItems = cacheManager.createStateItems(country);
		//}
		
		//return stateMenuItems;
		return cacheManager.createStateItems(country);
    }
	*/
	
	public List<SelectItem> getStateItems() {
		if(stateMenuItems==null){
			stateMenuItems = cacheManager.createStateItems(country);
		}
		
		return stateMenuItems;
    }
	
	private List<SelectItem> stateMenuItems;
	
	public HtmlSelectOneMenu getCountryInput() {
		if (this.cacheManager != null){
			List<SelectItem> activeCountryItems = cacheManager.createCountryItems();
			UISelectItems items = new UISelectItems();
			items.setValue(activeCountryItems);
			countryInput.getChildren().clear();
			countryInput.getChildren().add(items);
		}	    
		
		return countryInput;
	}

	public HtmlSelectOneMenu getStateInput() {
		return stateInput;
	}
	
	public void setCountryInput(HtmlSelectOneMenu countryInput) {
		this.countryInput = countryInput;
	}

	public void setStateInput(HtmlSelectOneMenu stateInput) {
		this.stateInput = stateInput;
	}

	public HtmlInputText getZipInput() {
		return zipInput;
	}

	public void setZipInput(HtmlInputText zipInput) {
		this.zipInput = zipInput;
	}
	
	
	public String getGeocode() {
		return trimAndUpper(geocode);
	}

	public void setGeocode(String geocode) {
		this.geocode = geocode;
		setInputValue(geocodeInput, geocode);
	}

	public String getCity() {
		return trimAndUpper(city);
	}

	public void setCity(String city) {
		this.city = city;
		setInputValue(cityInput, city);
	}

	public String getCounty() {
		return trimAndUpper(county);
	}

	public void setCounty(String county) {
		this.county = county;
		setInputValue(countyInput, county);
	}

	public String getState() {
		return trimAndUpper(state);
	}
	
	public String getStateDescription() {
		if(state!=null && state.length()>0){
			
			TaxCodeStateDTO aTaxCodeStateDTO = cacheManager.getTaxCodeStateMap().get(state);
			if(aTaxCodeStateDTO!=null){
				return aTaxCodeStateDTO.getName() + " (" + state + ")";
			}
			else{
				return " (" + state + ")";	
			}
		}
		else{
			return "";
		}
	}

	public void setState(String state) {
		this.state = state;
		setInputValue(stateInput, state);
	}

	public String getCountry() {
		return trimAndUpper(country);
	}
	
	public String getCountryDescription() {
		if(country!=null && country.length()>0){
			String countryDesc = cacheManager.getCountryMap().get(country);
			if (countryDesc == null) {
				return country;
			}
			else {
				return countryDesc + " (" + country + ")";
			
			}
		}
		else{
			return "";
		}
	}

	public void setCountry(String country) {
		this.country = country;
		setInputValue(countryInput, country);
		
		stateMenuItems = null;
	}
	
	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
		setInputValue(zipInput, zip);
	}
	
	public HtmlInputText getJurisdictionIdInput() {
		return jurisdictionIdInput;
	}

	public void setJurisdictionIdInput(HtmlInputText jurisdictionIdInput) {
		this.jurisdictionIdInput = jurisdictionIdInput;
	}

	public HtmlInputText getStj1NameInput() {
		return stj1NameInput;
	}

	public void setStj1NameInput(HtmlInputText stj1NameInput) {
		this.stj1NameInput = stj1NameInput;
	}

	public HtmlInputText getStj2NameInput() {
		return stj2NameInput;
	}

	public void setStj2NameInput(HtmlInputText stj2NameInput) {
		this.stj2NameInput = stj2NameInput;
	}

	public HtmlInputText getStj3NameInput() {
		return stj3NameInput;
	}

	public void setStj3NameInput(HtmlInputText stj3NameInput) {
		this.stj3NameInput = stj3NameInput;
	}

	public HtmlInputText getStj4NameInput() {
		return stj4NameInput;
	}

	public void setStj4NameInput(HtmlInputText stj4NameInput) {
		this.stj4NameInput = stj4NameInput;
	}

	public HtmlInputText getStj5NameInput() {
		return stj5NameInput;
	}

	public void setStj5NameInput(HtmlInputText stj5NameInput) {
		this.stj5NameInput = stj5NameInput;
	}

	public String getJurisdictionId() {
		return jurisdictionId;
	}

	public void setJurisdictionId(String jurisdictionId) {
		this.jurisdictionId = jurisdictionId;
		setInputValue(jurisdictionIdInput, jurisdictionId);
	}

	public String getStj1Name() {
		return stj1Name;
	}

	public void setStj1Name(String stj1Name) {
		this.stj1Name = stj1Name;
		setInputValue(stj1NameInput, stj1Name);
	}

	public String getStj2Name() {
		return stj2Name;
	}

	public void setStj2Name(String stj2Name) {
		this.stj2Name = stj2Name;
		setInputValue(stj2NameInput, stj2Name);
	}

	public String getStj3Name() {
		return stj3Name;
	}

	public void setStj3Name(String stj3Name) {
		this.stj3Name = stj3Name;
		setInputValue(stj3NameInput, stj3Name);
	}

	public String getStj4Name() {
		return stj4Name;
	}

	public void setStj4Name(String stj4Name) {
		this.stj4Name = stj4Name;
		setInputValue(stj4NameInput, stj4Name);
	}

	public String getStj5Name() {
		return stj5Name;
	}

	public void setStj5Name(String stj5Name) {
		this.stj5Name = stj5Name;
		setInputValue(stj5NameInput, stj5Name);
	}

	/**
   * @return the taxRateList
   */
  public List<JurisdictionTaxrate> getTaxRateList() {
    return this.taxRateList;
  }

  /**
   * @param taxRateList the taxRateList to set
   */
  public void setTaxRateList(List<JurisdictionTaxrate> taxRateList) {
    this.taxRateList = taxRateList;
  }

  /**
   * @return the taxRatePanel
   */
  public HtmlInputHidden getTaxRatePanel() {
    return this.taxRatePanel;
  }

  /**
   * @param taxRatePanel the taxRatePanel to set
   */
  public void setTaxRatePanel(HtmlInputHidden taxRatePanel) {
    this.taxRatePanel = taxRatePanel;
  }

  public void setCacheManager(CacheManager cacheManager) {
		if (this.cacheManager == null) {
			this.cacheManager = cacheManager;
			
			/*
			List<SelectItem> activeStateItems = new ArrayList<SelectItem>();
			for (TaxCodeStateDTO tcs : cacheManager.getTaxCodeStateMap().values()) {
				// Only add states that are active
				String active = tcs.getActiveFlag();
				if ((active != null) && active.equals("1")) {
					activeStateItems.add(new SelectItem(tcs.getTaxCodeState(), tcs.getName()));
				}
			}
			activeStateItems.add(0,new SelectItem("", "Select State"));

			UISelectItems items = new UISelectItems();
			items.setValue(activeStateItems);
			stateInput.getChildren().clear();
			stateInput.getChildren().add(items);*/
		}
	}

	public void setJurisdictionService(JurisdictionService jurisdictionService) {
		this.jurisdictionService = jurisdictionService;
	}
	
	public void setJurisdiction(Jurisdiction jurisdiction) {
		setGeocode((jurisdiction == null)? null:jurisdiction.getGeocode());
		setCity((jurisdiction == null)? null:jurisdiction.getCity());
		setCounty((jurisdiction == null)? null:jurisdiction.getCounty());
		setState((jurisdiction == null)? null:jurisdiction.getState());
		setCountry((jurisdiction == null)? null:jurisdiction.getCountry());
		setZip((jurisdiction == null)? null:jurisdiction.getZip());
		setZipPlus4((jurisdiction == null)? null:jurisdiction.getZipplus4());
		
		selectedJurisdiction = jurisdiction;
	}
	
	public Jurisdiction getSelectedJurisdiction(){
		if(selectedJurisdiction==null){
			selectedJurisdiction = new Jurisdiction();  
		}
		return selectedJurisdiction;
	}
	
	public void setSelectedJurisdiction(Jurisdiction selectedJurisdiction){
		this.selectedJurisdiction = selectedJurisdiction;
	}
	
	public void reset() {
		setGeocode("");
		setCity("");
		setCounty("");
		setState("");
		setCountry("");
		setZip("");
		setZipPlus4("");
		jurisdictionList = null;
		searchJurisdictionResultId = null;
        taxRateList.clear();
	}
	
	public Long getSearchJurisdictionResultId() {
		return searchJurisdictionResultId;
	}
	
	public void setSearchJurisdictionResultId(Long searchJurisdictionResultId) {
		this.searchJurisdictionResultId = searchJurisdictionResultId;
	}
	
	public Long getExternalJurisdictionId() {
		if (callbackId!= null) {
			return	callbackId.searchJurisdictionIdCallback(this);
		}
		else{
			return null;
		}
	}

	public List<Jurisdiction> getJurisdictionList() {
		return jurisdictionList;
	}
	
	//4235
	private String callbackScreen = "";
	
	public String findJurisdictionIdAction() {
		Jurisdiction jurisdictionFilter = new Jurisdiction();
		jurisdictionFilter.setGeocode(getInputValue(geocodeInput));		
		jurisdictionFilter.setCountry(getInputValue(countryInput));
		jurisdictionFilter.setCity(getInputValue(cityInput));
		jurisdictionFilter.setCounty(getInputValue(countyInput));
		jurisdictionFilter.setState(getInputValue(stateInput));
		jurisdictionFilter.setZip(getInputValue(zipInput));
		jurisdictionFilter.setZipplus4((getInputValue(zipPlus4Input)));
		taxJurisdictionBean.findId(this, "jurisdictionId", callbackScreen, jurisdictionFilter);
		return "maintanence_tax_rates";
	}
	
	public String findTxnVendorJurisAction() {
		Jurisdiction jurisdictionFilter = new Jurisdiction();
		jurisdictionFilter.setCountry(getInputValue(countryInput));
		jurisdictionFilter.setCity(getInputValue(cityInput));
		jurisdictionFilter.setCounty(getInputValue(countyInput));
		jurisdictionFilter.setState(getInputValue(stateInput));
		jurisdictionFilter.setZip(getInputValue(zipInput));
		taxJurisdictionBean.findId(this, "jurisdictionId", callbackScreen, jurisdictionFilter);
		return "maintanence_tax_rates";
	}
	
	public void findIdCallback(Long id, String context) {
		if (context.equalsIgnoreCase("jurisdictionId")) {
			Jurisdiction selectedJurisdiction = jurisdictionService.findById(id);
			setJurisdiction(selectedJurisdiction);
			if(isTxnLocnEditScreen() == true || isBillingTestToolScreen() == true) {
				setJurisdictionIdAndSTJNames(selectedJurisdiction);
			}
			
		}
		
		if (callbackSTJ != null) {
			callbackSTJ.searchJurisdictionCallbackSTJ();
		}
	}
	
	public void setJurisdictionIdAndSTJNames(Jurisdiction jurisdiction) {
		setJurisdictionId((jurisdiction == null)? null:(jurisdiction.getJurisdictionId()!=null ? jurisdiction.getJurisdictionId().toString() : null)); 
		setStj1Name((jurisdiction == null)? null:jurisdiction.getStj1Name());
		setStj2Name((jurisdiction == null)? null:jurisdiction.getStj2Name());
		setStj3Name((jurisdiction == null)? null:jurisdiction.getStj3Name());
		setStj4Name((jurisdiction == null)? null:jurisdiction.getStj4Name());
		setStj5Name((jurisdiction == null)? null:jurisdiction.getStj5Name());
		
	}
	
	public void setTaxJurisdictionBean(TaxJurisdictionBackingBean taxJurisdictionBean) {
		this.taxJurisdictionBean = taxJurisdictionBean;
	}

	public void setCallbackScreen(String callbackScreen) {
		this.callbackScreen = callbackScreen;
	}

	public void popupSearchAction(ActionEvent e) {
		SearchJurisdictionHandler popupHandler = getPopupHandler();
		popupHandler.setGeocode(getInputValue(geocodeInput));
		popupHandler.setCity(getInputValue(cityInput));
		popupHandler.setCounty(getInputValue(countyInput));
		popupHandler.setState(getInputValue(stateInput));
		popupHandler.setCountry(getInputValue(countryInput));
		popupHandler.setZip(getInputValue(zipInput));
		popupHandler.setZipPlus4(getInputValue(zipPlus4Input));
		
		Long externalId = getExternalJurisdictionId();
		if(externalId!=null){
			popupHandler.setExtJurisdictionId(externalId);
		}
		else{
			popupHandler.setExtJurisdictionId(null);
		}
			
		popupHandler.initializeSearch();
	}
	
	public void setStateToPopUp(ActionEvent e){
		SearchJurisdictionHandler popupHandler = getPopupHandler();
		popupHandler.setState(getInputValue(stateInput));
		popupHandler.setCountry(getInputValue(countryInput));
		popupHandler.jurisdictionList = new ArrayList<Jurisdiction>();
		popupHandler.taxRateList = new ArrayList<JurisdictionTaxrate>();
	}
	
	protected void initializeSearch() {
		jurisdictionList = performSearch();
		searchJurisdictionResultId = null;
    taxRateList.clear();
	}
	
	public void updateJurisdictionListener(ActionEvent e) {
		if (popupHandler != null) {
			searchJurisdictionResultId = popupHandler.getSearchJurisdictionResultId();
			// TODO we could optimize by getting object from list in popup handler
			setJurisdiction(jurisdictionService.findById(searchJurisdictionResultId));
			popupHandler.reset();
			
			if (callback != null) {
				callback.searchJurisdictionCallback();
			}
			if (callbackExt != null) {
				callbackExt.searchJurisdictionCallbackExt(this);
			}
			if (callbackLocation != null) {
				callbackLocation.searchJurisdictionCallbackLocation(this, SearchJurisdictionHandler.ALL);
			}
		}
	}
	
	public void jurisdictionClearAction(ActionEvent e) {
		reset();
	}
	
	public void jurisdictionSearchAction(ActionEvent e) {
		initializeSearch();
	}
	
	public String getInputValueOfGeocode(){
		return getInputValue(geocodeInput);
	}
	
	public String getInputValueOfCity(){
		return getInputValue(cityInput);
	}
	
	public String getInputValueOfCounty(){
		return getInputValue(countyInput);
	}
	
	public String getInputValueOfState(){
		return getInputValue(stateInput);
	}

	public String getInputValueOfCountry(){
		return getInputValue(countryInput);
	}
	
	public String getInputValueOfZip(){
		return getInputValue(zipInput);
	}
	
	public String getInputValueOfZipPlus4(){
		return getInputValue(zipPlus4Input);
	}
	
	public boolean getHasCountryExceptInput() {
		//Populate data from UI
		geocode=getInputValue(geocodeInput);
		city=getInputValue(cityInput);
		county=getInputValue(countyInput);
		state=getInputValue(stateInput);
		zip=getInputValue(zipInput);
		zipPlus4=getInputValue(zipPlus4Input);
		
		return (((geocode != null) && !geocode.equals("")) || 
				((city != null) && !city.equals("")) ||
				((county != null) && !county.equals("")) ||
				((state != null) && !state.equals("")) ||
				((zip != null) && !zip.equals("")) ||
				((zipPlus4 != null) && !zipPlus4.equals("")));
	}
	
	public boolean getHasSomeInput() {
		//Populate data from UI
		geocode=getInputValue(geocodeInput);
		city=getInputValue(cityInput);
		county=getInputValue(countyInput);
		state=getInputValue(stateInput);
		country=getInputValue(countryInput);
		zip=getInputValue(zipInput);
		zipPlus4=getInputValue(zipPlus4Input );
		
		return (((geocode != null) && !geocode.equals("")) || 
				((city != null) && !city.equals("")) ||
				((county != null) && !county.equals("")) ||
				((state != null) && !state.equals("")) ||
				((country != null) && !country.equals("")) ||
				((zip != null) && !zip.equals("")) ||
				((zipPlus4 != null) && !zipPlus4.equals("")));
	}
	
	public boolean getHasAllInput() {
		//Populate data from UI
		String geocode=getInputValue(geocodeInput);
		String city=getInputValue(cityInput);
		String county=getInputValue(countyInput);
		String state=getInputValue(stateInput);
		String country=getInputValue(countryInput);
		String zip=getInputValue(zipInput);
		//String zipPlus4=getInputValue(zipPlus4Input)
		
		return (((geocode != null) && !geocode.equals("")) && 
				((city != null) && !city.equals("")) &&
				((county != null) && !county.equals("")) &&
				((state != null) && !state.equals("")) &&
				((country != null) && !country.equals("")) &&
				((zip != null) && !zip.equals(""))); //No need to include ZipPlus4 when verifying
	}
	
	public Jurisdiction getJurisdiction() {
		Jurisdiction result = new Jurisdiction();
		result.setGeocode(getGeocode());
		result.setCity(getCity());
		result.setCounty(getCounty());
		result.setState(getState());
		result.setCountry(getCountry());
		result.setZip(getZip());
		//result.setZipplus4(zipPlus4); //PP-165
		
		if(extJurisdictionId!=null){
			result.setJurisdictionId(extJurisdictionId);
		}
		
		return result;
	}
	
	public Jurisdiction getJurisdictionFromDB() {
		if (getHasAllInput()) {
			List<Jurisdiction> list = jurisdictionService.searchByExample(getJurisdiction(), true);
			if ((list != null) && (list.size() == 1)) {
				return jurisdictionService.findById(list.get(0).getJurisdictionId());
			} else if ((list == null) || (list.size() == 0)) {
				logger.warn("Jurisdiction not found");
			} else {
				logger.warn("Multiple jurisdictions found");
			}
		}
		
		return null;
	}
	
	public boolean getIsValidPartialJurisdiction() {
		Long count = jurisdictionService.partialJurisdictionCount(getJurisdiction());
		if(count.intValue()>0){
			return true;
		}
		else{
			return false;
		}
	}
	public boolean getValidSearchJurisdictionResult() {
		return (searchJurisdictionResultId != null);
	}

	@SuppressWarnings("unchecked")
	public void selectJurisdiction(ActionEvent e) { 
	    HtmlDataTable table = (HtmlDataTable) ((HtmlAjaxSupport) e.getComponent()).getParent(); 
	    searchJurisdictionResultId = ((Jurisdiction) table.getRowData()).getId();
		logger.debug("Selected Jurisdiction ID: " + ((searchJurisdictionResultId == null)? "NULL":searchJurisdictionResultId));		
    if (taxRatePanel != null) updateTaxRates(e);
	}

	public void updateTaxRates(ActionEvent e) throws AbortProcessingException {
			JurisdictionTaxrate sampleTaxrate = new JurisdictionTaxrate();
			
			sampleTaxrate.setJurisdiction(new Jurisdiction(searchJurisdictionResultId));
			taxRateList = this.jurisdictionService.findByIdAndDate(sampleTaxrate);
			logger.debug("Number of tax rates for Jurisdiction ID " + searchJurisdictionResultId + ": " + taxRateList.size());
	}

	private List<Jurisdiction> performSearch() {
		if (getHasSomeInput()) {
			return jurisdictionService.searchByExample(getJurisdiction());
		}
		
		return null;
	}
	
	private void setInputValue(UIInput input, String value) {
		input.setLocalValueSet(false);
		input.setSubmittedValue(null);
		input.setValue(value);
	}
	
	private String getInputValue(UIInput input) {
		String value = (String) input.getSubmittedValue();
		value = (value != null)? value:(String) input.getValue();
		return trimAndUpper(value);
	}

	public HtmlInputText getZipPlus4Input() {
		return zipPlus4Input;
	}

	public void setZipPlus4Input(HtmlInputText zipPlus4Input) {
		this.zipPlus4Input = zipPlus4Input;
	}

	public String getZipPlus4() {
		return zipPlus4;
	}

	public void setZipPlus4(String zipPlus4) {
		this.zipPlus4 = zipPlus4;
		setInputValue(zipPlus4Input, zipPlus4);
	} 
	
	public boolean getZipPlus4Enabled() {
		return zipPlus4Enabled;
	}

	public void setZipPlus4Enabled(boolean zipPlus4Enabled) {
		this.zipPlus4Enabled = zipPlus4Enabled;
	} 
	
	public void setExtJurisdictionId(Long extJurisdictionId){
		this.extJurisdictionId = extJurisdictionId;
	}
	
	public String trimAndUpper(String value){
		if(value==null){
			return "";
		}
		else{
			return value.trim().toUpperCase();
		}
	}
}

