package com.ncsts.view.bean;

import java.lang.reflect.Method;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.component.html.HtmlSelectBooleanCheckbox;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import org.ajax4jsf.component.html.HtmlAjaxSupport;
import org.richfaces.component.html.HtmlDataTable;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;

import com.ncsts.common.CacheManager;
import com.ncsts.domain.ListCodes;
import com.ncsts.domain.SitusDriver;
import com.ncsts.domain.SitusDriverDetail;
import com.ncsts.domain.SitusMatrix;
import com.ncsts.dto.NexusJurisdictionItemDTO;
import com.ncsts.dto.SitusMatrixDTO;
import com.ncsts.dto.TaxCodeStateDTO;
import com.ncsts.jsf.model.SitusMatrixDataModel;
import com.ncsts.jsf.model.SitusMatrixRulesDataModel;
import com.ncsts.services.JurisdictionService;
import com.ncsts.services.ListCodesService;
import com.ncsts.services.SitusDriverService;
import com.ncsts.services.SitusMatrixService;
import com.ncsts.services.OptionService;

public abstract class SitusBaseMatrixBackingBean {
	private boolean initialized = false;
	
	private SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
	
	private CacheManager cacheManager;
	private SitusDriverService situsDriverService;
	private SitusMatrixService situsMatrixService;
	private FilePreferenceBackingBean filePreferenceBean;
	private ListCodesService listCodesService;
	private OptionService optionService;
	private SitusMatrixDataModel situsMatrixDataModel;
	private SitusMatrixRulesDataModel situsMatrixRulesDataModel;
	
	private EditAction currentAction = EditAction.VIEW;
	private SitusMatrix selectedSitusMatrix = new SitusMatrix();
	private SitusMatrix selectedSitusDetailMatrix = new SitusMatrix();
	private SitusMatrixDTO selectedSitusMatrixDTO = new SitusMatrixDTO();
	
	private SitusMatrix filterSitusMatrix = new SitusMatrix();
	private SitusMatrix headerSitusMatrix = new SitusMatrix();
	private SitusMatrix filterWhatIfSitusMatrix = new SitusMatrix();
	private SitusMatrix filterSitusMatrixSearch = new SitusMatrix();
	
	private int selectedRowIndex = -1;
	private int selectedDetailRowIndex = -1;
	
	private int selectedRulesRowIndex = -1;
	private SitusMatrix selectedRulesSitusMatrix = new SitusMatrix();
	
	private int selectedRulesDetailRowIndex = -1;
	private SitusMatrix selectedRulesDetailSitusMatrix = new SitusMatrix();
	
	private List<SelectItem> filterTransactionTypeCodeItems;
	private List<SelectItem> filterRateTypeCodeItems;
	private List<SelectItem> filterMethodOfDeliveryItems;
	private List<SelectItem> headerTransactionTypeCodeItems;
	private List<SelectItem> headerRateTypeCodeItems;
	private List<SelectItem> headerMethodOfDeliveryItems;
	private List<SelectItem> updateTransactionTypeCodeItems;
	private List<SelectItem> updateRateTypeCodeItems;
	private List<SelectItem> updateMethodOfDeliveryItems;
	private List<SelectItem> situsDriverTypeItems;
	private List<SelectItem> primaryLocationTypeItems;
	private List<SelectItem> secondaryLocationTypeItems;	
	private List<SelectItem> primaryTaxTypeItems;
	private List<SelectItem> secondaryTaxTypeItems;
	
	private Map<String, String > matrixNameMap = null;
	private Map<String, String > situsDriverTypeMap = null;
	private Map<String, String > situsMatrixNameMap = null;
	private Map<String, String > situsMatrixNameUpdateMap = null;
	private Map<String, Boolean > situsMatrixNameMandatoryMap = null;
	private Map<String, String > situsMatrixNameMainMap = null;
	private Map<String, String > transactionTypeMap = null;
	private Map<String, String > ratetypeMap = null;
	private Map<String, String > methodDeliveryMap = null;
	private Map<String, String > taxTypeMap = null;
	
	private SitusMatrixRuleAction currentSitusMatrixRuleAction = SitusMatrixRuleAction.closeMatrixRule;
	private SitusMatrixAction currentMatrixAction = SitusMatrixAction.closeSitusMatrix;
	private SitusMatrix updateSitusMatrixRule = new SitusMatrix();
	
	private List<SitusMatrix> situsMatrixList = new ArrayList<SitusMatrix>();	
	private List<SitusMatrixDTO> situsMatrixDTOList = new ArrayList<SitusMatrixDTO>();	
	private List<SitusMatrix> situsMatrixRulersDetailList = new ArrayList<SitusMatrix>();	
	private List<SitusMatrix> situsMatrixWhatIfList = new ArrayList<SitusMatrix>();	
	
	private SitusRoleAction actionRole = SitusRoleAction.userRole;
	
	private List<SelectItem> matrixSelectItems;
	private List<SelectItem> countryMenuItems;
	private List<SelectItem> stateMenuItems;
	private List<SelectItem> displayMenuItems;
	
	private List<SelectItem> displayJurisdictionLevelItems;
	
	private List<SelectItem> headerCountryMenuItems;
	private List<SelectItem> headerStateMenuItems;
	
	private String selectedDisplay = "";

	private TaxCodeStateDTO selectedState;
	private int selectedStateIndex = -1;
	private NexusJurisdictionItemDTO selectedLevel;
	private int selectedLevelIndex = -1;
	private NexusJurisdictionItemDTO selectedCounty;
	private int selectedCountyIndex = -1;
	private NexusJurisdictionItemDTO selectedCity;
	private int selectedCityIndex = -1;
	private NexusJurisdictionItemDTO selectedStj;
	private int selectedStjIndex = -1;
	
	private Map<String, NexusJurisdictionItemDTO> selectedLevels = new HashMap<String, NexusJurisdictionItemDTO>();
	private Map<String, NexusJurisdictionItemDTO> selectedCounties = new HashMap<String, NexusJurisdictionItemDTO>();
	private Map<String, NexusJurisdictionItemDTO> selectedCities = new HashMap<String, NexusJurisdictionItemDTO>();
	private Map<String, NexusJurisdictionItemDTO> selectedStjs = new HashMap<String, NexusJurisdictionItemDTO>();
	
	@Autowired
	private JurisdictionService jurisdictionService;
	
	private List<NexusJurisdictionItemDTO> countyList;
	private List<NexusJurisdictionItemDTO> cityList;
	private List<NexusJurisdictionItemDTO> stjList;
	private List<NexusJurisdictionItemDTO> levelList;
	
	private Set<Integer> levelRowsToUpdate;
	
	private SearchJurisdictionHandler filterHandler1 = new SearchJurisdictionHandler();
	private SearchJurisdictionHandler filterHandler2 = new SearchJurisdictionHandler();
	private SearchJurisdictionHandler filterHandler3 = new SearchJurisdictionHandler();
	private SearchJurisdictionHandler filterHandler4 = new SearchJurisdictionHandler();
	private SearchJurisdictionHandler filterHandler5 = new SearchJurisdictionHandler();
	private SearchJurisdictionHandler filterHandler6 = new SearchJurisdictionHandler();
	
	public SearchJurisdictionHandler getFilterHandler1() {
		return filterHandler1;
	}
	
	public SearchJurisdictionHandler getFilterHandler2() {
		return filterHandler2;
	}
	
	public SearchJurisdictionHandler getFilterHandler3() {
		return filterHandler3;
	}
	
	public SearchJurisdictionHandler getFilterHandler4() {
		return filterHandler4;
	}
	
	public SearchJurisdictionHandler getFilterHandler5() {
		return filterHandler5;
	}
	
	public SearchJurisdictionHandler getFilterHandler6() {
		return filterHandler6;
	}
	
	public String processDisplayWhatIf() {
		filterWhatIfSitusMatrix = new SitusMatrix();
		filterHandler1.reset();
		filterHandler2.reset();
		filterHandler3.reset();
		filterHandler4.reset();
		filterHandler5.reset();
		filterHandler6.reset();

	    return "situs_matrix_whatif";  	
	}
	
	public String processDisplayAddSitus() {
		if(this.getActionRole().equals(SitusRoleAction.adminRole)){
			return "situs_admin_drivers_main";
		}
		else{
			return "situs_user_drivers_main";
		}	
	}
	
	public void init(){
		filterHandler1.setJurisdictionService(jurisdictionService);
		filterHandler1.setCacheManager(cacheManager);

		filterHandler2.setJurisdictionService(jurisdictionService);
		filterHandler2.setCacheManager(cacheManager);
		
		filterHandler3.setJurisdictionService(jurisdictionService);
		filterHandler3.setCacheManager(cacheManager);
		
		filterHandler4.setJurisdictionService(jurisdictionService);
		filterHandler4.setCacheManager(cacheManager);
		
		filterHandler5.setJurisdictionService(jurisdictionService);
		filterHandler5.setCacheManager(cacheManager);
		
		filterHandler6.setJurisdictionService(jurisdictionService);
		filterHandler6.setCacheManager(cacheManager);
	}
	
	private enum SitusMatrixRuleAction  {
		addMatrixRule,
		dupSystemMatrixRule,
		copyaddMatrixRule,
		updateMatrixRule,
		copyupdateMatrixRule,
		deleteMatrixRule,
		viewMatrixRule,
		closeMatrixRule
	}
	
	private enum SitusMatrixAction  {
		addSitusMatrix,
		copyaddSitusMatrix,
		updateSitusMatrix,
		deleteSitusMatrix,
		closeSitusMatrix
	}
	
	private enum SitusRoleAction  {
		adminRole,
		userRole
	}
	
	//dupSystemMatrixRule
	
	public boolean getIsDupSystemEnabled() {
		return (currentSitusMatrixRuleAction.equals(SitusMatrixRuleAction.dupSystemMatrixRule));
	}
	
	public boolean getIsAddOrCopyAdd() {
		return (currentSitusMatrixRuleAction.equals(SitusMatrixRuleAction.addMatrixRule) || 
				currentSitusMatrixRuleAction.equals(SitusMatrixRuleAction.copyaddMatrixRule));
	}
	
	public boolean getIsRuleUpdate() {
		return currentSitusMatrixRuleAction.equals(SitusMatrixRuleAction.updateMatrixRule);
	}
	
	public boolean getIsRuleCopyUpdate() {
		return currentSitusMatrixRuleAction.equals(SitusMatrixRuleAction.copyupdateMatrixRule);
	}
	
	public boolean getIsRuleViewOnly() {
		return (currentSitusMatrixRuleAction.equals(SitusMatrixRuleAction.deleteMatrixRule) || 
				currentSitusMatrixRuleAction.equals(SitusMatrixRuleAction.viewMatrixRule));
	}
	
	public boolean getIsRuleView() {
		return currentSitusMatrixRuleAction.equals(SitusMatrixRuleAction.viewMatrixRule);
	}
	
	public String getRuleActionText() {
		String label = "";
		if(currentSitusMatrixRuleAction.equals(SitusMatrixRuleAction.addMatrixRule)){
			label = label + "Add a ";
		}
		if(currentSitusMatrixRuleAction.equals(SitusMatrixRuleAction.dupSystemMatrixRule)){
			label = label + "Duplicate a System ";
		}
		else if(currentSitusMatrixRuleAction.equals(SitusMatrixRuleAction.copyaddMatrixRule)){
			label = label + "Copy/Add a ";
		}
		else if(currentSitusMatrixRuleAction.equals(SitusMatrixRuleAction.updateMatrixRule)){
			//admin may update custome
			label = label + "Update a ";
			
			if(isAdminActionRole() && selectedRulesDetailSitusMatrix!=null && selectedRulesDetailSitusMatrix.getCustomBooleanFlag()){
				label = label + "Custom ";
			}
		}
		else if(currentSitusMatrixRuleAction.equals(SitusMatrixRuleAction.copyupdateMatrixRule)){
			label = label + "Copy/Update a ";
		}
		else if(currentSitusMatrixRuleAction.equals(SitusMatrixRuleAction.deleteMatrixRule)){
			label = label + "Delete a ";
			
			if(isAdminActionRole() && selectedRulesDetailSitusMatrix!=null && selectedRulesDetailSitusMatrix.getCustomBooleanFlag()){
				label = label + "Custom ";
			}
		}
		else if(currentSitusMatrixRuleAction.equals(SitusMatrixRuleAction.viewMatrixRule)){
			//user may view system
			label = label + "View a ";
			
			if(isAdminActionRole() && selectedRulesDetailSitusMatrix!=null && selectedRulesDetailSitusMatrix.getCustomBooleanFlag()){
				label = label + "Custom ";
			}
		}
		
		if(isUserActionRole() && selectedRulesDetailSitusMatrix!=null && !selectedRulesDetailSitusMatrix.getCustomBooleanFlag()){
		}
		else if(isUserActionRole()){
			label = label + "Custom ";
		}
		
		return label;
	}
	
	public String getMatrixActionText() {
		String label = "";
		if(currentMatrixAction.equals(SitusMatrixAction.addSitusMatrix)){
			label = label + "Add ";
		}
		else if(currentMatrixAction.equals(SitusMatrixAction.copyaddSitusMatrix)){
			label = label + "Copy/Add ";
		}
		else if(currentMatrixAction.equals(SitusMatrixAction.updateSitusMatrix)){
			label = label + "Update ";
		}
		else if(currentMatrixAction.equals(SitusMatrixAction.deleteSitusMatrix)){
			label = label + "Delete ";
		}
		
		if(isUserActionRole()){
			label = label + "Custom ";
		}
		
		label = label + "Situs Matrix Rules ";

		return label;
	}
	
	public void updateJurisdictionForUpdate(SitusMatrix aSitusMatrix){
		
		/*
		if(selectedLevel!=null && selectedLevel.getName()!=null){
			if(selectedLevel.getName().equalsIgnoreCase("All State")){
				aSitusMatrix.setSitusCounty("*STATE");
				aSitusMatrix.setSitusCity("*STATE");
				aSitusMatrix.setSitusStj("*STATE");
				return;
			}
			else if(selectedLevel.getName().equalsIgnoreCase("All Counties")){
				aSitusMatrix.setSitusCounty("*ALL");
				aSitusMatrix.setSitusCity("*COUNTY");
				aSitusMatrix.setSitusStj("*COUNTY");
				return;
			}
			else if(selectedLevel.getName().equalsIgnoreCase("All Cities")){
				aSitusMatrix.setSitusCounty("*CITY");
				aSitusMatrix.setSitusCity("*ALL");
				aSitusMatrix.setSitusStj("*CITY");
				return;
			}
			else if(selectedLevel.getName().equalsIgnoreCase("All Stjs")){
				aSitusMatrix.setSitusCounty("*STJ");
				aSitusMatrix.setSitusCity("*STJ");
				aSitusMatrix.setSitusStj("*ALL");
				return;
			}
		}
		
		if(selectedCounty!=null && selectedCounty.getName()!=null){
			aSitusMatrix.setSitusCounty(selectedCounty.getName());
			aSitusMatrix.setSitusCity("*COUNTY");
			aSitusMatrix.setSitusStj("*COUNTY");
			return;
		}
		
		if(selectedCity!=null && selectedCity.getName()!=null){
			aSitusMatrix.setSitusCounty("*CITY");
			aSitusMatrix.setSitusCity(selectedCity.getName());
			aSitusMatrix.setSitusStj("*CITY");
			return;
		}
		
		if(selectedStj!=null && selectedStj.getName()!=null){
			aSitusMatrix.setSitusCounty("*STJ");
			aSitusMatrix.setSitusCity("*STJ");
			aSitusMatrix.setSitusStj(selectedStj.getName());
			return;
		}
		
		aSitusMatrix.setSitusCounty("*ALL");
		aSitusMatrix.setSitusCity("*ALL");
		aSitusMatrix.setSitusStj("*ALL");
		*/
		
		return;	
	}
	
	public String processDisplayCommand() {
	    String command = (String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("command");
	    
	    if(command.equalsIgnoreCase("situsAdminMatrix") || command.equalsIgnoreCase("situsUserMatrix")){
	    	if(command.equalsIgnoreCase("situsAdminMatrix")){
	    		setActionRole(SitusRoleAction.adminRole);
	    	}
	    	else{
	    		setActionRole(SitusRoleAction.userRole);
	    	}
	    	
	    	//New Situs Driver may already be added. So refresh the screen.
	    	filterSitusMatrix.setTransactionTypeCode("");
			setInputValue(transactionTypeCodeMenu, "");
			if(filterTransactionTypeCodeItems!=null){
			 	filterTransactionTypeCodeItems.clear();
				filterTransactionTypeCodeItems=null;
			}

			filterSitusMatrix.setMethodDeliveryCode("");
			setInputValue(methodOfDeliveryMenu, "");
			if(filterMethodOfDeliveryItems!=null){
				filterMethodOfDeliveryItems.clear();
				filterMethodOfDeliveryItems=null;
			}

			filterSitusMatrix.setRatetypeCode("");
			setInputValue(rateTypeCodeMenu, "");
			if(filterRateTypeCodeItems!=null){
			 	filterRateTypeCodeItems.clear();
				filterRateTypeCodeItems=null;
			}

			getFilterTransactionTypeCodeItems();
			getFilterRateTypeCodeItems();
			getFilterMethodOfDeliveryItems();
			
			retrieveFilterSitusMatrix();
	    	
	    	return "situs_admin_matrix_main";  	
	    }
	    
	    updateSitusMatrixRule = new SitusMatrix();
    	
    	if(headerTransactionTypeCodeItems!=null){
			headerTransactionTypeCodeItems.clear();
			headerTransactionTypeCodeItems=null;
		}
    	if(headerRateTypeCodeItems!=null){
		 	headerRateTypeCodeItems.clear();
			headerRateTypeCodeItems=null;
		}
    	if(headerMethodOfDeliveryItems!=null){
			headerMethodOfDeliveryItems.clear();
			headerMethodOfDeliveryItems=null;
		}
    	getHeaderTransactionTypeCodeItems();
		getHeaderRateTypeCodeItems();
		getHeaderMethodOfDeliveryItems();
	    
	    SitusMatrixRuleAction action = SitusMatrixRuleAction.valueOf(command);
	    if(action.equals(SitusMatrixRuleAction.addMatrixRule)){    	
	    	//updateSitusMatrixRule = new SitusMatrix();
	    	
	    	//Default to criteria filter
	    	if(filterSitusMatrix.getTransactionTypeCode() != null && filterSitusMatrix.getTransactionTypeCode().length()>0){
	    		updateSitusMatrixRule.setTransactionTypeCode(filterSitusMatrix.getTransactionTypeCode());
			}
			if(filterSitusMatrix.getRatetypeCode() != null  && filterSitusMatrix.getRatetypeCode().length()>0){
				updateSitusMatrixRule.setRatetypeCode(filterSitusMatrix.getRatetypeCode());
			}
			if(filterSitusMatrix.getMethodDeliveryCode()!= null && filterSitusMatrix.getMethodDeliveryCode().length()>0){
				updateSitusMatrixRule.setMethodDeliveryCode(filterSitusMatrix.getMethodDeliveryCode());
			}
			
			//Refresh UI
			setInputValue(transactionTypeCodeMenuHeader, updateSitusMatrixRule.getTransactionTypeCode());
			setInputValue(rateTypeCodeMenuHeader, updateSitusMatrixRule.getRatetypeCode());
			setInputValue(methodOfDeliveryMenuHeader, updateSitusMatrixRule.getMethodDeliveryCode());
			
			if(filterSitusMatrix.getJurLevel()!= null && filterSitusMatrix.getJurLevel().length()>0){
				updateSitusMatrixRule.setJurLevel(filterSitusMatrix.getJurLevel());
			}		
			if(filterSitusMatrix.getSitusCountryCode()!= null && filterSitusMatrix.getSitusCountryCode().length()>0){
				updateSitusMatrixRule.setSitusCountryCode(filterSitusMatrix.getSitusCountryCode());
				
				if(filterSitusMatrix.getSitusStateCode()!= null && filterSitusMatrix.getSitusStateCode().length()>0){
					updateSitusMatrixRule.setSitusStateCode(filterSitusMatrix.getSitusStateCode());
				}
				
				if(headerStateMenuItems!=null) headerStateMenuItems.clear();
				 	headerStateMenuItems = createHeaderStateMenuItems(updateSitusMatrixRule.getSitusCountryCode());
			}
	    	
	    	//Detail
	    	updateSitusMatrixRule.setEffectiveDate(new com.ncsts.view.util.DateConverter().getUserLocalDate());
	    	
	    	try {
	    		updateSitusMatrixRule.setExpirationDate(sdf.parse("12/31/9999"));
	    	}
	    	catch(Exception e){
	    	}
	    	
	    	updateSitusMatrixRule.setActiveBooleanFlag(true);
	    	
	    	//admin <--> user
			if(isUserActionRole()){
				updateSitusMatrixRule.setCustomBooleanFlag(true);
			}
			else{
				updateSitusMatrixRule.setCustomBooleanFlag(false);
			}
			
			//Update country/state combo boxes
			if(headerStateMenuItems!=null) headerStateMenuItems.clear();
			headerStateMenuItems = createHeaderStateMenuItems(updateSitusMatrixRule.getSitusCountryCode());
			
			//Rebuild driver mapping 
			rebuildSitusMatrixNameUpdateMap(updateSitusMatrixRule);
	   	
	    	currentSitusMatrixRuleAction = action;
	    	
	    	return "situs_admin_matrix_rules_update";
	    }
	    else if(action.equals(SitusMatrixRuleAction.dupSystemMatrixRule)){
	    	//updateSitusMatrixRule = new SitusMatrix();
			BeanUtils.copyProperties(selectedRulesDetailSitusMatrix, updateSitusMatrixRule);
			updateSitusMatrixRule.setSitusMatrixId(null);
			
			//Refresh UI
			setInputValue(transactionTypeCodeMenuHeader, updateSitusMatrixRule.getTransactionTypeCode());
			setInputValue(rateTypeCodeMenuHeader, updateSitusMatrixRule.getRatetypeCode());
			setInputValue(methodOfDeliveryMenuHeader, updateSitusMatrixRule.getMethodDeliveryCode());
			
			//admin --> user, the selected rule should be system
			updateSitusMatrixRule.setCustomBooleanFlag(true);
			
			//Update country/state combo boxes
			if(headerStateMenuItems!=null) headerStateMenuItems.clear();
			headerStateMenuItems = createHeaderStateMenuItems(updateSitusMatrixRule.getSitusCountryCode());
			
			//Rebuild driver mapping 
			rebuildSitusMatrixNameUpdateMap(updateSitusMatrixRule);
	    	
	    	currentSitusMatrixRuleAction = action;
	    	return "situs_admin_matrix_rules_update";
	    }
	    else if(action.equals(SitusMatrixRuleAction.copyaddMatrixRule)){
	    	//updateSitusMatrixRule = new SitusMatrix();
			BeanUtils.copyProperties(selectedRulesDetailSitusMatrix, updateSitusMatrixRule);
			updateSitusMatrixRule.setSitusMatrixId(null);
			
			//Refresh UI
			setInputValue(transactionTypeCodeMenuHeader, updateSitusMatrixRule.getTransactionTypeCode());
			setInputValue(rateTypeCodeMenuHeader, updateSitusMatrixRule.getRatetypeCode());
			setInputValue(methodOfDeliveryMenuHeader, updateSitusMatrixRule.getMethodDeliveryCode());
			
			//admin <--> user
			if(isUserActionRole()){
				updateSitusMatrixRule.setCustomBooleanFlag(true);
			}
			else{
				updateSitusMatrixRule.setCustomBooleanFlag(false);
			}
			
			//Update country/state combo boxes
			if(headerStateMenuItems!=null) headerStateMenuItems.clear();
			headerStateMenuItems = createHeaderStateMenuItems(updateSitusMatrixRule.getSitusCountryCode());
			
			//Rebuild driver mapping 
			rebuildSitusMatrixNameUpdateMap(updateSitusMatrixRule);
	    	
	    	currentSitusMatrixRuleAction = action;
	    	return "situs_admin_matrix_rules_update";
	    }
	    else if(action.equals(SitusMatrixRuleAction.updateMatrixRule)){
	    	//updateSitusMatrixRule = new SitusMatrix();
			BeanUtils.copyProperties(selectedRulesDetailSitusMatrix, updateSitusMatrixRule);
			
			//Refresh UI
			setInputValue(transactionTypeCodeMenuHeader, updateSitusMatrixRule.getTransactionTypeCode());
			setInputValue(rateTypeCodeMenuHeader, updateSitusMatrixRule.getRatetypeCode());
			setInputValue(methodOfDeliveryMenuHeader, updateSitusMatrixRule.getMethodDeliveryCode());
			
			//Update country/state combo boxes
			if(headerStateMenuItems!=null) headerStateMenuItems.clear();
			headerStateMenuItems = createHeaderStateMenuItems(updateSitusMatrixRule.getSitusCountryCode());
			
			//Rebuild driver mapping 
			rebuildSitusMatrixNameUpdateMap(updateSitusMatrixRule);
	    	
	    	currentSitusMatrixRuleAction = action;
	    	return "situs_admin_matrix_rules_update";
	    }
	    else if(action.equals(SitusMatrixRuleAction.copyupdateMatrixRule)){
	    	//updateSitusMatrixRule = new SitusMatrix();
			BeanUtils.copyProperties(selectedRulesDetailSitusMatrix, updateSitusMatrixRule);
			updateSitusMatrixRule.setSitusMatrixId(null);
			
			//Refresh UI
			setInputValue(transactionTypeCodeMenuHeader, updateSitusMatrixRule.getTransactionTypeCode());
			setInputValue(rateTypeCodeMenuHeader, updateSitusMatrixRule.getRatetypeCode());
			setInputValue(methodOfDeliveryMenuHeader, updateSitusMatrixRule.getMethodDeliveryCode());
			
			//admin <--> user
			if(isUserActionRole()){
				updateSitusMatrixRule.setCustomBooleanFlag(true);
			}
			else{
				updateSitusMatrixRule.setCustomBooleanFlag(false);
			}
			
			//Update country/state combo boxes
			if(headerStateMenuItems!=null) headerStateMenuItems.clear();
			headerStateMenuItems = createHeaderStateMenuItems(updateSitusMatrixRule.getSitusCountryCode());
			
			//Rebuild driver mapping 
			rebuildSitusMatrixNameUpdateMap(updateSitusMatrixRule);
	    	
	    	currentSitusMatrixRuleAction = action;
	    	return "situs_admin_matrix_rules_update";
	    }
	    else if(action.equals(SitusMatrixRuleAction.deleteMatrixRule)){
	    	//updateSitusMatrixRule = new SitusMatrix();
			BeanUtils.copyProperties(selectedRulesDetailSitusMatrix, updateSitusMatrixRule);
			
			//Refresh UI
			setInputValue(transactionTypeCodeMenuHeader, updateSitusMatrixRule.getTransactionTypeCode());
			setInputValue(rateTypeCodeMenuHeader, updateSitusMatrixRule.getRatetypeCode());
			setInputValue(methodOfDeliveryMenuHeader, updateSitusMatrixRule.getMethodDeliveryCode());
			
			//Update country/state combo boxes
			if(headerStateMenuItems!=null) headerStateMenuItems.clear();
			headerStateMenuItems = createHeaderStateMenuItems(updateSitusMatrixRule.getSitusCountryCode());
	    	
			//Rebuild driver mapping 
			rebuildSitusMatrixNameUpdateMap(updateSitusMatrixRule);
			
	    	currentSitusMatrixRuleAction = action;
	    	return "situs_admin_matrix_rules_update";
	    }
	    else if(action.equals(SitusMatrixRuleAction.viewMatrixRule)){
	    	//updateSitusMatrixRule = new SitusMatrix();
			BeanUtils.copyProperties(selectedRulesDetailSitusMatrix, updateSitusMatrixRule);
			
			//Refresh UI
			setInputValue(transactionTypeCodeMenuHeader, updateSitusMatrixRule.getTransactionTypeCode());
			setInputValue(rateTypeCodeMenuHeader, updateSitusMatrixRule.getRatetypeCode());
			setInputValue(methodOfDeliveryMenuHeader, updateSitusMatrixRule.getMethodDeliveryCode());
			
			//Update country/state combo boxes
			if(headerStateMenuItems!=null) headerStateMenuItems.clear();
			headerStateMenuItems = createHeaderStateMenuItems(updateSitusMatrixRule.getSitusCountryCode());
			
			//Rebuild driver mapping 
			rebuildSitusMatrixNameUpdateMap(updateSitusMatrixRule);
	    	
	    	currentSitusMatrixRuleAction = action;
	    	return "situs_admin_matrix_rules_update";
	    }  
	    else if(action.equals(SitusMatrixRuleAction.closeMatrixRule)){
	    	
	    	return "xxxx";
	    }
	    
	    return "";
    }
	
	private String verifyDates(){
		String errMessage = "";
		
		if(this.updateSitusMatrixRule.getEffectiveDate() == null) {
			errMessage = "errMessage";
			
			FacesMessage message = new FacesMessage("Effective Date is required. ");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
		}
		else if(this.updateSitusMatrixRule.getExpirationDate() == null) {
			errMessage = "errMessage";
			
			FacesMessage message = new FacesMessage("Expiration Date is required. ");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
		}
		else if(this.updateSitusMatrixRule.getExpirationDate().getTime() < this.updateSitusMatrixRule.getEffectiveDate().getTime()) {
			errMessage = "errMessage";
			
			FacesMessage message = new FacesMessage("Expiration Date must be equal or greater than the Effective Date. ");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
		}
		
		return errMessage;
	}
	
	private String verifyDrivers(){
		String errMessage = "";
		
		//Varify country/state/jury level
		int errorCount = 0;
        if(updateSitusMatrixRule.getTransactionTypeCode() == null || updateSitusMatrixRule.getTransactionTypeCode().length()==0){
        	FacesMessage message = new FacesMessage("Transaction Type is mandatory. ");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
        	
    		errorCount++;
		}
		if(updateSitusMatrixRule.getRatetypeCode() == null || updateSitusMatrixRule.getRatetypeCode().length()==0){
			FacesMessage message = new FacesMessage("Rate Type is mandatory. ");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
			
			errorCount++;
		}
		if(updateSitusMatrixRule.getMethodDeliveryCode() == null || updateSitusMatrixRule.getMethodDeliveryCode().length()==0){
			FacesMessage message = new FacesMessage("Method of Delivery is mandatory. ");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
			
			errorCount++;
		}		
		if(updateSitusMatrixRule.getSitusCountryCode() == null || updateSitusMatrixRule.getSitusCountryCode().length()==0){
			FacesMessage message = new FacesMessage("Country is mandatory. ");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
			
			errorCount++;
		}
		if(updateSitusMatrixRule.getSitusStateCode() == null || updateSitusMatrixRule.getSitusStateCode().length()==0){
			FacesMessage message = new FacesMessage("State is mandatory. ");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
			
			errorCount++;
		}
		if(updateSitusMatrixRule.getJurLevel() == null || updateSitusMatrixRule.getJurLevel().length()==0){
			FacesMessage message = new FacesMessage("Jur Level is mandatory. ");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
			
			errorCount++;
		}
		
		//"*COUNTRY" at state code or juri level
		if(updateSitusMatrixRule.getSitusStateCode().equalsIgnoreCase("*COUNTRY") || updateSitusMatrixRule.getJurLevel().equalsIgnoreCase("*COUNTRY")){
			if(!(updateSitusMatrixRule.getSitusStateCode().equalsIgnoreCase("*COUNTRY") && updateSitusMatrixRule.getJurLevel().equalsIgnoreCase("*COUNTRY"))){
				FacesMessage message = new FacesMessage("State and Jurisdiction Level do not match. ");
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage(null, message);
				
				errorCount++;
			}
		}
		
		// Calculate Binary Weight
		long binaryWeight = 0;
		
		//Check 30 drivers
		boolean allNull = false;
		int notNull = 1; //See SYSTEM/SYSTEM/MINIMUMSITUSDRIVERS 
		int numberNotNull = 0;
		boolean mandatoryNotSelected = false;
	    try {
	    	NumberFormat formatter = new DecimalFormat("00");
	        Method theMethod = null;
	        String name = "";
	        String nameColumn = "";
	        Boolean aBoolean = null;
	        	
	        String value = null;
	        for (int i = 1; i <= 30; i++) {
	        	name = "Driver" + formatter.format(i);
	        	nameColumn = "DRIVER_" + formatter.format(i).toUpperCase();
	        	theMethod = updateSitusMatrixRule.getClass().getDeclaredMethod("get" + name, new Class[]{});
	        	theMethod.setAccessible(true);
	        	value = (String)theMethod.invoke(updateSitusMatrixRule, new Object[]{});
	        	  	
	        	if(value==null || value.length()==0 || value.equalsIgnoreCase("*ALL")){
	        		aBoolean =(Boolean)getSitusMatrixNameMandatoryMap().get(nameColumn);
	        		if(aBoolean!=null && aBoolean.booleanValue()){
	        			mandatoryNotSelected = true;
		        	}
	        		
	        		value = "*ALL";
	        		theMethod = updateSitusMatrixRule.getClass().getDeclaredMethod("set" + name, new Class[] {value.getClass()});
            		theMethod.setAccessible(true);
            		theMethod.invoke(updateSitusMatrixRule, new Object[]{value});
	        	}
	        	else{
	        		numberNotNull++;
	        		
	        		// Calculate Binary Weight
	        		Double mult = Math.pow(2, (30-i));
					binaryWeight = binaryWeight + mult.longValue();
	        	}
	        }
        }
        catch (Exception ex) {
        }
        
        if(mandatoryNotSelected){
        	FacesMessage message = new FacesMessage("Drivers in red are mandatory and must be selected. ");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
			
			errorCount++;
        }
        
        if(numberNotNull<notNull){
        	FacesMessage message = new FacesMessage("At least " + notNull + " driver(s) must be selected. ");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
			
			errorCount++;
        }
        
        //Calculate BinaryWeight
        updateSitusMatrixRule.setBinaryWeight(binaryWeight);

        if(errorCount>0){
        	errMessage = "errMessage";
        }
		
		return errMessage;
	}
	
	private SimpleDateFormat maxDate = new SimpleDateFormat("MM/dd/yyyy");
	
	public void validateExpirationDate(FacesContext context, UIComponent toValidate, Object value) {
	    // Ensure date > effectiveDate
		Date expDate = (Date) value;

		Date maxExpDate = null;
		try {
			maxExpDate = maxDate.parse("12/31/9999");
		} catch (ParseException e) {}

		if (expDate.after(maxExpDate)) {
			((UIInput)toValidate).setValid(false);
			FacesMessage message = new FacesMessage("Expiration Date: could not be understood as a date.");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage(toValidate.getClientId(context), message);
	    }
	}
	
	public void validateEffectiveDate(FacesContext context, UIComponent toValidate, Object value) {
	    // Ensure date > effectiveDate
		Date expDate = (Date) value;

		Date maxExpDate = null;
		try {
			maxExpDate = maxDate.parse("12/31/9999");
		} catch (ParseException e) {}

		if (expDate.after(maxExpDate)) {
			((UIInput)toValidate).setValid(false);
			FacesMessage message = new FacesMessage("Effective Date: could not be understood as a date.");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage(toValidate.getClientId(context), message);
	    }
	}
	
	public String processUpdateCommand() {
		if(currentMatrixAction.equals(SitusMatrixAction.deleteSitusMatrix)){
			//Delete all situs matrix
			SitusMatrix aSitusMatrixRule = new SitusMatrix();
			aSitusMatrixRule.setTransactionTypeCode(headerSitusMatrix.getTransactionTypeCode());
			aSitusMatrixRule.setSitusCountryCode(headerSitusMatrix.getSitusCountryCode());
			aSitusMatrixRule.setRatetypeCode(headerSitusMatrix.getRatetypeCode());
			aSitusMatrixRule.setSitusStateCode(headerSitusMatrix.getSitusStateCode());
			aSitusMatrixRule.setMethodDeliveryCode(headerSitusMatrix.getMethodDeliveryCode());
	    	
	    	situsMatrixService.inactiveSitusMatrix(aSitusMatrixRule);
	    	retrieveFilterSitusMatrix();
			
			return "situs_admin_matrix_main";
		}
	    else if(currentSitusMatrixRuleAction.equals(SitusMatrixRuleAction.addMatrixRule) ||
	    		currentSitusMatrixRuleAction.equals(SitusMatrixRuleAction.copyaddMatrixRule) ||
	    		currentSitusMatrixRuleAction.equals(SitusMatrixRuleAction.dupSystemMatrixRule)){	
	    	String errMessage = verifyDrivers();
			if (errMessage!=null && errMessage.length()>0) {
				return null;
			}
	    	
			errMessage = verifyDates();
			if (errMessage!=null && errMessage.length()>0) {
				return null;
			}
			
			//Verify Situs location & tax type
			if(updateSitusMatrixRule.getPrimarySitusCode()==null || updateSitusMatrixRule.getPrimarySitusCode().length()==0 ||
					updateSitusMatrixRule.getPrimaryTaxtypeCode()==null || updateSitusMatrixRule.getPrimaryTaxtypeCode().length()==0){
				FacesMessage message = new FacesMessage("Primary Situs Location & Tax Type must be selected. ");
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage(null, message);
				return null;
			}
			
			if((updateSitusMatrixRule.getSecondarySitusCode()!=null && updateSitusMatrixRule.getSecondarySitusCode().length()>0) ||
					(updateSitusMatrixRule.getSecondaryTaxtypeCode()!=null && updateSitusMatrixRule.getSecondaryTaxtypeCode().length()>0)){
				if(updateSitusMatrixRule.getSecondarySitusCode()==null || updateSitusMatrixRule.getSecondarySitusCode().length()==0 ||
						updateSitusMatrixRule.getSecondaryTaxtypeCode()==null || updateSitusMatrixRule.getSecondaryTaxtypeCode().length()==0){
					FacesMessage message = new FacesMessage("Must select both Secondary Situs Location and Tax Type. ");
					message.setSeverity(FacesMessage.SEVERITY_ERROR);
					FacesContext.getCurrentInstance().addMessage(null, message);
					return null;
				}
			}
			
			if(currentSitusMatrixRuleAction.equals(SitusMatrixRuleAction.dupSystemMatrixRule)){
				//Duplicated the combination of drivers is allowed.
				updateSitusMatrixRule.setCustomFlag("1");
			}
			else{
				List<SitusMatrix> aSitusMatrixList = situsMatrixService.findByExample(updateSitusMatrixRule);
				if(aSitusMatrixList!=null && aSitusMatrixList.size()>0){
					FacesMessage message = new FacesMessage("The combination of drivers must be unique. ");
					message.setSeverity(FacesMessage.SEVERITY_ERROR);
					FacesContext.getCurrentInstance().addMessage(null, message);
					return null;
				}
			}
			
			situsMatrixService.addSitusMatrix(updateSitusMatrixRule);
			resetMasterRulesRecords();
	    }
	    else if(currentSitusMatrixRuleAction.equals(SitusMatrixRuleAction.updateMatrixRule)){
	    	String errMessage = verifyDates();
			if (errMessage!=null && errMessage.length()>0) {
				return null;
			}
			
			//Only the Expiration Date, Comments, and Active? checkbox may be changed in the Details section.	
			situsMatrixService.updateSitusMatrix(updateSitusMatrixRule);
			resetMasterRulesRecords();
	    }
		else if(currentSitusMatrixRuleAction.equals(SitusMatrixRuleAction.copyupdateMatrixRule)){
			String errMessage = verifyDates();
			if (errMessage!=null && errMessage.length()>0) {
				return null;
			}
			
			//The Effective Date must be changed to a new date. All other Detail section fields may be changed if needed.
			Date oriEffectiveDate = selectedRulesDetailSitusMatrix.getEffectiveDate();
			Date newEffectiveDate = updateSitusMatrixRule.getEffectiveDate();
			if(oriEffectiveDate.getTime()==newEffectiveDate.getTime()){
				FacesMessage message = new FacesMessage("The Effective Date must be changed to a new date.");
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage(null, message);
				return null;
			}
			
			//Verify Situs location & tax type
			if(updateSitusMatrixRule.getPrimarySitusCode()==null || updateSitusMatrixRule.getPrimarySitusCode().length()==0 ||
					updateSitusMatrixRule.getPrimaryTaxtypeCode()==null || updateSitusMatrixRule.getPrimaryTaxtypeCode().length()==0){
				FacesMessage message = new FacesMessage("Primary Situs Location & Tax Type must be selected. ");
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage(null, message);
				return null;
			}
			
			if((updateSitusMatrixRule.getSecondarySitusCode()!=null && updateSitusMatrixRule.getSecondarySitusCode().length()>0) ||
					(updateSitusMatrixRule.getSecondaryTaxtypeCode()!=null && updateSitusMatrixRule.getSecondaryTaxtypeCode().length()>0)){
				if(updateSitusMatrixRule.getSecondarySitusCode()==null || updateSitusMatrixRule.getSecondarySitusCode().length()==0 ||
						updateSitusMatrixRule.getSecondaryTaxtypeCode()==null || updateSitusMatrixRule.getSecondaryTaxtypeCode().length()==0){
					FacesMessage message = new FacesMessage("Must select both Secondary Situs Location and Tax Type. ");
					message.setSeverity(FacesMessage.SEVERITY_ERROR);
					FacesContext.getCurrentInstance().addMessage(null, message);
					return null;
				}
			}
			
			situsMatrixService.addSitusMatrix(updateSitusMatrixRule);
			resetMasterRulesRecords();
			
		}
		else if(currentSitusMatrixRuleAction.equals(SitusMatrixRuleAction.deleteMatrixRule)){
			situsMatrixService.deleteSitusMatrix(updateSitusMatrixRule.getSitusMatrixId());
		}
		else if(currentSitusMatrixRuleAction.equals(SitusMatrixRuleAction.viewMatrixRule)){
			//Do nothing
		}
	    
		return "situs_admin_matrix_main";
    }
	
	public boolean getDisableDupSystemButton(){
		if(selectedRulesDetailRowIndex!=-1 && selectedRulesDetailSitusMatrix!=null && selectedRulesDetailSitusMatrix.getCustomBooleanFlag()==false )
		{return false;}
		else{return true;}
	}
	
	public boolean getDisableButton(){
		if(selectedRulesDetailRowIndex!=-1){return false;}
		else{return true;}
	}
	
	public boolean getDisableMatrixButton(){
		if(selectedDetailRowIndex !=-1){return false;}
		else{return true;}
	}
	
	public void sortAction(ActionEvent e) {
		situsMatrixDataModel.toggleSortOrder(e.getComponent().getParent().getId());
	}
	
	public void sortRulersAction(ActionEvent e) {
		situsMatrixRulesDataModel.toggleSortOrder(e.getComponent().getParent().getId());
	}
	
	public Set<Integer> getLevelRowsToUpdate() {
		if(levelRowsToUpdate == null) {
			levelRowsToUpdate = new HashSet<Integer>(4);
			levelRowsToUpdate.add(0);
			levelRowsToUpdate.add(1);
			levelRowsToUpdate.add(2);
			levelRowsToUpdate.add(3);
		}
		return levelRowsToUpdate;
	}
	
	public List<NexusJurisdictionItemDTO> getCountyList() {
		return countyList;
	}

	public void setCountyList(List<NexusJurisdictionItemDTO> countyList) {
		this.countyList = countyList;
	}

	public List<NexusJurisdictionItemDTO> getCityList() {
		return cityList;
	}

	public void setCityList(List<NexusJurisdictionItemDTO> cityList) {
		this.cityList = cityList;
	}

	public List<NexusJurisdictionItemDTO> getStjList() {
		return stjList;
	}

	public void setStjList(List<NexusJurisdictionItemDTO> stjList) {
		this.stjList = stjList;
	}
	
	public List<NexusJurisdictionItemDTO> getLevelList() {
		return levelList;
	}

	public void setLevelList(List<NexusJurisdictionItemDTO> levelList) {
		this.levelList = levelList;
	}
	
	public void resetSelections() {
		this.selectedLevel = null;
		this.selectedLevelIndex = -1;
		this.selectedCounty = null;
		this.selectedCountyIndex = -1;
		this.selectedCity = null;
		this.selectedCityIndex = -1;
		this.selectedStj = null;
		this.selectedStjIndex = -1;
	}
	
	public void selectedStateChanged(ActionEvent e) {
		HtmlDataTable table = (HtmlDataTable) ((HtmlAjaxSupport) e.getComponent()).getParent();
		this.selectedState = (TaxCodeStateDTO) table.getRowData();
		this.selectedStateIndex = table.getRowIndex();
	}
	
	public boolean isAllowRetrieveMatrixRules(SitusMatrix aSitusMatrix){
		boolean retrieveFlag = true;
		if (aSitusMatrix.getTransactionTypeCode()==null || aSitusMatrix.getTransactionTypeCode().equals("")) {
			retrieveFlag = false;
 	    }
		else if(aSitusMatrix.getRatetypeCode()==null || aSitusMatrix.getRatetypeCode().equals("")) {
   	    	retrieveFlag = false;
   	    }
		else if(aSitusMatrix.getMethodDeliveryCode()==null || aSitusMatrix.getMethodDeliveryCode().equals("")) {
   	    	retrieveFlag = false;
 	    }
		else if(aSitusMatrix.getSitusCountryCode()==null || aSitusMatrix.getSitusCountryCode().equals("")) {
			retrieveFlag = false;
 	    }
		else if(aSitusMatrix.getSitusStateCode()==null || aSitusMatrix.getSitusStateCode().equals("")) {
			retrieveFlag = false;
 	    }
		else if(aSitusMatrix.getJurLevel()==null || aSitusMatrix.getJurLevel().equals("")) {
			retrieveFlag = false;
 	    }
		
		
		return retrieveFlag;
	}
	
	public boolean getDisableAddButton(){
		return !isAllowRetrieveJurisdiction(headerSitusMatrix);
	}
	
	public boolean isAllowRetrieveJurisdiction(SitusMatrix aSitusMatrix){
		boolean retrieveFlag = true;
		if (aSitusMatrix.getTransactionTypeCode()==null || aSitusMatrix.getTransactionTypeCode().equals("")) {
			retrieveFlag = false;
 	    }
		else if(aSitusMatrix.getRatetypeCode()==null || aSitusMatrix.getRatetypeCode().equals("")) {
   	    	retrieveFlag = false;
   	    }
		else if(aSitusMatrix.getMethodDeliveryCode()==null || aSitusMatrix.getMethodDeliveryCode().equals("")) {
   	    	retrieveFlag = false;
 	    }
		else if(aSitusMatrix.getSitusCountryCode()==null || aSitusMatrix.getSitusCountryCode().equals("")) {
			retrieveFlag = false;
 	    }
		else if(aSitusMatrix.getSitusStateCode()==null || aSitusMatrix.getSitusStateCode().equals("")) {
			retrieveFlag = false;
 	    }
		
		return retrieveFlag;
	}
	
	public void retrieveMasterRulesRecords(SitusMatrix aSitusMatrix) {
		//Reset master records
		selectedRulesRowIndex  = -1;
		selectedRulesSitusMatrix = null;
		
		//Reset detail records
		this.selectedRulesDetailRowIndex  = -1;
		this.selectedRulesDetailSitusMatrix = null;
		this.setSitusMatrixRulersDetailList(null);
		
		if(isAllowRetrieveMatrixRules(aSitusMatrix)){
			situsMatrixRulesDataModel.setCriteria(aSitusMatrix);
		}
		else{
			SitusMatrix aNull = null;
			situsMatrixRulesDataModel.setCriteria(aNull);
		}
	}
	
	public void selectedLevelChanged(ActionEvent e) {
	}
	
	public void selectedCountyChanged(ActionEvent e) {
		resetSelections();
		HtmlDataTable table = (HtmlDataTable) ((HtmlAjaxSupport) e.getComponent()).getParent();
		this.selectedCounty = (NexusJurisdictionItemDTO) table.getRowData();
		this.selectedCountyIndex = table.getRowIndex();
		
		SitusMatrix aSitusMatrix = new SitusMatrix();	
		aSitusMatrix.setTransactionTypeCode(this.headerSitusMatrix.getTransactionTypeCode());
		aSitusMatrix.setRatetypeCode(this.headerSitusMatrix.getRatetypeCode());
		aSitusMatrix.setMethodDeliveryCode(this.headerSitusMatrix.getMethodDeliveryCode());
		aSitusMatrix.setSitusCountryCode(this.headerSitusMatrix.getSitusCountryCode());
		aSitusMatrix.setSitusStateCode(this.headerSitusMatrix.getSitusStateCode());
		
		retrieveMasterRulesRecords(aSitusMatrix);
	}
	
	public void selectedCityChanged(ActionEvent e) {
		resetSelections();
		HtmlDataTable table = (HtmlDataTable) ((HtmlAjaxSupport) e.getComponent()).getParent();
		this.selectedCity = (NexusJurisdictionItemDTO) table.getRowData();
		this.selectedCityIndex = table.getRowIndex();
		
		SitusMatrix aSitusMatrix = new SitusMatrix();	
		aSitusMatrix.setTransactionTypeCode(this.headerSitusMatrix.getTransactionTypeCode());
		aSitusMatrix.setRatetypeCode(this.headerSitusMatrix.getRatetypeCode());
		aSitusMatrix.setMethodDeliveryCode(this.headerSitusMatrix.getMethodDeliveryCode());
		aSitusMatrix.setSitusCountryCode(this.headerSitusMatrix.getSitusCountryCode());
		aSitusMatrix.setSitusStateCode(this.headerSitusMatrix.getSitusStateCode());
		
		retrieveMasterRulesRecords(aSitusMatrix);
	}
	
	public void selectedStjChanged(ActionEvent e) {
		resetSelections();
		HtmlDataTable table = (HtmlDataTable) ((HtmlAjaxSupport) e.getComponent()).getParent();
		this.selectedStj = (NexusJurisdictionItemDTO) table.getRowData();
		this.selectedStjIndex = table.getRowIndex();
		
		SitusMatrix aSitusMatrix = new SitusMatrix();	
		aSitusMatrix.setTransactionTypeCode(this.headerSitusMatrix.getTransactionTypeCode());
		aSitusMatrix.setRatetypeCode(this.headerSitusMatrix.getRatetypeCode());
		aSitusMatrix.setMethodDeliveryCode(this.headerSitusMatrix.getMethodDeliveryCode());
		aSitusMatrix.setSitusCountryCode(this.headerSitusMatrix.getSitusCountryCode());
		aSitusMatrix.setSitusStateCode(this.headerSitusMatrix.getSitusStateCode());
		
		retrieveMasterRulesRecords(aSitusMatrix);
	}
	
	private void getJurisdictions() {
		List<Object[]> list = jurisdictionService.findSitusJurisdiction(headerSitusMatrix.getSitusCountryCode(), headerSitusMatrix.getSitusStateCode(), headerSitusMatrix.getTransactionTypeCode(), 
								headerSitusMatrix.getRatetypeCode(), headerSitusMatrix.getMethodDeliveryCode(), "I", "level");		
		levelList = buildRegList(list);
		
		list = jurisdictionService.findSitusJurisdiction(headerSitusMatrix.getSitusCountryCode(), headerSitusMatrix.getSitusStateCode(), headerSitusMatrix.getTransactionTypeCode(), 
				headerSitusMatrix.getRatetypeCode(), headerSitusMatrix.getMethodDeliveryCode(), "I", "county");
		countyList = buildRegList(list);
		
		list = jurisdictionService.findSitusJurisdiction(headerSitusMatrix.getSitusCountryCode(), headerSitusMatrix.getSitusStateCode(), headerSitusMatrix.getTransactionTypeCode(), 
				headerSitusMatrix.getRatetypeCode(), headerSitusMatrix.getMethodDeliveryCode(), "I", "city");
		cityList = buildRegList(list);
		
		list = jurisdictionService.findSitusJurisdiction(headerSitusMatrix.getSitusCountryCode(), headerSitusMatrix.getSitusStateCode(), headerSitusMatrix.getTransactionTypeCode(), 
				headerSitusMatrix.getRatetypeCode(), headerSitusMatrix.getMethodDeliveryCode(), "I", "stj");
		stjList = buildRegList(list);
	}
	
	private List<NexusJurisdictionItemDTO> buildRegList(List<Object[]> list) {
		List<NexusJurisdictionItemDTO> result = null;
		if(list != null) {
			result = new ArrayList<NexusJurisdictionItemDTO>();
			for(Object[] n : list) {
				NexusJurisdictionItemDTO sj = new NexusJurisdictionItemDTO();
				sj.setName((String) n[0]);
				String aFlag = (n[1]==null) ? "" : (String)n[1];
				if(aFlag.length()>0) {
					sj.setHasNexus(true);
				}
				result.add(sj);
			}
		}
		
		return result;
	}
	
	private List<NexusJurisdictionItemDTO> buildList(List<Object[]> list) {
		List<NexusJurisdictionItemDTO> result = null;
		if(list != null) {
			result = new ArrayList<NexusJurisdictionItemDTO>();
			for(Object[] n : list) {
				NexusJurisdictionItemDTO sj = new NexusJurisdictionItemDTO();
				sj.setName((String) n[0]);
				if("1".equals(n[1] + "")) {
					sj.setHasNexus(true);
				}
				result.add(sj);
			}
		}
		
		return result;
	}
	
	public void levelCheckboxSelected(ActionEvent e) throws AbortProcessingException {
		HtmlSelectBooleanCheckbox chk = (HtmlSelectBooleanCheckbox) ((HtmlAjaxSupport) e.getComponent()).getParent();
		HtmlDataTable table = (HtmlDataTable) chk.getParent().getParent();
		NexusJurisdictionItemDTO dto = (NexusJurisdictionItemDTO) table.getRowData();
		
		if((Boolean) chk.getValue()) {
			this.selectedLevels.put(dto.getName(), dto);
			
			if("All State".equalsIgnoreCase(dto.getName())) {
				for(NexusJurisdictionItemDTO nj : levelList) {
					nj.setSelected(true);
					nj.setEnabled(true);
					this.selectedLevels.put(nj.getName(), nj);
				}
				
				if(countyList != null) {
					for(NexusJurisdictionItemDTO nj : countyList) {
						nj.setSelected(true);
						nj.setEnabled(true);
						this.selectedCounties.put(nj.getName(), nj);
					}
				}
				
				if(cityList != null) {
					for(NexusJurisdictionItemDTO nj : cityList) {
						nj.setSelected(true);
						nj.setEnabled(true);
						this.selectedCities.put(nj.getName(), nj);
					}
				}
				
				if(stjList != null) {
					for(NexusJurisdictionItemDTO nj : stjList) {
						nj.setSelected(true);
						nj.setEnabled(true);
						this.selectedStjs.put(nj.getName(), nj);
					}
				}
			}
			else if("All Counties".equalsIgnoreCase(dto.getName())) {
				if(countyList != null) {
					for(NexusJurisdictionItemDTO nj : countyList) {
						nj.setSelected(true);
						this.selectedCounties.put(nj.getName(), nj);
					}
				}
			}
			else if("All Cities".equalsIgnoreCase(dto.getName())) {
				if(cityList != null) {
					for(NexusJurisdictionItemDTO nj : cityList) {
						nj.setSelected(true);
						this.selectedCities.put(nj.getName(), nj);
					}
				}
			}
			else if("All Stjs".equalsIgnoreCase(dto.getName())) {
				if(stjList != null) {
					for(NexusJurisdictionItemDTO nj : stjList) {
						nj.setSelected(true);
						this.selectedStjs.put(nj.getName(), nj);
					}
				}
			}
		}
		else {
			this.selectedLevels.remove(dto.getName());
			
			//Uncheck all if All State uncheck
			if("All State".equalsIgnoreCase(dto.getName())) {
				disableAllJurisdictionCheck();
			}
		}
	}
	
	private void disableAllJurisdictionCheck(){
		for(NexusJurisdictionItemDTO nj : levelList) {
			nj.setSelected(false);
			if("All State".equalsIgnoreCase(nj.getName())){
				nj.setEnabled(true); //Only exception
			}
			else{
				nj.setEnabled(false);
			}
			this.selectedLevels.remove(nj.getName());
		}
		
		if(countyList != null) {
			for(NexusJurisdictionItemDTO nj : countyList) {
				nj.setSelected(false);
				nj.setEnabled(false);
				this.selectedCounties.remove(nj.getName());
			}
		}
		
		if(cityList != null) {
			for(NexusJurisdictionItemDTO nj : cityList) {
				nj.setSelected(false);
				nj.setEnabled(false);
				this.selectedCities.remove(nj.getName());
			}
		}
		
		if(stjList != null) {
			for(NexusJurisdictionItemDTO nj : stjList) {
				nj.setSelected(false);
				nj.setEnabled(false);
				this.selectedStjs.remove(nj.getName());
			}
		}
	}
	
	public void countyCheckboxSelected(ActionEvent e) throws AbortProcessingException {
		HtmlSelectBooleanCheckbox chk = (HtmlSelectBooleanCheckbox) ((HtmlAjaxSupport) e.getComponent()).getParent();
		HtmlDataTable table = (HtmlDataTable) chk.getParent().getParent();
		NexusJurisdictionItemDTO dto = (NexusJurisdictionItemDTO) table.getRowData();
		
		if((Boolean) chk.getValue()) {
			this.selectedCounties.put(dto.getName(), dto);
		}
		else {
			levelList.get(1).setSelected(false);
			this.selectedLevels.remove("All Counties");
			this.selectedCounties.remove(dto.getName());
		}		
	}
	
	public void cityCheckboxSelected(ActionEvent e) throws AbortProcessingException {
		HtmlSelectBooleanCheckbox chk = (HtmlSelectBooleanCheckbox) ((HtmlAjaxSupport) e.getComponent()).getParent();
		HtmlDataTable table = (HtmlDataTable) chk.getParent().getParent();
		NexusJurisdictionItemDTO dto = (NexusJurisdictionItemDTO) table.getRowData();
		
		if((Boolean) chk.getValue()) {
			this.selectedCities.put(dto.getName(), dto);
		}
		else {
			levelList.get(2).setSelected(false);
			this.selectedLevels.remove("All Cities");
			this.selectedCities.remove(dto.getName());
		}		
	}
	
	public void stjCheckboxSelected(ActionEvent e) throws AbortProcessingException {
		HtmlSelectBooleanCheckbox chk = (HtmlSelectBooleanCheckbox) ((HtmlAjaxSupport) e.getComponent()).getParent();
		HtmlDataTable table = (HtmlDataTable) chk.getParent().getParent();
		NexusJurisdictionItemDTO dto = (NexusJurisdictionItemDTO) table.getRowData();
		
		if((Boolean) chk.getValue()) {
			this.selectedStjs.put(dto.getName(), dto);
		}
		else {
			levelList.get(3).setSelected(false);
			this.selectedLevels.remove("All Stjs");
			this.selectedStjs.remove(dto.getName());
		}		
	}
	
	private void resetSelectionMaps() {
		this.selectedLevels.clear();
		this.selectedCounties.clear();
		this.selectedCities.clear();
		this.selectedStjs.clear();
	}
	
	public SitusBaseMatrixBackingBean(){
	}
	
	public String getSelectedDisplay() {
		return selectedDisplay;
	}

	public void setSelectedDisplay(String selectedDisplay) {
		this.selectedDisplay = selectedDisplay;
	}
	
	public List<SelectItem> getCountryMenuItems() {
		if(countryMenuItems==null){
			filterSitusMatrix.setSitusCountryCode("");
		}
		countryMenuItems = getCacheManager().createCountryItems();   
		return countryMenuItems;
	}

	public List<SelectItem> getStateMenuItems() {
		if (stateMenuItems == null) {
		   stateMenuItems = getCacheManager().createStateItems(filterSitusMatrix.getSitusCountryCode());
		   filterSitusMatrix.setSitusStateCode("");
		}
		return stateMenuItems;
	}
	
	public List<SelectItem> getHeaderCountryMenuItems() {
		if(headerCountryMenuItems==null){
			headerSitusMatrix.setSitusCountryCode("");
		}
		headerCountryMenuItems = getCacheManager().createCountryItems();   
		return headerCountryMenuItems;
	}

	public List<SelectItem> getHeaderStateMenuItems() {
		return headerStateMenuItems;
	}
	
	public List<SelectItem> createHeaderStateMenuItems(String strCountry) {
		List<SelectItem> selectItems = new ArrayList<SelectItem>();
		selectItems.add(new SelectItem("","Select State"));
		Collection<TaxCodeStateDTO> taxCodeStateDTOList = getCacheManager().getTaxCodeStateMap().values();
		if(strCountry!=null && strCountry.length()>0){  
			selectItems.add(new SelectItem("*COUNTRY","*COUNTRY"));
			for (TaxCodeStateDTO taxCodeStateDTO : taxCodeStateDTOList) {
			  if(strCountry.equalsIgnoreCase(taxCodeStateDTO.getCountry()) && ((taxCodeStateDTO.getActiveFlag() != null) && taxCodeStateDTO.getActiveFlag().equals("1")) ){
				  selectItems.add(new SelectItem(taxCodeStateDTO.getTaxCodeState(), 
						  taxCodeStateDTO.getName() + " (" + taxCodeStateDTO.getTaxCodeState() + ")"));
			  }
		  }
		}

		return selectItems;
	}
	
	public List<SelectItem> getDisplayMenuItems() {
		if (displayMenuItems == null) {
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
			selectItems.add(new SelectItem("","All Levels"));
			selectItems.add(new SelectItem("*COUNTRY","*COUNTRY"));
			selectItems.add(new SelectItem("*STATE","*STATE"));
			selectItems.add(new SelectItem("*COUNTY","*COUNTY"));
			selectItems.add(new SelectItem("*CITY","*CITY"));
			selectItems.add(new SelectItem("*STJ","*STJ"));
			
			displayMenuItems = selectItems;
		}
		return displayMenuItems;
	}
	
	public List<SelectItem> getDisplayJurisdictionLevelItems() {
		if (displayJurisdictionLevelItems == null) {
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
			selectItems.add(new SelectItem("","Select a Jurisdiction Level"));
			selectItems.add(new SelectItem("*COUNTRY","*COUNTRY"));
			selectItems.add(new SelectItem("*STATE","*STATE"));
			selectItems.add(new SelectItem("*COUNTY","*COUNTY"));
			selectItems.add(new SelectItem("*CITY","*CITY"));
			selectItems.add(new SelectItem("*STJ","*STJ"));
			
			displayJurisdictionLevelItems = selectItems;
		}
		return displayJurisdictionLevelItems;
	}
	
	public boolean getOkEnanled() {
		return (situsMatrixNameUpdateMap!=null && situsMatrixNameUpdateMap.size()>0);
	}
	
	public void searchHeaderChanged(ValueChangeEvent e){	
		clearJurisdiction();
		if(!isAllowRetrieveJurisdiction(headerSitusMatrix)){
			return;
		}
		
		this.getJurisdictions();
		this.disableAllJurisdictionCheck();
		
		resetMasterRulesRecords();
    }
	
	public void updateTransactionTypeChanged(ValueChangeEvent e){	
		String code = (String)e.getNewValue();
		updateSitusMatrixRule.setTransactionTypeCode(code);
		
		//Rebuild driver mapping 
		rebuildSitusMatrixNameUpdateMap(updateSitusMatrixRule);
    }
	
	public void updateRateTypeChanged(ValueChangeEvent e){	
		String code = (String)e.getNewValue();
		updateSitusMatrixRule.setRatetypeCode(code);
		
		//Rebuild driver mapping 
		rebuildSitusMatrixNameUpdateMap(updateSitusMatrixRule);
    }
	
	public void filterTransactionTypeCodeChanged(ValueChangeEvent e){	
		String code = (String)e.getNewValue();
		if(code == null) {
			return;
		}
		filterSitusMatrix.setTransactionTypeCode(code);
			
		String codeOld = (String)e.getOldValue();
		if(codeOld!=null && codeOld.length()>0){
			//Change from a selected value to a selected value. Repopulate other with distinct. 
			filterSitusMatrix.setRatetypeCode("");
			if(filterRateTypeCodeItems!=null){
			 	filterRateTypeCodeItems.clear();
				filterRateTypeCodeItems=null;
			}

			filterSitusMatrix.setMethodDeliveryCode("");
			if(filterMethodOfDeliveryItems!=null){
			 	filterMethodOfDeliveryItems.clear();
				filterMethodOfDeliveryItems=null;
			}
		}
		else{
			//Change from a default to a selected value. Repopulate other which still has a default. 
			if(filterSitusMatrix.getRatetypeCode()==null || filterSitusMatrix.getRatetypeCode().length()==0){
				filterSitusMatrix.setRatetypeCode("");
				if(filterRateTypeCodeItems!=null){
				 	filterRateTypeCodeItems.clear();
					filterRateTypeCodeItems=null;
				}
			}

			if(filterSitusMatrix.getMethodDeliveryCode()==null || filterSitusMatrix.getMethodDeliveryCode().length()==0){
				filterSitusMatrix.setMethodDeliveryCode("");
				if(filterMethodOfDeliveryItems!=null){
					filterMethodOfDeliveryItems.clear();
					filterMethodOfDeliveryItems=null;
				}
			}
		}
		
		getFilterTransactionTypeCodeItems();
		getFilterRateTypeCodeItems();
		getFilterMethodOfDeliveryItems();
    }
	
	public void filterRatetypeCodeChanged(ValueChangeEvent e){	
		String code = (String)e.getNewValue();
		if(code == null) {
			return;
		}
		filterSitusMatrix.setRatetypeCode(code);
		
		String codeOld = (String)e.getOldValue();
		if(codeOld!=null && codeOld.length()>0){
			//Change from a selected value to a selected value. Repopulate other with distinct. 
			filterSitusMatrix.setTransactionTypeCode("");
			if(filterTransactionTypeCodeItems!=null){
			 	filterTransactionTypeCodeItems.clear();
				filterTransactionTypeCodeItems=null;
			}

			filterSitusMatrix.setMethodDeliveryCode("");
			if(filterMethodOfDeliveryItems!=null){
			 	filterMethodOfDeliveryItems.clear();
				filterMethodOfDeliveryItems=null;
			}	
		}
		else{
			//Change from a default to a selected value. Repopulate other which still has a default. 
			if(filterSitusMatrix.getTransactionTypeCode()==null || filterSitusMatrix.getTransactionTypeCode().length()==0){
				filterSitusMatrix.setTransactionTypeCode("");
				if(filterTransactionTypeCodeItems!=null){
					filterTransactionTypeCodeItems.clear();
					filterTransactionTypeCodeItems=null;
				}
			}

			if(filterSitusMatrix.getMethodDeliveryCode()==null || filterSitusMatrix.getMethodDeliveryCode().length()==0){
				filterSitusMatrix.setMethodDeliveryCode("");
				if(filterMethodOfDeliveryItems!=null){
					filterMethodOfDeliveryItems.clear();
					filterMethodOfDeliveryItems=null;
				}
			}
		}
		
		getFilterTransactionTypeCodeItems();
		getFilterRateTypeCodeItems();
		getFilterMethodOfDeliveryItems();
    }
	
	public void filterMethodDeliveryCodeChanged(ValueChangeEvent e){	
		String code = (String)e.getNewValue();
		if(code == null) {
			return;
		}
		filterSitusMatrix.setMethodDeliveryCode(code);
		
		String codeOld = (String)e.getOldValue();
		if(codeOld!=null && codeOld.length()>0){
			//Change from a selected value to a selected value. Repopulate other with distinct. 
			
			filterSitusMatrix.setTransactionTypeCode("");
			if(filterTransactionTypeCodeItems!=null){
			 	filterTransactionTypeCodeItems.clear();
				filterTransactionTypeCodeItems=null;
			}

			filterSitusMatrix.setRatetypeCode("");
			if(filterRateTypeCodeItems!=null){
			 	filterRateTypeCodeItems.clear();
				filterRateTypeCodeItems=null;
			}

			//filterSitusMatrix.setMethodDeliveryCode("");
			//if(filterMethodOfDeliveryItems!=null){
			// 	filterMethodOfDeliveryItems.clear();
			//	filterMethodOfDeliveryItems=null;
			//}
		}
		else{
			//Change from a default to a selected value. Repopulate other which still has a default. 
			
			if(filterSitusMatrix.getTransactionTypeCode()==null || filterSitusMatrix.getTransactionTypeCode().length()==0){
				filterSitusMatrix.setTransactionTypeCode("");
				if(filterTransactionTypeCodeItems!=null){
					filterTransactionTypeCodeItems.clear();
					filterTransactionTypeCodeItems=null;
				}
			}
			
			if(filterSitusMatrix.getRatetypeCode()==null || filterSitusMatrix.getRatetypeCode().length()==0){
				filterSitusMatrix.setRatetypeCode("");
				if(filterRateTypeCodeItems!=null){
				 	filterRateTypeCodeItems.clear();
					filterRateTypeCodeItems=null;
				}
			}

			//if(filterSitusMatrix.getMethodDeliveryCode()==null || filterSitusMatrix.getMethodDeliveryCode().length()==0){
			//	if(filterMethodOfDeliveryItems!=null){
			//		filterMethodOfDeliveryItems.clear();
			//		filterMethodOfDeliveryItems=null;
			//	}
			//}
		}
		
		getFilterTransactionTypeCodeItems();
		getFilterRateTypeCodeItems();
		getFilterMethodOfDeliveryItems();
    }
	
	private HtmlSelectOneMenu transactionTypeCodeMenu = new HtmlSelectOneMenu();	
	private HtmlSelectOneMenu rateTypeCodeMenu = new HtmlSelectOneMenu();	
	private HtmlSelectOneMenu methodOfDeliveryMenu = new HtmlSelectOneMenu();	
	
	private HtmlSelectOneMenu transactionTypeCodeMenuHeader = new HtmlSelectOneMenu();	
	private HtmlSelectOneMenu rateTypeCodeMenuHeader = new HtmlSelectOneMenu();	
	private HtmlSelectOneMenu methodOfDeliveryMenuHeader = new HtmlSelectOneMenu();	
	
	public HtmlSelectOneMenu getTransactionTypeCodeMenu() {
		return transactionTypeCodeMenu;
	}
	public void setTransactionTypeCodeMenu(HtmlSelectOneMenu transactionTypeCodeMenu) {
		this.transactionTypeCodeMenu = transactionTypeCodeMenu;
	}
	public HtmlSelectOneMenu getRateTypeCodeMenu() {
		return rateTypeCodeMenu;
	}
	public void setRateTypeCodeMenu(HtmlSelectOneMenu rateTypeCodeMenu) {
		this.rateTypeCodeMenu = rateTypeCodeMenu;
	}
	public HtmlSelectOneMenu getMethodOfDeliveryMenu() {
		return methodOfDeliveryMenu;
	}
	public void setMethodOfDeliveryMenu(HtmlSelectOneMenu methodOfDeliveryMenu) {
		this.methodOfDeliveryMenu = methodOfDeliveryMenu;
	}
	
	public HtmlSelectOneMenu getTransactionTypeCodeMenuHeader() {
		return transactionTypeCodeMenuHeader;
	}
	public void setTransactionTypeCodeMenuHeader(HtmlSelectOneMenu transactionTypeCodeMenuHeader) {
		this.transactionTypeCodeMenuHeader = transactionTypeCodeMenuHeader;
	}
	public HtmlSelectOneMenu getRateTypeCodeMenuHeader() {
		return rateTypeCodeMenuHeader;
	}
	public void setRateTypeCodeMenuHeader(HtmlSelectOneMenu rateTypeCodeMenuHeader) {
		this.rateTypeCodeMenuHeader = rateTypeCodeMenuHeader;
	}
	public HtmlSelectOneMenu getMethodOfDeliveryMenuHeader() {
		return methodOfDeliveryMenuHeader;
	}
	public void setMethodOfDeliveryMenuHeader(HtmlSelectOneMenu methodOfDeliveryMenuHeader) {
		this.methodOfDeliveryMenuHeader = methodOfDeliveryMenuHeader;
	}
	
	private void setInputValue(UIInput input, String value) {
		input.setLocalValueSet(false);
		input.setSubmittedValue(null);
		input.setValue(value);
	}
	
	public void filterTransactionTypeCodeChanged(ActionEvent e) {
		String code = (String) ((HtmlSelectOneMenu) ((HtmlAjaxSupport) e.getComponent()).getParent()).getSubmittedValue();
		String codeOld = filterSitusMatrix.getTransactionTypeCode();
	
		filterSitusMatrix.setTransactionTypeCode(code);
		if(codeOld!=null && codeOld.length()>0){
			//Change from a selected value to a selected value. Repopulate other with distinct. 
			filterSitusMatrix.setRatetypeCode("");
			setInputValue(rateTypeCodeMenu, "");
			if(filterRateTypeCodeItems!=null){
			 	filterRateTypeCodeItems.clear();
				filterRateTypeCodeItems=null;
			}

			filterSitusMatrix.setMethodDeliveryCode("");
			setInputValue(methodOfDeliveryMenu, "");
			if(filterMethodOfDeliveryItems!=null){
			 	filterMethodOfDeliveryItems.clear();
				filterMethodOfDeliveryItems=null;
			}
		}
		else{
			//Change from a default to a selected value. Repopulate other which still has a default. 
			if(filterSitusMatrix.getRatetypeCode()==null || filterSitusMatrix.getRatetypeCode().length()==0){
				filterSitusMatrix.setRatetypeCode("");
				setInputValue(rateTypeCodeMenu, "");
				if(filterRateTypeCodeItems!=null){
				 	filterRateTypeCodeItems.clear();
					filterRateTypeCodeItems=null;
				}
			}

			if(filterSitusMatrix.getMethodDeliveryCode()==null || filterSitusMatrix.getMethodDeliveryCode().length()==0){
				filterSitusMatrix.setMethodDeliveryCode("");
				setInputValue(methodOfDeliveryMenu, "");
				if(filterMethodOfDeliveryItems!=null){
					filterMethodOfDeliveryItems.clear();
					filterMethodOfDeliveryItems=null;
				}
			}
		}
		
		getFilterTransactionTypeCodeItems();
		getFilterRateTypeCodeItems();
		getFilterMethodOfDeliveryItems();
    }
	
	public void filterRatetypeCodeChanged(ActionEvent e) {
		String code = (String) ((HtmlSelectOneMenu) ((HtmlAjaxSupport) e.getComponent()).getParent()).getSubmittedValue();
		String codeOld = filterSitusMatrix.getRatetypeCode();

		filterSitusMatrix.setRatetypeCode(code);
		if(codeOld!=null && codeOld.length()>0){
			//Change from a selected value to a selected value. Repopulate other with distinct. 
			
			filterSitusMatrix.setTransactionTypeCode("");
			setInputValue(transactionTypeCodeMenu, "");
			if(filterTransactionTypeCodeItems!=null){
			 	filterTransactionTypeCodeItems.clear();
				filterTransactionTypeCodeItems=null;
			}
			
			//filterSitusMatrix.setRatetypeCode("");
			//if(filterRateTypeCodeItems!=null){
			// 	filterRateTypeCodeItems.clear();
			//	filterRateTypeCodeItems=null;
			//}

			filterSitusMatrix.setMethodDeliveryCode("");
			setInputValue(methodOfDeliveryMenu, "");
			if(filterMethodOfDeliveryItems!=null){
			 	filterMethodOfDeliveryItems.clear();
				filterMethodOfDeliveryItems=null;
			}	
		}
		else{
			//Change from a default to a selected value. Repopulate other which still has a default. 
			
			if(filterSitusMatrix.getTransactionTypeCode()==null || filterSitusMatrix.getTransactionTypeCode().length()==0){
				filterSitusMatrix.setTransactionTypeCode("");
				setInputValue(transactionTypeCodeMenu, "");
				if(filterTransactionTypeCodeItems!=null){
					filterTransactionTypeCodeItems.clear();
					filterTransactionTypeCodeItems=null;
				}
			}
			
			//if(filterSitusMatrix.getRatetypeCode()==null || filterSitusMatrix.getRatetypeCode().length()==0){
			//	if(filterRateTypeCodeItems!=null){
			//	 	filterRateTypeCodeItems.clear();
			//		filterRateTypeCodeItems=null;
			//	}
			//}

			if(filterSitusMatrix.getMethodDeliveryCode()==null || filterSitusMatrix.getMethodDeliveryCode().length()==0){
				filterSitusMatrix.setMethodDeliveryCode("");
				setInputValue(methodOfDeliveryMenu, "");
				if(filterMethodOfDeliveryItems!=null){
					filterMethodOfDeliveryItems.clear();
					filterMethodOfDeliveryItems=null;
				}
			}
		}
		
		getFilterTransactionTypeCodeItems();
		getFilterRateTypeCodeItems();
		getFilterMethodOfDeliveryItems();
    }
	
	public void filterMethodDeliveryCodeChanged(ActionEvent e) {
		String code = (String) ((HtmlSelectOneMenu) ((HtmlAjaxSupport) e.getComponent()).getParent()).getSubmittedValue();
		String codeOld = filterSitusMatrix.getMethodDeliveryCode();
		
		filterSitusMatrix.setMethodDeliveryCode(code);
		if(codeOld!=null && codeOld.length()>0){
			//Change from a selected value to a selected value. Repopulate other with distinct. 
			
			filterSitusMatrix.setTransactionTypeCode("");
			setInputValue(transactionTypeCodeMenu, "");
			if(filterTransactionTypeCodeItems!=null){
			 	filterTransactionTypeCodeItems.clear();
				filterTransactionTypeCodeItems=null;
			}

			filterSitusMatrix.setRatetypeCode("");
			setInputValue(rateTypeCodeMenu, "");
			if(filterRateTypeCodeItems!=null){
			 	filterRateTypeCodeItems.clear();
				filterRateTypeCodeItems=null;
			}

			//filterSitusMatrix.setMethodDeliveryCode("");
			//if(filterMethodOfDeliveryItems!=null){
			// 	filterMethodOfDeliveryItems.clear();
			//	filterMethodOfDeliveryItems=null;
			//}
		}
		else{
			//Change from a default to a selected value. Repopulate other which still has a default. 
			
			if(filterSitusMatrix.getTransactionTypeCode()==null || filterSitusMatrix.getTransactionTypeCode().length()==0){
				filterSitusMatrix.setTransactionTypeCode("");
				setInputValue(transactionTypeCodeMenu, "");
				if(filterTransactionTypeCodeItems!=null){
					filterTransactionTypeCodeItems.clear();
					filterTransactionTypeCodeItems=null;
				}
			}
			
			if(filterSitusMatrix.getRatetypeCode()==null || filterSitusMatrix.getRatetypeCode().length()==0){
				filterSitusMatrix.setRatetypeCode("");
				setInputValue(rateTypeCodeMenu, "");
				if(filterRateTypeCodeItems!=null){
				 	filterRateTypeCodeItems.clear();
					filterRateTypeCodeItems=null;
				}
			}

			//if(filterSitusMatrix.getMethodDeliveryCode()==null || filterSitusMatrix.getMethodDeliveryCode().length()==0){
			//	if(filterMethodOfDeliveryItems!=null){
			//		filterMethodOfDeliveryItems.clear();
			//		filterMethodOfDeliveryItems=null;
			//	}
			//}
		}
		
		getFilterTransactionTypeCodeItems();
		getFilterRateTypeCodeItems();
		getFilterMethodOfDeliveryItems();
	}
	
	
	//xxxxxxxxxxxxxxxxxxxxxxxxxxxxx
	public void updateTransactionTypeChanged(ActionEvent e) {
		String code = (String) ((HtmlSelectOneMenu) ((HtmlAjaxSupport) e.getComponent()).getParent()).getSubmittedValue();
		String codeOld = updateSitusMatrixRule.getTransactionTypeCode();
	
		updateSitusMatrixRule.setTransactionTypeCode(code);
		if(codeOld!=null && codeOld.length()>0){
			//Change from a selected value to a selected value. Repopulate other with distinct. 
			updateSitusMatrixRule.setRatetypeCode("");
			setInputValue(rateTypeCodeMenuHeader, "");
			if(headerRateTypeCodeItems!=null){
			 	headerRateTypeCodeItems.clear();
				headerRateTypeCodeItems=null;
			}

			updateSitusMatrixRule.setMethodDeliveryCode("");
			setInputValue(methodOfDeliveryMenuHeader, "");
			if(headerMethodOfDeliveryItems!=null){
			 	headerMethodOfDeliveryItems.clear();
				headerMethodOfDeliveryItems=null;
			}
		}
		else{
			//Change from a default to a selected value. Repopulate other which still has a default. 
			if(updateSitusMatrixRule.getRatetypeCode()==null || updateSitusMatrixRule.getRatetypeCode().length()==0){
				updateSitusMatrixRule.setRatetypeCode("");
				setInputValue(rateTypeCodeMenuHeader, "");
				if(headerRateTypeCodeItems!=null){
				 	headerRateTypeCodeItems.clear();
					headerRateTypeCodeItems=null;
				}
			}

			if(updateSitusMatrixRule.getMethodDeliveryCode()==null || updateSitusMatrixRule.getMethodDeliveryCode().length()==0){
				updateSitusMatrixRule.setMethodDeliveryCode("");
				setInputValue(methodOfDeliveryMenuHeader, "");
				if(headerMethodOfDeliveryItems!=null){
					headerMethodOfDeliveryItems.clear();
					headerMethodOfDeliveryItems=null;
				}
			}
		}
		
		getHeaderTransactionTypeCodeItems();
		getHeaderRateTypeCodeItems();
		getHeaderMethodOfDeliveryItems();
		
		//Rebuild driver mapping 
		rebuildSitusMatrixNameUpdateMap(updateSitusMatrixRule);
    }
	
	public void updateRateTypeChanged(ActionEvent e) {
		String code = (String) ((HtmlSelectOneMenu) ((HtmlAjaxSupport) e.getComponent()).getParent()).getSubmittedValue();
		String codeOld = updateSitusMatrixRule.getRatetypeCode();

		updateSitusMatrixRule.setRatetypeCode(code);
		if(codeOld!=null && codeOld.length()>0){
			//Change from a selected value to a selected value. Repopulate other with distinct. 
			
			updateSitusMatrixRule.setTransactionTypeCode("");
			setInputValue(transactionTypeCodeMenuHeader, "");
			if(headerTransactionTypeCodeItems!=null){
			 	headerTransactionTypeCodeItems.clear();
				headerTransactionTypeCodeItems=null;
			}
			
			//updateSitusMatrixRule.setRatetypeCode("");
			//if(filterRateTypeCodeItems!=null){
			// 	filterRateTypeCodeItems.clear();
			//	filterRateTypeCodeItems=null;
			//}

			updateSitusMatrixRule.setMethodDeliveryCode("");
			setInputValue(methodOfDeliveryMenuHeader, "");
			if(headerMethodOfDeliveryItems!=null){
			 	headerMethodOfDeliveryItems.clear();
				headerMethodOfDeliveryItems=null;
			}	
		}
		else{
			//Change from a default to a selected value. Repopulate other which still has a default. 
			
			if(updateSitusMatrixRule.getTransactionTypeCode()==null || updateSitusMatrixRule.getTransactionTypeCode().length()==0){
				updateSitusMatrixRule.setTransactionTypeCode("");
				setInputValue(transactionTypeCodeMenuHeader, "");
				if(headerTransactionTypeCodeItems!=null){
					headerTransactionTypeCodeItems.clear();
					headerTransactionTypeCodeItems=null;
				}
			}
			
			//if(updateSitusMatrixRule.getRatetypeCode()==null || updateSitusMatrixRule.getRatetypeCode().length()==0){
			//	if(headerRateTypeCodeItems!=null){
			//	 	headerRateTypeCodeItems.clear();
			//		headerRateTypeCodeItems=null;
			//	}
			//}

			if(updateSitusMatrixRule.getMethodDeliveryCode()==null || updateSitusMatrixRule.getMethodDeliveryCode().length()==0){
				updateSitusMatrixRule.setMethodDeliveryCode("");
				setInputValue(methodOfDeliveryMenuHeader, "");
				if(headerMethodOfDeliveryItems!=null){
					headerMethodOfDeliveryItems.clear();
					headerMethodOfDeliveryItems=null;
				}
			}
		}
		
		getHeaderTransactionTypeCodeItems();
		getHeaderRateTypeCodeItems();
		getHeaderMethodOfDeliveryItems();
		
		//Rebuild driver mapping 
		rebuildSitusMatrixNameUpdateMap(updateSitusMatrixRule);
    }
	
	public void updateMethodofDeliveryChanged(ActionEvent e) {
		String code = (String) ((HtmlSelectOneMenu) ((HtmlAjaxSupport) e.getComponent()).getParent()).getSubmittedValue();
		String codeOld = updateSitusMatrixRule.getMethodDeliveryCode();
		
		updateSitusMatrixRule.setMethodDeliveryCode(code);
		if(codeOld!=null && codeOld.length()>0){
			//Change from a selected value to a selected value. Repopulate other with distinct. 
			
			updateSitusMatrixRule.setTransactionTypeCode("");
			setInputValue(transactionTypeCodeMenuHeader, "");
			if(headerTransactionTypeCodeItems!=null){
			 	headerTransactionTypeCodeItems.clear();
				headerTransactionTypeCodeItems=null;
			}

			updateSitusMatrixRule.setRatetypeCode("");
			setInputValue(rateTypeCodeMenuHeader, "");
			if(headerRateTypeCodeItems!=null){
			 	headerRateTypeCodeItems.clear();
				headerRateTypeCodeItems=null;
			}

			//updateSitusMatrixRule.setMethodDeliveryCode("");
			//if(headerMethodOfDeliveryItems!=null){
			// 	headerMethodOfDeliveryItems.clear();
			//	headerMethodOfDeliveryItems=null;
			//}
		}
		else{
			//Change from a default to a selected value. Repopulate other which still has a default. 
			
			if(updateSitusMatrixRule.getTransactionTypeCode()==null || updateSitusMatrixRule.getTransactionTypeCode().length()==0){
				updateSitusMatrixRule.setTransactionTypeCode("");
				setInputValue(transactionTypeCodeMenuHeader, "");
				if(headerTransactionTypeCodeItems!=null){
					headerTransactionTypeCodeItems.clear();
					headerTransactionTypeCodeItems=null;
				}
			}
			
			if(updateSitusMatrixRule.getRatetypeCode()==null || updateSitusMatrixRule.getRatetypeCode().length()==0){
				updateSitusMatrixRule.setRatetypeCode("");
				setInputValue(rateTypeCodeMenuHeader, "");
				if(headerRateTypeCodeItems!=null){
				 	headerRateTypeCodeItems.clear();
					headerRateTypeCodeItems=null;
				}
			}

			//if(updateSitusMatrixRule.getMethodDeliveryCode()==null || updateSitusMatrixRule.getMethodDeliveryCode().length()==0){
			//	if(filterMethodOfDeliveryItems!=null){
			//		filterMethodOfDeliveryItems.clear();
			//		filterMethodOfDeliveryItems=null;
			//	}
			//}
		}
		
		getHeaderTransactionTypeCodeItems();
		getHeaderRateTypeCodeItems();
		getHeaderMethodOfDeliveryItems();
		
		//Rebuild driver mapping 
		rebuildSitusMatrixNameUpdateMap(updateSitusMatrixRule);
	}
	//xxxxxxxxxxxxxxxxxxxxxxxxxxxxx
	
	private List<String> getDistinctSitusDriverColumn(String distinctColumn, String transactionTypeCode, String ratetypeCode, String methodDeliveryCode){
		//like distinctColumn=TRANSACTION_TYPE_CODE, RATETYPE_CODE, METHOD_DELIVERY_CODE
		//whereClause=TRANSACTION_TYPE_CODE='SALE' AND RATETYPE_CODE='0' AND METHOD_DELIVERY_CODE = '1'
		
		String whereClause = "";
		
		if(transactionTypeCode!=null && transactionTypeCode.length()>0){
			if(whereClause.length()>0){
				whereClause = whereClause + " AND ";
			}
			whereClause = whereClause + " TRANSACTION_TYPE_CODE='" + transactionTypeCode + "' ";
		}
		
		if(ratetypeCode!=null && ratetypeCode.length()>0){
			if(whereClause.length()>0){
				whereClause = whereClause + " AND ";
			}
			whereClause = whereClause + " RATETYPE_CODE='" + ratetypeCode + "' ";
		}
		
		if(methodDeliveryCode!=null && methodDeliveryCode.length()>0){
			if(whereClause.length()>0){
				whereClause = whereClause + " AND ";
			}
			whereClause = whereClause + " METHOD_DELIVERY_CODE='" + methodDeliveryCode + "' ";
		}
		
		return situsDriverService.getDistinctSitusDriverColumn(distinctColumn, whereClause);
	}
	
	public void updateMethodofDeliveryChanged(ValueChangeEvent e){	
		String code = (String)e.getNewValue();
		updateSitusMatrixRule.setMethodDeliveryCode(code);
		
		//Rebuild driver mapping 
		rebuildSitusMatrixNameUpdateMap(updateSitusMatrixRule);
    }
	
	public void searchHeaderCountryChanged(ValueChangeEvent e){
		String newCountry = (String)e.getNewValue();
		if(newCountry == null) {
			headerSitusMatrix.setSitusCountryCode("");
		}
		else {
			headerSitusMatrix.setSitusCountryCode(newCountry);
		}
	
		if(headerStateMenuItems!=null) headerStateMenuItems.clear();
		headerStateMenuItems = createHeaderStateMenuItems(headerSitusMatrix.getSitusCountryCode());
		headerSitusMatrix.setSitusStateCode("");
		
		clearJurisdiction();
		if(!isAllowRetrieveJurisdiction(headerSitusMatrix)){
			return;
		}
		
		//Impossible to get here...
		//Refresh Jurisdictions
		this.getJurisdictions();
		disableAllJurisdictionCheck();
		
		resetMasterRulesRecords();
    }
	
	public void searchHeaderStateChanged(ActionEvent e) {
		clearJurisdiction();
		if(!isAllowRetrieveJurisdiction(headerSitusMatrix)){
			return;
		}
		
		//Refresh Jurisdictions
		this.getJurisdictions();
		disableAllJurisdictionCheck();
		
		resetMasterRulesRecords();
	}
	
	public void searchHeaderStateChanged(ValueChangeEvent e){
		String newState = (String)e.getNewValue();
		if(newState != null && newState.length()>0 ) {
			headerSitusMatrix.setSitusStateCode(newState);
		}
		
		clearJurisdiction();
		if(!isAllowRetrieveJurisdiction(headerSitusMatrix)){
			return;
		}
		
		//Refresh Jurisdictions
		this.getJurisdictions();
		disableAllJurisdictionCheck();
		
		resetMasterRulesRecords();
    }
	
	public String searchHeaderStateChanged(){
		clearJurisdiction();
		if(!isAllowRetrieveJurisdiction(headerSitusMatrix)){
			return "";
		}
		
		//Refresh Jurisdictions
		this.getJurisdictions();
		disableAllJurisdictionCheck();
		
		resetMasterRulesRecords();
		
		return "";
    }
	
	private void resetMasterRulesRecords(){
		SitusMatrix aSitusMatrix = new SitusMatrix();	
		aSitusMatrix.setTransactionTypeCode(this.selectedSitusMatrix.getTransactionTypeCode());
		aSitusMatrix.setRatetypeCode(this.selectedSitusMatrix.getRatetypeCode());
		aSitusMatrix.setMethodDeliveryCode(this.selectedSitusMatrix.getMethodDeliveryCode());
		aSitusMatrix.setSitusCountryCode(this.selectedSitusDetailMatrix.getSitusCountryCode());
		aSitusMatrix.setSitusStateCode(this.selectedSitusDetailMatrix.getSitusStateCode());
		aSitusMatrix.setJurLevel(this.selectedSitusDetailMatrix.getJurLevel());
		
		retrieveMasterRulesRecords(aSitusMatrix);
	}
	
	private void clearJurisdiction(){
		levelList = new ArrayList<NexusJurisdictionItemDTO>();
		countyList = new ArrayList<NexusJurisdictionItemDTO>();
		cityList = new ArrayList<NexusJurisdictionItemDTO>(); 
		stjList = new ArrayList<NexusJurisdictionItemDTO>(); 
		resetSelections();
		resetSelectionMaps();
	}
	
	public void searchCountryChanged(ValueChangeEvent e){
		String newCountry = (String)e.getNewValue();
		if(newCountry == null) {
			filterSitusMatrix.setSitusCountryCode("");
		}
		else {
			filterSitusMatrix.setSitusCountryCode(newCountry);
		}
	
		if(stateMenuItems!=null) stateMenuItems.clear();

		stateMenuItems = getCacheManager().createStateItems(filterSitusMatrix.getSitusCountryCode());
		filterSitusMatrix.setSitusStateCode("");
    }
	
	public void setActionRole(SitusRoleAction  actionRole) {
		this.actionRole = actionRole;		
	}
	
	public boolean getIsAdminActionRole() {
		return isAdminActionRole();
	}
	
	public boolean isAdminActionRole(){
		return (this.actionRole.equals(SitusRoleAction.adminRole));
	}
	
	public boolean isUserActionRole(){
		return (this.actionRole.equals(SitusRoleAction.userRole));
	}
	
	public SitusRoleAction getActionRole() {
		return this.actionRole;		
	}
	
	public List<SelectItem> getMatrixSelectItems() { 
	   if (matrixSelectItems == null) {
		   List<SelectItem> selectItems = new ArrayList<SelectItem>();
		   selectItems.add(new SelectItem("",""));
			
		   Map<String, String > localMatrixNameMap = getMatrixNameMap();
		   int i=1;
		   for (String matrixCode : localMatrixNameMap.keySet()) {
			   selectItems.add(new SelectItem(new Long(i), "" + i));
			   i++;
		   }
		   	
		   matrixSelectItems = selectItems;
	   }	    
	   return matrixSelectItems;
	}
	
	public String getActionText() {
		if(currentAction.equals(EditAction.ADD)){
			return "Add ";
		}
		else if(currentAction.equals(EditAction.UPDATE)){
			return "Update";
		}
		else if(currentAction.equals(EditAction.DELETE)){
			return "Delete";
		}

		return "";
	}
	
	public String displayAddMatrixAction() {
		currentAction = EditAction.ADD;
		currentMatrixAction = SitusMatrixAction.addSitusMatrix;
		
		headerSitusMatrix = new SitusMatrix();
		headerSitusMatrix.setTransactionTypeCode("");
		headerSitusMatrix.setRatetypeCode("");
		headerSitusMatrix.setMethodDeliveryCode("");

		getHeaderCountryMenuItems();
		headerSitusMatrix.setSitusCountryCode("");
		
		if(headerStateMenuItems!=null) headerStateMenuItems.clear();

		headerStateMenuItems = getCacheManager().createStateItems("");
		headerSitusMatrix.setSitusStateCode("");
		
		clearJurisdiction();
		//if(!isAllowRetrieveJurisdiction(headerSitusMatrix)){
		//	return "";
		//}
		
		this.getJurisdictions();
		disableAllJurisdictionCheck();
		
		resetMasterRulesRecords();
		
		return "situs_admin_matrix_rules_main";
	}
	
	public boolean getIsUpdateSitusMatrix(){
		return (currentMatrixAction.equals(SitusMatrixAction.updateSitusMatrix)) ;
	}
	
	public boolean getIsDeleteSitusMatrix(){
		return (currentMatrixAction.equals(SitusMatrixAction.deleteSitusMatrix)) ;
	}
	
	public boolean getIsUserDisable(){
		if(selectedRulesDetailSitusMatrix!=null && !selectedRulesDetailSitusMatrix.getCustomBooleanFlag() && isUserActionRole()){
			return true;
		}
		
		return false;
	}
	
	public String displayUpdateMatrixAction() {
		if(selectedSitusMatrix==null || selectedSitusDetailMatrix==null){
			return "";
		}
		
		currentAction = EditAction.UPDATE;
		currentMatrixAction = SitusMatrixAction.updateSitusMatrix;
		
		headerSitusMatrix = new SitusMatrix();
		headerSitusMatrix.setTransactionTypeCode(selectedSitusMatrix.getTransactionTypeCode());
		headerSitusMatrix.setRatetypeCode(selectedSitusMatrix.getRatetypeCode());
		headerSitusMatrix.setMethodDeliveryCode(selectedSitusMatrix.getMethodDeliveryCode());

		getHeaderCountryMenuItems();
		headerSitusMatrix.setSitusCountryCode(selectedSitusDetailMatrix.getSitusCountryCode());
		
		if(headerStateMenuItems!=null) headerStateMenuItems.clear();

		headerStateMenuItems = getCacheManager().createStateItems(headerSitusMatrix.getSitusCountryCode());
		headerSitusMatrix.setSitusStateCode(selectedSitusDetailMatrix.getSitusStateCode());
		
		clearJurisdiction();
		if(!isAllowRetrieveJurisdiction(headerSitusMatrix)){
			return "";
		}
		
		this.getJurisdictions();
		disableAllJurisdictionCheck();
		
		resetMasterRulesRecords();
		
		return "situs_admin_matrix_rules_main";
	}
	
	public String displayCopyAddMatrixAction() {
		if(displayUpdateMatrixAction()==null || displayUpdateMatrixAction().length()==0){
			return "";
		}
		currentAction = EditAction.COPY_ADD;
		currentMatrixAction = SitusMatrixAction.copyaddSitusMatrix;
		
		return "situs_admin_matrix_rules_main";
	}
	
	public String displayDeleteMatrixAction() {	
		if(displayUpdateMatrixAction()==null || displayUpdateMatrixAction().length()==0){
			return "";
		}
		currentAction = EditAction.DELETE;
		currentMatrixAction = SitusMatrixAction.deleteSitusMatrix;
		
		return "situs_admin_matrix_rules_main";
	}
	
	public Map<String, String > getTransactionTypeMap(){
    	if(transactionTypeMap==null){
    		transactionTypeMap = new LinkedHashMap<String, String >();
    		
    		List<ListCodes> listCodes = cacheManager.getListCodesByType("TRANSTYPE");
 		   	if(listCodes != null) {
 			   for(ListCodes lc : listCodes) {
 				  transactionTypeMap.put(lc.getCodeCode(), lc.getDescription());
 			   }
 		   	}
    	}
    	return transactionTypeMap;
    }
	
	public Map<String, String > getRatetypeMap(){
    	if(ratetypeMap==null){
    		ratetypeMap = new LinkedHashMap<String, String >();
    		
    		List<ListCodes> listCodes = cacheManager.getListCodesByType("RATETYPE");
 		   	if(listCodes != null) {
 			   for(ListCodes lc : listCodes) {
 				  ratetypeMap.put(lc.getCodeCode(), lc.getDescription());
 			   }
 		   	}
    	}
    	return ratetypeMap;
    }
	
	public Map<String, String > getMethodDeliveryMap(){
    	if(methodDeliveryMap==null){
    		methodDeliveryMap = new LinkedHashMap<String, String >();
    		
    		List<ListCodes> listCodes = cacheManager.getListCodesByType("MOD");
 		   	if(listCodes != null) {
 			   for(ListCodes lc : listCodes) {
 				  methodDeliveryMap.put(lc.getCodeCode(), lc.getDescription());
 			   }
 		   	}
    	}
    	return methodDeliveryMap;
    }
	
	public Map<String, String > getMatrixNameMap(){
    	if(matrixNameMap==null){
    		matrixNameMap = new LinkedHashMap<String, String >();
    		
    		List<ListCodes> listCodes = cacheManager.getListCodesByType("LOCNTYPE");
 		   	if(listCodes != null) {
 			   for(ListCodes lc : listCodes) {
 				  matrixNameMap.put(lc.getCodeCode(), lc.getDescription());
 			   }
 		   	}
    	}
    	return matrixNameMap;
    }
	
	public Map<String, String > getTaxTypeMapMap(){
    	if(taxTypeMap==null){
    		taxTypeMap= new LinkedHashMap<String, String >();
    		
    		List<ListCodes> listCodes = cacheManager.getListCodesByType("TAXTYPE");
 		   	if(listCodes != null) {
 			   for(ListCodes lc : listCodes) {
 				  taxTypeMap.put(lc.getCodeCode(), lc.getDescription());
 			   }
 		   	}
    	}
    	return taxTypeMap;
    }
	
	public Map<String, String > getSitusDriverTypeMap(){
    	if(situsDriverTypeMap==null){
    		situsDriverTypeMap = new LinkedHashMap<String, String >();
    		situsDriverTypeMap.put("*ALL", "*ALL");
    		
    		List<ListCodes> listCodes = cacheManager.getListCodesByType("SITUSTYPE");
 		   	if(listCodes != null) {
 			   for(ListCodes lc : listCodes) {
 				  situsDriverTypeMap.put(lc.getCodeCode(), lc.getDescription());
 			   }
 		   	}
    	}
    	return situsDriverTypeMap;
    }

	public Map<String, String > getSitusMatrixNameMainMap(){
    	return situsMatrixNameMainMap;
    }
	
	public boolean rebuildSitusMatrixNameMainMap(SitusMatrix aSitusMatrixRule){
		if(situsMatrixNameMainMap!=null){
			situsMatrixNameMainMap.clear();
			situsMatrixNameMainMap=null;
		}
		
    	if(aSitusMatrixRule.getTransactionTypeCode()==null || aSitusMatrixRule.getTransactionTypeCode().length()==0 ||
    			aSitusMatrixRule.getMethodDeliveryCode()==null || aSitusMatrixRule.getMethodDeliveryCode().length()==0 ||
    			aSitusMatrixRule.getRatetypeCode()==null || aSitusMatrixRule.getRatetypeCode().length()==0){
    		
    		//Default map
    		situsMatrixNameMainMap = new LinkedHashMap<String, String >();
 		    for (String driverNumber : getSitusMatrixNameMap().keySet()) {
 			  situsMatrixNameMainMap.put(driverNumber, (String)getSitusMatrixNameMap().get(driverNumber)); 
 		    }
    		return false;
    	}
    	
    	SitusDriver aSitusDriver = new SitusDriver();
    	aSitusDriver.setTransactionTypeCode(aSitusMatrixRule.getTransactionTypeCode());
    	aSitusDriver.setMethodDeliveryCode(aSitusMatrixRule.getMethodDeliveryCode());
    	aSitusDriver.setRatetypeCode(aSitusMatrixRule.getRatetypeCode());
    	
    	SitusDriver matcdhedSitusDriver = (SitusDriver)situsDriverService.find(aSitusDriver);
    	if(matcdhedSitusDriver==null){
    		//Default map
    		situsMatrixNameMainMap = new LinkedHashMap<String, String >();
 		    for (String driverNumber : getSitusMatrixNameMap().keySet()) {
 			  situsMatrixNameMainMap.put(driverNumber, (String)getSitusMatrixNameMap().get(driverNumber)); 
 		    }
    		return false;
    	}
    	
    	List<SitusDriverDetail> aSitusDriverDetail = situsDriverService.findAllSitusDriverDetailBySitusDriverId(matcdhedSitusDriver.getSitusDriverId()); 
		
	    if(aSitusDriverDetail != null) {
	    	situsMatrixNameMainMap = new LinkedHashMap<String, String >();
	    	NumberFormat formatter = new DecimalFormat("00");
	   		Method theMethod = null;
	   		String name = "";
	   		String value = "";
	   		
	   		getMatrixNameMap();
	   		for(SitusDriverDetail lc : aSitusDriverDetail) {		
	   			name = "DRIVER_" + formatter.format(lc.getDriverId().intValue());
	   			situsMatrixNameMainMap.put(name, (String)getMatrixNameMap().get(lc.getLocationTypeCode())); 
		   }
	   }
	    else{
	    	return false;
	    }
		    	
	   return true;
	}
	
	public Map<String, String > getSitusMatrixNameMap(){
    	if(situsMatrixNameMap==null){
    		situsMatrixNameMap = new LinkedHashMap<String, String >();
    		
    		List<ListCodes> listCodes = cacheManager.getListCodesByType("LOCNTYPE");
 		   	if(listCodes != null) {
 		   		NumberFormat formatter = new DecimalFormat("00");
 		   		Method theMethod = null;
 		   		String name = "";
 		   		String value = "";
 		   		int i = 1;
 		   		for(ListCodes lc : listCodes) {		
 		   			name = "DRIVER_" + formatter.format(i);
 		   			situsMatrixNameMap.put(name, lc.getDescription()); 
 		   			i++;
 			   }
 		   	}
    	}
    	return situsMatrixNameMap;
    }
	
	public Map<String, String > getSitusMatrixNameUpdateMap(){
    	return situsMatrixNameUpdateMap;
    }
	
	public Map<String, Boolean > getSitusMatrixNameMandatoryMap(){
    	return situsMatrixNameMandatoryMap;
    }
	
	public boolean rebuildSitusMatrixNameUpdateMap(SitusMatrix aSitusMatrixRule){
		if(situsMatrixNameUpdateMap!=null){
			situsMatrixNameUpdateMap.clear();
			situsMatrixNameUpdateMap=null;
		}
		
		if(situsMatrixNameMandatoryMap!=null){
			situsMatrixNameMandatoryMap.clear();
			situsMatrixNameMandatoryMap=null;
		}
		
    	if(aSitusMatrixRule.getTransactionTypeCode()==null || aSitusMatrixRule.getTransactionTypeCode().length()==0 ||
    			aSitusMatrixRule.getMethodDeliveryCode()==null || aSitusMatrixRule.getMethodDeliveryCode().length()==0 ||
    			aSitusMatrixRule.getRatetypeCode()==null || aSitusMatrixRule.getRatetypeCode().length()==0){
    		return false;
    	}
    	
    	SitusDriver aSitusDriver = new SitusDriver();
    	aSitusDriver.setTransactionTypeCode(aSitusMatrixRule.getTransactionTypeCode());
    	aSitusDriver.setMethodDeliveryCode(aSitusMatrixRule.getMethodDeliveryCode());
    	aSitusDriver.setRatetypeCode(aSitusMatrixRule.getRatetypeCode());
    	
    	SitusDriver matcdhedSitusDriver = (SitusDriver)situsDriverService.find(aSitusDriver);
    	if(matcdhedSitusDriver==null){
    		return false;
    	}
    	
    	List<SitusDriverDetail> aSitusDriverDetail = situsDriverService.findAllSitusDriverDetailBySitusDriverId(matcdhedSitusDriver.getSitusDriverId()); 
		
	    if(aSitusDriverDetail != null) {
	    	situsMatrixNameUpdateMap = new LinkedHashMap<String, String >();
	    	situsMatrixNameMandatoryMap = new LinkedHashMap<String, Boolean >();
	    	
	    	NumberFormat formatter = new DecimalFormat("00");
	   		Method theMethod = null;
	   		String name = "";
	   		String value = "";
	   		
	   		getMatrixNameMap();
	   		for(SitusDriverDetail lc : aSitusDriverDetail) {		
	   			name = "DRIVER_" + formatter.format(lc.getDriverId().intValue());
	   			situsMatrixNameUpdateMap.put(name, (String)getMatrixNameMap().get(lc.getLocationTypeCode())); 
	   			situsMatrixNameMandatoryMap.put(name, lc.getMandatoryBooleanFlag()); 
		   }
	   }
	    else{
	    	return false;
	    }
		    	
	   return true;
	}
	
	public void retrieveFilterSitusMatrix() {
		rebuildSitusMatrixNameMainMap(new SitusMatrix());
		
		//Grid  1:
		selectedRowIndex = -1;
		selectedSitusMatrix = new SitusMatrix();
		SitusMatrix aNull = null;
		situsMatrixDataModel.setCriteria(aNull);

		//Grid 2:
		selectedDetailRowIndex= -1;
		selectedSitusDetailMatrix = new SitusMatrix();
		this.setSitusMatrixList(null);

		//Grid 3:
		selectedRulesRowIndex  = -1;
		selectedRulesSitusMatrix = new SitusMatrix();
		SitusMatrix bNull = null;
		situsMatrixRulesDataModel.setCriteria(bNull);

		//Grid 4:
		selectedRulesDetailRowIndex  = -1;
		selectedRulesDetailSitusMatrix = new SitusMatrix();
		this.setSitusMatrixRulersDetailList(null);
		

		SitusMatrix aSitusMatrix = new SitusMatrix();
		if(filterSitusMatrix.getTransactionTypeCode() != null){
			aSitusMatrix.setTransactionTypeCode(filterSitusMatrix.getTransactionTypeCode());
		}
		if(filterSitusMatrix.getRatetypeCode() != null){
			aSitusMatrix.setRatetypeCode(filterSitusMatrix.getRatetypeCode());
		}
		if(filterSitusMatrix.getMethodDeliveryCode()!= null){
			aSitusMatrix.setMethodDeliveryCode(filterSitusMatrix.getMethodDeliveryCode());
		}

		//0003320
		if(filterSitusMatrix.getSitusCountryCode()!= null){
			aSitusMatrix.setSitusCountryCode(filterSitusMatrix.getSitusCountryCode());
		}
		if(filterSitusMatrix.getSitusStateCode()!= null){
			aSitusMatrix.setSitusStateCode(filterSitusMatrix.getSitusStateCode());
		}
		if(filterSitusMatrix.getJurLevel()!= null){
			aSitusMatrix.setJurLevel(filterSitusMatrix.getJurLevel());
		}
		
		filterSitusMatrixSearch.setSitusCountryCode(filterSitusMatrix.getSitusCountryCode());
		filterSitusMatrixSearch.setSitusStateCode(filterSitusMatrix.getSitusStateCode());
		filterSitusMatrixSearch.setJurLevel(filterSitusMatrix.getJurLevel());

		situsMatrixDataModel.setCriteria(aSitusMatrix);
		
		updateMatrixDetailList();
	}

	public SitusMatrix getFilterWhatIfSitusMatrix(){
		return filterWhatIfSitusMatrix;
	}
	
	public SitusMatrix getFilterSitusMatrix(){
		return filterSitusMatrix;
	}
	
	public SitusMatrix getHeaderSitusMatrix(){
		return headerSitusMatrix;
	}
	
	public void setHeaderSitusMatrix(SitusMatrix headerSitusMatrix){
		this.headerSitusMatrix = headerSitusMatrix;
	}
	
	public SitusMatrixDTO getSelectedSitusMatrixDTO(){
		return selectedSitusMatrixDTO;
	}

	public SitusMatrix getSelectedSitusMatrix(){
		return selectedSitusMatrix;
	}
	
	public SitusMatrix getSelectedSitusDetailMatrix(){
		return selectedSitusDetailMatrix;
	}

	public SitusMatrix getUpdateSitusMatrixRule(){
		return updateSitusMatrixRule;
	}
	
	public SitusMatrix getSelectedRulesSitusMatrix(){
		return selectedRulesSitusMatrix ;
	}
	
	public SitusMatrix getSelectedRulesDetailSitusMatrix (){
		return selectedRulesDetailSitusMatrix  ;
	}
	
	public String resetFilterSearchAction(){
		filterSitusMatrix = new SitusMatrix();
		selectedDisplay = "all";
		
		filterSitusMatrix.setTransactionTypeCode("");
		setInputValue(transactionTypeCodeMenu, "");
		if(filterTransactionTypeCodeItems!=null){
		 	filterTransactionTypeCodeItems.clear();
			filterTransactionTypeCodeItems=null;
		}

		filterSitusMatrix.setRatetypeCode("");
		setInputValue(rateTypeCodeMenu, "");
		if(filterRateTypeCodeItems!=null){
		 	filterRateTypeCodeItems.clear();
			filterRateTypeCodeItems=null;
		}

		filterSitusMatrix.setMethodDeliveryCode("");
		setInputValue(methodOfDeliveryMenu, "");
		if(filterMethodOfDeliveryItems!=null){
		 	filterMethodOfDeliveryItems.clear();
			filterMethodOfDeliveryItems=null;
		}
		
		filterSitusMatrix.setSitusStateCode("");
		if(stateMenuItems !=null){
			stateMenuItems.clear();
			stateMenuItems =null;
		}
		
		return null;
	}

	public List<SelectItem> getFilterTransactionTypeCodeItems() { 
	   if (filterTransactionTypeCodeItems == null) {
		   List<SelectItem> selectItems = new ArrayList<SelectItem>();
			
		   selectItems.add(new SelectItem("","Select Transaction Types"));
		   List<String> columns = getDistinctSitusDriverColumn("TRANSACTION_TYPE_CODE", "", filterSitusMatrix.getRatetypeCode(), filterSitusMatrix.getMethodDeliveryCode());
		   if(columns != null) {
			   for(String column : columns) {
				   selectItems.add(new SelectItem(column, getTransactionTypeMap().get(column)));
			   }
		   }
			
		   filterTransactionTypeCodeItems = selectItems;
	   }	    
	   return filterTransactionTypeCodeItems;
	}
	
	public List<SelectItem> getFilterRateTypeCodeItems() { 
	   if (filterRateTypeCodeItems == null) {
		   List<SelectItem> selectItems = new ArrayList<SelectItem>();
			
		   selectItems.add(new SelectItem("","Select Rate Types"));
		   List<String> columns = getDistinctSitusDriverColumn("RATETYPE_CODE", filterSitusMatrix.getTransactionTypeCode(), "", filterSitusMatrix.getMethodDeliveryCode());
		   if(columns != null) {
			   for(String column : columns) {
				   selectItems.add(new SelectItem(column, this.getRatetypeMap().get(column)));
			   }
		   }
			
		   filterRateTypeCodeItems = selectItems;
	   }	    
	   return filterRateTypeCodeItems;
	}
	
	public List<SelectItem> getFilterMethodOfDeliveryItems() { 
	   if (filterMethodOfDeliveryItems == null) {
		   List<SelectItem> selectItems = new ArrayList<SelectItem>();
			
		   selectItems.add(new SelectItem("","Select Methods of Delivery"));
		   List<String> columns = getDistinctSitusDriverColumn("METHOD_DELIVERY_CODE", filterSitusMatrix.getTransactionTypeCode(), filterSitusMatrix.getRatetypeCode(), "");
		   if(columns != null) {
			   for(String column : columns) {
				   selectItems.add(new SelectItem(column, this.getMethodDeliveryMap().get(column)));
			   }
		   }
			
		   filterMethodOfDeliveryItems = selectItems;
	   }	    
	   return filterMethodOfDeliveryItems;
	}
	
	public List<SelectItem> getHeaderTransactionTypeCodeItems() { 
	   if (headerTransactionTypeCodeItems == null) {
		   List<SelectItem> selectItems = new ArrayList<SelectItem>();
			
		   selectItems.add(new SelectItem("","Select Transaction Type"));
		   List<String> columns = getDistinctSitusDriverColumn("TRANSACTION_TYPE_CODE", "", updateSitusMatrixRule.getRatetypeCode(), updateSitusMatrixRule.getMethodDeliveryCode());
		   if(columns != null) {
			   for(String column : columns) {
				   selectItems.add(new SelectItem(column, getTransactionTypeMap().get(column)));
			   }
		   }
			
		   headerTransactionTypeCodeItems = selectItems;
	   }	    
	   return headerTransactionTypeCodeItems;
	}
	
	public List<SelectItem> getHeaderRateTypeCodeItems() { 
	   if (headerRateTypeCodeItems == null) {
		   List<SelectItem> selectItems = new ArrayList<SelectItem>();
			
		   selectItems.add(new SelectItem("","Select Rate Type"));
		   List<String> columns = getDistinctSitusDriverColumn("RATETYPE_CODE", updateSitusMatrixRule.getTransactionTypeCode(), "", updateSitusMatrixRule.getMethodDeliveryCode());
		   if(columns != null) {
			   for(String column : columns) {
				   selectItems.add(new SelectItem(column, this.getRatetypeMap().get(column)));
			   }
		   }
			
		   headerRateTypeCodeItems = selectItems;
	   }	    
	   return headerRateTypeCodeItems;
	}
	
	public List<SelectItem> getHeaderMethodOfDeliveryItems() { 
	   if (headerMethodOfDeliveryItems == null) {
		   List<SelectItem> selectItems = new ArrayList<SelectItem>();
			
		   selectItems.add(new SelectItem("","Select Methods of Delivery"));
		   List<String> columns = getDistinctSitusDriverColumn("METHOD_DELIVERY_CODE", updateSitusMatrixRule.getTransactionTypeCode(), updateSitusMatrixRule.getRatetypeCode(), "");
		   if(columns != null) {
			   for(String column : columns) {
				   selectItems.add(new SelectItem(column, this.getMethodDeliveryMap().get(column)));
			   }
		   }

			
		   headerMethodOfDeliveryItems = selectItems;
	   }	    
	   return headerMethodOfDeliveryItems;
	}

	public List<SelectItem> getUpdateMethodOfDeliveryItems() { 
	   if (updateMethodOfDeliveryItems == null) {
		   List<SelectItem> selectItems = new ArrayList<SelectItem>();
			
		   selectItems.add(new SelectItem("","Select a Method of Delivery"));
		   List<ListCodes> listCodes = cacheManager.getListCodesByType("MOD");
		   if(listCodes != null) {
			   for(ListCodes lc : listCodes) {
				   selectItems.add(new SelectItem(lc.getCodeCode(), lc.getDescription()));
			   }
		   }
			
		   updateMethodOfDeliveryItems = selectItems;
	   }	    
	   return updateMethodOfDeliveryItems;
	}
	
	public List<SelectItem> getUpdateRateTypeCodeItems() { 
	   if (updateRateTypeCodeItems == null) {
		   List<SelectItem> selectItems = new ArrayList<SelectItem>();
			
		   selectItems.add(new SelectItem("","Select a Sales Type"));
		   List<ListCodes> listCodes = cacheManager.getListCodesByType("RATETYPE");
		   if(listCodes != null) {
			   for(ListCodes lc : listCodes) {
				   selectItems.add(new SelectItem(lc.getCodeCode(), lc.getDescription()));
			   }
		   }
			
		   updateRateTypeCodeItems = selectItems;
	   }	    
	   return updateRateTypeCodeItems;
	}
	
	public List<SelectItem> getUpdateTransactionTypeCodeItems() { 
	   if (updateTransactionTypeCodeItems == null) {
		   List<SelectItem> selectItems = new ArrayList<SelectItem>();
			
		   selectItems.add(new SelectItem("","Select a Transaction Type"));
		   List<ListCodes> listCodes = cacheManager.getListCodesByType("TRANSTYPE");
		   if(listCodes != null) {
			   for(ListCodes lc : listCodes) {
				   selectItems.add(new SelectItem(lc.getCodeCode(), lc.getDescription()));
			   }
		   }
			
		   updateTransactionTypeCodeItems = selectItems;
	   }	    
	   return updateTransactionTypeCodeItems;
	}

	public List<SelectItem> getPrimaryTaxTypeItems() { 
	   if (primaryTaxTypeItems == null) {
		   List<SelectItem> selectItems = new ArrayList<SelectItem>();
			
		   selectItems.add(new SelectItem("","Select Primary Tax Type"));
		   List<ListCodes> listCodes = cacheManager.getListCodesByType("TAXTYPE");
		   if(listCodes != null) {
			   for(ListCodes lc : listCodes) {
				   selectItems.add(new SelectItem(lc.getCodeCode(), lc.getDescription()));
			   }
		   }
			
		   primaryTaxTypeItems = selectItems;
	   }	    
	   return primaryTaxTypeItems;
	}
	
	public List<SelectItem> getSecondaryTaxTypeItems() { 
	   if (secondaryTaxTypeItems == null) {
		   List<SelectItem> selectItems = new ArrayList<SelectItem>();
			
		   selectItems.add(new SelectItem("","Select Secondary Tax Type"));
		   List<ListCodes> listCodes = cacheManager.getListCodesByType("TAXTYPE");
		   if(listCodes != null) {
			   for(ListCodes lc : listCodes) {
				   selectItems.add(new SelectItem(lc.getCodeCode(), lc.getDescription()));
			   }
		   }
			
		   secondaryTaxTypeItems = selectItems;
	   }	    
	   return secondaryTaxTypeItems;
	}

	public List<SelectItem> getPrimaryLocationTypeItems() { 
	   if (primaryLocationTypeItems == null) {
		   List<SelectItem> selectItems = new ArrayList<SelectItem>();
			
		   selectItems.add(new SelectItem("","Select Primary Situs Location"));
		   List<ListCodes> listCodes = cacheManager.getListCodesByType("LOCNTYPE");
		   if(listCodes != null) {
			   for(ListCodes lc : listCodes) {
				   selectItems.add(new SelectItem(lc.getCodeCode(), lc.getDescription()));
			   }
		   }
			
		   primaryLocationTypeItems = selectItems;
	   }	    
	   return primaryLocationTypeItems;
	}
	
	public List<SelectItem> getSecondaryLocationTypeItems() { 
		   if (secondaryLocationTypeItems == null) {
			   List<SelectItem> selectItems = new ArrayList<SelectItem>();
				
			   selectItems.add(new SelectItem("","Select Secondary Situs Location"));
			   List<ListCodes> listCodes = cacheManager.getListCodesByType("LOCNTYPE");
			   if(listCodes != null) {
				   for(ListCodes lc : listCodes) {
					   selectItems.add(new SelectItem(lc.getCodeCode(), lc.getDescription()));
				   }
			   }
				
			   secondaryLocationTypeItems = selectItems;
		   }	    
		   return secondaryLocationTypeItems;
		}
	
	public List<SelectItem> getSitusDriverTypeItems() { 
	   if (situsDriverTypeItems == null) {
		   List<SelectItem> selectItems = new ArrayList<SelectItem>();
			
		   selectItems.add(new SelectItem("","*ALL"));
		   List<ListCodes> listCodes = cacheManager.getListCodesByType("SITUSTYPE");
		   if(listCodes != null) {
			   for(ListCodes lc : listCodes) {
				   selectItems.add(new SelectItem(lc.getCodeCode(), lc.getDescription()));
			   }
		   }
			
		   situsDriverTypeItems = selectItems;
	   }	    
	   return situsDriverTypeItems;
	}
	
	public void selectedRowChanged(ActionEvent e) throws AbortProcessingException {
		HtmlDataTable table = (HtmlDataTable) ((HtmlAjaxSupport) e.getComponent()).getParent(); 
		setSelectedRowIndex(table.getRowIndex());
		selectedSitusMatrix = new SitusMatrix();
		selectedSitusMatrix = (SitusMatrix)table.getRowData();
		
		//Grid 2:
		selectedDetailRowIndex= -1;
		selectedSitusDetailMatrix = new SitusMatrix();
		this.setSitusMatrixList(null);

		//Grid 3:
		selectedRulesRowIndex  = -1;
		selectedRulesSitusMatrix = new SitusMatrix();
		SitusMatrix bNull = null;
		situsMatrixRulesDataModel.setCriteria(bNull);

		//Grid 4:
		selectedRulesDetailRowIndex  = -1;
		selectedRulesDetailSitusMatrix = new SitusMatrix();
		this.setSitusMatrixRulersDetailList(null);
		
		rebuildSitusMatrixNameMainMap(selectedSitusMatrix);
		
		updateMatrixDetailList();
	}
	
	public void selectedMatrixDetailRowChanged(ActionEvent e) throws AbortProcessingException {
		HtmlDataTable table = (HtmlDataTable) ((HtmlAjaxSupport) e.getComponent()).getParent(); 
		selectedDetailRowIndex = (table.getRowIndex());	
		selectedSitusDetailMatrix = (SitusMatrix)table.getRowData();
		
		//Grid 3:
		selectedRulesRowIndex  = -1;
		selectedRulesSitusMatrix = new SitusMatrix();
		SitusMatrix bNull = null;
		situsMatrixRulesDataModel.setCriteria(bNull);

		//Grid 4:
		selectedRulesDetailRowIndex  = -1;
		selectedRulesDetailSitusMatrix = new SitusMatrix();
		this.setSitusMatrixRulersDetailList(null);
		
		resetMasterRulesRecords();
	}
	
	public void selectedMasterRowChanged(ActionEvent e) throws AbortProcessingException {
		HtmlDataTable table = (HtmlDataTable) ((HtmlAjaxSupport) e.getComponent()).getParent(); 
		selectedRulesRowIndex = table.getRowIndex();
		selectedRulesSitusMatrix = new SitusMatrix();
		selectedRulesSitusMatrix = (SitusMatrix)table.getRowData();
		
		//Grid 4:
		selectedRulesDetailRowIndex  = -1;
		selectedRulesDetailSitusMatrix = new SitusMatrix();
		this.setSitusMatrixRulersDetailList(null);
		
		//Retrieve detail list
		displayMatrixRulesDetailAction();
	}
	
	public void selectedDetailRowChanged(ActionEvent e) throws AbortProcessingException {
		HtmlDataTable table = (HtmlDataTable) ((HtmlAjaxSupport) e.getComponent()).getParent(); 
		selectedRulesDetailRowIndex  = table.getRowIndex();
		selectedRulesDetailSitusMatrix  = new SitusMatrix();
		selectedRulesDetailSitusMatrix  = (SitusMatrix)table.getRowData();
	
	}
	
	public void displayMatrixRulesDetailAction() {
		if(selectedRulesSitusMatrix==null || selectedRulesSitusMatrix.getSitusMatrixId()==null){
			this.selectedRulesDetailRowIndex  = -1;
			this.selectedRulesDetailSitusMatrix = null;
			this.setSitusMatrixRulersDetailList(null);
			
			return;
		}
	
		this.selectedRulesDetailRowIndex  = -1;
		this.selectedRulesDetailSitusMatrix = null;
		this.setSitusMatrixRulersDetailList(null);
		
		List<SitusMatrix> list = situsMatrixService.getMasterRulesDetailRecords(selectedRulesSitusMatrix);
		this.setSitusMatrixRulersDetailList(list);
	}
	
	public List<SitusMatrixDTO> getSitusMatrixDTOList() {
		return situsMatrixDTOList ;
	}
	
	public void setSitusMatrixList(List<SitusMatrix> situsMatrixList) {
		this.situsMatrixList = situsMatrixList;
	}	
	
	public List<SitusMatrix> getSitusMatrixList() {
		return situsMatrixList ;
	}
	
	public void setSitusMatrixRulersDetailList(List<SitusMatrix> situsMatrixRulersDetailList) {
		this.situsMatrixRulersDetailList = situsMatrixRulersDetailList;
	}	
	
	public String processWhatIfCommand() {
		SitusMatrix aSitusMatrixRule = new SitusMatrix();
		aSitusMatrixRule.setTransactionTypeCode(filterWhatIfSitusMatrix.getTransactionTypeCode());
		aSitusMatrixRule.setRatetypeCode(filterWhatIfSitusMatrix.getRatetypeCode());
		aSitusMatrixRule.setMethodDeliveryCode(filterWhatIfSitusMatrix.getMethodDeliveryCode());
		
		List<SitusMatrix> list = situsMatrixService.getMasterRulesDetailRecords(aSitusMatrixRule);
		this.setSitusMatrixWhatIfList(list);
		
		return "situs_matrix_whatif";
	}

	public List<SitusMatrix> getSitusMatrixRulersDetailList() {
		return situsMatrixRulersDetailList ;
	}
	
	public void setSitusMatrixWhatIfList(List<SitusMatrix> situsMatrixWhatIfList) {
		this.situsMatrixWhatIfList = situsMatrixWhatIfList;
	}	
	
	public List<SitusMatrix> getSitusMatrixWhatIfList() {
		return situsMatrixWhatIfList ;
	}
	
	public void updateMatrixDetailList() {
		if(selectedSitusMatrix==null || selectedSitusMatrix.getSitusMatrixId()==null){
			this.setSitusMatrixList(null);
			return;
		}
		
		SitusMatrix situsMatrixFilter = new SitusMatrix();
		situsMatrixFilter.setTransactionTypeCode(selectedSitusMatrix.getTransactionTypeCode());
		situsMatrixFilter.setRatetypeCode(selectedSitusMatrix.getRatetypeCode());
		situsMatrixFilter.setMethodDeliveryCode(selectedSitusMatrix.getMethodDeliveryCode());
		
		//Get country and state from filter for detail list
		//0003320
		situsMatrixFilter.setSitusCountryCode(filterSitusMatrixSearch.getSitusCountryCode());
		situsMatrixFilter.setSitusStateCode(filterSitusMatrixSearch.getSitusStateCode());
		situsMatrixFilter.setJurLevel(filterSitusMatrixSearch.getJurLevel());
		
		this.setSitusMatrixList(null);
		List<SitusMatrix> list = situsMatrixService.getAllDetailRecords(situsMatrixFilter, "");
		this.setSitusMatrixList(list);
		
		selectedDetailRowIndex= -1;
		selectedSitusDetailMatrix = new SitusMatrix();
	}
	
	public boolean getDeleteAction() {
		return currentAction.equals(EditAction.DELETE);
	}
	
	public boolean getUpdateAction() {
		return currentAction.equals(EditAction.UPDATE);
	}
	
	public Boolean getDisplayButtons() {
		return selectedRowIndex != -1;
	}
	
	public int getSelectedRowIndex() {
		return selectedRowIndex;
	}
	
	public int getSelectedDetailRowIndex() {
		return selectedDetailRowIndex;
	}
	
	public void setSelectedRowIndex(int selectedRowIndex) {
		this.selectedRowIndex = selectedRowIndex;
	}

	public int getSelectedRulesRowIndex() {
		return selectedRulesRowIndex;
	}

	public void setSelectedRulesRowIndex(int selectedRulesRowIndex) {
		this.selectedRulesRowIndex = selectedRulesRowIndex;
	}

	public int getSelectedRulesDetailRowIndex () {
		return selectedRulesDetailRowIndex ;
	}

	public void setSelectedRulesDetailRowIndex (int selectedRulesDetailRowIndex ) {
		this.selectedRulesDetailRowIndex  = selectedRulesDetailRowIndex ;
	}
	
	public CacheManager getCacheManager() {
		return this.cacheManager;
	}

	public void setCacheManager(CacheManager cacheManager) {
		this.cacheManager = cacheManager;
	} 
	
	public SitusMatrixService getSitusMatrixService() {
		return situsMatrixService;
	}

	public void setSitusMatrixService(SitusMatrixService situsMatrixService) {
		this.situsMatrixService = situsMatrixService;
	}
	
	public SitusDriverService getSitusDriverService() {
		return situsDriverService;
	}

	public void setSitusDriverService(SitusDriverService situsDriverService) {
		this.situsDriverService = situsDriverService;
	}
	
	public FilePreferenceBackingBean getFilePreferenceBean() {
		return this.filePreferenceBean;
	}

	public void setFilePreferenceBean(FilePreferenceBackingBean filePreferenceBean) {
		this.filePreferenceBean = filePreferenceBean;
	}
	
	public ListCodesService getListCodesService() {
		return listCodesService;
	}

	public void setListCodesService(ListCodesService listCodesService) {
		this.listCodesService = listCodesService;
	}
	
	public OptionService getOptionService() {
		return optionService;
	}

	public void setOptionService(OptionService optionService) {
		this.optionService = optionService;
	}
	
	public SitusMatrixDataModel getSitusMatrixDataModel() {
		if(!initialized) {
			retrieveFilterSitusMatrix();
			initialized = true;
		}
		return situsMatrixDataModel;
	}

	public void setSitusMatrixDataModel(SitusMatrixDataModel situsMatrixDataModel) {
		this.situsMatrixDataModel = situsMatrixDataModel;
	}

	public SitusMatrixRulesDataModel getSitusMatrixRulesDataModel() {
		return situsMatrixRulesDataModel;
	}

	public void setSitusMatrixRulesDataModel(SitusMatrixRulesDataModel situsMatrixRulesDataModel) {
		this.situsMatrixRulesDataModel = situsMatrixRulesDataModel;
	}
}
