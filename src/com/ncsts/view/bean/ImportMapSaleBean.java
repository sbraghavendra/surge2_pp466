package com.ncsts.view.bean;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.faces.event.AbortProcessingException;
import javax.faces.event.ActionEvent;

import org.ajax4jsf.component.html.HtmlAjaxSupport;
import org.apache.log4j.Logger;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Expression;
import org.richfaces.component.html.HtmlCalendar;
import org.richfaces.component.html.HtmlDataTable;

import com.ncsts.dao.BatchMaintenanceDAO;
import com.ncsts.domain.Auditable;
import com.ncsts.domain.BatchMaintenance;
import com.ncsts.domain.BatchMetadata;
import com.ncsts.domain.ImportMapBatch;
import com.ncsts.jsf.model.BatchMaintenanceDataModel;
import com.ncsts.services.UserService;
import com.ncsts.view.util.BatchStatistics;

public class ImportMapSaleBean {

	private Logger logger = Logger.getLogger(ImportMapSaleBean.class);

	private UserService userService;

	private BatchMaintenanceDAO batchMaintenanceDAO;

	private BatchMaintenance exampleBatchMaintenance;

	private BatchMaintenance selectedBatch;
	private List<ImportMapBatch> selectedBatches;

	private HtmlCalendar batchStartTime = new HtmlCalendar();

	private int selectedRowIndex = -1;

	private BatchErrorsBackingBean batchErrorsBean;
	private BatchStatistics batchStatistics;

	private FilePreferenceBackingBean filePreferenceBean;
	private BatchMetadata batchMetadata;
	private BatchMaintenanceDataModel batchMaintenanceDataModel;
	private ImportMapSaleAddBean importMapSaleAddBean;

	private BCPTransactionDetailBean bcpTransactionDetailBean;

	public BCPTransactionDetailBean getBcpTransactionDetailBean() {
		return bcpTransactionDetailBean;
	}

	public void setBcpTransactionDetailBean(
			BCPTransactionDetailBean bcpTransactionDetailBean) {
		this.bcpTransactionDetailBean = bcpTransactionDetailBean;
	}

	public String errorsAction() {
		batchErrorsBean.setBatchMaintenance(selectedBatch);
		batchErrorsBean.setReturnView("importmapsale_main");

		// 0001701: Import errors make the system crash
		batchErrorsBean.getBatchErrorDataModel().setBatchMaintenance(
				selectedBatch);

		return "batch_maintenance_errors";
	}

	public String navigateAction() {
		return "importmapsale_main";
	}

	public List<ImportMapBatch> getSelectedBatches() {
		return this.selectedBatches;
	}

	public String processAction() {
		List<ImportMapBatch> batches = new ArrayList<ImportMapBatch>();
		Iterator<BatchMaintenance> iter = batchMaintenanceDataModel.iterator();
		while (iter.hasNext()) {
			BatchMaintenance bm = iter.next();
			if (bm.getSelected()) {
				batches.add(new ImportMapBatch(bm));
			}
		}
		if (batches.size() == 0 && selectedBatch != null)
			batches.add(new ImportMapBatch(selectedBatch));

		// sort in order (oldest to newest)
		Collections.sort(batches);
		for (ImportMapBatch trub : batches) {
			if (!trub.isImported()) {
				trub.setError("Not an imported batch");
			} else if (trub.isHeld()) {
				trub.setError("On Hold");
			}
		}

		selectedBatches = batches;

		return "importmapsale_process";
	}

	public String viewAction() {
		this.bcpTransactionDetailBean.setBatchId(this.selectedBatch
				.getbatchId());

		return "importmap_transactions";
	}

	public String submitProcessAction() {
		// Including checked rows
		boolean validCheck = false;
		Iterator<BatchMaintenance> iter = batchMaintenanceDataModel.iterator();
		BatchMaintenance batchMaintenance = null;
		while (iter.hasNext()) {
			batchMaintenance = (BatchMaintenance) iter.next();
			if (batchMaintenance.getSelected()
					&& ("I".equalsIgnoreCase(batchMaintenance
							.getBatchStatusCode()))
					&& !batchMaintenance.isHeld()) {

				batchMaintenance.setBatchStatusCode("FP");

				// Control already done with the timezone conversion
				batchMaintenance.setSchedStartTimestamp((Date) batchStartTime
						.getValue());

				batchMaintenance.setUpdateTimestamp(new Date());
				batchMaintenance.setStatusUpdateTimestamp(new Date());
				batchMaintenance.setStatusUpdateUserId(Auditable
						.currentUserCode());
				batchMaintenanceDataModel.getBatchMaintenanceDAO().update(
						batchMaintenance);
				batchMaintenanceDataModel.refreshData(false);
				validCheck = true;
			}
		}

		// Including highlighted row if needed
		if (!validCheck && (selectedBatch != null)
				&& ("I".equalsIgnoreCase(selectedBatch.getBatchStatusCode()))
				&& !selectedBatch.isHeld()) {
			selectedBatch.setBatchStatusCode("FP");

			// Control already done with the timezone conversion
			selectedBatch.setSchedStartTimestamp((Date) batchStartTime
					.getValue());

			selectedBatch.setUpdateTimestamp(new Date());
			selectedBatch.setStatusUpdateTimestamp(new Date());
			selectedBatch.setStatusUpdateUserId(Auditable.currentUserCode());
			batchMaintenanceDataModel.getBatchMaintenanceDAO().update(
					selectedBatch);
			batchMaintenanceDataModel.refreshData(false);
		}

		selectedRowIndex = -1;
		return "importmapsale_main";
	}

	public String statisticsAction() {
		batchStatistics = new BatchStatistics(selectedBatch,
				batchMaintenanceDAO, filePreferenceBean.getBatchPreferenceDTO());
		batchStatistics.calcStatistics();
		return "importmapsale_statistics";
	}

	public void refreshStatistics() {
		batchStatistics.calcStatistics();
	}

	public void selectedRowChanged(ActionEvent e)
			throws AbortProcessingException {
		logger.info("ImportMapBean: enter selectedRowChanged");
		HtmlDataTable table = (HtmlDataTable) ((HtmlAjaxSupport) e
				.getComponent()).getParent();
		this.selectedBatch = (BatchMaintenance) table.getRowData();
		this.selectedRowIndex = table.getRowIndex();
	}

	// Bug 3210
	public String refreshAction() {
		init();
		batchMaintenanceDataModel.refreshData(false);
		selectedRowIndex = -1;
		return null;
	}

	// Bug 2250
	public String selectAllAction() {
		DetachedCriteria criteria = DetachedCriteria
				.forClass(BatchMaintenance.class);
		criteria.add(Expression.eq("batchTypeCode", "IPS"));
		criteria.add(Expression.eq("batchStatusCode", "I"));
		criteria.add(Expression.or(Expression.eq("heldFlag", "0"),
				Expression.isNull("heldFlag")));

		batchMaintenanceDataModel.setCriteria(criteria);
		batchMaintenanceDataModel.refreshData(false);
		selectedRowIndex = -1;
		return null;
	}

	public Boolean getDisplayError() {
		return (selectedRowIndex != -1)
				&& (selectedBatch.getErrorSevCode() != null)
				&& (selectedBatch.getErrorSevCode().trim().length() > 0);
	}

	public Boolean getDisplayProcess() {
		if ((selectedRowIndex != -1) && (selectedBatch != null)
				&& "I".equalsIgnoreCase(selectedBatch.getBatchStatusCode())
				&& !selectedBatch.isHeld())
			return true;

		return getHasSelection();
	}

	private boolean getHasSelection() {
		Iterator<BatchMaintenance> iter = batchMaintenanceDataModel.iterator();
		while (iter.hasNext()) {
			if (iter.next().getSelected()) {
				return true;
			}
		}
		return false;
	}

	public Boolean getDisplayView() {
		return selectedRowIndex != -1 && selectedBatch != null
				&& "I".equalsIgnoreCase(selectedBatch.getBatchStatusCode())
				&& !selectedBatch.isHeld();
	}

	private void init() {
		batchMaintenanceDataModel.setDefaultSortOrder("batchId",
				BatchMaintenanceDataModel.SORT_DESCENDING);
		batchMaintenanceDataModel.setPageSize(50);

		// Maintenance
		exampleBatchMaintenance = new BatchMaintenance();
		exampleBatchMaintenance.setEntryTimestamp(null);
		exampleBatchMaintenance.setBatchStatusCode(null);
		exampleBatchMaintenance.setBatchTypeCode("IPS");
		batchMaintenanceDataModel.setCriteria(exampleBatchMaintenance);
	}

	public BatchMaintenanceDataModel getBatchMaintenanceDataModel() {
		if (exampleBatchMaintenance == null) {
			init();
		}
		return this.batchMaintenanceDataModel;
	}

	public void sortAction(ActionEvent e) {
		batchMaintenanceDataModel.toggleSortOrder(e.getComponent().getParent()
				.getId());
	}

	public void setBatchMaintenanceDataModel(
			BatchMaintenanceDataModel batchMaintenanceDataModel) {
		this.batchMaintenanceDataModel = batchMaintenanceDataModel;
	}

	public BatchMetadata getBatchMetadata() {
		if (this.batchMetadata == null) {
			this.batchMetadata = batchMaintenanceDAO
					.findBatchMetadataById("IPS");
		}
		return this.batchMetadata;
	}

	public int getSelectedRowIndex() {
		return this.selectedRowIndex;
	}

	/**
	 * @param selectedRowIndex
	 *            the selectedRowIndex to set
	 */
	public void setSelectedRowIndex(int selectedRowIndex) {
		this.selectedRowIndex = selectedRowIndex;
	}

	/**
	 * @return the batchErrorsBean
	 */
	public BatchErrorsBackingBean getBatchErrorsBean() {
		return this.batchErrorsBean;
	}

	/**
	 * @param batchErrorsBean
	 *            the batchErrorsBean to set
	 */
	public void setBatchErrorsBean(BatchErrorsBackingBean batchErrorsBean) {
		this.batchErrorsBean = batchErrorsBean;
	}

	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	public FilePreferenceBackingBean getFilePreferenceBean() {
		return filePreferenceBean;
	}

	public void setFilePreferenceBean(
			FilePreferenceBackingBean filePreferenceBean) {
		this.filePreferenceBean = filePreferenceBean;
	}

	public void viewMapImportAction() {
	}

	public String addImportMapAction() {
		importMapSaleAddBean.clear(this);
		return "Add";
	}

	public BatchMaintenanceDAO getBatchMaintenanceDAO() {
		return this.batchMaintenanceDAO;
	}

	public void setBatchMaintenanceDAO(BatchMaintenanceDAO batchMaintenanceDAO) {
		this.batchMaintenanceDAO = batchMaintenanceDAO;
	}

	public ImportMapSaleAddBean getImportMapSaleAddBean() {
		return this.importMapSaleAddBean;
	}

	public void setImportMapSaleAddBean(ImportMapSaleAddBean importMapSaleAddBean) {
		this.importMapSaleAddBean = importMapSaleAddBean;
	}

	public HtmlCalendar getBatchStartTime() {
		batchStartTime.resetValue();
		batchStartTime.setValue(filePreferenceBean.getBatchPreferenceDTO()
				.getDefaultBatchStartTime());
		// batchStartTime.setValue(new
		// SimpleDateFormat("MM/dd/yyyy hh:mm a").format(filePreferenceBean.getBatchPreferenceDTO().getDefaultBatchStartTime()));
		return this.batchStartTime;
	}

	public void setBatchStartTime(HtmlCalendar batchStartTime) {
		this.batchStartTime = batchStartTime;
	}

	public BatchMaintenance getSelectedBatch() {
		return this.selectedBatch;
	}

	public BatchStatistics getBatchStatistics() {
		return this.batchStatistics;
	}


	public boolean isNavigatedFlag() {
		return true;
	}

	public void setNavigatedFlag(boolean navigatedFlag) {}

}