package com.ncsts.view.bean;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.html.HtmlSelectBooleanCheckbox;
import javax.faces.context.FacesContext;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.ActionEvent;
import javax.servlet.http.HttpSession;

import org.ajax4jsf.component.html.HtmlAjaxSupport;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.ncsts.domain.EntityItem;
import com.ncsts.domain.Jurisdiction;
import com.ncsts.domain.NexusDefinition;
import com.ncsts.domain.NexusDefinitionDetail;
import com.ncsts.domain.NexusDefinitionLog;
import com.ncsts.domain.Option;
import com.ncsts.domain.OptionCodePK;
import com.ncsts.domain.User;
import com.ncsts.dto.UserDTO;
import com.ncsts.services.EntityItemService;
import com.ncsts.services.NexusDefinitionDetailService;
import com.ncsts.services.NexusDefinitionLogService;
import com.ncsts.services.NexusDefinitionService;
import com.ncsts.services.OptionService;
import com.ncsts.services.UserService;

@Component
@Scope("session")
public class NexusDefinitionAutoBackingBean implements InitializingBean {
	@Autowired
	private EntityItemService entityItemService;

	@Autowired
	private OptionService optionService;

	@Autowired
	private NexusDefinitionLogService nexusDefinitionLogService;

	@Autowired
	private NexusDefinitionDetailService nexusDefinitionDetailService;
	
	@Autowired
	private NexusDefinitionService nexusDefinitionService;
	
	@Autowired
	private UserService userService;

	private String sourceCode;
	private Long sourceId;
	private Long entityId;
	private Jurisdiction jurisdiction;
	private EntityItem selectedEntity;
	private String okView;
	private String cancelView;

	private boolean hasStateNexus;
	private boolean hasCountyNexus;
	private boolean hasCityNexus;
	private boolean hasStj1Nexus;
	private boolean hasStj2Nexus;
	private boolean hasStj3Nexus;
	private boolean hasStj4Nexus;
	private boolean hasStj5Nexus;
	
	private boolean countryChecked;
	private boolean stateChecked;
	private boolean countyChecked;
	private boolean cityChecked;
	private boolean stj1Checked;
	private boolean stj2Checked;
	private boolean stj3Checked;
	private boolean stj4Checked;
	private boolean stj5Checked;
	
	private String reason;
	
	private UserDTO userDTO = null;
	
	private SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");

	public String getSourceCode() {
		return sourceCode;
	}

	public void setSourceCode(String sourceCode) {
		this.sourceCode = sourceCode;
	}

	@Override
	public void afterPropertiesSet() throws Exception {
	}

	public EntityItem getSelectedEntity() {
		if (selectedEntity == null && entityId != null) {
			selectedEntity = entityItemService.findById(entityId);
		}
		return selectedEntity;
	}

	public void setSelectedEntity(EntityItem selectedEntity) {
		this.selectedEntity = selectedEntity;
	}

	public Long getSourceId() {
		return sourceId;
	}

	public void setSourceId(Long sourceId) {
		this.sourceId = sourceId;
	}

	public Jurisdiction getJurisdiction() {
		return jurisdiction;
	}

	public void setJurisdiction(Jurisdiction jurisdiction) {
		this.jurisdiction = jurisdiction;
	}

	public String getAutoAddNexus() {
		Option option = optionService.findByPK(new OptionCodePK("AUTOADDNEXUS",
				"SYSTEM", "SYSTEM"));
		return option.getValue();
	}

	public String getAutoAddNexusNoRights() {
		Option option = optionService.findByPK(new OptionCodePK(
				"AUTOADDNEXUSNORIGHTS", "SYSTEM", "SYSTEM"));
		return option.getValue();
	}

	public String getDefaultNexusType() {
		Option option = optionService.findByPK(new OptionCodePK(
				"DEFAULTNEXUSTYPE", "SYSTEM", "SYSTEM"));
		return option.getValue();
	}

	public void checkLevels() {
		resetFlags();

		EntityItem entityItem = getSelectedEntity();
		if (entityItem != null) {
			List<NexusDefinitionDetail> list = nexusDefinitionDetailService
					.findAllNexusDefinitionDetails(entityItem.getEntityId(), jurisdiction.getCountry(),
							jurisdiction.getState(), "*STATE", "*STATE",
							"*STATE", true);
			if (list != null && list.size() > 0) {
				hasStateNexus = true;
			}
			else {
				countryChecked = true;
				stateChecked = true;
			}

			if (StringUtils.hasText(jurisdiction.getCounty())) {
				list = nexusDefinitionDetailService
						.findAllNexusDefinitionDetails(entityItem.getEntityId(), 
								jurisdiction.getCountry(),
								jurisdiction.getState(),
								jurisdiction.getCounty(), "*COUNTY", "*COUNTY",
								true);
				if (list != null && list.size() > 0) {
					hasCountyNexus = true;
				}
				else {
					countyChecked = true;
				}
			}

			if (StringUtils.hasText(jurisdiction.getCity())) {
				list = nexusDefinitionDetailService
						.findAllNexusDefinitionDetails(entityItem.getEntityId(), 
								jurisdiction.getCountry(),
								jurisdiction.getState(), "*CITY",
								jurisdiction.getCity(), "*CITY", true);
				if (list != null && list.size() > 0) {
					hasCityNexus = true;
				}
				else {
					cityChecked = true;
				}
			}

			if (StringUtils.hasText(jurisdiction.getStj1Name())) {
				list = nexusDefinitionDetailService
						.findAllNexusDefinitionDetails(entityItem.getEntityId(), 
								jurisdiction.getCountry(),
								jurisdiction.getState(), "*STJ", "*STJ",
								jurisdiction.getStj1Name(), true);
				if (list != null && list.size() > 0) {
					hasStj1Nexus = true;
				}
				else {
					stj1Checked = true;
				}
			}

			if (StringUtils.hasText(jurisdiction.getStj2Name())) {
				list = nexusDefinitionDetailService
						.findAllNexusDefinitionDetails(entityItem.getEntityId(), 
								jurisdiction.getCountry(),
								jurisdiction.getState(), "*STJ", "*STJ",
								jurisdiction.getStj2Name(), true);
				if (list != null && list.size() > 0) {
					hasStj2Nexus = true;
				}
				else {
					stj2Checked = true;
				}
			}

			if (StringUtils.hasText(jurisdiction.getStj3Name())) {
				list = nexusDefinitionDetailService
						.findAllNexusDefinitionDetails(entityItem.getEntityId(), 
								jurisdiction.getCountry(),
								jurisdiction.getState(), "*STJ", "*STJ",
								jurisdiction.getStj3Name(), true);
				if (list != null && list.size() > 0) {
					hasStj3Nexus = true;
				}
				else {
					stj3Checked = true;
				}
			}

			if (StringUtils.hasText(jurisdiction.getStj4Name())) {
				list = nexusDefinitionDetailService
						.findAllNexusDefinitionDetails(entityItem.getEntityId(), 
								jurisdiction.getCountry(),
								jurisdiction.getState(), "*STJ", "*STJ",
								jurisdiction.getStj4Name(), true);
				if (list != null && list.size() > 0) {
					hasStj4Nexus = true;
				}
				else {
					stj4Checked = true;
				}
			}

			if (StringUtils.hasText(jurisdiction.getStj5Name())) {
				list = nexusDefinitionDetailService
						.findAllNexusDefinitionDetails(entityItem.getEntityId(), 
								jurisdiction.getCountry(),
								jurisdiction.getState(), "*STJ", "*STJ",
								jurisdiction.getStj5Name(), true);
				if (list != null && list.size() > 0) {
					hasStj5Nexus = true;
				}
				else {
					stj5Checked = true;
				}
			}
		}
	}

	public String okAction() {
		if(!StringUtils.hasText(this.reason) && !this.hasStateNexus && !this.hasCountyNexus && !this.hasCityNexus &&
				!this.hasStj1Nexus && !this.hasStj2Nexus && !this.hasStj3Nexus &&
				!this.hasStj4Nexus && !this.hasStj5Nexus &&
				!this.stateChecked && !this.countyChecked && !this.cityChecked &&
				!this.stj1Checked && !this.stj2Checked && !this.stj3Checked &&
				!this.stj4Checked && !this.stj5Checked) {
			FacesMessage message = new FacesMessage("Reason for selecting none is required.");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
			return null;
		}
		else if(!this.stateChecked && !this.countyChecked && !this.cityChecked &&
				!this.stj1Checked && !this.stj2Checked && !this.stj3Checked &&
				!this.stj4Checked && !this.stj5Checked) {
			try {
				processAction("NEVER", getUser(), reason);
			}
			catch(Exception e) {
				FacesMessage message = new FacesMessage(e.getMessage());
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage(null, message);
				return null;
			}
		}
		else {
			try {
				processAction("ALWAYS", getUser(), StringUtils.hasText(reason) ? reason : "");
			}
			catch(Exception e) {
				FacesMessage message = new FacesMessage(e.getMessage());
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage(null, message);
				return null;
			}
		}
		return okView;
	}
	
	private void autoCreate(String country, String state, String county, String city, String stj, String user, String comments)
		throws Exception {
		NexusDefinition nd = null;
		NexusDefinitionDetail ndd = null;
		NexusDefinitionLog log = null;
		
		nd = new NexusDefinition();
		nd.setEntityId(this.entityId);
		nd.setNexusCountryCode(country);
		nd.setNexusStateCode(state);
		nd.setNexusCounty(county);
		nd.setNexusCity(city);
		nd.setNexusStj(stj);
		
		List<NexusDefinition> list = nexusDefinitionService.findByExample(nd, 1);
		if(list != null && list.size() > 0) {
			nd = list.get(0);
			if(!"1".equals(nd.getNexusFlag())) {
				try {
					nd.setNexusFlag("1");
					nd.setAutoUserId(user);
					nexusDefinitionService.update(nd);
				}
				catch(Exception e) {
					throw new Exception(
							"Error updating Nexus Definition: " + e.getMessage());
				}
			}
		}
		else {
			nd.setNexusFlag("1");
			nd.setAutoUserId(user);
			try {
				nexusDefinitionService.save(nd);
			}
			catch(Exception e) {
				throw new Exception(
						"Error saving Nexus Definition: " + e.getMessage());
			}
		}
		
		ndd = new NexusDefinitionDetail();
		ndd.setNexusDefId(nd.getNexusDefId());
		ndd.setEffectiveDate(new com.ncsts.view.util.DateConverter().getUserLocalDate());
		try {
			ndd.setExpirationDate(sdf.parse("12/31/9999"));
		} catch (ParseException e) {}
		ndd.setNexusTypeCode(getDefNexusType());
		ndd.setActiveFlag("1");
		ndd.setAutoUserId(user);
		
		try {
			nexusDefinitionDetailService.save(ndd);
		}
		catch(Exception e) {
			throw new Exception(
					"Error saving Nexus Definition Detail: " + e.getMessage());
		}
		
		log = new NexusDefinitionLog();
		log.setSourceCode(this.sourceCode);
		log.setSourceId(this.sourceId);
		log.setEntityId(entityId);
		log.setNexusDefDetailId(ndd.getNexusDefDetailId());
		log.setNexusCountryCode(country);
		log.setNexusStateCode(state);
		log.setNexusCounty(county);
		log.setNexusCity(city);
		log.setNexusStj(stj);
		log.setComments(comments);
		log.setAutoUserId(user);

		try {
			nexusDefinitionLogService.save(log);
		} catch (Exception e) {
			throw new Exception(
					"Error saving Nexus Definition Log: " + e.getMessage());
		}
	}
	
	public String getDefNexusType() {
		Option option = optionService.findByPK(new OptionCodePK ("DEFAULTNEXUSTYPE", "SYSTEM", "SYSTEM"));
		return option.getValue();
	}

	public String cancelAction() {
		return cancelView;
	}

	public boolean isHasStateNexus() {
		return hasStateNexus;
	}

	public boolean isHasCountyNexus() {
		return hasCountyNexus;
	}

	public boolean isHasCityNexus() {
		return hasCityNexus;
	}

	public boolean isHasStj1Nexus() {
		return hasStj1Nexus;
	}

	public boolean isHasStj2Nexus() {
		return hasStj2Nexus;
	}

	public boolean isHasStj3Nexus() {
		return hasStj3Nexus;
	}

	public boolean isHasStj4Nexus() {
		return hasStj4Nexus;
	}

	public boolean isHasStj5Nexus() {
		return hasStj5Nexus;
	}

	public void resetFlags() {
		this.hasStateNexus = false;
		this.hasCountyNexus = false;
		this.hasCityNexus = false;
		this.hasStj1Nexus = false;
		this.hasStj2Nexus = false;
		this.hasStj3Nexus = false;
		this.hasStj4Nexus = false;
		this.hasStj5Nexus = false;
		
		this.countryChecked = false;
		this.stateChecked = false;
		this.countyChecked = false;
		this.cityChecked = false;
		this.stj1Checked = false;
		this.stj2Checked = false;
		this.stj3Checked = false;
		this.stj4Checked = false;
		this.stj5Checked = false;
		
		this.reason = null;
	}

	public boolean isCountryChecked() {
		return countryChecked;
	}

	public void setCountryChecked(boolean countryChecked) {
		this.countryChecked = countryChecked;
	}

	public boolean isStateChecked() {
		return stateChecked;
	}

	public void setStateChecked(boolean stateChecked) {
		this.stateChecked = stateChecked;
	}

	public boolean isCountyChecked() {
		return countyChecked;
	}

	public void setCountyChecked(boolean countyChecked) {
		this.countyChecked = countyChecked;
	}

	public boolean isCityChecked() {
		return cityChecked;
	}

	public void setCityChecked(boolean cityChecked) {
		this.cityChecked = cityChecked;
	}

	public boolean isStj1Checked() {
		return stj1Checked;
	}

	public void setStj1Checked(boolean stj1Checked) {
		this.stj1Checked = stj1Checked;
	}

	public boolean isStj2Checked() {
		return stj2Checked;
	}

	public void setStj2Checked(boolean stj2Checked) {
		this.stj2Checked = stj2Checked;
	}

	public boolean isStj3Checked() {
		return stj3Checked;
	}

	public void setStj3Checked(boolean stj3Checked) {
		this.stj3Checked = stj3Checked;
	}

	public boolean isStj4Checked() {
		return stj4Checked;
	}

	public void setStj4Checked(boolean stj4Checked) {
		this.stj4Checked = stj4Checked;
	}

	public boolean isStj5Checked() {
		return stj5Checked;
	}

	public void setStj5Checked(boolean stj5Checked) {
		this.stj5Checked = stj5Checked;
	}

	public String getOkView() {
		return okView;
	}

	public void setOkView(String okView) {
		this.okView = okView;
	}

	public String getCancelView() {
		return cancelView;
	}

	public void setCancelView(String cancelView) {
		this.cancelView = cancelView;
	}
	
	public String process(String sourceCode, Long sourceId, Long entityId, Jurisdiction jurisdiction) throws Exception {
		this.sourceCode = sourceCode;
		this.sourceId = sourceId;
		this.entityId = entityId;
		this.jurisdiction = jurisdiction;

		checkLevels();
		
		boolean hasAllLevels = this.hasStateNexus && this.hasCountyNexus && this.hasCityNexus;
		if(hasAllLevels && StringUtils.hasText(this.jurisdiction.getStj1Name())) {
			hasAllLevels = hasAllLevels && this.hasStj1Nexus;
		}
		if(hasAllLevels && StringUtils.hasText(this.jurisdiction.getStj2Name())) {
			hasAllLevels = hasAllLevels && this.hasStj2Nexus;
		}
		if(hasAllLevels && StringUtils.hasText(this.jurisdiction.getStj3Name())) {
			hasAllLevels = hasAllLevels && this.hasStj3Nexus;
		}
		if(hasAllLevels && StringUtils.hasText(this.jurisdiction.getStj4Name())) {
			hasAllLevels = hasAllLevels && this.hasStj4Nexus;
		}
		if(hasAllLevels && StringUtils.hasText(this.jurisdiction.getStj4Name())) {
			hasAllLevels = hasAllLevels && this.hasStj4Nexus;
		}
	
		if(hasAllLevels) {
			return "";
		}
		
		String autoAddNexus = getAutoAddNexus();
		
		if("NEVER".equalsIgnoreCase(autoAddNexus) || "ALWAYS".equalsIgnoreCase(autoAddNexus)) {
			processAction(autoAddNexus, "ADMIN", null);
			return "";
		}
		else if("PROMPT".equalsIgnoreCase(autoAddNexus)) {
			String userAddNexusFlag = getUserAddNexusFlag();
			if(!"1".equals(userAddNexusFlag)) {
				processAction(getAutoAddNexusNoRights(), getUser(), null);
				return "";
			}
			else {
				return "nexus_def_auto";
			}
		}
		else {
			return "";
		}
	}
	
	public void processAction(String autoAddNexus, String user, String comment) throws Exception {		
		NexusDefinitionLog log = null;
		if ("NEVER".equalsIgnoreCase(autoAddNexus)) {
			if(comment == null) {
				comment = "Never Create";
			}
			
			log = new NexusDefinitionLog();
			log.setSourceCode(this.sourceCode);
			log.setSourceId(this.sourceId);
			log.setEntityId(entityId);
			log.setNexusDefDetailId(0L);
			log.setNexusCountryCode(jurisdiction.getCountry());
			log.setNexusStateCode(jurisdiction.getState());
			log.setNexusCounty(jurisdiction.getCounty());
			log.setNexusCity(jurisdiction.getCity());
			if (StringUtils.hasText(jurisdiction.getStj1Name())) {
				log.setNexusStj(jurisdiction.getStj1Name());
			}
			else if (StringUtils.hasText(jurisdiction.getStj2Name())) {
				log.setNexusStj(jurisdiction.getStj2Name());
			}
			else if (StringUtils.hasText(jurisdiction.getStj3Name())) {
				log.setNexusStj(jurisdiction.getStj3Name());
			}
			else if (StringUtils.hasText(jurisdiction.getStj4Name())) {
				log.setNexusStj(jurisdiction.getStj4Name());
			}
			else if (StringUtils.hasText(jurisdiction.getStj5Name())) {
				log.setNexusStj(jurisdiction.getStj5Name());
			}
			log.setComments(comment);
			log.setAutoUserId(user);

			try {
				nexusDefinitionLogService.save(log);
			} catch (Exception e) {
				throw new Exception(
						"Error saving Nexus Definition Log: " + e.getMessage());
			}
		}
		else if ("ALWAYS".equalsIgnoreCase(autoAddNexus)) {
			if(comment == null) {
				comment = "Always Create";
			}
			
			if(stateChecked) {
				autoCreate(this.jurisdiction.getCountry(), this.jurisdiction.getState(), "*STATE", "*STATE", "*STATE", user, comment);
			}
			
			if(countyChecked) {
				autoCreate(this.jurisdiction.getCountry(), this.jurisdiction.getState(), this.jurisdiction.getCounty(), "*COUNTY", "*COUNTY", user, comment);
			}
			
			if(cityChecked) {
				autoCreate(this.jurisdiction.getCountry(), this.jurisdiction.getState(), "*CITY", this.jurisdiction.getCity(), "*CITY", user, comment);
			}
			
			if(stj1Checked) {
				autoCreate(this.jurisdiction.getCountry(), this.jurisdiction.getState(), "*STJ", "*STJ", this.jurisdiction.getStj1Name(), user, comment);
			}
			
			if(stj2Checked) {
				autoCreate(this.jurisdiction.getCountry(), this.jurisdiction.getState(), "*STJ", "*STJ", this.jurisdiction.getStj2Name(), user, comment);
			}
			
			if(stj3Checked) {
				autoCreate(this.jurisdiction.getCountry(), this.jurisdiction.getState(), "*STJ", "*STJ", this.jurisdiction.getStj3Name(), user, comment);
			}
			
			if(stj4Checked) {
				autoCreate(this.jurisdiction.getCountry(), this.jurisdiction.getState(), "*STJ", "*STJ", this.jurisdiction.getStj4Name(), user, comment);
			}
			
			if(stj5Checked) {
				autoCreate(this.jurisdiction.getCountry(), this.jurisdiction.getState(), "*STJ", "*STJ", this.jurisdiction.getStj5Name(), user, comment);
			}
		}
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}
	
	public String getUserAddNexusFlag() {
		User user = userService.findById(getUserDTO().getUserCode());
		return user == null ? null : user.getAddNexusFlag();
	}
	
	public String getUser() {
		return getUserDTO().getUserCode();
	}
	
	public UserDTO getUserDTO() {
		if(userDTO == null) {
			HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
			userDTO = (UserDTO) session.getAttribute("user");
		}
		
		return userDTO;
	}
	
	public void countryOrStateCheckboxChecked(ActionEvent e) throws AbortProcessingException {
		HtmlSelectBooleanCheckbox chk = (HtmlSelectBooleanCheckbox) ((HtmlAjaxSupport) e.getComponent()).getParent();
		if((Boolean) chk.getValue()) {
			countryChecked = true;
			stateChecked = true;
		}
		else {
			countryChecked = false;
			stateChecked = false;
		}
	}
}
