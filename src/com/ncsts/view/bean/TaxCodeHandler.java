package com.ncsts.view.bean;

import java.util.ArrayList;
import java.util.List;

import javax.faces.component.UIInput;
import javax.faces.component.UISelectItems;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.ajax4jsf.component.html.HtmlAjaxSupport;
import org.apache.log4j.Logger;
import org.richfaces.component.html.HtmlDataTable;

import com.ncsts.common.CacheManager;
import com.ncsts.common.LoggerFactory;
import com.ncsts.domain.TaxCode;
import com.ncsts.domain.TaxCodeDetail;
import com.ncsts.domain.TransactionDetail;
import com.ncsts.dto.TaxCodeDetailDTO;
import com.ncsts.dto.TaxCodeStateDTO;
import com.ncsts.services.TaxCodeDetailService;
import com.ncsts.view.bean.ViewTaxCodeBean.TaxCodeInfoCallback;

public class TaxCodeHandler {

	static private Logger logger = LoggerFactory.getInstance().getLogger(TaxCodeHandler.class);
	
	private boolean linkedMenus = true;
	
	private CacheManager cacheManager;	
	
//	private HtmlSelectOneMenu taxcodeStateMenu;// = new HtmlSelectOneMenu();
//	private HtmlSelectOneMenu taxcodeTypeMenu;// = new HtmlSelectOneMenu();
//	private HtmlSelectOneMenu taxcodeMenu;// = new HtmlSelectOneMenu();
	
	private HtmlSelectOneMenu taxcodeCountryMenu = new HtmlSelectOneMenu();
	private HtmlSelectOneMenu taxcodeStateMenu = new HtmlSelectOneMenu();
	private HtmlSelectOneMenu taxcodeTypeMenu = new HtmlSelectOneMenu();
	private HtmlSelectOneMenu taxcodeMenu = new HtmlSelectOneMenu();
	private HtmlInputText taxcodeDetailId = new HtmlInputText();
	private HtmlInputText taxcodeDesc = new HtmlInputText();
	private List<SelectItem> taxCodeItems; 
	
	private String taxcodeCountry;
	private String taxcodeState;
	private String taxcodeType;
	private String taxcodeCode;
	private String description;
	
	private Long taxcodeId;
	private Integer size;
	
	private String cchGroup;
	private String cchTaxCat;
	
	private String id;
	
	private Long searchTaxCodeResultId;
	
	private TaxCodeDetailService taxCodeDetailService;
	private TaxCodeSearchHandler popupHandler ;
	
	
	private List<TaxCodeDetailDTO> taxCodeDTOList;
	
	private TaxCodeInfoCallback taxCodeInfoCallback;
	private String callingPage = "";

	public void setCacheManager(CacheManager cacheManager) {
		this.cacheManager = cacheManager;
	}
	
	public List<SelectItem> getTaxCodeStateItems() {
		return cacheManager.createStateItems(taxcodeCountry);
    }
	
	public List<SelectItem> getCountryItems() {
		return cacheManager.createCountryItems();
    }
	
	public TaxCodeHandler(TaxCodeInfoCallback taxCodeInfoCallback, String callingPage) {
		this.taxCodeInfoCallback = taxCodeInfoCallback;
		this.callingPage = callingPage;
	}
	
	public void setCallingPage(String callingPage){
		this.callingPage = callingPage;
	}
	
	public String getCallingPage(){
		return callingPage;
	}
	
	public List<TaxCodeDetailDTO> getTaxCodeDTOList() {
		return taxCodeDTOList;
	}
	
	public int getSize() {
		size = taxCodeDTOList.size();
		return size;
	}
	
	public TaxCodeHandler() {
	}

	public String viewTaxCodeAction() {
		taxCodeInfoCallback.searchTaxCodeInfoCallback(getMenuValue(taxcodeCountryMenu), getMenuValue(taxcodeStateMenu), 
					getMenuValue(taxcodeTypeMenu), getMenuValue(taxcodeMenu), callingPage);
		return "view_taxcode";
	}
	
	public boolean getIsCallingPageExist(){
		return (taxCodeInfoCallback!=null && callingPage!=null && callingPage.length()>0);
	}
	
	
	public TaxCodeHandler(String id) {
		this.id = id;
	}
	
	public TaxCodeHandler(String id, boolean linkedMenus) {
		this.id = id;
		this.linkedMenus = linkedMenus;
	}
	
	public boolean getLinkedMenus() {
		return linkedMenus;
	}

	public void setLinkedMenus(boolean linkedMenus) {
		this.linkedMenus = linkedMenus;
	}

	public TaxCodeDetailService getTaxCodeDetailService() {
		return taxCodeDetailService;
	}
	
	public void setTaxCodeDetailService(TaxCodeDetailService taxCodeDetailService) {
		this.taxCodeDetailService = taxCodeDetailService;
		rebuildMenus();
	}
	
	public String getTaxcodeCountry() {
		return taxcodeCountry;
	}

	public void setTaxcodeCountry(String taxcodeCountry) {
		this.taxcodeCountry = taxcodeCountry;
		setMenuValue(taxcodeCountryMenu, this.taxcodeCountry);
	}
	
	public String getTaxcodeState() {
		return taxcodeState;
	}

	public void setTaxcodeState(String taxcodeState) {
		this.taxcodeState = taxcodeState;
		setMenuValue(taxcodeStateMenu, this.taxcodeState);
	}

	public String getTaxcodeType() {
		return taxcodeType;
	}

	public void setTaxcodeType(String taxcodeType) {
		this.taxcodeType = taxcodeType;
		setMenuValue(taxcodeTypeMenu, this.taxcodeType);
	}

	public String getTaxcodeCode() {
		return taxcodeCode;
	}

	public void setTaxcodeCode(String taxcodeCode) {
		this.taxcodeCode = taxcodeCode;
		setMenuValue(taxcodeMenu, this.taxcodeCode);
	}
	
	public Long getTaxcodeId() {
		return taxcodeId;
	}

	public void setTaxcodeId(Long taxcodeId) {
		this.taxcodeId = taxcodeId;
		setMenuValue(taxcodeDetailId, this.taxcodeId);
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
		setMenuValue(taxcodeDesc, this.description);
	}
	
	public void setTaxcodeDetail(TaxCodeDetail tcDetail) {
		setTaxcodeCountry((tcDetail == null)? null:tcDetail.getTaxcodeCountryCode());
		setTaxcodeState((tcDetail == null)? null:tcDetail.getTaxcodeStateCode());
		setTaxcodeType((tcDetail == null)? null:tcDetail.getTaxcodeTypeCode());
		setTaxcodeCode((tcDetail == null)? null:tcDetail.getTaxcodeCode());

		// Rebuild menu items
		rebuildMenus();
	}

	public String getCchGroup() {
		return cchGroup;
	}

	public void setCchGroup(String cchGroup) {
		this.cchGroup = cchGroup;
	}

	public String getCchTaxCat() {
		return cchTaxCat;
	}

	public void setCchTaxCat(String cchTaxCat) {
		this.cchTaxCat = cchTaxCat;
	}

	public void setTransaction(TransactionDetail trDetail) {
//AC	setTaxcodeCountry((trDetail == null)? null:trDetail.getTaxcodeCountryCode());
//AC	setTaxcodeState((trDetail == null)? null:trDetail.getTaxcodeStateCode());
//AC	setTaxcodeType((trDetail == null)? null:trDetail.getTaxcodeTypeCode());
		setTaxcodeCode((trDetail == null)? null:trDetail.getTaxcodeCode());
		
		//CCH		setCchGroup((trDetail == null)? null:trDetail.getCchGroupCode());
		//CCH		setCchTaxCat((trDetail == null)? null:trDetail.getCchTaxcatCode());

		// Rebuild menus
		rebuildMenus();
	}
	
	public HtmlSelectOneMenu getTaxcodeCountryMenu() {
		return taxcodeCountryMenu;
	}
	
	public void setTaxcodeCountryMenu(HtmlSelectOneMenu taxcodeCountryMenu) {
		this.taxcodeCountryMenu = taxcodeCountryMenu;
	}
	
	public HtmlSelectOneMenu getTaxcodeStateMenu() {
		//return null; //taxcodeStateMenu;
		return taxcodeStateMenu;
	}
	
	public void setTaxcodeStateMenu(HtmlSelectOneMenu taxcodeStateMenu) {
		this.taxcodeStateMenu = taxcodeStateMenu;
	}
	
	public HtmlSelectOneMenu getTaxcodeTypeMenu() {
		//return null; //taxcodeTypeMenu;
		return taxcodeTypeMenu;
	}
	
	public void setTaxcodeTypeMenu(HtmlSelectOneMenu taxcodeTypeMenu) {
		this.taxcodeTypeMenu = taxcodeTypeMenu;
	}
	
	public HtmlSelectOneMenu getTaxcodeMenu() {
		//return null; //taxcodeMenu;
		return taxcodeMenu;
	}
	
	public void setTaxcodeMenu(HtmlSelectOneMenu taxcodeMenu) {
		this.taxcodeMenu = taxcodeMenu;
	}
	
	public HtmlInputText getTaxcodeDetailId() {
		return taxcodeDetailId;
	}

	public HtmlInputText getTaxcodeDesc() {
		return taxcodeDesc;
	}

	public void setTaxcodeDetailId(HtmlInputText taxcodeDetailId) {
		this.taxcodeDetailId = taxcodeDetailId;
	}

	public void setTaxcodeDesc(HtmlInputText taxcodeDesc) {
		this.taxcodeDesc = taxcodeDesc;
	}
	
	public void reset() {
		setTaxcodeCountry(null);
		setTaxcodeState(null);
		setTaxcodeType(null);
		setTaxcodeCode(null);
		
		setCchGroup(null);
		setCchTaxCat(null);
		
		setTaxcodeDetailId(null);
		setTaxcodeDesc(null);
		
		taxcodeCountry = null;
		taxcodeState = null;
		taxcodeType = null;
		taxcodeCode = null;
		
		taxcodeId = null;
		description = null;
		
		taxCodeDTOList = null;
		searchTaxCodeResultId = null;

		// Rebuild menu items
		rebuildMenus();
	}
	
	private String getMenuValue(HtmlSelectOneMenu menu) {
		String result = (menu == null)? null:(String) menu.getSubmittedValue();
		return ((result == null) && (menu != null))? (String) menu.getValue():result;
	}
	
	private void setMenuValue(HtmlSelectOneMenu menu, String value) {
		if (menu != null) {
			menu.setLocalValueSet(false);
			menu.setSubmittedValue(null);
			menu.setValue(value);
		}
	}
	
	private String getMenuValue(UIInput menu) {
		String result = (String) menu.getSubmittedValue();
		return (result == null)? (String) menu.getValue():result;
	}
	
	private void setMenuValue(UIInput menu, Object value) {
		menu.setLocalValueSet(false);
		menu.setSubmittedValue(null);
		menu.setValue(value);
	}
	
	public void popupSearchAction(ActionEvent e) {
		TaxCodeSearchHandler popupHandler = getPopupHandler();
		
		popupHandler.reset();
		popupHandler.setTaxcodeCountry(getMenuValue(taxcodeCountryMenu));	
		popupHandler.setTaxcodeState(getMenuValue(taxcodeStateMenu));	
		popupHandler.setTaxcodeType(getMenuValue(taxcodeTypeMenu));
		popupHandler.setTaxcodeCode(getMenuValue(taxcodeMenu));
		popupHandler.initializeSearch();
	}
	
	public void taxCodeSearchAction(ActionEvent e) {
		if(getMenuValue(taxcodeDesc) != null && !getMenuValue(taxcodeDesc).equals(""))
			description = getMenuValue(taxcodeDesc);
		if(getMenuValue(taxcodeDetailId) != null && !getMenuValue(taxcodeDetailId).equals(""))
			taxcodeId = Long.valueOf(getMenuValue(taxcodeDetailId));
		initializeSearch();
	}
	
	public void taxCodeClearAction(ActionEvent e) {
		reset();
	}
	
	protected void initializeSearch() {
		taxCodeDTOList = performSearch();
	}
	
	private List<TaxCodeDetailDTO> performSearch() {
		taxcodeState = getMenuValue(taxcodeStateMenu);
		taxcodeType = getMenuValue(taxcodeTypeMenu);
		taxcodeCode = getMenuValue(taxcodeMenu);
		logger.debug("taxCodeDetailService   : " + taxCodeDetailService + "-" + taxcodeState + "-" + taxcodeType + "-" + taxcodeCode+ "-" + description+ "-"+ taxcodeId);
		return taxCodeDetailService.getTaxcodeDetails(taxcodeState, taxcodeType, taxcodeCode, description, taxcodeId);
	}
	
	public void setTaxCode(TaxCodeDetail taxCode) {
		setTaxcodeState((taxCode == null)? null:taxCode.getTaxcodeStateCode());
		setTaxcodeType((taxCode == null)? null:taxCode.getTaxcodeTypeCode());
		setTaxcodeCode((taxCode == null)? null:taxCode.getTaxcodeCode());
		setTaxcodeId((taxCode == null)? null:taxCode.getTaxcodeDetailId());
	}
	
	public void updateTaxCodeListener(ActionEvent e) {
		if (popupHandler != null) {
			
			searchTaxCodeResultId = popupHandler.getSearchTaxCodeResultId();
			// TODO we could optimize by getting object from list in popup handler
			setTaxCode(taxCodeDetailService.findById(searchTaxCodeResultId));
			
			rebuildCodeMenu(this.taxcodeCountry, this.taxcodeState, this.taxcodeType, this.taxcodeCode);
			
			popupHandler.reset();
		}
	}
	
	public TaxCodeSearchHandler getPopupHandler() {
		if (popupHandler == null) {
			popupHandler = new TaxCodeSearchHandler();
			popupHandler.setTaxCodeDetailService(taxCodeDetailService);
			popupHandler.setCacheManager(cacheManager);
		}
		return popupHandler;
	}
	
	public boolean getValidSearchTaxCodeResult() {
		return (searchTaxCodeResultId != null);
	}

	public void selectTaxCode(ActionEvent e) { 
	    HtmlDataTable table = (HtmlDataTable) ((HtmlAjaxSupport) e.getComponent()).getParent(); 
	    searchTaxCodeResultId = ((TaxCodeDetailDTO) table.getRowData()).getTaxcodeDetailId();
		logger.debug("Selected Jurisdiction ID: " + ((searchTaxCodeResultId == null)? "NULL":searchTaxCodeResultId));		
	}

	public Long getSearchTaxCodeResultId() {
		return searchTaxCodeResultId;
	}

	public void setSearchTaxCodeResultId(Long searchTaxCodeResultId) {
		this.searchTaxCodeResultId = searchTaxCodeResultId;
	}

	public TaxCodeDetail findTaxcodeDetail() {
		TaxCodeDetail result = null;
		List<TaxCodeDetail> detailList = 
			taxCodeDetailService.findTaxCodeDetailList(taxcodeCountry, taxcodeState, taxcodeType, taxcodeCode);
		if ((detailList != null) && (detailList.size() > 0)) {
			if (detailList.size() == 1) {
				logger.debug("Found one matching TaxCodeDetail: " + 
						String.format("%s - %s - %s", taxcodeState, taxcodeType, taxcodeCode));
				result = detailList.get(0);
			} else {
				logger.warn("Found multiple matching TaxCodeDetail: " +
						String.format("%s - %s - %s", taxcodeState, taxcodeType, taxcodeCode));
				result = detailList.get(0);
			}
		} else {
			logger.debug("Found no matching TaxCodeDetail: " + 
					String.format("%s - %s - %s", taxcodeState, taxcodeType, taxcodeCode));
		}
		return result;
	}
	
	public void rebuildMenus() {
		rebuildCodeMenu(this.taxcodeCountry, this.taxcodeState, this.taxcodeType, this.taxcodeCode);
	}
	
	public List<SelectItem> getTaxCodeItems() {
		return taxCodeItems;
	}
	
	public void taxcodeCountrySelected(ActionEvent e) {
		// Since the controls are not contained within their own
		// form, we need to use immediate=true which means
		// we need to get values from the submittedValue area!!
		taxcodeCountry = getMenuValue(taxcodeCountryMenu);
		taxcodeState = getMenuValue(taxcodeStateMenu);
		taxcodeType = getMenuValue(taxcodeTypeMenu);
		taxcodeCode = getMenuValue(taxcodeMenu);
		setMenuValue(taxcodeCountryMenu, taxcodeCountry);
		
		//Reset state, type, code
		//setTaxcodeCountry(null);
		setTaxcodeState(null);
		setTaxcodeType(null);
		setTaxcodeCode(null);
		//taxcodeCountry = null;
		taxcodeState = null;
		taxcodeType = null;
		taxcodeCode = null;
		
		rebuildCodeMenu(taxcodeCountry, taxcodeState, taxcodeType, taxcodeCode);
	}
	
	public void taxcodeStateSelected(ActionEvent e) {
		// Since the controls are not contained within their own
		// form, we need to use immediate=true which means
		// we need to get values from the submittedValue area!!
		taxcodeCountry = getMenuValue(taxcodeCountryMenu);
		taxcodeState = getMenuValue(taxcodeStateMenu);
		taxcodeType = getMenuValue(taxcodeTypeMenu);
		taxcodeCode = getMenuValue(taxcodeMenu);
		setMenuValue(taxcodeStateMenu, taxcodeState);
		
		//Reset type, code
		//setTaxcodeCountry(null);
		//setTaxcodeState(null);
		setTaxcodeType(null);
		setTaxcodeCode(null);
		//taxcodeCountry = null;
		//taxcodeState = null;
		taxcodeType = null;
		taxcodeCode = null;

		rebuildCodeMenu(taxcodeCountry, taxcodeState, taxcodeType, taxcodeCode);
	}
	
	public void taxcodeTypeSelected(ActionEvent e) {
		// Since the controls are not contained within their own
		// form, we need to use immediate=true which means
		// we need to get values from the submittedValue area!!
		taxcodeCountry = getMenuValue(taxcodeCountryMenu);
		taxcodeState = getMenuValue(taxcodeStateMenu);
		taxcodeType = getMenuValue(taxcodeTypeMenu);
		taxcodeCode = getMenuValue(taxcodeMenu);
		
		//Reset code
		//setTaxcodeCountry(null);
		//setTaxcodeState(null);
		//setTaxcodeType(null);
		setTaxcodeCode(null);
		//taxcodeCountry = null;
		//taxcodeState = null;
		//taxcodeType = null;
		taxcodeCode = null;
		
		rebuildCodeMenu(taxcodeCountry, taxcodeState, taxcodeType, taxcodeCode);
	}
	
	public void taxcodeCodeSelected(ActionEvent e) {
		// Since the controls are not contained within their own
		// form, we need to use immediate=true which means
		// we need to get values from the submittedValue area!!
		taxcodeCountry = getMenuValue(taxcodeCountryMenu);
		taxcodeState = getMenuValue(taxcodeStateMenu);
		taxcodeType = getMenuValue(taxcodeTypeMenu);
		taxcodeCode = getMenuValue(taxcodeMenu);
		
		//rebuildCodeMenu(taxcodeState, taxcodeType, taxcodeCode);
	}
	
	//taxcodeCodeSelected

	private void rebuildCodeMenu(String countryCode, String stateCode, String typeCode, String taxCode) {
		boolean taxCodeExists = false;
		logger.debug("Update tax code from state: " + stateCode + " type: " + typeCode);
		taxCodeItems = new ArrayList<SelectItem>();
		taxCodeItems.add(new SelectItem("","Tax Code"));
		List<TaxCode> codes = (linkedMenus && ((countryCode == null) || countryCode.equals("") || (stateCode == null) || stateCode.equals("") || (typeCode == null) || typeCode.equals("")))? 
				null:taxCodeDetailService.findTaxCodeListByCountry(countryCode, stateCode, typeCode, null);
		if (codes != null) {
			for (TaxCode code : codes) {
				String value = code.getTaxCodePK().getTaxcodeCode() + " - " + code.getDescription();
				if (value.length() > 80) {
					value = value.substring(0, 80) + "...";
				}
				taxCodeItems.add(new SelectItem(code.getTaxCodePK().getTaxcodeCode(), value));
				// Check if previously selected code still exists
				if ((taxCode != null) && code.getTaxCodePK().getTaxcodeCode().equals(taxCode)) {
					taxCodeExists = true;
				}
			}
		}
	
		/*
		UISelectItems items = new UISelectItems();
		items.setValue(itemList);
		items.setId(id + "_code_items");
		taxcodeMenu.getChildren().clear();
		taxcodeMenu.getChildren().add(items);
		 */
		if (!taxCodeExists) {
			logger.debug("Selected code not found, resetting selection");
			setTaxcodeCode(null);
		}
	}
	
}
