package com.ncsts.view.bean;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.ForeignKey;
import org.springframework.beans.BeanUtils;

import com.ncsts.dto.EntityItemDTO;


public class JurisdictionItem {
	
	public static String ROOT_TYPE = "Root";
	public static String COUNTRY_TYPE = "Country";
	public static String STATE_TYPE = "State";
	public static String COUNTY_TYPE = "County";
	public static String CITY_TYPE = "City";
	public static String STJ_TYPE = "Stj";
	
    private Long entityId;
    private Long parentEntityId;   
       
    private String country; 
    private String state;
    private String county; 
    private String city; 
    private String stj;
    private String citationRef;
    private String countyLocated; 
    
    private String itemType = "";
    
    private String type = "";
    private Boolean selectedFlag = new Boolean(false);
    private Boolean disabledFlag = new Boolean(false);
    private Boolean isAllFlag = new Boolean(false); 
    private Boolean needExpandFlag = new Boolean(false); 
    
    private Boolean newItemFlag = new Boolean(false);
    
    private Boolean indepentdentFlag = new Boolean(false);
    
    public boolean isItemType(String itemType){
    	if(this.itemType.equalsIgnoreCase(itemType)){
    		return true;
    	}
    	else{
    		return false;
    	}
    }
    
    private String description;
    
    public String getDescription(){
    	return this.description;
    }
    
    public void setDescription(String description){
    	this.description = description;
    }
    
    public Boolean getNotCountryItemType(){
    	return !COUNTRY_TYPE.equals(this.itemType);
    }

    public Boolean getIsAllowCheck(){
    	boolean allowed = (STATE_TYPE.equals(this.itemType) || (COUNTY_TYPE.equals(this.itemType)) || (CITY_TYPE.equals(this.itemType) && !isAllFlag) || (STJ_TYPE.equals(this.itemType) && !isAllFlag));	
    	return allowed;
    }
    
    public Boolean getIsAllowSelectAll(){
    	boolean allowed = ((STATE_TYPE.equals(this.itemType) && isAllFlag) || (COUNTY_TYPE.equals(this.itemType) && isAllFlag) || (CITY_TYPE.equals(this.itemType) && isAllFlag) || (STJ_TYPE.equals(this.itemType) && isAllFlag));	
    	return allowed;
    }
    
    public Boolean getIsCheckDisabled(){
    	boolean allowed = ((COUNTY_TYPE.equals(this.itemType)) && !indepentdentFlag);	
    	return allowed;
    }
    
    public String getKey(){
    	//Country:  *US****
    	//State: 	*US*AL***
    	//County: 	*US*AL*BARBOUR**
    	//City:		*US*AL**AUTAUGAVILLE*
    	//STJ:		*US*AL***CLIO POLICE JURY
    	
    	String key = country + "*";
    	
    	if(COUNTRY_TYPE.equals(this.itemType)){
    		key = key + "****";
    	}
    	else if(STATE_TYPE.equals(this.itemType)){
    		key = key + state + "****";
    	}
    	else if(COUNTY_TYPE.equals(this.itemType)){
    		key = key + state + "*" + description + "***";
    	}
    	else if(CITY_TYPE.equals(this.itemType)){
    		key = key + state + "*"+ countyLocated + "*" + description + "**"; //Add allocated country
    	}
    	else if(STJ_TYPE.equals(this.itemType)){
    		key = key + state + "**"+ countyLocated + "*" + description + "*";
    	}
    	else{
    		key = "*****";
    	}
    	
    	return key;
    }
    
    public String getCombinationKey(){
    	//Country:  *US****
    	//State: 	*US*AL***
    	//County: 	*US*AL*BARBOUR**
    	//City:		*US*AL**AUTAUGAVILLE*
    	//STJ:		*US*AL***CLIO POLICE JURY
    	
    	String key = country + "*";
    	
    	if(COUNTRY_TYPE.equals(this.itemType)){
    		key = key + "****";
    	}
    	else if(STATE_TYPE.equals(this.itemType)){
    		key = key + state + "****";
    	}
    	else if(COUNTY_TYPE.equals(this.itemType)){
    		key = key + state + "*" + description + "***";
    	}
    	else if(CITY_TYPE.equals(this.itemType)){
    		key = key + state + "**" + description + "**"; 
    	}
    	else if(STJ_TYPE.equals(this.itemType)){
    		key = key + state + "***" + description + "*";
    	}
    	else{
    		key = "*****";
    	}
    	
    	return key;
    }
    
    public String getItemType(){
    	return this.itemType;
    }
    
    public void setItemType(String itemType){
    	this.itemType = itemType;
    }
    
    public JurisdictionItem() { 
    }
    
    public JurisdictionItem(Long entityId ) { 
    	this.entityId = entityId;
    }
    
    public String getType() {
        return this.type;
    }
    
    public void setType(String type) {
    	this.type = type;
    } 
	
    public Long getEntityId() {
        return this.entityId;
    }
    
    public void setEntityId(Long entityId) {
    	this.entityId = entityId;
    }
      
    public Long getParentEntityId() {
        return this.parentEntityId;
    }
    
    public void setParentEntityId(Long parentEntityId) {
    	this.parentEntityId = parentEntityId;
    }  

    public String getCity() {
        return this.city;
    }
    
    public void setCity(String city) {
    	this.city = city;
    } 
    
    public String getStj() {
        return this.stj;
    }
    
    public void setStj(String stj) {
    	this.stj = stj;
    } 
    
    public String getCounty() {
        return this.county;
    }
    
    public void setCounty(String county) {
    	this.county = county;
    }
    
    public String getCountyLocated() {
        return this.countyLocated;
    }
    
    public void setCountyLocated(String countyLocated) {
    	this.countyLocated = countyLocated;
    }
    
    public String getState() {
        return this.state;
    }
    
    public void setState(String state) {
    	this.state = state;
    }  

    
    public String getCountry() {
        return this.country;
    }
    
    public void setCountry(String country) {
    	this.country = country;
    }  
    
    public Boolean getNeedExpandFlag() {
        return this.needExpandFlag;
    }
    
    public void setNeedExpandFlag(Boolean needExpandFlag) {
    	this.needExpandFlag = needExpandFlag;
    } 
    
    public Boolean getIsAllFlag() {
        return this.isAllFlag;
    }
    
    public void setIsAllFlag(Boolean isAllFlag) {
    	this.isAllFlag = isAllFlag;
    } 
    
    public Boolean getSelectedFlag() {
        return this.selectedFlag;
    }
    
    public void setSelectedFlag(Boolean selectedFlag) {
    	this.selectedFlag = selectedFlag;
    }
    
    public Boolean getNewItemFlag() {
        return this.newItemFlag;
    }
    
    public void setNewItemFlag(Boolean newItemFlag) {
    	this.newItemFlag = newItemFlag;
    }
    
    public Boolean getIndepentdentFlag() {
        return this.indepentdentFlag;
    }
    
    public void setIndepentdentFlag(Boolean indepentdentFlag) {
    	this.indepentdentFlag = indepentdentFlag;
    }
    
    public Boolean getDisabledFlag() {
        return this.disabledFlag;
    }
    
    public void setDisabledFlag(Boolean disabledFlag) {
    	this.disabledFlag = disabledFlag;
    }
          
    public String getCitationRef() {
		return citationRef;
	}

    public void setCitationRef(String citationRef) {
		this.citationRef = citationRef;
	}

	/**
	 * 
	 */
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }

        if ((other == null) || !(other instanceof JurisdictionItem)) {
            return false;
        }

        final JurisdictionItem revision = (JurisdictionItem)other;

        Long c1 = getEntityId();
        Long c2 = revision.getEntityId();
        if (!((c1 == c2) || (c1 != null && c1.equals(c2)))) {
            return false;
        }

        return true;
    }
}
