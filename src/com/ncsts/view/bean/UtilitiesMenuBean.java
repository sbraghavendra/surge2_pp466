package com.ncsts.view.bean;

/**
 * @author AChen
 * 
 */

import java.util.Map;

import javax.faces.event.ValueChangeEvent;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.ncsts.common.LoggerFactory;
import com.ncsts.domain.User;
import com.ncsts.services.UtilitiesMenuService;

public class UtilitiesMenuBean {
	
	private Logger logger = LoggerFactory.getInstance().getLogger(UtilitiesMenuBean.class);
	private UtilitiesMenuService utilitiesMenuService;
	private loginBean loginBean;
	
	@Autowired
	private BatchProcessBackingBean batchProcessBackingBean;

	private boolean isStart = false;
	private String returnMessage = "";
	private boolean transactionDetailCheck = false;
	private boolean taxMatrixCheck = false;
  	private String menuAction = "";
  	private boolean returnMode;
  	private EditAction currentAction = EditAction.VIEW;
  	private Map<String, String> busPropertyMap = null;
  	
  	private boolean isBuildspecificIndexType = true;
  	
	private String buildindexType = null;
	
	private String buildSpecIndexType = null;
	
  	public String getBuildindexType() {
		return buildindexType;
	}

	public void setBuildindexType(String buildindexType) {
		this.buildindexType = buildindexType;
	}

	public boolean getIsBuildspecificIndexType() {
		return isBuildspecificIndexType;
	}

	public void setBuildspecificIndexType(boolean isBuildspecificIndexType) {
		this.isBuildspecificIndexType = isBuildspecificIndexType;
	}
	
	public String getBuildSpecIndexType() {
		return buildSpecIndexType;
	}

	public void setBuildSpecIndexType(String buildSpecIndexType) {
		this.buildSpecIndexType = buildSpecIndexType;
	}

	public void updateBuildSpecificIndexes(ValueChangeEvent event) {
		buildindexType = (String)event.getNewValue();
		if(RadioBuildIndexType.BUILDALLINDEXES.getText().equals(buildindexType) ) {
			isBuildspecificIndexType = true; 
		}
		else {
			isBuildspecificIndexType = false;
		}
	}

	public enum RadioBuildIndexType {
		BUILDALLINDEXES("buildallindexes"),
		BUILDSPECIFICINDEXES("buildspecificindexes"),
		BUILDMATRIXDRIVERINDEXESALL("buildmatrixdriverindexall"),
		BUILDMATRIXDRIVERINDEXESLOCONLY("buildmatrixdriverindexesloconly"),
		BUILDMATRIXDRIVERINDEXESGANDS("buildmatrixdriverindexesgands"),
		BUILDTRANSACTIONDETAILINDEXALL("buildtransactiondetailsindexall"),
		BUILDTRANSACTIONDETAILINDEXDRIVERSONLY("buildtransactiondetailsindexdriversonly");
		
		private String buildindexType;
		private RadioBuildIndexType(String buildindexType) {
			this.buildindexType= buildindexType;
		}
		public String getText() {
			return buildindexType;
		}
	}

  	public boolean getTransactionDetailCheck() {
		return transactionDetailCheck;
	}

  	public void setTransactionDetailCheck(boolean transactionDetailCheck) {
		this.transactionDetailCheck = transactionDetailCheck;
	}

  	public boolean getTaxMatrixCheck() {
		return taxMatrixCheck;
	}

  	public void setTaxMatrixCheck(boolean taxMatrixCheck) {
		this.taxMatrixCheck = taxMatrixCheck;
	}

  	public String getActionText() {
		return currentAction.getActionText();
	}
 
	public UtilitiesMenuService getUtilitiesMenuService() {
		return utilitiesMenuService;
	}
	
	public void setUtilitiesMenuService(UtilitiesMenuService utilitiesMenuService) {
		this.utilitiesMenuService = utilitiesMenuService;
	}
	
	public loginBean getLoginBean() {
		return loginBean;
	}

	public void setLoginBean(loginBean loginBean) {
		this.loginBean = loginBean;
	}
	
	public String getReturnMessage(){
		return returnMessage;
	}
	
	public boolean getIsStart(){
		return isStart;
	}
	
	private String modifyMessage(String message){
		String modifyMessage = message;
		if (modifyMessage!=null && modifyMessage.length()>0){
			modifyMessage = "Error: " + modifyMessage;
		} else {
			modifyMessage = "";
		}
		return modifyMessage;
	}
	
	public boolean getIsBusRunning() {
		if (menuAction.equals("fixBrokenJob")) {
			String broken = busPropertyMap.get("BROKEN");
			if(broken!=null && broken.equalsIgnoreCase("N")){
				return true;
			}	
			else{
				return false;
			}
		} else {
			return false;
		}
	}
	
	public boolean getIsBusSuspended() {
		if (menuAction.equals("fixBrokenJob")) {
			String broken = busPropertyMap.get("BROKEN");
			if(broken!=null && broken.equalsIgnoreCase("Y")){
				return true;
			}	
			else{
				return false;
			}
		} else {
			return false;
		}
	}
	
	public boolean getIsAddBus() {
		if (menuAction.equals("fixBrokenJob")) {
			String broken = busPropertyMap.get("BROKEN");
			if(broken!=null && broken.length()>0){
				return false;
			}	
			else{
				return true;
			}
		} else {
			return false;
		}
	}
	
	public String createBusAction(){
		returnMessage = utilitiesMenuService.createBusService();
		return refreshBusAction();
	}
	
	public String refreshBusAction(){
		busPropertyMap = utilitiesMenuService.findBusProperty();
		return "fix_job";
	}
	
	public String resumeBusAction(){
		returnMessage = utilitiesMenuService.resumeBusService(Long.valueOf(busPropertyMap.get("JOB")), false);
		return refreshBusAction();
	}
	
	public String suspendBusAction(){
		returnMessage = utilitiesMenuService.resumeBusService(Long.valueOf(busPropertyMap.get("JOB")), true);
		return refreshBusAction();
	}
	
	public String getBusStatus() {
		String broken = busPropertyMap.get("BROKEN");
		if(broken!=null && broken.equalsIgnoreCase("Y")){
			return "Suspended";
		}
		else if(broken!=null && broken.equalsIgnoreCase("N")){
			return "Running";
		}	
		else{
			return "";
		}
	}

	public String runUtilityAction() {
		isStart = true;
		if (menuAction.equals("runMasterControlProgram")) {
			logger.info("start runMasterControlProgramAction");
			returnMessage = utilitiesMenuService.runMasterControlProgram();
			returnMessage = modifyMessage(returnMessage);
			logger.info("end runMasterControlProgramAction");
		/*} else if (menuAction.equals("fixBrokenJob")) {
			logger.info("start fixBrokenJob");
			returnMessage = utilitiesMenuService.fixBrokenJob(Long.valueOf(busPropertyMap.get("JOB")));
			returnMessage = modifyMessage(returnMessage);
			
			busPropertyMap = utilitiesMenuService.findBusProperty();
			String broken = busPropertyMap.get("BROKEN");
			if(!(broken!=null && broken.equalsIgnoreCase("N"))){
				isStart = false;
			}		
			
			logger.info("end fixBrokenJob");*/
		} else if (menuAction.equals("resetDataWindowSyntax")) {
			logger.info("start resetDatawindowSyntaxAction"); 
			String userCode = loginBean.getUsername().toUpperCase();
			returnMessage = utilitiesMenuService.resetDatawindowSyntax(userCode); 
			returnMessage = modifyMessage(returnMessage);
			logger.info("end resetDatawindowSyntaxAction");
		} else if (menuAction.equals("buildIndices")) {
			logger.info("start buildIndicesAction"); 
			String buildIndex = "";
			if(RadioBuildIndexType.BUILDALLINDEXES.getText().equals(getBuildindexType())) {
				buildIndex = "9A";
			}
			else if(RadioBuildIndexType.BUILDSPECIFICINDEXES.getText().equals(getBuildindexType())) {
				if(RadioBuildIndexType.BUILDMATRIXDRIVERINDEXESALL.getText().equals(getBuildSpecIndexType())) {
					buildIndex = "9";
				}
				if(RadioBuildIndexType.BUILDMATRIXDRIVERINDEXESLOCONLY.getText().equals(getBuildSpecIndexType())) {
					buildIndex = "3";
				}
				if(RadioBuildIndexType.BUILDMATRIXDRIVERINDEXESGANDS.getText().equals(getBuildSpecIndexType())) {
					buildIndex = "2";
				}
				if(RadioBuildIndexType.BUILDTRANSACTIONDETAILINDEXALL.getText().equals(getBuildSpecIndexType())) {
					buildIndex = "1A";
				}
				if(RadioBuildIndexType.BUILDTRANSACTIONDETAILINDEXDRIVERSONLY.getText().equals(getBuildSpecIndexType())) {
					buildIndex = "1";
				}
			}
			logger.info("buildIndex: " + buildIndex);
			if(buildIndex.length() > 0){
				returnMessage = utilitiesMenuService.buildIndices(buildIndex); 
				returnMessage = modifyMessage(returnMessage);
			} else {
				isStart = false;
				returnMessage = "Select a Build Indices.";
			}
			logger.info("end buildIndicesAction");
		} else if (menuAction.equals("reweightMatrices")) {
			logger.info("start reweightMatricesAction"); 
			returnMessage = utilitiesMenuService.reweightMatrices(); 
			returnMessage = modifyMessage(returnMessage);
			logger.info("end reweightMatricesAction");		
		} else if (menuAction.equals("resetAllSequences")) {
			logger.info("start resetAllSequencesAction"); 
			returnMessage = utilitiesMenuService.resetAllSequences(); 
			returnMessage = modifyMessage(returnMessage);
			logger.info("end resetAllSequencesAction");
		} else if (menuAction.equals("rebuildDriverReferences")) {
			logger.info("start rebuildDriverReferencesAction"); 
			returnMessage = utilitiesMenuService.rebuildDriverReferences(); 
			returnMessage = modifyMessage(returnMessage);
			logger.info("end rebuildDriverReferencesAction");
		} else if (menuAction.equals("truncateBCPTables")) {
			logger.info("start truncateBCPTablesAction"); 
			returnMessage = utilitiesMenuService.truncateBCPTables(); 
			returnMessage = modifyMessage(returnMessage);
			logger.info("end truncateBCPTablesAction");
		} else if(menuAction.equals("runBatchProcess")) {
			logger.info("start runBatchProcess"); 
			returnMessage = batchProcessBackingBean.runBatchProcess(); 
			logger.info("end runBatchProcess");
		} else {
			isStart = false;
		}
		return ""; 
	
	}
	
	public String cancelUtilityAction(){
		return "success";
	}
	
	public Map<String, String> getBusPropertyMap(){
		return busPropertyMap;
	}
	
	public String fixBrokenJob() {
		isStart = false;
		returnMessage = "";
		menuAction = "fixBrokenJob";	
		busPropertyMap = utilitiesMenuService.findBusProperty();
		String broken = busPropertyMap.get("BROKEN");
		if(broken!=null && broken.equalsIgnoreCase("N")){
			isStart = true;
		}
		return "fix_job";
	}
	
	public String runMasterControlProgram() {
		isStart = false;
		returnMessage = "";
		menuAction = "runMasterControlProgram";
		return "run_mcp";
	}
	
	public String resetDataWindowSyntax() {
		isStart = false;
		returnMessage = "";
		menuAction = "resetDataWindowSyntax";
		return "reset_data_window";
	}
	
	public String buildIndices() {
		isStart = false;
		returnMessage = "";
		menuAction = "buildIndices";
		isBuildspecificIndexType = true; 
		buildindexType = RadioBuildIndexType.BUILDALLINDEXES.getText();
		buildSpecIndexType = null;
		return "build_indices";
	}
	
	public String runBatchProcess() {
		isStart = false;
		returnMessage = "";
		menuAction = "runBatchProcess";
		return "run_batch_process";
	}
	
	public String reweightMatrices() {
		isStart = false;
		returnMessage = "";
		menuAction = "reweightMatrices";
		return "reweight_matrices";
	}
	
	public String resetAllSequences() {
		isStart = false;
		returnMessage = "";
		menuAction = "resetAllSequences";
		return "reset_sequences";
	}
	
	public String rebuildDriverReferences() {
		isStart = false;
		returnMessage = "";
		menuAction = "rebuildDriverReferences";
		return "rebuild_driver_refs";
	}
	
	public String truncateBCPTables() {
		isStart = false;
		returnMessage = "";
		menuAction = "truncateBCPTables";
		return "truncate_bcp";
	}

	public boolean getIsRunMasterControlProgram() {
		if (menuAction.equals("runMasterControlProgram")) {
			return true;
		} else {
			return false;
		}
	}
	
	public boolean getIsFixBrokenJob() {
		if (menuAction.equals("fixBrokenJob")) {
			return true;
		} else {
			return false;
		}
	}

	public boolean getIsResetDataWindowSyntax() {
		if (menuAction.equals("resetDataWindowSyntax")) {
			return true;
		} else {
			return false;
		}
	}

	public boolean getIsBuildIndices() {
		if (menuAction.equals("buildIndices")) {
			return true;
		} else {
			return false;
		}
	}

	public boolean getIsReweightMatrices() {
		if (menuAction.equals("reweightMatrices")) {
			return true;
		} else {
			return false;
		}
	}

	public boolean getIsResetAllSequences() {
		if (menuAction.equals("resetAllSequences")) {
			return true;
		} else {
			return false;
		}
	}

	public boolean getIsRebuildDriverReferences() {
		if (menuAction.equals("rebuildDriverReferences")) {
			return true;
		} else {
			return false;
		}
	}

	public boolean getIsTruncateBCPTables() {
		if (menuAction.equals("truncateBCPTables")) {
			return true;
		} else {
			return false;
		}
	}
	
	public boolean getIsRunBatchProcess() {
		if (menuAction.equals("runBatchProcess")) {
			return true;
		} else {
			return false;
		}
	}
	
	public User getCurrentUser() {
		return loginBean.getUser();
   }
}