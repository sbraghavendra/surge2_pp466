package com.ncsts.view.bean;

import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.GregorianCalendar;
import java.util.Hashtable;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIInput;
import javax.faces.component.html.HtmlSelectBooleanCheckbox;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;
import com.ncsts.services.JurisdictionService;

import org.ajax4jsf.component.html.HtmlAjaxSupport;
import org.apache.log4j.Logger;
import org.richfaces.component.html.HtmlDataTable;
import org.richfaces.model.selection.SimpleSelection;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;

import com.ncsts.common.CacheManager;
import com.ncsts.domain.CustEntity;
import com.ncsts.domain.CustEntityPK;
import com.ncsts.domain.EntityItem;
import com.ncsts.domain.EntityLevel;
import com.ncsts.domain.Jurisdiction;
import com.ncsts.domain.Cust;
import com.ncsts.domain.CustLocn;

import com.ncsts.domain.ListCodes;
import com.ncsts.dto.EntityItemDTO;
import com.ncsts.jsf.model.CustDataModel;
import com.ncsts.jsf.model.CustLocnDataModel;
import com.ncsts.services.CustService;
import com.ncsts.services.CustLocnService;
import com.ncsts.services.DriverReferenceService;
import com.ncsts.services.EntityItemService;
import com.ncsts.services.EntityLevelService;
import com.ncsts.services.ListCodesService;
import com.ncsts.services.OptionService;
import javax.faces.event.ValueChangeEvent;
import javax.persistence.Transient;

public class CustomerLocationSearchBean extends CustomerLocationBean {	
	private Logger logger = Logger.getLogger(CustomerLocationSearchBean.class);

	
}
