package com.ncsts.view.bean;

import java.lang.reflect.Method;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.ajax4jsf.component.html.HtmlAjaxSupport;
import org.apache.log4j.Logger;
import org.richfaces.component.html.HtmlCalendar;
import org.richfaces.component.html.HtmlDataTable;
import org.richfaces.model.selection.SimpleSelection;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.ncsts.common.CacheManager;
import com.ncsts.common.Util;
import com.ncsts.domain.DataDefinitionColumn;
import com.ncsts.domain.ListCodes;
import com.ncsts.domain.Procrule;
import com.ncsts.domain.ProcruleDetail;
import com.ncsts.domain.Proctable;
import com.ncsts.domain.TransactionDetail;
import com.ncsts.domain.User;
import com.ncsts.jsf.model.ProcruleDataModel;
import com.ncsts.services.ProcruleService;
import com.ncsts.services.ProctableService;
import com.ncsts.view.util.TogglePanelController;

public class ProcruleBean {	
	private Logger logger = Logger.getLogger(ProcruleBean.class);
	
	@Autowired
	private loginBean loginBean;
	
	private MatrixCommonBean matrixCommonBean;

	private Procrule filterProcrule = new Procrule();
	private Procrule selProcrule = new Procrule();
	private Procrule updateProcrule = new Procrule();
	
	private CacheManager cacheManager;
	private ProcruleService procruleService;
	private ProcruleDataModel procruleDataModel;
	private ProctableService proctableService;
	private String reorderRuletypeCode = "";

	private SimpleSelection selection = new SimpleSelection();
	private int selectedProcruleRowIndex = -1;
	private int selectedProcruleDetailRowIndex = -1;
	private EditAction currentAction = EditAction.VIEW;
	private TogglePanelController togglePanelController = null;
	private Boolean isShowSqlLike = false;
	
	private List<SelectItem> filterDistinctDateItems;
	private List<SelectItem> filterActiveRulesMenuItems;
	private List<SelectItem> filterRuleTypeMenuItems;
	private List<SelectItem> filterRuleTypeMenuSalesItems;
	private List<SelectItem> ruleTypeMenuItems;
	private List<SelectItem> reprocessMenuItems;
	private List<SelectItem> ruleTypeMenuSalesItems;
	private Map<String, String> reprocessDescriptionMap = null;
	private Map<String, String> reprocessExplanationMap = null;
	private List<WhenObject> whenObjectItems= new ArrayList<WhenObject>();
	private List<SetObject> setObjectItems= new ArrayList<SetObject>();
	private List<SelectObject> inSelectObject= new ArrayList<SelectObject>();
	private List<SelectObject> outSelectObject= new ArrayList<SelectObject>();
	private String radioSetSelected = "";
	private List<SelectItem> operatorItems;
	private List<SelectItem> operationGroupItems;
	private List<SelectItem> operationSetGroupItems;
	private List<SelectItem> valuelocationWhenItems;
	private List<SelectItem> valuelocationSetItems;
	private List<SelectItem> operatorGroupItems;
	private List<SelectItem> talbeSelectionItems;
	private List<SelectItem> valueOperatorItems;
	private List<SelectItem> textOperatorItems;	
	private List<SelectItem> relationItems;
	
	private List<SelectItem> operationGroupSalesItems;
	private List<SelectItem> operationSetGroupSalesItems;
	private List<SelectItem> valuelocationWhenSalesItems;
	private List<SelectItem> valuelocationSetSalesItems;
	private List<SelectItem> operatorGroupSalesItems;
	
	//For TB_BILLTRANS
	private List<SelectItem> allSaleTransItems;
	private List<SelectItem> allSaleTransNumberItems;
	private List<SelectItem> allSaleTransTextItems;
	private List<SelectItem> allSaleTransDateItems;

	private List<SelectItem> basicSaleTransItems;
	private List<SelectItem> basicSaleTransNumberItems;
	private List<SelectItem> basicSaleTransTextItems;
	private List<SelectItem> basicSaleTransDateItems;
	
	private List<SelectItem> transactionSaleTransItems;
	private List<SelectItem> transactionSaleTransNumberItems;
	private List<SelectItem> transactionSaleTransTextItems;
	private List<SelectItem> transactionSaleTransDateItems;
	
	private List<SelectItem> situsSaleTransItems;
	private List<SelectItem> situsSaleTransNumberItems;
	private List<SelectItem> situsSaleTransTextItems;
	private List<SelectItem> situsSaleTransDateItems;
	
	private List<SelectItem> amountsSaleTransItems;
	private List<SelectItem> amountsSaleTransNumberItems;
	private List<SelectItem> amountsSaleTransTextItems;
	private List<SelectItem> amountsSaleTransDateItems;
	
	private List<SelectItem> generalledgerSaleTransItems;
	private List<SelectItem> generalledgerSaleTransNumberItems;
	private List<SelectItem> generalledgerSaleTransTextItems;
	private List<SelectItem> generalledgerSaleTransDateItems;


	private List<SelectItem> customerSaleTransItems;
	private List<SelectItem> customerSaleTransNumberItems;
	private List<SelectItem> customerSaleTransTextItems;
	private List<SelectItem> customerSaleTransDateItems;
	
	private List<SelectItem> invoiceSaleTransItems;
	private List<SelectItem> invoiceSaleTransNumberItems;
	private List<SelectItem> invoiceSaleTransTextItems;
	private List<SelectItem> invoiceSaleTransDateItems;

	private List<SelectItem> locationSaleTransItems;
	private List<SelectItem> locationSaleTransNumberItems;
	private List<SelectItem> locationSaleTransTextItems;
	private List<SelectItem> locationSaleTransDateItems;
	
	private List<SelectItem> entitySaleTransItems;
	private List<SelectItem> entitySaleTransNumberItems;
	private List<SelectItem> entitySaleTransTextItems;
	private List<SelectItem> entitySaleTransDateItems;
	
	private List<SelectItem> userSaleTransItems;
	private List<SelectItem> userSaleTransNumberItems;
	private List<SelectItem> userSaleTransTextItems;
	private List<SelectItem> userSaleTransDateItems;
	
	//For TB_PURCHTRANS
	private List<SelectItem> allTransactionItems;
	private List<SelectItem> allTransactionNumberItems;
	private List<SelectItem> allTransactionTextItems;
	private List<SelectItem> allTransactionDateItems;

	private List<SelectItem> otherTransactionItems;
	private List<SelectItem> otherTransactionNumberItems;
	private List<SelectItem> otherTransactionTextItems;
	private List<SelectItem> otherTransactionDateItems;

	private List<SelectItem> transactionTransactionItems;
	private List<SelectItem> transactionTransactionNumberItems;
	private List<SelectItem> transactionTransactionTextItems;
	private List<SelectItem> transactionTransactionDateItems;
	
	private List<SelectItem> invoiceTransactionItems;
	private List<SelectItem> invoiceTransactionNumberItems;
	private List<SelectItem> invoiceTransactionTextItems;
	private List<SelectItem> invoiceTransactionDateItems;
	
	private List<SelectItem> generalLedgerTransactionItems;
	private List<SelectItem> generalLedgerTransactionNumberItems;
	private List<SelectItem> generalLedgerTransactionTextItems;
	private List<SelectItem> generalLedgerTransactionDateItems;

	private List<SelectItem> locationTransactionItems;
	private List<SelectItem> locationTransactionNumberItems;
	private List<SelectItem> locationTransactionTextItems;
	private List<SelectItem> locationTransactionDateItems;
	
	private List<SelectItem> vendorTransactionItems;
	private List<SelectItem> vendorTransactionNumberItems;
	private List<SelectItem> vendorTransactionTextItems;
	private List<SelectItem> vendorTransactionDateItems;
	
	private List<SelectItem> userTransactionItems;
	private List<SelectItem> userTransactionNumberItems;
	private List<SelectItem> userTransactionTextItems;
	private List<SelectItem> userTransactionDateItems;
	
	private List<SelectItem> projectTransactionItems;
	private List<SelectItem> projectTransactionNumberItems;
	private List<SelectItem> projectTransactionTextItems;
	private List<SelectItem> projectTransactionDateItems;
	
	private List<SelectItem> inventoryTransactionItems;
	private List<SelectItem> inventoryTransactionNumberItems;
	private List<SelectItem> inventoryTransactionTextItems;
	private List<SelectItem> inventoryTransactionDateItems;
	
	private List<SelectItem> purchaseOrderTransactionItems;
	private List<SelectItem> purchaseOrderTransactionNumberItems;
	private List<SelectItem> purchaseOrderTransactionTextItems;
	private List<SelectItem> purchaseOrderTransactionDateItems;
	
	private List<SelectItem> workOrderTransactionItems;
	private List<SelectItem> workOrderTransactionNumberItems;
	private List<SelectItem> workOrderTransactionTextItems;
	private List<SelectItem> workOrderTransactionDateItems;
	
	private List<SelectItem> paymentInfoTransactionItems;
	private List<SelectItem> paymentInfoTransactionNumberItems;
	private List<SelectItem> paymentInfoTransactionTextItems;
	private List<SelectItem> paymentInfoTransactionDateItems;
	
	private List<SelectItem> auditTransactionItems;
	private List<SelectItem> auditTransactionNumberItems;
	private List<SelectItem> auditTransactionTextItems;
	private List<SelectItem> auditTransactionDateItems;
	
	private List<Procrule> reorderProcruleArray= new ArrayList<Procrule>();
	private List<ProcruleDetail> selectedProcruleDetails = null;
	private List<ProcruleDetail> updatedProcruleDetails = null;
	
	private HtmlCalendar effectiveDateCalendar;
	private Date effectiveDate;
	private String selectedEffectiveDate = "";
	
	private boolean detailActiveBooleanFlag; 
	
	public boolean isDetailActiveBooleanFlag() {
		return detailActiveBooleanFlag;
	}

	public void setDetailActiveBooleanFlag(boolean detailActiveBooleanFlag) {
		this.detailActiveBooleanFlag = detailActiveBooleanFlag;
	}

	public String getSelectedEffectiveDate() {
		return selectedEffectiveDate;
	}

	public void setSelectedEffectiveDate(String selectedEffectiveDate) {
		this.selectedEffectiveDate = selectedEffectiveDate;
	}
	
	public Date getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}
	
	public HtmlCalendar getEffectiveDateCalendar() {
		if (effectiveDateCalendar == null) {
			effectiveDateCalendar = new HtmlCalendar();
		}
		return effectiveDateCalendar;
	}

	public void setEffectiveDateCalendar(HtmlCalendar effectiveDateCalendar) {
		this.effectiveDateCalendar = effectiveDateCalendar;
	}
	
	public  TogglePanelController getTogglePanelController(){
		if(togglePanelController==null){
			togglePanelController = new TogglePanelController();
			togglePanelController.addTogglePanel("ProcrulePanel", true);
		}
		return togglePanelController;
	}
	
	public void toggleShowSqlLike(ActionEvent event){
		isShowSqlLike = !isShowSqlLike;
	}
	
	public Boolean getIsShowSqlLike () {
		return isShowSqlLike ;
	}
	
	public void setTogglePanelController(TogglePanelController togglePanelController){
		this.togglePanelController = togglePanelController;;
	}
	
	public Procrule getSelProcrule(){
		return selProcrule;
	}
	
	public Procrule getUpdateProcrule(){
		return updateProcrule;
	}
	
	public class RuleTypeListComparator implements Comparator<ListCodes> {
		public int compare(ListCodes o1, ListCodes o2) {
			String l1 = "";
			if(o1.getMessage()!=null){
				l1 = new String((String)o1.getSeverityLevel());
			}
			
			String l2 = "";
			if(o2.getMessage()!=null){
				l2 = new String((String)o2.getMessage());
			}

			return l1.compareTo(l2);
		}
	}
	
	public List<SelectItem> getFilterRuleTypeMenuItems() {
		if(getIsPurchasingMenu()){
			if(filterRuleTypeMenuItems==null){
				List<SelectItem> selectItems = new ArrayList<SelectItem>();
		    	selectItems.add(new SelectItem("","All Rule Types"));	
		    	
		    	List<ListCodes> listCodes = cacheManager.getListCodesByType("RULETYPE");
				Collections.sort(listCodes, new RuleTypeListComparator());
				
				for (ListCodes code : listCodes) {
					if(code.getCodeCode()!=null && (code.getCodeCode().equalsIgnoreCase("PRE") || code.getCodeCode().equalsIgnoreCase("POST"))){
						selectItems.add(new SelectItem(code.getCodeCode(), code.getDescription()));
					}
				}	    	
		    	filterRuleTypeMenuItems = selectItems;    	
		    	filterProcrule.setRuletypeCode("");
			}

			return filterRuleTypeMenuItems;
		}
		else{
			if(filterRuleTypeMenuSalesItems==null){
				List<SelectItem> selectItems = new ArrayList<SelectItem>();
		    	selectItems.add(new SelectItem("","All Rule Types"));	
		    	
		    	List<ListCodes> listCodes = cacheManager.getListCodesByType("RULETYPE");
				Collections.sort(listCodes, new RuleTypeListComparator());
				
				for (ListCodes code : listCodes) {
					if(code.getCodeCode()!=null && (code.getCodeCode().equalsIgnoreCase("PRE") || code.getCodeCode().equalsIgnoreCase("POST") ||
							code.getCodeCode().equalsIgnoreCase("PRESITUS") || code.getCodeCode().equalsIgnoreCase("PRETAXCODE") )){
						selectItems.add(new SelectItem(code.getCodeCode(), code.getDescription()));
					}
				}

				filterRuleTypeMenuSalesItems = selectItems;		    	
		    	filterProcrule.setRuletypeCode("");
			}

			return filterRuleTypeMenuSalesItems;
		}				
	}
	
	public List<SelectItem> getFilterDistinctDateItems() {
		if(filterDistinctDateItems==null){
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
	    	selectItems.add(new SelectItem("",""));
	
	    	filterDistinctDateItems = selectItems;
		}
 
		return filterDistinctDateItems;
	}
	
	public void setFilterDistinctDateItems(List<SelectItem> filterDistinctDateItems) {
		this.filterDistinctDateItems = filterDistinctDateItems;
	}
	
	public List<SelectItem> getFilterActiveRulesMenuItems() {
		if(filterActiveRulesMenuItems==null){
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
	    	selectItems.add(new SelectItem("","All Rules"));
	    	selectItems.add(new SelectItem("1","Active Only"));
	    	selectItems.add(new SelectItem("0","Inactive Only"));

	    	filterActiveRulesMenuItems = selectItems;
	    	
			filterProcrule.setActiveFlag("");
		}
 
		return filterActiveRulesMenuItems;
	}
	
	public List<SelectItem> getRuleTypeMenuItems() {
		if(getIsPurchasingMenu()){
			if(ruleTypeMenuItems==null){
				List<SelectItem> selectItems = new ArrayList<SelectItem>();
				List<ListCodes> listCodes = cacheManager.getListCodesByType("RULETYPE");
				Collections.sort(listCodes, new RuleTypeListComparator());
				
				for (ListCodes code : listCodes) {
					if(code.getCodeCode()!=null && (code.getCodeCode().equalsIgnoreCase("PRE") || code.getCodeCode().equalsIgnoreCase("POST") )){
						selectItems.add(new SelectItem(code.getCodeCode(), code.getDescription()));
					}
				}
							
				ruleTypeMenuItems = selectItems;   
			}

			return ruleTypeMenuItems;
		}
		else{
			if(ruleTypeMenuSalesItems==null){
				List<SelectItem> selectItems = new ArrayList<SelectItem>();	
				List<ListCodes> listCodes = cacheManager.getListCodesByType("RULETYPE");
				Collections.sort(listCodes, new RuleTypeListComparator());
				
				for (ListCodes code : listCodes) {
					if(code.getCodeCode()!=null && (code.getCodeCode().equalsIgnoreCase("PRE") || code.getCodeCode().equalsIgnoreCase("POST") ||
							code.getCodeCode().equalsIgnoreCase("PRESITUS") || code.getCodeCode().equalsIgnoreCase("PRETAXCODE") )){
						selectItems.add(new SelectItem(code.getCodeCode(), code.getDescription()));
					}
				}
				
				ruleTypeMenuSalesItems = selectItems;
			}

			return ruleTypeMenuSalesItems;
		}
	}
	
	public Map<String, String> getReprocessDescriptionMap() {
		if (reprocessDescriptionMap == null) {
			reprocessDescriptionMap = new LinkedHashMap<String, String>();	
			List<ListCodes> listCodes = cacheManager.getListCodesByType("PRTRANSOPT");	
			for (ListCodes code : listCodes) {
				if(code.getCodeCode()!=null){
					if(code.getDescription()!=null){
						reprocessDescriptionMap.put(code.getCodeCode(), code.getDescription());
					}
					else{
						reprocessDescriptionMap.put(code.getCodeCode(), code.getCodeCode());
					}
				}
			}	
		}		
		return reprocessDescriptionMap;
	}
	
	public Map<String, String> getReprocessExplanationMap() {
		if (reprocessExplanationMap == null) {
			reprocessExplanationMap = new LinkedHashMap<String, String>();
			reprocessExplanationMap.put("*NONE", "");
			List<ListCodes> listCodes = cacheManager.getListCodesByType("PRTRANSOPT");	
			for (ListCodes code : listCodes) {
				if(code.getCodeCode()!=null){
					if(code.getExplanation()!=null){
						reprocessExplanationMap.put(code.getCodeCode(), code.getExplanation());
					}
					else{
						reprocessExplanationMap.put(code.getCodeCode(), code.getCodeCode());
					}
				}
			}	
		}		
		return reprocessExplanationMap;
	}

	public List<SelectItem> getReprocessMenuItems() {
		if(reprocessMenuItems==null){
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
			List<ListCodes> listCodes = cacheManager.getListCodesByType("PRTRANSOPT");
			Collections.sort(listCodes, new RuleTypeListComparator());
			
			selectItems.add(new SelectItem("*NONE","None"));
			if(getReprocessDescriptionMap().get("*REPROC")!=null){
				selectItems.add(new SelectItem("*REPROC", getReprocessDescriptionMap().get("*REPROC")));
			}
			else{
				selectItems.add(new SelectItem("*REPROC", "*REPROC"));
			}
			
			if(getReprocessDescriptionMap().get("*CREDIT")!=null){
				selectItems.add(new SelectItem("*CREDIT", getReprocessDescriptionMap().get("*CREDIT")));
			}
			else{
				selectItems.add(new SelectItem("*CREDIT", "*CREDIT"));
			}
			
			if(getReprocessDescriptionMap().get("*NEW")!=null){
				selectItems.add(new SelectItem("*NEW", getReprocessDescriptionMap().get("*NEW")));
			}
			else{
				selectItems.add(new SelectItem("*NEW", "*NEW"));
			}
			
			if(getReprocessDescriptionMap().get("*REV")!=null){
				selectItems.add(new SelectItem("*REV", getReprocessDescriptionMap().get("*REV")));
			}
			else{
				selectItems.add(new SelectItem("*REV", "*REV"));
			}
				
			reprocessMenuItems = selectItems;   
		}

		return reprocessMenuItems;
	}

	public ProcruleService getProcruleService() {
		return procruleService;
	}

	public void setProcruleService(ProcruleService procruleService) {
		this.procruleService = procruleService;
	}
	
	public ProctableService getProctableService() {
		return proctableService;
	}

	public void setProctableService(ProctableService proctableService) {
		this.proctableService = proctableService;
	}
	
	public SimpleSelection getSelection() {
		return selection;
	}
	
	public void setSelection(SimpleSelection selection) {
		this.selection = selection;
	}
	
	public MatrixCommonBean getMatrixCommonBean() {
		return matrixCommonBean;
	}

	public void setMatrixCommonBean(MatrixCommonBean matrixCommonBean) {
		this.matrixCommonBean = matrixCommonBean;
	}
	
	public void setProcruleDataModel(ProcruleDataModel procruleDataModel) {
		this.procruleDataModel = procruleDataModel;
	}
	
	public ProcruleDataModel getProcruleDataModel() {
		return procruleDataModel;
	}
	
	public int getSelectedProcruleRowIndex() {
		return selectedProcruleRowIndex;
	}
	
	public String getRadioSetSelected() {
		return radioSetSelected;
	}
	
	public void setRadioSetSelected(String radioSetSelected) {
		this.radioSetSelected = radioSetSelected;
	}
	
	public String getReorderRuletypeCode() {
		return reorderRuletypeCode;
	}
	
	public void setReorderRuletypeCode(String reorderRuletypeCode) {
		this.reorderRuletypeCode = reorderRuletypeCode;
	}
	
	public void retrieveFilterProcrule() {
		selectedProcruleRowIndex = -1;
		enableProcruleDelete = false;
		enableDefinitionDelete = false;
		selectedProcruleDetails = null;
		updatedProcruleDetails = null;
		setFilterDistinctDateItems(null);
		selectedEffectiveDate = "";
		
		Procrule aProcrule = new Procrule();
		aProcrule.setModuleCode(getCurrentModuleMenu());
		
		//Procrule filter
		if(filterProcrule.getProcruleName()!=null && filterProcrule.getProcruleName().trim().length()>0){
			aProcrule.setProcruleName(filterProcrule.getProcruleName().trim());
		}
		if(filterProcrule.getDescription()!=null && filterProcrule.getDescription().trim().length()>0){
			aProcrule.setDescription(filterProcrule.getDescription().trim());
		}
		if(filterProcrule.getActiveFlag()!=null && filterProcrule.getActiveFlag().length()>0){
			if(filterProcrule.getActiveFlag().equals("1")){
				aProcrule.setActiveFlag("1");
			}
			else{
				aProcrule.setActiveFlag("0");
			}
		}
		
		if(filterProcrule.getRuletypeCode()!=null && filterProcrule.getRuletypeCode().length()>0){
				aProcrule.setRuletypeCode(filterProcrule.getRuletypeCode());
		}
		
		procruleDataModel.setCriteria(aProcrule);
	}
	
	public void distinctDateChanged(ActionEvent e) {
		selectedProcruleDetails = new ArrayList<ProcruleDetail>();
		updatedProcruleDetails = new ArrayList<ProcruleDetail>();
		
		if(selProcrule==null || selProcrule.getProcruleId()==null || (selectedProcruleRowIndex == -1)){
			return; //Not selected
		}
		
		if(selectedEffectiveDate!=null && selectedEffectiveDate.length()>0){		
			SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");	
			Date effectiveDate = null;
			try {
				effectiveDate = format.parse(selectedEffectiveDate);
			}
			catch(ParseException err){}
			
			if(effectiveDate!=null){
				retrieveProcruleDetail(selProcrule.getProcruleId(), effectiveDate);
			}
		}
	}
	
	public void retrieveFilterProcruleDetail() {
		selectedProcruleDetails = new ArrayList<ProcruleDetail>();
		updatedProcruleDetails = new ArrayList<ProcruleDetail>();		
		setFilterDistinctDateItems(null);
		selectedEffectiveDate = "";
		
		if(selProcrule==null || selProcrule.getProcruleId()==null || (selectedProcruleRowIndex == -1)){
			return; //Not selected
		}
		
		//Add distinct dates to combox
		List<String> distinctDateList = new ArrayList<String>();
		distinctDateList = procruleService.findProcruleDetailDistinctDate(selProcrule.getProcruleId());
		
		if(distinctDateList!=null && distinctDateList.size()>0){		
			SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");	
			Date effectiveDate = null;
			try {
				effectiveDate = format.parse((String)distinctDateList.get(0));
			}
			catch(ParseException e){}
			
			if(effectiveDate!=null){
				retrieveProcruleDetail(selProcrule.getProcruleId(), effectiveDate);
			}
			
			//Build distinct dates
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
			for (String dateStr : distinctDateList) {
				selectItems.add(new SelectItem(dateStr, dateStr));
			}
			setFilterDistinctDateItems(selectItems);
			selectedEffectiveDate = (String)distinctDateList.get(0);
		}
	}
	
	public void retrieveProcruleDetail(Long procruleId, Date effectiveDate) {
		selectedProcruleDetails = new ArrayList<ProcruleDetail>();
		updatedProcruleDetails = new ArrayList<ProcruleDetail>();
		
		List<ProcruleDetail> actualRowsList = new ArrayList<ProcruleDetail>();	
		actualRowsList = procruleService.findProcruleDetail(selProcrule.getProcruleId(), effectiveDate);
		
		ProcruleDetail aProcruleDetail = null;
	    for(int i=0; i<actualRowsList.size(); i++ ){
	    	aProcruleDetail = (ProcruleDetail)actualRowsList.get(i);    	
	    	aProcruleDetail.setTextLike(getBusinessLike(aProcruleDetail, true));
	    	aProcruleDetail.setSqlLike(getBusinessLike(aProcruleDetail, false));
	    	
	    	if(i==0){
	    		if(!aProcruleDetail.getCondition().equalsIgnoreCase("When")){
		    		//Always Apply
	    			ProcruleDetail firstProcruleDetail = new ProcruleDetail();
	    			firstProcruleDetail.setCondition("When");
	    			if(aProcruleDetail.getActiveBooleanFlag()){
	    				firstProcruleDetail.setActiveBooleanFlag(true);
	    			}
	    			selectedProcruleDetails.add(firstProcruleDetail);
	    			
	    			ProcruleDetail alwaysProcruleDetail = new ProcruleDetail();
	    			alwaysProcruleDetail.setTextLike(" Always Apply");
	    			alwaysProcruleDetail.setSqlLike(" Always Apply");
	    			if(aProcruleDetail.getActiveBooleanFlag()){
	    				alwaysProcruleDetail.setActiveBooleanFlag(true);
	    			}
	    			selectedProcruleDetails.add(alwaysProcruleDetail);
		    	}
	    	}
	    		
	    	selectedProcruleDetails.add(aProcruleDetail);
	    	updatedProcruleDetails.add(aProcruleDetail);
	    }
	}
	
	private String getBusinessLike(ProcruleDetail aProcruleDetail, boolean bText){
		String retSql ="";
		
		String compareField = ""; //COMPANY_NBR
		String compareValue = ""; //COMPANY_NBR or 123
		String compareRelation = "";// =, <>, >...
		String compareValueCode = ""; //V: value, F: column
		if(aProcruleDetail.getCompareRelation()==null || aProcruleDetail.getCompareRelation().length()==0){
			return "";
		}
		
		compareField = aProcruleDetail.getCompareField();
		compareValue = aProcruleDetail.getCompareValue();
		compareRelation = aProcruleDetail.getCompareRelation();
		compareValueCode = aProcruleDetail.getCompareValueCode();
		
		//Compare field
		String transactionTable = "";
		if(getIsPurchasingMenu()){
			transactionTable = "TB_PURCHTRANS";
		}
		else{
			transactionTable = "TB_BILLTRANS";
		}
		Map<String,DataDefinitionColumn> propertyMap = cacheManager.getDataDefinitionPropertyMap(transactionTable);

		if(compareField!=null && compareField.length()>0){
			if(bText){
				String field = Util.lowerCaseFirst(Util.columnToProperty(compareField));
				DataDefinitionColumn aDataDefinitionColumn = (DataDefinitionColumn)propertyMap.get(field);
				if(aDataDefinitionColumn!=null){
					String textLike = aDataDefinitionColumn.getDescription();
					retSql = retSql + " " + textLike + " ";	
				}
			}
			else{
				retSql = retSql + " " + compareField + " ";
			}
		}
	
		//Relation 
		if(bText){
			retSql = retSql + " " + compareRelation + " ";
		}
		else{
			if(compareRelation.equalsIgnoreCase("IsNull")){
				retSql = retSql + " " + "Is Null" + " ";					
			}
			else if(compareRelation.equalsIgnoreCase("IsNotNull")){
				retSql = retSql + " " + "Is Not Null" + " ";
			}
			else if(compareRelation.equalsIgnoreCase("StartsWith")){
				retSql = retSql + " " + "Like" + " ";
			}
			else if(compareRelation.equalsIgnoreCase("EndsWith")){
				retSql = retSql + " " + "Like" + " ";
			}
			else if(compareRelation.equalsIgnoreCase("Contains")){
				retSql = retSql + " " + "Like" + " ";
			}
			else {
				retSql = retSql + " " + compareRelation + " ";
			}
		}
		
		if(compareValue!=null && compareValue.length()>0){
			if(bText){
				if(compareValueCode!=null && compareValueCode.equalsIgnoreCase("F")){//column name
					String field = Util.lowerCaseFirst(Util.columnToProperty(compareValue));
					DataDefinitionColumn aDataDefinitionColumn = (DataDefinitionColumn)propertyMap.get(field);
					if(aDataDefinitionColumn!=null){
						String textLike = aDataDefinitionColumn.getDescription();
						retSql = retSql + " " + textLike + " ";	
					}
				}
				else if(compareValueCode!=null && compareValueCode.equalsIgnoreCase("T") && compareRelation.equalsIgnoreCase("InTable")){//table name in When
					// value, no wrapping temporarily **********
				
					//get table name
					Long proctableId = 0L;
					try{
						proctableId = Long.parseLong(compareValue);
					}
					catch (Exception e){	
						e.getStackTrace();
					}
						
					Proctable aProctable = proctableService.findById(proctableId);						
					if(aProctable!=null){
						retSql = retSql + " " + aProctable.getProctableName() + " ";
					}
				}
				else if(compareValueCode!=null && compareValueCode.equalsIgnoreCase("T")){//table name
					// value, no wrapping temporarily **********
					retSql = retSql + " " + aProcruleDetail.getTableParms() + " ";	
				}
				else{// value, no wrapping
					retSql = retSql + " " + compareValue + " ";	
				}	
				
				//Operator
				if(aProcruleDetail.getOperatorCode()!=null && !aProcruleDetail.getOperatorCode().equalsIgnoreCase("n/a")){
					retSql = retSql + " " + aProcruleDetail.getOperatorCode() + " ";
					//retSql = retSql + " " + aProcruleDetail.getOperatorValue() + " ";
					if(aProcruleDetail.getOperatorValueCode()!=null && aProcruleDetail.getOperatorValueCode().equalsIgnoreCase("F")){//column name
						String field = Util.lowerCaseFirst(Util.columnToProperty(aProcruleDetail.getOperatorValue()));
						DataDefinitionColumn aDataDefinitionColumn = (DataDefinitionColumn)propertyMap.get(field);
						if(aDataDefinitionColumn!=null){
							String textLike = aDataDefinitionColumn.getDescription();
							retSql = retSql + " " + textLike + " ";	
						}
					}
					else{
						retSql = retSql + " " + aProcruleDetail.getOperatorValue() + " ";
					}
				}
			}
			else{
				if(compareValueCode!=null && compareValueCode.equalsIgnoreCase("F")){//column name
					retSql = retSql + " " + compareValue + " ";	
				}
				else if(compareValueCode!=null && compareValueCode.equalsIgnoreCase("T") && compareRelation.equalsIgnoreCase("InTable")){//table name in When
					retSql = retSql + " " + compareValue + " ";	
				}
				else if(compareValueCode!=null && compareValueCode.equalsIgnoreCase("T")){//table name
					// value, no wrapping temporarily **********
					retSql = retSql + " " + aProcruleDetail.getTableParms() + " ";	
				}
				else{// value, need wrapping
					if(compareRelation.equalsIgnoreCase("IsNull") || compareRelation.equalsIgnoreCase("IsNotNull")){				
					}		
					else if(compareRelation.equalsIgnoreCase("StartsWith")){
						retSql = retSql + " '" + compareValue + "%' ";
					}
					else if(compareRelation.equalsIgnoreCase("EndsWith")){
						retSql = retSql + " '%" + compareValue + "' ";
					}
					else if(compareRelation.equalsIgnoreCase("Contains")){
						retSql = retSql + " " + "'%" + compareValue + "%'" + " ";
					}
					else {
						String field = Util.upperCaseFirst(Util.columnToProperty(compareField));
						try{
							Method m = null;
							if(getIsPurchasingMenu()){
								m = TransactionDetail.class.getMethod("get" + field, new Class[] { });
							}
							else{
								m = com.seconddecimal.billing.domain.SaleTransaction.class.getMethod("get" + field, new Class[] { });
							}
							
							if (m.getReturnType().equals(Date.class)) {
								retSql = retSql + " " + "TO_DATE('" + compareValue + "','MM/dd/yyyy')" + " ";
							}
							else if (m.getReturnType().equals(Float.class) || m.getReturnType().equals(Double.class) || m.getReturnType().equals(Long.class)) {
								retSql = retSql + " " + compareValue + " ";
							} 
							else if(m.getReturnType().equals(String.class)) {
								retSql = retSql + " '" + compareValue + "' ";
							}
						}
						catch(Exception e){
							
						}
					}
				}
				
				//Operator
				if(aProcruleDetail.getOperatorCode()!=null && !aProcruleDetail.getOperatorCode().equalsIgnoreCase("n/a")){
					retSql = retSql + " " + aProcruleDetail.getOperatorCode() + " ";
					
					if(aProcruleDetail.getOperatorValueCode()!=null && aProcruleDetail.getOperatorValueCode().equalsIgnoreCase("V")){
						String field = Util.upperCaseFirst(Util.columnToProperty(compareField));
						try{
							Method m = null;
							if(getIsPurchasingMenu()){
								m = TransactionDetail.class.getMethod("get" + field, new Class[] { });
							}
							else{
								m = com.seconddecimal.billing.domain.SaleTransaction.class.getMethod("get" + field, new Class[] { });
							}
							
							if (m.getReturnType().equals(Date.class)) {
								retSql = retSql + " " + "TO_DATE('" + aProcruleDetail.getOperatorValue() + "','MM/dd/yyyy')" + " ";
							}
							else if (m.getReturnType().equals(Float.class) || m.getReturnType().equals(Double.class) || m.getReturnType().equals(Long.class)) {
								retSql = retSql + " " + aProcruleDetail.getOperatorValue() + " ";
							} 
							else if(m.getReturnType().equals(String.class)) {
								retSql = retSql + " '" + aProcruleDetail.getOperatorValue() + "' ";
							}
						}
						catch(Exception e){
							
						}
					}
					else{
						retSql = retSql + " " + aProcruleDetail.getOperatorValue() + " ";//column
					}
					
				}
			}
		}
		
		return retSql;
	}
	
	public List<ProcruleDetail> getSelectedProcruleDetails() {
		return selectedProcruleDetails;
	}

	public void selectedProcruleRowChanged(ActionEvent e) throws AbortProcessingException {
		HtmlDataTable table = (HtmlDataTable) ((HtmlAjaxSupport) e.getComponent()).getParent(); 
		setSelectedProcruleRowIndex(table.getRowIndex());
		
		selProcrule = new Procrule();
		selProcrule = (Procrule)table.getRowData();	
		enableProcruleDelete = procruleService.isAllowDeleteRules(selProcrule.getProcruleId());
		enableDefinitionDelete = false;
		
		retrieveFilterProcruleDetail();
	}
	
	public String getActionText() {
		if(currentAction.equals(EditAction.ADD)){
			return "Add a Process Rule / Details";
		}
		else if(currentAction.equals(EditAction.UPDATE_PROC_DETAILS)){
			return "Update a Process Rule / Details";
		}
		else if(currentAction.equals(EditAction.UPDATE)){
			return "Update a Process Rule";
		}
		else if(currentAction.equals(EditAction.DELETE)){
			return "Delete a Process Rule / Details";
		}
		else if(currentAction.equals(EditAction.VIEW)){
			return "View a Process Rule / Details";
		}
		else if(currentAction.equals(EditAction.COPY_ADD)){
			return "Copy/Add a Process Rule / Details";
		}
		else if(currentAction.equals(EditAction.COPY_UPDATE)){
			return "Copy/Update a Process Rule / Details";
		}
		else if(currentAction.equals(EditAction.DELETE_ALL)){
			return "Delete a Process Rule";
		}
		if(currentAction.equals(EditAction.ADD_NEW)){
			return "Add a Process Rule / Details";
		}

		return "";
	}
	
	public Boolean getDisplayCopyAddAction() {
		return (currentAction == EditAction.COPY_ADD);
	}
	
	public Boolean getDisplayDeleteAllAction() {
		return (currentAction == EditAction.DELETE_ALL);
	}
	
	public Boolean getDisplayAddAction() {
		return (currentAction == EditAction.ADD);
	}
	
	public Boolean getDisplayDeleteAction() {
		return (currentAction == EditAction.DELETE);
	}
	
	public Boolean getDisplayUpdateAction() {
		return (currentAction == EditAction.UPDATE);
	}
	
	public Boolean getDisplayUpdateProcRulesDetailAction() {
		return (currentAction == EditAction.UPDATE_PROC_DETAILS);
	}
	
	public Boolean getDisplayViewAction() {
		return (currentAction == EditAction.VIEW);
	}
	
	public Boolean getDisplayCopyUpdateAction() {
		return (currentAction == EditAction.COPY_UPDATE);
	}
	
	void resetData(){
		talbeSelectionItems=null;
	}
	
	public String displayAddProcruleAction() {
		this.currentAction = EditAction.ADD;
		resetData();

		updateProcrule= new Procrule();		
		updateProcrule.setActiveBooleanFlag(true);
		updateProcrule.setRuletypeCode("PRE");
		updateProcrule.setAlwaysBooleanFlag(false);
		updateProcrule.setModuleCode(getCurrentModuleMenu());	
		setEffectiveDate(new com.ncsts.view.util.DateConverter().getUserLocalDate());
		this.setDetailActiveBooleanFlag(true);

		//For when 
		whenObjectItems= new ArrayList<WhenObject>();
		WhenObject aWhenObject = new WhenObject();
		if(getIsPurchasingMenu()){
			aWhenObject.setOperationWhen("ALL"); //Temporarily default to ALL for purchasing
		}
		
		whenObjectItems.add(aWhenObject);
		
		//For Set
		setObjectItems= new ArrayList<SetObject>();
		SetObject aSetObject = new SetObject();
		if(getIsPurchasingMenu()){
			aSetObject.setOperationSet("ALL"); //Temporarily default to ALL for purchasing
		}
		setObjectItems.add(aSetObject);

		return "procrule_update";
	}
	
	public String reorderRuleTypeChange() {
		if(reorderProcruleArray!=null && reorderProcruleArray.size()>0){
			reorderProcruleArray.clear();
	    }

		reorderProcruleArray = procruleService.getProcrule(getCurrentModuleMenu(), reorderRuletypeCode);
	    return null;
	}
	
	public String displayReorderProcruleAction() {
		this.currentAction = EditAction.REORDER;
		
		if(filterProcrule.getRuletypeCode()!=null && filterProcrule.getRuletypeCode().length()>0){
			reorderRuletypeCode = filterProcrule.getRuletypeCode();
		}
		else{
			reorderRuletypeCode = "PRE";
		}

		reorderProcruleArray = procruleService.getProcrule(getCurrentModuleMenu(), reorderRuletypeCode);
		
		return "procrule_reorder";
	}

	public String displayDeleteAllProcruleAction() {
		this.currentAction = EditAction.DELETE_ALL;
		resetData();
		
		updateProcrule= new Procrule();		
		if (selProcrule != null) {
			BeanUtils.copyProperties(selProcrule, updateProcrule);
		}
		
		SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");	
		Date effectiveDate = null;
		try {
			effectiveDate = format.parse("12/31/9999");
		}
		catch(ParseException err){}
		
		if(effectiveDate!=null){
			this.effectiveDate = effectiveDate;
		}
		
		return "procrule_update";
	}
	
	public String displayDeleteProcruleAction() {
		displayUpdateProcruleDetailsAction();
		this.currentAction = EditAction.DELETE;
		resetData();
		
		enableDefinitionDelete = false;
		if(selectedEffectiveDate!=null && selectedEffectiveDate.length()>0){		
			SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");	
			Date effectiveDate = null;
			try {
				effectiveDate = format.parse(selectedEffectiveDate);
				
				enableDefinitionDelete = procruleService.isAllowDeleteDefinitions(selProcrule.getProcruleId(), effectiveDate);
			}
			catch(ParseException err){}		
		}
		
		return "procrule_update";
	}
	
	public String displayViewProcruleAction(){
		displayUpdateProcruleDetailsAction();
		this.currentAction = EditAction.VIEW;
		resetData();
		
		return "procrule_update";
	}
	
	public String displayUpdateProcruleAction(){
		this.currentAction = EditAction.UPDATE;
		resetData();
		
		if(selProcrule==null){
			return "procrule_main";
		}
		
		if(selectedEffectiveDate!=null && selectedEffectiveDate.length()>0){		
			SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");	
			Date effectiveDate = null;
			try {
				effectiveDate = format.parse(selectedEffectiveDate);
			}
			catch(ParseException err){}
			
			if(effectiveDate!=null){
				this.effectiveDate = effectiveDate;
			}
		}
		else{
			return "procrule_main";
		}
		
		updateProcrule= new Procrule();		
		if (selProcrule != null) {
			BeanUtils.copyProperties(selProcrule, updateProcrule);
		}
		
		List<ProcruleDetail> actualRowsList = new ArrayList<ProcruleDetail>();
		actualRowsList = procruleService.findProcruleDetail(selProcrule.getProcruleId(), effectiveDate);
		for(ProcruleDetail procruleDetail : actualRowsList) {				
			this.setDetailActiveBooleanFlag(procruleDetail.getActiveBooleanFlag());	  
		}
		
		return "procrule_update";
	}
	
	public String displayCopyUpdateProcruleAction(){
		displayUpdateProcruleDetailsAction();
		this.currentAction = EditAction.COPY_UPDATE;
		resetData();
		
		return "procrule_update";
	}
	
	public String displayCopyAddProcruleAction(){
		displayUpdateProcruleDetailsAction();
			
		updateProcrule.setProcruleId(null);
		updateProcrule.setProcruleName("");
		updateProcrule.setDescription("");
		updateProcrule.setActiveBooleanFlag(selProcrule.getActiveBooleanFlag());
		updateProcrule.setRuletypeCode(selProcrule.getRuletypeCode());
		updateProcrule.setAlwaysBooleanFlag(selProcrule.getAlwaysBooleanFlag());
		updateProcrule.setModuleCode(getCurrentModuleMenu());	
		updateProcrule.setReprocessCode(selProcrule.getReprocessCode()); 
		setEffectiveDate(new com.ncsts.view.util.DateConverter().getUserLocalDate());
		
		this.currentAction = EditAction.COPY_ADD;
		resetData();
		
		return "procrule_update";
	}
	
	public String displayUpdateProcruleDetailsAction() {
		this.currentAction = EditAction.UPDATE_PROC_DETAILS;
		resetData();
		
		if(selProcrule==null){
			return "procrule_main";
		}
		
		if(updatedProcruleDetails==null || updatedProcruleDetails.size()==0){
			return "procrule_main";
		}
		
		if(selectedEffectiveDate!=null && selectedEffectiveDate.length()>0){		
			SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");	
			Date effectiveDate = null;
			try {
				effectiveDate = format.parse(selectedEffectiveDate);
			}
			catch(ParseException err){}
			
			if(effectiveDate!=null){
				this.effectiveDate = effectiveDate;
			}
		}
		else{
			return "procrule_main";
		}
		
		updateProcrule= new Procrule();		
		if (selProcrule != null) {
			BeanUtils.copyProperties(selProcrule, updateProcrule);
		}
		
		//For when 
		whenObjectItems= new ArrayList<WhenObject>();
		WhenObject aWhenObject = null;

		//For Set
		setObjectItems= new ArrayList<SetObject>();
		SetObject aSetObject = null;

		boolean getWhen = false;
		boolean getSet = false;
		ProcruleDetail aProcruleDetail = null;
		for(int i=0; i<updatedProcruleDetails.size(); i++){	
			aProcruleDetail = (ProcruleDetail)updatedProcruleDetails.get(i);
			
			if(aProcruleDetail.getCondition()!=null && aProcruleDetail.getCondition().equalsIgnoreCase("When")){
				//Will get the first one
				getWhen = true;
				this.setDetailActiveBooleanFlag(aProcruleDetail.getActiveBooleanFlag()); 
				continue;
			}
			
			if(aProcruleDetail.getCondition()!=null && aProcruleDetail.getCondition().equalsIgnoreCase("Set")){
				//Will get the first one
				getSet = true;
				this.setDetailActiveBooleanFlag(aProcruleDetail.getActiveBooleanFlag()); 
				continue;
			}
			
			//We might apply always, no when object.
			if(!getSet){
				//Add to When array
				aWhenObject = new WhenObject();
				aWhenObject.setOperatorWhen(aProcruleDetail.getCondition());
				aWhenObject.setOperationWhen("ALL"); 
				aWhenObject.setOpenparenWhen((aProcruleDetail.getOpenparen()!=null && aProcruleDetail.getOpenparen().equalsIgnoreCase("1"))? true : false);
				
				//Set column, text, number, date type
				if(isNumberSalesColumnType(aProcruleDetail.getCompareField())){
			    	aWhenObject.setNumberColumnType();
			    }
			    else if(isTextSalesColumnType(aProcruleDetail.getCompareField())){
			    	aWhenObject.setTextColumnType();		    	
			    }
			    else if(isDateSalesColumnType(aProcruleDetail.getCompareField())){
			    	aWhenObject.setDateColumnType();
			    }
				
				aWhenObject.setFieldWhen(aProcruleDetail.getCompareField());
				if(aProcruleDetail.getCompareRelation().equalsIgnoreCase("InTable")){
					aWhenObject.setRelationWhen("InTable");	
					aWhenObject.setFieldTableWhen(aProcruleDetail.getCompareValue());
				}
				else{
					aWhenObject.setRelationWhen(aProcruleDetail.getCompareRelation());	
					if(aProcruleDetail.getCompareValueCode()!=null && aProcruleDetail.getCompareValueCode().equalsIgnoreCase("F")){
						//Cannot decide fromAll, Custome, or User...
						aWhenObject.setValuelocationWhen("ALL");
						aWhenObject.setFieldColumnWhen(aProcruleDetail.getCompareValue());
					}
					else{
						aWhenObject.setValuelocationWhen("VALUE");
						aWhenObject.setFieldTextWhen(aProcruleDetail.getCompareValue());
					}
				}
				
				aWhenObject.setCloseparenWhen((aProcruleDetail.getCloseparen()!=null && aProcruleDetail.getCloseparen().equalsIgnoreCase("1"))? true : false);
				
				whenObjectItems.add(aWhenObject);
			}
			else if(getSet){
				//Add to Set array
				aSetObject = new SetObject();
				
				if(aProcruleDetail.getCompareValueCode()!=null && aProcruleDetail.getCompareValueCode().equalsIgnoreCase("S")){
					aSetObject.setOperationSet("SPROC");
					aSetObject.setSprocName(aProcruleDetail.getCompareValue());
					aSetObject.setSprocParameters(aProcruleDetail.getTableParms());
				}
				else{
					//Set column, text, number, date type
					if(isNumberSalesColumnType(aProcruleDetail.getCompareField())){
						aSetObject.setNumberColumnType();
				    }
				    else if(isTextSalesColumnType(aProcruleDetail.getCompareField())){
				    	aSetObject.setTextColumnType();		    	
				    }
				    else if(isDateSalesColumnType(aProcruleDetail.getCompareField())){
				    	aSetObject.setDateColumnType();
				    }
					
					aSetObject.setOperationSet("ALL"); 
					aSetObject.setFieldSet(aProcruleDetail.getCompareField());
					if(aProcruleDetail.getCompareValueCode()!=null && aProcruleDetail.getCompareValueCode().equalsIgnoreCase("F")){
						//Cannot decide fromAll, Custome, or User...
						aSetObject.setValuelocationSet("ALL");
						aSetObject.setFieldColumnSet(aProcruleDetail.getCompareValue());
					}
					else if(aProcruleDetail.getCompareValueCode()!=null && aProcruleDetail.getCompareValueCode().equalsIgnoreCase("T")){
						aSetObject.setValuelocationSet("TABLE");
						aSetObject.setFieldTextSet(aProcruleDetail.getCompareValue());
						aSetObject.setParamTextSet(aProcruleDetail.getTableParms());
					}
					else{
						aSetObject.setValuelocationSet("VALUE");
						aSetObject.setFieldTextSet(aProcruleDetail.getCompareValue());
					}
					
					
					if(aProcruleDetail.getOperatorCode()!=null && !aProcruleDetail.getOperatorCode().equalsIgnoreCase("n/a")){
						aSetObject.setOperator(aProcruleDetail.getOperatorCode());					
						if(aProcruleDetail.getOperatorValueCode()!=null && aProcruleDetail.getOperatorValueCode().equalsIgnoreCase("V")){
							aSetObject.setOperatorSet("VALUE");
							aSetObject.setOperatorTextSet(aProcruleDetail.getOperatorValue());
						}
						else{
							aSetObject.setOperatorSet("ALL");
							aSetObject.setOperatorColumnSet(aProcruleDetail.getOperatorValue());
						}
						aSetObject.setOperator(aProcruleDetail.getOperatorCode());
					}
					else{
						aSetObject.setOperator("n/a");
					}
				}
				
				setObjectItems.add(aSetObject);
			}
			
		}
		
		if(whenObjectItems.size()==0){
			updateProcrule.setAlwaysBooleanFlag(true);
		}

		return "procrule_update";

	}
	
	public String okProcessRuleAction() {
		String result = null;
		switch (currentAction) {
			case DELETE:
				result = deleteProcessRuleAction();
				break;
				
			case DELETE_ALL:
				result = deleteAllProcessRuleAction();
				break;	
				
			case UPDATE_PROC_DETAILS:
				result = updateProcessRuleDetailAction();
				break;
				
			case UPDATE:
				result = updateProcessRuleAction();
				break;
				
			case COPY_UPDATE:
				result = copyupdateProcessRuleAction();
				break;
				
			case COPY_ADD:
				result = copyaddProcessRuleAction();
				break;
				
			case REORDER:
				result = reorderProcessRuleAction();
				break;
				
			case ADD:
				result = addProcessRuleAction();
				break;
				
			case VIEW:
				result = "procrule_main";
				break;
				
			default:
				result = cancelAction();
				break;
		}
		return result;
	}
	
	private List<ProcruleDetail> buildUpdateProcruleDetail(){
		List<ProcruleDetail> aProcruleEntityList= new ArrayList<ProcruleDetail>();
		
		ProcruleDetail aProcruleDetail = null;
		int lineNo = 1;
		if(whenObjectItems!=null && whenObjectItems.size()>0 && !updateProcrule.getAlwaysBooleanFlag()){
			aProcruleDetail = new ProcruleDetail();
			//aProcruleDetail.setProcruleId(procruleId);
			aProcruleDetail.setLineNo(new Long(lineNo++));
			aProcruleDetail.setCondition("When");
			aProcruleEntityList.add(aProcruleDetail);
			
			WhenObject aWhenObject = null;
			for(int i=0; i<whenObjectItems.size(); i++){
				aWhenObject = (WhenObject)whenObjectItems.get(i);
				aProcruleDetail = new ProcruleDetail();
				aProcruleDetail.setLineNo(new Long(lineNo++));
				if(i==0){
					aProcruleDetail.setCondition("");
				}
				else{
					aProcruleDetail.setCondition(aWhenObject.getOperatorWhen());
				}
				aProcruleDetail.setOpenparen(aWhenObject.getOpenparenWhen()? "1":"0");
				aProcruleDetail.setCompareField(aWhenObject.getFieldWhen());
				if(getIsPurchasingMenu()){
					aProcruleDetail.setCompareFieldTable("TB_PURCHTRANS");
				}
				else{
					aProcruleDetail.setCompareFieldTable("TB_BILLTRANS");
				}
				
				if(aWhenObject.getRelationWhen().equalsIgnoreCase("InTable")) {
					aProcruleDetail.setCompareRelation(aWhenObject.getRelationWhen());

					aProcruleDetail.setCompareValueCode("T"); //In Table in When Relation
					aProcruleDetail.setCompareValue(aWhenObject.getFieldTableWhen());
					aProcruleDetail.setCompareValueTable("");
				}
				else{
					aProcruleDetail.setCompareRelation(aWhenObject.getRelationWhen());
					if(aWhenObject.getValuelocationWhen()!=null){
						if(aWhenObject.getValuelocationWhen().equalsIgnoreCase("VALUE")){
							aProcruleDetail.setCompareValueCode("V");
							aProcruleDetail.setCompareValue(aWhenObject.getFieldTextWhen());
							aProcruleDetail.setCompareValueTable("");
						}
						else{
							aProcruleDetail.setCompareValueCode("F");
							aProcruleDetail.setCompareValue(aWhenObject.getFieldColumnWhen());
							if(getIsPurchasingMenu()){
								aProcruleDetail.setCompareValueTable("TB_PURCHTRANS");
							}
							else{
								aProcruleDetail.setCompareValueTable("TB_BILLTRANS");
							}
						}
					}	
				}
							
				aProcruleDetail.setCloseparen(aWhenObject.getCloseparenWhen()? "1":"0");
				aProcruleDetail.setTableParms("");
				
				aProcruleEntityList.add(aProcruleDetail);
			}
		}	
		
		if(setObjectItems!=null && setObjectItems.size()>0){
			aProcruleDetail = new ProcruleDetail();
			//aProcruleDetail.setProcruleId(procruleId);
			aProcruleDetail.setLineNo(new Long(lineNo++));
			aProcruleDetail.setCondition("Set");
			aProcruleEntityList.add(aProcruleDetail);

			SetObject aSetObject = null;
			for(int i=0; i<setObjectItems.size(); i++){
				aSetObject = (SetObject)setObjectItems.get(i);
				aProcruleDetail = new ProcruleDetail();
				aProcruleDetail.setLineNo(new Long(lineNo++));
				
				if(i==0){
					aProcruleDetail.setCondition("");
				}
				else{
					aProcruleDetail.setCondition("AND");
				}
				
				//aProcruleDetail.setOpenparen("");
				aProcruleDetail.setCompareField(aSetObject.getFieldSet());
				if(getIsPurchasingMenu()){
					aProcruleDetail.setCompareFieldTable("TB_PURCHTRANS");
				}
				else{
					aProcruleDetail.setCompareFieldTable("TB_BILLTRANS");
				}
				
				aProcruleDetail.setCompareRelation("=");
				if(aSetObject.getValuelocationSet()!=null){
					if(aSetObject.getOperationSet().equalsIgnoreCase("SPROC")){
						aProcruleDetail.setCompareValueCode("S");
						aProcruleDetail.setCompareValue(aSetObject.getSprocName());
						aProcruleDetail.setCompareValueTable("");
						aProcruleDetail.setTableParms(aSetObject.getSprocParameters());
					}
					else if(aSetObject.getValuelocationSet().equalsIgnoreCase("VALUE")){
						aProcruleDetail.setCompareValueCode("V");
						aProcruleDetail.setCompareValue(aSetObject.getFieldTextSet());
						aProcruleDetail.setCompareValueTable("");
					}
					else if(aSetObject.getValuelocationSet().equalsIgnoreCase("Table")){
						aProcruleDetail.setCompareValueCode("T");
						aProcruleDetail.setCompareValue(aSetObject.getFieldTextSet());
						aProcruleDetail.setCompareValueTable("");
						aProcruleDetail.setTableParms(aSetObject.getParamTextSet());
					}
					else{
						aProcruleDetail.setCompareValueCode("F");
						aProcruleDetail.setCompareValue(aSetObject.getFieldColumnSet());
						if(getIsPurchasingMenu()){
							aProcruleDetail.setCompareValueTable("TB_PURCHTRANS");
						}
						else{
							aProcruleDetail.setCompareValueTable("TB_BILLTRANS");
						}
					}	
					
					aProcruleDetail.setOperatorCode(aSetObject.getOperator());
					
					if(aSetObject.getOperatorSet().equalsIgnoreCase("VALUE")){
						aProcruleDetail.setOperatorValueCode("V");
						aProcruleDetail.setOperatorValue(aSetObject.getOperatorTextSet());
						aProcruleDetail.setOperatorValueTable("");
					}
					else{
						aProcruleDetail.setOperatorValueCode("F");
						aProcruleDetail.setOperatorValue(aSetObject.getOperatorColumnSet());					
						if(getIsPurchasingMenu()){
							aProcruleDetail.setOperatorValueTable("TB_PURCHTRANS");
						}
						else{
							aProcruleDetail.setOperatorValueTable("TB_BILLTRANS");
						}
					}
					
					aProcruleDetail.setAnonSql("");//No run
				}
				aProcruleDetail.setCloseparen("");
				
				aProcruleEntityList.add(aProcruleDetail);
			}
		}
		
		if(aProcruleEntityList!=null && aProcruleEntityList.size()>0){
			ProcruleDetail aDetail = null;
		    for(int i=0; i<aProcruleEntityList.size(); i++ ){
		    	aDetail = (ProcruleDetail)aProcruleEntityList.get(i);
		    	aDetail.setEffectiveDate(getEffectiveDate());
		    }
		}
		
		return aProcruleEntityList;
		
	}
	
	boolean validateRule(){
		boolean errorMessage = false;
		
		//Validate name, type
		if(updateProcrule.getProcruleName()==null || updateProcrule.getProcruleName().trim().length()==0){
			errorMessage = true;
			
			FacesMessage message = new FacesMessage("The Rule Name is mandatory.");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
		}
		
		if(updateProcrule.getRuletypeCode()==null || updateProcrule.getRuletypeCode().trim().length()==0){
			errorMessage = true;
			
			FacesMessage message = new FacesMessage("The Rule Type is mandatory.");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
		}
		
		
		if(errorMessage==false && (currentAction.equals(EditAction.ADD) || currentAction.equals(EditAction.COPY_ADD))  && procruleService.getIsRuleNameExist(updateProcrule.getProcruleName(), updateProcrule.getModuleCode())){
			errorMessage = true;
			
			FacesMessage message = new FacesMessage("Duplicate Rule Name is not Allowed.");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
		}
		
		if(errorMessage==false && currentAction.equals(EditAction.COPY_UPDATE)){
			if(procruleService.getIsRuleNameDateExist(updateProcrule.getProcruleName(), updateProcrule.getModuleCode(), this.effectiveDate)){
				errorMessage = true;
				
				FacesMessage message = new FacesMessage("Duplicate Rule Name and Effective Date are not Allowed.");
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage(null, message);
			}
		}
		
		return errorMessage;
	}
	
	boolean validateWhenSet(){
		boolean errorMessage = false;
		
		WhenObject aWhenObject = null;
		
		//Verify openparen/closeparen
		if(whenObjectItems!=null && whenObjectItems.size()>0){
			int openParen = 0;
			int closeParen = 0;
			for(int i=0; i<whenObjectItems.size(); i++){
				aWhenObject = (WhenObject)whenObjectItems.get(i);
				
				if(aWhenObject.getOpenparenWhen()){
					openParen++;
				}
				
				if(aWhenObject.getCloseparenWhen()){
					closeParen++;
					
					if(closeParen>openParen){
						errorMessage = true;
						
						FacesMessage message = new FacesMessage("The open and close parenthesis are not matched.");
						message.setSeverity(FacesMessage.SEVERITY_ERROR);
						FacesContext.getCurrentInstance().addMessage(null, message);
						
						break;
					}
				}
			}
			
			if(!errorMessage && (openParen!=closeParen)){
				//If there is no error so far, check again.
				errorMessage = true;
				
				FacesMessage message = new FacesMessage("The open and close parenthesis are not matched.");
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage(null, message);
			}		
		}
		
		//Verify when
		for(int i=0; i<whenObjectItems.size(); i++){
			aWhenObject = (WhenObject)whenObjectItems.get(i);
			
			if(aWhenObject.getFieldWhen()==null || aWhenObject.getFieldWhen().length()==0){
				errorMessage = true;
				
				FacesMessage message = new FacesMessage("When line " + (i+1) + ": The Field combo box cannot be empty.");
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage(null, message);
			}
			

			if(aWhenObject.getRelationWhen().equalsIgnoreCase("StartsWith") || aWhenObject.getRelationWhen().equalsIgnoreCase("EndsWith") || aWhenObject.getRelationWhen().equalsIgnoreCase("Contains")) {
				if((aWhenObject.getFieldTextWhen()==null || aWhenObject.getFieldTextWhen().length()==0)){
					errorMessage = true;
					
					FacesMessage message = new FacesMessage("When line " + (i+1) + ": The Field/Value text field cannot be empty.");
					message.setSeverity(FacesMessage.SEVERITY_ERROR);
					FacesContext.getCurrentInstance().addMessage(null, message);
				}
			}
			else if(aWhenObject.getRelationWhen().equalsIgnoreCase("IsNull") || aWhenObject.getRelationWhen().equalsIgnoreCase("IsNotNull")) {
				
			}	
			else if(aWhenObject.getRelationWhen().equalsIgnoreCase("InTable")) {
				if((aWhenObject.getFieldTableWhen()==null || aWhenObject.getFieldTableWhen().length()==0)){
					errorMessage = true;
					
					FacesMessage message = new FacesMessage("When line " + (i+1) + ": The In Table combo box cannot be empty.");
					message.setSeverity(FacesMessage.SEVERITY_ERROR);
					FacesContext.getCurrentInstance().addMessage(null, message);
				}
			}
			else if(aWhenObject.getValuelocationWhen().equalsIgnoreCase("VALUE")){
				if((aWhenObject.getFieldTextWhen()==null || aWhenObject.getFieldTextWhen().length()==0)){
					errorMessage = true;
					
					FacesMessage message = new FacesMessage("When line " + (i+1) + ": The Field/Value text field cannot be empty.");
					message.setSeverity(FacesMessage.SEVERITY_ERROR);
					FacesContext.getCurrentInstance().addMessage(null, message);
				}
			}
			else{
				if((aWhenObject.getFieldColumnWhen()==null || aWhenObject.getFieldColumnWhen().length()==0)){
					errorMessage = true;
					
					FacesMessage message = new FacesMessage("When line " + (i+1) + ": The Field/Value combo box cannot be empty.");
					message.setSeverity(FacesMessage.SEVERITY_ERROR);
					FacesContext.getCurrentInstance().addMessage(null, message);
				}
			}
		}
		
		//Verify set
		SetObject aSetObject = null;
		for(int i=0; i<setObjectItems.size(); i++){
			aSetObject = (SetObject)setObjectItems.get(i);
			
			if(!aSetObject.getOperationSet().equalsIgnoreCase("SPROC") && (aSetObject.getFieldSet()==null || aSetObject.getFieldSet().length()==0)){
				errorMessage = true;
				
				FacesMessage message = new FacesMessage("Set line " + (i+1) + ": The Field/Value combo box cannot be empty.");
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage(null, message);
			}
			
			if(!aSetObject.getOperationSet().equalsIgnoreCase("SPROC") && !aSetObject.getValuelocationSet().equalsIgnoreCase("TABLE") && (aSetObject.getOperator()!=null &&  !aSetObject.getOperator().equalsIgnoreCase("n/a"))){
				if( aSetObject.getOperatorSet().equalsIgnoreCase("VALUE") && (aSetObject.getOperatorTextSet()==null || aSetObject.getOperatorTextSet().length()==0)){
					errorMessage = true;
					
					FacesMessage message = new FacesMessage("Set line " + (i+1) + ": The Field/Value text field cannot be empty.");
					message.setSeverity(FacesMessage.SEVERITY_ERROR);
					FacesContext.getCurrentInstance().addMessage(null, message);
				}
				
				if( !aSetObject.getOperatorSet().equalsIgnoreCase("VALUE") && (aSetObject.getOperatorColumnSet()==null || aSetObject.getOperatorColumnSet().length()==0)){
					errorMessage = true;
					
					FacesMessage message = new FacesMessage("Set line " + (i+1) + ": The Field/Value combo box cannot be empty.");
					message.setSeverity(FacesMessage.SEVERITY_ERROR);
					FacesContext.getCurrentInstance().addMessage(null, message);
				}
			}
			
			if(!aSetObject.getOperationSet().equalsIgnoreCase("SPROC") && aSetObject.getValuelocationSet().equalsIgnoreCase("VALUE")){
				if((aSetObject.getFieldTextSet()==null || aSetObject.getFieldTextSet().length()==0)){
					errorMessage = true;
					
					FacesMessage message = new FacesMessage("Set line " + (i+1) + ": The Field/Value text field cannot be empty.");
					message.setSeverity(FacesMessage.SEVERITY_ERROR);
					FacesContext.getCurrentInstance().addMessage(null, message);
				}
			}
			else if(aSetObject.getOperationSet().equalsIgnoreCase("SPROC")){
				if((aSetObject.getSprocName()==null || aSetObject.getSprocName().length()==0)){
					errorMessage = true;
					
					FacesMessage message = new FacesMessage("Set line " + (i+1) + ": The Sproc field cannot be empty.");
					message.setSeverity(FacesMessage.SEVERITY_ERROR);
					FacesContext.getCurrentInstance().addMessage(null, message);
				}
				if((aSetObject.getSprocParameters()==null || aSetObject.getSprocParameters().length()==0)){
					errorMessage = true;
					
					FacesMessage message = new FacesMessage("Set line " + (i+1) + ": The Sproc parameters field in ( ) cannot be empty.");
					message.setSeverity(FacesMessage.SEVERITY_ERROR);
					FacesContext.getCurrentInstance().addMessage(null, message);
				}
			}
			else if(aSetObject.getValuelocationSet().equalsIgnoreCase("TABLE")){
				if((aSetObject.getFieldTextSet()==null || aSetObject.getFieldTextSet().length()==0)){
					errorMessage = true;
					
					FacesMessage message = new FacesMessage("Set line " + (i+1) + ": The Field/Value text field cannot be empty.");
					message.setSeverity(FacesMessage.SEVERITY_ERROR);
					FacesContext.getCurrentInstance().addMessage(null, message);
				}
				if((aSetObject.getParamTextSet()==null || aSetObject.getParamTextSet().length()==0)){
					errorMessage = true;
					
					FacesMessage message = new FacesMessage("Set line " + (i+1) + ": The Table/Parameters cannot be empty.");
					message.setSeverity(FacesMessage.SEVERITY_ERROR);
					FacesContext.getCurrentInstance().addMessage(null, message);
				}
			}
			else{
				if((aSetObject.getFieldColumnSet()==null || aSetObject.getFieldColumnSet().length()==0)){
					errorMessage = true;
					
					FacesMessage message = new FacesMessage("Set line " + (i+1) + ": The Field/Value combo box cannot be empty.");
					message.setSeverity(FacesMessage.SEVERITY_ERROR);
					FacesContext.getCurrentInstance().addMessage(null, message);
				}
			}
		}
		
		return errorMessage;
	}
	
	public String copyaddProcessRuleAction() {
		return addProcessRuleAction();
	}
	
	public String addProcessRuleAction() {
		String result = null;
		try {
			if(validateRule()){
				return "";
			}
			
			if(validateWhenSet()){
				return "";
			}
			
			//Update CustLocn
			Procrule aProcrule = new Procrule();
			if (updateProcrule != null) {
				BeanUtils.copyProperties(updateProcrule, aProcrule);
			}
			
			List<ProcruleDetail> aProcruleEntityList = buildUpdateProcruleDetail();
			for(ProcruleDetail procruleDetail : aProcruleEntityList) {				
				procruleDetail.setActiveBooleanFlag(this.detailActiveBooleanFlag);	 
			}
			this.procruleService.addProcruleDetailList(aProcrule, aProcruleEntityList);

			//Refresh & Retrieve
			retrieveFilterProcrule();
			
		    result = "procrule_main";		
		} catch (Exception ex) {
			ex.printStackTrace();
			result = "";
		}
		   
        return result;
	}
	
	
	public String copyupdateProcessRuleAction() {
		String result = null;
		try {
			if(validateRule()){
				return "";
			}
			
			if(validateWhenSet()){
				return "";
			}
					
			Procrule aProcrule = new Procrule();
			if (updateProcrule != null) {
				BeanUtils.copyProperties(updateProcrule, aProcrule);
			}
			aProcrule.setProcruleId(this.selProcrule.getProcruleId());//For update
			aProcrule.setOrderNo(this.selProcrule.getOrderNo());//For update
			
			List<ProcruleDetail> aProcruleEntityList = buildUpdateProcruleDetail();
			for(ProcruleDetail procruleDetail : aProcruleEntityList) {				
				procruleDetail.setActiveBooleanFlag(this.detailActiveBooleanFlag);	 
			}
			this.procruleService.updateProcruleDetailList(aProcrule, aProcruleEntityList);

			//Refresh & Retrieve
			retrieveFilterProcrule();
			
		    result = "procrule_main";		
		} catch (Exception ex) {
			ex.printStackTrace();
			result = "";
		}
		   
        return result;
	}
	
	public String updateProcessRuleDetailAction() {
		String result = null;
		try {
			if(validateRule()){
				return "";
			}
			
			if(validateWhenSet()){
				return "";
			}
					
			Procrule aProcrule = new Procrule();
			if (updateProcrule != null) {
				BeanUtils.copyProperties(updateProcrule, aProcrule);
			}
			aProcrule.setProcruleId(this.selProcrule.getProcruleId());//For update
			aProcrule.setOrderNo(this.selProcrule.getOrderNo());//For update
			
			this.procruleService.update(aProcrule);
			
			List<ProcruleDetail> actualRowsList = procruleService.findProcruleDetail(aProcrule.getProcruleId(), effectiveDate);
			for(ProcruleDetail procruleDetail : actualRowsList) {				
				procruleDetail.setActiveBooleanFlag(this.detailActiveBooleanFlag);	 
			}
			this.procruleService.updateProcruleDetailList(aProcrule, actualRowsList);

			//Refresh & Retrieve
			retrieveFilterProcrule();
			
			//Refresh & Retrieve
			retrieveFilterProcrule();
			
		    result = "procrule_main";		
		} catch (Exception ex) {
			ex.printStackTrace();
			result = "";
		}
		   
        return result;
	}
	
	public String updateProcessRuleAction() {
		String result = null;
		try {
			Procrule aProcrule = new Procrule();
			if (updateProcrule != null) {
				BeanUtils.copyProperties(updateProcrule, aProcrule);
			}
			aProcrule.setProcruleId(this.selProcrule.getProcruleId());//For update
			aProcrule.setOrderNo(this.selProcrule.getOrderNo());//For update
			
			this.procruleService.update(aProcrule);

			//Refresh & Retrieve
			retrieveFilterProcrule();
			
		    result = "procrule_main";		
		} catch (Exception ex) {
			ex.printStackTrace();
			result = "";
		}
		   
        return result;
	}
	
	public Boolean getIsReorderProcessRuleEmpty() {
		if(reorderProcruleArray==null || reorderProcruleArray.size()==0){
	    	return true;
	    }
		else{
			return false;
		}	
	}
	
	public String reorderProcessRuleAction() {
		String result = null;
		try {
			if(reorderProcruleArray==null || reorderProcruleArray.size()==0){
		    	return null;//Impossible
		    }
		    
		    List<Procrule> updateProcruleList= new ArrayList<Procrule>();
		    Procrule aProcrule = null;
		    for(int i=0; i<reorderProcruleArray.size(); i++ ){
		    	aProcrule = (Procrule)reorderProcruleArray.get(i);
		    	aProcrule.setOrderNo(new Long(i+1));
		    	
		    	updateProcruleList.add(aProcrule);
		    }

			this.procruleService.reorderProcruleList(updateProcruleList);

			//Refresh & Retrieve
			retrieveFilterProcrule();
			
		    result = "procrule_main";		
		} catch (Exception ex) {
			ex.printStackTrace();
			result = "";
		}
		   
        return result;
	}
	
	public String deleteAllProcessRuleAction() {
		String result = null;
		logger.info("start addAction"); 
		try {
			Procrule aProcrule = new Procrule();
			if (updateProcrule != null) {
				BeanUtils.copyProperties(updateProcrule, aProcrule);
			}
			
			this.procruleService.deleteProcrule(aProcrule);

			//Refresh & Retrieve
			retrieveFilterProcrule();
			
			result = "procrule_main";	
		} catch (Exception ex) {
			ex.printStackTrace();
			result = "";
		}
		   
        return result;
	}
	
	public String deleteProcessRuleAction() {
		String result = null;
		try {
			Procrule aProcrule = new Procrule();
			if (updateProcrule != null) {
				BeanUtils.copyProperties(updateProcrule, aProcrule);
			}
			
			if(enableDefinitionDelete){			
				//if single effective date, then delete header also.
				List<String> distinctDateList = new ArrayList<String>();
				distinctDateList = procruleService.findProcruleDetailDistinctDate(aProcrule.getProcruleId());
				boolean deleteAll = false;
				if(distinctDateList!=null && distinctDateList.size()==1){		
					SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");	
					Date effectiveDate = null;
					try {
						effectiveDate = format.parse((String)distinctDateList.get(0));
					}
					catch(ParseException e){}
					
					if(this.effectiveDate!=null && effectiveDate!=null){
						Calendar cal1 = Calendar.getInstance();
			        	Calendar cal2 = Calendar.getInstance();
			        	cal1.setTime(this.effectiveDate);
			        	cal2.setTime(effectiveDate);
	
			        	if(cal1.equals(cal2)){
			        		deleteAll = true;
			        	}
					}
				}
				
				if(deleteAll){
					this.procruleService.deleteProcrule(aProcrule);
				}
				else{
				
					List<ProcruleDetail> aProcruleEntityList = buildUpdateProcruleDetail();
					this.procruleService.deleteProcruleDetail(aProcrule, aProcruleEntityList);
				}
			}
			else{
				List<ProcruleDetail> actualRowsList = new ArrayList<ProcruleDetail>();	
				actualRowsList = procruleService.findProcruleDetail(aProcrule.getProcruleId(), effectiveDate);
					
				for(ProcruleDetail procruleDetail : actualRowsList) {				
					procruleDetail.setActiveBooleanFlag(false);	 
				}
				
				this.procruleService.updateProcruleDetailList(aProcrule, actualRowsList);
			}

			//Refresh & Retrieve
			retrieveFilterProcrule();
			
			result = "procrule_main";	
		} catch (Exception ex) {
			ex.printStackTrace();
			result = "";
		}
		   
        return result;
	}
	
	public String cancelAction() {
		return "procrule_main";
	}
	
	public Boolean getIsViewAction() {
		return currentAction.equals(EditAction.VIEW);
	}
	public Boolean getIsUpdateAction() {
		return currentAction.equals(EditAction.UPDATE);
	}
	
	public Boolean getIsUpdateProcRuleDetailAction() {
		return currentAction.equals(EditAction.UPDATE_PROC_DETAILS);
	}
	
	private boolean enableProcruleDelete = false;
	private boolean enableDefinitionDelete = false;

	public Boolean getDisplayProcruleDeleteButtons() {
		if(selectedProcruleRowIndex!=-1 && enableProcruleDelete){
			return true;
		}
		else{
			return false;
		}
	}
	
	public Boolean getDisplayProcruleUpdateButtons() {
		if(selectedProcruleRowIndex!=-1){
			return true;
		}
		else{
			return false;
		}
	}
	
	public Boolean getEnableDefinitionDelete() {
		return enableDefinitionDelete;
	}
	
	public Boolean getDisplayProcruleDetailButtons() {
		return ((selectedProcruleRowIndex!=-1) && (this.selectedEffectiveDate.length()>0));
	}
	
	private String isCustomerInformationOpen = "true";
	private String isLocationInformationOpen = "false";
	private Boolean isHideSearchPanel = false;
	
	public Boolean getIsHideSearchPanel() {
		return isHideSearchPanel;
	}
	
	public void toggleHideSearchPanel(ActionEvent event){
		isHideSearchPanel = !isHideSearchPanel;
	}
	
	public String toggleHideSearchPanel(){
		isHideSearchPanel = !isHideSearchPanel;
		return null;
	}
	
	public String getIsCustomerInformationOpen() {
		return isCustomerInformationOpen;
	}
	
	public void toggleCollapseAll(ActionEvent event) {
		isCustomerInformationOpen = "false";
		isLocationInformationOpen = "false";
	}
	
	public String getIsLocationInformationOpen() {
		return isLocationInformationOpen;
	}
	
	public void toggleExpandAll(ActionEvent event) {
		isCustomerInformationOpen = "true";
		isLocationInformationOpen = "true";
	}
	
	public void toggleCustomerInformation(ActionEvent event) {
		if(Boolean.valueOf(isCustomerInformationOpen)){
			isCustomerInformationOpen = "false";	
		}
		else{
			isCustomerInformationOpen = "true";
		}
	}
	
	public void toggleLocationInformation(ActionEvent event) {
		if(Boolean.valueOf(isLocationInformationOpen)){
			isLocationInformationOpen = "false";	
		}
		else{
			isLocationInformationOpen = "true";
		}
	}

	public void oncollapseCustomerInformation(ActionEvent e) throws AbortProcessingException {
		isCustomerInformationOpen = "false";
	}
	
	public void onexpandCustomerInformation(ActionEvent e) throws AbortProcessingException {
		isCustomerInformationOpen = "true";
	}
	
	public Boolean getDisplayCustLocnButtons() {
		return ((selectedProcruleRowIndex != -1) && (selectedProcruleDetailRowIndex != -1));
	}
	
	public String resetFilterSearchAction(){
		filterProcrule = new Procrule();
			
		return null;
	}

	public CacheManager getCacheManager() {
		return this.cacheManager;
	}

	public void setCacheManager(CacheManager cacheManager) {
		this.cacheManager = cacheManager;
	}   

	public Procrule getFilterProcrule() {
		return filterProcrule;
	}

	public void setFilterProcrule(Procrule filterProcrule) {
		this.filterProcrule = filterProcrule;
	}

	public void setSelectedProcruleRowIndex(int selectedProcruleRowIndex) {
		this.selectedProcruleRowIndex = selectedProcruleRowIndex;
	}
	
	public int getSelectedProcruleDetailRowIndex() {
		return selectedProcruleDetailRowIndex;
	}

	public void setSelectedProcruleDetailRowIndex(int selectedProcruleDetailRowIndex) {
		this.selectedProcruleDetailRowIndex = selectedProcruleDetailRowIndex;
	}
	
	public void sortAction(ActionEvent e) {
		procruleDataModel.toggleSortOrder(e.getComponent().getParent().getId());
	}

	public List<Procrule> getReorderProcruleArray() {
		 return reorderProcruleArray;
	}
	
	public List<WhenObject> getWhenObjectItems() {
	 	return whenObjectItems;
	}
	
	public List<SetObject> getSetObjectItems() {
	 	return setObjectItems;
	}
	
	public List<SelectObject> getInSelectObject() {
	 	return inSelectObject;
	}
	public List<SelectObject> getOutSelectObject() {
	 	return outSelectObject;
	}
	
	private boolean currentPurchasingSelected = false;
	
	public String navigateAction() {
		if(getIsPurchasingMenu() != currentPurchasingSelected){
			currentPurchasingSelected = getIsPurchasingMenu();
			
			//Need refresh
			resetFilterSearchAction();
			retrieveFilterProcrule();
		}

		return "procrule_main";
	}
	
	public boolean getIsPurchasingMenu(){
		return loginBean.getIsPurchasingSelected();
	}
	
	public void ruleTypeChanged(ActionEvent e) {
		updateProcrule.setReprocessCode("*NONE");
	}
	
	public void reprocessCodeChanged(ActionEvent e) {
	}
	
	public User getCurrentUser() {
		return matrixCommonBean.getLoginBean().getUser();
   }

	public String getCurrentModuleMenu(){
		if(getIsPurchasingMenu()){
			return "PURCHUSE";
		}
		else{
			return "BILLSALE";
		}
	}
	
	public List<SelectItem> getAllSaleTransItems() {
		if(getIsPurchasingMenu()){
			if(allTransactionItems==null){
				
				Map<String,DataDefinitionColumn> propertyMap = cacheManager.getDataDefinitionPropertyMap("TB_PURCHTRANS");
				List<SelectItem> selectItems = new ArrayList<SelectItem>();
		    	selectItems.add(new SelectItem("","Select a Field"));
		    	
				for (String transProperty : propertyMap.keySet()) {
					DataDefinitionColumn driverNames = propertyMap.get(transProperty);
					selectItems.add(new SelectItem(driverNames.getColumnName(),driverNames.getDescription()));
				}

				allTransactionItems = selectItems;
			}
			return allTransactionItems;
		}
		else{
			if(allSaleTransItems==null){
				
				Map<String,DataDefinitionColumn> propertyMap = cacheManager.getDataDefinitionPropertyMap("TB_BILLTRANS");
				List<SelectItem> selectItems = new ArrayList<SelectItem>();
		    	selectItems.add(new SelectItem("","Select a Field"));
		    	
				for (String transProperty : propertyMap.keySet()) {
					DataDefinitionColumn driverNames = propertyMap.get(transProperty);
					selectItems.add(new SelectItem(driverNames.getColumnName(),driverNames.getDescription()));
				}

				allSaleTransItems = selectItems;
			}
			return allSaleTransItems;
		}
	}
	
	//All columns
	public List<SelectItem> getAllSaleTransNumberItems() {
		if(getIsPurchasingMenu()){
			if(allTransactionNumberItems==null){
				Map<String,DataDefinitionColumn> propertyMap = cacheManager.getDataDefinitionPropertyMap("TB_PURCHTRANS");
				List<SelectItem> selectItems = new ArrayList<SelectItem>();
		    	selectItems.add(new SelectItem("","Select a Field"));
		    	
		    	for (String transProperty : propertyMap.keySet()) {
		    		DataDefinitionColumn driverNames = propertyMap.get(transProperty);
		    		if(driverNames!=null && isNumberSalesColumnType(driverNames.getColumnName())){ //Number column type
		    			selectItems.add(new SelectItem(driverNames.getColumnName(),driverNames.getDescription()));
		    		}
		    	}

		    	allTransactionNumberItems = selectItems;
			}
			return allTransactionNumberItems;
		}
		else{
			if(allSaleTransNumberItems==null){
				Map<String,DataDefinitionColumn> propertyMap = cacheManager.getDataDefinitionPropertyMap("TB_BILLTRANS");
				List<SelectItem> selectItems = new ArrayList<SelectItem>();
		    	selectItems.add(new SelectItem("","Select a Field"));
		    	
		    	for (String transProperty : propertyMap.keySet()) {
		    		DataDefinitionColumn driverNames = propertyMap.get(transProperty);
		    		if(driverNames!=null && isNumberSalesColumnType(driverNames.getColumnName())){ //Number column type
		    			selectItems.add(new SelectItem(driverNames.getColumnName(),driverNames.getDescription()));
		    		}
		    	}

		    	allSaleTransNumberItems = selectItems;
			}
			return allSaleTransNumberItems;
		}
	}
	
	public List<SelectItem> getAllSaleTransTextItems() {
		if(getIsPurchasingMenu()){
			if(allTransactionTextItems==null){
				Map<String,DataDefinitionColumn> propertyMap = cacheManager.getDataDefinitionPropertyMap("TB_PURCHTRANS");
				List<SelectItem> selectItems = new ArrayList<SelectItem>();
		    	selectItems.add(new SelectItem("","Select a Field"));
		    	
		    	for (String transProperty : propertyMap.keySet()) {
		    		DataDefinitionColumn driverNames = propertyMap.get(transProperty);
		    		if(driverNames!=null && isTextSalesColumnType(driverNames.getColumnName())){ //Text column type
		    			selectItems.add(new SelectItem(driverNames.getColumnName(),driverNames.getDescription()));
		    		}
		    	}

		    	allTransactionTextItems = selectItems;
			}
			return allTransactionTextItems;
		}
		else{
			if(allSaleTransTextItems==null){
				Map<String,DataDefinitionColumn> propertyMap = cacheManager.getDataDefinitionPropertyMap("TB_BILLTRANS");
				List<SelectItem> selectItems = new ArrayList<SelectItem>();
		    	selectItems.add(new SelectItem("","Select a Field"));
		    	
		    	for (String transProperty : propertyMap.keySet()) {
		    		DataDefinitionColumn driverNames = propertyMap.get(transProperty);
		    		if(driverNames!=null && isTextSalesColumnType(driverNames.getColumnName())){ //Text column type
		    			selectItems.add(new SelectItem(driverNames.getColumnName(),driverNames.getDescription()));
		    		}
		    	}

		    	allSaleTransTextItems = selectItems;
			}
			return allSaleTransTextItems;
		}
	}
	
	public List<SelectItem> getAllSaleTransDateItems() {
		if(getIsPurchasingMenu()){
			if(allTransactionDateItems==null){
				Map<String,DataDefinitionColumn> propertyMap = cacheManager.getDataDefinitionPropertyMap("TB_PURCHTRANS");
				List<SelectItem> selectItems = new ArrayList<SelectItem>();
		    	selectItems.add(new SelectItem("","Select a Field"));
		    	
		    	for (String transProperty : propertyMap.keySet()) {
		    		DataDefinitionColumn driverNames = propertyMap.get(transProperty);
		    		if(driverNames!=null && isDateSalesColumnType(driverNames.getColumnName())){ //Date column type
		    			selectItems.add(new SelectItem(driverNames.getColumnName(),driverNames.getDescription()));
		    		}
		    	}

		    	allTransactionDateItems = selectItems;
			}
			return allTransactionDateItems;
		}
		else{
			if(allSaleTransDateItems==null){
				Map<String,DataDefinitionColumn> propertyMap = cacheManager.getDataDefinitionPropertyMap("TB_BILLTRANS");
				List<SelectItem> selectItems = new ArrayList<SelectItem>();
		    	selectItems.add(new SelectItem("","Select a Field"));
		    	
		    	for (String transProperty : propertyMap.keySet()) {
		    		DataDefinitionColumn driverNames = propertyMap.get(transProperty);
		    		if(driverNames!=null && isDateSalesColumnType(driverNames.getColumnName())){ //Date column type
		    			selectItems.add(new SelectItem(driverNames.getColumnName(),driverNames.getDescription()));
		    		}
		    	}

		    	allSaleTransDateItems = selectItems;
			}
			return allSaleTransDateItems;
		}
	}
	
	public static String[] TB_TRANSACTION_DETAIL_OTHER_COLUMNS = {
			"ALLOCATION_MATRIX_ID",
			"ALLOCATION_SUBTRANS_ID",
			"ARCHIVE_BATCH_NO",
			"AUTO_TRANSACTION_COUNTRY_CODE",
			"AUTO_TRANSACTION_STATE_CODE",
			"CITY_DEFAULT_FLAG",
			"CITY_LOCAL_USE_AMOUNT",
			"CITY_LOCAL_USE_RATE",
			"CITY_SINGLE_FLAG",
			"CITY_SPLIT_AMOUNT",
			"CITY_SPLIT_USE_RATE",
			"CITY_TAXABLE_AMT",
			"CITY_TAXCODE_DETAIL_ID",
			"CITY_TAXCODE_TYPE_CODE",
			"CITY_USE_AMOUNT",
			"CITY_USE_RATE",
			"COMBINED_USE_RATE",
			"COUNTRY_USE_AMOUNT",
			"COUNTRY_USE_RATE",
			"COUNTY_DEFAULT_FLAG",
			"COUNTY_LOCAL_USE_AMOUNT",
			"COUNTY_LOCAL_USE_RATE",
			"COUNTY_MAXTAX_AMOUNT",
			"COUNTY_SINGLE_FLAG",
			"COUNTY_SPLIT_AMOUNT",
			"COUNTY_TAXABLE_AMT",
			"COUNTY_TAXCODE_DETAIL_ID",
			"COUNTY_TAXCODE_TYPE_CODE",
			"COUNTY_USE_AMOUNT",
			"COUNTY_USE_RATE",
			"ENTITY_CODE",
			"GL_EXTRACT_AMT",
			"GL_EXTRACT_BATCH_NO",
			"GL_EXTRACT_FLAG",
			"GL_EXTRACT_TIMESTAMP",
			"GL_EXTRACT_UPDATER",
			"GL_LOG_FLAG",
			"JURISDICTION_ID",
			"JURISDICTION_TAXRATE_ID",
			"LOAD_TIMESTAMP",
			"LOCATION_MATRIX_ID",
			"MANUAL_JURISDICTION_IND",
			"MANUAL_TAXCODE_IND",
			"MEASURE_TYPE_CODE",
			"MODIFY_TIMESTAMP",
			"MODIFY_USER_ID",
			"MULTI_TRANS_CODE",
			"ORIG_GL_LINE_ITM_DIST_AMT",
			"PROCESS_BATCH_NO",
			"SPLIT_SUBTRANS_ID",
			"STATE_MAXTAX_AMOUNT",
			"STATE_SPLIT_AMOUNT",
			"STATE_TAXABLE_AMT",
			"STATE_TAXCODE_DETAIL_ID",
			"STATE_TAXCODE_TYPE_CODE",
			"STATE_TIER2_MAX_AMOUNT",
			"STATE_TIER2_MIN_AMOUNT",
			"STATE_USE_AMOUNT",
			"STATE_USE_RATE",
			"STATE_USE_TIER2_AMOUNT",
			"STATE_USE_TIER2_RATE",
			"STATE_USE_TIER3_AMOUNT",
			"STATE_USE_TIER3_RATE",
			"STJ1_AMOUNT",
			"STJ1_RATE",
			"STJ2_AMOUNT",
			"STJ2_RATE",
			"STJ3_AMOUNT",
			"STJ3_RATE",
			"STJ4_AMOUNT",
			"STJ4_RATE",
			"STJ5_AMOUNT",
			"STJ5_RATE",
			"SUSPEND_IND",
			"TAX_ALLOC_MATRIX_ID",
			"TAX_MATRIX_ID",
			"TAXCODE_COUNTRY_CODE",
			"TAXCODE_DETAIL_ID",
			"TAXCODE_STATE_CODE",
			"TAXCODE_TYPE_CODE",
			"TAXTYPE_USED_CODE",
			"TB_CALC_TAX_AMT",
			"TRANSACTION_DETAIL_ID",
			"TRANSACTION_IND",
			"UPDATE_TIMESTAMP",
			"UPDATE_USER_ID"
	};
	
	public List<SelectItem> getOtherTransactionItems() {
		if(otherTransactionItems==null){
			Map<String,DataDefinitionColumn> propertyMap = cacheManager.getDataDefinitionPropertyMap("TB_PURCHTRANS");
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
	    	selectItems.add(new SelectItem("","Select a Field"));
	    	
	    	for(int i=0; i<TB_TRANSACTION_DETAIL_OTHER_COLUMNS.length; i++){
	    		DataDefinitionColumn driverNames = propertyMap.get(Util.columnToProperty(TB_TRANSACTION_DETAIL_OTHER_COLUMNS[i]));
	    		if(driverNames!=null){
	    			selectItems.add(new SelectItem(driverNames.getColumnName(),driverNames.getDescription()));
	    		}
	    	}

	    	otherTransactionItems = selectItems;
		}
		return otherTransactionItems;
	}
	
	public List<SelectItem> getOtherTransactionNumberItems() {
		if(otherTransactionNumberItems==null){
			Map<String,DataDefinitionColumn> propertyMap = cacheManager.getDataDefinitionPropertyMap("TB_PURCHTRANS");
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
	    	selectItems.add(new SelectItem("","Select a Field"));
	    	
	    	for(int i=0; i<TB_TRANSACTION_DETAIL_OTHER_COLUMNS.length; i++){
	    		DataDefinitionColumn driverNames = propertyMap.get(Util.columnToProperty(TB_TRANSACTION_DETAIL_OTHER_COLUMNS[i]));
	    		if(driverNames!=null && isNumberSalesColumnType(driverNames.getColumnName())){ //Number column type
	    			selectItems.add(new SelectItem(driverNames.getColumnName(),driverNames.getDescription()));
	    		}
	    	}

	    	otherTransactionNumberItems = selectItems;
		}
		return otherTransactionNumberItems;	
	}
	
	public List<SelectItem> getOtherTransactionTextItems() {
		if(otherTransactionTextItems==null){
			Map<String,DataDefinitionColumn> propertyMap = cacheManager.getDataDefinitionPropertyMap("TB_PURCHTRANS");
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
	    	selectItems.add(new SelectItem("","Select a Field"));
	    	
	    	for(int i=0; i<TB_TRANSACTION_DETAIL_OTHER_COLUMNS.length; i++){
	    		DataDefinitionColumn driverNames = propertyMap.get(Util.columnToProperty(TB_TRANSACTION_DETAIL_OTHER_COLUMNS[i]));
	    		if(driverNames!=null && isTextSalesColumnType(driverNames.getColumnName())){ //Text column type
	    			selectItems.add(new SelectItem(driverNames.getColumnName(),driverNames.getDescription()));
	    		}
	    	}

	    	otherTransactionTextItems = selectItems;
		}
		return otherTransactionTextItems;
	}
	
	public List<SelectItem> getOtherTransactionDateItems() {
		if(otherTransactionDateItems==null){
			Map<String,DataDefinitionColumn> propertyMap = cacheManager.getDataDefinitionPropertyMap("TB_PURCHTRANS");
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
	    	selectItems.add(new SelectItem("","Select a Field"));
	    	
	    	for(int i=0; i<TB_TRANSACTION_DETAIL_OTHER_COLUMNS.length; i++){
	    		DataDefinitionColumn driverNames = propertyMap.get(Util.columnToProperty(TB_TRANSACTION_DETAIL_OTHER_COLUMNS[i]));
	    		if(driverNames!=null && isDateSalesColumnType(driverNames.getColumnName())){ //Date column type
	    			selectItems.add(new SelectItem(driverNames.getColumnName(),driverNames.getDescription()));
	    		}
	    	}

	    	otherTransactionDateItems = selectItems;
		}
		return otherTransactionDateItems;
	}
	
	public static String[] TB_BILLTRANS_BASIC_COLUMNS = { 
			"TRANSACTION_TYPE","RATETYPE","METHOD_DELIVERY","GL_DATE","COMMENTS","CREDIT_IND","EXEMPT_IND","EXEMPT_REASON",
			"TAX_PAID_TO_VENDOR_FLAG","DO_NOT_CALC_FLAG","VENDOR_TAX_AMT","GEO_RECON_CODE","DISCOUNT_TYPE"
	};
	
	public List<SelectItem> getBasicSaleTransItems() {
		if(basicSaleTransItems==null){
			Map<String,DataDefinitionColumn> propertyMap = cacheManager.getDataDefinitionPropertyMap("TB_BILLTRANS");
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
	    	selectItems.add(new SelectItem("","Select a Field"));
	    	
	    	for(int i=0; i<TB_BILLTRANS_BASIC_COLUMNS.length; i++){
	    		DataDefinitionColumn driverNames = propertyMap.get(Util.columnToProperty(TB_BILLTRANS_BASIC_COLUMNS[i]));
	    		if(driverNames!=null){
	    			selectItems.add(new SelectItem(driverNames.getColumnName(),driverNames.getDescription()));
	    		}
	    	}

	    	basicSaleTransItems = selectItems;
		}
		return basicSaleTransItems;
	}
	
	public List<SelectItem> getBasicSaleTransNumberItems() {		
		if(basicSaleTransNumberItems==null){
			Map<String,DataDefinitionColumn> propertyMap = cacheManager.getDataDefinitionPropertyMap("TB_BILLTRANS");
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
	    	selectItems.add(new SelectItem("","Select a Field"));
	    	
	    	for(int i=0; i<TB_BILLTRANS_BASIC_COLUMNS.length; i++){
	    		DataDefinitionColumn driverNames = propertyMap.get(Util.columnToProperty(TB_BILLTRANS_BASIC_COLUMNS[i]));
	    		if(driverNames!=null && isNumberSalesColumnType(driverNames.getColumnName())){ //Number column type
	    			selectItems.add(new SelectItem(driverNames.getColumnName(),driverNames.getDescription()));
	    		}
	    	}

	    	basicSaleTransNumberItems = selectItems;
		}
		return basicSaleTransNumberItems;
	}
	
	public List<SelectItem> getBasicSaleTransTextItems() {
		if(basicSaleTransTextItems==null){
			Map<String,DataDefinitionColumn> propertyMap = cacheManager.getDataDefinitionPropertyMap("TB_BILLTRANS");
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
	    	selectItems.add(new SelectItem("","Select a Field"));
	    	
	    	for(int i=0; i<TB_BILLTRANS_BASIC_COLUMNS.length; i++){
	    		DataDefinitionColumn driverNames = propertyMap.get(Util.columnToProperty(TB_BILLTRANS_BASIC_COLUMNS[i]));
	    		if(driverNames!=null && isTextSalesColumnType(driverNames.getColumnName())){ //Text column type
	    			selectItems.add(new SelectItem(driverNames.getColumnName(),driverNames.getDescription()));
	    		}
	    	}

	    	basicSaleTransTextItems = selectItems;
		}
		return basicSaleTransTextItems;
	}
	
	public List<SelectItem> getBasicSaleTransDateItems() {
		if(basicSaleTransDateItems==null){
			Map<String,DataDefinitionColumn> propertyMap = cacheManager.getDataDefinitionPropertyMap("TB_BILLTRANS");
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
	    	selectItems.add(new SelectItem("","Select a Field"));
	    	
	    	for(int i=0; i<TB_BILLTRANS_BASIC_COLUMNS.length; i++){
	    		DataDefinitionColumn driverNames = propertyMap.get(Util.columnToProperty(TB_BILLTRANS_BASIC_COLUMNS[i]));
	    		if(driverNames!=null && isDateSalesColumnType(driverNames.getColumnName())){ //Date column type
	    			selectItems.add(new SelectItem(driverNames.getColumnName(),driverNames.getDescription()));
	    		}
	    	}

	    	basicSaleTransDateItems = selectItems;
		}
		return basicSaleTransDateItems;
	}
	
	public static String[] TB_BILLTRANS_TRANSACTION = { 
		"ENTERED_DATE",
		"TRANSACTION_TYPE",
		"RATETYPE",
		"METHOD_DELIVERY",
		"COMMENTS",
		"CREDIT_IND",
		"GL_DATE",
		"EXEMPT_IND",
		"EXEMPT_REASON",
		"TAX_PAID_TO_VENDOR_FLAG",
		"DO_NOT_CALC_FLAG",
		"VENDOR_TAX_AMT",
		"GEO_RECON_CODE",
		"DISCOUNT_TYPE",
		"PROCESS_METHOD",
		"TRANSACTION_TYPE_CODE",
		"RATETYPE_CODE",
		"METHOD_DELIVERY_CODE",
		"EXEMPT_CODE",
		"FREIGHT_ITEM_FLAG",
		"DISCOUNT_ITEM_FLAG",
		"TAXCODE_CODE"
	};
	
	public List<SelectItem> getTransactionSaleTransItems() {
		if(transactionSaleTransItems==null){
			Map<String,DataDefinitionColumn> propertyMap = cacheManager.getDataDefinitionPropertyMap("TB_BILLTRANS");
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
	    	selectItems.add(new SelectItem("","Select a Field"));
	    	
	    	for(int i=0; i<TB_BILLTRANS_TRANSACTION.length; i++){
	    		DataDefinitionColumn driverNames = propertyMap.get(Util.columnToProperty(TB_BILLTRANS_TRANSACTION[i]));
	    		if(driverNames!=null){
	    			selectItems.add(new SelectItem(driverNames.getColumnName(),driverNames.getDescription()));
	    		}
	    	}
	
	    	transactionSaleTransItems = selectItems;
		}
		return transactionSaleTransItems;
	}
	
	public List<SelectItem> getTransactionSaleTransNumberItems() {		
		if(transactionSaleTransNumberItems==null){
			Map<String,DataDefinitionColumn> propertyMap = cacheManager.getDataDefinitionPropertyMap("TB_BILLTRANS");
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
	    	selectItems.add(new SelectItem("","Select a Field"));
	    	
	    	for(int i=0; i<TB_BILLTRANS_TRANSACTION.length; i++){
	    		DataDefinitionColumn driverNames = propertyMap.get(Util.columnToProperty(TB_BILLTRANS_TRANSACTION[i]));
	    		if(driverNames!=null && isNumberSalesColumnType(driverNames.getColumnName())){ //Number column type
	    			selectItems.add(new SelectItem(driverNames.getColumnName(),driverNames.getDescription()));
	    		}
	    	}
	
	    	transactionSaleTransNumberItems = selectItems;
		}
		return transactionSaleTransNumberItems;
	}
	
	public List<SelectItem> getTransactionSaleTransTextItems() {
		if(transactionSaleTransTextItems==null){
			Map<String,DataDefinitionColumn> propertyMap = cacheManager.getDataDefinitionPropertyMap("TB_BILLTRANS");
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
	    	selectItems.add(new SelectItem("","Select a Field"));
	    	
	    	for(int i=0; i<TB_BILLTRANS_TRANSACTION.length; i++){
	    		DataDefinitionColumn driverNames = propertyMap.get(Util.columnToProperty(TB_BILLTRANS_TRANSACTION[i]));
	    		if(driverNames!=null && isTextSalesColumnType(driverNames.getColumnName())){ //Text column type
	    			selectItems.add(new SelectItem(driverNames.getColumnName(),driverNames.getDescription()));
	    		}
	    	}
	
	    	transactionSaleTransTextItems = selectItems;
		}
		return transactionSaleTransTextItems;
	}
	
	public List<SelectItem> getTransactionSaleTransDateItems() {
		if(transactionSaleTransDateItems==null){
			Map<String,DataDefinitionColumn> propertyMap = cacheManager.getDataDefinitionPropertyMap("TB_BILLTRANS");
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
	    	selectItems.add(new SelectItem("","Select a Field"));
	    	
	    	for(int i=0; i<TB_BILLTRANS_TRANSACTION.length; i++){
	    		DataDefinitionColumn driverNames = propertyMap.get(Util.columnToProperty(TB_BILLTRANS_TRANSACTION[i]));
	    		if(driverNames!=null && isDateSalesColumnType(driverNames.getColumnName())){ //Date column type
	    			selectItems.add(new SelectItem(driverNames.getColumnName(),driverNames.getDescription()));
	    		}
	    	}
	
	    	transactionSaleTransDateItems = selectItems;
		}
		return transactionSaleTransDateItems;
	}
	
	public static String[] TB_BILLTRANS_SITUS = { 
		"COUNTRY_CODE",
		"STATE_CODE",
		"COUNTY_NAME",
		"CITY_NAME",
		"STJ1_NAME",
		"STJ2_NAME",
		"STJ3_NAME",
		"STJ4_NAME",
		"STJ5_NAME",
		"STJ6_NAME",
		"STJ7_NAME",
		"STJ8_NAME",
		"STJ9_NAME",
		"STJ10_NAME"
	};
	
	public List<SelectItem> getSitusSaleTransItems() {
		if(situsSaleTransItems==null){
			Map<String,DataDefinitionColumn> propertyMap = cacheManager.getDataDefinitionPropertyMap("TB_BILLTRANS");
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
	    	selectItems.add(new SelectItem("","Select a Field"));
	    	
	    	for(int i=0; i<TB_BILLTRANS_SITUS.length; i++){
	    		DataDefinitionColumn driverNames = propertyMap.get(Util.columnToProperty(TB_BILLTRANS_SITUS[i]));
	    		if(driverNames!=null){
	    			selectItems.add(new SelectItem(driverNames.getColumnName(),driverNames.getDescription()));
	    		}
	    	}
	
	    	situsSaleTransItems = selectItems;
		}
		return situsSaleTransItems;
	}
	
	public List<SelectItem> getSitusSaleTransNumberItems() {		
		if(situsSaleTransNumberItems==null){
			Map<String,DataDefinitionColumn> propertyMap = cacheManager.getDataDefinitionPropertyMap("TB_BILLTRANS");
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
	    	selectItems.add(new SelectItem("","Select a Field"));
	    	
	    	for(int i=0; i<TB_BILLTRANS_SITUS.length; i++){
	    		DataDefinitionColumn driverNames = propertyMap.get(Util.columnToProperty(TB_BILLTRANS_SITUS[i]));
	    		if(driverNames!=null && isNumberSalesColumnType(driverNames.getColumnName())){ //Number column type
	    			selectItems.add(new SelectItem(driverNames.getColumnName(),driverNames.getDescription()));
	    		}
	    	}
	
	    	situsSaleTransNumberItems = selectItems;
		}
		return situsSaleTransNumberItems;
	}
	
	public List<SelectItem> getSitusSaleTransTextItems() {
		if(situsSaleTransTextItems==null){
			Map<String,DataDefinitionColumn> propertyMap = cacheManager.getDataDefinitionPropertyMap("TB_BILLTRANS");
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
	    	selectItems.add(new SelectItem("","Select a Field"));
	    	
	    	for(int i=0; i<TB_BILLTRANS_SITUS.length; i++){
	    		DataDefinitionColumn driverNames = propertyMap.get(Util.columnToProperty(TB_BILLTRANS_SITUS[i]));
	    		if(driverNames!=null && isTextSalesColumnType(driverNames.getColumnName())){ //Text column type
	    			selectItems.add(new SelectItem(driverNames.getColumnName(),driverNames.getDescription()));
	    		}
	    	}
	
	    	situsSaleTransTextItems = selectItems;
		}
		return situsSaleTransTextItems;
	}
	
	public List<SelectItem> getSitusSaleTransDateItems() {
		if(situsSaleTransDateItems==null){
			Map<String,DataDefinitionColumn> propertyMap = cacheManager.getDataDefinitionPropertyMap("TB_BILLTRANS");
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
	    	selectItems.add(new SelectItem("","Select a Field"));
	    	
	    	for(int i=0; i<TB_BILLTRANS_SITUS.length; i++){
	    		DataDefinitionColumn driverNames = propertyMap.get(Util.columnToProperty(TB_BILLTRANS_SITUS[i]));
	    		if(driverNames!=null && isDateSalesColumnType(driverNames.getColumnName())){ //Date column type
	    			selectItems.add(new SelectItem(driverNames.getColumnName(),driverNames.getDescription()));
	    		}
	    	}
	
	    	situsSaleTransDateItems = selectItems;
		}
		return situsSaleTransDateItems;
	}
	
	public static String[] TB_BILLTRANS_AMOUNTS = { 
		"EXEMPT_AMT",
		"EXCLUSION_AMT",
		"TAXABLE_AMT",
		"TAX_AMT"
	};
	
	public List<SelectItem> getAmountsSaleTransItems() {
		if(amountsSaleTransItems==null){
			Map<String,DataDefinitionColumn> propertyMap = cacheManager.getDataDefinitionPropertyMap("TB_BILLTRANS");
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
	    	selectItems.add(new SelectItem("","Select a Field"));
	    	
	    	for(int i=0; i<TB_BILLTRANS_AMOUNTS.length; i++){
	    		DataDefinitionColumn driverNames = propertyMap.get(Util.columnToProperty(TB_BILLTRANS_AMOUNTS[i]));
	    		if(driverNames!=null){
	    			selectItems.add(new SelectItem(driverNames.getColumnName(),driverNames.getDescription()));
	    		}
	    	}
	
	    	amountsSaleTransItems = selectItems;
		}
		return amountsSaleTransItems;
	}
	
	public List<SelectItem> getAmountsSaleTransNumberItems() {		
		if(amountsSaleTransNumberItems==null){
			Map<String,DataDefinitionColumn> propertyMap = cacheManager.getDataDefinitionPropertyMap("TB_BILLTRANS");
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
	    	selectItems.add(new SelectItem("","Select a Field"));
	    	
	    	for(int i=0; i<TB_BILLTRANS_AMOUNTS.length; i++){
	    		DataDefinitionColumn driverNames = propertyMap.get(Util.columnToProperty(TB_BILLTRANS_AMOUNTS[i]));
	    		if(driverNames!=null && isNumberSalesColumnType(driverNames.getColumnName())){ //Number column type
	    			selectItems.add(new SelectItem(driverNames.getColumnName(),driverNames.getDescription()));
	    		}
	    	}
	
	    	amountsSaleTransNumberItems = selectItems;
		}
		return amountsSaleTransNumberItems;
	}
	
	public List<SelectItem> getAmountsSaleTransTextItems() {
		if(amountsSaleTransTextItems==null){
			Map<String,DataDefinitionColumn> propertyMap = cacheManager.getDataDefinitionPropertyMap("TB_BILLTRANS");
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
	    	selectItems.add(new SelectItem("","Select a Field"));
	    	
	    	for(int i=0; i<TB_BILLTRANS_AMOUNTS.length; i++){
	    		DataDefinitionColumn driverNames = propertyMap.get(Util.columnToProperty(TB_BILLTRANS_AMOUNTS[i]));
	    		if(driverNames!=null && isTextSalesColumnType(driverNames.getColumnName())){ //Text column type
	    			selectItems.add(new SelectItem(driverNames.getColumnName(),driverNames.getDescription()));
	    		}
	    	}
	
	    	amountsSaleTransTextItems = selectItems;
		}
		return amountsSaleTransTextItems;
	}
	
	public List<SelectItem> getAmountsSaleTransDateItems() {
		if(amountsSaleTransDateItems==null){
			Map<String,DataDefinitionColumn> propertyMap = cacheManager.getDataDefinitionPropertyMap("TB_BILLTRANS");
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
	    	selectItems.add(new SelectItem("","Select a Field"));
	    	
	    	for(int i=0; i<TB_BILLTRANS_AMOUNTS.length; i++){
	    		DataDefinitionColumn driverNames = propertyMap.get(Util.columnToProperty(TB_BILLTRANS_AMOUNTS[i]));
	    		if(driverNames!=null && isDateSalesColumnType(driverNames.getColumnName())){ //Date column type
	    			selectItems.add(new SelectItem(driverNames.getColumnName(),driverNames.getDescription()));
	    		}
	    	}
	
	    	amountsSaleTransDateItems = selectItems;
		}
		return amountsSaleTransDateItems;
	}
	
	public static String[] TB_BILLTRANS_GENERALLEDGER = {
		"GL_PROFIT_CENTER_NBR",
		"GL_PROFIT_CENTER_NAME",
		"GL_DEPARTMENT_NBR",
		"GL_DEPARTMENT_NAME",
		"INVENTORY_NBR",
		"INVENTORY_NAME",
		"PROJECT_NBR",
		"PROJECT_NAME",
		"USAGE_CODE",
		"USAGE_NAME",
		"GL_LOCAL_ACCT_NBR",
		"GL_LOCAL_ACCT_NAME",
		"GL_LOCAL_SUB_ACCT_NBR",
		"GL_LOCAL_SUB_ACCT_NAME",
		"GL_FULL_ACCT_NBR",
		"GL_FULL_ACCT_NAME"
	};
	
	public List<SelectItem> getGeneralledgerSaleTransItems() {
		if(generalledgerSaleTransItems==null){
			Map<String,DataDefinitionColumn> propertyMap = cacheManager.getDataDefinitionPropertyMap("TB_BILLTRANS");
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
	    	selectItems.add(new SelectItem("","Select a Field"));
	    	
	    	for(int i=0; i<TB_BILLTRANS_GENERALLEDGER.length; i++){
	    		DataDefinitionColumn driverNames = propertyMap.get(Util.columnToProperty(TB_BILLTRANS_GENERALLEDGER[i]));
	    		if(driverNames!=null){
	    			selectItems.add(new SelectItem(driverNames.getColumnName(),driverNames.getDescription()));
	    		}
	    	}
	
	    	generalledgerSaleTransItems = selectItems;
		}
		return generalledgerSaleTransItems;
	}
	
	public List<SelectItem> getGeneralledgerSaleTransNumberItems() {		
		if(generalledgerSaleTransNumberItems==null){
			Map<String,DataDefinitionColumn> propertyMap = cacheManager.getDataDefinitionPropertyMap("TB_BILLTRANS");
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
	    	selectItems.add(new SelectItem("","Select a Field"));
	    	
	    	for(int i=0; i<TB_BILLTRANS_GENERALLEDGER.length; i++){
	    		DataDefinitionColumn driverNames = propertyMap.get(Util.columnToProperty(TB_BILLTRANS_GENERALLEDGER[i]));
	    		if(driverNames!=null && isNumberSalesColumnType(driverNames.getColumnName())){ //Number column type
	    			selectItems.add(new SelectItem(driverNames.getColumnName(),driverNames.getDescription()));
	    		}
	    	}
	
	    	generalledgerSaleTransNumberItems = selectItems;
		}
		return generalledgerSaleTransNumberItems;
	}
	
	public List<SelectItem> getGeneralledgerSaleTransTextItems() {
		if(generalledgerSaleTransTextItems==null){
			Map<String,DataDefinitionColumn> propertyMap = cacheManager.getDataDefinitionPropertyMap("TB_BILLTRANS");
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
	    	selectItems.add(new SelectItem("","Select a Field"));
	    	
	    	for(int i=0; i<TB_BILLTRANS_GENERALLEDGER.length; i++){
	    		DataDefinitionColumn driverNames = propertyMap.get(Util.columnToProperty(TB_BILLTRANS_GENERALLEDGER[i]));
	    		if(driverNames!=null && isTextSalesColumnType(driverNames.getColumnName())){ //Text column type
	    			selectItems.add(new SelectItem(driverNames.getColumnName(),driverNames.getDescription()));
	    		}
	    	}
	
	    	generalledgerSaleTransTextItems = selectItems;
		}
		return generalledgerSaleTransTextItems;
	}
	
	public List<SelectItem> getGeneralledgerSaleTransDateItems() {
		if(generalledgerSaleTransDateItems==null){
			Map<String,DataDefinitionColumn> propertyMap = cacheManager.getDataDefinitionPropertyMap("TB_BILLTRANS");
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
	    	selectItems.add(new SelectItem("","Select a Field"));
	    	
	    	for(int i=0; i<TB_BILLTRANS_GENERALLEDGER.length; i++){
	    		DataDefinitionColumn driverNames = propertyMap.get(Util.columnToProperty(TB_BILLTRANS_GENERALLEDGER[i]));
	    		if(driverNames!=null && isDateSalesColumnType(driverNames.getColumnName())){ //Date column type
	    			selectItems.add(new SelectItem(driverNames.getColumnName(),driverNames.getDescription()));
	    		}
	    	}
	
	    	generalledgerSaleTransDateItems = selectItems;
		}
		return generalledgerSaleTransDateItems;
	}

	public static String[] TB_TRANSACTION_DETAIL_TRANSACTION_COLUMNS = { 
			"COMMENTS",
			"ENTERED_DATE",
			"PRODUCT_CODE",
			"REJECT_FLAG",
			"SOURCE_TRANSACTION_ID",
			"SUSPEND_IND",
			"TAXCODE_CODE",
			"TRANSACTION_COUNTRY_CODE",
			"TRANSACTION_IND",
			"TRANSACTION_STATE_CODE",
			"TRANSACTION_STATUS"
	};
	
	public List<SelectItem> getTransactionTransactionItems() {
		if(transactionTransactionItems==null){
			Map<String,DataDefinitionColumn> propertyMap = cacheManager.getDataDefinitionPropertyMap("TB_PURCHTRANS");
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
	    	selectItems.add(new SelectItem("","Select a Field"));
	    	
	    	for(int i=0; i<TB_TRANSACTION_DETAIL_TRANSACTION_COLUMNS.length; i++){
	    		DataDefinitionColumn driverNames = propertyMap.get(Util.columnToProperty(TB_TRANSACTION_DETAIL_TRANSACTION_COLUMNS[i]));
	    		if(driverNames!=null){
	    			selectItems.add(new SelectItem(driverNames.getColumnName(),driverNames.getDescription()));
	    		}
	    	}

	    	transactionTransactionItems = selectItems;
		}
		return transactionTransactionItems;
	}
	
	public List<SelectItem> getTransactionTransactionNumberItems() {
		if(transactionTransactionNumberItems==null){
			Map<String,DataDefinitionColumn> propertyMap = cacheManager.getDataDefinitionPropertyMap("TB_PURCHTRANS");
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
	    	selectItems.add(new SelectItem("","Select a Field"));
	    	
	    	for(int i=0; i<TB_TRANSACTION_DETAIL_TRANSACTION_COLUMNS.length; i++){
	    		DataDefinitionColumn driverNames = propertyMap.get(Util.columnToProperty(TB_TRANSACTION_DETAIL_TRANSACTION_COLUMNS[i]));
	    		if(driverNames!=null && isNumberSalesColumnType(driverNames.getColumnName())){ //Number column type
	    			selectItems.add(new SelectItem(driverNames.getColumnName(),driverNames.getDescription()));
	    		}
	    	}

	    	transactionTransactionNumberItems = selectItems;
		}
		return transactionTransactionNumberItems;
	}
	
	public List<SelectItem> getTransactionTransactionTextItems() {
		if(transactionTransactionTextItems==null){
			Map<String,DataDefinitionColumn> propertyMap = cacheManager.getDataDefinitionPropertyMap("TB_PURCHTRANS");
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
	    	selectItems.add(new SelectItem("","Select a Field"));
	    	
	    	for(int i=0; i<TB_TRANSACTION_DETAIL_TRANSACTION_COLUMNS.length; i++){
	    		DataDefinitionColumn driverNames = propertyMap.get(Util.columnToProperty(TB_TRANSACTION_DETAIL_TRANSACTION_COLUMNS[i]));
	    		if(driverNames!=null && isTextSalesColumnType(driverNames.getColumnName())){ //Text column type
	    			selectItems.add(new SelectItem(driverNames.getColumnName(),driverNames.getDescription()));
	    		}
	    	}

	    	transactionTransactionTextItems = selectItems;
		}
		return transactionTransactionTextItems;
	}
	
	public List<SelectItem> getTransactionTransactionDateItems() {
		if(transactionTransactionDateItems==null){
			Map<String,DataDefinitionColumn> propertyMap = cacheManager.getDataDefinitionPropertyMap("TB_PURCHTRANS");
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
	    	selectItems.add(new SelectItem("","Select a Field"));
	    	
	    	for(int i=0; i<TB_TRANSACTION_DETAIL_TRANSACTION_COLUMNS.length; i++){
	    		DataDefinitionColumn driverNames = propertyMap.get(Util.columnToProperty(TB_TRANSACTION_DETAIL_TRANSACTION_COLUMNS[i]));
	    		if(driverNames!=null && isDateSalesColumnType(driverNames.getColumnName())){ //Date column type
	    			selectItems.add(new SelectItem(driverNames.getColumnName(),driverNames.getDescription()));
	    		}
	    	}

	    	transactionTransactionDateItems = selectItems;
		}
		return transactionTransactionDateItems;
	}
	
	public static String[] TB_BILLTRANS_CUSTOMER_COLUMNS = { 
			"CUST_NBR","CUST_NAME","CUST_LOCN_CODE","LOCATION_NAME","CUST_ENTITY_ID","CUST_ENTITY_CODE","CUST_JURISDICTION_ID","CUST_GEOCODE",
			"CUST_ADDRESS_LINE_1","CUST_ADDRESS_LINE_2","CUST_CITY","CUST_COUNTY","CUST_STATE_CODE","CUST_ZIP","CUST_ZIPPLUS4","CUST_COUNTRY_CODE",
			"CERT_NBR","CERT_TYPE","CERT_SIGN_DATE","CERT_EXPIRATION_DATE","CERT_IMAGE"
	};
	
	public List<SelectItem> getCustomerSaleTransItems() {
		if(customerSaleTransItems==null){
			Map<String,DataDefinitionColumn> propertyMap = cacheManager.getDataDefinitionPropertyMap("TB_BILLTRANS");
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
	    	selectItems.add(new SelectItem("","Select a Field"));
	    	
	    	for(int i=0; i<TB_BILLTRANS_CUSTOMER_COLUMNS.length; i++){
	    		DataDefinitionColumn driverNames = propertyMap.get(Util.columnToProperty(TB_BILLTRANS_CUSTOMER_COLUMNS[i]));
	    		if(driverNames!=null){
	    			selectItems.add(new SelectItem(driverNames.getColumnName(),driverNames.getDescription()));
	    		}
	    	}
	
	    	customerSaleTransItems = selectItems;
		}
		return customerSaleTransItems;
	}
	
	public List<SelectItem> getCustomerSaleTransNumberItems() {
		if(customerSaleTransNumberItems==null){
			Map<String,DataDefinitionColumn> propertyMap = cacheManager.getDataDefinitionPropertyMap("TB_BILLTRANS");
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
	    	selectItems.add(new SelectItem("","Select a Field"));
	    	
	    	for(int i=0; i<TB_BILLTRANS_CUSTOMER_COLUMNS.length; i++){
	    		DataDefinitionColumn driverNames = propertyMap.get(Util.columnToProperty(TB_BILLTRANS_CUSTOMER_COLUMNS[i]));
	    		if(driverNames!=null && isNumberSalesColumnType(driverNames.getColumnName())){ //Number column type
	    			selectItems.add(new SelectItem(driverNames.getColumnName(),driverNames.getDescription()));
	    		}
	    	}

	    	customerSaleTransNumberItems = selectItems;
		}
		return customerSaleTransNumberItems;
	}
	
	public List<SelectItem> getCustomerSaleTransTextItems() {
		if(customerSaleTransTextItems==null){
			Map<String,DataDefinitionColumn> propertyMap = cacheManager.getDataDefinitionPropertyMap("TB_BILLTRANS");
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
	    	selectItems.add(new SelectItem("","Select a Field"));
	    	
	    	for(int i=0; i<TB_BILLTRANS_CUSTOMER_COLUMNS.length; i++){
	    		DataDefinitionColumn driverNames = propertyMap.get(Util.columnToProperty(TB_BILLTRANS_CUSTOMER_COLUMNS[i]));
	    		if(driverNames!=null && isTextSalesColumnType(driverNames.getColumnName())){ //Text column type
	    			selectItems.add(new SelectItem(driverNames.getColumnName(),driverNames.getDescription()));
	    		}
	    	}

	    	customerSaleTransTextItems = selectItems;
		}
		return customerSaleTransTextItems;
	}
	
	public List<SelectItem> getCustomerSaleTransDateItems() {
		if(customerSaleTransDateItems==null){
			Map<String,DataDefinitionColumn> propertyMap = cacheManager.getDataDefinitionPropertyMap("TB_BILLTRANS");
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
	    	selectItems.add(new SelectItem("","Select a Field"));
	    	
	    	for(int i=0; i<TB_BILLTRANS_CUSTOMER_COLUMNS.length; i++){
	    		DataDefinitionColumn driverNames = propertyMap.get(Util.columnToProperty(TB_BILLTRANS_CUSTOMER_COLUMNS[i]));
	    		if(driverNames!=null && isDateSalesColumnType(driverNames.getColumnName())){ //Date column type
	    			selectItems.add(new SelectItem(driverNames.getColumnName(),driverNames.getDescription()));
	    		}
	    	}

	    	customerSaleTransDateItems = selectItems;
		}
		return customerSaleTransDateItems;
	}
	
	public static String[] TB_TRANSACTION_DETAIL_GENERALLEDGER_COLUMNS = { 
			"GL_CC_NBR_DEPT_ID",
			"GL_CC_NBR_DEPT_NAME",
			"GL_COMPANY_NAME",
			"GL_COMPANY_NBR",
			"GL_DATE",
			"GL_DIVISION_NAME",
			"GL_DIVISION_NBR",
			"GL_FULL_ACCT_NAME",
			"GL_FULL_ACCT_NBR",
			"GL_LINE_ITM_DIST_AMT",
			"GL_LOCAL_ACCT_NAME",
			"GL_LOCAL_ACCT_NBR",
			"GL_LOCAL_SUB_ACCT_NAME",
			"GL_LOCAL_SUB_ACCT_NBR"
    	};
	
	public List<SelectItem> getGeneralLedgerTransactionItems() {
		if(generalLedgerTransactionItems==null){
			Map<String,DataDefinitionColumn> propertyMap = cacheManager.getDataDefinitionPropertyMap("TB_PURCHTRANS");
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
	    	selectItems.add(new SelectItem("","Select a Field"));
	    	
	    	for(int i=0; i<TB_TRANSACTION_DETAIL_GENERALLEDGER_COLUMNS.length; i++){
	    		DataDefinitionColumn driverNames = propertyMap.get(Util.columnToProperty(TB_TRANSACTION_DETAIL_GENERALLEDGER_COLUMNS[i]));
	    		if(driverNames!=null){
	    			selectItems.add(new SelectItem(driverNames.getColumnName(),driverNames.getDescription()));
	    		}
	    	}

	    	generalLedgerTransactionItems = selectItems;
		}
		return generalLedgerTransactionItems;
	}
	
	public List<SelectItem> getGeneralLedgerTransactionNumberItems() {
		if(generalLedgerTransactionNumberItems==null){
			Map<String,DataDefinitionColumn> propertyMap = cacheManager.getDataDefinitionPropertyMap("TB_PURCHTRANS");
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
	    	selectItems.add(new SelectItem("","Select a Field"));
	    	
	    	for(int i=0; i<TB_TRANSACTION_DETAIL_GENERALLEDGER_COLUMNS.length; i++){
	    		DataDefinitionColumn driverNames = propertyMap.get(Util.columnToProperty(TB_TRANSACTION_DETAIL_GENERALLEDGER_COLUMNS[i]));
	    		if(driverNames!=null && isNumberSalesColumnType(driverNames.getColumnName())){ //Number column type
	    			selectItems.add(new SelectItem(driverNames.getColumnName(),driverNames.getDescription()));
	    		}
	    	}

	    	generalLedgerTransactionNumberItems = selectItems;
		}
		return generalLedgerTransactionNumberItems;
	}
	
	public List<SelectItem> getGeneralLedgerTransactionTextItems() {
		if(generalLedgerTransactionTextItems==null){
			Map<String,DataDefinitionColumn> propertyMap = cacheManager.getDataDefinitionPropertyMap("TB_PURCHTRANS");
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
	    	selectItems.add(new SelectItem("","Select a Field"));
	    	
	    	for(int i=0; i<TB_TRANSACTION_DETAIL_GENERALLEDGER_COLUMNS.length; i++){
	    		DataDefinitionColumn driverNames = propertyMap.get(Util.columnToProperty(TB_TRANSACTION_DETAIL_GENERALLEDGER_COLUMNS[i]));
	    		if(driverNames!=null && isTextSalesColumnType(driverNames.getColumnName())){ //Text column type
	    			selectItems.add(new SelectItem(driverNames.getColumnName(),driverNames.getDescription()));
	    		}
	    	}

	    	generalLedgerTransactionTextItems = selectItems;
		}
		return generalLedgerTransactionTextItems;
	}
	
	public List<SelectItem> getGeneralLedgerTransactionDateItems() {
		if(generalLedgerTransactionDateItems==null){
			Map<String,DataDefinitionColumn> propertyMap = cacheManager.getDataDefinitionPropertyMap("TB_PURCHTRANS");
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
	    	selectItems.add(new SelectItem("","Select a Field"));
	    	
	    	for(int i=0; i<TB_TRANSACTION_DETAIL_GENERALLEDGER_COLUMNS.length; i++){
	    		DataDefinitionColumn driverNames = propertyMap.get(Util.columnToProperty(TB_TRANSACTION_DETAIL_GENERALLEDGER_COLUMNS[i]));
	    		if(driverNames!=null && isDateSalesColumnType(driverNames.getColumnName())){ //Date column type
	    			selectItems.add(new SelectItem(driverNames.getColumnName(),driverNames.getDescription()));
	    		}
	    	}

	    	generalLedgerTransactionDateItems = selectItems;
		}
		return generalLedgerTransactionDateItems;
	}
	
	public static String[] TB_TRANSACTION_DETAIL_INVOICE_COLUMNS = { 
			"INVOICE_DATE",
			"INVOICE_DESC",
			"INVOICE_DISCOUNT_AMT",
			"INVOICE_FREIGHT_AMT",
			"INVOICE_LINE_AMT",
			"INVOICE_LINE_NAME",
			"INVOICE_LINE_NBR",
			"INVOICE_LINE_TAX",
			"INVOICE_LINE_TYPE",
			"INVOICE_LINE_TYPE_NAME",
			"INVOICE_NBR",
			"INVOICE_TAX_AMT",
			"INVOICE_TAX_FLG",
			"INVOICE_TOTAL_AMT"
    	};
	
	public List<SelectItem> getInvoiceTransactionItems() {
		if(invoiceTransactionItems==null){
			Map<String,DataDefinitionColumn> propertyMap = cacheManager.getDataDefinitionPropertyMap("TB_PURCHTRANS");
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
	    	selectItems.add(new SelectItem("","Select a Field"));
	    	
	    	for(int i=0; i<TB_TRANSACTION_DETAIL_INVOICE_COLUMNS.length; i++){
	    		DataDefinitionColumn driverNames = propertyMap.get(Util.columnToProperty(TB_TRANSACTION_DETAIL_INVOICE_COLUMNS[i]));
	    		if(driverNames!=null){
	    			selectItems.add(new SelectItem(driverNames.getColumnName(),driverNames.getDescription()));
	    		}
	    	}

	    	invoiceTransactionItems = selectItems;
		}
		return invoiceTransactionItems;
	}
	
	public List<SelectItem> getInvoiceTransactionNumberItems() {
		if(invoiceTransactionNumberItems==null){
			Map<String,DataDefinitionColumn> propertyMap = cacheManager.getDataDefinitionPropertyMap("TB_PURCHTRANS");
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
	    	selectItems.add(new SelectItem("","Select a Field"));
	    	
	    	for(int i=0; i<TB_TRANSACTION_DETAIL_INVOICE_COLUMNS.length; i++){
	    		DataDefinitionColumn driverNames = propertyMap.get(Util.columnToProperty(TB_TRANSACTION_DETAIL_INVOICE_COLUMNS[i]));
	    		if(driverNames!=null && isNumberSalesColumnType(driverNames.getColumnName())){ //Number column type
	    			selectItems.add(new SelectItem(driverNames.getColumnName(),driverNames.getDescription()));
	    		}
	    	}

	    	invoiceTransactionNumberItems = selectItems;
		}
		return invoiceTransactionNumberItems;
	}
	
	public List<SelectItem> getInvoiceTransactionTextItems() {
		if(invoiceTransactionTextItems==null){
			Map<String,DataDefinitionColumn> propertyMap = cacheManager.getDataDefinitionPropertyMap("TB_PURCHTRANS");
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
	    	selectItems.add(new SelectItem("","Select a Field"));
	    	
	    	for(int i=0; i<TB_TRANSACTION_DETAIL_INVOICE_COLUMNS.length; i++){
	    		DataDefinitionColumn driverNames = propertyMap.get(Util.columnToProperty(TB_TRANSACTION_DETAIL_INVOICE_COLUMNS[i]));
	    		if(driverNames!=null && isTextSalesColumnType(driverNames.getColumnName())){ //Text column type
	    			selectItems.add(new SelectItem(driverNames.getColumnName(),driverNames.getDescription()));
	    		}
	    	}

	    	invoiceTransactionTextItems = selectItems;
		}
		return invoiceTransactionTextItems;
	}
	
	public List<SelectItem> getInvoiceTransactionDateItems() {
		if(invoiceTransactionDateItems==null){
			Map<String,DataDefinitionColumn> propertyMap = cacheManager.getDataDefinitionPropertyMap("TB_PURCHTRANS");
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
	    	selectItems.add(new SelectItem("","Select a Field"));
	    	
	    	for(int i=0; i<TB_TRANSACTION_DETAIL_INVOICE_COLUMNS.length; i++){
	    		DataDefinitionColumn driverNames = propertyMap.get(Util.columnToProperty(TB_TRANSACTION_DETAIL_INVOICE_COLUMNS[i]));
	    		if(driverNames!=null && isDateSalesColumnType(driverNames.getColumnName())){ //Date column type
	    			selectItems.add(new SelectItem(driverNames.getColumnName(),driverNames.getDescription()));
	    		}
	    	}

	    	invoiceTransactionDateItems = selectItems;
		}
		return invoiceTransactionDateItems;
	}
	
	public static String[] TB_BILLTRANS_INVOICE_COLUMNS = { 
			"INVOICE_DATE","INVOICE_NBR","INVOICE_DESC","INVOICE_GROSS_AMT","INVOICE_FREIGHT_AMT","INVOICE_DISC_AMT",
			"INVOICE_DISC_TYPE_CODE","INVOICE_LINE_NBR","INVOICE_LINE_DESC","INVOICE_LINE_ITEM_COUNT","INVOICE_LINE_ITEM_AMT",
			"INVOICE_LINE_GROSS_AMT","INVOICE_LINE_FREIGHT_AMT","INVOICE_LINE_DISC_AMT","INVOICE_LINE_DISC_TYPE_CODE",
			"INVOICE_LINE_PRODUCT_CODE","INVOICE_TAX_PAID_AMT","INVOICE_DIFF_AMT","INVOICE_LINE_TAX_PAID_AMT","INVOICE_LINE_DIFF_AMT",
			"INVOICE_DIFF_BATCH_NO","INVOICE_TAX_AMT","INVOICE_FREIGHTABLE_AMT","INVOICE_MISCABLE_AMT","INVOICE_PROFRT_FLAG",
			"INVOICE_PROMISC_FLAG","INVOICE_MISC_AMT","INVOICE_LINE_MISC_AMT","INVOICE_DIFF_BATCH_NO","PRODUCT_NAME","PRODUCT_CLASS_CODE","PRODUCT_CLASS_NAME"
    	};
	
	public List<SelectItem> getInvoiceSaleTransItems() {
		if(invoiceSaleTransItems==null){
			Map<String,DataDefinitionColumn> propertyMap = cacheManager.getDataDefinitionPropertyMap("TB_BILLTRANS");
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
	    	selectItems.add(new SelectItem("","Select a Field"));
	    	
	    	for(int i=0; i<TB_BILLTRANS_INVOICE_COLUMNS.length; i++){
	    		DataDefinitionColumn driverNames = propertyMap.get(Util.columnToProperty(TB_BILLTRANS_INVOICE_COLUMNS[i]));
	    		if(driverNames!=null){
	    			selectItems.add(new SelectItem(driverNames.getColumnName(),driverNames.getDescription()));
	    		}
	    	}

	    	invoiceSaleTransItems = selectItems;
		}
		return invoiceSaleTransItems;
	}
	
	public List<SelectItem> getInvoiceSaleTransNumberItems() {
		if(invoiceSaleTransNumberItems==null){
			Map<String,DataDefinitionColumn> propertyMap = cacheManager.getDataDefinitionPropertyMap("TB_BILLTRANS");
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
	    	selectItems.add(new SelectItem("","Select a Field"));
	    	
	    	for(int i=0; i<TB_BILLTRANS_INVOICE_COLUMNS.length; i++){
	    		DataDefinitionColumn driverNames = propertyMap.get(Util.columnToProperty(TB_BILLTRANS_INVOICE_COLUMNS[i]));
	    		if(driverNames!=null && isNumberSalesColumnType(driverNames.getColumnName())){ //Number column type
	    			selectItems.add(new SelectItem(driverNames.getColumnName(),driverNames.getDescription()));
	    		}
	    	}

	    	invoiceSaleTransNumberItems = selectItems;
		}
		return invoiceSaleTransNumberItems;
	}
	
	public List<SelectItem> getInvoiceSaleTransTextItems() {
		if(invoiceSaleTransTextItems==null){
			Map<String,DataDefinitionColumn> propertyMap = cacheManager.getDataDefinitionPropertyMap("TB_BILLTRANS");
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
	    	selectItems.add(new SelectItem("","Select a Field"));
	    	
	    	for(int i=0; i<TB_BILLTRANS_INVOICE_COLUMNS.length; i++){
	    		DataDefinitionColumn driverNames = propertyMap.get(Util.columnToProperty(TB_BILLTRANS_INVOICE_COLUMNS[i]));
	    		if(driverNames!=null && isTextSalesColumnType(driverNames.getColumnName())){ //Text column type
	    			selectItems.add(new SelectItem(driverNames.getColumnName(),driverNames.getDescription()));
	    		}
	    	}

	    	invoiceSaleTransTextItems = selectItems;
		}
		return invoiceSaleTransTextItems;
	}
	
	public List<SelectItem> getInvoiceSaleTransDateItems() {
		if(invoiceSaleTransDateItems==null){
			Map<String,DataDefinitionColumn> propertyMap = cacheManager.getDataDefinitionPropertyMap("TB_BILLTRANS");
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
	    	selectItems.add(new SelectItem("","Select a Field"));
	    	
	    	for(int i=0; i<TB_BILLTRANS_INVOICE_COLUMNS.length; i++){
	    		DataDefinitionColumn driverNames = propertyMap.get(Util.columnToProperty(TB_BILLTRANS_INVOICE_COLUMNS[i]));
	    		if(driverNames!=null && isDateSalesColumnType(driverNames.getColumnName())){ //Date column type
	    			selectItems.add(new SelectItem(driverNames.getColumnName(),driverNames.getDescription()));
	    		}
	    	}

	    	invoiceSaleTransDateItems = selectItems;
		}
		return invoiceSaleTransDateItems;
	}
	
	public static String[] TB_TRANSACTION_DETAIL_LOCATION_COLUMNS = { 
			"SHIPTO_CITY",	
			"SHIPTO_COUNTRY_CODE",	
			"SHIPTO_COUNTY",	
			"SHIPTO_ADDRESS_LINE_1",	
			"SHIPTO_ADDRESS_LINE_2",	
			"SHIPTO_STATE_CODE",	
			"SHIPTO_ZIP",	
			"SHIPTO_LOCATION",	
			"SHIPTO_LOCATION_NAME"
	};
	
	public List<SelectItem> getLocationTransactionItems() {
		if(locationTransactionItems==null){
			Map<String,DataDefinitionColumn> propertyMap = cacheManager.getDataDefinitionPropertyMap("TB_PURCHTRANS");
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
	    	selectItems.add(new SelectItem("","Select a Field"));
	    	
	    	for(int i=0; i<TB_TRANSACTION_DETAIL_LOCATION_COLUMNS.length; i++){
	    		DataDefinitionColumn driverNames = propertyMap.get(Util.columnToProperty(TB_TRANSACTION_DETAIL_LOCATION_COLUMNS[i]));
	    		if(driverNames!=null){
	    			selectItems.add(new SelectItem(driverNames.getColumnName(),driverNames.getDescription()));
	    		}
	    	}

	    	locationTransactionItems = selectItems;
		}
		return locationTransactionItems;
	}
	
	public List<SelectItem> getLocationTransactionNumberItems() {
		if(locationTransactionNumberItems==null){
			Map<String,DataDefinitionColumn> propertyMap = cacheManager.getDataDefinitionPropertyMap("TB_PURCHTRANS");
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
	    	selectItems.add(new SelectItem("","Select a Field"));
	    	
	    	for(int i=0; i<TB_TRANSACTION_DETAIL_LOCATION_COLUMNS.length; i++){
	    		DataDefinitionColumn driverNames = propertyMap.get(Util.columnToProperty(TB_TRANSACTION_DETAIL_LOCATION_COLUMNS[i]));
	    		if(driverNames!=null && isNumberSalesColumnType(driverNames.getColumnName())){ //Number column type
	    			selectItems.add(new SelectItem(driverNames.getColumnName(),driverNames.getDescription()));
	    		}
	    	}

	    	locationTransactionNumberItems = selectItems;
		}
		return locationTransactionNumberItems;
	}
	
	public List<SelectItem> getLocationTransactionTextItems() {
		if(locationTransactionTextItems==null){
			Map<String,DataDefinitionColumn> propertyMap = cacheManager.getDataDefinitionPropertyMap("TB_PURCHTRANS");
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
	    	selectItems.add(new SelectItem("","Select a Field"));
	    	
	    	for(int i=0; i<TB_TRANSACTION_DETAIL_LOCATION_COLUMNS.length; i++){
	    		DataDefinitionColumn driverNames = propertyMap.get(Util.columnToProperty(TB_TRANSACTION_DETAIL_LOCATION_COLUMNS[i]));
	    		if(driverNames!=null && isTextSalesColumnType(driverNames.getColumnName())){ //Text column type
	    			selectItems.add(new SelectItem(driverNames.getColumnName(),driverNames.getDescription()));
	    		}
	    	}

	    	locationTransactionTextItems = selectItems;
		}
		return locationTransactionTextItems;
	}
	
	public List<SelectItem> getLocationTransactionDateItems() {
		if(locationTransactionDateItems==null){
			Map<String,DataDefinitionColumn> propertyMap = cacheManager.getDataDefinitionPropertyMap("TB_PURCHTRANS");
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
	    	selectItems.add(new SelectItem("","Select a Field"));
	    	
	    	for(int i=0; i<TB_TRANSACTION_DETAIL_LOCATION_COLUMNS.length; i++){
	    		DataDefinitionColumn driverNames = propertyMap.get(Util.columnToProperty(TB_TRANSACTION_DETAIL_LOCATION_COLUMNS[i]));
	    		if(driverNames!=null && isDateSalesColumnType(driverNames.getColumnName())){ //Date column type
	    			selectItems.add(new SelectItem(driverNames.getColumnName(),driverNames.getDescription()));
	    		}
	    	}

	    	locationTransactionDateItems = selectItems;
		}
		return locationTransactionDateItems;
	}
	
	public static String[] TB_BILLTRANS_LOCATION_COLUMNS = { 
			"SHIPTO_ENTITY_ID","SHIPTO_ENTITY_CODE","SHIPTO_JURISDICTION_ID","SHIPTO_GEOCODE","SHIPTO_ADDRESS_LINE_1","SHIPTO_ADDRESS_LINE_2",
			"SHIPTO_CITY","SHIPTO_COUNTY","SHIPTO_STATE_CODE","SHIPTO_ZIP","SHIPTO_ZIPPLUS4","SHIPTO_COUNTRY_CODE","SHIPTO_STJ1_NAME",
			"SHIPTO_STJ2_NAME","SHIPTO_STJ3_NAME","SHIPTO_STJ4_NAME","SHIPTO_STJ5_NAME","SHIPFROM_ENTITY_ID","SHIPFROM_ENTITY_CODE",
			"SHIPFROM_JURISDICTION_ID","SHIPFROM_GEOCODE","SHIPFROM_ADDRESS_LINE_1","SHIPFROM_ADDRESS_LINE_2",
			"SHIPFROM_CITY","SHIPFROM_COUNTY","SHIPFROM_STATE_CODE","SHIPFROM_ZIP","SHIPFROM_ZIPPLUS4","SHIPFROM_COUNTRY_CODE",
			"SHIPFROM_STJ1_NAME","SHIPFROM_STJ2_NAME","SHIPFROM_STJ3_NAME","SHIPFROM_STJ4_NAME","SHIPFROM_STJ5_NAME","ORDRACPT_ENTITY_ID",
			"ORDRACPT_ENTITY_CODE","ORDRACPT_JURISDICTION_ID","ORDRACPT_GEOCODE","ORDRACPT_ADDRESS_LINE_1","ORDRACPT_ADDRESS_LINE_2","ORDRACPT_CITY",
			"ORDRACPT_COUNTY","ORDRACPT_STATE_CODE","ORDRACPT_ZIP","ORDRACPT_ZIPPLUS4","ORDRACPT_COUNTRY_CODE","ORDRACPT_STJ1_NAME",
			"ORDRACPT_STJ2_NAME","ORDRACPT_STJ3_NAME","ORDRACPT_STJ4_NAME","ORDRACPT_STJ5_NAME","ORDRORGN_ENTITY_ID","ORDRORGN_ENTITY_CODE",
			"ORDRORGN_JURISDICTION_ID","ORDRORGN_GEOCODE","ORDRORGN_ADDRESS_LINE_1","ORDRORGN_ADDRESS_LINE_2","ORDRORGN_CITY","ORDRORGN_COUNTY",
			"ORDRORGN_STATE_CODE","ORDRORGN_ZIP","ORDRORGN_ZIPPLUS4","ORDRORGN_COUNTRY_CODE","ORDRORGN_STJ1_NAME","ORDRORGN_STJ2_NAME",
			"ORDRORGN_STJ3_NAME","ORDRORGN_STJ4_NAME","ORDRORGN_STJ5_NAME","FIRSTUSE_ENTITY_ID","FIRSTUSE_ENTITY_CODE","FIRSTUSE_JURISDICTION_ID",
			"FIRSTUSE_GEOCODE","FIRSTUSE_ADDRESS_LINE_1","FIRSTUSE_ADDRESS_LINE_2","FIRSTUSE_CITY","FIRSTUSE_COUNTY","FIRSTUSE_STATE_CODE",
			"FIRSTUSE_ZIP","FIRSTUSE_ZIPPLUS4","FIRSTUSE_COUNTRY_CODE","FIRSTUSE_STJ1_NAME","FIRSTUSE_STJ2_NAME","FIRSTUSE_STJ3_NAME",
			"FIRSTUSE_STJ4_NAME","FIRSTUSE_STJ5_NAME","BILLTO_ENTITY_ID","BILLTO_ENTITY_CODE","BILLTO_JURISDICTION_ID","BILLTO_GEOCODE",
			"BILLTO_ADDRESS_LINE_1","BILLTO_ADDRESS_LINE_2","BILLTO_CITY","BILLTO_COUNTY","BILLTO_STATE_CODE","BILLTO_ZIP","BILLTO_ZIPPLUS4",
			"BILLTO_COUNTRY_CODE","BILLTO_STJ1_NAME","BILLTO_STJ2_NAME","BILLTO_STJ3_NAME","BILLTO_STJ4_NAME","BILLTO_STJ5_NAME",
			"TTLXFR_ENTITY_ID","TTLXFR_ENTITY_CODE","TTLXFR_JURISDICTION_ID","TTLXFR_GEOCODE","TTLXFR_ADDRESS_LINE_1","TTLXFR_ADDRESS_LINE_2",
			"TTLXFR_CITY","TTLXFR_COUNTY","TTLXFR_STATE_CODE","TTLXFR_ZIP","TTLXFR_ZIPPLUS4","TTLXFR_COUNTRY_CODE",
			"TTLXFR_STJ1_NAME","TTLXFR_STJ2_NAME","TTLXFR_STJ3_NAME","TTLXFR_STJ4_NAME","TTLXFR_STJ5_NAME"	
	};
	
	public List<SelectItem> getLocationSaleTransItems() {
		if(locationSaleTransItems==null){
			Map<String,DataDefinitionColumn> propertyMap = cacheManager.getDataDefinitionPropertyMap("TB_BILLTRANS");
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
	    	selectItems.add(new SelectItem("","Select a Field"));
	    	
	    	for(int i=0; i<TB_BILLTRANS_LOCATION_COLUMNS.length; i++){
	    		DataDefinitionColumn driverNames = propertyMap.get(Util.columnToProperty(TB_BILLTRANS_LOCATION_COLUMNS[i]));
	    		if(driverNames!=null){
	    			selectItems.add(new SelectItem(driverNames.getColumnName(),driverNames.getDescription()));
	    		}
	    	}

	    	locationSaleTransItems = selectItems;
		}
		return locationSaleTransItems;
	}
	
	public List<SelectItem> getLocationSaleTransNumberItems() {
		if(locationSaleTransNumberItems==null){
			Map<String,DataDefinitionColumn> propertyMap = cacheManager.getDataDefinitionPropertyMap("TB_BILLTRANS");
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
	    	selectItems.add(new SelectItem("","Select a Field"));
	    	
	    	for(int i=0; i<TB_BILLTRANS_LOCATION_COLUMNS.length; i++){
	    		DataDefinitionColumn driverNames = propertyMap.get(Util.columnToProperty(TB_BILLTRANS_LOCATION_COLUMNS[i]));
	    		if(driverNames!=null && isNumberSalesColumnType(driverNames.getColumnName())){ //Number column type
	    			selectItems.add(new SelectItem(driverNames.getColumnName(),driverNames.getDescription()));
	    		}
	    	}

	    	locationSaleTransNumberItems = selectItems;
		}
		return locationSaleTransNumberItems;
	}
	
	public List<SelectItem> getLocationSaleTransTextItems() {
		if(locationSaleTransTextItems==null){
			Map<String,DataDefinitionColumn> propertyMap = cacheManager.getDataDefinitionPropertyMap("TB_BILLTRANS");
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
	    	selectItems.add(new SelectItem("","Select a Field"));
	    	
	    	for(int i=0; i<TB_BILLTRANS_LOCATION_COLUMNS.length; i++){
	    		DataDefinitionColumn driverNames = propertyMap.get(Util.columnToProperty(TB_BILLTRANS_LOCATION_COLUMNS[i]));
	    		if(driverNames!=null && isTextSalesColumnType(driverNames.getColumnName())){ //Text column type
	    			selectItems.add(new SelectItem(driverNames.getColumnName(),driverNames.getDescription()));
	    		}
	    	}

	    	locationSaleTransTextItems = selectItems;
		}
		return locationSaleTransTextItems;
	}
	
	public List<SelectItem> getLocationSaleTransDateItems() {
		if(locationSaleTransDateItems==null){
			Map<String,DataDefinitionColumn> propertyMap = cacheManager.getDataDefinitionPropertyMap("TB_BILLTRANS");
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
	    	selectItems.add(new SelectItem("","Select a Field"));
	    	
	    	for(int i=0; i<TB_BILLTRANS_LOCATION_COLUMNS.length; i++){
	    		DataDefinitionColumn driverNames = propertyMap.get(Util.columnToProperty(TB_BILLTRANS_LOCATION_COLUMNS[i]));
	    		if(driverNames!=null && isDateSalesColumnType(driverNames.getColumnName())){ //Date column type
	    			selectItems.add(new SelectItem(driverNames.getColumnName(),driverNames.getDescription()));
	    		}
	    	}

	    	locationSaleTransDateItems = selectItems;
		}
		return locationSaleTransDateItems;
	}
	
	public static String[] TB_TRANSACTION_DETAIL_VENDOR_COLUMNS  = { 
			"VENDOR_ADDRESS_CITY",
			"VENDOR_ADDRESS_COUNTRY",
			"VENDOR_ADDRESS_COUNTY",
			"VENDOR_ADDRESS_LINE_1",
			"VENDOR_ADDRESS_LINE_2",
			"VENDOR_ADDRESS_LINE_3",
			"VENDOR_ADDRESS_LINE_4",
			"VENDOR_ADDRESS_STATE",
			"VENDOR_ADDRESS_ZIP",
			"VENDOR_NAME",
			"VENDOR_NBR",
			"VENDOR_TYPE",
			"VENDOR_TYPE_NAME"
	};
	
	public List<SelectItem> getVendorTransactionItems() {
		if(vendorTransactionItems==null){
			Map<String,DataDefinitionColumn> propertyMap = cacheManager.getDataDefinitionPropertyMap("TB_PURCHTRANS");
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
	    	selectItems.add(new SelectItem("","Select a Field"));
	    	
	    	for(int i=0; i<TB_TRANSACTION_DETAIL_VENDOR_COLUMNS.length; i++){
	    		DataDefinitionColumn driverNames = propertyMap.get(Util.columnToProperty(TB_TRANSACTION_DETAIL_VENDOR_COLUMNS[i]));
	    		if(driverNames!=null){
	    			selectItems.add(new SelectItem(driverNames.getColumnName(),driverNames.getDescription()));
	    		}
	    	}

	    	vendorTransactionItems = selectItems;
		}
		return vendorTransactionItems;
	}
	
	public List<SelectItem> getVendorTransactionNumberItems() {
		if(vendorTransactionNumberItems==null){
			Map<String,DataDefinitionColumn> propertyMap = cacheManager.getDataDefinitionPropertyMap("TB_PURCHTRANS");
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
	    	selectItems.add(new SelectItem("","Select a Field"));
	    	
	    	for(int i=0; i<TB_TRANSACTION_DETAIL_VENDOR_COLUMNS.length; i++){
	    		DataDefinitionColumn driverNames = propertyMap.get(Util.columnToProperty(TB_TRANSACTION_DETAIL_VENDOR_COLUMNS[i]));
	    		if(driverNames!=null && isNumberSalesColumnType(driverNames.getColumnName())){ //Number column type
	    			selectItems.add(new SelectItem(driverNames.getColumnName(),driverNames.getDescription()));
	    		}
	    	}

	    	vendorTransactionNumberItems = selectItems;
		}
		return vendorTransactionNumberItems;
	}
	
	public List<SelectItem> getVendorTransactionTextItems() {
		if(vendorTransactionTextItems==null){
			Map<String,DataDefinitionColumn> propertyMap = cacheManager.getDataDefinitionPropertyMap("TB_PURCHTRANS");
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
	    	selectItems.add(new SelectItem("","Select a Field"));
	    	
	    	for(int i=0; i<TB_TRANSACTION_DETAIL_VENDOR_COLUMNS.length; i++){
	    		DataDefinitionColumn driverNames = propertyMap.get(Util.columnToProperty(TB_TRANSACTION_DETAIL_VENDOR_COLUMNS[i]));
	    		if(driverNames!=null && isTextSalesColumnType(driverNames.getColumnName())){ //Text column type
	    			selectItems.add(new SelectItem(driverNames.getColumnName(),driverNames.getDescription()));
	    		}
	    	}

	    	vendorTransactionTextItems = selectItems;
		}
		return vendorTransactionTextItems;
	}
	
	public List<SelectItem> getVendorTransactionDateItems() {
		if(vendorTransactionDateItems==null){
			Map<String,DataDefinitionColumn> propertyMap = cacheManager.getDataDefinitionPropertyMap("TB_PURCHTRANS");
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
	    	selectItems.add(new SelectItem("","Select a Field"));
	    	
	    	for(int i=0; i<TB_TRANSACTION_DETAIL_VENDOR_COLUMNS.length; i++){
	    		DataDefinitionColumn driverNames = propertyMap.get(Util.columnToProperty(TB_TRANSACTION_DETAIL_VENDOR_COLUMNS[i]));
	    		if(driverNames!=null && isDateSalesColumnType(driverNames.getColumnName())){ //Date column type
	    			selectItems.add(new SelectItem(driverNames.getColumnName(),driverNames.getDescription()));
	    		}
	    	}

	    	vendorTransactionDateItems = selectItems;
		}
		return vendorTransactionDateItems;
	}
	
	public static String[] TB_BILLTRANS_ENTITY_COLUMNS = { 
			"ENTITY_ID","ENTITY_CODE","COMPANY_NBR","COMPANY_DESC","DIVISION_NBR","DIVISION_DESC"
	};
	
	public List<SelectItem> getEntitySaleTransItems() {
		if(entitySaleTransItems==null){
			Map<String,DataDefinitionColumn> propertyMap = cacheManager.getDataDefinitionPropertyMap("TB_BILLTRANS");
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
	    	selectItems.add(new SelectItem("","Select a Field"));
	    	
	    	for(int i=0; i<TB_BILLTRANS_ENTITY_COLUMNS.length; i++){
	    		DataDefinitionColumn driverNames = propertyMap.get(Util.columnToProperty(TB_BILLTRANS_ENTITY_COLUMNS[i]));
	    		if(driverNames!=null){
	    			selectItems.add(new SelectItem(driverNames.getColumnName(),driverNames.getDescription()));
	    		}
	    	}

	    	entitySaleTransItems = selectItems;
		}
		return entitySaleTransItems;
	}
	
	public List<SelectItem> getEntitySaleTransNumberItems() {
		if(entitySaleTransNumberItems==null){
			Map<String,DataDefinitionColumn> propertyMap = cacheManager.getDataDefinitionPropertyMap("TB_BILLTRANS");
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
	    	selectItems.add(new SelectItem("","Select a Field"));
	    	
	    	for(int i=0; i<TB_BILLTRANS_ENTITY_COLUMNS.length; i++){
	    		DataDefinitionColumn driverNames = propertyMap.get(Util.columnToProperty(TB_BILLTRANS_ENTITY_COLUMNS[i]));
	    		if(driverNames!=null && isNumberSalesColumnType(driverNames.getColumnName())){ //Number column type
	    			selectItems.add(new SelectItem(driverNames.getColumnName(),driverNames.getDescription()));
	    		}
	    	}

	    	entitySaleTransNumberItems = selectItems;
		}
		return entitySaleTransNumberItems;
	}
	
	public List<SelectItem> getEntitySaleTransTextItems() {
		if(entitySaleTransTextItems==null){
			Map<String,DataDefinitionColumn> propertyMap = cacheManager.getDataDefinitionPropertyMap("TB_BILLTRANS");
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
	    	selectItems.add(new SelectItem("","Select a Field"));
	    	
	    	for(int i=0; i<TB_BILLTRANS_ENTITY_COLUMNS.length; i++){
	    		DataDefinitionColumn driverNames = propertyMap.get(Util.columnToProperty(TB_BILLTRANS_ENTITY_COLUMNS[i]));
	    		if(driverNames!=null && isTextSalesColumnType(driverNames.getColumnName())){ //Text column type
	    			selectItems.add(new SelectItem(driverNames.getColumnName(),driverNames.getDescription()));
	    		}
	    	}

	    	entitySaleTransTextItems = selectItems;
		}
		return entitySaleTransTextItems;	
	}
	
	public List<SelectItem> getEntitySaleTransDateItems() {
		if(entitySaleTransDateItems==null){
			Map<String,DataDefinitionColumn> propertyMap = cacheManager.getDataDefinitionPropertyMap("TB_BILLTRANS");
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
	    	selectItems.add(new SelectItem("","Select a Field"));
	    	
	    	for(int i=0; i<TB_BILLTRANS_ENTITY_COLUMNS.length; i++){
	    		DataDefinitionColumn driverNames = propertyMap.get(Util.columnToProperty(TB_BILLTRANS_ENTITY_COLUMNS[i]));
	    		if(driverNames!=null && isDateSalesColumnType(driverNames.getColumnName())){ //Date column type
	    			selectItems.add(new SelectItem(driverNames.getColumnName(),driverNames.getDescription()));
	    		}
	    	}

	    	entitySaleTransDateItems = selectItems;
		}
		return entitySaleTransDateItems;
	}
	
	///
	public static String[] TB_TRANSACTION_DETAIL_PAYMENTINFO_COLUMNS = { 
			"CHECK_AMT",
			"CHECK_DATE",
			"CHECK_DESC",
			"CHECK_NBR",
			"CHECK_NO",
			"VOUCHER_DATE",
			"VOUCHER_ID",
			"VOUCHER_LINE_DESC",
			"VOUCHER_LINE_NBR",
			"VOUCHER_NAME"
	};
	
	public List<SelectItem> getPaymentInfoTransactionItems() {
		if(paymentInfoTransactionItems==null){
			Map<String,DataDefinitionColumn> propertyMap = cacheManager.getDataDefinitionPropertyMap("TB_PURCHTRANS");
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
	    	selectItems.add(new SelectItem("","Select a Field"));
	    	
	    	for(int i=0; i<TB_TRANSACTION_DETAIL_PAYMENTINFO_COLUMNS.length; i++){
	    		DataDefinitionColumn driverNames = propertyMap.get(Util.columnToProperty(TB_TRANSACTION_DETAIL_PAYMENTINFO_COLUMNS[i]));
	    		if(driverNames!=null){
	    			selectItems.add(new SelectItem(driverNames.getColumnName(),driverNames.getDescription()));
	    		}
	    	}

	    	paymentInfoTransactionItems = selectItems;
		}
		return paymentInfoTransactionItems;
	}
	
	public List<SelectItem> getPaymentInfoTransactionNumberItems() {
		if(paymentInfoTransactionNumberItems==null){
			Map<String,DataDefinitionColumn> propertyMap = cacheManager.getDataDefinitionPropertyMap("TB_PURCHTRANS");
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
	    	selectItems.add(new SelectItem("","Select a Field"));
	    	
	    	for(int i=0; i<TB_TRANSACTION_DETAIL_PAYMENTINFO_COLUMNS.length; i++){
	    		DataDefinitionColumn driverNames = propertyMap.get(Util.columnToProperty(TB_TRANSACTION_DETAIL_PAYMENTINFO_COLUMNS[i]));
	    		if(driverNames!=null && isNumberSalesColumnType(driverNames.getColumnName())){ //Number column type
	    			selectItems.add(new SelectItem(driverNames.getColumnName(),driverNames.getDescription()));
	    		}
	    	}

	    	paymentInfoTransactionNumberItems = selectItems;
		}
		return paymentInfoTransactionNumberItems;
	}
	
	public List<SelectItem> getPaymentInfoTransactionTextItems() {
		if(paymentInfoTransactionTextItems==null){
			Map<String,DataDefinitionColumn> propertyMap = cacheManager.getDataDefinitionPropertyMap("TB_PURCHTRANS");
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
	    	selectItems.add(new SelectItem("","Select a Field"));
	    	
	    	for(int i=0; i<TB_TRANSACTION_DETAIL_PAYMENTINFO_COLUMNS.length; i++){
	    		DataDefinitionColumn driverNames = propertyMap.get(Util.columnToProperty(TB_TRANSACTION_DETAIL_PAYMENTINFO_COLUMNS[i]));
	    		if(driverNames!=null && isTextSalesColumnType(driverNames.getColumnName())){ //Text column type
	    			selectItems.add(new SelectItem(driverNames.getColumnName(),driverNames.getDescription()));
	    		}
	    	}

	    	paymentInfoTransactionTextItems = selectItems;
		}
		return paymentInfoTransactionTextItems;		
	}
	
	public List<SelectItem> getPaymentInfoTransactionDateItems() {
		if(paymentInfoTransactionDateItems==null){
			Map<String,DataDefinitionColumn> propertyMap = cacheManager.getDataDefinitionPropertyMap("TB_PURCHTRANS");
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
	    	selectItems.add(new SelectItem("","Select a Field"));
	    	
	    	for(int i=0; i<TB_TRANSACTION_DETAIL_PAYMENTINFO_COLUMNS.length; i++){
	    		DataDefinitionColumn driverNames = propertyMap.get(Util.columnToProperty(TB_TRANSACTION_DETAIL_PAYMENTINFO_COLUMNS[i]));
	    		if(driverNames!=null && isDateSalesColumnType(driverNames.getColumnName())){ //Date column type
	    			selectItems.add(new SelectItem(driverNames.getColumnName(),driverNames.getDescription()));
	    		}
	    	}

	    	paymentInfoTransactionDateItems = selectItems;
		}
		return paymentInfoTransactionDateItems;
	}
	///
	
	public static String[] TB_TRANSACTION_DETAIL_WORKORDER_COLUMNS = { 
			"WO_CLASS",
			"WO_CLASS_DESC",
			"WO_DATE",
			"WO_ENTITY",
			"WO_ENTITY_DESC",
			"WO_LINE_NAME",
			"WO_LINE_NBR",
			"WO_LINE_TYPE",
			"WO_LINE_TYPE_DESC",
			"WO_NAME",
			"WO_NBR",
			"WO_SHUT_DOWN_CD",
			"WO_SHUT_DOWN_CD_DESC",
			"WO_TYPE",
			"WO_TYPE_DESC"
	};
	
	public List<SelectItem> getWorkOrderTransactionItems() {
		if(workOrderTransactionItems==null){
			Map<String,DataDefinitionColumn> propertyMap = cacheManager.getDataDefinitionPropertyMap("TB_PURCHTRANS");
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
	    	selectItems.add(new SelectItem("","Select a Field"));
	    	
	    	for(int i=0; i<TB_TRANSACTION_DETAIL_WORKORDER_COLUMNS.length; i++){
	    		DataDefinitionColumn driverNames = propertyMap.get(Util.columnToProperty(TB_TRANSACTION_DETAIL_WORKORDER_COLUMNS[i]));
	    		if(driverNames!=null){
	    			selectItems.add(new SelectItem(driverNames.getColumnName(),driverNames.getDescription()));
	    		}
	    	}

	    	workOrderTransactionItems = selectItems;
		}
		return workOrderTransactionItems;
	}
	
	public List<SelectItem> getWorkOrderTransactionNumberItems() {
		if(workOrderTransactionNumberItems==null){
			Map<String,DataDefinitionColumn> propertyMap = cacheManager.getDataDefinitionPropertyMap("TB_PURCHTRANS");
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
	    	selectItems.add(new SelectItem("","Select a Field"));
	    	
	    	for(int i=0; i<TB_TRANSACTION_DETAIL_WORKORDER_COLUMNS.length; i++){
	    		DataDefinitionColumn driverNames = propertyMap.get(Util.columnToProperty(TB_TRANSACTION_DETAIL_WORKORDER_COLUMNS[i]));
	    		if(driverNames!=null && isNumberSalesColumnType(driverNames.getColumnName())){ //Number column type
	    			selectItems.add(new SelectItem(driverNames.getColumnName(),driverNames.getDescription()));
	    		}
	    	}

	    	workOrderTransactionNumberItems = selectItems;
		}
		return workOrderTransactionNumberItems;
	}
	
	public List<SelectItem> getWorkOrderTransactionTextItems() {
		if(workOrderTransactionTextItems==null){
			Map<String,DataDefinitionColumn> propertyMap = cacheManager.getDataDefinitionPropertyMap("TB_PURCHTRANS");
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
	    	selectItems.add(new SelectItem("","Select a Field"));
	    	
	    	for(int i=0; i<TB_TRANSACTION_DETAIL_WORKORDER_COLUMNS.length; i++){
	    		DataDefinitionColumn driverNames = propertyMap.get(Util.columnToProperty(TB_TRANSACTION_DETAIL_WORKORDER_COLUMNS[i]));
	    		if(driverNames!=null && isTextSalesColumnType(driverNames.getColumnName())){ //Text column type
	    			selectItems.add(new SelectItem(driverNames.getColumnName(),driverNames.getDescription()));
	    		}
	    	}

	    	workOrderTransactionTextItems = selectItems;
		}
		return workOrderTransactionTextItems;		
	}
	
	public List<SelectItem> getWorkOrderTransactionDateItems() {
		if(workOrderTransactionDateItems==null){
			Map<String,DataDefinitionColumn> propertyMap = cacheManager.getDataDefinitionPropertyMap("TB_PURCHTRANS");
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
	    	selectItems.add(new SelectItem("","Select a Field"));
	    	
	    	for(int i=0; i<TB_TRANSACTION_DETAIL_WORKORDER_COLUMNS.length; i++){
	    		DataDefinitionColumn driverNames = propertyMap.get(Util.columnToProperty(TB_TRANSACTION_DETAIL_WORKORDER_COLUMNS[i]));
	    		if(driverNames!=null && isDateSalesColumnType(driverNames.getColumnName())){ //Date column type
	    			selectItems.add(new SelectItem(driverNames.getColumnName(),driverNames.getDescription()));
	    		}
	    	}

	    	workOrderTransactionDateItems = selectItems;
		}
		return workOrderTransactionDateItems;
	}
	
	public static String[] TB_TRANSACTION_DETAIL_AUDIT_COLUMNS = { 
			"AUDIT_FLAG",
			"AUDIT_TIMESTAMP",
			"AUDIT_USER_ID"
	};
	
	public List<SelectItem> getAuditTransactionItems() {
		if(auditTransactionItems==null){
			Map<String,DataDefinitionColumn> propertyMap = cacheManager.getDataDefinitionPropertyMap("TB_PURCHTRANS");
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
	    	selectItems.add(new SelectItem("","Select a Field"));
	    	
	    	for(int i=0; i<TB_TRANSACTION_DETAIL_AUDIT_COLUMNS.length; i++){
	    		DataDefinitionColumn driverNames = propertyMap.get(Util.columnToProperty(TB_TRANSACTION_DETAIL_AUDIT_COLUMNS[i]));
	    		if(driverNames!=null){
	    			selectItems.add(new SelectItem(driverNames.getColumnName(),driverNames.getDescription()));
	    		}
	    	}

	    	auditTransactionItems = selectItems;
		}
		return auditTransactionItems;
	}
	
	public List<SelectItem> getAuditTransactionNumberItems() {
		if(auditTransactionNumberItems==null){
			Map<String,DataDefinitionColumn> propertyMap = cacheManager.getDataDefinitionPropertyMap("TB_PURCHTRANS");
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
	    	selectItems.add(new SelectItem("","Select a Field"));
	    	
	    	for(int i=0; i<TB_TRANSACTION_DETAIL_AUDIT_COLUMNS.length; i++){
	    		DataDefinitionColumn driverNames = propertyMap.get(Util.columnToProperty(TB_TRANSACTION_DETAIL_AUDIT_COLUMNS[i]));
	    		if(driverNames!=null && isNumberSalesColumnType(driverNames.getColumnName())){ //Number column type
	    			selectItems.add(new SelectItem(driverNames.getColumnName(),driverNames.getDescription()));
	    		}
	    	}

	    	auditTransactionNumberItems = selectItems;
		}
		return auditTransactionNumberItems;
	}
	
	public List<SelectItem> getAuditTransactionTextItems() {
		if(auditTransactionTextItems==null){
			Map<String,DataDefinitionColumn> propertyMap = cacheManager.getDataDefinitionPropertyMap("TB_PURCHTRANS");
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
	    	selectItems.add(new SelectItem("","Select a Field"));
	    	
	    	for(int i=0; i<TB_TRANSACTION_DETAIL_AUDIT_COLUMNS.length; i++){
	    		DataDefinitionColumn driverNames = propertyMap.get(Util.columnToProperty(TB_TRANSACTION_DETAIL_AUDIT_COLUMNS[i]));
	    		if(driverNames!=null && isTextSalesColumnType(driverNames.getColumnName())){ //Text column type
	    			selectItems.add(new SelectItem(driverNames.getColumnName(),driverNames.getDescription()));
	    		}
	    	}

	    	auditTransactionTextItems = selectItems;
		}
		return auditTransactionTextItems;		
	}
	
	public List<SelectItem> getAuditTransactionDateItems() {
		if(auditTransactionDateItems==null){
			Map<String,DataDefinitionColumn> propertyMap = cacheManager.getDataDefinitionPropertyMap("TB_PURCHTRANS");
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
	    	selectItems.add(new SelectItem("","Select a Field"));
	    	
	    	for(int i=0; i<TB_TRANSACTION_DETAIL_AUDIT_COLUMNS.length; i++){
	    		DataDefinitionColumn driverNames = propertyMap.get(Util.columnToProperty(TB_TRANSACTION_DETAIL_AUDIT_COLUMNS[i]));
	    		if(driverNames!=null && isDateSalesColumnType(driverNames.getColumnName())){ //Date column type
	    			selectItems.add(new SelectItem(driverNames.getColumnName(),driverNames.getDescription()));
	    		}
	    	}

	    	auditTransactionDateItems = selectItems;
		}
		return auditTransactionDateItems;
	}
	
	public static String[] TB_TRANSACTION_DETAIL_PURCHASEORDER_COLUMNS = { 
			"PO_DATE",
			"PO_LINE_NAME",
			"PO_LINE_NBR",
			"PO_LINE_TYPE",
			"PO_LINE_TYPE_NAME",
			"PO_NAME",
			"PO_NBR"
	};
	
	public List<SelectItem> getPurchaseOrderTransactionItems() {
		if(purchaseOrderTransactionItems==null){
			Map<String,DataDefinitionColumn> propertyMap = cacheManager.getDataDefinitionPropertyMap("TB_PURCHTRANS");
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
	    	selectItems.add(new SelectItem("","Select a Field"));
	    	
	    	for(int i=0; i<TB_TRANSACTION_DETAIL_PURCHASEORDER_COLUMNS.length; i++){
	    		DataDefinitionColumn driverNames = propertyMap.get(Util.columnToProperty(TB_TRANSACTION_DETAIL_PURCHASEORDER_COLUMNS[i]));
	    		if(driverNames!=null){
	    			selectItems.add(new SelectItem(driverNames.getColumnName(),driverNames.getDescription()));
	    		}
	    	}

	    	purchaseOrderTransactionItems = selectItems;
		}
		return purchaseOrderTransactionItems;
	}
	
	public List<SelectItem> getPurchaseOrderTransactionNumberItems() {
		if(purchaseOrderTransactionNumberItems==null){
			Map<String,DataDefinitionColumn> propertyMap = cacheManager.getDataDefinitionPropertyMap("TB_PURCHTRANS");
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
	    	selectItems.add(new SelectItem("","Select a Field"));
	    	
	    	for(int i=0; i<TB_TRANSACTION_DETAIL_PURCHASEORDER_COLUMNS.length; i++){
	    		DataDefinitionColumn driverNames = propertyMap.get(Util.columnToProperty(TB_TRANSACTION_DETAIL_PURCHASEORDER_COLUMNS[i]));
	    		if(driverNames!=null && isNumberSalesColumnType(driverNames.getColumnName())){ //Number column type
	    			selectItems.add(new SelectItem(driverNames.getColumnName(),driverNames.getDescription()));
	    		}
	    	}

	    	purchaseOrderTransactionNumberItems = selectItems;
		}
		return purchaseOrderTransactionNumberItems;
	}
	
	public List<SelectItem> getPurchaseOrderTransactionTextItems() {
		if(purchaseOrderTransactionTextItems==null){
			Map<String,DataDefinitionColumn> propertyMap = cacheManager.getDataDefinitionPropertyMap("TB_PURCHTRANS");
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
	    	selectItems.add(new SelectItem("","Select a Field"));
	    	
	    	for(int i=0; i<TB_TRANSACTION_DETAIL_PURCHASEORDER_COLUMNS.length; i++){
	    		DataDefinitionColumn driverNames = propertyMap.get(Util.columnToProperty(TB_TRANSACTION_DETAIL_PURCHASEORDER_COLUMNS[i]));
	    		if(driverNames!=null && isTextSalesColumnType(driverNames.getColumnName())){ //Text column type
	    			selectItems.add(new SelectItem(driverNames.getColumnName(),driverNames.getDescription()));
	    		}
	    	}

	    	purchaseOrderTransactionTextItems = selectItems;
		}
		return purchaseOrderTransactionTextItems;		
	}
	
	public List<SelectItem> getPurchaseOrderTransactionDateItems() {
		if(purchaseOrderTransactionDateItems==null){
			Map<String,DataDefinitionColumn> propertyMap = cacheManager.getDataDefinitionPropertyMap("TB_PURCHTRANS");
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
	    	selectItems.add(new SelectItem("","Select a Field"));
	    	
	    	for(int i=0; i<TB_TRANSACTION_DETAIL_PURCHASEORDER_COLUMNS.length; i++){
	    		DataDefinitionColumn driverNames = propertyMap.get(Util.columnToProperty(TB_TRANSACTION_DETAIL_PURCHASEORDER_COLUMNS[i]));
	    		if(driverNames!=null && isDateSalesColumnType(driverNames.getColumnName())){ //Date column type
	    			selectItems.add(new SelectItem(driverNames.getColumnName(),driverNames.getDescription()));
	    		}
	    	}

	    	purchaseOrderTransactionDateItems = selectItems;
		}
		return purchaseOrderTransactionDateItems;
	}
	
	public static String[] TB_TRANSACTION_DETAIL_INVENTORY_COLUMNS = { 
			"INVENTORY_CLASS",
			"INVENTORY_CLASS_NAME",
			"INVENTORY_NAME",
			"INVENTORY_NBR"
	};
	
	public List<SelectItem> getInventoryTransactionItems() {
		if(inventoryTransactionItems==null){
			Map<String,DataDefinitionColumn> propertyMap = cacheManager.getDataDefinitionPropertyMap("TB_PURCHTRANS");
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
	    	selectItems.add(new SelectItem("","Select a Field"));
	    	
	    	for(int i=0; i<TB_TRANSACTION_DETAIL_INVENTORY_COLUMNS.length; i++){
	    		DataDefinitionColumn driverNames = propertyMap.get(Util.columnToProperty(TB_TRANSACTION_DETAIL_INVENTORY_COLUMNS[i]));
	    		if(driverNames!=null){
	    			selectItems.add(new SelectItem(driverNames.getColumnName(),driverNames.getDescription()));
	    		}
	    	}

	    	inventoryTransactionItems = selectItems;
		}
		return inventoryTransactionItems;
	}
	
	public List<SelectItem> getInventoryTransactionNumberItems() {
		if(inventoryTransactionNumberItems==null){
			Map<String,DataDefinitionColumn> propertyMap = cacheManager.getDataDefinitionPropertyMap("TB_PURCHTRANS");
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
	    	selectItems.add(new SelectItem("","Select a Field"));
	    	
	    	for(int i=0; i<TB_TRANSACTION_DETAIL_INVENTORY_COLUMNS.length; i++){
	    		DataDefinitionColumn driverNames = propertyMap.get(Util.columnToProperty(TB_TRANSACTION_DETAIL_INVENTORY_COLUMNS[i]));
	    		if(driverNames!=null && isNumberSalesColumnType(driverNames.getColumnName())){ //Number column type
	    			selectItems.add(new SelectItem(driverNames.getColumnName(),driverNames.getDescription()));
	    		}
	    	}

	    	inventoryTransactionNumberItems = selectItems;
		}
		return inventoryTransactionNumberItems;
	}
	
	public List<SelectItem> getInventoryTransactionTextItems() {
		if(inventoryTransactionTextItems==null){
			Map<String,DataDefinitionColumn> propertyMap = cacheManager.getDataDefinitionPropertyMap("TB_PURCHTRANS");
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
	    	selectItems.add(new SelectItem("","Select a Field"));
	    	
	    	for(int i=0; i<TB_TRANSACTION_DETAIL_INVENTORY_COLUMNS.length; i++){
	    		DataDefinitionColumn driverNames = propertyMap.get(Util.columnToProperty(TB_TRANSACTION_DETAIL_INVENTORY_COLUMNS[i]));
	    		if(driverNames!=null && isTextSalesColumnType(driverNames.getColumnName())){ //Text column type
	    			selectItems.add(new SelectItem(driverNames.getColumnName(),driverNames.getDescription()));
	    		}
	    	}

	    	inventoryTransactionTextItems = selectItems;
		}
		return inventoryTransactionTextItems;		
	}
	
	public List<SelectItem> getInventoryTransactionDateItems() {
		if(inventoryTransactionDateItems==null){
			Map<String,DataDefinitionColumn> propertyMap = cacheManager.getDataDefinitionPropertyMap("TB_PURCHTRANS");
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
	    	selectItems.add(new SelectItem("","Select a Field"));
	    	
	    	for(int i=0; i<TB_TRANSACTION_DETAIL_INVENTORY_COLUMNS.length; i++){
	    		DataDefinitionColumn driverNames = propertyMap.get(Util.columnToProperty(TB_TRANSACTION_DETAIL_INVENTORY_COLUMNS[i]));
	    		if(driverNames!=null && isDateSalesColumnType(driverNames.getColumnName())){ //Date column type
	    			selectItems.add(new SelectItem(driverNames.getColumnName(),driverNames.getDescription()));
	    		}
	    	}

	    	inventoryTransactionDateItems = selectItems;
		}
		return inventoryTransactionDateItems;
	}
	
	public static String[] TB_TRANSACTION_DETAIL_PROJECT_COLUMNS = { 
			"AFE_CATEGORY_NAME",
			"AFE_CATEGORY_NBR",
			"AFE_CONTRACT_STRUCTURE",
			"AFE_CONTRACT_TYPE",
			"AFE_PROJECT_NAME",
			"AFE_PROJECT_NBR",
			"AFE_PROPERTY_CAT",
			"AFE_SUB_CAT_NAME",
			"AFE_SUB_CAT_NBR",
			"AFE_USE"	
	};
	
	public List<SelectItem> getProjectTransactionItems() {
		if(projectTransactionItems==null){
			Map<String,DataDefinitionColumn> propertyMap = cacheManager.getDataDefinitionPropertyMap("TB_PURCHTRANS");
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
	    	selectItems.add(new SelectItem("","Select a Field"));
	    	
	    	for(int i=0; i<TB_TRANSACTION_DETAIL_PROJECT_COLUMNS.length; i++){
	    		DataDefinitionColumn driverNames = propertyMap.get(Util.columnToProperty(TB_TRANSACTION_DETAIL_PROJECT_COLUMNS[i]));
	    		if(driverNames!=null){
	    			selectItems.add(new SelectItem(driverNames.getColumnName(),driverNames.getDescription()));
	    		}
	    	}

	    	projectTransactionItems = selectItems;
		}
		return projectTransactionItems;
	}
	
	public List<SelectItem> getProjectTransactionNumberItems() {
		if(projectTransactionNumberItems==null){
			Map<String,DataDefinitionColumn> propertyMap = cacheManager.getDataDefinitionPropertyMap("TB_PURCHTRANS");
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
	    	selectItems.add(new SelectItem("","Select a Field"));
	    	
	    	for(int i=0; i<TB_TRANSACTION_DETAIL_PROJECT_COLUMNS.length; i++){
	    		DataDefinitionColumn driverNames = propertyMap.get(Util.columnToProperty(TB_TRANSACTION_DETAIL_PROJECT_COLUMNS[i]));
	    		if(driverNames!=null && isNumberSalesColumnType(driverNames.getColumnName())){ //Number column type
	    			selectItems.add(new SelectItem(driverNames.getColumnName(),driverNames.getDescription()));
	    		}
	    	}

	    	projectTransactionNumberItems = selectItems;
		}
		return projectTransactionNumberItems;
	}
	
	public List<SelectItem> getProjectTransactionTextItems() {
		if(projectTransactionTextItems==null){
			Map<String,DataDefinitionColumn> propertyMap = cacheManager.getDataDefinitionPropertyMap("TB_PURCHTRANS");
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
	    	selectItems.add(new SelectItem("","Select a Field"));
	    	
	    	for(int i=0; i<TB_TRANSACTION_DETAIL_PROJECT_COLUMNS.length; i++){
	    		DataDefinitionColumn driverNames = propertyMap.get(Util.columnToProperty(TB_TRANSACTION_DETAIL_PROJECT_COLUMNS[i]));
	    		if(driverNames!=null && isTextSalesColumnType(driverNames.getColumnName())){ //Text column type
	    			selectItems.add(new SelectItem(driverNames.getColumnName(),driverNames.getDescription()));
	    		}
	    	}

	    	projectTransactionTextItems = selectItems;
		}
		return projectTransactionTextItems;		
	}
	
	public List<SelectItem> getProjectTransactionDateItems() {
		if(projectTransactionDateItems==null){
			Map<String,DataDefinitionColumn> propertyMap = cacheManager.getDataDefinitionPropertyMap("TB_PURCHTRANS");
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
	    	selectItems.add(new SelectItem("","Select a Field"));
	    	
	    	for(int i=0; i<TB_TRANSACTION_DETAIL_PROJECT_COLUMNS.length; i++){
	    		DataDefinitionColumn driverNames = propertyMap.get(Util.columnToProperty(TB_TRANSACTION_DETAIL_PROJECT_COLUMNS[i]));
	    		if(driverNames!=null && isDateSalesColumnType(driverNames.getColumnName())){ //Date column type
	    			selectItems.add(new SelectItem(driverNames.getColumnName(),driverNames.getDescription()));
	    		}
	    	}

	    	projectTransactionDateItems = selectItems;
		}
		return projectTransactionDateItems;
	}
	
	public static String[] TB_TRANSACTION_DETAIL_USER_COLUMNS = { 
			"USER_DATE_01",
			"USER_DATE_02",
			"USER_DATE_03",
			"USER_DATE_04",
			"USER_DATE_05",
			"USER_DATE_06",
			"USER_DATE_07",
			"USER_DATE_08",
			"USER_DATE_09",
			"USER_DATE_10",
			"USER_DATE_11","USER_DATE_12","USER_DATE_13","USER_DATE_14","USER_DATE_15","USER_DATE_16","USER_DATE_17","USER_DATE_18","USER_DATE_19","USER_DATE_20","USER_DATE_21",
			"USER_DATE_22","USER_DATE_23","USER_DATE_24","USER_DATE_25",
			"USER_NUMBER_01",
			"USER_NUMBER_02",
			"USER_NUMBER_03",
			"USER_NUMBER_04",
			"USER_NUMBER_05",
			"USER_NUMBER_06",
			"USER_NUMBER_07",
			"USER_NUMBER_08",
			"USER_NUMBER_09",
			"USER_NUMBER_10",
			"USER_NUMBER_11","USER_NUMBER_12","USER_NUMBER_13","USER_NUMBER_14","USER_NUMBER_15","USER_NUMBER_16","USER_NUMBER_17","USER_NUMBER_18","USER_NUMBER_19","USER_NUMBER_20",
			"USER_NUMBER_21","USER_NUMBER_22","USER_NUMBER_23","USER_NUMBER_24","USER_NUMBER_25",
			"USER_TEXT_01",
			"USER_TEXT_02",
			"USER_TEXT_03",
			"USER_TEXT_04",
			"USER_TEXT_05",
			"USER_TEXT_06",
			"USER_TEXT_07",
			"USER_TEXT_08",
			"USER_TEXT_09",
			"USER_TEXT_10",
			"USER_TEXT_11",
			"USER_TEXT_12",
			"USER_TEXT_13",
			"USER_TEXT_14",
			"USER_TEXT_15",
			"USER_TEXT_16",
			"USER_TEXT_17",
			"USER_TEXT_18",
			"USER_TEXT_19",
			"USER_TEXT_20",
			"USER_TEXT_21",
			"USER_TEXT_22",
			"USER_TEXT_23",
			"USER_TEXT_24",
			"USER_TEXT_25",
			"USER_TEXT_26",
			"USER_TEXT_27",
			"USER_TEXT_28",
			"USER_TEXT_29",
			"USER_TEXT_30",
			"USER_TEXT_31","USER_TEXT_32","USER_TEXT_33","USER_TEXT_34","USER_TEXT_35","USER_TEXT_36","USER_TEXT_37","USER_TEXT_38","USER_TEXT_39","USER_TEXT_40",
			"USER_TEXT_41","USER_TEXT_42","USER_TEXT_43","USER_TEXT_44","USER_TEXT_45","USER_TEXT_46","USER_TEXT_47","USER_TEXT_48","USER_TEXT_49","USER_TEXT_50",
			"USER_TEXT_51","USER_TEXT_52","USER_TEXT_53","USER_TEXT_54","USER_TEXT_55","USER_TEXT_56","USER_TEXT_57","USER_TEXT_58","USER_TEXT_59","USER_TEXT_60",
			"USER_TEXT_61","USER_TEXT_62","USER_TEXT_63","USER_TEXT_64","USER_TEXT_65","USER_TEXT_66","USER_TEXT_67","USER_TEXT_68","USER_TEXT_69","USER_TEXT_70",
			"USER_TEXT_71","USER_TEXT_72","USER_TEXT_73","USER_TEXT_74","USER_TEXT_75","USER_TEXT_76","USER_TEXT_77","USER_TEXT_78","USER_TEXT_79","USER_TEXT_80",
			"USER_TEXT_81","USER_TEXT_82","USER_TEXT_83","USER_TEXT_84","USER_TEXT_85","USER_TEXT_86","USER_TEXT_87","USER_TEXT_88","USER_TEXT_89","USER_TEXT_90",
			"USER_TEXT_91","USER_TEXT_92","USER_TEXT_93","USER_TEXT_94","USER_TEXT_95","USER_TEXT_96","USER_TEXT_97","USER_TEXT_98","USER_TEXT_99","USER_TEXT_100"
	};
	
	public List<SelectItem> getUserTransactionItems() {
		if(userTransactionItems==null){
			Map<String,DataDefinitionColumn> propertyMap = cacheManager.getDataDefinitionPropertyMap("TB_PURCHTRANS");
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
	    	selectItems.add(new SelectItem("","Select a Field"));
	    	
	    	for(int i=0; i<TB_TRANSACTION_DETAIL_USER_COLUMNS.length; i++){
	    		DataDefinitionColumn driverNames = propertyMap.get(Util.columnToProperty(TB_TRANSACTION_DETAIL_USER_COLUMNS[i]));
	    		if(driverNames!=null){
	    			selectItems.add(new SelectItem(driverNames.getColumnName(),driverNames.getDescription()));
	    		}
	    	}

	    	userTransactionItems = selectItems;
		}
		return userTransactionItems;
	}
	
	public List<SelectItem> getUserTransactionNumberItems() {
		if(userTransactionNumberItems==null){
			Map<String,DataDefinitionColumn> propertyMap = cacheManager.getDataDefinitionPropertyMap("TB_PURCHTRANS");
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
	    	selectItems.add(new SelectItem("","Select a Field"));
	    	
	    	for(int i=0; i<TB_TRANSACTION_DETAIL_USER_COLUMNS.length; i++){
	    		DataDefinitionColumn driverNames = propertyMap.get(Util.columnToProperty(TB_TRANSACTION_DETAIL_USER_COLUMNS[i]));
	    		if(driverNames!=null && isNumberSalesColumnType(driverNames.getColumnName())){ //Number column type
	    			selectItems.add(new SelectItem(driverNames.getColumnName(),driverNames.getDescription()));
	    		}
	    	}

	    	userTransactionNumberItems = selectItems;
		}
		return userTransactionNumberItems;
	}
	
	public List<SelectItem> getUserTransactionTextItems() {
		if(userTransactionTextItems==null){
			Map<String,DataDefinitionColumn> propertyMap = cacheManager.getDataDefinitionPropertyMap("TB_PURCHTRANS");
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
	    	selectItems.add(new SelectItem("","Select a Field"));
	    	
	    	for(int i=0; i<TB_TRANSACTION_DETAIL_USER_COLUMNS.length; i++){
	    		DataDefinitionColumn driverNames = propertyMap.get(Util.columnToProperty(TB_TRANSACTION_DETAIL_USER_COLUMNS[i]));
	    		if(driverNames!=null && isTextSalesColumnType(driverNames.getColumnName())){ //Text column type
	    			selectItems.add(new SelectItem(driverNames.getColumnName(),driverNames.getDescription()));
	    		}
	    	}

	    	userTransactionTextItems = selectItems;
		}
		return userTransactionTextItems;		
	}
	
	public List<SelectItem> getUserTransactionDateItems() {
		if(userTransactionDateItems==null){
			Map<String,DataDefinitionColumn> propertyMap = cacheManager.getDataDefinitionPropertyMap("TB_PURCHTRANS");
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
	    	selectItems.add(new SelectItem("","Select a Field"));
	    	
	    	for(int i=0; i<TB_TRANSACTION_DETAIL_USER_COLUMNS.length; i++){
	    		DataDefinitionColumn driverNames = propertyMap.get(Util.columnToProperty(TB_TRANSACTION_DETAIL_USER_COLUMNS[i]));
	    		if(driverNames!=null && isDateSalesColumnType(driverNames.getColumnName())){ //Date column type
	    			selectItems.add(new SelectItem(driverNames.getColumnName(),driverNames.getDescription()));
	    		}
	    	}

	    	userTransactionDateItems = selectItems;
		}
		return userTransactionDateItems;
	}
	
	public static String[] TB_BILLTRANS_USER_COLUMNS = { 
			"USER_TEXT_01","USER_TEXT_02","USER_TEXT_03","USER_TEXT_04","USER_TEXT_05","USER_TEXT_06","USER_TEXT_07","USER_TEXT_08","USER_TEXT_09","USER_TEXT_10",
			"USER_TEXT_11","USER_TEXT_12","USER_TEXT_13","USER_TEXT_14","USER_TEXT_15","USER_TEXT_16","USER_TEXT_17","USER_TEXT_18","USER_TEXT_19","USER_TEXT_20",
			"USER_TEXT_21","USER_TEXT_22","USER_TEXT_23","USER_TEXT_24","USER_TEXT_25","USER_TEXT_26","USER_TEXT_27","USER_TEXT_28","USER_TEXT_29","USER_TEXT_30",		
			"USER_TEXT_31","USER_TEXT_32","USER_TEXT_33","USER_TEXT_34","USER_TEXT_35","USER_TEXT_36","USER_TEXT_37","USER_TEXT_38","USER_TEXT_39","USER_TEXT_40",
			"USER_TEXT_41","USER_TEXT_42","USER_TEXT_43","USER_TEXT_44","USER_TEXT_45","USER_TEXT_46","USER_TEXT_47","USER_TEXT_48","USER_TEXT_49","USER_TEXT_50",
			"USER_TEXT_51","USER_TEXT_52","USER_TEXT_53","USER_TEXT_54","USER_TEXT_55","USER_TEXT_56","USER_TEXT_57","USER_TEXT_58","USER_TEXT_59","USER_TEXT_60",
			"USER_TEXT_61","USER_TEXT_62","USER_TEXT_63","USER_TEXT_64","USER_TEXT_65","USER_TEXT_66","USER_TEXT_67","USER_TEXT_68","USER_TEXT_69","USER_TEXT_70",
			"USER_TEXT_71","USER_TEXT_72","USER_TEXT_73","USER_TEXT_74","USER_TEXT_75","USER_TEXT_76","USER_TEXT_77","USER_TEXT_78","USER_TEXT_79","USER_TEXT_80",
			"USER_TEXT_81","USER_TEXT_82","USER_TEXT_83","USER_TEXT_84","USER_TEXT_85","USER_TEXT_86","USER_TEXT_87","USER_TEXT_88","USER_TEXT_89","USER_TEXT_90",
			"USER_TEXT_91","USER_TEXT_92","USER_TEXT_93","USER_TEXT_94","USER_TEXT_95","USER_TEXT_96","USER_TEXT_97","USER_TEXT_98","USER_TEXT_99","USER_TEXT_100",			
			"USER_NUMBER_01","USER_NUMBER_02","USER_NUMBER_03","USER_NUMBER_04","USER_NUMBER_05","USER_NUMBER_06","USER_NUMBER_07","USER_NUMBER_08","USER_NUMBER_09","USER_NUMBER_10",
			"USER_NUMBER_11","USER_NUMBER_12","USER_NUMBER_13","USER_NUMBER_14","USER_NUMBER_15","USER_NUMBER_16","USER_NUMBER_17","USER_NUMBER_18","USER_NUMBER_19","USER_NUMBER_20",
			"USER_NUMBER_21","USER_NUMBER_22","USER_NUMBER_23","USER_NUMBER_24","USER_NUMBER_25",
			"USER_DATE_01","USER_DATE_02","USER_DATE_03","USER_DATE_04","USER_DATE_05","USER_DATE_06","USER_DATE_07","USER_DATE_08","USER_DATE_09","USER_DATE_10",
			"USER_DATE_11","USER_DATE_12","USER_DATE_13","USER_DATE_14","USER_DATE_15","USER_DATE_16","USER_DATE_17","USER_DATE_18","USER_DATE_19","USER_DATE_20","USER_DATE_21",
			"USER_DATE_22","USER_DATE_23","USER_DATE_24","USER_DATE_25"
	};
	
	public List<SelectItem> getUserSaleTransItems() {
		if(userSaleTransItems==null){
			Map<String,DataDefinitionColumn> propertyMap = cacheManager.getDataDefinitionPropertyMap("TB_BILLTRANS");
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
	    	selectItems.add(new SelectItem("","Select a Field"));
	    	
	    	for(int i=0; i<TB_BILLTRANS_USER_COLUMNS.length; i++){
	    		DataDefinitionColumn driverNames = propertyMap.get(Util.columnToProperty(TB_BILLTRANS_USER_COLUMNS[i]));
	    		if(driverNames!=null){
	    			selectItems.add(new SelectItem(driverNames.getColumnName(),driverNames.getDescription()));
	    		}
	    	}

	    	userSaleTransItems = selectItems;
		}
		return userSaleTransItems;
	}
	
	public List<SelectItem> getUserSaleTransNumberItems() {
		if(userSaleTransNumberItems==null){
			Map<String,DataDefinitionColumn> propertyMap = cacheManager.getDataDefinitionPropertyMap("TB_BILLTRANS");
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
	    	selectItems.add(new SelectItem("","Select a Field"));
	    	
	    	for(int i=0; i<TB_BILLTRANS_USER_COLUMNS.length; i++){
	    		DataDefinitionColumn driverNames = propertyMap.get(Util.columnToProperty(TB_BILLTRANS_USER_COLUMNS[i]));
	    		if(driverNames!=null && isNumberSalesColumnType(driverNames.getColumnName())){ //Number column type
	    			selectItems.add(new SelectItem(driverNames.getColumnName(),driverNames.getDescription()));
	    		}
	    	}

	    	userSaleTransNumberItems = selectItems;
		}
		return userSaleTransNumberItems;
	}
	
	public List<SelectItem> getUserSaleTransTextItems() {
		if(userSaleTransTextItems==null){
			Map<String,DataDefinitionColumn> propertyMap = cacheManager.getDataDefinitionPropertyMap("TB_BILLTRANS");
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
	    	selectItems.add(new SelectItem("","Select a Field"));
	    	
	    	for(int i=0; i<TB_BILLTRANS_USER_COLUMNS.length; i++){
	    		DataDefinitionColumn driverNames = propertyMap.get(Util.columnToProperty(TB_BILLTRANS_USER_COLUMNS[i]));
	    		if(driverNames!=null && isTextSalesColumnType(driverNames.getColumnName())){ //Text column type
	    			selectItems.add(new SelectItem(driverNames.getColumnName(),driverNames.getDescription()));
	    		}
	    	}

	    	userSaleTransTextItems = selectItems;
		}
		return userSaleTransTextItems;
	}
	
	public List<SelectItem> getUserSaleTransDateItems() {
		if(userSaleTransDateItems==null){
			Map<String,DataDefinitionColumn> propertyMap = cacheManager.getDataDefinitionPropertyMap("TB_BILLTRANS");
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
	    	selectItems.add(new SelectItem("","Select a Field"));
	    	
	    	for(int i=0; i<TB_BILLTRANS_USER_COLUMNS.length; i++){
	    		DataDefinitionColumn driverNames = propertyMap.get(Util.columnToProperty(TB_BILLTRANS_USER_COLUMNS[i]));
	    		if(driverNames!=null && isDateSalesColumnType(driverNames.getColumnName())){ //Date column type
	    			selectItems.add(new SelectItem(driverNames.getColumnName(),driverNames.getDescription()));
	    		}
	    	}

	    	userSaleTransDateItems = selectItems;
		}
		return userSaleTransDateItems;
	}
	
	public List<SelectItem> getNullSelectionItems(){
		List<SelectItem> selectItems = new ArrayList<SelectItem>();
	    selectItems.add(new SelectItem("","Select a Field"));
		return selectItems;
	}

	public List<SelectItem> getOperationGroupItems() {	
		if(getIsPurchasingMenu()){
			if(operationGroupItems==null){
				List<SelectItem> selectItems = new ArrayList<SelectItem>();
				selectItems.add(new SelectItem("ALL","All"));			
				selectItems.add(new SelectItem("TRANS_AUDIT","Audit"));
				selectItems.add(new SelectItem("TRANS_GENERALLEDGER","General Ledger"));
				selectItems.add(new SelectItem("TRANS_INVENTORY","Inventory"));
				selectItems.add(new SelectItem("TRANS_INVOICE","Invoice"));
				selectItems.add(new SelectItem("TRANS_LOCATION","Location"));
				selectItems.add(new SelectItem("TRANS_PAYMENTINFO","Payment Info"));
				selectItems.add(new SelectItem("TRANS_PROJECT","Project"));
				selectItems.add(new SelectItem("TRANS_PURCHASEORDER","Purchase Order"));
				selectItems.add(new SelectItem("TRANS_TRANSACTION","Transaction"));
				selectItems.add(new SelectItem("TRANS_USER","User"));
				selectItems.add(new SelectItem("TRANS_VENDOR","Vendor"));
				selectItems.add(new SelectItem("TRANS_WORKORDER","Work Order"));			
		    	operationGroupItems = selectItems;
			}
			return operationGroupItems;
		}
		else{
			if(operationGroupSalesItems==null){
				List<SelectItem> selectItems = new ArrayList<SelectItem>();
				selectItems.add(new SelectItem("ALL","All"));
		    	selectItems.add(new SelectItem("SALE_BASIC","Basic"));
		    	selectItems.add(new SelectItem("SALE_CUSTOMER","Customer"));
		    	selectItems.add(new SelectItem("SALE_INVOICE","Invoice"));
		    	selectItems.add(new SelectItem("SALE_LOCATION","Location"));
		    	selectItems.add(new SelectItem("SALE_ENTITY","Entity"));
		    	selectItems.add(new SelectItem("SALE_USER","User"));	    	
		    	selectItems.add(new SelectItem("SALE_TRANSACTION","Transaction"));
		    	selectItems.add(new SelectItem("SALE_SITUS","Situs"));
		    	selectItems.add(new SelectItem("SALE_AMOUNTS","Amounts"));
		    	selectItems.add(new SelectItem("SALE_GENERALLEDGER","General Ledger"));
		    	
		    	operationGroupSalesItems = selectItems;
			}
			return operationGroupSalesItems;
		}
	}
	
	public List<SelectItem> getOperationSetGroupItems() {
		if(getIsPurchasingMenu()){
			if(operationSetGroupItems==null){
				List<SelectItem> selectItems = new ArrayList<SelectItem>();
				selectItems.add(new SelectItem("ALL","All"));
				selectItems.add(new SelectItem("TRANS_AUDIT","Audit"));
				selectItems.add(new SelectItem("TRANS_GENERALLEDGER","General Ledger"));
				selectItems.add(new SelectItem("TRANS_INVENTORY","Inventory"));
				selectItems.add(new SelectItem("TRANS_INVOICE","Invoice"));
				selectItems.add(new SelectItem("TRANS_LOCATION","Location"));
				selectItems.add(new SelectItem("TRANS_PAYMENTINFO","Payment Info"));
				selectItems.add(new SelectItem("TRANS_PROJECT","Project"));
				selectItems.add(new SelectItem("TRANS_PURCHASEORDER","Purchase Order"));
				selectItems.add(new SelectItem("TRANS_TRANSACTION","Transaction"));
				selectItems.add(new SelectItem("TRANS_USER","User"));
				selectItems.add(new SelectItem("TRANS_VENDOR","Vendor"));
				selectItems.add(new SelectItem("TRANS_WORKORDER","Work Order"));			
		    	selectItems.add(new SelectItem("SPROC","SProc"));
		    	operationSetGroupItems = selectItems;
			}
			return operationSetGroupItems;
		}
		else{
			if(operationSetGroupSalesItems==null){
				List<SelectItem> selectItems = new ArrayList<SelectItem>();
				selectItems.add(new SelectItem("ALL","All"));
		    	selectItems.add(new SelectItem("SALE_BASIC","Basic"));
		    	selectItems.add(new SelectItem("SALE_CUSTOMER","Customer"));
		    	selectItems.add(new SelectItem("SALE_INVOICE","Invoice"));
		    	selectItems.add(new SelectItem("SALE_LOCATION","Location"));
		    	selectItems.add(new SelectItem("SALE_ENTITY","Entity"));
		    	selectItems.add(new SelectItem("SALE_USER","User"));
		    	selectItems.add(new SelectItem("SALE_TRANSACTION","Transaction"));
		    	selectItems.add(new SelectItem("SALE_SITUS","Situs"));
		    	selectItems.add(new SelectItem("SALE_AMOUNTS","Amounts"));
		    	selectItems.add(new SelectItem("SALE_GENERALLEDGER","General Ledger"));
		    	selectItems.add(new SelectItem("SPROC","SProc"));
		    	operationSetGroupSalesItems = selectItems;
			}
			return operationSetGroupSalesItems;
		}
	}
	
	public List<SelectItem> getOperatorItems() {
		if(operatorItems==null){
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
	    	selectItems.add(new SelectItem("AND","And"));
	    	selectItems.add(new SelectItem("OR","Or"));
	    	operatorItems = selectItems;
		}
		return operatorItems;
	}
	
	public List<SelectItem> getRelationItems() {
		if(relationItems==null){
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
			selectItems.add(new SelectItem("=","="));
	    	selectItems.add(new SelectItem("<","<"));
	    	selectItems.add(new SelectItem(">",">"));
	    	selectItems.add(new SelectItem("<=","<="));
	    	selectItems.add(new SelectItem(">=",">="));
	    	selectItems.add(new SelectItem("<>","<>"));	    	
	    	selectItems.add(new SelectItem("IsNull","Is Null"));
	    	selectItems.add(new SelectItem("IsNotNull","Is Not Null"));    	
	    	selectItems.add(new SelectItem("StartsWith","Starts With"));
	    	selectItems.add(new SelectItem("EndsWith","Ends With"));
	    	selectItems.add(new SelectItem("Contains","Contains"));
	    	selectItems.add(new SelectItem("InTable","In Table"));
	    	relationItems = selectItems;
		}
		return relationItems;
	}
	
	public List<SelectItem> getValuelocationWhenItems() {
		if(getIsPurchasingMenu()){
			if(valuelocationWhenItems==null){
				List<SelectItem> selectItems = new ArrayList<SelectItem>();
				selectItems.add(new SelectItem("ALL","All"));			
				selectItems.add(new SelectItem("TRANS_AUDIT","Audit"));
				selectItems.add(new SelectItem("TRANS_GENERALLEDGER","General Ledger"));
				selectItems.add(new SelectItem("TRANS_INVENTORY","Inventory"));
				selectItems.add(new SelectItem("TRANS_INVOICE","Invoice"));
				selectItems.add(new SelectItem("TRANS_LOCATION","Location"));
				selectItems.add(new SelectItem("TRANS_PAYMENTINFO","Payment Info"));
				selectItems.add(new SelectItem("TRANS_PROJECT","Project"));
				selectItems.add(new SelectItem("TRANS_PURCHASEORDER","Purchase Order"));
				selectItems.add(new SelectItem("TRANS_TRANSACTION","Transaction"));
				selectItems.add(new SelectItem("TRANS_USER","User"));
				selectItems.add(new SelectItem("TRANS_VENDOR","Vendor"));
				selectItems.add(new SelectItem("TRANS_WORKORDER","Work Order"));	
				selectItems.add(new SelectItem("VALUE","Value"));
		    	valuelocationWhenItems = selectItems;
			}
			return valuelocationWhenItems;
		}
		else{
			if(valuelocationWhenSalesItems==null){
				List<SelectItem> selectItems = new ArrayList<SelectItem>();
				selectItems.add(new SelectItem("ALL","All"));
		    	selectItems.add(new SelectItem("SALE_BASIC","Basic"));
		    	selectItems.add(new SelectItem("SALE_CUSTOMER","Customer"));
		    	selectItems.add(new SelectItem("SALE_INVOICE","Invoice"));
		    	selectItems.add(new SelectItem("SALE_LOCATION","Location"));
		    	selectItems.add(new SelectItem("SALE_ENTITY","Entity"));
		    	selectItems.add(new SelectItem("SALE_USER","User"));
		    	selectItems.add(new SelectItem("SALE_TRANSACTION","Transaction"));
		    	selectItems.add(new SelectItem("SALE_SITUS","Situs"));
		    	selectItems.add(new SelectItem("SALE_AMOUNTS","Amounts"));
		    	selectItems.add(new SelectItem("SALE_GENERALLEDGER","General Ledger"));
		    	selectItems.add(new SelectItem("VALUE","Value"));
		    	valuelocationWhenSalesItems = selectItems;
			}
			return valuelocationWhenSalesItems;
		}
	}
	
	public List<SelectItem> getNumberOperatorItems() {
		
		if(valueOperatorItems==null){
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
			selectItems.add(new SelectItem("n/a","n/a"));
	    	selectItems.add(new SelectItem("+","+"));
	    	selectItems.add(new SelectItem("-","-"));
	    	selectItems.add(new SelectItem("*","*"));
	    	selectItems.add(new SelectItem("/","/"));
	    	valueOperatorItems = selectItems;
		}
		return valueOperatorItems;
	}
	
	public List<SelectItem> getTextOperatorItems() {
		
		if(textOperatorItems==null){
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
			selectItems.add(new SelectItem("n/a","n/a"));
	    	selectItems.add(new SelectItem("+","+"));
	    	textOperatorItems = selectItems;
		}
		return textOperatorItems;
	}
	
	public List<SelectItem> getNullOperatorItems() {
		List<SelectItem> selectItems = new ArrayList<SelectItem>();
		selectItems.add(new SelectItem("n/a","n/a"));
	    return selectItems;
	}
	
	public List<SelectItem> getValuelocationSetItems() {
		if(getIsPurchasingMenu()){
			if(valuelocationSetItems==null){
				List<SelectItem> selectItems = new ArrayList<SelectItem>();
				selectItems.add(new SelectItem("ALL","All"));			
				selectItems.add(new SelectItem("TRANS_AUDIT","Audit"));
				selectItems.add(new SelectItem("TRANS_GENERALLEDGER","General Ledger"));
				selectItems.add(new SelectItem("TRANS_INVENTORY","Inventory"));
				selectItems.add(new SelectItem("TRANS_INVOICE","Invoice"));
				selectItems.add(new SelectItem("TRANS_LOCATION","Location"));
				selectItems.add(new SelectItem("TRANS_PAYMENTINFO","Payment Info"));
				selectItems.add(new SelectItem("TRANS_PROJECT","Project"));
				selectItems.add(new SelectItem("TRANS_PURCHASEORDER","Purchase Order"));
				selectItems.add(new SelectItem("TRANS_TRANSACTION","Transaction"));
				selectItems.add(new SelectItem("TRANS_USER","User"));
				selectItems.add(new SelectItem("TRANS_VENDOR","Vendor"));
				selectItems.add(new SelectItem("TRANS_WORKORDER","Work Order"));					
		    	selectItems.add(new SelectItem("VALUE","Value"));
		    	selectItems.add(new SelectItem("TABLE","Table"));
		    	valuelocationSetItems = selectItems;
			}
			return valuelocationSetItems;
		}
		else{
			if(valuelocationSetSalesItems==null){
				List<SelectItem> selectItems = new ArrayList<SelectItem>();
				selectItems.add(new SelectItem("ALL","All"));
		    	selectItems.add(new SelectItem("SALE_BASIC","Basic"));
		    	selectItems.add(new SelectItem("SALE_CUSTOMER","Customer"));
		    	selectItems.add(new SelectItem("SALE_INVOICE","Invoice"));
		    	selectItems.add(new SelectItem("SALE_LOCATION","Location"));
		    	selectItems.add(new SelectItem("SALE_ENTITY","Entity"));
		    	selectItems.add(new SelectItem("SALE_USER","User"));
		    	selectItems.add(new SelectItem("SALE_TRANSACTION","Transaction"));
		    	selectItems.add(new SelectItem("SALE_SITUS","Situs"));
		    	selectItems.add(new SelectItem("SALE_AMOUNTS","Amounts"));
		    	selectItems.add(new SelectItem("SALE_GENERALLEDGER","General Ledger"));
		    	selectItems.add(new SelectItem("VALUE","Value"));
		    	selectItems.add(new SelectItem("TABLE","Table"));
		    	valuelocationSetSalesItems = selectItems;
			}
			return valuelocationSetSalesItems;
		}
	}
	
	public List<SelectItem> getOperatorGroupItems() {
		if(getIsPurchasingMenu()){
			if(operatorGroupItems==null){
				List<SelectItem> selectItems = new ArrayList<SelectItem>();
				selectItems.add(new SelectItem("ALL","All"));			
				selectItems.add(new SelectItem("TRANS_AUDIT","Audit"));
				selectItems.add(new SelectItem("TRANS_GENERALLEDGER","General Ledger"));
				selectItems.add(new SelectItem("TRANS_INVENTORY","Inventory"));
				selectItems.add(new SelectItem("TRANS_INVOICE","Invoice"));
				selectItems.add(new SelectItem("TRANS_LOCATION","Location"));
				selectItems.add(new SelectItem("TRANS_PAYMENTINFO","Payment Info"));
				selectItems.add(new SelectItem("TRANS_PROJECT","Project"));
				selectItems.add(new SelectItem("TRANS_PURCHASEORDER","Purchase Order"));
				selectItems.add(new SelectItem("TRANS_TRANSACTION","Transaction"));
				selectItems.add(new SelectItem("TRANS_USER","User"));
				selectItems.add(new SelectItem("TRANS_VENDOR","Vendor"));
				selectItems.add(new SelectItem("TRANS_WORKORDER","Work Order"));		
		    	selectItems.add(new SelectItem("VALUE","Value"));
		    	operatorGroupItems = selectItems;
			}
			return operatorGroupItems;
		}
		else{
			if(operatorGroupSalesItems==null){
				List<SelectItem> selectItems = new ArrayList<SelectItem>();
				selectItems.add(new SelectItem("ALL","All"));
		    	selectItems.add(new SelectItem("SALE_BASIC","Basic"));
		    	selectItems.add(new SelectItem("SALE_CUSTOMER","Customer"));
		    	selectItems.add(new SelectItem("SALE_INVOICE","Invoice"));
		    	selectItems.add(new SelectItem("SALE_LOCATION","Location"));
		    	selectItems.add(new SelectItem("SALE_ENTITY","Entity"));
		    	selectItems.add(new SelectItem("SALE_USER","User"));
		    	selectItems.add(new SelectItem("SALE_TRANSACTION","Transaction"));
		    	selectItems.add(new SelectItem("SALE_SITUS","Situs"));
		    	selectItems.add(new SelectItem("SALE_AMOUNTS","Amounts"));
		    	selectItems.add(new SelectItem("SALE_GENERALLEDGER","General Ledger"));
		    	selectItems.add(new SelectItem("VALUE","Value"));
		    	operatorGroupSalesItems = selectItems;
			}
			return operatorGroupSalesItems;
		}
	}
	

	public List<SelectItem> getTalbeSelectionItems() {		
		if(talbeSelectionItems==null){
			List<Proctable> findAll = this.proctableService.findAll();
			if(findAll!=null){
				Proctable aProctable = null;
				List<SelectItem> selectItems = new ArrayList<SelectItem>();
				selectItems.add(new SelectItem("", "Select a Table"));
			    for(int i=0; i<findAll.size(); i++ ){
			    	aProctable = (Proctable)findAll.get(i);
			    	
			    	if(getIsPurchasingMenu()){
			    		if(aProctable.getModuleCode().equalsIgnoreCase("PURCHUSE")){
			    			selectItems.add(new SelectItem(aProctable.getId().toString(), aProctable.getDescription()));
			    		}
					}
					else{
						if(aProctable.getModuleCode().equalsIgnoreCase("BILLSALE")){
			    			selectItems.add(new SelectItem(aProctable.getId().toString(), aProctable.getDescription()));
			    		}
					}
			    }
			    talbeSelectionItems = selectItems;
			}	
		}
		return talbeSelectionItems;
	}
	
	public Integer getFirstWhenObject() {
		if(whenObjectItems!=null && whenObjectItems.size()>=1){
			return new Integer(0);
		}
		else{
			return new Integer(-1);
		}
	}
	
	public Integer getLastWhenObject() {
		if(whenObjectItems!=null && whenObjectItems.size()>=1){
			return new Integer(whenObjectItems.size() - 1);
		}
		else{
			return new Integer(-1);
		}
	}
	
	public Integer getFirstSetObject() {
		if(setObjectItems!=null && setObjectItems.size()>=1){
			return new Integer(0);
		}
		else{
			return new Integer(-1);
		}
	}
	
	public Integer getLastSetObject() {
		if(setObjectItems!=null && setObjectItems.size()>=1){
			return new Integer(setObjectItems.size() - 1);
		}
		else{
			return new Integer(-1);
		}
	}
	
	public void addWhenItem(ActionEvent e) throws AbortProcessingException {
		
		WhenObject aWhenObject = new WhenObject();
		if(getIsPurchasingMenu()){
			aWhenObject.setOperationWhen("ALL"); //Temporarily default to ALL for purchasing
		}
		whenObjectItems.add(aWhenObject);
	}
	
	public void processAddWhenCommand(ActionEvent event) {
	    String command = (String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("command");
	    
	    WhenObject aWhenObject = new WhenObject();
	    if(getIsPurchasingMenu()){
			aWhenObject.setOperationWhen("ALL"); //Temporarily default to ALL for purchasing
		}
		whenObjectItems.add(aWhenObject);

	    return;
	}
	
	public void processRemoveWhenCommand(ActionEvent event) {
	    String command = (String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("command");
	    
	    int commandInt = 0;
	    try{
	    	commandInt = Integer.parseInt(command);
	    }
	    catch(Exception e){
	    	return;
	    }
	    
	    whenObjectItems.remove(commandInt);

	    return;
	}
	
	public void processAddSetCommand(ActionEvent event) {
	    String command = (String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("command");
	    
	    SetObject aSetObject = new SetObject();
	    if(getIsPurchasingMenu()){
			aSetObject.setOperationSet("ALL"); //Temporarily default to ALL for purchasing
		}
		setObjectItems.add(aSetObject);

	    return;
	}
	
	public void processRemoveSetCommand(ActionEvent event) {
	    String command = (String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("command");
	    
	    int commandInt = 0;
	    try{
	    	commandInt = Integer.parseInt(command);
	    }
	    catch(Exception e){
	    	return;
	    }
	    
	    setObjectItems.remove(commandInt);

	    return;
	}
	
	private Proctable selectedProctable;
	
	public Proctable getSelectedProctable() {
		return selectedProctable;
	}

	public void setSelectedProctable(Proctable selectedProctable) {
		this.selectedProctable = selectedProctable;
	}
	
	private SetObject selectedSetObjectForParameters = null;
	
	public String processEditSetCommand() {
		String result = null;
		radioSetSelected = "";
		
		String command = (String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("command");
    
	    int commandInt = 0;
	    try{
	    	commandInt = Integer.parseInt(command);

	    	if(setObjectItems==null || setObjectItems.size()==0){
		    	return null;//Impossible
		    }
		    
		    SetObject  aSetObject = (SetObject)setObjectItems.get(commandInt);
		    selectedSetObjectForParameters = aSetObject;
		    if(aSetObject.getFieldTextSet() !=null && aSetObject.getFieldTextSet().length()>0){
		    	
		    	int tableId = 0;
		    	tableId = Integer.parseInt(aSetObject.getFieldTextSet());	
		    	selectedProctable = this.proctableService.findById(new Long(tableId));
				if(selectedProctable==null || selectedProctable.getProctableId()==null){
					return null;
				}
				
				
				SelectObject aSelectObject = null;
				String columnType = "";
				String columnName = "";

				//For In Columns
		    	inSelectObject= new ArrayList<SelectObject>();
		    	outSelectObject= new ArrayList<SelectObject>();
   	
		    	Class<?> cls = Proctable.class;
				NumberFormat formatter = new DecimalFormat("00");
				String methodName = null;
				Method m = null;

		    	try {
					for(int i=0; i<20; i++){
						aSelectObject = new SelectObject();

						methodName = "getInColumnType" + formatter.format(i+1); 
						m = cls.getMethod(methodName, new Class<?> [] {});
						if (m != null) {
							Object val = m.invoke(selectedProctable, new Object [] {});
							if (val != null){
								columnType = (String)val;
							}
							else{
								break;
							}
						}
						
						methodName = "getInColumnName" + formatter.format(i+1); 
						m = cls.getMethod(methodName, new Class<?> [] {});
						if (m != null) {
							Object val = m.invoke(selectedProctable, new Object [] {});
							if (val != null){
								columnName = (String)val;
							}
							else{
								break;
							}
						}
						
						if(columnType.equalsIgnoreCase("F")){
							//aSelectObject.setColumnName(matrixCommonBean.getSalesColumnNameMap().get(columnName));						
							if(getIsPurchasingMenu()){
								aSelectObject.setColumnName(matrixCommonBean.getTransactionColumnNameMap().get(columnName));
							}
							else{
								aSelectObject.setColumnName(matrixCommonBean.getSalesColumnNameMap().get(columnName));
							}
							
							aSelectObject.setDisplayCheck(true);
							aSelectObject.setSelected(false);
						}
						else{//"V"
							aSelectObject.setColumnName(columnName);
							aSelectObject.setDisplayCheck(false);
							aSelectObject.setSelected(true);//*NULL
							aSelectObject.setColumnValue("*NULL");
						}
						
						inSelectObject.add(aSelectObject);		
					}
					
					for(int i=0; i<10; i++){
						aSelectObject = new SelectObject();

						methodName = "getOutColumnName" + formatter.format(i+1); 
						m = cls.getMethod(methodName, new Class<?> [] {});
						if (m != null) {
							Object val = m.invoke(selectedProctable, new Object [] {});
							if (val != null) {
								columnName = (String)val;
							}
							else{
								break;
							}
						}

						aSelectObject.setColumnName(columnName);
						aSelectObject.setDisplayCheck(true);
						aSelectObject.setSelected(false);

						outSelectObject.add(aSelectObject);		
					}
				} catch (Exception ex) {
					ex.printStackTrace();
					return null;
				}
		    	result = "proctable_select";
		    }
	    }
	    catch(Exception e){
	    	e.printStackTrace();
	    }
		   
        return result;
	}
	
	public void selectTableCheckChange(ActionEvent e) {
		String command = (String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("command");

	    int commandInt = 0;
	    try{
	    	commandInt = Integer.parseInt(command);
	    }
	    catch(Exception ee){
	    	return;
	    }
	    
	    if(inSelectObject==null || inSelectObject.size()==0){
	    	return;//Impossible
	    }
	    
	    SelectObject  aSelectObject = (SelectObject)inSelectObject.get(commandInt);
	    aSelectObject.setColumnValue("");

	}
	
	public String selectTableCheckChange() {
		String command = (String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("command");

	    int commandInt = 0;
	    try{
	    	commandInt = Integer.parseInt(command);
	    }
	    catch(Exception e){
	    	return null;
	    }
	    
	    if(inSelectObject==null || inSelectObject.size()==0){
	    	return null;//Impossible
	    }
	    
	    SelectObject  aSelectObject = (SelectObject)inSelectObject.get(commandInt);
	    aSelectObject.setColumnValue("");

	    return null;
	}
	
	public String okSelectProcessAction() {
		String result = null;
		try {	
			
			if(validateSelectProcess()){
				return "";
			}
			
			
			//Get result
			String resultValue = "";
			
			SelectObject aSelectObject = null;
			for(int i=0; i< inSelectObject.size(); i++){
				if(resultValue.length()>0){
					resultValue += ",";
				}
				
				aSelectObject = (SelectObject)inSelectObject.get(i);
				if(aSelectObject.getSelected()){
					resultValue += "'" + aSelectObject.getColumnValue() + "'";					
				}
				else{
					resultValue += "*FIELD";
				}
			}
			
			int radioSetSelectedInt = 0;
		    try{
		    	radioSetSelectedInt = Integer.parseInt(radioSetSelected);
		    }
		    catch(Exception e){
		    }
			
			aSelectObject = (SelectObject)outSelectObject.get(radioSetSelectedInt);
			resultValue = aSelectObject.getColumnName() + "(" + resultValue + ")";

			//Update selected TB_PROCRULE_DETAIL:TABLE_PARMS
			selectedSetObjectForParameters.setParamTextSet(resultValue);

			//Refresh & Retrieve
			//retrieveFilterProctable();
			
		    result = "procrule_update";		
		} catch (Exception ex) {
			ex.printStackTrace();
			result = "";
		}
		   
        return result;
	}
	
	boolean validateSelectProcess(){
		boolean errorMessage = false;
		
		SelectObject aSelectObject = null;	
		for(int i=0; i< inSelectObject.size(); i++){
			aSelectObject = (SelectObject)inSelectObject.get(i);
			if(aSelectObject.getSelected() && (aSelectObject.getColumnValue()==null || aSelectObject.getColumnValue().length()==0)){
				errorMessage = true;
				
				FacesMessage message = new FacesMessage(aSelectObject.getColumnName() + ": the value cannot be empty.");
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage(null, message);		
			}
		}
		
		int radioSetSelectedInt = -1;
	    try{
	    	radioSetSelectedInt = Integer.parseInt(radioSetSelected);
	    }
	    catch(Exception e){
	    }
		
		if(radioSetSelectedInt==-1){
			errorMessage = true;
			
			FacesMessage message = new FacesMessage("One and only one Out Column must be selecteded.");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);		
		}

		return errorMessage;
	}
	
	public void processEditSetCommand(ActionEvent event) {
	    String command = (String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("command");
	      
	    int commandInt = 0;
	    try{
	    	commandInt = Integer.parseInt(command);
	    }
	    catch(Exception e){
	    	return;
	    }
	
	    return;
	}
	
	public Integer getFirstReorderProcruleObject() {
		if(reorderProcruleArray!=null && reorderProcruleArray.size()>=1){
			return new Integer(0);
		}
		else{
			return new Integer(-1);
		}
	}
	
	public Integer getLastReorderProcruleObject() {
		if(reorderProcruleArray!=null && reorderProcruleArray.size()>=1){
			return new Integer(reorderProcruleArray.size() - 1);
		}
		else{
			return new Integer(-1);
		}
	}
	
	public void processUpReorderCommand(ActionEvent event) {
	    String command = (String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("command");
	    
	    int commandInt = 0;
	    try{
	    	commandInt = Integer.parseInt(command);
	    }
	    catch(Exception e){
	    	return;
	    }
	    
	    if(reorderProcruleArray==null || reorderProcruleArray.size()==0){
	    	return;//Impossible
	    }
	    
	    List<Procrule> arrangeObjectItems= new ArrayList<Procrule>();
	    for(int i=0; i<reorderProcruleArray.size(); i++ ){
	    	if(i == (commandInt-1)){
	    		arrangeObjectItems.add(reorderProcruleArray.get(commandInt));
	    	}
	    	else if(i == commandInt){
	    		arrangeObjectItems.add(reorderProcruleArray.get(commandInt-1));
	    	}
	    	else{
	    		arrangeObjectItems.add(reorderProcruleArray.get(i));
	    	}
	    }
	    
	    reorderProcruleArray = arrangeObjectItems;

	    return;
	}
	
	public void processDownReorderCommand(ActionEvent event) {
	    String command = (String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("command");
	    
	    int commandInt = 0;
	    try{
	    	commandInt = Integer.parseInt(command);
	    }
	    catch(Exception e){
	    	return;
	    }
	    
	    if(reorderProcruleArray==null || reorderProcruleArray.size()==0){
	    	return;//Impossible
	    }
	    
	    List<Procrule> arrangeObjectItems= new ArrayList<Procrule>();
	    for(int i=0; i<reorderProcruleArray.size(); i++ ){
	    	if(i == commandInt){
	    		arrangeObjectItems.add(reorderProcruleArray.get(commandInt+1));
	    	}
	    	else if(i == (commandInt+1)){
	    		arrangeObjectItems.add(reorderProcruleArray.get(commandInt));
	    	}
	    	else{
	    		arrangeObjectItems.add(reorderProcruleArray.get(i));
	    	}
	    }
	    
	    reorderProcruleArray = arrangeObjectItems;

	    return;
	}
	
	public void processUpUpReorderCommand(ActionEvent event) {
	    String command = (String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("command");
	    
	    int commandInt = 0;
	    try{
	    	commandInt = Integer.parseInt(command);
	    }
	    catch(Exception e){
	    	return;
	    }
	    
	    if(reorderProcruleArray==null || reorderProcruleArray.size()==0){
	    	return;//Impossible
	    }
	    
	    List<Procrule> arrangeObjectItems= new ArrayList<Procrule>();
	    //Add to the top
	    arrangeObjectItems.add(reorderProcruleArray.get(commandInt));
	    for(int i=0; i<reorderProcruleArray.size(); i++ ){	    	
	    	if(i == commandInt){
	    		//Has been moved to the top
	    	}
	    	else{
	    		arrangeObjectItems.add(reorderProcruleArray.get(i));
	    	}
	    }
	    
	    reorderProcruleArray = arrangeObjectItems;

	    return;
	}
	
	public String processDownDownReorderCommand() {
	    String command = (String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("command");
	    
	    int commandInt = 0;
	    try{
	    	commandInt = Integer.parseInt(command);
	    }
	    catch(Exception e){
	    	return null;
	    }
	    
	    if(reorderProcruleArray==null || reorderProcruleArray.size()==0){
	    	return null;//Impossible
	    }
	    
	    List<Procrule> arrangeObjectItems= new ArrayList<Procrule>();
	    for(int i=0; i<reorderProcruleArray.size(); i++ ){	
	    	if(i == commandInt){
	    		//Has not been moved to the bottom yet
	    	}
	    	else{
	    		arrangeObjectItems.add(reorderProcruleArray.get(i));
	    	}
	    }
	    //Add to the bottom
	    arrangeObjectItems.add(reorderProcruleArray.get(commandInt));
	    
	    reorderProcruleArray = arrangeObjectItems;

	    return null;
	}
	
	public void processDownDownReorderCommand(ActionEvent event) {
	    String command = (String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("command");
	    
	    int commandInt = 0;
	    try{
	    	commandInt = Integer.parseInt(command);
	    }
	    catch(Exception e){
	    	return;
	    }
	    
	    if(reorderProcruleArray==null || reorderProcruleArray.size()==0){
	    	return;//Impossible
	    }
	    
	    List<Procrule> arrangeObjectItems= new ArrayList<Procrule>();
	    for(int i=0; i<reorderProcruleArray.size(); i++ ){    	
	    	if(i == commandInt){
	    		//Has not been moved to the bottom yet
	    	}
	    	else{
	    		arrangeObjectItems.add(reorderProcruleArray.get(i));
	    	}
	    }
	    //Add to the bottom
	    arrangeObjectItems.add(reorderProcruleArray.get(commandInt));
	    
	    reorderProcruleArray = arrangeObjectItems;

	    return;
	}
	
	public void processUpWhenCommand(ActionEvent event) {
	    String command = (String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("command");
	    
	    int commandInt = 0;
	    try{
	    	commandInt = Integer.parseInt(command);
	    }
	    catch(Exception e){
	    	return;
	    }
	    
	    if(whenObjectItems==null || whenObjectItems.size()==0 || commandInt==0){
	    	return;//Impossible
	    }
	    
	    List<WhenObject> arrangeObjectItems= new ArrayList<WhenObject>();
	    for(int i=0; i<whenObjectItems.size(); i++ ){
	    	if(i == (commandInt-1)){
	    		arrangeObjectItems.add(whenObjectItems.get(commandInt));
	    	}
	    	else if(i == commandInt){
	    		arrangeObjectItems.add(whenObjectItems.get(commandInt-1));
	    	}
	    	else{
	    		arrangeObjectItems.add(whenObjectItems.get(i));
	    	}
	    }
	    
	    whenObjectItems = arrangeObjectItems;

	    return;
	}
	
	public void processDownWhenCommand(ActionEvent event) {
	    String command = (String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("command");
	    
	    int commandInt = 0;
	    try{
	    	commandInt = Integer.parseInt(command);
	    }
	    catch(Exception e){
	    	return;
	    }
	    
	    if(whenObjectItems==null || whenObjectItems.size()==0 || commandInt==(whenObjectItems.size()-1)){
	    	return;//Impossible
	    }
	    
	    List<WhenObject> arrangeObjectItems= new ArrayList<WhenObject>();
	    for(int i=0; i<whenObjectItems.size(); i++ ){
	    	if(i == commandInt){
	    		arrangeObjectItems.add(whenObjectItems.get(commandInt+1));
	    	}
	    	else if(i == (commandInt+1)){
	    		arrangeObjectItems.add(whenObjectItems.get(commandInt));
	    	}
	    	else{
	    		arrangeObjectItems.add(whenObjectItems.get(i));
	    	}
	    }
	    
	    whenObjectItems = arrangeObjectItems;

	    return;
	}
	
	public void processOpenWhenCommand(ActionEvent event) {
	    String command = (String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("command");
	    
	    int commandInt = 0;
	    try{
	    	commandInt = Integer.parseInt(command);
	    }
	    catch(Exception e){
	    	return;
	    }
	    
	    if(whenObjectItems==null || whenObjectItems.size()==0){
	    	return;//Impossible
	    }
	    
	    WhenObject  aWhenObject = (WhenObject)whenObjectItems.get(commandInt);
	    aWhenObject.setOpenparenWhen(!aWhenObject.getOpenparenWhen());

	    return;
	}
	
	public void processCloseWhenCommand(ActionEvent event) {
	    String command = (String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("command");
	    
	    int commandInt = 0;
	    try{
	    	commandInt = Integer.parseInt(command);
	    }
	    catch(Exception e){
	    	return;
	    }
	    
	    if(whenObjectItems==null || whenObjectItems.size()==0){
	    	return;//Impossible
	    }
	    
	    WhenObject  aWhenObject = (WhenObject)whenObjectItems.get(commandInt);
	    aWhenObject.setCloseparenWhen(!aWhenObject.getCloseparenWhen());

	    return;
	}
	
	public String relationWhenChange() {
		String command = (String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("command");
	    
	    int commandInt = 0;
	    try{
	    	commandInt = Integer.parseInt(command);
	    }
	    catch(Exception e){
	    	return null;
	    }
	    
	    if(whenObjectItems==null || whenObjectItems.size()==0){
	    	return null;//Impossible
	    }
	    
	    
	    WhenObject  aWhenObject = (WhenObject)whenObjectItems.get(commandInt);
	    
	    if(aWhenObject.getRelationWhen().equalsIgnoreCase("IsNull") || aWhenObject.getRelationWhen().equalsIgnoreCase("IsNotNull") || aWhenObject.getRelationWhen().equalsIgnoreCase("StartsWith") 
	    		|| aWhenObject.getRelationWhen().equalsIgnoreCase("EndsWith") || aWhenObject.getRelationWhen().equalsIgnoreCase("Contains") ){
	    
	    	aWhenObject.setValuelocationWhen("VALUE");
	    	aWhenObject.setFieldColumnWhen("");
	    	aWhenObject.setFieldTextWhen("");
	    }

	    return null;
	}
	
	
	public String valuelocationWhenChange() {
		String command = (String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("command");
	    
	    int commandInt = 0;
	    try{
	    	commandInt = Integer.parseInt(command);
	    }
	    catch(Exception e){
	    	return null;
	    }
	    
	    if(whenObjectItems==null || whenObjectItems.size()==0){
	    	return null;//Impossible
	    }
	    
	    WhenObject  aWhenObject = (WhenObject)whenObjectItems.get(commandInt);
	    aWhenObject.setFieldColumnWhen("");
	    aWhenObject.setFieldTextWhen("");

	    return null;
	}
	
	public String operatorGroupSetChange() {
		String command = (String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("command");
	    
	    int commandInt = 0;
	    try{
	    	commandInt = Integer.parseInt(command);
	    }
	    catch(Exception e){
	    	return null;
	    }
	    
	    if(setObjectItems==null || setObjectItems.size()==0){
	    	return null;//Impossible
	    }
	    
	    SetObject  aSetObject = (SetObject)setObjectItems.get(commandInt);
	    aSetObject.setOperatorColumnSet("");
	    aSetObject.setOperatorTextSet("");
	    
	    return null;
	}
	
	public String valuelocationSetChange() {
		String command = (String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("command");
	    
	    int commandInt = 0;
	    try{
	    	commandInt = Integer.parseInt(command);
	    }
	    catch(Exception e){
	    	return null;
	    }
	    
	    if(setObjectItems==null || setObjectItems.size()==0){
	    	return null;//Impossible
	    }
	    
	    SetObject  aSetObject = (SetObject)setObjectItems.get(commandInt);
	    aSetObject.setFieldColumnSet("");
	    aSetObject.setFieldTextSet("");
	    
	    //0002936
	    //aSetObject.setNullColumnType();
	    //aSetObject.setTextColumnType();
	    
	    aSetObject.setOperator("");
	 //   aSetObject.setOperatorSet("");
	    aSetObject.setOperatorColumnSet("");
	    aSetObject.setOperatorTextSet("");
	    
	    if(aSetObject.getValuelocationSet().equalsIgnoreCase("TABLE")){
	    	talbeSelectionItems=null;
	    }

	    return null;
	}
	
	public boolean isTextSalesColumnType(String columnName){
		String transactionTable = "";
		if(getIsPurchasingMenu()){
			transactionTable = "TB_PURCHTRANS";
		}
		else{
			transactionTable = "TB_BILLTRANS";
		}
		
		if(columnName==null || columnName.length()==0){
			return false;
		}

		Map<String,DataDefinitionColumn> propertyMap = cacheManager.getDataDefinitionPropertyMap(transactionTable);
		DataDefinitionColumn driverName = propertyMap.get(Util.columnToProperty(columnName));
		if(driverName!=null && driverName.getDataType()!=null && driverName.getDataType().length()>0){
			if(driverName.getDataType().equalsIgnoreCase("VARCHAR2") || driverName.getDataType().equalsIgnoreCase("CHAR")){
				return true;
			}
		}	
		return false;
	}
	
	public boolean isNumberSalesColumnType(String columnName){
		String transactionTable = "";
		if(getIsPurchasingMenu()){
			transactionTable = "TB_PURCHTRANS";
		}
		else{
			transactionTable = "TB_BILLTRANS";
		}
		
		if(columnName==null || columnName.length()==0){
			return false;
		}
		
		Map<String,DataDefinitionColumn> propertyMap = cacheManager.getDataDefinitionPropertyMap(transactionTable);
		DataDefinitionColumn driverName = propertyMap.get(Util.columnToProperty(columnName));
		if(driverName!=null && driverName.getDataType()!=null && driverName.getDataType().length()>0){
			if(driverName.getDataType().equalsIgnoreCase("NUMBER")){
				return true;
			}
		}	
		return false;
	}
	
	public boolean isDateSalesColumnType(String columnName){
		String transactionTable = "";
		if(getIsPurchasingMenu()){
			transactionTable = "TB_PURCHTRANS";
		}
		else{
			transactionTable = "TB_BILLTRANS";
		}
		
		if(columnName==null || columnName.length()==0){
			return false;
		}
		
		Map<String,DataDefinitionColumn> propertyMap = cacheManager.getDataDefinitionPropertyMap(transactionTable);
		DataDefinitionColumn driverName = propertyMap.get(Util.columnToProperty(columnName));
		if(driverName!=null && driverName.getDataType()!=null && driverName.getDataType().length()>0){
			if(driverName.getDataType().equalsIgnoreCase("DATE")){
				return true;
			}
		}	
		return false;
	}
	
	public String fieldWhenChange() {
		String command = (String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("command");
	    
	    int commandInt = 0;
	    try{
	    	commandInt = Integer.parseInt(command);
	    }
	    catch(Exception e){
	    	return null;
	    }
	    
	    if(whenObjectItems==null || whenObjectItems.size()==0){
	    	return null;//Impossible
	    }
	    
	    WhenObject  aWhenObject = (WhenObject)whenObjectItems.get(commandInt);
	   
	    String columnName = aWhenObject.getFieldWhen();
	    
	   
	    if(isNumberSalesColumnType(columnName)){
	    	if(aWhenObject.getIsNumberColumnType()){//no change
	    		
	    	}
	    	else {
	    		aWhenObject.setNumberColumnType();
	    		//Clear all
	    		aWhenObject.setFieldColumnWhen("");
	    	    aWhenObject.setFieldTextWhen("");
	    //	    aWhenObject.setOperator("");
	    	    //aSetObject.setOperatorSet("VALUE");
	    //	    aWhenObject.setOperatorColumnWhen("");
	    //	    aWhenObject.setOperatorTextSet("");
	    	}
	    }
	    else if(isTextSalesColumnType(columnName)){
	    	if(aWhenObject.getIsTextColumnType()){//no change
	    		
	    	}
	    	else {
	    		aWhenObject.setTextColumnType();
	    		//Clear all
	    		aWhenObject.setFieldColumnWhen("");
	    	    aWhenObject.setFieldTextWhen("");
	    	//    aWhenObject.setOperator("");
	    	    //aSetObject.setOperatorSet("VALUE");
	    	//    aWhenObject.setOperatorColumnSet("");
	    	//    aWhenObject.setOperatorTextSet("");
	    	}
	    	
	    }
	    else if(isDateSalesColumnType(columnName)){
	    	if(aWhenObject.getIsDateColumnType()){//no change
	    		
	    	}
	    	else {
	    		aWhenObject.setDateColumnType();
	    		//Clear all
	    		aWhenObject.setFieldColumnWhen("");
	    	    aWhenObject.setFieldTextWhen("");
	    	//    aWhenObject.setOperator("");
	    	//    aWhenObject.setOperatorSet("VALUE");
	    	//    aWhenObject.setOperatorColumnSet("");
	    	//    aWhenObject.setOperatorTextSet("");
	    	}
	    }

	    return null;
	}
	
	public String fieldSetChange() {
		String command = (String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("command");
	    
	    int commandInt = 0;
	    try{
	    	commandInt = Integer.parseInt(command);
	    }
	    catch(Exception e){
	    	return null;
	    }
	    
	    if(setObjectItems==null || setObjectItems.size()==0){
	    	return null;//Impossible
	    }
	    
	    SetObject  aSetObject = (SetObject)setObjectItems.get(commandInt);
	   
	    String columnName = aSetObject.getFieldSet();
	    
	   
	    if(isNumberSalesColumnType(columnName)){
	    	if(aSetObject.getIsNumberColumnType()){//no change
	    		
	    	}
	    	else {
	    		aSetObject.setNumberColumnType();
	    		//Clear all
	    		aSetObject.setFieldColumnSet("");
	    	    aSetObject.setFieldTextSet("");
	    	    aSetObject.setOperator("");
	    	    //aSetObject.setOperatorSet("VALUE");
	    	    aSetObject.setOperatorColumnSet("");
	    	    aSetObject.setOperatorTextSet("");
	    	}
	    }
	    else if(isTextSalesColumnType(columnName)){
	    	if(aSetObject.getIsTextColumnType()){//no change
	    		
	    	}
	    	else {
	    		aSetObject.setTextColumnType();
	    		//Clear all
	    		aSetObject.setFieldColumnSet("");
	    	    aSetObject.setFieldTextSet("");
	    	    aSetObject.setOperator("");
	    	    //aSetObject.setOperatorSet("VALUE");
	    	    aSetObject.setOperatorColumnSet("");
	    	    aSetObject.setOperatorTextSet("");
	    	}
	    	
	    }
	    else if(isDateSalesColumnType(columnName)){
	    	if(aSetObject.getIsDateColumnType()){//no change
	    		
	    	}
	    	else {
	    		aSetObject.setDateColumnType();
	    		//Clear all
	    		aSetObject.setFieldColumnSet("");
	    	    aSetObject.setFieldTextSet("");
	    	    aSetObject.setOperator("");
	    	    aSetObject.setOperatorSet("VALUE");
	    	    aSetObject.setOperatorColumnSet("");
	    	    aSetObject.setOperatorTextSet("");
	    	}
	    }
	    else{
	    	aSetObject.setNullColumnType();;
    		//Clear all
    		aSetObject.setFieldColumnSet("");
    	    aSetObject.setFieldTextSet("");
    	    aSetObject.setOperator("");
    	    aSetObject.setOperatorSet("VALUE");
    	    aSetObject.setOperatorColumnSet("");
    	    aSetObject.setOperatorTextSet("");
	    }

	    return null;
	}
	
	public String operatorSetChange() {
		String command = (String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("command");
	    
	    int commandInt = 0;
	    try{
	    	commandInt = Integer.parseInt(command);
	    }
	    catch(Exception e){
	    	return null;
	    }
	    
	    if(setObjectItems==null || setObjectItems.size()==0){
	    	return null;//Impossible
	    }
		    
	    SetObject  aSetObject = (SetObject)setObjectItems.get(commandInt);
	    aSetObject.setOperatorSet("VALUE");
	    aSetObject.setOperatorColumnSet("");
	    aSetObject.setOperatorTextSet("");

	    return null;
	}
	
	public String tableNameChange() {
		String command = (String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("command");
	    
	    int commandInt = 0;
	    try{
	    	commandInt = Integer.parseInt(command);
	    }
	    catch(Exception e){
	    	return null;
	    }
	    
	    if(setObjectItems==null || setObjectItems.size()==0){
	    	return null;//Impossible
	    }
	    
	    SetObject  aSetObject = (SetObject)setObjectItems.get(commandInt);
	    aSetObject.setParamTextSet("");

	    return null;
	}
	
	public String valueCategoryWhenChange() {
		String command = (String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("command");
	    
	    int commandInt = 0;
	    try{
	    	commandInt = Integer.parseInt(command);
	    }
	    catch(Exception e){
	    	return null;
	    }
	    
	    if(whenObjectItems==null || whenObjectItems.size()==0){
	    	return null;//Impossible
	    }
	    
	    WhenObject  aWhenObject = (WhenObject)whenObjectItems.get(commandInt);
	    aWhenObject.setFieldWhen("");

	    return null;
	}

	public String valueCategorySetChange() {
		String command = (String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("command");
	    
	    int commandInt = 0;
	    try{
	    	commandInt = Integer.parseInt(command);
	    }
	    catch(Exception e){
	    	return null;
	    }
	    
	    if(setObjectItems==null || setObjectItems.size()==0){
	    	return null;//Impossible
	    }
	    
	    SetObject  aSetObject = (SetObject)setObjectItems.get(commandInt);
	    aSetObject.setFieldSet("");

	    return null;
	}
	
	public String valueAlwaysApplyChange() {
		if(updateProcrule.getAlwaysBooleanFlag()){
			if(whenObjectItems!=null && whenObjectItems.size()>0){
				whenObjectItems= new ArrayList<WhenObject>();
			}
			
			updateProcrule.setReprocessCode("*NONE");
		}
		else{
			WhenObject aWhenObject = new WhenObject();
			if(getIsPurchasingMenu()){
				aWhenObject.setOperationWhen("ALL"); //Temporarily default to ALL for purchasing
			}
			whenObjectItems.add(aWhenObject);
		}

	    return null;
	}

	public class WhenObject{
		private String operatorWhen = "AND";
		private Boolean openparenWhen = false;
		private String operationWhen = "SALE_BASIC";
		private String fieldWhen = "";
		private String relationWhen = "EQUALITY";		
		private String valuelocationWhen = "VALUE";
		private String fieldColumnWhen = "";
		private String fieldTextWhen = "";
		private Boolean closeparenWhen = false;
		private String selectedColumnType = "";		
		private String fieldTableWhen = "";
		
		public Boolean getIsFieldValueBoxDisabled(){
			if(relationWhen.equalsIgnoreCase("IsNull") || relationWhen.equalsIgnoreCase("IsNotNull") || relationWhen.equalsIgnoreCase("StartsWith") 
	    		|| relationWhen.equalsIgnoreCase("EndsWith") || relationWhen.equalsIgnoreCase("Contains") ){
				return true;
			}
			else{
				return false;
			}
		}
	    
		public Boolean getIsFieldValueDisabled(){
			if(relationWhen.equalsIgnoreCase("IsNull") || relationWhen.equalsIgnoreCase("IsNotNull")){
				return true;
			}
			else{
				return false;
			}
		}

		public Boolean getIsValueSelection(){
			return (valuelocationWhen.equalsIgnoreCase("VALUE")) ? true : false;
		}
		
		public String getFieldWhen() {
			return this.fieldWhen;
		}

		public void setFieldWhen(String fieldWhen) {
			this.fieldWhen = fieldWhen;
		}
		
		public String getFieldColumnWhen() {
			return this.fieldColumnWhen;
		}

		public void setFieldColumnWhen(String fieldColumnWhen) {
			this.fieldColumnWhen = fieldColumnWhen;
		}
		
		public String getFieldTableWhen() {
			return this.fieldTableWhen;
		}

		public void setFieldTableWhen(String fieldTableWhen) {
			this.fieldTableWhen = fieldTableWhen;
		}
		
		public Boolean getOpenparenWhen() {
			return this.openparenWhen;
		}

		public void setOpenparenWhen(Boolean openparenWhen) {
			this.openparenWhen = openparenWhen;
		} 
		
		public Boolean getCloseparenWhen() {
			return this.closeparenWhen;
		}

		public void setCloseparenWhen(Boolean closeparenWhen) {
			this.closeparenWhen = closeparenWhen;
		} 
		
		public String getValuelocationWhen() {
			return this.valuelocationWhen;
		}

		public void setValuelocationWhen(String valuelocationWhen) {
			this.valuelocationWhen= valuelocationWhen;
		}

		public String getOperatorWhen() {
			return this.operatorWhen;
		}

		public void setOperatorWhen(String operatorWhen) {
			this.operatorWhen = operatorWhen;
		}
		
		public String getRelationWhen() {
			return this.relationWhen;
		}

		public void setRelationWhen(String relationWhen) {
			this.relationWhen = relationWhen;
		}
		
		public String getOperationWhen() {
			return this.operationWhen;
		}

		public void setOperationWhen(String operationWhen) {
			this.operationWhen = operationWhen;
		}
		
		public String getFieldTextWhen() {
			return this.fieldTextWhen;
		}

		public void setFieldTextWhen(String fieldTextWhen) {
			this.fieldTextWhen = fieldTextWhen;
		}
		
		public void setNumberColumnType() {
			this.selectedColumnType = "NUMBER";
		}
		
		public void setTextColumnType() {
			this.selectedColumnType = "TEXT";
		}
		
		public void setDateColumnType() {
			this.selectedColumnType = "DATE";
		}
		
		public void setNullColumnType() {
			this.selectedColumnType = "";
		}
		
		public boolean getIsNumberColumnType() {
			return (selectedColumnType.equals("NUMBER"));
		}
		
		public boolean getIsTextColumnType() {
			return (selectedColumnType.equals("TEXT"));
		}
		
		public boolean getIsDateColumnType() {
			return (selectedColumnType.equals("DATE"));
		}
		
		public boolean getIsNullColumnType() {
			return (!selectedColumnType.equals("DATE") && !selectedColumnType.equals("NUMBER") && !selectedColumnType.equals("TEXT"));
		}
	}
	
	public class SetObject{
		
		private String operationSet = "SALE_BASIC";
		private String fieldSet = "";
		private String valuelocationSet = "VALUE";
		private String fieldColumnSet = "";
		private String fieldTextSet = "";
		private String paramTextSet = "";
		//For operator
		private String operator = "";
		private String operatorSet = "VALUE";
		private String operatorColumnSet = "";
		private String operatorTextSet = "";
		private String selectedColumnType = "";
		
		private String sprocName = "";
		private String sprocParameters = "";
		
		public boolean getIsNAOperator() {
			return (operator == null || operator.length()==0 || operator.equals("n/a"));
		}	
			
		public void setNumberColumnType() {
			this.selectedColumnType = "NUMBER";
		}
		
		public void setTextColumnType() {
			this.selectedColumnType = "TEXT";
		}
		
		public void setDateColumnType() {
			this.selectedColumnType = "DATE";
		}
		
		public void setNullColumnType() {
			this.selectedColumnType = "";
		}
		
		public boolean getIsNumberColumnType() {
			return (selectedColumnType.equals("NUMBER"));
		}
		
		public boolean getIsTextColumnType() {
			return (selectedColumnType.equals("TEXT"));
		}
		
		public boolean getIsDateColumnType() {
			return (selectedColumnType.equals("DATE"));
		}
		
		public boolean getIsNullColumnType() {
			return (!selectedColumnType.equals("DATE") && !selectedColumnType.equals("NUMBER") && !selectedColumnType.equals("TEXT"));
		}
		
		public Boolean getIsValueSelection(){
			return (valuelocationSet.equalsIgnoreCase("VALUE")) ? true : false;
		}
		
		public String getFieldSet() {
			return this.fieldSet;
		}

		public void setFieldSet(String fieldSet) {
			this.fieldSet = fieldSet;
		}
		
		public String getFieldColumnSet() {
			return this.fieldColumnSet;
		}

		public void setFieldColumnSet(String fieldColumnSet) {
			this.fieldColumnSet = fieldColumnSet;
		}
		
		public String getOperatorColumnSet() {
			return this.operatorColumnSet;
		}

		public void setOperatorColumnSet(String operatorColumnSet) {
			this.operatorColumnSet = operatorColumnSet;
		}


		public String getValuelocationSet() {
			return this.valuelocationSet;
		}

		public void setValuelocationSet(String valuelocationSet) {
			this.valuelocationSet= valuelocationSet;
		}

		public String getOperationSet() {
			return this.operationSet;
		}

		public void setOperationSet(String operationSet) {
			this.operationSet = operationSet;
		}
		
		public String getOperator() {
			return this.operator;
		}

		public void setOperator(String operator) {
			this.operator = operator;
		}
		
		//
		public String getOperatorSet() {
			return this.operatorSet;
		}

		public void setOperatorSet(String operatorSet) {
			this.operatorSet = operatorSet;
		}
		
		public String getFieldTextSet() {
			return this.fieldTextSet;
		}

		public void setFieldTextSet(String fieldTextSet) {
			this.fieldTextSet = fieldTextSet;
		}
		
		public String getSprocName() {
			return this.sprocName;
		}

		public void setSprocName(String sprocName) {
			this.sprocName = sprocName;
		}
		
		public String getSprocParameters() {
			return this.sprocParameters;
		}

		public void setSprocParameters(String sprocParameters) {
			this.sprocParameters = sprocParameters;
		}
		
		public String getOperatorTextSet() {
			return this.operatorTextSet;
		}

		public void setOperatorTextSet(String operatorTextSet) {
			this.operatorTextSet = operatorTextSet;
		}

		public String getParamTextSet() {
			return this.paramTextSet;
		}

		public void setParamTextSet(String paramTextSet) {
			this.paramTextSet = paramTextSet;
		}
	}
	
	public class SelectObject{
		private Boolean selected = false;
		private String valuelocationSet = "VALUE";
		private String columnName = "";
		private String columnValue = "";
		private Boolean displayCheck = false;
		
		public Boolean getSelected() {
			return this.selected;
		}

		public void setSelected(Boolean selected) {
			this.selected = selected;
		}
		
		public String getColumnName() {
			return this.columnName;
		}

		public void setColumnName(String columnName) {
			this.columnName = columnName;
		}

		public String getValuelocationSet() {
			return this.valuelocationSet;
		}

		public void setValuelocationSet(String valuelocationSet) {
			this.valuelocationSet= valuelocationSet;
		}
		
		public String getColumnValue() {
			return this.columnValue;
		}

		public void setColumnValue(String columnValue) {
			this.columnValue = columnValue;
		}

		public Boolean getDisplayCheck() {
			return this.displayCheck;
		}

		public void setDisplayCheck(Boolean displayCheck) {
			this.displayCheck = displayCheck;
		}
	}	
}
