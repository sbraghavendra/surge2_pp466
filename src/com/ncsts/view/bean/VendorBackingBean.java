package com.ncsts.view.bean;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;

import org.ajax4jsf.component.html.HtmlAjaxSupport;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.richfaces.component.html.HtmlDataTable;
import org.richfaces.model.TreeNode;
import org.richfaces.model.TreeNodeImpl;
import org.springframework.beans.factory.annotation.Autowired;

import com.ncsts.common.LoggerFactory;
import com.ncsts.domain.DataDefinitionColumn;
import com.ncsts.domain.EntityDefault;
import com.ncsts.domain.EntityItem;
import com.ncsts.domain.EntityLevel;
import com.ncsts.domain.EntityLocnSet;
import com.ncsts.domain.EntityLocnSetDtl;
import com.ncsts.domain.Jurisdiction;
import com.ncsts.domain.ListCodes;
import com.ncsts.domain.Menu;
import com.ncsts.domain.Role;
import com.ncsts.domain.User;
import com.ncsts.domain.UserEntity;
import com.ncsts.domain.VendorItem;
import com.ncsts.dto.EntityDefaultDTO;
import com.ncsts.dto.EntityLocnSetDTO;
import com.ncsts.dto.EntityLocnSetDtlDTO;
import com.ncsts.dto.VendorItemDTO;
import com.ncsts.jsf.model.VendorDataModel;
import com.ncsts.management.UserSecurityBean;
import com.ncsts.services.JurisdictionService;
import com.ncsts.services.RoleService;
import com.ncsts.services.UserEntityService;
import com.ncsts.services.UserService;
import com.ncsts.services.VendorItemService;
import com.ncsts.view.util.ConfigSetting;
import com.ncsts.view.util.TogglePanelController;

public class VendorBackingBean {
    
	private Logger logger = LoggerFactory.getInstance().getLogger(EntityBackingBean.class);
	
	public interface FindVendorIdCallback {
		void findVendorIdCallback(Long id, String code, String context);
	}
	
	private FindVendorIdCallback findIdCallback = null;
    
    @Autowired
    private NexusVendorBackingBean nexusVendorBackingBean;
   
    @Autowired
    private CustomerLocationSearchBean custLoc;

    private VendorDataModel vendorDataModel;
    
	private EditAction currentAction = EditAction.VIEW;
    
    public static String MAP_MAPPED = "mapped";
    public static String MAP_SELECTED = "selected";
    public static String MAP_NONE = "";
    
    public static final String ROLE_EXCEPTION_TYPE = "";
    public static final String GRANTED_EXCEPTION_TYPE = "G";
    public static final String DENIED_EXCEPTION_TYPE = "D";
    
	private List<User> usersList = new ArrayList<User>();
	private List<Role> rolesList = new ArrayList<Role>();
	private List<Menu> menusList = new ArrayList<Menu>();		
	
	private List<DataDefinitionColumn> dataDefinitionColList = new ArrayList<DataDefinitionColumn>();
    private UserService userService;       
    private RoleService roleService;
    private VendorItemService vendorItemService; 
    private UserEntityService userEntityService;
    private String rerender; 
    private EntityItem selectedEntity;
    private EntityLevel selectedEntityLevel;
    private UserEntity selectedUserEntity;    
    private String mode;
    private boolean transColDisplay=false;
    private boolean displayUpdateButton = false;
    private boolean displayAddButton = false;
    private boolean updateDisplay=false;
    private boolean disableDeleteButton=false;
    private boolean showUpdateButton=false;
    private boolean showOkButton = false;
	private VendorItemDTO vendorItemDTO = new VendorItemDTO();;
	private ConfigSetting userConfig;
	private int selectedRowIndex = -1;
	private int selectedEntityDefaultRowIndex = -1;
	private int selectedLocationListRowIndex = -1;
	private int selectedLocationListDetailRowIndex = -1;
	private loginBean loginBean;
	private UserSecurityBean userSecurityBean;
    private VendorItem selectedEntityItem = null;
    private VendorItem selectedGridVendorItem = null;
    private EntityDefaultDTO entityDefaultDTO;
    private EntityLocnSetDTO entityLocnSetDTO;
    private EntityLocnSetDtlDTO entityLocnSetDtlDTO;
    private EntityDefault selectedEntityDefault = null;
    private EntityLocnSet selectedEntityLocnSet = null;
    private EntityLocnSetDtl selectedEntityLocnSetDtl = null;  
    private Map<Long, EntityItem > globalEntityArray = new LinkedHashMap<Long, EntityItem >();
    private SearchJurisdictionHandler filterHandler = new SearchJurisdictionHandler();
    private JurisdictionService jurisdictionService;
    private boolean entityLocked = false;
    private Long entityCopyFromSelected = null;
    private Long entityParentSelected = null;
    private List<SelectItem> copyFromList = null;
    private List<SelectItem> entityLevelList = null;
    private List<SelectItem> defaultTypeList = null; 
    private List<SelectItem> defaultCodeList = null;
    private List<SelectItem> defaultValueList = null;
    private String showhide="";
	private String findIdAction;
	private String findIdContext;
    private String selectedRowKey =null;
    private String returnView=null;
    
    private TaxJurisdictionBackingBean taxJurisdictionBean;
    protected MatrixCommonBean matrixCommonBean;
    
    private String addressLine1 = "";
	private String addressLine2 = "";
	private String vendorCode = "";
	private String vendorName = "";
	
	private SearchJurisdictionHandler filterHandlerShipto = new SearchJurisdictionHandler(true);
	private TogglePanelController togglePanelController = null;
	
	public  TogglePanelController getTogglePanelController(){
		if(togglePanelController==null){
			togglePanelController = new TogglePanelController();
			togglePanelController.addTogglePanel("VendorInformationPanel", true);
		}
		return togglePanelController;
	}
	
	public void setTogglePanelController(TogglePanelController togglePanelController){
		this.togglePanelController = togglePanelController;;
	}
    
    public TaxJurisdictionBackingBean getTaxJurisdictionBean() {
		return taxJurisdictionBean;
	}
    
    public MatrixCommonBean getMatrixCommonBean() {
		return matrixCommonBean;
	}

	public void setMatrixCommonBean(MatrixCommonBean matrixCommonBean) {
		this.matrixCommonBean = matrixCommonBean;
		filterHandlerShipto.setJurisdictionService(matrixCommonBean.getJurisdictionService());
		filterHandlerShipto.setCacheManager(matrixCommonBean.getCacheManager());
		filterHandlerShipto.setTaxJurisdictionBean(taxJurisdictionBean);
		filterHandlerShipto.setCallbackScreen("vendor_main");
		
		filterHandler.setJurisdictionService(matrixCommonBean.getJurisdictionService());
		filterHandler.setCacheManager(matrixCommonBean.getCacheManager());
		filterHandler.setTaxJurisdictionBean(taxJurisdictionBean);
		filterHandler.setCallbackScreen("vendor_update");
	}

	public void setTaxJurisdictionBean(TaxJurisdictionBackingBean taxJurisdictionBean) {
		this.taxJurisdictionBean = taxJurisdictionBean;
	}
    
    public String getReturnView() {
		return returnView;
	}

	public void setReturnView(String returnView) {
		this.returnView = returnView;
	}

	public String getSelectedRowKey(){
    	return selectedRowKey;
    }
    
    private enum VendorDefaultAction  {
		closeEntityDefaultMain,
		displayAddSingle,
		displayUpdateSingle,
		displayDeleteSingle,
		okSingle,
		cancelSingle,
		closeSingleMain,
		displayAddLocationSet,
		displayUpdateLocationSet,
		displayDeleteLocationSet,
		okLocationSet,
		cancelLocationSet,
		displayAddLocationSetDetail,
		displayUpdateLocationSetDetail,
		displayDeleteLocationSetDetail,
		okLocationSetDetail,
		cancelLocationSetDetail,
		closeLocationSetDetailMain
	}
    
    public void setVendorDataModel(VendorDataModel vendorDataModel) {
		this.vendorDataModel = vendorDataModel;
	}
	
	public VendorDataModel getVendorDataModel() {
		return vendorDataModel;
	}
	
	public void findId(FindVendorIdCallback callback, String context, String action) {
		findIdCallback = callback;
		findIdContext = context;
		findIdAction = action;

		resetFilterSearchAction();
		retrieveFilterSearchAction();
	}

	public String findAction() {
		if(selectedGridVendorItem!=null && selectedGridVendorItem.getVendorId()!=null){
			findIdCallback.findVendorIdCallback(selectedGridVendorItem.getVendorId(), selectedGridVendorItem.getVendorCode(), findIdContext);
		}
		else{
			findIdCallback.findVendorIdCallback(999999999L, "", findIdContext);
		}
	
		exitFindMode();
		return findIdAction;
	}

	public String cancelFindAction() {
		exitFindMode();
		return findIdAction;
	}

	public boolean getFindMode() {
		return (findIdCallback != null);
	}

	public void exitFindMode() {
		if (findIdCallback != null) {
			findIdCallback = null;
		}
	}

	public String navigateAction() {
		exitFindMode();
		return "vendor_main";
	}
    
    public void onpicklistchangedAction(ActionEvent e){
	}
    
    public VendorBackingBean() {
	}
    
    public void init() {
    	filterHandlerShipto.reset();
		filterHandlerShipto.setCountry("");
		filterHandlerShipto.setState("");
    }
    
    public List<SelectItem> getEntityLevelList(){
    	return entityLevelList;
    }
    
    private Long selectedDefaultTypeList = 1L;
    
    public Long getSelectedDefaultTypeList(){
    	return this.selectedDefaultTypeList;
    }
    
    public void setSelectedDefaultTypeList(Long selectedDefaultTypeList){
    	this.selectedDefaultTypeList=selectedDefaultTypeList;
    }
    
    public List<SelectItem> getDefaultTypeList(){
    	if(defaultTypeList==null){
    		defaultTypeList = new ArrayList<SelectItem>();
    		//defaultTypeList.add(new SelectItem(new Long(0L),"Select a Default Type"));
    		defaultTypeList.add(new SelectItem(new Long(1L),"Single Values"));
    		defaultTypeList.add(new SelectItem(new Long(2L),"Location Sets"));
    	}
    	return defaultTypeList;
    }
    
    public List<SelectItem> getDefaultCodeList(){
    	return defaultCodeList;
    }
    
    public List<SelectItem> getDefaultValueList(){
    	if(defaultValueList==null){
    		defaultValueList = new ArrayList<SelectItem>();
    		//defaultValueList.add(new SelectItem(new Long(0L),"Select a Default Value"));
    		defaultValueList.add(new SelectItem(new Long(1L),"Single Values"));
    		defaultValueList.add(new SelectItem(new Long(2L),"Location Sets"));
    	}
    	return defaultValueList;
    }
    
    public String defaultTypeChange() {
		return null;
	}
    
    public boolean getIsEntityDefaultTableMainPanel(){
		if(selectedDefaultTypeList==0L){
			return true;
		}
		else{
			return false;
		}
	}
    
    public boolean getIsEntityDefaultListTablePanel(){
		if(selectedDefaultTypeList==1L){
			return true;
		}
		else{
			return false;
		}
	}
    
    public boolean getIsLocationListTablePanel(){
		if(selectedDefaultTypeList==2L){
			return true;
		}
		else{
			return false;
		}
	}
    
    public void rebuildEntityLevelList(Long parentEntityLevel, boolean isAll){
    	entityLevelList = new ArrayList<SelectItem>();
    	if(isAll){
    		entityLevelList.add(new SelectItem(new Long(0L),"ALL Entities"));
    	}
    	else if(parentEntityLevel.intValue()==0){
    		entityLevelList.add(new SelectItem(new Long(1L),"Company"));
    	}
    	else if(parentEntityLevel.intValue()==1){
    		entityLevelList.add(new SelectItem(new Long(2L),"Division"));
    		entityLevelList.add(new SelectItem(new Long(3L),"Location"));
    	}
    	else if(parentEntityLevel.intValue()==2){
    		entityLevelList.add(new SelectItem(new Long(3L),"Location"));
    	}
    }
    
    public List<SelectItem> getCopyFromList(){
    	if(copyFromList==null){
    		copyFromList = new ArrayList<SelectItem>();
    		copyFromList.add(new SelectItem(new Long(1L),"Defaults"));
    		copyFromList.add(new SelectItem(new Long(2L),"Nexus"));
    		copyFromList.add(new SelectItem(new Long(3L),"Registrations"));
    	}
    	return copyFromList;
    }
    
    public Long getEntityCopyFromSelected(){
    	return entityCopyFromSelected;
    }
    
    public void setEntityCopyFromSelected(Long entityCopyFromSelected){
    	this.entityCopyFromSelected = entityCopyFromSelected;
    }
    
    public Long getEntityParentSelected(){
    	return entityParentSelected;
    }
    
    public void setEntityParentSelected(Long entityParentSelected){
    	this.entityParentSelected = entityParentSelected;
    }
    
    public JurisdictionService getJurisdictionService() {
		return jurisdictionService;
	}

	public void setJurisdictionService(JurisdictionService jurisdictionService) {
		this.jurisdictionService = jurisdictionService;
		
		filterHandlerShipto.setJurisdictionService(getJurisdictionService());
	}
	
	public SearchJurisdictionHandler getFilterHandler() {
		return filterHandler;
	}
	
	public void resetFilter() {
		filterHandler.reset();
		filterHandler.setCountry("");
		filterHandler.setState("");
	}

    public String getEntityNameText() {
    	if(selectedEntityItem!=null){
    		return selectedEntityItem.getVendorCode() + " - " + selectedEntityItem.getVendorName();
    	}
    	else{
    		return "";
    	}
	}
    
    public void selectedEntityRowChanged (ActionEvent e) throws AbortProcessingException { 
		logger.info("enter selectedEntityRowChanged");
		HtmlDataTable table = (HtmlDataTable) ((HtmlAjaxSupport) e.getComponent()) .getParent(); 
		
		selectedRowIndex = table.getRowIndex();
		selectedGridVendorItem = (VendorItem)table.getRowData();
	}	
    
    public void selectedEntityDefaultRowChanged(ActionEvent e) throws AbortProcessingException { 
		logger.info("enter selectedEntityDefaultRowChanged");
		HtmlDataTable table = (HtmlDataTable) ((HtmlAjaxSupport) e.getComponent()) .getParent(); 
		
		selectedEntityDefaultRowIndex = table.getRowIndex();
		selectedEntityDefault = (EntityDefault)table.getRowData();
	}	
    
    public void selectedLocationListDetailRowChanged(ActionEvent e) throws AbortProcessingException { 
		logger.info("enter selectedLocationListDetailRowChanged");
		HtmlDataTable table = (HtmlDataTable) ((HtmlAjaxSupport) e.getComponent()) .getParent(); 
		
		selectedLocationListDetailRowIndex = table.getRowIndex();
		selectedEntityLocnSetDtl = (EntityLocnSetDtl)table.getRowData();
	}
  
    private void addNodes(String path, TreeNode node, Long parentId) {
        //Add to local array
        Map<Long, EntityItem > localEntityArray = new LinkedHashMap<Long, EntityItem >();
    	for (Long entityKey : globalEntityArray.keySet()) {
    		EntityItem entityItem = (EntityItem)globalEntityArray.get(entityKey);
    		if(parentId.equals(entityItem.getParentEntityId())){
    			localEntityArray.put(entityKey, entityItem);
			}
    	}
    	
    	//Remove 
    	for (Long entityKey : localEntityArray.keySet()) {
    		globalEntityArray.remove(entityKey);
    	}
    	
        //Build
        for(EntityItem tcd : localEntityArray.values()) {
        	TreeNodeImpl nodeImpl = new TreeNodeImpl();
            nodeImpl.setData(tcd);
            node.addChild(tcd.getEntityId(), nodeImpl);

            addNodes(null, nodeImpl, tcd.getEntityId());		
		}
    }

    public void setSelectedEntityItem(VendorItem selectedEntityItem){
		this.selectedEntityItem = selectedEntityItem;
		this.selectedRowKey = null;
	}
	
	public VendorItem getSelectedEntityItem(){
		return this.selectedEntityItem;
	}
   
    public boolean getAddAction() {
		return currentAction.equals(EditAction.ADD);
	}
	
	public boolean getDeleteAction() {
		return currentAction.equals(EditAction.DELETE);
	}
	
	public boolean getDisableActiveFlag() {
		return !selectedEntityItem.getActiveBooleanFlag();
	}

	public boolean getUpdateAction() {
		return currentAction.equals(EditAction.UPDATE);
	}
	
	public boolean getViewAction() {
		return currentAction.equals(EditAction.VIEW);
	}
	
	public boolean getCopyAddAction() {
		return currentAction.equals(EditAction.COPY_ADD);
	}
	
	public boolean getEntityLocked() {
		return this.entityLocked;
	}
	
	public void setEntityLocked(boolean entityLocked) {
		this.entityLocked = entityLocked;
	}
	
	public String getActionText() {
		if (currentAction.equals(EditAction.COPY_ADD)){
			return "Copy/Add";
		}
		else if (currentAction.equals(EditAction.UPDATE)) {
			return "Update";
		}
		else if (currentAction.equals(EditAction.ADD)) {
			return "Add";
		}
		else if (currentAction.equals(EditAction.DELETE)) {
			return "Delete";
		}
		else if (currentAction.equals(EditAction.VIEW)) {
			return "View";
		}
		else{
			return "";
		}
	}
	
	public void filterSaveAction(ActionEvent e) {
    }
	
	public boolean getDisableAdd(){
		return true;
	}
	
	public boolean getDisableCopyAdd(){
		if(selectedGridVendorItem!=null){
			return false;
		}
		else{
			return true;
		}
	}
	
	public boolean getDisableUpdate(){
		if(selectedGridVendorItem!=null){
			return false;
		}
		else{
			return true;
		}
	}
	
	public boolean getDisableView(){
		if(selectedGridVendorItem!=null){
			return false;
		}
		else{
			return true;
		}
	}
	
	public boolean getDisableCopyTo(){
		if(selectedGridVendorItem!=null){
			return false;
		}
		else{
			return true;
		}
	}
	
	public boolean getDisableDelete(){
		if(selectedGridVendorItem!=null){
			boolean vendorUsed = vendorItemService.isVendorUsed(selectedGridVendorItem.getVendorCode(),selectedGridVendorItem.getVendorName());
			if(vendorUsed){
				return true;
			}

			return false;
		}
		else{
			return true;
		}
	}
	
	public boolean getDisableDefaultUpdate(){
		if(selectedEntityDefault!=null){
			return false;
		}
		else{
			return true;
		}
	}
	
	public boolean getDisableDefaultDelete(){
		if(selectedEntityDefault!=null){
			return false;
		}
		else{
			return true;
		}
	}
	
	public boolean getDisableLocationUpdate(){
		if(selectedEntityLocnSet!=null){
			return false;
		}
		else{
			return true;
		}
	}
	
	public boolean getDisableLocationDtlAdd(){
		if(selectedEntityLocnSet!=null){
			return false;
		}
		else{
			return true;
		}
	}
	
	public boolean getDisableLocationDtlUpdate(){
		if(selectedEntityLocnSetDtl!=null){
			return false;
		}
		else{
			return true;
		}
	}
	
	public boolean validateVendorCode(String code) {
	    // Ensure code is unique and not empty	
		String errorStr = "";
		if(code == null || code.length() <= 0){
			errorStr = "Vendor Code is mandatory.";		
	    }
		else if (currentAction.isAddAction() && (vendorItemService.findByCode(code) != null)) {
	    	errorStr = "Vendor Code already exists.";
	    }

		if(errorStr!=null && errorStr.length()>0){
			FacesMessage message = new FacesMessage(errorStr);
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);		
			return true;
		}
		else{
			return false;
		}	
	}
	
	public boolean validateVendorName(String name) {
	    // Ensure code is unique and not empty	
		String errorStr = "";
		if(name == null || name.length() <= 0){
			errorStr = "Vendor Name is mandatory.";		
	    }
		else if (vendorItemService.findByName(name) != null) {
	    	errorStr = "Vendor Name already exists.";
	    }
		
		if(errorStr!=null && errorStr.length()>0){
			FacesMessage message = new FacesMessage(errorStr);
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);	
			return true;
		}
		else{
			return false;
		}	
	}
	
	public boolean validateJurisdiction() {
		//Validate full and partial Jurisdiction		
		Jurisdiction aJurisdiction = null;
		boolean validJurisdiction = true;
		if(filterHandler.getHasSomeInput()){
			validJurisdiction = filterHandler.getIsValidPartialJurisdiction();
		}
		
		if(!validJurisdiction){
			FacesMessage message = new FacesMessage("An Invalid Jurisdiction was entered.");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);				
		}
		
		return validJurisdiction;
	}
	
	public String displayAddAction() {
		String result = null;
		logger.info("start displayAddAction"); 
		currentAction = EditAction.ADD;
		
		vendorItemDTO = new VendorItemDTO();

		resetFilter();

		vendorItemDTO.setGeocode("");
		vendorItemDTO.setCountry("");
		vendorItemDTO.setState("");
		vendorItemDTO.setCounty("");
		vendorItemDTO.setCity("");
		vendorItemDTO.setZip("");
		vendorItemDTO.setActiveBooleanFlag(true);
		
		filterHandler.setGeocode(filterHandlerShipto.getGeocode());
		if(filterHandlerShipto.getCountry()!=null && filterHandlerShipto.getCountry().length()>0){
			filterHandler.setCountry(filterHandlerShipto.getCountry());
		}
		else{
			filterHandler.setCountry(matrixCommonBean.getUserPreferenceDTO().getUserCountry());
		}
		filterHandler.setState(filterHandlerShipto.getState());
		filterHandler.setCounty(filterHandlerShipto.getCounty());
		filterHandler.setCity(filterHandlerShipto.getCity());
		filterHandler.setZip(filterHandlerShipto.getZip());
		filterHandler.setZipPlus4(filterHandlerShipto.getZipPlus4());
	
		return "vendor_update";
	}
	
	public String addAction() {
		String result = null;
		logger.info("start addAction"); 
		try {
			boolean displayError = false;
			if(validateVendorCode(vendorItemDTO.getVendorCode())){
				displayError = true;
			}
			
			if(validateVendorName(vendorItemDTO.getVendorName())){
				displayError = true;
			}
			
			if(!validateJurisdiction()){
				displayError = true;
			}
			
			if(displayError){
				return "";
			}
			
			VendorItem vendorItem = new VendorItem();
			vendorItem.setVendorCode(vendorItemDTO.getVendorCode());
			vendorItem.setVendorName(vendorItemDTO.getVendorName());
			vendorItem.setAddressLine1(vendorItemDTO.getAddressLine1());
			vendorItem.setAddressLine2(vendorItemDTO.getAddressLine2());
			vendorItem.setActiveFlag(vendorItemDTO.getActiveFlag());
			Jurisdiction localJurisdiction = filterHandler.getJurisdiction();
			vendorItem.setGeocode(localJurisdiction.getGeocode());
			vendorItem.setCountry(localJurisdiction.getCountry());
			vendorItem.setState(localJurisdiction.getState());
			vendorItem.setCounty(localJurisdiction.getCounty());
			vendorItem.setCity(localJurisdiction.getCity());
			vendorItem.setZip(localJurisdiction.getZip());
			vendorItem.setUpdateTimestamp(new Date());
			
			vendorItemService.saveOrUpdate(vendorItem);
			
			//Reload datagrid
			retrieveFilterSearchAction();

			selectedRowIndex = -1;

		    result = "vendor_main";		
		} catch (Exception ex) {
			ex.printStackTrace();
			result = "";
		}
		
	    logger.info("end addAction");	    
        return result;
	}	
	
	public String displayNexusDefinitionAction() {
		selectedEntityItem = null;
		
		if(selectedGridVendorItem==null){
			return "";
		}
		
		nexusVendorBackingBean.setSelectedVendorItemFromDefinition(selectedGridVendorItem);

		return "nexusvendor_main";
	}
	
	public String displayUpdateAction() {
		return displayUpdateAction(EditAction.UPDATE);
	}
	
	public String displayUpdateAction(EditAction editAction) {
		selectedEntityItem = null;
		
		if(selectedGridVendorItem==null){
			return "";
		}
		
		logger.info("start displayAddAction"); 
		currentAction = editAction;
		setVendorDetails();

		return "vendor_update";
	}	
	
	public String updateAction() {
		String result = null;
		logger.info("start updateAction"); 
		try {
			boolean displayError = false;	
			if(vendorItemDTO.getVendorName()==null || !selectedGridVendorItem.getVendorName().equalsIgnoreCase(vendorItemDTO.getVendorName())){
				if(validateVendorName(vendorItemDTO.getVendorName())){
					displayError = true;
				}
			}
			
			if(!validateJurisdiction()){

				displayError = true;
			}
			
			if(displayError){
				return "";
			}
			
			VendorItem vendorItem = new VendorItem();
			vendorItem.setVendorId(vendorItemDTO.getVendorId());
			vendorItem.setVendorCode(vendorItemDTO.getVendorCode());
			vendorItem.setVendorName(vendorItemDTO.getVendorName());
			vendorItem.setAddressLine1(vendorItemDTO.getAddressLine1());
			vendorItem.setAddressLine2(vendorItemDTO.getAddressLine2());
			vendorItem.setActiveFlag(vendorItemDTO.getActiveFlag());
			Jurisdiction localJurisdiction = filterHandler.getJurisdiction();
			vendorItem.setGeocode(localJurisdiction.getGeocode());
			vendorItem.setCountry(localJurisdiction.getCountry());
			vendorItem.setState(localJurisdiction.getState());
			vendorItem.setCounty(localJurisdiction.getCounty());
			vendorItem.setCity(localJurisdiction.getCity());
			vendorItem.setZip(localJurisdiction.getZip());
			vendorItem.setZipplus4(localJurisdiction.getZipplus4());
			vendorItem.setUpdateTimestamp(new java.util.Date(new Date().getTime()));

			vendorItemService.saveOrUpdate(vendorItem);

			//Reload datagrid
			retrieveFilterSearchAction();
			
			selectedRowIndex = -1;
			selectedGridVendorItem = null;
			
		    result = "vendor_main";		
		} catch (Exception ex) {
			ex.printStackTrace();
			result = "";
		}
		
	    logger.info("end updateAction");	    
        return result;
	}	
	
	public String displayCopyAddAction() {
		this.displayUpdateAction(EditAction.COPY_ADD);

		vendorItemDTO.setVendorId(null);
		vendorItemDTO.setVendorCode("");
		vendorItemDTO.setUpdateUserId("");
		vendorItemDTO.setUpdateTimestamp(null);
		
		return "vendor_update";
	}	

	public String copyAddAction(){
		return this.addAction();
	}
		
	public String displayDeleteAction() {
		selectedEntityItem = null;
		
		if(selectedGridVendorItem==null){
			return "";
		}
		
		logger.info("start displayAddAction"); 
		currentAction = EditAction.DELETE;
		
		setVendorDetails();
		
		if(vendorItemService.isNexusDefinitionsUsed(vendorItemDTO.getVendorId())){
			//Update active_flag = ��
			FacesMessage message = new FacesMessage("Nexus Definitions exist for this Vendor and cannot be deleted. Click Ok to set to Inactive.");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
		}

		return "vendor_update";
	}	
	
	public String displayVendorView(String fromView,String vendorId,String vendorCode) {
		selectedEntityItem = null;
		if(vendorId !=null ){
			if(StringUtils.isBlank(vendorId)){
				vendorId = "0";
			}
			selectedGridVendorItem = vendorItemService.findById(Long.valueOf(vendorId));
			if(selectedGridVendorItem == null){
				//Check for vendor code
				selectedGridVendorItem = vendorItemService.findByCode(vendorCode);
				if(selectedGridVendorItem == null){
					return "";
				}
			}
			setVendorDetails();
			currentAction = EditAction.VIEW;
		    setReturnView("trans_view");
			return "vendor_update";
		}
		return null;
		
	}	
	
	public String displayViewAction() {
		selectedEntityItem = null;
		
		if(selectedGridVendorItem==null){
			return "";
		}
		logger.info("start displayViewAction"); 
		setVendorDetails();
		currentAction = EditAction.VIEW;
		setReturnView("vendor_main");
		return "vendor_update";
	}
	
	private void setVendorDetails() {
		vendorItemDTO = new VendorItemDTO();		
		vendorItemDTO.setVendorId(selectedGridVendorItem.getVendorId());
		vendorItemDTO.setVendorCode(selectedGridVendorItem.getVendorCode());
		vendorItemDTO.setVendorName(selectedGridVendorItem.getVendorName());
		vendorItemDTO.setAddressLine1(selectedGridVendorItem.getAddressLine1());
		vendorItemDTO.setAddressLine2(selectedGridVendorItem.getAddressLine2());
		vendorItemDTO.setZipplus4(selectedGridVendorItem.getZipplus4());	
		vendorItemDTO.setActiveFlag(selectedGridVendorItem.getActiveFlag());
		
		//Set Jurisdiction filter
		resetFilter();
		vendorItemDTO.setGeocode(selectedGridVendorItem.getGeocode());
		vendorItemDTO.setCountry(selectedGridVendorItem.getCountry());
		vendorItemDTO.setState(selectedGridVendorItem.getState());
		vendorItemDTO.setCounty(selectedGridVendorItem.getCounty());
		vendorItemDTO.setCity(selectedGridVendorItem.getCity());;
		vendorItemDTO.setZip(selectedGridVendorItem.getZip());
		vendorItemDTO.setUpdateUserId(selectedGridVendorItem.getUpdateUserId());
		vendorItemDTO.setUpdateTimestamp(selectedGridVendorItem.getUpdateTimestamp());

		filterHandler.setGeocode(selectedGridVendorItem.getGeocode());
		filterHandler.setCountry(selectedGridVendorItem.getCountry());
		filterHandler.setState(selectedGridVendorItem.getState());
		filterHandler.setCounty(selectedGridVendorItem.getCounty());
		filterHandler.setCity(selectedGridVendorItem.getCity());
		filterHandler.setZip(selectedGridVendorItem.getZip());
		filterHandler.setZipPlus4(selectedGridVendorItem.getZipplus4());
	}
	
	
	public String deleteAction() {
		String result = null;
		logger.info("start deleteAction"); 
		try {
			Long vendorId = vendorItemDTO.getVendorId();		
			if(vendorItemService.isNexusDefinitionsUsed(vendorId)){
				//Update TB_VENDOR.active_flag = '0' //tb_vendor_nexus_dtl.active_flag = 0 AND set tb_vendor_nexus.nexus_flag=0.

				VendorItem vendorItem = vendorItemService.findById(vendorId);
				vendorItem.setActiveFlag("0");		
				vendorItemService.disableVendor(vendorId);
			}
			else{
				//Delete	
				vendorItemService.deleteVendor(vendorId);
			}

			//Reload datagrid
			retrieveFilterSearchAction();
			
			selectedRowIndex = -1;
			selectedGridVendorItem = null;
			
		    result = "vendor_main";		
		} catch (Exception ex) {
			ex.printStackTrace();
			result = "";
		}
		
	    logger.info("end deleteAction");	    
        return result;
	}

	private boolean disableDefaultAdd = true;
	
	public boolean getDisableDefaultAdd(){
		return disableDefaultAdd;
	}
	
	public String okAction() {
		String result = null;
		switch (currentAction) {
			case DELETE:
				result = deleteAction();
				break;
				
			case UPDATE:
				result = updateAction();
				break;
				
			case ADD:
				result = addAction();
				break;
				
			case COPY_ADD:
				result = copyAddAction();
				break;
			case VIEW:
				result = returnBack();
				break;
	
			default:
				result = cancelAction();
				break;
		}
		return result;
	}
	
	public String cancelAction() {
	    return "vendor_main";
	}
	
	public void lockChangeAction(ValueChangeEvent event) {
    	String newValue = (String) event.getNewValue();
        String oldValue = (String) event.getOldValue();
    }
	
	public void defaultCodeSelectionChanged(ValueChangeEvent e) {
		String code = (String)e.getNewValue();
		
		defaultValueList = createValueList(code);
	}
	
	public void defaultCodeSelectionChanged(ActionEvent e) {
		String code = (String) ((HtmlSelectOneMenu) ((HtmlAjaxSupport) e.getComponent()).getParent()).getSubmittedValue();

		defaultValueList = createValueList(code);
	}
	
	public List<SelectItem> createValueList(String code) {
		List<SelectItem> selectItems = new ArrayList<SelectItem>();
		
		if(code!=null && code.length()>0){
			
			if(currentAction.equals(EditAction.ADD)){
				selectItems.add(new SelectItem("","Select a Value"));
			}
			List<ListCodes> listCodes = matrixCommonBean.getCacheManager().getListCodesByType(code);
			if(listCodes != null) {
				for(ListCodes lc : listCodes) {
					selectItems.add(new SelectItem(lc.getCodeCode(), lc.getDescription()));
				}
			}
		}
		
		return selectItems;
	}
	
	public List<SelectItem> createCodeList(Map<String, String > mapExcluded) {
		List<SelectItem> selectItems = new ArrayList<SelectItem>();
		selectItems.add(new SelectItem("","Select a Default Code"));
		List<ListCodes> listCodes = matrixCommonBean.getCacheManager().getListCodesByType("DEFCODES");
		if(listCodes != null) {
			for(ListCodes lc : listCodes) {
				String code = (String)mapExcluded.get(lc.getCodeCode());
				if(code==null || code.length()==0){
					selectItems.add(new SelectItem(lc.getCodeCode(), lc.getDescription()));
				}
			}
		}
		return selectItems;
	}
	
    public void setSelectedGridVendorItem(VendorItem selectedGridVendorItem){
		this.selectedGridVendorItem = selectedGridVendorItem;
	}
	
	public VendorItem getSelectedGridVendorItem(){
		return this.selectedGridVendorItem;
	}
	
	public void SetSelectedEntityDefault(EntityDefault selectedEntityDefault){
		this.selectedEntityDefault = selectedEntityDefault;
	}
	
	public EntityDefault getSelectedEntityDefault(){
		return this.selectedEntityDefault;
	}

	public void SetSelectedEntityLocnSet(EntityLocnSet selectedEntityLocnSet){
		this.selectedEntityLocnSet = selectedEntityLocnSet;
	}
	
	public EntityLocnSet getSelectedEntityLocnSet(){
		return this.selectedEntityLocnSet;
	}
	
	public void SetSelectedEntityLocnSetDtl(EntityLocnSetDtl selectedEntityLocnSetDtl){
		this.selectedEntityLocnSetDtl = selectedEntityLocnSetDtl;
	}
	
	public EntityLocnSetDtl getSelectedEntityLocnSetDtl(){
		return this.selectedEntityLocnSetDtl;
	}

	public UserSecurityBean getUserSecurityBean() {
		return userSecurityBean;
	}

	public void setUserSecurityBean(UserSecurityBean userSecurityBean) {
		this.userSecurityBean= userSecurityBean;
	}
	
	public int getSelectedRowIndex() {
		return selectedRowIndex;
	}

	public void setSelectedRowIndex(int selectedRowIndex) {
		this.selectedRowIndex = selectedRowIndex;
	}
	
	public int getSelectedEntityDefaultRowIndex() {
		return selectedEntityDefaultRowIndex;
	}

	public void setSelectedEntityDefaultRowIndex(int selectedEntityDefaultRowIndex) {
		this.selectedEntityDefaultRowIndex = selectedEntityDefaultRowIndex;
	}

	public int getSelectedLocationListRowIndex() {
		return selectedLocationListRowIndex ;
	}

	public void setSelectedLocationListRowIndex(int selectedLocationListRowIndex) {
		this.selectedLocationListRowIndex = selectedLocationListRowIndex;
	}
	
	public int getSelectedLocationListDetailRowIndex() {
		return selectedLocationListDetailRowIndex;
	}

	public void setSelectedLocationListDetailRowIndex(int selectedLocationListDetailRowIndex) {
		this.selectedLocationListDetailRowIndex = selectedLocationListDetailRowIndex;
	}

	public boolean isShowUpdateButton() {
		return showUpdateButton;
	}

	public void setShowUpdateButton(boolean showUpdateButton) {
		this.showUpdateButton = showUpdateButton;
	}

	public boolean isDisableDeleteButton() {
		return disableDeleteButton;
	}

	public void setDisableDeleteButton(boolean disableDeleteButton) {
		this.disableDeleteButton = disableDeleteButton;
	}

	public boolean isUpdateDisplay() {
		return updateDisplay;
	}

	public void setUpdateDisplay(boolean updateDisplay) {
		this.updateDisplay = updateDisplay;
	}

	public boolean isDisplayAddButton() {
		return displayAddButton;
	}

	public void setDisplayAddButton(boolean displayAddButton) {
		this.displayAddButton = displayAddButton;
	}

	public boolean isDisplayUpdateButton() {
		return displayUpdateButton;
	}

	public void setDisplayUpdateButton(boolean displayUpdateButton) {
		this.displayUpdateButton = displayUpdateButton;
	}

	public boolean isTransColDisplay() {
		return transColDisplay;
	}

	public void setTransColDisplay(boolean transColDisplay) {
		this.transColDisplay = transColDisplay;
	}
	
    public UserService getUserService() {
    	return userService;
    }
    
    public void setUserService(UserService userService) {
    	this.userService = userService;
    }	
    
    public RoleService getRoleService() {
    	return roleService;
    }
    
    public void setRoleService(RoleService roleService) {
    	this.roleService = roleService;
    }       
    
    public VendorItemService getVendorItemService() {
    	return vendorItemService;
    }
    
    public void setVendorItemService(VendorItemService vendorItemService) {
    	this.vendorItemService = vendorItemService;
    }  
    
    public UserEntityService getUserEntityService() {
    	return userEntityService;
    }
    
    public void setUserEntityService(UserEntityService userEntityService) {
    	this.userEntityService = userEntityService;
    }    
	
	Calendar calendar = Calendar.getInstance();

	private boolean noAdd = false;

	private int selectedFilterIndex;

	
	public boolean isNoAdd() {
		return noAdd;
	}

	public void setNoAdd(boolean noAdd) {
		this.noAdd = noAdd;
	}
	
	public int getSelectedFilterIndex() {
		return selectedFilterIndex;
	}

	public void setSelectedFilterIndex(int selectedFilterIndex) {
		this.selectedFilterIndex = selectedFilterIndex;
	}

	public List<User> getUsersList() {
		return usersList;
	}

	public void setUsersList(List<User> usersList) {
		this.usersList = usersList;
	}
	
	public List<Role> getRolesList() {
		return rolesList;
	}

	public void setRolesList(List<Role> rolesList) {
		this.rolesList = rolesList;
	}	
	
	public List<Menu> getMenusList() {
		return menusList;
	}

	public void setMenusList(List<Menu> menusList) {
		this.menusList = menusList;
	}	
	
	public String getRerender() {
		return rerender;
	}
	
	public void setRerender(String rerender) {
		this.rerender = rerender;
	}
	
	public EntityItem getSelectedEntity() {
		return selectedEntity;
	}

	public void setSelectedEntity(EntityItem selectedEntity) {
		this.selectedEntity = selectedEntity;
	}

	public String getMode() {
		return this.mode;
	}
	
	public void setMode(String mode) {
		this.mode = mode;
	}
	
	public EntityLevel getSelectedEntityLevel() {
		return this.selectedEntityLevel;
	}
	
	public void setSelectedEntityLevel(EntityLevel entityLevel) {
		this.selectedEntityLevel = entityLevel;
	}	
	
	public UserEntity getSelectedUserEntity() {
		return this.selectedUserEntity;
	}
	
	public void setSelectedUserEntity(UserEntity userEntity) {
		this.selectedUserEntity = userEntity;
	}		

	public VendorItemDTO getVendorItemDTO() {
		return vendorItemDTO;
	}
	
	public EntityDefaultDTO getEntityDefaultDTO() {
		return entityDefaultDTO;
	}
	
	public EntityLocnSetDtlDTO getEntityLocnSetDtlDTO() {
		return entityLocnSetDtlDTO;
	}
	
	public EntityLocnSetDTO getEntityLocnSetDTO() {
		return entityLocnSetDTO;
	}
	
	public void validateUserCode(FacesContext context, UIComponent toValidate, Object value) {
	    // Ensure code is unique
		String code = (String) value;
	    if (currentAction.isAddAction() && (userService.findById(code) != null)) {
			((UIInput)toValidate).setValid(false);
			FacesMessage message = new FacesMessage("User code already exists.");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage(toValidate.getClientId(context), message);
	    }
	}
			
	public boolean validateEntity(String code, String name) {
	    // Ensure code is unique and not empty
		logger.debug("code : " + code + "name : " + name );		
		if(name == null || code == null || name.length() <= 0 || code.length() <= 0){
			FacesMessage message  = new FacesMessage("Entity code and Entity name cannot be empty.");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
			return false;
	    }else if (currentAction.isAddAction() && (vendorItemService.findByCode(code) != null)) {
			FacesMessage message = new FacesMessage("Entity code already exists.");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
			return false;
	    }
		return true;
	}
	
	public List<DataDefinitionColumn> getDataDefinitionColList() {
		return dataDefinitionColList;
	}

	public void setDataDefinitionColList(
			List<DataDefinitionColumn> dataDefinitionColList) {
		this.dataDefinitionColList = dataDefinitionColList;
	}

	public boolean isShowOkButton() {
		return showOkButton;
	}

	public void setShowOkButton(boolean showOkButton) {
		this.showOkButton = showOkButton;
	}

	public void setUserConfig(String file) {
		if (this.userConfig == null) { 
			try {
				this.userConfig = new ConfigSetting(file);
			} catch (IOException e) {
				logger.error(e);
			}
		}
	}

	public loginBean getLoginBean() {
		return loginBean;
	}

	public void setLoginBean(loginBean loginBean) {
		this.loginBean = loginBean;
	}	
	
	public SearchJurisdictionHandler getFilterHandlerShipto() {
		return filterHandlerShipto;
	}
	
	public String resetFilterSearchAction(){
		addressLine1 = "";
		addressLine2 = "";
		vendorCode = "";
		vendorName = "";
		
		filterHandlerShipto.reset();
		filterHandlerShipto.setCountry("");
		filterHandlerShipto.setState("");
			
		return null;
	}
	
	public String retrieveFilterSearchAction(){

		VendorItem filter = new VendorItem();
		
		//filter.setAddressLine1(addressLine1);
		//filter.setAddressLine2(addressLine2);
		filter.setVendorCode(vendorCode);
		filter.setVendorName(vendorName);		
		filter.setGeocode(filterHandlerShipto.getGeocode());
		filter.setCity(filterHandlerShipto.getCity());
		filter.setCounty(filterHandlerShipto.getCounty());
		filter.setState(filterHandlerShipto.getState());
		filter.setCountry(filterHandlerShipto.getCountry());
		filter.setZip(filterHandlerShipto.getZip());
		filter.setZipplus4(filterHandlerShipto.getZipPlus4());
		
		vendorDataModel.setCriteria(filter, buildWhereClause());

		selectedGridVendorItem = null;
		selectedRowIndex = -1;
		
		String rowStr = matrixCommonBean.getMatrixDTO().getSysLimitedThreshold();
		int rowpage = 0;
		if(rowStr==null || rowStr.length()==0){
			rowpage = 100; 
		}
		else{
			rowpage = Integer.parseInt(rowStr);
		}
		
		String muitipleStr = matrixCommonBean.getMatrixDTO().getSysLimitedThresholdPages();
		int muitiplepage = 0;
		if(muitipleStr==null || muitipleStr.length()==0){
			muitiplepage = 4; 
		}
		else{
			muitiplepage = Integer.parseInt(muitipleStr);
		}
		
		
		vendorDataModel.setPageSize(rowpage);
		vendorDataModel.setDbFetchMultiple(muitiplepage);
			
		return null;
	}
	
	public void sortAction(ActionEvent e) {
		vendorDataModel.toggleSortOrder(e.getComponent().getParent().getId());
	}
	
	public String resetSortAction() {
		vendorDataModel.resetSortOrder();
		
		selectedGridVendorItem = null;
		selectedRowIndex = -1;
			
		return null;
	}
	
	public String getAddressLine1() {
		return addressLine1;
	}

	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}
	
	public String getAddressLine2() {
		return addressLine2;
	}

	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}
	
	public String getVendorCode() {
		return vendorCode;
	}

	public void setVendorCode(String vendorCode) {
		this.vendorCode = vendorCode;
	}
	
	public String getVendorName() {
		return vendorName;
	}

	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}
	
	public String getShowhide() {
		return showhide;
	}

	public void setShowhide(String showhide) {
		this.showhide = showhide;
	}

	private String buildWhereClause(){
		String whereClause = " 1=1 ";
	
		return whereClause;
	}
	public boolean getIsEntitySelected() {
		return (showhide!=null && showhide.equals("show"));
	}

	private String returnBack() {
		if(this.returnView != null) {
			return this.returnView;
		}
			
		return null;
	}	  
	
	 public User getCurrentUser() {
			return loginBean.getUser();
	 }
}
