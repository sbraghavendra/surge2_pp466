package com.ncsts.view.bean;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import javax.faces.component.UIComponent;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlPanelGrid;
import javax.faces.component.html.HtmlSelectBooleanCheckbox;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;

import org.ajax4jsf.component.UIDataAdaptor;
import org.ajax4jsf.component.html.HtmlAjaxSupport;
import org.apache.log4j.Logger;
import org.richfaces.component.html.HtmlDataTable;
import org.springframework.beans.factory.annotation.Autowired;

import com.ncsts.domain.BCPPurchaseTransaction;
import com.ncsts.domain.TaxMatrix;
//import com.ncsts.jsf.model.BCPSaleTransactionDataModel;
import com.ncsts.jsf.model.BCPTransactionDataModel;
import com.ncsts.view.util.StateObservable;
import com.ncsts.view.util.TogglePanelController;

public class BCPTransactionDetailBean implements Observer, HttpSessionBindingListener {
	private static final Logger logger = Logger.getLogger(BCPTransactionDetailBean.class);
	
	private StateObservable ov = null;
	
	private Long batchId;
	
	private HtmlDataTable trDetailTable;
	private BCPTransactionDataModel bcpTransactionDataModel;
	private MatrixCommonBean matrixCommonBean;
	
	private boolean displayAllFields = false;
	
	private HtmlPanelGrid filterSelectionPanel;
	private TaxMatrix filterSelectionTaxMatrix = new TaxMatrix();
	private SearchDriverHandler driverHandler = new SearchDriverHandler();
	
	private int selectedRowIndex = -1;
	private BCPPurchaseTransaction selectedTransaction;
	
	private TransactionSplittingToolBean<BCPPurchaseTransaction> transactionSplittingToolBean;
	private List<TransactionSplittingToolBean<?>> currentSplittingToolBean;
	
	private Long futureSplitWarning = 0L;
	
	private String selectedCountry = "";
	private String selectedState = "";
	
	@Autowired
	private DriverHandlerBean driverHandlerBean;
	
	private TogglePanelController togglePanelController = null;
	
	public  TogglePanelController getTogglePanelController(){
		if(togglePanelController==null){
			togglePanelController = new TogglePanelController();
			togglePanelController.addTogglePanel("TaxBcpDriversPanel", true);
			togglePanelController.addTogglePanel("TaxBcpMiscPanel", false);
		}
		return togglePanelController;
	}
	
	public void setTogglePanelController(TogglePanelController togglePanelController){
		this.togglePanelController = togglePanelController;;
	}
	
	public Long getBatchId() {
		return batchId;
	}

	public void setBatchId(Long batchId) {
		this.batchId = batchId;
		refreshData();
		bcpTransactionDataModel.setDefaultSortOrder("bcpTransactionId", BCPTransactionDataModel.SORT_DESCENDING);
	}

	private void refreshData() {
		filterSelectionTaxMatrix = new TaxMatrix();
		selectedRowIndex = -1;
		selectedTransaction = null;
		
		BCPPurchaseTransaction BCPPurchaseTransaction = new BCPPurchaseTransaction();
		BCPPurchaseTransaction.setProcessBatchNo(batchId);
		bcpTransactionDataModel.setFilterCriteria(BCPPurchaseTransaction);
	}

	public HtmlDataTable getTrDetailTable() {
		if (trDetailTable == null) {
			trDetailTable = new HtmlDataTable();
			trDetailTable.setId("aMDetailList");
			trDetailTable.setVar("trdetail");
			matrixCommonBean.createBCPPurchaseTransactionTable(trDetailTable,
					"BCPPurchaseTransactionBean", "BCPPurchaseTransactionBean.bcpTransactionDataModel", 
					matrixCommonBean.getBCPTransactionPropertyMap(), false, displayAllFields, false, false, true);
		}
		
		return trDetailTable;
	}

	public void setTrDetailTable(HtmlDataTable trDetailTable) {
		this.trDetailTable = trDetailTable;
	}

	public BCPTransactionDataModel getBcpTransactionDataModel() {
		return bcpTransactionDataModel;
	}

	public void setBcpTransactionDataModel(
			BCPTransactionDataModel bcpTransactionDataModel) {
		this.bcpTransactionDataModel = bcpTransactionDataModel;
	}

	public MatrixCommonBean getMatrixCommonBean() {
		return matrixCommonBean;
	}

	public void setMatrixCommonBean(MatrixCommonBean matrixCommonBean) {
		this.matrixCommonBean = matrixCommonBean;
		
		driverHandler.setDriverReferenceService(matrixCommonBean.getDriverReferenceService());
		driverHandler.setCacheManager(matrixCommonBean.getCacheManager());
	}
	
	public Boolean getMultiSelect() {
		return false;
	}
	
	public void selectedTransactionChanged (ActionEvent e) throws AbortProcessingException {
		UIComponent uiComponent = e.getComponent().getParent();
		UIDataAdaptor table = (UIDataAdaptor) uiComponent;
		selectedTransaction = (BCPPurchaseTransaction) table.getRowData();
		selectedRowIndex = table.getRowIndex();
	}
	
	public void displayChange(ActionEvent e) {
		HtmlSelectBooleanCheckbox check = (HtmlSelectBooleanCheckbox) ((HtmlAjaxSupport) e.getComponent()).getParent();
	    displayAllFields = (check.getSubmittedValue() == null)? 
	    		((check.getValue() == null)? false:(Boolean) check.getValue()):Boolean.parseBoolean((String) check.getSubmittedValue());
	    
		matrixCommonBean.createBCPPurchaseTransactionTable(trDetailTable,
				"BCPPurchaseTransactionBean", "BCPPurchaseTransactionBean.bcpTransactionDataModel", 
				matrixCommonBean.getBCPTransactionPropertyMap(), false, displayAllFields, false, false, true);
	}
	
	public String resetFilter() {
		filterSelectionTaxMatrix = new TaxMatrix();
		
		return null;
	}
	
	public String listTransactions() {
		BCPPurchaseTransaction trDetail = new BCPPurchaseTransaction();
		trDetail.setProcessBatchNo(batchId);
		
		String country = selectedCountry;
		if ((country != null) && !country.equals("")) {
			trDetail.setTransactionCountryCode(country);
		}
		
		String state = selectedState;
		if ((state != null) && !state.equals("")) {
			trDetail.setTransactionStateCode(state);
		}
		
		matrixCommonBean.setBCPTransactionProperties(filterSelectionTaxMatrix, trDetail);
		bcpTransactionDataModel.setFilterCriteria(trDetail);
		
		return null;
	}
	
	public HtmlPanelGrid getFilterSelectionPanel() {
		filterSelectionPanel = MatrixCommonBean.createDriverPanel(
				TaxMatrix.DRIVER_CODE, matrixCommonBean.getCacheManager(), 
				matrixCommonBean.getUserEntityService().getEntityItemDTO(),
				"filterPanel", 
				"BCPPurchaseTransactionBean", "filterSelectionTaxMatrix", "matrixCommonBean", 
				false, false, false, true, true);
		
		matrixCommonBean.prepareTaxDriverPanel(filterSelectionPanel);
		return filterSelectionPanel;
	}
	
	public void setFilterSelectionPanel(HtmlPanelGrid filterSelectionPanel) {
		this.filterSelectionPanel = filterSelectionPanel;
	}
	
	public TaxMatrix getFilterSelectionTaxMatrix() {
		return filterSelectionTaxMatrix;
	}
	
	public SearchDriverHandler getDriverHandler() {
		return driverHandler;
	}
	
	public void searchDriver(ActionEvent e) {
		driverHandlerBean.initSearch(
				filterSelectionTaxMatrix, 
				(HtmlInputText) e.getComponent().getParent().getChildren().get(0), e.getComponent());
		driverHandlerBean.setCallBackScreen(null);
	}
	
	public String closeAction() {
		this.batchId = null;
		filterSelectionTaxMatrix = new TaxMatrix();
		selectedRowIndex = -1;
		selectedTransaction = null;
		
		return "importmap_main";
	}
	
	public boolean getDisplaySplit() {
		return this.selectedTransaction != null && 
			this.selectedTransaction.getGlLineItmDistAmt() != null && 
			this.selectedTransaction.getGlLineItmDistAmt().abs().compareTo(BigDecimal.ZERO) >0;
	}
	
	public boolean getDisplayFutureSplit() {
		return getDisplaySplit() &&
			this.selectedTransaction.getGlLineItmDistAmt() != null && 
			this.selectedTransaction.getGlLineItmDistAmt().abs().compareTo(BigDecimal.ZERO) >0 &&
			!"FS".equals(this.selectedTransaction.getMultiTransCode()) &&
			!"OS".equals(this.selectedTransaction.getMultiTransCode()) &&
			!"S".equals(this.selectedTransaction.getMultiTransCode()) &&
			!"OA".equals(this.selectedTransaction.getMultiTransCode()) &&
			!"A".equals(this.selectedTransaction.getMultiTransCode())
		;
	}
	
	public int getSelectedRowIndex() {
		return selectedRowIndex;
	}

	public void setSelectedRowIndex(int selectedRowIndex) {
		this.selectedRowIndex = selectedRowIndex;
	}
	
	public BCPPurchaseTransaction getSelectedTransaction() {
		return selectedTransaction;
	}
	
	public void sortAction(ActionEvent e) {
		bcpTransactionDataModel.toggleSortOrder(e.getComponent().getId().replace("s_trans_", ""));
	}
	
	public String splitAction() {
		this.transactionSplittingToolBean.setTransactionDetail(this.selectedTransaction);
		if(this.currentSplittingToolBean == null) {
			this.currentSplittingToolBean = new ArrayList<TransactionSplittingToolBean<?>>();
		}
		else {
			this.currentSplittingToolBean.clear();
		}
		this.currentSplittingToolBean.add(this.transactionSplittingToolBean);
		return "split_transactions";
	}
	
	public String futureSplitAction() {
		if(futureSplitWarning == 0L) {
			futureSplitWarning = 1L;
			return null;
		}
		else {
			futureSplitWarning = 0L;
		}
		
		selectedTransaction.setTransactionInd("H");
		selectedTransaction.setMultiTransCode("FS");
		bcpTransactionDataModel.getCommonDAO().update(selectedTransaction);
		return null;
	}

	@Override
	public void valueBound(HttpSessionBindingEvent arg0) {
		StateObservable ov = StateObservable.getInstance();
		this.ov = ov;
		this.ov.addObserver(this);
	}

	@Override
	public void valueUnbound(HttpSessionBindingEvent arg0) {
		if(ov!=null){
			ov.deleteObserver(this);
		}
	}

	@Override
	public void update(Observable o, Object arg) {
		if (o == ov) {
			
		}
	}

	public boolean isDisplayAllFields() {
		return displayAllFields;
	}

	public void setDisplayAllFields(boolean displayAllFields) {
		this.displayAllFields = displayAllFields;
	}

	public TransactionSplittingToolBean<BCPPurchaseTransaction> getTransactionSplittingToolBean() {
		return transactionSplittingToolBean;
	}

	public void setTransactionSplittingToolBean(
			TransactionSplittingToolBean<BCPPurchaseTransaction> transactionSplittingToolBean) {
		this.transactionSplittingToolBean = transactionSplittingToolBean;
	}

	public List<TransactionSplittingToolBean<?>> getCurrentSplittingToolBean() {
		return currentSplittingToolBean;
	}

	public void setCurrentSplittingToolBean(
			List<TransactionSplittingToolBean<?>> currentSplittingToolBean) {
		this.currentSplittingToolBean = currentSplittingToolBean;
	}

	public Long getFutureSplitWarning() {
		return futureSplitWarning;
	}

	public void setFutureSplitWarning(Long futureSplitWarning) {
		this.futureSplitWarning = futureSplitWarning;
	}
	
	public void cancelWarning(ActionEvent e) {
		this.futureSplitWarning = 0L;
	}

	public String getSelectedState() {
		return selectedState;
	}

	public void setSelectedState(String selectedState) {
		this.selectedState = selectedState;
	}

	public String getSelectedCountry() {
		return selectedCountry;
	}

	public void setSelectedCountry(String selectedCountry) {
		this.selectedCountry = selectedCountry;
	}
	
	public void searchCountryChanged(ValueChangeEvent e){
		String newCountry = (String)e.getNewValue();
		if(newCountry == null) {
			selectedCountry = "";
		}
		else {
			selectedCountry = newCountry;
		}
	
		if(stateMenuItems!=null) stateMenuItems.clear();

		stateMenuItems = getMatrixCommonBean().getCacheManager().createStateItems(selectedCountry);
		selectedState = "";
    }
	
	private List<SelectItem> countryMenuItems;
	public List<SelectItem> getCountryMenuItems() {
		if(countryMenuItems==null){
			selectedCountry = "";
		}
		countryMenuItems = getMatrixCommonBean().getCacheManager().createCountryItems();   
		return countryMenuItems;
	}

	private List<SelectItem> stateMenuItems;
	public List<SelectItem> getStateMenuItems() {
		if (stateMenuItems == null) {
		   stateMenuItems = getMatrixCommonBean().getCacheManager().createStateItems(selectedCountry);
		   selectedState = "";
		}
		return stateMenuItems;
	}
}
