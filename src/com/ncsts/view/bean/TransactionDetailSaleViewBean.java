package com.ncsts.view.bean;

import com.ncsts.domain.BillTransaction;

public class TransactionDetailSaleViewBean extends TransactionDetailSaleBean {
	private static final String DEFAULT_DISPLAY = "limitedInformationFilter";
	
	public TransactionDetailSaleViewBean(){
		super();
		
		//0004440
		this.getFilterTaxCodeHandler().setReturnRuleUrl("transactions_view");
		this.getFilterTaxCodeHandler1().setReturnRuleUrl("transactions_view");
		
		this.setSelectedDisplay(DEFAULT_DISPLAY);
		this.setIsReadonly(true);
	}
	
	public void setSaleTransactionForView(BillTransaction aSaleTransaction){
		this.setSaleTransactionFilter(aSaleTransaction);
		this.fillSelectedTransaction();
	}

}
