package com.ncsts.view.bean;

import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.ActionEvent;

import org.ajax4jsf.component.UIDataAdaptor;
import org.apache.log4j.Logger;
import org.apache.myfaces.custom.fileupload.HtmlInputFileUpload;
import org.apache.myfaces.custom.fileupload.UploadedFile;
import org.richfaces.component.html.HtmlCalendar;
import org.springframework.beans.factory.annotation.Autowired;

import com.ncsts.common.CacheManager;
import com.ncsts.common.DatabaseUtil;
import com.ncsts.dao.JurisdictionDAO;
import com.ncsts.domain.Auditable;
import com.ncsts.domain.BCPJurisdictionTaxRate;
import com.ncsts.domain.BCPJurisdictionTaxRateText;
import com.ncsts.domain.BatchErrorLog;
import com.ncsts.domain.BatchMaintenance;
import com.ncsts.domain.BatchMetadata;
import com.ncsts.domain.Option;
import com.ncsts.domain.OptionCodePK;
import com.ncsts.domain.TaxRateUpdatesBatch;
import com.ncsts.domain.User;
import com.ncsts.dto.BatchMetadataList;
import com.ncsts.dto.DbPropertiesDTO;
import com.ncsts.jsf.model.BCPJurisdictionTaxRateDataModel;
import com.ncsts.jsf.model.BCPJurisdictionTaxRateTextDataModel;
import com.ncsts.jsf.model.BatchMaintenanceDataModel;
import com.ncsts.management.DbPropertiesBean;
import com.ncsts.services.BatchMaintenanceService;
import com.ncsts.services.OptionService;
import com.ncsts.view.util.BatchStatistics;
import com.ncsts.view.util.FacesUtils;
import com.ncsts.view.util.TaxRateUpdateBatchValidator;
import com.ncsts.view.util.TaxRateUploadParser;

public class DataUtilityTaxRateUpdatesBackingBean {
    private Logger logger = Logger.getLogger(DataUtilityTaxRateUpdatesBackingBean.class);

    private enum AddState {
        NEED_FILE,
        HAVE_FILE,
        PROCESSING,
        COMPLETE
    }

    private JurisdictionDAO jurisdictionDAO;
    private FilePreferenceBackingBean filePreferenceBean;
    private BatchMaintenanceService batchMaintenanceService;
    private BatchMaintenance exampleBatchMaintenance;

    private BatchErrorsBackingBean batchErrorsBean;

    private BatchMaintenanceBackingBean batchMaintenanceBean;

    private BatchMaintenance selectedBatch = null;

    private List<TaxRateUpdatesBatch> selectedBatches;

    private int selectedBatchIndex = -1;

    private BatchMaintenanceDataModel batchMaintenanceDataModel;
    private BCPJurisdictionTaxRateDataModel bcpJurisdictionTaxRateDataModel;
    private BCPJurisdictionTaxRateTextDataModel bcpJurisdictionTaxRateTextDataModel;

    @Autowired
    private OptionService optionService;

    private BatchMetadata batchMetadata;

    private HtmlInputFileUpload uploadFile;

    private Date systemDate = null;

    private TaxRateUploadParser parser = null;//new TaxRateUploadParser(FacesUtils.getServletContext().getRealPath("") + "/taxratefile/");

    private AddState state;

    private HtmlCalendar batchStartTime = new HtmlCalendar();
    private Thread background;
    private String backgroundError;
    private BatchStatistics batchStatistics;

    private DbPropertiesBean dbPropertiesBean;
    private DbPropertiesDTO dbPropertiesDTO = null;
    private BatchMetadataList batchMetadataList;
    
    private long noOfBytesInFile;
	private long noOfRows;
	private String importStatus;
	private String fileName;
	private UploadedFile uploadedFile;
	
	@Autowired
	loginBean loginbean;
	
	@Autowired
	private CacheManager cacheManager;
	
	public void setCacheManager(CacheManager cacheManager) {
		this.cacheManager = cacheManager;
	}
	
	public UploadedFile getUploadedFile() {
		return uploadedFile;
	}

	public void setUploadedFile(UploadedFile uploadedFile) {
		this.uploadedFile = uploadedFile;
	}
	
	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	
	public long getNoOfBytesInFile() {
		return noOfBytesInFile;
	}

	public void setNoOfBytesInFile(int noOfBytesInFile) {
		this.noOfBytesInFile = noOfBytesInFile;
	}

	public long getNoOfRows() {
		return noOfRows;
	}

	public void setNoOfRows(int noOfRows) {
		this.noOfRows = noOfRows;
	}

	public String getImportStatus() {
		return importStatus;
	}

	public void setImportStatus(String importStatus) {
		this.importStatus = importStatus;
	}
	
	private boolean fileSelected = false;
	
	public void fileSelectedActionListener(ActionEvent event) {
		fileSelected=true;
    }

    public String validateImportedFile() throws IOException {
        if (this.uploadFile.getUploadedFile() != null && fileSelected) {
        	this.uploadedFile = uploadFile.getUploadedFile();
        	
            boolean foundError = false; //4323 - see below
            parser.upload(this.uploadFile.getUploadedFile().getName(), this.uploadFile.getUploadedFile().getInputStream());

            if(parser.getErrors()!=null && parser.getErrors().size()>0) {
				foundError = true;
				this.error = true;
				this.importStatus = parser.getErrors().get(0);
				return "data_utility_tradd";
			}

            if (foundError == false) {
                state = AddState.HAVE_FILE; ////4323 -- prior to this we were always changing state and advancing screen even if there was no file etc...
            }
            
            this.noOfBytesInFile = parser.getTotalBytes();
			this.noOfRows = parser.getLinesInFile();
            this.importStatus = "Click Ok to import the selected File";
    		this.error = false;
    		this.displayOk = true;
    		this.displaySelect = false;
        }
	    else {
			this.importStatus = "Enter or Browse for a File and click Select to Select a File for Import";
			this.error = true;
			return "data_utility_tradd";
		}
  
        return "data_utility_tradd";
    }
    
    private boolean displayCancel = true;
	private boolean displayClose = false;
	private boolean error = false;

	public boolean isError() {
		return error;
	}

	public void setError(boolean error) {
		this.error = error;
	}
	
	public boolean isDisplayCancel() {
		return displayCancel;
	}

	public void setDisplayCancel(boolean displayCancel) {
		this.displayCancel = displayCancel;
	}

	public boolean isDisplayClose() {
		return displayClose;
	}

	public void setDisplayClose(boolean displayClose) {
		this.displayClose = displayClose;
	}

    public String processAddUpdateAction() {
        background = null;
        state = AddState.NEED_FILE;
        if(parser!=null){
        	parser.clear();
        }
        parser = new TaxRateUploadParser(FacesUtils.getServletContext().getRealPath("") + "/taxratefile/");
        
        if(this.uploadFile==null){
        	this.uploadFile = new HtmlInputFileUpload();
        }    
        
        this.importStatus = "Enter or Browse for a File and click Select to Select a File for Import";
        this.fileName = "";
        this.error = false;
		this.displaySelect = true;
		this.displayOk = false;
		this.displayCancel = true;
		this.displayClose = false;
		this.noOfBytesInFile = 0;
		this.noOfRows = 0;
		this.batchNumber = 0;
		this.fileSelected=false;

        return "data_utility_tradd";
    }

    public void sortAction(ActionEvent e) {
        batchMaintenanceDataModel.toggleSortOrder(e.getComponent().getParent().getId());
    }

    public String selectAllAction() {
        batchMaintenanceDataModel.setSelectAllTaxRateUpdates();
        return null;
    }

    public String refreshAction() {
        init();
        batchMaintenanceDataModel.refreshData(false);
        selectedBatchIndex = -1;
        return "uploadSuccess";
    }
    
    private boolean displayOk = false;
    private boolean displaySelect = false;
    private long batchNumber;
    
    public long getBatchNumber() {
		return batchNumber;
	}

	public void setBatchNumber(long batchNumber) {
		this.batchNumber = batchNumber;
	}
    
    public boolean isDisplayOk() {
		return displayOk;
	}

	public void setDisplayOk(boolean displayOk) {
		this.displayOk = displayOk;
	}
	
	public boolean isDisplaySelect() {
		return displaySelect;
	}

	public void setDisplaySelect(boolean displaySelect) {
		this.displaySelect = displaySelect;
	}

    public String importOkSubmit() {
        selectedBatch = new BatchMaintenance();
        // since this runs in a background thread, it has no access to a FacesContext, so we need to get it now in foreground
        selectedBatch.setUpdateUserId(Auditable.currentUserCode());
        selectedBatch.setUpdateTimestamp(new Date());
        selectedBatch.setBatchTypeCode(TaxRateUpdatesBatch.BATCH_CODE);
        selectedBatch.setBatchStatusCode(TaxRateUpdatesBatch.IMPORTING);
        selectedBatch.setTotalBytes(parser.getTotalBytes());
        selectedBatch.setVc01(parser.getUploadFileName());
        selectedBatch.setVc02(parser.getFileType());
        //fixed for 0002743
        if (parser.getReleaseVersion() != null)
            selectedBatch.setNu01(new Double(parser.getReleaseVersion()));
        selectedBatch.setTs01(parser.getReleaseDate());
        selectedBatch.setEntryTimestamp(new Date());
        selectedBatch.setBatchId(null); // new record
        batchMaintenanceDataModel.getBatchMaintenanceDAO().save(selectedBatch);
        
        this.batchNumber = selectedBatch.getBatchId();
        this.importStatus = "Importing";
        this.displayOk = false;
        this.displayClose = true;
        this.displayCancel = false;
        this.displayClose = true;

        processUpdateInBackground(selectedBatch);
		
		selectedBatch = null;

        return null;
    }
   /* 
    public String importOkSubmit() {

		String fileName = this.uploadedFile.getName();
		try {
			newBatch = createBatch(fileName);
			if (newBatch != null) {
				processUpdateInBackground(newBatch);
				newBatch = null;
			}
		} catch (Exception e) {
			e.printStackTrace();
			this.importStatus = "Error while inserting rows";
			return "import_config_file";

		}
		this.importStatus = "Importing";
		this.displayOk = false;
		this.displayClose = true;
		this.displayCancel = false;
		this.displayClose = true;
		return "import_config_file";

	}
    */
    public static DateFormat logDateformat = new SimpleDateFormat(
			"yyyy-MM-dd HH:mm:ss.SSS");
    private Connection conn = null;
	private BufferedWriter outAID = null;
	long startTime = 0;
	
	public void writeLogAID(String message) {
		writeLogAID(message, true);
	}

	public void writeLogAID(String message, boolean email) {
		Date d = new Date(System.currentTimeMillis());
		String currentDateTime = logDateformat.format(d);
		try {
			outAID.write("[" + currentDateTime + "] " + message);
			outAID.newLine();
			outAID.flush();
		} catch (Exception e) {
		}
	}
	
	public boolean initVerification(Connection connection, BufferedWriter out) {
		this.conn = connection;
		this.outAID = out;
		
		startTime = System.currentTimeMillis();
		writeLogAID("************ Time counted ..... ************");
		writeLogAID(" ");

		try {
			writeLogAID("Database URL:      " + conn.getMetaData().getURL());
		} catch (Exception e) {
		}

		return true;
	}
	
	
    
    public String addActionAID(String fileName, String filePath, String folderPath) {
    	writeLogAID("File Imported:    " + filePath); // We actually use this
		// file to import.
    	writeLogAID(" ");
    	writeLogAID("** Started import tax rates procedure");
    	
    	try {
    		boolean foundError = false; //4323 - see below
            InputStream inputStream = new FileInputStream(filePath);
            parser = new TaxRateUploadParser(folderPath);
            parser.upload(fileName, inputStream);
                              
            for (String m : parser.getErrors()) {
                addError(m);
                foundError = true;
            }
            if (foundError == false) {
               // state = AddState.HAVE_FILE; ////4323 -- prior to this we were always changing state and advancing screen even if there was no file etc...
            }
		} catch (IOException dfe) {
			writeLogAID("Error: Importing stopped with error, " + dfe.getMessage());
			return null;
		}
    	


        selectedBatch = new BatchMaintenance();
        selectedBatch.setUpdateUserId("STSCORP");
        selectedBatch.setUpdateTimestamp(new Date());
        selectedBatch.setBatchTypeCode(TaxRateUpdatesBatch.BATCH_CODE);
        selectedBatch.setBatchStatusCode(TaxRateUpdatesBatch.IMPORTING);
        selectedBatch.setTotalBytes(parser.getTotalBytes());
        selectedBatch.setVc01(parser.getUploadFileName());
        selectedBatch.setVc02(parser.getFileType());
        //fixed for 0002743
        if (parser.getReleaseVersion() != null)
            selectedBatch.setNu01(new Double(parser.getReleaseVersion()));
        selectedBatch.setTs01(parser.getReleaseDate());
        selectedBatch.setEntryTimestamp(new Date());
        selectedBatch.setBatchId(null); // new record
        
        //batchMaintenanceDataModel.getBatchMaintenanceDAO().save(selectedBatch);
        
        BatchMaintenance batchMaintenance = ImportMapAddBean.addBatchUsingJDBC(selectedBatch, conn);
        if (batchMaintenance == null) {
			// Cannot create a new batch
			writeLogAID("Error: Cannot create new Batch Id. Importing stopped.");

			return null;
		}
		writeLogAID("Batch Id: " + batchMaintenance.getBatchId() + " created, Batch Status Code: " + selectedBatch.getBatchStatusCode());

		processUpdateInBackgroundAID(selectedBatch, conn, folderPath);
        selectedBatch = null;
        return null;
    }
    
    private void processUpdateInBackgroundAID(final BatchMaintenance batch, Connection conn, String folderPath) {
       // String selectedDb = ChangeContextHolder.getDataBaseType();
        //dbPropertiesDTO = dbPropertiesBean.getDbPropertiesDTO(selectedDb);

      //  state = AddState.PROCESSING;
       // backgroundError = null;
      //  background = new Thread(new Runnable() {
        //    public void run() {
       //         System.out.println("############ processUpdateInBackground for Tax Rate update using URL: " + dbPropertiesDTO.getUrl());

         //       try {
         //           Thread.sleep(2000);
          //      } catch (Exception ex) {
          //      }

          //      try {
          //          Class.forName(dbPropertiesDTO.getDriverClassName());
          //      } catch (ClassNotFoundException e) {
          //          System.out.println("############ Can't load class: " + dbPropertiesDTO.getDriverClassName());
           //         return;
           //    }
            //    Connection conn = null;

                try {
                    //Standalone connection
                   // conn = DriverManager.getConnection(dbPropertiesDTO.getUrl(), dbPropertiesDTO.getUserName(), dbPropertiesDTO.getPassword());
                   // conn.setAutoCommit(false);
                    
                    writeLogAID("Importing data file....");

                    //Import tax Rates update
                    importTaxRateUpdatesUsingJDBC(batch, parser, conn);
                    
                    writeLogAID("Importing data file.... done");


                    //4947
                    if (parser.getErrorCountTotal() > 0) {
                        batch.setHeldFlag("1");
                        batch.setErrorSevCode(BatchMaintenance.ErrorState.Error);
                        backgroundError = "Error count: " + parser.getErrorCountTotal();
                    }//Fixed for issue 0002743
                    if (parser.isAbort())
                        batch.setBatchStatusCode(TaxRateUpdatesBatch.ABORTED);
                    else
                        batch.setBatchStatusCode("IA");
                    
                    batch.setNu02(parser.getStateUseRateTotal());
                    batch.setTotalRows(new Long(parser.getRows()));
                    batch.setStatusUpdateTimestamp(new Date());
                    batch.setStatusUpdateUserId(batch.getUpdateUserId());
                    
                    updateBatchUsingJDBC(batch, conn);
                    
                    //Continue to process the batch when HeldFlag is not 1 and BatchStatusCode is 'I'
                    if((batch.getHeldFlag()==null || !batch.getHeldFlag().equalsIgnoreCase("1")) && batch.getBatchStatusCode().equalsIgnoreCase("IA")){
        				writeLogAID("** Started 'Flagged for Process' procedure for Batch Id, " + batch.getbatchId());

        				batch.setBatchStatusCode("FP");
        				// Control already done with the timezone conversion, UTC
        				batch.setSchedStartTimestamp(new Date());
        				batch.setUpdateTimestamp(new Date());
        				batch.setStatusUpdateTimestamp(new Date());
        				batch.setStatusUpdateUserId("STSCORP");
        				updateBatchUsingJDBC(batch, conn);

        				writeLogAID("Set Batch Status Code to FP");
        				writeLogAID("** Finished 'Flagged for Process' procedure");
        				writeLogAID(" ");
        			}
                } catch (Throwable e) {
                    e.printStackTrace();
                    
                    writeLogAID("Error: Importing aborted with error, "
        					+ e.getMessage());

                    //logger.debug(e);
                    //backgroundError = ("Unable to save batch: " + e.getMessage());
                } finally {
                    //if (conn != null)
                    //    try {
                     //       conn.close();
                     //   } catch (SQLException ignore) {

                    //    }
                }

                // force completion
              //  state = AddState.COMPLETE;
              //  logger.debug("Background complete");
            //}
       // });
       // background.start();
       //logger.debug("Background started");
    }
    
    public static void main(String[] args) {
            
            int mb = 1024*1024;
             
            //Getting the runtime reference from system
            Runtime runtime = Runtime.getRuntime();
             
            System.out.println("##### Heap utilization statistics [MB] #####");
             
            //Print used memory
            System.out.println("Used Memory:"
                + (runtime.totalMemory() - runtime.freeMemory()) / mb);
     
            //Print free memory
            System.out.println("Free Memory:"
                + runtime.freeMemory() / mb);
             
            //Print total available memory
            System.out.println("Total Memory:" + runtime.totalMemory() / mb);
     
            //Print Maximum available memory
            System.out.println("Max Memory:" + runtime.maxMemory() / mb);
   

    	
		System.out.println("############ main for Automatic Import of Data");

		String fileName = "exp_UPDATE_2015_7_5.txt";
		String filePath = "C:\\AP_IMPORT\\TAX_RATES\\exp_UPDATE_2015_7_5.txt";
		//String specStr = "MBF_PCO_TEST";
		String folderPath = "C:\\AP_IMPORT\\TAX_RATES\\";
		String logFile = "C:\\AP_IMPORT\\TAX_RATES\\exp_UPDATE_2015_7_5.log";

		BufferedWriter out = null;
		try {
			out = new BufferedWriter(new FileWriter(logFile));
			// out.write("aString");
		} catch (Exception e) {
		}

		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
		} catch (ClassNotFoundException e) {
			System.out
					.println("xxxxxxxxxxxxxx Can't load class: oracle.jdbc.driver.OracleDriver");
			return;
		}

		Connection conn = null;
		try {
			// Standalone connection
			conn = DriverManager.getConnection("jdbc:oracle:thin:@10.1.106.14:1521:TESTDB4", "STSCORP","STSCORP");
			conn.setAutoCommit(false);

			DataUtilityTaxRateUpdatesBackingBean importTaxRateBean = new DataUtilityTaxRateUpdatesBackingBean();
			if (importTaxRateBean.initVerification(conn, out)) {
				importTaxRateBean.addActionAID(fileName, filePath, folderPath);
			} else {
				System.out.println("Error: Stop importing tax rates, " + filePath);
			}

		} catch (Throwable e) {
			e.printStackTrace();
		} finally {
			if (conn != null)
				try {
					conn.close();
				} catch (SQLException ignore) {
				}
		}

		try {
			out.close();
		} catch (Exception e) {
		}
	}

    public String getProgress() {
        if (backgroundError != null) {
            addError(backgroundError);
            this.importStatus = "Import Complete";
            return "100.0";
        } else if (state == AddState.COMPLETE) {
            logger.debug("getProgress: complete");
            this.importStatus = "Import Complete";
            return "100.0";
        }
        NumberFormat nf = NumberFormat.getNumberInstance();
        nf.setMaximumFractionDigits(1);
        try {
            String s = "0.0";
            Double scale = parser.getProgress() * 99.0;
            if (scale >= 0.01) {
                s = nf.format(scale);
            }
            logger.debug("Progress at: " + s);
            return s;
        } catch (Exception e) {
            logger.error(e.getMessage());
            return "0.0";
        }
    }

    public boolean getDisplayFileSelect() {
        return state == AddState.NEED_FILE;
    }

    public boolean getDisplayChangeFile() {
        return state == AddState.HAVE_FILE;
    }

    public boolean getDisplayFileHeader() {
        switch (state) {
            case HAVE_FILE:
                return true;
            case PROCESSING:
                return true;
            case COMPLETE:
                return true;
            default:
                return false;
        }
    }

    public Boolean getDisplayProgress() {
        if (background != null)
            logger.debug("Progress: " + state.toString() + ", " + background.getState().toString());
        else
            logger.debug("Progress: " + state.toString());
        switch (state) {
            case PROCESSING:
                return true;
            case COMPLETE:
                return true;
            default:
                return false;
        }
    }

    public boolean getDisplayComplete() {
        return state == AddState.COMPLETE;
    }

    public Boolean getDisplayUploadAdd() {
        logger.debug("displayUploadAdd: " + state);
        return state == AddState.HAVE_FILE;
    }

    public boolean getBackgroundInProgress() {
        return state == AddState.PROCESSING;
    }

    public String closeAction() {
        background = null;

        batchMaintenanceDataModel.refreshData(false);
        // a new batch has been created, so clear selection
        selectedBatchIndex = -1;
        selectedBatch = null;
        return "tru_cancel_action";
    }

    private void processUpdateInBackground(final BatchMaintenance batch) {
        String selectedDb = ChangeContextHolder.getDataBaseType();
        dbPropertiesDTO = dbPropertiesBean.getDbPropertiesDTO(selectedDb);

        state = AddState.PROCESSING;
        backgroundError = null;
        background = new Thread(new Runnable() {
            public void run() {
                System.out.println("############ processUpdateInBackground for Tax Rate update using URL: " + dbPropertiesDTO.getUrl());

                try {
                    Thread.sleep(2000);
                } catch (Exception ex) {
                }

                try {
                    Class.forName(dbPropertiesDTO.getDriverClassName());
                } catch (ClassNotFoundException e) {
                    System.out.println("############ Can't load class: " + dbPropertiesDTO.getDriverClassName());
                    return;
                }
                Connection conn = null;

                try {
                    //Standalone connection
                    conn = DriverManager.getConnection(dbPropertiesDTO.getUrl(), dbPropertiesDTO.getUserName(), dbPropertiesDTO.getPassword());
                    conn.setAutoCommit(false);

                    //Import tax Rates update
                    importTaxRateUpdatesUsingJDBC(batch, parser, conn);

                    //4947
                    if (parser.getErrorCountTotal() > 0) {
                        batch.setHeldFlag("1");
                        batch.setErrorSevCode(BatchMaintenance.ErrorState.Error);
                        backgroundError = "Error count: " + parser.getErrorCountTotal();
                    }//Fixed for issue 0002743
                    if (parser.isAbort())
                        batch.setBatchStatusCode(TaxRateUpdatesBatch.ABORTED);
                    else
                        batch.setBatchStatusCode(TaxRateUpdatesBatch.IMPORTED);
                    batch.setNu02(parser.getStateUseRateTotal());
                    batch.setTotalRows(new Long(parser.getRows()));
                    batch.setStatusUpdateTimestamp(new Date());
                    batch.setStatusUpdateUserId(batch.getUpdateUserId());

                    updateBatchUsingJDBC(batch, conn);

                    //Since this runs in a background thread, it has no access to a FacesContext.
                    //addInfo("Uploaded " + parser.getRows() + " tax rate jurisdiction update records");

                } catch (Throwable e) {
                    e.printStackTrace();
                    logger.debug(e);
                    backgroundError = ("Unable to save batch: " + e.getMessage());
                } finally {
                    if (conn != null)
                        try {
                            conn.close();
                        } catch (SQLException ignore) {

                        }
                }

                // force completion
                state = AddState.COMPLETE;
                logger.debug("Background complete");
            }
        });
        background.start();
        logger.debug("Background started");
    }

    protected BatchMaintenance importTaxRateUpdatesUsingJDBC(BatchMaintenance batchMaintenance,
                                                             TaxRateUploadParser parser, Connection conn) throws Exception {

        Iterator<BCPJurisdictionTaxRate> truIter = parser.iterator();
        
        if (outAID != null) {
			writeLogAID("   ... importTaxRateUpdatesUsingJDBC");
		}

        String sql = null;
        String sqlText = null;
        PreparedStatement prep = null;
        PreparedStatement prepText = null;
        Statement stat = null;
        
        if (outAID != null) {
			writeLogAID("   ... before loop");
		}

        int i = 1;
        while (truIter.hasNext()) {
            BCPJurisdictionTaxRate tr = truIter.next();

            //4947
            if (tr != null && tr.getLine() != null) {
                BCPJurisdictionTaxRateText txt = new BCPJurisdictionTaxRateText(batchMaintenance.getBatchId(), tr.getLine(), tr.getText());

                if (prep == null) {
                    sql = tr.getRawInsertStatement();
                    prep = conn.prepareStatement(sql);
                    sqlText = txt.getRawInsertStatement();
                    prepText = conn.prepareStatement(sqlText);
                    stat = conn.createStatement();
                }

                tr.setBcpJurisdictionTaxRateId(getNextBcpJurisdictionTaxRateId(stat));
                tr.setBatchId(batchMaintenance.getbatchId());

                //populate data to Prepared Statement
                tr.populatePreparedStatement(conn, prep);
                prep.addBatch();

                txt.populatePreparedStatement(conn, prepText);
                prepText.addBatch();

                if ((i++ % 50) == 0) {
                    logger.debug("Begin Flush: " + i);
                    
                    if (outAID != null) {
						writeLogAID("   ... Begin Flush: record " + (i - 1));
					}

                    prep.executeBatch();
                    prepText.executeBatch();
                    conn.commit();
                    prep.clearBatch();
                    prepText.clearBatch();
                    logger.debug("End Flush");
                    
                    if (outAID != null) {
						writeLogAID("   ... End Flush");
					}

                    parser.setLinesWritten(i);
                }
                tr = null;
                txt = null;
            }

            //0001701: Import errors make the system crash
            if (parser.getBatchErrorLogs().size() >= 50) {
                //flush every 50+ errors
                flushErrorLog(conn, batchMaintenance);
            }
        }

        if (prep != null) {
        	if (outAID != null) {
				writeLogAID("   ... Begin Final Flush: record " + (i - 1));
			}

            prep.executeBatch();
            prepText.executeBatch();
            conn.commit();
            prep.clearBatch();
            prepText.clearBatch();
            
            if (outAID != null) {
				writeLogAID("   ... End Final Flush");
			}

            try {
                prep.close();
            } catch (Exception ex) {
            }
            try {
                prepText.close();
            } catch (Exception ex) {
            }
        }
        
        if (outAID != null) {
			writeLogAID("   ### Total Rows Imported: " + (i - 1));
		}

        // perform the checksum and generate any more ErrorLogs
        parser.setCacheManager(cacheManager);
        parser.checksum();

        //0001701: Import errors make the system crash
        flushErrorLog(conn, batchMaintenance);
        
		if (outAID != null) {
			writeLogAID("   ### Total Error Count: "
					+ parser.getErrorCountTotal());
		}

        if (stat != null) {
            try {
                stat.close();
            } catch (Exception ex) {
            }
        }

        parser.setLinesWritten(i);

        return batchMaintenance;
    }

    void flushErrorLog(Connection conn, BatchMaintenance batchMaintenance) throws Exception {
        String sql = null;
        PreparedStatement prep = null;
        Statement statErrorNext = null;

        int ie = 1;
        for (BatchErrorLog error : parser.getBatchErrorLogs()) {
            if (prep == null) {
                sql = error.getRawInsertStatement();
                prep = conn.prepareStatement(sql);
                //fixed for issue 002743
                if (error.getErrorDefCode().equalsIgnoreCase("IP12"))
                    parser.setAbort(true);
                statErrorNext = conn.createStatement();
            }


            error.setBatchErrorLogId(getNextBatchErrorLogId(statErrorNext));
            error.setProcessType(TaxRateUpdatesBatch.IMPORT);
            error.setProcessTimestamp(batchMaintenance.getEntryTimestamp());
            error.setBatchId(batchMaintenance.getBatchId());
            error.populatePreparedStatement(conn, prep);
            prep.addBatch();

            if ((ie++ % 50) == 0) {
                prep.executeBatch();
                conn.commit();
                prep.clearBatch();
            }
        }

        if (prep != null) {
            prep.executeBatch();
            conn.commit();
            prep.clearBatch();

            try {
                prep.close();
            } catch (Exception ex) {
            }
        }

        if (statErrorNext != null) {
            try {
                statErrorNext.close();
            } catch (Exception ex) {
            }
        }

        parser.resetBatchErrorLogs();
    }

    protected void updateBatchUsingJDBC(BatchMaintenance batchMaintenance, Connection conn) throws Exception {
        String sql = batchMaintenance.getRawUpdateStatement();
        PreparedStatement prep = conn.prepareStatement(sql);
        batchMaintenance.populateUpdatePreparedStatement(conn, prep);
        prep.addBatch();

        prep.executeBatch();
        conn.commit();
        prep.clearBatch();

        return;
    }

    private long getNextBcpJurisdictionTaxRateId(Statement stat) {
        long nextId = -1;
        try {
            String sql = DatabaseUtil.getSequenceQuery(DatabaseUtil.getDatabaseProductName(stat), "sq_tb_bcp_juris_taxrate_id", "NEXTID");

            ResultSet rs = stat.executeQuery(sql);
            if (rs.next()) {
                nextId = rs.getLong("NEXTID");
            }
            rs.close();
        } catch (Exception e) {
            logger.error("############ getNextBcpJurisdictionTaxRateId() failed: " + e.toString());
        }

        return nextId;
    }

    private long getNextBatchErrorLogId(Statement statErrorNext) {
        long nextId = -1;
        try {
            String sql = DatabaseUtil.getSequenceQuery(DatabaseUtil.getDatabaseProductName(statErrorNext), "sq_tb_batch_error_log_id", "NEXTID");

            //Statement stat = conn.createStatement();
            ResultSet rs = statErrorNext.executeQuery(sql);
            if (rs.next()) {
                nextId = rs.getLong("NEXTID");
            }
            rs.close();
        } catch (Exception e) {
            logger.error("############ getNextBatchErrorLogId() failed: " + e.toString());
        } finally {
        }

        return nextId;
    }

    /**
     * Test if anything is "selected"
     */
    private boolean getHasSelection() {
        Iterator<BatchMaintenance> iter = batchMaintenanceDataModel.iterator();
        while (iter.hasNext()) {
            if (iter.next().getSelected()) {
                return true;
            }
        }
        return false;
    }

    public void selectedRowChanged(ActionEvent e) throws AbortProcessingException {
        UIComponent uiComponent = e.getComponent().getParent();
        if (!(uiComponent instanceof UIDataAdaptor)) {
            logger.info("Invalid class to event listener: " + uiComponent.getClass().getName());
        }
        UIDataAdaptor table = (UIDataAdaptor) uiComponent;
        if (table != null) {
            this.selectedBatch = new TaxRateUpdatesBatch((BatchMaintenance) table.getRowData());
            this.selectedBatchIndex = table.getRowIndex();
            BatchMetadata batchMetadata = batchMaintenanceService.findBatchMetadataById(selectedBatch.getBatchTypeCode());
            batchMetadataList = new BatchMetadataList(batchMetadata, (BatchMaintenance)table.getRowData());
        }
    }

    public String viewAction() {
        bcpJurisdictionTaxRateDataModel.setBatchMaintenance(selectedBatch);
        if (getUploadRecordCount()) {
            return viewFormattedAction();
        } else {
            return viewUnformattedAction();
        }
    }

    public String viewUnformattedAction() {
        bcpJurisdictionTaxRateTextDataModel.setBatchMaintenance(selectedBatch);
//		parser.loadFileData(this.selectedBatch.getVc01());
//		for (String m : parser.getErrors()) {
//			addError(m);
//		}
        return "tru_view_unformatted";
    }

    public String viewFormattedAction() {
        bcpJurisdictionTaxRateDataModel.setBatchMaintenance(selectedBatch);
        return "tru_view_formatted";
    }

    public String cancelAction() {
        background = null;
        if(parser!=null){
        	parser.clear();
        }
        return "tru_cancel_action";
    }

    public String errorsAction() {
        batchErrorsBean.setBatchMaintenance(selectedBatch);
        batchErrorsBean.setReturnView("taxrate_updates");

        //0001701: Import errors make the system crash
        batchErrorsBean.getBatchErrorDataModel().setBatchMaintenance(selectedBatch);

        return "batch_maintenance_errors";
    }

    public String statisticsAction() {
        batchStatistics = new BatchStatistics(selectedBatch,
                batchMaintenanceDataModel.getBatchMaintenanceDAO(),
                filePreferenceBean.getBatchPreferenceDTO());
        batchStatistics.calcStatistics();
        return "tru_batch_statistics";
    }

    public void refreshStatistics() {
        batchStatistics.calcStatistics();
    }

    public String processAction() {

        //0003417
        int lastTaxRateRelUpdate = 0;
        Option option = optionService.findByPK(new OptionCodePK("LASTTAXRATERELUPDATE", "SYSTEM", "SYSTEM"));
        String lastTaxRateRelUpdateString = (option == null || option.getValue() == null) ? "" : option.getValue();
        try {
            lastTaxRateRelUpdate = Integer.parseInt(lastTaxRateRelUpdateString);
        } catch (NumberFormatException e) {
        }

        Date lastTaxRateDateUpdate = null;
        option = optionService.findByPK(new OptionCodePK("LASTTAXRATEDATEUPDATE", "SYSTEM", "SYSTEM"));
        String lastTaxRateDateUpdateString = (option == null || option.getValue() == null) ? "" : option.getValue();
        try {
            lastTaxRateDateUpdate = new SimpleDateFormat("yyyy_MM").parse(lastTaxRateDateUpdateString);
        } catch (ParseException pe) {
        }

        TaxRateUpdateBatchValidator validator = new TaxRateUpdateBatchValidator(
                batchMaintenanceDataModel, jurisdictionDAO, lastTaxRateRelUpdate, lastTaxRateDateUpdate);
        Iterator<BatchMaintenance> iter = batchMaintenanceDataModel.iterator();
        selectedBatches = new ArrayList<TaxRateUpdatesBatch>();
        while (iter.hasNext()) {

            //TaxRateUpdateBatchValidator validator = new TaxRateUpdateBatchValidator(
            //		batchMaintenanceDataModel, jurisdictionDAO, filePreferenceBean.getImportMapProcessPreferenceDTO());
            //Iterator<BatchMaintenance> iter = batchMaintenanceDataModel.iterator();
            //selectedBatches = new ArrayList<TaxRateUpdatesBatch>();
            //while (iter.hasNext()) {
            BatchMaintenance bm = iter.next();
            if (bm.getSelected())
                selectedBatches.add(new TaxRateUpdatesBatch(bm));
        }
        if ((selectedBatches.size() < 1) && (selectedBatch != null))
            selectedBatches.add(new TaxRateUpdatesBatch(selectedBatch));
        // Iterate through batches in reverse order, adjusting last processed along the way
        Collections.sort(selectedBatches, new BatchComparator());
        for (int i = selectedBatches.size(); i > 0; i--) {
            validator.validate(selectedBatches.get(i - 1));
        }

        return "tru_submit_batches";
    }

    public String submitProcessAction() {
        //4634 Removed by MF, AC
        //if (((Date)batchStartTime.getValue()).before(new Date())) {
        //	JsfHelper.addError("Cannot use submit date in the past");
        //	return null;
        //}

        for (TaxRateUpdatesBatch batch : selectedBatches) {
            if (batch.getError() == null && selectedBatches.size() == 1) {
                BatchMaintenance bm = batchMaintenanceDataModel.getById(batch.getBatchId());
                bm.setBatchStatusCode(TaxRateUpdatesBatch.PROCESS);
                bm.setSchedStartTimestamp((Date) batchStartTime.getValue());
                batchMaintenanceService.update(bm);
                batchMaintenanceDataModel.refreshData(false);
            }

            if (selectedBatches.size() > 1) {
                if (batch.getError() != null) {
                    selectedBatches.remove(batch.getBatchId());
                    logger.info("!!!!!Batch id Ommited !!!!!!! " + batch.getBatchId());
                    continue;
                }
                BatchMaintenance bm = batchMaintenanceDataModel.getById(batch.getBatchId());
                logger.info("Batch id Updated " + batch.getBatchId());
                bm.setBatchStatusCode(TaxRateUpdatesBatch.PROCESS);
                bm.setSchedStartTimestamp((Date) batchStartTime.getValue());
                batchMaintenanceService.update(bm);
            }
        }
        batchMaintenanceDataModel.refreshData(false);
        selectedBatchIndex = -1;
        return "taxrate_updates";
    }

    private void init() {
        batchMaintenanceDataModel.setDefaultSortOrder("batchId", BatchMaintenanceDataModel.SORT_DESCENDING);
        exampleBatchMaintenance = new BatchMaintenance();
        batchMaintenanceBean.resetSearchAction(); 
        exampleBatchMaintenance.setEntryTimestamp(null);
        exampleBatchMaintenance.setBatchStatusCode(null);
        exampleBatchMaintenance.setBatchTypeCode(TaxRateUpdatesBatch.BATCH_CODE);
        batchMaintenanceBean.setBatchTypeCode(TaxRateUpdatesBatch.BATCH_CODE) ; 
    	batchMaintenanceBean.setCalledFromConfigImportExpMenu(false);
        batchMaintenanceBean.prepareBatchTypeSpecificsFields();
        batchMaintenanceDataModel.setPageSize(50);
        batchMaintenanceDataModel.setCriteria(exampleBatchMaintenance);

    }

    private void addError(String s) {
        addMessage(s, FacesMessage.SEVERITY_ERROR);
    }

    private void addInfo(String s) {
        addMessage(s, FacesMessage.SEVERITY_INFO);
    }

    private void addMessage(String s, FacesMessage.Severity severity) {
        FacesContext.getCurrentInstance().addMessage(null,
                new FacesMessage(severity, s, null));
    }

    /**
     * @return the trBatchMaintenanceDataModel
     */
    public BatchMaintenanceDataModel getBatchMaintenanceDataModel() {
       
        return this.batchMaintenanceDataModel;
    }

    public Boolean getDisplayProcess() {
        if ((selectedBatchIndex != -1) && (selectedBatch != null)
                && (TaxRateUpdatesBatch.canBeProcessed(selectedBatch))) return true;
        return getHasSelection();
    }

    public Boolean getDisplayError() {
        return (selectedBatchIndex != -1) && (selectedBatch.getErrorSevCode() != null) &&
                (selectedBatch.getErrorSevCode().trim().length() > 0) && (!selectedBatch.getErrorSevCode().trim().equals("0"));
    }

    public Boolean getUploadRecordCount() {
        return (bcpJurisdictionTaxRateDataModel.count() > 0);
    }

    /**
     * @return the batchMaintenanceService
     */
    public BatchMaintenanceService getBatchMaintenanceService() {
        return this.batchMaintenanceService;
    }

    /**
     * @param batchMaintenanceService the batchMaintenanceService to set
     */
    public void setBatchMaintenanceService(BatchMaintenanceService batchMaintenanceService) {
        this.batchMaintenanceService = batchMaintenanceService;
    }

    /**
     * @return the batchErrorsBean
     */
    public BatchErrorsBackingBean getBatchErrorsBean() {
        return this.batchErrorsBean;
    }

    /**
     * @param batchErrorsBean the batchErrorsBean to set
     */
    public void setBatchErrorsBean(BatchErrorsBackingBean batchErrorsBean) {
        this.batchErrorsBean = batchErrorsBean;
    }

    /**
     * @return the batchMaintenanceBean
     */
    public BatchMaintenanceBackingBean getBatchMaintenanceBean() {
        return this.batchMaintenanceBean;
    }

    /**
     * @param batchMaintenanceBean the batchMaintenanceBean to set
     */
    public void setBatchMaintenanceBean(BatchMaintenanceBackingBean batchMaintenanceBean) {
        this.batchMaintenanceBean = batchMaintenanceBean;
    }

    /**
     * @return the selectedBatch
     */
    public BatchMaintenance getSelectedBatch() {
        return this.selectedBatch;
    }

    /**
     * @return the selectedBatchIndex
     */
    public int getSelectedBatchIndex() {
        return this.selectedBatchIndex;
    }

    /**
     * @param selectedBatchIndex the selectedBatchIndex to set
     */
    public void setSelectedBatchIndex(int selectedBatchIndex) {
        this.selectedBatchIndex = selectedBatchIndex;
    }

    /**
     * @param batchMaintenanceDataModel the batchMaintenanceDataModel to set
     */
    public void setBatchMaintenanceDataModel(BatchMaintenanceDataModel batchMaintenanceDataModel) {
        this.batchMaintenanceDataModel = batchMaintenanceDataModel;
    }

    /**
     * @return the batchMetadata
     */
    public BatchMetadata getBatchMetadata() {
        if (this.batchMetadata == null) {
            this.batchMetadata = batchMaintenanceService.findBatchMetadataById(TaxRateUpdatesBatch.BATCH_CODE);
        }
        return this.batchMetadata;
    }

    /**
     * @param batchMetadata the batchMetadata to set
     */
    public void setBatchMetadata(BatchMetadata batchMetadata) {
        this.batchMetadata = batchMetadata;
    }

    /**
     * @return the uploadFile
     */
    public HtmlInputFileUpload getUploadFile() {
        return this.uploadFile;
    }

    public void setUploadFile(HtmlInputFileUpload uploadFile) {
        this.uploadFile = uploadFile;
    }

    public String getFileType() {
        return parser.getFileType();
    }

    public String getUploadFileName() {
        return parser.getUploadFileName();
    }

    public Date getSystemDate() {
        return this.systemDate;
    }

    public void setSystemDate(Date systemDate) {
        this.systemDate = systemDate;
    }

    public String getLastTaxRateRelUpdate() {
    	Option option = optionService.findByPK(new OptionCodePK("LASTTAXRATERELUPDATE", "SYSTEM", "SYSTEM"));
    	return (option == null || option.getValue() == null) ? "" : option.getValue();
    }
    
    public String getLastTaxRateDateUpdate() {
    	Option option = optionService.findByPK(new OptionCodePK("LASTTAXRATEDATEUPDATE", "SYSTEM", "SYSTEM"));
    	return (option == null || option.getValue() == null) ? "" : option.getValue();
    }

    public Date getReleaseDate() {
        return parser.getReleaseDate();
    }


    public String getReleaseVersion() {
        return this.parser.getReleaseVersion();
    }
    
    public String getReleaseDateAndVersion() {
    	String dateAndVersion = "";
    	if(parser.getReleaseDate()!=null){	
    		dateAndVersion = new SimpleDateFormat("yyyy_MM").format(parser.getReleaseDate());   				
    	    dateAndVersion = dateAndVersion + " / ";
    	}
    	
    	if(parser.getReleaseVersion()!=null){
    		dateAndVersion = dateAndVersion + Double.valueOf(parser.getReleaseVersion());
    	}
    	
        return dateAndVersion;
    }

    public BCPJurisdictionTaxRateDataModel getBcpJurisdictionTaxRateDataModel() {
        return this.bcpJurisdictionTaxRateDataModel;
    }

    public void setBcpJurisdictionTaxRateDataModel(BCPJurisdictionTaxRateDataModel model) {
        this.bcpJurisdictionTaxRateDataModel = model;
    }

    public HtmlCalendar getBatchStartTime() {
        batchStartTime.resetValue();
        batchStartTime.setValue(filePreferenceBean.getBatchPreferenceDTO().getDefaultBatchStartTime());
        return this.batchStartTime;
    }

    public void setBatchStartTime(HtmlCalendar batchStartTime) {
        this.batchStartTime = batchStartTime;
    }

    public FilePreferenceBackingBean getFilePreferenceBean() {
        return this.filePreferenceBean;
    }

    public void setFilePreferenceBean(FilePreferenceBackingBean filePreferenceBean) {
        this.filePreferenceBean = filePreferenceBean;
    }

    public BCPJurisdictionTaxRateTextDataModel getBcpJurisdictionTaxRateTextDataModel() {
        return this.bcpJurisdictionTaxRateTextDataModel;
    }

    public void setBcpJurisdictionTaxRateTextDataModel(
            BCPJurisdictionTaxRateTextDataModel bcpJurisdictionTaxRateTextDataModel) {
        this.bcpJurisdictionTaxRateTextDataModel = bcpJurisdictionTaxRateTextDataModel;
    }

    public BatchStatistics getBatchStatistics() {
        return this.batchStatistics;
    }

    public JurisdictionDAO getJurisdictionDAO() {
        return this.jurisdictionDAO;
    }

    public void setJurisdictionDAO(JurisdictionDAO jurisdictionDAO) {
        this.jurisdictionDAO = jurisdictionDAO;
    }

    public List<TaxRateUpdatesBatch> getSelectedBatches() {
        return this.selectedBatches;
    }

    public class BatchComparator implements Comparator<TaxRateUpdatesBatch> {

        @Override
        public int compare(TaxRateUpdatesBatch o1, TaxRateUpdatesBatch o2) {
            if (o1.getReleaseDate().getTime() > o2.getReleaseDate().getTime())
                return -1;
            else if (o1.getReleaseDate().getTime() < o2.getReleaseDate().getTime())
                return 1;
            else {
                if (o1.getReleaseVersion() > o2.getReleaseVersion())
                    return -1;
                else if (o1.getReleaseVersion() < o2.getReleaseVersion())
                    return 1;
            }
            return 0;
        }

    }

    public void setDbPropertiesBean(DbPropertiesBean dbPropertiesBean) {
        this.dbPropertiesBean = dbPropertiesBean;
    }


    public String updateUserFieldsAction() {
        return "data_utility_tax_rate_update_user_fields";
    }

    public String updateUserFields() {
        batchMaintenanceService.update(batchMetadataList.update());
        return "data_utility_tax_rate_update";
    }

    public String cancelUpdateUserFields() {
        return "data_utility_tax_rate_update";
    }



    public BatchMetadataList getBatchMetadataList() {
        return batchMetadataList;
    }

    public void setBatchMetadataList(BatchMetadataList batchMetadataList) {
        this.batchMetadataList = batchMetadataList;
    }

    public boolean isNavigatedFlag() {
        return true;
    }

    public void setNavigatedFlag(boolean navigatedFlag) {}
    
    public User getCurrentUser() {
		return loginbean.getUser();
   }

  	public void retrieveBatchMaintenance() { 
  		BatchMaintenance batchMaintenance = new BatchMaintenance();
		batchMaintenance.setBatchTypeCode(exampleBatchMaintenance.getBatchTypeCode());
  		this.batchMaintenanceDataModel =  batchMaintenanceBean.prepareBatchMaintenance(batchMaintenance);
  	}
  	
  	public String resetBatchFields(){
  		batchMaintenanceBean.setResetBatchAccordion(false);
  		batchMaintenanceBean.resetOtherBatchFields();
  		return null;
  	}
  	
  	public String navigateAction() {
  		init();
  		return "data_utility_tax_rate_update";
  	}

}
