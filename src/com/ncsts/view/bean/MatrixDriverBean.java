package com.ncsts.view.bean;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIInput;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlSelectBooleanCheckbox;
import javax.faces.context.FacesContext;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;

import org.ajax4jsf.component.html.HtmlAjaxSupport;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.richfaces.component.html.HtmlDataTable;

import com.ncsts.common.CacheManager;
import com.ncsts.common.LoggerFactory;
import com.ncsts.domain.DataDefinitionColumn;
import com.ncsts.domain.DriverNames;
import com.ncsts.domain.DriverNamesPK;
import com.ncsts.domain.DriverReference;
import com.ncsts.domain.ListCodes;
import com.ncsts.domain.User;
import com.ncsts.dto.DataDefinitionColumnDTO;
import com.ncsts.dto.DriverNamesDTO;
import com.ncsts.dto.DriverReferenceDTO;
import com.ncsts.services.DataDefinitionService;
import com.ncsts.services.DriverReferenceService;
//import com.ncsts.services.ListCodesService;

/**
 * 
 * This is the backing bean for the matrixDriverView.jsp page.
 * 
 */
public class MatrixDriverBean {

  private Logger logger = LoggerFactory.getInstance().getLogger(MatrixDriverBean.class);

  private DriverReferenceService driverService;

//  private ListCodesService listCodesService;

  private DataDefinitionService dataDefinitionService;

  private List<SelectItem> categoriesMenuItems;

  private List<DriverNamesDTO> driverNames = new ArrayList<DriverNamesDTO>();

  private List<DriverReferenceDTO> driverReferences = new ArrayList<DriverReferenceDTO>();

  List<DataDefinitionColumnDTO> dataDefColumn = new ArrayList<DataDefinitionColumnDTO>();

  private HtmlInputText columnSearch = new HtmlInputText();

  private HtmlInputText descriptionSearch = new HtmlInputText();

  private HtmlInputText dataTypeSearch = new HtmlInputText();

  private List<DataDefinitionColumn> dataDefinitionColList = new ArrayList<DataDefinitionColumn>();

  private boolean showOkButton = false;

  private DataDefinitionColumn selectedDriver;
  
  private loginBean loginBean;

  // private SearchTrColumnNameHandler filterHandler = new
  // SearchTrColumnNameHandler();

  private String selectedCategory = null;

  private String selectedCategoryName = "Driver Type";
  
  @SuppressWarnings("unused")
private String actionHeading = "Add/Update/Delete a(n) Matrix Driver Type";

  private String tableName = "TB_PURCHTRANS";
 
  private DriverNamesDTO selectedDriverName = null;

  private int selectedDriverNamesIndex = -1;

  private Long driverId = 0L;

  private String matrixColumnName = null;

  private HtmlInputText transDtlColName = new HtmlInputText();
  private HtmlInputText tableColName = new HtmlInputText();
    
  private String  tableNames="TB_BILLTRANS";//PP-396
  
  //private String tableNameUser="TB_TRANS_USER";

  private String transDtlColDesc = "";

  private String userId;

  private Date userTimestamp;

  private Boolean businessUnitFlag = false;
  
  private Boolean globalBusinessUnitFlag = false; 
  
  private Boolean selectedDriverFlag = false;
  
  private Long globalDriverId = 0L;  

  private Boolean mandatoryFlag = false;

  private Boolean nullDriverFlag =  false;

  private Boolean wildcardFlag = false;

  private Boolean rangeFlag = false;

  private Boolean toNumberFlag = false;

  private Boolean activeFlag = true;

  private Boolean changeTransColumn = false;// flag to disable TransDtlColName
                                            // for used drivers.Condition to be
                                            // added..

  private boolean isDelete;
  
  private boolean updateDriver=false;//4356

  private EditAction currentAction = EditAction.VIEW;
  
  private CacheManager cacheManager;

  /**
   * Return true if there is a selected category
   */
  public Boolean getIsCategorySelected() {
    return ((selectedCategory != null) && !("S".equalsIgnoreCase(selectedCategory)));
  }

  public Boolean getCanDelete() {
    return (getIsCategorySelected()) && (selectedDriverNamesIndex >= 0) && (selectedDriverName != null) && (selectedDriverName
        .getDriverNamesPK().getDriverId().equals(driverService.getDriverNamesId(selectedCategory)));
  }
  
  public loginBean getLoginBean() {
		return this.loginBean;
  }

	public void setLoginBean(loginBean loginBean) {
		this.loginBean = loginBean;
	}

  public Boolean getCanUpdate() {
    return (getIsCategorySelected()) && (selectedDriverNamesIndex >= 0);
  }
  
  public Boolean getCanAdd() {
    //4866, replaced IsCategorySelected with CanAdd in matrixDriverView.jsp
	if(selectedCategory == null){
	  return false;
	}
  
	if (selectedCategory.equals("T")) {
		if(driverService.getDriverNamesId(selectedCategory)<30) return true;
	} 
	else if (selectedCategory.equals("GSB")) {
		if(driverService.getDriverNamesId(selectedCategory)<30) return true;
	}
	else if (selectedCategory.equals("L")) {
		if(driverService.getDriverNamesId(selectedCategory)<10) return true;
	} 
	else if (selectedCategory.equals("E")) {
		if(driverService.getDriverNamesId(selectedCategory)<10) return true;
	}
	   
    return false;
  }

  public List<SelectItem> getCategoriesMenuItems() {
      categoriesMenuItems = new ArrayList<SelectItem>();
      categoriesMenuItems.add(new SelectItem("S", "Select Driver Type"));
     // if (listCodesService != null) {
    	  List<ListCodes> listCodesList = cacheManager.getListCodesByType("MDTYPE");
       // List<ListCodes> listCodesList = listCodesService.getListCodesByCodeTypeCode("MDTYPE");
        for (ListCodes listCodes : listCodesList) {
          if (listCodes != null && !StringUtils.isEmpty(listCodes.getCodeCode()) && !StringUtils
              .isEmpty(listCodes.getDescription())) {
            categoriesMenuItems.add(new SelectItem(listCodes.getCodeCode(), listCodes
                .getDescription()));
          }
        }
      //}

    return categoriesMenuItems;
  }

  public void onCategoryChange(ValueChangeEvent eventValue) {
    logger.debug("Caught value change from category drop down");
    String newValue = (String) eventValue.getNewValue();
    String oldValue = (String) eventValue.getOldValue();
    selectedCategory = newValue;
    selectedDriverNamesIndex = -1;
    selectedDriverName = null;
    for (Iterator<SelectItem> iter = categoriesMenuItems.iterator(); iter.hasNext();) {
      SelectItem item = iter.next();
      if (selectedCategory.equalsIgnoreCase(item.getValue().toString()))
        selectedCategoryName = item.getLabel();
    }//Fixed for issue 0004622
    if (StringUtils.isNotEmpty(newValue) && !newValue.equals(oldValue)) {
       
        populateDriverNames(newValue);
    
    } else if (StringUtils.isEmpty(newValue)) {
      driverNames = new ArrayList<DriverNamesDTO>();
    }
  }

  // TODO : Add "description" to DTO from TB_DATA_DEF_COLUMN
  private void populateDriverNames(String driverCode) {
    logger.info(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>> populateDriverNames START >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");	  
    if (driverService != null) {
      List<DriverNames> dn = driverService.getDriverNamesByCode(driverCode);
      if (dn != null && !dn.isEmpty()) {
        driverNames = new ArrayList<DriverNamesDTO>();
        for (DriverNames driverName : dn) {
          String columnName = driverName.getTransDtlColName();

          DriverNamesDTO dto = new DriverNamesDTO();
          if(driverCode.equalsIgnoreCase("T")){
	          DataDefinitionColumn ddc = dataDefinitionService.getDataDefinitionColumnByTable(tableName, columnName);
	          try{
		          if(ddc!=null){
		        	  dto.setDescription(ddc.getDescription());
		          }
	          }
	          catch(Exception e){
	        	  System.out.println(e);
	          }
          }
          else if(driverCode.equalsIgnoreCase("GSB")){
        	  DataDefinitionColumn ddc = dataDefinitionService.getDataDefinitionColumnByTable(tableNames, columnName);
		         try{     
		        	 if(ddc!=null){
		        		 dto.setDescription(ddc.getDescription());
		             }
		         }
		         catch(Exception e){
		        	 System.out.println(e);
		         }         
          }
          else{
        	  DataDefinitionColumn ddc = dataDefinitionService.getDataDefinitionColumnByTable(tableName, columnName);
        	  dto.setDescription(ddc.getDescription());
          }
          
          try {
        	  BeanUtils.copyProperties(dto, driverName);
          } catch (Exception e) {
        	  e.printStackTrace();
          }
          if (dto != null && dto.getBusinessUnitFlagBoolean()) {
        	  logger.info("setting business unit flag == true");
        	  DriverNamesPK pk = dto.getDriverNamesPK();
        	  this.setGlobalDriverId(pk.getDriverId());
        	  this.setGlobalBusinessUnitFlag(true);
          } /*else {
        	  logger.info("setting business unit flag == false");        	  
        	  this.setGlobalDriverId(0L);        	  
        	  this.setGlobalBusinessUnitFlag(false);        	  
          }*/
          logger.info("201 ------------------>>> driverid = " + this.getDriverId());
          logger.info("202 ------------------>>> businessUnitFlag = " + this.getBusinessUnitFlag());           
          logger.info("203 ------------------>>> globalDriverId = " + this.getGlobalDriverId());
          logger.info("204 ------------------>>> globalBusinessUnitFlag = " + this.getGlobalBusinessUnitFlag());          
          driverNames.add(dto);
        }
      } else {
        driverNames = new ArrayList<DriverNamesDTO>();
      }
     // if (driverReferences != null) driverReferences.clear();//4356 require this to be commented
      // selectedDriverName = null;
      logger.info(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>> populateDriverNames END >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");      
    }

  }
  // public void selectionFilterChanged(ActionEvent e) {
  // HtmlScrollableDataTable table =
  // (HtmlScrollableDataTable)((HtmlAjaxSupport)e.getComponent()).getParent();
  // Selection selection = table.getSelection();
  // List<DataDefinitionColumnDTO> ddc = new
  // ArrayList<DataDefinitionColumnDTO>();
  // if(selection != null && selection.size() > 0){
  // for(Iterator<Object> iter= selection.getKeys(); iter.hasNext();){
  // Object next = iter.next();
  // table.setRowKey(next);
  // ddc.add((DataDefinitionColumnDTO)table.getRowData());
  // }
  // }
  // this.setTransDtlColName(ddc.get(0).getColumnName());
  // this.setTransDtlColDesc(ddc.get(0).getDescription());
  // }

  public void selectedNameRowChanged(ActionEvent e) throws AbortProcessingException {
	logger.info("------------------>>> selectedNameRowChanged START ------------------------------------------");	  
    HtmlDataTable table = (HtmlDataTable) ((HtmlAjaxSupport) e.getComponent()).getParent();
    selectedDriverNamesIndex = table.getRowIndex();
    selectedDriverName = ((DriverNamesDTO) table.getRowData());

    // Populate the references now that a driver name has been selected
    if (selectedDriverName != null) {
      populateReferenceItems(selectedDriverName);
      logger.debug("243. Selected driver: " + selectedDriverName.getDescription());
      logger.debug("244.  >>>> Selected driverId: " + selectedDriverName.getDriverNamesPK().getDriverId()); 
	  logger.info("------------------>>> this.driverId = " + this.driverId);		
	  logger.info("------------------>>> this.globalDriverId = " + this.globalDriverId); 
	  logger.info("------------------>>> this.globalBusinessUnitFlag = " + this.globalBusinessUnitFlag);			  
	  if (this.globalBusinessUnitFlag) {   	  
	      if (selectedDriverName.getDriverNamesPK().getDriverId() == this.globalDriverId) {
	    	  logger.info("------------------>>> this.setSelectedDriverFlag(true)");
	          this.setSelectedDriverFlag(false);
	      } else {
	    	  logger.info("------------------>>> this.setSelectedDriverFlag(false)");    	  
	          this.setSelectedDriverFlag(true);
	      }
	  } else {
    	  logger.info("------------------>>> this.setSelectedDriverFlag(false)");		  
          this.setSelectedDriverFlag(false);
	  }  
	  logger.info("------------------>>> this.selectedDriverFlag = " + this.selectedDriverFlag); 	  
    } else {
      logger.debug("No Selected driver");
    }
	logger.info("------------------>>> selectedNameRowChanged END ------------------------------------------");	    
  }

  // public void selectedNameRowChanged(ActionEvent e) throws
  // AbortProcessingException {
  // HtmlScrollableDataTable table = (HtmlScrollableDataTable)
  // ((HtmlAjaxSupport) e.getComponent()).getParent();
  // selectedDriverNameIndex = table.getRowIndex();
  // Selection selection = table.getSelection();
  // this.selectedDriverName = null;

  // if (selection != null && selection.size() > 0){
  // List<DriverNamesDTO> selectedDriverNames = new ArrayList<DriverNamesDTO>();
  // for (Iterator<Object> iter = selection.getKeys(); iter.hasNext();) {
  // Object next = iter.next();
  // table.setRowKey(next);
  // selectedDriverNames.add((DriverNamesDTO)table.getRowData());
  // }
  // this.selectedDriverName = selectedDriverNames.get(0);
  // }
  // // Populate the references now that a driver name has been selected
  // if (selectedDriverName != null) {
  // populateReferenceItems(selectedDriverName);
  // logger.debug("Selected driver: " + selectedDriverName.getDescription());
  // } else {
  // logger.debug("No Selected driver");
  // }
  // }

  private void populateReferenceItems(DriverNamesDTO selectedDriverName) {
    List<DriverReference> references = driverService.findByTransactionDetailColumnName(
        selectedDriverName.getTransDtlColName(), 100);
    driverReferences = new ArrayList<DriverReferenceDTO>();
    for (DriverReference dr : references) {
      driverReferences.add(dr.getDriverReferenceDTO());
    }
  }

  public void getByColumnName(ActionEvent e) {
    String columnName = "";
    String description = "";
    String dataType = "";
    if (columnSearch != null || columnSearch.getValue() != "") {
      columnName = columnSearch.getValue().toString().toUpperCase().trim();
    }
    if (descriptionSearch != null || descriptionSearch.getValue() != "") {
      description = descriptionSearch.getValue().toString().trim();
    }
    if (dataTypeSearch != null || dataTypeSearch.getValue() != "") {
      dataType = dataTypeSearch.getValue().toString().toUpperCase().trim();
    }
    if (!dataDefinitionColList.isEmpty()) {
      dataDefinitionColList.clear();
    }

    dataDefinitionColList = dataDefinitionService.getAllDataDefinitionByColumnNameAndDescDriver(
        columnName, description, dataType,selectedCategory);
    showOkButton = false;
  }

  public void getAllByTableName(ActionEvent e) {
    String colName = transDtlColName.getSubmittedValue().toString().trim();
    String desc = "";
    String dataSearch = "";
    if (colName.length() > 0) {
      logger.info("getAllByTableName trans col value " + colName);
      setInputValue(columnSearch, colName);//Fixed for issue 0004622
      dataDefinitionColList = dataDefinitionService.getAllDataDefinitionByColumnNameAndDescDriver(
          colName, desc, dataSearch,selectedCategory);
    } else {
      dataDefinitionColList = dataDefinitionService.getAllDataDefinitionByColumnNameAndDescDriver(
    	  colName, desc, dataSearch,selectedCategory);
          setInputValue(columnSearch, "");
      }
    setInputValue(descriptionSearch, "");
    setInputValue(dataTypeSearch, "");
    showOkButton = false;
  }
  
  private void setInputValue(UIInput input, String value) {
	input.setLocalValueSet(false);
	input.setSubmittedValue(null);
	input.setValue(value);
  }

  public void selectedEntityLevelRowChangedToAdd(ActionEvent e) throws AbortProcessingException {
    logger.info("enter selectedEntityLevelRowChangedToAdd");
    HtmlDataTable table = (HtmlDataTable) ((HtmlAjaxSupport) e.getComponent()).getParent();
    selectedDriver = (DataDefinitionColumn) table.getRowData();
    if (selectedDriver != null) {
      showOkButton = true;
    }

    logger.info(" entity level selected For Add= " + this.selectedDriver
        .getDataDefinitionColumnPK().getColumnName());
    logger.info("exit selectedEntityLevelRowChangedToAdd");

  }

  public String actionOK() {
    switch (currentAction) {
    case ADD:
      return addOkMatrixAction();
    default:
      return updateOkMatrixAction();
    }
  }

  public String addMatrixAction() {
    currentAction = EditAction.ADD;
    resetInputs();
    changeTransColumn = true;
    logger.debug("seleceted category  " + selectedCategoryName);
    if (getIsCategorySelected()) {
    	
      //4866
	  if (selectedCategory.equals("T")) {
		if(driverService.getDriverNamesId(selectedCategory)>=30){
			FacesMessage message = new FacesMessage("Tax Driver may only have 30 drivers.");
		    message.setSeverity(FacesMessage.SEVERITY_ERROR);
		    FacesContext.getCurrentInstance().addMessage(null, message);
		    return "";
		}
	  } 
	  if (selectedCategory.equals("GSB")) {
			if(driverService.getDriverNamesId(selectedCategory)>=30){
				FacesMessage message = new FacesMessage("Tax Driver may only have 30 drivers.");
			    message.setSeverity(FacesMessage.SEVERITY_ERROR);
			    FacesContext.getCurrentInstance().addMessage(null, message);
			    return "";
			}
		  } 
	  else if (selectedCategory.equals("L")) {
		if(driverService.getDriverNamesId(selectedCategory)>=10){
			FacesMessage message = new FacesMessage("Location Driver may only have 10 drivers.");
		    message.setSeverity(FacesMessage.SEVERITY_ERROR);
		    FacesContext.getCurrentInstance().addMessage(null, message);
		    return "";
		}
	  } 
	  else if (selectedCategory.equals("E")) {
		if(driverService.getDriverNamesId(selectedCategory)>=10){
			FacesMessage message = new FacesMessage("Extract Driver may only have 10 drivers.");
		    message.setSeverity(FacesMessage.SEVERITY_ERROR);
		    FacesContext.getCurrentInstance().addMessage(null, message);
		    return "";
		}
	  }

      this.driverId = driverService.getDriverNamesId(selectedCategory) + 1L;
      if (driverId < 10L) {
        this.matrixColumnName = "DRIVER_0" + driverId;
      } else {
        this.matrixColumnName = "DRIVER_" + driverId;
      }
      // dataDefColumn.clear();
      // List<DataDefinitionColumn> dataDefinition =
      // dataDefinitionService.getAllDataDefinitionColumnByTable(this.tableName);
      // for(DataDefinitionColumn ddc : dataDefinition){
      // dataDefColumn.add(ddc.getDataDefinitionColumnDTO());
      // }
      return "matrix_add";
    } else {
      FacesMessage message = new FacesMessage("Please select a driver type before Adding");
      message.setSeverity(FacesMessage.SEVERITY_ERROR);
      FacesContext.getCurrentInstance().addMessage(null, message);

      return "";
    }
  }

  // public String searchTransactionDetails(){
  // dataDefColumn.clear();
  // List<DataDefinitionColumn> dataDefinition =
  // dataDefinitionService.getAllDataDefinitionColumnByColumnName(transDtlColName);
  // for(DataDefinitionColumn ddc : dataDefinition){
  // dataDefColumn.add(ddc.getDataDefinitionColumnDTO());
  // }
  // return "matrix_add";
  // }
  
  public void resetForUpdate(){
	  if("1".equalsIgnoreCase(selectedDriverName.getActiveFlag())){
		  activeFlag=true;
	  }else {
		  activeFlag=false;
	}
	  if("1".equalsIgnoreCase(selectedDriverName.getBusinessUnitFlag())){
		  businessUnitFlag=true;
	  }else {
		  businessUnitFlag=false;
	}
	  if("1".equalsIgnoreCase(selectedDriverName.getMandatoryFlag())){
		  mandatoryFlag=true;
	  }else {
		  mandatoryFlag=false;
	}
	  if("1".equalsIgnoreCase(selectedDriverName.getNullDriverFlag())){
		  nullDriverFlag=true;
	  }else {
		  nullDriverFlag=false;
	}
	  if("1".equalsIgnoreCase(selectedDriverName.getRangeFlag())){
		  rangeFlag=true;
	  }else {
		  rangeFlag=false;
	}
	  if("1".equalsIgnoreCase(selectedDriverName.getToNumberFlag())){
		  toNumberFlag=true;
	  }else {
		  toNumberFlag=false;
	}
	  if("1".equalsIgnoreCase(selectedDriverName.getWildcardFlag())){
		  wildcardFlag=true;
	  }else {
		  wildcardFlag=false;
	}
	 
  }

  public String updateMatrixAction() {
	resetForUpdate();
    isDelete = false;
    String s = setupMatrixAction(EditAction.UPDATE, "matrix_update");
    try {
        changeTransColumn = driverService.allowColChange(selectedCategory, matrixColumnName);
    } catch (Exception e) {
    	logger.error("Unable to determine if column can be changed", e);
    }
    return s;
  }

  public String deleteMatrixAction() {
	resetForUpdate();
    isDelete = true;
    changeTransColumn = false;
    return setupMatrixAction(EditAction.DELETE, "matrix_delete");
  }

  public String setupMatrixAction(EditAction action, String view) {
    currentAction = action;
    transDtlColName.setValue(selectedDriverName.getTransDtlColName());
    tableColName.setValue(selectedDriverName.getTableName());
    try{
    if( selectedDriverName.getDescription()!=null)
    transDtlColDesc = selectedDriverName.getDescription();
    }catch(Exception e){}
    driverId = selectedDriverName.getDriverNamesPK().getDriverId();
    matrixColumnName = selectedDriverName.getMatrixColName();
    userId = selectedDriverName.getUpdateUserId();
    userTimestamp = selectedDriverName.getUpdateTimestamp();
    if (selectedDriverName != null) {
      return view;
    } else {
      FacesMessage message = new FacesMessage("Please select a driver to Update");
      message.setSeverity(FacesMessage.SEVERITY_ERROR);
      FacesContext.getCurrentInstance().addMessage(null, message);

      return "";
    }
  }

  // public String deleteMatrixAction(){
  // currentAction = EditAction.DELETE;
  // transDtlColName.setValue(selectedDriverName.getTransDtlColName());
  // transDtlColDesc = selectedDriverName.getDescription();
  // driverId = selectedDriverName.getDriverNamesPK().getDriverId();
  // matrixColumnName = selectedDriverName.getMatrixColName();
  // userId = selectedDriverName.getUpdateUserId();
  // userTimestamp = selectedDriverName.getUpdateTimestamp();
  // if(selectedDriverName != null){
  // if(!selectedDriverName.getDriverNamesPK().getDriverId().equals(driverService.getDriverNamesId(selectedCategory))){
  // isDelete = false;
  // FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Only
  // the highest numbered Driver can be Deleted"));
  // return "";
  // }
  // this.setTransDtlColName(selectedDriverName.getTransDtlColName());
  // this.setTransDtlColDesc(selectedDriverName.getDescription());
  // return "matrix_delete";
  // }else{
  // FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Please
  // select a driver to Delete"));
  // return "";
  // }
  // }

  public boolean getDeleteAction() {
    return currentAction.equals(EditAction.DELETE);
  }

  public String addOkMatrixAction() {
    String colName = transDtlColName.getValue().toString().trim();
    String tableName=tableColName.getValue().toString().trim();
      if (colName.length() > 0) {
       logger.debug("Inside Add                " + colName + ", " + selectedCategory);
       DataDefinitionColumn dataDefinitionColumn = dataDefinitionService.getDataDefinitionColumnByTable(tableName, colName);
       if(dataDefinitionColumn!=null){
    	  //Check data type for %CHAR% only
    	  String datatype =  dataDefinitionColumn.getDataType();
    	  if(datatype.indexOf("CHAR")<0){
    		  	FacesMessage message = new FacesMessage("Trans. Dtl. Column Name: invalid data type for driver - " + datatype);
  	        	message.setSeverity(FacesMessage.SEVERITY_ERROR);
  	        	FacesContext.getCurrentInstance().addMessage(null, message);
  	        	return ""; 
    	  }
    	     	   
	      if (driverService.findDriverForTransactionDetailColumnName(colName, selectedCategory, this.matrixColumnName) == null) {
	        DriverNamesDTO dtoAdd = new DriverNamesDTO();
	        DriverNamesPK driverNamesPK = new DriverNamesPK(selectedCategory, driverId);
	        dtoAdd.setDriverNamesPK(driverNamesPK);
	        dtoAdd.setTransDtlColName(colName);
	        dtoAdd.setTableName(tableName);
	        dtoAdd.setMatrixColName(this.matrixColumnName);
	        dtoAdd.setActiveFlagBoolean(activeFlag);
	        dtoAdd.setBusinessUnitFlagBoolean(businessUnitFlag);
	        dtoAdd.setMandatoryFlagBoolean(mandatoryFlag);
	        dtoAdd.setNullDriverFlagBoolean(nullDriverFlag);
	        dtoAdd.setRangeFlagBoolean(rangeFlag);
	        dtoAdd.setToNumberFlagBoolean(toNumberFlag);
	        dtoAdd.setWildcardFlagBoolean(wildcardFlag);
	        if (businessUnitFlag != null) {
	        	if (businessUnitFlag) {
	        		logger.info("------------------>>> setting globalBusinessUnitFlag = true");
	        		globalBusinessUnitFlag = true; 		
	        	} 
	        	else {
	        		logger.info("------------------>>> setting globalBusinessUnitFlag = false");        		
	        		globalBusinessUnitFlag = false;
	        	}
	        }
	        driverService.saveOrUpdateNames(dtoAdd);
	        
	         	  populateDriverNames(this.selectedCategory);// repopulate, since items
                  // may have been added or
                  // deleted
	            
	        selectedDriverNamesIndex = -1;
	        selectedDriverName = null;
	        cacheManager.flushDataDefinitionMap(tableName);
	        cacheManager.flushTransactionDriverPropertyNamesMap(selectedCategory);
	        
	        //4639, Refresh cache
	        cacheManager.flushMatrixColDriverNames(selectedCategory);
	        disableAllMenu();
	        
	        return "matrix_view";
	      } 
	      else {
	    	FacesMessage message = new FacesMessage("Duplicate Trans. Dtl. Column Name not allowed");
	    	message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
	  
	        return "";
	      }
	   } 
       else {
	    	FacesMessage message = new FacesMessage("Trans. Dtl. Column Name: invalid");
	        message.setSeverity(FacesMessage.SEVERITY_ERROR);
	        FacesContext.getCurrentInstance().addMessage(null, message);
			return "";
	   }
    }
    else {
    	FacesMessage message = new FacesMessage("Trans. Dtl. Column Name: is empty");
        message.setSeverity(FacesMessage.SEVERITY_ERROR);
  	  	FacesContext.getCurrentInstance().addMessage(null, message);
        return "";
    	
	}
  }

  public String updateOkMatrixAction() {
	  String tableName="";
	 
	  if(selectedCategory.equalsIgnoreCase("GSB"))
	  {
	  tableName="TB_BILLTRANS";
	  }else{
	  tableName="TB_PURCHTRANS";
	  }
	  if (isDelete) {
		  logger.debug("Inside delete               ");
		  DriverNamesDTO dtoAdd = new DriverNamesDTO();
		  dtoAdd = this.selectedDriverName;
		  driverService.deleteNames(dtoAdd);//Fixed for Issue 0004622
		  populateDriverNames(this.selectedCategory);
	           // repopulate, since items may
		  // have been added or deleted
		  selectedDriverNamesIndex = -1;
		  selectedDriverName = null;
	  } else {
		  String colName = getTransDtlColName().getValue().toString().trim();
		  
		  if (colName.length() > 0) {
			  logger.debug("Inside Update           " + colName.length());
			  logger.debug("Inside Update           " +colName);
			  logger.info("!!!!!!!!!!selectedCategory!!!!!!!!!!!!!!!!!!!!!"+selectedCategory);
			  // jmw
			  // if(driverService.findTransactionDetailColumnName(this.transDtlColName,
			  // selectedCategory) == null){
			  //kirk sez: i think driverNames will be "find drivers for transactions that use this collName in this category"
			  DriverNames driverNames = driverService.findTransactionDetailColumnName(colName, selectedCategory, this.matrixColumnName);
			  String selectedMatrixColumnName=driverService.findCurrentTrColumnName(selectedCategory,matrixColumnName);
			  DataDefinitionColumn dataDefinitionColumn = dataDefinitionService.getDataDefinitionColumnByTable(tableName, colName);
			  if(dataDefinitionColumn!=null){
				//Check data type for %CHAR% only
		    	  String datatype =  dataDefinitionColumn.getDataType();
		    	  if(datatype.indexOf("CHAR")<0){
		    		  	FacesMessage message = new FacesMessage("Trans. Dtl. Column Name: invalid data type for driver - " + datatype);
		  	        	message.setSeverity(FacesMessage.SEVERITY_ERROR);
		  	        	FacesContext.getCurrentInstance().addMessage(null, message);
		  	        	return ""; 
		    	  }

				  //  bb 4357  if (driverNames.getDriverNamesPK().getDriverId().equals(selectedDriverName.getDriverNamesPK().getDriverId()) 
				  //  bb  4357  && driverNames.getTransDtlColName().equalsIgnoreCase(colName)) {
				  if( selectedMatrixColumnName.equals(colName)|| driverNames==null  ){ 

					  logger.info("Inside IF condition id==id and colName == colName");
					  //if(selectedMatrixColumnName.equals(colName) || isMatrixUpdateDriver()){  //4356 BB  //isMatrixUpdateDriver should probably be "isOkToUpdateMatrixDriver"?
						  logger.info("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!INSIDE UPDATE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
						  DriverNamesDTO dtoAdd = this.selectedDriverName;
						  DriverNamesPK driverNamesPK = new DriverNamesPK(selectedCategory, driverId);
						  dtoAdd.setDriverNamesPK(driverNamesPK);
						  dtoAdd.setTransDtlColName(colName);
						  dtoAdd.setMatrixColName(this.matrixColumnName);
						  dtoAdd.setActiveFlagBoolean(activeFlag);
						  dtoAdd.setBusinessUnitFlagBoolean(businessUnitFlag);
						  dtoAdd.setTableName(tableName);
						  if (businessUnitFlag != null) {
							  if (businessUnitFlag) {
								  logger.info("------------------>>> setting globalBusinessUnitFlag = true");
								  globalBusinessUnitFlag = true; 		
							  } else {
								  logger.info("------------------>>> setting globalBusinessUnitFlag = false");        		
								  globalBusinessUnitFlag = false;
							  }
						  }
						  dtoAdd.setMandatoryFlagBoolean(mandatoryFlag);
						  dtoAdd.setNullDriverFlagBoolean(nullDriverFlag);
						  dtoAdd.setRangeFlagBoolean(rangeFlag);
						  dtoAdd.setToNumberFlagBoolean(toNumberFlag);
						  dtoAdd.setWildcardFlagBoolean(wildcardFlag);
						  driverService.saveOrUpdateNames(dtoAdd);
						  populateDriverNames(this.selectedCategory); // repopulate, since items
						  // may have been added or d
						  selectedDriverNamesIndex = -1;
						  selectedDriverName = null;
						  // jmw }else{
						  // jmw FacesContext.getCurrentInstance().addMessage(null, new
						  // FacesMessage("Duplicate Trans. Dtl. Column Name not allowed"));
						  // jmw return "";
						  // jmw 

					  //} else{ // else for if(selectedMatrixColumnName.equals(colName)|| isMatrixUpdateDriver()){  
						  //logger.info("Inside Else condition id==id and colName != colName");
						//  FacesMessage message = new FacesMessage("Transaction Detail Column Name used in "+selectedCategoryName);
						//  message.setSeverity(FacesMessage.SEVERITY_ERROR);
						//  FacesContext.getCurrentInstance().addMessage(null, message);
						//  return "";
					  //}
				  }else { //else for if( selectedMatrixColumnName.equals(colName)|| driverNames==null  ){
					  logger.info("Inside Else condition id==id and colName != colName");
					  //kirk sez: I *think* this message is misleading?
					  //FacesMessage message = new FacesMessage("Transaction column name already used by another driver");
					  FacesMessage message = new FacesMessage("Transaction Detail Column Name used in "+selectedCategoryName);
					  message.setSeverity(FacesMessage.SEVERITY_ERROR);
					  FacesContext.getCurrentInstance().addMessage(null, message);
					  return "";
				  }
			  }else { //else of if(dataDefinitionService.getDataDefinitionColumnByTable(tableName, colName)!=null){
				  FacesMessage message = new FacesMessage("Trans. Dtl. Column Name: invalid");
				  message.setSeverity(FacesMessage.SEVERITY_ERROR);
				  FacesContext.getCurrentInstance().addMessage(null, message);
				  return "";
			  }
		  } else { //else of if (colName.length() > 0) {
			  FacesMessage message = new FacesMessage("Trans. Dtl. Column Name: is empty");
			  message.setSeverity(FacesMessage.SEVERITY_ERROR);
			  FacesContext.getCurrentInstance().addMessage(null, message);

			  return "";
		  }
	  }
	  
	  cacheManager.flushDataDefinitionMap(tableName);
	  
	  cacheManager.flushTransactionDriverPropertyNamesMap(selectedCategory);
	
	  //4639, Refresh cache
      cacheManager.flushMatrixColDriverNames(selectedCategory);
      disableAllMenu();
      
	  return "matrix_view";
  }
  
  private void disableAllMenu(){
	  loginBean.setShowFileMenu(false);
	  loginBean.setShowTransactionsMenu(false);
	  loginBean.setShowDataUtilityMenu(false);
	  loginBean.setShowMaintenanceMenu(false);
	  loginBean.setShowshowSecurityMenu(false);
	  loginBean.setShowUtilitiesMenu(false);
	  loginBean.setShowSetupMenu(false);
	  
	  loginBean.setShowInactivedMenuMark(true);
  }
  
  public String cancelAction() {
   // this.selectedDriverName = null;
   // this.selectedDriverNamesIndex = -1;
    this.driverId = null;
    resetInputs();//Fixed for issue 0004622
    populateDriverNames(this.selectedCategory);
       // repopulate, since items may
       // have been added or deleted
    return "matrix_view";
  }

  public List<DriverNamesDTO> getDriverNames() {
    return driverNames;
  }

  public void setDriverNames(List<DriverNamesDTO> driverNames) {
    this.driverNames = driverNames;
  }

  public List<DriverReferenceDTO> getDriverReferences() {
    return driverReferences;
  }

  public void setDriverReferences(List<DriverReferenceDTO> driverReferences) {
    this.driverReferences = driverReferences;
  }

  public List<DataDefinitionColumnDTO> getTranasctionDetailColumn() {
    return dataDefColumn;
  }

  public void setTranasctionDetailColumn(List<DataDefinitionColumnDTO> dataDefColumn) {
    this.dataDefColumn = dataDefColumn;
  }

  public DriverReferenceService getDriverService() {
    return driverService;
  }

  public void setDriverService(DriverReferenceService driverService) {
    this.driverService = driverService;
  }

  public String getSelectedCategory() {
    return selectedCategory;
  }

  public void setSelectedCategory(String selectedCategory) {
    this.selectedCategory = selectedCategory;
  }

  public String getSelectedCategoryName() {
    return selectedCategoryName;
  }

  public void setSelectedCategoryName(String selectedCategoryName) {
    this.selectedCategoryName = selectedCategoryName;
  }

  public String getActionHeading() {
	  String txtActionHeading = getActionText().trim();
	  String txtCategoryName = getSelectedCategoryName().trim();
	  if ("AEIOU".indexOf(txtCategoryName.toUpperCase().substring(0,1)) >= 0) {
		  txtActionHeading = txtActionHeading + " an ";
	  } else {
		  txtActionHeading = txtActionHeading + " a ";
	  }
	  txtActionHeading = txtActionHeading + txtCategoryName;
	  return txtActionHeading;
  }

  public void setActionHeading(String actionHeading) {
	    this.actionHeading = actionHeading;
	  }

  public DriverNamesDTO getSelectedDriverName() {
    return selectedDriverName;
  }

  public Long getDriverId() {
    return driverId;
  }
  
  public Long getGlobalDriverId() {
    return globalDriverId;
  }  

  public void setDriverId(Long driverId) {
    this.driverId = driverId;
  }
  
  public void setGlobalDriverId(Long globalDriverId) {
	    this.globalDriverId = globalDriverId;
  }  

  public String getMatrixColumnName() {
    return matrixColumnName;
  }

  public void setMatrixColumnName(String matrixColumnName) {
    this.matrixColumnName = matrixColumnName;
  }

 
  public Boolean getBusinessUnitFlag() {
    return businessUnitFlag;
  }

  public Boolean getGlobalBusinessUnitFlag() {
	  return globalBusinessUnitFlag;
  }  
  
/*  public Boolean getSelectedDriverFlag() {
	  if (this.globalBusinessUnitFlag) {
	      if (this.driverId == this.globalDriverId) {
	    	  logger.info("------------------>>> globalBusinessUnitFlag = true && this.driverId == this.globalDriverId");
	          return true;  
	      } else {
	    	  logger.info("------------------>>> globalBusinessUnitFlag = true && this.driverId != this.globalDriverId");    	  
	    	  return false;
	      }
	  } else {
    	  logger.info("------------------>>> globalBusinessUnitFlag != true");		  
		  return false;
	  }
  }*/
  
  public Boolean getSelectedDriverFlag() {
	  return selectedDriverFlag;
  }
  
  public Boolean getMandatoryFlag() {
    return mandatoryFlag;
  }

  public Boolean getNullDriverFlag() {
    return nullDriverFlag;
  }

  public Boolean getWildcardFlag() {
    return wildcardFlag;
  }

  public Boolean getRangeFlag() {
    return rangeFlag;
  }

  public Boolean getToNumberFlag() {
    return toNumberFlag;
  }

  public Boolean getActiveFlag() {
    return activeFlag;
  }

  public void setTableName(String tableName) {
    this.tableName = tableName;
  }

  public void setBusinessUnitFlag(Boolean businessUnitFlag) {
    this.businessUnitFlag = businessUnitFlag;
  }
  
  public void setSelectedDriverFlag(Boolean selectedDriverFlag) {
	    this.selectedDriverFlag = selectedDriverFlag;
	  }
  
  public void setGlobalBusinessUnitFlag(Boolean globalBusinessUnitFlag) {
	this.globalBusinessUnitFlag = globalBusinessUnitFlag;
  }  

  public void setMandatoryFlag(Boolean mandatoryFlag) {
    this.mandatoryFlag = mandatoryFlag;
  }

  public void setNullDriverFlag(Boolean nullDriverFlag) {
    this.nullDriverFlag = nullDriverFlag;
  }

  public void setWildcardFlag(Boolean wildcardFlag) {
    this.wildcardFlag = wildcardFlag;
  }

public void setRangeFlag(Boolean rangeFlag) {
    this.rangeFlag = rangeFlag;
  }

  public void setToNumberFlag(Boolean toNumberFlag) {
    this.toNumberFlag = toNumberFlag;
  }

  public void setActiveFlag(Boolean activeFlag) {
    this.activeFlag = activeFlag;
  }

  public Boolean getChangeTransColumn() {
    return changeTransColumn;
  }

  public void setChangeTransColumn(Boolean changeTransColumn) {
    this.changeTransColumn = changeTransColumn;
  }

  public void setSelectedDriverName(DriverNamesDTO selectedDriverName) {
    this.selectedDriverName = selectedDriverName;
  }

  public HtmlInputText getTransDtlColName() {
    return transDtlColName;
  }

  public void setTransDtlColName(HtmlInputText transDtlColName) {
    this.transDtlColName = transDtlColName;
  }

  public String getTransDtlColDesc() {
    return transDtlColDesc;
  }

  public void setTransDtlColDesc(String transDtlColDesc) {
    this.transDtlColDesc = transDtlColDesc;
  }

  public DataDefinitionService getDataDefinitionService() {
    return dataDefinitionService;
  }

  public void setDataDefinitionService(DataDefinitionService dataDefinitionService) {
    this.dataDefinitionService = dataDefinitionService;
  }

  public String getActionText() {
    return currentAction.getActionText();
  }

  /**
   * @return the selectedDriverNamesIndex
   */
  public int getSelectedDriverNamesIndex() {
    return this.selectedDriverNamesIndex;
  }

  /**
   * @param selectedDriverNamesIndex the selectedDriverNamesIndex to set
   */
  public void setSelectedDriverNamesIndex(int selectedDriverNamesIndex) {
    this.selectedDriverNamesIndex = selectedDriverNamesIndex;
  }

  // /**
  // * @return the filterHandler
  // */
  // public SearchTrColumnNameHandler getFilterHandler() {
  // this.filterHandler.setDataDefinitionService(this.dataDefinitionService);
  // return this.filterHandler;
  // }
  // /**
  // * @param filterHandler the filterHandler to set
  // */
  // public void setFilterHandler(SearchTrColumnNameHandler filterHandler) {
  // this.filterHandler = filterHandler;
  // }
  public void cancelEntityLevelAction() {
    if (!dataDefinitionColList.isEmpty()) {
      dataDefinitionColList.clear();
    }
    if (selectedDriver != null) {
      selectedDriver = null;
    }
    transDtlColName.resetValue();
    transDtlColDesc = "";
    showOkButton = false;

  }

  public void setAddAction() {
    if (selectedDriver != null) {
      transDtlColName.setValue(selectedDriver.getColumnName());
      tableColName.setValue(selectedDriver.getTableName());
      transDtlColDesc = selectedDriver.getDescription();
      showOkButton = false;
      
      transDtlColNameChanged(null);
    }
  }
  public void resetInputs() {
	 businessUnitFlag = false;
	 mandatoryFlag = false;
	 wildcardFlag = false;
	 rangeFlag = false;
	 toNumberFlag = false;
	 nullDriverFlag=false;
	 activeFlag = true;
	 transDtlColName.setValue("");
	 tableColName.setValue("");
    transDtlColDesc = "";
    setInputValue(columnSearch, "");
    setInputValue(descriptionSearch, "");
    setInputValue(dataTypeSearch, "");
    userId = "";
    userTimestamp = null;
    showOkButton = false;
  }
  
 // public void transDtlColNameChanged(ActionEvent event) {
//	  String colName = transDtlColName.getSubmittedValue().toString().trim();  
//	  if(colName!=null && colName.equalsIgnoreCase("TRANSACTION_STATE_CODE")){
//		  nullDriverFlag = false;
//		  wildcardFlag = false;
	//  }
 //}
  
  public void selectTableCheckChange(ActionEvent e) throws AbortProcessingException {
		HtmlSelectBooleanCheckbox chk = (HtmlSelectBooleanCheckbox) ((HtmlAjaxSupport) e.getComponent()).getParent();
  }
  
  public void transDtlColNameChanged(ValueChangeEvent event) {
	 // String colName = transDtlColName.getValue()  SubmittedValue().toString().trim();  
	  
	  
	  String colName = (String) transDtlColName.getValue();
	  
	  if(colName!=null && colName.equalsIgnoreCase("TRANSACTION_STATE_CODE")){
		  nullDriverFlag = false;
		  wildcardFlag = false;
	  }
  }
  
  
  public boolean getIsStateCodeUsed() {
	  String colName = (String) transDtlColName.getValue();
	  if(colName!=null && colName.equalsIgnoreCase("TRANSACTION_STATE_CODE")){
		  return true;
	  }
	  else{
		  return false;
	  }
  }

  public HtmlInputText getColumnSearch() {
    return columnSearch;
  }

  public void setColumnSearch(HtmlInputText columnSearch) {
    this.columnSearch = columnSearch;
  }

  public HtmlInputText getDescriptionSearch() {
    return descriptionSearch;
  }

  public void setDescriptionSearch(HtmlInputText descriptionSearch) {
    this.descriptionSearch = descriptionSearch;
  }

  public HtmlInputText getDataTypeSearch() {
    return dataTypeSearch;
  }

  public void setDataTypeSearch(HtmlInputText dataTypeSearch) {
    this.dataTypeSearch = dataTypeSearch;
  }

  public List<DataDefinitionColumn> getDataDefinitionColList() {
    // dataDefinitionColList =
    // dataDefinitionService.getAllDataDefinitionColumnByTable(tableName);
    return dataDefinitionColList;
  }

  public void setDataDefinitionColList(List<DataDefinitionColumn> dataDefinitionColList) {
    this.dataDefinitionColList = dataDefinitionColList;
  }

  public boolean isShowOkButton() {
    return showOkButton;
  }

  public void setShowOkButton(boolean showOkButton) {
    this.showOkButton = showOkButton;
  }

  /**
   * @return the userId
   */
  public String getUserId() {
    return this.userId;
  }

  /**
   * @param userId the userId to set
   */
  public void setUserId(String userId) {
    this.userId = userId;
  }

  /**
   * @return the userTimestamp
   */
  public Date getUserTimestamp() {
    return this.userTimestamp;
  }

  /**
   * @param userTimestamp the userTimestamp to set
   */
  public void setUserTimestamp(Date userTimestamp) {
    this.userTimestamp = userTimestamp;
  }

  ////isMatrixUpdateDriver should probably be "isOkToUpdateMatrixDriver"?
  public boolean isMatrixUpdateDriver() { //4356 BB
		String category=Character.toString(selectedCategoryName.charAt(0));
		if(category.equals("E")||category.equals("L")||category.equals("T")){
			updateDriver=(driverService.getTransactionDetailUpdate(category,matrixColumnName ));
			//updateDriver=true; // WAS: (driverService.getTransactionDetailUpdate(category,matrixColumnName )); //WE MIGHT HAVE TO RESTORE SOMETHING LIKE THIS
			//I think this might be a problem in terms of indirection; it's looking up, e.g. "Driver_07" but is that what we really want?
		}
		return updateDriver;
	  }

	public HtmlInputText getTableColName() {
		return tableColName;
	}
	
	public void setTableColName(HtmlInputText tableColName) {
		this.tableColName = tableColName;
	}
	
	public CacheManager getCacheManager() {
		return cacheManager;
	
	}
	
	public void setCacheManager(CacheManager cacheManager) {
		this.cacheManager = cacheManager;
	}
	public User getCurrentUser() {
		return loginBean.getUser();
	}
}
