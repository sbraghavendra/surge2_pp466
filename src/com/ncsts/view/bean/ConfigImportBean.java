package com.ncsts.view.bean;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UISelectItems;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.ajax4jsf.component.UIDataAdaptor;
import org.ajax4jsf.component.html.HtmlAjaxSupport;
import org.apache.log4j.Logger;
import org.apache.myfaces.custom.fileupload.HtmlInputFileUpload;
import org.apache.myfaces.custom.fileupload.UploadedFile;
import org.richfaces.component.html.HtmlCalendar;
import org.richfaces.component.html.HtmlDataTable;
import org.springframework.beans.factory.annotation.Autowired;

import com.ncsts.common.CacheManager;
import com.ncsts.dao.BatchMaintenanceDAO;
import com.ncsts.domain.BatchMaintenance;
import com.ncsts.domain.BatchMetadata;
import com.ncsts.domain.ConfigImportBatch;
import com.ncsts.domain.DataDefinitionTable;
import com.ncsts.domain.ListCodes;
import com.ncsts.domain.Option;
import com.ncsts.domain.OptionCodePK;
import com.ncsts.domain.User;
import com.ncsts.dto.BatchMaintenanceDTO;
import com.ncsts.dto.BatchMetadataList;
import com.ncsts.dto.DataDefinitionTableDTO;
import com.ncsts.dto.DbPropertiesDTO;
import com.ncsts.jsf.model.BCPConfigStagingTextDataModel;
import com.ncsts.jsf.model.BatchMaintenanceDataModel;
import com.ncsts.management.DbPropertiesBean;
import com.ncsts.services.BatchMaintenanceService;
import com.ncsts.services.ConfigImportService;
import com.ncsts.services.DataDefinitionService;
import com.ncsts.services.OptionService;
import com.ncsts.services.UserService;
import com.ncsts.view.util.BatchStatistics;
import com.ncsts.view.util.ConfigImportBatchValidator;
import com.ncsts.view.util.FacesUtils;
import com.ncsts.view.util.ImportConfigFileUploadParser;
import com.ncsts.view.util.TogglePanelController;

public class ConfigImportBean {

	private Logger logger = Logger.getLogger(ConfigImportBean.class);
	public static final String IMPORTING = "IX";
	public static final String IMPORTED = "I";
	public static final String PROCESS = "F";
	public static final String PROCESSING = "X";
	public static final String BATCH_CODE = "CI";
	public static final String ABORTED = "ABI";

	private enum AddState {
		NEED_FILE, HAVE_FILE, PROCESSING, COMPLETE
	}

	private BatchMetadataList batchMetadataList;
	private List<BatchMaintenance> batchList;
	private HtmlInputFileUpload uploadFile;
	private UserService userService;
	private BatchMaintenanceDAO batchMaintenanceDAO;
	private BatchMaintenance newBatch;
	private HtmlCalendar batchStartTime = new HtmlCalendar();
	private int selectedBatchIndex = -1;
	private BatchMaintenance selectedBatch = null;
	private List<ConfigImportBatch> selectedBatches;
	private BatchErrorsBackingBean batchErrorsBean;
	private BatchMetadata batchMetadata;
	private BatchMaintenanceDataModel batchMaintenanceDataModel;
	private HtmlSelectOneMenu batchTypeMenu; // = new HtmlSelectOneMenu();
	private HtmlSelectOneMenu batchStatusMenu;
	private HtmlSelectOneMenu errorSevMenu;
	private String findDefaultBatchType = "";
	private String findDefaultStatusType = "";
	private String findDefaultErrorSevType = "";
	private CacheManager cacheManager;
	private BatchMaintenanceDTO batchMaintenanceDTO = new BatchMaintenanceDTO();
	private Date myCalendarEntryDate;
	private String fileName;
	private String fileType;
	private int lineNumber;
	private int lineNumberTotal;
	private long noOfBytesInFile;
	private long noOfRows;
	private long batchNumber;
	private String importStatus;
	private HtmlSelectOneMenu appendMenu;
	private String append;
	private String headerDate;
	private DbPropertiesBean dbPropertiesBean;
	private DbPropertiesDTO dbPropertiesDTO = null;
	private Thread background;
	private String backgroundError;
	private AddState state;
	private boolean displaySelect = false;
	private boolean displayOk = false;
	private String fileTypeDescription;
	private boolean displayCancel = true;
	private boolean displayClose = false;
	private boolean displayCreateTemplate = true;
	private boolean displayExport = true;
	private UploadedFile uploadedFile;
	protected DataDefinitionService dataDefinitionService;
	private List<DataDefinitionTableDTO> dataDefinitionTableList = new ArrayList<DataDefinitionTableDTO>();
	private DataDefinitionTableDTO selectedDataDefinitionTable;
	private int selectedTableIndex = -1;
	private String selectedExportTableName;
	private String exportStatus;
	private OptionService optionService;
	private HtmlInputText exportFileText;
	private BCPConfigStagingTextDataModel importConfigTextDataModel;
	private BatchMaintenance exampleBatchMaintenance;
	private BatchMaintenanceService batchMaintenanceService;
	private FilePreferenceBackingBean filePreferenceBean;
	private BatchStatistics batchStatistics;
	private String viewMainBatch;
	private TogglePanelController togglePanelController = null;
	private boolean isValidRow;
    private ConfigImportService configImportService;
    private BatchMaintenanceBackingBean batchMaintenanceBean;
    
    @Autowired
    private loginBean lb;
    
	public BatchMaintenanceBackingBean getBatchMaintenanceBean() {
		return batchMaintenanceBean;
	}

	public void setBatchMaintenanceBean(
			BatchMaintenanceBackingBean batchMaintenanceBean) {
		this.batchMaintenanceBean = batchMaintenanceBean;
	}

	public ConfigImportService getConfigImportService() {
		return configImportService;
	}

	public void setConfigImportService(ConfigImportService configImportService) {
		this.configImportService = configImportService;
	}

	public boolean isValidRow() {
		return isValidRow;
	}

	public void setValidRow(boolean isValidRow) {
		this.isValidRow = isValidRow;
	}

	public TogglePanelController getTogglePanelController() {
		if (togglePanelController == null) {
			togglePanelController = new TogglePanelController();
			togglePanelController
					.addTogglePanel("ImportConfigBatchPanel", true);
		}
		return togglePanelController;
	}

	public void setTogglePanelController(
			TogglePanelController togglePanelController) {
		this.togglePanelController = togglePanelController;
		;
	}

	public String getViewMainBatch() {
		return viewMainBatch;
	}

	public void setViewMainBatch(String viewMainBatch) {
		this.viewMainBatch = viewMainBatch;
	}

	private boolean error = false;

	public boolean isError() {
		return error;
	}

	public void setError(boolean error) {
		this.error = error;
	}

	public boolean isDisplayProgress() {
		if (background != null)
			logger.debug("Progress: " + state.toString() + ", "
					+ background.getState().toString());

		switch (state) {
		case PROCESSING:
			return true;
		case COMPLETE:
			return true;
		default:
			return false;
		}
	}
	public BatchStatistics getBatchStatistics() {
		return batchStatistics;
	}

	public void setBatchStatistics(BatchStatistics batchStatistics) {
		this.batchStatistics = batchStatistics;
	}

	public FilePreferenceBackingBean getFilePreferenceBean() {
		return filePreferenceBean;
	}

	public void setFilePreferenceBean(
			FilePreferenceBackingBean filePreferenceBean) {
		this.filePreferenceBean = filePreferenceBean;
	}

	public BatchMaintenanceService getBatchMaintenanceService() {
		return batchMaintenanceService;
	}

	public void setBatchMaintenanceService(
			BatchMaintenanceService batchMaintenanceService) {
		this.batchMaintenanceService = batchMaintenanceService;
	}

	public BatchMaintenance getExampleBatchMaintenance() {
		return exampleBatchMaintenance;
	}

	public void setExampleBatchMaintenance(
			BatchMaintenance exampleBatchMaintenance) {
		this.exampleBatchMaintenance = exampleBatchMaintenance;
	}

	public BCPConfigStagingTextDataModel getImportConfigTextDataModel() {
		return importConfigTextDataModel;
	}

	public void setImportConfigTextDataModel(
			BCPConfigStagingTextDataModel importConfigTextDataModel) {
		this.importConfigTextDataModel = importConfigTextDataModel;
	}

	public HtmlInputText getExportFileText() {
		return exportFileText;
	}

	public void setExportFileText(HtmlInputText exportFileText) {
		this.exportFileText = exportFileText;
	}

	public OptionService getOptionService() {
		return optionService;
	}

	public void setOptionService(OptionService optionService) {
		this.optionService = optionService;
	}

	public String getExportStatus() {
		return exportStatus;
	}

	public void setExportStatus(String exportStatus) {
		this.exportStatus = exportStatus;
	}

	public String getSelectedExportTableName() {
		return selectedExportTableName;
	}

	public void setSelectedExportTableName(String selectedExportTableName) {
		this.selectedExportTableName = selectedExportTableName;
	}

	public DataDefinitionTableDTO getSelectedDataDefinitionTable() {
		return selectedDataDefinitionTable;
	}

	public void setSelectedDataDefinitionTable(
			DataDefinitionTableDTO selectedDataDefinitionTable) {
		this.selectedDataDefinitionTable = selectedDataDefinitionTable;
	}

	public int getSelectedTableIndex() {
		return selectedTableIndex;
	}

	public void setSelectedTableIndex(int selectedTableIndex) {
		this.selectedTableIndex = selectedTableIndex;
	}

	public UploadedFile getUploadedFile() {
		return uploadedFile;
	}

	public void setUploadedFile(UploadedFile uploadedFile) {
		this.uploadedFile = uploadedFile;
	}

	public boolean isDisplayCreateTemplate() {
		return displayCreateTemplate;
	}

	public void setDisplayCreateTemplate(boolean displayCreateTemplate) {
		this.displayCreateTemplate = displayCreateTemplate;
	}

	public boolean isDisplayCancel() {
		return displayCancel;
	}

	public void setDisplayCancel(boolean displayCancel) {
		this.displayCancel = displayCancel;
	}

	public boolean isDisplayClose() {
		return displayClose;
	}

	public void setDisplayClose(boolean displayClose) {
		this.displayClose = displayClose;
	}

	public String getFileTypeDescription() {
		return fileTypeDescription;
	}

	public void setFileTypeDescription(String fileTypeDescription) {
		this.fileTypeDescription = fileTypeDescription;
	}

	public boolean isDisplaySelect() {
		return displaySelect;
	}

	public void setDisplaySelect(boolean displaySelect) {
		this.displaySelect = displaySelect;
	}

	public boolean isDisplayOk() {
		return displayOk;
	}

	public void setDisplayOk(boolean displayOk) {
		this.displayOk = displayOk;
	}

	public DbPropertiesBean getDbPropertiesBean() {
		return dbPropertiesBean;
	}

	public boolean isDisplayExport() {
		return displayExport;
	}

	public void setDisplayExport(boolean displayExport) {
		this.displayExport = displayExport;
	}

	public void setDbPropertiesBean(DbPropertiesBean dbPropertiesBean) {
		this.dbPropertiesBean = dbPropertiesBean;
	}

	public HtmlSelectOneMenu getAppendMenu() {

		if (appendMenu == null) {
			appendMenu = new HtmlSelectOneMenu();
			final Collection<SelectItem> appendList = new ArrayList<SelectItem>();
			appendList.add(new SelectItem("A", "Append, Ignore Duplicates"));
			appendList.add(new SelectItem("U", "Append, Overwrite Duplicates"));
			appendList.add(new SelectItem("C", "Clear before Insert"));

			final UISelectItems items = new UISelectItems();
			items.setValue(appendList);
			items.setId("appendType_item");
			appendMenu.getChildren().add(items);
			appendMenu.setValue("Append");
		}
		return appendMenu;

	}

	public void setAppendMenu(HtmlSelectOneMenu appendMenu) {
		this.appendMenu = appendMenu;
	}

	public String getAppend() {
		return append;
	}

	public void setAppend(String append) {
		this.append = append;
	}

	private ImportConfigFileUploadParser parser = new ImportConfigFileUploadParser(
			FacesUtils.getServletContext().getRealPath("/")) {
	};

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFileType() {
		return fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	public int getLineNumber() {
		return lineNumber;
	}

	public void setLineNumber(int lineNumber) {
		this.lineNumber = lineNumber;
	}

	public int getLineNumberTotal() {
		return lineNumberTotal;
	}

	public void setLineNumberTotal(int lineNumberTotal) {
		this.lineNumberTotal = lineNumberTotal;
	}

	public long getNoOfBytesInFile() {
		return noOfBytesInFile;
	}

	public void setNoOfBytesInFile(int noOfBytesInFile) {
		this.noOfBytesInFile = noOfBytesInFile;
	}

	public long getNoOfRows() {
		return noOfRows;
	}

	public void setNoOfRows(int noOfRows) {
		this.noOfRows = noOfRows;
	}

	public long getBatchNumber() {
		return batchNumber;
	}

	public void setBatchNumber(long batchNumber) {
		this.batchNumber = batchNumber;
	}

	public String getImportStatus() {
		return importStatus;
	}

	public void setImportStatus(String importStatus) {
		this.importStatus = importStatus;
	}

	public HtmlInputFileUpload getUploadFile() {
		return uploadFile;
	}

	public void setUploadFile(HtmlInputFileUpload uploadFile) {
		this.uploadFile = uploadFile;
	}

	public HtmlSelectOneMenu getErrorSevMenu() {

		if (errorSevMenu == null) {
			errorSevMenu = new HtmlSelectOneMenu();
			final Collection<SelectItem> errorSevList = new ArrayList<SelectItem>();
			errorSevList.add(new SelectItem("", "Select Error Sev Type"));
			// batchTypeList.add(new SelectItem("*ALL","*ALL"));
			// List<ListCodes> listCodeList =
			// listCodesService.getListCodesByCodeTypeCode("BATCHTYPE");
			List<ListCodes> listCodeList = cacheManager
					.getListCodesByType("ERRORSEV");
			for (ListCodes listcodes : listCodeList) {
				if (listcodes != null && listcodes.getCodeTypeCode() != null
						&& listcodes.getDescription() != null) {
					errorSevList.add(new SelectItem(listcodes.getCodeCode(),
							listcodes.getDescription()));
				}
			}
			final UISelectItems items = new UISelectItems();
			items.setValue(errorSevList);
			items.setId("errorSev_item");
			errorSevMenu.getChildren().add(items);

			errorSevMenu.setValue(findDefaultErrorSevType);
		}
		return errorSevMenu;
	}

	public void setErrorSevMenu(HtmlSelectOneMenu errorSevMenu) {
		this.errorSevMenu = errorSevMenu;
	}

	public String getFindDefaultErrorSevType() {
		return findDefaultErrorSevType;
	}

	public void setFindDefaultErrorSevType(String findDefaultErrorSevType) {
		this.findDefaultErrorSevType = findDefaultErrorSevType;
	}

	public Date getMyCalendarEntryDate() {
		return myCalendarEntryDate;
	}

	public void setMyCalendarEntryDate(Date myCalendarEntryDate) {
		this.myCalendarEntryDate = myCalendarEntryDate;
	}

	public void setBatchStatusMenu(HtmlSelectOneMenu batchStatusMenu) {
		this.batchStatusMenu = batchStatusMenu;
	}

	public String getHeaderDate() {
		return headerDate;
	}

	public void setHeaderDate(String headerDate) {
		this.headerDate = headerDate;
	}

	public String getFindDefaultBatchType() {
		return findDefaultBatchType;
	}

	public void setFindDefaultBatchType(String findDefaultBatchType) {
		this.findDefaultBatchType = findDefaultBatchType;
	}

	public String getFindDefaultStatusType() {
		return findDefaultStatusType;
	}

	public void setFindDefaultStatusType(String findDefaultStatusType) {
		this.findDefaultStatusType = findDefaultStatusType;
	}

	public CacheManager getCacheManager() {
		return cacheManager;
	}

	public void setCacheManager(CacheManager cacheManager) {
		this.cacheManager = cacheManager;
	}

	public BatchMaintenanceDTO getBatchMaintenanceDTO() {
		return batchMaintenanceDTO;
	}

	public void setBatchMaintenanceDTO(BatchMaintenanceDTO batchMaintenanceDTO) {
		this.batchMaintenanceDTO = batchMaintenanceDTO;
	}

	public void setBatchTypeMenu(HtmlSelectOneMenu batchTypeMenu) {
		this.batchTypeMenu = batchTypeMenu;
	}

	public Logger getLogger() {
		return logger;
	}

	public void setLogger(Logger logger) {
		this.logger = logger;
	}

	public List<BatchMaintenance> getBatchList() {
		return batchList;
	}

	public void setBatchList(List<BatchMaintenance> batchList) {
		this.batchList = batchList;
	}

	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	public BatchMaintenanceDAO getBatchMaintenanceDAO() {
		return batchMaintenanceDAO;
	}

	public void setBatchMaintenanceDAO(BatchMaintenanceDAO batchMaintenanceDAO) {
		this.batchMaintenanceDAO = batchMaintenanceDAO;
	}

	public BatchMaintenance getNewBatch() {
		return newBatch;
	}

	public void setSelectedBatch(BatchMaintenance selectedBatch) {
		this.selectedBatch = selectedBatch;
	}

	public BatchMaintenance getSelectedBatch() {
		return this.selectedBatch;
	}

	public HtmlCalendar getBatchStartTime() {
		batchStartTime.resetValue();
		batchStartTime.setValue(filePreferenceBean.getBatchPreferenceDTO()
				.getDefaultBatchStartTime());
		return this.batchStartTime;
	}

	public void setBatchStartTime(HtmlCalendar batchStartTime) {
		this.batchStartTime = batchStartTime;
	}

	public int getSelectedBatchIndex() {
		return selectedBatchIndex;
	}

	public void setSelectedBatchIndex(int selectedBatchIndex) {
		this.selectedBatchIndex = selectedBatchIndex;
	}

	public BatchErrorsBackingBean getBatchErrorsBean() {
		return batchErrorsBean;
	}

	public void setBatchErrorsBean(BatchErrorsBackingBean batchErrorsBean) {
		this.batchErrorsBean = batchErrorsBean;
	}

	public BatchMetadata getBatchMetadata() {
		if (this.batchMetadata == null) {
			this.batchMetadata = batchMaintenanceService
					.findBatchMetadataById(ConfigImportBatch.BATCH_CODE);
		}
		return this.batchMetadata;
	}

	public void setBatchMetadata(BatchMetadata batchMetadata) {
		this.batchMetadata = batchMetadata;
	}

	public BatchMaintenanceDataModel getBatchMaintenanceDataModel() {

		return batchMaintenanceDataModel;
	}

	public void setBatchMaintenanceDataModel(
			BatchMaintenanceDataModel batchMaintenanceDataModel) {
		this.batchMaintenanceDataModel = batchMaintenanceDataModel;
	}

	public DataDefinitionService getDataDefinitionService() {
		return dataDefinitionService;
	}

	public void setDataDefinitionService(
			DataDefinitionService dataDefinitionService) {
		this.dataDefinitionService = dataDefinitionService;
	}

	public List<ConfigImportBatch> getSelectedBatches() {
		return selectedBatches;
	}

	public String navigateAction() {
		init();
		return "view_import_config_batch";
	}

	public HtmlSelectOneMenu getBatchTypeMenu() {

		if (batchTypeMenu == null) {
			batchTypeMenu = new HtmlSelectOneMenu();
			final Collection<SelectItem> batchTypeList = new ArrayList<SelectItem>();
			batchTypeList.add(new SelectItem("",
					"Select Configuration File Type"));
			List<DataDefinitionTable> dataDefList = dataDefinitionService.getAllImportExportTypes();

			if (dataDefList!=null) {
				for (DataDefinitionTable dataDef : dataDefList) {
					if (dataDef.getConfigFileTypeCode()!=null)
						batchTypeList.add(new SelectItem(dataDef.getConfigFileTypeCode(), dataDef.getDescription() +" ("+dataDef.getConfigFileTypeCode()+")"));

				}
			}

/*
			List<ListCodes> listCodeList = cacheManager
					.getListCodesByType("CONFIGTYPE");
			for (ListCodes listcodes : listCodeList) {
				if (listcodes != null && listcodes.getCodeTypeCode() != null
						&& listcodes.getDescription() != null) {
					batchTypeList.add(new SelectItem(listcodes.getCodeCode(),
							listcodes.getDescription()));
				}
			}
*/

			final UISelectItems items = new UISelectItems();
			items.setValue(batchTypeList);
			items.setId("batchTypeId_item");
			batchTypeMenu.getChildren().add(items);

			batchTypeMenu.setValue(findDefaultBatchType);
		}
		return batchTypeMenu;
	}

	public HtmlSelectOneMenu getBatchStatusMenu() {
		batchStatusMenu = new HtmlSelectOneMenu();
		final Collection<SelectItem> batchStatuslist = new ArrayList<SelectItem>();
		batchStatuslist.add(new SelectItem("", "Select Batch Status"));
		List<ListCodes> listCodeList = cacheManager
				.getListCodesByType("BATCHSTAT");
		for (ListCodes listcodes : listCodeList) {
			if (listcodes != null && listcodes.getCodeTypeCode() != null
					&& listcodes.getDescription() != null) {
				batchStatuslist.add(new SelectItem(listcodes.getCodeCode(),
						listcodes.getDescription()));
			}
		}
		final UISelectItems items = new UISelectItems();
		items.setId("batchStatus_ITEM");
		items.setValue(batchStatuslist);
		batchStatusMenu.getChildren().add(items);

		batchStatusMenu.setValue(findDefaultStatusType);

		return batchStatusMenu;
	}

	private void init() {
		batchMaintenanceDataModel.setDefaultSortOrder("batchId",
				BatchMaintenanceDataModel.SORT_DESCENDING);
		exampleBatchMaintenance = new BatchMaintenance();
		exampleBatchMaintenance.setEntryTimestamp(null);
		exampleBatchMaintenance.setBatchStatusCode(null);
		exampleBatchMaintenance.setBatchTypeCode(ConfigImportBean.BATCH_CODE);
		batchMaintenanceBean.setBatchTypeCode(ConfigImportBean.BATCH_CODE) ; 
		batchMaintenanceBean.setCalledFromConfigImportExpMenu(true);
		batchMaintenanceBean.prepareBatchTypeSpecificsFields();
		batchMaintenanceDataModel.setPageSize(50);
		displayExport = true;
	}

	public BatchMaintenance retrieveConfigImport() {
		exampleBatchMaintenance = new BatchMaintenance();
		exampleBatchMaintenance.setBatchTypeCode(ConfigImportBean.BATCH_CODE);
		selectedBatchIndex = -1;
		logger.debug("Date from myCalandarEntryDate " + myCalendarEntryDate);
		if (myCalendarEntryDate != null) {
			exampleBatchMaintenance.setEntryTimestamp(myCalendarEntryDate);
		}
		if (batchStatusMenu != null && batchStatusMenu.getValue() != null
				&& !"".equalsIgnoreCase(batchStatusMenu.getValue().toString())) {
			logger.debug("Batch status Menu "
					+ batchStatusMenu.getValue().toString());
			exampleBatchMaintenance.setBatchStatusCode(batchStatusMenu
					.getValue().toString());

		}
		if (batchTypeMenu != null && batchTypeMenu.getValue() != null
				&& !"".equalsIgnoreCase(batchTypeMenu.getValue().toString())) {
			logger.debug("Batch Type Code "
					+ batchTypeMenu.getValue().toString());
			exampleBatchMaintenance
					.setVc01(batchTypeMenu.getValue().toString());
		}
		if (errorSevMenu != null && errorSevMenu.getValue() != null
				&& !"".equalsIgnoreCase(errorSevMenu.getValue().toString())) {
			exampleBatchMaintenance.setErrorSevCode(errorSevMenu.getValue()
					.toString());
		}
		return exampleBatchMaintenance;
	}

	public void sortAction(ActionEvent e) {
		batchMaintenanceDataModel.toggleSortOrder(e.getComponent().getParent()
				.getId());
	}

	public String viewAction() {
		importConfigTextDataModel.setBatchMaintenance(selectedBatch);
		return viewUnformattedAction();
	}

	public String viewUnformattedAction() {

		return "config_import_view_unformatted";
	}

	public String viewFormattedAction() {
		importConfigTextDataModel.setBatchMaintenance(selectedBatch);
		return null;
	}

	public String importAction() {
		parser.clear();
		this.clear();
		this.importStatus = "Enter or Browse for a File and click Select to Select a File for Import";
		this.displaySelect = true;
		this.displayOk = false;
		this.displayCancel = true;
		this.displayClose = false;
		return "import_config_file";
	}

	public String validateImportedFile() {

		BufferedReader br = null;
		InputStream inputStream = null;
		parser.getErrors().clear();
		try {

			if (this.uploadFile.getUploadedFile() != null) {
				boolean foundError = false;
				this.uploadedFile = uploadFile.getUploadedFile();
				inputStream = this.uploadFile.getUploadedFile()
						.getInputStream();
				br = new BufferedReader(new InputStreamReader(inputStream));

				if (validateHeader(br)) {
					parser.upload(this.uploadFile, this.uploadFile
							.getUploadedFile().getInputStream(), optionService);
				} else {
					this.importStatus = parser.getErrors().get(0);
					return "import_config_file";
				}
				
				if(parser.getErrors()!=null && parser.getErrors().size()>0) {

					foundError = true;
					this.error = true;
					if (parser.getErrors().get(0)!=null && !parser.getErrors().get(0).equals("")) {
						this.importStatus = parser.getErrors().get(0);
					} else {
						this.importStatus = "Unrecognized File Type.<br> Choose a different File or Click Cancel to return to Main screen.";
					}
					return "import_config_file";
				}
				if (foundError == false) {
					state = AddState.HAVE_FILE;
				}

			} else {
				this.importStatus = "Enter or Browse for a File and click Select to Select a File for Import";
				this.error = true;
				return "import_config_file";
			}
			this.noOfBytesInFile = parser.getTotalBytes();
			this.noOfRows = parser.getLineNumberTotal();
			this.fileName = uploadFile.getUploadedFile().getName();
		} catch (Exception e) {
			parser.getErrors().add(e.getMessage());
		} finally {
			try {
				if (inputStream != null)
					inputStream.close();
				if (br != null)
					br.close();
			} catch (IOException e) {
				e.printStackTrace();
				logger.debug("Exception while closing Stream");
				parser.getErrors().add(e.getMessage());
			}
		}
		this.importStatus = "Click Ok to import the selected File";
		this.error = false;
		this.displayOk = true;
		this.displaySelect = false;
		return "import_config_file";
	}

	public String importOkSubmit() {

		String fileName = this.uploadedFile.getName();
		try {
			newBatch = createBatch(fileName);
			if (newBatch != null) {
				processUpdateInBackground(newBatch);
				newBatch = null;
			}
		} catch (Exception e) {
			e.printStackTrace();
			this.importStatus = "Error while inserting rows";
			return "import_config_file";

		}
		this.importStatus = "Importing";
		this.displayOk = false;
		this.displayClose = true;
		this.displayCancel = false;
		this.displayClose = true;
		return "import_config_file";

	}

	public boolean validateHeader(BufferedReader reader) {
			String fileType = null;
			String append = null;
			String headerDate = null;
			String configBatchRegexp = "^.*configbatch.*$"; 
	        String typePatternRegexp = "^.*type\\W*(\\w+\\W*(?!date|append|configbatch|autoprocess)\\w+)\\W*.*$";
	        String datePatternRegexp = "^.*date\\W*(\\d+/\\d+/\\d+)\\W*.*$";
	        String appendPatternRegexp = "^.*append\\W*(\\W*(?!date|type|configbatch|autoprocess)\\w+)\\W*.*$";
	        String autoProcessPatternRegexp = "^.*autoprocess\\W*(t|y|1)\\W*.*$";
			Pattern configBatch = Pattern.compile(configBatchRegexp, Pattern.CASE_INSENSITIVE);
	        Pattern typePattern = Pattern.compile(typePatternRegexp, Pattern.CASE_INSENSITIVE);
	        Pattern datePattern = Pattern.compile(datePatternRegexp, Pattern.CASE_INSENSITIVE);
	        Pattern appendPattern = Pattern.compile(appendPatternRegexp, Pattern.CASE_INSENSITIVE);
	        Pattern autoProcessPattern = Pattern.compile(autoProcessPatternRegexp, Pattern.CASE_INSENSITIVE);
	        boolean valid = false;
			try {
				String firstLine = "";
				while (firstLine.equalsIgnoreCase(""))
					firstLine = reader.readLine();
				
				
				Matcher m = configBatch.matcher(firstLine);
				if (!m.find()) {
					parser.getErrors().add("Invalid Config File. <br> Choose a different File or click Cancel to return to the Main screen. Templates are available for download from the Main screen.");
					this.error = true;
					return false;
				}
				
				
				
				m = typePattern.matcher(firstLine);
				if (!m.find()) {
					parser.getErrors().add("Missing  File Type. <br> Choose a different File or click Cancel to return to the Main screen. Templates are available for download from the Main screen.");
					this.error = true;
					return false;
				} else {
					fileType = m.group(1);
					fileType = fileType.trim().toUpperCase();
				}
				
				
				
				logger.info("fileType:"+fileType);
				
				m = datePattern.matcher(firstLine);
				if (m.find())
					headerDate = m.group(1);
					
				logger.info("headerDate:"+headerDate);

				m = appendPattern.matcher(firstLine);
				if (m.find()) {
					append = m.group(1);
				}  else {
					append = "A";
				}
				logger.info("append:"+append);

				append=append.toUpperCase();
				if (append != null && !append.equals("") && getAppendList().contains(append))
					this.appendMenu.setSubmittedValue(append);
				else
					this.appendMenu.setSubmittedValue("A");
				this.append = append;
				this.fileType = fileType;
				this.headerDate = headerDate;
			
			parser.setFileType(fileType);
/*			List<ListCodes> listCodes=cacheManager.getListCodesByType("CONFIGTYPE");
			for(ListCodes listCode:listCodes){
				if(listCode.getCodeCode().trim().equalsIgnoreCase(fileType)){
					valid = true;
					this.fileTypeDescription =listCode.getDescription();
					this.error = false;
					break;
				
				}
			}*/
				DataDefinitionTableDTO definitionTableDTO = dataDefinitionService.getDataDefinitionByConfigType(fileType);
				if (definitionTableDTO != null && "1".equalsIgnoreCase(definitionTableDTO.getImportFlag())) {
					this.fileTypeDescription = definitionTableDTO.getDescription();
					valid = true;
					this.error = false;
				}


			if(!valid){
				parser.getErrors()
				.add("Unrecognized File Type.<br> Choose a different File or click Cancel to return to the Main screen. Templates are available for download from the Main screen");
				this.error = true;
				return false;
			}
		} catch (Exception e) {
			logger.info("############ getting error to validate the Config file type"
					+ e.toString());
			parser.getErrors().add(e.getMessage());
			this.error = true;
		}
		return valid;
	}

	public String getImportedQuery(String fileType) {
		DataDefinitionTableDTO dataDefDto=dataDefinitionService.getDataDefinitionByConfigType(fileType);
		return dataDefDto.getImportInsertSql();
	}

	private BatchMaintenance createBatch(String fileName) {
		newBatch = new BatchMaintenance();
		newBatch.setUpdateUserId(lb.getUsername().toUpperCase());
		newBatch.setEntryTimestamp(new Date());
		newBatch.setVc01(this.getFileType());
		newBatch.setVc02(fileName);
		if(this.appendMenu.getSubmittedValue()!=null)
		  newBatch.setVc03(this.appendMenu.getSubmittedValue().toString());
		newBatch.setBatchTypeCode(BATCH_CODE);
		newBatch.setBatchStatusCode(IMPORTING);
		newBatch.setHeldFlag("0");
		newBatch.setUpdateTimestamp(new Date());
		newBatch.setTotalBytes(parser.getTotalBytes());
		if (headerDate != null)
			newBatch.setTs01(new Date(headerDate));
		newBatch.setTotalRows(this.getNoOfRows());
		newBatch = configImportService.addBatchUsingJDBC(newBatch);
		if (newBatch == null || newBatch.getbatchId() == null) {
			this.importStatus = "Error While Creating Batch";
			this.parser.getErrors().add("Error While Creating Batch");
		}
		this.batchNumber=newBatch.getbatchId();
		return newBatch;

	}
	private void processUpdateInBackground(final BatchMaintenance batch) {
		String selectedDb = ChangeContextHolder.getDataBaseType();
		dbPropertiesDTO = dbPropertiesBean.getDbPropertiesDTO(selectedDb);

		state = AddState.PROCESSING;
		backgroundError = null;
		background = new Thread(new Runnable() {
			public void run() {
				logger.info("############ processUpdateInBackground for ConfigImport using URL: "
								+ dbPropertiesDTO.getUrl());

				try {
					Thread.sleep(2000);
				} catch (Exception ex) {
				}
				try {
					  importConfigFileUsingJDBC(batch, parser);

/*					if (parser.getErrorCountTotal() > 0) {
						batch.setHeldFlag("1");
						batch.setErrorSevCode(BatchMaintenance.ErrorState.Error);
						backgroundError = "Error count: "
								+ parser.getErrorCountTotal();
					}*/
					 
					  

					  
					if (parser.getCriticalErrorCount() > 0) {
						batch.setHeldFlag("1");
						batch.setErrorSevCode(BatchMaintenance.ErrorState.Error);
						backgroundError = "Error count: "
								+ parser.getErrorCountTotal();
					} else if (parser.getErrorCountTotal() > 0) {
						batch.setErrorSevCode(BatchMaintenance.ErrorState.Warning);
					}
					  
					
					if (parser.isAbort()) {
						batch.setBatchStatusCode(ABORTED);
					} else {
						batch.setBatchStatusCode(IMPORTED);
					
						batch.setBatchStatusDesc("Imported");
						batch.setTotalRows(new Long(parser.getLinesWritten()));
						batch.setStatusUpdateTimestamp(new Date());
						batch.setStatusUpdateUserId(batch.getUpdateUserId());
	
						configImportService.updateBatchUsingJDBC(batch);
					}

				} catch (Throwable e) {
					e.printStackTrace();
					logger.debug(e);
					backgroundError = ("Unable to save batch: " + e
							.getMessage());
				}
				// force completion
				state = AddState.COMPLETE;
				logger.debug("Background complete");
				parser.clear();
			}
		});
		background.start();
		logger.debug("Background started");
	}

	protected BatchMaintenance importConfigFileUsingJDBC(
			BatchMaintenance batchMaintenance,
			ImportConfigFileUploadParser parser)
			throws Exception {
		InputStream inputStream = null;
		try {
			if (this.uploadedFile != null) {
				inputStream = this.uploadedFile.getInputStream();
				batchMaintenance=configImportService.importConfigFile(batchMaintenance, parser, inputStream);
			}else{
				return null;
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		this.importStatus = "Import Complete";
		return batchMaintenance;
	}
	public void clear() {
		this.fileName = "";
		this.noOfBytesInFile = 0;
		this.noOfRows = 0;
		this.fileType = "";
		this.fileTypeDescription = "";
		this.batchNumber = 0;
		this.error = false;
		this.uploadedFile = null;
		this.uploadFile = null;
		this.selectedExportTableName = "";
	}

	public void selectedRowChanged(ActionEvent e)
			throws AbortProcessingException {
		UIComponent uiComponent = e.getComponent().getParent();
		if (!(uiComponent instanceof UIDataAdaptor)) {
			logger.info("Invalid class to event listener: "
					+ uiComponent.getClass().getName());
		}
		UIDataAdaptor table = (UIDataAdaptor) uiComponent;
		if (table != null) {
			this.selectedBatch = (BatchMaintenance) table.getRowData();
			this.selectedBatchIndex = table.getRowIndex();

			BatchMetadata batchMetadata = batchMaintenanceService.findBatchMetadataById(selectedBatch.getBatchTypeCode());
			batchMetadataList = new BatchMetadataList(batchMetadata, selectedBatch);
		}
	}

	public String viewExportAction() {
		fileName = "";
		displayExport = true;
		displayCreateTemplate = true;
		displayCancel = true;
		displayClose = false;
		selectedTableIndex = -1;
		exportStatus = "Click Create Template or Export to proceed";
		return "view_export_tables";
	}

	public void selectedTableRowChanged(ActionEvent e)
			throws AbortProcessingException {
		HtmlDataTable table = (HtmlDataTable) ((HtmlAjaxSupport) e
				.getComponent()).getParent();
		this.selectedDataDefinitionTable = (DataDefinitionTableDTO) table
				.getRowData();
		this.selectedTableIndex = table.getRowIndex();

		if (selectedDataDefinitionTable != null) {
			this.selectedExportTableName = selectedDataDefinitionTable
					.getTableName();
			logger.debug("Selected Table Name ::" + selectedExportTableName);
		}
	}

	
	public String exportTableToFile() {
		
			if (selectedExportTableName == null
					|| selectedExportTableName.equals("")) {
				exportStatus = "Select a Table to export";
				addError(exportStatus);
				return null;
			}
        configImportService.exportTableToFile(selectedExportTableName);
		exportStatus = "Exporting Table";
		displayCreateTemplate = true;
		displayExport = true;
		displayCancel = true;
		return null;
		
	}

	public String createTemplate() {
		if (selectedExportTableName == null
				|| selectedExportTableName.equals("")) {
			exportStatus = "Select a Table to export";
			addError(exportStatus);
			return null;
		}
		configImportService.createTemplate(selectedExportTableName);
		exportStatus = "Creating Template";
		displayCreateTemplate = true;
		displayExport = true;
		displayCancel = true;
		return null;
	}
	public String exportOkSubmit() {
		return null;
	}

	public String processAction() {

		int lastImportVersion = 0;
		Option option = optionService.findByPK(new OptionCodePK(
				"LASTIMPORTVERSION", "SYSTEM", "SYSTEM"));
		String lastImportUpdateString = (option == null || option.getValue() == null) ? ""
				: option.getValue();
		try {
			lastImportVersion = Integer.parseInt(lastImportUpdateString);
		} catch (NumberFormatException e) {
		}

		Date lastImportDate = null;
		option = optionService.findByPK(new OptionCodePK("LASTIMPORTDATE",
				"SYSTEM", "SYSTEM"));
		String lastImportDateString = (option == null || option.getValue() == null) ? ""
				: option.getValue();
		try {
			lastImportDate = new SimpleDateFormat("yyyy_MM")
					.parse(lastImportDateString);
		} catch (ParseException pe) {
		}

		ConfigImportBatchValidator validator = new ConfigImportBatchValidator(
				batchMaintenanceDataModel, lastImportVersion, lastImportDate);
		Iterator<BatchMaintenance> iter = batchMaintenanceDataModel.iterator();
		selectedBatches = new ArrayList<ConfigImportBatch>();
		while (iter.hasNext()) {
			BatchMaintenance bm = iter.next();
			if (bm.getSelected())
				selectedBatches.add(new ConfigImportBatch(bm));
		}
		if ((selectedBatches.size() < 1) && (selectedBatch != null))
			selectedBatches.add(new ConfigImportBatch(selectedBatch));
		// Iterate through batches in reverse order, adjusting last processed
		// along the way
		Collections.sort(selectedBatches, new BatchComparator());
		for (int i = selectedBatches.size(); i > 0; i--) {
			validator.validate(selectedBatches.get(i - 1));
		}

		return "import_submit_batches";
	}

	public String submitProcessAction() {

		for (ConfigImportBatch batch : selectedBatches) {
			if (batch.getError() == null && selectedBatches.size() == 1) {
				BatchMaintenance bm = (BatchMaintenance) batchMaintenanceDataModel
						.getById(batch.getBatchId());
				bm.setBatchStatusCode(ConfigImportBatch.PROCESS);
				bm.setSchedStartTimestamp((Date) batchStartTime.getValue());
				batchMaintenanceService.update(bm);
				batchMaintenanceDataModel.refreshData(false);
			}

			if (selectedBatches.size() > 1) {
				if (batch.getError() != null) {
					selectedBatches.remove(batch.getBatchId());
					logger.info("!!!!!Batch id Ommited !!!!!!! "
							+ batch.getBatchId());
					continue;
				}
				BatchMaintenance bm = (BatchMaintenance) batchMaintenanceDataModel
						.getById(batch.getBatchId());
				logger.info("Batch id Updated " + batch.getBatchId());
				bm.setBatchStatusCode(ConfigImportBatch.PROCESS);
				bm.setSchedStartTimestamp((Date) batchStartTime.getValue());
				batchMaintenanceService.update(bm);
			}
		}
		batchMaintenanceDataModel.refreshData(false);
		selectedBatchIndex = -1;
		return "view_import_config_batch";
	}

	public class BatchComparator implements Comparator<ConfigImportBatch> {

		@Override
		public int compare(ConfigImportBatch o1, ConfigImportBatch o2) {
			if (o1.getReleaseDate() != null
					&& o2.getReleaseDate() != null
					&& (o1.getReleaseDate().getTime() > o2.getReleaseDate()
							.getTime()))
				return -1;
			else if (o1.getReleaseDate() != null
					&& o2.getReleaseDate() != null
					&& (o1.getReleaseDate().getTime() < o2.getReleaseDate()
							.getTime()))
				return 1;
			else {
				if (o1.getReleaseVersion() > o2.getReleaseVersion())
					return -1;
				else if (o1.getReleaseVersion() < o2.getReleaseVersion())
					return 1;
			}
			return 0;
		}

	}

	public String statisticsAction() {
		batchStatistics = new BatchStatistics(selectedBatch,
				batchMaintenanceDataModel.getBatchMaintenanceDAO(),
				filePreferenceBean.getBatchPreferenceDTO());
		batchStatistics.calcStatistics();
		return "import_batch_statistics";
	}

	public String errorsAction() {
		batchErrorsBean.setBatchMaintenance(selectedBatch);
		batchErrorsBean.setReturnView("view_import_config_batch");
		batchErrorsBean.getBatchErrorDataModel().setBatchMaintenance(
				selectedBatch);
		return "batch_maintenance_errors";
	}

	public String selectAllAction() {
		batchMaintenanceDataModel.setSelectAllImportConfigs();
		return null;
	}

	public String selectAction() {
		return null;
	}

	public String cancelAction() {
		parser.clear();
		this.clear();
		init();
		return "view_import_config_batch";
	}

	public String closeAction() {
		parser.clear();
		this.clear();
		batchMaintenanceDataModel.setDefaultSortOrder("batchId",
				BatchMaintenanceDataModel.SORT_DESCENDING);
		exampleBatchMaintenance.setBatchTypeCode(ConfigImportBean.BATCH_CODE);
		batchMaintenanceDataModel.setPageSize(50);
		selectedBatchIndex = -1;
		logger.debug("Date from myCalandarEntryDate " + myCalendarEntryDate);
		if (myCalendarEntryDate != null) {
			exampleBatchMaintenance.setEntryTimestamp(myCalendarEntryDate);
		}
		if (batchStatusMenu != null && batchStatusMenu.getValue() != null
				&& !"".equalsIgnoreCase(batchStatusMenu.getValue().toString())) {
			logger.debug("Batch status Menu "
					+ batchStatusMenu.getValue().toString());
			exampleBatchMaintenance.setBatchStatusCode(batchStatusMenu
					.getValue().toString());
		}
		if (batchTypeMenu != null && batchTypeMenu.getValue() != null
				&& !"".equalsIgnoreCase(batchTypeMenu.getValue().toString())) {
			logger.debug("Batch Type Code "
					+ batchTypeMenu.getValue().toString());
			exampleBatchMaintenance
					.setVc01(batchTypeMenu.getValue().toString());
		}
		if (errorSevMenu != null && errorSevMenu.getValue() != null
				&& !"".equalsIgnoreCase(errorSevMenu.getValue().toString())) {

			exampleBatchMaintenance.setErrorSevCode(errorSevMenu.getValue()
					.toString());
		}
		batchMaintenanceDataModel.setCriteria(exampleBatchMaintenance);

		return "view_import_config_batch";
	}

	public boolean getDisplayImport() {
		return true;
	}

	public boolean getDisplayProcess() {
		if ((selectedBatchIndex != -1) && (selectedBatch != null)
				&& (ConfigImportBatch.canBeProcessed(selectedBatch)))
			return true;
		return getHasSelection();
	}

	public static boolean canBeProcessed(BatchMaintenance batch) {
		return (IMPORTED.equalsIgnoreCase(batch.getBatchStatusCode()) && !batch
				.isHeld());
	}

	/**
	 * Test if anything is "selected"
	 */
	private boolean getHasSelection() {
		Iterator<BatchMaintenance> iter = batchMaintenanceDataModel.iterator();
		while (iter.hasNext()) {
			if (iter.next().getSelected()) {
				return true;
			}
		}
		return false;
	}

	public void refreshStatistics() {
		batchStatistics.calcStatistics();
	}

	public boolean getDisplayAll() {
		return true;
	}

	public boolean getDisplayError() {
		return (selectedBatchIndex != -1)
				&& (selectedBatch.getErrorSevCode() != null)
				&& (selectedBatch.getErrorSevCode().trim().length() > 0);
	}

	public boolean getDisplayView() {
		return true;
	}

	public boolean getFindMode() {
		return true;
	}

	public List<DataDefinitionTableDTO> getDataDefinitionTableList() {

		dataDefinitionTableList = dataDefinitionService
				.getExportDataDefinitionTables();
		this.setDataDefinitionTableList(dataDefinitionTableList);

		if (dataDefinitionTableList != null)
			logger.debug(" dataDefinitionTableList size "
					+ dataDefinitionTableList.size());
		else
			logger.debug(" dataDefinitionTableList is Null");

		return dataDefinitionTableList;
	}

	public void setDataDefinitionTableList(
			List<DataDefinitionTableDTO> dataDefinitionTableList) {
		this.dataDefinitionTableList = dataDefinitionTableList;
	}
	public String getProgress() {
		if (backgroundError != null) {
			addError(backgroundError);
			return "100.0";
		} else if (state == AddState.COMPLETE) {
			logger.debug("getProgress: complete");
			return "100.0";
		}
		NumberFormat nf = NumberFormat.getNumberInstance();
		nf.setMaximumFractionDigits(1);
		try {
			String s = "0.0";
			Double scale = parser.getProgress() * 99.0;
			if (scale >= 0.01) {
				s = nf.format(scale);
			}
			logger.debug("Progress at: " + s);
			return s;
		} catch (Exception e) {
			logger.error(e.getMessage());
			return "0.0";
		}
	}

	private void addError(String s) {
		addMessage(s, FacesMessage.SEVERITY_ERROR);
	}

	private void addMessage(String s, FacesMessage.Severity severity) {
		FacesContext.getCurrentInstance().addMessage(null,
				new FacesMessage(severity, s, null));
	}
	private List getAppendList(){
		List<String> list=new ArrayList<String>();
		list.add("A");
		list.add("U");
		list.add("C");
		return list;
	}


	public String updateUserFieldsAction() {
		return "import_config_batch_update_user_fields";
	}

	public String updateUserFields() {
		batchMaintenanceService.update(batchMetadataList.update());
		return "view_import_config_batch";
	}

	public String cancelUpdateUserFields() {
		return "view_import_config_batch";
	}



	public BatchMetadataList getBatchMetadataList() {
		return batchMetadataList;
	}

	public void setBatchMetadataList(BatchMetadataList batchMetadataList) {
		this.batchMetadataList = batchMetadataList;
	}

	public boolean isNavigatedFlag() {
		return true;
	}

	public void setNavigatedFlag(boolean navigatedFlag) {}
	
	
	public User getCurrentUser() {
		return lb.getUser();
   }

	public void retrieveBatchMaintenance() {
		BatchMaintenance batchMaintenance = retrieveConfigImport();
		batchMaintenance.setBatchTypeCode(ConfigImportBean.BATCH_CODE);
		batchMaintenanceBean.setCalledFromConfigImportExpMenu(true);
		this.batchMaintenanceDataModel =  batchMaintenanceBean.prepareBatchMaintenance(batchMaintenance); 
	}
	
	public String resetBatchFields(){
		batchMaintenanceBean.setResetBatchAccordion(false);
		batchMaintenanceBean.resetOtherBatchFields();
		return null;
	}


}
