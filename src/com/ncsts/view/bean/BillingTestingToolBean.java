package com.ncsts.view.bean;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import javax.faces.component.UIComponent;
import javax.faces.component.html.HtmlColumn;
import javax.faces.component.html.HtmlSelectBooleanCheckbox;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;

import com.ncsts.domain.*;

import org.ajax4jsf.component.UIDataAdaptor;
import org.ajax4jsf.component.html.HtmlAjaxSupport;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.myfaces.component.html.ext.HtmlOutputText;
import org.richfaces.component.html.HtmlDataTable;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.ncsts.common.CacheManager;
import com.ncsts.common.LogMemory;
import com.ncsts.dao.BillTransactionDAO;
import com.ncsts.dao.GenericDAO;
import com.ncsts.dao.TransactionDetailBillDAO;
import com.ncsts.dao.TransactionDetailBillDAO;
import com.ncsts.jsf.model.TransactionBillDataMemoryModel;
import com.ncsts.services.CustLocnService;
import com.ncsts.services.CustService;
import com.ncsts.services.EntityItemService;
import com.ncsts.services.JurisdictionService;
import com.ncsts.services.PurchaseTransactionService;
import com.ncsts.services.ScenarioService;
import com.ncsts.services.TaxCodeService;
import com.ncsts.view.bean.SearchJurisdictionHandler.SearchJurisdictionCallbackExt;
import com.ncsts.view.bean.SearchJurisdictionHandler.SearchJurisdictionCallbackLocation;
import com.ncsts.view.util.JsfHelper;
import com.ncsts.view.util.TogglePanelController;
import com.ncsts.ws.message.BillTransactionDocument;
import com.ncsts.ws.message.MicroApiTransactionDocument;
import com.seconddecimal.billing.exceptions.ProcessAbortedException;
import com.ncsts.exception.PurchaseTransactionProcessingException;
import com.ncsts.common.constants.PurchaseTransactionServiceConstants;
import com.ncsts.common.constants.PurchaseTransactionServiceConstants.LogSourceEnum;

@Component
@Scope("session")
public class BillingTestingToolBean implements SearchJurisdictionCallbackExt,
		SearchJurisdictionCallbackLocation {
	private static final Log logger = LogFactory
			.getLog(BillingTestingToolBean.class);

	@Autowired
	private ScenarioService scenarioService;

	@Autowired
	private CacheManager cacheManager;

	@Autowired
	private TaxCodeService taxCodeService;

	@Autowired
	private JurisdictionService jurisdictionService;

	@Autowired
	private EntityItemService entityItemService;

	@Autowired
	private CustomerLocationLookupBean customerLocationLookupBean;

	@Autowired
	private CustService custService;

	@Autowired
	private CustLocnService custLocnService;

	@Autowired
	private MatrixCommonBean matrixCommonBean;
	
	@Autowired
	private BillTransactionDAO billTransactionDAO;
	
	@Autowired
	private TransactionDetailBillDAO transactionDetailBillDAO;
	
	@Autowired
	private PurchaseTransactionService purchaseTransactionService;

//	@Autowired
//	private BillTransactionProcess BillTransactionProcess;

//	@Autowired
//	private TransactionDetailBillDAO transactionDetailBillDAO;

	@Autowired
	private TransactionDetailSaleViewBean transactionDetailSaleViewBean;
	
	@Autowired
	private FilePreferenceBackingBean filePreferenceBean;
	
	@Autowired
	private TaxJurisdictionBackingBean taxJurisdictionBean;
	
	protected static String STATUS_PROCESSED = "Processed";
	protected static String STATUS_SAVED = "Saved";
	protected static String STATUS_BLANK = "";

	private List<SelectItem> processMethods;
	private List<SelectItem> scenarioNames;
	private List<SelectItem> transactionTypes;
	private List<SelectItem> rateTypes;
	private List<SelectItem> modTypes;
	private List<SelectItem> taxCodes;
	private List<SelectItem> companies;
	private List<SelectItem> divisions;
	private List<SelectItem> certTypes;
	private List<SelectItem> invoiceDiscTypeCodes;
	private List<SelectItem> flags;
	private List<SelectItem> invoiceExemptReasonCodes;
	private List<SelectItem> entities;

	private String selectedPage;
	private BillTransaction currentInvoice;
	private ProcessMethod selectedProcessMethod;
	private String selectedScenarioName;
	private List<BillTransaction> invoiceLines;
	private BillTransaction currentInvoiceLine;
	private String invoiceLineSelectedPage;
	private String saveScenarioName;
	private String nextScenarioName;
	private BillTransaction selectedTransaction;
	private int selectedRowIndex = -1;
	private EditAction currentAction;

	private SearchJurisdictionHandler shipToJurisdictionHandler;
	private SearchJurisdictionHandler shipFromJurisdictionHandler;
	private SearchJurisdictionHandler ordrorgnJurisdictionHandler;
	private SearchJurisdictionHandler ordracptJurisdictionHandler;
	private SearchJurisdictionHandler firstuseJurisdictionHandler;
	private SearchJurisdictionHandler billtoJurisdictionHandler;
	private SearchJurisdictionHandler customerJurisdictionHandler;
	
	private SearchJurisdictionHandler shipToJurisdictionHandlerLine;
	private SearchJurisdictionHandler shipFromJurisdictionHandlerLine;
	private SearchJurisdictionHandler ordrorgnJurisdictionHandlerLine;
	private SearchJurisdictionHandler ordracptJurisdictionHandlerLine;
	private SearchJurisdictionHandler firstuseJurisdictionHandlerLine;
	private SearchJurisdictionHandler billtoJurisdictionHandlerLine;
	private SearchJurisdictionHandler customerJurisdictionHandlerLine;

	private Map<String, String> BillTransactionUserPropertyMap;

	private TransactionBillDataMemoryModel transactionSaleDataModel;
	private HtmlDataTable trDetailTable;
	private boolean displayAllFields = false;

	private boolean displayNewScenarioName = false;
	private boolean displayScenarioExists = false;
	private boolean scenarioChanged = false;
	private boolean transactionsChanged = false;
	private boolean displaySaveFirstWarning = false;

	private TogglePanelController togglePanelController = null;
	private TogglePanelController lineTogglePanelController = null;

	private boolean allSelected;
	private List<MicroApiTransactionDocument> saveToolDocs;

	private enum ProcessMethod {
		BATCH, BillScenario
	}

	public BillingTestingToolBean() {
		this.currentInvoice = new BillTransaction();
		this.currentInvoice.setGlDate(new Date());
		this.currentInvoice.setEnteredDate(new Date());
		this.invoiceLines = new ArrayList<BillTransaction>();
		this.selectedProcessMethod = ProcessMethod.BillScenario;
		this.selectedScenarioName = "*NEW";
		this.selectedPage = "basicinformationFilter";
	}

	public List<SelectItem> getProcessMethods() {
		if (this.processMethods == null) {
			this.processMethods = new ArrayList<SelectItem>(2);
			// this.processMethods
			// .add(new SelectItem(ProcessMethod.BATCH, "Batch"));
			this.processMethods.add(new SelectItem(ProcessMethod.BillScenario,
					"BillScenario"));
		}
		return processMethods;
	}

	public void setProcessMethods(List<SelectItem> processMethods) {
		this.processMethods = processMethods;
	}

	public List<SelectItem> getScenarioNames() {
		if (this.scenarioNames == null) {
			this.scenarioNames = new ArrayList<SelectItem>();
			this.scenarioNames.add(new SelectItem("*NEW", "*NEW"));
			List<String> list = scenarioService.getScenarioNames();
			Collections.sort(list, String.CASE_INSENSITIVE_ORDER);
			if (list != null && list.size() > 0) {
				for (String cn : list) {
					this.scenarioNames.add(new SelectItem(cn, cn));
				}
			}
		}
		return scenarioNames;
	}

	public void setScenarioNames(List<SelectItem> scenarioNames) {
		this.scenarioNames = scenarioNames;
	}

	public String getSelectedPage() {
		return selectedPage;
	}

	public void setSelectedPage(String selectedPage) {
		this.selectedPage = selectedPage;
	}

	public void pageSelectionChanged(ValueChangeEvent e) {
		this.selectedPage = (String) e.getNewValue();
		saveJurisdictionValues();
	}

	public void invoiceLinePageSelectionChanged(ValueChangeEvent e) {
		this.invoiceLineSelectedPage = (String) e.getNewValue();
	}

	public ScenarioService getScenarioService() {
		return scenarioService;
	}

	public void setScenarioService(ScenarioService scenarioService) {
		this.scenarioService = scenarioService;
	}

	public List<SelectItem> getTransactionTypes() {
		if (this.transactionTypes == null) {
			this.transactionTypes = new ArrayList<SelectItem>();
			this.transactionTypes.add(new SelectItem("",
					"Select a Transaction Type"));

			List<ListCodes> codes = cacheManager
					.getListCodesByType("TRANSTYPE");
			if (codes != null) {
				for (ListCodes lc : codes) {
					if (lc != null && lc.getCodeCode() != null
							&& lc.getDescription() != null) {
						this.transactionTypes.add(new SelectItem(lc
								.getCodeCode().trim(), lc.getDescription()
								.trim()));
					}
				}
			}
		}

		return transactionTypes;
	}

	public void setTransactionTypes(List<SelectItem> transactionTypes) {
		this.transactionTypes = transactionTypes;
	}

	public List<SelectItem> getRateTypes() {
		if (this.rateTypes == null) {
			this.rateTypes = new ArrayList<SelectItem>();
			this.rateTypes.add(new SelectItem("", "Select a Rate Type"));

			List<ListCodes> codes = cacheManager.getListCodesByType("RATETYPE");
			if (codes != null) {
				for (ListCodes lc : codes) {
					if (lc != null && lc.getCodeCode() != null
							&& lc.getDescription() != null) {
						this.rateTypes.add(new SelectItem(lc.getCodeCode()
								.trim(), lc.getDescription().trim()));
					}
				}
			}
		}

		return rateTypes;
	}

	public void setRateTypes(List<SelectItem> rateTypes) {
		this.rateTypes = rateTypes;
	}

	public List<SelectItem> getModTypes() {
		if (this.modTypes == null) {
			this.modTypes = new ArrayList<SelectItem>();
			this.modTypes
					.add(new SelectItem("", "Select a Method of Delivery"));

			List<ListCodes> codes = cacheManager.getListCodesByType("MOD");
			if (codes != null) {
				for (ListCodes lc : codes) {
					if ((lc != null && lc.getCodeCode() != null
							&& lc.getDescription() != null) && !lc.getCodeCode().equals("*ALL")) {
						this.modTypes.add(new SelectItem(lc.getCodeCode()
								.trim(), lc.getDescription().trim()));
					}
				}
			}
		}

		return modTypes;
	}

	public void setModTypes(List<SelectItem> modTypes) {
		this.modTypes = modTypes;
	}

	public List<SelectItem> getTaxCodes() {
		if (this.taxCodes == null) {
			this.taxCodes = new ArrayList<SelectItem>();
			this.taxCodes.add(new SelectItem("", "Select a TaxCode"));
			List<TaxCode> list = taxCodeService
					.findTaxCodeList(null, null, "1");
			if (list != null) {
				for (TaxCode tc : list) {
					this.taxCodes.add(new SelectItem(tc.getTaxcodeCode(), tc
							.getTaxcodeCode() + " - " + tc.getDescription()));
				}
			}
		}

		return taxCodes;
	}

	public void setTaxCodes(List<SelectItem> taxCodes) {
		this.taxCodes = taxCodes;
	}
	
	public void setShipToJurisdictionHandlerLine(
			SearchJurisdictionHandler shipToJurisdictionHandlerLine) {
		this.shipToJurisdictionHandlerLine = shipToJurisdictionHandlerLine;
	}

	public void setShipFromJurisdictionHandlerLine(
			SearchJurisdictionHandler shipFromJurisdictionHandlerLine) {
		this.shipFromJurisdictionHandlerLine = shipFromJurisdictionHandlerLine;
	}

	public void setOrdrorgnJurisdictionHandlerLine(
			SearchJurisdictionHandler ordrorgnJurisdictionHandlerLine) {
		this.ordrorgnJurisdictionHandlerLine = ordrorgnJurisdictionHandlerLine;
	}

	public void setOrdracptJurisdictionHandlerLine(
			SearchJurisdictionHandler ordracptJurisdictionHandlerLine) {
		this.ordracptJurisdictionHandlerLine = ordracptJurisdictionHandlerLine;
	}

	public void setFirstuseJurisdictionHandlerLine(
			SearchJurisdictionHandler firstuseJurisdictionHandlerLine) {
		this.firstuseJurisdictionHandlerLine = firstuseJurisdictionHandlerLine;
	}

	public void setCustomerJurisdictionHandlerLine(
			SearchJurisdictionHandler customerJurisdictionHandlerLine) {
		this.customerJurisdictionHandlerLine = customerJurisdictionHandlerLine;
	}

	public SearchJurisdictionHandler getShipToJurisdictionHandlerLine() {
		if (this.shipToJurisdictionHandlerLine == null) {
			this.shipToJurisdictionHandlerLine = new SearchJurisdictionHandler();
			this.shipToJurisdictionHandlerLine.setCacheManager(cacheManager);
			this.shipToJurisdictionHandlerLine.setJurisdictionService(jurisdictionService);
			this.shipToJurisdictionHandlerLine.setCallbackExt(this);
			this.shipToJurisdictionHandlerLine.setCallbackLocation(this);
			
			this.shipToJurisdictionHandlerLine.reset();
			this.shipToJurisdictionHandlerLine.setCountry(filePreferenceBean.getUserPreferenceDTO().getUserCountry());
			this.shipToJurisdictionHandlerLine.setState("");
		}

		return shipToJurisdictionHandlerLine;
		
		
	}

	public SearchJurisdictionHandler getShipFromJurisdictionHandlerLine() {
		if (this.shipFromJurisdictionHandlerLine == null) {
			this.shipFromJurisdictionHandlerLine = new SearchJurisdictionHandler();
			this.shipFromJurisdictionHandlerLine.setCacheManager(cacheManager);
			this.shipFromJurisdictionHandlerLine.setJurisdictionService(jurisdictionService);
			this.shipFromJurisdictionHandlerLine.setCallbackExt(this);
			this.shipFromJurisdictionHandlerLine.setCallbackLocation(this);
			
			this.shipFromJurisdictionHandlerLine.reset();
			this.shipFromJurisdictionHandlerLine.setCountry(filePreferenceBean.getUserPreferenceDTO().getUserCountry());
			this.shipFromJurisdictionHandlerLine.setState("");
		}
		if(this.currentAction.equals(EditAction.ADD)  || this.currentAction.equals(EditAction.UPDATE))
			shipFromJurisdictionHandlerLine.setCallbackScreen("billing_test_tool_add_line");
		else
			shipFromJurisdictionHandlerLine.setCallbackScreen("billing_test_tool");
		
		return shipFromJurisdictionHandlerLine;
	}

	public SearchJurisdictionHandler getOrdrorgnJurisdictionHandlerLine() {
		if (this.ordrorgnJurisdictionHandlerLine == null) {
			this.ordrorgnJurisdictionHandlerLine = new SearchJurisdictionHandler();
			this.ordrorgnJurisdictionHandlerLine.setCacheManager(cacheManager);
			this.ordrorgnJurisdictionHandlerLine.setJurisdictionService(jurisdictionService);
			this.ordrorgnJurisdictionHandlerLine.setCallbackExt(this);
			this.ordrorgnJurisdictionHandlerLine.setCallbackLocation(this);
			
			this.ordrorgnJurisdictionHandlerLine.reset();
			this.ordrorgnJurisdictionHandlerLine.setCountry(filePreferenceBean.getUserPreferenceDTO().getUserCountry());
			this.ordrorgnJurisdictionHandlerLine.setState("");
		}
		
		if(this.currentAction.equals(EditAction.ADD) || this.currentAction.equals(EditAction.UPDATE))
			ordrorgnJurisdictionHandlerLine.setCallbackScreen("billing_test_tool_add_line");
		else
			ordrorgnJurisdictionHandlerLine.setCallbackScreen("billing_test_tool");

		return ordrorgnJurisdictionHandlerLine;
	}

	public SearchJurisdictionHandler getOrdracptJurisdictionHandlerLine() {
		if (this.ordracptJurisdictionHandlerLine == null) {
			this.ordracptJurisdictionHandlerLine = new SearchJurisdictionHandler();
			this.ordracptJurisdictionHandlerLine.setCacheManager(cacheManager);
			this.ordracptJurisdictionHandlerLine.setJurisdictionService(jurisdictionService);
			this.ordracptJurisdictionHandlerLine.setCallbackExt(this);
			this.ordracptJurisdictionHandlerLine.setCallbackLocation(this);
			
			this.ordracptJurisdictionHandlerLine.reset();
			this.ordracptJurisdictionHandlerLine.setCountry(filePreferenceBean.getUserPreferenceDTO().getUserCountry());
			this.ordracptJurisdictionHandlerLine.setState("");
		}
		
		if(this.currentAction.equals(EditAction.ADD) || this.currentAction.equals(EditAction.UPDATE))
			ordracptJurisdictionHandlerLine.setCallbackScreen("billing_test_tool_add_line");
		else
			ordracptJurisdictionHandlerLine.setCallbackScreen("billing_test_tool");

		return ordracptJurisdictionHandlerLine;
	}

	public SearchJurisdictionHandler getFirstuseJurisdictionHandlerLine() {
		if (this.firstuseJurisdictionHandlerLine == null) {
			this.firstuseJurisdictionHandlerLine = new SearchJurisdictionHandler();
			this.firstuseJurisdictionHandlerLine.setCacheManager(cacheManager);
			this.firstuseJurisdictionHandlerLine.setJurisdictionService(jurisdictionService);
			this.firstuseJurisdictionHandlerLine.setCallbackExt(this);
			this.firstuseJurisdictionHandlerLine.setCallbackLocation(this);
			
			this.firstuseJurisdictionHandlerLine.reset();
			this.firstuseJurisdictionHandlerLine.setCountry(filePreferenceBean.getUserPreferenceDTO().getUserCountry());
			this.firstuseJurisdictionHandlerLine.setState("");
		}
		
		if(this.currentAction.equals(EditAction.ADD) || this.currentAction.equals(EditAction.UPDATE))
			firstuseJurisdictionHandlerLine.setCallbackScreen("billing_test_tool_add_line");
		else
			firstuseJurisdictionHandlerLine.setCallbackScreen("billing_test_tool");

		return firstuseJurisdictionHandlerLine;
	}

	public SearchJurisdictionHandler getBilltoJurisdictionHandlerLine() {
		if (this.billtoJurisdictionHandlerLine == null) {
			this.billtoJurisdictionHandlerLine = new SearchJurisdictionHandler();
			this.billtoJurisdictionHandlerLine.setCacheManager(cacheManager);
			this.billtoJurisdictionHandlerLine.setJurisdictionService(jurisdictionService);
			this.billtoJurisdictionHandlerLine.setCallbackExt(this);
			this.billtoJurisdictionHandlerLine.setCallbackLocation(this);
			
			this.billtoJurisdictionHandlerLine.reset();
			this.billtoJurisdictionHandlerLine.setCountry(filePreferenceBean.getUserPreferenceDTO().getUserCountry());
			this.billtoJurisdictionHandlerLine.setState("");
		}
		
		if(this.currentAction.equals(EditAction.ADD) || this.currentAction.equals(EditAction.UPDATE))
			billtoJurisdictionHandlerLine.setCallbackScreen("billing_test_tool_add_line");
		else
			billtoJurisdictionHandlerLine.setCallbackScreen("billing_test_tool");

		return billtoJurisdictionHandlerLine;
	}
	
	public SearchJurisdictionHandler getShipToJurisdictionHandler() {
		if (this.shipToJurisdictionHandler == null) {
			this.shipToJurisdictionHandler = new SearchJurisdictionHandler();
			this.shipToJurisdictionHandler.setBillingTestToolScreen(true);
			this.shipToJurisdictionHandler.setCacheManager(cacheManager);
			this.shipToJurisdictionHandler.setJurisdictionService(jurisdictionService);
			this.shipToJurisdictionHandler.setCallbackExt(this);
			this.shipToJurisdictionHandler.setCallbackLocation(this);
			this.shipToJurisdictionHandler.setTaxJurisdictionBean(taxJurisdictionBean);
			this.shipToJurisdictionHandler.reset();
			this.shipToJurisdictionHandler.setCountry(filePreferenceBean.getUserPreferenceDTO().getUserCountry());
			this.shipToJurisdictionHandler.setState("");
			this.shipToJurisdictionHandler.setCallbackScreen("billing_test_tool");
			
			if (this.currentInvoice != null) {
				shipToJurisdictionHandler.setGeocode(this.currentInvoice
						.getShiptoGeocode());
				shipToJurisdictionHandler.setCity(this.currentInvoice
						.getShiptoCity());
				shipToJurisdictionHandler.setCounty(this.currentInvoice
						.getShiptoCounty());
				shipToJurisdictionHandler.setState(this.currentInvoice
						.getShiptoStateCode());
				shipToJurisdictionHandler.setCountry(this.currentInvoice
						.getShiptoCountryCode());
				shipToJurisdictionHandler.setZip(this.currentInvoice
						.getShiptoZip());
				shipToJurisdictionHandler.setZipPlus4(this.currentInvoice
						.getShiptoZipplus4());
				shipToJurisdictionHandler.setStj1Name(this.currentInvoice
						.getShiptoStj1Name());
				shipToJurisdictionHandler.setStj2Name(this.currentInvoice
						.getShiptoStj2Name());
				shipToJurisdictionHandler.setStj3Name(this.currentInvoice
						.getShiptoStj3Name());
				shipToJurisdictionHandler.setStj4Name(this.currentInvoice
						.getShiptoStj4Name());
				shipToJurisdictionHandler.setStj5Name(this.currentInvoice
						.getShiptoStj5Name());
				
			}
		}
		
		

		return shipToJurisdictionHandler;
	}

	public SearchJurisdictionHandler getShipFromJurisdictionHandler() {
		if (this.shipFromJurisdictionHandler == null) {
			this.shipFromJurisdictionHandler = new SearchJurisdictionHandler();
			this.shipFromJurisdictionHandler.setCacheManager(cacheManager);
			
			this.shipFromJurisdictionHandler.setBillingTestToolScreen(true);
			this.shipFromJurisdictionHandler.setTaxJurisdictionBean(taxJurisdictionBean);
			this.shipFromJurisdictionHandler
					.setJurisdictionService(jurisdictionService);
			this.shipFromJurisdictionHandler.setCallbackExt(this);
			this.shipFromJurisdictionHandler.setCallbackLocation(this);
			this.shipFromJurisdictionHandler.setCallbackScreen("billing_test_tool");

			if (this.currentInvoice != null) {
				shipFromJurisdictionHandler.setGeocode(this.currentInvoice
						.getShipfromGeocode());
				shipFromJurisdictionHandler.setCity(this.currentInvoice
						.getShipfromCity());
				shipFromJurisdictionHandler.setCounty(this.currentInvoice
						.getShipfromCounty());
				shipFromJurisdictionHandler.setState(this.currentInvoice
						.getShipfromStateCode());
				shipFromJurisdictionHandler.setCountry(this.currentInvoice
						.getShipfromCountryCode());
				shipFromJurisdictionHandler.setZip(this.currentInvoice
						.getShipfromZip());
				shipFromJurisdictionHandler.setZipPlus4(this.currentInvoice
						.getShipfromZipplus4());
				shipFromJurisdictionHandler.setStj1Name(this.currentInvoice
						.getShipfromStj1Name());
				shipFromJurisdictionHandler.setStj2Name(this.currentInvoice
						.getShipfromStj2Name());
				shipFromJurisdictionHandler.setStj3Name(this.currentInvoice
						.getShipfromStj3Name());
				shipFromJurisdictionHandler.setStj4Name(this.currentInvoice
						.getShipfromStj4Name());
				shipFromJurisdictionHandler.setStj5Name(this.currentInvoice
						.getShipfromStj5Name());
			}
		}

		return shipFromJurisdictionHandler;
	}

	public SearchJurisdictionHandler getOrdrorgnJurisdictionHandler() {
		if (this.ordrorgnJurisdictionHandler == null) {
			this.ordrorgnJurisdictionHandler = new SearchJurisdictionHandler();
			this.ordrorgnJurisdictionHandler.setCacheManager(cacheManager);
			this.ordrorgnJurisdictionHandler.setTaxJurisdictionBean(taxJurisdictionBean);
			this.ordrorgnJurisdictionHandler
					.setJurisdictionService(jurisdictionService);
			this.ordrorgnJurisdictionHandler.setCallbackExt(this);
			this.ordrorgnJurisdictionHandler.setCallbackLocation(this);
			this.ordrorgnJurisdictionHandler.setCallbackScreen("billing_test_tool");
			this.ordrorgnJurisdictionHandler.setBillingTestToolScreen(true);

			if (this.currentInvoice != null) {
				ordrorgnJurisdictionHandler.setGeocode(this.currentInvoice
						.getOrdrorgnGeocode());
				ordrorgnJurisdictionHandler.setCity(this.currentInvoice
						.getOrdrorgnCity());
				ordrorgnJurisdictionHandler.setCounty(this.currentInvoice
						.getOrdrorgnCounty());
				ordrorgnJurisdictionHandler.setState(this.currentInvoice
						.getOrdrorgnStateCode());
				ordrorgnJurisdictionHandler.setCountry(this.currentInvoice
						.getOrdrorgnCountryCode());
				ordrorgnJurisdictionHandler.setZip(this.currentInvoice
						.getOrdrorgnZip());
				ordrorgnJurisdictionHandler.setZipPlus4(this.currentInvoice
						.getOrdrorgnZipplus4());
				ordrorgnJurisdictionHandler.setStj1Name(this.currentInvoice
						.getOrdrorgnStj1Name());
				ordrorgnJurisdictionHandler.setStj2Name(this.currentInvoice
						.getOrdrorgnStj2Name());
				ordrorgnJurisdictionHandler.setStj3Name(this.currentInvoice
						.getOrdrorgnStj3Name());
				ordrorgnJurisdictionHandler.setStj4Name(this.currentInvoice
						.getOrdrorgnStj4Name());
				ordrorgnJurisdictionHandler.setStj5Name(this.currentInvoice
						.getOrdrorgnStj5Name());
			}
		}

		return ordrorgnJurisdictionHandler;
	}

	public SearchJurisdictionHandler getOrdracptJurisdictionHandler() {
		if (this.ordracptJurisdictionHandler == null) {
			this.ordracptJurisdictionHandler = new SearchJurisdictionHandler();
			this.ordracptJurisdictionHandler.setCacheManager(cacheManager);
			this.ordracptJurisdictionHandler.setTaxJurisdictionBean(taxJurisdictionBean);
			this.ordracptJurisdictionHandler
					.setJurisdictionService(jurisdictionService);
			this.ordracptJurisdictionHandler.setCallbackExt(this);
			this.ordracptJurisdictionHandler.setCallbackLocation(this);
			this.ordracptJurisdictionHandler.setCallbackScreen("billing_test_tool");
			this.ordracptJurisdictionHandler.setBillingTestToolScreen(true);

			if (this.currentInvoice != null) {
				ordracptJurisdictionHandler.setGeocode(this.currentInvoice
						.getOrdracptGeocode());
				ordracptJurisdictionHandler.setCity(this.currentInvoice
						.getOrdracptCity());
				ordracptJurisdictionHandler.setCounty(this.currentInvoice
						.getOrdracptCounty());
				ordracptJurisdictionHandler.setState(this.currentInvoice
						.getOrdracptStateCode());
				ordracptJurisdictionHandler.setCountry(this.currentInvoice
						.getOrdracptCountryCode());
				ordracptJurisdictionHandler.setZip(this.currentInvoice
						.getOrdracptZip());
				ordracptJurisdictionHandler.setZipPlus4(this.currentInvoice
						.getOrdracptZipplus4());
				ordracptJurisdictionHandler.setStj1Name(this.currentInvoice
						.getOrdracptStj1Name());
				ordracptJurisdictionHandler.setStj2Name(this.currentInvoice
						.getOrdracptStj2Name());
				ordracptJurisdictionHandler.setStj3Name(this.currentInvoice
						.getOrdracptStj3Name());
				ordracptJurisdictionHandler.setStj4Name(this.currentInvoice
						.getOrdracptStj4Name());
				ordracptJurisdictionHandler.setStj5Name(this.currentInvoice
						.getOrdracptStj5Name());
			}
		}

		return ordracptJurisdictionHandler;
	}

	public SearchJurisdictionHandler getFirstuseJurisdictionHandler() {
		if (this.firstuseJurisdictionHandler == null) {
			this.firstuseJurisdictionHandler = new SearchJurisdictionHandler();
			this.firstuseJurisdictionHandler.setCacheManager(cacheManager);
			this.firstuseJurisdictionHandler.setTaxJurisdictionBean(taxJurisdictionBean);
			this.firstuseJurisdictionHandler
					.setJurisdictionService(jurisdictionService);
			this.firstuseJurisdictionHandler.setCallbackExt(this);
			this.firstuseJurisdictionHandler.setCallbackLocation(this);
			this.firstuseJurisdictionHandler.setCallbackScreen("billing_test_tool");
			this.firstuseJurisdictionHandler.setBillingTestToolScreen(true);

			if (this.currentInvoice != null) {
				firstuseJurisdictionHandler.setGeocode(this.currentInvoice
						.getFirstuseGeocode());
				firstuseJurisdictionHandler.setCity(this.currentInvoice
						.getFirstuseCity());
				firstuseJurisdictionHandler.setCounty(this.currentInvoice
						.getFirstuseCounty());
				firstuseJurisdictionHandler.setState(this.currentInvoice
						.getFirstuseStateCode());
				firstuseJurisdictionHandler.setCountry(this.currentInvoice
						.getFirstuseCountryCode());
				firstuseJurisdictionHandler.setZip(this.currentInvoice
						.getFirstuseZip());
				firstuseJurisdictionHandler.setZipPlus4(this.currentInvoice
						.getFirstuseZipplus4());
				firstuseJurisdictionHandler.setStj1Name(this.currentInvoice
						.getFirstuseStj1Name());
				firstuseJurisdictionHandler.setStj2Name(this.currentInvoice
						.getFirstuseStj2Name());
				firstuseJurisdictionHandler.setStj3Name(this.currentInvoice
						.getFirstuseStj3Name());
				firstuseJurisdictionHandler.setStj4Name(this.currentInvoice
						.getFirstuseStj4Name());
				firstuseJurisdictionHandler.setStj5Name(this.currentInvoice
						.getFirstuseStj5Name());
			}
		}

		return firstuseJurisdictionHandler;
	}

	public SearchJurisdictionHandler getBilltoJurisdictionHandler() {
		if (this.billtoJurisdictionHandler == null) {
			this.billtoJurisdictionHandler = new SearchJurisdictionHandler();
			this.billtoJurisdictionHandler.setCacheManager(cacheManager);
			this.billtoJurisdictionHandler
					.setJurisdictionService(jurisdictionService);
			this.billtoJurisdictionHandler.setCallbackExt(this);
			this.billtoJurisdictionHandler.setCallbackLocation(this);
			this.billtoJurisdictionHandler.setTaxJurisdictionBean(taxJurisdictionBean);
			this.billtoJurisdictionHandler.setCallbackScreen("billing_test_tool");
			this.billtoJurisdictionHandler.setBillingTestToolScreen(true);

			if (this.currentInvoice != null) {
				billtoJurisdictionHandler.setGeocode(this.currentInvoice
						.getBilltoGeocode());
				billtoJurisdictionHandler.setCity(this.currentInvoice
						.getBilltoCity());
				billtoJurisdictionHandler.setCounty(this.currentInvoice
						.getBilltoCounty());
				billtoJurisdictionHandler.setState(this.currentInvoice
						.getBilltoStateCode());
				billtoJurisdictionHandler.setCountry(this.currentInvoice
						.getBilltoCountryCode());
				billtoJurisdictionHandler.setZip(this.currentInvoice
						.getBilltoZip());
				billtoJurisdictionHandler.setZipPlus4(this.currentInvoice
						.getBilltoZipplus4());
				billtoJurisdictionHandler.setStj1Name(this.currentInvoice
						.getBilltoStj1Name());
				billtoJurisdictionHandler.setStj2Name(this.currentInvoice
						.getBilltoStj2Name());
				billtoJurisdictionHandler.setStj3Name(this.currentInvoice
						.getBilltoStj3Name());
				billtoJurisdictionHandler.setStj4Name(this.currentInvoice
						.getBilltoStj4Name());
				billtoJurisdictionHandler.setStj5Name(this.currentInvoice
						.getBilltoStj5Name());
			}
		}

		return billtoJurisdictionHandler;
	}
	
	///

	public SearchJurisdictionHandler getCustomerJurisdictionHandler() {
		if (this.customerJurisdictionHandler == null) {
			this.customerJurisdictionHandler = new SearchJurisdictionHandler();
			this.customerJurisdictionHandler.setCacheManager(cacheManager);
			this.customerJurisdictionHandler.setBillingTestToolScreen(true);
			this.customerJurisdictionHandler
					.setJurisdictionService(jurisdictionService);
			this.customerJurisdictionHandler.setCallbackExt(this);
			this.customerJurisdictionHandler.setCallbackLocation(this);
			this.customerJurisdictionHandler.setTaxJurisdictionBean(taxJurisdictionBean);
			this.customerJurisdictionHandler.setCallbackScreen("billing_test_tool");

			if (this.currentInvoice != null) {
				customerJurisdictionHandler.setGeocode(this.currentInvoice
						.getCustGeocode());
				customerJurisdictionHandler.setCity(this.currentInvoice
						.getCustCity());
				customerJurisdictionHandler.setCounty(this.currentInvoice
						.getCustCounty());
				customerJurisdictionHandler.setState(this.currentInvoice
						.getCustStateCode());
				customerJurisdictionHandler.setCountry(this.currentInvoice
						.getCustCountryCode());
				customerJurisdictionHandler.setZip(this.currentInvoice
						.getCustZip());
				customerJurisdictionHandler.setZipPlus4(this.currentInvoice
						.getCustZipplus4());
			}
		}

		return customerJurisdictionHandler;
	}

	public void setShipToJurisdictionHandler(
			SearchJurisdictionHandler shipToJurisdictionHandler) {
		this.shipToJurisdictionHandler = shipToJurisdictionHandler;
	}

	public void setShipFromJurisdictionHandler(
			SearchJurisdictionHandler shipFromJurisdictionHandler) {
		this.shipFromJurisdictionHandler = shipFromJurisdictionHandler;
	}

	public void setOrdrorgnJurisdictionHandler(
			SearchJurisdictionHandler ordrorgnJurisdictionHandler) {
		this.ordrorgnJurisdictionHandler = ordrorgnJurisdictionHandler;
	}

	public void setOrdracptJurisdictionHandler(
			SearchJurisdictionHandler ordracptJurisdictionHandler) {
		this.ordracptJurisdictionHandler = ordracptJurisdictionHandler;
	}

	public void setFirstuseJurisdictionHandler(
			SearchJurisdictionHandler firstuseJurisdictionHandler) {
		this.firstuseJurisdictionHandler = firstuseJurisdictionHandler;
	}

	public void setCustomerJurisdictionHandler(
			SearchJurisdictionHandler customerJurisdictionHandler) {
		this.customerJurisdictionHandler = customerJurisdictionHandler;
	}

	public List<SelectItem> getCompanies() {
		if (this.companies == null) {
			this.companies = new ArrayList<SelectItem>();
			this.companies.add(new SelectItem("", "Select a Company"));
			List<EntityItem> list = entityItemService
					.findAllEntityItemsByLevel(1L);
			if (list != null) {
				for (EntityItem e : list) {
					this.companies.add(new SelectItem(e.getEntityCode(), e
							.getEntityCode() + " - " + e.getEntityName()));
				}
			}
		}
		return companies;
	}

	public void setCompanies(List<SelectItem> companies) {
		this.companies = companies;
	}

	public List<SelectItem> getDivisions() {
		if (this.divisions == null) {
			this.divisions = new ArrayList<SelectItem>();
			this.divisions.add(new SelectItem("", "Select a Division"));
		}
		return divisions;
	}

	public void setDivisions(List<SelectItem> divisions) {
		this.divisions = divisions;
	}

	public void companySelectionChanged(ValueChangeEvent e) {
		String val = (String) e.getNewValue();
		this.divisions = new ArrayList<SelectItem>();
		this.divisions.add(new SelectItem("", "Select a Division"));

		EntityItem companyEntity = null;
		if (val != null && StringUtils.hasLength(val)) {
			try {
				companyEntity = entityItemService.findByCode(val);
				if (companyEntity != null) {
					long companyId = companyEntity.getEntityId();
					List<EntityItem> list = entityItemService
							.findAllEntityItemsByLevelAndParent(2L, companyId);
					if (list != null) {
						for (EntityItem ei : list) {
							this.divisions.add(new SelectItem(ei
									.getEntityCode(), ei.getEntityCode()
									+ " - " + ei.getEntityName()));
						}
					}
				}
			} catch (Exception ex) {
				logger.error(ex.getMessage(), ex);
			}
		}

		if (companyEntity == null) {
			this.currentInvoice.setEntityCode(null);
			this.currentInvoice.setEntityId(null);
			this.currentInvoice.setGlCompanyName(null);
		} else {
			this.currentInvoice.setGlCompanyName(companyEntity.getDescription());
			this.currentInvoice.setEntityId(companyEntity.getEntityId());
			this.currentInvoice.setEntityCode(companyEntity.getEntityCode());
		}
	}

	public void transactionTypeSelectionChanged(ValueChangeEvent e){
		String val = (String) e.getNewValue();
		if (val != null && StringUtils.hasLength(val)) {
			try {
				this.currentInvoice.setTransactionTypeCode(val);
		
			}catch(Exception ex){
				logger.error(ex.getMessage(), ex);}
			}else{
				this.currentInvoice.setTransactionTypeCode(null);
			}
			
		
	}
	public void ratetypeSelectionChanged(ValueChangeEvent e){
		String val = (String) e.getNewValue();
		if (val != null && StringUtils.hasLength(val)) {
			try {
				this.currentInvoice.setRatetypeCode(val);
		
			}catch(Exception ex){
				logger.error(ex.getMessage(), ex);}
			}else{
				this.currentInvoice.setRatetypeCode(null);
			}
			
		
	}
	public void methodDeliverySelectionChanged(ValueChangeEvent e){
		String val = (String) e.getNewValue();
		if (val != null && StringUtils.hasLength(val)) {
			try {
				this.currentInvoice.setMethodDeliveryCode(val);
		
			}catch(Exception ex){
				logger.error(ex.getMessage(), ex);}
			}else{
				this.currentInvoice.setMethodDeliveryCode(null);
			}
			
		
	}
	public void divisionSelectionChanged(ValueChangeEvent e) {
		String val = (String) e.getNewValue();
		if (val == null) {
			//this.currentInvoice.setDivisionDesc(null);
		} else {
			EntityItem entity = null;
			try {
				entity = entityItemService.findByCode(val);
			} catch (Exception ex) {
				logger.debug(ex.getMessage(), ex);
			}

			if (entity == null) {
				this.currentInvoice.setGlDivisionName(null);
			} else {
				this.currentInvoice.setGlDivisionName(entity.getDescription());
			}
		}
	}

	public String searchCustomerLocationAction() {
		Cust aFilterCust = new Cust();
		aFilterCust.setCustNbr(currentInvoice.getCustNbr());

		CustLocn aFilterCustLocn = new CustLocn();
		aFilterCustLocn.setCustLocnCode(currentInvoice.getCustLocnCode());

		customerLocationLookupBean.setBean(this, "billingTestingToolBean");

		return customerLocationLookupBean.beginLookupAction(aFilterCust,
				aFilterCustLocn);
	}

	public void custLocnSelectionChanged(Cust aCust, CustLocn aCustLocn) {
		if (aCust != null) {
			currentInvoice.setCustNbr(aCust.getCustNbr());
			currentInvoice.setCustName(aCust.getCustName());
		}

		if (aCustLocn != null) {
			currentInvoice.setCustLocnCode(aCustLocn.getCustLocnCode());
			currentInvoice.setLocationName(aCustLocn.getLocationName());

			currentInvoice.setShiptoAddressLine1(aCustLocn.getAddressLine1());
			currentInvoice.setShiptoAddressLine2(aCustLocn.getAddressLine2());
			currentInvoice.setShiptoGeocode(aCustLocn.getGeocode());
			currentInvoice.setShiptoCity(aCustLocn.getCity());
			currentInvoice.setShiptoCounty(aCustLocn.getCounty());
			currentInvoice.setShiptoStateCode(aCustLocn.getState());
			currentInvoice.setShiptoCountryCode(aCustLocn.getCountry());
			currentInvoice.setShiptoZip(aCustLocn.getZip());
			currentInvoice.setShiptoZipplus4(aCustLocn.getZipplus4());

			getShipToJurisdictionHandler();
			shipToJurisdictionHandler.setGeocode(aCustLocn.getGeocode());
			shipToJurisdictionHandler.setCity(aCustLocn.getCity());
			shipToJurisdictionHandler.setCounty(aCustLocn.getCounty());
			shipToJurisdictionHandler.setState(aCustLocn.getState());
			shipToJurisdictionHandler.setCountry(aCustLocn.getCountry());
			shipToJurisdictionHandler.setZip(aCustLocn.getZip());
			shipToJurisdictionHandler.setZipPlus4(aCustLocn.getZipplus4());
		}
	}

	public BillTransaction getCurrentInvoice() {
		return currentInvoice;
	}

	public void setCurrentInvoice(BillTransaction currentInvoice) {
		this.currentInvoice = currentInvoice;
	}

	public ProcessMethod getSelectedProcessMethod() {
		return selectedProcessMethod;
	}

	public void setSelectedProcessMethod(ProcessMethod selectedProcessMethod) {
		this.selectedProcessMethod = selectedProcessMethod;
	}

	public String getSelectedScenarioName() {
		return selectedScenarioName;
	}

	public void setSelectedScenarioName(String selectedScenarioName) {
		this.selectedScenarioName = selectedScenarioName;
	}

	public String customerIdChanged() {
		String newCustNbr = currentInvoice.getCustNbr();

		if (newCustNbr == null || newCustNbr.length() == 0) {
			currentInvoice.setCustNbr("");
			currentInvoice.setCustName("");
		} else {
			currentInvoice.setCustNbr(newCustNbr);
			Cust newCust = custService.getCustByNumber(newCustNbr.trim());
			if (newCust == null) {
				currentInvoice.setCustName("");
			} else {
				currentInvoice.setCustName(newCust.getCustName());
			}

		}

		return null;
	}

	public void recalLocationCode(ValueChangeEvent vce) {
		String newCustLocnCode = (String) vce.getNewValue();
		currentInvoice.setCustLocnCode(newCustLocnCode);

		if (newCustLocnCode == null || newCustLocnCode.length() == 0) {
			currentInvoice.setCustLocnCode("");
			currentInvoice.setLocationName("");
		} else {
			if (currentInvoice.getCustNbr() != null
					&& currentInvoice.getCustNbr().length() > 0) {
				Cust newCust = custService.getCustByNumber(currentInvoice
						.getCustNbr().trim());
				if (newCust != null) {
					CustLocn newCustLocn = custLocnService.getCustLocnByCustId(
							newCust.getCustId(), newCustLocnCode.trim());

					if (newCustLocn != null) {
						currentInvoice.setLocationName(newCustLocn
								.getLocationName());
					} else {
						currentInvoice.setLocationName("");
					}
				} else {
					currentInvoice.setLocationName("");
				}
			} else {
				currentInvoice.setLocationName("");
			}
		}
	}

	public List<SelectItem> getCertTypes() {
		if (certTypes == null) {
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
			selectItems.add(new SelectItem("", "Select a Certificate Type"));

			List<ListCodes> listCodeList = cacheManager
					.getListCodesByType("CERTTYPE");
			for (ListCodes listcodes : listCodeList) {
				if (listcodes != null && listcodes.getCodeTypeCode() != null
						&& listcodes.getDescription() != null) {
					selectItems.add(new SelectItem(listcodes.getCodeCode(),
							listcodes.getDescription()));
				}
			}

			certTypes = selectItems;
		}

		return certTypes;
	}

	public List<SelectItem> getInvoiceDiscTypeCodes() {
		if (invoiceDiscTypeCodes == null) {
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
			selectItems.add(new SelectItem("", "Select a Discount Type"));

			List<ListCodes> listCodeList = cacheManager
					.getListCodesByType("DISCTYPE");
			for (ListCodes listcodes : listCodeList) {
				if (listcodes != null && listcodes.getCodeTypeCode() != null
						&& listcodes.getDescription() != null) {
					selectItems.add(new SelectItem(listcodes.getCodeCode(),
							listcodes.getDescription()));
				}
			}

			invoiceDiscTypeCodes = selectItems;
		}

		return invoiceDiscTypeCodes;
	}

	public List<SelectItem> getFlags() {
		if (flags == null) {
			flags = new ArrayList<SelectItem>();
			flags.add(new SelectItem("0", "Off"));
			flags.add(new SelectItem("1", "On"));
		}
		return flags;
	}

	public List<SelectItem> getInvoiceExemptReasonCodes() {
		if (invoiceExemptReasonCodes == null) {
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
			selectItems.add(new SelectItem("", "Select a Exempt Reason"));

			List<ListCodes> listCodeList = cacheManager
					.getListCodesByType("EXREASON");
			for (ListCodes listcodes : listCodeList) {
				if (listcodes != null && listcodes.getCodeTypeCode() != null
						&& listcodes.getDescription() != null) {
					selectItems.add(new SelectItem(listcodes.getCodeCode(),
							listcodes.getDescription()));
				}
			}

			invoiceExemptReasonCodes = selectItems;
		}

		return invoiceExemptReasonCodes;
	}

	public List<SelectItem> getEntities() {
		if (entities == null) {
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
			selectItems.add(new SelectItem(new Long(0L), "Select an Entity"));

			List<EntityItem> eList = entityItemService.findAllEntityItems();
			if (eList != null && eList.size() > 0) {
				for (EntityItem entityItem : eList) {
					if (entityItem.getEntityId().intValue() != 0) {
						selectItems.add(new SelectItem(
								entityItem.getEntityId(), entityItem
										.getEntityCode()
										+ " - "
										+ entityItem.getEntityName()));
					}
				}
			}

			entities = selectItems;
		}

		return entities;
	}

	public Map<String, String> getBillTransactionUserPropertyMap() {
		if (BillTransactionUserPropertyMap == null) {

			Map<String, String> userMap = new HashMap<String, String>();
			Map<String, DataDefinitionColumn> columnsMap = matrixCommonBean
					.getBillTransactionPropertyMap();

			DataDefinitionColumn dataDefinitionColumn = null;
			NumberFormat formatter = new DecimalFormat("00");
			String columnName = "";

			// /userText01

			for (int i = 1; i <= 100; i++) {
				columnName = "userText" + formatter.format(i);
				dataDefinitionColumn = (DataDefinitionColumn) columnsMap
						.get(columnName);
				String description = "";
				if (dataDefinitionColumn != null
						&& dataDefinitionColumn.getDescription() != null
						&& dataDefinitionColumn.getDescription().length() > 0) {
					description = dataDefinitionColumn.getDescription();
					if (!description.startsWith("User Text")) {
						userMap.put(columnName, description);
					} else {
						userMap.put(columnName, "User Text " + i);
					}
				} else {
					userMap.put(columnName, "User Text " + i);
				}
			}

			for (int i = 1; i <= 25; i++) {
				columnName = "userNumber" + formatter.format(i);
				dataDefinitionColumn = (DataDefinitionColumn) columnsMap
						.get(columnName);
				String description = "";
				if (dataDefinitionColumn != null
						&& dataDefinitionColumn.getDescription() != null
						&& dataDefinitionColumn.getDescription().length() > 0) {
					description = dataDefinitionColumn.getDescription();
					if (!description.startsWith("User Number")) {
						userMap.put(columnName, description);
					} else {
						userMap.put(columnName, "User Number " + i);
					}
				} else {
					userMap.put(columnName, "User Number " + i);
				}
			}

			for (int i = 1; i <= 25; i++) {
				columnName = "userDate" + formatter.format(i);
				dataDefinitionColumn = (DataDefinitionColumn) columnsMap
						.get(columnName);
				String description = "";
				if (dataDefinitionColumn != null
						&& dataDefinitionColumn.getDescription() != null
						&& dataDefinitionColumn.getDescription().length() > 0) {
					description = dataDefinitionColumn.getDescription();
					if (!description.startsWith("User Date")) {
						userMap.put(columnName, description);
					} else {
						userMap.put(columnName, "User Date " + i);
					}
				} else {
					userMap.put(columnName, "User Date " + i);
				}
			}

			BillTransactionUserPropertyMap = userMap;
		}

		return BillTransactionUserPropertyMap;
	}

	public TransactionBillDataMemoryModel gettransactionSaleDataModel() {
		if (this.transactionSaleDataModel == null) {
			this.transactionSaleDataModel = new TransactionBillDataMemoryModel();
			this.transactionSaleDataModel
					.setCommonDAO((GenericDAO<BillTransaction, Long>) transactionDetailBillDAO);
			this.transactionSaleDataModel
					.setBillTransactions(this.invoiceLines);
		}

		return transactionSaleDataModel;
	}

	public void settransactionSaleDataModel(
			TransactionBillDataMemoryModel transactionSaleDataModel) {
		this.transactionSaleDataModel = transactionSaleDataModel;
	}

	public HtmlDataTable getTrDetailTable() {
		if (trDetailTable == null) {
			trDetailTable = new HtmlDataTable();
			trDetailTable.setId("aMDetailList");
			trDetailTable.setVar("trdetail");
			trDetailTable.setRowClasses("odd-row,even-row");
			matrixCommonBean.createBillTransactionDetailTable(trDetailTable,
					"billingTestingToolBean",
					"billingTestingToolBean.transactionSaleDataModel",
					matrixCommonBean.getBillTransactionPropertyMap(), true,
					displayAllFields, false, false, true);
		}

		return trDetailTable;
	}

	public boolean isDisplayAllFields() {
		return displayAllFields;
	}

	public void setDisplayAllFields(boolean displayAllFields) {
		this.displayAllFields = displayAllFields;
	}
	
	//TODO:Check tax amounts and types here,
	public String getValidTaxMessage() {
		String message = "";
		if(selectedTransaction !=null){
			DecimalFormat formatter = new DecimalFormat("#0.00");
			RoundingMode ROUNDING_MODE = RoundingMode.HALF_UP;
			formatter.setRoundingMode(ROUNDING_MODE);
			
			message = "Total Tax:  $";
			if(selectedTransaction.getTbCalcTaxAmt()!=null){
				message = message + formatter.format(selectedTransaction.getTbCalcTaxAmt());
			}
			else{
				message = message + "0.00";
			}
			
			message = message + "    Combined Rate:  ";
			if(selectedTransaction.getCombinedRate()!=null){
				message = message + (selectedTransaction.getCombinedRate().floatValue()*100f);
			}
			else{
				message = message + "0.00";
			}
			
			message = message + "%";
		}

		return message;
	}

	public void displayChange(ActionEvent e) {

		HtmlSelectBooleanCheckbox check = (HtmlSelectBooleanCheckbox) ((HtmlAjaxSupport) e
				.getComponent()).getParent();
		displayAllFields = (check.getSubmittedValue() == null) ? ((check
				.getValue() == null) ? false : (Boolean) check.getValue())
				: Boolean.parseBoolean((String) check.getSubmittedValue());
		// Rebuild the table
		logger.debug("Rebuilding table - all fields: " + displayAllFields);
		matrixCommonBean.createBillTransactionDetailTable(trDetailTable,
				"billingTestingToolBean",
				"billingTestingToolBean.transactionSaleDataModel",
				matrixCommonBean.getBillTransactionPropertyMap(), true,
				displayAllFields, false, false, true);

		String rowStr = (displayAllFields) ? matrixCommonBean.getMatrixDTO()
				.getSysFullThreshold() : matrixCommonBean.getMatrixDTO()
				.getSysLimitedThreshold();
		int rowpage = 0;
		if (rowStr == null || rowStr.length() == 0) {
			rowpage = (displayAllFields) ? 25 : 100;
		} else {
			rowpage = Integer.parseInt(rowStr);
		}

		String muitipleStr = (displayAllFields) ? matrixCommonBean
				.getMatrixDTO().getSysFullThresholdPages() : matrixCommonBean
				.getMatrixDTO().getSysLimitedThresholdPages();
		int muitiplepage = 0;
		if (muitipleStr == null || muitipleStr.length() == 0) {
			muitiplepage = (displayAllFields) ? 4 : 4;
		} else {
			muitiplepage = Integer.parseInt(muitipleStr);
		}

		transactionSaleDataModel.setPageSize(rowpage);
		transactionSaleDataModel.setDbFetchMultiple(muitiplepage);

		resetForHeaderRearrangeTemp();

		LogMemory.executeGC();
	}

	public void resetForHeaderRearrangeTemp() {
		matrixCommonBean.setForHeaderRearrangeTemp(null);
	}

	public String getSelectAllWarning() {
		return "";
	}

	public void setTrDetailTable(HtmlDataTable trDetailTable) {
		this.trDetailTable = trDetailTable;
	}

	public void newInvoice() {
		this.currentInvoice = new BillTransaction();
		this.currentInvoice.setGlDate(new Date());
		shipToJurisdictionHandler = null;
		shipFromJurisdictionHandler = null;
		ordrorgnJurisdictionHandler = null;
		ordracptJurisdictionHandler = null;
		firstuseJurisdictionHandler = null;
		billtoJurisdictionHandler = null;
		customerJurisdictionHandler = null;
		saveScenarioName = null;
		scenarioChanged = false;
		transactionsChanged = false;
	}

	public void clearAll() {
		this.currentInvoice = new BillTransaction();
		this.currentInvoice.setGlDate(new Date());
		this.invoiceLines = new ArrayList<BillTransaction>();
		shipToJurisdictionHandler = null;
		shipFromJurisdictionHandler = null;
		ordrorgnJurisdictionHandler = null;
		ordracptJurisdictionHandler = null;
		firstuseJurisdictionHandler = null;
		billtoJurisdictionHandler = null;
		customerJurisdictionHandler = null;
		saveScenarioName = null;
		scenarioChanged = false;
		transactionsChanged = false;
		this.selectedScenarioName = "*NEW";
		clearGrid();
	}

	public String addLineItem() {
		currentAction = EditAction.ADD;
	
		saveJurisdictionValues();
		this.currentInvoiceLine = new BillTransaction();
		String[] ignoreValues = {"invoiceLineTaxPaidAmt","invoiceLineTaxAmt", "transientTransaction","errorCode","errorText","abortFlag","billtransId","nextId","countyTier3TaxAmt", "autoTransactionCountryCode","distr04Amt","auditTimestamp","glDivisionName","invoiceLineCount","stj2ExemptAmt","stj5TaxAmt","relatedSubtransId","glExtractUpdater","afeProjectNbr","invoiceTaxAmt","exclusionAmt","transactionStateCode","tbCalcTaxAmt","allocationMatrixId","stj8ExemptAmt","cityTier2TaxAmt","afeContractStructure","stateExemptAmt","geoReconCode","cityExemptAmt","distr08Amt","taxDriver10","allocationSubtransId","stj10TaxableAmt","taxDriver11","cityTier3TaxableAmt","locationDriver01","taxDriver12","locationDriver02","taxDriver13","taxDriver14","taxDriver15","cityTier1TaxableAmt","taxDriver16","countyTier3TaxableAmt","taxDriver17","taxDriver18","taxDriver19","cityTier2TaxableAmt","stj9ExemptAmt","stj10TaxAmt","locationDriver05","usageCode","locationDriver06","locationDriver03","locationDriver04","locationDriver09","productClassCode","locationDriver07","locationDriver08","afeContractType","taxDriver01","inventoryName","taxDriver02","taxDriver03","taxDriver04","taxDriver05","taxDriver06","taxDriver07","taxDriver08","taxDriver09","afeCategoryNbr","countyTier1TaxableAmt","countyTier2TaxableAmt","stj3TaxAmt","fatalErrorCode","auditUserId","urlLink","afeProjectName","countryTier2TaxAmt","stj6TaxableAmt","distr03Amt","processingNotes","cpFilingEntityCode","glLocalSubAcctName","stj2TaxableAmt","cityLocalTaxAmt","afeUse","stateTier1TaxAmt","stj4TaxableAmt","custEntityCode","glFullAcctNbr","reprocessCode","invoiceLineDiscAmt","invoiceLineProductCode","glCcNbrDeptName","stj7TaxAmt","stj1ExemptAmt","locationDriver10","sourceTransactionId","transactionCountryCode","afeSubCatName","entityCode","afeSubCatNbr","glProfitCenterNbr","stj8TaxableAmt","stj7ExemptAmt","stj3ExemptAmt","countryTier1TaxAmt","invoiceLineDiscTypeCode","stj10ExemptAmt","glProfitCenterName","stj4TaxAmt","transactionStatus","invoiceTotalAmt","exemptAmt","countyTier2TaxAmt","countyLocalTaxAmt","discountType","distr07Amt","auditFlag","certImage","stj1TaxAmt","invoiceLineFreightAmt","entityId","glFullAcctName","glExtractBatchNo","glLocalSubAcctNbr","productCode","distr02Amt","distr10Amt","invoiceLineTax","autoTransactionStateCode","stj5ExemptAmt","loadTimestamp","stj9TaxAmt","glCompanyName","stateTier1TaxableAmt","countryTier1TaxableAmt","stateTier2TaxableAmt","stateTier2TaxAmt","stateTier3TaxableAmt","suspendInd","countryTier3TaxableAmt","enteredDate","countryTier2TaxableAmt","productClassName","usageName","multiTransCode","inventoryClassName","countyExemptAmt","transactionInd","glExtractFlag","stj6TaxAmt","processtypeCode","manualTaxcodeInd","bcpBilltransId","countryExemptAmt","invoiceLineType","invoiceLineAmt","processMethod","distr06Amt","glLocalAcctNbr","custEntityId","afePropertyCat","cityTier1TaxAmt","rejectFlag","processBatchNo","inventoryNbr","taxDriver30","stj5TaxableAmt","stj7TaxableAmt","invoiceLineMiscAmt","glLocalAcctName","countyTier1TaxAmt","procruleSessionId","stj3TaxableAmt","stj1TaxableAmt","distr05Amt","inventoryClass","distr01Amt","taxDriver20","taxDriver21","taxDriver22","taxDriver23","taxDriver24","taxDriver25","taxDriver26","taxDriver27","taxableAmt","taxDriver28","taxDriver29","splitSubtransId","productName","vendorTaxAmt","taxAllocMatrixId","stj9TaxableAmt","countryTier3TaxAmt","stj2TaxAmt","invoiceTaxFlg","glExtractTimestamp","taxMatrixId","glCcNbrDeptId","glLineItmDistAmt","invoiceLineGrossAmt","stj6ExemptAmt","afeCategoryName","entityLocnSetDtlId","invoiceLineTypeName","distr09Amt","invoiceLineName","invoiceLineWeight","stj8TaxAmt","invoiceLineNbr","stateTier3TaxAmt","stj4ExemptAmt","cityTier3TaxAmt","unlockEntityId","shiptoLocation","shiptoLocationName","shiptoManualJurInd","shiptoEntityId","shiptoEntityCode","shiptoLat","shiptoLong","shiptoLocnMatrixId","shiptoCountryNexusindCode","shiptoStateNexusindCode","shipfromEntityId","shipfromEntityCode","shipfromLat","shipfromLong","shipfromLocnMatrixId","shipfromCountryNexusindCode","shipfromStateNexusindCode","shipfromCountyNexusindCode","shipfromCityNexusindCode","ordracptEntityId","ordracptEntityCode","ordracptLat","ordracptLong","ordracptLocnMatrixId","ordracptCountryNexusindCode","ordracptStateNexusindCode","ordracptCountyNexusindCode","ordracptCityNexusindCode","ordrorgnEntityId","ordrorgnEntityCode","ordrorgnLat","ordrorgnLong","ordrorgnLocnMatrixId","ordrorgnStj1Name","ordrorgnStj2Name","ordrorgnStj3Name","ordrorgnStj4Name","ordrorgnStj5Name","ordrorgnCountryNexusindCode","ordrorgnStateNexusindCode","ordrorgnCountyNexusindCode","ordrorgnCityNexusindCode","firstuseEntityId","firstuseEntityCode","firstuseLat","firstuseLong","firstuseLocnMatrixId","firstuseCountryNexusindCode","firstuseStateNexusindCode","firstuseCountyNexusindCode","firstuseCityNexusindCode","billtoEntityId","billtoEntityCode","billtoLat","billtoLong","billtoLocnMatrixId","billtoCountryNexusindCode","billtoStateNexusindCode","billtoCountyNexusindCode","billtoCityNexusindCode","ttlxfrEntityId","ttlxfrEntityCode","ttlxfrLat","ttlxfrLong","ttlxfrLocnMatrixId","ttlxfrGeocode","ttlxfrStj1Name","ttlxfrStj2Name","ttlxfrStj3Name","ttlxfrStj4Name","ttlxfrStj5Name","ttlxfrCountryNexusindCode","ttlxfrStateNexusindCode","ttlxfrCountyNexusindCode","ttlxfrCityNexusindCode","ttlxfrStj1NexusindCode","ttlxfrStj2NexusindCode","ttlxfrStj3NexusindCode","ttlxfrStj4NexusindCode","ttlxfrStj5NexusindCode","countrySitusMatrixId","stateSitusMatrixId","countySitusMatrixId","citySitusMatrixId","stj1SitusMatrixId","stj2SitusMatrixId","stj3SitusMatrixId","stj4SitusMatrixId","stj5SitusMatrixId","countrySitusCode","stateSitusCode","countySitusCode","citySitusCode","stj1SitusCode","stj2SitusCode","stj3SitusCode","stj4SitusCode","stj5SitusCode","stj6SitusCode","stj7SitusCode","stj8SitusCode","stj9SitusCode","stj10SitusCode","stj1Name","stj2Name","stj3Name","stj4Name","stj5Name","stj6Name","stj7Name","stj8Name","stj9Name","stj10Name","countrySitusJurId","stateSitusJurId","countySitusJurId","citySitusJurId","stj1SitusJurId","stj2SitusJurId","stj3SitusJurId","stj4SitusJurId","stj5SitusJurId","stj6SitusJurId","stj7SitusJurId","stj8SitusJurId","stj9SitusJurId","stj10SitusJurId","countryTaxcodeDetailId","stateTaxcodeDetailId","countyTaxcodeDetailId","cityTaxcodeDetailId","stj1TaxcodeDetailId","stj2TaxcodeDetailId","stj3TaxcodeDetailId","stj4TaxcodeDetailId","stj5TaxcodeDetailId","stj6TaxcodeDetailId","stj7TaxcodeDetailId","stj8TaxcodeDetailId","stj9TaxcodeDetailId","stj10TaxcodeDetailId","countryTaxrateId","stateTaxrateId","countyTaxrateId","cityTaxrateId","stj1TaxrateId","stj2TaxrateId","stj3TaxrateId","stj4TaxrateId","stj5TaxrateId","stj6TaxrateId","stj7TaxrateId","stj8TaxrateId","stj9TaxrateId","stj10TaxrateId","countryTaxcodeOverFlag","stateTaxcodeOverFlag","countyTaxcodeOverFlag","cityTaxcodeOverFlag","stj1TaxcodeOverFlag","stj2TaxcodeOverFlag","stj3TaxcodeOverFlag","stj4TaxcodeOverFlag","stj5TaxcodeOverFlag","stj6TaxcodeOverFlag","stj7TaxcodeOverFlag","stj8TaxcodeOverFlag","stj9TaxcodeOverFlag","stj10TaxcodeOverFlag","countryTaxtypeUsedCode","stateTaxtypeUsedCode","countyTaxtypeUsedCode","cityTaxtypeUsedCode","stj1TaxtypeUsedCode","stj2TaxtypeUsedCode","stj3TaxtypeUsedCode","stj4TaxtypeUsedCode","stj5TaxtypeUsedCode","stj6TaxtypeUsedCode","stj7TaxtypeUsedCode","stj8TaxtypeUsedCode","stj9TaxtypeUsedCode","stj10TaxtypeUsedCode","countryNexusindCode","stateNexusindCode","countyNexusindCode","cityNexusindCode","stj1NexusindCode","stj2NexusindCode","stj3NexusindCode","stj4NexusindCode","stj5NexusindCode","stj6NexusindCode","stj7NexusindCode","stj8NexusindCode","stj9NexusindCode","stj10NexusindCode","countryNexusDefDetailId","stateNexusDefDetailId","countyNexusDefDetailId","cityNexusDefDetailId","stj1NexusDefDetailId","stj2NexusDefDetailId","stj3NexusDefDetailId","stj4NexusDefDetailId","stj5NexusDefDetailId","stj6NexusDefDetailId","stj7NexusDefDetailId","stj8NexusDefDetailId","stj9NexusDefDetailId","stj10NexusDefDetailId","countryTaxcodeTypeCode","stateTaxcodeTypeCode","countyTaxcodeTypeCode","cityTaxcodeTypeCode","stj1TaxcodeTypeCode","stj2TaxcodeTypeCode","stj3TaxcodeTypeCode","stj4TaxcodeTypeCode","stj5TaxcodeTypeCode","stj6TaxcodeTypeCode","stj7TaxcodeTypeCode","stj8TaxcodeTypeCode","stj9TaxcodeTypeCode","stj10TaxcodeTypeCode","glToDate","countryTier1Rate","countryTier2Rate","countryTier3Rate","countryTier1Setamt","countryTier2Setamt","countryTier3Setamt","countryTier1MaxAmt","countryTier2MinAmt","countryTier2MaxAmt","countryTier2EntamFlag","countryTier3EntamFlag","countrySpecialSetamt","countrySpecialRate","countryMaxtaxAmt","countryTaxableThresholdAmt","countryMinimumTaxableAmt","countryMaximumTaxableAmt","countryMaximumTaxAmt","countryBaseChangePct","countryTaxProcessTypeCode","stateTier1Rate","stateTier2Rate","stateTier3Rate","stateTier1Setamt","stateTier2Setamt","stateTier3Setamt","stateTier1MaxAmt","stateTier2MinAmt","stateTier2MaxAmt","stateTier2EntamFlag","stateTier3EntamFlag","stateSpecialSetamt","stateSpecialRate","stateMaxtaxAmt","stateTaxableThresholdAmt","stateMinimumTaxableAmt","stateMaximumTaxableAmt","stateMaximumTaxAmt","stateBaseChangePct","stateTaxProcessTypeCode","countyTier1Rate","countyTier2Rate","countyTier3Rate","countyTier1Setamt","countyTier2Setamt","countyTier3Setamt","countyTier1MaxAmt","countyTier2MinAmt","countyTier2MaxAmt","countyTier2EntamFlag","countyTier3EntamFlag","countySpecialSetamt","countySpecialRate","countyMaxtaxAmt","countyTaxableThresholdAmt","countyMinimumTaxableAmt","countyMaximumTaxableAmt","countyMaximumTaxAmt","countyBaseChangePct","countyTaxProcessTypeCode","cityTier1Rate","cityTier2Rate","cityTier3Rate","cityTier1Setamt","cityTier2Setamt","cityTier3Setamt","cityTier1MaxAmt","cityTier2MinAmt","cityTier2MaxAmt","cityTier2EntamFlag","cityTier3EntamFlag","citySpecialSetamt","citySpecialRate","cityMaxtaxAmt","cityTaxableThresholdAmt","cityMinimumTaxableAmt","cityMaximumTaxableAmt","cityMaximumTaxAmt","cityBaseChangePct","cityTaxProcessTypeCode","stj1Rate","stj1Setamt","stj2Rate","stj2Setamt","stj3Rate","stj3Setamt","stj4Rate","stj4Setamt","stj5Rate","stj5Setamt","stj6Rate","stj6Setamt","stj7Rate","stj7Setamt","stj8Rate","stj8Setamt","stj9Rate","stj9Setamt","stj10Rate","stj10Setamt","combinedRate","countryTaxtypeCode","countryRatetypeCode","countryGoodSvcTypeCode","countryCitationRef","countryExemptReason","countryGroupByDriver","countryAllocBucket","countryAllocProrateByCode","countryAllocConditionCode","stateTaxtypeCode","stateRatetypeCode","stateGoodSvcTypeCode","stateCitationRef","stateExemptReason","stateGroupByDriver","stateAllocBucket","stateAllocProrateByCode","stateAllocConditionCode","countyTaxtypeCode","countyRatetypeCode","countyGoodSvcTypeCode","countyCitationRef","countyExemptReason","countyGroupByDriver","countyAllocBucket","countyAllocProrateByCode","countyAllocConditionCode","cityTaxtypeCode","cityRatetypeCode","cityGoodSvcTypeCode","cityCitationRef","cityExemptReason","cityGroupByDriver","cityAllocBucket","cityAllocProrateByCode","cityAllocConditionCode","stj1TaxtypeCode","stj1RatetypeCode","stj1GoodSvcTypeCode","stj1CitationRef","stj1ExemptReason","stj1GroupByDriver","stj1AllocBucket","stj1AllocProrateByCode","stj1AllocConditionCode","stj2TaxtypeCode","stj2RatetypeCode","stj2GoodSvcTypeCode","stj2CitationRef","stj2ExemptReason","stj2GroupByDriver","stj2AllocBucket","stj2AllocProrateByCode","stj2AllocConditionCode","stj3TaxtypeCode","stj3RatetypeCode","stj3GoodSvcTypeCode","stj3CitationRef","stj3ExemptReason","stj3GroupByDriver","stj3AllocBucket","stj3AllocProrateByCode","stj3AllocConditionCode","stj4TaxtypeCode","stj4RatetypeCode","stj4GoodSvcTypeCode","stj4CitationRef","stj4ExemptReason","stj4GroupByDriver","stj4AllocBucket","stj4AllocProrateByCode","stj4AllocConditionCode","stj5TaxtypeCode","stj5RatetypeCode","stj5GoodSvcTypeCode","stj5CitationRef","stj5ExemptReason","stj5GroupByDriver","stj5AllocBucket","stj5AllocProrateByCode","stj5AllocConditionCode","stj6TaxtypeCode","stj6RatetypeCode","stj6GoodSvcTypeCode","stj6CitationRef","stj6ExemptReason","stj6GroupByDriver","stj6AllocBucket","stj6AllocProrateByCode","stj6AllocConditionCode","stj7TaxtypeCode","stj7RatetypeCode","stj7GoodSvcTypeCode","stj7CitationRef","stj7ExemptReason","stj7GroupByDriver","stj7AllocBucket","stj7AllocProrateByCode","stj7AllocConditionCode","stj8TaxtypeCode","stj8RatetypeCode","stj8GoodSvcTypeCode","stj8CitationRef","stj8ExemptReason","stj8GroupByDriver","stj8AllocBucket","stj8AllocProrateByCode","stj8AllocConditionCode","stj9TaxtypeCode","stj9RatetypeCode","stj9GoodSvcTypeCode","stj9CitationRef","stj9ExemptReason","stj9GroupByDriver","stj9AllocBucket","stj9AllocProrateByCode","stj9AllocConditionCode","stj10TaxtypeCode","stj10RatetypeCode","stj10GoodSvcTypeCode","stj10CitationRef","stj10ExemptReason","stj10GroupByDriver","stj10AllocBucket","stj10AllocProrateByCode","stj10AllocConditionCode","countryNexusTypeCode","stateNexusTypeCode","countyNexusTypeCode","cityNexusTypeCode","stj1NexusTypeCode","stj2NexusTypeCode","stj3NexusTypeCode","stj4NexusTypeCode","stj5NexusTypeCode","stj6NexusTypeCode","stj7NexusTypeCode","stj8NexusTypeCode","stj9NexusTypeCode","stj10NexusTypeCode","countryPrimarySitusCode","countryPrimaryTaxtypeCode","countrySecondarySitusCode","countrySecondaryTaxtypeCode","countryAllLevelsFlag","statePrimarySitusCode","statePrimaryTaxtypeCode","stateSecondarySitusCode","stateSecondaryTaxtypeCode","stateAllLevelsFlag","countyPrimarySitusCode","countyPrimaryTaxtypeCode","countySecondarySitusCode","countySecondaryTaxtypeCode","countyAllLevelsFlag","cityPrimarySitusCode","cityPrimaryTaxtypeCode","citySecondarySitusCode","citySecondaryTaxtypeCode","cityAllLevelsFlag","stj1PrimarySitusCode","stj1PrimaryTaxtypeCode","stj1SecondarySitusCode","stj1SecondaryTaxtypeCode","stj1AllLevelsFlag","stj2PrimarySitusCode","stj2PrimaryTaxtypeCode","stj2SecondarySitusCode","stj2SecondaryTaxtypeCode","stj2AllLevelsFlag","stj3PrimarySitusCode","stj3PrimaryTaxtypeCode","stj3SecondarySitusCode","stj3SecondaryTaxtypeCode","stj3AllLevelsFlag","stj4PrimarySitusCode","stj4PrimaryTaxtypeCode","stj4SecondarySitusCode","stj4SecondaryTaxtypeCode","stj4AllLevelsFlag","stj5PrimarySitusCode","stj5PrimaryTaxtypeCode","stj5SecondarySitusCode","stj5SecondaryTaxtypeCode","stj5AllLevelsFlag","stj6PrimarySitusCode","stj6PrimaryTaxtypeCode","stj6SecondarySitusCode","stj6SecondaryTaxtypeCode","stj6AllLevelsFlag","stj7PrimarySitusCode","stj7PrimaryTaxtypeCode","stj7SecondarySitusCode","stj7SecondaryTaxtypeCode","stj7AllLevelsFlag","stj8PrimarySitusCode","stj8PrimaryTaxtypeCode","stj8SecondarySitusCode","stj8SecondaryTaxtypeCode","stj8AllLevelsFlag","stj9PrimarySitusCode","stj9PrimaryTaxtypeCode","stj9SecondarySitusCode","stj9SecondaryTaxtypeCode","stj9AllLevelsFlag","stj10PrimarySitusCode","stj10PrimaryTaxtypeCode","stj10SecondarySitusCode","stj10SecondaryTaxtypeCode","stj10AllLevelsFlag","processStatus","isReprocessing","measureTypeCode","transEntityName","inheritEntityName","suspendCode","reprocessFlag","vendorNexusFlag","invoiceVerificationFlag","defaultMatrixFlag","prorateAmt","pushToTrans","pushToState","pushToCounty","pushToCity","pushToStj1","pushToStj2","pushToStj3","pushToStj4","pushToStj5","pushToStj6","pushToStj7","pushToStj8","pushToStj9","pushToStj10","pushTotal","instanceCreatedId","stj1JurisdTaxRate","stj1JurisdTaxSetAmount","stj2JurisdTaxRate","stj2JurisdTaxSetAmount","stj3JurisdTaxRate","stj3JurisdTaxSetAmount","stj4JurisdTaxRate","stj4JurisdTaxSetAmount","stj5JurisdTaxRate","stj5JurisdTaxSetAmount","stj6JurisdTaxRate","stj6JurisdTaxSetAmount","stj7JurisdTaxRate","stj7JurisdTaxSetAmount","stj8JurisdTaxRate","stj8JurisdTaxSetAmount","stj9JurisdTaxRate","stj9JurisdTaxSetAmount","stj10JurisdTaxRate","stj10JurisdTaxSetAmount","result","lineItemTestAmt"};
		BeanUtils.copyProperties(this.currentInvoice, this.currentInvoiceLine, ignoreValues);	
		currentInvoiceLine.setResult(STATUS_BLANK);
		this.currentInvoiceLine.setBilltransId(null);
		this.currentInvoiceLine.setProcessMethod("BillScenario");
		this.currentInvoiceLine.setEnteredDate(new Date());
		this.currentInvoiceLine.setCalculateInd("1");
		this.invoiceLineSelectedPage = "basicinformationFilter";
		this.lineTogglePanelController = null;
		
		shipToJurisdictionHandlerLine = new SearchJurisdictionHandler();
		shipFromJurisdictionHandlerLine = new SearchJurisdictionHandler();
		ordrorgnJurisdictionHandlerLine = new SearchJurisdictionHandler();
		ordracptJurisdictionHandlerLine = new SearchJurisdictionHandler();
		firstuseJurisdictionHandlerLine = new SearchJurisdictionHandler();
		billtoJurisdictionHandlerLine = new SearchJurisdictionHandler();
		customerJurisdictionHandlerLine = new SearchJurisdictionHandler();
		
		jurisdictionCopyOver(shipToJurisdictionHandler, shipToJurisdictionHandlerLine);
		jurisdictionCopyOver(shipFromJurisdictionHandler, shipFromJurisdictionHandlerLine);
		jurisdictionCopyOver(ordrorgnJurisdictionHandler, ordrorgnJurisdictionHandlerLine);
		jurisdictionCopyOver(ordracptJurisdictionHandler, ordracptJurisdictionHandlerLine);
		jurisdictionCopyOver(firstuseJurisdictionHandler, firstuseJurisdictionHandlerLine);
		jurisdictionCopyOver(billtoJurisdictionHandler, billtoJurisdictionHandlerLine);
		jurisdictionCopyOver(customerJurisdictionHandler, customerJurisdictionHandlerLine);

		shipToJurisdictionHandlerLine.setCallbackScreen("billing_test_tool_add_line");
		shipFromJurisdictionHandlerLine.setCallbackScreen("billing_test_tool_add_line");
		ordrorgnJurisdictionHandlerLine.setCallbackScreen("billing_test_tool_add_line");
		ordracptJurisdictionHandlerLine.setCallbackScreen("billing_test_tool_add_line");
		firstuseJurisdictionHandlerLine.setCallbackScreen("billing_test_tool_add_line");
		billtoJurisdictionHandlerLine.setCallbackScreen("billing_test_tool_add_line");
		shipToJurisdictionHandlerLine.setBillingTestToolScreen(true);
		shipFromJurisdictionHandlerLine.setBillingTestToolScreen(true);
		ordrorgnJurisdictionHandlerLine.setBillingTestToolScreen(true);
		ordracptJurisdictionHandlerLine.setBillingTestToolScreen(true);
		firstuseJurisdictionHandlerLine.setBillingTestToolScreen(true);
		billtoJurisdictionHandlerLine.setBillingTestToolScreen(true);

		
		return "billing_test_tool_add_line";
	}
	
	public void jurisdictionCopyOver(SearchJurisdictionHandler fromHandler, SearchJurisdictionHandler toHandler) {
		if (fromHandler != null) {
			toHandler.setCacheManager(cacheManager);
			toHandler.setJurisdictionService(jurisdictionService);
			toHandler.setCallbackExt(this);
			toHandler.setCallbackLocation(this);
			
			toHandler.reset();
			toHandler.setJurisdictionId(fromHandler.getJurisdictionId());
			toHandler.setCountry(fromHandler.getCountry());
			toHandler.setState(fromHandler.getState());
			toHandler.setGeocode(fromHandler.getGeocode());
			toHandler.setCity(fromHandler.getCity());
			toHandler.setCounty(fromHandler.getCounty());
			toHandler.setZip(fromHandler.getZip());
			toHandler.setZipPlus4(fromHandler.getZipPlus4());
			toHandler.setTaxJurisdictionBean(taxJurisdictionBean);
			toHandler.setStj1Name(fromHandler.getStj1Name());
			toHandler.setStj2Name(fromHandler.getStj2Name());
			toHandler.setStj3Name(fromHandler.getStj3Name());
			toHandler.setStj4Name(fromHandler.getStj4Name());
			toHandler.setStj5Name(fromHandler.getStj5Name());
			


		}
	}

	public String updateLineItem() {
		currentAction = EditAction.UPDATE;
		//Set Jurisdiction Handlers
//		shipToJurisdictionHandlerLine = getShipToJurisdictionHandlerLine();
//		shipFromJurisdictionHandlerLine = getShipFromJurisdictionHandlerLine();
//		ordrorgnJurisdictionHandlerLine = getOrdrorgnJurisdictionHandlerLine();
//		ordracptJurisdictionHandlerLine = getOrdracptJurisdictionHandlerLine();
//		firstuseJurisdictionHandlerLine = getFirstuseJurisdictionHandlerLine();
//		billtoJurisdictionHandlerLine = getBilltoJurisdictionHandlerLine();
//
//		BeanUtils.copyProperties(shipToJurisdictionHandler,shipToJurisdictionHandlerLine);
//		BeanUtils.copyProperties(shipFromJurisdictionHandler,shipFromJurisdictionHandlerLine);
//		BeanUtils.copyProperties(ordrorgnJurisdictionHandler,ordrorgnJurisdictionHandlerLine);
//		BeanUtils.copyProperties(ordracptJurisdictionHandler,ordracptJurisdictionHandlerLine);
//		BeanUtils.copyProperties(firstuseJurisdictionHandler,firstuseJurisdictionHandlerLine);
//		BeanUtils.copyProperties(billtoJurisdictionHandler,billtoJurisdictionHandlerLine);
		saveJurisdictionValues();
		this.currentInvoiceLine = new BillTransaction();
		
		BeanUtils.copyProperties(this.currentInvoice, this.currentInvoiceLine, new String[] {"instanceCreatedId"});	
		
		currentInvoiceLine.setResult(STATUS_BLANK);
		this.invoiceLineSelectedPage = "basicinformationFilter";
		this.lineTogglePanelController = null;
		this.currentInvoiceLine.setEnteredDate(new Date());

		shipToJurisdictionHandlerLine = new SearchJurisdictionHandler();
		shipFromJurisdictionHandlerLine = new SearchJurisdictionHandler();
		ordrorgnJurisdictionHandlerLine = new SearchJurisdictionHandler();
		ordracptJurisdictionHandlerLine = new SearchJurisdictionHandler();
		firstuseJurisdictionHandlerLine = new SearchJurisdictionHandler();
		billtoJurisdictionHandlerLine = new SearchJurisdictionHandler();
		
		jurisdictionCopyOver(shipToJurisdictionHandler, shipToJurisdictionHandlerLine);
		jurisdictionCopyOver(shipFromJurisdictionHandler, shipFromJurisdictionHandlerLine);
		jurisdictionCopyOver(ordrorgnJurisdictionHandler, ordrorgnJurisdictionHandlerLine);
		jurisdictionCopyOver(ordracptJurisdictionHandler, ordracptJurisdictionHandlerLine);
		jurisdictionCopyOver(firstuseJurisdictionHandler, firstuseJurisdictionHandlerLine);
		jurisdictionCopyOver(billtoJurisdictionHandler, billtoJurisdictionHandlerLine);

		return "billing_test_tool_add_line";
	}

	public List<BillTransaction> getInvoiceLines() {
		return invoiceLines;
	}

	public void setInvoiceLines(List<BillTransaction> invoiceLines) {
		this.invoiceLines = invoiceLines;
	}

	public BillTransaction getCurrentInvoiceLine() {
		return currentInvoiceLine;
	}

	public void setCurrentInvoiceLine(BillTransaction currentInvoiceLine) {
		this.currentInvoiceLine = currentInvoiceLine;
	}

	public String getInvoiceLineSelectedPage() {
		return invoiceLineSelectedPage;
	}

	public void setInvoiceLineSelectedPage(String invoiceLineSelectedPage) {
		this.invoiceLineSelectedPage = invoiceLineSelectedPage;
	}

	public String saveInvoiceLine() {
		saveJurisdictionInvoiceValues();
//		BeanUtils.copyProperties(shipToJurisdictionHandlerLine, shipToJurisdictionHandler);
//		BeanUtils.copyProperties(shipFromJurisdictionHandlerLine, shipFromJurisdictionHandler);
//		BeanUtils.copyProperties(ordrorgnJurisdictionHandlerLine, ordrorgnJurisdictionHandler);
//		BeanUtils.copyProperties(ordracptJurisdictionHandlerLine, ordracptJurisdictionHandler);
//		BeanUtils.copyProperties(firstuseJurisdictionHandlerLine, firstuseJurisdictionHandler);
//		BeanUtils.copyProperties(billtoJurisdictionHandlerLine, billtoJurisdictionHandler);
		jurisdictionCopyOver(shipToJurisdictionHandlerLine, shipToJurisdictionHandler);
		jurisdictionCopyOver(shipFromJurisdictionHandlerLine, shipFromJurisdictionHandler);
		jurisdictionCopyOver(ordrorgnJurisdictionHandlerLine, ordrorgnJurisdictionHandler);
		jurisdictionCopyOver(ordracptJurisdictionHandlerLine, ordracptJurisdictionHandler);
		jurisdictionCopyOver(firstuseJurisdictionHandlerLine, firstuseJurisdictionHandler);
		jurisdictionCopyOver(billtoJurisdictionHandlerLine, billtoJurisdictionHandler);
		jurisdictionCopyOver(customerJurisdictionHandlerLine, customerJurisdictionHandler);

		if (currentAction.equals(EditAction.ADD)) {
			
			if (this.invoiceLines == null) {
				this.invoiceLines = new ArrayList<BillTransaction>();
			}

			if (this.currentInvoiceLine != null) {
				this.invoiceLines.add(this.currentInvoiceLine);
				BeanUtils.copyProperties(currentInvoiceLine, currentInvoice);
			}
		} else if (currentAction.equals(EditAction.UPDATE)) {
			saveJurisdictionInvoiceValues();
			if (this.invoiceLines != null) {
				List<BillTransaction> newList = new ArrayList<BillTransaction>();
				for (BillTransaction t : this.invoiceLines) {
					if (t.getId().equals(this.currentInvoiceLine.getId())) {
						//Reset status
						currentInvoiceLine.setBilltransId(null);				
						currentInvoiceLine.setResult(STATUS_BLANK);
						newList.add(this.currentInvoiceLine);
						BeanUtils.copyProperties(currentInvoiceLine, currentInvoice);
					} else {
						//Reset status
						t.setBilltransId(null);				
						t.setResult(STATUS_BLANK);
						newList.add(t);
					}
				}

				this.invoiceLines = newList;
			}
		}

		this.transactionSaleDataModel = null;
		this.allSelected = false;

		this.scenarioChanged = true;
		this.transactionsChanged = true;
		
		this.selectedTransaction = null;
		this.selectedRowIndex = -1;
		this.currentAction = EditAction.DEFAULT;
		return "billing_test_tool";
	}

	public String cancelInvoiceLine() {
		this.currentInvoiceLine = null;
		this.currentAction = EditAction.DEFAULT;
		return "billing_test_tool";
	}

	public String process() {
		saveToolDocs = null;
		if (this.invoiceLines != null && this.invoiceLines.size() > 0) {
			Set<Long> selectedIds = null;
			if(this.transactionSaleDataModel != null) {
				selectedIds = this.transactionSaleDataModel
					.getSelectionMap().keySet();
			}
			
			List<BillTransaction> list = new ArrayList<BillTransaction>();
			if (selectedIds != null && selectedIds.size() > 0) {
				for (BillTransaction t : this.invoiceLines) {
					if (selectedIds.contains(t.getId())) {
						list.add(t);
					}
				}
			} else if(this.selectedTransaction != null) {
				list.add(this.selectedTransaction);
			}
			//TODO:This is the logic we will need to call the BillTransactionAPI in.
			
			if (list != null && list.size() > 0) {
				List<MicroApiTransactionDocument> docList = buildDocument(list);
				if (docList != null && docList.size() > 0) {
					for (MicroApiTransactionDocument doc : docList) {
						if (doc != null) {
							try {
								//String returnMessage = BillTransactionProcess.process(doc,  true, Auditable.currentUserCode());
								purchaseTransactionService.billTransactionProcess(doc, false, LogSourceEnum.TestingTool);

//								if(returnMessage!=null && returnMessage.length()>0){
//									if(returnMessage.endsWith("\n")){
//										returnMessage = returnMessage.substring(0, returnMessage.length()-1);
//									}
//								}
								
								//0003992, Calculate combined_rate for only rates where the calculated tax amount <> 0	
								List<MicroApiTransaction> transactions = doc.getMicroApiTransaction();
								for(int i = 0; i < transactions.size(); i++) {
									MicroApiTransaction transaction = transactions.get(i);
									BillTransaction updatedBillTrans = new BillTransaction();
									transaction.updateBillTransaction(updatedBillTrans);
									transaction.updateBillTransaction(list.get(i));
									if(updatedBillTrans.getTbCalcTaxAmt()==null || (updatedBillTrans.getTbCalcTaxAmt().doubleValue()==0.0d)){
										list.get(i).setCombinedRate(new BigDecimal(0.0));
									}
									
									//Set processed
									list.get(i).setResult(updatedBillTrans.getProcessStatus());
									
									
									//updatedBillTrans.setBilltransId(null);
									//list.get(i).setTransactionStatus(updatedBillTrans.getProcessStatus());

									//Need to use updateBillTransaction to update corresponding element in the invoice lines list.
									
									
								}

							} catch (PurchaseTransactionProcessingException e) {
								e.printStackTrace();
							}
						}
					}
					
					this.transactionsChanged = true;
					if(this.selectedTransaction != null) {
						this.selectedTransaction = null;
						this.selectedRowIndex = -1;
					}
				}
				saveToolDocs = docList;
			}
		}
		//this.invoiceLines.get(0).setTransactionInd("E");
		return "";
	}
//TODO: Work out if this was the correct way of making a BillTransactionDocument.
	private List<MicroApiTransactionDocument> buildDocument(
			List<BillTransaction> invoiceLines) {
		BillTransactionDocument billDoc = new BillTransactionDocument();
		billDoc.setBillTransaction(invoiceLines);
		MicroApiTransactionDocument doc = new MicroApiTransactionDocument(billDoc);
//		List<BillTransactionDocument> docList = new ArrayList<BillTransactionDocument>();
//		if (invoiceLines != null) {
//			Map<String, List<BillTransaction>> sortedMap = new TreeMap<String, List<BillTransaction>>();
//			for (BillTransaction t : invoiceLines) {
//				if (t != null) {
//					String key = StringUtils.hasText(t.getInvoiceNbr()) ? t
//							.getInvoiceNbr() : "*NULL";
//					if (sortedMap.containsKey(key)) {
//						sortedMap.get(key).add(t);
//					} else {
//						List<BillTransaction> list = new ArrayList<BillTransaction>();
//						list.add(t);
//						sortedMap.put(key, list);
//					}
//				}
//			}
//
//			for (Map.Entry<String, List<BillTransaction>> e : sortedMap
//					.entrySet()) {
//				if ("*NULL".equals(e.getKey())) {
//					for (BillTransaction t : e.getValue()) {
//						BillTransactionDocument doc = new BillTransactionDocument();
//						List<BillTransaction> l = new ArrayList<BillTransaction>();
//						l.add(t);
//						doc.setBillTransaction(l);
//						docList.add(doc);
//					}
//				} else {
//					BillTransactionDocument doc = new BillTransactionDocument();
//					doc.setBillTransaction(e.getValue());
//					docList.add(doc);
//				}
//			}
//		}
//
//		return docList;
		List<MicroApiTransactionDocument> docList = new ArrayList<MicroApiTransactionDocument>();
		docList.add(doc);
		return docList;
	}

	public String clearGrid() {
		this.allSelected = false;
		this.selectedTransaction = null;
		this.selectedRowIndex = -1;
		this.transactionSaleDataModel = new TransactionBillDataMemoryModel();
		
		this.transactionSaleDataModel
				.setCommonDAO((GenericDAO<BillTransaction, Long>) transactionDetailBillDAO);

		return "";
	}

	public String saveTransactions() {
		
		if (this.invoiceLines != null && this.invoiceLines.size() > 0) {
			Set<Long> selectedIds = null;
			if(this.transactionSaleDataModel != null) {
				selectedIds = this.transactionSaleDataModel.getSelectionMap().keySet();
			}
			
			List<BillTransaction> list = new ArrayList<BillTransaction>();
			if (selectedIds != null && selectedIds.size() > 0) {
				for (BillTransaction t : this.invoiceLines) {
					if (selectedIds.contains(t.getId()) && t.getResult()!=null && (t.getResult().equals(STATUS_PROCESSED) || t.getResult().equals(STATUS_SAVED))) {
						//Always save to a new transaction
						t.setBilltransId(null);
						transactionDetailBillDAO.save(t);					
						t.setResult(STATUS_SAVED);
						list.add(t);
					}
				}
			} else if(this.selectedTransaction != null && selectedTransaction.getResult()!=null && (selectedTransaction.getResult().equals(STATUS_PROCESSED) || selectedTransaction.getResult().equals(STATUS_SAVED))) {
				//Always save to a new transaction
				selectedTransaction.setBilltransId(null);
				transactionDetailBillDAO.save(selectedTransaction);					
				selectedTransaction.setResult(STATUS_SAVED);
				list.add(selectedTransaction);
			}
			//Log results
			//List<MicroApiTransactionDocument> docList = buildDocument(list);
			
			for(MicroApiTransactionDocument doc : saveToolDocs) {
				for(BillTransaction saveCompare : list) {
					if(doc.getMicroApiTransaction().get(0).getInstanceCreatedId() == saveCompare.getInstanceCreatedId()) {
						purchaseTransactionService.billTransactionLogSave(doc);
					}
				}
			}
		}
		
		/*
		if (this.invoiceLines != null && this.invoiceLines.size() > 0) {
			for (BillTransaction t : this.invoiceLines) {
				try {
					
					//if (t.getBilltransId() == null
					//		|| t.getBilltransId().equals(0L)) {
					//	t.setBilltransId(null);
					//	transactionDetailBillDAO.save(t);
					//} else {
					//	transactionDetailBillDAO.update(t);
					//}
					
					//Always save to a new transaction
					t.setBilltransId(null);
					transactionDetailBillDAO.save(t);
		
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			this.transactionSaleDataModel = null;
			this.transactionsChanged = false;
			this.selectedTransaction = null;
			this.selectedRowIndex = -1;
		}
		*/
		
		return "";
	}

	public String saveScenarioOverwrite() {
		deleteScenario(this.saveScenarioName);
		saveScenario(this.saveScenarioName);
		this.scenarioChanged = false;
		this.displayNewScenarioName = false;
		this.displayScenarioExists = false;
		this.invoiceLines = null;
		this.transactionSaleDataModel = null;

		openScenario(this.saveScenarioName);

		this.saveScenarioName = null;

		return "";
	}

	public String saveScenario() {
		this.displayScenarioExists = false;
		if (this.displayNewScenarioName
				&& !"*NEW".equals(this.saveScenarioName)) {
			if (!StringUtils.hasText(this.saveScenarioName)) {
				this.displayNewScenarioName = true;
			} else {
				if (this.scenarioNames != null) {
					for (SelectItem si : this.scenarioNames) {
						if (si.getValue().equals(this.saveScenarioName)) {
							this.displayScenarioExists = true;
							break;
						}
					}
				}

				if (this.displayScenarioExists) {
					this.displayNewScenarioName = false;
					return "";
				} else {
					if (this.invoiceLines != null) {
						saveScenario(this.saveScenarioName);
						this.scenarioChanged = false;
						this.scenarioNames = null;
						this.invoiceLines = null;
						this.transactionSaleDataModel = null;

						openScenario(this.saveScenarioName);

						this.saveScenarioName = null;
					}
					this.displayNewScenarioName = false;
					this.displayScenarioExists = false;
				}
			}
		} else {
			if ("*NEW".equals(this.selectedScenarioName)) {
				this.displayNewScenarioName = true;
			} else {
				this.displayNewScenarioName = false;
				deleteScenario(this.selectedScenarioName);
				saveScenario(this.selectedScenarioName);
				this.scenarioChanged = false;
				this.displaySaveFirstWarning = false;
			}
		}
		return "";
	}

	private void deleteScenario(String name) {
		int count = scenarioService.deleteByName(name);
		if(count == 0) 
			logger.debug("No Scenarios Deleted");
	}

	private void saveScenario(String name) {
		if (this.invoiceLines != null) {
			for (BillTransaction t : this.invoiceLines) {
				BillScenario sc = new BillScenario();
				sc.setBillscenarioName(name);
				BeanUtils.copyProperties(t, sc);
				sc.setBillscenarioId(null);
				scenarioService.save(sc);
			}
		}
	}

	public String saveAsScenario() {
		return "";
	}

	public String deleteScenario() {
		deleteScenario(this.selectedScenarioName);
		this.scenarioNames = null;
		clearAll();

		return "";
	}

	public String saveAs() {
		return "";
	}

	public boolean getHasInvoiceLines() {
		return (this.invoiceLines != null && this.invoiceLines.size() > 0);
	}

	public void scenarioSelectionChanged(ValueChangeEvent e) {
		if (this.getShowSaveScenario()) {
			String oldValue = (String) e.getOldValue();
			this.nextScenarioName = (String) e.getNewValue();
			this.selectedScenarioName = oldValue;
			this.displaySaveFirstWarning = true;
		} else {
			this.nextScenarioName = null;
			this.displaySaveFirstWarning = false;
			String value = (String) e.getNewValue();
			this.selectedScenarioName = value;
		}
	}

	public String openScenario() {
		if (!this.getShowSaveScenario()) {
			openScenario(this.selectedScenarioName);
		}

		return "";
	}

	public void openScenario(String name) {
		if (name != null) {
			if ("*NEW".equals(name)) {
				clearAll();
			} else {
				clearAll();
				this.selectedScenarioName = name;
				BillScenario sc = new BillScenario();
				sc.setBillscenarioName(name);
				List<BillScenario> list = scenarioService.findByExample(sc, -1);
				if (list != null && list.size() > 0) {
					this.invoiceLines = new ArrayList<BillTransaction>();
					for (BillScenario s : list) {
						BillTransaction t = new BillTransaction();
						BeanUtils.copyProperties(s, t);
						t.setBilltransId(null);
						this.invoiceLines.add(t);
					}
					this.currentInvoice = new BillTransaction();
					BeanUtils
							.copyProperties(list.get(0), this.currentInvoice,
									new String[] { "invoiceLineNbr",
											"invoiceLineDesc",
											"invoiceLineItemCount",
											"invoiceLineItemAmt",
											"invoiceLineGrossAmt",
											"invoiceLineFreightAmt",
											"invoiceLineDiscAmt",
											"invoiceLineDiscTypeCode",
											"invoiceLineTaxPaidAmt",
											"invoiceLineDiffAmt",
											"invoiceLineMiscAmt" });
					this.currentInvoice.setBilltransId(null);
				}
				
				if(currentInvoice.getGlCompanyNbr() != null && currentInvoice.getGlCompanyNbr() != "")
					populateDivisions(currentInvoice.getGlCompanyNbr());
				
				this.transactionSaleDataModel = new TransactionBillDataMemoryModel();
				this.transactionSaleDataModel
						.setCommonDAO((GenericDAO<BillTransaction, Long>) transactionDetailBillDAO);
				this.transactionSaleDataModel
						.setBillTransactions(this.invoiceLines);

				this.scenarioChanged = false;
				this.transactionsChanged = true;
			}
		}
	}
	
	private void populateDivisions(String val) {
		EntityItem companyEntity = null;
		if (val != null && StringUtils.hasLength(val)) {
			try {
				companyEntity = entityItemService.findByCode(val);
				if (companyEntity != null) {
					long companyId = companyEntity.getEntityId();
					List<EntityItem> list = entityItemService
							.findAllEntityItemsByLevelAndParent(2L, companyId);
					if (list != null) {
						for (EntityItem ei : list) {
							this.divisions.add(new SelectItem(ei
									.getEntityCode(), ei.getEntityCode()
									+ " - " + ei.getEntityName()));
						}
					}
				}
			} catch (Exception ex) {
				logger.error(ex.getMessage(), ex);
			}
		}
	}

	public String getSaveScenarioName() {
		return saveScenarioName;
	}

	public void setSaveScenarioName(String saveScenarioName) {
		this.saveScenarioName = saveScenarioName;
	}

	public boolean getShowDeleteScenario() {
		boolean value = !"*NEW".equals(this.selectedScenarioName)
				&& this.scenarioNames != null && this.scenarioNames.size() > 0;
		return value;
	}

	public boolean isDisplayNewScenarioName() {
		return displayNewScenarioName;
	}

	public void setDisplayNewScenarioName(boolean displayNewScenarioName) {
		this.displayNewScenarioName = displayNewScenarioName;
	}

	public void cancelNewScenarioName() {
		this.displayNewScenarioName = false;
		this.displayScenarioExists = false;
		this.saveScenarioName = null;
	}

	public boolean isDisplayScenarioExists() {
		return displayScenarioExists;
	}

	public void setDisplayScenarioExists(boolean displayScenarioExists) {
		this.displayScenarioExists = displayScenarioExists;
	}

	public boolean getShowSaveScenario() {
		return scenarioChanged;
	}

	public boolean isTransactionsChanged() {
		return transactionsChanged;
	}

	public void setTransactionsChanged(boolean transactionsChanged) {
		this.transactionsChanged = transactionsChanged;
	}

	public void cancelWarning(ActionEvent e) {
		// needed by warning popup
	}

	public boolean isDisplaySaveFirstWarning() {
		return displaySaveFirstWarning;
	}

	public void setDisplaySaveFirstWarning(boolean displaySaveFirstWarning) {
		this.displaySaveFirstWarning = displaySaveFirstWarning;
	}

	public String getNextScenarioName() {
		return nextScenarioName;
	}

	public void setNextScenarioName(String nextScenarioName) {
		this.nextScenarioName = nextScenarioName;
	}

	public String cancelModified() {
		this.displaySaveFirstWarning = false;
		openScenario(nextScenarioName);
		this.selectedScenarioName = this.nextScenarioName;
		this.nextScenarioName = null;
		return "";
	}

	public BillTransaction getSelectedTransaction() {
		return selectedTransaction;
	}

	public void setSelectedTransaction(BillTransaction selectedTransaction) {
		this.selectedTransaction = selectedTransaction;
	}

	public void selectedTransactionChanged (ActionEvent e)
			throws AbortProcessingException {
		UIComponent uiComponent = e.getComponent().getParent();
		UIDataAdaptor table = (UIDataAdaptor) uiComponent;
		selectedTransaction = (BillTransaction) table.getRowData();
		selectedRowIndex = table.getRowIndex();
		
		this.currentInvoice = new BillTransaction();
		BeanUtils.copyProperties(selectedTransaction, this.currentInvoice,
				new String[] { "invoiceLineNbr", "invoiceLineDesc",
						"invoiceLineItemCount", "invoiceLineItemAmt",
						"invoiceLineGrossAmt", "invoiceLineFreightAmt",
						"invoiceLineDiscAmt", "invoiceLineDiscTypeCode",
						"invoiceLineTaxPaidAmt", "invoiceLineDiffAmt",
						"invoiceLineMiscAmt" });
		this.currentInvoice.setBilltransId(null);
		shipToJurisdictionHandler = null;
		shipFromJurisdictionHandler = null;
		ordrorgnJurisdictionHandler = null;
		ordracptJurisdictionHandler = null;
		firstuseJurisdictionHandler = null;
		billtoJurisdictionHandler = null;
		customerJurisdictionHandler = null;
	}

	public int getSelectedRowIndex() {
		return selectedRowIndex;
	}

	public void setSelectedRowIndex(int selectedRowIndex) {
		this.selectedRowIndex = selectedRowIndex;
	}

	public String deleteTransaction() {
		if (this.invoiceLines != null) {
			this.invoiceLines.remove(this.selectedTransaction);
			this.transactionSaleDataModel = null;
			this.transactionsChanged = true;
			this.scenarioChanged = true;
			this.selectedTransaction = null;
			this.selectedRowIndex = -1;
		}
		return "";
	}

	public boolean getDisplayDeleteTransaction() {
		return this.selectedTransaction != null;
	}

	public boolean getDisplayUpdateTransaction() {
		return this.selectedTransaction != null;
	}

	public boolean getDisplayViewTransaction() {
		return this.selectedTransaction != null;
	}

	public String viewTransaction() {
		//PP-394

		if (this.selectedTransaction != null) {
			this.transactionDetailSaleViewBean.setSaleTransactionFilter(this.selectedTransaction);
			
			this.transactionDetailSaleViewBean.fillSelectedTransaction();

			this.transactionDetailSaleViewBean
					.setViewReturnPage("billing_test_tool");
			return "transactions_view";
		}
		return null;
	}

	public boolean getDisplaySaveTransactions() {	
		if (this.invoiceLines != null && this.invoiceLines.size() > 0) {
			Set<Long> selectedIds = null;
			if(this.transactionSaleDataModel != null) {
				selectedIds = this.transactionSaleDataModel.getSelectionMap().keySet();
			}
			
			List<BillTransaction> list = new ArrayList<BillTransaction>();
			if (selectedIds != null && selectedIds.size() > 0) {
				for (BillTransaction t : this.invoiceLines) {
					if (selectedIds.contains(t.getId()) && t.getResult()!=null && (t.getResult().equals(STATUS_PROCESSED) || t.getResult().equals(STATUS_SAVED))) {
						return true;
					}
				}
			} else if(this.selectedTransaction != null && selectedTransaction.getResult()!=null && (selectedTransaction.getResult().equals(STATUS_PROCESSED) || selectedTransaction.getResult().equals(STATUS_SAVED))) {
				return true;
			}
		}
		
		return false;
	}
	
	public void searchJurisdictionCallbackSTJCopy(BillTransaction currentInvoice, String handlerType, SearchJurisdictionHandler fromHandler) {
		switch(handlerType) {
			case "SHIPTO" :
				currentInvoice.setShiptoStj1Name(fromHandler.getStj1Name());
				currentInvoice.setShiptoStj2Name(fromHandler.getStj2Name());
				currentInvoice.setShiptoStj3Name(fromHandler.getStj3Name());
				currentInvoice.setShiptoStj4Name(fromHandler.getStj4Name());
				currentInvoice.setShiptoStj5Name(fromHandler.getStj5Name());
				break;
			case "SHIPFROM" : 
				currentInvoice.setShipfromStj1Name(fromHandler.getStj1Name());
				currentInvoice.setShipfromStj2Name(fromHandler.getStj2Name());
				currentInvoice.setShipfromStj3Name(fromHandler.getStj3Name());
				currentInvoice.setShipfromStj4Name(fromHandler.getStj4Name());
				currentInvoice.setShipfromStj5Name(fromHandler.getStj5Name());
				break;
			case "ORDRORGN" : 
				currentInvoice.setOrdrorgnStj1Name(fromHandler.getStj1Name());
				currentInvoice.setOrdrorgnStj2Name(fromHandler.getStj2Name());
				currentInvoice.setOrdrorgnStj3Name(fromHandler.getStj3Name());
				currentInvoice.setOrdrorgnStj4Name(fromHandler.getStj4Name());
				currentInvoice.setOrdrorgnStj5Name(fromHandler.getStj5Name());
				break;
			case "ORDRACPT" : 
				currentInvoice.setOrdracptStj1Name(fromHandler.getStj1Name());
				currentInvoice.setOrdracptStj2Name(fromHandler.getStj2Name());
				currentInvoice.setOrdracptStj3Name(fromHandler.getStj3Name());
				currentInvoice.setOrdracptStj4Name(fromHandler.getStj4Name());
				currentInvoice.setOrdracptStj5Name(fromHandler.getStj5Name());
				break;
			case "FIRSTUSE" : 
				currentInvoice.setFirstuseStj1Name(fromHandler.getStj1Name());
				currentInvoice.setFirstuseStj2Name(fromHandler.getStj2Name());
				currentInvoice.setFirstuseStj3Name(fromHandler.getStj3Name());
				currentInvoice.setFirstuseStj4Name(fromHandler.getStj4Name());
				currentInvoice.setFirstuseStj5Name(fromHandler.getStj5Name());
				break;
			case "BILLTO" : 
				currentInvoice.setBilltoStj1Name(fromHandler.getStj1Name());
				currentInvoice.setBilltoStj2Name(fromHandler.getStj2Name());
				currentInvoice.setBilltoStj3Name(fromHandler.getStj3Name());
				currentInvoice.setBilltoStj4Name(fromHandler.getStj4Name());
				currentInvoice.setBilltoStj5Name(fromHandler.getStj5Name());
				break;
			default : 
				logger.error("Invalid Handler Type: " + handlerType);
				break;
				
		}
	}

	public void searchJurisdictionCallbackExt(
			SearchJurisdictionHandler searchJurisdictionHandler) {
		if (searchJurisdictionHandler == shipToJurisdictionHandler) {
			if(shipToJurisdictionHandler.getJurisdictionId() != null && !shipToJurisdictionHandler.getJurisdictionId().equals("") )
				currentInvoice.setShiptoJurisdictionId(Long.parseLong(shipToJurisdictionHandler
						.getJurisdictionId()));
			currentInvoice.setShiptoGeocode(shipToJurisdictionHandler
					.getGeocode());
			currentInvoice.setShiptoCity(shipToJurisdictionHandler.getCity());
			currentInvoice.setShiptoCounty(shipToJurisdictionHandler
					.getCounty());
			currentInvoice.setShiptoStateCode(shipToJurisdictionHandler
					.getState());
			currentInvoice.setShiptoCountryCode(shipToJurisdictionHandler
					.getCountry());
			currentInvoice.setShiptoZip(shipToJurisdictionHandler.getZip());
			currentInvoice.setShiptoZipplus4(shipToJurisdictionHandler
					.getZipPlus4());
			searchJurisdictionCallbackSTJCopy(currentInvoice, "SHIPTO", searchJurisdictionHandler);
		} else if (searchJurisdictionHandler == shipFromJurisdictionHandler) {
			if(shipFromJurisdictionHandler.getJurisdictionId() != null && !shipFromJurisdictionHandler.getJurisdictionId().equals(""))
				currentInvoice.setShipfromJurisdictionId(Long.parseLong(shipFromJurisdictionHandler
							.getJurisdictionId()));
			currentInvoice.setShipfromGeocode(shipFromJurisdictionHandler
					.getGeocode());
			currentInvoice.setShipfromCity(shipFromJurisdictionHandler
					.getCity());
			currentInvoice.setShipfromCounty(shipFromJurisdictionHandler
					.getCounty());
			currentInvoice.setShipfromStateCode(shipFromJurisdictionHandler
					.getState());
			currentInvoice.setShipfromCountryCode(shipFromJurisdictionHandler
					.getCountry());
			currentInvoice.setShipfromZip(shipFromJurisdictionHandler.getZip());
			currentInvoice.setShipfromZipplus4(shipFromJurisdictionHandler
					.getZipPlus4());
			searchJurisdictionCallbackSTJCopy(currentInvoice, "SHIPFROM", searchJurisdictionHandler);
		} else if (searchJurisdictionHandler == ordrorgnJurisdictionHandler) {
			if(ordrorgnJurisdictionHandler.getJurisdictionId() != null && !ordrorgnJurisdictionHandler.getJurisdictionId().equals(""))
			currentInvoice.setOrdrorgnJurisdictionId(Long.parseLong(ordrorgnJurisdictionHandler
							.getJurisdictionId()));
			currentInvoice.setOrdrorgnGeocode(ordrorgnJurisdictionHandler
					.getGeocode());
			currentInvoice.setOrdrorgnCity(ordrorgnJurisdictionHandler
					.getCity());
			currentInvoice.setOrdrorgnCounty(ordrorgnJurisdictionHandler
					.getCounty());
			currentInvoice.setOrdrorgnStateCode(ordrorgnJurisdictionHandler
					.getState());
			currentInvoice.setOrdrorgnCountryCode(ordrorgnJurisdictionHandler
					.getCountry());
			currentInvoice.setOrdrorgnZip(ordrorgnJurisdictionHandler.getZip());
			currentInvoice.setOrdrorgnZipplus4(ordrorgnJurisdictionHandler
					.getZipPlus4());
			searchJurisdictionCallbackSTJCopy(currentInvoice, "ORDRORGN", searchJurisdictionHandler);
		} else if (searchJurisdictionHandler == ordracptJurisdictionHandler) {
			if(ordracptJurisdictionHandler.getJurisdictionId() != null && !ordracptJurisdictionHandler.getJurisdictionId().equals(""))
			currentInvoice.setOrdracptJurisdictionId(Long.parseLong(ordracptJurisdictionHandler
							.getJurisdictionId()));
			currentInvoice.setOrdracptGeocode(ordracptJurisdictionHandler
					.getGeocode());
			currentInvoice.setOrdracptCity(ordracptJurisdictionHandler
					.getCity());
			currentInvoice.setOrdracptCounty(ordracptJurisdictionHandler
					.getCounty());
			currentInvoice.setOrdracptStateCode(ordracptJurisdictionHandler
					.getState());
			currentInvoice.setOrdracptCountryCode(ordracptJurisdictionHandler
					.getCountry());
			currentInvoice.setOrdracptZip(ordracptJurisdictionHandler.getZip());
			currentInvoice.setOrdracptZipplus4(ordracptJurisdictionHandler
					.getZipPlus4());
			searchJurisdictionCallbackSTJCopy(currentInvoice, "ORDRACPT", shipToJurisdictionHandler);
		} else if (searchJurisdictionHandler == firstuseJurisdictionHandler) {
			if(firstuseJurisdictionHandler.getJurisdictionId() != null && !firstuseJurisdictionHandler.getJurisdictionId().equals(""))
			currentInvoice.setFirstuseJurisdictionId(Long.parseLong(firstuseJurisdictionHandler
							.getJurisdictionId()));
			currentInvoice.setFirstuseGeocode(firstuseJurisdictionHandler
					.getGeocode());
			currentInvoice.setFirstuseCity(firstuseJurisdictionHandler
					.getCity());
			currentInvoice.setFirstuseCounty(firstuseJurisdictionHandler
					.getCounty());
			currentInvoice.setFirstuseStateCode(firstuseJurisdictionHandler
					.getState());
			currentInvoice.setFirstuseCountryCode(firstuseJurisdictionHandler
					.getCountry());
			currentInvoice.setFirstuseZip(firstuseJurisdictionHandler.getZip());
			currentInvoice.setFirstuseZipplus4(firstuseJurisdictionHandler
					.getZipPlus4());
			searchJurisdictionCallbackSTJCopy(currentInvoice, "FIRSTUSE", searchJurisdictionHandler);
		} else if (searchJurisdictionHandler == billtoJurisdictionHandler) {
			if(billtoJurisdictionHandler.getJurisdictionId() != null && !billtoJurisdictionHandler.getJurisdictionId().equals(""))
			currentInvoice.setBilltoJurisdictionId(Long.parseLong(billtoJurisdictionHandler
					.getJurisdictionId()));
			currentInvoice.setBilltoGeocode(billtoJurisdictionHandler
					.getGeocode());
			currentInvoice.setBilltoCity(billtoJurisdictionHandler.getCity());
			currentInvoice.setBilltoCounty(billtoJurisdictionHandler
					.getCounty());
			currentInvoice.setBilltoStateCode(billtoJurisdictionHandler
					.getState());
			currentInvoice.setBilltoCountryCode(billtoJurisdictionHandler
					.getCountry());
			currentInvoice.setBilltoZip(billtoJurisdictionHandler.getZip());
			currentInvoice.setBilltoZipplus4(billtoJurisdictionHandler
					.getZipPlus4());
			searchJurisdictionCallbackSTJCopy(currentInvoice, "BILLTO", searchJurisdictionHandler);
		} else if (searchJurisdictionHandler == customerJurisdictionHandler) {
			if(customerJurisdictionHandler.getJurisdictionId() != null && !customerJurisdictionHandler.getJurisdictionId().equals(""))
			currentInvoice.setCustJurisdictionId(Long.parseLong(customerJurisdictionHandler
					.getJurisdictionId()));
			currentInvoice.setCustGeocode(customerJurisdictionHandler
					.getGeocode());
			currentInvoice.setCustCity(customerJurisdictionHandler.getCity());
			currentInvoice.setCustCounty(customerJurisdictionHandler
					.getCounty());
			currentInvoice.setCustStateCode(customerJurisdictionHandler
					.getState());
			currentInvoice.setCustCountryCode(customerJurisdictionHandler
					.getCountry());
			currentInvoice.setCustZip(customerJurisdictionHandler.getZip());
			currentInvoice.setCustZipplus4(customerJurisdictionHandler
					.getZipPlus4());
		}
		
		//For Invoice Line
		else if (searchJurisdictionHandler == shipToJurisdictionHandlerLine) {
			if(shipToJurisdictionHandlerLine.getJurisdictionId() != null && !shipToJurisdictionHandlerLine.getJurisdictionId().equals(""))
			currentInvoiceLine.setShiptoJurisdictionId(Long.parseLong(shipToJurisdictionHandlerLine
					.getJurisdictionId()));
			currentInvoiceLine.setShiptoGeocode(shipToJurisdictionHandlerLine
					.getGeocode());
			currentInvoiceLine.setShiptoCity(shipToJurisdictionHandlerLine.getCity());
			currentInvoiceLine.setShiptoCounty(shipToJurisdictionHandlerLine
					.getCounty());
			currentInvoiceLine.setShiptoStateCode(shipToJurisdictionHandlerLine
					.getState());
			currentInvoiceLine.setShiptoCountryCode(shipToJurisdictionHandlerLine
					.getCountry());
			currentInvoiceLine.setShiptoZip(shipToJurisdictionHandlerLine.getZip());
			currentInvoiceLine.setShiptoZipplus4(shipToJurisdictionHandlerLine
					.getZipPlus4());
			searchJurisdictionCallbackSTJCopy(currentInvoiceLine, "SHIPTO", searchJurisdictionHandler);
		} else if (searchJurisdictionHandler == shipFromJurisdictionHandlerLine) {
			if(shipFromJurisdictionHandlerLine.getJurisdictionId() != null && !shipFromJurisdictionHandlerLine.getJurisdictionId().equals(""))
			currentInvoiceLine
					.setShipfromJurisdictionId(Long.parseLong(shipFromJurisdictionHandlerLine
							.getJurisdictionId()));
			currentInvoiceLine.setShipfromGeocode(shipFromJurisdictionHandlerLine
					.getGeocode());
			currentInvoiceLine.setShipfromCity(shipFromJurisdictionHandlerLine
					.getCity());
			currentInvoiceLine.setShipfromCounty(shipFromJurisdictionHandlerLine
					.getCounty());
			currentInvoiceLine.setShipfromStateCode(shipFromJurisdictionHandlerLine
					.getState());
			currentInvoiceLine.setShipfromCountryCode(shipFromJurisdictionHandlerLine
					.getCountry());
			currentInvoiceLine.setShipfromZip(shipFromJurisdictionHandlerLine.getZip());
			currentInvoiceLine.setShipfromZipplus4(shipFromJurisdictionHandlerLine
					.getZipPlus4());
			searchJurisdictionCallbackSTJCopy(currentInvoiceLine, "SHIPFROM", searchJurisdictionHandler);
		} else if (searchJurisdictionHandler == ordrorgnJurisdictionHandlerLine) {
			if(ordrorgnJurisdictionHandlerLine.getJurisdictionId() != null && !ordrorgnJurisdictionHandlerLine.getJurisdictionId().equals(""))
			currentInvoiceLine
					.setOrdrorgnJurisdictionId(Long.parseLong(ordrorgnJurisdictionHandlerLine
							.getJurisdictionId()));
			currentInvoiceLine.setOrdrorgnGeocode(ordrorgnJurisdictionHandlerLine
					.getGeocode());
			currentInvoiceLine.setOrdrorgnCity(ordrorgnJurisdictionHandlerLine
					.getCity());
			currentInvoiceLine.setOrdrorgnCounty(ordrorgnJurisdictionHandlerLine
					.getCounty());
			currentInvoiceLine.setOrdrorgnStateCode(ordrorgnJurisdictionHandlerLine
					.getState());
			currentInvoiceLine.setOrdrorgnCountryCode(ordrorgnJurisdictionHandlerLine
					.getCountry());
			currentInvoiceLine.setOrdrorgnZip(ordrorgnJurisdictionHandlerLine.getZip());
			currentInvoiceLine.setOrdrorgnZipplus4(ordrorgnJurisdictionHandlerLine
					.getZipPlus4());
			searchJurisdictionCallbackSTJCopy(currentInvoiceLine, "ORDRORGN", searchJurisdictionHandler);
		} else if (searchJurisdictionHandler == ordracptJurisdictionHandlerLine) {
			if(ordracptJurisdictionHandlerLine.getJurisdictionId() != null && !ordracptJurisdictionHandlerLine.getJurisdictionId().equals(""))
			currentInvoiceLine
					.setOrdracptJurisdictionId(Long.parseLong(ordracptJurisdictionHandlerLine
							.getJurisdictionId()));
			currentInvoiceLine.setOrdracptGeocode(ordracptJurisdictionHandlerLine
					.getGeocode());
			currentInvoiceLine.setOrdracptCity(ordracptJurisdictionHandlerLine
					.getCity());
			currentInvoiceLine.setOrdracptCounty(ordracptJurisdictionHandlerLine
					.getCounty());
			currentInvoiceLine.setOrdracptStateCode(ordracptJurisdictionHandlerLine
					.getState());
			currentInvoiceLine.setOrdracptCountryCode(ordracptJurisdictionHandlerLine
					.getCountry());
			currentInvoiceLine.setOrdracptZip(ordracptJurisdictionHandlerLine.getZip());
			currentInvoiceLine.setOrdracptZipplus4(ordracptJurisdictionHandlerLine
					.getZipPlus4());
			searchJurisdictionCallbackSTJCopy(currentInvoiceLine, "ORDRACPT", searchJurisdictionHandler);
		} else if (searchJurisdictionHandler == firstuseJurisdictionHandlerLine) {
			if(firstuseJurisdictionHandlerLine.getJurisdictionId() != null && !firstuseJurisdictionHandlerLine.getJurisdictionId().equals(""))
			currentInvoiceLine
					.setFirstuseJurisdictionId(Long.parseLong(firstuseJurisdictionHandlerLine
							.getJurisdictionId()));
			currentInvoiceLine.setFirstuseGeocode(firstuseJurisdictionHandlerLine
					.getGeocode());
			currentInvoiceLine.setFirstuseCity(firstuseJurisdictionHandlerLine
					.getCity());
			currentInvoiceLine.setFirstuseCounty(firstuseJurisdictionHandlerLine
					.getCounty());
			currentInvoiceLine.setFirstuseStateCode(firstuseJurisdictionHandlerLine
					.getState());
			currentInvoiceLine.setFirstuseCountryCode(firstuseJurisdictionHandlerLine
					.getCountry());
			currentInvoiceLine.setFirstuseZip(firstuseJurisdictionHandlerLine.getZip());
			currentInvoiceLine.setFirstuseZipplus4(firstuseJurisdictionHandlerLine
					.getZipPlus4());
			searchJurisdictionCallbackSTJCopy(currentInvoiceLine, "FIRSTUSE", searchJurisdictionHandler);
		} else if (searchJurisdictionHandler == billtoJurisdictionHandlerLine) {
			if(billtoJurisdictionHandlerLine.getJurisdictionId() != null && !billtoJurisdictionHandlerLine.getJurisdictionId().equals(""))
			currentInvoiceLine.setBilltoJurisdictionId(Long.parseLong(billtoJurisdictionHandlerLine
					.getJurisdictionId()));
			currentInvoiceLine.setBilltoGeocode(billtoJurisdictionHandlerLine
					.getGeocode());
			currentInvoiceLine.setBilltoCity(billtoJurisdictionHandlerLine.getCity());
			currentInvoiceLine.setBilltoCounty(billtoJurisdictionHandlerLine
					.getCounty());
			currentInvoiceLine.setBilltoStateCode(billtoJurisdictionHandlerLine
					.getState());
			currentInvoiceLine.setBilltoCountryCode(billtoJurisdictionHandlerLine
					.getCountry());
			currentInvoiceLine.setBilltoZip(billtoJurisdictionHandlerLine.getZip());
			currentInvoiceLine.setBilltoZipplus4(billtoJurisdictionHandlerLine
					.getZipPlus4());
			searchJurisdictionCallbackSTJCopy(currentInvoiceLine, "BILLTO", searchJurisdictionHandler);
		} else if (searchJurisdictionHandler == customerJurisdictionHandlerLine) {
			if(customerJurisdictionHandlerLine.getJurisdictionId() != null && !customerJurisdictionHandlerLine.getJurisdictionId().equals(""))
			currentInvoiceLine.setCustJurisdictionId(Long.parseLong(customerJurisdictionHandlerLine
					.getJurisdictionId()));
			currentInvoiceLine.setCustGeocode(customerJurisdictionHandlerLine
					.getGeocode());
			currentInvoiceLine.setCustCity(customerJurisdictionHandlerLine.getCity());
			currentInvoiceLine.setCustCounty(customerJurisdictionHandlerLine
					.getCounty());
			currentInvoiceLine.setCustStateCode(customerJurisdictionHandlerLine
					.getState());
			currentInvoiceLine.setCustCountryCode(customerJurisdictionHandlerLine
					.getCountry());
			currentInvoiceLine.setCustZip(customerJurisdictionHandlerLine.getZip());
			currentInvoiceLine.setCustZipplus4(customerJurisdictionHandlerLine
					.getZipPlus4());
		}
	}

	public void searchJurisdictionCallbackLocation(
			SearchJurisdictionHandler searchJurisdictionHandler,
			String componentName) {
		if (searchJurisdictionHandler == shipToJurisdictionHandler) {
			if (SearchJurisdictionHandler.GEOCODE.equals(componentName)) {
				currentInvoice.setShiptoGeocode(shipToJurisdictionHandler
						.getGeocode());
			}
			if (SearchJurisdictionHandler.CITY.equals(componentName)) {
				currentInvoice.setShiptoCity(shipToJurisdictionHandler
						.getCity());
			}
			if (SearchJurisdictionHandler.COUNTRY.equals(componentName)) {
				currentInvoice.setShiptoCounty(shipToJurisdictionHandler
						.getCounty());
			}
			if (SearchJurisdictionHandler.STATE.equals(componentName)) {
				currentInvoice.setShiptoStateCode(shipToJurisdictionHandler
						.getState());
			}
			if (SearchJurisdictionHandler.COUNTRY.equals(componentName)) {
				currentInvoice.setShiptoCountryCode(shipToJurisdictionHandler
						.getCountry());
			}
			if (SearchJurisdictionHandler.ZIPCODE.equals(componentName)) {
				currentInvoice.setShiptoZip(shipToJurisdictionHandler.getZip());
			}
			if (SearchJurisdictionHandler.PLUS4.equals(componentName)) {
				currentInvoice.setShiptoZipplus4(shipToJurisdictionHandler
						.getZipPlus4());
			}
		} else if (searchJurisdictionHandler == shipFromJurisdictionHandler) {
			if (SearchJurisdictionHandler.GEOCODE.equals(componentName)) {
				currentInvoice.setShipfromGeocode(shipFromJurisdictionHandler
						.getGeocode());
			}
			if (SearchJurisdictionHandler.CITY.equals(componentName)) {
				currentInvoice.setShipfromCity(shipFromJurisdictionHandler
						.getCity());
			}
			if (SearchJurisdictionHandler.COUNTRY.equals(componentName)) {
				currentInvoice.setShipfromCounty(shipFromJurisdictionHandler
						.getCounty());
			}
			if (SearchJurisdictionHandler.STATE.equals(componentName)) {
				currentInvoice.setShipfromStateCode(shipFromJurisdictionHandler
						.getState());
			}
			if (SearchJurisdictionHandler.COUNTRY.equals(componentName)) {
				currentInvoice
						.setShipfromCountryCode(shipFromJurisdictionHandler
								.getCountry());
			}
			if (SearchJurisdictionHandler.ZIPCODE.equals(componentName)) {
				currentInvoice.setShipfromZip(shipFromJurisdictionHandler
						.getZip());
			}
			if (SearchJurisdictionHandler.PLUS4.equals(componentName)) {
				currentInvoice.setShipfromZipplus4(shipFromJurisdictionHandler
						.getZipPlus4());
			}
		} else if (searchJurisdictionHandler == ordrorgnJurisdictionHandler) {
			if (SearchJurisdictionHandler.GEOCODE.equals(componentName)) {
				currentInvoice.setOrdrorgnGeocode(ordrorgnJurisdictionHandler
						.getGeocode());
			}
			if (SearchJurisdictionHandler.CITY.equals(componentName)) {
				currentInvoice.setOrdrorgnCity(ordrorgnJurisdictionHandler
						.getCity());
			}
			if (SearchJurisdictionHandler.COUNTRY.equals(componentName)) {
				currentInvoice.setOrdrorgnCounty(ordrorgnJurisdictionHandler
						.getCounty());
			}
			if (SearchJurisdictionHandler.STATE.equals(componentName)) {
				currentInvoice.setOrdrorgnStateCode(ordrorgnJurisdictionHandler
						.getState());
			}
			if (SearchJurisdictionHandler.COUNTRY.equals(componentName)) {
				currentInvoice
						.setOrdrorgnCountryCode(ordrorgnJurisdictionHandler
								.getCountry());
			}
			if (SearchJurisdictionHandler.ZIPCODE.equals(componentName)) {
				currentInvoice.setOrdrorgnZip(ordrorgnJurisdictionHandler
						.getZip());
			}
			if (SearchJurisdictionHandler.PLUS4.equals(componentName)) {
				currentInvoice.setOrdrorgnZipplus4(ordrorgnJurisdictionHandler
						.getZipPlus4());
			}
		} else if (searchJurisdictionHandler == ordracptJurisdictionHandler) {
			if (SearchJurisdictionHandler.GEOCODE.equals(componentName)) {
				currentInvoice.setOrdracptGeocode(ordracptJurisdictionHandler
						.getGeocode());
			}
			if (SearchJurisdictionHandler.CITY.equals(componentName)) {
				currentInvoice.setOrdracptCity(ordracptJurisdictionHandler
						.getCity());
			}
			if (SearchJurisdictionHandler.COUNTRY.equals(componentName)) {
				currentInvoice.setOrdracptCounty(ordracptJurisdictionHandler
						.getCounty());
			}
			if (SearchJurisdictionHandler.STATE.equals(componentName)) {
				currentInvoice.setOrdracptStateCode(ordracptJurisdictionHandler
						.getState());
			}
			if (SearchJurisdictionHandler.COUNTRY.equals(componentName)) {
				currentInvoice
						.setOrdracptCountryCode(ordracptJurisdictionHandler
								.getCountry());
			}
			if (SearchJurisdictionHandler.ZIPCODE.equals(componentName)) {
				currentInvoice.setOrdracptZip(ordracptJurisdictionHandler
						.getZip());
			}
			if (SearchJurisdictionHandler.PLUS4.equals(componentName)) {
				currentInvoice.setOrdracptZipplus4(ordracptJurisdictionHandler
						.getZipPlus4());
			}
		} else if (searchJurisdictionHandler == firstuseJurisdictionHandler) {
			if (SearchJurisdictionHandler.GEOCODE.equals(componentName)) {
				currentInvoice.setOrdracptGeocode(firstuseJurisdictionHandler
						.getGeocode());
			}
			if (SearchJurisdictionHandler.CITY.equals(componentName)) {
				currentInvoice.setOrdracptCity(firstuseJurisdictionHandler
						.getCity());
			}
			if (SearchJurisdictionHandler.COUNTRY.equals(componentName)) {
				currentInvoice.setOrdracptCounty(firstuseJurisdictionHandler
						.getCounty());
			}
			if (SearchJurisdictionHandler.STATE.equals(componentName)) {
				currentInvoice.setOrdracptStateCode(firstuseJurisdictionHandler
						.getState());
			}
			if (SearchJurisdictionHandler.COUNTRY.equals(componentName)) {
				currentInvoice
						.setOrdracptCountryCode(firstuseJurisdictionHandler
								.getCountry());
			}
			if (SearchJurisdictionHandler.ZIPCODE.equals(componentName)) {
				currentInvoice.setOrdracptZip(firstuseJurisdictionHandler
						.getZip());
			}
			if (SearchJurisdictionHandler.PLUS4.equals(componentName)) {
				currentInvoice.setOrdracptZipplus4(firstuseJurisdictionHandler
						.getZipPlus4());
			}
		} else if (searchJurisdictionHandler == billtoJurisdictionHandler) {
			if (SearchJurisdictionHandler.GEOCODE.equals(componentName)) {
				currentInvoice.setBilltoGeocode(billtoJurisdictionHandler
						.getGeocode());
			}
			if (SearchJurisdictionHandler.CITY.equals(componentName)) {
				currentInvoice.setBilltoCity(billtoJurisdictionHandler
						.getCity());
			}
			if (SearchJurisdictionHandler.COUNTRY.equals(componentName)) {
				currentInvoice.setBilltoCounty(billtoJurisdictionHandler
						.getCounty());
			}
			if (SearchJurisdictionHandler.STATE.equals(componentName)) {
				currentInvoice.setBilltoStateCode(billtoJurisdictionHandler
						.getState());
			}
			if (SearchJurisdictionHandler.COUNTRY.equals(componentName)) {
				currentInvoice.setBilltoCountryCode(billtoJurisdictionHandler
						.getCountry());
			}
			if (SearchJurisdictionHandler.ZIPCODE.equals(componentName)) {
				currentInvoice.setBilltoZip(billtoJurisdictionHandler.getZip());
			}
			if (SearchJurisdictionHandler.PLUS4.equals(componentName)) {
				currentInvoice.setBilltoZipplus4(billtoJurisdictionHandler
						.getZipPlus4());
			}
		} else if (searchJurisdictionHandler == customerJurisdictionHandler) {
			if (SearchJurisdictionHandler.GEOCODE.equals(componentName)) {
				currentInvoice.setCustGeocode(customerJurisdictionHandler
						.getGeocode());
			}
			if (SearchJurisdictionHandler.CITY.equals(componentName)) {
				currentInvoice.setCustCity(customerJurisdictionHandler
						.getCity());
			}
			if (SearchJurisdictionHandler.COUNTRY.equals(componentName)) {
				currentInvoice.setCustCounty(customerJurisdictionHandler
						.getCounty());
			}
			if (SearchJurisdictionHandler.STATE.equals(componentName)) {
				currentInvoice.setCustStateCode(customerJurisdictionHandler
						.getState());
			}
			if (SearchJurisdictionHandler.COUNTRY.equals(componentName)) {
				currentInvoice.setCustCountryCode(customerJurisdictionHandler
						.getCountry());
			}
			if (SearchJurisdictionHandler.ZIPCODE.equals(componentName)) {
				currentInvoice.setCustZip(customerJurisdictionHandler.getZip());
			}
			if (SearchJurisdictionHandler.PLUS4.equals(componentName)) {
				currentInvoice.setCustZipplus4(customerJurisdictionHandler
						.getZipPlus4());
			}
		}
		//For invoice line
		else if (searchJurisdictionHandler == shipToJurisdictionHandlerLine) {
			if (SearchJurisdictionHandler.GEOCODE.equals(componentName)) {
				currentInvoice.setShiptoGeocode(shipToJurisdictionHandlerLine
						.getGeocode());
			}
			if (SearchJurisdictionHandler.CITY.equals(componentName)) {
				currentInvoice.setShiptoCity(shipToJurisdictionHandlerLine
						.getCity());
			}
			if (SearchJurisdictionHandler.COUNTRY.equals(componentName)) {
				currentInvoice.setShiptoCounty(shipToJurisdictionHandlerLine
						.getCounty());
			}
			if (SearchJurisdictionHandler.STATE.equals(componentName)) {
				currentInvoice.setShiptoStateCode(shipToJurisdictionHandlerLine
						.getState());
			}
			if (SearchJurisdictionHandler.COUNTRY.equals(componentName)) {
				currentInvoice.setShiptoCountryCode(shipToJurisdictionHandlerLine
						.getCountry());
			}
			if (SearchJurisdictionHandler.ZIPCODE.equals(componentName)) {
				currentInvoice.setShiptoZip(shipToJurisdictionHandlerLine.getZip());
			}
			if (SearchJurisdictionHandler.PLUS4.equals(componentName)) {
				currentInvoice.setShiptoZipplus4(shipToJurisdictionHandlerLine
						.getZipPlus4());
			}
		} else if (searchJurisdictionHandler == shipFromJurisdictionHandlerLine) {
			if (SearchJurisdictionHandler.GEOCODE.equals(componentName)) {
				currentInvoice.setShipfromGeocode(shipFromJurisdictionHandlerLine
						.getGeocode());
			}
			if (SearchJurisdictionHandler.CITY.equals(componentName)) {
				currentInvoice.setShipfromCity(shipFromJurisdictionHandlerLine
						.getCity());
			}
			if (SearchJurisdictionHandler.COUNTRY.equals(componentName)) {
				currentInvoice.setShipfromCounty(shipFromJurisdictionHandlerLine
						.getCounty());
			}
			if (SearchJurisdictionHandler.STATE.equals(componentName)) {
				currentInvoice.setShipfromStateCode(shipFromJurisdictionHandlerLine
						.getState());
			}
			if (SearchJurisdictionHandler.COUNTRY.equals(componentName)) {
				currentInvoice
						.setShipfromCountryCode(shipFromJurisdictionHandlerLine
								.getCountry());
			}
			if (SearchJurisdictionHandler.ZIPCODE.equals(componentName)) {
				currentInvoice.setShipfromZip(shipFromJurisdictionHandlerLine
						.getZip());
			}
			if (SearchJurisdictionHandler.PLUS4.equals(componentName)) {
				currentInvoice.setShipfromZipplus4(shipFromJurisdictionHandlerLine
						.getZipPlus4());
			}
		} else if (searchJurisdictionHandler == ordrorgnJurisdictionHandlerLine) {
			if (SearchJurisdictionHandler.GEOCODE.equals(componentName)) {
				currentInvoice.setOrdrorgnGeocode(ordrorgnJurisdictionHandlerLine
						.getGeocode());
			}
			if (SearchJurisdictionHandler.CITY.equals(componentName)) {
				currentInvoice.setOrdrorgnCity(ordrorgnJurisdictionHandlerLine
						.getCity());
			}
			if (SearchJurisdictionHandler.COUNTRY.equals(componentName)) {
				currentInvoice.setOrdrorgnCounty(ordrorgnJurisdictionHandlerLine
						.getCounty());
			}
			if (SearchJurisdictionHandler.STATE.equals(componentName)) {
				currentInvoice.setOrdrorgnStateCode(ordrorgnJurisdictionHandlerLine
						.getState());
			}
			if (SearchJurisdictionHandler.COUNTRY.equals(componentName)) {
				currentInvoice
						.setOrdrorgnCountryCode(ordrorgnJurisdictionHandlerLine
								.getCountry());
			}
			if (SearchJurisdictionHandler.ZIPCODE.equals(componentName)) {
				currentInvoice.setOrdrorgnZip(ordrorgnJurisdictionHandlerLine
						.getZip());
			}
			if (SearchJurisdictionHandler.PLUS4.equals(componentName)) {
				currentInvoice.setOrdrorgnZipplus4(ordrorgnJurisdictionHandlerLine
						.getZipPlus4());
			}
		} else if (searchJurisdictionHandler == ordracptJurisdictionHandlerLine) {
			if (SearchJurisdictionHandler.GEOCODE.equals(componentName)) {
				currentInvoice.setOrdracptGeocode(ordracptJurisdictionHandlerLine
						.getGeocode());
			}
			if (SearchJurisdictionHandler.CITY.equals(componentName)) {
				currentInvoice.setOrdracptCity(ordracptJurisdictionHandlerLine
						.getCity());
			}
			if (SearchJurisdictionHandler.COUNTRY.equals(componentName)) {
				currentInvoice.setOrdracptCounty(ordracptJurisdictionHandlerLine
						.getCounty());
			}
			if (SearchJurisdictionHandler.STATE.equals(componentName)) {
				currentInvoice.setOrdracptStateCode(ordracptJurisdictionHandlerLine
						.getState());
			}
			if (SearchJurisdictionHandler.COUNTRY.equals(componentName)) {
				currentInvoice
						.setOrdracptCountryCode(ordracptJurisdictionHandlerLine
								.getCountry());
			}
			if (SearchJurisdictionHandler.ZIPCODE.equals(componentName)) {
				currentInvoice.setOrdracptZip(ordracptJurisdictionHandlerLine
						.getZip());
			}
			if (SearchJurisdictionHandler.PLUS4.equals(componentName)) {
				currentInvoice.setOrdracptZipplus4(ordracptJurisdictionHandlerLine
						.getZipPlus4());
			}
		} else if (searchJurisdictionHandler == firstuseJurisdictionHandlerLine) {
			if (SearchJurisdictionHandler.GEOCODE.equals(componentName)) {
				currentInvoice.setOrdracptGeocode(firstuseJurisdictionHandlerLine
						.getGeocode());
			}
			if (SearchJurisdictionHandler.CITY.equals(componentName)) {
				currentInvoice.setOrdracptCity(firstuseJurisdictionHandlerLine
						.getCity());
			}
			if (SearchJurisdictionHandler.COUNTRY.equals(componentName)) {
				currentInvoice.setOrdracptCounty(firstuseJurisdictionHandlerLine
						.getCounty());
			}
			if (SearchJurisdictionHandler.STATE.equals(componentName)) {
				currentInvoice.setOrdracptStateCode(firstuseJurisdictionHandlerLine
						.getState());
			}
			if (SearchJurisdictionHandler.COUNTRY.equals(componentName)) {
				currentInvoice
						.setOrdracptCountryCode(firstuseJurisdictionHandlerLine
								.getCountry());
			}
			if (SearchJurisdictionHandler.ZIPCODE.equals(componentName)) {
				currentInvoice.setOrdracptZip(firstuseJurisdictionHandlerLine
						.getZip());
			}
			if (SearchJurisdictionHandler.PLUS4.equals(componentName)) {
				currentInvoice.setOrdracptZipplus4(firstuseJurisdictionHandlerLine
						.getZipPlus4());
			}
		} else if (searchJurisdictionHandler == billtoJurisdictionHandlerLine) {
			if (SearchJurisdictionHandler.GEOCODE.equals(componentName)) {
				currentInvoice.setBilltoGeocode(billtoJurisdictionHandlerLine
						.getGeocode());
			}
			if (SearchJurisdictionHandler.CITY.equals(componentName)) {
				currentInvoice.setBilltoCity(billtoJurisdictionHandlerLine
						.getCity());
			}
			if (SearchJurisdictionHandler.COUNTRY.equals(componentName)) {
				currentInvoice.setBilltoCounty(billtoJurisdictionHandlerLine
						.getCounty());
			}
			if (SearchJurisdictionHandler.STATE.equals(componentName)) {
				currentInvoice.setBilltoStateCode(billtoJurisdictionHandlerLine
						.getState());
			}
			if (SearchJurisdictionHandler.COUNTRY.equals(componentName)) {
				currentInvoice.setBilltoCountryCode(billtoJurisdictionHandlerLine
						.getCountry());
			}
			if (SearchJurisdictionHandler.ZIPCODE.equals(componentName)) {
				currentInvoice.setBilltoZip(billtoJurisdictionHandlerLine.getZip());
			}
			if (SearchJurisdictionHandler.PLUS4.equals(componentName)) {
				currentInvoice.setBilltoZipplus4(billtoJurisdictionHandlerLine
						.getZipPlus4());
			}
		} else if (searchJurisdictionHandler == customerJurisdictionHandlerLine) {
			if (SearchJurisdictionHandler.GEOCODE.equals(componentName)) {
				currentInvoice.setCustGeocode(customerJurisdictionHandlerLine
						.getGeocode());
			}
			if (SearchJurisdictionHandler.CITY.equals(componentName)) {
				currentInvoice.setCustCity(customerJurisdictionHandlerLine
						.getCity());
			}
			if (SearchJurisdictionHandler.COUNTRY.equals(componentName)) {
				currentInvoice.setCustCounty(customerJurisdictionHandlerLine
						.getCounty());
			}
			if (SearchJurisdictionHandler.STATE.equals(componentName)) {
				currentInvoice.setCustStateCode(customerJurisdictionHandlerLine
						.getState());
			}
			if (SearchJurisdictionHandler.COUNTRY.equals(componentName)) {
				currentInvoice.setCustCountryCode(customerJurisdictionHandlerLine
						.getCountry());
			}
			if (SearchJurisdictionHandler.ZIPCODE.equals(componentName)) {
				currentInvoice.setCustZip(customerJurisdictionHandlerLine.getZip());
			}
			if (SearchJurisdictionHandler.PLUS4.equals(componentName)) {
				currentInvoice.setCustZipplus4(customerJurisdictionHandlerLine
						.getZipPlus4());
			}
		}
	}

	public TogglePanelController getTogglePanelController() {
		if (togglePanelController == null) {
			togglePanelController = new TogglePanelController();
			togglePanelController.addTogglePanel("TransactionInformationPanel",true);
			togglePanelController.addTogglePanel("InvoiceInformationPanel", false);
			togglePanelController.addTogglePanel("LocationInformationPanel", false);

			togglePanelController.addTogglePanel("CustomerInformationPanel", true);

			togglePanelController.addTogglePanel("InvoiceDetailInformationPanel", true);
			togglePanelController.addTogglePanel("MiscInformationPanel", false);

			togglePanelController.addTogglePanel("ShipToInformationPanel", true);
			togglePanelController.addTogglePanel("ShipFromInformationPanel", false);
			togglePanelController.addTogglePanel("OrderOriginInformationPanel", false);
			togglePanelController.addTogglePanel("OrderAcceptanceInformationPanel", false);
			togglePanelController.addTogglePanel("FirstUseInformationPanel", false);
			togglePanelController.addTogglePanel("BillToInformationPanel", false);

			togglePanelController.addTogglePanel("UserTextInformationPanel", true);
			togglePanelController.addTogglePanel("UserNumberInformationPanel", false);
			togglePanelController.addTogglePanel("UserDateInformationPanel",false);

			togglePanelController.addTogglePanel("InvoiceLineInformationPanel",true);

			togglePanelController.addTogglePanel("InvoiceLineDetailInformationPanel", true);
			togglePanelController.addTogglePanel("InvoiceLineMiscInformationPanel", false);
		}
		return togglePanelController;
	}

	public void setTogglePanelController(
			TogglePanelController togglePanelController) {
		this.togglePanelController = togglePanelController;
		;
	}

	public TogglePanelController getLineTogglePanelController() {
		if (lineTogglePanelController == null) {
			lineTogglePanelController = new TogglePanelController();
			lineTogglePanelController.addTogglePanel("InvoiceLineInformationPanel", true);

			lineTogglePanelController.addTogglePanel("InvoiceLineDetailInformationPanel", true);
			lineTogglePanelController.addTogglePanel("InvoiceLineMiscInformationPanel", false);
			
			//Locations:
			lineTogglePanelController.addTogglePanel("ShipToInformationPanel", true);
			lineTogglePanelController.addTogglePanel("ShipFromInformationPanel", false);
			lineTogglePanelController.addTogglePanel("OrderOriginInformationPanel", false);
			lineTogglePanelController.addTogglePanel("OrderAcceptanceInformationPanel", false);
			lineTogglePanelController.addTogglePanel("FirstUseInformationPanel", false);
			lineTogglePanelController.addTogglePanel("BillToInformationPanel", false);

			lineTogglePanelController.addTogglePanel("UserTextInformationPanel", true);
			lineTogglePanelController.addTogglePanel("UserNumberInformationPanel", false);
			lineTogglePanelController.addTogglePanel("UserDateInformationPanel", false);
		}
		return lineTogglePanelController;
	}

	public void setLineTogglePanelController(
			TogglePanelController lineTogglePanelController) {
		this.lineTogglePanelController = lineTogglePanelController;
	}

	private void saveJurisdictionValues() {
		searchJurisdictionCallbackExt(this.shipToJurisdictionHandler);
		searchJurisdictionCallbackExt(this.shipFromJurisdictionHandler);
		searchJurisdictionCallbackExt(this.ordrorgnJurisdictionHandler);
		searchJurisdictionCallbackExt(this.ordracptJurisdictionHandler);
		searchJurisdictionCallbackExt(this.firstuseJurisdictionHandler);
		searchJurisdictionCallbackExt(this.billtoJurisdictionHandler);
		searchJurisdictionCallbackExt(this.customerJurisdictionHandler);
	}
	
	private void saveJurisdictionInvoiceValues() {
		searchJurisdictionCallbackExt(this.shipToJurisdictionHandlerLine);
		searchJurisdictionCallbackExt(this.shipFromJurisdictionHandlerLine);
		searchJurisdictionCallbackExt(this.ordrorgnJurisdictionHandlerLine);
		searchJurisdictionCallbackExt(this.ordracptJurisdictionHandlerLine);
		searchJurisdictionCallbackExt(this.firstuseJurisdictionHandlerLine);
		searchJurisdictionCallbackExt(this.billtoJurisdictionHandlerLine);
		searchJurisdictionCallbackExt(this.customerJurisdictionHandler);
	}

	public Boolean getMultiSelect() {
		return true;
	}

	public void multiSelectionChanged(ActionEvent e)
			throws AbortProcessingException {
		// nothing for now
		System.out.println("do something1");
	}

	public boolean isAllSelected() {
		return allSelected;
	}

	public void setAllSelected(boolean allSelected) {
		this.allSelected = allSelected;
	}

	public void selectAllChange(ActionEvent e) {
		HtmlSelectBooleanCheckbox check = (HtmlSelectBooleanCheckbox) ((HtmlAjaxSupport) e
				.getComponent()).getParent();
		transactionSaleDataModel.tooggleSelectAll((Boolean) check.getValue(),
				-1); // second argument is not used as method is overridden
	}

	public boolean isSelectAllEnabled() {
		return true;
	}

	public String getSelectAllCaption() {
		return "Select All";
	}
	
	public boolean getDisplayProcessTransactions() {
		return this.selectedTransaction != null || (this.transactionSaleDataModel != null && this.transactionSaleDataModel.getSelectionMap().size() > 0);
	}
	
	public void sortAction(ActionEvent e) {
		List<BillTransaction> storedBillTransactions = new ArrayList<BillTransaction>();
		storedBillTransactions.addAll(transactionSaleDataModel.getBillTransactions());
		String id = e.getComponent().getId();
		if(id.startsWith("s_trans_")){
			//PP-357
			transactionSaleDataModel.toggleSortOrder(e.getComponent().getId().replace("s_trans_", ""));
		}
		else{		
			transactionSaleDataModel.toggleSortOrder(e.getComponent().getParent().getId());
		}
		
		//transactionSaleDataModel.setBillTransactions(storedBillTransactions);
	}

}
