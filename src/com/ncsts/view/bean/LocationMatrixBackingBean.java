package com.ncsts.view.bean;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;

import org.ajax4jsf.component.html.HtmlAjaxSupport;
import org.apache.log4j.Logger;
import org.richfaces.component.html.HtmlDataTable;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.ncsts.common.LoggerFactory;
import com.ncsts.common.constants.PurchaseTransactionServiceConstants;
import com.ncsts.common.constants.PurchaseTransactionServiceConstants.SuspendReason;
import com.ncsts.domain.EntityItem;
import com.ncsts.domain.Jurisdiction;
import com.ncsts.domain.JurisdictionAllocationMatrixDetail;
import com.ncsts.domain.LocationMatrix;
import com.ncsts.domain.ProcessedTransactionDetail;
import com.ncsts.domain.PurchaseTransaction;
import com.ncsts.exception.PurchaseTransactionProcessingException;
import com.ncsts.jsf.model.LocationMatrixDataModel;
import com.ncsts.services.EntityItemService;
import com.ncsts.services.LocationMatrixService;
import com.ncsts.services.TransactionDetailService;
import com.ncsts.view.util.StateObservable;

/**
 * @author Anand
 *
 */
public class LocationMatrixBackingBean extends BaseMatrixBackingBean<LocationMatrix,LocationMatrixService>
implements SearchJurisdictionHandler.SearchJurisdictionCallback, SearchJurisdictionHandler.SearchJurisdictionCallbackSTJ, Observer, HttpSessionBindingListener {
	
	static private Logger logger = LoggerFactory.getInstance().getLogger(LocationMatrixBackingBean.class);
	@Autowired
	private NexusDefinitionAutoBackingBean nexusDefinitionAutoBackingBean;
	
	private StateObservable ov = null;
	
	// Selection filter inputs
	private String defaultLineFilter = MatrixCommonBean.DEFAULT_LINE_DEFAULT_VALUE;
	//private Long entityListFilter;
	private SearchJurisdictionHandler filterHandler = new SearchJurisdictionHandler();
	
	private SearchJurisdictionHandler selectedHandler = new SearchJurisdictionHandler();
	
	// Flags that modify suspend request
	private Date effectiveDate;
	private Boolean deleteMatrixLine;
	private Boolean suspendExtracted;
	public String entityCode;
	public String entityCodeDesc;
	public boolean entityCodeError = false;
	
	public String entityCodeFilter;
	private  CommonCallBack commonCallBack;
	private boolean isEntityCheck=false;
	private ProcessedTransactionDetail prossdTrDatailBean; 
	
	private TransactionDetailService transactionDetailService;
	private TaxJurisdictionBackingBean taxJurisdictionBean;
	
	private int selectedMasterRowIndex= -1;
	private LocationMatrix selectedMasterMatrixSource = null;
	
	private List<SelectItem> entityList = null;
	private List<SelectItem> entityListItems = null;
	private Map<Long,String> entityMap;
	
	@Autowired
	private EntityItemService entityItemService;
	
	private String selectedColumn = "";
	private boolean isViewChanged = false;
	
	public boolean getIsViewChanged() {
		return isViewChanged;
	}
	public void setViewChanged(boolean isViewChanged) {
		this.isViewChanged = isViewChanged;
	}

	public String getSelectedColumn() {
		return selectedColumn;
	}
	public void setSelectedColumn(String selectedColumn) {
		this.selectedColumn = selectedColumn;
	}
	
	public TaxJurisdictionBackingBean getTaxJurisdictionBean() {
		return taxJurisdictionBean;
	}

	public void setTaxJurisdictionBean(TaxJurisdictionBackingBean taxJurisdictionBean) {
		this.taxJurisdictionBean = taxJurisdictionBean;
	}
	
	public void init() {
		updateMatrixList();
	}
	
	public void searchJurisdictionCallbackSTJ() {
		//Reset STJ_X to checked
		if(selectedHandler.getSelectedJurisdiction()!=null){
			/*
			Jurisdiction aJurisdiction = selectedHandler.getSelectedJurisdiction();
			selectedMatrix.setStj1BooleanFlag((aJurisdiction.getStj1Name()!=null && aJurisdiction.getStj1Name().length()>0));
			selectedMatrix.setStj2BooleanFlag((aJurisdiction.getStj2Name()!=null && aJurisdiction.getStj2Name().length()>0));
			selectedMatrix.setStj3BooleanFlag((aJurisdiction.getStj3Name()!=null && aJurisdiction.getStj3Name().length()>0));
			selectedMatrix.setStj4BooleanFlag((aJurisdiction.getStj4Name()!=null && aJurisdiction.getStj4Name().length()>0));
			selectedMatrix.setStj5BooleanFlag((aJurisdiction.getStj5Name()!=null && aJurisdiction.getStj5Name().length()>0));	
			*/
			//PP-480
			selectedMatrix.setStj1BooleanFlag(true);
			selectedMatrix.setStj2BooleanFlag(true);
			selectedMatrix.setStj3BooleanFlag(true);
			selectedMatrix.setStj4BooleanFlag(true);
			selectedMatrix.setStj5BooleanFlag(true);	
		}
	}
	
	public void searchJurisdictionCallback() {
		//Reset STJ_X to checked
		if(selectedHandler.getSelectedJurisdiction()!=null){
			//0005080
			//Undisplayed STJs will remain �1�.  Displayed STJs could be changed.
			/*
			Jurisdiction aJurisdiction = selectedHandler.getSelectedJurisdiction();
			selectedMatrix.setStj1BooleanFlag((aJurisdiction.getStj1Name()!=null && aJurisdiction.getStj1Name().length()>0));
			selectedMatrix.setStj2BooleanFlag((aJurisdiction.getStj2Name()!=null && aJurisdiction.getStj2Name().length()>0));
			selectedMatrix.setStj3BooleanFlag((aJurisdiction.getStj3Name()!=null && aJurisdiction.getStj3Name().length()>0));
			selectedMatrix.setStj4BooleanFlag((aJurisdiction.getStj4Name()!=null && aJurisdiction.getStj4Name().length()>0));
			selectedMatrix.setStj5BooleanFlag((aJurisdiction.getStj5Name()!=null && aJurisdiction.getStj5Name().length()>0));	
			*/
			selectedMatrix.setStj1BooleanFlag(true);
			selectedMatrix.setStj2BooleanFlag(true);
			selectedMatrix.setStj3BooleanFlag(true);
			selectedMatrix.setStj4BooleanFlag(true);
			selectedMatrix.setStj5BooleanFlag(true);	
		}
	}
	
	public Map<Long, String> getEntityMap() {
		if (entityMap == null) {
			entityMap = new LinkedHashMap<Long, String>();
			
			List<EntityItem> eList = entityItemService.findAllEntityItems();//source list   
    	    if(eList != null && eList.size() > 0){
    	    	for(EntityItem entityItem:eList){
    	    		if(entityItem.getActiveBooleanFlag()){
    	    			entityMap.put(entityItem.getEntityId(),entityItem.getEntityCode() + " - " + entityItem.getEntityName());
    	    		}
    	    	}
    	    }
		}
		return entityMap;
	}

	public List<SelectItem> getEntityList(){
	    	if(entityList==null){
	    		entityList = new ArrayList<SelectItem>();
	    		entityList.add(new SelectItem(new Long(-1L),"Select Entity"));
	    		List<EntityItem> eList = entityItemService.findAllEntityItems();//source list   
	    	    if(eList != null && eList.size() > 0){
	    	    	for(EntityItem entityItem:eList){
	    	    		if(entityItem.getActiveBooleanFlag()){
	    	    			entityList.add(new SelectItem(entityItem.getEntityId(),entityItem.getEntityCode() + " - " + entityItem.getEntityName()));
	    	    		}
	    	    	}
	    	    } 		
	    	}
	    	return entityList;
	    }
	
	public List<SelectItem> getEntityListItems(){
    	if(entityListItems==null){
    		entityListItems = new ArrayList<SelectItem>();
    		entityListItems.add(new SelectItem(new Long(-1L),"Select Entity"));
    		List<EntityItem> eList = entityItemService.findAllEntityItems();//source list   
    	    if(eList != null && eList.size() > 0){
    	    	for(EntityItem entityItem:eList){
    	    		if(entityItem.getActiveBooleanFlag()){
    	    			//entityListItems.add(new SelectItem(entityItem.getEntityId(),entityItem.getEntityCode() + " - " + entityItem.getEntityName()));
    	    			entityListItems.add(new SelectItem(entityItem.getEntityCode(),entityItem.getEntityCode() + " - " + entityItem.getEntityName()));
    	    		}
    	    	}
    	    } 		
    	}
    	return entityListItems;
    }
	
	/*public void setEntityListFilter(Long entityListFilter){
		this.entityListFilter = entityListFilter;
	}
	
	public Long getEntityListFilter(){
		return entityListFilter;
	}*/
	
	public int getselectedMasterRowIndex() {
		return selectedMasterRowIndex;
	}

	public void setselectedMasterRowIndex(int selectedMasterRowIndex) {
		this.selectedMasterRowIndex= selectedMasterRowIndex;
	}
	
	@Override
	public void resetSelection() {
		selectedMasterRowIndex= -1;
		selectedMasterMatrixSource = null;
		super.resetSelection();
	}
	//0002600
public Map<String, String> getSortOrder() {
return getTransactionDataModel().getSortOrder();
    }

	public void sortAction(ActionEvent e) {
		String id = e.getComponent().getId();
		if(id.startsWith("s_trans_")){
			//PP-357
			getTransactionDataModel().toggleSortOrder(e.getComponent().getId().replace("s_trans_", ""));
		}
		else{		
			getMatrixDataModel().toggleSortOrder(e.getComponent().getParent().getId());
		}
	}

	public List<LocationMatrix> getSelectedMatrixDetails() {
		LocationMatrix matrix = selectedMasterMatrixSource;
		List<LocationMatrix> result = (selectedMasterMatrixSource == null)?
				(new ArrayList<LocationMatrix>()) : getMatrixService().getDetailRecords(matrix,
						getMatrixDataModel().getDetailOrderBy());
				
		logger.debug("Detail items: " + result.size());
		return result;
	}
	
	@SuppressWarnings("unchecked")
	public synchronized void selectedMasterRowChanged(ActionEvent e) throws AbortProcessingException {
	    HtmlDataTable table = (HtmlDataTable) ((HtmlAjaxSupport) e.getComponent()).getParent(); 
	    selectedMasterRowIndex = table.getRowIndex();
	    selectedMasterMatrixSource = (LocationMatrix) table.getRowData();
	    
	    super.resetSelection();
	}
	
	@Override
	public String getBeanName() {
		return "locationMatrixBean";
	}

	@Override
	public boolean getIncludeDescFields() {
		return true;
	}
	
	@Override
	public String getMainPageAction() {
		return "location_matrix_main";
	}

	@Override
	public String getDetailFormName() {
		return "matrixDetailForm";
	}

	@Override
	public void setMatrixCommonBean(MatrixCommonBean matrixCommonBean) {
		super.setMatrixCommonBean(matrixCommonBean);
		filterHandler.setJurisdictionService(matrixCommonBean.getJurisdictionService());
		filterHandler.setCacheManager(matrixCommonBean.getCacheManager());
		filterHandler.setTaxJurisdictionBean(taxJurisdictionBean);
		filterHandler.setCallbackScreen("location_matrix_main");
		
		//MF - do not set default country
		//filterHandler.reset();
		//filterHandler.setCountry(getMatrixCommonBean().getUserPreferenceDTO().getUserCountry());
		//filterHandler.setState("");
		
		
		selectedHandler.setJurisdictionService(matrixCommonBean.getJurisdictionService());
		selectedHandler.setCacheManager(matrixCommonBean.getCacheManager());
		selectedHandler.setTaxJurisdictionBean(taxJurisdictionBean);
		selectedHandler.setCallbackScreen("location_matrix_detail");
	}
	
	public void update(Observable obs, Object obj) {
		if (obs == ov) {
			filterHandler.setChanged();
			selectedHandler.setChanged();
		}
	}
	
	public void valueBound(HttpSessionBindingEvent event){
		StateObservable ov = StateObservable.getInstance();
		this.ov = ov;
		this.ov.addObserver(this);
	}

	public void valueUnbound(HttpSessionBindingEvent event){	
		if(ov!=null){
			ov.deleteObserver(this);
		}
	}

	public String getDefaultLineFilter() {
		return defaultLineFilter;
	}

	public void setDefaultLineFilter(String defaultLineFilter) {
		this.defaultLineFilter = defaultLineFilter;
	}

	public Date getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public Boolean getDeleteMatrixLine() {
		return deleteMatrixLine;
	}

	public void setDeleteMatrixLine(Boolean deleteMatrixLine) {
		this.deleteMatrixLine = deleteMatrixLine;
	}

	public Boolean getSuspendExtracted() {
		return suspendExtracted;
	}

	public void setSuspendExtracted(Boolean suspendExtracted) {
		this.suspendExtracted = suspendExtracted;
	}

	public ProcessedTransactionDetail getProssdTrDatailBean() {
		return prossdTrDatailBean;
	}

	public void setProssdTrDatailBean(
			ProcessedTransactionDetail prossdTrDatailBean) {
		this.prossdTrDatailBean = prossdTrDatailBean;
	}

	public TransactionDetailService getTransactionDetailService() {
		return transactionDetailService;
	}

	public void setTransactionDetailService(TransactionDetailService transactionDetailService) {
		this.transactionDetailService = transactionDetailService;
	}
	
	public void willonchange(){
		if(!suspendExtracted){
			deleteMatrixLine = false;
		}
	}

	public void updateSuspendedCounts(ActionEvent e) throws AbortProcessingException {
		deleteMatrixLine = false;
		suspendExtracted = false;
		
		prossdTrDatailBean = transactionDetailService.getSuspendedLocationMatrixTransactionDetails(
				getSelectedMatrix().getLocationMatrixId());
	}

	public SearchJurisdictionHandler getFilterHandler() {
		return filterHandler;
	}

	public SearchJurisdictionHandler getSelectedHandler() {
		return selectedHandler;
	}

	@Override
	public String resetFilter() {
		filterHandler.reset();
		//MF - do not set default country
		//filterHandler.setCountry(getMatrixCommonBean().getUserPreferenceDTO().getUserCountry());
		//filterHandler.setState("");
		
		defaultLineFilter = MatrixCommonBean.DEFAULT_LINE_DEFAULT_VALUE;
		//entityListFilter = null;
		entityCode=null;
		entityCodeDesc = null;
		entityCodeError = false;
		
		entityCodeFilter = null;
		super.resetFilter();
		return "location_matrix_main";
	}

	@Override
	protected void updateMatrixFilter(LocationMatrix matrixFilter) {
		matrixFilter.setJurisdiction(filterHandler.getJurisdiction());
		if (!defaultLineFilter.equals(MatrixCommonBean.DEFAULT_LINE_DEFAULT_VALUE)) {
			matrixFilter.setDefaultFlag(defaultLineFilter);
		} else {
			matrixFilter.setDefaultFlag(null);
		}
	}

	public boolean getIsEntityCheck() {
    	return isEntityCheck;
	}
	
	public void setEntityCheck(boolean isEntityCheck) {
		this.isEntityCheck = isEntityCheck;
	}
			
	public String getEntityCode() {
		return entityCode;
	}
	public void setEntityCode(String entityCode) {
	   	this.entityCode = entityCode;
	}
	
	public String getEntityCodeDesc() {
		return entityCodeDesc;
	}
	public void setEntityCodeDesc(String entityCodeDesc) {
	   	this.entityCodeDesc = entityCodeDesc;
    }
	
	public boolean getEntityCodeError() {
		return entityCodeError;
	}
	public void setEntityCodeError(boolean entityCodeError) {
	   	this.entityCodeError = entityCodeError;
    }
	
	public String getEntityCodeFilter() {
		return entityCodeFilter;
	}
	public void setEntityCodeFilter(String entityCodeFilter) {
	   	this.entityCodeFilter = entityCodeFilter;
	}
	
	public CommonCallBack getCommonCallBack() {
		return commonCallBack;
	}
	public void setCommonCallBack(CommonCallBack commonCallBack) {
		this.commonCallBack = commonCallBack;
	}

	public String updateMatrixList() {
		
		LocationMatrix matrix = newMatrix();
		BeanUtils.copyProperties(getFilterSelection(), matrix);
		
		// Now, map entity level value to the driver properties
		matrixCommonBean.setDriverEntityProperties(matrix);
		
		// Fix the driver values
		matrix.adjustDriverFields();
		
		/*if(entityListFilter!=null){
			matrix.setEntityId(entityListFilter);
		}*/
		EntityItem aEntityItem = this.entityItemService.findByCode(entityCodeFilter);
		if(aEntityItem!=null){
			matrix.setEntityId(aEntityItem.getEntityId());
		}else if (entityCodeFilter != null && aEntityItem == null && !entityCodeFilter.equals("")) {
			matrix.setEntityId(-1L);
		}
		
		// NULLs get converted to 0, change back to null
		Long id = matrix.getId();
		if ((id != null) && (id <= 0L)) {
			matrix.setId(null);
		}
		
		updateMatrixFilter(matrix);
		
		((LocationMatrixDataModel) getMatrixDataModel()).setFilterCriteria(matrix, 
				matrix.getJurisdiction().getGeocode(), matrix.getJurisdiction().getCity(), matrix.getJurisdiction().getCounty(), 
				matrix.getJurisdiction().getState(), matrix.getJurisdiction().getCountry(), matrix.getJurisdiction().getZip(), matrix.getDefaultFlag(), matrix.getLocationMatrixId());

	  	resetSelection();
	  	
	  	return "location_matrix_main";
    }
	
	public boolean getIsAddAction() {
		return (currentAction .equals(EditAction.ADD) || currentAction.equals(EditAction.COPY_ADD));
	}
	
	public void setFilterCriteria(LocationMatrix matrix){
    	((LocationMatrixDataModel) getMatrixDataModel()).setFilterCriteria(matrix, 
				matrix.getJurisdiction().getGeocode(), matrix.getJurisdiction().getCity(), matrix.getJurisdiction().getCounty(), 
				matrix.getJurisdiction().getState(), matrix.getJurisdiction().getCountry(), matrix.getJurisdiction().getZip(), matrix.getDefaultFlag(), matrix.getLocationMatrixId());
    }
	@Override
	protected void prepareDetailDisplay(EditAction action, PurchaseTransaction detail) {
		super.prepareDetailDisplay(action, detail);
		selectedHandler.setJurisdiction(selectedMatrix.getJurisdiction());
		if(action.equals(EditAction.ADD_FROM_TRANSACTION))
			selectedMatrix.setActiveBooleanFlag(true);
		//0002050: Location Matrix issues
		selectedHandler.setCallback(this);
		selectedHandler.setCallbackSTJ(this);
		
		//Company config
		this.entityCodeError = false;
		if(detail!=null){
			EntityItem aEntityItem = this.entityItemService.findByCode(detail.getEntityCode());
			if(aEntityItem!=null){
				selectedMatrix.setEntityId(aEntityItem.getEntityId());
				this.setEntityCode(aEntityItem.getEntityCode());
				this.setEntityCodeDesc(aEntityItem.getEntityCode() + " - " + aEntityItem.getEntityName());
			}
			else{
				//0009220
				selectedMatrix.setEntityId(-1L);
				//this.setEntityCode("");
				this.setEntityCode(detail.getEntityCode());//Ok to display missing code
				this.setEntityCodeDesc(detail.getEntityCode() + " - Entity not Defined");
				this.entityCodeError = true;
				FacesMessage message = new FacesMessage(this.getEntityCodeDesc());
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage(null, message);
			}
		}
	
		if(selectedHandler.getJurisdiction() == null || selectedHandler.getJurisdiction().getGeocode() == null) {
			if(filterHandler.getCountry()==null || filterHandler.getCountry().equals("")){
				//Set default country
				selectedHandler.reset();
				selectedHandler.setCountry(getMatrixCommonBean().getUserPreferenceDTO().getUserCountry());
				selectedHandler.setState("");
			}
			else{
				//Set default country
				selectedHandler.reset();
				selectedHandler.setCountry(filterHandler.getCountry());
				selectedHandler.setState("");
			}
		}
		
		entityList=null;
	}
	
	public String displayAddAction(){
		entityCode = entityCodeFilter;
		if(getIsNavigatedTransactionProcessScreen() == Boolean.FALSE) {
			refreshWhatIfDataGrid(null, null);
		}
		
		else {
			String searchColumnName = "";
			String selectedUserName = null;
			if(selectedColumn == null || selectedColumn.length() == 0){
				searchColumnName = "*DEFAULT";
				selectedUserName = "*GLOBAL";
			}
			else {
				selectedUserName = selectedColumn.substring(0, selectedColumn.indexOf("/"));
				searchColumnName = selectedColumn.substring(selectedColumn.indexOf("/") + 1);
				if ((selectedUserName == null || selectedUserName.isEmpty()) || (searchColumnName == null || searchColumnName.isEmpty())){
					selectedUserName = "*GLOBAL";
					searchColumnName = "*DEFAULT";
				}
				
			}
			refreshWhatIfDataGrid(searchColumnName, selectedUserName);
		}
		String returnText = super.displayAddAction();
		selectedMatrix.setActiveBooleanFlag(true);

		return returnText;
	}
	
	public String displayCopyAddAction(){	
		String nextPage = super.displayCopyAddAction();
		refreshWhatIfDataGrid(null, null);

		EntityItem aEntityItem = this.entityItemService.findById(selectedMatrix.getEntityId());
		entityCode = "";
		entityCodeDesc = "";
		entityCodeError = false;
		if(aEntityItem!=null){
			entityCode = aEntityItem.getEntityCode();
			entityCodeDesc = aEntityItem.getEntityCode() + " - " + aEntityItem.getEntityName();
		}

		return nextPage;
	}
	
	@Override
	public void displayTransactionsAction(ActionEvent e) {
		//9005
		if(entityCode==null || entityCode.trim().length()==0){
			FacesMessage message = new FacesMessage("Entity is Mandatory. ");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
			return;
		}
		EntityItem aEntityItem = this.entityItemService.findByCode(entityCode.trim());
		if(aEntityItem!=null){
			selectedMatrix.setEntityId(aEntityItem.getEntityId());
		}
		else{
			selectedMatrix.setEntityId(-1L);
		}
		
		getTransactionsAction();
	}
	
	public String displayBackDateAction(){
		String nextPage = super.displayBackDateAction();

		EntityItem aEntityItem = this.entityItemService.findById(selectedMatrix.getEntityId());
		entityCode = "";
		entityCodeDesc = "";
		entityCodeError = false;
		if(aEntityItem!=null){
			entityCode = aEntityItem.getEntityCode();
			entityCodeDesc = aEntityItem.getEntityCode() + " - " + aEntityItem.getEntityName();
		}
		
		return nextPage;
	}
	
	public String displayUpdateAction(){	
		String nextPage = super.displayUpdateAction();
		refreshWhatIfDataGrid(null,null);

		EntityItem aEntityItem = this.entityItemService.findById(selectedMatrix.getEntityId());
		entityCode = "";
		entityCodeDesc = "";
		entityCodeError = false;
		if(aEntityItem!=null){
			entityCode = aEntityItem.getEntityCode();
			entityCodeDesc = aEntityItem.getEntityCode() + " - " + aEntityItem.getEntityName();
		}
		
		return nextPage;
	}
	
	public String displayDeleteAction(){	
		String nextPage = super.displayDeleteAction();

		EntityItem aEntityItem = this.entityItemService.findById(selectedMatrix.getEntityId());
		entityCode = "";
		entityCodeDesc = "";
		entityCodeError = false;
		if(aEntityItem!=null){
			entityCode = aEntityItem.getEntityCode();
			entityCodeDesc = aEntityItem.getEntityCode() + " - " + aEntityItem.getEntityName();
		}
		
		return nextPage;
	}
	
	public String displayViewAction(){	
		String nextPage = super.displayViewAction();

		EntityItem aEntityItem = this.entityItemService.findById(selectedMatrix.getEntityId());
		entityCode = "";
		entityCodeDesc = "";
		entityCodeError = false;
		if(aEntityItem!=null){
			entityCode = aEntityItem.getEntityCode();
			entityCodeDesc = aEntityItem.getEntityCode() + " - " + aEntityItem.getEntityName();
		}
		
		return nextPage;
	}
	
	public void displaySuspendAction(ActionEvent e) {
		prossdTrDatailBean = null;
		deleteMatrixLine = false;
		suspendExtracted = false;
	}
	
	@Override
	public String saveAction() {
		//Validate Entity Code
		if(entityCode==null || entityCode.trim().length()==0){
			FacesMessage message = new FacesMessage("Entity is Mandatory. ");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
			return null;
		}
		
		EntityItem aEntityItem = this.entityItemService.findByCode(entityCode.trim());
		if(aEntityItem!=null){
			selectedMatrix.setEntityId(aEntityItem.getEntityId());
		}else{
			FacesMessage message = new FacesMessage("Entity " + entityCode.trim() + " not Defined. ");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
			return null;
		}
				
		if (((selectedMatrix.getCountryBooleanFlag() == null) || !selectedMatrix.getCountryBooleanFlag()) &&
			((selectedMatrix.getStateBooleanFlag() == null) || !selectedMatrix.getStateBooleanFlag()) &&
			((selectedMatrix.getCountyBooleanFlag() == null) || !selectedMatrix.getCountyBooleanFlag()) &&
			((selectedMatrix.getCityBooleanFlag() == null) || !selectedMatrix.getCityBooleanFlag())) {
			FacesMessage message = new FacesMessage("At least one Calculate Tax must be selected.");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
			return null;
		}
		
		Jurisdiction jurisdiction = null;
		if (!currentAction.equals(EditAction.UPDATE)) {
			if (selectedMatrix.getEntityId()==null || selectedMatrix.getEntityId()==-1L) {
				FacesMessage message = new FacesMessage("An Entity must be selected.");
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage(null, message);
				return null;
			}
			
			// Verify if any empty fields
			String geocode = selectedHandler.getInputValueOfGeocode();
			String city = selectedHandler.getInputValueOfCity();
			String county = selectedHandler.getInputValueOfCounty();
			String state = selectedHandler.getInputValueOfState();
			String country = selectedHandler.getInputValueOfCountry();
			String zip = selectedHandler.getInputValueOfZip();
			boolean jurisdictionIncomplete = false;
			if(geocode.length()==0){
				FacesMessage message = new FacesMessage("GeoCode is required.");
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage(null, message);
				jurisdictionIncomplete = true;
			}
			if(city.length()==0){
				FacesMessage message = new FacesMessage("City is required.");
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage(null, message);
				jurisdictionIncomplete = true;
			}
			if(county.length()==0){
				FacesMessage message = new FacesMessage("County is required.");
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage(null, message);
				jurisdictionIncomplete = true;
			}
			if(state.length()==0){
				FacesMessage message = new FacesMessage("State is required.");
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage(null, message);
				jurisdictionIncomplete = true;
			}
			if(country.length()==0){
				FacesMessage message = new FacesMessage("Country is required.");
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage(null, message);
				jurisdictionIncomplete = true;
			}
			if(zip.length()==0){
				FacesMessage message = new FacesMessage("Zip Code is required.");
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage(null, message);
				jurisdictionIncomplete = true;
			}
	
			if(jurisdictionIncomplete){
				return null;
			}
			  
			// Re-verify jurisdiction, 
			jurisdiction = selectedHandler.getJurisdictionFromDB();
			if (jurisdiction == null) {
				boolean emptyError = false;
				if(selectedHandler.getGeocode().length()==0){
					FacesMessage message = new FacesMessage("GeoCode is required.");
					message.setSeverity(FacesMessage.SEVERITY_ERROR);
					FacesContext.getCurrentInstance().addMessage(null, message);
					emptyError = true;
				}
				if(selectedHandler.getCity().length()==0){
					FacesMessage message = new FacesMessage("City is required.");
					message.setSeverity(FacesMessage.SEVERITY_ERROR);
					FacesContext.getCurrentInstance().addMessage(null, message);
					emptyError = true;
				}
				if(selectedHandler.getCounty().length()==0){
					FacesMessage message = new FacesMessage("County is required.");
					message.setSeverity(FacesMessage.SEVERITY_ERROR);
					FacesContext.getCurrentInstance().addMessage(null, message);
					emptyError = true;
				}
				if(selectedHandler.getState().length()==0){
					FacesMessage message = new FacesMessage("State is required.");
					message.setSeverity(FacesMessage.SEVERITY_ERROR);
					FacesContext.getCurrentInstance().addMessage(null, message);
					emptyError = true;
				}
				if(selectedHandler.getCountry().length()==0){
					FacesMessage message = new FacesMessage("Country is required.");
					message.setSeverity(FacesMessage.SEVERITY_ERROR);
					FacesContext.getCurrentInstance().addMessage(null, message);
					emptyError = true;
				}
				if(selectedHandler.getZip().length()==0){
					FacesMessage message = new FacesMessage("Zip Code is required.");
					message.setSeverity(FacesMessage.SEVERITY_ERROR);
					FacesContext.getCurrentInstance().addMessage(null, message);
					emptyError = true;
				}
				
				if(!emptyError){
					FacesMessage message = new FacesMessage("Jurisdiction not found or not unique.");
					message.setSeverity(FacesMessage.SEVERITY_ERROR);
					FacesContext.getCurrentInstance().addMessage(null, message);
				}
				return null;
			}
			
			selectedMatrix.setJurisdiction(jurisdiction);
		}
		//Midtier project code modified - january 2009
		//Start of trigger 
		Long binaryWeight = super.CalcBinaryWeight();
		String significantDigits = super.CalcSignificantDigits();
		// Default Flag
		if(selectedMatrix.getDefaultFlag() == null) {
			selectedMatrix.setDefaultFlag("0");
		}
		if(selectedMatrix.getDefaultFlag() != "1") {
			selectedMatrix.setBinaryWt(binaryWeight);
			selectedMatrix.setDefltBinaryWt(0L);
			
			selectedMatrix.setSignificantDigits(significantDigits);
			selectedMatrix.setDefaultSignificantDigits(null);
		} else {
			selectedMatrix.setBinaryWt(0L);
			selectedMatrix.setDefltBinaryWt(binaryWeight);
			
			selectedMatrix.setSignificantDigits(null);
			selectedMatrix.setDefaultSignificantDigits(significantDigits);
		}
		logger.debug("\n \t binaryWeight by tb_location_matrix_b_iu : " + selectedMatrix.getBinaryWt());
		logger.debug("\n \t significantDigits by tb_location_matrix_b_iu : " + selectedMatrix.getSignificantDigits());
		//End of trigger 
		
		System.out.println("before validate");
		if(super.validateDriverSel(getViewPanel())){
			Long entityId = selectedMatrix.getEntityId();
			matrixId = null;
			System.out.println("calling save action");
			String returnPage = super.saveAction();
			
			
			
			if(jurisdiction!=null && (currentAction.equals(EditAction.ADD) || currentAction.equals(EditAction.ADD_FROM_TRANSACTION) || currentAction.equals(EditAction.COPY_ADD)) ){	
				if(returnPage!=null && returnPage.length()>0){
					try {
						nexusDefinitionAutoBackingBean.setOkView(returnPage);
						String result = nexusDefinitionAutoBackingBean.process("LM", matrixId, entityId, jurisdiction);
						//Once the new bean is saved, update all suspended Transactions that match the criteria.
						try {
							selectedMatrix = matrixService.findById(matrixId);
							System.out.println(selectedMatrix.getLocationMatrixId());
							super.processSuspendedTransactions(SuspendReason.SUSPEND_LOCN);
						} catch (PurchaseTransactionProcessingException e1) {
							logger.info("No suspended transactions to be processed");

						}

						if(!"".equals(result)) {
							return result;
						}
					}
					catch(Exception e) {
						e.printStackTrace();
					}
				}		
			}
			return returnPage;
		}else{
			return null;
		}
	}
	
	public String suspendAction() { 
		transactionDetailService.suspendLocationMatrixTransactions(purchaseTransactionService,
				getSelectedMatrix().getLocationMatrixId(), suspendExtracted, true, deleteMatrixLine);
		
		getMatrixDataModel().refreshData(false);
		resetSelection();
		return getMainPageAction();
	}

	@Override
	public String deleteAction() {
		if (currentAction.equals(EditAction.DELETE) && getTransactionDetailService().getLocationMatrixInUse(getSelectedMatrix().getLocationMatrixId())) {
			FacesMessage message = new FacesMessage("Location matrix is in use and cannot be deleted.");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
			return null;
		}
		return super.deleteAction();
	}
	
	public void selectlocActionListener(ActionEvent event) {		   
	}
	
	public void setEntityIdChanged(ValueChangeEvent e){
		Long entityId = (Long)e.getNewValue();
		if(entityId == null) {
			selectedMatrix.setEntityId(-1L);
		}
		else {
			selectedMatrix.setEntityId(entityId);
		}
    }
	
	public String viewLocnMatrix(Long locMatrixId,String fromview) {
		return setActionAndPrepare(locMatrixId,fromview);
	}
	
	private String setActionAndPrepare(Long locMatrixId,String fromview){
		if("viewFromTransaction".equals(fromview)) {
			currentAction = EditAction.VIEW_FROM_TRANSACTION;
		}
		else {
			currentAction = EditAction.VIEW_FROM_TRANSACTION_LOG_ENTRY;
		}
		
		selectedMatrix = matrixService.findById(locMatrixId);
		if(selectedMatrix!=null){
			prepareDetailDisplay(currentAction, null);
			EntityItem aEntityItem = this.entityItemService.findById(selectedMatrix.getEntityId());
			entityCode = "";
			entityCodeDesc = "";
			entityCodeError = false;
			if(aEntityItem!=null){
				entityCode = aEntityItem.getEntityCode();
				entityCodeDesc = aEntityItem.getEntityCode() + " - " + aEntityItem.getEntityName();
			}
			return "location_matrix_txnview";
		}else {
			return null;
		}
		
	}
	
	@Override
	public void searchDriver(ActionEvent e) {
		String id = (String) e.getComponent().getAttributes().get("ID");
		super.searchDriver(e);

		if(id.equalsIgnoreCase("filterPanel")) {
			getDriverHandlerBean().setCallBackScreen("location_matrix_main");
		}
		else{
			getDriverHandlerBean().setCallBackScreen("location_matrix_detail");
		}		
	}	
}
