package com.ncsts.view.bean;

import com.ncsts.domain.BCPPurchaseTransaction;
import com.ncsts.domain.TransactionDetail;

public class TransactionSplittingToolBeanFactory {
	public TransactionSplittingToolBean<BCPPurchaseTransaction> createBCPTransactionSplittingToolBean() {
		TransactionSplittingToolBean<BCPPurchaseTransaction> instance = new TransactionSplittingToolBean<BCPPurchaseTransaction>();
		return instance;
	}
	
	public TransactionSplittingToolBean<TransactionDetail> createTransactionSplittingToolBean() {
		TransactionSplittingToolBean<TransactionDetail> instance = new TransactionSplittingToolBean<TransactionDetail>();
		return instance;
	}
}
