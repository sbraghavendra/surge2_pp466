package com.ncsts.view.bean;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;
import java.util.Set;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.component.html.HtmlSelectBooleanCheckbox;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;

import org.ajax4jsf.component.html.HtmlAjaxSupport;
import org.apache.log4j.Logger;
import org.apache.myfaces.custom.fileupload.HtmlInputFileUpload;
import org.apache.myfaces.custom.fileupload.UploadedFile;
import org.hibernate.exception.ConstraintViolationException;
import org.richfaces.component.html.HtmlDataTable;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.util.StringUtils;

import com.ncsts.common.CacheManager;
import com.ncsts.common.VirtualMap;
import com.ncsts.domain.CCHCode;
import com.ncsts.domain.CCHCodePK;
import com.ncsts.domain.CCHTaxMatrix;
import com.ncsts.domain.CCHTaxMatrixPK;
import com.ncsts.domain.GlExtractMap;
import com.ncsts.domain.ListCodes;
import com.ncsts.domain.NexusDefinitionDetail;
import com.ncsts.domain.Option;
import com.ncsts.domain.OptionCodePK;
import com.ncsts.domain.ReferenceDetail;
import com.ncsts.domain.ReferenceDetailPK;
import com.ncsts.domain.ReferenceDocument;
import com.ncsts.domain.TaxCode;
import com.ncsts.domain.TaxCodeDetail;
import com.ncsts.domain.TaxCodePK;
import com.ncsts.domain.TaxCodeStatePK;
import com.ncsts.domain.User;
import com.ncsts.dto.TaxCodeStateDTO;
import com.ncsts.dto.UserDTO;
import com.ncsts.services.CCHCodeService;
import com.ncsts.services.CCHTaxMatrixService;
import com.ncsts.services.GlExtractMapService;
import com.ncsts.services.JurisdictionService;
import com.ncsts.services.OptionService;
import com.ncsts.services.ReferenceDocumentService;
import com.ncsts.services.TaxCodeDetailService;
import com.ncsts.services.TaxCodeService;
import com.ncsts.services.TaxCodeStateService;
import com.ncsts.view.util.StateObservable;

public class SupportingCodeBackingBean implements SearchJurisdictionHandler.SearchJurisdictionCallback, Observer, HttpSessionBindingListener {
	
	enum ActiveTableType {
		TAXCODE_TYPE,
		TAXCODE_STATE,
		TAXCODE_COUNTRY,
		TAXCODE,
		DOCREF,
		DOCREF_TYPE,
		DOCREF_DETAIL,
		CCHCODE,
		CCHGROUP,
		CCHGROUPITEM,
		NONE
	}
	
	private Logger logger = Logger.getLogger(SupportingCodeBackingBean.class);

	private StateObservable ov = null;
	private List<SelectItem> firstComboItems = new ArrayList<SelectItem>();
	private List<SelectItem> secondComboItems = new ArrayList<SelectItem>();
	private List<SelectItem> thirdComboItems = new ArrayList<SelectItem>();
	
	private List<SelectItem> measureTypeList;
	private List<SelectItem> taxTypeList;
	private List<SelectItem> refTypeList;
	private List<SelectItem> tcTypeList;
	
	private SearchJurisdictionHandler jurisdictionHandler = new SearchJurisdictionHandler();
	private String jurisdictionState;
	private String jurisdictionCountry;
	private String docRName;
	
	private SearchDocRefHandler docRefHandler = new SearchDocRefHandler();
	
	private VirtualMap<String,Boolean> stateSelection = new VirtualMap<String,Boolean>(new Boolean(false));
	private VirtualMap<String,Long> stateJurisdiction = new VirtualMap<String,Long>(new Long(0L));
	private List<TaxCodeStateDTO> taxCodeStateChoices;
	
	private List<TaxCode> typeTaxCodeList = new ArrayList<TaxCode>(); //this list will hold the taxcode list for type tab
	private List<TaxCodeStateDTO> typeStateList = new ArrayList<TaxCodeStateDTO>(); //this list will hold the state list for type tab
	
	private List<ListCodes> taxCodeTypeList = new ArrayList<ListCodes>(); // this list will hold the types list for tax code tab
	private List<TaxCodeStateDTO> taxCodeStateList = new ArrayList<TaxCodeStateDTO>(); //this list will hold the states for tax code tab
	
	private List<ListCodes> stateTypeList = new ArrayList<ListCodes>(); // this list will hold the types list for state tab
	private List<TaxCode> stateTaxCodeList = new ArrayList<TaxCode>(); //this list will hold the taxcode list for state tab
	
	private List<ReferenceDetail> docRefList = new ArrayList<ReferenceDetail>();//this will hold doc ref List for state/type/taxCode
	
	private List<TaxCodeStateDTO> stateList;
	private List<TaxCode> taxCodeList;
	private List<ReferenceDocument> docList; 
	private List<CCHCode> cchCodeList;
	private List<CCHTaxMatrix> cchGroupList;
	
	private List<CCHTaxMatrix> cchGroupItemsList = new ArrayList<CCHTaxMatrix>();
		
	private boolean viewChange = true; // this is by default true
	private boolean firstComboValueChange;
	private boolean secondComboValueChange;
	private boolean thirdComboValueChange;
	
	private boolean state;
	private boolean country;
	private boolean ref;
	private boolean doc;
	private boolean cchTax;
	private boolean cchGroup;
	private boolean cchGItem;
	
	/* these constants are declared for first combo */
	private static final String ITEM_TYPE = "By Type";
    private static final String ITEM_STATE = "By State";
    private static final String ITEM_COUNTRY = "By Country";
    private static final String ITEM_TAX_CODE = "By TaxCode";
    private static final String	ITEM_SUPPORTING_CODE = "Supporting Codes";
    
    private String selectedView=ITEM_TYPE;
    private String selectedFirstComboValue="0";
    private String selectedSecondComboValue="0";
    private String selectedThirdComboValue="0";
    
    private String selectedCountryComboValue="";
    
    private String selectedCertificateLevelValue = "";
    
    private ListCodesBackingBean listCodesBean;
	private TaxCodeDetailService taxCodeDetailService;
	private TaxCodeStateService taxCodeStateService;
	private TaxCodeService taxCodeService;
	private ReferenceDocumentService referenceDocumentService;
	private CCHCodeService cchCodeService;
	private CCHTaxMatrixService cchTaxMatrixService;
	private JurisdictionService jurisdictionService;
	private GlExtractMapService glExtractMapService;
	private CacheManager cacheManager;
	private OptionService optionService;
	
	private EditAction currentAction = EditAction.VIEW;
	private List<String> stateMeasureTypes = new ArrayList<String>();
	private TaxCodeStateDTO selectedState;
	private TaxCodeStateDTO selectedCountry;
	private TaxCode selectedTaxCode;
	private ReferenceDocument selectedDocRef;
	private ReferenceDetail selectedDocRefDetail;
	private CCHCode selectedCCHCode;
	private CCHTaxMatrix selectedCCHGroup;
	private CCHTaxMatrix selectedCCHGroupItem;

	private TaxCodeStateDTO selectedStateSource;
	private TaxCodeStateDTO selectedCountrySource;
	private TaxCode selectedTaxCodeSource;
	private ReferenceDocument selectedDocRefSource;
	private ReferenceDetail selectedDocRefDetailSource;
	private ListCodes selectedDocRefTypeSource;
	private CCHCode selectedCCHCodeSource;
	private CCHTaxMatrix selectedCCHGroupSource;
	private CCHTaxMatrix selectedCCHGroupItemSource;

	private int selectedStateIndex = -1;
	private int selectedCountryIndex = -1;
	private int selectedTaxCodeIndex = -1;
	private int selectedDocRefIndex = -1;
	private int selectedDocRefDetailIndex = -1;
	private int selectedDocRefTypeIndex = -1;
	private int selectedCCHCodeIndex = -1;
	private int selectedCCHGroupIndex = -1;
	private int selectedCCHGroupItemIndex = -1;
	
	private String taxCodeTypeCode;
	
	private boolean updateStates = false;

	private String url = null;
	
	private String selectedSupportingCode = "0";
	private String selectedRefType = "";
	private String enteredCode = "";
	private String enteredState = "";
	private String enteredCountry = "";
	private String enteredStateCountry = "";
	private String selectedLocalTaxabilityValue = "";
	private String displayWarningMessage = "";
	
	private List<SelectItem> refTypeListForFilter;
	private List<SelectItem> countryItemsForFilter;
	private List<SelectItem> localTaxabilityItems;
	private List<SelectItem> certificateLevelItems;
	
	public List<TaxCodeStateDTO> docStateFilteredList = null;
	public List<TaxCodeStateDTO> docCountryFilteredList = null;
	
	public List<TaxCodeStateDTO> stateQuickUpdateList = null;
	public List<TaxCodeStateDTO> countryQuickUpdateList = null;
	
	private HtmlInputFileUpload uploadFile;
	private UploadedFile uploadedFile;
	
	@Autowired
	private loginBean loginBean;
	
	public HtmlInputFileUpload getUploadFile() {
		return uploadFile;
	}

	public void setUploadFile(HtmlInputFileUpload uploadFile) {
		this.uploadFile = uploadFile;
	}

	public UploadedFile getUploadedFile() {
		return uploadedFile;
	}

	public void setUploadedFile(UploadedFile uploadedFile) {
		this.uploadedFile = uploadedFile;
	}

	private static final SelectItem[] VIEWITEMS = new SelectItem[]{
        new SelectItem(ITEM_TYPE,ITEM_TYPE),
        new SelectItem(ITEM_STATE,ITEM_STATE),
        new SelectItem(ITEM_COUNTRY,ITEM_COUNTRY),
        new SelectItem(ITEM_TAX_CODE,ITEM_TAX_CODE),
        new SelectItem(ITEM_SUPPORTING_CODE,ITEM_SUPPORTING_CODE)
        };
	
	public SelectItem[] getViewItems() {
		return VIEWITEMS;
	}
	
	public List<SelectItem> getCountryItems() {
		List<SelectItem> selectItems = new ArrayList<SelectItem>();
		selectItems.add(new SelectItem("","Select Country"));
		
		//List<ListCodes> listCodes = getListCodesBean().getListCodesService().getListCodesByCodeTypeCode("COUNTRY");
		//for(ListCodes listCode : listCodes){
		//	selectItems.add(new SelectItem(listCode.getCodeCode(), listCode.getDescription() + " (" + listCode.getCodeCode() + ")"));
    	//}
		
		Map<String,String> countryMap = cacheManager.getCountryMap();
		for (String countryCode : countryMap.keySet()) {
			String countryName = countryMap.get(countryCode);
			selectItems.add(new SelectItem(countryCode, countryName + " (" + countryCode + ")"));
		}

		return selectItems;
	}
	
	public String navigateAction() {
        return "taxability_codes";
	}

    public String viewSubmitted() {
    	firstComboItems.clear();
    	secondComboItems.clear();
        thirdComboItems.clear();
        viewChange = true;
        firstComboValueChange = false;
        secondComboValueChange = false;
        thirdComboValueChange = false;
        state = false;
        ref = false;
        doc = false;
        cchTax = false;
		cchGroup = false;
		cchGItem = false;
        selectedFirstComboValue = "0"; //default value
        selectedSecondComboValue = "0"; // default value
        selectedThirdComboValue = "0"; // default value
        resetSelection();
        if (selectedView.equals(ITEM_TYPE)) {
        	for(ListCodes lc : getTypeList()){
        		firstComboItems.add(new SelectItem(lc.getCodeCode(),lc.getDescription()));
        	}
        	return "taxability_type";
        }else if(selectedView.equals(ITEM_TAX_CODE)){
        	Set<String> codes = new HashSet<String>();
        	for(TaxCode tc : getTaxCodeList()){
        		String code = tc.getTaxCodePK().getTaxcodeCode();
        		if (!codes.contains(code)) {
        			codes.add(code);
        			firstComboItems.add(new SelectItem(code,code));
        		}
        	}
        	return "taxability_taxcode";
        }else if(selectedView.equals(ITEM_STATE)){
        	for(TaxCodeStateDTO tcs : getStateList()){
        		firstComboItems.add(new SelectItem(tcs.getTaxCodeState()+ "-" + tcs.getCountry(),tcs.getName() + " - " + tcs.getCountry()));
        	}
        	return "taxability_state";
        }else{
        	return "taxability_supporting_code";
        }
    }
    
    public void firstComboValueChanged(ValueChangeEvent e){
    	handleFirstComboChange((String)e.getNewValue());
    }
    
    private void handleFirstComboChange(String newValue) {
    	//clear sub item combo values
    	secondComboItems.clear();
    	thirdComboItems.clear();
    	selectedSecondComboValue = "0"; //default value
    	selectedThirdComboValue = "0"; //default value
    	selectedFirstComboValue = newValue;
        resetSelection();
    	//logger.debug("selected view == " + selectedView + "first combo value == " + selectedFirstComboValue);
    	if (selectedView.equals(ITEM_TAX_CODE)) {
    		taxCodeTypeList = taxCodeDetailService.findListCodeForTaxCode(selectedFirstComboValue);
    		for(ListCodes lc : taxCodeTypeList){
        		secondComboItems.add(new SelectItem(lc.getCodeCode(),lc.getDescription()));
        	}
    	} else if(selectedView.equals(ITEM_TYPE)) {
    		typeTaxCodeList = taxCodeDetailService.findByTaxCodeCode(selectedFirstComboValue);
    		for(TaxCode tc : typeTaxCodeList){
            	String key = tc.getTaxCodePK().getTaxcodeCode();
    			secondComboItems.add(new SelectItem(key,tc.getTaxCodePK().getTaxcodeCode()+" - "+tc.getDescription()));
    		}
    	} else if(selectedView.equals(ITEM_STATE)) {
    		stateTypeList = taxCodeDetailService.findListCodeForState(selectedFirstComboValue);
    		for(ListCodes lc : stateTypeList){
        		secondComboItems.add(new SelectItem(lc.getCodeCode(),lc.getDescription()));
        	}
    	} else {
    		state = (selectedFirstComboValue.equalsIgnoreCase("5"));
    		ref = (selectedFirstComboValue.equalsIgnoreCase("4"));
    		doc = (selectedFirstComboValue.equalsIgnoreCase("3"));
    		cchTax = (selectedFirstComboValue.equalsIgnoreCase("2"));
    		cchGroup = (selectedFirstComboValue.equalsIgnoreCase("1"));
    		cchGItem = false;
    		if (cchGroup) {
    			for(CCHTaxMatrix cmd : getCchGroupList()){
    				secondComboItems.add(new SelectItem(cmd.getCchTaxMatrixPK().getGroupCode(),
    						cmd.getCchTaxMatrixPK().getGroupCode() + " - " + cmd.getGroupDesc()));
    			}
    		}
    	}
    	 viewChange = (selectedFirstComboValue.equals("0"));
         firstComboValueChange = (!selectedFirstComboValue.equals("0"));
         secondComboValueChange = false;
         thirdComboValueChange = false;
         url = null;
    	//findByTaxCodeCode
    }
    
    public void secondComboValueChanged(ValueChangeEvent e) {
    	handleSecondComboChange((String)e.getNewValue());
    }
    
    private void handleSecondComboChange(String newValue) {
    	logger.debug("second combo value changed");
    	if (newValue.equals("0")) {
    		handleFirstComboChange(selectedFirstComboValue);
    	} else {
	    	thirdComboItems.clear();
	    	selectedThirdComboValue = "0"; //default value
	    	selectedSecondComboValue = newValue;
	        resetSelection();
	    	if (selectedView.equals(ITEM_TAX_CODE)) {
	    		taxCodeStateList = taxCodeStateService.findByTaxCode(selectedSecondComboValue, selectedFirstComboValue);
	    		for(TaxCodeStateDTO tcs : taxCodeStateList){
	        		thirdComboItems.add(new SelectItem(tcs.getTaxCodeState()+ "-" + tcs.getCountry(),tcs.getName() + " - " + tcs.getCountry()));
	        	}
	    	} else if(selectedView.equals(ITEM_TYPE)) {
	    		String[] code = selectedSecondComboValue.split(":");
	    		typeStateList = taxCodeStateService.findByTaxCode(code[0], code[1]);
	    		for(TaxCodeStateDTO tcs : typeStateList){
	    			thirdComboItems.add(new SelectItem(tcs.getTaxCodeState()+ "-" + tcs.getCountry(), tcs.getName() + " - " + tcs.getCountry()));
	    		}
	    	} else if(selectedView.equals(ITEM_STATE)) {
	    		stateTaxCodeList = taxCodeDetailService.findTaxCodeListByStateCode(selectedFirstComboValue, selectedSecondComboValue);
	            for(TaxCode taxCodeDTO : stateTaxCodeList){
	            	String key = taxCodeDTO.getTaxCodePK().getTaxcodeCode();
	            	thirdComboItems.add(new SelectItem(key, taxCodeDTO.getTaxCodePK().getTaxcodeCode()+" - "+taxCodeDTO.getDescription()));
	            }
	    	} else {
	    		cchGroup = false;
	    		state = false;
	    		ref = false;
	    		doc = false;
	    		cchTax = false;
	    		cchGItem = true;
	    		cchGroupItemsList = taxCodeDetailService.getAllCCHGroupItems(selectedSecondComboValue);
	    	}
	    	
	    	 viewChange = false;
	         firstComboValueChange = false;
	         secondComboValueChange = true;
	         thirdComboValueChange = false;
    	}
    	url = null;
    }
    
    public void thirdComboValueChanged(ValueChangeEvent e) {
    	handleThirdComboChange((String) e.getNewValue());
    }
    
    private void handleThirdComboChange(String newValue) {
    	if (newValue.equals("0")) {
    		handleSecondComboChange(selectedSecondComboValue);
    	} else {
	    	selectedThirdComboValue = newValue;
	        resetSelection();
	    	if (selectedView.equals(ITEM_TAX_CODE)) {
	    		docRefList = taxCodeDetailService.getDocumentReferences(selectedSecondComboValue, selectedFirstComboValue, selectedThirdComboValue);
	    	} else if(selectedView.equals(ITEM_TYPE)) {
	    		String[] code = selectedSecondComboValue.split(":");
	    		docRefList = taxCodeDetailService.getDocumentReferences(code[0], code[1], selectedThirdComboValue);
	    	} else if(selectedView.equals(ITEM_STATE)) {
	    		String[] code = selectedThirdComboValue.split(":");
	    		docRefList = taxCodeDetailService.getDocumentReferences(code[0], code[1], selectedFirstComboValue);
	        } else {
	    		logger.debug("Doing nothing");
	    	}
	    	
	    	viewChange = false;
	        firstComboValueChange = false;
	        secondComboValueChange = false;
	        thirdComboValueChange = true;
    	}
    	url = null;
    }
    
    private void refreshCurrentTable() {
    	if (thirdComboValueChange) {
    		handleThirdComboChange(selectedThirdComboValue);
    	} else if (secondComboValueChange) {
    		handleSecondComboChange(selectedSecondComboValue);
    	} else if (firstComboValueChange) {
    		handleFirstComboChange(selectedFirstComboValue);
    	} else {
    		viewSubmitted();
    	}
    }
    
    private String getCurrentStateCode() {
    	if (selectedView.equals(ITEM_TAX_CODE)) {
    		return selectedThirdComboValue;
    	} else if(selectedView.equals(ITEM_TYPE)) {
    		return selectedThirdComboValue;
    	} else if(selectedView.equals(ITEM_STATE)) {
    		return selectedFirstComboValue;
    	}
    	return null;
    }
    
    private String getCurrentTaxCode() {
    	if (selectedView.equals(ITEM_TAX_CODE)) {
    		return selectedFirstComboValue;
    	} else if(selectedView.equals(ITEM_TYPE)) {
    		return selectedSecondComboValue.contains(":")? selectedSecondComboValue.split(":")[1]:"";
    	} else if(selectedView.equals(ITEM_STATE)) {
    		return selectedThirdComboValue.contains(":")? selectedThirdComboValue.split(":")[1]:"";
    	}
    	return null;
    }
    
    private String getCurrentTypeCode() {
    	if (selectedView.equals(ITEM_TAX_CODE)) {
    		return selectedSecondComboValue;
    	} else if(selectedView.equals(ITEM_TYPE)) {
    		return selectedFirstComboValue;
    	} else if(selectedView.equals(ITEM_STATE)) {
    		return selectedSecondComboValue;
    	}
    	return null;
    }
    
    /* Combo values getter and setter method */
    public List<SelectItem> getFirstComboItems() {
		return firstComboItems;
	}
	public List<SelectItem> getSecondComboItems() {
		return secondComboItems;
	}
	public List<SelectItem> getThirdComboItems() {
		return thirdComboItems;
	}

	/* required lists getter and setter method */
	public List<TaxCodeStateDTO> getStateList() {
		if (stateList == null) {
			stateList = new ArrayList<TaxCodeStateDTO>();
			stateList.addAll(cacheManager.getTaxCodeCountryStateMap().values());
		}
		return stateList;
	}

	public List<ListCodes> getTypeList() {
		return cacheManager.getListCodesByType("TCTYPE");
	}
	
	public Map<String,TaxCode> getTaxCodeMap() {
		return cacheManager.getTaxCodeMap();
	}

	public Map<String,ListCodes> getTaxCodeTypeMap() {
		return cacheManager.getListCodesMapByType("TCTYPE");
	}

	public Map<String,ListCodes> getRefTypeMap() {
		return cacheManager.getListCodesMapByType("REFTYPE");
	}

	public Map<String,TaxCodeStateDTO> getStateMap() {
		return cacheManager.getTaxCodeStateMap();
	}
	
	public Map<String,String> getCountryMap() {
		return cacheManager.getCountryMap();
	}

	public final List<TaxCode> getTaxCodeList() {
		if (taxCodeList == null) {
			taxCodeList = taxCodeDetailService.getAllTaxCode();
		}
		return taxCodeList;
	}

	public List<ListCodes> getTaxCodeTypeList() {
		return taxCodeTypeList;
	}
	public List<TaxCodeStateDTO> getTaxCodeStateList() {
		return taxCodeStateList;
	}
	public List<TaxCode> getTypeTaxCodeList() {
		return typeTaxCodeList;
	}
	public List<TaxCodeStateDTO> getTypeStateList() {
		return typeStateList;
	}
	public List<ListCodes> getStateTypeList() {
		return stateTypeList;
	}
	public List<TaxCode> getStateTaxCodeList() {
		return stateTaxCodeList;
	}
	public List<ReferenceDetail> getDocRefList() {
		return docRefList;
	}

	/* flag getter and setter methods */
	public boolean isViewChange() {
		return viewChange;
	}
	public boolean isFirstComboValueChange() {
		return firstComboValueChange;
	}
	public boolean isSecondComboValueChange() {
		return secondComboValueChange;
	}
	public boolean isThirdComboValueChange() {
		return thirdComboValueChange;
	}

	/* selected values getter and setter methods */
	public String getSelectedFirstComboValue() {
		return selectedFirstComboValue;
	}
	public void setSelectedFirstComboValue(String selectedFirstComboValue) {
		this.selectedFirstComboValue = selectedFirstComboValue;
	}
	public String getSelectedSecondComboValue() {
		return selectedSecondComboValue;
	}
	public void setSelectedSecondComboValue(String selectedSecondComboValue) {
		this.selectedSecondComboValue = selectedSecondComboValue;
	}
	public String getSelectedThirdComboValue() {
		return selectedThirdComboValue;
	}
	public void setSelectedThirdComboValue(String selectedThirdComboValue) {
		this.selectedThirdComboValue = selectedThirdComboValue;
	}
	public String getSelectedView() {
		return selectedView;
	}
	public void setSelectedView(String selectedView) {
		this.selectedView = selectedView;
	}
	
	public String getSelectedCountryComboValue() {
		return selectedCountryComboValue;
	}
	
	public void setSelectedCountryComboValue(String selectedCountryComboValue) {
		this.selectedCountryComboValue = selectedCountryComboValue;
	}
	
	public List<TaxCodeStateDTO> getDocStateList() {
		return getStateList();
	}

	public List<ListCodes> getDocRefTypeList() {
		return cacheManager.getListCodesByType("REFTYPE"); 
	}

	public List<ReferenceDocument> getDocList() {
		if (docList == null) {
			docList = taxCodeDetailService.getAllDocumentReferences();
		}
		return docList;
	}

	public List<CCHCode> getCchCodeList() {
		if (cchCodeList == null) {
			cchCodeList = taxCodeDetailService.getAllCCHCode();
		}
		return cchCodeList;
	}

	public List<CCHTaxMatrix> getCchGroupList() {
		if (cchGroupList == null) {
			cchGroupList = taxCodeDetailService.getAllCCHGroup();
		} 
		return cchGroupList;
	}

	public List<CCHTaxMatrix> getCchGroupItemsList() {
		return cchGroupItemsList;
	}
	
	public boolean getIsCountry() {
		return country;
	}
	public boolean getIsState() {
		return state;
	}
	public boolean getIsRef() {
		return ref;
	}
	public boolean getIsDoc() {
		return doc;
	}

	public boolean isState() {
		return state;
	}
	public boolean isCountry() {
		return country;
	}
	public boolean isRef() {
		return ref;
	}
	public boolean isDoc() {
		return doc;
	}
	public boolean isCchTax() {
		return cchTax;
	}
	public boolean isCchGroup() {
		return cchGroup;
	}
	public boolean isCchGItem() {
		return cchGItem;
	}
	
	/* service getter and setter methods */
	public TaxCodeStateService getTaxCodeStateService() {
		return taxCodeStateService;
	}

	public void setTaxCodeStateService(TaxCodeStateService taxCodeStateService) {
		this.taxCodeStateService = taxCodeStateService;
	}

	public TaxCodeDetailService getTaxCodeDetailService() {
		return taxCodeDetailService;
	}

	public void setTaxCodeDetailService(TaxCodeDetailService taxCodeDetailService) {
		this.taxCodeDetailService = taxCodeDetailService;
	}

	public TaxCodeService getTaxCodeService() {
		return taxCodeService;
	}

	public void setTaxCodeService(TaxCodeService taxCodeService) {
		this.taxCodeService = taxCodeService;
	}


	public ReferenceDocumentService getReferenceDocumentService() {
		return referenceDocumentService;
	}

	public void setReferenceDocumentService(ReferenceDocumentService referenceDocumentService) {
		this.referenceDocumentService = referenceDocumentService;
		docRefHandler.setReferenceDocumentService(referenceDocumentService);
	}

	public CCHCodeService getCchCodeService() {
		return cchCodeService;
	}

	public void setCchCodeService(CCHCodeService cchCodeService) {
		this.cchCodeService = cchCodeService;
	}

	public CCHTaxMatrixService getCchTaxMatrixService() {
		return cchTaxMatrixService;
	}

	public void setCchTaxMatrixService(CCHTaxMatrixService cchTaxMatrixService) {
		this.cchTaxMatrixService = cchTaxMatrixService;
	}

	public ListCodesBackingBean getListCodesBean() {
		return listCodesBean;
	}

	public void setListCodesBean(ListCodesBackingBean listCodesBean) {
		this.listCodesBean = listCodesBean;
	}

	public JurisdictionService getJurisdictionService() {
		return jurisdictionService;
	}

	public void setJurisdictionService(JurisdictionService jurisdictionService) {
		this.jurisdictionService = jurisdictionService;
		this.jurisdictionHandler.setJurisdictionService(jurisdictionService);
	}

	public CacheManager getCacheManager() {
		return cacheManager;
	}

	public void setCacheManager(CacheManager cacheManager) {
		this.cacheManager = cacheManager;
		this.jurisdictionHandler.setCacheManager(cacheManager);
		
		// Initialize dropdown on starting page
    	for(ListCodes lc : getTypeList()){
    		firstComboItems.add(new SelectItem(lc.getCodeCode(),lc.getDescription()));
    	}
	}
	
	public void update(Observable obs, Object obj) {
		if (obs == ov) {
			jurisdictionHandler.setChanged();
		}
	}
	
	public void valueBound(HttpSessionBindingEvent event){
		StateObservable ov = StateObservable.getInstance();
		this.ov = ov;
		this.ov.addObserver(this);
	}

	public void valueUnbound(HttpSessionBindingEvent event){	
		if(ov!=null){
			ov.deleteObserver(this);
		}
	}

	public boolean getDisableAddButton() {
        if (selectedView.equals(ITEM_TYPE)) {
        	if (selectedFirstComboValue.equals("0")) {
        		return true;
        	} else if (selectedSecondComboValue.equals("0")) {
        		return false;
        	} else if (selectedThirdComboValue.equals("0")) {
        		return true;
        	}
        	return false;
        }else if(selectedView.equals(ITEM_TAX_CODE)){
        	if (selectedFirstComboValue.equals("0")) {
        		return false;
        	} else if (selectedSecondComboValue.equals("0")) {
        		return true;
        	} else if (selectedThirdComboValue.equals("0")) {
        		return true;
        	}
        	return false;
        }else if(selectedView.equals(ITEM_STATE)){
        	if (selectedFirstComboValue.equals("0")) {
        		return true;
        	} else if (selectedSecondComboValue.equals("0")) {
        		return true;
        	} else if (selectedThirdComboValue.equals("0")) {
        		return false;
        	}
        	return false;
        }
    	return true;
	}
	
	public boolean getDisableUpdateButton() {
        if (selectedView.equals(ITEM_TYPE)) {
        	if (selectedFirstComboValue.equals("0")) {
        		return true;
        	} else if (selectedSecondComboValue.equals("0")) {
        		return !getValidSelection();
        	} else if (selectedThirdComboValue.equals("0")) {
        		return false;
        	}
        	return true;
        }else if(selectedView.equals(ITEM_TAX_CODE)){
        	if (selectedFirstComboValue.equals("0")) {
        		return !getValidSelection();
        	} else if (selectedSecondComboValue.equals("0")) {
        		return true;
        	} else if (selectedThirdComboValue.equals("0")) {
        		return false;
        	}
        	return true;
        }else if(selectedView.equals(ITEM_STATE)){
        	if (selectedFirstComboValue.equals("0")) {
        		return true;
        	} else if (selectedSecondComboValue.equals("0")) {
        		return true;
        	} else if (selectedThirdComboValue.equals("0")) {
        		return !getValidSelection();
        	}
        	return true;
        }
    	return true;
	}
	
	public boolean getDisableDeleteButton() {
        if (selectedView.equals(ITEM_TYPE)) {
        	if (selectedFirstComboValue.equals("0")) {
        		return true;
        	} else if (selectedSecondComboValue.equals("0")) {
        		return !getValidSelection();
        	} else if (selectedThirdComboValue.equals("0")) {
        		return true;
        	}
        	return !getValidSelection();
        }else if(selectedView.equals(ITEM_TAX_CODE)){
        	if (selectedFirstComboValue.equals("0")) {
        		return !getValidSelection();
        	} else if (selectedSecondComboValue.equals("0")) {
        		return true;
        	} else if (selectedThirdComboValue.equals("0")) {
        		return true;
        	}
        	return !getValidSelection();
        }else if(selectedView.equals(ITEM_STATE)){
        	if (selectedFirstComboValue.equals("0")) {
        		return true;
        	} else if (selectedSecondComboValue.equals("0")) {
        		return true;
        	} else if (selectedThirdComboValue.equals("0")) {
        		return !getValidSelection();
        	}
        	return !getValidSelection();
        } else {
        	if (selectedFirstComboValue.equalsIgnoreCase("2")) {
        		return ((selectedCCHCodeSource == null) ||
        				!getFlagValue(selectedCCHCodeSource.getCustomFlag()));
        	} else if (selectedFirstComboValue.equalsIgnoreCase("1")) {
        		if (selectedSecondComboValue.equalsIgnoreCase("0")) {
            		return ((selectedCCHGroupSource == null) ||
            				!getFlagValue(selectedCCHGroupSource.getCustomFlag()));
        		} else {
            		return ((selectedCCHGroupItemSource == null) ||
            				!getFlagValue(selectedCCHGroupItemSource.getCustomFlag()));
        		}
        	}
        }
    	return !getValidSelection();
	}
	
	static private boolean getFlagValue(String flag) {
		return ((flag != null) && flag.equals("1"));
	}
	
	public boolean getDisableViewButton() {
		return !getViewEnabled();
	}
	
	public boolean getUpdateStates() {
		return updateStates;
	}

	public boolean getAddAction() {
		return currentAction.isAddAction();
	}

	public boolean getDeleteAction() {
		return currentAction.equals(EditAction.DELETE);
	}
	
	public boolean getViewAction() {
		return currentAction.equals(EditAction.VIEW);
	}

	public boolean getUpdateAction() {
		return currentAction.equals(EditAction.UPDATE);
	}

	public String getActionText() {
		return currentAction.getActionText();
	}
	
	public TaxCodeStateDTO getSelectedState() {
		return selectedState;
	}
	
	public TaxCodeStateDTO getSelectedCountry() {
		return selectedCountry;
	}

	public void setSelectedCountry(TaxCodeStateDTO selectedCountry) {
		this.selectedCountry = selectedCountry;
	}
	
	public List<String> getStateMeasureTypes() {
		return stateMeasureTypes;
	}

	public void setStateMeasureTypes(List<String> stateMeasureTypes) {
		this.stateMeasureTypes = stateMeasureTypes;
	}

	public TaxCode getSelectedTaxCode() {
		return selectedTaxCode;
	}

	public ReferenceDocument getSelectedDocRef() {
		return selectedDocRef;
	}

	public ReferenceDetail getSelectedDocRefDetail() {
		return selectedDocRefDetail;
	}

	public CCHCode getSelectedCCHCode() {
		return selectedCCHCode;
	}

	public CCHTaxMatrix getSelectedCCHGroup() {
		return selectedCCHGroup;
	}

	public CCHTaxMatrix getSelectedCCHGroupItem() {
		return selectedCCHGroupItem;
	}

	private void resetSelection() {
		selectedStateIndex = -1;
		selectedCountryIndex = -1;
		selectedTaxCodeIndex = -1;
		selectedDocRefIndex = -1;
		selectedDocRefDetailIndex = -1;
		selectedDocRefTypeIndex = -1;
		selectedCCHCodeIndex = -1;
		selectedCCHGroupIndex = -1;
		selectedCCHGroupItemIndex = -1;
		
		selectedStateSource = null;
		selectedCountrySource = null;
		selectedTaxCodeSource = null;
		taxCodeTypeCode = null;
		selectedDocRefSource = null;
		selectedDocRefDetailSource = null;
		selectedDocRefTypeSource = null;
		selectedCCHCodeSource = null;
		selectedCCHGroupSource = null;
		selectedCCHGroupItemSource = null;
		url = null;
	}
	
	public void selectedStateChanged(ActionEvent e) throws AbortProcessingException { 
		HtmlDataTable table = (HtmlDataTable) ((HtmlAjaxSupport) e.getComponent()).getParent();
		this.selectedStateSource = (TaxCodeStateDTO) table.getRowData();
		this.selectedStateIndex = table.getRowIndex();
	}
	
	public void selectedCountryChanged(ActionEvent e) throws AbortProcessingException { 
		HtmlDataTable table = (HtmlDataTable) ((HtmlAjaxSupport) e.getComponent()).getParent();
		this.selectedCountrySource = (TaxCodeStateDTO) table.getRowData();
		this.selectedCountryIndex = table.getRowIndex();
	}

	public void selectedTypeChanged(ActionEvent e) throws AbortProcessingException { 
		if (getActiveTableType().equals(ActiveTableType.DOCREF_TYPE)) {
			HtmlDataTable table = (HtmlDataTable) ((HtmlAjaxSupport) e.getComponent()).getParent();
			this.selectedDocRefTypeSource = (ListCodes) table.getRowData();
			this.selectedDocRefTypeIndex = table.getRowIndex();
		}
	}

	public void selectedTaxCodeChanged(ActionEvent e) throws AbortProcessingException { 
		HtmlDataTable table = (HtmlDataTable) ((HtmlAjaxSupport) e.getComponent()).getParent();
		this.selectedTaxCodeSource = (TaxCode) table.getRowData();
		this.selectedTaxCodeIndex = table.getRowIndex();
	}

	public void selectedDocRefChanged(ActionEvent e) throws AbortProcessingException { 
		HtmlDataTable table = (HtmlDataTable) ((HtmlAjaxSupport) e.getComponent()).getParent();
		this.selectedDocRefSource = (ReferenceDocument) table.getRowData();
		this.selectedDocRefIndex = table.getRowIndex();
	}

	public void selectedDocRefDetailChanged(ActionEvent e) throws AbortProcessingException { 
		HtmlDataTable table = (HtmlDataTable) ((HtmlAjaxSupport) e.getComponent()).getParent();
		this.selectedDocRefDetailSource = (ReferenceDetail) table.getRowData();
		this.selectedDocRefSource = selectedDocRefDetailSource.getReferenceDetailPK().getRefDoc();
		this.selectedDocRefDetailIndex = table.getRowIndex();
	}

	public void selectedCCHCodeChanged(ActionEvent e) throws AbortProcessingException { 
		HtmlDataTable table = (HtmlDataTable) ((HtmlAjaxSupport) e.getComponent()).getParent();
		this.selectedCCHCodeSource = (CCHCode) table.getRowData();
		this.selectedCCHCodeIndex = table.getRowIndex();
	}

	public void selectedCCHGroupChanged(ActionEvent e) throws AbortProcessingException { 
		HtmlDataTable table = (HtmlDataTable) ((HtmlAjaxSupport) e.getComponent()).getParent();
		this.selectedCCHGroupSource = (CCHTaxMatrix) table.getRowData();
		this.selectedCCHGroupIndex = table.getRowIndex();
	}

	public void selectedCCHGroupItemChanged(ActionEvent e) throws AbortProcessingException { 
		HtmlDataTable table = (HtmlDataTable) ((HtmlAjaxSupport) e.getComponent()).getParent();
		this.selectedCCHGroupItemSource = (CCHTaxMatrix) table.getRowData();
		this.selectedCCHGroupItemIndex = table.getRowIndex();
	}

	public int getSelectedStateIndex() {
		return selectedStateIndex;
	}

	public int getSelectedTaxCodeIndex() {
		return selectedTaxCodeIndex;
	}

	public int getSelectedDocRefIndex() {
		return selectedDocRefIndex;
	}

	public int getSelectedDocRefDetailIndex() {
		return selectedDocRefDetailIndex;
	}

	public int getSelectedDocRefTypeIndex() {
		return selectedDocRefTypeIndex;
	}

	public int getSelectedCCHCodeIndex() {
		return selectedCCHCodeIndex;
	}

	public int getSelectedCCHGroupIndex() {
		return selectedCCHGroupIndex;
	}

	public int getSelectedCCHGroupItemIndex() {
		return selectedCCHGroupItemIndex;
	}

	public String viewAction() {
		resetSelection();
		
        if (selectedView.equals(ITEM_TYPE)) {
        	return "taxability_type";
        }else if(selectedView.equals(ITEM_TAX_CODE)){
        	return "taxability_taxcode";
        }else if(selectedView.equals(ITEM_STATE)){
        	return "taxability_state";
        }
        return "setup_supporting_codes";
	}
	
	private ActiveTableType getActiveTableType() {
        if (selectedView.equals(ITEM_TYPE)) {
        	if (selectedFirstComboValue.equals("0")) {
        		return ActiveTableType.TAXCODE_TYPE;
        	} else if (selectedSecondComboValue.equals("0")) {
        		return ActiveTableType.TAXCODE;
        	} else if (selectedThirdComboValue.equals("0")) {
        		return ActiveTableType.TAXCODE_STATE;
        	}
        	return ActiveTableType.DOCREF_DETAIL;
        }else if(selectedView.equals(ITEM_TAX_CODE)){
        	if (selectedFirstComboValue.equals("0")) {
        		return ActiveTableType.TAXCODE;
        	} else if (selectedSecondComboValue.equals("0")) {
        		return ActiveTableType.TAXCODE_TYPE;
        	} else if (selectedThirdComboValue.equals("0")) {
        		return ActiveTableType.TAXCODE_STATE;
        	}
        	return ActiveTableType.DOCREF_DETAIL;
        }else if(selectedView.equals(ITEM_STATE)){
        	if (selectedFirstComboValue.equals("0")) {
        		return ActiveTableType.TAXCODE_STATE;
        	} else if (selectedSecondComboValue.equals("0")) {
        		return ActiveTableType.TAXCODE_TYPE;
        	} else if (selectedThirdComboValue.equals("0")) {
        		return ActiveTableType.TAXCODE;
        	}
        	return ActiveTableType.DOCREF_DETAIL;
        }
        
    	if (selectedFirstComboValue.equals("0")) {
    	} else if (selectedFirstComboValue.equalsIgnoreCase("5")) { // state
       		return ActiveTableType.TAXCODE_STATE;
    	} else if (selectedFirstComboValue.equalsIgnoreCase("4")) { // ref
        	return ActiveTableType.DOCREF_TYPE;
    	} else if (selectedFirstComboValue.equalsIgnoreCase("3")) { // doc
        	return ActiveTableType.DOCREF;
    	} else if (selectedFirstComboValue.equalsIgnoreCase("2")) { // country
    		return ActiveTableType.TAXCODE_COUNTRY;
    	} else if (selectedFirstComboValue.equalsIgnoreCase("1")) { // cchGroup
    		if (selectedSecondComboValue.equals("0")) {
    			return ActiveTableType.CCHGROUP;
    		}
			return ActiveTableType.CCHGROUPITEM;
    	}
        
        return ActiveTableType.NONE;
	}
	
	public boolean getValidSelection() {
		switch (getActiveTableType()) {
			case TAXCODE_TYPE:
				return false;
			case TAXCODE_STATE:
				return (selectedStateSource != null);
			case TAXCODE:
				return (selectedTaxCodeSource != null);
			case DOCREF:
				return (selectedDocRefSource != null);
			case DOCREF_TYPE:
				return (selectedDocRefTypeSource != null);
			case DOCREF_DETAIL:
				return (selectedDocRefDetailSource != null);
			case CCHCODE:
				return (selectedCCHCodeSource != null);
			case CCHGROUP:
				return (selectedCCHGroupSource != null);
			case CCHGROUPITEM:
				return (selectedCCHGroupItemSource != null);
		}
		
		return false;
	}
	
	public boolean getViewEnabled() {
		return (selectedDocRefSource != null);
	}
	
	private String displayAction() {
		switch (getActiveTableType()) {
			case TAXCODE_TYPE:
			case DOCREF_TYPE:
				return null;
			case TAXCODE_STATE:
				if (selectedView.equals(ITEM_SUPPORTING_CODE)) {
					selectedState = selectedStateSource;
					this.selectedCountryComboValue = selectedState.getCountry();
					this.selectedLocalTaxabilityValue = selectedState.getLocalTaxabilityCode();
					selectedState.setModifiedFlag("1");
					return "taxability_state_add";
				} else {
					updateStates = true;
					TaxCodePK id = new TaxCodePK();
					id.setTaxcodeCode(getCurrentTaxCode());
					return initTaxCode(taxCodeService.findById(id));
				}
			case TAXCODE:
				return initTaxCode(selectedTaxCodeSource);
			case DOCREF:
				selectedDocRef = selectedDocRefSource;
				return "taxability_docref_add";
			case DOCREF_DETAIL:
				selectedDocRefDetail = selectedDocRefDetailSource;
				return "taxability_docref_taxcode_add";
			case CCHCODE:
				selectedCCHCode = selectedCCHCodeSource;
				return "taxability_cchcode_add";
			case CCHGROUP:
				selectedCCHGroup = selectedCCHGroupSource; 
				return "taxability_cchgroup_add";
			case CCHGROUPITEM:
				selectedCCHGroupItem = selectedCCHGroupItemSource; 
				return "taxability_cchgroupitem_add";
		}
		
		return null;
	}
	
	private String initTaxCode(TaxCode taxCode) {
		// Determine current selections
		selectedTaxCode = taxCode;
		taxCodeTypeCode = null;
		stateSelection.clear();
		stateJurisdiction.clear();
		TaxCodeDetail example = new TaxCodeDetail();
		example.setTaxcodeCode(selectedTaxCode.getTaxCodePK().getTaxcodeCode());
		for (TaxCodeDetail detail : taxCodeDetailService.findByExample(example, 0)) {
			stateSelection.put(detail.getTaxcodeStateCode()+"-"+detail.getTaxcodeCountryCode(), new Boolean(true));
			stateJurisdiction.put(detail.getTaxcodeStateCode()+"-"+detail.getTaxcodeCountryCode(), null);
		}
		return "taxability_taxcode_add";
	}
	
	public String displayRefTypeViewAction() {
		refTypeListForFilter = null;
		selectedDocRefTypeIndex = -1;
		return listCodesBean.externalViewAction(selectedDocRefTypeSource.getListCodesDTO(), "setup_supporting_codes");
	}
	
	public void displayViewAction() {
		String url = selectedDocRefSource.getUrl();
		logger.debug("File url : " + url);
		try {
			if(url!=null&&(url.startsWith("http:") || url.startsWith("https:"))) {
				setUrl(url);
			}
			else if (url!=null &&(url.startsWith("www") || url.startsWith("WWW"))) {
				//Runtime.getRuntime().exec("rundll32 url.dll,FileProtocolHandler "+url); opens in same window
				//Runtime.getRuntime().exec("\"C:\\Program Files\\Internet Explorer\\IEXPLORE.EXE\" " + url);
				
				setUrl("http://"+url);
				
			} else {
				setUrl(null);
				url=getRefDocPath();
				String path = url + "\\" + selectedDocRefSource.getDocName();
				logger.debug("File path : " + path);
				logger
						.debug("File name : "
								+ selectedDocRefSource.getDocName());
				try {

					File file = new File(path);
					FacesContext context = FacesContext.getCurrentInstance();
					HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
					FileInputStream istr = new FileInputStream(path);
					BufferedInputStream bstr = new BufferedInputStream(istr);
					int contentLength = (int) file.length(); // get the file
																// size (in
																// bytes)
					byte[] data = new byte[contentLength]; // allocate byte
															// array of right
															// size
					bstr.read(data, 0, contentLength); // read into byte array
					bstr.close();
					istr.close();
					response.setContentType("application/octet-stream");
					response.setContentLength(contentLength);
					response.setHeader("Content-disposition",
							"attachment; filename=\""
									+ selectedDocRefSource.getDocName() + "\"");
					ServletOutputStream out = response.getOutputStream();
					out.write(data);
					out.flush();
					out.close();
					context.responseComplete();
				} catch (FileNotFoundException fe) {
					logger.debug("File not found");
					FacesMessage msg = new FacesMessage("Path not found");
					msg.setSeverity(FacesMessage.SEVERITY_ERROR);
					FacesContext.getCurrentInstance().addMessage(null, msg);
				}
			}
		} catch (IOException e) {
			logger.debug("Error reading file");
			FacesMessage msg = new FacesMessage("Path not found");
			msg.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, msg);
		}
	}
	
	public String displayUpdateAction() {
		currentAction = EditAction.UPDATE;
		updateStates = false;
		ActiveTableType table = getActiveTableType();
		if (table.equals(ActiveTableType.DOCREF_TYPE)) { 
			refTypeList = null;
			return listCodesBean.externalUpdateAction(selectedDocRefTypeSource.getListCodesDTO(), viewAction());
		}
		return displayAction();
	}
	
	public String displayDeleteAction() {
		currentAction = EditAction.DELETE;
		ActiveTableType table = getActiveTableType();
		if (table.equals(ActiveTableType.DOCREF_TYPE)) { 
			refTypeList = null;
			return listCodesBean.externalDeleteAction(selectedDocRefTypeSource.getListCodesDTO(), viewAction());
		}
		return displayAction();
	}	
	
	private String getRefDocPath(){
		OptionCodePK optionCodePK = new OptionCodePK();
		optionCodePK.setOptionCode("REFDOCFILEPATH");
		optionCodePK.setOptionTypeCode("SYSTEM");
		optionCodePK.setUserCode("SYSTEM");
		Option option = optionService.findByPK(optionCodePK);
		
		if(option.getValue().endsWith("\\*STATE")){
			String tempValue = option.getValue().substring(0, option.getValue().lastIndexOf("\\*STATE"));
			return tempValue;
		}else {
			return option.getValue();
		}
	}

	public String displayAddAction() {
		currentAction = EditAction.ADD;
		switch (getActiveTableType()) {
			case TAXCODE_TYPE:
				tcTypeList = null;
				return listCodesBean.externalAddAction("TCTYPE", viewAction());
			case TAXCODE_STATE:
				selectedState =  new TaxCodeStateDTO();
				selectedState.setActiveBooleanFlag(true);
			//	selectedState.setTaxCodeState("");
				System.out.println("xxxxx selectedState.getTaxCodeState():  " + selectedState.getTaxCodeState());
				System.out.println("xxxxx selectedState.getCountry():  " + selectedState.getCountry());
				
				this.selectedCountryComboValue = "";
				
				this.stateMeasureTypes.clear();
				this.stateMeasureTypes.add("0");
				return "taxability_state_add";
			case TAXCODE:
			{
				selectedTaxCode = new TaxCode();
				//Default to the first item, like TPP
				Iterator<SelectItem> iter = getMeasureTypeList().iterator(); 
				if(iter.hasNext()){
					SelectItem detail = iter.next();
				}

				TaxCodePK id = new TaxCodePK();
				String typeCode = getCurrentTypeCode();
				selectedTaxCode.setTaxCodePK(id);
				taxCodeTypeCode = null;
				updateStates = false;
				return "taxability_taxcode_add";
			}
			case DOCREF:
				selectedDocRef = new ReferenceDocument();
				selectedDocRef.setUrl(getRefDocPath());
				return "taxability_docref_add";
			case DOCREF_TYPE:
				refTypeList = null;
				return listCodesBean.externalAddAction("REFTYPE", viewAction());
			case DOCREF_DETAIL:
			{
				selectedDocRefDetail = new ReferenceDetail();
				ReferenceDetailPK id = new ReferenceDetailPK();
				TaxCodeDetail example = new TaxCodeDetail();
				example.setTaxcodeCode(getCurrentTaxCode());
				
				//0000024: Add Federal Level into PinPoint, format here AL-US
				String stateCountry = getCurrentStateCode();
				String state = stateCountry.substring(0,2);
				String country = stateCountry.substring(3,5);
				example.setTaxcodeStateCode(state);
				example.setTaxcodeCountryCode(country);
				
				example.setTaxcodeTypeCode(getCurrentTypeCode());
				List<TaxCodeDetail> list = taxCodeDetailService.findByExample(example, 0);
				if ((list != null) && (list.size() > 0)) {
					if (list.size() > 1) {
						logger.warn("Multiple TaxCodeDetail records found for " 
								+ example.getTaxcodeStateCode() 
								+ " - " + example.getTaxcodeTypeCode()
								+ " - " + example.getTaxcodeCode());
					}
					id.setTaxcodeDetail(list.get(0));
				} else {
					logger.error("TaxCodeDetail record not found for " 
							+ example.getTaxcodeStateCode() 
							+ " - " + example.getTaxcodeTypeCode()
							+ " - " + example.getTaxcodeCode());
					id.setTaxcodeDetail(new TaxCodeDetail());
				}
				id.setRefDoc(new ReferenceDocument());
				selectedDocRefDetail.setReferenceDetailPK(id);
				return "taxability_docref_taxcode_add";
			}
			case CCHCODE:
			{
				selectedCCHCode = new CCHCode();
				CCHCodePK id = new CCHCodePK();
				id.setFileName("DETAIL");
				id.setFieldName("TAXCAT");
				selectedCCHCode.setCodePK(id);
				return "taxability_cchcode_add";
			}
			case CCHGROUP:
			{
				selectedCCHGroup = new CCHTaxMatrix();
				CCHTaxMatrixPK id = new CCHTaxMatrixPK();
				id.setRecType("G");
				id.setItem("000");
				selectedCCHGroup.setCchTaxMatrixPK(id);
				return "taxability_cchgroup_add";
			}
			case CCHGROUPITEM:
			{
				// Find current group
				CCHTaxMatrixPK id = new CCHTaxMatrixPK();
				id.setRecType("G");
				id.setGroupCode(selectedSecondComboValue);
				id.setItem("000");
				CCHTaxMatrix group = cchTaxMatrixService.findById(id);
				
				// Create new item
				selectedCCHGroupItem = new CCHTaxMatrix();
				id = new CCHTaxMatrixPK();
				id.setRecType("I");
				id.setGroupCode(selectedSecondComboValue);
				selectedCCHGroupItem.setCchTaxMatrixPK(id);
				selectedCCHGroupItem.setGroupDesc((group == null)? null:group.getGroupDesc());
				return "taxability_cchgroupitem_add";
			}
		}
		
		return null;
	}
	
	public String okQuickUpdateAction(){
		
		ActiveTableType t = getActiveTableType();
		
		if(t.equals(ActiveTableType.TAXCODE_STATE)) {
			List<TaxCodeStateDTO> quickUpdateList = new ArrayList<TaxCodeStateDTO>(); 
			for(TaxCodeStateDTO state : stateQuickUpdateList) {	
				if(state.getActiveBooleanFlag() != state.getActiveTempBooleanFlag()){
					state.setActiveBooleanFlag(state.getActiveTempBooleanFlag());
					quickUpdateList.add(state);
				}
			}
			
			//If active flag changed, do update
			if(quickUpdateList.size()>0){
				taxCodeStateService.batchQuickUpdate(quickUpdateList);
			}
		}
		else if(t.equals(ActiveTableType.TAXCODE_COUNTRY)) {
			List<TaxCodeStateDTO> quickUpdateList = new ArrayList<TaxCodeStateDTO>(); 
			for(TaxCodeStateDTO state : countryQuickUpdateList) {	
				if(state.getActiveBooleanFlag() != state.getActiveTempBooleanFlag()){
					state.setActiveBooleanFlag(state.getActiveTempBooleanFlag());
					quickUpdateList.add(state);
				}
			}
			
			//If active flag changed, do update
			if(quickUpdateList.size()>0){
				taxCodeStateService.batchQuickUpdate(quickUpdateList);
			}
		}
		
		stateList = null;
		taxCodeStateChoices = null;
		cacheManager.flushTaxCodeCountryStateMap();
		resetSelection();
		refreshCurrentTable();
		
		//Notify state combo box to refresh
		StateObservable.getInstance().processUpdate();
		
		searchFilter();

		return viewAction();
	}
	
	public String okStateAction() {		
		switch (currentAction) {
			case ADD:
				String error1Message = "";
				if(selectedCountryComboValue==null || selectedCountryComboValue.length()==0){
					error1Message = "Country cannot be empty. ";	
				}
				
				if(selectedState.getTaxCodeState()==null || selectedState.getTaxCodeState().length()==0){
					error1Message = error1Message + "State Code cannot be empty. ";
				}
				
				if(error1Message.length()>0){
					FacesMessage message = new FacesMessage(error1Message);
					message.setSeverity(FacesMessage.SEVERITY_ERROR);
					FacesContext.getCurrentInstance().addMessage(null, message);
					return null;
				}
								
				TaxCodeStatePK aPK = new TaxCodeStatePK();
				aPK.setCountry(selectedCountryComboValue);
				aPK.setTaxcodeStateCode(selectedState.getTaxCodeState());
				TaxCodeStateDTO taxCodeStateDTO = taxCodeStateService.findByPK(aPK);
				
				if(taxCodeStateDTO==null){
					selectedState.setCountry(selectedCountryComboValue);
					selectedState.setLocalTaxabilityCode(selectedLocalTaxabilityValue);
					selectedState.setCpCertLevelCode(selectedCertificateLevelValue);
					taxCodeStateService.save(selectedState);
				}
				else{
					FacesMessage message = new FacesMessage("Country and State Code must be unique.");
					message.setSeverity(FacesMessage.SEVERITY_ERROR);
					FacesContext.getCurrentInstance().addMessage(null, message);
					return null;
				}
				break;
				
			case DELETE:
				boolean JurisdictionInUsed = taxCodeStaeInJurisdiction(selectedState.getTaxCodeState());
				boolean GLExtractMapInUsed = taxCodeStateInGLExtractMap(selectedState.getTaxCodeState());
				boolean TaxCodeDetailInUsed = taxCodeStaeInTaxCodeDetail(selectedState.getTaxCodeState());
				if(JurisdictionInUsed || GLExtractMapInUsed || TaxCodeDetailInUsed){
					String errorMessage = "";
					
					if(JurisdictionInUsed){
						if(errorMessage.length()==0){
							errorMessage = "Jurisdiction";
						}
						else{
							errorMessage = errorMessage + ", Jurisdiction";
						}
					}
					
					if(GLExtractMapInUsed){
						if(errorMessage.length()==0){
							errorMessage = "GLExtractMap";
						}
						else{
							errorMessage = errorMessage + ", GLExtractMap";
						}
					}
					
					if(TaxCodeDetailInUsed){
						if(errorMessage.length()==0){
							errorMessage = "TaxCodeDetail";
						}
						else{
							errorMessage = errorMessage + ", TaxCodeDetail";
						}
					}	
					
					FacesMessage message = new FacesMessage("Cannot remove TaxCode state record that is used in " + errorMessage + ".");
					message.setSeverity(FacesMessage.SEVERITY_ERROR);
					FacesContext.getCurrentInstance().addMessage(null, message);
					
					logger.info("Cannot remove TaxCode state record that is in use. "+selectedState.getTaxCodeState());
					return null;
				}else {
					logger.info("WILL BE REMOVED");
					TaxCodeStatePK bPK = new TaxCodeStatePK();
					bPK.setCountry(selectedState.getCountry());
					bPK.setTaxcodeStateCode(selectedState.getTaxCodeState());
					taxCodeStateService.remove(bPK);
					break;
				}
				
				
			case UPDATE:
				
				selectedState.setCountry(selectedCountryComboValue);
				selectedState.setLocalTaxabilityCode(selectedLocalTaxabilityValue);
				selectedState.setCpCertLevelCode(selectedCertificateLevelValue);
				taxCodeStateService.update(selectedState);
				break;
		}
	
		stateList = null;
		taxCodeStateChoices = null;
		cacheManager.flushTaxCodeCountryStateMap();
		resetSelection();
		refreshCurrentTable();
		
		//Notify state combo box to refresh
		StateObservable.getInstance().processUpdate();
		
		searchFilter();

		return viewAction();
	}
	
	public String okCountryAction() {	
			
		switch (currentAction) {
			case ADD:
				String error1Message = "";

				if(selectedCountry.getCountry()==null || selectedCountry.getCountry().length()==0){
					error1Message = error1Message + "Country Code cannot be empty. ";
				}
				
				if(selectedCountry.getName()==null || selectedCountry.getName().length()==0){
					error1Message = error1Message + "Country Name cannot be empty. ";
				}
				
				if(error1Message.length()>0){
					FacesMessage message = new FacesMessage(error1Message);
					message.setSeverity(FacesMessage.SEVERITY_ERROR);
					FacesContext.getCurrentInstance().addMessage(null, message);
					return null;
				}
				
				List<TaxCodeStateDTO> list = null;
				String c = selectedCountry.getCountry().toUpperCase();
				if(c!=null && !c.equals("")){
					list=taxCodeStateService.findByCountryName(c);
				}
			
				if(list==null || list.size()==0){
					selectedCountry.setCountry(selectedCountry.getCountry().toUpperCase());
					selectedCountry.setTaxCodeState("*COUNTRY");
					taxCodeStateService.save(selectedCountry);
				}
				else{
					FacesMessage message = new FacesMessage("Country Code must be unique.");
					message.setSeverity(FacesMessage.SEVERITY_ERROR);
					FacesContext.getCurrentInstance().addMessage(null, message);
					return null;
				}
				break;
				
			case DELETE:
				boolean isCountryUsed = taxCodeStateService.isCountryUsed(selectedCountry.getCountry());
				
				if(isCountryUsed){
					FacesMessage message = new FacesMessage(selectedCountry.getCountry() + " cannot be deleted because states exist. Delete states first.");
					message.setSeverity(FacesMessage.SEVERITY_ERROR);
					FacesContext.getCurrentInstance().addMessage(null, message);
					
					return null;				
				}
					
				taxCodeStateService.deleteCountry(selectedCountry);
				break;
	
			case UPDATE:
				taxCodeStateService.updateCountry(selectedCountry);
				break;
				
			case VIEW:
				break;
		}
		
		//Update countries
		enteredStateCountry = "";
		countryItemsForFilter = null;
		cacheManager.flushEntryCache("COUNTRYCODE_MAP");
	
		stateList = null;
		taxCodeStateChoices = null;
		cacheManager.flushTaxCodeCountryStateMap();
		resetSelection();
		refreshCurrentTable();
		
		//Notify state combo box to refresh
		StateObservable.getInstance().processUpdate();
		
		searchFilter();

		return viewAction();
	}
	
	//0001102
	public boolean taxCodeStaeInTaxCodeDetail(String state){
		if(taxCodeDetailService.findTaxCodeTypeForState(state).size()>0){
			logger.info("USED IN TB_TAXCODE_DETAIL	");
			return true;
		}else {
			logger.info("NOT  USED IN TB_TAXCODE_DETAIL");
			return false;
		}
	}
	
	//4146
	public boolean taxCodeStaeInJurisdiction(String state){
		if(jurisdictionService.findByTaxCodeStateCode(state).size()>0){
			logger.info("USED IN JURISDICTION	");
			return true;
		}else {
			logger.info("NOT  USED IN JURISDICTION");
			return false;
		}
	}
	
	//4146 
	public boolean taxCodeStateInGLExtractMap(String state){
		List<GlExtractMap> list = glExtractMapService.findByStateCode(state);
		if(list.size() >0){
			logger.info("USE IN GLEXTRACTMAP");
			return true;
		}else {
			logger.info("NOT  USED IN GLEXTRACTMAP");
			return false;
		}
	}

// Bug 4147	
	private TaxCodeDetail addToDetail(TaxCode selectedTaxCode) {
		logger.info(selectedFirstComboValue);
		TaxCodeDetail taxCodeDetail = new TaxCodeDetail();
		taxCodeDetail.setTaxcodeStateCode(selectedFirstComboValue);
		taxCodeDetail.setTaxcodeCode(selectedTaxCode.getTaxcodeCode());
		return taxCodeDetail;
	}
	
	public String okTaxCodeAction() {
		switch (currentAction) {
			case ADD:
				taxCodeService.save(selectedTaxCode);
				if(selectedView.equals(ITEM_STATE)) { //bug 4290 says "only add detail when in state mode..."
					TaxCodeDetail taxCodeDetail = addToDetail(selectedTaxCode);
					taxCodeDetailService.save(taxCodeDetail);
				}
				break;
				
			case DELETE:
				try {
					taxCodeDetailService.removeTaxCode(selectedTaxCode);
				} catch (Exception ex) {
					FacesMessage message = new FacesMessage("Cannot remove TaxCode detail record that is in use.");
					message.setSeverity(FacesMessage.SEVERITY_ERROR);
					FacesContext.getCurrentInstance().addMessage(null, message);
					return null;
				}
				try {
					taxCodeService.remove(selectedTaxCode);
				} catch (ConstraintViolationException ex) {
					FacesMessage message = new FacesMessage("Cannot remove TaxCode record that is in use.");
					message.setSeverity(FacesMessage.SEVERITY_ERROR);
					FacesContext.getCurrentInstance().addMessage(null, message);
					return null;
				}
				break;
				
			case UPDATE:
				try {
					taxCodeDetailService.updateDetail(selectedTaxCode, stateSelection.keySet(), stateJurisdiction);
				} catch (ConstraintViolationException ex) {
					FacesMessage message = new FacesMessage("Cannot remove TaxCode detail record that is in use.");
					message.setSeverity(FacesMessage.SEVERITY_ERROR);
					FacesContext.getCurrentInstance().addMessage(null, message);
					return null;
				}catch (Exception ex) {
					FacesMessage message = new FacesMessage("Cannot remove TaxCode detail record that is in use.");
					message.setSeverity(FacesMessage.SEVERITY_ERROR);
					FacesContext.getCurrentInstance().addMessage(null, message);
					return null;
				}
				taxCodeService.update(selectedTaxCode);
				break;
		}
		
		taxCodeList = null;
		cacheManager.flushTaxCodeMap();
		resetSelection();
		refreshCurrentTable();
		return viewAction();
	}

	public String okDocRefAction() {
		switch (currentAction) {
			case ADD:
			case UPDATE:
				String name = selectedDocRef.getDocName();
				String url= selectedDocRef.getUrl();
				String key= selectedDocRef.getKeyWords();
				
				if (((name == null) || name.trim().equals("")) && ((url == null) || url.trim().equals(""))) {
					FacesMessage message = new FacesMessage("Document Name or Path/URL is required.");
					message.setSeverity(FacesMessage.SEVERITY_ERROR);
					FacesContext.getCurrentInstance().addMessage(null, message);
					return null;
				}
				/*if (((name != null) && !name.trim().equals("")) && ((url == null) || url.trim().equals(""))) {
					FacesMessage message = new FacesMessage("Path/URL is required.");
					message.setSeverity(FacesMessage.SEVERITY_ERROR);
					FacesContext.getCurrentInstance().addMessage(null, message);
					return null;
				}*/
				
				if ((key != null) && (key.trim().length()>100)) {
					FacesMessage message = new FacesMessage("Keywords cannot be longer than 100 characters.");
					message.setSeverity(FacesMessage.SEVERITY_ERROR);
					FacesContext.getCurrentInstance().addMessage(null, message);
					return null;
				}
				if ((name != null) && (name.trim().length()>50)) {
					FacesMessage message = new FacesMessage("Document Name cannot be longer than 50 characters.");
					message.setSeverity(FacesMessage.SEVERITY_ERROR);
					FacesContext.getCurrentInstance().addMessage(null, message);
					return null;
				}
				if ((url != null) && (url.trim().length()>255)) {
					FacesMessage message = new FacesMessage("Path/URL cannot be longer than 255 characters.");
					message.setSeverity(FacesMessage.SEVERITY_ERROR);
					FacesContext.getCurrentInstance().addMessage(null, message);
					return null;
				}
				
				break;
		}
		
		switch (currentAction) {
			case ADD:
				referenceDocumentService.save(selectedDocRef);
				if(this.uploadedFile!=null){
					try{
						saveRefDocFile(this.uploadedFile);
					}catch (Exception e) {
						// TODO: handle exception
					}
				}
				break;
				
			case DELETE:
				try {
					referenceDocumentService.remove(selectedDocRef);
				} catch (ConstraintViolationException ex) {
					FacesMessage message = new FacesMessage("Cannot remove reference document record that is in use.");
					message.setSeverity(FacesMessage.SEVERITY_ERROR);
					FacesContext.getCurrentInstance().addMessage(null, message);
					return null;
				}
				break;
				
			case UPDATE:
				referenceDocumentService.update(selectedDocRef);
				break;
		}
		
		docList = null;
		resetSelection();
		refreshCurrentTable();
		searchFilter();
		return viewAction();
	}
	
	public String okDocRefDetailAction() {
		switch (currentAction) {
			case ADD:
				ReferenceDocument refDoc = referenceDocumentService.findById(selectedDocRefDetail.getReferenceDetailPK().getRefDoc().getRefDocCode());
				if (refDoc == null) {
					FacesMessage message = new FacesMessage("Document not found.");
					message.setSeverity(FacesMessage.SEVERITY_ERROR);
					FacesContext.getCurrentInstance().addMessage(null, message);
					return null;
				}
				selectedDocRefDetail.getReferenceDetailPK().setRefDoc(refDoc);
				try {
					taxCodeDetailService.addDocumentReference(selectedDocRefDetail);
				} catch (DataIntegrityViolationException ex) {
					FacesMessage message = new FacesMessage("Reference already exists.");
					message.setSeverity(FacesMessage.SEVERITY_ERROR);
					FacesContext.getCurrentInstance().addMessage(null, message);
					return null;
				}
				break;
				
			case DELETE:
				taxCodeDetailService.removeDocumentReference(selectedDocRefDetail);
				break;
		}
		
		docRefList = null;
		resetSelection();
		refreshCurrentTable();
		return viewAction();
	}
	
	public String okCCHCodeAction() {
		switch (currentAction) {
			case ADD:
				selectedCCHCode.setCustomFlag("1");
				selectedCCHCode.setModifiedFlag("0");
				cchCodeService.save(selectedCCHCode);
				break;
				
			case DELETE:
				String customFlag = selectedCCHCode.getCustomFlag();
				if ((customFlag == null) || !customFlag.equals("1")) {
					FacesMessage message = new FacesMessage("Only custom codes can be deleted.");
					message.setSeverity(FacesMessage.SEVERITY_ERROR);
					FacesContext.getCurrentInstance().addMessage(null, message);
					return null;
				}
				try {
					cchCodeService.remove(selectedCCHCode);
				} catch (ConstraintViolationException ex) {
					FacesMessage message = new FacesMessage("Cannot remove CCH code detail record that is in use.");
					message.setSeverity(FacesMessage.SEVERITY_ERROR);
					FacesContext.getCurrentInstance().addMessage(null, message);
					return null;
				}
				break;
				
			case UPDATE:
				selectedCCHCode.setModifiedFlag("1");
				cchCodeService.update(selectedCCHCode);
				break;
		}
		
		cchCodeList = null;
		cacheManager.flushCchCodeMap();
		resetSelection();
		refreshCurrentTable();
		return viewAction();
	}
	
	public String okCCHGroupAction() {
		switch (currentAction) {
			case ADD:
				selectedCCHGroup.setCustomFlag("1");
				selectedCCHGroup.setModifiedFlag("0");
				cchTaxMatrixService.save(selectedCCHGroup);
				break;
				
			case DELETE:
				String customFlag = selectedCCHGroup.getCustomFlag();
				if ((customFlag == null) || !customFlag.equals("1")) {
					FacesMessage message = new FacesMessage("Only custom groups can be deleted.");
					message.setSeverity(FacesMessage.SEVERITY_ERROR);
					FacesContext.getCurrentInstance().addMessage(null, message);
					return null;
				}
				try {
					cchTaxMatrixService.remove(selectedCCHGroup);
				} catch (ConstraintViolationException ex) {
					FacesMessage message = new FacesMessage("Cannot remove CCH Group record that is in use.");
					message.setSeverity(FacesMessage.SEVERITY_ERROR);
					FacesContext.getCurrentInstance().addMessage(null, message);
					return null;
				}
				break;
				
			case UPDATE:
				selectedCCHGroup.setModifiedFlag("1");
				cchTaxMatrixService.update(selectedCCHGroup);
				break;
		}
		
		cchGroupList = null;
		cacheManager.flushCchGroupMap();
		resetSelection();
		refreshCurrentTable();
		return viewAction();
	}
	
	public String okCCHGroupItemAction() {
		switch (currentAction) {
			case ADD:
				selectedCCHGroupItem.setCustomFlag("1");
				selectedCCHGroupItem.setModifiedFlag("0");
				cchTaxMatrixService.save(selectedCCHGroupItem);
				break;
				
			case DELETE:
				String customFlag = selectedCCHGroupItem.getCustomFlag();
				if ((customFlag == null) || !customFlag.equals("1")) {
					FacesMessage message = new FacesMessage("Only custom items can be deleted.");
					message.setSeverity(FacesMessage.SEVERITY_ERROR);
					FacesContext.getCurrentInstance().addMessage(null, message);
					return null;
				}
				try {
					cchTaxMatrixService.remove(selectedCCHGroupItem);
				} catch (ConstraintViolationException ex) {
					FacesMessage message = new FacesMessage("Cannot remove CCH Item record that is in use.");
					message.setSeverity(FacesMessage.SEVERITY_ERROR);
					FacesContext.getCurrentInstance().addMessage(null, message);
					return null;
				}
				break;
				
			case UPDATE:
				selectedCCHGroupItem.setModifiedFlag("1");
				cchTaxMatrixService.update(selectedCCHGroupItem);
				break;
		}
		
		cchGroupItemsList = null;
		resetSelection();
		refreshCurrentTable();
		return viewAction();
	}
	
	public void searchJurisdictionCallback() {
		// TODO Auto-generated method stub
		Long id = jurisdictionHandler.getSearchJurisdictionResultId();
		stateJurisdiction.put(jurisdictionState+"-"+jurisdictionCountry, id);
		stateSelection.put(jurisdictionState+"-"+jurisdictionCountry, new Boolean(true));
	}
	
	public boolean getIsTaxable() {
		return ((taxCodeTypeCode != null) && taxCodeTypeCode.equalsIgnoreCase("T"));
	}
	
	public void taxCodeTypeChange(ValueChangeEvent e) {
		taxCodeTypeCode = (String) e.getNewValue();
	}
	
	//0000024: Add Federal Level into PinPoint
	/*
	public void validateStateCode(FacesContext context, UIComponent toValidate, Object value) {
		if (currentAction.isAddAction()) {
		    // Ensure name is unique
			String code = (String) value;
			if (taxCodeStateService.findById(code) != null) {
				((UIInput)toValidate).setValid(false);
				FacesMessage message = new FacesMessage("State Code already exists.");
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				context.addMessage(toValidate.getClientId(context), message);
		    }
		}
	}*/

	public void validateCCHCode(FacesContext context, UIComponent toValidate, Object value) {
		if (currentAction.isAddAction()) {
		    // Ensure name is unique
			String code = (String) value;
			CCHCodePK id = new CCHCodePK();
			id.setFileName("DETAIL");
			id.setFieldName("TAXCAT");
			id.setCode(code);
			if (cchCodeService.findById(id) != null) {
				((UIInput)toValidate).setValid(false);
				FacesMessage message = new FacesMessage("CCH Tax Category already exists.");
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				context.addMessage(toValidate.getClientId(context), message);
		    }
		}
	}

	public void validateCCHGroupCode(FacesContext context, UIComponent toValidate, Object value) {
		if (currentAction.isAddAction()) {
			CCHTaxMatrixPK id = new CCHTaxMatrixPK();
			id.setRecType("G");
			id.setGroupCode((String) value);
			id.setItem("000");
			if (cchTaxMatrixService.findById(id) != null) {
				((UIInput)toValidate).setValid(false);
				FacesMessage message = new FacesMessage("CCH Group already exists.");
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				context.addMessage(toValidate.getClientId(context), message);
			}
	    }
	}

	public void validateCCHGroupItemCode(FacesContext context, UIComponent toValidate, Object value) {
	    // Ensure name is unique
		if (currentAction.isAddAction()) {
			CCHTaxMatrixPK id = new CCHTaxMatrixPK();
			id.setRecType("I");
			id.setGroupCode(selectedCCHGroupItem.getCchTaxMatrixPK().getGroupCode());
			id.setItem((String) value);
			if (cchTaxMatrixService.findById(id) != null) {
				((UIInput)toValidate).setValid(false);
				FacesMessage message = new FacesMessage("CCH Item already exists.");
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				context.addMessage(toValidate.getClientId(context), message);
			}
	    }
	}

	public void validateTaxCode(FacesContext context, UIComponent toValidate, Object value) {
	    // Ensure name is unique
		if (currentAction.isAddAction()) {
			TaxCodePK id = new TaxCodePK();
			id.setTaxcodeCode((String) value);
			if (taxCodeService.findById(id) != null) {
				((UIInput)toValidate).setValid(false);
				FacesMessage message = new FacesMessage("Tax Code already exists.");
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				context.addMessage(toValidate.getClientId(context), message);
			}
	    }
	}

	public void validateDocRefCode(FacesContext context, UIComponent toValidate, Object value) {
	    // Ensure name is unique
		if (currentAction.isAddAction()) {
			String id = (String) value;
			if (referenceDocumentService.findById(id) != null) {
				((UIInput)toValidate).setValid(false);
				FacesMessage message = new FacesMessage("Reference Code already exists.");
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				context.addMessage(toValidate.getClientId(context), message);
			}
	    }
	}

	public List<SelectItem> getMeasureTypeList() {
		if (measureTypeList == null) {
			measureTypeList = new ArrayList<SelectItem>();
			for (ListCodes code : cacheManager.getListCodesByType("MEASURETYP")) {
				measureTypeList.add(new SelectItem(code.getCodeCode(), code.getDescription()));
			}
			Collections.sort(measureTypeList, new MeasureTypeListComparator());
		}
		return measureTypeList;
	}

	public List<SelectItem> getTcTypeList() {
		if (tcTypeList == null) {
			tcTypeList = new ArrayList<SelectItem>();
			tcTypeList.add(new SelectItem("", "Select Tax Type"));
			for (ListCodes code : cacheManager.getListCodesByType("TCTYPE")) {
				tcTypeList.add(new SelectItem(code.getCodeCode(), code.getCodeCode() + " - " + code.getDescription()));
			}
		}
		return tcTypeList;
	}
	
	public List<SelectItem> getTaxTypeList() {
		if (taxTypeList == null) {
			taxTypeList = new ArrayList<SelectItem>();
			taxTypeList.add(new SelectItem("", "Select Tax Type"));
			for (ListCodes code : cacheManager.getListCodesByType("TAXTYPE")) {
				taxTypeList.add(new SelectItem(code.getCodeCode(), code.getCodeCode() + " - " + code.getDescription()));
			}
		}
		return taxTypeList;
	}
	
	public List<SelectItem> getRefTypeList() {
		if (refTypeList == null) {
			refTypeList = new ArrayList<SelectItem>();
			refTypeList.add(new SelectItem("", "Select Type Code"));
			for (ListCodes code : cacheManager.getListCodesByType("REFTYPE")) {
				refTypeList.add(new SelectItem(code.getCodeCode(), code.getCodeCode() + " - " + code.getDescription()));
			}
		}
		return refTypeList;
	}
	
	public List<TaxCodeStateDTO> getTaxCodeStateChoices() {
		if (taxCodeStateChoices == null) {
			// TODO filter by measure type?  if so, it becomes selection dependent!
			taxCodeStateChoices = taxCodeStateService.getActiveTaxCodeState();
		}
		
		return taxCodeStateChoices;
	}
	
	public Map<String, Boolean> getStateSelection() {
		return stateSelection;
	}

	public Map<String, Long> getStateJurisdiction() {
		return stateJurisdiction;
	}

	public SearchJurisdictionHandler getJurisdictionHandler() {
		return jurisdictionHandler;
	}
	
	public void popupSearchAction(ActionEvent e) {
		jurisdictionState = (String) e.getComponent().getAttributes().get("stateCode");
		jurisdictionCountry = (String) e.getComponent().getAttributes().get("countryCode");
		
		logger.debug("Jurisdiction search for: " + jurisdictionState);
		logger.debug("Jurisdiction search for: " + jurisdictionCountry);
		jurisdictionHandler.reset();
		Long id = stateJurisdiction.get(jurisdictionState+"-"+jurisdictionCountry);
		if (id != 0L) {
			jurisdictionHandler.setJurisdiction(jurisdictionService.findById(id));
		} else {
			jurisdictionHandler.setState(jurisdictionState);
			jurisdictionHandler.setCountry(jurisdictionCountry);
		}
		// pass event to handler

		jurisdictionHandler.setCallback(this);
		// Just the state is required, no search at this point,
		//user can enter more search criteria after the pop up is displayed - 3788
		
		//jurisdictionHandler.popupSearchAction(e);
		jurisdictionHandler.setStateToPopUp(e);
	}
	
	public SearchDocRefHandler getDocRefHandler() {
		return docRefHandler;
	}

	public void popupDocRefSearchAction(ActionEvent e) {
		docRefHandler.setRefDoc(selectedDocRefDetail.getReferenceDetailPK().getRefDoc());

		if (currentAction.isAddAction()) {
			ExternalContext ec = FacesContext.getCurrentInstance()
					.getExternalContext();
			docRName = ec.getRequestParameterMap().get("detailForm:NameCheck");
			docRefHandler.setRefDocCode(docRName);
	  }
	}
	
	public void docRefSelectedListener(ActionEvent e) {
		selectedDocRefDetail.getReferenceDetailPK().setRefDoc(
				referenceDocumentService.findById(docRefHandler.getSelectedItemKey().getId()));
	}
	
	public class MeasureTypeListComparator implements Comparator<SelectItem> {
		public int compare(SelectItem o1, SelectItem o2) {
			Long l1 = new Long((String)o1.getValue());
			Long l2 = new Long((String)o2.getValue());
			return l1.compareTo(l2);
		}
	}
	
	//used from DirectoryBrowserBackingBean
	public void setDocNameAndPath(String documentName, String url){
		selectedDocRef.setDocName(documentName);
		selectedDocRef.setUrl(url);
	}
	
	public void urlResetAction(ActionEvent e) {
		url = null;
    }

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public GlExtractMapService getGlExtractMapService() {
		return glExtractMapService;
	}

	public void setGlExtractMapService(GlExtractMapService glExtractMapService) {
		this.glExtractMapService = glExtractMapService;
	}

	public OptionService getOptionService() {
		return optionService;
	}

	public void setOptionService(OptionService optionService) {
		this.optionService = optionService;
	}

	public String getSelectedSupportingCode() {
		return selectedSupportingCode;
	}

	public void setSelectedSupportingCode(String selectedSupportingCode) {
		this.selectedSupportingCode = selectedSupportingCode;
	}

	public String getSelectedRefType() {
		return selectedRefType;
	}

	public void setSelectedRefType(String selectedRefType) {
		this.selectedRefType = selectedRefType;
	}

	public String getEnteredCode() {
		return enteredCode;
	}

	public void setEnteredCode(String enteredCode) {
		this.enteredCode = enteredCode;
	}
	
	public List<SelectItem> getRefTypeItems() {
		if (refTypeListForFilter == null) {
			refTypeListForFilter = new ArrayList<SelectItem>();
			refTypeListForFilter.add(new SelectItem("", "All Types"));
			for (ListCodes code : cacheManager.getListCodesByType("REFTYPE")) {
				refTypeListForFilter.add(new SelectItem(code.getCodeCode(), code.getCodeCode() + " - " + code.getDescription()));
			}
		}
		
		return refTypeListForFilter;
	}
	
	public boolean getDisplayRefTypeForFilter() {
		return getActiveTableType().equals(ActiveTableType.DOCREF);
	}
	
	public boolean getDisplayCodeEntryForFilter() {
		ActiveTableType t = getActiveTableType();
		return t.equals(ActiveTableType.DOCREF) || t.equals(ActiveTableType.DOCREF_TYPE) ;
	}
	
	public void supportingCodeSelectionChanged(ValueChangeEvent e) {
		selectedView = ITEM_SUPPORTING_CODE;
		selectedFirstComboValue = selectedSupportingCode = (String)e.getNewValue();
		
		if("0".equals(selectedFirstComboValue)) {
			resetSelection();
			
			country = false;
			state = false;
			ref = false;
			doc = false;
		}
		else {
			searchFilter();
		}
	}
	
	public void supportingCodeSelectionChanged(ActionEvent e) {
		selectedView = ITEM_SUPPORTING_CODE;
		selectedFirstComboValue = selectedSupportingCode;
		
		if("0".equals(selectedSupportingCode)) {
			resetSelection();
			
			country = false;
			state = false;
			ref = false;
			doc = false;
		}
		else {
			searchFilter();
		}
		
		
		docStateFilteredList = null;
		docCountryFilteredList = null;
	}
	
	public String resetFilter() {
		selectedRefType = "";
		enteredCode = "";
		enteredState = "";
		enteredCountry = "";
		enteredStateCountry = "";
		
		return null;
	}
	
	public String searchFilter() {
		country = false;
		state = false;
		ref = false;
		doc = false;
		displayWarningMessage = "";
		
		resetSelection();
		
		selectedFirstComboValue = selectedSupportingCode;
	
		ActiveTableType t = getActiveTableType();
		
		if("0".equals(selectedSupportingCode)) {
			FacesMessage message = new FacesMessage("Select a Supporting Code.");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
			doc = false;
		}
		else if(t.equals(ActiveTableType.DOCREF)) {
			docList = taxCodeDetailService.getDocumentReferencesByTypeAndCode(selectedRefType, enteredCode);
			doc = true;
		}
		else if(t.equals(ActiveTableType.DOCREF_TYPE)) {
			
			ref = true;
		}
		else if(t.equals(ActiveTableType.TAXCODE_STATE)) {
			state = true;
			
			if(enteredStateCountry!=null && enteredStateCountry.length()>0){
				TaxCodeStatePK bPK = new TaxCodeStatePK();
				bPK.setCountry(enteredStateCountry);
				bPK.setTaxcodeStateCode("*COUNTRY");
				TaxCodeStateDTO state = taxCodeStateService.findByPK(bPK);
				if(state!=null && !state.getActiveBooleanFlag()){
					displayWarningMessage = state.getName() +  " is currently not an active country";
				}
			}
		}
		else if(t.equals(ActiveTableType.TAXCODE_COUNTRY)) {
			country = true;
		}
		
		docStateFilteredList = null;
		docCountryFilteredList = null;
		
		return null;
	}
	
	public String displaySupportingCodesAddAction() {
		currentAction = EditAction.ADD;
		ActiveTableType t = getActiveTableType();
		
		if(t.equals(ActiveTableType.DOCREF)) {
			selectedDocRef = new ReferenceDocument();
			//selectedDocRef.setUrl(getRefDocPath());
			return "taxability_docref_add";
		}
		else if(t.equals(ActiveTableType.DOCREF_TYPE)) {
			return listCodesBean.externalAddAction("REFTYPE", "setup_supporting_codes");
		}
		else if(t.equals(ActiveTableType.TAXCODE_STATE)) {
			selectedState =  new TaxCodeStateDTO();
			selectedState.setActiveBooleanFlag(true);
			selectedState.setCustomFlag("1");
			selectedState.setModifiedFlag("0");
			this.selectedCountryComboValue = getDefUserCountry();
			this.selectedLocalTaxabilityValue = "S";
			return "taxability_state_add";
		}
		else if(t.equals(ActiveTableType.TAXCODE_COUNTRY)) {
			selectedCountry =  new TaxCodeStateDTO();
			selectedCountry.setActiveBooleanFlag(true);
			selectedCountry.setCustomFlag("1");
			return "taxability_country_add";
		}
		
		return null;
	}
	
	public boolean getSupportingCodesValidSelection() {
		ActiveTableType t = getActiveTableType();
		
		if(t.equals(ActiveTableType.DOCREF)) {
			return selectedDocRefSource != null;
		}
		else if(t.equals(ActiveTableType.DOCREF_TYPE)) {
			return selectedDocRefTypeIndex != -1;
		}
		else if(t.equals(ActiveTableType.TAXCODE_STATE)) {
			return selectedStateIndex != -1;
		}
		else if(t.equals(ActiveTableType.TAXCODE_COUNTRY)) {
			return selectedCountryIndex != -1;
		}
		
		return false;
	}
	
	public List<TaxCodeStateDTO> getStateQuickUpdateList(){
		return stateQuickUpdateList;
	}
	
	public List<TaxCodeStateDTO> getCountryQuickUpdateList(){
		return countryQuickUpdateList;
	}
	
	private boolean indicateAllSelected = false;
	
	public boolean isAllSelected() {
		return indicateAllSelected;
	}
	
	public String getDisplayWarningMessage() {
		return displayWarningMessage;
	}
	
	public void setAllSelected(boolean indicateAllSelected) {
		this.indicateAllSelected = indicateAllSelected;
	}
	
	public void selectAllChange(ActionEvent e){
		HtmlSelectBooleanCheckbox check = (HtmlSelectBooleanCheckbox) ((HtmlAjaxSupport) e.getComponent()).getParent();
		boolean newCheck = (Boolean)check.getValue();
		
		ActiveTableType t = getActiveTableType();
		if(t.equals(ActiveTableType.TAXCODE_STATE)) {	
			for(TaxCodeStateDTO state : stateQuickUpdateList) {
				state.setActiveTempBooleanFlag(newCheck);
			}
		}
		else if(t.equals(ActiveTableType.TAXCODE_COUNTRY)) {
			for(TaxCodeStateDTO state : countryQuickUpdateList) {
				state.setActiveTempBooleanFlag(newCheck);
			}
		}
	}
	
	public void selectSingleChange(ActionEvent e){
		HtmlSelectBooleanCheckbox check = (HtmlSelectBooleanCheckbox) ((HtmlAjaxSupport) e.getComponent()).getParent();
		boolean newCheck = (Boolean)check.getValue();
	}
	
	//PP-342
	public boolean getQuickUpdateEnable() {
		ActiveTableType t = getActiveTableType();
		if(t.equals(ActiveTableType.TAXCODE_STATE)) {	
			if(docStateFilteredList!=null && docStateFilteredList.size()>0){	
				return true;
			}
			else{
				return false;
			}
		}
		else if(t.equals(ActiveTableType.TAXCODE_COUNTRY)) {
			if(docCountryFilteredList!=null && docCountryFilteredList.size()>0){	
				return true;
			}
			else{
				return false;
			}
		}
		
		return false;
	}
	
	//PP-342
	public String displayQuickUpdateAction() {
		currentAction = EditAction.UPDATE;
		ActiveTableType t = getActiveTableType();
		
		indicateAllSelected = false;
		
		if(t.equals(ActiveTableType.TAXCODE_STATE)) {	
			if(docStateFilteredList!=null){	
				stateQuickUpdateList = new ArrayList<TaxCodeStateDTO>(); 
				for(TaxCodeStateDTO state : docStateFilteredList) {
					TaxCodeStateDTO ndd = new TaxCodeStateDTO();
					BeanUtils.copyProperties(state, ndd);
					ndd.setActiveTempBooleanFlag(ndd.getActiveBooleanFlag());	
					stateQuickUpdateList.add(ndd);
				}
				return "taxability_state_quick_update";
			}
			else{
				return null;
			}
		}
		else if(t.equals(ActiveTableType.TAXCODE_COUNTRY)) {
			if(docCountryFilteredList!=null){	
				countryQuickUpdateList = new ArrayList<TaxCodeStateDTO>(); 
				for(TaxCodeStateDTO state : docCountryFilteredList) {
					TaxCodeStateDTO ndd = new TaxCodeStateDTO();
					BeanUtils.copyProperties(state, ndd);
					ndd.setActiveTempBooleanFlag(ndd.getActiveBooleanFlag());	
					countryQuickUpdateList.add(ndd);
				}
				return "taxability_country_quick_update";
			}
			else{
				return null;
			}
		}
		
		return null;
	}
	
	public String displaySupportingCodesUpdateAction() {
		currentAction = EditAction.UPDATE;
		ActiveTableType t = getActiveTableType();
		
		if(t.equals(ActiveTableType.DOCREF)) {
			selectedDocRef = selectedDocRefSource;
			return "taxability_docref_add";
		}
		else if(t.equals(ActiveTableType.DOCREF_TYPE)) {
			refTypeListForFilter = null;
			selectedDocRefTypeIndex = -1;
			return listCodesBean.externalUpdateAction(selectedDocRefTypeSource.getListCodesDTO(), "setup_supporting_codes");
		}
		else if(t.equals(ActiveTableType.TAXCODE_STATE)) {
			return displayAction();
		}
		else if(t.equals(ActiveTableType.TAXCODE_COUNTRY)) {
			//PP-342
			selectedCountry =  new TaxCodeStateDTO();
			selectedCountry.setCountry(selectedCountrySource.getCountry());
			selectedCountry.setName(selectedCountrySource.getName());
			selectedCountry.setActiveBooleanFlag(selectedCountrySource.getActiveBooleanFlag());	
			selectedCountry.setUpdateUserId(selectedCountrySource.getUpdateUserId());
			selectedCountry.setUpdateTimestamp(selectedCountrySource.getUpdateTimestamp());

			return "taxability_country_add";
		}
		
		return null;
	}
	
	public String displaySupportingCodesViewAction() {
		String returnScreen =  displaySupportingCodesUpdateAction();
		currentAction = EditAction.VIEW;
		return returnScreen;
		
	}
	
	public String displaySupportingCodesDeleteAction() {
		currentAction = EditAction.DELETE;
		ActiveTableType t = getActiveTableType();
		
		if(t.equals(ActiveTableType.DOCREF)) {
			selectedDocRef = selectedDocRefSource;
			return "taxability_docref_add";
		}
		else if(t.equals(ActiveTableType.DOCREF_TYPE)) {
			refTypeListForFilter = null;
			selectedDocRefTypeIndex = -1;
			return listCodesBean.externalDeleteAction(selectedDocRefTypeSource.getListCodesDTO(), "setup_supporting_codes");
		}
		else if(t.equals(ActiveTableType.TAXCODE_STATE)) {
			return displayAction();
		}
		else if(t.equals(ActiveTableType.TAXCODE_COUNTRY)) {
			selectedCountry =  new TaxCodeStateDTO();
			selectedCountry.setCountry(selectedCountrySource.getCountry());
			selectedCountry.setName(selectedCountrySource.getName());
			selectedCountry.setActiveBooleanFlag(selectedCountrySource.getActiveBooleanFlag());	
			selectedCountry.setUpdateUserId(selectedCountrySource.getUpdateUserId());
			selectedCountry.setUpdateTimestamp(selectedCountrySource.getUpdateTimestamp());

			return "taxability_country_add";
		}
		
		return null;
	}
	
	public boolean getSupportingCodesDisableDeleteButton() {
		return !getSupportingCodesValidSelection();
	}
	
	public List<ListCodes> getDocRefTypeFilteredList() {
		List<ListCodes> list = null;
		if(StringUtils.hasText(enteredCode)) {
			list = new ArrayList<ListCodes>();
			String s = enteredCode.toLowerCase();
			/*for(ListCodes lc : getDocRefTypeList()) {
				//5851
				//if(lc.getCodeCode().toLowerCase().contains(s)) {
				if(lc.getCodeCode().equalsIgnoreCase(s)) {
					list.add(lc);
				}
			}*/
			list=listCodesBean.filterListCodes("REFTYPE", enteredCode);
		}
		else {
			list = getDocRefTypeList();
		}
		
		return list;
	}
	
	public List<TaxCodeStateDTO> getDocStateFilteredList() {
		
		if(docStateFilteredList!=null){
			return docStateFilteredList;
		}
		
		List<TaxCodeStateDTO> list = null;
		
		boolean filterByCountry = StringUtils.hasText(enteredStateCountry);
		boolean filterByState = StringUtils.hasText(enteredState); 
		if(filterByCountry || filterByState) {
			list = new ArrayList<TaxCodeStateDTO>();
			String s = enteredState.toLowerCase();
			String c = enteredStateCountry.toUpperCase();
			
			//filter states with & with out wild chars(5851)
			List<TaxCodeStateDTO> stateList = null;
			
			if(s!=null && !s.equals("")){
				stateList=taxCodeStateService.findByStateName(s);
			}else
				stateList=getDocStateList();
			for(TaxCodeStateDTO state : stateList) {
				if(filterByCountry) {
					if(state.getCountry().toUpperCase().equals(c) || c.equalsIgnoreCase("*ALL")) {
						list.add(state);
						System.out.println("YES: " + state.getCountry().toUpperCase() + ", " + state.getTaxCodeState());
					}
					else {
						System.out.println("NO : " + state.getCountry().toUpperCase() + ", " + state.getTaxCodeState());
					}
				}
				else if(filterByState) {
						list.add(state);
				}				
			}
		}
		else {
			list = getDocStateList();
		}
		
		docStateFilteredList = list;
		
		return docStateFilteredList;
	}
	
	public List<TaxCodeStateDTO> getDocCountryFilteredList() {
		
		if(docCountryFilteredList!=null){
			return docCountryFilteredList;
		}
		
		List<TaxCodeStateDTO> list = null;
		boolean filterByCountry = StringUtils.hasText(enteredCountry); 
		if(filterByCountry) {
			list = new ArrayList<TaxCodeStateDTO>();
			String c = enteredCountry.toUpperCase();
			if(c!=null && !c.equals("")){
				list=taxCodeStateService.findByCountryName(c);
			}
		}
		else{
			list=taxCodeStateService.findByCountryName(null);
		}
		
		docCountryFilteredList = list;
		
		return docCountryFilteredList;
	}

	public String getEnteredState() {
		return enteredState;
	}

	public void setEnteredState(String enteredState) {
		this.enteredState = enteredState;
	}
	
	public String getEnteredCountry() {
		return enteredCountry;
	}

	public void setEnteredCountry(String enteredCountry) {
		this.enteredCountry = enteredCountry;
	}
	
	public String getEnteredStateCountry() {
		return enteredStateCountry;
	}

	public void setEnteredStateCountry(String enteredStateCountry) {
		this.enteredStateCountry = enteredStateCountry;
	}
	
	public List<SelectItem> getCountryItemsForFilter() {
		if (countryItemsForFilter == null) {
			countryItemsForFilter = new ArrayList<SelectItem>();
			Map<String,String> countryMap = cacheManager.getCountryMap();
			for (String countryCode : countryMap.keySet()) {
				String countryName = countryMap.get(countryCode);
				countryItemsForFilter.add(new SelectItem(countryCode, countryName + " (" + countryCode + ")"));
			}
		}
		
		return countryItemsForFilter;
	}
	
	public String getDefUserCountry() {
		HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
		UserDTO userDTO = (UserDTO) session.getAttribute("user");
		String userCode = userDTO.getUserCode(); 
		Option option = optionService.findByPK(new OptionCodePK ("DEFUSERCOUNTRY", "USER", userCode));
		return option.getValue();
	}

	public String getSelectedLocalTaxabilityValue() {
		return selectedLocalTaxabilityValue;
	}

	public void setSelectedLocalTaxabilityValue(String selectedLocalTaxabilityValue) {
		this.selectedLocalTaxabilityValue = selectedLocalTaxabilityValue;
	}

	public List<SelectItem> getLocalTaxabilityItems() {
		if(localTaxabilityItems == null) {
			localTaxabilityItems = new ArrayList<SelectItem>();
			localTaxabilityItems.add(new SelectItem("", "Select a Local Taxability"));
			Map<String, String> localTaxMap = cacheManager.getLocalTaxabilityCodeMap();
			for(Map.Entry<String, String> e : localTaxMap.entrySet()) {
				localTaxabilityItems.add(new SelectItem(e.getKey(), e.getValue()));
			}
		}
		return localTaxabilityItems;
	}

	public void setLocalTaxabilityItems(List<SelectItem> localTaxabilityItems) {
		this.localTaxabilityItems = localTaxabilityItems;
	}
	
	
	public List<SelectItem> getCertificateLevelItems() {
		if (certificateLevelItems == null) {
			certificateLevelItems = new ArrayList<SelectItem>();
			certificateLevelItems.add(new SelectItem("", "Select a Certificate Level"));
			List<ListCodes> certLevelItems = cacheManager.getListCodesByType("LOCALTAX");
			for (ListCodes lc : certLevelItems) 
				certificateLevelItems.add(new SelectItem(lc.getCodeCode(), lc.getDescription()));	
		}		
		return certificateLevelItems;
	}

	public void setCertificateLevelItems(List<SelectItem> certificateLevelItems) {
		this.certificateLevelItems = certificateLevelItems;
	}
	
	
	private void saveRefDocFile(UploadedFile uploadFile) throws IOException{
		String filePath=getRefDocPath();
		String fileName=uploadedFile.getName();
		filePath=filePath+"/"+fileName;
		File destination=new File(filePath);
		InputStream input = null;
		    OutputStream output = null;
		    try {
		        input =uploadFile.getInputStream();
		        output = new FileOutputStream(destination);
		        byte[] buf = new byte[1024];
		        int bytesRead;
		        while ((bytesRead = input.read(buf)) > 0) {
		            output.write(buf, 0, bytesRead);
		        }
		    } finally {
		        input.close();
		        output.close();
		    }

	}

	public String getSelectedCertificateLevelValue() {
		return selectedCertificateLevelValue;
	}

	public void setSelectedCertificateLevelValue(
			String selectedCertificateLevelValue) {
		this.selectedCertificateLevelValue = selectedCertificateLevelValue;
	}

	public User getCurrentUser() {
		return loginBean.getUser();
   }

}