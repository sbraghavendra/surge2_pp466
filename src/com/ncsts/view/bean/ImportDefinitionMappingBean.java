package com.ncsts.view.bean;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.faces.component.UIComponent;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.ActionEvent;

import org.ajax4jsf.component.UIDataAdaptor;
import org.apache.log4j.Logger;

import com.ncsts.common.CacheManager;
import com.ncsts.dao.ImportDefinitionDAO;
import com.ncsts.domain.DataDefinitionColumn;
import com.ncsts.domain.ImportConnection;
import com.ncsts.domain.ImportDefinitionDetail;
import com.ncsts.dto.ImportDefDTO;
import com.ncsts.exception.DateFormatException;
import com.ncsts.services.ImportDefAndSpecsService;
import com.ncsts.view.util.ImportMapUploadParser;
import com.ncsts.view.util.JsfHelper;
import com.ncsts.view.util.PathResolver;
import com.ncsts.view.util.SortOrder;

public class ImportDefinitionMappingBean {

	private Logger logger = Logger.getLogger(ImportDefinitionMappingBean.class);

	/* Maximum # of lines to read for view formatted/unformatted */
	private final int MAX_LINES_TO_READ=100;
	private String uploadPath = PathResolver.getSampleFileUploadPath();

	private enum Mode {
		ColumnMappings,
		Formatted, 
		Unformatted;
	}
	
	private ImportDefDTO selectedImportDef;
	
	private CacheManager cacheManager;
	
	private int selectedTransactionIndex = -1;
	private int selectedImportIndex = -1;;
	private int selectedMappingIndex = -1;
	
	private ImportDefinitionDAO importDefinitionDAO;
	private FilePreferenceBackingBean filePreferenceBean;
	private ImportDefAndSpecsService importDefAndSpecsService;
	private ImportDefinitionAndSpecsBean importDefinitionAndSpecsBean = null;
	
	private boolean dirty;
	private Mode mode;
	
	private ImportMapUploadParser parser;
	private List<ImportColumn> importColumns;
	private List<MappedColumn> mappings;
	private List<UnformattedRow> unformattedData;
	private List<List<String>> formattedData;
	private TransactionColumns transactionColumns;
	private SortOrder<MappedColumn> sortOrder = new SortOrder<MappedColumn>(SortOrder.AscendingDescending,
			new MappedColumn());
	
	public class ImportColumn {
		protected String name;
		public ImportColumn() {
			super();
		}
		public ImportColumn(String name) {
			this.name = name;
		}
		public boolean getIsAssigned() {
			for (MappedColumn mc : mappings) {
				if (mc.getImportColumnName().equalsIgnoreCase(this.name)) return true;
			}
			return false;
		}
		public String getName() { return name; }
    public boolean equals(Object other) {
    	return (this == other) ||
      	((other instanceof ImportColumn) && (((ImportColumn)other).name.equalsIgnoreCase(name)));
    }
    public int hashCode() {
    	return name.hashCode();
    }
	}
	
	@SuppressWarnings("serial")
	public class MappedColumn extends ImportDefinitionDetail implements Comparable<MappedColumn> {
		public MappedColumn() {
			super();
		}
		public MappedColumn(ImportDefinitionDetail detail) {
			super(detail);
		}
		public boolean getExistsInImport() {
			return importColumns.contains(new ImportColumn(getImportColumnName()));
		}
		public String getSelectEllipsis() {
			if ((this.getSelectClause() != null) && (this.getSelectClause().length() > 12)) {
				return this.getSelectClause().substring(0, 12) + "...";
			} else {
				return getSelectClause();
			}
		}
    public boolean equals(Object other) {
    	return (this == other) ||
      	((other instanceof MappedColumn) 
      			&& (((MappedColumn)other).getTransDtlColumnName().equalsIgnoreCase(getTransDtlColumnName())));
    }
    public int hashCode() {
    	return getTransDtlColumnName().hashCode();
    }
		/* Sort by the current column */
		public int compareTo(MappedColumn column) {
			Map.Entry<String, SortOrder.Order> entry = sortOrder.getCurrentOrder();
			String a = (String)sortOrder.getValue(this, entry.getKey());
			String b = (String)sortOrder.getValue(column, entry.getKey());
			if ((a == null) && (b==null)) return 0;
			if (b == null) return 0;
			if (a == null) return 1;
			if (entry.getValue() == SortOrder.Order.Ascending) {
				return a.compareTo(b);
			} else {
				return b.compareTo(a);
			}
		}
	}
	
	@SuppressWarnings("serial")
	public class TransactionColumns extends ArrayList<String> {
		private boolean isSaleTrans = false;
		private boolean isTransandSale = false;
		private boolean isTransan = false;
	
		
		public TransactionColumns() {
			super();
			init();
		}
		
		public TransactionColumns(boolean isSaleTrans,boolean isTransandSale,boolean isTransan) {
			super();
			this.isSaleTrans = isSaleTrans;
			this.isTransandSale=isTransandSale;
			this.isTransan=isTransan;
			init();
		}
		
		void addToMap(Collection<DataDefinitionColumn> result){
			for (DataDefinitionColumn detail : result) {
				try{
					if(detail.isMappable()) {
						if(isSaleTrans){
							add("Bill:"+detail.getColumnName());
						}
						
						if(isTransan){
							add("Purch:"+detail.getColumnName());
						}
						
						if(isTransandSale){
							if(detail.getTableName().equalsIgnoreCase("TB_PURCHTRANS")){
								add("Purch:"+detail.getColumnName());
							}
							else if(detail.getTableName().equalsIgnoreCase("TB_BILLTRANS")){
								add("Bill:"+detail.getColumnName());
							}	     
						}					
					}  
				}
				catch(Exception e){
					System.out.println("exception"+e);
				}
			}
		}
		
		public void init() {
			clear();
			boolean isSaleTrans = "IPS".equals(selectedImportDef.getImportDefinitionType());
			boolean isTransandSale="PCO".equals(selectedImportDef.getImportDefinitionType());
		    boolean isTransan="IP".equals(selectedImportDef.getImportDefinitionType());
			
			Collection<DataDefinitionColumn> result = null;
			
			HashMap<String, String> lcImportHeaders = new HashMap<String, String>();
			
			if(parser==null){//for DB		
				for (String s : headerNames) {
					lcImportHeaders.put(s.toLowerCase(), s);
				}
			}
			else{//for FD
				for (String s : parser.getHeaders()) {
					lcImportHeaders.put(s.toLowerCase(), s);
				}
			}
			
			if(isSaleTrans){
				result=	cacheManager.getSaleTransactionPropertyMap().values();	
				addToMap(result);
			}
			else if(isTransandSale){		
				result=cacheManager.getTransactionPropertyMap().values();
				addToMap(result);
				result=cacheManager.getSaleTransactionPropertyMap().values();	
				addToMap(result);
			}
			else if(isTransan) {
				result=cacheManager.getTransactionPropertyMap().values();
				addToMap(result);
			}
			else{				
				result=cacheManager.getTransactionPropertyMap().values();
				addToMap(result);
			}
		}
		
		/* get all the unmapped columns, sorted by alpha */
		public List<String> getSortedAndFIltered() {
			List<String> filteredList = new ArrayList<String>();
			for (String s : this) {
				if (! isMappedTransactionColumn(s)){
					filteredList.add(s);
				}
			}
			Collections.sort(filteredList);
			return filteredList;
		}
	}
	
	public class UnformattedRow {
		public int line;
		public String text;
		public UnformattedRow(int line, String text) {
			this.line = line;
			this.text = text;
		}
		public int getLine() {
			return this.line;
		}
		public String getText() {
			return this.text;
		}
	}
	
	private boolean isMappedTransactionColumn(String column) {
		String str="";
		for (MappedColumn mc : mappings) {
			str=mc.getTransDtlColumnName();
			if (str.equalsIgnoreCase(column)){
				return true;
			}
		}
		return false;
	}

	private void clear() {
		dirty = false;
		selectedMappingIndex = -1;
		selectedImportIndex = -1;
		selectedTransactionIndex = -1;
		
		//0006994
		importColumns = new ArrayList<ImportColumn>();
		Map<String, ImportColumn> columnMap = new LinkedHashMap<String,ImportColumn>();
		List<String> columnList = new ArrayList<String>();
		
		if(parser==null){//for DB		
			for (String s : headerNames) {
				//5427 Convert to upper for import headers
				columnMap.put(s.toUpperCase(),new ImportColumn(s.toUpperCase()));
				columnList.add(s.toUpperCase());
			}
		}
		else{//for FD
			for (String s : parser.getHeaders()) {
				//5427 Convert to upper for import headers
				columnMap.put(s.toUpperCase(),new ImportColumn(s.toUpperCase()));
				columnList.add(s.toUpperCase());
			}
		}

		Collections.sort(columnList);
		ImportColumn selectedImportColumn = null;
		for(int i=0; i< columnList.size(); i++){
			selectedImportColumn = columnMap.get(columnList.get(i));
			if(selectedImportColumn!=null){
				importColumns.add(selectedImportColumn);
			}
		}

		mappings = new ArrayList<MappedColumn>();
		System.out.println("code:"+selectedImportDef.getImportDefinitionCode());
		
		for (ImportDefinitionDetail detail : 	importDefinitionDAO.getAllHeaders(selectedImportDef.getImportDefinitionCode())) {
			if(detail.getTableName().equalsIgnoreCase("TB_PURCHTRANS")){
				String str="Purch:"+detail.getTransDtlColumnName();
				detail.setTransDtlColumnName(str);
			    mappings.add(new MappedColumn(detail));
			}else if(detail.getTableName().equalsIgnoreCase("TB_BILLTRANS")){
				String str="Bill:"+detail.getTransDtlColumnName();
				detail.setTransDtlColumnName(str);
			   mappings.add(new MappedColumn(detail));
			}
		}
		boolean isSaleTrans = "IPS".equals(selectedImportDef.getImportDefinitionType());
		boolean isTransandSale="PCO".equals(selectedImportDef.getImportDefinitionType());
		boolean isTransan="IP".equals(selectedImportDef.getImportDefinitionType());
		transactionColumns = new TransactionColumns(isSaleTrans,isTransandSale,isTransan);
		
			
	}

	public String displayMapFDAction(ImportDefDTO def, ImportDefinitionAndSpecsBean importDefinitionAndSpecsBean){
		
		this.importDefinitionAndSpecsBean = importDefinitionAndSpecsBean; 	
		this.selectedImportDef = def;
		this.unformattedData = null;
		this.formattedData = null;
		this.mode = Mode.ColumnMappings;
		try {
			parser = new ImportMapUploadParser(selectedImportDef);
			parser.setUploadPath(uploadPath);
			parser.setPreferences(filePreferenceBean.getImportMapProcessPreferenceDTO());
			parser.parseHeader();
		} catch (IOException e) {
			JsfHelper.addError("form:SampleFileName", "Not available");
  	} catch (DateFormatException dfe) {
  		JsfHelper.addError(dfe.getMessage());
		}
		clear();
		return "import_def_map";
	}
	
	private List<String> headerNames = null;
	
	public String displayMapDBAction(ImportDefDTO def, ImportDefinitionAndSpecsBean importDefinitionAndSpecsBean){
		this.importDefinitionAndSpecsBean = importDefinitionAndSpecsBean; 
		this.selectedImportDef = def;
		this.unformattedData = null;
		this.formattedData = null;
		this.mode = Mode.ColumnMappings;
		//try {
			//Make JDBC connection to get all table names
			//SELECT TABLE_NAME FROM TB_VIEW_TABLE_NAMES ORDER BY TABLE_NAME			
			List<ImportConnection> importConnections = importDefAndSpecsService.getConnectionsByCode(selectedImportDef.getImportConnectionCode());		
			if(importConnections==null || importConnections.size()==0){
				return null;
			}
				
			ImportConnection importConnection = importConnections.get(0);
				
			try {
				Class.forName(importConnection.getDbDriverClassName());
		    } catch (ClassNotFoundException e) {
		    	e.printStackTrace();
		    	return null;
		    }

		    Connection conn = null;
		    Statement stmt = null;
		    ResultSet rs = null;
		    ResultSet rset = null;
		    try {
		    	parser = null;
		    	headerNames = new ArrayList<String>();
		    	formattedDbData = new ArrayList<List<String>>();
		    	
		    	conn = DriverManager.getConnection(importConnection.getDbUrl(), importConnection.getDbUserName(), importConnection.getDbPassword());
		    	stmt = conn.createStatement();
		    	
		    	//Get columns names
		    	rs = stmt.executeQuery("SELECT column_name FROM USER_TAB_COLUMNS WHERE table_name = '" + selectedImportDef.getDbTableName() + "'");		    	
		    	String columnName = "";
				while (rs.next()) {
					columnName = rs.getString("column_name");	
					headerNames.add(columnName);
				}
				
				//Get data
				String columnString = "";
				for(String header : headerNames){
					if(columnString.length()>0){
						columnString = columnString + ",";
					}
					columnString = columnString + header;
				}
				
				rset = stmt.executeQuery("SELECT " + columnString + " FROM " + selectedImportDef.getDbTableName() + " WHERE ROWNUM <= " + MAX_LINES_TO_READ);
				while(rset!=null && rset.next()){
					ArrayList<String> rowList = new ArrayList<String>();
					String value = "";
					for(String header : headerNames){	
						if(rset.getObject(header)!=null){
							value = rset.getObject(header).toString();	
						}
						else{
							value = "";
						}
						
						rowList.add(value);
					}
					formattedDbData.add(rowList);
				}
				
				rs.close();rs = null;
		    	rset.close();rset = null;
		    	stmt.close();stmt = null;
		    	conn.close();conn = null;
		    } catch (SQLException e) {
		    	e.printStackTrace();
		    } finally {
		      if (rs != null) try {rs.close();} catch (SQLException ignore) {}
		      if (rset != null) try {rset.close();} catch (SQLException ignore) {}
		      if (stmt != null) try {stmt.close();} catch (SQLException ignore) {}
		      if (conn != null) try { conn.close();} catch (SQLException ignore) {}
		    }
		//} catch (IOException e) {
		//	JsfHelper.addError("form:SampleFileName", "Not available");
		//} catch (DateFormatException dfe) {
		//	JsfHelper.addError(dfe.getMessage());
		//}
		clear();
		return "import_def_map";
	}
	
	public boolean getShowMappings() {
		return mode == Mode.ColumnMappings;
	}
	public boolean getShowMappingsButton() {
		return mode != Mode.ColumnMappings;
	}
	public boolean getShowFormatted() {
		return mode == Mode.Formatted;
	}
	public boolean getShowUnformatted() {
		return mode == Mode.Unformatted;
	}
	public boolean getShowUnformattedButton() {
		return (mode == Mode.Formatted) || (mode == Mode.ColumnMappings);
	}
	public boolean getShowFormattedButton() {
		return (mode == Mode.Unformatted) || (mode == Mode.ColumnMappings);
	}
	public boolean getShowSaveButton() {
		return (mode == Mode.ColumnMappings) && dirty;
	}
	public boolean getShowUpdateButton() {
		return (mode == Mode.ColumnMappings) && (selectedMappingIndex != -1);
	}
	public boolean getShowCancelButton() {
		return (!dirty) || (mode == Mode.ColumnMappings);
	}
	public String getCurrentMenu() {
		switch (mode) {
		case ColumnMappings: return "Column Mappings";
		case Formatted: return "View Formatted";
		case Unformatted: return "View Unformatted";
		default: return "";
		}
	}
	public void importColumnChanged(ActionEvent e) throws AbortProcessingException { 
		UIComponent uiComponent = e.getComponent().getParent();
		UIDataAdaptor table = (UIDataAdaptor) uiComponent;
		selectedImportIndex = table.getRowIndex();
	}
	public void transColumnChanged(ActionEvent e) throws AbortProcessingException { 
		UIComponent uiComponent = e.getComponent().getParent();
		UIDataAdaptor table = (UIDataAdaptor) uiComponent;
		selectedTransactionIndex = table.getRowIndex();
	}
	public void mappingColumnChanged(ActionEvent e) throws AbortProcessingException { 
		UIComponent uiComponent = e.getComponent().getParent();
		UIDataAdaptor table = (UIDataAdaptor) uiComponent;
		selectedMappingIndex = table.getRowIndex();
		logger.debug("selected Mapping: " + selectedMappingIndex);
	}
	
	public void showFormattedAction() {
		mode = Mode.Formatted;
	}
	public void showUnformattedAction() {
		mode = Mode.Unformatted;
	}
	public void showMappingsAction() {
		 mode = Mode.ColumnMappings;
		 selectedMappingIndex = -1;
		 selectedTransactionIndex = -1;
		 selectedImportIndex = -1;
	}
	public void assignAction() {
		ImportDefinitionDetail detail = new ImportDefinitionDetail();
		ImportColumn ic = importColumns.get(selectedImportIndex);
		detail.setImportColumnName(ic.name);
		String transDtlold=transactionColumns.getSortedAndFIltered().get(selectedTransactionIndex);
		if(transDtlold.substring(0, 6).equals("Purch:")){
			detail.setTableName("TB_PURCHTRANS");
		}else if(transDtlold.substring(0, 5).equals("Bill:")){
			detail.setTableName("TB_BILLTRANS");
		}
		//if(transDtlold.substring(0, 2).equals("P_")||transDtlold.substring(0, 2).equals("S_")){
			
		//String transDtlnew = transDtlold.replace(transDtlold.substring(0, 2),"");
	  	//detail.setTransDtlColumnName(transDtlnew);
		//}
		detail.setTransDtlColumnName(transDtlold);
		detail.setImportDefinitionCode(selectedImportDef.getImportDefinitionCode());
		mappings.add(new MappedColumn(detail));
		selectedTransactionIndex = -1;
		selectedMappingIndex = -1;
		dirty=true;
	}
	
	public void processAssignAll(Map<String, DataDefinitionColumn> result, HashMap<String, String> lcImportHeaders){
		Map<String, String> mappedColumns = new LinkedHashMap<String,String>();	
		for (MappedColumn mc : mappings) {
			mappedColumns.put(mc.getTransDtlColumnName(), mc.getTransDtlColumnName());
		}

		String lcTrans="";
		MappedColumn mc=null;
		DataDefinitionColumn ddtable=null;
		for (Map.Entry<String, DataDefinitionColumn> entry: result.entrySet()) {
			try{
				ddtable=entry.getValue();
				lcTrans = entry.getValue().getColumnName().toLowerCase();
				
				if(ddtable.getTableName().equalsIgnoreCase("TB_PURCHTRANS")){
					mc = new MappedColumn(new ImportDefinitionDetail(selectedImportDef.getImportDefinitionCode(),
							"Purch:"+entry.getValue().getColumnName(),lcImportHeaders.get(lcTrans)));
				}
				else if(ddtable.getTableName().equalsIgnoreCase("TB_BILLTRANS")){					
					mc = new MappedColumn(new ImportDefinitionDetail(selectedImportDef.getImportDefinitionCode(),
							"Bill:"+entry.getValue().getColumnName(),lcImportHeaders.get(lcTrans)));
				}
			
				if(entry.getValue().isMappable() && lcImportHeaders.containsKey(lcTrans) && !mappedColumns.containsKey(mc.getImportColumnName())) {
					if(mc.getImportColumnName().substring(0, 6).equals("Purch:")){					
						ImportDefinitionDetail detail=new ImportDefinitionDetail(selectedImportDef.getImportDefinitionCode(),entry.getValue().getColumnName(),lcImportHeaders.get(lcTrans));
						String str="Purch:"+detail.getTransDtlColumnName();
						detail.setTransDtlColumnName(str.toUpperCase());
						detail.setTableName("TB_PURCHTRANS");
						mappings.add(new MappedColumn(detail));						
					}
					else if(mc.getImportColumnName().substring(0, 5).equals("Bill:")){
						ImportDefinitionDetail detail=new ImportDefinitionDetail(selectedImportDef.getImportDefinitionCode(),entry.getValue().getColumnName(),lcImportHeaders.get(lcTrans));
						String str="Bill:"+detail.getTransDtlColumnName();
					    detail.setTransDtlColumnName(str.toUpperCase());
						detail.setTableName("TB_BILLTRANS");
						mappings.add(new MappedColumn(detail)); 
					}	
				}

			}catch(Exception e){
				System.out.println(e);	
			}
		}
	}
	
	/**
	 *  assign all that match ignoring case 
	*/
	public void assignAllAction() throws IOException {
		boolean isSaleTrans = "IPS".equals(selectedImportDef.getImportDefinitionType());
		boolean isTransandSale="PCO".equals(selectedImportDef.getImportDefinitionType());
		boolean isTransan="IP".equals(selectedImportDef.getImportDefinitionType());
		
		Map<String, DataDefinitionColumn> result = null;
		HashMap<String, String> lcImportHeaders = new HashMap<String, String>();
		
		if(parser==null){//for DB		
			for (String s : headerNames) {
				lcImportHeaders.put(s.toLowerCase(), s);
			}
		}
		else{//for FD
			for (String s : parser.getHeaders()) {
				lcImportHeaders.put(s.toLowerCase(), s);
			}
		}

		if(isSaleTrans){
			result=cacheManager.getSaleTransactionPropertyMap();
			processAssignAll(result, lcImportHeaders);
		}else if(isTransandSale){			
			result=cacheManager.getTransactionPropertyMap();
			processAssignAll(result, lcImportHeaders);
			
			result=cacheManager.getSaleTransactionPropertyMap();
			processAssignAll(result, lcImportHeaders);
	
		}else  if(isTransan){
			result=cacheManager.getTransactionPropertyMap();
			processAssignAll(result, lcImportHeaders);
		}else{
			result=cacheManager.getTransactionPropertyMap();	
		}
		
		selectedMappingIndex = -1;
		dirty=true;
	}
	
	public void removeAction() {
		mappings.remove(selectedMappingIndex);
		selectedMappingIndex = -1;
		dirty=true;
	}
	public void removeAllAction() {
		clear();
		mappings.clear();
		dirty=true;
	}

	public String saveAction() {
		List<ImportDefinitionDetail> idd = new ArrayList<ImportDefinitionDetail>();
		for (MappedColumn mc : mappings) {
			String transDtlnew = null;
			if(mc.getTransDtlColumnName().substring(0, 6).equals("Purch:")){
				transDtlnew = mc.getTransDtlColumnName().replaceFirst(mc.getTransDtlColumnName().substring(0, 6),"");
			}
			else if(mc.getTransDtlColumnName().substring(0, 5).equals("Bill:")) {
				transDtlnew = mc.getTransDtlColumnName().replaceFirst(mc.getTransDtlColumnName().substring(0, 5),"");
			}
			
			//String transDtlnew = mc.getTransDtlColumnName().replaceFirst(mc.getTransDtlColumnName().substring(0, 2),"");
			 mc.setTransDtlColumnName(transDtlnew);
			idd.add(new ImportDefinitionDetail((ImportDefinitionDetail)mc));
			}
		importDefinitionDAO.replace(selectedImportDef.getImportDefinitionCode(), idd);
		
		if(importDefinitionAndSpecsBean!=null){
			importDefinitionAndSpecsBean.listDefAndSpecs();
		}
		return "import_specs_main";
		}
		
	
	
	/** 
	 * sort by selected column
	 */
	public void sortAction(ActionEvent e) {
		sortOrder.toggle(e.getComponent().getParent().getId());
		Collections.sort(mappings);
	}
	public void formattedAction(ActionEvent e) {
	}
	public List<UnformattedRow> getUnformattedData() {
		if(parser==null){//for DB		
			return null;
		}
		else{//for FD
		}
		
		if (unformattedData != null) return unformattedData;
		try {
			unformattedData = new ArrayList<UnformattedRow>();
			int i = 0;
			for (String s : parser.readLines(MAX_LINES_TO_READ)) {
				unformattedData.add(new UnformattedRow(++i, s));
			}
		} catch (Exception e) {
			JsfHelper.addError("Error reading sample file: " + e.getMessage());
		}
		return unformattedData;
	}

	public List<String> getFormattedColumns() {
		if(parser==null){//for DB		
			return headerNames;
		}
		else{//for FD
			return parser.getHeaders();
		}
	}

	public Boolean getShowColumn() {
		String s = formattedColnum < parser.getHeaders().size() ? "true" : "false";
		formattedColnum++;
		return new Boolean(s);
	}

	private int formattedColnum;
	
	private List<List<String>> formattedDbData = null;
	
	public List<List<String>> getFormattedData() {
		if(parser==null){//for DB		
			return formattedDbData;
		}
		else{//for FD
			formattedColnum = 0;
			if (formattedData == null) { 
				try {
					formattedData = parser.parseLines((MAX_LINES_TO_READ));
				} catch (Exception e) {
					JsfHelper.addError("Error reading sample file: " + e.getMessage());
				}
			}
			return formattedData;
		}
	}
	
	public void defineSelectAction() {
		dirty=true;
	}
	public CacheManager getCacheManager() {
		return this.cacheManager;
	}
	public void setCacheManager(CacheManager cacheManager) {
		this.cacheManager = cacheManager;
	}
	public ImportDefinitionDAO getImportDefinitionDAO() {
		return this.importDefinitionDAO;
	}
	public void setImportDefinitionDAO(ImportDefinitionDAO importDefinitionDAO) {
		this.importDefinitionDAO = importDefinitionDAO;
	}
	public int getSelectedMappingIndex() {
		return this.selectedMappingIndex;
	}
	public List<String> getTransactionColumns() {
		return this.transactionColumns.getSortedAndFIltered();
	}
	public List<MappedColumn> getMappings() {
		return mappings;
	}

	public int getSelectedTransactionIndex() {
		return this.selectedTransactionIndex;
	}

	public int getSelectedImportIndex() {
		return this.selectedImportIndex;
	}

	public List<ImportColumn> getImportColumns() {
		return this.importColumns;
	}

	public ImportDefDTO getSelectedImportDef() {
		return this.selectedImportDef;
	}

	public boolean getDirty() {
		return dirty;
	}

	public Map<String, String> getSortOrder() {
		return this.sortOrder.get();
	}

	public FilePreferenceBackingBean getFilePreferenceBean() {
		return this.filePreferenceBean;
	}

	public void setFilePreferenceBean(FilePreferenceBackingBean filePreferenceBackingBean) {
		this.filePreferenceBean = filePreferenceBackingBean;
	}

	public ImportDefAndSpecsService getImportDefAndSpecsService() {
		return importDefAndSpecsService;
	}
	
	public void setImportDefAndSpecsService(ImportDefAndSpecsService importDefAndSpecsService) {
		this.importDefAndSpecsService = importDefAndSpecsService;
	}
}
