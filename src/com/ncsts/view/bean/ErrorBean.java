package com.ncsts.view.bean;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.component.html.HtmlOutputText;
import javax.faces.context.FacesContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.ncsts.domain.Error;
import com.ncsts.dto.UserSecurityDTO;
import com.ncsts.management.UserSecurityBean;

@Component
@Scope("session")
public class ErrorBean {
	public static final String SUPPORT_EMAIL = "pinpoint.support@ryan.com";
	
	@Autowired
	private UserSecurityBean userSecurityBean;
	
	private HtmlOutputText appVersion;
	private String appVersionString = null;
	private String error;
	private String requestUri;
	private String jsessionid;
	
	private String name;
	private String companyName;
	private String phone;
	
	public String getErrorBK() {
		HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);
		Error e = (Error) session.getAttribute("pinpoint.error");
		
		if(e != null) {
			Map<String, Object> errorMap = e.getErrorMap();
			StringBuffer sb = new StringBuffer("");
			sb.append(getAppVersionString()).append("\n\n");
			sb.append("Request URI: ").append(errorMap.get("javax.servlet.error.request_uri")).append("\n\n");
			sb.append("Exception: ").append(errorMap.get("javax.servlet.error.exception")).append("\n\n");
			sb.append("Status Code: ").append(errorMap.get("javax.servlet.error.status_code")).append("\n\n");
			sb.append("Servlet Name: ").append(errorMap.get("javax.servlet.error.servlet_name")).append("\n\n");
			sb.append("Stack Trace: ").append(getStackTrace(errorMap));
			error = sb.toString();
			
			requestUri = (String) errorMap.get("javax.servlet.error.request_uri");
		}
		else {
			error = null;
		}
		
		return error;
	}
	
	public String getError() {
		HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
		
		HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);
		Error err = (Error) session.getAttribute("pinpoint.error");
		
		Map map = new HashMap();	
		if(err != null) {
			Map<String, Object> errorMap = err.getErrorMap();
			map.put("javax.servlet.error.request_uri", errorMap.get("javax.servlet.error.request_uri"));
			map.put("javax.servlet.error.exception", errorMap.get("javax.servlet.error.exception"));
			map.put("javax.servlet.error.status_code", errorMap.get("javax.servlet.error.status_code"));
			map.put("javax.servlet.error.servlet_name", errorMap.get("javax.servlet.error.servlet_name"));
			session.removeAttribute("pinpoint.error");
		}
		else{
			map.put("javax.servlet.error.request_uri", request.getAttribute("javax.servlet.error.request_uri"));
			map.put("javax.servlet.error.exception", request.getAttribute("javax.servlet.error.exception"));
			map.put("javax.servlet.error.status_code", request.getAttribute("javax.servlet.error.status_code"));
			map.put("javax.servlet.error.servlet_name", request.getAttribute("javax.servlet.error.servlet_name"));
		}

		jsessionid = request.getSession().getId();

		Error e = new Error();
		e.setErrorMap(map);	

		Map<String, Object> errorMap = e.getErrorMap();
		StringBuffer sb = new StringBuffer("");
		sb.append(getAppVersionString()).append("\n\n");			
		sb.append("Occurred on: " + new Date().toString()).append("\n\n");
		sb.append("Request URI: ").append(errorMap.get("javax.servlet.error.request_uri")).append("\n\n");
		sb.append("Exception: ").append(errorMap.get("javax.servlet.error.exception")).append("\n\n");
		sb.append("Status Code: ").append(errorMap.get("javax.servlet.error.status_code")).append("\n\n");
		sb.append("Servlet Name: ").append(errorMap.get("javax.servlet.error.servlet_name")).append("\n\n");
		sb.append("Stack Trace: ").append(getStackTrace(errorMap));
		error = sb.toString();
		
		String requestCurrent = (String) errorMap.get("javax.servlet.error.request_uri");
		if(requestCurrent!=null && requestCurrent.length()>0){
			requestUri = requestCurrent;
		}

		return error;
	}
	
	public void setError(String error) {
		this.error = error;
	}
	
	public void sendError() {
		try {
			String subject = "PinPoint Error Report";
			StringBuffer content = new StringBuffer();
			
			content.append("Name: ").append(this.name).append("\n");
			content.append("Company Name: ").append(this.companyName).append("\n");
			content.append("Phone Number: ").append(this.phone).append("\n\n");
			
			content.append(getError()).append("\n\n");
			
			try {
				List<UserSecurityDTO> userList = userSecurityBean.getUserSecurityList();
				if(userList != null) {
					content.append("Users in the system:\n");
					for(UserSecurityDTO u : userList) {
						content.append(String.format("%s - %s - %s\n", u.getUserName(), u.getEmail(), u.getRoleName()));
					}
				}
			}
			catch(Exception e){
				e.printStackTrace();
			}
			
			userSecurityBean.sendErrorReport(SUPPORT_EMAIL, subject, content.toString());
			FacesMessage message = new FacesMessage("Email has been sent to support team");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
		}
		catch(Exception e) {
			String msg = "Failed to email error report to support team. Please send error details to " + SUPPORT_EMAIL;
			FacesMessage message = new FacesMessage(msg);
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
		}
	}
	
	public String getStackTrace(Map<String, Object> map) {
        Throwable ex = (Throwable) map.get("javax.servlet.error.exception");

        StringWriter writer = new StringWriter();
        PrintWriter pw = new PrintWriter(writer);
        fillStackTrace(ex, pw);

        return writer.toString();
    }
	
	private void fillStackTrace(Throwable ex, PrintWriter pw) {
        if (null == ex) {
            return;
        }

        ex.printStackTrace(pw);

        if (ex instanceof ServletException) {
            Throwable cause = ((ServletException) ex).getRootCause();

            if (null != cause) {
                pw.println("Root Cause:");
                fillStackTrace(cause, pw);
            }
        } else {
            Throwable cause = ex.getCause();

            if (null != cause) {
                pw.println("Cause:");
                fillStackTrace(cause, pw);
            }
        }
    }

	public HtmlOutputText getAppVersion() {
		return appVersion;
	}

	public void setAppVersion(HtmlOutputText appVersion) {
		this.appVersion = appVersion;
	}
	
	public String getSupportEmail() {
		return SUPPORT_EMAIL;
	}

	public String getAppVersionString() {
		if(!StringUtils.hasText(this.appVersionString) && this.appVersion != null) {
			this.appVersionString = (String) this.appVersion.getAttributes().get("appVersionString");
		}
		
		return this.appVersionString;
	}

	public void setAppVersionString(String appVersionString) {
		this.appVersionString = appVersionString;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	public boolean getDisableSend() {
		return !StringUtils.hasText(companyName);
	}
	
	public void backAction() {
		FacesContext facesContext = FacesContext.getCurrentInstance();
		try {
			String backUrl = this.requestUri;
			if(jsessionid!=null && jsessionid.length()>0 && backUrl!=null && backUrl.length()>0){
				backUrl = backUrl + ";jsessionid=" + jsessionid;
			}
			facesContext.getExternalContext().redirect(backUrl);
			facesContext.responseComplete();
		}
		catch(Exception e) {
			
		}
	}
}
