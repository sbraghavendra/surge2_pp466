package com.ncsts.view.bean;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.ActionEvent;

import org.ajax4jsf.component.UIDataAdaptor;
import org.apache.log4j.Logger;
import org.apache.myfaces.custom.fileupload.HtmlInputFileUpload;
import org.richfaces.component.html.HtmlCalendar;
import org.springframework.beans.factory.annotation.Autowired;

import com.ncsts.common.CacheManager;
import com.ncsts.common.DatabaseUtil;
import com.ncsts.domain.Auditable;
import com.ncsts.domain.BCPTaxCodeRefDoc;
import com.ncsts.domain.BCPTaxCodeRefDocText;
import com.ncsts.domain.BCPTaxCodeRule;
import com.ncsts.domain.BCPTaxCodeRuleText;
import com.ncsts.domain.BatchErrorLog;
import com.ncsts.domain.BatchMaintenance;
import com.ncsts.domain.BatchMetadata;
import com.ncsts.domain.ListCodes;
import com.ncsts.domain.TaxCodeRulesBatch;
import com.ncsts.domain.TaxRateUpdatesBatch;
import com.ncsts.domain.User;
import com.ncsts.dto.BatchMetadataList;
import com.ncsts.dto.DbPropertiesDTO;
import com.ncsts.jsf.model.BCPTaxCodeRefDocDataModel;
import com.ncsts.jsf.model.BCPTaxCodeRefDocTextDataModel;
import com.ncsts.jsf.model.BCPTaxCodeRuleDataModel;
import com.ncsts.jsf.model.BCPTaxCodeRuleTextDataModel;
import com.ncsts.jsf.model.BatchMaintenanceDataModel;
import com.ncsts.management.DbPropertiesBean;
import com.ncsts.services.BatchMaintenanceService;
import com.ncsts.view.util.BatchStatistics;
import com.ncsts.view.util.FacesUtils;
import com.ncsts.view.util.StateObservable;
import com.ncsts.view.util.TaxCodeRefDocUploadParser;
import com.ncsts.view.util.TaxCodeRulesBatchValidator;
import com.ncsts.view.util.TaxCodeRulesUploadParser;

public class TaxCodeRulesBackingBean {
	private Logger logger = Logger.getLogger(TaxCodeRulesBackingBean.class);
	private StateObservable ov = null;

	private enum AddState {
		NEED_FILE, HAVE_FILE, PROCESSING, COMPLETE
	}

	private BatchMaintenanceBackingBean batchMaintenanceBean;
	private FilePreferenceBackingBean filePreferenceBean;
	private BatchMaintenanceService batchMaintenanceService;
	private BatchStatistics batchStatistics;
	private BatchErrorsBackingBean batchErrorsBean;
	private BatchMaintenance exampleBatchMaintenance;

	private BatchMaintenance selectedBatch = null;
	private int selectedBatchIndex = -1;
	private List<TaxCodeRulesBatch> selectedBatches;

	private BatchMaintenanceDataModel batchMaintenanceDataModel;
	private BatchMetadata batchMetadata;

	private BatchMetadataList batchMetadataList;

	private BCPTaxCodeRuleDataModel bcpTaxCodeRuleDataModel;
	private BCPTaxCodeRuleTextDataModel bcpTaxCodeRuleTextDataModel;
	
	private BCPTaxCodeRefDocDataModel bcpTaxCodeRefDocDataModel;
	private BCPTaxCodeRefDocTextDataModel bcpTaxCodeRefDocTextDataModel;

	private Thread background;
	private String backgroundError;
	private AddState state;
	
	private HtmlCalendar batchStartTime = new HtmlCalendar();

	private TaxCodeRulesUploadParser taxCodeRulesUploadParser;
	
	private TaxCodeRefDocUploadParser taxCodeRefDocUploadParser;

	private HtmlInputFileUpload uploadFile;

	private DbPropertiesBean dbPropertiesBean;
	private DbPropertiesDTO dbPropertiesDTO = null;
	
	private static String TAXCODE_FILETYPE_CODE = "TAXCODE";
	
	private static String REFDOC_FILETYPE_CODE = "REFDOC";
	
	private static String TAXCODE_RULE_SEQ_NAME = "sq_tb_bcp_taxcode_rule_id";
	
	private static String TAXCODE_REFDOC_SEQ_NAME = "sq_tb_bcp_taxcode_refdoc_id";
	
	private static String REFDOC_CODE = "REFDOC";
	
	private static String TAXCODE_CODE = "TAXCODE";
	
	private String fileTypeCode;
	
	@Autowired
	private loginBean loginbean;
	
	@Autowired
	private CacheManager cacheManager;
	
	public void setCacheManager(CacheManager cacheManager) {
		this.cacheManager = cacheManager;
	}

	public void setFileTypeCode(String fileTypeCode) {
		this.fileTypeCode = fileTypeCode;
	}

	public BCPTaxCodeRefDocDataModel getBcpTaxCodeRefDocDataModel() {
		return bcpTaxCodeRefDocDataModel;
	}

	public void setBcpTaxCodeRefDocDataModel(
			BCPTaxCodeRefDocDataModel bcpTaxCodeRefDocDataModel) {
		this.bcpTaxCodeRefDocDataModel = bcpTaxCodeRefDocDataModel;
	}

	public BCPTaxCodeRefDocTextDataModel getBcpTaxCodeRefDocTextDataModel() {
		return bcpTaxCodeRefDocTextDataModel;
	}

	public void setBcpTaxCodeRefDocTextDataModel(
			BCPTaxCodeRefDocTextDataModel bcpTaxCodeRefDocTextDataModel) {
		this.bcpTaxCodeRefDocTextDataModel = bcpTaxCodeRefDocTextDataModel;
	}

	public BatchMaintenanceBackingBean getBatchMaintenanceBean() {
		return batchMaintenanceBean;
	}

	public void setBatchMaintenanceBean(
			BatchMaintenanceBackingBean batchMaintenanceBean) {
		this.batchMaintenanceBean = batchMaintenanceBean;
	}


	public BatchMaintenance getSelectedBatch() {
		return selectedBatch;
	}

	public void setSelectedBatch(BatchMaintenance selectedBatch) {
		this.selectedBatch = selectedBatch;
	}

	public int getSelectedBatchIndex() {
		return selectedBatchIndex;
	}

	public void setSelectedBatchIndex(int selectedBatchIndex) {
		this.selectedBatchIndex = selectedBatchIndex;
	}

	public BatchMaintenanceDataModel getBatchMaintenanceDataModel() {
		if (exampleBatchMaintenance == null) {
			init();
		}
		return batchMaintenanceDataModel;
	}

	public void setBatchMaintenanceDataModel(
			BatchMaintenanceDataModel batchMaintenanceDataModel) {
		this.batchMaintenanceDataModel = batchMaintenanceDataModel;
	}

	public BatchMetadata getBatchMetadata() {
		if (this.batchMetadata == null) {
			this.batchMetadata = batchMaintenanceService
					.findBatchMetadataById(TaxCodeRulesBatch.BATCH_CODE);
		}
		return batchMetadata;
	}

	public void setBatchMetadata(BatchMetadata batchMetadata) {
		this.batchMetadata = batchMetadata;
	}

	public BatchMaintenanceService getBatchMaintenanceService() {
		return batchMaintenanceService;
	}

	public void setBatchMaintenanceService(
			BatchMaintenanceService batchMaintenanceService) {
		this.batchMaintenanceService = batchMaintenanceService;
	}

	public String processAddUpdateAction() {
		background = null;
		state = AddState.NEED_FILE;
		fileTypeCode = null;
		
		if(taxCodeRulesUploadParser != null) {
	       	taxCodeRulesUploadParser.clear();
		}
		if(taxCodeRefDocUploadParser != null){
			taxCodeRefDocUploadParser.clear();
		}
		
		this.uploadFile = new HtmlInputFileUpload();
		return "tax_code_rules_add";
	}

	public HtmlInputFileUpload getUploadFile() {
		return uploadFile;
	}

	public void setUploadFile(HtmlInputFileUpload uploadFile) {
		this.uploadFile = uploadFile;
	}

	public String selectAllAction() {
		batchMaintenanceDataModel.setSelectAllTaxCodeRulesUpdates();
		return null;
	}

	public Boolean getDisplayProcess() {
		if ((selectedBatchIndex != -1) && (selectedBatch != null)
				&& (TaxCodeRulesBatch.canBeProcessed(selectedBatch)))
			return true;
		return getHasSelection();
	}

	private boolean getHasSelection() {
		Iterator<BatchMaintenance> iter = batchMaintenanceDataModel.iterator();
		while (iter.hasNext()) {
			if (iter.next().getSelected()) {
				return true;
			}
		}
		return false;
	}

	public String processAction() {
		TaxCodeRulesBatchValidator validator = new TaxCodeRulesBatchValidator(
				batchMaintenanceDataModel,
				filePreferenceBean.getImportMapProcessPreferenceDTO());
		Iterator<BatchMaintenance> iter = batchMaintenanceDataModel.iterator();
		selectedBatches = new ArrayList<TaxCodeRulesBatch>();
		while (iter.hasNext()) {
			BatchMaintenance bm = iter.next();
			if (bm.getSelected())
				selectedBatches.add(new TaxCodeRulesBatch(bm));
		}
		if ((selectedBatches.size() < 1) && (selectedBatch != null))
			selectedBatches.add(new TaxCodeRulesBatch(selectedBatch));
		// Iterate through batches in reverse order, adjusting last processed
		// along the way
		Collections.sort(selectedBatches, new BatchComparator());
		for (int i = selectedBatches.size(); i > 0; i--) {
			validator.validate(selectedBatches.get(i - 1));
		}

		return "tax_code_rules_submit_batches";
	}

	public class BatchComparator implements Comparator<TaxCodeRulesBatch> {
		@Override
		public int compare(TaxCodeRulesBatch o1, TaxCodeRulesBatch o2) {
			if (o1.getReleaseDate().getTime() > o2.getReleaseDate().getTime())
				return -1;
			else if (o1.getReleaseDate().getTime() < o2.getReleaseDate()
					.getTime())
				return 1;
			else {
				if (o1.getReleaseVersion() > o2.getReleaseVersion())
					return -1;
				else if (o1.getReleaseVersion() < o2.getReleaseVersion())
					return 1;
			}
			return 0;
		}

	}

	public FilePreferenceBackingBean getFilePreferenceBean() {
		return filePreferenceBean;
	}

	public void setFilePreferenceBean(
			FilePreferenceBackingBean filePreferenceBean) {
		this.filePreferenceBean = filePreferenceBean;
	}

	public String statisticsAction() {
		batchStatistics = new BatchStatistics(selectedBatch,
				batchMaintenanceDataModel.getBatchMaintenanceDAO(),
				filePreferenceBean.getBatchPreferenceDTO());
		batchStatistics.calcStatistics();
		return "tax_code_rules_batch_statistics";
	}

	public BatchStatistics getBatchStatistics() {
		return batchStatistics;
	}

	public void setBatchStatistics(BatchStatistics batchStatistics) {
		this.batchStatistics = batchStatistics;
	}

	public String errorsAction() {
		batchErrorsBean.setBatchMaintenance(selectedBatch);
		batchErrorsBean.setReturnView("tax_code_rules");
		return "batch_maintenance_errors";
	}

	public BatchErrorsBackingBean getBatchErrorsBean() {
		return batchErrorsBean;
	}

	public void setBatchErrorsBean(BatchErrorsBackingBean batchErrorsBean) {
		this.batchErrorsBean = batchErrorsBean;
	}

	public Boolean getDisplayError() {
		return (selectedBatchIndex != -1)
				&& (selectedBatch.getErrorSevCode() != null)
				&& (selectedBatch.getErrorSevCode().trim().length() > 0);
	}

	public String viewAction() {
		if(TAXCODE_CODE.equals(selectedBatch.getVc03())){
			bcpTaxCodeRuleDataModel.setBatchMaintenance(selectedBatch);
			if (getUploadRecordCount()) {
				return viewFormattedAction();
			} else {
				return viewUnformattedAction();
			}
		}
		else {
			bcpTaxCodeRefDocDataModel.setBatchMaintenance(selectedBatch);
			if (getUploadRefDocRecordCount()) {
				return viewFormattedRefDocAction();
			} else {
				return viewUnformattedRefDocAction();
			}
		}
	}
	
	public Boolean getUploadRecordCount() {
		return (bcpTaxCodeRuleDataModel.count() > 0);
	}
	
	public Boolean getUploadRefDocRecordCount() {
		return (bcpTaxCodeRefDocDataModel.count() > 0);
	}

	public String viewFormattedAction() {
		bcpTaxCodeRuleDataModel.setBatchMaintenance(selectedBatch);
		return "tax_code_rules_view_formatted";
	}

	public String viewUnformattedAction() {
		bcpTaxCodeRuleTextDataModel.setBatchMaintenance(selectedBatch);
		return "tax_code_rules_view_unformatted";
	}
	
	public String viewFormattedRefDocAction() {
		bcpTaxCodeRefDocDataModel.setBatchMaintenance(selectedBatch);
		return "tax_code_refdoc_view_formatted";
	}

	public String viewUnformattedRefDocAction() {
		bcpTaxCodeRefDocTextDataModel.setBatchMaintenance(selectedBatch);
		return "tax_code_refdoc_view_unformatted";
	}

	public BCPTaxCodeRuleDataModel getBcpTaxCodeRuleDataModel() {
		return bcpTaxCodeRuleDataModel;
	}

	public void setBcpTaxCodeRuleDataModel(
			BCPTaxCodeRuleDataModel bcpTaxCodeRuleDataModel) {
		this.bcpTaxCodeRuleDataModel = bcpTaxCodeRuleDataModel;
	}

	public BCPTaxCodeRuleTextDataModel getBcpTaxCodeRuleTextDataModel() {
		return bcpTaxCodeRuleTextDataModel;
	}

	public void setBcpTaxCodeRuleTextDataModel(
			BCPTaxCodeRuleTextDataModel bcpTaxCodeRuleTextDataModel) {
		this.bcpTaxCodeRuleTextDataModel = bcpTaxCodeRuleTextDataModel;
	}

	public String refreshAction() {
		init();
		batchMaintenanceDataModel.refreshData(false);
		selectedBatchIndex = -1;
		return "taxCodeRulesUploadSuccess";
	}

	private void init() {
		batchMaintenanceDataModel.setDefaultSortOrder("batchId",
				BatchMaintenanceDataModel.SORT_DESCENDING);
		exampleBatchMaintenance = new BatchMaintenance();
		batchMaintenanceBean.resetSearchAction(); 
		exampleBatchMaintenance.setEntryTimestamp(null);
		exampleBatchMaintenance.setBatchStatusCode(null);
		exampleBatchMaintenance.setBatchTypeCode(TaxCodeRulesBatch.BATCH_CODE);
		batchMaintenanceBean.setBatchTypeCode(TaxCodeRulesBatch.BATCH_CODE) ; 
		batchMaintenanceBean.setCalledFromConfigImportExpMenu(false);
	    batchMaintenanceBean.prepareBatchTypeSpecificsFields();
		batchMaintenanceDataModel.setPageSize(50);
		batchMaintenanceDataModel.setCriteria(exampleBatchMaintenance);
	}

	public String getLastTaxCodeRuleRelUpdate() {
		return filePreferenceBean.getImportMapProcessPreferenceDTO()
				.getLastTaxCodeRulesDateUpdate();
	}

	public boolean getDisplayFileSelect() {
		return state == AddState.NEED_FILE;
	}

	public boolean getDisplayFileHeader() {
		switch (state) {
		case HAVE_FILE:
			return true;
		case PROCESSING:
			return true;
		case COMPLETE:
			return true;
		default:
			return false;
		}
	}

	public boolean getDisplayChangeFile() {
		return state == AddState.HAVE_FILE;
	}

	public String getUploadFileName() {
		if(TAXCODE_FILETYPE_CODE.equals(this.fileTypeCode)) {
	       	return taxCodeRulesUploadParser.getUploadFileName();
		}
		return taxCodeRefDocUploadParser.getUploadFileName();
	}

	public Boolean getDisplayUploadAdd() {
		logger.debug("displayUploadAdd: " + state);
		return state == AddState.HAVE_FILE;
	}

	public String getFileType() {
		if(TAXCODE_FILETYPE_CODE.equals(this.fileTypeCode)) {
	       	return taxCodeRulesUploadParser.getFileType();
		}
		return taxCodeRefDocUploadParser.getFileType();
	}

	public Date getReleaseDate() {
		if(TAXCODE_FILETYPE_CODE.equals(this.fileTypeCode)) {
			return taxCodeRulesUploadParser.getReleaseDate();
		}
		return taxCodeRefDocUploadParser.getReleaseDate();
	}

	public String getReleaseVersion() {
		if(TAXCODE_FILETYPE_CODE.equals(this.fileTypeCode)) {
			return taxCodeRulesUploadParser.getReleaseVersion();
		}
		return taxCodeRefDocUploadParser.getReleaseVersion();
	}

	public String getProgress() {
		if (backgroundError != null) {
			addError(backgroundError);
			return "100.0";
		} else if (state == AddState.COMPLETE) {
			logger.debug("getProgress: complete");
			return "100.0";
		}
		Double scale = 0.0;
		NumberFormat nf = NumberFormat.getNumberInstance();
		nf.setMaximumFractionDigits(1);
		try {
			String s = "0.0";
			if(TAXCODE_FILETYPE_CODE.equals(fileTypeCode)) {
				scale = taxCodeRulesUploadParser.getProgress() * 99.0;
			}
			else {
				scale = taxCodeRefDocUploadParser.getProgress() * 99.0;
			}
			if (scale >= 0.01) {
				s = nf.format(scale);
			}
			logger.debug("Progress at: " + s);
			return s;
		} catch (Exception e) {
			logger.error(e.getMessage());
			return "0.0";
		}
	}

	private void addError(String s) {
		addMessage(s, FacesMessage.SEVERITY_ERROR);
	}

	private void addMessage(String s, FacesMessage.Severity severity) {
		FacesContext.getCurrentInstance().addMessage(null,
				new FacesMessage(severity, s, null));
	}

	public Boolean getDisplayProgress() {
		if (background != null)
			logger.debug("Progress: " + state.toString() + ", "
					+ background.getState().toString());
		else
			logger.debug("Progress: " + state.toString());
		switch (state) {
		case PROCESSING:
			return true;
		case COMPLETE:
			return true;
		default:
			return false;
		}
	}

	public boolean getDisplayComplete() {
		return state == AddState.COMPLETE;
	}

	public String backgroundCompleteAction() {
		background = null;

		batchMaintenanceDataModel.refreshData(false);
		selectedBatchIndex = -1;
		selectedBatch = null;
		return "tax_code_rules_cancel_action";
	}

	public String cancelAction() {
		background = null;
		
		if(this.fileTypeCode == null) {
			return "tax_code_rules_cancel_action";
		}
		else if(TAXCODE_FILETYPE_CODE.equals(fileTypeCode)) {
	       	taxCodeRulesUploadParser.clear();
		}
		else if(REFDOC_FILETYPE_CODE.equals(fileTypeCode)) {
			taxCodeRefDocUploadParser.clear();
		}
		return "tax_code_rules_cancel_action";
	}

	public String getLastTaxCodeRulesRelUpdate() {
		return filePreferenceBean.getImportMapProcessPreferenceDTO()
				.getLastTaxCodeRulesDateUpdate();
	}

	public void fileUploadAction() throws IOException {

		fileTypeCode = getFileTypeCode(); 
		
		if(TAXCODE_FILETYPE_CODE.equals(fileTypeCode)) {
			taxCodeRulesUploadParser = new TaxCodeRulesUploadParser(FacesUtils.getServletContext().getRealPath("") + "/taxcoderulesfile/");
			if (this.uploadFile.getUploadedFile() != null) {
				boolean foundError = false; // 4323 - see below
				taxCodeRulesUploadParser.upload(this.uploadFile.getUploadedFile().getName(),
						this.uploadFile.getUploadedFile().getInputStream());
				for (String m : taxCodeRulesUploadParser.getErrors()) {
					addError(m);
					foundError = true;
				}
				if (foundError == false) {
					state = AddState.HAVE_FILE; // //4323 -- prior to this we were
												// always changing state and
												// advancing screen even if there
												// was no file etc...
				}
			}
		}
		else {
			taxCodeRefDocUploadParser  = new TaxCodeRefDocUploadParser(FacesUtils.getServletContext().getRealPath("") + "/taxcoderulesfile/");
			if (this.uploadFile.getUploadedFile() != null) {
				boolean foundError = false; // 4323 - see below
				taxCodeRefDocUploadParser.upload(this.uploadFile.getUploadedFile().getName(),
						this.uploadFile.getUploadedFile().getInputStream());
				for (String m : taxCodeRefDocUploadParser.getErrors()) {
					addError(m);
					foundError = true;
				}
				if (foundError == false) {
					state = AddState.HAVE_FILE; // //4323 -- prior to this we were
												// always changing state and
												// advancing screen even if there
												// was no file etc...
				}
			}
		}
		
		
	}
	
	public String getFileTypeCode() { 
		String fileType = null;
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(this.uploadFile.getUploadedFile().getInputStream()));
			String firstLine = reader.readLine();		
			fileType = UpdateFilesBackingBean.getHeaderValueOfType(firstLine, UpdateFilesBackingBean.FILE_VALUE);
			
		}catch(Exception e) {
			System.out.println("Exception occured at the time of reading file....");
		}
		return fileType;
	}

	public String addAction() {
		selectedBatch = new BatchMaintenance();
		// since this runs in a background thread, it has no access to a
		// FacesContext, so we need to get it now in foreground
		selectedBatch.setUpdateUserId(Auditable.currentUserCode());
		selectedBatch.setUpdateTimestamp(new Date());
		selectedBatch.setBatchTypeCode(TaxCodeRulesBatch.BATCH_CODE);
		selectedBatch.setBatchStatusCode(TaxCodeRulesBatch.IMPORTING);
		if(TAXCODE_FILETYPE_CODE.equals(this.fileTypeCode)) {
			selectedBatch.setVc03(TAXCODE_CODE);
			selectedBatch.setTotalBytes(taxCodeRulesUploadParser.getTotalBytes());
			selectedBatch.setVc01(taxCodeRulesUploadParser.getUploadFileName());
			selectedBatch.setVc02(taxCodeRulesUploadParser.getFileType());
			selectedBatch.setNu01(new Double(taxCodeRulesUploadParser.getReleaseVersion()));
			selectedBatch.setTs01(taxCodeRulesUploadParser.getReleaseDate());
		}
		 else {
			 selectedBatch.setVc03(REFDOC_CODE);
			 selectedBatch.setTotalBytes(taxCodeRefDocUploadParser.getTotalBytes());
			 selectedBatch.setVc01(taxCodeRefDocUploadParser.getUploadFileName());
			 selectedBatch.setVc02(taxCodeRefDocUploadParser.getFileType());
			 selectedBatch.setNu01(new Double(taxCodeRefDocUploadParser.getReleaseVersion()));
			 selectedBatch.setTs01(taxCodeRefDocUploadParser.getReleaseDate());
		 } 
		
		selectedBatch.setEntryTimestamp(new Date());
		selectedBatch.setBatchId(null); // new record
		batchMaintenanceDataModel.getBatchMaintenanceDAO().save(selectedBatch);

		processUpdateInBackground(selectedBatch);
		selectedBatch = null;
		return null;
	}
	
	public static DateFormat logDateformat = new SimpleDateFormat(
			"yyyy-MM-dd HH:mm:ss.SSS");
    private Connection conn = null;
	private BufferedWriter outAID = null;
	long startTime = 0;
	
	public void writeLogAID(String message) {
		writeLogAID(message, true);
	}

	public void writeLogAID(String message, boolean email) {
		Date d = new Date(System.currentTimeMillis());
		String currentDateTime = logDateformat.format(d);
		try {
			outAID.write("[" + currentDateTime + "] " + message);
			outAID.newLine();
			outAID.flush();
		} catch (Exception e) {
		}
	}
	
	public boolean initVerification(Connection connection, BufferedWriter out) {
		this.conn = connection;
		this.outAID = out;
		
		startTime = System.currentTimeMillis();
		writeLogAID("************ Time counted ..... ************");
		writeLogAID(" ");

		try {
			writeLogAID("Database URL:      " + conn.getMetaData().getURL());
		} catch (Exception e) {
		}

		return true;
	}
	
    public String addActionAID(String fileName, String filePath, String folderPath) {
    	
    	if(TAXCODE_FILETYPE_CODE.equals(fileTypeCode)) {
    		return addActionTaxCodeAID(fileName,filePath, folderPath);
    	}
    	else {
    		return addActionRefDocAID(fileName,filePath, folderPath);
    	}
    }
    
    public String addActionTaxCodeAID(String fileName, String filePath, String folderPath) { 
    	writeLogAID("File Imported:    " + filePath); // We actually use this
		// file to import.
    	writeLogAID(" ");
    	writeLogAID("** Started import TaxCodes procedure");

    	try {
            boolean foundError = false; //4323 - see below
            InputStream inputStream = new FileInputStream(filePath);
            taxCodeRulesUploadParser = new TaxCodeRulesUploadParser(folderPath);
            taxCodeRulesUploadParser.upload(fileName, inputStream);
            
            for (String m : taxCodeRulesUploadParser.getErrors()) {
                addError(m);
                foundError = true;
            }
            if (foundError == false) {
               // state = AddState.HAVE_FILE; ////4323 -- prior to this we were always changing state and advancing screen even if there was no file etc...
            }
            
            if(taxCodeRulesUploadParser.isprocessHeaderMissing()){
            	setBatchMaintenance(); 
            	selectedBatch.setBatchStatusCode("ABI");
            	selectedBatch.setErrorSevCode("30");
            	writeLogAID("** Import File Missing Key Components - Import Aborted **");
            	BatchMaintenance batchMaintenance = ImportMapAddBean.addBatchUsingJDBC(selectedBatch, conn); //for creating batch Id
	        	setBatchErrorLog(batchMaintenance, conn);
	        	return "";
            }
            
		} catch (IOException dfe) {
			writeLogAID("Error: Importing stopped with error, " + dfe.getMessage());
			return null;
		}
    	
    	setBatchMaintenance();
    	return processTaxCodeFileTypeAndBatch(folderPath);
    
    }
    
    public String addActionRefDocAID(String fileName, String filePath, String folderPath) { 
    	writeLogAID("File Imported:    " + filePath); // We actually use this
		// file to import.
    	writeLogAID(" ");
    	writeLogAID("** Started import TaxCodes procedure");

    	try {
            boolean foundError = false; //4323 - see below
            InputStream inputStream = new FileInputStream(filePath);
            taxCodeRefDocUploadParser = new TaxCodeRefDocUploadParser(folderPath);
            taxCodeRefDocUploadParser.upload(fileName, inputStream);
            
            for (String m : taxCodeRefDocUploadParser.getErrors()) { 
                addError(m);
                foundError = true;
            }
            
            if (foundError == false) {
               // state = AddState.HAVE_FILE; ////4323 -- prior to this we were always changing state and advancing screen even if there was no file etc...
            }
            
            if(taxCodeRefDocUploadParser.isprocessHeaderMissing()){
            	setBatchMaintenance(); 
                selectedBatch.setBatchStatusCode("ABI");
                selectedBatch.setErrorSevCode("30");
                writeLogAID("** Import File Missing Key Components - Import Aborted **");
            	BatchMaintenance batchMaintenance = ImportMapAddBean.addBatchUsingJDBC(selectedBatch, conn); //for creating batch Id
	        	setBatchErrorLog(batchMaintenance, conn);
	        	return "";
            }
        
		} catch (IOException dfe) {
			writeLogAID("Error: Importing stopped with error, " + dfe.getMessage());
			return null;
		}
    	
    	setBatchMaintenance();
    	return processTaxCodeFileTypeAndBatch(folderPath);
    
    }
    
    public void setBatchMaintenance() {
    	
    	selectedBatch = new BatchMaintenance();
	    selectedBatch.setUpdateUserId("STSCORP");
        selectedBatch.setUpdateTimestamp(new Date());
        selectedBatch.setBatchTypeCode(TaxCodeRulesBatch.BATCH_CODE);
        selectedBatch.setBatchStatusCode(TaxCodeRulesBatch.IMPORTING);
    	
    	if(TAXCODE_FILETYPE_CODE.equals(fileTypeCode)) {
    		selectedBatch.setTotalBytes(taxCodeRulesUploadParser.getTotalBytes());
	        selectedBatch.setVc01(taxCodeRulesUploadParser.getUploadFileName());
	        selectedBatch.setVc02(taxCodeRulesUploadParser.getFileType());
	        selectedBatch.setVc03(TAXCODE_CODE);
	        selectedBatch.setTs01(taxCodeRulesUploadParser.getReleaseDate());
	        selectedBatch.setEntryTimestamp(new Date());
	        selectedBatch.setBatchId(null); // new record
	        //fixed for 0002743
	        if (taxCodeRulesUploadParser.getReleaseVersion() != null){
	            selectedBatch.setNu01(new Double(taxCodeRulesUploadParser.getReleaseVersion()));
	        }
    	}
    	else {
    		selectedBatch.setTotalBytes(taxCodeRefDocUploadParser.getTotalBytes());
            selectedBatch.setVc01(taxCodeRefDocUploadParser.getUploadFileName());
            selectedBatch.setVc02(taxCodeRefDocUploadParser.getFileType());
            selectedBatch.setVc03(REFDOC_CODE);
            selectedBatch.setTs01(taxCodeRefDocUploadParser.getReleaseDate());
            selectedBatch.setEntryTimestamp(new Date());
            selectedBatch.setBatchId(null); // new record
            //fixed for 0002743
            if (taxCodeRefDocUploadParser.getReleaseVersion() != null){
                selectedBatch.setNu01(new Double(taxCodeRefDocUploadParser.getReleaseVersion()));
            }
    	}
    	
    }
    
    public String processTaxCodeFileTypeAndBatch(String folderPath) {
    	 BatchMaintenance batchMaintenance = ImportMapAddBean.addBatchUsingJDBC(selectedBatch, conn);
         if (batchMaintenance == null) {
 			// Cannot create a new batch
 			writeLogAID("Error: Cannot create new Batch Id. Importing stopped.");

 			return null;
 		}
 		writeLogAID("Batch Id: " + batchMaintenance.getBatchId() + " created, Batch Status Code: " + selectedBatch.getBatchStatusCode());

 		processUpdateInBackgroundAID(selectedBatch, conn, folderPath);
        selectedBatch = null;
        return null;
    }
    
    private void processUpdateInBackgroundAID(final BatchMaintenance batch, Connection conn, String folderPath) {  
         try {
             writeLogAID("Importing data file....");

             //Import TaxCodes update
             
             writeLogAID("Importing data file.... done");
             
             if(TAXCODE_FILETYPE_CODE.equals(fileTypeCode)) {
            	 importTaxCodeRuleUpdatesUsingJDBC(batch, taxCodeRulesUploadParser, conn);
            	 
            	  //4947
                 if (taxCodeRulesUploadParser.getBatchErrorLogs().size() > 0) {
                     batch.setHeldFlag("1");
                     batch.setErrorSevCode(BatchMaintenance.ErrorState.Error);
                     backgroundError = "Error count: " + taxCodeRulesUploadParser.getBatchErrorLogs().size();
                 }//Fixed for issue 0002743
                 if (taxCodeRulesUploadParser.isAbort())
                     batch.setBatchStatusCode(TaxRateUpdatesBatch.ABORTED);
                 else
                     batch.setBatchStatusCode("IA");
                 batch.setNu02(taxCodeRulesUploadParser.getSpecialRateTotal().doubleValue());
                 batch.setTotalRows(new Long(taxCodeRulesUploadParser.getRows()));
             }
             
             else {
            	 importTaxCodeRefDocUpdatesUsingJDBC(batch, taxCodeRefDocUploadParser, conn);
            	  //4947
                 if (taxCodeRefDocUploadParser.getBatchErrorLogs().size() > 0) {
                     batch.setHeldFlag("1");
                     batch.setErrorSevCode(BatchMaintenance.ErrorState.Error);
                     backgroundError = "Error count: " + taxCodeRefDocUploadParser.getBatchErrorLogs().size();
                 }//Fixed for issue 0002743
                 if (taxCodeRefDocUploadParser.isAbort())
                     batch.setBatchStatusCode(TaxRateUpdatesBatch.ABORTED);
                 else
                     batch.setBatchStatusCode("IA");
                 batch.setTotalRows(new Long(taxCodeRefDocUploadParser.getRows()));
             }
             
             batch.setStatusUpdateTimestamp(new Date());
             batch.setStatusUpdateUserId(batch.getUpdateUserId());
             
             updateBatchUsingJDBC(batch, conn);
             
             //Continue to process the batch when HeldFlag is not 1 and BatchStatusCode is 'IA'
             if((batch.getHeldFlag()==null || !batch.getHeldFlag().equalsIgnoreCase("1")) && batch.getBatchStatusCode().equalsIgnoreCase("IA")){
 				writeLogAID("** Started 'Flagged for Process' procedure for Batch Id, " + batch.getbatchId());

 				batch.setBatchStatusCode("FP");
 				// Control already done with the timezone conversion, UTC
 				batch.setSchedStartTimestamp(new Date());
 				batch.setUpdateTimestamp(new Date());
 				batch.setStatusUpdateTimestamp(new Date());
 				batch.setStatusUpdateUserId("STSCORP");
 				updateBatchUsingJDBC(batch, conn);

 				writeLogAID("Set Batch Status Code to FP");
 				writeLogAID("** Finished 'Flagged for Process' procedure");
 				writeLogAID(" ");
 			}
         }catch (Throwable e)
         {
             e.printStackTrace();
             
             writeLogAID("Error: Importing aborted with error, " + e.getMessage());

         }finally
         {
         }
     }

	private void processUpdateInBackground(final BatchMaintenance batch) {
		String selectedDb = ChangeContextHolder.getDataBaseType();
		dbPropertiesDTO = dbPropertiesBean.getDbPropertiesDTO(selectedDb);

		state = AddState.PROCESSING;
		backgroundError = null;
		background = new Thread(new Runnable() {
			public void run() {
				System.out
						.println("############ processUpdateInBackground for Tax Rate update using URL: "
								+ dbPropertiesDTO.getUrl());

				try {
					Thread.sleep(2000);
				} catch (Exception ex) {
				}

				try {
					Class.forName(dbPropertiesDTO.getDriverClassName());
				} catch (ClassNotFoundException e) {
					System.out.println("############ Can't load class: "
							+ dbPropertiesDTO.getDriverClassName());
					return;
				}
				Connection conn = null;

				try {
					// Standalone connection
					conn = DriverManager.getConnection(
							dbPropertiesDTO.getUrl(),
							dbPropertiesDTO.getUserName(),
							dbPropertiesDTO.getPassword());
					conn.setAutoCommit(false);

					if(TAXCODE_FILETYPE_CODE.equals(fileTypeCode)) {
						List<BatchErrorLog> taxcodeErrorBatchList = taxCodeRulesUploadParser.getBatchErrorLogs();
						if(taxcodeErrorBatchList != null && taxcodeErrorBatchList.size() > 0) {
							taxCodeRulesUploadParser.resetBatchErrorLogs();
						}
						importTaxCodeRuleUpdatesUsingJDBC(batch, taxCodeRulesUploadParser, conn);
						batch.setNu02(taxCodeRulesUploadParser.getSpecialRateTotal().doubleValue());
						 if (taxCodeRulesUploadParser.getBatchErrorLogs().size() > 0) {
								batch.setHeldFlag("1");
								batch.setErrorSevCode(BatchMaintenance.ErrorState.Error);
						}
						batch.setTotalRows(new Long(taxCodeRulesUploadParser.getRows()));
					}
					else {
						List<BatchErrorLog> refdocErrorBatchList = taxCodeRefDocUploadParser.getBatchErrorLogs();
						if(refdocErrorBatchList != null && refdocErrorBatchList.size() > 0) {
							taxCodeRefDocUploadParser.resetBatchErrorLogs();
						}
						importTaxCodeRefDocUpdatesUsingJDBC(batch, taxCodeRefDocUploadParser, conn);
						if (taxCodeRefDocUploadParser.getBatchErrorLogs().size() > 0) {
							batch.setHeldFlag("1");
							batch.setErrorSevCode(BatchMaintenance.ErrorState.Error);
						}
						batch.setTotalRows(new Long(taxCodeRefDocUploadParser.getRows()));
					}
					
					// 4947
					
					batch.setBatchStatusCode(TaxCodeRulesBatch.IMPORTED);
					batch.setStatusUpdateTimestamp(new Date());
					batch.setStatusUpdateUserId(batch.getUpdateUserId());

					updateBatchUsingJDBC(batch, conn);
				} catch (Throwable e) {
					e.printStackTrace();
					logger.debug(e);
					backgroundError = ("Unable to save batch: " + e
							.getMessage());
				} finally {
					if (conn != null)
						try {
							conn.close();
						} catch (SQLException ignore) {

						}
				}

				// force completion
				state = AddState.COMPLETE;
				logger.debug("Background complete");
			}
		});
		background.start();
		logger.debug("Background started");
	}
	
	protected BatchMaintenance importTaxCodeRuleUpdatesUsingJDBC(
			BatchMaintenance batchMaintenance, TaxCodeRulesUploadParser parser,
			Connection conn) throws Exception {
		
		//Retrieve error codes
		Map<String,String> result = UpdateFilesBackingBean.getErrorListCodes(conn);
		parser.setErrorListCodes(result);

		Iterator<BCPTaxCodeRule> truIter = parser.iterator();
		
		if (outAID != null) {
			writeLogAID("   ... importTaxCodeRuleUpdatesUsingJDBC");
		}

		String sql = null;
		String sqlText = null;
		PreparedStatement prep = null;
		PreparedStatement prepText = null;
		Statement stat = null;
		
		if (outAID != null) {
			writeLogAID("   ... before loop");
		}

		int i = 1;
		while (truIter.hasNext()) {
			BCPTaxCodeRule tr = truIter.next();

			// 4947
			if (tr != null && tr.getLine() != null) {
				BCPTaxCodeRuleText txt = new BCPTaxCodeRuleText(
						batchMaintenance.getBatchId(), tr.getLine(),
						tr.getText());

				if (prep == null) {
					sql = tr.getRawInsertStatement(); 
					prep = conn.prepareStatement(sql);
					sqlText = txt.getRawInsertStatement();
					prepText = conn.prepareStatement(sqlText);
					stat = conn.createStatement();
				}

				tr.setBcpTaxcodeRuleId(getNextBcpTaxCodeId(TAXCODE_RULE_SEQ_NAME, stat));
				tr.setBatchId(batchMaintenance.getbatchId());

				// populate data to Prepared Statement
				tr.populatePreparedStatement(conn, prep);
				prep.addBatch();

				txt.populatePreparedStatement(conn, prepText);
				prepText.addBatch();

				if ((i++ % 50) == 0) {
					logger.debug("Begin Flush: " + i);
					
					if (outAID != null) {
						writeLogAID("   ... Begin Flush: record " + (i - 1));
					}
					
					prep.executeBatch();
					prepText.executeBatch();
					conn.commit();
					prep.clearBatch();
					prepText.clearBatch();
					logger.debug("End Flush");
					
					if (outAID != null) {
						writeLogAID("   ... End Flush");
					}
					
					parser.setLinesWritten(i);
				}
				tr = null;
				txt = null;
			}
		}

		if (prep != null) {
			if (outAID != null) {
				writeLogAID("   ... Begin Final Flush: record " + (i - 1));
			}
			
			prep.executeBatch();
			prepText.executeBatch();
			conn.commit();
			prep.clearBatch();
			prepText.clearBatch();
			
			if (outAID != null) {
				writeLogAID("   ... End Final Flush");
			}

			try {
				prep.close();
			} catch (Exception ex) {
			}
			try {
				prepText.close();
			} catch (Exception ex) {
			}
		}
		
		if (outAID != null) {
			writeLogAID("   ### Total Rows Imported: " + (i - 1));
		}

		// perform the checksum and generate any more ErrorLogs
		parser.setCacheManager(cacheManager);
		parser.checksum();
		
		flushErrorLog(conn, batchMaintenance);
		
		if (outAID != null) {
			writeLogAID("   ### Total Error Count: "
					+ parser.getBatchErrorLogs().size());
		}

		if (stat != null) {
			try {
				stat.close();
			} catch (Exception ex) {
			}
		}

		parser.setLinesWritten(i);

		return batchMaintenance;
	}

	protected BatchMaintenance importTaxCodeRefDocUpdatesUsingJDBC(
			BatchMaintenance batchMaintenance, TaxCodeRefDocUploadParser parser,
			Connection conn) throws Exception {
		
		//Retrieve error codes
		Map<String,String> result = UpdateFilesBackingBean.getErrorListCodes(conn);
		parser.setErrorListCodes(result);
		
	    Iterator<BCPTaxCodeRefDoc> truIter = parser.iterator();
		
		if (outAID != null) {
			writeLogAID("   ... importTaxCodeRefDocUpdatesUsingJDBC");
		}

		String sql = null;
		String sqlText = null;
		PreparedStatement prep = null;
		PreparedStatement prepText = null;
		Statement stat = null;
		
		if (outAID != null) {
			writeLogAID("   ... before loop");
		}

		int i = 1;
		while (truIter.hasNext()) {
			BCPTaxCodeRefDoc tr = truIter.next();

			// 4947
			if (tr != null && tr.getLine() != null) {
				BCPTaxCodeRefDocText txt = new BCPTaxCodeRefDocText(
						batchMaintenance.getBatchId(), tr.getLine(),
						tr.getText());

				if (prep == null) {
					sql = tr.getRawInsertStatement(); 
					prep = conn.prepareStatement(sql);
					sqlText = txt.getRawInsertStatement();
					prepText = conn.prepareStatement(sqlText);
					stat = conn.createStatement();
				}

				tr.setBcpTaxcodeRefdocId(getNextBcpTaxCodeId(TAXCODE_REFDOC_SEQ_NAME, stat));
				tr.setBatchId(batchMaintenance.getbatchId());

				// populate data to Prepared Statement
				tr.populatePreparedStatement(conn, prep);
				prep.addBatch();

				txt.populatePreparedStatement(conn, prepText);
				prepText.addBatch();

				if ((i++ % 50) == 0) {
					logger.debug("Begin Flush: " + i);
					
					if (outAID != null) {
						writeLogAID("   ... Begin Flush: record " + (i - 1));
					}
					
					prep.executeBatch();
					prepText.executeBatch();
					conn.commit();
					prep.clearBatch();
					prepText.clearBatch();
					logger.debug("End Flush");
					
					if (outAID != null) {
						writeLogAID("   ... End Flush");
					}
					
					parser.setLinesWritten(i);
				}
				tr = null;
				txt = null;
			}
		}

		if (prep != null) {
			if (outAID != null) {
				writeLogAID("   ... Begin Final Flush: record " + (i - 1));
			}
			
			prep.executeBatch();
			prepText.executeBatch();
			conn.commit();
			prep.clearBatch();
			prepText.clearBatch();
			
			if (outAID != null) {
				writeLogAID("   ... End Final Flush");
			}

			try {
				prep.close();
			} catch (Exception ex) {
			}
			try {
				prepText.close();
			} catch (Exception ex) {
			}
		}
		
		if (outAID != null) {
			writeLogAID("   ### Total Rows Imported: " + (i - 1));
		}

		// perform the checksum and generate any more ErrorLogs
		parser.setCacheManager(cacheManager);
		parser.checksum();
		
		flushErrorLog(conn, batchMaintenance);
		
		if (outAID != null) {
			writeLogAID("   ### Total Error Count: "
					+ parser.getBatchErrorLogs().size());
		}

		if (stat != null) {
			try {
				stat.close();
			} catch (Exception ex) {
			}
		}

		parser.setLinesWritten(i);

		return batchMaintenance;
	}
	
	
	void flushErrorLog(Connection conn, BatchMaintenance batchMaintenance) throws Exception {
		String sql = null;
        PreparedStatement prep = null;
        Statement statErrorNext = null;

		int ie = 1;
		for (BatchErrorLog error : TAXCODE_FILETYPE_CODE.equals(fileTypeCode) ? taxCodeRulesUploadParser.getBatchErrorLogs() : taxCodeRefDocUploadParser.getBatchErrorLogs()) {
			if (prep == null) {
				sql = error.getRawInsertStatement();
				prep = conn.prepareStatement(sql);
				statErrorNext = conn.createStatement();
			}
			
			if (error.getErrorDefCode().equalsIgnoreCase("IP12")){
				if(TAXCODE_FILETYPE_CODE.equals(fileTypeCode)) {
					taxCodeRulesUploadParser.setAbort(true);
				}
				else {
					taxCodeRefDocUploadParser.setAbort(true);
				}
				
			}

			error.setBatchErrorLogId(getNextBatchErrorLogId(statErrorNext));
			error.setProcessType(TaxCodeRulesBatch.IMPORT);
			error.setProcessTimestamp(batchMaintenance.getEntryTimestamp());
			error.setBatchId(batchMaintenance.getBatchId());
			error.populatePreparedStatement(conn, prep);
			prep.addBatch();

			if ((ie++ % 50) == 0) {
				prep.executeBatch();
				conn.commit();
				prep.clearBatch();
			}
		}

		if (prep != null) {
			prep.executeBatch();
			conn.commit();
			prep.clearBatch();

			try {
				prep.close();
			} catch (Exception ex) {
			}
		}
		
		if (statErrorNext != null) {
			try {
				statErrorNext.close();
			} catch (Exception ex) {
			}
		}
		
    }

	private long getNextBcpTaxCodeId(String seqname, Statement stat) {
		long nextId = -1;
		try {
			String sql = DatabaseUtil.getSequenceQuery(
					DatabaseUtil.getDatabaseProductName(stat),
					seqname, "NEXTID");

			ResultSet rs = stat.executeQuery(sql);
			if (rs.next()) {
				nextId = rs.getLong("NEXTID");
			}
			rs.close();
		} catch (Exception e) {
			logger.error("############ getNextBcpTaxCodeId() failed: "
					+ e.toString());
		}

		return nextId;
	}

	private long getNextBatchErrorLogId(Statement statErrorNext) {
		long nextId = -1;
		try {
			String sql = DatabaseUtil.getSequenceQuery(
					DatabaseUtil.getDatabaseProductName(statErrorNext),
					"sq_tb_batch_error_log_id", "NEXTID");

			// Statement stat = conn.createStatement();
			ResultSet rs = statErrorNext.executeQuery(sql);
			if (rs.next()) {
				nextId = rs.getLong("NEXTID");
			}
			rs.close();
		} catch (Exception e) {
			logger.error("############ getNextBatchErrorLogId() failed: "
					+ e.toString());
		} finally {
		}

		return nextId;
	}

	protected void updateBatchUsingJDBC(BatchMaintenance batchMaintenance,
			Connection conn) throws Exception {
		String sql = batchMaintenance.getRawUpdateStatement();
		PreparedStatement prep = conn.prepareStatement(sql);
		batchMaintenance.populateUpdatePreparedStatement(conn, prep);
		prep.addBatch();

		prep.executeBatch();
		conn.commit();
		prep.clearBatch();

		return;
	}

	public DbPropertiesBean getDbPropertiesBean() {
		return dbPropertiesBean;
	}

	public void setDbPropertiesBean(DbPropertiesBean dbPropertiesBean) {
		this.dbPropertiesBean = dbPropertiesBean;
	}

	public void selectedRowChanged(ActionEvent e)
			throws AbortProcessingException {
		UIComponent uiComponent = e.getComponent().getParent();
		if (!(uiComponent instanceof UIDataAdaptor)) {
			logger.info("Invalid class to event listener: "
					+ uiComponent.getClass().getName());
		}
		UIDataAdaptor table = (UIDataAdaptor) uiComponent;
		if (table != null) {
			this.selectedBatch = new TaxCodeRulesBatch(
					(BatchMaintenance) table.getRowData());
			this.selectedBatchIndex = table.getRowIndex();

			BatchMetadata batchMetadata = batchMaintenanceService.findBatchMetadataById(selectedBatch.getBatchTypeCode());
			batchMetadataList = new BatchMetadataList(batchMetadata, (BatchMaintenance) table.getRowData());

		}
	}
	
	public String submitProcessAction() {
	  	for (TaxCodeRulesBatch batch : selectedBatches) {
	  		if (batch.getError() == null && selectedBatches.size()==1) {
	  			BatchMaintenance bm = batchMaintenanceDataModel.getById(batch.getBatchId());
	  			bm.setBatchStatusCode(TaxCodeRulesBatch.PROCESS);
	  			bm.setSchedStartTimestamp((Date)batchStartTime.getValue());
	  			batchMaintenanceService.update(bm);
	  			batchMaintenanceDataModel.refreshData(false);
	  		}
	  		  		
	  		if( selectedBatches.size()>1 ){
	  			if(batch.getError()!=null){
	  				selectedBatches.remove(batch.getBatchId());
	  				logger.info("!!!!!Batch id Ommited !!!!!!! "+batch.getBatchId());
	  				continue;
	  			}
	  			BatchMaintenance bm = batchMaintenanceDataModel.getById(batch.getBatchId());
	  			logger.info("Batch id Updated "+batch.getBatchId());
	  			bm.setBatchStatusCode(TaxCodeRulesBatch.PROCESS);
	  			bm.setSchedStartTimestamp((Date)batchStartTime.getValue());
	  			batchMaintenanceService.update(bm);
	  		}
	  	}
	  	batchMaintenanceDataModel.refreshData(false);
	  	selectedBatchIndex = -1;
		return "tax_code_rules";
	  }

	public HtmlCalendar getBatchStartTime() {
		batchStartTime.resetValue();
	    batchStartTime.setValue(filePreferenceBean.getBatchPreferenceDTO().getDefaultBatchStartTime());
		return batchStartTime;
	}

	public void setBatchStartTime(HtmlCalendar batchStartTime) {
		this.batchStartTime = batchStartTime;
	}

	public List<TaxCodeRulesBatch> getSelectedBatches() {
		return selectedBatches;
	}

	public BatchMetadataList getBatchMetadataList() {
		return batchMetadataList;
	}

	public void setBatchMetadataList(BatchMetadataList batchMetadataList) {
		this.batchMetadataList = batchMetadataList;
	}

	public String updateUserFieldsAction() {
		return "tax_code_rules_user_fields_update";
	}

	public String updateUserFields() {
		batchMaintenanceService.update(batchMetadataList.update());
		return "tax_code_rules_overview";
	}

	public String cancelUpdateUserFields() {
		return "tax_code_rules_overview";
	}

	public boolean isNavigatedFlag() {
		return true;
	}
	public void setNavigatedFlag(boolean navigatedFlag) {}
	
	public void sortAction(ActionEvent e) {
		batchMaintenanceDataModel.toggleSortOrder(e.getComponent().getParent().getId());
	}

	public static void main(String[] args) {
		System.out.println("############ main for Automatic Import of Data");
	
		String name = "TCR_UPDATE_2019_02_v6_header+3";
		String fileName = name + ".txt";
		String filePath = "C:\\File Updates - Test\\" + name + ".txt";
		String folderPath = "C:\\File Updates - Test\\";
		String logFile = "C:\\File Updates - Test\\" + name + ".log";
	
		BufferedWriter out = null;
		try {
			out = new BufferedWriter(new FileWriter(logFile));
		} catch (Exception e) {
		}
	
		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
		} catch (ClassNotFoundException e) {
			System.out
					.println("xxxxxxxxxxxxxx Can't load class: oracle.jdbc.driver.OracleDriver");
			return;
		}
	
		Connection conn = null;
		try {
			// Standalone connection
			conn = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe", "testdb4_login","STSCORP");
			conn.setAutoCommit(false);
	
			TaxCodeRulesBackingBean importTaxCodeBean = new TaxCodeRulesBackingBean();
			if (importTaxCodeBean.initVerification(conn, out)) {
				importTaxCodeBean.addActionAID(fileName, filePath, folderPath);
			} else {
				System.out.println("Error: Stop importing tax rates, " + filePath);
			}
	
		} catch (Throwable e) {
			e.printStackTrace();
		} finally {
			if (conn != null)
				try {
					conn.close();
				} catch (SQLException ignore) {
				}
		}
	
		try {
			out.close();
		} catch (Exception e) {
		}
	}
	
	public User getCurrentUser() {
		return loginbean.getUser();
   }

  	public void retrieveBatchMaintenance() { 
  		BatchMaintenance batchMaintenance = new BatchMaintenance();
		batchMaintenance.setBatchTypeCode(exampleBatchMaintenance.getBatchTypeCode());
  		this.batchMaintenanceDataModel =  batchMaintenanceBean.prepareBatchMaintenance(batchMaintenance);
  	}
  	
  	public String resetBatchFields(){
  		batchMaintenanceBean.setResetBatchAccordion(false);
  		batchMaintenanceBean.resetOtherBatchFields();
  		return null;
  	}
  	
  	public String navigateAction() {
  		init();
  		return "tax_code_rules";
  	}
  	
  	public void setBatchErrorLog(BatchMaintenance batchMaintenance,Connection conn) {
  		
  		try {
  			String sql = null;
  	  		PreparedStatement prep = null;
  	  		Statement statErrorNext = null;
  	  		
  			BatchErrorLog errorLog = new BatchErrorLog();
  			
  			sql = errorLog.getRawInsertStatement();
  			prep = conn.prepareStatement(sql);
  			statErrorNext = conn.createStatement();

  			
  			errorLog.setBatchErrorLogId(getNextBatchErrorLogId(statErrorNext));
  			errorLog.setBatchId(batchMaintenance.getBatchId());
  			errorLog.setProcessType(TaxCodeRulesBatch.IMPORT);
  			errorLog.setProcessTimestamp(batchMaintenance.getEntryTimestamp());
  			errorLog.setRowNo(new Long("1")); 
  			errorLog.setErrorDefCode("IP12");
  			errorLog.setImportHeaderColumn("Invalid Header - Missing Key Components"); 
  			//String description = cacheManager.getErrorListCodes().get("IP12").getDescription();
  			errorLog.populatePreparedStatement(conn, prep);
  			prep.addBatch();
  			
  			prep.executeBatch();
  			conn.commit();
  			prep.clearBatch();

  		try {
  			prep.close();
  		} catch (Exception ex) {
  		}

  		if (statErrorNext != null) {
  			try {
  				statErrorNext.close();
  			} catch (Exception ex) {
  			}
  		}
  		
  	    }catch(Exception e) {
  			writeLogAID("Error happened in the method setBatchErrorLog...." ); 
  	    }
  	}
}
