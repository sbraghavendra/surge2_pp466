package com.ncsts.view.bean;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.TimeZone;

import javax.faces.application.Application;
import javax.faces.application.ViewHandler;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.ConvertUtils;
import org.apache.commons.beanutils.converters.BigDecimalConverter;
import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;

import com.ncsts.common.CacheManager;
import com.ncsts.common.LogMemory;
import com.ncsts.common.LoggerFactory;
import com.ncsts.domain.Auditable;
import com.ncsts.domain.EntityItem;
import com.ncsts.domain.License;
import com.ncsts.domain.ListCodes;
import com.ncsts.domain.Menu;
import com.ncsts.domain.Option;
import com.ncsts.domain.OptionCodePK;
import com.ncsts.domain.Role;
import com.ncsts.domain.RoleMenu;
import com.ncsts.domain.User;
import com.ncsts.domain.UserEntity;
import com.ncsts.domain.UserEntityPK;
import com.ncsts.domain.UserMenuEx;
import com.ncsts.dto.UserDTO;
import com.ncsts.dto.UserRolesDTO;
import com.ncsts.dto.UserSecurityDTO;
import com.ncsts.management.UserSecurityBean;
import com.ncsts.security.ValidMenuOptions;
import com.ncsts.services.EntityItemService;
import com.ncsts.services.MenuService;
import com.ncsts.services.OptionService;
import com.ncsts.services.RoleService;
import com.ncsts.services.RoleUserMenuService;
import com.ncsts.services.UserEntityService;
import com.ncsts.services.UserMenuExService;
import com.ncsts.services.UserService;
import com.ncsts.view.util.ConfigSetting;
import com.ncsts.view.util.DesEncrypter;
import com.opensymphony.oscache.base.NeedsRefreshException;
import com.opensymphony.oscache.web.filter.ExpiresRefreshPolicy;
import org.springframework.beans.factory.DisposableBean;

public class loginBean {


	private Logger logger = LoggerFactory.getInstance().getLogger(loginBean.class);	

	public static final String DEFAULT_USER_NAME = "Enter User Name";
    public static Properties helpMap = null;
    
    public static final String BILLING_TAX_TYPE = "Billing/Sales";
    public static final String PURCHASING_TAX_TYPE = "Purchasing/Use";
    private String selectedTaxType = "";

    private String username;
    private String password;
    private String message;
    private String selectedDb;
    private String noUserMessage = null;
    private TimeZone timeZone;
    private boolean validLogin = false;
    private boolean expiredLogin = false;

    private UserService userService;
    private OptionService optionService;
    private MenuService menuService;
    
	private UserEntityService userEntityService;
	private RoleUserMenuService roleUserMenuService;
	private UserMenuExService userMenuExService;
	private RoleService roleService;
	private EntityItemService entityItemService;
	
    private UserDTO userDTO;
    private Role role;
    private EntityItem entityItem;

    private boolean showAdminMenu = false;
    private boolean showAdminSitusDriversMenu = false;
    private boolean showAdminSitusMatrixMenu = false;
    
    private boolean showFileMenu = true;
    private boolean showFilePreferencesMenu = false;
	private boolean showFileEntityMenu = false;

    private boolean showTransactionsMenu = true;
	private boolean showTransactionsProcessMenu = false;
	private boolean showTransactionsStatisticsMenu = false;
	private boolean showViewSaleTransactionsMenu = false;

	private boolean showDataUtilityMenu = true;
	private boolean showDataUtilityBatchMenu = false;
	private boolean showDataUtilityImportMapMenu = false;
	private boolean showDataUtilityImportMapSalesMenu = false;
	private boolean showMaintenanceTaxabilityMenubill=false;
	private boolean showDataUtilityGLExtractMenu = false;
	private boolean showDataUtilityGLMarkTransMenu = false;
	private boolean showDataUtilityTaxRateUpdateMenu = false;

	private boolean showMaintenanceMenu = true;
	private boolean showMaintenanceTaxabilityMenu = false;
	private boolean showMaintenanceLocationMenu = false;
	private boolean showMaintenanceAllocationMenu = false;
	private boolean showMaintenanceTaxAllocationMenu = false;
	private boolean showMaintenanceTaxabilityCodeMenu = false;
	private boolean showMaintenanceTaxCodeRulesMenu = false;
	private boolean showMaintenanceSitusRulesMenu = false;
	private boolean showMaintenanceCustLocnMenu = false;
	private boolean showMaintenanceTaxRateMenu = false;
	private boolean showMaintenanceDriverReferenceMenu = false;
	private boolean showTaxHolidayMaintenanceMenu = false;
	private boolean showEntitiesMaintenanceMenu = false;
	private boolean showVendorsMaintenanceMenu = false;
	private boolean showNexusDefMaintenanceMenu = false;
	private boolean showMaintenanceSitusMatrixMenu = false;
	private boolean showCustomerLocationMainMenu = false;
	private boolean showExemptionCertificateMainMenu = false;
	private boolean showProcessRulesMaintenanceMenu = false;
	private boolean showProcessTablesMaintenanceMenu = false;

	private boolean showSecurityMenu = true;
	private boolean showSecurityPasswordMenu = false;
	private boolean showSecurityModulesMenu = false;
	private boolean showSecurityEntityStructureMenu = false;

	private boolean showUtilitiesMenu = true;
	private boolean showUtilitiesRunMCP = false;
	private boolean showFixBrokenJob = false;
	private boolean showUtilitiesResetDataTables = false;
	private boolean showUtilitiesBuildIndices = false;
	private boolean showUtilitiesReweightMatrices = false;
	private boolean showUtilitiesResetSequences = false;
	private boolean showUtilitiesRebuildDriverRefs = false;
	private boolean showUtilitiesTruncateBCPTables = false;
	private boolean showUtilitiesApiTest = false;
	private boolean showTransDetailList = false;
	private boolean showRunBatchProcess = false;
	private boolean showUtilitiesBillingTestTool = false;
	private boolean showUtilitiesPurchaseTestTool = false;
	private boolean showAidFileManager = false;

	private boolean showSetupMenu = true;
	private boolean showSetupDatabaseMenu = false;
	private boolean showSetupDataDefinitionsMenu = false;
	private boolean showSetupMatrixDriversMenu = false;
	private boolean showSetupSitusDriversMenu = false;
	private boolean showSetupImportDefinitionsMenu = false;
	private boolean showSetupListsMenu = false;
	private boolean showSetupDatabaseStatLinesMenu = false;
	private boolean showSetupSupportingCodesMenu = false;
	private boolean showFiscalYearMenu=false;
	
	private boolean showInactivedMenuMark = false;

	private CacheManager cacheManager;
	private ConfigSetting databaseConfig;
	private ConfigSetting userConfig;
	@SuppressWarnings("unused")
	private RoutingDataSource dataSource;
    private String helpBaseUrl;
    private String helpProps;
    private String buildProps;
    @SuppressWarnings("unused")
	private String helpFile;

    
    UserSecurityDTO userSecurityDTO = null;
    
    private UserSecurityBean userSecurityBean;
    private FilePreferenceBackingBean filePreferenceBackingBean;
    
    private boolean adminUserFlag = false;
    private boolean enableChangeFlag = false;
    private boolean enableCancelFlag = false;
    private boolean directToPinPointFlag = false;
    private boolean aidMenuEnabled = false;
    
    private String appStartupError = null;
    private License license;
    private Set<String> purchaseMenus;
    private Set<String> saleMenus;

	private PageViewHandler viewHandler;



    public void  resetAidMenuEnabled() {
    	Option option = optionService.findByPK(new OptionCodePK("AIDSETTINGPATH", "ADMIN","ADMIN"));
		String aidInstallation = option.getValue();	
		this.setAidMenuEnabled(isAidSettingFileExist(aidInstallation));
	}
     
    public boolean isAidSettingFileExist(String path){
		if(path!=null && path.length()>0){
			File aFile = new File(path);
			if(aFile.isDirectory()){
				if(new File(path + aFile.separator + "conf" + aFile.separator + "AIDSetting.xml").isFile()){
					return true;
				}
			}
		}
		
		return false;
	}
    
    public boolean  getAidMenuEnabled() {
		return aidMenuEnabled;
	}

	public void setAidMenuEnabled(boolean  aidMenuEnabled) {
		this.aidMenuEnabled = aidMenuEnabled;
	}
	
    public boolean isEnableCancelFlag() {
    	return enableCancelFlag;
    }

    public void setEnableCancelFlag(boolean enableCancelFlag) {
    	this.enableCancelFlag = enableCancelFlag;
    }
    
    public boolean isEnableChangeFlag() {
    	return enableChangeFlag;
    }

    public void setEnableChangeFlag(boolean enableChangeFlag) {
    	this.enableChangeFlag = enableChangeFlag;
    }

    public boolean isAdminUserFlag() {
    	return adminUserFlag;
    }

    public void setAdminUserFlag(boolean adminUserFlag) {
    	this.adminUserFlag = adminUserFlag;
    }
    
	public UserSecurityBean getUserSecurityBean() {
		return userSecurityBean;
	}

	public void setUserSecurityBean(UserSecurityBean userSecurityBean) {
		this.userSecurityBean= userSecurityBean;
	}
	
	public FilePreferenceBackingBean getFilePreferenceBackingBean() {
		return filePreferenceBackingBean;
	}

	public void setFilePreferenceBackingBean(FilePreferenceBackingBean filePreferenceBackingBean) {
		this.filePreferenceBackingBean= filePreferenceBackingBean;
	}
	
	public UserMenuExService getUserMenuExService(){
		return userMenuExService;
	}
	
	public void setUserMenuExService (UserMenuExService userMenuExService){
		this.userMenuExService = userMenuExService;
	}
	
	public RoleUserMenuService getRoleUserMenuService(){
		return roleUserMenuService;
	}
	
	public void setRoleUserMenuService (RoleUserMenuService roleUserMenuService){
		this.roleUserMenuService = roleUserMenuService;
	}
	
	public UserEntityService getUserEntityService(){
		return userEntityService;
	}
	
	public void setUserEntityService (UserEntityService userEntityService){
		this.userEntityService = userEntityService;
	}
	
	public RoleService getRoleService(){
		return roleService;
	}
	
	public void setRoleService (RoleService roleService){
		this.roleService = roleService;
	}
	
	public MenuService getMenuService() {
    	return menuService;
    }
    
    public void setMenuService(MenuService menuService) {
    	this.menuService = menuService;
    }

	public EntityItemService entityItemService(){
		return entityItemService;
	}
	
	public void setEntityItemService (EntityItemService entityItemService){
		this.entityItemService = entityItemService;
	}
	
	public boolean getShowAdminMenu(){
		return showAdminMenu;
	}
	
	public void setShowAdminMenu(boolean showAdminMenu){
		this.showAdminMenu = showAdminMenu;
	}

	public boolean getShowFileMenu (){
		return showFileMenu;
	}
	
	public void setShowFileMenu (boolean showFileMenu){
		this.showFileMenu = showFileMenu;
	}
	
	public boolean getShowFilePreferencesMenu (){
		return showFilePreferencesMenu;
	}
	
	public void setShowFilePreferencesMenu (boolean showFilePreferencesMenu){
		this.showFilePreferencesMenu = showFilePreferencesMenu;
	}
	
	public boolean getShowFileEntityMenu (){
		return showFileEntityMenu;
	}
	
	public void setShowFileEntityMenu (boolean showFileEntityMenu){
		this.showFileEntityMenu = showFileEntityMenu;
	}
    
	public boolean getShowTransactionsMenu (){
		return showTransactionsMenu;
	}
	
	public void setShowTransactionsMenu (boolean showTransactionsMenu){
		this.showTransactionsMenu = showTransactionsMenu;
	}
	
	public boolean getShowTransactionsProcessMenu (){
		return showTransactionsProcessMenu;
	}
	
	public void setShowTransactionsProcessMenu (boolean showTransactionsProcessMenu){
		this.showTransactionsProcessMenu = showTransactionsProcessMenu;
	}
	
	public boolean getShowTransactionsStatisticsMenu (){
		return showTransactionsStatisticsMenu;
	}
	
	public void setShowTransactionsStatisticsMenu (boolean showTransactionsStatisticsMenu){
		this.showTransactionsStatisticsMenu = showTransactionsStatisticsMenu;
	}
	
	public boolean getShowViewSaleTransactionsMenu (){
		return showViewSaleTransactionsMenu;
	}
	
	public void setShowViewSaleTransactionsMenu (boolean showViewSaleTransactionsMenu){
		this.showViewSaleTransactionsMenu = showViewSaleTransactionsMenu;
	}

	public boolean getShowDataUtilityMenu (){
		return showDataUtilityMenu;
	}
	
	public void setShowDataUtilityMenu (boolean showDataUtilityMenu){
		this.showDataUtilityMenu = showDataUtilityMenu;
	}
	
	public boolean getShowDataUtilityBatchMenu (){
		return showDataUtilityBatchMenu;
	}
	
	public void setShowDataUtilityBatchMenu (boolean showDataUtilityBatchMenu){
		this.showDataUtilityBatchMenu = showDataUtilityBatchMenu;
	}
	
	public boolean getShowDataUtilityImportMapMenu (){
		return showDataUtilityImportMapMenu;
	}
	
	public void setShowDataUtilityImportMapMenu (boolean showDataUtilityImportMapMenu){
		this.showDataUtilityImportMapMenu = showDataUtilityImportMapMenu;
	}
	
	public boolean getShowDataUtilityGLExtractMenu (){
		return showDataUtilityGLExtractMenu;
	}
	
	public void setShowDataUtilityGLExtractMenu (boolean showDataUtilityGLExtractMenu){
		this.showDataUtilityGLExtractMenu = showDataUtilityGLExtractMenu;
	}
	
	public boolean getShowDataUtilityGLMarkTransMenu (){
		return showDataUtilityGLMarkTransMenu;
	}
	
	public void setShowDataUtilityGLMarkTransMenu (boolean showDataUtilityGLMarkTransMenu){
		this.showDataUtilityGLMarkTransMenu = showDataUtilityGLMarkTransMenu;
	}
	
	public boolean getShowDataUtilityTaxRateUpdateMenu (){
		return showDataUtilityTaxRateUpdateMenu;
	}
	
	public void setShowDataUtilityTaxRateUpdateMenu (boolean showDataUtilityTaxRateUpdateMenu){
		this.showDataUtilityTaxRateUpdateMenu = showDataUtilityTaxRateUpdateMenu;
	}
	
	public boolean getShowMaintenanceMenu (){
		return showMaintenanceMenu;
	}
	
	public void setShowMaintenanceMenu (boolean showMaintenanceMenu){
		this.showMaintenanceMenu = showMaintenanceMenu;
	}
	
	public boolean getShowMaintenanceTaxabilityMenu (){
		return showMaintenanceTaxabilityMenu;
	}
	
	public void setShowMaintenanceTaxabilityMenu (boolean showMaintenanceTaxabilityMenu){
		this.showMaintenanceTaxabilityMenu = showMaintenanceTaxabilityMenu;
	}
	
	public boolean getShowMaintenanceLocationMenu (){
		return showMaintenanceLocationMenu;
	}
	
	public void setShowMaintenanceLocationMenu (boolean showMaintenanceLocationMenu){
		this.showMaintenanceLocationMenu = showMaintenanceLocationMenu;
	}
	
	public boolean getShowMaintenanceAllocationMenu (){
		return showMaintenanceAllocationMenu;
	}
	
	public void setShowMaintenanceAllocationMenu (boolean showMaintenanceAllocationMenu){
		this.showMaintenanceAllocationMenu = showMaintenanceAllocationMenu;
	}
	
	public boolean getShowMaintenanceTaxAllocationMenu() {
		return showMaintenanceTaxAllocationMenu;
	}

	public void setShowMaintenanceTaxAllocationMenu(
			boolean showMaintenanceTaxAllocationMenu) {
		this.showMaintenanceTaxAllocationMenu = showMaintenanceTaxAllocationMenu;
	}
	
	public boolean getShowAidFileManager() {
		return showAidFileManager;
	}

	public void setShowAidFileManager(boolean showAidFileManager) {
		this.showAidFileManager = showAidFileManager;
	}

	public boolean getShowMaintenanceTaxabilityCodeMenu (){
		return showMaintenanceTaxabilityCodeMenu;
	}
	
	public void setShowMaintenanceTaxabilityCodeMenu (boolean showMaintenanceTaxabilityCodeMenu){
		this.showMaintenanceTaxabilityCodeMenu = showMaintenanceTaxabilityCodeMenu;
	}
		
	public boolean getShowMaintenanceTaxCodeRulesMenu() {
		return showMaintenanceTaxCodeRulesMenu;
	}

	public void setShowMaintenanceTaxCodeRulesMenu(
			boolean showMaintenanceTaxCodeRulesMenu) {
		this.showMaintenanceTaxCodeRulesMenu = showMaintenanceTaxCodeRulesMenu;
	}
	public boolean getShowMaintenanceSitusRulesMenu() {
		return showMaintenanceSitusRulesMenu;
	}

	public void setShowMaintenanceSitusRulesMenu(
			boolean showMaintenanceSitusRulesMenu) {
		this.showMaintenanceSitusRulesMenu = showMaintenanceSitusRulesMenu;
	}
	public boolean getShowMaintenanceCustLocnMenu() {
		return showMaintenanceCustLocnMenu;
	}

	public void setShowMaintenanceCustLocnMenu(
			boolean showMaintenanceCustLocnMenu) {
		this.showMaintenanceCustLocnMenu = showMaintenanceCustLocnMenu;
	}
	public boolean getShowMaintenanceTaxRateMenu (){
		return showMaintenanceTaxRateMenu;
	}
	
	public void setShowMaintenanceTaxRateMenu (boolean showMaintenanceTaxRateMenu){
		this.showMaintenanceTaxRateMenu = showMaintenanceTaxRateMenu;
	}
	
	public boolean getShowMaintenanceDriverReferenceMenu (){
		return showMaintenanceDriverReferenceMenu;
	}
	
	public void setShowMaintenanceDriverReferenceMenu (boolean showMaintenanceDriverReferenceMenu){
		this.showMaintenanceDriverReferenceMenu = showMaintenanceDriverReferenceMenu;
	}
	
	public boolean getShowTaxHolidayMaintenanceMenu (){
		return showTaxHolidayMaintenanceMenu;
	}
	
	public void setShowTaxHolidayMaintenanceMenu (boolean showTaxHolidayMaintenanceMenu){
		this.showTaxHolidayMaintenanceMenu = showTaxHolidayMaintenanceMenu;
	}
	
	public boolean getShowCustomerLocationMainMenu (){
		return showCustomerLocationMainMenu;
	}
	
	public void setShowCustomerLocationMainMenu (boolean showCustomerLocationMainMenu){
		this.showCustomerLocationMainMenu = showCustomerLocationMainMenu;
	}
	
	public boolean getShowProcessRulesMaintenanceMenu(){
		return showProcessRulesMaintenanceMenu;
	}
	
	public void setShowProcessRulesMaintenanceMenu(boolean showProcessRulesMaintenanceMenu){
		this.showProcessRulesMaintenanceMenu = showProcessRulesMaintenanceMenu;
	}

	public boolean getShowProcessTablesMaintenanceMenu(){
		return showProcessTablesMaintenanceMenu;
	}
	
	public void setShowProcessTablesMaintenanceMenu(boolean showProcessTablesMaintenanceMenu){
		this.showProcessTablesMaintenanceMenu = showProcessTablesMaintenanceMenu;
	}
	
	public boolean getShowExemptionCertificateMainMenu(){
		return showExemptionCertificateMainMenu;
	}
	
	public void setShowExemptionCertificateMainMenu(boolean showExemptionCertificateMainMenu){
		this.showExemptionCertificateMainMenu = showExemptionCertificateMainMenu;
	}
	
	public boolean getShowEntitiesMaintenanceMenu (){
		return showEntitiesMaintenanceMenu;
	}
	
	public void setShowEntitiesMaintenanceMenu (boolean showEntitiesMaintenanceMenu){
		this.showEntitiesMaintenanceMenu = showEntitiesMaintenanceMenu;
	}
	
	public boolean getShowVendorsMaintenanceMenu(){
		return showVendorsMaintenanceMenu;
	}
	
	public void setShowVendorsMaintenanceMenu(boolean showVendorsMaintenanceMenu){
		this.showVendorsMaintenanceMenu = showVendorsMaintenanceMenu;
	}
	
	public boolean getShowSecurityMenu (){
		return showSecurityMenu;
	}
	
	public void setShowshowSecurityMenu (boolean showSecurityMenu){
		this.showSecurityMenu = showSecurityMenu;
	}
	
	public boolean getShowSecurityPasswordMenu (){
		return showSecurityPasswordMenu;
	}
	
	public void setShowSecurityPasswordMenu (boolean showSecurityPasswordMenu){
		this.showSecurityPasswordMenu = showSecurityPasswordMenu;
	}
	
	public boolean getShowSecurityModulesMenu (){
		return showSecurityModulesMenu;
	}
	
	public void setShowSecurityModulesMenu (boolean showSecurityModulesMenu){
		this.showSecurityModulesMenu = showSecurityModulesMenu;
	}
	
	public boolean getShowSecurityEntityStructureMenu (){
		return showSecurityEntityStructureMenu;
	}
	
	public void setShowSecurityEntityStructureMenu (boolean showSecurityEntityStructureMenu){
		this.showSecurityEntityStructureMenu = showSecurityEntityStructureMenu;
	}
	
	public boolean getShowUtilitiesMenu (){
		return showUtilitiesMenu;
	}
	
	public void setShowUtilitiesMenu (boolean showUtilitiesMenu){
		this.showUtilitiesMenu = showUtilitiesMenu;
	}
	
	public boolean getShowUtilitiesRunMCP (){
		return showUtilitiesRunMCP;
	}
	
	public void setShowUtilitiesRunMCP (boolean showUtilitiesRunMCP){
		this.showUtilitiesRunMCP = showUtilitiesRunMCP;
	}
	
	public boolean getShowFixBrokenJob (){
		return showFixBrokenJob;
	}
	
	public void setShowFixBrokenJob (boolean showFixBrokenJob){
		this.showFixBrokenJob = showFixBrokenJob;
	}
	
	public boolean getShowUtilitiesResetDataTables (){
		return showUtilitiesResetDataTables;
	}
	
	public void setShowUtilitiesResetDataTables (boolean showUtilitiesResetDataTables){
		this.showUtilitiesResetDataTables = showUtilitiesResetDataTables;
	}
	
	public boolean getShowUtilitiesBuildIndices (){
		return showUtilitiesBuildIndices;
	}
	
	public void setShowUtilitiesBuildIndices (boolean showUtilitiesBuildIndices){
		this.showUtilitiesBuildIndices = showUtilitiesBuildIndices;
	}
	
	public boolean getShowUtilitiesReweightMatrices (){
		return showUtilitiesReweightMatrices;
	}
	
	public void setShowUtilitiesReweightMatrices (boolean showUtilitiesReweightMatrices){
		this.showUtilitiesReweightMatrices = showUtilitiesReweightMatrices;
	}
	
	public boolean getShowUtilitiesResetSequences (){
		return showUtilitiesResetSequences;
	}
	
	public void setShowUtilitiesResetSequences (boolean showUtilitiesResetSequences){
		this.showUtilitiesResetSequences = showUtilitiesResetSequences;
	}
	
	public boolean getShowUtilitiesRebuildDriverRefs (){
		return showUtilitiesRebuildDriverRefs;
	}
	
	public void setShowUtilitiesRebuildDriverRefs (boolean showUtilitiesRebuildDriverRefs){
		this.showUtilitiesRebuildDriverRefs = showUtilitiesRebuildDriverRefs;
	}
	
	public boolean getShowUtilitiesTruncateBCPTables (){
		return showUtilitiesTruncateBCPTables;
	}
	
	public void setShowUtilitiesTruncateBCPTables (boolean showUtilitiesTruncateBCPTables){
		this.showUtilitiesTruncateBCPTables = showUtilitiesTruncateBCPTables;
	}
	
	public boolean getShowUtilitiesApiTest (){
		return showUtilitiesApiTest;
	}
	
	public void setShowUtilitiesApiTest (boolean showUtilitiesApiTest){
		this.showUtilitiesApiTest = showUtilitiesApiTest;
	}
	
	public boolean getShowUtilitiesBillingTestTool (){
		return showUtilitiesBillingTestTool;
	}
	
	public void setShowUtilitiesBillingTestTool(boolean showUtilitiesBillingTestTool){
		this.showUtilitiesBillingTestTool = showUtilitiesBillingTestTool;
	}
	
	public boolean getShowUtilitiesPurchaseTestTool (){
		return showUtilitiesPurchaseTestTool;
	}
	
	public void setShowUtilitiesPurchaseTestTool(boolean showUtilitiesPurchaseTestTool){
		this.showUtilitiesPurchaseTestTool = showUtilitiesPurchaseTestTool;
	}
	
	public boolean getShowTransDetailList(){
		return showTransDetailList;
	}
	
	public void setShowTransDetailList(boolean showTransDetailList){
		this.showTransDetailList = showTransDetailList;
	}

	public boolean getShowSetupMenu (){
		return showSetupMenu;
	}
	
	public void setShowSetupMenu (boolean showSetupMenu){
		this.showSetupMenu = showSetupMenu;
	}
	
	public boolean getShowSetupDatabaseMenu (){
		return showSetupDatabaseMenu;
	}
	
	public void setShowSetupDatabaseMenu (boolean showSetupDatabaseMenu){
		this.showSetupDatabaseMenu = showSetupDatabaseMenu;
	}
	
	public boolean getShowSetupDataDefinitionsMenu (){
		return showSetupDataDefinitionsMenu;
	}
	
	public void setShowSetupDataDefinitionsMenu (boolean showSetupDataDefinitionsMenu){
		this.showSetupDataDefinitionsMenu = showSetupDataDefinitionsMenu;
	}
	
	public boolean getShowSetupMatrixDriversMenu (){
		return showSetupMatrixDriversMenu;
	}
	
	public void setShowSetupMatrixDriversMenu (boolean showSetupMatrixDriversMenu){
		this.showSetupMatrixDriversMenu = showSetupMatrixDriversMenu;
	}

	public boolean getShowSetupSitusDriversMenu(){
		return showSetupSitusDriversMenu;
	}
	
	public void setShowSetupSitusDriversMenu(boolean showSetupSitusDriversMenu){
		this.showSetupSitusDriversMenu = showSetupSitusDriversMenu;
	}
	
	public boolean getShowMaintenanceSitusMatrixMenu(){
		return showMaintenanceSitusMatrixMenu;
	}
	
	public void setShowMaintenanceSitusMatrixMenu(boolean showMaintenanceSitusMatrixMenu){
		this.showMaintenanceSitusMatrixMenu = showMaintenanceSitusMatrixMenu;
	}
	
	public boolean getShowSetupImportDefinitionsMenu (){
		return showSetupImportDefinitionsMenu;
	}
	
	public void setShowSetupImportDefinitionsMenu (boolean showSetupImportDefinitionsMenu){
		this.showSetupImportDefinitionsMenu = showSetupImportDefinitionsMenu;
	}
	
	public boolean getShowSetupListsMenu (){
		return showSetupListsMenu;
	}
	
	public void setShowSetupListsMenu (boolean showSetupListsMenu){
		this.showSetupListsMenu = showSetupListsMenu;
	}
	
	public boolean getShowSetupDatabaseStatLinesMenu (){
		return showSetupDatabaseStatLinesMenu;
	}
	
	public void setShowSetupDatabaseStatLinesMenu (boolean showSetupDatabaseStatLinesMenu){
		this.showSetupDatabaseStatLinesMenu = showSetupDatabaseStatLinesMenu;
	}

	public boolean getShowSetupSupportingCodesMenu() {
		return showSetupSupportingCodesMenu;
	}

	public void setShowSetupSupportingCodesMenu(boolean showSetupSupportingCodesMenu) {
		this.showSetupSupportingCodesMenu = showSetupSupportingCodesMenu;
	}

	public boolean getShowInactivedMenuMark(){
		return showInactivedMenuMark;
	}
	
	public void setShowInactivedMenuMark(boolean showInactivedMenuMark){
		this.showInactivedMenuMark = showInactivedMenuMark;
	}
	
	public CacheManager getCacheManager() {
		return this.cacheManager;
	}

	public void setCacheManager(CacheManager cacheManager) {
		this.cacheManager = cacheManager;
	}
	
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

		public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }  
    
    public UserService getUserService() {
    	return userService;
    }
    
    public void setUserService(UserService userService) {
    	this.userService = userService;
    }

    public UserDTO getUserDTO() {
    	return userDTO;
    }
    // get current user settings. Until there is a reliable cache, must get from DB,
    //  bc values can be changed elsewhere...
    public User getUser() {
    	return userService.findById(userDTO.getUserCode());
    }
    
    public void setUserDTO (UserDTO userDTO) {
    	this.userDTO = userDTO;
    }

    public void setRole (Role role) {
    	this.role = role;
    }
    
    public Role getRole() {
    	return role;
    }
    
    public EntityItem getEntityItem() {
    	return entityItem;
    }

    public void setEntityItem (EntityItem entityItem) {
    	this.entityItem = entityItem;
    }
    
    // set user timezone, if present as per-user or system
    private void setTimeZone(HttpSession session, String user) {
    	timeZone= null;
  		Option uTZ = optionService.findByPK(
  				new OptionCodePK("USERTIMEZONE", "USER",  user));
  		Option sTZ = optionService.findByPK(
  				new OptionCodePK("DEFUSERTIMEZONE", "SYSTEM", "SYSTEM"));
  		Option sysTZ = optionService.findByPK(
  				new OptionCodePK("SYSTEMTIMEZONE", "SYSTEM", "SYSTEM"));
    	try {
    		if ((uTZ != null) && (uTZ.getValue() != null)) {
    			timeZone = TimeZone.getTimeZone(uTZ.getValue());
    		} else if ((sTZ != null) && (sTZ.getValue() != null)) {
  				timeZone = TimeZone.getTimeZone(sTZ.getValue());
    		} else if ((sysTZ != null) && (sysTZ.getValue() != null)) {
  				timeZone = TimeZone.getTimeZone(sysTZ.getValue());
    		}
    	} catch (Exception e) {
    		logger.error(e);
    	}
    	if (timeZone == null) timeZone = TimeZone.getDefault();
  		session.setAttribute("timezone", timeZone);
  		logger.info("Setting timeZone for " + user + " to " + timeZone.getDisplayName());
    }

    /* hack to avoid the initial login screen for developers... 
     * Passes required settings via parameters
     * Safe because it still requires a valid user/pw to work 
     * */
    public String developerLoginAction() {
    	FacesContext facesContext = FacesContext.getCurrentInstance();
    	Map<String,String> params = facesContext.getExternalContext().getRequestParameterMap();
    	username = params.get("username");
    	password = params.get("password");
    	validLogin=false;
    	loginActionListener(null);
    	// incase login failed
    	if (!validLogin) return "logout";
			this.setUser();
			this.setUserPrivilege(null);
			if (params.get("timeZone") != null) { 
      	HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
    		timeZone = TimeZone.getTimeZone(params.get("timeZone"));
    		session.setAttribute("timezone", timeZone);
    	}
    	if (params.get("role") != null) 	setDynamicMenus (username.toUpperCase(), params.get("role"), 0L);
    	return params.get("initialPage");
    }
    

    private void resetFlags(){
    	 showAdminMenu = false;
    	 showFileMenu = true;
    	 showFilePreferencesMenu = false;
    	 showFileEntityMenu = false;
    		
    	 setMenuVisibility("m_process", true);
    	 showTransactionsProcessMenu = false;
    	 showTransactionsStatisticsMenu = false;
    	 showViewSaleTransactionsMenu = false;
    		
    	 showDataUtilityMenu = true;
    	 showDataUtilityBatchMenu = false;
    	 showDataUtilityImportMapMenu = false;
    	 showDataUtilityImportMapSalesMenu = false;
    	 showMaintenanceTaxabilityMenubill=false;
    	 showDataUtilityGLExtractMenu = false;
    	 showDataUtilityGLMarkTransMenu = false;
    	 showDataUtilityTaxRateUpdateMenu = false;
    	 showTransDetailList = false;
    		
    	 showMaintenanceMenu = true;
    	 showMaintenanceTaxabilityMenu = false;
    	 showMaintenanceLocationMenu = false;
    	 showMaintenanceAllocationMenu = false;
    	 showMaintenanceTaxAllocationMenu = false;
    	 showMaintenanceTaxabilityCodeMenu = false;
    	 showMaintenanceTaxCodeRulesMenu = false;
    	 showMaintenanceSitusRulesMenu = false;
    	 showMaintenanceCustLocnMenu = false;
    	 showMaintenanceTaxRateMenu = false;
    	 showMaintenanceDriverReferenceMenu = false;
    	 showTaxHolidayMaintenanceMenu = false;
    	 showCustomerLocationMainMenu = false;
    	 showExemptionCertificateMainMenu = false;
    	 showEntitiesMaintenanceMenu = false;
    	 showVendorsMaintenanceMenu = false;
    	 showNexusDefMaintenanceMenu = false;
    	 showProcessRulesMaintenanceMenu = false;
    	 showProcessTablesMaintenanceMenu = false;
    		
    	 showSecurityMenu = true;
    	 showSecurityPasswordMenu = false;
    	 showSecurityModulesMenu = false;
    	 showSecurityEntityStructureMenu = false;
    		
    	 showUtilitiesMenu = true;
    	 showUtilitiesRunMCP = false;
    	 showFixBrokenJob = false;
    	 showUtilitiesResetDataTables = false;
    	 showUtilitiesBuildIndices = false;
    	 showUtilitiesReweightMatrices = false;
    	 showUtilitiesResetSequences = false;
    	 showUtilitiesRebuildDriverRefs = false;
    	 showUtilitiesTruncateBCPTables = false;
    	 showUtilitiesApiTest = false;
    	 showRunBatchProcess = false;
    	 showUtilitiesBillingTestTool = false;
    	 showUtilitiesPurchaseTestTool = false;
    	 showAidFileManager = false;

    	 showSetupMenu = true;
    	 showSetupDatabaseMenu = false;
    	 showSetupDataDefinitionsMenu = false;
    	 showSetupMatrixDriversMenu = false;
    	 showSetupSitusDriversMenu = false;
    	 showMaintenanceSitusMatrixMenu = false;
    	 showSetupImportDefinitionsMenu = false;
    	 showSetupListsMenu = false;
    	 showSetupDatabaseStatLinesMenu = false;
    	 showSetupSupportingCodesMenu = false;
    	 showFiscalYearMenu=false;
    }
      
    public String loginAdminAction() {
    	return "usersecurity_main";
    }
    
    public String loginChangePasswordAction() {
    	return "usersecurity_changepassword";
    }

    /* don't do any DB operations in here, as we have not yet set the DB */
    private void setUser(){
    	if(username != null){
    		UserDTO user = new UserDTO();
    		user.setUserCode(username.toUpperCase());
    		HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);
    		// Save the user in the session
    		session.setAttribute("user", user);
    		
    		// Reset/Initialize
    		//setMessage(null);
    		setPassword(null);
    		validLogin = true;
    	}
    }
 
    private Map<String, String > subMenuMap = null;
    
    public Map<String, String > getSubMenuMap(){
    	if(subMenuMap==null){
    		subMenuMap = new LinkedHashMap<String, String >();
    		
    		List<Menu> allMenu = menuService.findAllMenus();
    		for(Menu menu : allMenu){
    			subMenuMap.put(menu.getMenuCode(), menu.getOptionName());
        	}
    	}
    	return subMenuMap;
    }
    
    /* OK, now that user has selected a DB, we can do the rest of the set up */
    public boolean setUserPrivilege(UserDTO user) {
    	User u = userService.findById(username.toUpperCase());
    	if (u == null) {
    		logger.error("No user record for login: " + username);
			this.setNoUserMessage("No User record for " + username);
			validLogin=false;
			selectedDb = null;
			setPassword(null);
			setUsername(DEFAULT_USER_NAME);
    		return false;
    	} else {
    		if (u.getActiveBooleanFlag() == false) {
    			logger.error("User is not active: " + username);
    			this.setNoUserMessage("User " + username + " is not active.");
    			validLogin=false;
    			selectedDb = null;
    			setPassword(null);
    			setUsername(DEFAULT_USER_NAME);
    			return false;
    		}
    	}

		if(user == null) {
			user = new UserDTO();
		}
		BeanUtils.copyProperties(u, user);
		HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
		setTimeZone(session, user.getUserCode());
		this.setUserDTO(user);
		
		// Find the role based on the User Code (MBF: and entity)
		// JMW: I believe priority should be given to last entity used and that role...
		UserEntity userEntity = null;
		if (user.getLastUsedEntityId() != null) {
			userEntity = userEntityService.findById(new UserEntityPK(user.getUserCode(),user.getLastUsedEntityId()));
			if (userEntity != null) {
				user.setUserRole(userEntity.getRoleCode());
				logger.info("userRole: " + user.getUserRole());
				// Set dynamic menus
				setDynamicMenus(username.toUpperCase(), user.getUserRole(), user.getLastUsedEntityId());
				// set entity and role information
				setEntityItem(user.getLastUsedEntityId());
				setRole(user.getUserRole());
			}
		}
		return true;
    }

    public void setEntityItem (long entityId) {
		logger.info("Entity ID = " + entityId);
		entityItem = entityItemService.findById(entityId);    	
    }
    
    public void setRole (String roleId) {
		logger.info("Role ID = " + roleId);
		role = roleService.findById(roleId);    	
    }
    

    public void loginActionAdminListener(ActionEvent event) {
	}
    
    public String cancelchangePasswordAction(){
    	userSecurityBean.clearPasswordAction();
    	
    	//If the app forces changing password and the user is not an admin, lock this user. 
    	if(userSecurityDTO!=null && userSecurityDTO.getChange().intValue()==1){
    		HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
            UserDTO user = (UserDTO) session.getAttribute("user");
            
            // Is an Admin?
			UserRolesDTO userRolesDTO = userSecurityBean.getUserRoles(userSecurityDTO.getRoleId());
			if(!(userRolesDTO!=null && userRolesDTO.getAdmin().intValue()==1)){
				// ******** Not an admin, the password will be locked. ******** //
				userSecurityBean.lockPassword(userSecurityDTO.getUserName());
				
				logoutActionListener(null); 
			}
		}

	        
		return "logout";
	}
    
    public String donePasswordAction(){
    	userSecurityBean.clearPasswordAction();
    	
    	HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
        UserDTO user = (UserDTO) session.getAttribute("user");
        
        logoutActionListener(null);     
        return "logout";
	}
    
    public String loginAction() {

    	if(validLogin && selectedDb!=null  && !selectedDb.equals("0")){
    		return loginSelectedDb();
    	}
    	
    	logger.info("start loginAction");
    	
    	this.purchaseMenus = ValidMenuOptions.getPurchaseMenus();
		this.saleMenus = ValidMenuOptions.getSaleMenus();
    	
    	HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
    	
    	if (validLogin) {
    		UserDTO user = (UserDTO) session.getAttribute("user");
    		
    		//Check if administrator for user account
    		if(userSecurityDTO==null){
    			return "logout";
    		}
    		
    		//If changing password is needed, force to redirect to change password page.
			if(userSecurityDTO!=null && userSecurityDTO.getChange().intValue()==1){
				return "usersecurity_changepassword";
			}
    			
    		//If not admin and only one db available, go to PinPoint directly.
    		if(userSecurityDTO!=null && directToPinPointFlag==true){
    			List<String> dbPropertiesList = userSecurityBean.findUserDbPropertiesList(userSecurityDTO.getUserId());
    			if(dbPropertiesList.size()==1){	
    				// Only one database so navigate automatically.
        			logger.info(" only one DB available   ");
        			String dbName = (String)dbPropertiesList.get(0);
        			logger.info(" selected DB = " + dbName);
            		ChangeContextHolder.clearDataBase();
            		ChangeContextHolder.setDataBaseType(dbName);
            		session.setAttribute("selectedDb", dbName);   
        			this.setSelectedDb(dbName);  
        			LogMemory.executeGC();
        			
        			//Build sub menu map
        			getSubMenuMap();
        			
        			return this.getNavigation(user);
    			}
    		}
    		
    	}
	            
    	return "logout";
    }
    
    public void loginActionListener(ActionEvent event) {
    	//Already proceed & wait for login
    	if(validLogin && selectedDb!=null  && !selectedDb.equals("0")){
    		return;
    	}
    	
    	logger.info("start loginActionListener");
    	ChangeContextHolder.clearDataBase();
    	
    	directToPinPointFlag = false;
    	adminUserFlag = false;
        enableChangeFlag = false;
        enableCancelFlag = false;
        expiredLogin=false;
    	userSecurityDTO = null;
    	setMessage(null);
    	     
    	if (!validLogin) {    	
    		userSecurityDTO = userSecurityBean.getUserSecurity(username.toUpperCase());
    		
    		if(userSecurityDTO!=null){
    			String encryptedPassword = userSecurityDTO.getPassword();
    			String userConfigPassword = DesEncrypter.getInstance().decrypt(encryptedPassword);

    			if (userConfigPassword!=null && userConfigPassword.equals(password)) {
	    			logger.info("user logged in");
	    			
	    			//Check if the user has been locked.
	    			if(userSecurityDTO.getLock()!=null && userSecurityDTO.getLock().intValue()==1){
	    				setMessage("Your password has been locked. Please contact your administrator.");
	    			}
	    			else{   
	    				int iDays = userSecurityBean.passwordExpiredInDays(username.toUpperCase());	
	    				if(userSecurityDTO.getChange().intValue()==1){
	    					//If changing password is needed.
	    					setMessage("Your new password must be changed.");
	    					expiredLogin=true;
	    					enableChangeFlag = true;
	    					enableCancelFlag = true;
	    				}
	    				else if(iDays==-1){
	    					setMessage("Your password is expired.");
	    					expiredLogin=true;
	    					enableChangeFlag = true;
	    					enableCancelFlag = true;
	    				}
	    				else if(iDays==-2 || iDays>=0){//>=0: will be expired, -2: in good standing
	    					if(iDays>=2){
	    						setMessage("Your password will be expired in "+iDays+" days.");
	    						enableChangeFlag = true;
	    					}
	    					else if(iDays>=0){
	    						setMessage("Your password will be expired in "+iDays+" day.");
	    						enableChangeFlag = true;
	    					}
	    					else{
	    						//Go to PinPoint directly if possible
	    						directToPinPointFlag = true;
	    					}
	    					
	    					enableCancelFlag = true;
	    					    					
    		    			//Enable Admin
    		    			UserRolesDTO userRolesDTO = userSecurityBean.getUserRoles(userSecurityDTO.getRoleId());
    		    			if(userRolesDTO!=null && userRolesDTO.getAdmin().intValue()==1){
    		    				adminUserFlag = true;
    		    				
    		    				//Not to go to PinPoint directly
    		    				directToPinPointFlag = false;
    		    			}
    		    			
    		    			//Reset attempts
    		    			if(!userSecurityBean.resetAttemptToUserSecurity(username.toUpperCase())){
    		    				//Try one more time, just in case the database is locked. 
    		    				if(!userSecurityBean.resetAttemptToUserSecurity(username.toUpperCase())){
	    		    				//Update SQLite failed
	    		    				setMessage("Cannot update user property, please try again!");
	    			    			logger.info("user not logged in");
	    			    			
	    			    			setNoUserMessage(null);
	    			    			setPassword(null);
	    			    			directToPinPointFlag = false;
	    			    	    	adminUserFlag = false;
	    			    	        enableChangeFlag = false;
	    			    	        enableCancelFlag = false;
	    			    	        expiredLogin=false;
	    			    	    	userSecurityDTO = null;
	    			    	    	return;
    		    				}
    		    			}
	    				}
	    				
	    				// loginAction will take care of the navigation if needed ...
	    				setUser();
	    			}
	    			
	    		} else {
	    			//Add to attempt
	    			userSecurityBean.addAttemptToUserSecurity(username.toUpperCase());
	    			
	    			setMessage("Login is invalid, please try again!");
	    			logger.info("user not logged in");
	    			setPassword(null);
	    			setNoUserMessage(null);
	    			userSecurityDTO = null;
	    		}
    		}
    		else{
    			setMessage("Login is invalid, please try again!");
				logger.info("user not logged in");
				setPassword(null);
				setNoUserMessage(null);
				userSecurityDTO = null;
    		}
    	}
    	
    	LogMemory.executeGC();
	}
    
    public void loginActionListenerBK(ActionEvent event) {
    	logger.info("start loginActionListener");
    	ChangeContextHolder.clearDataBase();
    	     
    	if (!validLogin) {    		
    		if (userConfig.propertyExists("user." + username.toUpperCase() + ".password")){
    			String userConfigPassword = userConfig.getProperty("user." + username.toUpperCase() + ".password");
    			logger.info("userConfigPassword: " + userConfigPassword);
    		
    			if (userConfigPassword.equals (password)) {
	    			logger.info("user logged in");
	    			// loginAction will take care of the navigation if needed ...
	    			setUser();
	    		} else {
	    			setMessage("Login is invalid, please try again!");
	    			logger.info("user not logged in");
	    			setPassword(null);
	    			setNoUserMessage(null);
	    		}
    			
	    	} else {
				setMessage("Login is invalid, please try again!");
				logger.info("user not logged in");
				setPassword(null);
				setNoUserMessage(null);
			}
    	}
    	
    	LogMemory.executeGC();
	}
    
    public void forgottenPasswordActionListener(ActionEvent event) {
    	logger.info("start forgottenPasswordActionListener");
	    
	    if (username==null || username.length()==0){
	    	setMessage("Please enter user name before click 'forgotten password'!");
    		return;
    	}
	    
	    userSecurityDTO = userSecurityBean.getUserSecurity(username.toUpperCase());
		if(userSecurityDTO==null){
			setMessage("User email not found!");
    		return;
		}
		
		//Check if the user has been locked.
		if(userSecurityDTO.getLock()!=null && userSecurityDTO.getLock().intValue()==1){
			setMessage("Your password has been locked. Please contact your administrator.");
			return;
		}
		
		//Reset password here
		userSecurityDTO.setPassword(DesEncrypter.getInstance().encrypt(userSecurityBean.getNewPassword()));
		//password has been encrypted already.
    	if(!userSecurityBean.resetPassword(userSecurityDTO.getUserName(), 
    			userSecurityDTO.getPassword())){
    		setMessage("Cannot reset password.");
            return;
    	}

		String errorMessage = userSecurityBean.sendEmailNotification(userSecurityDTO, "PASSWORD_RESET");
	    if(errorMessage!=null && errorMessage.length()>0){
	    	setMessage(errorMessage);
			return;
	    }
	    else{
	    	setMessage("Your new password has been sent to your mail box!");
	    	return;
	    }
	}
    
    public void send(String smtpHost, int smtpPort, String from, String to, String subject, String content)
    		throws AddressException, MessagingException {
    	// Create a mail session
    	java.util.Properties props = new java.util.Properties();
		props.put("mail.smtp.host", smtpHost);
		props.put("mail.smtp.port", ""+smtpPort);
		Session session = Session.getInstance(props, null);
		
		// Construct the message
		Message msg = new MimeMessage(session);
		msg.setFrom(new InternetAddress(from));
		msg.setRecipient(Message.RecipientType.TO, new InternetAddress(to));
		msg.setSubject(subject);
		msg.setText(content);
		
		// Send the message
		Transport.send(msg);
	}

    public void setDynamicMenus (String userCode, String userRole, Long userEntity){
    	resetFlags();
    	setUserRoleDynamicMenus(userRole);
    	setUserExceptionDynamicMenus (userCode, userEntity);		
    }
    
    public void setUserRoleDynamicMenus (String userRole){
    	
    	// Set menus based on roles
		List<RoleMenu> roleMenuList = roleUserMenuService.findRoleMenuByRoleCode(userRole);
		for(Iterator<RoleMenu> itr = roleMenuList.iterator(); itr.hasNext();){
			RoleMenu roleMenu = itr.next();	
			logger.info("Role Menu Code:  " + roleMenu.getRoleMenuPK().getMenuCode());	
			
			setMenuVisibility (roleMenu.getRoleMenuPK().getMenuCode(),true);
			
		}
    }
    
    public void setUserExceptionDynamicMenus (String userCode, Long userEntity){
    	
    	//List<UserMenuEx> exMenuList = userMenuExService.findByUserCode(username);
    	if(userCode.equals("STSCORP")){
    		logger.info("############################################### USER STSCORP");
    		setMenuForStscorp();
    	}
    	else{
    		logger.info("############################################### !!!!!!!!!!!!!!!!!!!!!!!!!! STSCORP");
    	List<UserMenuEx> exMenuList = userMenuExService.findByUserEntity(username.toUpperCase(), userEntity);
		for(Iterator<UserMenuEx> itume = exMenuList.iterator(); itume.hasNext();){
			UserMenuEx userMenuEx = itume.next();	
			logger.info("User Menu Ex Menu Code:  " + userMenuEx.getUserMenuExPk().getMenuCode());	
			
			if("G".equalsIgnoreCase(userMenuEx.getExceptionType()))
				setMenuVisibility(userMenuEx.getUserMenuExPk().getMenuCode(),true);
			else if("D".equalsIgnoreCase(userMenuEx.getExceptionType()))
				setMenuVisibility(userMenuEx.getUserMenuExPk().getMenuCode(),false);
		}
    	}
    }
    
    public void setMenuForStscorp() {
    	setMenuVisibility("m_admin", true);
    	setMenuVisibility("m_adminsitusdrivers", true);
    	setMenuVisibility("m_adminsitusmatrix", true);
    	
    	showFileMenu = true;
    	setMenuVisibility("m_properties", true);
    	setMenuVisibility("m_openentity", true);
    	
    	showTransactionsMenu = true;
    	setMenuVisibility("m_transactions", true);
    	setMenuVisibility("m_statistics", true);
    	setMenuVisibility("m_viewtrans", true);
    	setMenuVisibility("m_billingtesttool", true);
    	setMenuVisibility("m_purchtesttool",true);

		showDataUtilityMenu = true;
		setMenuVisibility("m_batchesmaintstatus", true);
		setMenuVisibility("m_importprocess", true);
		setMenuVisibility("m_importprocesssales", true);
		setMenuVisibility("m_glextract", true);
    	setMenuVisibility("m_taxrateupdate", true);
    	setMenuVisibility("m_transdetaillist", true);

		showMaintenanceMenu = true;
    	setMenuVisibility("m_taxabilitymatrix", true);
    	setMenuVisibility("m_locationmatrix", true);
    	setMenuVisibility("m_allocationmatrix", true);
    	setMenuVisibility("m_taxallocationmatrix", true);
    	setMenuVisibility("m_taxabilitycodes", true);
    	setMenuVisibility("m_taxcoderules", true);
    	setMenuVisibility("m_situsrules", true);
    	setMenuVisibility("m_custlocn", true);
    	setMenuVisibility("m_taxrates", true);
    	setMenuVisibility("m_driverreference", true);
    	setMenuVisibility("m_taxholiday", true);
    	setMenuVisibility("m_customerlocation", true);
    	setMenuVisibility("m_exempcert", true);
    	setMenuVisibility("m_entities", true);
    	setMenuVisibility("m_nexus_def", true);

		showSecurityMenu = true;
    	setMenuVisibility("m_changepassword", true);
    	setMenuVisibility("m_securitymodules", true);
    	setMenuVisibility("m_entitystructurelevels", true);

		showUtilitiesMenu = true;
    	setMenuVisibility("m_runmcp", true);
    	setMenuVisibility("m_resetdwsyntax", true);
    	setMenuVisibility("m_buildindexes", true);
    	setMenuVisibility("m_reweightmatrixes", true);
    	setMenuVisibility("m_resetsequences", true);
    	setMenuVisibility("m_rebuilddriverreferences", true);
    	setMenuVisibility("m_truncatebcptables", true);
    	setMenuVisibility("m_apitest", true);
    	setMenuVisibility("m_runbatchprocess", true);
    	setMenuVisibility("m_aidfilemanager", true);

		showSetupMenu = true;
    	setMenuVisibility("m_databasesetup", true);
    	setMenuVisibility("m_datadefinitions", true);
    	setMenuVisibility("m_matrixdrivers", true);
    	setMenuVisibility("m_situsdrivers", true);
    	setMenuVisibility("m_situsmatrix", true);
    	setMenuVisibility("m_importdefinitionsandspecs", true);
    	setMenuVisibility("m_lists", true);
    	setMenuVisibility("m_dbstatisticslines", true);
    	setMenuVisibility("m_supportingcodes", true);
    	setMenuVisibility("m_fiscal_years", true);
    }
    
    public void setMenuForStscorpOld() {
    	showAdminMenu = true;
		showFileMenu = true;
		showFilePreferencesMenu = true;
		showFileEntityMenu = true;

		showTransactionsMenu = true;
		showTransactionsProcessMenu = true;
		showTransactionsStatisticsMenu = true;
		showViewSaleTransactionsMenu = true;

		showDataUtilityMenu = true;
		showDataUtilityBatchMenu = true;
		showDataUtilityImportMapMenu = true;
		showDataUtilityImportMapSalesMenu = true;
		showMaintenanceTaxabilityMenubill=true;
		showDataUtilityGLExtractMenu = true;
		showDataUtilityGLMarkTransMenu = true;
		showDataUtilityTaxRateUpdateMenu = true;
		showTransDetailList = true;

		showMaintenanceMenu = true;
		showMaintenanceTaxabilityMenu = true;
		showMaintenanceLocationMenu = true;
		showMaintenanceAllocationMenu = true;
		showMaintenanceTaxAllocationMenu = true;
		showMaintenanceTaxabilityCodeMenu = true;
		showMaintenanceTaxCodeRulesMenu = true;
		showMaintenanceSitusRulesMenu = true;
		showMaintenanceCustLocnMenu = true;
		showMaintenanceTaxRateMenu = true;
		showMaintenanceDriverReferenceMenu = true;
		showTaxHolidayMaintenanceMenu  = true;
		showCustomerLocationMainMenu = true;
		showExemptionCertificateMainMenu = true;
		showEntitiesMaintenanceMenu = true;
		showVendorsMaintenanceMenu = true;
		showNexusDefMaintenanceMenu = true;
		showProcessRulesMaintenanceMenu = true;
		showProcessTablesMaintenanceMenu = true;

		showSecurityMenu = true;
		showSecurityPasswordMenu = true;
		showSecurityModulesMenu = true;
		showSecurityEntityStructureMenu = true;

		showUtilitiesMenu = true;
		showUtilitiesRunMCP = true;
		showUtilitiesResetDataTables = true;
		showUtilitiesBuildIndices = true;
		showUtilitiesReweightMatrices = true;
		showUtilitiesResetSequences = true;
		showUtilitiesRebuildDriverRefs = true;
		showUtilitiesTruncateBCPTables = true;
		showUtilitiesApiTest = true;
		showRunBatchProcess = true;
		showUtilitiesBillingTestTool = true;
		showUtilitiesPurchaseTestTool = true;
		showAidFileManager = true;

		showSetupMenu = true;
		showSetupDatabaseMenu = true;
		showSetupDataDefinitionsMenu = true;
		showSetupMatrixDriversMenu = true;
		showSetupSitusDriversMenu = true;
		showMaintenanceSitusMatrixMenu = true;
		showSetupImportDefinitionsMenu = true;
		showSetupListsMenu = true;
		showSetupDatabaseStatLinesMenu = true;
		showSetupSupportingCodesMenu = true;
		showFiscalYearMenu=true;
    }
    
    
    public void setMenuVisibility (String menuCode, boolean visibility) {
    	HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
		UserDTO userDTO = (UserDTO) session.getAttribute("user");
		String lastModule = userDTO.getLastModule(); 
		String userCode = userDTO.getUserCode();
		Option option = optionService.findByPK(new OptionCodePK ("LASTMODULE", "USER", userCode));	
    
    	if(selectedTaxType==null || selectedTaxType.length()==0){   		
    		if((this.license != null && license.hasSaleLicense())) {			
    			if(option != null && option.getValue() != null && option.getValue().equalsIgnoreCase("BILLSALE")){ 			
    				selectedTaxType = BILLING_TAX_TYPE;				
    			}
    			else if(option !=null && option.getValue()!=null && lastModule!=null && option.getValue().equalsIgnoreCase("LASTMODULE") && !lastModule.equalsIgnoreCase("PURCHUSE")){   	    	
    	    		selectedTaxType = BILLING_TAX_TYPE; 	    		
         		}
    			else if(option==null || option.getValue()==null || option.getValue().trim().length()==0 || lastModule == null ){
         			selectedTaxType = BILLING_TAX_TYPE;
         		}		
    		}
       	}
    	
    	//0005307
    	if(selectedTaxType==null || selectedTaxType.length()==0){ 
    		if((this.license != null && license.hasPurchaseLicense())) {         		  			
    			if(option != null && option.getValue() != null && option.getValue().equalsIgnoreCase("PURCHUSE")){ 
    					selectedTaxType = PURCHASING_TAX_TYPE;
    			}
    			else if (option !=null && option.getValue()!=null && lastModule!=null &&  option.getValue().equalsIgnoreCase("LASTMODULE") && !lastModule.equalsIgnoreCase("BILLSALE")){
        	    		selectedTaxType = PURCHASING_TAX_TYPE;   
    			}
    			else if(option==null || option.getValue()==null || option.getValue().trim().length()==0 || lastModule == null ){
         			selectedTaxType = PURCHASING_TAX_TYPE;
         		}	
        	}
    		else if(selectedTaxType==null){
        		selectedTaxType = PURCHASING_TAX_TYPE;
        	}
    	}
        	
    	if(this.license == null || license.getLicenseType() == 0) {
    		visibility = false;
    	}
    	else if(this.license.hasPurchaseLicense() && selectedTaxType.equals(PURCHASING_TAX_TYPE)) {
    		if(!this.purchaseMenus.contains(menuCode)) {
    			visibility = false;
    		}
    	}
    	else if(this.license.hasSaleLicense() && selectedTaxType.equals(BILLING_TAX_TYPE)) {
    		if(!this.saleMenus.contains(menuCode)) {
    			visibility = false;
    		}
    	}
    	
    	/*
    	if(this.license == null || license.getLicenseType() == 0) {
    		visibility = false;
    	}
    	else if(this.license.hasPurchaseLicense() && !this.license.hasSaleLicense()) {
    		if(!this.purchaseMenus.contains(menuCode)) {
    			visibility = false;
    		}
    	}
    	else if(this.license.hasSaleLicense() && !this.license.hasPurchaseLicense()) {
    		if(!this.saleMenus.contains(menuCode)) {
    			visibility = false;
    		}
    	}
    	*/
    	
    	if (menuCode.equals("m_archive")) {
    		// future use
    	} else if(menuCode.equals("m_admin")) {
    		showAdminMenu = visibility;
    	} else if(menuCode.equals("m_adminsitusdrivers")) {
    		showAdminSitusDriversMenu = visibility;
    	} else if(menuCode.equals("m_adminsitusmatrix")) {
    		showAdminSitusMatrixMenu = visibility;
    	} else if(menuCode.equals("m_openentity")) {
    		showFileEntityMenu = visibility;
    	} else if (menuCode.equals("m_resetsequences")) {
	    	showUtilitiesResetSequences = visibility;
		} else if (menuCode.equals("m_runmcp")) {
			showUtilitiesRunMCP = visibility;
		} else if (menuCode.equals("m_checkthebus")) {
			showFixBrokenJob = visibility;	
		} else if (menuCode.equals("m_changepassword")) {
			showSecurityPasswordMenu = visibility; 
		} else if (menuCode.equals("m_crystalreports")) {
			// not used anymore - now on link menu
		} else if (menuCode.equals("m_driverreference")) {
			showMaintenanceDriverReferenceMenu = visibility; 
		} else if (menuCode.equals("m_securitymodules")) {
			showSecurityModulesMenu = visibility; 
		} else if (menuCode.equals("m_databasesetup")) {
			showSetupDatabaseMenu = visibility; 
		} else if(menuCode.equals("m_fiscal_years")){
			showFiscalYearMenu = visibility; 
		}else if (menuCode.equals("m_datadefinitions")) {
			showSetupDataDefinitionsMenu = visibility; 
		} else if (menuCode.equals("m_dbstatisticslines")) {
			showSetupDatabaseStatLinesMenu = visibility;
		} else if (menuCode.equals("m_supportingcodes")) {
			showSetupSupportingCodesMenu = visibility;
		} else if (menuCode.equals("m_entitystructurelevels")) {
			showSecurityEntityStructureMenu = visibility; 
		} else if (menuCode.equals("m_taxholiday")) {
			showTaxHolidayMaintenanceMenu  = visibility; 	
		} else if (menuCode.equals("m_customerlocation")) {
			showCustomerLocationMainMenu  = visibility; 
		} else if (menuCode.equals("m_processrules")) {
			showProcessRulesMaintenanceMenu  = visibility; 	
		} else if (menuCode.equals("m_processtables")) {
			showProcessTablesMaintenanceMenu  = visibility; 	
		} else if (menuCode.equals("m_exempcert")) {
			showExemptionCertificateMainMenu  = visibility; 
		} else if (menuCode.equals("m_entities")) {
			showEntitiesMaintenanceMenu  = visibility;
		} else if (menuCode.equals("m_vendors")) {
			showVendorsMaintenanceMenu  = visibility;
		} else if (menuCode.equals("m_nexus_def")) {
			showNexusDefMaintenanceMenu  = visibility;
		} else if (menuCode.equals("m_resetdwsyntax")) {
			showUtilitiesResetDataTables = visibility;
		} else if (menuCode.equals("m_importdefinitionsandspecs")) {
			showSetupImportDefinitionsMenu = visibility;
		} else if (menuCode.equals("m_importprocess")) {
			showDataUtilityImportMapMenu = visibility;
		} else if (menuCode.equals("m_importprocesssales")) {
			showDataUtilityImportMapSalesMenu = visibility;
		} else if (menuCode.equals("m_taxabilitymatrix")) {
			showMaintenanceTaxabilityMenu = visibility; 
		}else if(menuCode.equals("m_taxabilitymatrix_bill")){
			showMaintenanceTaxabilityMenubill=visibility;
		}else if (menuCode.equals("m_taxrateupdate")) {
			showDataUtilityTaxRateUpdateMenu = visibility; 
		} else if (menuCode.equals("m_properties")) {
			showFilePreferencesMenu = visibility; 
		} else if (menuCode.equals("m_rebuilddriverreferences")) {
	    	showUtilitiesRebuildDriverRefs = visibility;
			//showMaintenanceDriverReferenceMenu = visibility;
		} else if (menuCode.equals("m_batchesmaintstatus")) {
			showDataUtilityBatchMenu = visibility; 
		} else if (menuCode.equals("m_glextract")) {
			showDataUtilityGLExtractMenu = visibility; 
		} else if (menuCode.equals("m_glmarktrans")) {
			showDataUtilityGLMarkTransMenu = visibility; 
		} else if (menuCode.equals("m_lists")) {
			showSetupListsMenu = visibility; 
		} else if (menuCode.equals("m_statistics")) {
			showTransactionsStatisticsMenu = visibility;
		} else if (menuCode.equals("m_viewtrans")) {
			showViewSaleTransactionsMenu = visibility;
		} else if (menuCode.equals("m_taxrates")) {
			showMaintenanceTaxRateMenu = visibility; 
		} else if (menuCode.equals("m_buildindexes")) {
			showUtilitiesBuildIndices = visibility;
			//showFileEntityMenu = visibility;
		} else if (menuCode.equals("m_locationmatrix")) {
			showMaintenanceLocationMenu = visibility;
		} else if (menuCode.equals("m_taxabilitycodes")) {
			showMaintenanceTaxabilityCodeMenu = visibility;
		} else if (menuCode.equals("m_taxcoderules")) {
			showMaintenanceTaxCodeRulesMenu = visibility; 
		} else if (menuCode.equals("m_situsrules")) {
			showMaintenanceSitusRulesMenu = visibility; 
		} else if (menuCode.equals("m_custlocn")) {
			showMaintenanceCustLocnMenu = visibility; 
		} else if (menuCode.equals("m_windowsexplorer")) {
			// not used
		} else if (menuCode.equals("m_logonprofiles")) {
			// not used
		} else if (menuCode.equals("m_matrixdrivers")) {
			showSetupMatrixDriversMenu = visibility;
		} else if (menuCode.equals("m_situsdrivers")) {
			showSetupSitusDriversMenu = visibility;	
		} else if (menuCode.equals("m_situsmatrix")) {
			showMaintenanceSitusMatrixMenu = visibility;	
		} else if (menuCode.equals("m_transactions")) {
			showTransactionsProcessMenu = visibility; 
		} else if (menuCode.equals("m_rulesandsoftcodes")) {
			// never used
		} else if (menuCode.equals("m_allocationmatrix")) {
			showMaintenanceAllocationMenu = visibility;
		} else if (menuCode.equals("m_taxallocationmatrix")) {
			showMaintenanceTaxAllocationMenu = visibility;	
		} else if (menuCode.equals("m_aidfilemanager")) {
			showAidFileManager = visibility;	
		} else if (menuCode.equals("m_truncatebcptables")) {
	    	showUtilitiesTruncateBCPTables = visibility;
		} else if (menuCode.equals("m_apitest")) {
			showUtilitiesApiTest = visibility;
		} else if (menuCode.equals("m_runbatchprocess")) {
			showRunBatchProcess = visibility;
		} else if (menuCode.equals("m_billingtesttool")) {
			showUtilitiesBillingTestTool = visibility;
		} else if (menuCode.equals("m_purchtesttool")) {
			showUtilitiesPurchaseTestTool = visibility;
		}else if (menuCode.equals("m_transdetaillist")) {
			showTransDetailList = visibility;
		} else if (menuCode.equals("m_reweightmatrixes")) {
	    	showUtilitiesReweightMatrices = visibility;
		} else if (menuCode.equals("m_taxpartnerextract")) {
			// not implemented
		} else {
		}	

    }
    
    public void logoutActionListener(ActionEvent event) {
    	try {
    		userService.updateUserLogoff(Auditable.currentUserCode());
    	}
    	catch(Exception e) {
    		e.printStackTrace();
    	}
    	
		FacesContext context = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) context.getExternalContext().getSession(false);
		if (session != null) {
			session.invalidate();
		}
		ChangeContextHolder.clearDataBase();
		validLogin=false;
		setNoUserMessage(null);
		setMessage(null);
		setPassword(null);
		setUsername(DEFAULT_USER_NAME);
		
		adminUserFlag = false;
        enableChangeFlag = false;
        enableCancelFlag = false;
        expiredLogin=false;
    	userSecurityDTO = null;
		
		LogMemory.executeGC();
	}
    
    /*
     if (validLogin) {
    		UserDTO user = (UserDTO) session.getAttribute("user");
    		
    		//Check if administrator for user account
    		if(userSecurityDTO==null){
    			return "logout";
    		}
    		
    		//If changing password is needed, force to redirect to change password page.
			if(userSecurityDTO!=null && userSecurityDTO.getChange().intValue()==1){
				return "usersecurity_changepassword";
			}
    			
    		//If not admin and only one db available, go to PinPoint directly.
    		if(userSecurityDTO!=null && directToPinPointFlag==true){
    			List<String> dbPropertiesList = userSecurityBean.findUserDbPropertiesList(userSecurityDTO.getUserId());
    			if(dbPropertiesList.size()==1){	
    				// Only one database so navigate automatically.
        			logger.info(" only one DB available   ");
        			String dbName = (String)dbPropertiesList.get(0);
        			logger.info(" selected DB = " + dbName);
            		ChangeContextHolder.clearDataBase();
            		ChangeContextHolder.setDataBaseType(dbName);
            		session.setAttribute("selectedDb", dbName);   
        			this.setSelectedDb(dbName);  
        			LogMemory.executeGC();
        			
        			return this.getNavigation(user);
    			}
    		}
    		
    	}
     */
    
    public boolean getIsValidLoginAdminRole() {
    	if (validLogin) {
    		HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
            UserDTO user = (UserDTO) session.getAttribute("user");
            
            
            String dbName = (String)session.getAttribute("selectedDb");   
    		
    		//Check if administrator for user account
    		if(user!=null && dbName!=null && adminUserFlag){
    			return true;
    		}
    	}
    	
    	return false;
    }
    
    public boolean getIsAllowedViewCpEntity() {
    	if (validLogin) {
    		Option option = optionService.findByPK(new OptionCodePK("CONTROLPOINTENABLED", "ADMIN", "ADMIN"));
			if(option !=null) {
				if(option.getValue()!=null && option.getValue().equals("1")){
					return true;
				}
			}
    	}
    	
    	return false;
    }
    
    public void selectBillingActionListener(ActionEvent event) {	
    	selectedTaxType = BILLING_TAX_TYPE;	
    	HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
		UserDTO userDTO = (UserDTO) session.getAttribute("user");
		String userCode = userDTO.getUserCode();
		userDTO.setLastModule("BILLSALE");
		session.setAttribute("user", userDTO);
    	User user = userService.findById(userCode);
       	user.setLastModule("BILLSALE");
       	userService.saveOrUpdate(user);
    	displayTaxTypeMenu();
    }
    
    public boolean getIsBillingSelected() {
    	return (selectedTaxType!=null && selectedTaxType.equals(BILLING_TAX_TYPE));
    }
    
    public boolean getIsPurchasingSelected() {
    	return (selectedTaxType!=null && selectedTaxType.equals(PURCHASING_TAX_TYPE));
    }
    
    public boolean getHasPurchaseLicense() {
    	return (this.license != null && license.hasPurchaseLicense());
    }
    
    public boolean getHasBillingLicense() {
    	return (this.license != null && license.hasSaleLicense());
    }
    
    public void selectPurchasingActionListener(ActionEvent event) {	
    	selectedTaxType = PURCHASING_TAX_TYPE;
    	HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
		UserDTO userDTO = (UserDTO) session.getAttribute("user");
		userDTO.setLastModule("PURCHUSE");
		session.setAttribute("user", userDTO);
		String userCode = userDTO.getUserCode();   
    	User user = userService.findById(userCode);
       	user.setLastModule("PURCHUSE");
       	userService.saveOrUpdate(user);
    	displayTaxTypeMenu();
    }
    
    public void displayTaxTypeMenu(){	
    	UserDTO user = getUserDTO();
    	UserEntity userEntity = null;
		if (user.getLastUsedEntityId() != null) {
			userEntity = userEntityService.findById(new UserEntityPK(user.getUserCode(),user.getLastUsedEntityId()));
			if (userEntity != null) {
				setDynamicMenus(username.toUpperCase(), user.getUserRole(), user.getLastUsedEntityId());
			}
		}
    }

    public String logoutAction () {
		return "logout";
    }
    
    public boolean getValidLogin() {
    	return validLogin;
    }
    
    public boolean getValidSelection(){
    	return (validLogin && selectedDb!=null  && !selectedDb.equals("0"));
    }

    public boolean getExpiredLogin() {
    	return expiredLogin;
    }
    
    public String getSelectedDb() {
    	return selectedDb;
    }
    
    public void setSelectedDb(String selectedDb) {
    	this.selectedDb = selectedDb;
    }
    
    public String selectedDbChanged() {
    	if (selectedDb != null) {
    		/*
    		HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
    		ChangeContextHolder.clearDataBase();
    		ChangeContextHolder.setDataBaseType(selectedDb);
    		session.setAttribute("selectedDb", selectedDb);
    		
            logger.info("set selected db in session with id = " + session.getId() + ", selected DB = " + selectedDb);  
    		UserDTO user = (UserDTO) session.getAttribute("user");
    		if (isValidUser(user)) {
    			return this.getNavigation(user);
    		} else {
    			return "logout";
    		}
    		*/
    	}
		return null;
    }
    
    public void selectedDbChanged(ActionEvent e) {
    	if (selectedDb != null) {
    		/*
    		HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
    		ChangeContextHolder.clearDataBase();
    		ChangeContextHolder.setDataBaseType(selectedDb);
    		session.setAttribute("selectedDb", selectedDb);
    		
            logger.info("set selected db in session with id = " + session.getId() + ", selected DB = " + selectedDb);  
    		UserDTO user = (UserDTO) session.getAttribute("user");
    		if (isValidUser(user)) {
    			return this.getNavigation(user);
    		} else {
    			return "logout";
    		}
    		*/
    	}
    }
    
    public String loginSelectedDb() {
    	if (selectedDb != null) {
    		HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
    		ChangeContextHolder.clearDataBase();
    		ChangeContextHolder.setDataBaseType(selectedDb);
    		session.setAttribute("selectedDb", selectedDb);
    		
            logger.info("set selected db in session with id = " + session.getId() + ", selected DB = " + selectedDb);  
    		UserDTO user = (UserDTO) session.getAttribute("user");
    		if (isValidUser(user)) {
    			return this.getNavigation(user);
    		} else {
    			return "logout";
    		}
    	}
		return null;
    }

    public boolean isValidUser(UserDTO user) {
		if("".equals(userService.findById(user.getUserCode())) || userService.findById(user.getUserCode())==null){
			this.setNoUserMessage("User "+user.getUserCode()+" not in "+selectedDb);
			validLogin=false;
			selectedDb = null;
			setPassword(null);
			setUsername(DEFAULT_USER_NAME);
			
			adminUserFlag = false;
	        enableChangeFlag = false;
	        enableCancelFlag = false;
	        expiredLogin=false;
	    	userSecurityDTO = null;
			return false;
		} else {
			User myuser = (User) userService.findById(user.getUserCode()); 
			if (myuser.getActiveBooleanFlag() == false) {
				this.setNoUserMessage("User " + user.getUserCode() + " is not active in " + selectedDb);
				validLogin=false;
				selectedDb = null;
				setPassword(null);
				setUsername(DEFAULT_USER_NAME);
				
				adminUserFlag = false;
		        enableChangeFlag = false;
		        enableCancelFlag = false;
		        expiredLogin=false;
		    	userSecurityDTO = null;
				return false;
			} else {
				this.setNoUserMessage(null);
				return true;
			}
		}    	
    }
    
    public License getLicense() {
    	try {
	    	Option option = optionService.findByPK(new OptionCodePK("LICENSEKEY", "SYSTEM", "SYSTEM"));
	    	if(option != null && option.getValue() != null) {
	    		license = new License(option.getValue());
	    	}
	    	else {
	    		license = null;
	    	}
    	}
    	catch(Exception e) {
    		license = null;
    	}
    	
    	return license;
    }

    private String getNavigation(UserDTO user){
    	if(user != null){
    		getLicense();
    		
    		if (setUserPrivilege(user)) {
    			//check license here and redirect
    			if((license == null || license.getLicenseType() == 0 || license.isLicenseExpired())) {
    				if("2".equals(user.getAdminFlag()) || user.getUserCode().equalsIgnoreCase("STSCORP")) {
    					resetFlags();
    					return "filepref_system_general";
    				}
    				else {
    					setMessage("Invalid License");
    		    		logger.info("Invalid License");
    		    		setPassword(null);
    		    		setNoUserMessage(null);
    		    		return "logout";
    				}
    			}
    			
    			Long lastusedEntity = userDTO.getLastUsedEntityId();
    			logger.debug("lastusedentity : " + lastusedEntity);

   				// to check if last used entity is valid
   			    User loginuser = userService.findById(user.getUserCode());
   			    UserEntity userEntity = userEntityService.findById(new UserEntityPK(user.getUserCode(),user.getLastUsedEntityId()));
   			    logger.debug("user entity is : " + userEntity);
				List<Object[]> objList = userEntityService.findForUserCode(user.getUserCode());//no of entities associated with the user.
   				if (lastusedEntity == null || (lastusedEntity != null && userEntity == null)) {
   					if(objList.size() == 0 || objList.size() > 1) {
   						return "security_user_entity";
   					}
   					else {
   						for(Iterator<Object[]> iterator  = objList.iterator(); iterator.hasNext();){
   							Object[] object = (Object[]) iterator.next();
   							setLastEntityId(object,loginuser);
   						}
   						return "importmap_main";
   					}
   				}
   				else if (optionService.isAolEnabled(userDTO.getUserCode())) {
       				return "importmap_main"; 
   				} else {
   					return "login_success";
   				}
    		} else {
    			setPassword(null);
    			return "logout";
    		}
    	} else {
    		setMessage("Login is invalid, please try again!");
    		logger.info("user not logged in");
    		setPassword(null);
    		setNoUserMessage(null);
    		return null;
    	}
    }
    
    public void setLastEntityId(Object[] object, User loginuser)  {
    	EntityItem entityItem = (EntityItem)object[2];
		String roleCode = object[4].toString();
		String userCode = object[0].toString();
		loginuser.setLastUsedEntityId(entityItem.getEntityId());
		userService.saveOrUpdate(loginuser);
		setEntityItem(entityItem.getEntityId());
		setRole(roleCode);
		setDynamicMenus(userCode, roleCode, entityItem.getEntityId());
    }
    
    public List<SelectItem> getDbItems() {
    	List<SelectItem> list = new ArrayList<SelectItem>();
    	HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
    	
    	if (validLogin) {
    		UserDTO user = (UserDTO) session.getAttribute("user");
    		if(userSecurityDTO!=null){
    			List<String> dbPropertiesList = userSecurityBean.findUserDbPropertiesList(userSecurityDTO.getUserId());
    			for (String dbName : dbPropertiesList) {			
    				list.add(new SelectItem(dbName, dbName));
    			} 
    		}
    	}
    	return list;
    }

	public TimeZone getTimeZone() {
		if (this.timeZone != null) 
			return this.timeZone;
		HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
		if (session.getAttribute("timezone") != null) 
			return (TimeZone)(session.getAttribute("timezone"));
		return TimeZone.getDefault();
	}
	
	public void setTimeZone(String id) {
		this.timeZone = TimeZone.getTimeZone(id);
	}

	public void setOptionService(OptionService optionService) {
		this.optionService = optionService;
	}

	public String getNoUserMessage() {
		return noUserMessage;
	}

	public void setNoUserMessage(String noUserMessage) {
		this.noUserMessage = noUserMessage;
	}

	public void setDatabaseConfig(String file) {
		if (this.databaseConfig == null) { 
			try {
				this.databaseConfig = new ConfigSetting(file);
			} catch (IOException e) {
				logger.error(e);
			}
		}
	}

	public void setUserConfig(String file) {
		if (this.userConfig == null) { 
			try {
				this.userConfig = new ConfigSetting(file);
			} catch (IOException e) {
				logger.error(e);
			}
		}
	}
	
	public String getEntityInfo() {
		String info = "";
		
		if(this.getEntityItem()!=null && this.getEntityItem().getEntityName()!=null && this.getEntityItem().getEntityName().length()>0 &&
				this.getRole()!=null && this.getRole().getRoleName()!=null && this.getRole().getRoleName().length()>0){
			info = "Entity: " + this.getEntityItem().getEntityName() + " as " + this.getRole().getRoleName();
		}
		
		return info;
	}
	
	public void setDataSource (RoutingDataSource dataSource) {
		this.dataSource = dataSource;
	}


	public void init() {

		if (helpMap == null) {
			InputStream input = null;
			try {
				helpMap = new Properties();
				//build_id\\help-map.properties;
				String filename = getHelpProps();
	    		input = loginBean.class.getClassLoader().getResourceAsStream(filename);
	    		if(input==null){
	    	        System.out.println("############ Sorry, unable to find " + filename);
	    		}
	    		else{
	    			helpMap.load(input); 
	    		}
			} catch (Exception e) {
				logger.error("Unable to load main menu help map", e);
				return;
			}
			finally {
				if (input!=null) {
					try {
						input.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		}
		
		if (helpBaseUrl == null) {
			InputStream input = null;
			try {
				Properties  buildMap = new Properties();
				//build_id\\help-map.properties;
				String filename = getBuildProps();
	    		input = loginBean.class.getClassLoader().getResourceAsStream(filename);
	    		if(input==null){
	    	        System.out.println("############ Sorry, unable to find " + filename);
	    		}
	    		else{
	    			buildMap.load(input);
	    			helpBaseUrl = buildMap.getProperty("helpfile");
					if(helpBaseUrl==null || helpBaseUrl.trim().length()==0){
						helpBaseUrl = "";
						System.out.println("############ Sorry, unable to find property helpfile from " + filename);
					}
	    		}
			} catch (Exception e) {
				logger.error("Unable to load main menu help map", e);
				return;
			}
			finally {
				if (input!=null) {
					try {
						input.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		}
		
		Application app = FacesContext.getCurrentInstance().getApplication();
		ViewHandler vh = app.getViewHandler();
        if (!(vh instanceof PageViewHandler)) {
			viewHandler = new PageViewHandler(vh);
			app.setViewHandler(viewHandler);
		} else {
			viewHandler = (PageViewHandler)vh;
		}
	}

	public String getHelpBaseUrl() {
		return helpBaseUrl;
	}

	public void setHelpBaseUrl(String helpBaseUrl) {
		this.helpBaseUrl = helpBaseUrl;
	}
	
	public String getHelpFile() {
		String page = viewHandler.getPage();
		page = page.substring(0, page.indexOf(".js"));
		String file = null;
		if (page != null) {
			file = helpMap.getProperty(page) != null ?
					helpMap.getProperty(page) : "";
		}
		return file;
	}
	
	public void setHelpFile(String helpFile) {
		this.helpFile = helpFile;
	}

	public String getHelpProps() {
		return helpProps;
	}

	public void setHelpProps(String helpProps) {
		this.helpProps = helpProps;
	}
	
	public String getBuildProps() {
		return buildProps;
	}

	public void setBuildProps(String buildProps) {
		this.buildProps = buildProps;
	}

	public String getAppStartupError() {
		return appStartupError;
	}

	public void setAppStartupError(String appStartupError) {
		this.appStartupError = appStartupError;
	}

	public boolean isShowNexusDefMaintenanceMenu() {
		return showNexusDefMaintenanceMenu;
	}

	public void setShowNexusDefMaintenanceMenu(boolean showNexusDefMaintenanceMenu) {
		this.showNexusDefMaintenanceMenu = showNexusDefMaintenanceMenu;
	}

	public boolean isShowDataUtilityImportMapSalesMenu() {
		return showDataUtilityImportMapSalesMenu;
	}
	

	public void setShowMaintenanceTaxabilityMenubill(
			boolean showMaintenanceTaxabilityMenubill) {
		this.showMaintenanceTaxabilityMenubill = showMaintenanceTaxabilityMenubill;
	}

	public boolean isshowMaintenanceTaxabilityMenubill() {
		return showMaintenanceTaxabilityMenubill;
	}

	public void setShowDataUtilityImportMapSalesMenu(
			boolean showDataUtilityImportMapSalesMenu) {
		this.showDataUtilityImportMapSalesMenu = showDataUtilityImportMapSalesMenu;
	}

	public boolean isShowAdminSitusDriversMenu() {
		return showAdminSitusDriversMenu;
	}

	public void setShowAdminSitusDriversMenu(boolean showAdminSitusDriversMenu) {
		this.showAdminSitusDriversMenu = showAdminSitusDriversMenu;
	}

	public boolean isShowAdminSitusMatrixMenu() {
		return showAdminSitusMatrixMenu;
	}

	public void setShowAdminSitusMatrixMenu(boolean showAdminSitusMatrixMenu) {
		this.showAdminSitusMatrixMenu = showAdminSitusMatrixMenu;
	}

	public boolean isShowRunBatchProcess() {
		return showRunBatchProcess;
	}

	public void setShowRunBatchProcess(boolean showRunBatchProcess) {
		this.showRunBatchProcess = showRunBatchProcess;
	}

	public boolean getShowFiscalYearMenu(){
		return showFiscalYearMenu;
	}
	public boolean isShowFiscalYearMenu() {
		return showFiscalYearMenu;
	}

	public void setShowFiscalYearMenu(boolean showFiscalYearMenu) {
		this.showFiscalYearMenu = showFiscalYearMenu;
	}


}
