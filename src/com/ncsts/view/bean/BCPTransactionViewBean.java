package com.ncsts.view.bean;

import javax.faces.component.html.HtmlPanelGrid;

import com.ncsts.domain.BCPPurchaseTransaction;
import com.ncsts.domain.LocationMatrix;
import com.ncsts.domain.TaxMatrix;

public class BCPTransactionViewBean {

	private MatrixCommonBean matrixCommonBean;
	private BCPPurchaseTransaction selectedTransaction;
	private String okAction;
	private HtmlPanelGrid taxDriverViewPanel;
	private HtmlPanelGrid locationDriverViewPanel;
	
	public HtmlPanelGrid getTaxDriverViewPanel() {
		if (taxDriverViewPanel == null) {
			taxDriverViewPanel = MatrixCommonBean.createDriverPanel(
					TaxMatrix.DRIVER_CODE, matrixCommonBean.getCacheManager(),
					matrixCommonBean.getEntityItemDTO(),
					"taxDriverViewPanel", 
					"bcpTransactionViewBean", "selectedTransaction", "matrixCommonBean", 
					true, false, true, false, false);
		}
		
		matrixCommonBean.prepareTaxDriverPanel(taxDriverViewPanel);
		return taxDriverViewPanel;
	}

	public void setTaxDriverViewPanel(HtmlPanelGrid taxDriverViewPanel) {
		this.taxDriverViewPanel = taxDriverViewPanel;
	}

	public HtmlPanelGrid getLocationDriverViewPanel() {
		if (locationDriverViewPanel == null) {
			locationDriverViewPanel = MatrixCommonBean.createDriverPanel(
					LocationMatrix.DRIVER_CODE, matrixCommonBean.getCacheManager(),
					matrixCommonBean.getEntityItemDTO(),
					"locationDriverViewPanel", 
					"bcpTransactionViewBean", "selectedTransaction", "matrixCommonBean", 
					true, false, true, false, false);
		}
		
		return locationDriverViewPanel;
	}

	public void setLocationDriverViewPanel(HtmlPanelGrid locationDriverViewPanel) {
		this.locationDriverViewPanel = locationDriverViewPanel;
	}

	public MatrixCommonBean getMatrixCommonBean() {
		return matrixCommonBean;
	}

	public void setMatrixCommonBean(MatrixCommonBean matrixCommonBean) {
		this.matrixCommonBean = matrixCommonBean;
	}

	public BCPPurchaseTransaction getSelectedTransaction() {
		return selectedTransaction;
	}
	
	public void setSelectedTransaction(BCPPurchaseTransaction selectedTransaction) {
		this.selectedTransaction = selectedTransaction;
	}
	
	public String getOkAction() {
		return okAction;
	}
	
	public void setOkAction(String okAction) {
		this.okAction = okAction;
	}
	
	public String okAction() {
		return okAction;
	}
}
