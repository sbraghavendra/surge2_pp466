package com.ncsts.view.bean;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.component.html.HtmlSelectBooleanCheckbox;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;

import org.ajax4jsf.component.html.HtmlAjaxSupport;
import org.apache.log4j.Logger;
import org.richfaces.component.html.HtmlDataTable;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;

import com.ncsts.common.CacheManager;
import com.ncsts.common.LoggerFactory;
import com.ncsts.common.Util;
import com.ncsts.domain.DataDefinitionColumn;
import com.ncsts.domain.DriverNames;
import com.ncsts.domain.EntityItem;
import com.ncsts.domain.GSBMatrix;
import com.ncsts.domain.ProcessedTransactionDetail;
import com.ncsts.domain.TaxCode;
import com.ncsts.domain.TaxCodeDetail;
import com.ncsts.domain.PurchaseTransaction;
import com.ncsts.jsf.model.GSBMatrixDataModel;
import com.ncsts.services.EntityItemService;
import com.ncsts.services.GSBMatrixService;
import com.ncsts.services.PurchaseTransactionService;
import com.ncsts.services.TaxCodeDetailService;
import com.ncsts.services.TransactionDetailService;
import com.ncsts.view.util.TogglePanelController;


@Scope(value = "session", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class GSBMatrixViewBean extends BaseMatrixBackingBean<GSBMatrix, GSBMatrixService> implements TaxCodeBackingBean.FindCodeCallback
{ 
	private Logger logger = LoggerFactory.getInstance().getLogger(GSBMatrixViewBean.class);
	
	@Autowired
	private TaxCodeBackingBean taxCodeBackingBean;
	@Autowired
	private EntityItemService entityItemService;
	
	@Autowired
	private PurchaseTransactionService purchaseTransactionService;
	
	private TaxCodeCodeHandler filterTaxCodeHandler;
	private TaxCodeCodeHandler thenTaxCodeHandler;
	private TaxCodeCodeHandler elseTaxCodeHandler;
	private boolean invalidDate = false;
	private boolean isEntityCheck=false;
	private List<SelectItem> entityList = null;
	private Map<Long,String> entityMap;
	
	// Selection filter inputs
	private String globalCompanyNumberFilter = MatrixCommonBean.GLOBAL_COMPANY_NUMBER_DEFAULT_VALUE;
	private String defaultLineFilter = MatrixCommonBean.DEFAULT_LINE_DEFAULT_VALUE;
	private String activeFilter=MatrixCommonBean.ACTIVE_ALL_VALUE;
	// Flags that modify suspend request
	private Date effectiveDate;
	private Boolean deleteMatrixLine;
	private Boolean suspendExtracted;
	private ProcessedTransactionDetail prossdTrDatailBean; 
	
	private TransactionDetailService transactionDetailService;
	
	private loginBean loginBean;
	private CacheManager cacheManager;
	private  CommonCallBack commonCallBack;
	private int selectedMasterRowIndex= -1;
	private GSBMatrix selectedMasterMatrixSource = null;
	private ViewTaxCodeBean viewTaxCodeBean;
	private TogglePanelController togglePanelController = null;
    public  Long entityId;
	public  String entityCode;
	public  String entityCodeFilter;
	private HtmlSelectBooleanCheckbox driverGlobalFlagBind = new HtmlSelectBooleanCheckbox();
	private TaxCodeDetailService taxCodeDetailService;
	public  TogglePanelController getTogglePanelController(){
		if(togglePanelController==null){
			togglePanelController = new TogglePanelController();
			togglePanelController.addTogglePanel("TaxDriverPanel1", true);
			togglePanelController.addTogglePanel("TaxPanel1", false);
		}
		return togglePanelController;
	}
	public void setTogglePanelController(TogglePanelController togglePanelController){
		this.togglePanelController = togglePanelController;;
	}
	@Override
	public String getBeanName() {
		return "GSBMatrixViewBean";
	}
	@Override
	public String getMainPageAction() {
		return "tax_matrix_sales_main";
	}

	@Override
	public String getDetailFormName() {
		return "matrixDetailForm";
	}
	@Override
	public boolean getIncludeDescFields() {
		// TODO Auto-generated method stub
		return false;
	}
	
	public GSBMatrixViewBean() {
		
		filterTaxCodeHandler = new TaxCodeCodeHandler();
		thenTaxCodeHandler = new TaxCodeCodeHandler();
		elseTaxCodeHandler = new TaxCodeCodeHandler();
	}
	
	public void init() {
		updateMatrixList();
	}
     public String updateMatrixList () {
		
		GSBMatrix matrix = newMatrix();
		
		BeanUtils.copyProperties(getFilterSelection(), matrix);
		
		// Now, map entity level value to the driver properties
		matrixCommonBean.setDriverEntityProperties(matrix);
		
		// Fix the driver values
		matrix.adjustDriverFields();
		EntityItem aEntityItem = this.entityItemService.findByCode(entityCodeFilter);
		if (aEntityItem != null) {
			matrix.setEntityId(aEntityItem.getEntityId());
		} else if (entityCodeFilter != null && aEntityItem == null && !entityCodeFilter.equals("")) {
			matrix.setEntityId(-1L);
		}

		updateMatrixFilter(matrix);
        matrix.setModuleCode("BILLSALE");
        matrix.setEffectiveDate(effectiveDate == null ? new com.ncsts.view.util.DateConverter().getUserLocalDate() : effectiveDate);
        
        
		// getMatrixService().getUniqueDrivers(matrix);
		
		((GSBMatrixDataModel) getMatrixDataModel()).setFilterCriteria(matrix, 
				"", 
				"", 
				filterTaxCodeHandler.getTaxcodeCode(), 
				"", 
				"",
				defaultLineFilter, "", "",effectiveDate, globalCompanyNumberFilter, activeFilter,getFilterSelection().getTaxMatrixId(),"BILLSALE");
		
	  	resetSelection();
	  	return "tax_matrix_sales_main";
    }



private List<SelectItem> countryMenuItems;

public List<SelectItem> getCountryMenuItems() {
	countryMenuItems = getMatrixCommonBean().getCacheManager().createCountryItems();   
	return countryMenuItems;
}

public List<SelectItem> getCountryMenuItemsDetail() {
	  return getMatrixCommonBean().getCacheManager().createCountryItems();
}

private List<SelectItem> stateMenuItems;

public List<SelectItem> getStateMenuItems() { 
   return stateMenuItems;
}

public void searchCountryChanged(ValueChangeEvent e){
	if(stateMenuItems!=null) stateMenuItems.clear();
}

private List<SelectItem> stateMenuItemsDetail;

public List<SelectItem> getStateMenuItemsDetail() { 
	if(stateMenuItemsDetail!=null) stateMenuItemsDetail.clear();
    return stateMenuItemsDetail;
}

public String findTaxCodeThenAction() {
	taxCodeBackingBean.findCode(this, "TaxCodeThen", "tax_matrix_detail", thenTaxCodeHandler.getTaxcodeCode());	
	return "taxability_codes";
}

public String findTaxCodeElseAction() {
	taxCodeBackingBean.findCode(this, "TaxCodeElse", "tax_matrix_detail", elseTaxCodeHandler.getTaxcodeCode());	
	return "taxability_codes";
}

public String findTaxCodeFilterAction() {
	taxCodeBackingBean.findCode(this, "TaxCodeFilter", "tax_matrix_main", filterTaxCodeHandler.getTaxcodeCode());	
	return "taxability_codes";
}

public void findCodeCallback(String code, String context) {
	if(context.equalsIgnoreCase("TaxCodeThen")) {
		thenTaxCodeHandler.setTaxcodeCode(code);
	}
	else if(context.equalsIgnoreCase("TaxCodeElse")) {
		elseTaxCodeHandler.setTaxcodeCode(code);
	} 	
	else if(context.equalsIgnoreCase("TaxCodeFilter")) {
		filterTaxCodeHandler.setTaxcodeCode(code);
	}
}

public ViewTaxCodeBean getViewTaxCodeBean() {
	return viewTaxCodeBean;
}

public void setViewTaxCodeBean(ViewTaxCodeBean viewTaxCodeBean) {
	this.viewTaxCodeBean = viewTaxCodeBean;
}

public int getselectedMasterRowIndex() {
	return selectedMasterRowIndex;
}

public void setselectedMasterRowIndex(int selectedMasterRowIndex) {
	this.selectedMasterRowIndex= selectedMasterRowIndex;
}

public Map<String, String> getSortOrder() {
	return getTransactionSaleDataModel().getSortOrder();
}


public void sortAction(ActionEvent e) {
	String id = e.getComponent().getId();
	if(id.startsWith("s_trans_")){
		//PP-357
		getTransactionSaleDataModel().toggleSortOrder(e.getComponent().getId().replace("s_trans_", ""));
	}
	else{		
		getMatrixDataModel().toggleSortOrder(e.getComponent().getParent().getId());
	}
}

@Override
public void resetSelection() {
	selectedMasterRowIndex= -1;
	selectedMasterMatrixSource = null;
	super.resetSelection();
}

public List<GSBMatrix> getSelectedMatrixDetails() {
	GSBMatrix matrix = selectedMasterMatrixSource;
	List<GSBMatrix> result = (selectedMasterMatrixSource == null)?
			(new ArrayList<GSBMatrix>()) : getMatrixService().getDetailRecords(matrix,
					getMatrixDataModel().getDetailOrderBy(),"BILLSALE");
 logger.debug("Detail items: " + result.size());
	return result;
}

public synchronized void selectedMasterRowChanged(ActionEvent e) throws AbortProcessingException {
    HtmlDataTable table = (HtmlDataTable) ((HtmlAjaxSupport) e.getComponent()).getParent(); 
    selectedMasterRowIndex = table.getRowIndex();
    selectedMasterMatrixSource = (GSBMatrix) table.getRowData();
    
    super.resetSelection();
}
public TransactionDetailService getTransactionDetailService() {
	return transactionDetailService;
}

public void setTransactionDetailService(TransactionDetailService transactionDetailService) {
	this.transactionDetailService = transactionDetailService;
}
public ProcessedTransactionDetail getProssdTrDatailBean() {
	return prossdTrDatailBean;
}

public String getGlobalCompanyNumberFilter() {
	return globalCompanyNumberFilter;
}

public void setGlobalCompanyNumberFilter(String globalCompanyNumberFilter) {
	this.globalCompanyNumberFilter = globalCompanyNumberFilter;
}

public String getDefaultLineFilter() {
	return defaultLineFilter;
}

public void setDefaultLineFilter(String defaultLineFilter) {
	this.defaultLineFilter = defaultLineFilter;
}
public String getActiveFilter() {
	return activeFilter;
}
public void setActiveFilter(String activeFilter) {
	this.activeFilter = activeFilter;
}
public Boolean getDeleteMatrixLine() {
	return deleteMatrixLine;
}

public void setDeleteMatrixLine(Boolean deleteMatrixLine) {
	this.deleteMatrixLine = deleteMatrixLine;
}

public Date getEffectiveDate() {
	return effectiveDate;
}

public void setEffectiveDate(Date effectiveDate) {
	this.effectiveDate = effectiveDate;
}

public Boolean getSuspendExtracted() {
	return suspendExtracted;
}

public void setSuspendExtracted(Boolean suspendExtracted) {
	this.suspendExtracted = suspendExtracted;
}

public TaxCodeCodeHandler getFilterTaxCodeHandler() {
	return filterTaxCodeHandler;
}

public TaxCodeCodeHandler getThenTaxCodeHandler() {
	return thenTaxCodeHandler;
}

public TaxCodeCodeHandler getElseTaxCodeHandler() {
	return elseTaxCodeHandler;
}

public void countrySelected(ActionEvent e) {		
	if(stateMenuItemsDetail!=null) stateMenuItemsDetail.clear();
}

public void stateSelected(ActionEvent e) {
}

@Override
public void setMatrixCommonBean(MatrixCommonBean matrixCommonBean) {
	super.setMatrixCommonBean(matrixCommonBean);
	filterTaxCodeHandler.setTaxCodeDetailService(matrixCommonBean.getTaxCodeDetailService());
	thenTaxCodeHandler.setTaxCodeDetailService(matrixCommonBean.getTaxCodeDetailService());
	elseTaxCodeHandler.setTaxCodeDetailService(matrixCommonBean.getTaxCodeDetailService());
	
	//0004440
	filterTaxCodeHandler.setTaxCodeBackingBean(taxCodeBackingBean);
	filterTaxCodeHandler.setReturnRuleUrl("tax_matrix_sales_main");
	thenTaxCodeHandler.setTaxCodeBackingBean(taxCodeBackingBean);
	thenTaxCodeHandler.setReturnRuleUrl("tax_matrix_sales_details");
	elseTaxCodeHandler.setTaxCodeBackingBean(taxCodeBackingBean);
	elseTaxCodeHandler.setReturnRuleUrl("tax_matrix_sales_details");
	
	filterTaxCodeHandler.setCacheManager(matrixCommonBean.getCacheManager());
	//MF - do not set default country
	//filterTaxCodeHandler.reset();
	//filterTaxCodeHandler.setTaxcodeCountry(getMatrixCommonBean().getUserPreferenceDTO().getUserCountry());
	//filterTaxCodeHandler.setTaxcodeState("");
	//filterTaxCodeHandler.rebuildMenus();
	
	thenTaxCodeHandler.setCacheManager(matrixCommonBean.getCacheManager());
	elseTaxCodeHandler.setCacheManager(matrixCommonBean.getCacheManager());
}

public void relationSelected(ActionEvent e) {
	// Since the controls are not contained within their own
	// form, we need to use immediate=true which means
	// we need to get values from the submittedValue area!!
	String relation = (String) ((HtmlSelectOneMenu) ((HtmlAjaxSupport) e.getComponent()).getParent()).getSubmittedValue();
	selectedMatrix.setRelationSign(relation);
}

public boolean getElseDisabled() {
	String sign = (selectedMatrix == null)? null:selectedMatrix.getRelationSign();
	return ((sign == null) || sign.equals("") || sign.equals("na"));
}

public void willonchange(){
	if(!suspendExtracted){
		deleteMatrixLine = false;
	}
}
// Modified for fixing bug 0003972 - 02/26/2009
public void updateSuspendedCounts(ActionEvent e) {
	logger.debug("Getting suspend counts for id: " + selectedMatrixSource.getId());
	if("".equals(selectedMatrixSource.getEffectiveDate()) || selectedMatrixSource.getEffectiveDate()==null){
		setInvalidDate(true);
					
	}else {
		setInvalidDate(false);
		deleteMatrixLine = false;
		suspendExtracted = false;
		effectiveDate = selectedMatrixSource.getEffectiveDate();

		prossdTrDatailBean = transactionDetailService.getSuspendedTaxMatrixTransactionDetails(
				selectedMatrixSource.getId(), getEffectiveDate());
	}
	
}

public String getThenTaxCodeStateWarning() {
	/*
	String state = selectedMatrix.getMatrixStateCode();
	String country = ((TaxMatrix)selectedMatrix).getMatrixCountryCode();
	
	logger.debug("getThenTaxCodeStateWarning state: " + state);
	
	if((state != null) && state.trim().equals("")){
		state = null;
	}
	
	if((country != null) && country.trim().equals("")){
		country = null;
	}
	
	String thenState = thenTaxCodeHandler.getTaxcodeState();
	String thenCountry = thenTaxCodeHandler.getTaxcodeCountry();
	logger.debug("thenState: " + thenState);
	
	if((thenState!=null) && thenState.trim().equals("")){
		thenState = null;	
	}
	
	if((thenCountry!=null) && thenCountry.trim().equals("")){
		thenCountry = null;	
	}
	
	String returnStr = "";
    if (!((state == thenState) || (state != null && state.equalsIgnoreCase(thenState))) ||
    		!((country == thenCountry) || (country != null && country.equalsIgnoreCase(thenCountry)))) {
    	if(getAddFromTransactionAction()){
    		if(thenState!=null && thenState.equalsIgnoreCase("*ALL")){
    			returnStr = "* Then TaxCode State '*ALL' overrides Transaction State";
    		}
    		else{
    			returnStr = "* Then TaxCode Country/State is different from Transaction State";
    		}
    	}
    	else{
    		returnStr = "* Then TaxCode Country/State is different from Matrix State";
    	}
	}
    */
    return "";
}

public String getElseTaxCodeStateWarning() {
	/*
	String state = selectedMatrix.getMatrixStateCode();
	String country = ((TaxMatrix)selectedMatrix).getMatrixCountryCode();
	
	logger.debug("getElseTaxCodeStateWarning state: " + state);
	
	if((state != null) && state.trim().equals("")){
		state = null;
	}
	
	if((country != null) && country.trim().equals("")){
		country = null;
	}
	
	String elseState = elseTaxCodeHandler.getTaxcodeState();
	String elseCountry = elseTaxCodeHandler.getTaxcodeCountry();

	logger.debug("elseState: " + elseState);
	
	if((elseState!=null) && elseState.trim().equals("")){
		elseState = null;	
	}
	
	if((elseCountry!=null) && elseCountry.trim().equals("")){
		elseCountry = null;	
	}
	
	String returnStr = "";
    if (!getElseDisabled() && (!((state == elseState) || (state != null && state.equalsIgnoreCase(elseState)))) ||
    		!getElseDisabled() && (!((country == elseCountry) || (country != null && country.equalsIgnoreCase(elseCountry))))) {
    	if(getAddFromTransactionAction()){
    		if(elseState!=null && elseState.equalsIgnoreCase("*ALL")){
    			returnStr = "* Else TaxCode State '*ALL' overrides Transaction State";
    		}
    		else{
    			returnStr = "* Else TaxCode Country/State is different from Transaction State";
    		}
    	}
    	else{
    		returnStr = "* Else TaxCode Country/State is different from Matrix State";
    	}
	}
    */
    return "";
}

@Override
public String resetFilter() {
	globalCompanyNumberFilter = MatrixCommonBean.GLOBAL_COMPANY_NUMBER_DEFAULT_VALUE;
	defaultLineFilter = MatrixCommonBean.DEFAULT_LINE_DEFAULT_VALUE;
	activeFilter=MatrixCommonBean.ACTIVE_DEFAULT_VALUE;
	filterTaxCodeHandler.reset();
	//MF - do not set default country
	//filterTaxCodeHandler.setTaxcodeCountry(getMatrixCommonBean().getUserPreferenceDTO().getUserCountry());
	//filterTaxCodeHandler.setTaxcodeState("");
	//filterTaxCodeHandler.rebuildMenus();
	
	if(stateMenuItems!=null) {
		stateMenuItems.clear();
		stateMenuItems = null;
	}
	entityId = null;
	entityCode =null;
	entityCodeFilter = null;
	effectiveDate=null;
	super.resetFilter();
	
	if(stateMenuItems!=null) stateMenuItems.clear();

	return "tax_matrix_sales_main";
}

@Override
protected void updateMatrixFilter(GSBMatrix matrixFilter) {
  	// Setup up Default Line/Global Company Number options
	matrixFilter.setDefaultFlag(
  			defaultLineFilter.equals(MatrixCommonBean.DEFAULT_LINE_ALL_VALUE)? null:defaultLineFilter);
  	
	matrixFilter.setDriverGlobalFlag(
				globalCompanyNumberFilter.equals(MatrixCommonBean.GLOBAL_COMPANY_NUMBER_ALL_VALUE)? null:globalCompanyNumberFilter);
	matrixFilter.setActiveFlag(activeFilter.equals(MatrixCommonBean.ACTIVE_ALL_VALUE)?null:activeFilter);

}

public boolean getIsAddAction() {
	return (currentAction .equals(EditAction.ADD) || currentAction.equals(EditAction.COPY_ADD));
}

@Override
protected void updateTransactionDetailFilter(PurchaseTransaction trDetail) {
}

private void updateTaxcodeDetail(PurchaseTransaction detail) {
	//Default country
	String defaultCountry = "";//(detail != null)? detail.getTransactionCountryCode():filterTaxCodeHandler.getTaxcodeCountry();
	
	//Default state
	String defaultState = "";//(detail != null)? detail.getTransactionStateCode():filterTaxCodeHandler.getTaxcodeState();
	
	String defaultType = "";//(detail != null)? detail.getTaxcodeTypeCode():filterTaxCodeHandler.getTaxcodeType();
	if ((defaultType == null) || defaultType.equals("")) {
		defaultType = getMatrixCommonBean().getUserPreferenceDTO().getUserTaxCode();
	}
	if(detail!=null){
		EntityItem aEntityItem = this.entityItemService.findByCode(detail.getEntityCode());
		if(aEntityItem!=null){
			selectedMatrix.setEntityId(aEntityItem.getEntityId());
		}
		else{
			selectedMatrix.setEntityId(-1L);
		}
	}
	if ((detail == null) && (selectedMatrix != null)) {
	} else {
		if (currentAction .equals(EditAction.ADD)) {
			//default
			TaxCodeDetail tcDetail = new TaxCodeDetail();
			
			if(!defaultCountry.equals("")){
				tcDetail.setTaxcodeCountryCode(defaultCountry);
				tcDetail.setTaxcodeStateCode(defaultState);
			}
			else{
				tcDetail.setTaxcodeCountryCode(getMatrixCommonBean().getUserPreferenceDTO().getUserCountry());
				tcDetail.setTaxcodeStateCode("");
			}
			tcDetail.setTaxcodeTypeCode(defaultType);
			tcDetail.setTaxcodeCode((detail != null)? detail.getTaxcodeCode():filterTaxCodeHandler.getTaxcodeCode());
			thenTaxCodeHandler.setTaxcodeDetail(tcDetail);
		}
		else{
			TaxCodeDetail tcDetail = new TaxCodeDetail();
			tcDetail.setTaxcodeCountryCode("");
			tcDetail.setTaxcodeStateCode("");
			tcDetail.setTaxcodeTypeCode("");
			tcDetail.setTaxcodeCode("");
			thenTaxCodeHandler.setTaxcodeDetail(tcDetail);
		}
	}
		
	if ((detail == null) && (selectedMatrix != null)) {
	} else {
		if (currentAction .equals(EditAction.ADD)) {
			//default
			TaxCodeDetail tcDetail = new TaxCodeDetail();
			
			if(!defaultCountry.equals("")){
				tcDetail.setTaxcodeCountryCode(defaultCountry);
				tcDetail.setTaxcodeStateCode(defaultState);
			}
			else{
				tcDetail.setTaxcodeCountryCode(getMatrixCommonBean().getUserPreferenceDTO().getUserCountry());
				tcDetail.setTaxcodeStateCode("");
			}

			tcDetail.setTaxcodeTypeCode(defaultType);
			tcDetail.setTaxcodeCode((detail != null)? detail.getTaxcodeCode():filterTaxCodeHandler.getTaxcodeCode());
			elseTaxCodeHandler.setTaxcodeDetail(tcDetail);
		}
		else{
			TaxCodeDetail tcDetail = new TaxCodeDetail();
			tcDetail.setTaxcodeCountryCode("");
			tcDetail.setTaxcodeStateCode("");
			tcDetail.setTaxcodeTypeCode("");
			tcDetail.setTaxcodeCode("");
			elseTaxCodeHandler.setTaxcodeDetail(tcDetail);
		}
	}
}

@Override
protected void prepareDetailDisplay(EditAction action, PurchaseTransaction detail) {
	super.prepareDetailDisplay(action, detail);
	// Update the tax code details
	updateTaxcodeDetail(detail);
	if ((selectedMatrix != null) && action.equals(EditAction.ADD)) {			
	  	// Setup up Default Line/Global Company Number options
		if (!defaultLineFilter.equals(MatrixCommonBean.DEFAULT_LINE_ALL_VALUE)) {
			selectedMatrix.setDefaultFlag(defaultLineFilter);
		}
		if (!globalCompanyNumberFilter.equals(MatrixCommonBean.GLOBAL_COMPANY_NUMBER_ALL_VALUE)) {
			selectedMatrix.setDriverGlobalFlag(globalCompanyNumberFilter);
		}
		if (!activeFilter.equals(MatrixCommonBean.ACTIVE_DEFAULT_VALUE)) {
			selectedMatrix.setDriverGlobalFlag(activeFilter);
		}
	}
}

// Modified for fixing bug 0003972 - 02/26/2009
public void displaySuspendAction(ActionEvent e) {
	setInvalidDate(false);
	prossdTrDatailBean = null;
	effectiveDate = null;
	deleteMatrixLine = false;
	suspendExtracted = false;
}

/**
 * This would suspend the records in transaction details
 */
// Modified for fixing bug 0003972 - 02/26/2009
public String suspendAction() { 
	logger.info("#################### "+selectedMatrixSource.getTaxMatrixId());
	logger.info("$$$$$$$$$$$$$$$$$$$$$$$$ "+selectedMatrixSource.getEffectiveDate());
	effectiveDate = selectedMatrixSource.getEffectiveDate();
	transactionDetailService.suspendTaxMatrixTransactions(purchaseTransactionService,
			selectedMatrixSource.getTaxMatrixId(), this.getEffectiveDate(),
			suspendExtracted, true, deleteMatrixLine);
	
	getMatrixDataModel().refreshData(false);
	resetSelection();
	return "tax_matrix_sales_main";
}

@Override
public void displayTransactionsAction(ActionEvent e) {
	boolean valid = validateDrivers(FacesContext.getCurrentInstance(), null);
	
	if(!valid){
		return;
	}
	
	valid = validateCountryState(FacesContext.getCurrentInstance());
	if(!valid){
		return;
	}
	
	//9005
	if(entityCode==null || entityCode.trim().length()==0){
		FacesMessage message = new FacesMessage("Entity is Mandatory. ");
		message.setSeverity(FacesMessage.SEVERITY_ERROR);
		FacesContext.getCurrentInstance().addMessage(null, message);
		return;
	}
	EntityItem aEntityItem = this.entityItemService.findByCode(entityCode.trim());
	if(aEntityItem!=null){
		selectedMatrix.setEntityId(aEntityItem.getEntityId());
	}
	else{
		selectedMatrix.setEntityId(-1L);
	}
			
	getSaleTransactionsAction();
}

protected boolean validateCountryState(FacesContext context){
	boolean valid = true;		
	if ((selectedMatrix.getRelationAmount() == null) && !(getElseDisabled()==true || getUpdateAction()==true || getReadOnlyAction()==true)) {
		FacesMessage message = new FacesMessage("Amount is required. ");
		message.setSeverity(FacesMessage.SEVERITY_ERROR);
		context.addMessage(null, message);
		valid = false;
	}
	
	if (thenTaxCodeHandler.getTaxcodeCode()== null || thenTaxCodeHandler.getTaxcodeCode().equals("")) {
		FacesMessage message = new FacesMessage("Then TaxCode Code is required. ");
		message.setSeverity(FacesMessage.SEVERITY_ERROR);
		context.addMessage(null, message);
		valid = false;
	}
	
	if(getElseDisabled()==false){
		if (elseTaxCodeHandler.getTaxcodeCode()== null || elseTaxCodeHandler.getTaxcodeCode().equals("")) {
			FacesMessage message = new FacesMessage("Else TaxCode Code is required. ");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage(null, message);
			valid = false;
		}
	}
	
	return valid;
}

/** ADD, COPY/ADD, UPDATE **/

@Override
public String saveAction() {
	//Validate Entity Code
	if(entityCode==null || entityCode.trim().length()==0){
		FacesMessage message = new FacesMessage("Entity is Mandatory. ");
		message.setSeverity(FacesMessage.SEVERITY_ERROR);
		FacesContext.getCurrentInstance().addMessage(null, message);
		return null;
	}
	
	EntityItem aEntityItem = this.entityItemService.findByCode(entityCode.trim());
	if(aEntityItem!=null){
		selectedMatrix.setEntityId(aEntityItem.getEntityId());
	}else{
		FacesMessage message = new FacesMessage("Entity is Invalid. ");
		message.setSeverity(FacesMessage.SEVERITY_ERROR);
		FacesContext.getCurrentInstance().addMessage(null, message);
		return null;
	}
		
	boolean valid = validateDrivers(FacesContext.getCurrentInstance(), null);
	
	if(!valid){
		return null;
	}
	
	valid = validateCountryState(FacesContext.getCurrentInstance());
	if(!valid){
		return null;
	}
	
	selectedMatrix.setThenTaxcodeCode(thenTaxCodeHandler.getTaxcodeCode());
	if(getElseDisabled()) {
		selectedMatrix.setElseTaxcodeCode(null);
	}
	else {
		selectedMatrix.setElseTaxcodeCode(elseTaxCodeHandler.getTaxcodeCode());
	}
	
	//0001891: Creating a Tax Matrix allows zero Relation Amount
	if (!getElseDisabled()) {
		if(selectedMatrix.getRelationAmount()==null || selectedMatrix.getRelationAmount()<=0){
			FacesMessage message = new FacesMessage("You must enter a relation amount. ");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
			return null;
		}
	}
	
	if(selectedMatrix.getEntityId()==null || selectedMatrix.getEntityId()==-1L){
		FacesMessage message = new FacesMessage("Entity is mandatory.");
		message.setSeverity(FacesMessage.SEVERITY_ERROR);
		FacesContext.getCurrentInstance().addMessage(null, message);
		return null;
	}
	//Midtier project code modified - january 2009
	//Start of trigger
	Long binaryWeight =super.CalcBinaryWeight();
	String significantDigits = super.CalcSignificantDigits();
	
	//bug 0003803 -- I dunno. This seems like a bit of a hack, I don't understand the 
	//relationship quite between the TaxMatrixViewBean and the BaseMatrixBackingBean, 
	//but it does seem to get the right value there.... essentially it's taking whats 
	//in the bean and ensuring it gets properly represented in the selectedMatrix
	if(getDriverGlobalFlag()){
		selectedMatrix.setDriverGlobalFlag("1");
	} else {
		selectedMatrix.setDriverGlobalFlag("0");
	}

	if ("1".equalsIgnoreCase(selectedMatrix.getDriverGlobalFlag())) {
		Double mult = Math.pow(2,31);
		binaryWeight = binaryWeight + + mult.longValue();
	}
	// Default Flag
	if(selectedMatrix.getDefaultFlag() == null) {
		selectedMatrix.setDefaultFlag("0");
	}
	if(selectedMatrix.getActiveFlag()==null){
		selectedMatrix.setActiveFlag("0");
	}
	if(selectedMatrix.getDefaultFlag() != "1") {
		selectedMatrix.setBinaryWeight(binaryWeight);
		selectedMatrix.setDefaultBinaryWeight(0L);
		
		selectedMatrix.setSignificantDigits(significantDigits);
		selectedMatrix.setDefaultSignificantDigits(null);
	} else {
		selectedMatrix.setBinaryWeight(0L);
		selectedMatrix.setDefaultBinaryWeight(binaryWeight);
		
		selectedMatrix.setSignificantDigits(null);
		selectedMatrix.setDefaultSignificantDigits(significantDigits);
	}

	selectedMatrix.setModuleCode("BILLSALE");
	logger.debug("\n \t binaryWeight by tb_tax_matrix_b_iu : " + selectedMatrix.getBinaryWeight());
	logger.debug("\n \t significantDigits by tb_tax_matrix_b_iu : " + selectedMatrix.getSignificantDigits());
	//End of trigger
	
	if(super.validateDriverSel(getViewPanel())){
		return super.saveAction();
	}else{
		return null;
	}
}

@Override
public String deleteAction() {
	if (currentAction.equals(EditAction.DELETE) && getTransactionDetailService().getGSBMatrixInUse(getSelectedMatrix().getTaxMatrixId())){
		getTransactionDetailService().updateaActiveFlagMatrixInUse(getSelectedMatrix().getTaxMatrixId());	
		return super.refreshAction();
	}
	
	return super.deleteAction();
}

@Override
public void addFromTransaction(PurchaseTransaction detail) {
	super.addFromTransaction(detail);
	
	//Default setting for thenTaxCodeHandler
	thenTaxCodeHandler.reset();
	thenTaxCodeHandler.rebuildMenus();
	
	//Default setting for elseTaxCodeHandler
	elseTaxCodeHandler.reset();
	elseTaxCodeHandler.rebuildMenus();
}

public String getGlobalLineLabel(){
	String label  = "";
	if (matrixCommonBean.getUserGlobalBooleanFlag()) {
		Map<String,DriverNames> transactionDriverNamesMap = getMatrixCommonBean().getCacheManager().getTransactionPropertyDriverNamesMap(GSBMatrix.DRIVER_CODE);
		Map<String,DataDefinitionColumn> transactionPropertyMap = getMatrixCommonBean().getCacheManager().getDataDefinitionPropertyMap("TB_SALETRANS");
		
		for (String property : transactionDriverNamesMap.keySet()) {
			DriverNames driverNames = transactionDriverNamesMap.get(property);
			if(((driverNames.getBusinessUnitFlag() != null) && driverNames.getBusinessUnitFlag().equals("1"))){
				if (transactionPropertyMap.get(property) != null) {
					String abbrDesc = transactionPropertyMap.get(property).getAbbrDesc();	
					label  = "Global " + abbrDesc + ": ";
					break;
				}
			}
		}
	}
	return label;
}
	
public boolean getIsGlobalMandatory(){
	Map<String,DriverNames> transactionDriverNamesMap = getMatrixCommonBean().getCacheManager().getTransactionPropertyDriverNamesMap(GSBMatrix.DRIVER_CODE);
	
	for (String property : transactionDriverNamesMap.keySet()) {
		DriverNames driverNames = transactionDriverNamesMap.get(property);
		if(((driverNames.getBusinessUnitFlag() != null) && driverNames.getBusinessUnitFlag().equals("1")) && 
		   ((driverNames.getMandatoryFlag() != null) && driverNames.getMandatoryFlag().equals("1"))	){
			return true;
		}
	}
	return false;
}

public String displayAddActions(){
	entityCode = entityCodeFilter;
	this.thenTaxCodeHandler.reset();
	this.elseTaxCodeHandler.reset();
	driverGlobalFlagBind.setSelected(false);
	return super.displayAddAction();
}

protected void setDriverGlobalFlagComponent(){
	if(selectedMatrix.getDriverGlobalFlag()!=null && selectedMatrix.getDriverGlobalFlag().equals("1")){
		driverGlobalFlagBind.setSelected(true);
	} else {
		driverGlobalFlagBind.setSelected(false);
	}
}

public String displayCopyAddAction(){	
	String nextPage = super.displayCopyAddAction();
	setDriverGlobalFlagComponent();
	setTaxCodeHandlers();
	
	EntityItem aEntityItem = this.entityItemService.findById(selectedMatrix.getEntityId());
	entityCode = "";
	if(aEntityItem!=null){
		entityCode = aEntityItem.getEntityCode();
	}
	
	return nextPage;
}

public String displayUpdateAction(){	
	String nextPage = super.displayUpdateAction();
	setDriverGlobalFlagComponent();
	setTaxCodeHandlers();
	
	EntityItem aEntityItem = this.entityItemService.findById(selectedMatrix.getEntityId());
	entityCode = "";
	if(aEntityItem!=null){
		entityCode = aEntityItem.getEntityCode();
	}
	
	return nextPage;
}

public String displayDeleteAction(){	
	String nextPage = super.displayDeleteAction();
	setDriverGlobalFlagComponent();
	setTaxCodeHandlers();
	
	EntityItem aEntityItem = this.entityItemService.findById(selectedMatrix.getEntityId());
	entityCode = "";
	if(aEntityItem!=null){
		entityCode = aEntityItem.getEntityCode();
	}
	
	if (currentAction.equals(EditAction.DELETE) && getTransactionDetailService().getGSBMatrixInUse(getSelectedMatrix().getTaxMatrixId())){
		FacesMessage message = new FacesMessage("Matrix used on Transaction(s) and cannot be deleted. Click Ok to set to Inactive.");
		message.setSeverity(FacesMessage.SEVERITY_ERROR);
		FacesContext.getCurrentInstance().addMessage(null, message);	
	}
	
	return nextPage;
}

public String displayViewAction(){	
	String nextPage = super.displayViewAction();
	setDriverGlobalFlagComponent();
	setTaxCodeHandlers();
	
	EntityItem aEntityItem = this.entityItemService.findById(selectedMatrix.getEntityId());
	entityCode = "";
	if(aEntityItem!=null){
		entityCode = aEntityItem.getEntityCode();
	}
	
	return nextPage;
}

private void setTaxCodeHandlers() {
	this.thenTaxCodeHandler.reset();
	this.elseTaxCodeHandler.reset();
	
	if(this.selectedMatrix != null) {
		if(this.selectedMatrix.getThenTaxcodeCode() != null) {
			this.thenTaxCodeHandler.setTaxcodeCode(this.selectedMatrix.getThenTaxcodeCode());
		}
		
		if(this.selectedMatrix.getElseTaxcodeCode() != null) {
			this.elseTaxCodeHandler.setTaxcodeCode(this.selectedMatrix.getElseTaxcodeCode());
		}
	}
}

public HtmlSelectBooleanCheckbox getDriverGlobalFlagBind() {
	return driverGlobalFlagBind;
}

public void setDriverGlobalFlagBind(
		HtmlSelectBooleanCheckbox driverGlobalFlagBind) {
	this.driverGlobalFlagBind = driverGlobalFlagBind;
}

public boolean isInvalidDate() {
	return invalidDate;
}

public void setInvalidDate(boolean invalidDate) {
	this.invalidDate = invalidDate;
}

public boolean getIsEntityCheck() {
	return isEntityCheck;
}

public void setEntityCheck(boolean isEntityCheck) {
this.isEntityCheck = isEntityCheck;
}
public String viewFromTransaction(Long id, String view) {
	String nextPage = super.viewFromTransaction(id, view);
	setTaxCodeHandlers();
	return nextPage;
}
public loginBean getLoginBean() {
	return loginBean;
}
public void setLoginBean(loginBean loginBean) {
	this.loginBean = loginBean;
}
public Map<Long, String> getEntityMap() {
	if (entityMap == null) {
		entityMap = new LinkedHashMap<Long, String>();
		
		List<EntityItem> eList = entityItemService.findAllEntityItems();//source list   
	    if(eList != null && eList.size() > 0){
	    	for(EntityItem entityItem:eList){
	    		entityMap.put(entityItem.getEntityId(),entityItem.getEntityCode() + " - " + entityItem.getEntityName());
	    	}
	    }
	}
	return entityMap;
}
public Map<String,TaxCode> getTaxCodeMap() {
	return cacheManager.getTaxCodeMap();
}
public String closeAction(){
	
	return "entity_main";

}
public List<SelectItem> getEntityList(){
    	if(entityList==null){
    		entityList = new ArrayList<SelectItem>();
    		entityList.add(new SelectItem(new Long(-1L),"Select Entity"));
    		List<EntityItem> eList = entityItemService.findAllEntityItemsBylockflag();//source list   
    	    if(eList != null && eList.size() > 0){
    	    	for(EntityItem entityItem:eList){
    	    		if(entityItem.getActiveBooleanFlag()){
    	    			entityList.add(new SelectItem(entityItem.getEntityId(),entityItem.getEntityCode() + " - " + entityItem.getEntityName()));
    	    		}
    	    	}
    	    } 		
    	}
    	return entityList;
    }

/*public List<SelectItem> getEntityListItems(){
	if(entityListItems==null){
		entityListItems = new ArrayList<SelectItem>();
		entityListItems.add(new SelectItem(new Long(-1L),"Select Entity"));
		List<EntityItem> eList = entityItemService.findAllEntityItemsBylockflag();//source list   
		   if(eList != null && eList.size() > 0){
	    	for(EntityItem entityItem:eList){
	    		//if(entityItem.getActiveBooleanFlag()){
	    			entityListItems.add(new SelectItem(entityItem.getEntityId(),entityItem.getEntityCode() + " - " + entityItem.getEntityName()));
	    		//}
	    	}
	    } 		
	}
	return entityListItems;
}*/

public Long getEntityId() {
	return entityId;
}
public void setEntityId(Long entityId) {
	this.entityId = entityId;
}

public String getEntityCode() {
	return entityCode;
}
public void setEntityCode(String entityCode) {
	this.entityCode = entityCode;
}
public String getEntityCodeFilter() {
	return entityCodeFilter;
}
public void setEntityCodeFilter(String entityCodeFilter) {
   	this.entityCodeFilter = entityCodeFilter;
}
public CommonCallBack getCommonCallBack() {
	return commonCallBack;
}
public void setCommonCallBack(CommonCallBack commonCallBack) {
	this.commonCallBack = commonCallBack;
}

	@Override
	public void searchDriver(ActionEvent e) {
		String id = (String) e.getComponent().getAttributes().get("ID");
		super.searchDriver(e);
		if(id.equalsIgnoreCase("filterPanel")) {
			getDriverHandlerBean().setCallBackScreen("tax_matrix_sales_main");// same as tax_matrix_main
		}
		else{
			getDriverHandlerBean().setCallBackScreen("tax_matrix_sales_details");//same as tax_matrix_detail
		}		
	}
}
