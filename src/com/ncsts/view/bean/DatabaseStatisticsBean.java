package com.ncsts.view.bean;


import java.io.IOException;
import java.lang.reflect.Method;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;

import javax.faces.application.FacesMessage;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlPanelGrid;
import javax.faces.context.FacesContext;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;

import org.ajax4jsf.component.html.HtmlAjaxSupport;
import org.apache.log4j.Logger;
import org.apache.myfaces.component.html.ext.HtmlOutputText;
import org.richfaces.component.html.HtmlColumn;
import org.richfaces.component.html.HtmlDataTable;
import org.springframework.beans.factory.annotation.Autowired;

import com.ncsts.common.CacheManager;
import com.ncsts.common.Util;
import com.ncsts.domain.DataDefinitionColumn;
import com.ncsts.domain.DriverNames;
import com.ncsts.domain.DwFilter;
import com.ncsts.domain.DwFilterPK;
import com.ncsts.domain.Jurisdiction;
import com.ncsts.domain.LocationMatrix;
import com.ncsts.domain.PurchaseTransaction;
import com.ncsts.domain.TaxMatrix;
import com.ncsts.domain.TempLocationMatrixCount;
import com.ncsts.domain.TempStatistics;
import com.ncsts.domain.TempTaxMatrixCount;
import com.ncsts.domain.TransactionBlob;
import com.ncsts.dto.DatabaseTransactionStatisticsDTO;
import com.ncsts.dto.DriverNamesDTO;
import com.ncsts.dto.EntityItemDTO;
import com.ncsts.dto.LocationMatrixTransactionStatiticsDTO;
import com.ncsts.dto.TaxMatrixTransactionStatiticsDTO;
import com.ncsts.dto.TransactionDetailDTO;
import com.ncsts.jsf.model.StatisticsDataModel;
import com.ncsts.services.BatchMaintenanceService;
import com.ncsts.services.DatabaseStatisticsService;
import com.ncsts.services.DwFilterService;
import com.ncsts.services.JurisdictionService;
import com.ncsts.services.TransactionDetailService;
import com.ncsts.services.UserEntityService;
import com.ncsts.view.util.JsfHelper;
import com.ncsts.view.util.StateObservable;
import com.ncsts.view.util.TogglePanelController;

public class DatabaseStatisticsBean implements BaseMatrixBackingBean.FindIdCallback, ViewTaxCodeBean.TaxCodeInfoCallback ,
		Observer, HttpSessionBindingListener{

	private Logger logger = Logger.getLogger(DatabaseStatisticsBean.class);
	
	@Autowired
	private TaxCodeBackingBean taxCodeBackingBean;
	
	private StateObservable ov = null;

	@SuppressWarnings("unused")
	private static final int ORA_ERROR= 54;

	private HtmlPanelGrid filterSelectionPanel;
	// Selection filter inputs
	private TaxCodeCodeHandler filterTaxCodeHandler;// = new TaxCodeSearchHandler();
	private SearchJurisdictionHandler filterHandler = new SearchJurisdictionHandler();
	private SearchBatchIdHandler batchIdHandler = new SearchBatchIdHandler();

	private TaxMatrix filterSelectionTaxMatrix = new TaxMatrix();
	private LocationMatrix filterSelectionLocationMatrix = new LocationMatrix();

	private DatabaseStatisticsService databaseStatisticsService;
	private TransactionDetailService transactionDetailService;
	private BatchMaintenanceService batchMaintenanceService;
	private UserEntityService userEntityService;

	private List<DatabaseTransactionStatisticsDTO> tempStatisticsData = null;
	private DatabaseTransactionStatisticsDTO selectedDatabaseTransactionStatisticsDTO = null;
	private DatabaseTransactionStatisticsDTO selectedDrillDownStatistics = null;
	private TaxMatrixTransactionStatiticsDTO taxMatrixTransactionStatiticsDTO = null;
	private LocationMatrixTransactionStatiticsDTO locationMatrixTransactionStatiticsDTO = null;
	private List<TransactionDetailDTO> trDetailViewList = null;
	private List<DriverNamesDTO> taxmatrixDrivers = null;
	private List<DriverNamesDTO> locationmatrixDrivers = null;
	private BatchMaintenanceBackingBean batchMaintenanceBean;
	private TaxJurisdictionBackingBean taxJurisdictionBean;

	private MatrixCommonBean matrixCommonBean;
	private Date fromGLDate;
	private Date toGLDate;
	private Long taxMatrixId;
	private Long batchNumber;
	private Long locationMatrixId;

	private PurchaseTransaction currentTransaction;
	private JurisdictionService jurisdictionService; // this service is declared to get the Jurisdiction details
	private Jurisdiction currentJurisdiction;
	private TransactionViewBean transactionViewBean;
	private LocationMatrixBackingBean locationMatrixBean;
	private TaxMatrixViewBean taxMatrixBean;
	private TransactionBlobBean transactionBlobBean;

	private boolean isValid = false;
	private boolean viewDrillDown = false;

	private int selectedRowTrIndex = -1;
	private int selectedRowIndex = -1;
	private int selectedRowDrillDownIndex = -1;
	private String whereClauseforSearch;
	private String andClauseTaxMatrix;
	private String andClauseLocationMatrix;
	private HtmlDataTable trDetailTable;
	private String statusMessage = null;
	private PurchaseTransaction trDrillDownExample;
	private StatisticsDataModel statisticsDataModel;
	private List<DatabaseTransactionStatisticsDTO> drillDownList;
	private HtmlDataTable trDrillDownTable;
	private List<String> nullField;
	private String callingDBStatsViewPage = "";
	
	private String selectedCountry = "";
	private String selectedState = "";

	private ViewTaxCodeBean viewTaxCodeBean;
	
	private static final String DEFAULT_DISPLAY = "tableTransactions";
	private String selectedDisplay = DEFAULT_DISPLAY;
	private static final String TABLE_TAX_MATRIX = "tableTaxMatrix";
	private static final String LOCATION_MATRIX = "locationMatrix";
	
	@Autowired
	private DriverHandlerBean driverHandlerBean;
	
	@Autowired
	private TransactionDetailBean transactionDetailBean;
	
	private HtmlPanelGrid filterTaxDriverSelectionPanel; 
	private HtmlPanelGrid filterLocDriverSelectionPanel;
	private List<SelectItem> filterItems;
	private String selectedFilter = "";
	@Autowired
	private DwFilterService dwFilterService;
	private TogglePanelController togglePanelController = null;
	private Long processBatchId;
	private  List<DwFilter> dwFilters;
	
	@Autowired
	private SelectionFilterBean selectionFilterBean;
	
	private AdvancedFilterBean advancedFilterBean; 
	
	private static final String ADVANCE_FILTER_DBSTATITCIS_SCREEN = "dbstatitics";
	
	private static String TRANSACTION_STATISTICS_CALLING_SCREEN = "transactionStatitics";
	
	private String selectedUserName;
	private String selectedFilterName = "";
	
	public AdvancedFilterBean getAdvancedFilterBean() {
		return advancedFilterBean;
	}

	public void setAdvancedFilterBean(AdvancedFilterBean advancedFilterBean) {
		this.advancedFilterBean = advancedFilterBean;
	}

	public Long getProcessBatchId() {
		return processBatchId;
	}

	public void setProcessBatchId(Long processBatchId) {
		this.processBatchId = processBatchId;
	}

	public String getSelectedFilter() {
		return selectedFilter;
	}

	public void setSelectedFilter(String selectedFilter) {
		this.selectedFilter = selectedFilter;
	}
	
	
	
	public TogglePanelController getTogglePanelController() {
		if (togglePanelController == null) {
			togglePanelController = new TogglePanelController();
			togglePanelController.addTogglePanel("dbStatsmiscInformation", true);
			togglePanelController.addTogglePanel("dbStatstaxDriversInformation", false);
			togglePanelController.addTogglePanel("dbStatslocationDriversInformation", false);
		}
		return togglePanelController;
	}
	
	public TaxJurisdictionBackingBean getTaxJurisdictionBean() {
		return taxJurisdictionBean;
	}

	public void setTaxJurisdictionBean(TaxJurisdictionBackingBean taxJurisdictionBean) {
		this.taxJurisdictionBean = taxJurisdictionBean;
	}
	
	public String getSelectedDisplay() {
		return selectedDisplay;
	}

	public void setSelectedDisplay(String selectedDisplay) {
		this.selectedDisplay = selectedDisplay;
	}

	public void displaySelectionChanged(ValueChangeEvent e) {
		this.selectedDisplay = (String)e.getNewValue();
	}

	public DatabaseStatisticsBean() {
		filterTaxCodeHandler = new TaxCodeCodeHandler("stats_main");
	}

	public ViewTaxCodeBean getViewTaxCodeBean() {
		return viewTaxCodeBean;
	}

	public void setViewTaxCodeBean(ViewTaxCodeBean viewTaxCodeBean) {
		this.viewTaxCodeBean = viewTaxCodeBean;
	}
	
	public TransactionBlobBean getTransactionBlobBean() {
		return transactionBlobBean;
	}

	public void setTransactionBlobBean(
			TransactionBlobBean transactionBlobBean) {
		this.transactionBlobBean = transactionBlobBean;
	}

	public void searchTaxCodeInfoCallback(String taxcodeCountry, String taxcodeState, String taxcodeType, String taxcodeCode, String callingPage){
		viewTaxCodeBean.reset();
		viewTaxCodeBean.setTaxCode(taxcodeCountry, taxcodeState, taxcodeType, taxcodeCode);
		viewTaxCodeBean.setCallingPage(callingPage);
	}

	public LocationMatrixTransactionStatiticsDTO getLocationMatrixTransactionStatiticsDTO() {
		return locationMatrixTransactionStatiticsDTO;
	}

	public void setLocationMatrixTransactionStatiticsDTO(
			LocationMatrixTransactionStatiticsDTO locationMatrixTransactionStatiticsDTO) {
		this.locationMatrixTransactionStatiticsDTO = locationMatrixTransactionStatiticsDTO;
	}

	public TaxMatrixTransactionStatiticsDTO getTaxMatrixTransactionStatiticsDTO() {
		return taxMatrixTransactionStatiticsDTO;
	}

	public void setTaxMatrixTransactionStatiticsDTO(
			TaxMatrixTransactionStatiticsDTO taxMatrixTransactionStatiticsDTO) {
		this.taxMatrixTransactionStatiticsDTO = taxMatrixTransactionStatiticsDTO;
	}

	public BatchMaintenanceService getBatchMaintenanceService() {
		return batchMaintenanceService;
	}

	public void setBatchMaintenanceService(BatchMaintenanceService batchMaintenanceService) {
		this.batchMaintenanceService = batchMaintenanceService;
		batchIdHandler.setBatchMaintenanceService(batchMaintenanceService);
	}

	public DatabaseStatisticsService getDatabaseStatisticsService() {
		return databaseStatisticsService;
	}
	
	public BatchMaintenanceBackingBean  getBatchMaintenanceBean() {
		return batchMaintenanceBean;
	}

	public void setBatchMaintenanceBean(BatchMaintenanceBackingBean  batchMaintenanceBean) {
		this.batchMaintenanceBean = batchMaintenanceBean;
	}
	
	public UserEntityService getUserEntityService() {
		return this.userEntityService;
	}

	public void setUserEntityService(UserEntityService userEntityService) {
		this.userEntityService = userEntityService;
	}

	public TaxMatrix getFilterSelectionTaxMatrix() {
		return filterSelectionTaxMatrix;
	}


	public LocationMatrix getFilterSelectionLocationMatrix() {
		return filterSelectionLocationMatrix;
	}

	public EntityItemDTO getEntityItemDTO() {
		return userEntityService.getEntityItemDTO();
	}
	
	public List<SelectItem> getFilterItems() {
		List<DwFilter> dwFilterItems= dwFilterService.getFilterNamesByUserCode("TB_PURCHTRANS", matrixCommonBean.getLoginBean().getUserDTO().getUserCode());
		
		filterItems = new ArrayList<SelectItem>();
		filterItems.add(new SelectItem("", "*NOFILTER"));
		
		for(DwFilter dwFilter : dwFilterItems){
			filterItems.add(new SelectItem(dwFilter.getUserCode() + "/" + dwFilter.getFilterName(), dwFilter.getUserCode() + "/" + dwFilter.getFilterName()));
		}
		return filterItems;
	}
	
	public void setFilterItems(List<SelectItem> filterItems) {
		this.filterItems = filterItems;
	}

	public HtmlPanelGrid getFilterTaxDriverSelectionPanel() {

		filterTaxDriverSelectionPanel = MatrixCommonBean.createDriverPanel(
					TaxMatrix.DRIVER_CODE, matrixCommonBean.getCacheManager(), 
					matrixCommonBean.getUserEntityService().getEntityItemDTO(),
					"filterPanel", 
					"databaseStatisticsBean", "filterSelectionTaxMatrix", "matrixCommonBean", 
					false, false, false, true, true); 
				matrixCommonBean.prepareTaxDriverPanel(filterTaxDriverSelectionPanel);
		return filterTaxDriverSelectionPanel;
	}

	public void setFilterTaxDriverSelectionPanel(
			HtmlPanelGrid filterTaxDriverSelectionPanel) {
		this.filterTaxDriverSelectionPanel = filterTaxDriverSelectionPanel;
	}

	public HtmlPanelGrid getFilterLocDriverSelectionPanel() {
		filterLocDriverSelectionPanel = MatrixCommonBean.createDriverPanel(
				LocationMatrix.DRIVER_CODE, matrixCommonBean.getCacheManager(), 
					matrixCommonBean.getUserEntityService().getEntityItemDTO(),
					"filterPanel", 
					"databaseStatisticsBean", "filterSelectionLocationMatrix", "matrixCommonBean", 
					false, false, false, true, true); 
		matrixCommonBean.prepareTaxDriverPanel(filterLocDriverSelectionPanel);
		return filterLocDriverSelectionPanel;
	}
	
	public void setFilterLocDriverSelectionPanel(
			HtmlPanelGrid filterLocDriverSelectionPanel) {
		this.filterLocDriverSelectionPanel = filterLocDriverSelectionPanel;
	}


	public void selectedTransactionChanged (ActionEvent e) throws AbortProcessingException {
		logger.info("transaction table selection event fired.");
	    HtmlDataTable table = (HtmlDataTable) (((HtmlAjaxSupport) e.getComponent()).getParent());
	    PurchaseTransaction selectedTransaction = (PurchaseTransaction) table.getRowData();
	    selectedRowTrIndex = table.getRowIndex();

	    currentTransaction = transactionDetailService.findById(selectedTransaction.getPurchtransId());
		Long id = currentTransaction.getShiptoJurisdictionId();
		if(id != null)
			currentJurisdiction = jurisdictionService.findById(id);

		if (currentJurisdiction == null)
			currentJurisdiction = new Jurisdiction();

		this.setIsValid(true);
		logger.debug("selectedTransaction " + currentTransaction.getPurchtransId());
	}


	public Date getFromGLDate() {
		return fromGLDate;
	}

	public void setFromGLDate(Date fromGLDate) {
		this.fromGLDate = fromGLDate;
	}

	public Date getToGLDate() {
		return toGLDate;
	}

	public void setToGLDate(Date toGLDate) {
		this.toGLDate = toGLDate;
	}

	public Long getTaxMatrixId() {
		return taxMatrixId;
	}

	public void setTaxMatrixId(Long taxMatrixId) {
		this.taxMatrixId = taxMatrixId;
	}

	public Long getBatchNumber() {
		return batchNumber;
	}

	public void setBatchNumber(Long batchNumber) {
		this.batchNumber = batchNumber;
	}

	public Long getLocationMatrixId() {
		return locationMatrixId;
	}

	public void setLocationMatrixId(Long locationMatrixId) {
		this.locationMatrixId = locationMatrixId;
	}
	public LocationMatrixBackingBean getLocationMatrixBean() {
		return locationMatrixBean;
	}

	public void setLocationMatrixBean(LocationMatrixBackingBean locationMatrixBean) {
		this.locationMatrixBean = locationMatrixBean;
	}

	public TaxMatrixViewBean getTaxMatrixBean() {
		return taxMatrixBean;
	}

	public void setTaxMatrixBean(TaxMatrixViewBean taxMatrixBean) {
		this.taxMatrixBean = taxMatrixBean;
	}

	public int getSelectedRowTrIndex() {
		return selectedRowTrIndex;
	}

	public int getSelectedRowIndex() {
		return selectedRowIndex;
	}

	public int getSelectedRowDrillDownIndex() {
		return selectedRowDrillDownIndex;
	}

	public void setSelectedRowDrillDownIndex(int selectedRowDrillDownIndex) {
		this.selectedRowDrillDownIndex = selectedRowDrillDownIndex;
	}

	public void setSelectedRowTrIndex(int selectedRowTrIndex) {
		this.selectedRowTrIndex = selectedRowTrIndex;
	}

	public void setSelectedRowIndex(int selectedRowIndex) {
		this.selectedRowIndex = selectedRowIndex;
	}

	public PurchaseTransaction getCurrentTransaction() {
		return currentTransaction;
	}

	public void setCurrentTransaction(PurchaseTransaction currentTransaction) {
		this.currentTransaction = currentTransaction;
	}

	public StatisticsDataModel getStatisticsDataModel() {
		return statisticsDataModel;
	}

	public void setStatisticsDataModel(StatisticsDataModel statisticsDataModel) {
		this.statisticsDataModel = statisticsDataModel;
		this.statisticsDataModel.setPageSize(50);
		this.statisticsDataModel.setDbFetchMultiple(10);
	}

	public HtmlDataTable getTrDetailTable() {
		if (trDetailTable == null) {
			trDetailTable = new HtmlDataTable();
			trDetailTable.setId("aMDetailList");
			trDetailTable.setVar("trdetail");
			matrixCommonBean.createTransactionStatisticsTable(trDetailTable,
					"databaseStatisticsBean", "databaseStatisticsBean.statisticsDataModel",
					matrixCommonBean.getTransactionPropertyMap(),false);
		}
		return trDetailTable;
	}

	public void setTrDetailTable(HtmlDataTable trDetailTable) {
		this.trDetailTable = trDetailTable;
	}

	public Boolean getMultiSelect() {
		return false;
	}

	public boolean getIsValid() {
		return isValid;
	}
	
	public boolean getIsTempNull() {
		return (tempStatisticsData == null || TABLE_TAX_MATRIX.equals(this.selectedDisplay) || LOCATION_MATRIX.equals(this.selectedDisplay));
	}

	public void setIsValid(boolean isValid) {
		this.isValid = isValid;
	}

	public String getStatusMessage() {
		return statusMessage;
	}

	private void setStatusMessage(String statusMessage) {
		this.statusMessage  = statusMessage;
	}


	public boolean isViewDrillDown() {
		return viewDrillDown;
	}

	public void setViewDrillDown(boolean viewDrillDown) {
		this.viewDrillDown = viewDrillDown;
	}

	public MatrixCommonBean getMatrixCommonBean() {
		return matrixCommonBean;
	}


	public SearchBatchIdHandler getBatchIdHandler() {
		return batchIdHandler;
	}


	public TaxCodeCodeHandler getFilterTaxCodeHandler() {
		return filterTaxCodeHandler;
	}

	public SearchJurisdictionHandler getFilterHandler() {
		return filterHandler;
	}

	public void setMatrixCommonBean(MatrixCommonBean matrixCommonBean) {
		this.matrixCommonBean = matrixCommonBean;
		filterTaxCodeHandler.setTaxCodeDetailService(matrixCommonBean.getTaxCodeDetailService());
		filterTaxCodeHandler.setCacheManager(matrixCommonBean.getCacheManager());
		
		//0004440
		filterTaxCodeHandler.setTaxCodeBackingBean(taxCodeBackingBean);
		filterTaxCodeHandler.setReturnRuleUrl("transactions_db_stats"); 
		
		//MF - do not set default country
		//filterTaxCodeHandler.reset();
		//filterTaxCodeHandler.setTaxcodeCountry(getMatrixCommonBean().getUserPreferenceDTO().getUserCountry());
		//filterTaxCodeHandler.setTaxcodeState("");
		//filterTaxCodeHandler.rebuildMenus();
		

		filterHandler.setJurisdictionService(matrixCommonBean.getJurisdictionService());
		filterHandler.setCacheManager(matrixCommonBean.getCacheManager());
		filterHandler.setTaxJurisdictionBean(taxJurisdictionBean);
		filterHandler.setCallbackScreen("transactions_db_stats");
		
		//MF - do not set default country
		//filterHandler.reset();
		//filterHandler.setCountry(getMatrixCommonBean().getUserPreferenceDTO().getUserCountry());
		//filterHandler.setState("");
		

		batchIdHandler.setCacheManager(matrixCommonBean.getCacheManager());
	}
	
	public void update(Observable obs, Object obj) {
		if (obs == ov) {
			filterHandler.setChanged();
		}
	}
	
	public void valueBound(HttpSessionBindingEvent event){
		StateObservable ov = StateObservable.getInstance();
		this.ov = ov;
		this.ov.addObserver(this);
	}

	public void valueUnbound(HttpSessionBindingEvent event){	
		if(ov!=null){
			ov.deleteObserver(this);
		}
	}

	public void searchDriver(ActionEvent e) {
		String driverCode = (String) e.getComponent().getAttributes().get("DRIVER_CODE");
		driverHandlerBean.initSearch(
				(driverCode.equals(TaxMatrix.DRIVER_CODE)? filterSelectionTaxMatrix:filterSelectionLocationMatrix),
				(HtmlInputText) e.getComponent().getParent().getChildren().get(0), e.getComponent());
		driverHandlerBean.setCallBackScreen("transactions_db_stats");
	}

	public void batchIdSelectedListener(ActionEvent e) {
		batchNumber = batchIdHandler.getSelectedItemKey();
	}

	public String findTaxMatrixIdAction() {
		taxMatrixBean.findId(this, "taxMatrixId", "stats_main");
		return "tax_matrix_main";
	}

	public String findLocationMatrixIdAction() {
		locationMatrixBean.findId(this, "locationMatrixId", "stats_main");
		return "location_matrix_main";
	}
	
	public String findProcessBatchIdAction() {
		batchMaintenanceBean.findId(this, "batchNumber", "transactions_db_stats", "IP");
		return "data_utility_batch";
	}

	public void findIdCallback(Long id, String context) {
		if (context.equalsIgnoreCase("taxMatrixId")) {
			taxMatrixId = id;
		} else if (context.equalsIgnoreCase("locationMatrixId")) {
			locationMatrixId = id;
		} else if (context.equalsIgnoreCase("batchNumber")) {
			batchNumber = id;
			processBatchId = batchNumber;
		}
	}

	public String viewDbTrStatisticsAction() {
		this.setIsValid(false);
		selectedRowTrIndex = -1;
		String whereQuery = selectedDatabaseTransactionStatisticsDTO.getWhereClause();
		logger.debug("Inside View whereQuery " + whereQuery);
		statisticsDataModel.setFilterCriteria(new PurchaseTransaction(), whereQuery, new ArrayList<String>());
		callingDBStatsViewPage  = "transactionStatus_main";
		return "viewDbTrStatisticsAction";

	}

	public void selectedDrillDownRowChanged(ActionEvent event) throws AbortProcessingException {
		logger.info("enter selectedDrillDownRowChanged");
		HtmlDataTable table = (HtmlDataTable) (((HtmlAjaxSupport) event.getComponent()).getParent());
		selectedDrillDownStatistics = (DatabaseTransactionStatisticsDTO) table.getRowData();
		selectedRowDrillDownIndex = table.getRowIndex();
		nullField = new ArrayList<String>();

		String groupBy = selectedDatabaseTransactionStatisticsDTO.getGroupByOnDrilldown();
		String whereClause = selectedDatabaseTransactionStatisticsDTO.getWhereClause();
		trDrillDownExample = new PurchaseTransaction();

		StringBuffer addToWhere = new StringBuffer("");
		String[] columns =  groupBy.split(",");
		int j = 0;
		try{
		for(String str : columns){
			Method m = PurchaseTransaction.class.getMethod("get" + Util.upperCaseFirst(Util.columnToProperty(str)), new Class[] { });
			String[] arry = selectedDrillDownStatistics.getDynamicCols().toArray(new String[] { });
			if (m.getReturnType() == String.class)
					Util.setProperty(trDrillDownExample, Util
							.columnToProperty(str), arry[j], m.getReturnType());
				else if (m.getReturnType() == Long.class)
					Util.setProperty(trDrillDownExample, Util
							.columnToProperty(str), Long.valueOf(arry[j]), m
							.getReturnType());
				else if (m.getReturnType() == Date.class){
					SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSSSS");
					Util.setProperty(trDrillDownExample, Util
							.columnToProperty(str), df.parse(arry[j]), m
							.getReturnType());
				}else if (m.getReturnType() == Float.class)
					Util.setProperty(trDrillDownExample, Util
							.columnToProperty(str), Float.valueOf(arry[j]), m
							.getReturnType());
			//for View
			if(arry[j] == null)
				nullField.add(Util.columnToProperty(str));
			//for SaveAs
			if(arry[j] == null){
				addToWhere.append(str + " is null");
			}else{
				addToWhere.append(str + " = ");
				if( m.getReturnType() == String.class || m.getReturnType() == Date.class ||  m.getReturnType() == Character.class){
					addToWhere.append("'"+arry[j]+"'");
				}else{
					addToWhere.append(arry[j]);
				}
			}
			j++;
			if(j< columns.length){
				addToWhere.append(" AND ");
			}
		}
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		}catch (SecurityException e) {
			e.printStackTrace();
		}catch (ParseException e) {
			e.printStackTrace();//for Date conversion
		}
		
		if ((whereClause!= null) && !whereClause.trim().equals("")) {
			if (addToWhere.length()>0) {
				whereClause = whereClause.concat(" AND ");
		whereClause = whereClause.concat(addToWhere.toString());
			}
		}
		else{
			if (addToWhere.length()>0) {
				whereClause = "";
				whereClause = whereClause.concat(" WHERE ");
				whereClause = whereClause.concat(addToWhere.toString());
			}	
		}

		//for SaveAs
		selectedDrillDownStatistics.setWhereClause(whereClause);
		logger.debug("query for DD : "+ whereClause);
		this.setIsValid(true);
	}

	public String viewFromDrillDown(){
		String whereClause = selectedDatabaseTransactionStatisticsDTO.getWhereClause();
		statisticsDataModel.setFilterCriteria(trDrillDownExample, whereClause, nullField);
		selectedRowTrIndex = -1;
		callingDBStatsViewPage  = "viewDrillDownAction";
		this.setIsValid(false); 
		return "viewDbTrStatisticsAction";
	}

	public String drillDownStatisticsAction() {
		this.setSelectedRowDrillDownIndex(-1);
		this.setSelectedDrillDownStatistics(null);
		trDrillDownTable = new HtmlDataTable();
		trDrillDownTable.setId("ddDetailList");
		trDrillDownTable.setVar("trStsdetail");

		String groupBy = selectedDatabaseTransactionStatisticsDTO.getGroupByOnDrilldown();
		String whereClause = selectedDatabaseTransactionStatisticsDTO.getWhereClause();
		drillDownList = databaseStatisticsService.getStatisticsOnGroupBy(groupBy, whereClause);
		//Add dynamic groupBy columns
		String[] columns =  groupBy.split(",");
		int j = 0;
		for(String str : columns){

			HtmlColumn col = new HtmlColumn();
			col.setId("id_" + str);

			HtmlOutputText header = new HtmlOutputText();
			header.setValue(str.toUpperCase());
			col.getFacets().put("header", header);
			trDrillDownTable.getChildren().add(col);

			HtmlOutputText o = new HtmlOutputText();
			o.setValueExpression("value", JsfHelper.createValueExpression("#{trStsdetail.dynamicCols[" + j + "]}", String.class));
			col.getChildren().add(o);
			trDrillDownTable.getChildren().add(col);

			j++;
		}
		return "viewDrillDownAction";
	}

	public HtmlDataTable getTrDrillDownTable() {
		return trDrillDownTable;
	}

	public void setTrDrillDownTable(HtmlDataTable trDrillDownTable) {
		this.trDrillDownTable = trDrillDownTable;
	}

	public List<DatabaseTransactionStatisticsDTO> getDrillDownList() {
		return drillDownList;
	}

	public void setDrillDownList(
			List<DatabaseTransactionStatisticsDTO> drillDownList) {
		this.drillDownList = drillDownList;
	}

	public DatabaseTransactionStatisticsDTO getSelectedDrillDownStatistics() {
		return selectedDrillDownStatistics;
	}

	public void setSelectedDrillDownStatistics(
			DatabaseTransactionStatisticsDTO selectedDrillDownStatistics) {
		this.selectedDrillDownStatistics = selectedDrillDownStatistics;
	}

	public void setDatabaseStatisticsService(
			DatabaseStatisticsService databaseStatisticsService) {
		this.databaseStatisticsService = databaseStatisticsService;
	}

	public JurisdictionService getJurisdictionService() {
		return jurisdictionService;
	}

	public void setJurisdictionService(JurisdictionService jurisdictionService) {
		this.jurisdictionService = jurisdictionService;
	}

	public Jurisdiction getCurrentJurisdiction() {
		return currentJurisdiction;
	}

	public void setCurrentJurisdiction(Jurisdiction currentJurisdiction) {
		this.currentJurisdiction = currentJurisdiction;
	}



	 public void getCount(String newView) {
	        logger.info("event selected ==" + newView);

	    	CacheManager cacheManager = matrixCommonBean.getCacheManager();
	    	Map<String,DriverNames> transactionDriverNamesMap = cacheManager.getTransactionPropertyDriverNamesMap(newView);
	    	Map<String,DataDefinitionColumn> transactionPropertyMap = cacheManager.getDataDefinitionPropertyMap("TB_PURCHTRANS");
	    	Long count = 0l;
	    	if("T".equalsIgnoreCase(newView)){
		    	taxmatrixDrivers = new ArrayList<DriverNamesDTO>();
		    	TempTaxMatrixCount tempTaxMatrixCount = taxMatrixTransactionStatiticsDTO.getTempTaxMatrixCount();
				for (String transProperty : transactionDriverNamesMap.keySet()) {
					DriverNamesDTO dto = new DriverNamesDTO();
					if (transactionPropertyMap.get(transProperty) != null) {
						String description = transactionPropertyMap.get(transProperty).getAbbrDesc() + " : ";
						DriverNames driverNames = transactionDriverNamesMap.get(transProperty);
						String driver = driverNames.getMatrixColName();
						count = (Long)Util.getProperty(tempTaxMatrixCount, Util.columnToProperty(driver));
						dto.setDescription(description);
						dto.setDriverCount(count);
						taxmatrixDrivers.add(dto);
					}
				}
	    	}
	    	else if("L".equalsIgnoreCase(newView)){
				locationmatrixDrivers = new ArrayList<DriverNamesDTO>();
				TempLocationMatrixCount tempLocationMatrixCount = locationMatrixTransactionStatiticsDTO.getTempLocationMatrixCount();
				for (String transProperty : transactionDriverNamesMap.keySet()) {
					DriverNamesDTO dto = new DriverNamesDTO();
					if (transactionPropertyMap.get(transProperty) != null) {
						String description = transactionPropertyMap.get(transProperty).getAbbrDesc() + " : ";
						DriverNames driverNames = transactionDriverNamesMap.get(transProperty);
						String driver = driverNames.getMatrixColName();
						if(tempLocationMatrixCount != null) {
							count = (Long)Util.getProperty(tempLocationMatrixCount, Util.columnToProperty(driver));
						}
						dto.setDescription(description);
						dto.setDriverCount(count);
						locationmatrixDrivers.add(dto);
					}
				}
			}
	 }

	/**
	 * On click of button Search this function will be called
	 */
	public void retriveAll(ActionEvent event) throws AbortProcessingException {

		this.setStatusMessage("Search will take a few minutes.  Please come back to this page without exiting this session if you would like to review your search results.");

		if(processModifyLookupQuery()){//though this always seems to be true...
			//it seems to be ok that sometimes these are blank, fo the full search
			logger.debug("whereClauseforSearch : " + whereClauseforSearch);
			logger.debug("andClauseTaxMatrix : " + andClauseTaxMatrix);
			logger.debug("andClauseLocationMatrix : " + andClauseLocationMatrix);

			taxMatrixTransactionStatiticsDTO = databaseStatisticsService.getTaxMatrixStatistics(andClauseTaxMatrix);
			this.getCount("T");
			locationMatrixTransactionStatiticsDTO = databaseStatisticsService.getLocationMatrixStatistics(andClauseLocationMatrix);
			this.getCount("L");
			//for calling SP
			try{
				if(whereClauseforSearch.trim().isEmpty()){ //fix for 4364; "every row the same" - 
					whereClauseforSearch = null; //null works, empty string leads to repeated rows in Postgres
				}
			tempStatisticsData = databaseStatisticsService.getTransactionsStatistics(whereClauseforSearch);
			}catch(SQLException e){
//removing some error code - i mean why should we not show non-ORA errors?				
//kirk removes				if(e.getErrorCode() == ORA_ERROR){
					FacesMessage msg  = new FacesMessage(e.getMessage());
					msg.setSeverity(FacesMessage.SEVERITY_ERROR);
					FacesContext.getCurrentInstance().addMessage(null,msg);
					tempStatisticsData = databaseStatisticsService.getTransactionsStatistics();
//kirk removes				}else{
					logger.debug("Error in stored procedure call" + e.getMessage());
//kirk removes				}
			}
		}
		
		this.setIsValid(false);
		this.setStatusMessage("Search Complete.");
	}


	public List<DatabaseTransactionStatisticsDTO> getTempStatisticsData() {
		//0002663
		/*
		if(tempStatisticsData == null){
			//The first time by using default country
			String country = (String)matrixCommonBean.getUserPreferenceDTO().getUserCountry();
			String whereStr = "";
			if ((country != null) && !country.equals("")) {
				whereStr = " where UPPER(transaction_country_code)=UPPER('" + country + "') ";
				//			" and UPPER(matrix_country_code)=UPPER('" + country + "') ";
			}
			
			//0001934: Company Config - Select Entity
			CacheManager cacheManager = matrixCommonBean.getCacheManager();
			EntityItemDTO entityItemDTO = getEntityItemDTO();
			Map<String, String> parentEntityLevelMap = cacheManager.getParentEntityLevelMap(entityItemDTO.getEntityId());
			
			String entitySql = "";
			for (String transDtlColName : parentEntityLevelMap.keySet()) {
				String entityCode = parentEntityLevelMap.get(transDtlColName);	
				if(entitySql.length()>0){
					entitySql = entitySql + " AND ";
				}	
				entitySql = entitySql + " UPPER("+transDtlColName+")=UPPER('" + entityCode + "') ";
			}
			
			if(entitySql.length()>0){
				if(whereStr.length()==0){
					whereStr = " where ";
				}
				whereStr = whereStr + entitySql;
			}

			try{
				tempStatisticsData = databaseStatisticsService.getTransactionsStatistics(whereStr);
			}catch(SQLException e){
				FacesMessage msg  = new FacesMessage(e.getMessage());
				msg.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage(null,msg);
				tempStatisticsData = databaseStatisticsService.getTransactionsStatistics();
				logger.debug("Error in stored procedure call" + e.getMessage());		
			}
		}
		*/
		
		return tempStatisticsData;
	}

	public void setTempStatisticsData(List<DatabaseTransactionStatisticsDTO> tempStatisticsData) {
		this.tempStatisticsData = tempStatisticsData;
	}
	public String resetFilter(){
		filterSelectionTaxMatrix = new TaxMatrix();
		filterSelectionLocationMatrix = new LocationMatrix();
		
		filterTaxCodeHandler.reset();
		//MF - do not set default country
		//filterTaxCodeHandler.setTaxcodeCountry(getMatrixCommonBean().getUserPreferenceDTO().getUserCountry());
		//filterTaxCodeHandler.setTaxcodeState("");
		//filterTaxCodeHandler.rebuildMenus();
		
		filterHandler.reset();
		//MF - do not set default country
		//filterHandler.setCountry(getMatrixCommonBean().getUserPreferenceDTO().getUserCountry());
		//filterHandler.setState("");
		
		
		fromGLDate = null;
		toGLDate = null;
		taxMatrixId = null;
		processBatchId = null;
		batchNumber = null;
		locationMatrixId = null;
		//FacesUtils.refreshForm("STSTrDBStatusFilter");
		//return "";
		
		selectedCountry = "";		
		if(stateMenuItems!=null) stateMenuItems.clear();

		stateMenuItems = getMatrixCommonBean().getCacheManager().createStateItems(selectedCountry);
		selectedState = "";
		
		return null;
	}


	public void selectedRowChanged(ActionEvent e) throws AbortProcessingException {
		logger.info("enter selectedRowChanged");
		HtmlDataTable table = (HtmlDataTable) (((HtmlAjaxSupport) e.getComponent()).getParent());
		selectedDatabaseTransactionStatisticsDTO = (DatabaseTransactionStatisticsDTO) table.getRowData();
	    selectedRowIndex = table.getRowIndex();
	    String whereQuery = selectedDatabaseTransactionStatisticsDTO.getWhereClause();
		this.setIsValid(true);
		logger.debug("whereQuery " + whereQuery);
		logger.debug("GroupByOnDrilldown " + selectedDatabaseTransactionStatisticsDTO.getGroupByOnDrilldown());
		if(selectedDatabaseTransactionStatisticsDTO.getGroupByOnDrilldown() != null)
			this.setViewDrillDown(true);
		else
			this.setViewDrillDown(false);
	}

	public void sortAction(ActionEvent e) {
		String id = e.getComponent().getId();
		if(id.startsWith("s_trans_")){
			//PP-357
			statisticsDataModel.toggleSortOrder(e.getComponent().getId().replace("s_trans_", ""));
		}
		else{		
			statisticsDataModel.toggleSortOrder(e.getComponent().getId().replace("s_", ""));
		}
		this.setIsValid(false);
		selectedRowTrIndex = -1;
	}

	public DatabaseTransactionStatisticsDTO getSelectedDatabaseTransactionStatisticsDTO() {
		return selectedDatabaseTransactionStatisticsDTO;
	}

	public void setSelectedDatabaseTransactionStatisticsDTO(
			DatabaseTransactionStatisticsDTO selectedDatabaseTransactionStatisticsDTO) {
		this.selectedDatabaseTransactionStatisticsDTO = selectedDatabaseTransactionStatisticsDTO;
	}

	public TransactionDetailService getTransactionDetailService() {
		return transactionDetailService;
	}

	public void setTransactionDetailService(
			TransactionDetailService transactionDetailService) {
		this.transactionDetailService = transactionDetailService;
	}

	public List<TransactionDetailDTO> getTrDetailViewList() {
		return trDetailViewList;
	}


	public TransactionViewBean getTransactionViewBean() {
		return transactionViewBean;
	}

	public void setTransactionViewBean(TransactionViewBean transactionViewBean) {
		this.transactionViewBean = transactionViewBean;
	}

	public List<DriverNamesDTO> getTaxmatrixDrivers(){
		if(taxmatrixDrivers == null){
			taxmatrixDrivers = new ArrayList<DriverNamesDTO>();
			CacheManager cacheManager = matrixCommonBean.getCacheManager();
			Map<String,DriverNames> transactionDriverNamesMap = cacheManager.getTransactionPropertyDriverNamesMap("T");
			Map<String,DataDefinitionColumn> transactionPropertyMap = cacheManager.getDataDefinitionPropertyMap("TB_PURCHTRANS");
			for (String transProperty : transactionDriverNamesMap.keySet()) {
				DriverNamesDTO dto = new DriverNamesDTO();
				if (transactionPropertyMap.get(transProperty) != null) {
					String description = transactionPropertyMap.get(transProperty).getAbbrDesc() + " : ";
					dto.setDescription(description);
					taxmatrixDrivers.add(dto);
				}
			}
		}
		return taxmatrixDrivers;
	}

	public void setTaxmatrixDrivers(List<DriverNamesDTO> taxmatrixDrivers) {
		this.taxmatrixDrivers = taxmatrixDrivers;
	}

	public List<DriverNamesDTO> getLocationmatrixDrivers() {
		if(locationmatrixDrivers == null){
			locationmatrixDrivers = new ArrayList<DriverNamesDTO>();
			CacheManager cacheManager = matrixCommonBean.getCacheManager();
			Map<String,DriverNames> transactionDriverNamesMap = cacheManager.getTransactionPropertyDriverNamesMap("L");
			Map<String,DataDefinitionColumn> transactionPropertyMap = cacheManager.getDataDefinitionPropertyMap("TB_PURCHTRANS");
			for (String transProperty : transactionDriverNamesMap.keySet()) {
				DriverNamesDTO dto = new DriverNamesDTO();
                if (transactionPropertyMap.get(transProperty) != null) {
    				String description = transactionPropertyMap.get(transProperty).getAbbrDesc() + " : ";
    				dto.setDescription(description);
    				locationmatrixDrivers.add(dto);
                }
			}
		}
		return locationmatrixDrivers;
	}

	public void setLocationmatrixDrivers(List<DriverNamesDTO> locationmatrixDrivers) {
		this.locationmatrixDrivers = locationmatrixDrivers;
	}
	
	public String cancelAction(){
		this.setCurrentTransaction(null);
		this.setCurrentJurisdiction(null);
		
		//5138
		if(callingDBStatsViewPage.equalsIgnoreCase("viewDrillDownAction")){

			//Return to drill down view, "viewDrillDownAction" or just put "drillDownStatisticsAction" in cancelAction
			return drillDownStatisticsAction();
		}

		return callingDBStatsViewPage;
	}
	
	private List<SelectItem> stateMenuItems;
	public List<SelectItem> getStateMenuItems() {
		if (stateMenuItems == null) {
			stateMenuItems = getMatrixCommonBean().getCacheManager().createStateItems(selectedCountry);
			selectedState = "";
		}
		 
		return stateMenuItems;
	}
	
	public void searchCountryChanged(ValueChangeEvent e){
		String newCountry = (String)e.getNewValue();
		if(newCountry == null) {
			selectedCountry = "";
		}
		else {
			selectedCountry = newCountry;
		}
		
		if(stateMenuItems!=null) stateMenuItems.clear();
		
		stateMenuItems = getMatrixCommonBean().getCacheManager().createStateItems(selectedCountry);
		selectedState = "";
    }
	
	private List<SelectItem> countryMenuItems;
	public List<SelectItem> getCountryMenuItems() {
		if(countryMenuItems==null){
			selectedCountry = "";
		}
		countryMenuItems = getMatrixCommonBean().getCacheManager().createCountryItems();   
		return countryMenuItems;
	}

	public String displayViewAction() {
		this.setIsValid(false);
		transactionViewBean.setSelectedTransaction(getCurrentTransaction());
		transactionViewBean.setOkAction("viewDbTrStatisticsAction");
		transactionDetailBean.setCurrentAction(EditAction.VIEW_FROM_STATITICS); 
		return "STSDBTrview";
	}

	public void statisticsSaveAs(){
	    try{
    		List<TempStatistics> tempStatisticsList=new ArrayList<TempStatistics>();
    		for(DatabaseTransactionStatisticsDTO databaseTransactionStatisticsDTO:tempStatisticsData) {
    			tempStatisticsList.add(databaseTransactionStatisticsDTO.getTempStatistics());
	        }
    		Util.writeToExcelFile(databaseStatisticsService.saveAs(tempStatisticsList),FacesContext.getCurrentInstance());
	    	
	    }catch(IOException io){
			logger.debug("Error in saving file");
		}
	}
	
	public String transactionsSaveAs(){
		if ((selectedDrillDownStatistics != null)
				|| (selectedDatabaseTransactionStatisticsDTO.getGroupByOnDrilldown() == null)){
			try{
				//List<TransactionDetail> trs = statisticsDataModel.fetchData(0, 10000);
				//Util.writeToExcelFile(transactionDetailService.saveAs(trs) , FacesContext.getCurrentInstance());
				String sqlStatement = transactionDetailService.getStatisticsSQLStatement(new PurchaseTransaction(), 
						selectedDatabaseTransactionStatisticsDTO.getWhereClause(), statisticsDataModel.getSaveAsOrderBy(), 0, 1000000, new ArrayList<String>());
				
				
				transactionBlobBean.setSqlScripts(sqlStatement);
				transactionBlobBean.setRedirectPage("viewDbTrStatisticsAction");
				transactionBlobBean.setAction(EditAction.ADD);
				
				TransactionBlob workingBlob = new TransactionBlob();
				transactionBlobBean.setWorkingBlob(workingBlob);
				transactionBlobBean.setProcessAction(EditAction.START);
				
				return "transactionblob_add";
				
			}catch(Exception e){
				logger.debug("Error in saving file:" + e);
			}
		}
		
		return null;
		
		
	}
/*
	public void transactionsSaveAs(){
		logger.debug("Inside transactions SaveAs");
		if ((selectedDrillDownStatistics != null)
					|| (selectedDatabaseTransactionStatisticsDTO.getGroupByOnDrilldown() == null)){
			try{
				List<TransactionDetail> trs = statisticsDataModel.fetchData(0, 10000);
				Util.writeToExcelFile(transactionDetailService.saveAs(trs) , FacesContext.getCurrentInstance());
			}catch(Exception e){
				logger.debug("Error in saving file:" + e);
			}
		}
	}
*/
	public void drillDownSaveAs(){
		logger.debug("Inside drill down SaveAs");
		String whereQuery = selectedDatabaseTransactionStatisticsDTO.getWhereClause();
		String groupBy = selectedDatabaseTransactionStatisticsDTO.getGroupByOnDrilldown();
		try{
			Util.writeToExcelFile(databaseStatisticsService.saveStatisticsOnGroupBy(groupBy,whereQuery) , FacesContext.getCurrentInstance());
		}catch(IOException io){
			logger.debug("Error in saving file");
		}
	}
	
	// 02-06-2009 Method Modified for exact search and also wild card search
	// BUG# 0003810
	 private boolean processModifyLookupQuery() {
		 logger.debug("Inside process query..");
		    StringBuffer whereClauseTransactionExt = new StringBuffer();
		    StringBuffer andClauseTaxMatrixExt = new StringBuffer();
		    StringBuffer andClauseLocationMatrixExt = new StringBuffer();

		    SimpleDateFormat format = new SimpleDateFormat("MM/dd/yy");
		    String value = "";
		       
		    //for country selection
		    value = selectedCountry;
			if ((value != null) && !value.equals("")) {
				appendANDToWhere(whereClauseTransactionExt,
	                       "UPPER(transaction_country_code)", "=",
	                       "UPPER('" + value + "')");
			}

		    //for state selection
		    value = selectedState;
			if ((value != null) && !value.equals("")) {
				appendANDToWhere(whereClauseTransactionExt,
	                       "UPPER(transaction_state_code)", "=",
	                       "UPPER('" + value + "')");
			}

		    //For Tax driver selection
			CacheManager cacheManager = matrixCommonBean.getCacheManager();
			Map<String,DriverNames> transactionTaxDriverNamesMap =
				cacheManager.getTransactionPropertyDriverNamesMap(filterSelectionTaxMatrix.getDriverCode());
			
			//0001934: Company Config - Select Entity
			EntityItemDTO entityItemDTO = getEntityItemDTO();
			Map<String, String> parentEntityLevelMap = cacheManager.getParentEntityLevelMap(entityItemDTO.getEntityId());
			for (String property : transactionTaxDriverNamesMap.keySet()) {
				DriverNames driverNames = transactionTaxDriverNamesMap.get(property);
				String driver = driverNames.getMatrixColName();
				String transDtlColumnName = driverNames.getTransDtlColName();
				
				value = Util.getPropertyAsString(filterSelectionTaxMatrix, Util.columnToProperty(driver));
				//if (driverNames.isBusinessUnit() && driverNames.isActive() && !entityItemDTO.isEntityCodeAll()) {
			    //	value = entityItemDTO.getEntityCode();
				//}
				if (!entityItemDTO.isEntityCodeAll() && parentEntityLevelMap.containsKey(driverNames.getTransDtlColName())) {
					value = (String)parentEntityLevelMap.get(driverNames.getTransDtlColName());
				}
				
				if ((value != null) && !value.equals("") && !value.equals("*ALL")) {
					logger.debug("Setting property '"+property+"' to value '"+value+"' from " + driver + " transDtlColumnName " +transDtlColumnName);
					  if(isValidValue(value)){
				          if(isValidMinusSign(value)){
				            appendANDToWhereMinus(whereClauseTransactionExt, "UPPER("+ transDtlColumnName +")",
				                           "UPPER('" + processExtractMinusValue(value) + "')");
				            appendANDToANDMinus(andClauseTaxMatrixExt, "UPPER("+ driver +")",
				                         "UPPER('" + processExtractMinusValue(value) + "')");
				          }
				          else{
				        	// BUG# 0003810
				        	  if(value.endsWith("%")){
				        		  appendANDToWhere(whereClauseTransactionExt, "UPPER("+ transDtlColumnName +")", "LIKE",
				                           "UPPER('" + value + "')");
				            appendANDToAND(andClauseTaxMatrixExt, "UPPER("+ driver +")", "LIKE",
				                         "UPPER('" + value + "')");
				        		  
				        	  }else {
				        		     appendANDToWhere(whereClauseTransactionExt, "UPPER("+ transDtlColumnName +")", "=",
					                           "UPPER('" + value + "')");
					            appendANDToAND(andClauseTaxMatrixExt, "UPPER("+ driver +")", "=",
					                         "UPPER('" + value + "')");
							}
				          }
				        }
				}
			}

			Map<String,DriverNames> transactionLocDriverNamesMap =
				cacheManager.getTransactionPropertyDriverNamesMap(filterSelectionLocationMatrix.getDriverCode());

			for (String property : transactionLocDriverNamesMap.keySet()) {
				DriverNames driverNames = transactionLocDriverNamesMap.get(property);
				String driver = driverNames.getMatrixColName();
				String transDtlColumnName = driverNames.getTransDtlColName();

				value = Util.getPropertyAsString(filterSelectionLocationMatrix, Util.columnToProperty(driver));
				if ((value != null) && !value.equals("") && !value.equals("*ALL")) {
					logger.debug("Setting property '"+property+"' to value '"+value+"' from " + driver + " transDtlColumnName " +transDtlColumnName);
					  if(isValidValue(value)){
						  if(isValidValue(value)){
					          if(isValidMinusSign(value)){
					        	appendANDToWhereMinus(whereClauseTransactionExt, "UPPER("+ transDtlColumnName +")",
				                           "UPPER('" + processExtractMinusValue(value) + "')");
					            appendANDToANDMinus(andClauseLocationMatrixExt, "UPPER("+ driver +")",
					                         "UPPER('" + processExtractMinusValue(value) + "')");
					          }
					          else{
					        	// BUG# 0003810
					        	  if(value.endsWith("%")){
					        		  appendANDToWhere(whereClauseTransactionExt, "UPPER("+ transDtlColumnName +")", "LIKE",
					                           "UPPER('" + value + "')");
						            appendANDToAND(andClauseLocationMatrixExt, "UPPER("+ driver +")", "LIKE",
						                         "UPPER('" + value + "')");
					        		  
					        	  }else {
					        		  appendANDToWhere(whereClauseTransactionExt, "UPPER("+ transDtlColumnName +")", "=",
					                           "UPPER('" + value + "')");
						            appendANDToAND(andClauseLocationMatrixExt, "UPPER("+ driver +")", "=",
						                         "UPPER('" + value + "')");
								}
					        	
					          }
					        }
				        }
				}
			}

		    //for from gl_date
		    if (fromGLDate != null ) {
		      value = format.format(fromGLDate);
		      appendANDToWhere(whereClauseTransactionExt, "gl_date", ">=",
		                       "TO_DATE('" + value + "', 'mm/dd/rr')");
		      appendANDToAND(andClauseTaxMatrixExt, "effective_date", ">=",
		                       "TO_DATE('" + value + "', 'mm/dd/rr')");
		      appendANDToAND(andClauseLocationMatrixExt, "effective_date", ">=",
		                       "TO_DATE('" + value + "', 'mm/dd/rr')");
		    }

		    //for to gl_date
		    if (toGLDate != null ) {
			  value = format.format(toGLDate);
		      appendANDToWhere(whereClauseTransactionExt, "gl_date", "<=",
		                       "TO_DATE('" + value + "', 'mm/dd/rr')");
		      appendANDToAND(andClauseTaxMatrixExt, "expiration_date", "<=",
		                       "TO_DATE('" + value + "', 'mm/dd/rr')");
		      appendANDToAND(andClauseLocationMatrixExt, "expiration_date", "<=",
		                       "TO_DATE('" + value + "', 'mm/dd/rr')");
		    }
		    
		    String advancedFilter = advancedFilterBean.getAdvancedFilter();
		    if(advancedFilter != null && !advancedFilter.isEmpty()) {
		    	if (whereClauseTransactionExt.length() == 0) {
		    		whereClauseTransactionExt.append(" WHERE ");
		    		whereClauseTransactionExt.append(advancedFilter);
				} else {
					whereClauseTransactionExt.append(" AND ");
					whereClauseTransactionExt.append(advancedFilter);
				}
		    }

		    //for tax code state selection
			String taxcodeCode = filterTaxCodeHandler.getTaxcodeCode();

		    //for tax code
			if ((taxcodeCode != null) && !taxcodeCode.equals("")) {
		      value = taxcodeCode;

		      //ID 316, AC 5/11/2007
		      if(isValidValue(value)){
		        if(isValidMinusSign(value)){
		          appendANDToWhereMinus(whereClauseTransactionExt, "UPPER(taxcode_code)",
		                       "UPPER('" + processExtractMinusValue(value) + "')");

		          andClauseTaxMatrixExt.append(" AND (    (then_taxcode_code <> '"+processExtractMinusValue(value)+"' OR then_taxcode_code IS NULL)"   );
		          andClauseTaxMatrixExt.append("       OR (else_taxcode_code <> '"+processExtractMinusValue(value)+"' OR else_taxcode_code IS NULL) )" );
		        }
		        else{
		        	// 02-06-2009 Modified for exact search and also wild card search
		        	// BUG# 0003810
		        	if(value.endsWith("%")){
		        			logger.info(" INSIDE IF % "+value);
		        		appendANDToWhere(whereClauseTransactionExt, "UPPER(taxcode_code)", "LIKE", "UPPER('" + value + "')");

			          andClauseTaxMatrixExt.append(" AND (    (then_taxcode_code LIKE '"+value+"')"   );
			          andClauseTaxMatrixExt.append("       OR (else_taxcode_code LIKE '"+value+"') )" );
		        		
		        	}else {
		        		logger.info(" INSIDE else @no@ % "+value);
		        		appendANDToWhere(whereClauseTransactionExt, "UPPER(taxcode_code)", "=", "UPPER('" + value + "')");

			          andClauseTaxMatrixExt.append(" AND (    (then_taxcode_code = '"+value+"')"   );
			          andClauseTaxMatrixExt.append("       OR (else_taxcode_code = '"+value+"') )" );
					}
		         }
		      }
		    }

		    //for tax matrix id
		    if (taxMatrixId != null && taxMatrixId > 0) {
		      value = taxMatrixId.toString();
		      appendANDToWhere(whereClauseTransactionExt, "tax_matrix_id", "=", value );
		    }

		    //for location matrix id
		    if (locationMatrixId != null && locationMatrixId > 0) {
		      value = locationMatrixId.toString();
		      String locationMatrixIdappend = "(SHIPTO_LOCN_MATRIX_ID = value) or (SHIPFROM_LOCN_MATRIX_ID = value) or (ORDRACPT_LOCN_MATRIX_ID = value) or " +
						"(ORDRORGN_LOCN_MATRIX_ID = value) or (FIRSTUSE_LOCN_MATRIX_ID = value) or (BILLTO_LOCN_MATRIX_ID = value) or " +
						"(TTLXFR_LOCN_MATRIX_ID = value)";
		     // appendANDToWhere(whereClauseTransactionExt, "location_matrix_id", "=", value );
		      if (whereClauseTransactionExt.length() == 0) {
		    	  whereClauseTransactionExt.append(" WHERE");
				} else {
					whereClauseTransactionExt.append(" AND");
				}
		      whereClauseTransactionExt.append(" " +locationMatrixIdappend);
		    }

		    //for batch number
		    batchNumber = processBatchId;
		    if (batchNumber != null && batchNumber > 0) {
		      value = batchNumber.toString();
		      appendANDToWhere(whereClauseTransactionExt, "process_batch_no", "=", value );
		    }

		    String geocode = filterHandler.getGeocode();
		    String city = filterHandler.getCity();
		    String county = filterHandler.getCounty();
		    String state = filterHandler.getState();
		    String country = filterHandler.getCountry();
		    String zip = filterHandler.getZip();
		    //for geocode
		    if (geocode != null && !geocode.equals("")) {
		      value = geocode;

		      //ID 316, AC 5/11/2007
		      if(isValidValue(value)){
		        if(isValidMinusSign(value)){
		          appendANDToWhereMinus(whereClauseTransactionExt, "UPPER(geocode)",
		                       "UPPER('" + processExtractMinusValue(value) + "')");
		          appendANDToANDMinus(andClauseLocationMatrixExt, "UPPER(geocode)",
		                       "UPPER('" + processExtractMinusValue(value) + "')");
		        }
		        else{
		        	// BUG# 0003810
		        	if(value.endsWith("%")){
				          appendANDToWhere(whereClauseTransactionExt, "UPPER(geocode)", "LIKE", "UPPER('" + value + "')");
				          appendANDToAND(andClauseLocationMatrixExt, "UPPER(geocode)", "LIKE", "UPPER('" + value + "')");
		        		
		        	}else{
		        	appendANDToWhere(whereClauseTransactionExt, "UPPER(geocode)", "=", "UPPER('" + value + "')");
		          	appendANDToAND(andClauseLocationMatrixExt, "UPPER(geocode)", "=", "UPPER('" + value + "')");
		        	}
		        }
		      }
		    }

		    //for city
		    if (city != null && !city.equals("")) {
		      value = city;

		      //ID 316, AC 5/11/2007
		      if(isValidValue(value)){
		        if(isValidMinusSign(value)){
		          appendANDToWhereMinus(whereClauseTransactionExt, "UPPER(city)",
		                       "UPPER('" + processExtractMinusValue(value) + "')");

		          appendANDToANDMinus(andClauseLocationMatrixExt, "UPPER(city)",
		                       "UPPER('" + processExtractMinusValue(value) + "')");
		        }
		        else{
		        	// BUG# 0003810
		        	if(value.endsWith("%")){
		        		 appendANDToWhere(whereClauseTransactionExt, "UPPER(city)", "LIKE", "UPPER('" + value + "')");
			             appendANDToAND(andClauseLocationMatrixExt, "UPPER(city)", "LIKE", "UPPER('" + value + "')");
		        		
		        	}else {
		        		 appendANDToWhere(whereClauseTransactionExt, "UPPER(city)", "=", "UPPER('" + value + "')");
		        		 appendANDToAND(andClauseLocationMatrixExt, "UPPER(city)", "=", "UPPER('" + value + "')");
					}
		         
		        }
		      }
		    }

		    //for county
		    if (county != null && !county.equals("")) {
		      value = county;

		      //ID 316, AC 5/11/2007
		      if(isValidValue(value)){
		        if(isValidMinusSign(value)){
		          appendANDToWhereMinus(whereClauseTransactionExt, "UPPER(county)",
		                       "UPPER('" + processExtractMinusValue(value) + "')");
		          appendANDToANDMinus(andClauseLocationMatrixExt, "UPPER(county)",
		                       "UPPER('" + processExtractMinusValue(value) + "')");
		        }
		        else{
		        	// BUG# 0003810
		        	if(value.endsWith("%")){
		        		 appendANDToWhere(whereClauseTransactionExt, "UPPER(county)", "LIKE", "UPPER('" + value + "')");
		        		 appendANDToAND(andClauseLocationMatrixExt, "UPPER(county)", "LIKE", "UPPER('" + value + "')");
		        	}
		        	else {
		        		 appendANDToWhere(whereClauseTransactionExt, "UPPER(county)", "=", "UPPER('" + value + "')");
		        		 appendANDToAND(andClauseLocationMatrixExt, "UPPER(county)", "=", "UPPER('" + value + "')");
					}
		         
		        }
		      }
		    }

		    //for state
		    if (state != null && !state.equals("")) {
		      value = state;
		      appendANDToWhere(whereClauseTransactionExt, "UPPER(tb_jurisdiction.state)", "=",
		                       "UPPER('" + value + "')");

		      appendANDToAND(andClauseLocationMatrixExt, "UPPER(tb_jurisdiction.state)", "=",
		                       "UPPER('" + value + "')");
		    }
		    
		  //for country
		    if (country != null && !country.equals("")) {
		      value = state;
		      appendANDToWhere(whereClauseTransactionExt, "UPPER(tb_jurisdiction.country)", "=",
		                       "UPPER('" + value + "')");

		      appendANDToAND(andClauseLocationMatrixExt, "UPPER(tb_jurisdiction.country)", "=",
		                       "UPPER('" + value + "')");
		    }

		    //for zip
		    if (zip != null && !zip.equals("")) {
		      value = zip;
		   // BUG# 0003810
		      if(value.endsWith("%"))
		      {
		    	  appendANDToWhere(whereClauseTransactionExt, "UPPER(zip)", "LIKE", "UPPER('" + value + "')");
			      appendANDToAND(andClauseLocationMatrixExt, "UPPER(zip)", "LIKE", "UPPER('" + value + "')");
		      }else {
		    	  appendANDToWhere(whereClauseTransactionExt, "UPPER(zip)", "=", "UPPER('" + value + "')");
			      appendANDToAND(andClauseLocationMatrixExt, "UPPER(zip)", "=", "UPPER('" + value + "')");
			}
		    }
		    whereClauseforSearch= whereClauseTransactionExt.toString();
		    andClauseTaxMatrix = andClauseTaxMatrixExt.toString();
		    andClauseLocationMatrix = andClauseLocationMatrixExt.toString();
		    return true;
		  }

	private void appendToWhere(StringBuffer ls_where, String columnName,
			String operator, String value) {
		ls_where.append(" " + columnName + " " + operator + " " + value);
	}

	private void appendANDToWhere(StringBuffer ls_where, String columnName,
			String operator, String value) {
		if (ls_where.length() == 0) {
			ls_where.append(" WHERE");
		} else {
			ls_where.append(" AND");
		}
		appendToWhere(ls_where, columnName, operator, value);
	}

	private void appendANDToAND(StringBuffer ls_where, String columnName,
			String operator, String value) {
		ls_where.append(" AND");
		appendToWhere(ls_where, columnName, operator, value);
	}

	// /AND (DRIVER_01 <> UPPER('AAAA') OR DRIVER_01 IS NULL)
	private void appendANDToWhereMinus(StringBuffer ls_where,
			String columnName, String value) {
		if (ls_where.length() == 0) {
			ls_where.append(" WHERE ");
		} else {
			ls_where.append(" AND ");
		}
		appendToWhereMinus(ls_where, columnName, value);
	}

	private void appendANDToANDMinus(StringBuffer ls_where, String columnName,
			String value) {
		ls_where.append(" AND ");
		appendToWhereMinus(ls_where, columnName, value);
	}

	private void appendToWhereMinus(StringBuffer ls_where, String columnName,
			String value) {
		ls_where.append("(" + columnName + " <> " + value + " OR " + columnName
				+ " IS NULL)");
	}

	private String processExtractMinusValue(String value) {
		if (value == null || value.trim().length() == 0) {
			return "";
		}

		String temp = value.trim();

		if (temp.equalsIgnoreCase("-")) {
			return "";
		}

		if (temp.startsWith("-")) {
			return temp.substring(1, temp.length()).trim();
		}

		if (temp.endsWith("-")) {
			return temp.substring(0, temp.length() - 1).trim();
		}

		return temp;
	}

	private boolean isValidValue(String value) {
		if (value.equalsIgnoreCase("-")) {
			return false;
		}

		return true;
	}

	private boolean isValidMinusSign(String value) {
		if (value.equalsIgnoreCase("-")) {
			return false;
		}

		if (value.startsWith("-") || value.endsWith("-")) {
			return true;
		}

		return false;
	}


	public void defaultImportMapProcessListener(ActionEvent e){
		batchIdHandler.setDefaultSearch("IP");
		batchIdHandler.reset();
	}
	
	public void filterSaveAction(ActionEvent e) {
		
    }

	public String getSelectedCountry() {
		return selectedCountry;
	}

	public void setSelectedCountry(String selectedCountry) {
		this.selectedCountry = selectedCountry;
	}

	public String getSelectedState() {
		return selectedState;
	}

	public void setSelectedState(String selectedState) {
		this.selectedState = selectedState;
	}
	
	public void filterChangeListener(ActionEvent e) {	
		filterSelectionTaxMatrix = new TaxMatrix();
		filterSelectionLocationMatrix = new LocationMatrix();
		filterTaxCodeHandler.reset();
		filterHandler.reset();
		fromGLDate = null;
		toGLDate = null;
		taxMatrixId = null;
		locationMatrixId = null;
		processBatchId = null;
		
		selectionFilterBean.setUserDTO(matrixCommonBean.getLoginBean().getUserDTO());
		if(selectedFilter != "") {
			selectedUserName = selectedFilter.substring(0, selectedFilter.indexOf("/"));
			selectedFilterName = selectedFilter.substring(selectedFilter.indexOf("/") + 1 ,selectedFilter.length());
			selectedFilter = selectedUserName + "/" + selectedFilterName;
			dwFilters = dwFilterService.getColumnsByFilterNameandUser("TB_PURCHTRANS", selectedUserName, selectedFilterName);
		}
		
		else {
			selectionFilterBean.setCurrentFilterName("");
		}

		//Create filters column, values map
		Map<String, String> columnValuesMap = new HashMap<String, String>();
		for(DwFilter dwFilter   : dwFilters){
			columnValuesMap.put(dwFilter.getColumnName(), dwFilter.getFilterValue());
		}
		
		String value = "";
		String matrixColumnName = "";
		Map<String,DriverNames> taxMatrixDriverNamesMap = getMatrixCommonBean().getCacheManager().getTransactionPropertyDriverNamesMap(TaxMatrix.DRIVER_CODE);
		for (String columnName : columnValuesMap.keySet()) {
			value = columnValuesMap.get(columnName);		
			if (taxMatrixDriverNamesMap.get(columnName) != null) {
				matrixColumnName =  taxMatrixDriverNamesMap.get(columnName).getMatrixColName();			
				Util.setProperty(filterSelectionTaxMatrix, Util.columnToProperty(matrixColumnName), value, String.class);
			}
		}
		
		Map<String,DriverNames> locationMatrixDriverNamesMap = getMatrixCommonBean().getCacheManager().getTransactionPropertyDriverNamesMap(LocationMatrix.DRIVER_CODE);
		for (String columnName : columnValuesMap.keySet()) {
			value = columnValuesMap.get(columnName);
			if (locationMatrixDriverNamesMap.get(columnName) != null) {
				matrixColumnName =  locationMatrixDriverNamesMap.get(columnName).getMatrixColName();			
				Util.setProperty(filterSelectionLocationMatrix, Util.columnToProperty(matrixColumnName), value, String.class);
			}
		}
		//Set Advanced filter
		if(columnValuesMap.get("*ADVANCED")!=null){
			advancedFilterBean.setAdvancedFilter(columnValuesMap.get("*ADVANCED"));
		}
		else{
			advancedFilterBean.setAdvancedFilter("");
		}
		
		
		SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
		if(columnValuesMap.get("fromGLDate")!=null){
			String fromGLDateString = columnValuesMap.get("fromGLDate");
			try{
				fromGLDate =  format.parse(fromGLDateString);
			}
			catch(Exception e1){			
			}
		}
		if(columnValuesMap.get("toGLDate")!=null){
			String toGLDateString = columnValuesMap.get("toGLDate");
			try{
				toGLDate =  format.parse(toGLDateString);
			}
			catch(Exception e1){			
			}
		}
		
		if(columnValuesMap.get("taxMatrixId")!=null){
			String taxMatrixIdStr = columnValuesMap.get("taxMatrixId");
			try{		
				taxMatrixId = Long.parseLong(taxMatrixIdStr);
			}
			catch(Exception ee){	
			}
		}
		
		if(columnValuesMap.get("locationMatrixId")!=null){
			String locationMatrixIdStr = columnValuesMap.get("locationMatrixId");
			try{		
				locationMatrixId = Long.parseLong(locationMatrixIdStr);
			}
			catch(Exception ee){	
			}
		}
		
		if(columnValuesMap.get("processBatchNo")!=null){
			String processBatchNoStr = columnValuesMap.get("processBatchNo");
			try{		
				processBatchId = Long.parseLong(processBatchNoStr);
			}
			catch(Exception ee){	
			}
		}
		
		if(columnValuesMap.get("taxcodeCode")!=null){
			String taxcodeCode = columnValuesMap.get("taxcodeCode");
			filterTaxCodeHandler.setTaxcodeCode(taxcodeCode);
		}
		
		if(columnValuesMap.get("geocode")!=null){
			String geocode = columnValuesMap.get("geocode");
			filterHandler.setGeocode(geocode);
		}
		
		if(columnValuesMap.get("city")!=null){
			String city = columnValuesMap.get("city");
			filterHandler.setCity(city);
		}
		
		if(columnValuesMap.get("county")!=null){
			String county = columnValuesMap.get("county");
			filterHandler.setCounty(county);
		}
		
		if(columnValuesMap.get("state")!=null){
			String state = columnValuesMap.get("state");
			filterHandler.setState(state);
		}
		
		if(columnValuesMap.get("country")!=null){
			String country = columnValuesMap.get("country");
			filterHandler.setCountry(country);
		}
		
		if(columnValuesMap.get("zip")!=null){
			String zip = columnValuesMap.get("zip");
			filterHandler.setZip(zip);
		}
		
	}
	
	public String filterEditAction() {
		List<DwFilter> dwFilterList = new ArrayList<DwFilter>();
		
		//Create filters column, values map
		Map<String, String> columnValuesMap = new HashMap<String, String>();
		
		String value = "";
		String transactionColumnname = "";
		String name = "";
		Map<String,DriverNames> taxDriversMap =  getMatrixCommonBean().getCacheManager().getMatrixColDriverNamesMap(TaxMatrix.DRIVER_CODE);
		for (String matrixColumnName : taxDriversMap.keySet()) {
			DriverNames driverNames = taxDriversMap.get(matrixColumnName);
			transactionColumnname = driverNames.getTransDtlColName();
			value = (String) Util.getProperty(filterSelectionTaxMatrix, Util.columnToProperty(matrixColumnName));			
			if(value!=null && value.length()>0){
				name = Util.columnToProperty(transactionColumnname);
				if(columnValuesMap.get(name)==null){
					columnValuesMap.put(name, value);
					
					DwFilter dwFilter = new DwFilter(new DwFilterPK("","","",name));
					dwFilter.setFilterValue(value);
					dwFilterList.add(dwFilter);
				}
			}
		}
		
		Map<String,DriverNames> locationDriversMap =  getMatrixCommonBean().getCacheManager().getMatrixColDriverNamesMap(LocationMatrix.DRIVER_CODE);
		for (String matrixColumnName : locationDriversMap.keySet()) {
			DriverNames driverNames = locationDriversMap.get(matrixColumnName);
			transactionColumnname = driverNames.getTransDtlColName();
			value = (String) Util.getProperty(filterSelectionLocationMatrix, Util.columnToProperty(matrixColumnName));	
			if(value!=null && value.length()>0){
				name = Util.columnToProperty(transactionColumnname);
				if(columnValuesMap.get(name)==null){
					columnValuesMap.put(name, value);
					
					DwFilter dwFilter = new DwFilter(new DwFilterPK("","","",name));
					dwFilter.setFilterValue(value);
					dwFilterList.add(dwFilter);
				}
			}
		}
		
		//Other fields
		
		if ((taxMatrixId != null) && (taxMatrixId > 0L)) {
			columnValuesMap.put("taxMatrixId", taxMatrixId.toString());
			DwFilter dwFilter = new DwFilter(new DwFilterPK("","","","taxMatrixId"));
			dwFilter.setFilterValue(taxMatrixId.toString());
			dwFilterList.add(dwFilter);
		}
		
		if ((locationMatrixId != null) && (locationMatrixId > 0L)) {
			columnValuesMap.put("locationMatrixId", locationMatrixId.toString());			
			DwFilter dwFilter = new DwFilter(new DwFilterPK("","","","locationMatrixId"));
			dwFilter.setFilterValue(locationMatrixId.toString());
			dwFilterList.add(dwFilter);
		}
		
		if ((processBatchId != null) && (processBatchId > 0L)) {
			columnValuesMap.put("processBatchNo", processBatchId.toString());	
			DwFilter dwFilter = new DwFilter(new DwFilterPK("","","","processBatchNo"));
			dwFilter.setFilterValue(processBatchId.toString());
			dwFilterList.add(dwFilter);
		}

		String taxcodeCode = filterTaxCodeHandler.getTaxcodeCode();	
		if ((taxcodeCode != null) && !taxcodeCode.equals("")) {
			columnValuesMap.put("taxcodeCode", taxcodeCode);		
			DwFilter dwFilter = new DwFilter(new DwFilterPK("","","","taxcodeCode"));
			dwFilter.setFilterValue(taxcodeCode);
			dwFilterList.add(dwFilter);
		}
		
		String geocode = filterHandler.getGeocode();
		if ((geocode != null) && !geocode.equals("")) {
			columnValuesMap.put("geocode", geocode);		
			DwFilter dwFilter = new DwFilter(new DwFilterPK("","","","geocode"));
			dwFilter.setFilterValue(geocode);
			dwFilterList.add(dwFilter);
		}
		
		String city = filterHandler.getCity();
		if ((city != null) && !city.equals("")) {
			columnValuesMap.put("city", city);		
			DwFilter dwFilter = new DwFilter(new DwFilterPK("","","","city"));
			dwFilter.setFilterValue(city);
			dwFilterList.add(dwFilter);
		}
		
		String county = filterHandler.getCounty();
		if ((county != null) && !county.equals("")) {
			columnValuesMap.put("county", county);		
			DwFilter dwFilter = new DwFilter(new DwFilterPK("","","","county"));
			dwFilter.setFilterValue(county);
			dwFilterList.add(dwFilter);
		}
		
		String state = filterHandler.getState();
		if ((state != null) && !state.equals("")) {
			columnValuesMap.put("state", state);		
			DwFilter dwFilter = new DwFilter(new DwFilterPK("","","","state"));
			dwFilter.setFilterValue(state);
			dwFilterList.add(dwFilter);
		}
		
		String country = filterHandler.getCountry();
		if ((country != null) && !country.equals("")) {
			columnValuesMap.put("country", country);		
			DwFilter dwFilter = new DwFilter(new DwFilterPK("","","","country"));
			dwFilter.setFilterValue(country);
			dwFilterList.add(dwFilter);
		}
		
		String zip = filterHandler.getZip();
		if ((zip != null) && !zip.equals("")) {
			columnValuesMap.put("zip", zip);		
			DwFilter dwFilter = new DwFilter(new DwFilterPK("","","","zip"));
			dwFilter.setFilterValue(zip);
			dwFilterList.add(dwFilter);
		}
		
        SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy"); 
        if(fromGLDate!=null){
        	String dateString = format.format(fromGLDate);
			columnValuesMap.put("fromGLDate", dateString);	
			DwFilter dwFilter = new DwFilter(new DwFilterPK("","","","fromGLDate"));
			dwFilter.setFilterValue(dateString);
			dwFilterList.add(dwFilter);
        }
        
        if(toGLDate!=null){
        	String dateString = format.format(toGLDate);
			columnValuesMap.put("toGLDate", dateString);	
			DwFilter dwFilter = new DwFilter(new DwFilterPK("","","","toGLDate"));
			dwFilter.setFilterValue(dateString);
			dwFilterList.add(dwFilter);
        }
        
        String advancedFilter = advancedFilterBean.getAdvancedFilter(); 
		if(advancedFilter!=null && advancedFilter.trim().length()>0){
			columnValuesMap.put("*ADVANCED", advancedFilter.trim());		
			DwFilter dwFilter = new DwFilter(new DwFilterPK("","","","*ADVANCED"));
			dwFilter.setFilterValue(advancedFilter.trim());
			dwFilterList.add(dwFilter);
		}
		
		selectionFilterBean.setUserDTO(matrixCommonBean.getLoginBean().getUserDTO());
		if(selectedFilter != "") {
			selectedUserName = selectedFilter.substring(0, selectedFilter.indexOf("/"));
			selectedFilterName = selectedFilter.substring(selectedFilter.indexOf("/") + 1 ,selectedFilter.length());
			selectedFilter = selectedUserName + "/" + selectedFilterName;
			dwFilters = dwFilterService.getColumnsByFilterNameandUser("TB_PURCHTRANS", selectedUserName, selectedFilterName);
			selectionFilterBean.setCurrentFilterName(selectedFilter);
		}
		else {
			selectionFilterBean.setCurrentFilterName("");
		}
        
		selectionFilterBean.setScreenName("TB_PURCHTRANS");
		selectionFilterBean.setDwFilters(dwFilters);  
		selectionFilterBean.setDwFilterList(dwFilterList);
		selectionFilterBean.setallbackBeanDbStats(this);
		selectionFilterBean.setCallingScreen(TRANSACTION_STATISTICS_CALLING_SCREEN);

		for (int i=0; i<dwFilterList.size() ; i++) {
			DwFilter dwFilter = dwFilterList.get(i);
			dwFilter.setWindowName("TB_PURCHTRANS");
			dwFilter.setUserCode(matrixCommonBean.getLoginBean().getUserDTO().getUserCode());
		}

		return "filter_update";
	}
	
	public String advancedFilterAction() {
		advancedFilterBean.setFromScreenName(ADVANCE_FILTER_DBSTATITCIS_SCREEN); 
		advancedFilterBean.initAdvancedFilter();
		return "advanced_filter";
	}
	
	public boolean getIsAdvancedFilterEnabled() {
		return advancedFilterBean.isAdvancedFilterEnabled();
	}
}
