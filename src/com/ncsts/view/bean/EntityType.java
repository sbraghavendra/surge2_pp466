package com.ncsts.view.bean;


public enum EntityType {
	shipto,
	shipfrom,
	ordrorgn,
	ordracpt,
	firstuse,
	billto,
	ttlxfr;
}
