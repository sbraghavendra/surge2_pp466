package com.ncsts.view.bean;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlPanelGrid;
import javax.faces.component.html.HtmlSelectBooleanCheckbox;
import javax.faces.context.FacesContext;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.persistence.Column;
import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;

import org.ajax4jsf.component.UIDataAdaptor;
import org.ajax4jsf.component.html.HtmlAjaxSupport;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateFormatUtils;
import org.apache.log4j.Logger;
import org.richfaces.component.html.HtmlDataTable;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.ncsts.common.LogMemory;
import com.ncsts.common.LoggerFactory;
import com.ncsts.common.Util;
import com.ncsts.common.constants.PurchaseTransactionServiceConstants.LocationType;
import com.ncsts.common.constants.PurchaseTransactionServiceConstants.LocationTypeCalcFlags;
import com.ncsts.common.constants.PurchaseTransactionServiceConstants.LogSourceEnum;
import com.ncsts.common.constants.PurchaseTransactionServiceConstants.SuspendReason;
import com.ncsts.common.helper.FieldSortOrder;
import com.ncsts.common.helper.OrderBy;
import com.ncsts.dao.BatchMaintenanceDAO;
import com.ncsts.domain.Auditable;
import com.ncsts.domain.BatchMaintenance;
import com.ncsts.domain.BillTransaction;
import com.ncsts.domain.DataDefinitionColumn;
import com.ncsts.domain.DriverNames;
import com.ncsts.domain.DwFilter;
import com.ncsts.domain.DwFilterPK;
import com.ncsts.domain.EntityItem;
import com.ncsts.domain.Jurisdiction;
import com.ncsts.domain.JurisdictionTaxrate;
import com.ncsts.domain.ListCodes;
import com.ncsts.domain.LocationMatrix;
import com.ncsts.domain.Option;
import com.ncsts.domain.OptionCodePK;
import com.ncsts.domain.PurchaseTransaction;
import com.ncsts.domain.PurchaseTransactionLog;
import com.ncsts.domain.TaxCode;
import com.ncsts.domain.TaxCodeDetail;
import com.ncsts.domain.TaxCodePK;
import com.ncsts.domain.TaxMatrix;
import com.ncsts.domain.TransactionHeader;
import com.ncsts.domain.User;
import com.ncsts.dto.TransactionMatrixCompareDTO;
import com.ncsts.exception.PurchaseTransactionProcessingException;
import com.ncsts.jsf.model.TransactionDataModel;
import com.ncsts.services.BatchMaintenanceService;
import com.ncsts.services.DwColumnService;
import com.ncsts.services.DwFilterService;
import com.ncsts.services.EntityItemService;
import com.ncsts.services.JurisdictionService;
import com.ncsts.services.LocationMatrixService;
import com.ncsts.services.OptionService;
import com.ncsts.services.PurchaseTransactionLogService;
import com.ncsts.services.PurchaseTransactionService;
import com.ncsts.services.TaxCodeDetailService;
import com.ncsts.services.TaxCodeService;
import com.ncsts.services.TransactionDetailBillService;
import com.ncsts.services.TransactionDetailService;
import com.ncsts.services.UserService;
import com.ncsts.view.util.DateTimeConverter;
import com.ncsts.view.util.StateObservable;
import com.ncsts.view.util.TogglePanelController;

public class TransactionDetailBean implements BaseMatrixBackingBean.FindIdCallback, BaseMatrixBackingBean.SearchCallback, SearchJurisdictionHandler.SearchJurisdictionCallback,
														ViewTaxCodeBean.TaxCodeInfoCallback, AdvancedSortBean.SortCallback, TaxCodeBackingBean.SearchCallback, TaxCodeBackingBean.FindCodeCallback,
														Observer, HttpSessionBindingListener {

	static private Logger logger = LoggerFactory.getInstance().getLogger(TransactionDetailBean.class);
	
	private StateObservable ov = null;
	
	private List<SelectItem> statusItems;
	private List<SelectItem> filterItems;
	private List<SelectItem> viewItems;
	private String filterFormState = "open";
	private HtmlPanelGrid filterSelectionPanel;
	private HtmlPanelGrid taxDriverViewPanel;
	private HtmlPanelGrid locationDriverViewPanel;
	private HtmlPanelGrid filterTaxDriverSelectionPanel; 
	private HtmlPanelGrid filterLocDriverSelectionPanel; 
	// Selection filter inputs
	private TaxCodeCodeHandler filterTaxCodeHandler;// = new TaxCodeSearchHandler("trans_main");
	private TaxMatrix filterSelectionTaxMatrix = new TaxMatrix();
	private LocationMatrix filterSelectionLocationMatrix = new LocationMatrix();
	private LocationMatrix filterSelectionLocationMatrixTemp = new LocationMatrix();
	private String transactionStatus = "";
	private String selectedFilter = "";
	private String selectedColumn = "";
	private String selectedView = "";
	private Date fromGLDate;
	private Date toGLDate;
	private Long purchtransId;  
	private Long jurisdictionId;
	private Long taxMatrixId;
	private Long locationMatrixId;
	private Long glExtractId;
	private Long processBatchId;
	
	private String selectedCountry = "";
	private String selectedState = "";
	
	private int selectedRowIndex = -1;
	private PurchaseTransaction selectedTransaction;
	private PurchaseTransaction selectedTransactionTemp;
	
    private PurchaseTransaction updatedTransaction;
	
	private int currentTransactionIndex = -1;
	private List<PurchaseTransaction> currentTransactionList;
	private Jurisdiction currentJurisdiction;
	//private SearchJurisdictionHandler currentJurisdictionHandler = new SearchJurisdictionHandler();
	private Boolean applyToAll = false;
	private Boolean changesMade = false;
	
	private SearchBatchIdHandler batchIdHandler = new SearchBatchIdHandler();
	
	private SearchJurisdictionHandler newJurisdictionHandler = new SearchJurisdictionHandler();
	private SearchJurisdictionHandler newJurisdictionHandlerModify = new SearchJurisdictionHandler();
	private SearchJurisdictionHandler jurisdictionHandler = new SearchJurisdictionHandler();
	
	private TaxCodeCodeHandler newTaxCodeHandler = new TaxCodeCodeHandler("taxCode");
	private TaxCodeCodeHandler newTaxCodeHandlerUpdate = new TaxCodeCodeHandler("taxCodeUpdate");
	
	private String comments; // this will hold the modified comments
	private boolean suspendTax; // this boolean value will decide the transaction will be suspended for Tax Matrix.
	private boolean suspendLocation; //this boolean value will decide the transaction will be susspended for Location Matrix
	private boolean suspendTaxcode;
	private boolean suspendTaxrate;
	
	private boolean suspendFlag;
	private String suspendType;
	
	String lastViewColumn;

	private JurisdictionService jurisdictionService; // this service is declared to get the Jurisdiction details
	private TransactionDetailService transactionDetailService;
	private BatchMaintenanceService batchMaintenanceService;
	private MatrixCommonBean matrixCommonBean;
	private TransactionViewBean transactionViewBean;
	private AllocationMatrixBackingBean allocationMatrixBean;
	private LocationMatrixBackingBean locationMatrixBean;
	private TaxMatrixViewBean taxMatrixBean;
	private TaxJurisdictionBackingBean taxJurisdictionBean;
	private AdvancedFilterBean advancedFilterBean;
	private AdvancedSortBean advancedSortBean;
	private TransactionBlobBean transactionBlobBean;
	private BatchMaintenance downloadBatchMaintenance;
	private BatchMaintenanceDAO batchMaintenanceDAO;
	private DownloadAddBean downloadAddBean;
	
	@Autowired
	private BatchMaintenanceBackingBean batchMaintenanceBean;
	
	@Autowired
	private TaxCodeBackingBean taxCodeBackingBean;
	
	@Autowired
	private DwFilterService dwFilterService;
	
	@Autowired
	private DwColumnService dwColumnService;
	
	@Autowired
	private SelectionFilterBean selectionFilterBean;
	
	@Autowired
	private SelectionColumnBean selectionColumnBean;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private DriverHandlerBean driverHandlerBean;
	
	@Autowired
	private FilePreferenceBackingBean filePreferenceBean;
		
	@Autowired
	private PurchaseTransactionService purchaseTransactionService;
	
	private TransactionDataModel transactionDataModel;
	
	private HtmlDataTable trDetailTable;
	private boolean displayAllFields = false;
	private boolean indicateAllSelected = false;
	
	private TaxCodeDetail existedTaxCodeDetail;
	
	private boolean displayWarning = false;
	private boolean displayIncomplete = false;
	
	private boolean ignoreTaxCode = false;
	
	private Map<String,Double> selectionTotals = new HashMap<String,Double>();
	
	private List<JurisdictionTaxrate> jurisdictionTaxrateList = new ArrayList<JurisdictionTaxrate>();

	private JurisdictionTaxrate selectedTaxRate;

	private int selectedTaxRateIndex;
	
	private ViewTaxCodeBean viewTaxCodeBean;
	
	private long totalSelected = 1;
	
	private List<Long> selectedIds = null;
	private String multiTypeSelected = "";

	// 4184
	private PurchaseTransaction transactionDetail;
	
	// 4184
	private List<PurchaseTransaction> traList = new ArrayList<PurchaseTransaction>();
	
	// 4184
	//SaveAs
	private boolean dispSaveAs = false;
	
	// this is a "magic number" that seems a safe amount w/o taking too much heap space
	static final int MAX_SELECT_ALL = 10000;
	
	//3987
	private OptionService optionService;
	private static final String optionCode = "SAVEGRIDCHANGES";

	private static final String user = "USER";
	  
	private Boolean renderOptionPanel;
	  
	private boolean saveNow;
	
	private boolean showOkButton = false;
	
	private long searchWait;
	private long searchSleep;
	
	private TransactionSplittingToolBean<PurchaseTransaction> transactionSplittingToolBean;
	private List<TransactionSplittingToolBean<?>> currentSplittingToolBean;
	
	private String multiTransCode;
	private Long subTransId;
	private String applyJurisdictionMessage= "";
	
	private TransactionDataModel splitGroupDataModel;
	private HtmlDataTable trDetailSplitTable;
	
	private int selectedSplitRowIndex = -1;
	private PurchaseTransaction selectedSplitTransaction;
	private PurchaseTransaction selectedSplitTransactionTemp;
	
	private boolean displayAllViewSplitFields = false;
	
	private boolean showSplitGroupViewSplitButton = false;
	private TogglePanelController togglePanelController = null;
	private String selectedTaxabilityCode="";
	private List<SelectItem> taxabilityCodeMenuItems;
	private String returnTransUrl = null; 
	private int selectedTransactionLogIndex = -1;
	private PurchaseTransactionLog selectedTransactionLog = null;
	private List<PurchaseTransactionLog> transactionLogDetailList;
	private int transLogHistoryTablePageSize = 50;
	private int transLogHistoryTablePageNumber; 
	private PurchaseTransactionLogService purchaseTransactionLogService;
	private TaxCodeService taxCodeService;

	private EditAction currentAction;
	
	private String transState;
	
	private User currentUser;
	
	private static String  PROCESS_STATUS_COLUMN =  "processStatus";
	
	private  Map<String,String> whatIfMap;
	
	private int selectedCompareTaxMatrixIndex = -1;
	private int selectedCompareLocationMatrixIndex = -1;
	
	private LocationMatrixService locationMatrixService;
	
	private EntityItemService entityItemService; 
	
	private Map<Long,String> entityMap;
	private Map<String,String> moduleMap;
	
	private List<TransactionMatrixCompareDTO> transactionMatrixList; 
	
	private String compareToMatrixType; 
	private String compareToMatrixTypeSearched;
	
	private Long compareToMatrixId;// 
	
	private static final String TRANS_MODULE_CODE = "PURCHUSE"; 
	
	private static final String COMPARE_TRANSACTION_LABEL = "Transaction:";
			
	private static final String COMPARE_MATRIX_LABEL = "Compare Matrix:";
	
	private static final String COMPARE_RESULT_LABEL = "Results:";
	
	public TaxMatrix selectedCompareTaxMatrix;
	public LocationMatrix selectedCompareLocationMatrix;
	public List<TaxMatrix> matchingTaxMatrixList;
	List<LocationMatrix> matchingLocationMatrixList;
	
	private static final String ADVANCE_FILTER_TRANSACTION_SCREEN = "transactionProcess";
	private static String TRANSACTION_PROCESS_CALLING_SCREEN = "transactionProcess";
	private  List<DwFilter> dwFilters;
	
	private LocationType locationType;
	@Autowired
	private TaxCodeDetailService taxCodeDetailService;
	
	private static final String ONE = "1";
	private static final String TWO = "2";
	private static final String THREE = "3";
	private static final String FOUR = "4";
	private static final String FIVE = "5";
	private static final String SIX = "6";
	private static final String SEVEN = "7";
	private static final String EIGHT = "8";
	private static final String NINE = "9";
	private static final String TEN = "10";
	
	private String selectedUserName;
	private String selectedFilterName = "";
	
	@Autowired
	private TransactionDetailBillService transactionDetailBillService;
	private boolean isHoldAction = false;
	
	public boolean getIsHoldAction() {
		return isHoldAction;
	}

	public void setHoldAction(boolean isHoldAction) {
		this.isHoldAction = isHoldAction;
	}
	
	public String getSelectedUserName() {
		return selectedUserName;
	}

	public void setSelectedUserName(String selectedUserName) {
		this.selectedUserName = selectedUserName;
	}
	
	public String getLastViewColumn() {
		return lastViewColumn;
	}

	public void setLastViewColumn(String lastViewColumn) {
		this.lastViewColumn = lastViewColumn;
	}

	public String getSelectedFilterName() {
		return selectedFilterName;
	}

	public void setSelectedFilterName(String selectedFilterName) {
		this.selectedFilterName = selectedFilterName;
	}

	public EditAction getCurrentAction() {
		return currentAction;
	}

	public void setCurrentAction(EditAction currentAction) {
		this.currentAction = currentAction;
	}

	public LocationMatrixService getLocationMatrixService() {
		return locationMatrixService;
	}

	public void setLocationMatrixService(LocationMatrixService locationMatrixService) {
		this.locationMatrixService = locationMatrixService;
	}

	public EntityItemService getEntityItemService() {
		return entityItemService;
	}

	public void setEntityItemService(EntityItemService entityItemService) {
		this.entityItemService = entityItemService;
	}
	
	public List<TransactionMatrixCompareDTO> getTransactionMatrixList() {
		return transactionMatrixList;
	}

	public void setTransactionMatrixList(List<TransactionMatrixCompareDTO> transactionMatrixList) {
		this.transactionMatrixList = transactionMatrixList;
	}

	public String getCompareToMatrixType() {
		return compareToMatrixType;
	}

	public void setCompareToMatrixType(String compareToMatrixType) {
		this.compareToMatrixType = compareToMatrixType;
	}

	public Long getCompareToMatrixId() {
		return compareToMatrixId;
	}

	public void setCompareToMatrixId(Long compareToMatrixId) {
		this.compareToMatrixId = compareToMatrixId;
	}


	public PurchaseTransaction getUpdatedTransaction() {
		return updatedTransaction;
	}

	public boolean isSuspendFlag() {
		return suspendFlag;
	}

	public void setSuspendFlag(boolean suspendFlag) {
		this.suspendFlag = suspendFlag;
	}

	public String getSuspendType() {
		return suspendType;
	}

	public void setSuspendType(String suspendType) {
		this.suspendType = suspendType;
	}

	public void setUpdatedTransaction(PurchaseTransaction updatedTransaction) {
		this.updatedTransaction = updatedTransaction;
	}

	public LocationMatrix getFilterSelectionLocationMatrixTemp() {
		return filterSelectionLocationMatrixTemp;
	}

	public void setFilterSelectionLocationMatrixTemp(
			LocationMatrix filterSelectionLocationMatrixTemp) {
		this.filterSelectionLocationMatrixTemp = filterSelectionLocationMatrixTemp;
	}

	public PurchaseTransaction getSelectedTransactionTemp() {
		return selectedTransactionTemp;
	}

	public void setSelectedTransactionTemp(
			PurchaseTransaction selectedTransactionTemp) {
		this.selectedTransactionTemp = selectedTransactionTemp;
	}

	public PurchaseTransactionLog getSelectedTransactionLog() {
		return selectedTransactionLog;
	}
	
	public void setSelectedTransaction(PurchaseTransaction selectedTransaction) {
		this.selectedTransaction = selectedTransaction;
	}

	public void setSelectedTransactionLog(
			PurchaseTransactionLog selectedTransactionLog) {
		this.selectedTransactionLog = selectedTransactionLog;
	}

	public TaxCodeService getTaxCodeService() {
		return taxCodeService;
	}

	public void setTaxCodeService(TaxCodeService taxCodeService) {
		this.taxCodeService = taxCodeService;
	}

	public PurchaseTransactionLogService getPurchaseTransactionLogService() {
		return purchaseTransactionLogService;
	}

	public void setPurchaseTransactionLogService(
			PurchaseTransactionLogService purchaseTransactionLogService) {
		this.purchaseTransactionLogService = purchaseTransactionLogService;
	}

	public int getTransLogHistoryTablePageNumber() {
		return transLogHistoryTablePageNumber;
	}

	public void setTransLogHistoryTablePageNumber(int transLogHistoryTablePageNumber) {
		this.transLogHistoryTablePageNumber = transLogHistoryTablePageNumber;
	}
	
	
	public int getSelectedTransactionLogIndex() {
		return selectedTransactionLogIndex;
	}

	public void setSelectedTransactionLogIndex(int selectedTransactionLogIndex) {
		this.selectedTransactionLogIndex = selectedTransactionLogIndex;
	}

	
	
	public String getReturnTransUrl() {
		return returnTransUrl;
	}

	public void setReturnTransUrl(String returnTransUrl) {
		this.returnTransUrl = returnTransUrl;
	}

	public TransactionDetailBean() {
		filterTaxCodeHandler = new TaxCodeCodeHandler("trans_main");
		newTaxCodeHandlerUpdate = new TaxCodeCodeHandler("trans_update");
	}
	
	public void init() {
		lastViewColumn = getLastView();
		//this.selectedUserName = matrixCommonBean.getLoginBean().getUserDTO().getUserCode();
		
		if(lastViewColumn!=null && lastViewColumn.length() > 0 && lastViewColumn.contains("/")){
			this.selectedColumn = lastViewColumn.substring(lastViewColumn.indexOf("/") + 1 ,lastViewColumn.length());
			this.selectedUserName = lastViewColumn.substring(0, lastViewColumn.indexOf("/"));
		} else if(lastViewColumn !=null && lastViewColumn.length() > 0) {
			this.selectedUserName = "*GLOBAL";
			this.selectedColumn = "DEFAULT";
		}
		setSelectedColumn(selectedUserName + "/" + selectedColumn);

	}
	
	public DownloadAddBean getDownloadAddBean() {
		return this.downloadAddBean;
	}

	public void setDownloadAddBean(DownloadAddBean downloadAddBean) {
		this.downloadAddBean = downloadAddBean;
	}
	
	public BatchMaintenanceDAO getBatchMaintenanceDAO() {
		return this.batchMaintenanceDAO;
	}

	public void setBatchMaintenanceDAO(BatchMaintenanceDAO batchMaintenanceDAO) {
		this.batchMaintenanceDAO = batchMaintenanceDAO;
	}

	public ViewTaxCodeBean getViewTaxCodeBean() {
		return viewTaxCodeBean;
	}

	public void setViewTaxCodeBean(ViewTaxCodeBean viewTaxCodeBean) {
		this.viewTaxCodeBean = viewTaxCodeBean;
	}

	public void searchTaxCodeInfoCallback(String taxcodeCountry, String taxcodeState, String taxcodeType, String taxcodeCode, String callingPage){
		viewTaxCodeBean.reset();
		viewTaxCodeBean.setTaxCode(taxcodeCountry, taxcodeState, taxcodeType, taxcodeCode);
		viewTaxCodeBean.setCallingPage(callingPage);
	}
	
	public String getApplyJurisdictionMessage() {
		return applyJurisdictionMessage;
	}

	public TransactionDetailService getTransactionDetailService() {
		return transactionDetailService;
	}

	public void setTransactionDetailService(TransactionDetailService transactionDetailService) {
		this.transactionDetailService = transactionDetailService;
	}
	
	public BatchMaintenanceService getBatchMaintenanceService() {
		return batchMaintenanceService;
	}

	public void setBatchMaintenanceService(BatchMaintenanceService batchMaintenanceService) {
		this.batchMaintenanceService = batchMaintenanceService;
		batchIdHandler.setBatchMaintenanceService(batchMaintenanceService);
	}

	public JurisdictionService getJurisdictionService() {
		return jurisdictionService;
	}
	
	public void setJurisdictionService(JurisdictionService jurisdictionService) {
		this.jurisdictionService = jurisdictionService;
	}
	
	public AdvancedFilterBean getAdvancedFilterBean() {
		return advancedFilterBean;
	}

	public void setAdvancedFilterBean(
			AdvancedFilterBean advancedFilterBean) {
		this.advancedFilterBean = advancedFilterBean;
	}
	
	public AdvancedSortBean getAdvancedSortBean() {
		return advancedSortBean;
	}

	public void setAdvancedSortBean(
			AdvancedSortBean advancedSortBean) {
		this.advancedSortBean = advancedSortBean;
	}
	
	public TransactionBlobBean getTransactionBlobBean() {
		return transactionBlobBean;
	}

	public void setTransactionBlobBean(
			TransactionBlobBean transactionBlobBean) {
		this.transactionBlobBean = transactionBlobBean;
	}
	
	public BatchMaintenance getDownloadBatchMaintenance() {
		return downloadBatchMaintenance;
	}

	public void setDownloadBatchMaintenance(BatchMaintenance downloadBatchMaintenance) {
		this.downloadBatchMaintenance = downloadBatchMaintenance;
	}

	public List<JurisdictionTaxrate> getJurisdictionTaxrateList() {
		return jurisdictionTaxrateList;
	}

	public void setJurisdictionTaxrateList(
			List<JurisdictionTaxrate> jurisdictionTaxrateList) {
		this.jurisdictionTaxrateList = jurisdictionTaxrateList;
	}
	
	public JurisdictionTaxrate getSelectedTaxRate() {
		return selectedTaxRate;
	}

	public int getSelectedTaxRateIndex() {
		return selectedTaxRateIndex;
	}

	public void setSelectedTaxRate(JurisdictionTaxrate selectedTaxRate) {
		this.selectedTaxRate = selectedTaxRate;
	}

	public void setSelectedTaxRateIndex(int selectedTaxRateIndex) {
		this.selectedTaxRateIndex = selectedTaxRateIndex;
	}
	
	// Method modified for 3987. New boolean variable added 
	public HtmlDataTable getTrDetailTable() {
		if (trDetailTable == null || filePreferenceBean.isMaxdecimalvalChanged()) {
			trDetailTable = new HtmlDataTable();
			trDetailTable.setId("aMDetailList");
			trDetailTable.setVar("trdetail");
			trDetailTable.setRowClasses("odd-row,even-row");
			//matrixCommonBean.createTransactionDetailTable(trDetailTable,
			//		"transactionDetailBean", "transactionDetailBean.transactionDataModel", 
			//		matrixCommonBean.getTransactionPropertyMap(), true, displayAllFields, false, false, true);
			
//			if(selectedColumn!=null && selectedColumn.length() > 0 && selectedColumn.contains("/")){
//				init();
//			}
			String maxdecimalplace = filePreferenceBean.getMaxdecimalplace();
			if(selectedColumn == null || selectedColumn.isEmpty()) {
				setSelectedColumn("*GLOBAL/DEFAULT");
			}
			String searchColumnName = selectedColumn.substring(selectedColumn.indexOf("/") + 1);
			matrixCommonBean.createViewTransactionDetailTable(trDetailTable,
					"transactionDetailBean", "transactionDetailBean.transactionDataModel", searchColumnName, selectedUserName, true, maxdecimalplace);

			//			if(lastViewColumn.indexOf("/") != -1)
//				lastViewColumn = lastViewColumn.substring(lastViewColumn.indexOf("/") + 1);
		}
		taxMatrixBean.setNavigatedTransactionProcessScreen(Boolean.TRUE);
		locationMatrixBean.setNavigatedTransactionProcessScreen(Boolean.TRUE);
		taxMatrixBean.setSelectedColumn(selectedColumn);
		locationMatrixBean.setSelectedColumn(selectedColumn);
		if(lastViewColumn == null || lastViewColumn.length() == 0 || lastViewColumn.indexOf("/") == -1) {
			lastViewColumn = "*GLOBAL/DEFAULT";
		}
		selectedUserName = lastViewColumn.substring(0, lastViewColumn.indexOf("/"));
		selectedColumn = lastViewColumn.substring(lastViewColumn.indexOf("/") + 1);
		setSelectedColumn(selectedUserName + "/" + selectedColumn);

		return trDetailTable;
	}

	public void setTrDetailTable(HtmlDataTable trDetailTable) {
		this.trDetailTable = trDetailTable;
	}
	
	public void showOk(){
		setShowOkButton(true);
		
	}
	
	  public byte getGridOption() {
		  Option option = new Option();
		
			String userCode = null;
			try {
				userCode = matrixCommonBean.getLoginBean().getUserDTO().getUserCode();
			}
			catch(Exception e) {}
			
		    OptionCodePK optionCodePK = new OptionCodePK();
		    optionCodePK.setOptionCode(optionCode);
		    optionCodePK.setOptionTypeCode(user);
		    optionCodePK.setUserCode(userCode);
		    option = optionService.findByPK(optionCodePK);
		    if(option.getValue()==null || option.getValue()==""){
		    	return 2;
		    }
		    return Byte.parseByte(option.getValue());
		  }
	  
	  public void prepareReorderAction(){
		  setShowOkButton(false);
		   if(getGridOption() ==-1){
			 setRenderOptionPanel(true);
		  }
	  }
	  

	  
	  public void saveGridAction(){
		  setSaveNow(true);
		  reArrangeColumns();
	  }
	  
	  public void doNotSave(){
		  setSaveNow(false);
		  reArrangeColumns();
	  }
	  
	  
	  public boolean saveGridLayout(){
			boolean saveGrid = false;
			if(getGridOption() == 1){
				saveGrid = false;
			}
			if (getGridOption() == 0  || saveNow) {
				saveGrid = true;
			}
		  
		  
		  return saveGrid;
	  }
	
	
	// Method which calls the headed re order - 3987
	public void reArrangeColumns(){
		if(matrixCommonBean.getForHeaderRearrangeTemp() != null) {
			matrixCommonBean.setForHeaderRearrange(matrixCommonBean.getForHeaderRearrangeTemp());
		}
	
		matrixCommonBean.createTransactionDetailTable(trDetailTable,
				"transactionDetailBean", "transactionDetailBean.transactionDataModel", 
				matrixCommonBean.getTransactionPropertyMap(), true, displayAllFields, true, saveGridLayout(), false);
			logger.info("Save Pref. "+saveGridLayout());	
	}
	
	public void refreshColumns() {
		matrixCommonBean.refreshColumns(displayAllFields);
		showOk();
	}
	
	public void resetForHeaderRearrangeTemp() {
		matrixCommonBean.setForHeaderRearrangeTemp(null);
	}

	public HtmlPanelGrid getFilterSelectionPanel() {
		// panel construction a function of current entity setting, so it cannot be created once... create each time...
//		if (true || (filterSelectionPanel == null)) {
			filterSelectionPanel = MatrixCommonBean.createDriverPanel(
					TaxMatrix.DRIVER_CODE, matrixCommonBean.getCacheManager(), 
					matrixCommonBean.getUserEntityService().getEntityItemDTO(),
					"filterPanel", 
					"transactionDetailBean", "filterSelectionTaxMatrix", "matrixCommonBean", 
					false, false, false, true, true); 
			
			// Add in location matrix drivers
			MatrixCommonBean.createDriverPanel(
					LocationMatrix.DRIVER_CODE, 
					matrixCommonBean.getCacheManager(),
					matrixCommonBean.getUserEntityService().getEntityItemDTO(),
					filterSelectionPanel, 
					"transactionDetailBean", "filterSelectionLocationMatrix", "matrixCommonBean", 
					false, false, false, true, true);
//		}
		matrixCommonBean.prepareTaxDriverPanel(filterSelectionPanel);
		return filterSelectionPanel;
	}
	
	public void setFilterSelectionPanel(HtmlPanelGrid filterSelectionPanel) {
		this.filterSelectionPanel = filterSelectionPanel;
	}
	
	public HtmlPanelGrid getTaxDriverViewPanel() {
		if (taxDriverViewPanel == null) {
			taxDriverViewPanel = MatrixCommonBean.createDriverPanel(
					TaxMatrix.DRIVER_CODE, matrixCommonBean.getCacheManager(), 
					matrixCommonBean.getUserEntityService().getEntityItemDTO(),
					"taxDriverViewPanel", 
					"transactionDetailBean", "currentTransaction", "matrixCommonBean", 
					true, false, true, false, false);
		}
		matrixCommonBean.prepareTaxDriverPanel(taxDriverViewPanel);
		return taxDriverViewPanel;
	}

	public void setTaxDriverViewPanel(HtmlPanelGrid taxDriverViewPanel) {
		this.taxDriverViewPanel = taxDriverViewPanel;
	}

	public HtmlPanelGrid getLocationDriverViewPanel() {
		if (locationDriverViewPanel == null) {
			locationDriverViewPanel = MatrixCommonBean.createDriverPanel(
					LocationMatrix.DRIVER_CODE, matrixCommonBean.getCacheManager(), 
					matrixCommonBean.getUserEntityService().getEntityItemDTO(),
					"locationDriverViewPanel", 
					"transactionDetailBean", "currentTransaction", "matrixCommonBean", 
					true, false, true, false, false);
		}
		
		return locationDriverViewPanel;
	}

	public void setLocationDriverViewPanel(HtmlPanelGrid locationDriverViewPanel) {
		this.locationDriverViewPanel = locationDriverViewPanel;
	}

	public MatrixCommonBean getMatrixCommonBean() {
		return matrixCommonBean;
	}

	public void setMatrixCommonBean(MatrixCommonBean matrixCommonBean) {
		this.matrixCommonBean = matrixCommonBean;
		
		transactionStatus = matrixCommonBean.getUserPreferenceDTO().getUserTransInd();
		
		//4764 set default
		if(transactionStatus==null || transactionStatus.length()==0){
			transactionStatus = "*ALL";
		}
		
		filterTaxCodeHandler.setTaxCodeDetailService(matrixCommonBean.getTaxCodeDetailService());
		
		//0004440
		filterTaxCodeHandler.setTaxCodeBackingBean(taxCodeBackingBean);
		filterTaxCodeHandler.setReturnRuleUrl("transactions_process");
		newTaxCodeHandler.setTaxCodeBackingBean(taxCodeBackingBean);
		newTaxCodeHandler.setReturnRuleUrl("apply_taxcode");	
		newTaxCodeHandlerUpdate.setTaxCodeBackingBean(taxCodeBackingBean);
		newTaxCodeHandlerUpdate.setReturnRuleUrl("trans_update");

		
		//MF - do not set default country
		//filterTaxCodeHandler.reset();
		//filterTaxCodeHandler.setTaxcodeCountry(getMatrixCommonBean().getUserPreferenceDTO().getUserCountry());
		//filterTaxCodeHandler.setTaxcodeState("");
		//filterTaxCodeHandler.rebuildMenus();
		
		newTaxCodeHandler.setTaxCodeDetailService(matrixCommonBean.getTaxCodeDetailService());
		newTaxCodeHandlerUpdate.setTaxCodeDetailService(matrixCommonBean.getTaxCodeDetailService());	
		filterTaxCodeHandler.setCacheManager(matrixCommonBean.getCacheManager());
		newTaxCodeHandler.setCacheManager(matrixCommonBean.getCacheManager());
		newTaxCodeHandlerUpdate.setCacheManager(matrixCommonBean.getCacheManager());

		/*currentJurisdictionHandler.setJurisdictionService(matrixCommonBean.getJurisdictionService());
		currentJurisdictionHandler.setCacheManager(matrixCommonBean.getCacheManager());*/
		newJurisdictionHandler.setJurisdictionService(matrixCommonBean.getJurisdictionService());
		newJurisdictionHandler.setCacheManager(matrixCommonBean.getCacheManager());
		newJurisdictionHandler.setTaxJurisdictionBean(taxJurisdictionBean);
		newJurisdictionHandler.setCallbackScreen("apply_jurisdiction");
		
		newJurisdictionHandlerModify.setJurisdictionService(matrixCommonBean.getJurisdictionService());
		newJurisdictionHandlerModify.setCacheManager(matrixCommonBean.getCacheManager());
		newJurisdictionHandlerModify.setTaxJurisdictionBean(taxJurisdictionBean);
		newJurisdictionHandlerModify.setCallbackScreen("trans_update");
		
		jurisdictionHandler.setJurisdictionService(matrixCommonBean.getJurisdictionService());
		jurisdictionHandler.setCacheManager(matrixCommonBean.getCacheManager());

		batchIdHandler.setCacheManager(matrixCommonBean.getCacheManager());
		
		transactionViewBean.setTxnLocnHandler(transactionViewBean.getTxnLocnShiptoJurisdictionHandler());
		transactionViewBean.setTxnLocnHandler(transactionViewBean.getTxnLocnShipfromJurisdictionHandler());
		transactionViewBean.setTxnLocnHandler(transactionViewBean.getTxnLocnOrdrorgnJurisdictionHandler());
		transactionViewBean.setTxnLocnHandler(transactionViewBean.getTxnLocnOrdracptJurisdictionHandler());
		transactionViewBean.setTxnLocnHandler(transactionViewBean.getTxnLocnFirstuseJurisdictionHandler());
		transactionViewBean.setTxnLocnHandler(transactionViewBean.getTxnLocnBilltoJurisdictionHandler());
		transactionViewBean.setTxnLocnHandler(transactionViewBean.getTxnLocnTtlxfrJurisdictionHandler());
	}
	
	public void update(Observable obs, Object obj) {
		if (obs == ov) {
			newJurisdictionHandler.setChanged();
			jurisdictionHandler.setChanged();
		}
	}
	
	public void valueBound(HttpSessionBindingEvent event){
		StateObservable ov = StateObservable.getInstance();
		this.ov = ov;
		this.ov.addObserver(this);
	}

	public void valueUnbound(HttpSessionBindingEvent event){	
		if(ov!=null){
			ov.deleteObserver(this);
		}
	}

	public TransactionViewBean getTransactionViewBean() {
		return transactionViewBean;
	}

	public void setTransactionViewBean(TransactionViewBean transactionViewBean) {
		this.transactionViewBean = transactionViewBean;
	}

	public AllocationMatrixBackingBean getAllocationMatrixBean() {
		return allocationMatrixBean;
	}

	public void setAllocationMatrixBean(
			AllocationMatrixBackingBean allocationMatrixBean) {
		this.allocationMatrixBean = allocationMatrixBean;
	}

	public LocationMatrixBackingBean getLocationMatrixBean() {
		return locationMatrixBean;
	}

	public void setLocationMatrixBean(LocationMatrixBackingBean locationMatrixBean) {
		this.locationMatrixBean = locationMatrixBean;
	}

	public TaxMatrixViewBean getTaxMatrixBean() {
		return taxMatrixBean;
	}

	public void setTaxMatrixBean(TaxMatrixViewBean taxMatrixBean) {
		this.taxMatrixBean = taxMatrixBean;
	}

	public TaxJurisdictionBackingBean getTaxJurisdictionBean() {
		return taxJurisdictionBean;
	}

	public void setTaxJurisdictionBean(TaxJurisdictionBackingBean taxJurisdictionBean) {
		this.taxJurisdictionBean = taxJurisdictionBean;
	}

	public TaxMatrix getFilterSelectionTaxMatrix() {
		return filterSelectionTaxMatrix;
	}

	public LocationMatrix getFilterSelectionLocationMatrix() {
		return filterSelectionLocationMatrix;
	}

	public Date getFromGLDate() {
		return fromGLDate;
	}

	public void setFromGLDate(Date fromGLDate) {
		this.fromGLDate = fromGLDate;
	}

	public Date getToGLDate() {
		return toGLDate;
	}

	public void setToGLDate(Date toGLDate) {
		this.toGLDate = toGLDate;
	}

	public Long getPurchtransId() {
		return purchtransId;
	}

	public void setPurchtransId(Long purchtransId) {
		this.purchtransId = purchtransId;
	}
	
	public Long getJurisdictionId() {
		return jurisdictionId;
	}

	public void setJurisdictionId(Long jurisdictionId) {
		this.jurisdictionId = jurisdictionId;
	}

	public Long getTaxMatrixId() {
		return taxMatrixId;
	}

	public void setTaxMatrixId(Long taxMatrixId) {
		this.taxMatrixId = taxMatrixId;
	}

	public Long getLocationMatrixId() {
		return locationMatrixId;
	}

	public void setLocationMatrixId(Long locationMatrixId) {
		this.locationMatrixId = locationMatrixId;
	}

	public Long getGlExtractId() {
		return glExtractId;
	}

	public void setGlExtractId(Long glExtractId) {
		this.glExtractId = glExtractId;
	}

	public Long getProcessBatchId() {
		return processBatchId;
	}

	public void setProcessBatchId(Long processBatchId) {
		this.processBatchId = processBatchId;
	}

	
	public TaxCodeCodeHandler getFilterTaxCodeHandler() {
		return filterTaxCodeHandler;
	}

	public int getSelectedRowIndex() {
		return selectedRowIndex;
	}

	public void setSelectedRowIndex(int selectedRowIndex) {
		this.selectedRowIndex = selectedRowIndex;
	}

	public List<SelectItem> getStatusItems() {
        // changes by MBF for PP-234
        if (statusItems == null) {
           statusItems = new ArrayList<SelectItem>();
           //statusItems.add(new SelectItem("", "Select Transaction Status"));
           statusItems.add(new SelectItem("*ALL", "All"));
           statusItems.add(new SelectItem("P",  "&#160;&#160;&#160;Processed"));
           statusItems.add(new SelectItem("PO", "&#160;&#160;&#160;&#160;&#160;&#160;Open"));
           statusItems.add(new SelectItem("PE", "&#160;&#160;&#160;&#160;&#160;&#160;Exempt"));
           statusItems.add(new SelectItem("PC", "&#160;&#160;&#160;&#160;&#160;&#160;Closed"));
           // removed: statusItems.add(new SelectItem("U", "&#160;&#160;&#160;Unprocessed"));
           statusItems.add(new SelectItem("US", "&#160;&#160;&#160;Suspended"));
           statusItems.add(new SelectItem("USL","&#160;&#160;&#160;&#160;&#160;&#160;No Location Matrix"));
           statusItems.add(new SelectItem("UST","&#160;&#160;&#160;&#160;&#160;&#160;No G&S Matrix"));
           statusItems.add(new SelectItem("USD","&#160;&#160;&#160;&#160;&#160;&#160;No TaxCode Rule"));
           statusItems.add(new SelectItem("USR","&#160;&#160;&#160;&#160;&#160;&#160;No Tax Rate"));
           statusItems.add(new SelectItem("UH", "&#160;&#160;&#160;Held"));
           statusItems.add(new SelectItem("UHN","&#160;&#160;&#160;&#160;&#160;&#160;Normal Held"));
           statusItems.add(new SelectItem("FS", "&#160;&#160;&#160;&#160;&#160;&#160;Future Split"));
           statusItems.add(new SelectItem("PR", "&#160;&#160;&#160;Original"));
           for (SelectItem item : statusItems) {
                  item.setEscape(false);
           }
        }
        return statusItems;
	}
	
	public String getTransactionStatus() {
		return transactionStatus;
	}

	public void setTransactionStatus(String transactionStatus) {
		this.transactionStatus = transactionStatus;
	}
	
	public String getSelectedFilter() {
		return selectedFilter;
	}

	public void setSelectedFilter(String selectedFilter) {
		this.selectedFilter = selectedFilter;
	}
	
	public String getSelectedColumn() {
		return selectedColumn;
	}

	public void setSelectedColumn(String selectedColumn) {
		this.selectedColumn = selectedColumn;
	}
	
	public String getSelectedView() {
		return selectedView;
	}

	public void setSelectedView(String selectedView) {
		this.selectedView = selectedView;
	}
	
	public boolean isDisplayAllFields() {
		return displayAllFields;
	}

	public void setDisplayAllFields(boolean displayAllFields) {
		this.displayAllFields = displayAllFields;
	}

	public boolean getTaxMatrixIdDisabled() {
		return ((transactionStatus != null) && transactionStatus.equalsIgnoreCase("UST"));
	}

	public boolean getLocationMatrixIdDisabled() {
		return ((transactionStatus != null) && transactionStatus.equalsIgnoreCase("USL"));
	}
	
	public boolean getTaxCodeDisabled() {
		//return ((transactionStatus == null) || !transactionStatus.startsWith("P"));
		//return ((transactionStatus == "") || transactionStatus.equalsIgnoreCase("U")|| transactionStatus.equalsIgnoreCase("US")|| transactionStatus.startsWith("USL")
		//		|| transactionStatus.startsWith("UST")|| transactionStatus.startsWith("UH") || transactionStatus.startsWith("FS"));
		
		//0002882
		return ((transactionStatus == "") || transactionStatus.equalsIgnoreCase("U")|| transactionStatus.equalsIgnoreCase("US")
				|| transactionStatus.startsWith("UST"));
	}
	
	public boolean getHeldTransaction() {
		PurchaseTransaction trDetail = getCurrentTransaction();
		if (trDetail != null) {
			String status = trDetail.getTransactionInd();
			return ((status != null) && status.equalsIgnoreCase("H"));
		}
		return false;
	}

	public boolean getUpdateDisabled() {
		for (PurchaseTransaction trDetail : transactionDataModel.getItems(getSelectedIds())) {
			String status = trDetail.getTransactionInd();
			if ((status != null) && status.equalsIgnoreCase("H")) {
				return true;
			}
		}

		return (getSelectionCount() == 0);
	}
	
	//0005238
	public boolean getReprocessDisabled() {
		for (PurchaseTransaction trDetail : transactionDataModel.getItems(getSelectedIds())) {
			String status = trDetail.getTransactionInd();

		}
		return (getSelectionCount() == 0);
	}	

	public boolean getSuspendDisabled() {
		for (PurchaseTransaction trDetail : transactionDataModel.getItems(getSelectedIds())) {
			if("TRANSACTION Ind. Error".equalsIgnoreCase(trDetail.getProcessStatus())){
				return false;
			}
			
			String suspendInd = trDetail.getSuspendInd();
			String status = trDetail.getTransactionInd();
			
			//0004008
			boolean rateSuspend = false;
			if(suspendInd!=null && suspendInd.equalsIgnoreCase("R") && status!=null && status.equalsIgnoreCase("S")){
				rateSuspend = true;
			}
			
			//0002615
			boolean taxSuspend = false;
			if(suspendInd!=null && suspendInd.equalsIgnoreCase("T") && status!=null && status.equalsIgnoreCase("S") && trDetail.getShiptoJurisdictionId()!=null && trDetail.getShiptoJurisdictionId()>0l){
				taxSuspend = true;
			}
			
			//0004343
			boolean taxCodeRuleSuspend = false;
			if(suspendInd!=null && suspendInd.equalsIgnoreCase("D") && status!=null && status.equalsIgnoreCase("S")){
				taxCodeRuleSuspend = true;
			}
			
			if ((status == null) || (!status.equalsIgnoreCase("P") && !rateSuspend  && !taxSuspend && !taxCodeRuleSuspend)) {
				return true;
			}
		}

		return (getSelectionCount() == 0);
	}
	
	public boolean getApplyTaxMatrixDisabled() {
		for (PurchaseTransaction trDetail : transactionDataModel.getItems(getSelectedIds())) {
			String status = trDetail.getTransactionInd();
			if ((status == null) || !status.equalsIgnoreCase("S")) {
				return true;
			}
			
			String suspend = trDetail.getSuspendInd();
			if ((suspend == null) || !suspend.equalsIgnoreCase("T")) {
				return true;
			}
		}

		return (getSelectionCount() == 0);
	}
	
	public boolean getApplyLocationMatrixDisabled() {
		for (PurchaseTransaction trDetail : transactionDataModel.getItems(getSelectedIds())) {
			String status = trDetail.getTransactionInd();
			if ((status == null) || !status.equalsIgnoreCase("S")) {
				return true;
			}
			
			String suspend = trDetail.getSuspendInd();
			if ((suspend == null) || !suspend.equalsIgnoreCase("L")) {
				return true;
			}
		}

		return (getSelectionCount() == 0);
	}
	
	//0002615
	public boolean getSetSuspendTaxMatrix() {	
		for (PurchaseTransaction trDetail : transactionDataModel.getItems(getSelectedIds())) {
			String suspendInd = trDetail.getSuspendInd();
			String status = trDetail.getTransactionInd();
			
			//0002615
			if(suspendInd!=null && suspendInd.equalsIgnoreCase("T") && status!=null && status.equalsIgnoreCase("S") && trDetail.getShiptoJurisdictionId()!=null && trDetail.getShiptoJurisdictionId()>0l){
				//At least one T, S, JurisdictionId >0
				return true;
			}
		}

		return false;
	}
	
	public boolean getCopyAddDisabled() {
		PurchaseTransaction trDetail = getCurrentTransaction();
		Long id = (trDetail == null)? null:trDetail.getTaxMatrixId();
		return ((id == null) || (id <= 0L));
	}
	
	public Boolean getMultiSelect() {
		return ((transactionStatus != null) && 
				!transactionStatus.equals("U") &&
				!transactionStatus.equals("US") &&
				!transactionStatus.equals("UH"));
	}
	
	public int getSelectionCount() {
		return Math.max(getMultiSelect()? getSelectedIds().size():0,
				(selectedTransaction != null)? 1:0);
	}
	
	public String getMultiTypeSelected(){
		return multiTypeSelected;
	}
	
	private List<Long> getSelectedCommentIds() {
		if (getMultiSelect() && (transactionDataModel.getSelectionMap().size() > 0)) {
			List<Long> result = new ArrayList<Long>();
			StringBuffer sb =  new StringBuffer();
			//go over everything that has been selected, and if it's true add it in
			for (Map.Entry<Long, Boolean> e : transactionDataModel.getSelectionMap().entrySet()) {
				if (logger.isDebugEnabled() && e.getValue()) {
					if (sb.length() > 0) sb.append(", ");
					sb.append(e.getKey().toString());
				}
				if (e.getValue()) {
					result.add(e.getKey());
				}
			}
			return result;
		} else {
			List<Long> result = new ArrayList<Long>();
			if (selectedTransaction != null) {
				result.add(selectedTransaction.getPurchtransId());
			}
			return result;
		}
	}
	/*
	 * 3922 changed this to lists instead of sets in order to preserve an order... 
	 */
	private List<Long> getSelectedIds() {
		if(selectedIds!=null){
			return selectedIds;
		}
		
		multiTypeSelected = "";
		String initType = "";
		
		selectedIds = new ArrayList<Long>();
		if (getMultiSelect() && (transactionDataModel.getSelectionMap().size() > 0) && selectedTransaction != null) {	
			//go over everything that has been selected, and if it's true add it in
			
			//PP-156 for the same trans type as selectedTransaction
			String status = (transactionStatus == null)? "":transactionStatus;
			String selectedStatus = "";
			if(status.length()==0 || status.equalsIgnoreCase("*ALL")){
				selectedStatus = selectedTransaction.getProcessStatus();
			}

			String tempStatus = "";
			boolean multiTypeFound = false;
			for (Map.Entry<Long, Boolean> e : transactionDataModel.getSelectionMap().entrySet()) {
				if (e.getValue()) {
					if(selectedStatus.length()>0){
						PurchaseTransaction trDetail = transactionDataModel.getById(e.getKey());;
						tempStatus = trDetail.getProcessStatus();
						if(selectedStatus.equalsIgnoreCase(tempStatus)) {
							selectedIds.add(e.getKey());
						}
						
						if(multiTypeSelected.length()==0){
							if(initType.length()==0){
								initType = tempStatus;		
							}
							else{
								if(!tempStatus.equalsIgnoreCase(initType)){
									multiTypeFound = true;
								}
							}
							
							if(multiTypeFound && selectedIds.size()>0){
								multiTypeSelected=selectedTransaction.getProcessStatus();						
							}
						}		
					}
					else{
						selectedIds.add(e.getKey());
					}
				}
			}
			
			if(selectedIds.size()>0){
				return selectedIds;
			}
		} 
				
		if (selectedTransaction != null) {
			selectedIds.add(selectedTransaction.getPurchtransId());
		}
		
		return selectedIds;	
	}
	
	public PurchaseTransaction getSelectedTransaction() {
		return selectedTransaction;
	}
	
	public boolean isNullTransInd() {
		return selectedTransaction != null && selectedTransaction.getTransactionInd() == null;
	}
	
	public boolean isOriginalSplit() {
		return selectedTransaction != null && selectedTransaction.getMultiTransCode() != null && selectedTransaction.getMultiTransCode().matches("O(.*)");
	}
	
	public boolean isMultiStatusTypeChecked() {
		return (multiTypeSelected != null && multiTypeSelected.length()>0);
	}
	
	public boolean isTransactionIndError() {
		return selectedTransaction != null && ("TRANSACTION Ind. Error".equalsIgnoreCase(selectedTransaction.getProcessStatus()));
	}
	
	public boolean isTransactionProcessed() {
		return selectedTransaction != null && ("P".equalsIgnoreCase(selectedTransaction.getTransactionInd()));
	}
	
	public boolean getValidSelection() {
		return (getSelectionCount() > 0);
	}
	
	public boolean isTransactionProcessedOpen() {
		String selectedStatus = selectedTransaction.getProcessStatus();
		if(selectedTransaction != null && ((selectedStatus != null) && "Processed/Open".equalsIgnoreCase(selectedStatus))){
			return false;
		}
		return true;
	}
	
	public boolean getValidForComment() {
		return (selectedTransaction != null);
	}
	
	public boolean getValidLogSelection() {
		return selectedTransactionLogIndex != -1 ? true:false;
	}
	
	public String transProcessCloseAction() {
		this.selectedTransactionLogIndex = -1;	
		return "transactions_process";
	}
	
	public String getPageDescription() {
		int rows = transactionLogDetailList == null ? 0 : transactionLogDetailList.size();
		int start = rows == 0 ? 0 : ((transLogHistoryTablePageNumber - 1) * transLogHistoryTablePageSize) + 1;
		int end = transLogHistoryTablePageNumber * transLogHistoryTablePageSize;
		if(end > rows) {
			end = rows;
		}
		
		String desc = String.format("Displaying rows %d through %d (%d matching rows)", start, end, rows);
		return desc;
	}
	
	
	public boolean getValidSplitSelection() {
		return selectedSplitTransaction != null;
	}
	
	public boolean getDisplaySplitGroupViewSplit() {
		return showSplitGroupViewSplitButton;
	}
	
	public String splitGroupViewSplitAction() {
		PurchaseTransaction trDetail = new PurchaseTransaction();
		if(this.selectedSplitTransaction.getSplitSubtransId() != null && this.selectedSplitTransaction.getSplitSubtransId() > 0L) {
			trDetail.setSplitSubtransId(this.selectedSplitTransaction.getSplitSubtransId());
		}
		else {
			trDetail.setPurchtransId(this.selectedSplitTransaction.getPurchtransId());
		}
		splitGroupDataModel.setFilterCriteria(trDetail, null, null, null, null, null, null);
		showSplitGroupViewSplitButton = false;
		return "view_split_set";
	}
	
	public String splitGroupViewAllocAction() {
		selectedSplitRowIndex = -1;
		
		PurchaseTransaction trDetail = new PurchaseTransaction();
		if(this.selectedSplitTransaction.getSplitSubtransId() != null && this.selectedSplitTransaction.getSplitSubtransId() > 0L) {
			trDetail.setAllocationSubtransId(this.selectedSplitTransaction.getAllocationSubtransId());
		}
		else {
			trDetail.setPurchtransId(this.selectedSplitTransaction.getPurchtransId());
		}
		splitGroupDataModel.setFilterCriteria(trDetail, null, null, null, null, null, null);
		this.showSplitGroupViewSplitButton = true;
		return "view_split_set";
	}
	
	public boolean getDisplaySplitGroupViewAlloc() {
		return selectedSplitTransaction != null && "OA".equalsIgnoreCase(selectedSplitTransaction.getMultiTransCode()) && !showSplitGroupViewSplitButton;
	}
	
	public boolean getDisplaySplit() {
		return this.selectedTransaction != null && 
			(
				"H".equalsIgnoreCase(this.selectedTransaction.getTransactionInd()) ||
				"S".equalsIgnoreCase(this.selectedTransaction.getTransactionInd())
			) &&
			this.selectedTransaction.getGlLineItmDistAmt() != null && 
			this.selectedTransaction.getGlLineItmDistAmt().abs().compareTo(BigDecimal.ZERO) >0 &&
			!"OS".equals(this.selectedTransaction.getMultiTransCode()) &&
			!"S".equals(this.selectedTransaction.getMultiTransCode()) &&
			!"OA".equals(this.selectedTransaction.getMultiTransCode()) &&
			!"A".equals(this.selectedTransaction.getMultiTransCode());
	}
	
	public boolean getDisplayViewSplit() {
		return this.selectedTransaction != null && 
			(
			"OS".equals(this.selectedTransaction.getMultiTransCode()) ||
			"S".equals(this.selectedTransaction.getMultiTransCode()) ||
			"OA".equals(this.selectedTransaction.getMultiTransCode()) ||
			"A".equals(this.selectedTransaction.getMultiTransCode())
			);
	}
	
	public String viewSplitAction() {
		currentAction = EditAction.VIEW_SPLIT;
		returnTransUrl = "transactions_process";
		selectedSplitRowIndex = -1;
		selectedSplitTransaction = null;
		selectedSplitTransactionTemp = null;
		PurchaseTransaction trDetail = new PurchaseTransaction();
		if(this.selectedTransaction.getSplitSubtransId() != null && this.selectedTransaction.getSplitSubtransId() > 0L) {
			trDetail.setSplitSubtransId(this.selectedTransaction.getSplitSubtransId());
		}
		else {
			trDetail.setPurchtransId(this.selectedTransaction.getPurchtransId());
		}
		splitGroupDataModel.setFilterCriteria(trDetail, null, null, null, null, null, null);
		showSplitGroupViewSplitButton = false;
		return "view_split_set";
	}
	
	/**
	 * This is actually returning if the select all checkbox is checked -
	 * 
	 */
	public boolean isAllSelected() {
		return indicateAllSelected;
	}
	public void setAllSelected(boolean indicateAllSelected) {
		this.indicateAllSelected = indicateAllSelected;
	}
	public void selectAllChange(ActionEvent e){
		HtmlSelectBooleanCheckbox check = (HtmlSelectBooleanCheckbox) ((HtmlAjaxSupport) e.getComponent()).getParent();
		transactionDataModel.tooggleSelectAll((Boolean)check.getValue(), MAX_SELECT_ALL);
		updateFieldSum();
	}
	public boolean isSelectAllEnabled(){
		return true;
		//we used to use this as a limiter...
		//return (transactionDataModel.getRowCount() < MAX_SELECT_ALL); 
	}
	
	public String getSelectAllCaption(){
		if(transactionDataModel.getRowCount() <= MAX_SELECT_ALL){
			return "Select All";
		} else {
			return "Select First "+MAX_SELECT_ALL;
		}
	}
	
	/**
	 * A warning is posted if they hit select all but not all were actually selected...
	 */
	public String getSelectAllWarning(){
		if( indicateAllSelected  && transactionDataModel.getRowCount() > MAX_SELECT_ALL){
			return "- Warning: "+MAX_SELECT_ALL + " record selection limit";
		} else {
			return "";			
		}
	}
	
	public void resetDetailTable(){
		
		String rowStr = (displayAllFields)? matrixCommonBean.getMatrixDTO().getSysFullThreshold() : 
			matrixCommonBean.getMatrixDTO().getSysLimitedThreshold();
		int rowpage = 0;
		if(rowStr==null || rowStr.length()==0){
			rowpage = (displayAllFields)? 25:100; 
		}
		else{
			rowpage = Integer.parseInt(rowStr);
		}
		
		String muitipleStr = (displayAllFields)? matrixCommonBean.getMatrixDTO().getSysFullThresholdPages() : 
			matrixCommonBean.getMatrixDTO().getSysLimitedThresholdPages();
		int muitiplepage = 0;
		if(muitipleStr==null || muitipleStr.length()==0){
			muitiplepage = (displayAllFields)? 4:4; 
		}
		else{
			muitiplepage = Integer.parseInt(muitipleStr);
		}
		
		if(rowpage==transactionDataModel.getPageSize() && muitiplepage==transactionDataModel.getDbFetchMultiple()){
			return;
		}
		
		// Rebuild the table
		logger.debug("Rebuilding table - resetDetailTable: " + displayAllFields);
		matrixCommonBean.createTransactionDetailTable(trDetailTable,
				"transactionDetailBean", "transactionDetailBean.transactionDataModel", 
				matrixCommonBean.getTransactionPropertyMap(), true, displayAllFields, false, false, true); 
		
		transactionDataModel.setPageSize(rowpage);
		transactionDataModel.setDbFetchMultiple(muitiplepage);
		
		resetForHeaderRearrangeTemp();
		
		LogMemory.executeGC();
	}
	
	//Albert Chen ****************************
	public void displayViewTransChange(ActionEvent e) {
		HtmlSelectBooleanCheckbox check = (HtmlSelectBooleanCheckbox) ((HtmlAjaxSupport) e.getComponent()).getParent();
	    displayAllFields = (check.getSubmittedValue() == null)? 
	    		((check.getValue() == null)? false:(Boolean) check.getValue()):Boolean.parseBoolean((String) check.getSubmittedValue());
		// Rebuild the table
		matrixCommonBean.createViewTransactionDetailTable(trDetailTable,
				"transactionDetailBean", "transactionDetailBean.transactionDataModel", "*GLOBAL/*LIMITED", selectedUserName, true, null);
		
		String rowStr = (displayAllFields)? matrixCommonBean.getMatrixDTO().getSysFullThreshold() : 
			matrixCommonBean.getMatrixDTO().getSysLimitedThreshold();
		int rowpage = 0;
		if(rowStr==null || rowStr.length()==0){
			rowpage = (displayAllFields)? 25:100; 
		}
		else{
			rowpage = Integer.parseInt(rowStr);
		}
		
		String muitipleStr = (displayAllFields)? matrixCommonBean.getMatrixDTO().getSysFullThresholdPages() : 
			matrixCommonBean.getMatrixDTO().getSysLimitedThresholdPages();
		int muitiplepage = 0;
		if(muitipleStr==null || muitipleStr.length()==0){
			muitiplepage = (displayAllFields)? 4:4; 
		}
		else{
			muitiplepage = Integer.parseInt(muitipleStr);
		}
		
		transactionDataModel.setPageSize(rowpage);
		transactionDataModel.setDbFetchMultiple(muitiplepage);
		
		resetForHeaderRearrangeTemp();
		
		LogMemory.executeGC();
	}
	
	// 3987. New boolean variable added
	public void displayChange(ActionEvent e) {
		HtmlSelectBooleanCheckbox check = (HtmlSelectBooleanCheckbox) ((HtmlAjaxSupport) e.getComponent()).getParent();
	    displayAllFields = (check.getSubmittedValue() == null)? 
	    		((check.getValue() == null)? false:(Boolean) check.getValue()):Boolean.parseBoolean((String) check.getSubmittedValue());
		// Rebuild the table
		logger.debug("Rebuilding table - all fields: " + displayAllFields);
		matrixCommonBean.createTransactionDetailTable(trDetailTable,
				"transactionDetailBean", "transactionDetailBean.transactionDataModel", 
				matrixCommonBean.getTransactionPropertyMap(), true, displayAllFields, false, false, true);
		
		String rowStr = (displayAllFields)? matrixCommonBean.getMatrixDTO().getSysFullThreshold() : 
			matrixCommonBean.getMatrixDTO().getSysLimitedThreshold();
		int rowpage = 0;
		if(rowStr==null || rowStr.length()==0){
			rowpage = (displayAllFields)? 25:100; 
		}
		else{
			rowpage = Integer.parseInt(rowStr);
		}
		
		String muitipleStr = (displayAllFields)? matrixCommonBean.getMatrixDTO().getSysFullThresholdPages() : 
			matrixCommonBean.getMatrixDTO().getSysLimitedThresholdPages();
		int muitiplepage = 0;
		if(muitipleStr==null || muitipleStr.length()==0){
			muitiplepage = (displayAllFields)? 4:4; 
		}
		else{
			muitiplepage = Integer.parseInt(muitipleStr);
		}
		
		transactionDataModel.setPageSize(rowpage);
		transactionDataModel.setDbFetchMultiple(muitiplepage);
		
		resetForHeaderRearrangeTemp();
		
		LogMemory.executeGC();
	}
	
	public void displayViewSplitChange(ActionEvent e) {
		HtmlSelectBooleanCheckbox check = (HtmlSelectBooleanCheckbox) ((HtmlAjaxSupport) e.getComponent()).getParent();
	    displayAllViewSplitFields = (check.getSubmittedValue() == null)? 
	    		((check.getValue() == null)? false:(Boolean) check.getValue()):Boolean.parseBoolean((String) check.getSubmittedValue());
		matrixCommonBean.createTransactionDetailTable(trDetailSplitTable,
				"transactionDetailBean", "transactionDetailBean.splitGroupDataModel", 
				matrixCommonBean.getTransactionPropertyMap(), false, displayAllViewSplitFields, false, false, true, null);
	}
	
	public Map<String, String> getTransactionSortMap(){
		Map<String, String> sortMap = new HashMap<String, String>();
		matrixCommonBean.createViewTransactionSortMap(sortMap,
				"transactionDetailBean", "transactionDetailBean.transactionDataModel", 
				matrixCommonBean.getTransactionPropertyMap(), true, selectedColumn, selectedUserName);
//		
//		for (String field : sortMap.keySet()) {
//			String description = sortMap.get(field);
//		}
		
		return sortMap;
	}
	
	public Map<String, String> getSortOrder() {
		return transactionDataModel.getSortOrder();
    }
	
	public String resetFilter() {
		filterSelectionTaxMatrix = new TaxMatrix();
		filterSelectionLocationMatrix = new LocationMatrix();

		filterTaxCodeHandler.reset();
		
		//MF - do not set default country
		//filterTaxCodeHandler.setTaxcodeCountry(getMatrixCommonBean().getUserPreferenceDTO().getUserCountry());
		//filterTaxCodeHandler.setTaxcodeState("");
		//filterTaxCodeHandler.rebuildMenus();
		
		fromGLDate = null;
		toGLDate = null;
		// transactionStatus= ""; // Not cleared per #2918
		//transactionId = null;
		purchtransId = null;
		jurisdictionId = null;
		taxMatrixId = null;
		locationMatrixId = null;
		glExtractId = null;
		processBatchId = null;
		multiTransCode = null;
		subTransId = null;
		
		selectedCountry = "*ALL";
		if(stateMenuItems!=null) stateMenuItems.clear();
		stateMenuItems = getMatrixCommonBean().getCacheManager().createStateItems(selectedCountry);
		selectedState = "*ALL";
		selectedTaxabilityCode="";
		
		return null;
	}

	public String redisplayTransactionList() {
		return "transactions_process";
	}
	
	public void prepareDisplayAction() {
		selectedTransaction = selectedTransactionTemp;
		
		currentTransactionList = transactionDataModel.getItems(getSelectedIds());
		currentTransactionIndex = 1;
		changesMade = false;
		totalSelected = getSelectionCount();
			
		initDisplayFields();
		this.isHoldAction = false;
	}
	
	// Bug 4100
	public void prepareDisplayActionForComments() {
		selectedTransaction = selectedTransactionTemp;
		currentTransactionList = transactionDataModel.getItems(getSelectedCommentIds());
		currentTransactionIndex = 1;
		changesMade = false;
	
		totalSelected = Math.max(currentTransactionList.size(), (selectedTransaction != null)? 1:0);

		applyToAll = false;
		comments = "";
	}
	
	public TaxCodeDetail getExistedTaxCodeDetail(){
		return existedTaxCodeDetail;
	}
	
	private void initDisplayFields() {
		currentJurisdiction = new Jurisdiction();
		PurchaseTransaction trDetail = getCurrentTransaction();
		Long id = (trDetail == null)? null:trDetail.getShiptoJurisdictionId();
		if (id != null) {
			currentJurisdiction = jurisdictionService.findById(id);
		}
		
		//Set default country
		newJurisdictionHandler.reset();
		newJurisdictionHandler.setCountry(getMatrixCommonBean().getUserPreferenceDTO().getUserCountry());
		newJurisdictionHandler.setState("");	
		newJurisdictionHandlerModify.reset();
		
		//Set default country
		newJurisdictionHandlerModify.setCountry(getMatrixCommonBean().getUserPreferenceDTO().getUserCountry());
		newJurisdictionHandlerModify.setState("");
		
		newTaxCodeHandler.reset();
		newTaxCodeHandlerUpdate.reset();
		//Set default country
		newTaxCodeHandlerUpdate.reset();
		filterTaxCodeHandler.rebuildMenus();
		
		suspendTax = false;
		suspendLocation = false;	
		suspendTaxcode = false;
		suspendTaxrate = false;
		suspendFlag = false;
		suspendType = null;
		existedTaxCodeDetail = new TaxCodeDetail();
		
		applyToAll = false;
		comments = "";
	}
	
	public Boolean getApplyToAll() {
		return applyToAll;
	}

	public void setApplyToAll(Boolean applyToAll) {
		this.applyToAll = applyToAll;
	}

	public int getCurrentTransactionIndex() {
		return currentTransactionIndex;
	}

	public PurchaseTransaction getCurrentTransaction() {
		return selectedTransaction = ((currentTransactionList == null) || (currentTransactionIndex > currentTransactionList.size())) ? selectedTransaction
				: currentTransactionList.get(currentTransactionIndex - 1);
	}
	
	public Jurisdiction getCurrentJurisdiction() {
		return currentJurisdiction;
	}

	public SearchJurisdictionHandler getNewJurisdictionHandler() {
		return newJurisdictionHandler;
	}
	
	public SearchJurisdictionHandler getNewJurisdictionHandlerModify() {
		return newJurisdictionHandlerModify;
	}
	
	public SearchJurisdictionHandler getJurisdictionHandler() {
		return jurisdictionHandler;
	}

	public TaxCodeCodeHandler getNewTaxCodeHandler() {
		return newTaxCodeHandler;
	}
	
	public TaxCodeCodeHandler getNewTaxCodeHandlerUpdate() {
		return newTaxCodeHandlerUpdate;
	}
	
	public SearchBatchIdHandler getBatchIdHandler() {
		return batchIdHandler;
	}

	public void searchDriver(ActionEvent e) {
		String driverCode = (String) e.getComponent().getAttributes().get("DRIVER_CODE");	
		driverHandlerBean.initSearch(
				(driverCode.equals(TaxMatrix.DRIVER_CODE)? filterSelectionTaxMatrix:filterSelectionLocationMatrix), 
				(HtmlInputText) e.getComponent().getParent().getChildren().get(0), e.getComponent());
		driverHandlerBean.setCallBackScreen("trans_main");
	}
	
	public String displayDriverSearch() {
		return "driver_handler_main";
	}
	
	public boolean getMoreTransactions() {
		return (currentTransactionList == null)? 
				false:(currentTransactionIndex < currentTransactionList.size());
	}

	public boolean getProcessedAllTransactions() {
		return (currentTransactionList == null) ? true : 
			(currentTransactionIndex >= currentTransactionList.size());
	}
	
	public boolean getFinishedAllTransactions() {
		return (currentTransactionList == null || currentTransactionIndex==0);
	}	
	
	public void nextTransactionActionListener(ActionEvent e) {
		nextTransactionAction();
	}

	// using this for non-popup pages Cancel button
	public String nextTransactionActionForPage() {
		boolean more = getMoreTransactions();
		nextTransactionAction();
		if (more) {
			return "trans_update";
		} else {
			return "trans_main_redirect";
		}
	}
	//0005238
	public String nextTransactionReprocessActionForPage() {
		boolean more = getMoreTransactions();
		nextTransactionAction();
		if (more) {
			return "trans_reprocess";
		} else {
			return "trans_main_redirect";
		}
	}
	// using this for non-popup pages Cancel button
	public String nextTransactionApplyTaxCodeActionForPage() {
		boolean more = getMoreTransactions();
		nextTransactionAction();
		if (more) {
			return "apply_taxcode";
		} else {
			return "trans_main_redirect";
		}
	}
	
	// using this for non-popup pages Cancel button
	public String nextTransactionApplyJurisdictionActionForPage() {
		boolean more = getMoreTransactions();
		nextTransactionAction();
		if (more) {
			return "apply_jurisdiction";
		} else {
			return "trans_main_redirect";
		}
	}
	
	public String cancelAll(){
		logger.info("cancelAll() called -  changesMade: "+ changesMade);
		
		// 4181
		if (changesMade){
			transactionDataModel.refreshData(false);
			resetSelection();
			searchCallback("search");
		}
		else{
			//PP-156
			FacesContext context = FacesContext.getCurrentInstance(); 
			Map map = context.getExternalContext().getRequestParameterMap(); 
			String actionName= (String) map.get("command");
			if(actionName!=null && actionName.equalsIgnoreCase("commentAction")){
				totalSelected = 1;
				return null;
			}
		}
		
		totalSelected = 1;
		return "trans_main_redirect";	
	}
	
	// method modified for bug - 3922 and 3924
	// 4100
	public String nextTransactionAction() {
		logger.info("Inside nextTransactionAction ************"); 

		//	4181 removeCurrentTransaction();
		if (getMoreTransactions()) {
			currentTransactionIndex += 1;
			
			//PP-156
			FacesContext context = FacesContext.getCurrentInstance(); 
			Map map = context.getExternalContext().getRequestParameterMap(); 
			String actionName= (String) map.get("command");
			if(actionName!=null && actionName.equalsIgnoreCase("commentAction")){
				applyToAll = false;
				comments = "";
				return null;
			}
				
			initDisplayFields();
			return null;
		} else {
			currentTransactionIndex += 1;
		}
		
		if (changesMade) {
			logger.info("Data Changed ");
			/* 4181
			transactionDataModel.refreshSelection();
			Long id = (selectedTransaction == null)? null:selectedTransaction.getTransactionDetailId();
			if ((id != null) && (id > 0L) && !transactionDataModel.getSelectionMap().get(id)) {
				transactionDataModel.refreshItem(id);
				
			}
			*/
			transactionDataModel.refreshData(false);
			resetSelection();
			searchCallback("search");

			totalSelected = 1;
			return "trans_main_redirect";
		}
		totalSelected = 1;
		
		return null;
	}
	
	
	public Map<String,Double> getFieldSum() {
		return selectionTotals;
	}
	
	// method modified for bug - 3922 and 3924
	public void updateFieldSum() {
		selectedIds = null;
		
		selectionTotals = new HashMap<String,Double>();
		Map<Long, Boolean> selection = transactionDataModel.getSelectionMap();
		totalSelected = getSelectionCount();
		for (Long id : selection.keySet()) {
			PurchaseTransaction trDetail = transactionDataModel.getById(id);
			if ((trDetail != null) && selection.get(id)) {
				for (Method m :PurchaseTransaction.class.getMethods()) {
					String name = m.getName();
					String field = Util.lowerCaseFirst(name.substring(3));
					// Look for properties only
					if (name.startsWith("get") && (m.getParameterTypes().length == 0) &&
							(m.getReturnType().equals(Float.class) || m.getReturnType().equals(Double.class) || m.getReturnType().equals(BigDecimal.class))) {

						Double a = selectionTotals.get(field);
						Object b = Util.getProperty(trDetail, field);
						if (b instanceof Number) {
							Double total = ((a == null)? 0.0:a) + ((b == null)? 0.0:((Number)b).doubleValue());
//							logger.debug("Updating total for " + field + " to " + total);
							selectionTotals.put(field, total);
						}
					}
				}
			}
		}
	}
	
	public void raiseErrorMessage(String messageStr){
		FacesMessage message = new FacesMessage(messageStr);
		message.setSeverity(FacesMessage.SEVERITY_ERROR);
		FacesContext.getCurrentInstance().addMessage(null, message);
	}
	
	public String displayCopyAddAction() {
		prepareDisplayAction();
		TaxMatrix matrix = taxMatrixBean.getMatrixService().findById(getCurrentTransaction().getTaxMatrixId());
		if(matrix==null){
			raiseErrorMessage("Tax Matrix Id not found");
			return "";
		}
		
		taxMatrixBean.addFromTransaction(matrix);
		return "tax_matrix_detail";
	}
	
	public String displayProcessAction() {
		prepareDisplayAction();
		PurchaseTransaction detail = getCurrentTransaction();
		String status = detail.getTransactionInd(); 
		if (status != null) {		
			if (status.equalsIgnoreCase("H")) {
				logger.debug("Modify Held Transaction");
				return displayUpdateAction();
			}
			
			if (status.equalsIgnoreCase("S")) {
				String suspend = detail.getSuspendInd();
				if (suspend != null) {
					if (suspend.equalsIgnoreCase("T")) {
						taxMatrixBean.addFromTransaction(detail);
						
						String searchColumnName = "";
						String selectedUserName = null;
						if(selectedColumn == null || selectedColumn.length() == 0){
							searchColumnName = "*DEFAULT";
							selectedUserName = "*GLOBAL";
						}
						else{
							selectedUserName = whatIfMap.get(selectedColumn);
							searchColumnName = selectedColumn;
						}
						taxMatrixBean.refreshWhatIfDataGrid(searchColumnName, selectedUserName);
						
						searchTaxMatrixAction();
						return "tax_matrix_detail";
					}
					
					if (suspend.equalsIgnoreCase("L")) {
						String searchColumnName = "";
						String selectedUserName = null;
						if(selectedColumn == null || selectedColumn.length() == 0){
							searchColumnName = "*DEFAULT";
							selectedUserName = "*GLOBAL";
						}
						else{
							selectedUserName = whatIfMap.get(selectedColumn);
							searchColumnName = selectedColumn;
						}
						locationMatrixBean.refreshWhatIfDataGrid(searchColumnName, selectedUserName);
						locationMatrixBean.addFromTransaction(detail);
						searchLocationMatrixAction();
						return "location_matrix_detail";
					}
					
					if (suspend.equalsIgnoreCase("D")) {
						taxCodeBackingBean.addFromTransaction(detail);
						searchTaxCodeAction();
						return "taxcode_rule_detail";
					}
					
					if (suspend.equalsIgnoreCase("R")) {
						Jurisdiction jurisdiction = getCurrentJurisdiction();
						if (jurisdiction != null) {
							setSelectedTaxRate(null);
							JurisdictionTaxrate sampleTaxrate = new JurisdictionTaxrate();
							sampleTaxrate.setJurisdiction(new Jurisdiction(jurisdiction.getJurisdictionId()));
							
							//TODO: AC
							//sampleTaxrate.setMeasureTypeCode(detail.getMeasureTypeCode());
							//sampleTaxrate.setMeasureTypeCode("");
							
							
							//logger.info("getMeasureTypeCode "+sampleTaxrate.getMeasureTypeCode());
							jurisdictionTaxrateList = taxJurisdictionBean.viewTransactionTaxrate(sampleTaxrate);
							return "transaction_taxrate_add";
						}
						
						logger.warn("Cannot add tax rate - no jurisdiction set!");
						return null;
					}
				}
				
				logger.warn("Unknown suspend indicator: " + suspend);
				return null;
			}
		}
		
		logger.warn("Unknown transaction indicator: " + status);
		return null;
	}
	
	//3990
	public String addTaxRateAction(){
		Jurisdiction jurisdiction = getCurrentJurisdiction();
		jurisdiction.setJurisdictionIdString(jurisdiction.getJurisdictionId().toString());
		taxJurisdictionBean.setTransactionDetailBean(this);
		return taxJurisdictionBean.addFromTransaction(jurisdiction,"");
	}
	
	//method modified for 3938
	public String copyTaxRateAction(){
		Long jurisdictionId = getCurrentJurisdiction().getJurisdictionId();
		taxJurisdictionBean.setTransactionDetailBean(this);
		taxJurisdictionBean.copyAddFromTransaction(jurisdictionId, selectedTaxRate.getJurisdictionTaxrateId());
		listTransactions();
		return "trans_main";
	}
	
	public void taxrateRowChanged(ActionEvent e){
		HtmlDataTable table = (HtmlDataTable) e.getComponent().getParent();
		selectedTaxRate = (JurisdictionTaxrate) table.getRowData();
		selectedTaxRateIndex = table.getRowIndex();
	}
	
	public void filterSaveAction(ActionEvent e) {
    }
	
	public String displayUpdateAction() {
		prepareDisplayAction();
		return "trans_update";
	}
	//0005238
	public String displayReprocessAction() {
		prepareDisplayAction();
		return "trans_reprocess";
	}
	
	public int getTransLogHistoryTablePageSize() {
		return transLogHistoryTablePageSize;
	}

	public void setTransLogHistoryTablePageSize(int transLogHistoryTablePageSize) {
		this.transLogHistoryTablePageSize = transLogHistoryTablePageSize;
	}
	
	//The following method was added for PP-124
	public String displayHistoryAction() {
		transactionLogDetailList = null;
		return "trans_history";
	}
	
	public List<PurchaseTransactionLog> getTransactiondetailLogList() {
		if(transactionLogDetailList == null) {
			transactionLogDetailList = purchaseTransactionLogService.findTransactionDetailLogList(new Long(selectedTransaction.getPurchtransId()));
			transLogHistoryTablePageNumber = 1;
		}
	
	return transactionLogDetailList;
	
	}
	
	public String getTaxcodeDescription(String taxcodeCode) {
		TaxCodePK id = new TaxCodePK();
		id.setTaxcodeCode(taxcodeCode);
		TaxCode taxCode = taxCodeService.findById(id);
		if(taxCode!=null){
			StringBuffer taxcodeDescription = new StringBuffer();
			taxcodeDescription = taxcodeDescription.append(taxCode.getTaxcodeCode()).append(" - ").append(taxCode.getDescription());
			 
			return taxcodeDescription.toString();
		}
		return null;
	}
	
	public String displayHoldAction() {
		prepareDisplayAction();
		this.isHoldAction = true;
		return "trans_reprocess";
	}
	
	public String displayApplyTaxCodeAction() {
		prepareDisplayAction();
		return "apply_taxcode";
	}
	
	public String displayApplyJurisdictionAction() {
		prepareDisplayAction();
		applyJurisdictionMessage = "";
		return "apply_jurisdiction";
	}
	
	public String displayApplyAction() {
		prepareDisplayAction();
		return "trans_apply";
		
	}
	
	public String displayViewAction() {
		currentAction = EditAction.VIEW;
		transactionViewBean.setScreenType("view"); 
		returnTransUrl = "trans_main";
		setReturnTransUrl(returnTransUrl);
		return displayViewScreen();
	}
	
	public String displayEditAction() {
		currentAction = EditAction.EDIT;
		transactionViewBean.setTransactionDetailBean(this);
		transactionViewBean.setSelectedDisplay("taxDriver");
		transactionViewBean.setScreenType("edit"); 
		transactionViewBean.setTransactionDetailService(transactionDetailService);
		setReturnTransUrl("trans_main");
		String view =  displayViewScreen();
		setTransactionValues();
		return view;
	}
	
	public void setTransactionValues() {
		updatedTransaction = new PurchaseTransaction();
		BeanUtils.copyProperties(selectedTransaction, updatedTransaction);
		transactionViewBean.setUpdatedTransaction(updatedTransaction); 
		resetFilter(transactionViewBean.getTxnLocnShiptoJurisdictionHandler());
		resetFilter(transactionViewBean.getTxnLocnShipfromJurisdictionHandler());
		resetFilter(transactionViewBean.getTxnLocnOrdrorgnJurisdictionHandler());
		resetFilter(transactionViewBean.getTxnLocnOrdracptJurisdictionHandler());
		resetFilter(transactionViewBean.getTxnLocnFirstuseJurisdictionHandler());
		resetFilter(transactionViewBean.getTxnLocnBilltoJurisdictionHandler());
		resetFilter(transactionViewBean.getTxnLocnTtlxfrJurisdictionHandler());
		transactionViewBean.setTxnLocnJurisdiction();
		transactionViewBean.setTxnVendorDetails();
	}
	
	public String displayViewScreen() {
		
		transactionViewBean.setSelectedTransaction(getCurrentTransaction());
		//fixed for issue 0004627
		transactionViewBean.setFilterFormState(filterFormState);
		transactionViewBean.setOkAction(returnTransUrl);
		
		
		transactionViewBean.setSelectedTransaction(selectedTransaction);
		if (this.selectedTransaction.getProcruleSessionId() != null
				&& !this.selectedTransaction.getProcruleSessionId().equals("")){
			transactionViewBean.setListviewResults(transactionViewBean.getViewResults());
		}
		
		setTransactionValues();
		initiateTaxesFlags(getCurrentTransaction()); 
		
		return "trans_view";
		
	}
	
	public void initiateTaxesFlags(PurchaseTransaction transaction) {
		
		setInitialCalcTaxFlags(transaction);
	    if(transaction.getTransactionInd()==null || !transaction.getTransactionInd().equalsIgnoreCase("P")){
	    	return;
	    }
	    setCalcTaxFlags(transaction); 
	}
	
	public void setCalcTaxFlags(PurchaseTransaction transaction)  {
		
		setCountryFlag(transaction, transaction.getCountrySitusCode());
		setStateFlag(transaction, transaction.getStateSitusCode());
		setCountyFlag(transaction, transaction.getCountySitusCode());
		setCityFlag(transaction, transaction.getCitySitusCode());
		setStj1to5Flag(transaction, transaction.getStj1SitusCode(), ONE);
		setStj1to5Flag(transaction, transaction.getStj2SitusCode(), TWO);
		setStj1to5Flag(transaction, transaction.getStj3SitusCode(), THREE);
		setStj1to5Flag(transaction, transaction.getStj4SitusCode(), FOUR);
		setStj1to5Flag(transaction, transaction.getStj5SitusCode(), FIVE);
		setStj6to10Flags(transaction, transaction.getStj6SitusCode(), transaction.getStj6Name(), SIX);
		setStj6to10Flags(transaction, transaction.getStj7SitusCode(), transaction.getStj7Name(), SEVEN);
		setStj6to10Flags(transaction, transaction.getStj8SitusCode(), transaction.getStj8Name(), EIGHT);
		setStj6to10Flags(transaction, transaction.getStj9SitusCode(), transaction.getStj9Name(), NINE);
		setStj6to10Flags(transaction, transaction.getStj10SitusCode(), transaction.getStj10Name(), TEN);
	}
	
	public LocationMatrix findLocationMatrix(PurchaseTransaction transaction, LocationTypeCalcFlags locationType) {
		LocationMatrix locationMatrix = null;
		switch (locationType) {
		case BT:
			if(transaction.getBilltoLocnMatrixId()!=null && transaction.getBilltoLocnMatrixId().intValue()>0){
		    	locationMatrix = locationMatrixService.findById(transaction.getBilltoLocnMatrixId());
		    }
			break;

		case ST:
			if(transaction.getShiptoLocnMatrixId()!=null && transaction.getShiptoLocnMatrixId().intValue()>0){
		    	locationMatrix = locationMatrixService.findById(transaction.getShiptoLocnMatrixId());
		    }
			break;

		case TT:
			if(transaction.getTtlxfrLocnMatrixId()!=null && transaction.getTtlxfrLocnMatrixId().intValue()>0){
		    	locationMatrix = locationMatrixService.findById(transaction.getTtlxfrLocnMatrixId());
		    }
			break;

		case UL:
			if(transaction.getFirstuseLocnMatrixId()!=null && transaction.getFirstuseLocnMatrixId().intValue()>0){
		    	locationMatrix = locationMatrixService.findById(transaction.getFirstuseLocnMatrixId());
		    }
			break;

		case SF:
			if(transaction.getShipfromLocnMatrixId()!=null && transaction.getShipfromLocnMatrixId().intValue()>0){
		    	locationMatrix = locationMatrixService.findById(transaction.getShipfromLocnMatrixId());
		    }
			break;

		case OO:
			if(transaction.getOrdrorgnLocnMatrixId()!=null && transaction.getOrdrorgnLocnMatrixId().intValue()>0){
		    	locationMatrix = locationMatrixService.findById(transaction.getOrdrorgnLocnMatrixId());
		    }
			break;

		case OA:
			if(transaction.getOrdracptLocnMatrixId()!=null && transaction.getOrdracptLocnMatrixId().intValue()>0){
		    	locationMatrix = locationMatrixService.findById(transaction.getOrdracptLocnMatrixId());
		    }
			break;

		default:
			break;
		}
		return locationMatrix;
	}
	
	public void setCountryFlag(PurchaseTransaction transaction, String countrySitusCode) {
		if(countrySitusCode != null) {
			LocationTypeCalcFlags locationTypeCalcFlags =  LocationTypeCalcFlags.valueOf(countrySitusCode);
			LocationMatrix locationMatrix = findLocationMatrix(transaction, locationTypeCalcFlags);
			if(locationMatrix != null) {
				transaction.setCountryFlag(locationMatrix.getCountryFlag());
			}
			else{
				noLocMatrixMatrixOrNoSitusCodeFoundforCountry(transaction);
			}
		}
		else {
			noLocMatrixMatrixOrNoSitusCodeFoundforCountry(transaction);
		}
	}
	
	public void noLocMatrixMatrixOrNoSitusCodeFoundforCountry(PurchaseTransaction transaction) {
		if(transaction.getCountryTaxcodeTypeCode()!=null && transaction.getCountryTaxcodeTypeCode().equalsIgnoreCase("T")){
			transaction.setCountryFlag("1");
	   }
    	else {
    		transaction.setCountryFlag("0");
    	}
	}
	
	
	public void setStateFlag(PurchaseTransaction transaction, String stateSitusCode) {
		if(stateSitusCode != null) {
			LocationTypeCalcFlags locationType = LocationTypeCalcFlags.valueOf(stateSitusCode);
			LocationMatrix locationMatrix = findLocationMatrix(transaction, locationType);
			if(locationMatrix != null) {
				transaction.setStateFlag(locationMatrix.getStateFlag());
			}
			else{
				noLocMatrixMatrixOrNoSitusCodeFoundforState(transaction);
			}
		}
		else {
			noLocMatrixMatrixOrNoSitusCodeFoundforState(transaction);
		}
	}
	
	public void noLocMatrixMatrixOrNoSitusCodeFoundforState(PurchaseTransaction transaction) {
		if(transaction.getStateTaxcodeTypeCode()!=null && transaction.getStateTaxcodeTypeCode().equalsIgnoreCase("T")){
			transaction.setStateFlag("1");
	   }
    	else {
    		transaction.setStateFlag("0");
    	}
	}
	
	public void setCountyFlag(PurchaseTransaction transaction, String countySitusCode) {
		if(countySitusCode != null) {
			LocationTypeCalcFlags locationType = LocationTypeCalcFlags.valueOf(countySitusCode);
			LocationMatrix locationMatrix = findLocationMatrix(transaction, locationType);
			if(locationMatrix != null) {
				transaction.setCountyFlag(locationMatrix.getCountyFlag());
			}
			else{
				noLocMatrixMatrixOrNoSitusCodeFoundforCounty(transaction);
			}
		}
		else {
			noLocMatrixMatrixOrNoSitusCodeFoundforCounty(transaction);
		}
	}
	
	public void noLocMatrixMatrixOrNoSitusCodeFoundforCounty(PurchaseTransaction transaction) {
		if(transaction.getCountyTaxcodeTypeCode()!=null && transaction.getCountyTaxcodeTypeCode().equalsIgnoreCase("T")){
			transaction.setCountyFlag("1");
	   }
    	else {
    		transaction.setCountyFlag("0");
    	}
	}
	
	public void setCityFlag(PurchaseTransaction transaction, String citySitusCode) {
		if(citySitusCode != null) {
			LocationTypeCalcFlags locationType = LocationTypeCalcFlags.valueOf(citySitusCode);
			LocationMatrix locationMatrix = findLocationMatrix(transaction, locationType);
			if(locationMatrix != null) {
				transaction.setCityFlag(locationMatrix.getCityFlag());
			}
			else{
				noLocMatrixMatrixOrNoSitusCodeFoundforCity(transaction);
			}
		}
		else {
			noLocMatrixMatrixOrNoSitusCodeFoundforCity(transaction);
		}
	}
	
	public void noLocMatrixMatrixOrNoSitusCodeFoundforCity(PurchaseTransaction transaction) {
		if(transaction.getCityTaxcodeTypeCode()!=null && transaction.getCityTaxcodeTypeCode().equalsIgnoreCase("T")){
			transaction.setCityFlag("1");
	   }
    	else {
    		transaction.setCityFlag("0");
    	}
	}
	
	public void setStj1to5Flag(PurchaseTransaction transaction, String stjSitusCode, String stjNumber) {
		try {
			Class<?>  aClassPT =  transaction.getClass();
			Method methodPT = aClassPT.getMethod("setStj" + stjNumber + "Flag", String.class); 
			if(stjSitusCode != null) {
				LocationMatrix locationMatrix = null;
				LocationTypeCalcFlags locationType = LocationTypeCalcFlags.valueOf(stjSitusCode);
				locationMatrix = findLocationMatrix(transaction, locationType);
				if(locationMatrix != null) {
					Class<?>  aClassLM = locationMatrix.getClass();
					Method methodLM = aClassLM.getMethod("getStj" + stjNumber + "BooleanFlag");
					Boolean valueLM = (Boolean)methodLM.invoke(locationMatrix, new Object[0]);
					methodPT.invoke(transaction, valueLM == Boolean.TRUE ? "1" : "0");
				}
				else {
					setNoLocMatrixOrNoJurisdictionOrNoSitusCodeFoundforSTJ(transaction, stjNumber, aClassPT, methodPT);
				}
			}
			else {
				setNoLocMatrixOrNoJurisdictionOrNoSitusCodeFoundforSTJ(transaction, stjNumber, aClassPT, methodPT);
			}
		
		}catch(Exception e) {
			logger.debug("exception occured while setting values stj1 to 5");
		}
	}
	
	public void setStj6to10Flags(PurchaseTransaction transaction, String stjSitusCode, String stjName, String stjNumber) {
		try {
			Class<?>  aClassPT = transaction.getClass();
			Method methodPT = aClassPT.getMethod("setStj" + stjNumber + "Flag", String.class);
			if(stjSitusCode != null &&  stjName != null) {
				LocationTypeCalcFlags locationType = LocationTypeCalcFlags.valueOf(stjSitusCode);
				LocationMatrix locationMatrix = findLocationMatrix(transaction, locationType);
				if(locationMatrix != null) {
					Jurisdiction jurisdiction = locationMatrix.getJurisdiction();
					if(jurisdiction != null){
						Class<?>  aClassJur = jurisdiction.getClass();
						for(int i = 1; i<=5; i++){
							Method methodJur = aClassJur.getMethod("getStj" + i + "Name");
							String valueJur = (String)methodJur.invoke(jurisdiction, new Object[0]);
							if(stjName.equals(valueJur)) {
								Class<?>  aClassLM = locationMatrix.getClass();
								Method methodLM = aClassLM.getMethod("getStj" + i + "BooleanFlag");
								Boolean valueLM = (Boolean)methodLM.invoke(locationMatrix, new Object[0]);
								methodPT.invoke(transaction, valueLM == Boolean.TRUE ? "1" : "0");
								break;
							}
						}
					}
					else {
						setNoLocMatrixOrNoJurisdictionOrNoSitusCodeFoundforSTJ(transaction, stjNumber, aClassPT, methodPT);
					}
				}
				else {
					setNoLocMatrixOrNoJurisdictionOrNoSitusCodeFoundforSTJ(transaction, stjNumber, aClassPT, methodPT);
				}
			
			}
			else {
				setNoLocMatrixOrNoJurisdictionOrNoSitusCodeFoundforSTJ(transaction, stjNumber, aClassPT, methodPT);
			}
		}catch(Exception e){
				logger.debug("exception occured while setting values stj6 to stj10");
			}
	}
	
	public void setNoLocMatrixOrNoJurisdictionOrNoSitusCodeFoundforSTJ(PurchaseTransaction transaction, String stjNumber, Class<?>  aClassPT, Method methodPTFound) throws Exception  {
		Method methodStjTaxCodeTypeCode = aClassPT.getMethod("getStj" + stjNumber + "TaxcodeTypeCode");
		String stjTaxCodeTypeCodevalue = (String)methodStjTaxCodeTypeCode.invoke(transaction, new Object[0]);
		if(stjTaxCodeTypeCodevalue!=null && stjTaxCodeTypeCodevalue.equalsIgnoreCase("T")){
			methodPTFound.invoke(transaction, "1");
	    }
		else {
			methodPTFound.invoke(transaction, "0");
		}
	}
	
	public void setInitialCalcTaxFlags(PurchaseTransaction transaction) {
	    transaction.setCountryFlag("0");
		transaction.setStateFlag("0");
		transaction.setCountyFlag("0");
		transaction.setCityFlag("0");
		setSTJInitialCalcFlags(transaction);
	}
	
	public void setSTJInitialCalcFlags(PurchaseTransaction transaction) {
		transaction.setStj1Flag("0");
	    transaction.setStj2Flag("0");
	    transaction.setStj3Flag("0");
	    transaction.setStj4Flag("0");
	    transaction.setStj5Flag("0");
	    transaction.setStj6Flag("0");
	    transaction.setStj7Flag("0");
	    transaction.setStj8Flag("0");
	    transaction.setStj9Flag("0");
	    transaction.setStj10Flag("0");
	}
	
	public void resetFilter(SearchJurisdictionHandler filterHandler) {
		filterHandler.reset();
		filterHandler.setCountry("");
		filterHandler.setState("");
		filterHandler.setEntityCode("");
		filterHandler.setJurisdictionId("");
		filterHandler.setStj1Name("");
		filterHandler.setStj2Name("");
		filterHandler.setStj3Name("");
		filterHandler.setStj4Name("");
		filterHandler.setStj5Name("");
		
	}
	
	
	public String viewTransactionFromHistorty() {
		returnTransUrl = "trans_history";
		setReturnTransUrl(returnTransUrl);
		this.currentAction = EditAction.VIEW_FROM_TRANSACTION_LOG_HISTORY;
		return displayViewScreen();
    }
	
	public String viewTransactionFromLog() {
		this.currentAction = EditAction.VIEW_FROM_TRANSACTION_LOG_ENTRY;
		returnTransUrl = "trans_log_entry_view";
		setReturnTransUrl(returnTransUrl);
		return displayViewScreen();
	}
	
	public String displaySplitViewAction() {
		transactionViewBean.setSelectedTransaction(selectedSplitTransaction);
		transactionViewBean.setOkAction("view_split_set");
		return "trans_view";
	}
	
	public String displayViewFromUpdateAction() {
		transactionViewBean.setSelectedTransaction(getCurrentTransaction());
		transactionViewBean.setOkAction("trans_update");
		return "trans_view";
	}
	
	public Long getDisplayWarning() {
		return (displayWarning)? 1L:0L;
	}
	
	public Long getDisplayIncomplete() {
		return (displayIncomplete)? 1L:0L;
	}
	
	public String cancelIncomplete() {
		displayIncomplete = false;
		return null;
	}
	
	public String cancelWarning() {
		displayWarning = false;
		return null;
		
		/* 5214
		boolean held = isHeld();
		if (applyToAll) {
			while (getMoreTransactions()) {
				currentTransactionIndex++;
			}
		}
		displayWarning = false;

		return held ?
			nextTransactionActionForPage() :
			nextTransactionAction(); 
		*/
	}
	
	private boolean isHeld() {
		PurchaseTransaction trDetail = getCurrentTransaction();
		String status = trDetail.getTransactionInd();
		return ((status != null) && status.equalsIgnoreCase("H"));
	}

	private boolean displayHeldWarning(boolean isHeld) {
		String taxcodeCode = getNewTaxCodeHandlerUpdate().getTaxcodeCode();
		if (isHeld && !displayWarning && (taxcodeCode != null) && !"".equals(taxcodeCode)) {
			displayWarning = true;
			return true;
		}
		return false;
	}
	
	public String updateNoOverrideAction() {
		ignoreTaxCode = true;
		String s = updateFromUpdateAction();
		ignoreTaxCode = false;
		return s;
	}

	public void applyToAllChanged() {
		// This method is here only to make sure that
		// the applyToAll variable is set in the server model
	}

	public String updateAction() {
		boolean held = isHeld();
		if (displayHeldWarning(held))
			return "trans_update";
		processTransaction();
		if (applyToAll) {
			while (getMoreTransactions()) {
				currentTransactionIndex++;
				processTransaction();
			}
		}
		displayWarning = false;

		return held ?
			nextTransactionActionForPage() :
			nextTransactionAction();
	}
	
	//0005238
	public String updateFromReprocessAction() {
		for(PurchaseTransaction transactionDetail: currentTransactionList) {
			transactionDetail.setIsReprocessing(true);
		}
		
		boolean held = isHeld();
		if (displayHeldWarning(held))
			return "trans_reprocess";
		
		displayIncomplete = false;
		if(!processTransaction()){
			return "trans_reprocess";
		}
		
		if (applyToAll) {
			while (getMoreTransactions()) {
				currentTransactionIndex++;
				//It should pass the first processTransaction() already
				processTransaction();
			}
		}
		displayWarning = false;
		
		if(held){
			return nextTransactionReprocessActionForPage();
		}
		else{
			String strPage = nextTransactionAction();
			if(strPage==null){
				return "trans_reprocess"; 
			}
			else{
				return strPage;
			}
		}
	}
	
	public String updateHoldAction() {
		isHoldAction = true;
		PurchaseTransaction trDetail = getCurrentTransaction();
		updateHoldTransactions(trDetail);
		changesMade = true;
		if (applyToAll) {
			while (getMoreTransactions()) {
				currentTransactionIndex++;
				PurchaseTransaction moretrDetail = getCurrentTransaction();
				updateHoldTransactions(moretrDetail);
			}
		}
		
		String strPage = nextTransactionAction();
		if(strPage==null){
			return "trans_reprocess"; 
		}
		else{
			return strPage;
		}
	}
	
	public void updateHoldTransactions(PurchaseTransaction trDetail) {
		trDetail.setTransactionInd("H");
		transactionDetailService.updatetoHoldTransaction(trDetail);
		 Option pco  = optionService.findByPK(new OptionCodePK("PCO", "ADMIN", "ADMIN") );
		 if (pco != null && pco.getValue().equalsIgnoreCase("1")) {
			 BillTransaction BillTransaction = transactionDetailBillService.findById(trDetail.getId());
				if(BillTransaction != null){
					BillTransaction.setTransactionInd("S");
					transactionDetailBillService.update(BillTransaction);
				}
		 }
	}
	
	public String updateFromUpdateAction() {
		boolean held = isHeld();
		if (displayHeldWarning(held))
			return "trans_update";
		
		displayIncomplete = false;
		if(!processTransaction()){
			return "trans_update";
		}
		
		if (applyToAll) {
			while (getMoreTransactions()) {
				currentTransactionIndex++;
				//It should pass the first processTransaction() already
				processTransaction();
			}
		}
		displayWarning = false;
		
		if(held){
			return nextTransactionActionForPage();
		}
		else{
			String strPage = nextTransactionAction();
			if(strPage==null){
				return "trans_update"; 
			}
			else{
				return strPage;
			}
		}
	}
	
	public void resetlocation(LocationType locnType, PurchaseTransaction trDetail) {
		//LocationType {BILLTO,	SHIPTO, TITLETRANSFER, FIRSTUSE, SHIPFROM, ORDERORIGIN, ORDERACCEPT} ;
		String locationMethod = "Shipto";
        switch (locnType) {
			case BILLTO:
				locationMethod = "Billto";
				break;
	
			case  SHIPTO:
				locationMethod = "Shipto";
				break;
	
			case  TITLETRANSFER:
				locationMethod = "Ttlxfr";
				break;
	
			case  FIRSTUSE:
				locationMethod = "Firstuse";
				break;
	
			case  SHIPFROM:
				locationMethod = "Shipfrom";
				break;
	
			case  ORDERORIGIN:
				locationMethod = "Ordrorgn";
				break;
	
			case  ORDERACCEPT:
				locationMethod = "Ordracpt";
				break;
	
			default:
				break;
		}
		
		if(locationMethod==null || locationMethod.length()==0) {
			return;
		}
		
		//String locationMethod = getLocationMethodName(locnType);
        Method theMethod = null;
        try {
    		theMethod = trDetail.getClass().getDeclaredMethod("set" + locationMethod + "Geocode", new Class[] {String.class});
    		theMethod.setAccessible(true);
    		theMethod.invoke(trDetail, new Object[]{null});
    		
    		theMethod = trDetail.getClass().getDeclaredMethod("set" + locationMethod + "AddressLine1", new Class[] {String.class});
    		theMethod.setAccessible(true);
    		theMethod.invoke(trDetail, new Object[]{null});
    		
    		theMethod = trDetail.getClass().getDeclaredMethod("set" + locationMethod + "AddressLine2", new Class[] {String.class});
    		theMethod.setAccessible(true);
    		theMethod.invoke(trDetail, new Object[]{null});
    		
    		theMethod = trDetail.getClass().getDeclaredMethod("set" + locationMethod + "City", new Class[] {String.class});
    		theMethod.setAccessible(true);
    		theMethod.invoke(trDetail, new Object[]{null});
    		
    		theMethod = trDetail.getClass().getDeclaredMethod("set" + locationMethod + "County", new Class[] {String.class});
    		theMethod.setAccessible(true);
    		theMethod.invoke(trDetail, new Object[]{null});
    		
    		theMethod = trDetail.getClass().getDeclaredMethod("set" + locationMethod + "StateCode", new Class[] {String.class});
    		theMethod.setAccessible(true);
    		theMethod.invoke(trDetail, new Object[]{null});
    		
    		theMethod = trDetail.getClass().getDeclaredMethod("set" + locationMethod + "Zip", new Class[] {String.class});
    		theMethod.setAccessible(true);
    		theMethod.invoke(trDetail, new Object[]{null});
    		
    		theMethod = trDetail.getClass().getDeclaredMethod("set" + locationMethod + "Zipplus4", new Class[] {String.class});
    		theMethod.setAccessible(true);
    		theMethod.invoke(trDetail, new Object[]{null});
    		
    		theMethod = trDetail.getClass().getDeclaredMethod("set" + locationMethod + "CountryCode", new Class[] {String.class});
    		theMethod.setAccessible(true);
    		theMethod.invoke(trDetail, new Object[]{null});
    		
    		theMethod = trDetail.getClass().getDeclaredMethod("set" + locationMethod + "Stj1Name", new Class[] {String.class});
    		theMethod.setAccessible(true);
    		theMethod.invoke(trDetail, new Object[]{null});
    		
    		theMethod = trDetail.getClass().getDeclaredMethod("set" + locationMethod + "Stj2Name", new Class[] {String.class});
    		theMethod.setAccessible(true);
    		theMethod.invoke(trDetail, new Object[]{null});
    		
    		theMethod = trDetail.getClass().getDeclaredMethod("set" + locationMethod + "Stj3Name", new Class[] {String.class});
    		theMethod.setAccessible(true);
    		theMethod.invoke(trDetail, new Object[]{null});
    		
    		theMethod = trDetail.getClass().getDeclaredMethod("set" + locationMethod + "Stj4Name", new Class[] {String.class});
    		theMethod.setAccessible(true);
    		theMethod.invoke(trDetail, new Object[]{null});
    		
    		theMethod = trDetail.getClass().getDeclaredMethod("set" + locationMethod + "Stj5Name", new Class[] {String.class});
    		theMethod.setAccessible(true);
    		theMethod.invoke(trDetail, new Object[]{null});
    		
    		theMethod = trDetail.getClass().getDeclaredMethod("set" + locationMethod + "CountryNexusindCode", new Class[] {String.class});
    		theMethod.setAccessible(true);
    		theMethod.invoke(trDetail, new Object[]{null});
    		
    		theMethod = trDetail.getClass().getDeclaredMethod("set" + locationMethod + "StateNexusindCode", new Class[] {String.class});
    		theMethod.setAccessible(true);
    		theMethod.invoke(trDetail, new Object[]{null});
    		
    		theMethod = trDetail.getClass().getDeclaredMethod("set" + locationMethod + "CountyNexusindCode", new Class[] {String.class});
    		theMethod.setAccessible(true);
    		theMethod.invoke(trDetail, new Object[]{null});
    		
    		theMethod = trDetail.getClass().getDeclaredMethod("set" + locationMethod + "CityNexusindCode", new Class[] {String.class});
    		theMethod.setAccessible(true);
    		theMethod.invoke(trDetail, new Object[]{null});
    		
    		theMethod = trDetail.getClass().getDeclaredMethod("set" + locationMethod + "Stj1NexusindCode", new Class[] {String.class});
    		theMethod.setAccessible(true);
    		theMethod.invoke(trDetail, new Object[]{null});
    		
    		theMethod = trDetail.getClass().getDeclaredMethod("set" + locationMethod + "Stj2NexusindCode", new Class[] {String.class});
    		theMethod.setAccessible(true);
    		theMethod.invoke(trDetail, new Object[]{null});
    		
    		theMethod = trDetail.getClass().getDeclaredMethod("set" + locationMethod + "Stj3NexusindCode", new Class[] {String.class});
    		theMethod.setAccessible(true);
    		theMethod.invoke(trDetail, new Object[]{null});
    		
    		theMethod = trDetail.getClass().getDeclaredMethod("set" + locationMethod + "Stj4NexusindCode", new Class[] {String.class});
    		theMethod.setAccessible(true);
    		theMethod.invoke(trDetail, new Object[]{null});
    		
    		theMethod = trDetail.getClass().getDeclaredMethod("set" + locationMethod + "Stj5NexusindCode", new Class[] {String.class});
    		theMethod.setAccessible(true);
    		theMethod.invoke(trDetail, new Object[]{null});
        } catch (NoSuchMethodException ne) {
            logger.error("Invalid method name!" + ne.getMessage());
        } catch (IllegalAccessException ie) {
            logger.error("Insufficient permissions to call a getter!" + ie.getMessage());
        }  catch (InvocationTargetException ie) {
            logger.error("Invocation target exception while calling a getter!" + ie.getCause().getMessage());
        }
	}
	
	public boolean processTransaction() {
		return processTransaction(LogSourceEnum.processTransaction);
	}

	public boolean processTransaction(LogSourceEnum callingFromMethod) {
		PurchaseTransaction trDetail = getCurrentTransaction();
		Boolean reproc = true;
		boolean inComplete = false;
		boolean acceptTransaction = true;
		
		// Fetch & reverify jurisdiction, but only if
		// source record had a jurisdiction to begin with
		Jurisdiction inputJurisdiction = null;
		if (newJurisdictionHandlerModify.getHasCountryExceptInput()) {
			inputJurisdiction = newJurisdictionHandlerModify.getJurisdictionFromDB();
			if (inputJurisdiction == null) {
				FacesMessage message = new FacesMessage("Jurisdiction not found or not unique.");
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage(null, message);
				inComplete  = true;
			}
		}

		// Get tax code handler before the loop
		TaxCodeCodeHandler handler = getNewTaxCodeHandlerUpdate();//getNewTaxCodeHandler();
		String taxcodeCode = handler.getTaxcodeCode();
	
		if(inComplete){
			displayIncomplete = true;
			return false;
		}
		
		changesMade = true;

		String status = trDetail.getTransactionInd();
		boolean isHeldTransaction = ((status != null) && status.equalsIgnoreCase("H"));

		if ((taxcodeCode != null) && !taxcodeCode.equals("") && !ignoreTaxCode) {
			trDetail.setManualTaxcodeInd((isHeldTransaction)? "MO":"MC");	// Manual Override:Manual Change
			trDetail.setTaxcodeCode(taxcodeCode);
			
			//0001643
			//11/12/2019 added country, stj1 - 10
			trDetail.setCountryTaxcodeDetailId(null);
			trDetail.setStateTaxcodeDetailId(null);
			trDetail.setCountyTaxcodeDetailId(null);
			trDetail.setCityTaxcodeDetailId(null);
			trDetail.setStj1TaxcodeDetailId(null);
			trDetail.setStj2TaxcodeDetailId(null);
			trDetail.setStj3TaxcodeDetailId(null);
			trDetail.setStj4TaxcodeDetailId(null);
			trDetail.setStj5TaxcodeDetailId(null);
			trDetail.setStj6TaxcodeDetailId(null);
			trDetail.setStj7TaxcodeDetailId(null);
			trDetail.setStj8TaxcodeDetailId(null);
			trDetail.setStj9TaxcodeDetailId(null);
			trDetail.setStj10TaxcodeDetailId(null);
			
			acceptTransaction = false;
			reproc = false;
			//TODO: JJ
			//trDetail.setMeasureTypeCode(detail.getMeasureTypeCode());
		}

		//CCH		trDetail.setCchGroupCode(cchGroup);
		//CCH		trDetail.setCchTaxcatCode(cchTaxCat);
	
		// Update jurisdiction 
		if (inputJurisdiction != null) {
			trDetail.setShiptoManualJurInd("MC");	// Manual Change
			trDetail.setShiptoJurisdictionId(inputJurisdiction.getId());
			//0008935
			trDetail.setShiptoLocnMatrixId(null);
			
			acceptTransaction = false;
			reproc = false;
			
			trDetail.setCountryTaxcodeDetailId(null);
			trDetail.setStateTaxcodeDetailId(null);
			trDetail.setCountyTaxcodeDetailId(null);
			trDetail.setCityTaxcodeDetailId(null);
			trDetail.setStj1TaxcodeDetailId(null);
			trDetail.setStj2TaxcodeDetailId(null);
			trDetail.setStj3TaxcodeDetailId(null);
			trDetail.setStj4TaxcodeDetailId(null);
			trDetail.setStj5TaxcodeDetailId(null);
			trDetail.setStj6TaxcodeDetailId(null);
			trDetail.setStj7TaxcodeDetailId(null);
			trDetail.setStj8TaxcodeDetailId(null);
			trDetail.setStj9TaxcodeDetailId(null);
			trDetail.setStj10TaxcodeDetailId(null);
			
			resetlocation(LocationType.SHIPTO, trDetail);	
		}
		
		if(isHeldTransaction && acceptTransaction){
			//0001400: Processing Held Transactions
			transactionDetailService.acceptHeldTransaction(trDetail.getPurchtransId());
			
			//0003204 Append any comments
			if ((comments != null) && !comments.trim().equals("")) {
				transactionDetailService.appendComment(trDetail.getPurchtransId(), comments);
			}
			reproc = false;
			return true;
		}
		
		if (isHeldTransaction) {
			trDetail.setTransactionInd("P");
			reproc = false;
			//TODO: JJ
			//5020
//			if(detail!=null && detail.getJurisdictionId()!=null && detail.getJurisdictionId()>0){
//				trDetail.setManualJurisdictionInd("AO");	// Automatic Override
//				trDetail.setJurisdictionId(detail.getJurisdictionId());
//			}
		}
		else{
			//0007926: On Update and Reprocess set the Transaction Indicator to "REPROC" and the Suspend Indicator to NULL
			//trDetail.setTransactionInd("REPROC");
			trDetail.setTransactionInd(null);
			trDetail.setSuspendInd(null);
		}
		
		// Apply the updates
		logger.debug("Updating transaction " + trDetail.getPurchtransId());
		
		// Append any comments
		if ((comments != null) && !comments.trim().equals("")) {
			//0008938
			String newComment = "";
			if(trDetail.getComments()!=null && trDetail.getComments().length()>0){
				newComment = trDetail.getComments() + "\n";
			}
			newComment = newComment + String.format("%1$tD %1$tT - %2$s", new Date(), comments);
			reproc = false;
			trDetail.setComments(newComment);
		}
		
		try {
			if(reproc)
				purchaseTransactionService.getPurchaseTransactionReprocess(purchaseTransactionService.purchTransToMicroApiTransactionDocument(trDetail), true);
			else
				purchaseTransactionService.taxEstimate(purchaseTransactionService.purchTransToMicroApiTransactionDocument(trDetail), true, callingFromMethod);
		}
		catch(PurchaseTransactionProcessingException e) {
			FacesMessage message = new FacesMessage("Unable to Process Selected Transaction(s)");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
		}
		
		return true;
	}
	
	public String applyLocationMatrixAction() {
		applyJurisdictionMessage = "";

		//Validate fields
		boolean errorMessage = false;
		if(newJurisdictionHandler.getGeocode()==null || newJurisdictionHandler.getGeocode().length()==0){
			if(errorMessage){
				applyJurisdictionMessage = applyJurisdictionMessage + "\n";
			}	
			errorMessage = true;				
			applyJurisdictionMessage = applyJurisdictionMessage + "GepCode is required.";
		}
		
		if(newJurisdictionHandler.getCity()==null || newJurisdictionHandler.getCity().length()==0){
			if(errorMessage){
				applyJurisdictionMessage = applyJurisdictionMessage + "\n";
			}
			errorMessage = true;				
			applyJurisdictionMessage = applyJurisdictionMessage + "City is required.";
		}
		
		if(newJurisdictionHandler.getCounty()==null || newJurisdictionHandler.getCounty().length()==0){
			if(errorMessage){
				applyJurisdictionMessage = applyJurisdictionMessage + "\n";
			}
			errorMessage = true;				
			applyJurisdictionMessage = applyJurisdictionMessage + "County is required.";
		}
		
		if(newJurisdictionHandler.getState()==null || newJurisdictionHandler.getState().length()==0){
			if(errorMessage){
				applyJurisdictionMessage = applyJurisdictionMessage + "\n";
			}
			errorMessage = true;				
			applyJurisdictionMessage = applyJurisdictionMessage + "State is required.";
		}
		
		if(newJurisdictionHandler.getCountry()==null || newJurisdictionHandler.getCountry().length()==0){
			if(errorMessage){
				applyJurisdictionMessage = applyJurisdictionMessage + "\n";
			}
			errorMessage = true;				
			applyJurisdictionMessage = applyJurisdictionMessage + "Country is required.";
		}
		
		if(newJurisdictionHandler.getZip()==null || newJurisdictionHandler.getZip().length()==0){
			if(errorMessage){
				applyJurisdictionMessage = applyJurisdictionMessage + "\n";
			}
			errorMessage = true;				
			applyJurisdictionMessage = applyJurisdictionMessage + "Zip Code is required.";
		}
		
		if(errorMessage){
			return "apply_jurisdiction";
		}
		
		// Fetch & reverify jurisdiction
		Jurisdiction inputJurisdiction = newJurisdictionHandler.getJurisdictionFromDB();
		if (inputJurisdiction == null) {
			applyJurisdictionMessage = applyJurisdictionMessage + "Jurisdiction not found or not unique.";
			return "apply_jurisdiction";
		}
		
		PurchaseTransaction trDetail = getCurrentTransaction();
		do {
			changesMade = true;
		
			// Update jurisdiction 
			trDetail.setTransactionInd(null);
			trDetail.setSuspendInd(null);
			trDetail.setShiptoManualJurInd("MA");	// Manual Apply
			trDetail.setShiptoJurisdictionId(inputJurisdiction.getId());
			
			//Apply the updates, 11/13/2019
			//logger.debug("Updating transaction " + trDetail.getPurchtransId());
			//transactionDetailService.update(trDetail);
			
			//Append any comments
			//if ((comments != null) && !comments.trim().equals("")) {
			//	transactionDetailService.appendComment(trDetail.getPurchtransId(), comments);
			//}
			
			if ((comments != null) && !comments.trim().equals("")) {
				String newComment = "";
				if(trDetail.getComments()!=null && trDetail.getComments().length()>0){
					newComment = trDetail.getComments() + "\n";
				}
				//newComment = newComment + String.format("%1$tD %1$tT - %2$s", new Date(), comments);
				newComment = newComment + comments;
				trDetail.setComments(newComment);
			}
	
			try {
				purchaseTransactionService.taxEstimate(purchaseTransactionService.purchTransToMicroApiTransactionDocument(trDetail), true, LogSourceEnum.applyLocationMatrixAction);
			}
			catch(PurchaseTransactionProcessingException e) {
				FacesMessage message = new FacesMessage("Unable to Apply Selected Transaction(s)");
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage(null, message);
			}
			
			// Apply to next record if apply to all
			if (applyToAll && getMoreTransactions()) {
				currentTransactionIndex++;
				trDetail = getCurrentTransaction();
			} else {
				trDetail = null;
			}
		} while (trDetail != null);
		return nextTransactionAction();
	}
	
	public String applyTaxMatrixAction() {
		// Get tax code handler before the loop
		TaxCodeCodeHandler handler = getNewTaxCodeHandler();
		TaxCode tc = handler.findTaxcode();

		if (tc == null) {
			FacesMessage message = new FacesMessage("TaxCode not found.");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
			return null;
		}

		String taxcodeCode = handler.getTaxcodeCode();
		PurchaseTransaction trDetail = getCurrentTransaction();
		do {
			changesMade = true;

			trDetail.setTransactionInd(null);
			trDetail.setSuspendInd(null);
			trDetail.setManualTaxcodeInd("MA");	// Manual Apply
			trDetail.setTaxcodeCode(taxcodeCode);
			
			//Apply the updates, 11/13/2019
			//logger.debug("Updating transaction " + trDetail.getPurchtransId());
			//transactionDetailService.update(trDetail);
			
			if ((comments != null) && !comments.trim().equals("")) {
				String newComment = "";
				if(trDetail.getComments()!=null && trDetail.getComments().length()>0){
					newComment = trDetail.getComments() + "\n";
				}
				//newComment = newComment + String.format("%1$tD %1$tT - %2$s", new Date(), comments);
				newComment = newComment + comments;
				trDetail.setComments(newComment);
			}
	
			try {
				purchaseTransactionService.taxEstimate(purchaseTransactionService.purchTransToMicroApiTransactionDocument(trDetail), true, LogSourceEnum.applyTaxMatrixAction);
			}
			catch(PurchaseTransactionProcessingException e) {
				FacesMessage message = new FacesMessage("Unable to Apply Selected Transaction(s)");
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage(null, message);
			}

			// Append any comments, 11/13/2019
			//if ((comments != null) && !comments.trim().equals("")) {
			//	transactionDetailService.appendComment(trDetail.getPurchtransId(), comments);
			//}
			
			// Apply to next record if apply to all
			if (applyToAll && getMoreTransactions()) {
				currentTransactionIndex++;
				trDetail = getCurrentTransaction();
			} else {
				trDetail = null;
			}
		} while (trDetail != null);
		return nextTransactionAction();
	}

	public String suspendAction() {
		
		SuspendReason suspendReason = SuspendReason.valueOf(getSuspendType());

		for (Long id : getSelectedIds()) {
			logger.debug("Suspending: " + id);
			transactionDetailService.suspendTransaction(purchaseTransactionService, id, suspendReason);
		}
		
		transactionDataModel.refreshData(false);
		resetSelection();
		searchCallback("search");
		totalSelected = 1;
		
		return "trans_main";
	}
	
	public void removeCurrentTransaction(){
		transactionDataModel.getSelectionMap().remove(getCurrentTransaction().getPurchtransId());
		
	}
    /**
     * Ugh, confusing.... why is this here and there's something similar in TransactionDetailServiceImpl???
     */

	// Bug 4184, method newly added
	  public StringBuffer saveAs() throws Exception {
//		  return transactionDetailService.saveAs(transactionDataModel.getAllData());
		  return transactionDetailService.saveAs(traList);
	  }
	
	// Bug 4184, method newly added
	/*   
	public String saveAction(){
		String sqlStatement = getProcessSQLStatement();
		transactionBlobBean.setSqlScripts(sqlStatement);
		transactionBlobBean.setRedirectPage("transactions_process");
		transactionBlobBean.setAction(EditAction.ADD);
		
		TransactionBlob workingBlob = new TransactionBlob();
		transactionBlobBean.setWorkingBlob(workingBlob);
		transactionBlobBean.setProcessAction(EditAction.START);
		
		return "transactionblob_add";
	}
	*/
	
	//0000027
	public String saveAction(){
		//Create criteria
		PurchaseTransaction trDetail = (PurchaseTransaction)transactionDataModel.getExampleInstance();
		Date fromGLDate = transactionDataModel.getEffectiveDate();
		Date toGLDate = transactionDataModel.getExpirationDate(); 
		Boolean includeUnprocessed = transactionDataModel.getIncludeUnprocessed();
		Boolean includeProcessed = transactionDataModel.getIncludeProcessed();
		String includeSuspended = transactionDataModel.getIncludeSuspended();
		Boolean glExtractFlagIsNull = transactionDataModel.getGlExtractFlagIsNull();
		String advancedFilter = transactionDataModel.getAdvancedFilter();	
		String sqlCriteria = transactionDetailService.getProcessSQLCriteria(trDetail, fromGLDate,
				toGLDate, includeUnprocessed, includeProcessed, includeSuspended, glExtractFlagIsNull,
				advancedFilter, transactionDataModel.getSaveAsOrderBy(), 0,
				10000000); //Testing 10,000,000 rows
		
		//Create order by
		Class<?> cls = PurchaseTransaction.class;
		String transactionColumn = "";
		OrderBy orderBy = transactionDataModel.getOrderBy();
		String orderByStr = "PURCHTRANS_ID asc";
		if (orderBy != null && orderBy.getFields().size()>0){
        	orderByStr = "";
            for (FieldSortOrder field : orderBy.getFields()){
            	if(orderByStr.length()==0){
            	}
            	else{
            		orderByStr = orderByStr + " , ";
            	}
            	
                if (field.getAscending()){  
                    try{
    					Field f = cls.getDeclaredField(field.getName());
    					transactionColumn = f.getAnnotation(Column.class).name();
    					orderByStr = orderByStr + transactionColumn + " asc ";
    				}
    				catch(Exception e){
    					System.out.println(e.toString());
    				}

                } else {
                    try{
    					Field f = cls.getDeclaredField(field.getName());
    					transactionColumn = f.getAnnotation(Column.class).name();
    					orderByStr = orderByStr + transactionColumn + " desc ";
    				}
    				catch(Exception e){
    					System.out.println(e.toString());
    				}
                }
            }
        }

		BatchMaintenance batch = new BatchMaintenance();
		batch.setUpdateUserId(Auditable.currentUserCode());
		batch.setStatusUpdateUserId(Auditable.currentUserCode());
		
		Date aDate = new Date();
		batch.setEntryTimestamp(aDate);
	
		String exportFileExt = batchMaintenanceBean.getFilePreferenceBean().getBatchPreferenceDTO().getExportFileExt();
		if(exportFileExt!=null){
			exportFileExt = exportFileExt.replace(".", "");	
			if(exportFileExt.length()>0){
				exportFileExt = "." + exportFileExt;
			}
			else{
				exportFileExt = ".csv";
			}
		}
		else{
			exportFileExt = ".csv";
		}
		batch.setVc01(DownloadAddBean.createDownloadFileName() + exportFileExt);
		
	  	batch.setBatchTypeCode("DL");
	  	batch.setBatchStatusCode("DX");

		batch.setTotalRows(new Long(transactionDataModel.getRowCount()));
		downloadBatchMaintenance = batch;
		
		if(sqlCriteria!=null && sqlCriteria.length()>0){
			downloadAddBean.setExtendWhere(sqlCriteria);
		}
		else{
			downloadAddBean.setExtendWhere("(1=1)");
		}
		if(orderByStr!=null && orderByStr.length()>0){
			downloadAddBean.setStrOrderBy(orderByStr);
		}
		else{
			downloadAddBean.setStrOrderBy("");
		}
		downloadAddBean.setReturnScreen("transactions_process");
		downloadAddBean.setDownloadBatchMaintenance(downloadBatchMaintenance);	
		
		//Get datagrid's columns
		List<TransactionHeader> transactionHeaders = null;
		if(selectedColumn==null || selectedColumn.length()==0 || selectedColumn.equalsIgnoreCase("*GLOBAL/*DEFAULT")){
			transactionHeaders = selectionColumnBean.getDefaultView(); //
		}
		else{
			String searchColumnName = "";
			String selectedUserName = null;	
			selectedUserName = selectedColumn.substring(0, selectedColumn.indexOf("/"));
			searchColumnName = selectedColumn.substring(selectedColumn.indexOf("/") + 1 ,selectedColumn.length());;
			transactionHeaders = dwColumnService.getColumnsByUserandDataWindowName("TB_PURCHTRANS", searchColumnName, selectedUserName);
		}
		List<String> columnList = new ArrayList<String>();
		Map<String,DataDefinitionColumn> transactionPropertyMap = matrixCommonBean.getCacheManager().getTransactionPropertyMap(); 
		DataDefinitionColumn dataDefinitionColumn = null;
		downloadAddBean.setHeadersSize(transactionHeaders.size());
		downloadAddBean.setProcessStatusFound(false);
		for (int i = 0; i < transactionHeaders.size(); i++) {
			TransactionHeader transactionHeader = (TransactionHeader)transactionHeaders.get(i);
			if(PROCESS_STATUS_COLUMN.equals(transactionHeader.getColumnName())) {
				int processStatusPosition = i;
				downloadAddBean.setProcessStatusFound(true);
				processStatusPosition = processStatusPosition + 2 ; 
				downloadAddBean.setProcessStatusPosition(processStatusPosition); 
				downloadAddBean.setHeadersSize(transactionHeaders.size() + 1); 
			}
			if(transactionPropertyMap.get(transactionHeader.getColumnName())!=null){
				dataDefinitionColumn = transactionPropertyMap.get(transactionHeader.getColumnName());
				columnList.add(dataDefinitionColumn.getColumnName());
			}
		}
		downloadAddBean.setColumnList(columnList);
		
		//Check if Downloading File is in process.
		downloadAddBean.setStopDownload(false);
		if (transactionDataModel.getRowCount()>1000000) {
			downloadAddBean.setStopDownload(true);
			downloadAddBean.setMessage("Cannot download more than 1,000,000 rows.");
		}
		else {
			List<BatchMaintenance> batchMaintenanceList = batchMaintenanceService.findByStatusUpdateUser("DL", "DX", Auditable.currentUserCode());		
			
			if(batchMaintenanceList!=null && batchMaintenanceList.size()>0){
				downloadAddBean.setStopDownload(true);
				downloadAddBean.setMessage("Cannot start a new download while previous download is running.");	
			}
		}
		
		return "transaction_download";
	}


	//TODO REMOVE THIS FUNCTION
	public String  getProcessSQLStatement(){
		boolean includeUnprocessed = false;
		boolean glExtractFlagIsNull = false;

        PurchaseTransaction trDetail = new PurchaseTransaction();
        //PurchaseTransaction purchTrans = new PurchaseTransaction();
		
		String state = selectedState;
		if ((state != null) && !state.equals("")) {
			trDetail.setTransactionStateCode(state);
			//purchTrans.setTransactionStateCode(state);
		}
		
		String country = selectedCountry;
		if ((country != null) && !country.equals("")) {
			trDetail.setTransactionCountryCode(country);
			//purchTrans.setTransactionCountryCode(country);
		}
		
		// Now, map the drivers to transaction detail properties
		matrixCommonBean.setTransactionProperties(filterSelectionTaxMatrix, trDetail);
		matrixCommonBean.setTransactionProperties(filterSelectionLocationMatrix, trDetail);
		
		if (!getTaxMatrixIdDisabled() && (taxMatrixId != null) && (taxMatrixId > 0L)) {
			trDetail.setTaxMatrixId(taxMatrixId);
		}
		
		if (!getLocationMatrixIdDisabled() && (locationMatrixId != null) && (locationMatrixId > 0L)) {
			trDetail.setShiptoLocnMatrixId(locationMatrixId);
		}
		
		if ((jurisdictionId != null) && (jurisdictionId > 0L)) {
			trDetail.setShiptoJurisdictionId(jurisdictionId);
		}
		
		if ((purchtransId != null) && (purchtransId > 0L)) {
			trDetail.setPurchtransId(purchtransId);
		}
		if ((purchtransId != null) && (purchtransId > 0L)) {
			trDetail.setPurchtransId(purchtransId); 
		}
		
		
		if ((glExtractId != null) && (glExtractId > 0L)) {
			trDetail.setGlExtractBatchNo(glExtractId);
		}
		
		if ((processBatchId != null) && (processBatchId > 0L)) {
			trDetail.setProcessBatchNo(processBatchId);
		}
		
		if(multiTransCode != null && multiTransCode.length() > 0) {
			trDetail.setMultiTransCode(multiTransCode);
		}
		
		if ((subTransId != null) && (subTransId > 0L)) {
			trDetail.setSplitSubtransId(subTransId);
		}

		// Set taxcode filters, if enabled
		if (!getTaxCodeDisabled()) {
			String taxcodeCode = filterTaxCodeHandler.getTaxcodeCode();
			
			if ((taxcodeCode != null) && !taxcodeCode.equals("")) {
				trDetail.setTaxcodeCode(taxcodeCode);
			}
			
			//CCH			String cchCat = filterTaxCodeHandler.getCchTaxCat();
			//CCH			String cchGroup = filterTaxCodeHandler.getCchGroup();
			//CCH			if ((cchCat != null) && !cchCat.equals("")) {
			//CCH				trDetail.setCchTaxcatCode(cchCat);
			//CCH			}
			//CCH			if ((cchGroup != null) && !cchGroup.equals("")) {
			//CCH				trDetail.setCchGroupCode(cchGroup);
			//CCH			}
		}
		
		String advancedFilter = advancedFilterBean.getAdvancedFilter();
		
		String status = (transactionStatus == null)? "" : transactionStatus;
		if (status.equalsIgnoreCase("P")) {	 // All - Processed Transactions
			trDetail.setTransactionInd("P");
		} else if (status.equalsIgnoreCase("PO")) {	// Processed - Open Transactions      //MBF
			trDetail.setTransactionInd("P");
			glExtractFlagIsNull = true;
			if(advancedFilter == null || advancedFilter.length() == 0) {
				advancedFilter = "(STATE_TAXCODE_TYPE_CODE='T' OR COUNTY_TAXCODE_TYPE_CODE='T' OR CITY_TAXCODE_TYPE_CODE='T' " +
						"OR (STJ1_TAXCODE_TYPE_CODE IS NOT NULL AND STJ1_TAXCODE_TYPE_CODE='T') " +
						"OR (STJ2_TAXCODE_TYPE_CODE IS NOT NULL AND STJ2_TAXCODE_TYPE_CODE='T') " +
						"OR (STJ3_TAXCODE_TYPE_CODE IS NOT NULL AND STJ3_TAXCODE_TYPE_CODE='T') " +
						"OR (STJ4_TAXCODE_TYPE_CODE IS NOT NULL AND STJ4_TAXCODE_TYPE_CODE='T') " +
						"OR (STJ5_TAXCODE_TYPE_CODE IS NOT NULL AND STJ5_TAXCODE_TYPE_CODE='T') " +
						"OR (STJ6_TAXCODE_TYPE_CODE IS NOT NULL AND STJ6_TAXCODE_TYPE_CODE='T') " +
						"OR (STJ7_TAXCODE_TYPE_CODE IS NOT NULL AND STJ7_TAXCODE_TYPE_CODE='T') " +
						"OR (STJ8_TAXCODE_TYPE_CODE IS NOT NULL AND STJ8_TAXCODE_TYPE_CODE='T') " +
						"OR (STJ9_TAXCODE_TYPE_CODE IS NOT NULL AND STJ9_TAXCODE_TYPE_CODE='T') " +
						"OR (STJ10_TAXCODE_TYPE_CODE IS NOT NULL AND STJ10_TAXCODE_TYPE_CODE='T'))";
			}
			else {
				advancedFilter += " AND (STATE_TAXCODE_TYPE_CODE='T' OR COUNTY_TAXCODE_TYPE_CODE='T' OR CITY_TAXCODE_TYPE_CODE='T' " +
						"OR (STJ1_TAXCODE_TYPE_CODE IS NOT NULL AND STJ1_TAXCODE_TYPE_CODE='T') " +
						"OR (STJ2_TAXCODE_TYPE_CODE IS NOT NULL AND STJ2_TAXCODE_TYPE_CODE='T') " +
						"OR (STJ3_TAXCODE_TYPE_CODE IS NOT NULL AND STJ3_TAXCODE_TYPE_CODE='T') " +
						"OR (STJ4_TAXCODE_TYPE_CODE IS NOT NULL AND STJ4_TAXCODE_TYPE_CODE='T') " +
						"OR (STJ5_TAXCODE_TYPE_CODE IS NOT NULL AND STJ5_TAXCODE_TYPE_CODE='T') " +
						"OR (STJ6_TAXCODE_TYPE_CODE IS NOT NULL AND STJ6_TAXCODE_TYPE_CODE='T') " +
						"OR (STJ7_TAXCODE_TYPE_CODE IS NOT NULL AND STJ7_TAXCODE_TYPE_CODE='T') " +
						"OR (STJ8_TAXCODE_TYPE_CODE IS NOT NULL AND STJ8_TAXCODE_TYPE_CODE='T') " +
						"OR (STJ9_TAXCODE_TYPE_CODE IS NOT NULL AND STJ9_TAXCODE_TYPE_CODE='T') " +
						"OR (STJ10_TAXCODE_TYPE_CODE IS NOT NULL AND STJ10_TAXCODE_TYPE_CODE='T'))";
			}
        } else if (status.equalsIgnoreCase("PE")) {	// Processed - Exempt Transactions      //MBF
            trDetail.setTransactionInd("P");
			glExtractFlagIsNull = true;
			if(advancedFilter == null || advancedFilter.length() == 0) {
				advancedFilter = "(STATE_TAXCODE_TYPE_CODE='E' AND COUNTY_TAXCODE_TYPE_CODE='E' AND CITY_TAXCODE_TYPE_CODE='E' " +
						"AND (STJ1_TAXCODE_TYPE_CODE IS NULL OR STJ1_TAXCODE_TYPE_CODE='E') " +
						"AND (STJ2_TAXCODE_TYPE_CODE IS NULL OR STJ2_TAXCODE_TYPE_CODE='E') " +
						"AND (STJ3_TAXCODE_TYPE_CODE IS NULL OR STJ3_TAXCODE_TYPE_CODE='E') " +
						"AND (STJ4_TAXCODE_TYPE_CODE IS NULL OR STJ4_TAXCODE_TYPE_CODE='E') " +
						"AND (STJ5_TAXCODE_TYPE_CODE IS NULL OR STJ5_TAXCODE_TYPE_CODE='E') " +
						"AND (STJ6_TAXCODE_TYPE_CODE IS NULL OR STJ6_TAXCODE_TYPE_CODE='E') " +
						"AND (STJ7_TAXCODE_TYPE_CODE IS NULL OR STJ7_TAXCODE_TYPE_CODE='E') " +
						"AND (STJ8_TAXCODE_TYPE_CODE IS NULL OR STJ8_TAXCODE_TYPE_CODE='E') " +
						"AND (STJ9_TAXCODE_TYPE_CODE IS NULL OR STJ9_TAXCODE_TYPE_CODE='E') " +
						"AND (STJ10_TAXCODE_TYPE_CODE IS NULL OR STJ10_TAXCODE_TYPE_CODE='E'))";
			}
			else {
				advancedFilter += " AND (STATE_TAXCODE_TYPE_CODE='E' AND COUNTY_TAXCODE_TYPE_CODE='E' AND CITY_TAXCODE_TYPE_CODE='E'" +
						"AND (STJ1_TAXCODE_TYPE_CODE IS NULL OR STJ1_TAXCODE_TYPE_CODE='E') " +
						"AND (STJ2_TAXCODE_TYPE_CODE IS NULL OR STJ2_TAXCODE_TYPE_CODE='E') " +
						"AND (STJ3_TAXCODE_TYPE_CODE IS NULL OR STJ3_TAXCODE_TYPE_CODE='E') " +
						"AND (STJ4_TAXCODE_TYPE_CODE IS NULL OR STJ4_TAXCODE_TYPE_CODE='E') " +
						"AND (STJ5_TAXCODE_TYPE_CODE IS NULL OR STJ5_TAXCODE_TYPE_CODE='E') " +
						"AND (STJ6_TAXCODE_TYPE_CODE IS NULL OR STJ6_TAXCODE_TYPE_CODE='E') " +
						"AND (STJ7_TAXCODE_TYPE_CODE IS NULL OR STJ7_TAXCODE_TYPE_CODE='E') " +
						"AND (STJ8_TAXCODE_TYPE_CODE IS NULL OR STJ8_TAXCODE_TYPE_CODE='E') " +
						"AND (STJ9_TAXCODE_TYPE_CODE IS NULL OR STJ9_TAXCODE_TYPE_CODE='E') " +
						"AND (STJ10_TAXCODE_TYPE_CODE IS NULL OR STJ10_TAXCODE_TYPE_CODE='E'))";
			}
		} else if (status.equalsIgnoreCase("PC")) {	// Processed - Closed Processed Transactions 
			trDetail.setTransactionInd("P");
			trDetail.setGlExtractFlag("1");
        } else if(status.equalsIgnoreCase("PR")) {	// Original Transactions       //MBF
            if(advancedFilter == null || advancedFilter.length() == 0) {
                advancedFilter = "(MULTI_TRANS_CODE LIKE 'O%')";
            }
            else {
                advancedFilter += " AND (MULTI_TRANS_CODE LIKE 'O%')";
            }
		} else if (status.equalsIgnoreCase("U")) {	// All Unprocessed Transactions
			includeUnprocessed = true;
		} else if (status.equalsIgnoreCase("US")) {	// Unprocessed - Suspended Transactions
			trDetail.setTransactionInd("S");
		} else if(status.equalsIgnoreCase("UST")) {	// Unprocessed - Suspended - Tax Matrix Transactions
			trDetail.setTransactionInd("S");
			trDetail.setSuspendInd("T");
		} else if(status.equalsIgnoreCase("USL")) {	// Unprocessed - Suspended - Location Matrix Transactions
			trDetail.setTransactionInd("S");
			trDetail.setSuspendInd("L");
		} else if(status.equalsIgnoreCase("USD")) {	// Unprocessed - Suspended - No TaxCode Rule
			trDetail.setTransactionInd("S");
			trDetail.setSuspendInd("D");
		} else if(status.equalsIgnoreCase("USR")) {	// Unprocessed - Suspended - Tax Rate Transactions
			trDetail.setTransactionInd("S");
			trDetail.setSuspendInd("R");
		} else if(status.equalsIgnoreCase("UH")) {	// Unprocessed - Held Transactions
			trDetail.setTransactionInd("H");
		} else if(status.equalsIgnoreCase("UHN")) {	// Unprocessed - Held Normal Transactions
			trDetail.setTransactionInd("H");
			if(advancedFilter == null || advancedFilter.length() == 0) {
				advancedFilter = "(MULTI_TRANS_CODE is null OR MULTI_TRANS_CODE = '')";
			}
			else {
				advancedFilter += " AND (MULTI_TRANS_CODE is null OR MULTI_TRANS_CODE = '')";
			}
		} else if(status.equalsIgnoreCase("FS")) {	// Unprocessed - Future Split Transactions
			trDetail.setTransactionInd("H");
			trDetail.setMultiTransCode("FS");
		}
		String sqlStatement = transactionDetailService.getProcessSQLStatement(trDetail, fromGLDate,
				toGLDate, includeUnprocessed, false, null, glExtractFlagIsNull,
				advancedFilter, transactionDataModel.getSaveAsOrderBy(), 0,
				1000000); //Testing 100,000 rows
		
				// a limit of 10000 records is placed here because of memory restrictions on the server
		        // transactionDataModel.getRowCount()
		return sqlStatement;
	}  
	  

	// Bug 4184, method newly added
	public List<PurchaseTransaction> getRecordsToSave(){

		boolean includeUnprocessed = false;
		boolean glExtractFlagIsNull = false;
		//String taxcodeCode = null;

        PurchaseTransaction trDetail = new PurchaseTransaction();
		
		String state = selectedState;
		if ((state != null) && !state.equals("")) {
			trDetail.setTransactionStateCode(state);
		}
		
		String country = selectedCountry;
		if ((country != null) && !country.equals("")) {
			trDetail.setTransactionCountryCode(country);
		}
		
		// Now, map the drivers to transaction detail properties
		matrixCommonBean.setTransactionProperties(filterSelectionTaxMatrix, trDetail);
		matrixCommonBean.setTransactionProperties(filterSelectionLocationMatrix, trDetail);
		
		if (!getTaxMatrixIdDisabled() && (taxMatrixId != null) && (taxMatrixId > 0L)) {
			trDetail.setTaxMatrixId(taxMatrixId);
		}
		
		if (!getLocationMatrixIdDisabled() && (locationMatrixId != null) && (locationMatrixId > 0L)) {
			trDetail.setShiptoLocnMatrixId(locationMatrixId);
		}
		
		if ((jurisdictionId != null) && (jurisdictionId > 0L)) {
			trDetail.setShiptoJurisdictionId(jurisdictionId);
		}
		
		if ((purchtransId != null) && (purchtransId > 0L)) {
			trDetail.setPurchtransId(purchtransId);
		}
		
		if ((glExtractId != null) && (glExtractId > 0L)) {
			trDetail.setGlExtractBatchNo(glExtractId);
		}
		
		if ((processBatchId != null) && (processBatchId > 0L)) {
			trDetail.setProcessBatchNo(processBatchId);
		}
		
		if(multiTransCode != null && multiTransCode.length() > 0) {
			trDetail.setMultiTransCode(multiTransCode);
		}
		
		if ((subTransId != null) && (subTransId > 0L)) {
			trDetail.setSplitSubtransId(subTransId);
		}

		// Set taxcode filters, if enabled
		if (!getTaxCodeDisabled()) {
			String taxcodeCode = filterTaxCodeHandler.getTaxcodeCode();

			if ((taxcodeCode != null) && !taxcodeCode.equals("")) {
				trDetail.setTaxcodeCode(taxcodeCode);
			}
			
			//CCH			String cchCat = filterTaxCodeHandler.getCchTaxCat();
			//CCH			String cchGroup = filterTaxCodeHandler.getCchGroup();
			//CCH			if ((cchCat != null) && !cchCat.equals("")) {
			//CCH				trDetail.setCchTaxcatCode(cchCat);
			//CCH			}
			//CCH			if ((cchGroup != null) && !cchGroup.equals("")) {
			//CCH				trDetail.setCchGroupCode(cchGroup);
			//CCH			}
		}
		
		String advancedFilter = advancedFilterBean.getAdvancedFilter();
		
		String status = (transactionStatus == null)? "":transactionStatus;
		if (status.equalsIgnoreCase("P")) {	 // All - Processed Transactions
			trDetail.setTransactionInd("P");
		} else if (status.equalsIgnoreCase("PO")) {	// Processed - Open Transactions
			trDetail.setTransactionInd("P");
			glExtractFlagIsNull = true;
		} else if (status.equalsIgnoreCase("PC")) {	// Processed - Closed Processed Transactions 
			trDetail.setTransactionInd("P");
			trDetail.setGlExtractFlag("1");
		} else if (status.equalsIgnoreCase("U")) {	// All Unprocessed Transactions
			includeUnprocessed = true;
		} else if (status.equalsIgnoreCase("US")) {	// Unprocessed - Suspended Transactions
			trDetail.setTransactionInd("S");
		} else if(status.equalsIgnoreCase("UST")) {	// Unprocessed - Suspended - Tax Matrix Transactions
			trDetail.setTransactionInd("S");
			trDetail.setSuspendInd("T");
		} else if(status.equalsIgnoreCase("USL")) {	// Unprocessed - Suspended - Location Matrix Transactions
			trDetail.setTransactionInd("S");
			trDetail.setSuspendInd("L");
		} else if(status.equalsIgnoreCase("USD")) {	// Unprocessed - Suspended - No TaxCode Rule
			trDetail.setTransactionInd("S");
			trDetail.setSuspendInd("D");
		} else if(status.equalsIgnoreCase("USR")) {	// Unprocessed - Suspended - Tax Rate Transactions
			trDetail.setTransactionInd("S");
			trDetail.setSuspendInd("R");
		} else if(status.equalsIgnoreCase("UH")) {	// Unprocessed - Held Transactions
			trDetail.setTransactionInd("H");
		} else if(status.equalsIgnoreCase("UHN")) {	// Unprocessed - Held Normal Transactions
			trDetail.setTransactionInd("H");
			if(advancedFilter == null || advancedFilter.length() == 0) {
				advancedFilter = "(MULTI_TRANS_CODE is null OR MULTI_TRANS_CODE = '')";
			}
			else {
				advancedFilter += " AND (MULTI_TRANS_CODE is null OR MULTI_TRANS_CODE = '')";
			}
		} else if(status.equalsIgnoreCase("FS")) {	// Unprocessed - Future Split Transactions
			trDetail.setTransactionInd("H");
			trDetail.setMultiTransCode("FS");
		}
		List<PurchaseTransaction> list = transactionDetailService.saveRecords	(trDetail, fromGLDate,
				toGLDate, includeUnprocessed, false, null, glExtractFlagIsNull,
				advancedFilter,transactionDataModel.getSaveAsOrderBy(), 0,
				10000); //Testing 100,000 rows
		
				// a limit of 10000 records is placed here because of memory restrictions on the server
		        // transactionDataModel.getRowCount()
		return list;
	}
	
	public String getUserDateTime(){
		String comments = "";
		
		Map<String,ListCodes> timezonMap = matrixCommonBean.getCacheManager().getListCodesMapByType("TIMEZONE");
		ListCodes timeZone = timezonMap.get(matrixCommonBean.getUserPreferenceDTO().getUserTimeZoneID());
		String timeZoneString = (timeZone!=null) ? timeZone.getDescription():"";
		
		DateTimeConverter converter = new DateTimeConverter();
		Date nowDate = converter.getUserLocalDateTime();
		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyy hh:mm:ss");
		comments = dateFormat.format(nowDate) + " (" + timeZoneString + ") - ";
		return comments;
	}
	
	public String insertDateTimeAction(){
		String comments = getCurrentTransaction().getComments();
		comments = comments + getUserDateTime();

		getCurrentTransaction().setComments(comments);
		return null;
	}
	
	public String addCommentsAction() {
		changesMade = true;
		
		//TaxCode content
		/*
		// ALways modify per #2555
		transactionDetailService.update(getCurrentTransaction());
		// in English I think that's saying we will always modify the comments, at least for the first transaction 
		*/
		
		transactionDetailService.update(getCurrentTransaction());
		
		if ((comments != null) && !comments.trim().equals("")) {
			// Append case
			List<Long> ids = new ArrayList<Long>();
			if (applyToAll) {
				for (; currentTransactionIndex <= currentTransactionList.size(); currentTransactionIndex++) {
					ids.add(currentTransactionList.get(currentTransactionIndex-1).getPurchtransId());
				}
			} else {
				ids.add(getCurrentTransaction().getPurchtransId());
			}
			
			logger.debug("Commenting on: " + ids.size());
			
			transactionDetailService.appendComment(ids, getUserDateTime() + comments);
		}
		//transactionDataModel.refreshData(true);
		return nextTransactionAction();
	}
	
	public void viewTransactionAction(PurchaseTransaction transaction, String returnView) {
		selectedTransaction = transaction;
	}
	
	public void statusChangeListener(ActionEvent e) {
		if (getTaxMatrixIdDisabled()) {
			taxMatrixId = null;
		}
		if (getLocationMatrixIdDisabled()) {
			locationMatrixId = null;
			selectedCountry = "";
			if(stateMenuItems!=null) stateMenuItems.clear();
			stateMenuItems = getMatrixCommonBean().getCacheManager().createCountryStateItems(selectedCountry);
			selectedState = "";
		}
		if (getTaxCodeDisabled()) {
			filterTaxCodeHandler.reset();
		}
	}
	
	public List<SelectItem> getFilterItems() {
		List<DwFilter> dwFilterItems= dwFilterService.getFilterNamesByUserCode("TB_PURCHTRANS", matrixCommonBean.getLoginBean().getUserDTO().getUserCode());
		
		filterItems = new ArrayList<SelectItem>();
		filterItems.add(new SelectItem("", "*NOFILTER"));
		
		for(DwFilter dwFilter : dwFilterItems){
			filterItems.add(new SelectItem(dwFilter.getUserCode() + "/" + dwFilter.getFilterName(), dwFilter.getUserCode() + "/" + dwFilter.getFilterName()));
		}
		return filterItems;
	}
	
	public void setFilterItems(List<SelectItem> filterItems) {
		this.filterItems = filterItems;
	}
	
	public void filterChangeListener(ActionEvent e) {	
		filterSelectionTaxMatrix = new TaxMatrix();
		filterSelectionLocationMatrix = new LocationMatrix();
		filterTaxCodeHandler.reset();
		fromGLDate = null;
		toGLDate = null;
		if(selectedFilter!=null && selectedFilter.length()>0){
			transactionStatus= "";
		}
		purchtransId = null;
		jurisdictionId = null;
		taxMatrixId = null;
		locationMatrixId = null;
		glExtractId = null;
		processBatchId = null;
		multiTransCode = null;
		subTransId = null;
		
		selectedCountry = "*ALL";
		selectedState = "*ALL";
		selectedTaxabilityCode="";
		
		selectionFilterBean.setUserDTO(matrixCommonBean.getLoginBean().getUserDTO());
		if(selectedFilter != "") {
			selectedUserName = selectedFilter.substring(0, selectedFilter.indexOf("/"));
			selectedFilterName = selectedFilter.substring(selectedFilter.indexOf("/") + 1 ,selectedFilter.length());
			selectedFilter = selectedUserName + "/" + selectedFilterName;
			dwFilters = dwFilterService.getColumnsByFilterNameandUser("TB_PURCHTRANS", selectedUserName, selectedFilterName);
		}
		
		else {
			selectionFilterBean.setCurrentFilterName("");
		}
		

		//Create filters column, values map
		Map<String, String> columnValuesMap = new HashMap<String, String>();
		for(DwFilter dwFilter   : dwFilters){
			columnValuesMap.put(dwFilter.getColumnName(), dwFilter.getFilterValue());
		}
		
		String value = "";
		String matrixColumnName = "";
		Map<String,DriverNames> taxMatrixDriverNamesMap = getMatrixCommonBean().getCacheManager().getTransactionPropertyDriverNamesMap(TaxMatrix.DRIVER_CODE);
		for (String columnName : columnValuesMap.keySet()) {
			value = columnValuesMap.get(columnName);		
			if (taxMatrixDriverNamesMap.get(columnName) != null) {
				matrixColumnName =  taxMatrixDriverNamesMap.get(columnName).getMatrixColName();			
				Util.setProperty(filterSelectionTaxMatrix, Util.columnToProperty(matrixColumnName), value, String.class);
			}
		}
		
		Map<String,DriverNames> locationMatrixDriverNamesMap = getMatrixCommonBean().getCacheManager().getTransactionPropertyDriverNamesMap(LocationMatrix.DRIVER_CODE);
		for (String columnName : columnValuesMap.keySet()) {
			value = columnValuesMap.get(columnName);
			if (locationMatrixDriverNamesMap.get(columnName) != null) {
				matrixColumnName =  locationMatrixDriverNamesMap.get(columnName).getMatrixColName();			
				Util.setProperty(filterSelectionLocationMatrix, Util.columnToProperty(matrixColumnName), value, String.class);
			}
		}
		
		//Set Advanced filter
		if(columnValuesMap.get("*ADVANCED")!=null){
			advancedFilterBean.setAdvancedFilter(columnValuesMap.get("*ADVANCED"));
		}
		else{
			advancedFilterBean.setAdvancedFilter("");
		}
		
		//Set country code
		if(columnValuesMap.get("transactionCountryCode")!=null){
			selectedCountry = columnValuesMap.get("transactionCountryCode");
		}
		//Redo country
		if(countryMenuItems!=null) countryMenuItems.clear();
		countryMenuItems = getMatrixCommonBean().getCacheManager().createCountryItems();   
		
		//Set states
		if(stateMenuItems!=null) stateMenuItems.clear();
		stateMenuItems = getMatrixCommonBean().getCacheManager().createStateItems(selectedCountry);
		
		if(columnValuesMap.get("transactionStateCode")!=null){
			selectedState = columnValuesMap.get("transactionStateCode");
		}
		
		if(columnValuesMap.get("transactionStatus")!=null){
			transactionStatus = columnValuesMap.get("transactionStatus");
		}
		
		//purchtransId
		if(columnValuesMap.get("purchtransId")!=null){
			String purchtransIdStr = columnValuesMap.get("purchtransId");
			try{		
				purchtransId = Long.parseLong(purchtransIdStr);
			}
			catch(Exception ee){	
			}
		}
		
		if(columnValuesMap.get("taxMatrixId")!=null){
			String taxMatrixIdStr = columnValuesMap.get("taxMatrixId");
			try{		
				taxMatrixId = Long.parseLong(taxMatrixIdStr);
			}
			catch(Exception ee){	
			}
		}
		
		if(columnValuesMap.get("locationMatrixId")!=null){
			String locationMatrixIdStr = columnValuesMap.get("locationMatrixId");
			try{		
				locationMatrixId = Long.parseLong(locationMatrixIdStr);
			}
			catch(Exception ee){	
			}
		}
		
		if(columnValuesMap.get("jurisdictionId")!=null){
			String jurisdictionIdStr = columnValuesMap.get("jurisdictionId");
			try{		
				jurisdictionId = Long.parseLong(jurisdictionIdStr);
			}
			catch(Exception ee){	
			}
		}
		
		if(columnValuesMap.get("glExtractBatchNo")!=null){
			String glExtractBatchNoStr = columnValuesMap.get("glExtractBatchNo");
			try{		
				glExtractId = Long.parseLong(glExtractBatchNoStr);
			}
			catch(Exception ee){	
			}
		}
		
		if(columnValuesMap.get("processBatchNo")!=null){
			String processBatchNoStr = columnValuesMap.get("processBatchNo");
			try{		
				processBatchId = Long.parseLong(processBatchNoStr);
			}
			catch(Exception ee){	
			}
		}
		
		if(columnValuesMap.get("splitSubtransId")!=null){
			String splitSubtransIdStr = columnValuesMap.get("splitSubtransId");
			try{		
				subTransId = Long.parseLong(splitSubtransIdStr);
			}
			catch(Exception ee){	
			}
		}
		
		if(columnValuesMap.get("multiTransCode")!=null){
			multiTransCode = columnValuesMap.get("multiTransCode");
		}
		
		if(columnValuesMap.get("taxabilityCode")!=null){
			selectedTaxabilityCode = columnValuesMap.get("taxabilityCode");
		}
		
		if(columnValuesMap.get("taxcodeCode")!=null){
			String taxcodeCode = columnValuesMap.get("taxcodeCode");
			filterTaxCodeHandler.setTaxcodeCode(taxcodeCode);
		}
		
		SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
		if(columnValuesMap.get("fromGLDate")!=null){
			String fromGLDateString = columnValuesMap.get("fromGLDate");
			try{
				fromGLDate =  format.parse(fromGLDateString);
			}
			catch(Exception e1){			
			}
		}
		if(columnValuesMap.get("toGLDate")!=null){
			String toGLDateString = columnValuesMap.get("toGLDate");
			try{
				toGLDate =  format.parse(toGLDateString);
			}
			catch(Exception e1){			
			}
		}
	}
	
	public String filterEditAction() {
		List<DwFilter> dwFilterList = new ArrayList<DwFilter>();
		
		//Create filters column, values map
		Map<String, String> columnValuesMap = new HashMap<String, String>();
		
		String value = "";
		String transactionColumnname = "";
		String name = "";
		Map<String,DriverNames> taxDriversMap =  getMatrixCommonBean().getCacheManager().getMatrixColDriverNamesMap(TaxMatrix.DRIVER_CODE);
		for (String matrixColumnName : taxDriversMap.keySet()) {
			DriverNames driverNames = taxDriversMap.get(matrixColumnName);
			transactionColumnname = driverNames.getTransDtlColName();
			value = (String) Util.getProperty(filterSelectionTaxMatrix, Util.columnToProperty(matrixColumnName));			
			if(value!=null && value.length()>0){
				name = Util.columnToProperty(transactionColumnname);
				if(columnValuesMap.get(name)==null){
					columnValuesMap.put(name, value);
					
					DwFilter dwFilter = new DwFilter(new DwFilterPK("","","",name));
					dwFilter.setFilterValue(value);
					dwFilterList.add(dwFilter);
				}
			}
		}
		
		Map<String,DriverNames> locationDriversMap =  getMatrixCommonBean().getCacheManager().getMatrixColDriverNamesMap(LocationMatrix.DRIVER_CODE);
		for (String matrixColumnName : locationDriversMap.keySet()) {
			DriverNames driverNames = locationDriversMap.get(matrixColumnName);
			transactionColumnname = driverNames.getTransDtlColName();
			value = (String) Util.getProperty(filterSelectionLocationMatrix, Util.columnToProperty(matrixColumnName));	
			if(value!=null && value.length()>0){
				name = Util.columnToProperty(transactionColumnname);
				if(columnValuesMap.get(name)==null){
					columnValuesMap.put(name, value);
					
					DwFilter dwFilter = new DwFilter(new DwFilterPK("","","",name));
					dwFilter.setFilterValue(value);
					dwFilterList.add(dwFilter);
				}
			}
		}
		
		//Other fields
		String country = selectedCountry;
		if ((country != null) && !country.equals("")) {
			columnValuesMap.put("transactionCountryCode", country);	
			DwFilter dwFilter = new DwFilter(new DwFilterPK("","","","transactionCountryCode"));
			dwFilter.setFilterValue(country);
			dwFilterList.add(dwFilter);
		}
		
		String state = selectedState;
		if ((state != null) && !state.equals("")) {
			columnValuesMap.put("transactionStateCode", state);		
			DwFilter dwFilter = new DwFilter(new DwFilterPK("","","","transactionStateCode"));
			dwFilter.setFilterValue(state);
			dwFilterList.add(dwFilter);
		}
		
		String status = (transactionStatus == null)? "":transactionStatus;
		if(status.length() > 0) {
			columnValuesMap.put("transactionStatus", status);
			
			DwFilter dwFilter = new DwFilter(new DwFilterPK("","","","transactionStatus"));
			dwFilter.setFilterValue(status);
			dwFilterList.add(dwFilter);
		}
		
		if ((purchtransId != null) && (purchtransId > 0L)) {
			columnValuesMap.put("purchtransId", purchtransId.toString());		
			DwFilter dwFilter = new DwFilter(new DwFilterPK("","","","purchtransId"));
			dwFilter.setFilterValue(purchtransId.toString());
			dwFilterList.add(dwFilter);
		}
		
		if (!getTaxMatrixIdDisabled() && (taxMatrixId != null) && (taxMatrixId > 0L)) {
			columnValuesMap.put("taxMatrixId", taxMatrixId.toString());
			DwFilter dwFilter = new DwFilter(new DwFilterPK("","","","taxMatrixId"));
			dwFilter.setFilterValue(taxMatrixId.toString());
			dwFilterList.add(dwFilter);
		}
		
		if (!getLocationMatrixIdDisabled() && (locationMatrixId != null) && (locationMatrixId > 0L)) {
			columnValuesMap.put("locationMatrixId", locationMatrixId.toString());			
			DwFilter dwFilter = new DwFilter(new DwFilterPK("","","","locationMatrixId"));
			dwFilter.setFilterValue(locationMatrixId.toString());
			dwFilterList.add(dwFilter);
		}
		
		if ((jurisdictionId != null) && (jurisdictionId > 0L)) {
			columnValuesMap.put("jurisdictionId", jurisdictionId.toString());	
			DwFilter dwFilter = new DwFilter(new DwFilterPK("","","","jurisdictionId"));
			dwFilter.setFilterValue(jurisdictionId.toString());
			dwFilterList.add(dwFilter);
		}

		if ((glExtractId != null) && (glExtractId > 0L)) {
			columnValuesMap.put("glExtractBatchNo", glExtractId.toString());	
			DwFilter dwFilter = new DwFilter(new DwFilterPK("","","","glExtractBatchNo"));
			dwFilter.setFilterValue(glExtractId.toString());
			dwFilterList.add(dwFilter);
		}
		
		if ((processBatchId != null) && (processBatchId > 0L)) {
			columnValuesMap.put("processBatchNo", processBatchId.toString());	
			DwFilter dwFilter = new DwFilter(new DwFilterPK("","","","processBatchNo"));
			dwFilter.setFilterValue(processBatchId.toString());
			dwFilterList.add(dwFilter);
		}

		if ((subTransId != null) && (subTransId > 0L)) {
			columnValuesMap.put("splitSubtransId", subTransId.toString());		
			DwFilter dwFilter = new DwFilter(new DwFilterPK("","","","splitSubtransId"));
			dwFilter.setFilterValue(subTransId.toString());
			dwFilterList.add(dwFilter);
		}
		
		if(multiTransCode != null && multiTransCode.length() > 0) {
			columnValuesMap.put("multiTransCode", multiTransCode);	
			DwFilter dwFilter = new DwFilter(new DwFilterPK("","","","multiTransCode"));
			dwFilter.setFilterValue(multiTransCode);
			dwFilterList.add(dwFilter);
		}

        if(selectedTaxabilityCode!=null && !selectedTaxabilityCode.equals("") && !getTaxableTypeDisabled()){
			columnValuesMap.put("taxabilityCode", selectedTaxabilityCode.trim());	
			DwFilter dwFilter = new DwFilter(new DwFilterPK("","","","taxabilityCode"));
			dwFilter.setFilterValue(selectedTaxabilityCode.trim());
			dwFilterList.add(dwFilter);
        }
        
        // Set taxcode filters, if enabled
 		if (!getTaxCodeDisabled()) {
 			String taxcodeCode = filterTaxCodeHandler.getTaxcodeCode();	
 			if ((taxcodeCode != null) && !taxcodeCode.equals("")) {
 				columnValuesMap.put("taxcodeCode", taxcodeCode);		
 				DwFilter dwFilter = new DwFilter(new DwFilterPK("","","","taxcodeCode"));
 				dwFilter.setFilterValue(taxcodeCode);
 				dwFilterList.add(dwFilter);
 			}
 		}
        
        SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy"); 
        if(fromGLDate!=null){
        	String dateString = format.format(fromGLDate);
			columnValuesMap.put("fromGLDate", dateString);	
			DwFilter dwFilter = new DwFilter(new DwFilterPK("","","","fromGLDate"));
			dwFilter.setFilterValue(dateString);
			dwFilterList.add(dwFilter);
        }
        
        if(toGLDate!=null){
        	String dateString = format.format(toGLDate);
			columnValuesMap.put("toGLDate", dateString);	
			DwFilter dwFilter = new DwFilter(new DwFilterPK("","","","toGLDate"));
			dwFilter.setFilterValue(dateString);
			dwFilterList.add(dwFilter);
        }
        
        String advancedFilter = advancedFilterBean.getAdvancedFilter(); 
		if(advancedFilter!=null && advancedFilter.trim().length()>0){
			columnValuesMap.put("*ADVANCED", advancedFilter.trim());		
			DwFilter dwFilter = new DwFilter(new DwFilterPK("","","","*ADVANCED"));
			dwFilter.setFilterValue(advancedFilter.trim());
			dwFilterList.add(dwFilter);
		}
		
		dwFilters = dwFilterService.getColumnsByFilterNameandUser("TB_PURCHTRANS", selectedUserName, selectedFilter);
		selectionFilterBean.setUserDTO(matrixCommonBean.getLoginBean().getUserDTO());
		if(selectedFilter != "") {
			selectedUserName = selectedFilter.substring(0, selectedFilter.indexOf("/"));
			selectedFilterName = selectedFilter.substring(selectedFilter.indexOf("/") + 1 ,selectedFilter.length());
			selectedFilter = selectedUserName + "/" + selectedFilterName;
			dwFilters = dwFilterService.getColumnsByFilterNameandUser("TB_PURCHTRANS", selectedUserName, selectedFilterName);
			selectionFilterBean.setCurrentFilterName(selectedFilter);
		}
		else {
			selectionFilterBean.setCurrentFilterName("");
		}
		selectionFilterBean.setScreenName("TB_PURCHTRANS");
		selectionFilterBean.setDwFilterList(dwFilterList);
		selectionFilterBean.setDwFilters(dwFilters);  
		selectionFilterBean.setallbackBeanC(this);
		selectionFilterBean.setCallingScreen(TRANSACTION_PROCESS_CALLING_SCREEN);

		for (int i=0; i<dwFilterList.size() ; i++) {
			DwFilter dwFilter = dwFilterList.get(i);
			dwFilter.setWindowName("TB_PURCHTRANS");
			dwFilter.setUserCode(matrixCommonBean.getLoginBean().getUserDTO().getUserCode());
		}

		return "filter_update";
	}
	
	
	public List<SelectItem> getViewItems() {
		if (viewItems == null) {
			List<TransactionHeader> transactionHeaders = dwColumnService.getFilterNamesByUserCode("TB_PURCHTRANS", matrixCommonBean.getLoginBean().getUserDTO().getUserCode());
			
			viewItems = new ArrayList<SelectItem>();
			whatIfMap = new HashMap<String, String>();
			viewItems.add(new SelectItem("", "*GLOBAL/*DEFAULT"));
			whatIfMap.put("*DEFAULT", "*GLOBAL");
			for(TransactionHeader transactionHeader : transactionHeaders){
				if(!transactionHeader.getDataWindowName().equalsIgnoreCase("*DEFAULT")){
					viewItems.add(new SelectItem(transactionHeader.getUserCode()  + "/" +  transactionHeader.getDataWindowName(), transactionHeader.getUserCode()  + "/" +  transactionHeader.getDataWindowName()));
					whatIfMap.put(transactionHeader.getDataWindowName(), transactionHeader.getUserCode()) ;
				}
			}
		}
		taxMatrixBean.setWhatIfMap(whatIfMap); 
		return viewItems;
	}
	
	public void setViewItems(List<SelectItem> viewItems) {
		this.viewItems = viewItems;
	}
	
	///Albert Chen ****************************
	public void viewChangeListener(ActionEvent e) {	

		String searchColumnName = "";
		String selectedUserName = null;
		List<TransactionHeader> transactionHeaders = null;

		if(selectedColumn==null || selectedColumn.length()==0){
			selectedUserName = "*GLOBAL";
			searchColumnName = "*DEFAULT";
			setSelectedColumn(selectedUserName + "/" + searchColumnName);
		}
		else{
			selectedUserName = selectedColumn.substring(0, selectedColumn.indexOf("/"));

			searchColumnName = selectedColumn.substring(selectedColumn.indexOf("/") + 1 ,selectedColumn.length());;
		}
		transactionHeaders = dwColumnService.getColumnsByUserandDataWindowName("TB_PURCHTRANS", searchColumnName, selectedUserName);
		if((transactionHeaders==null || transactionHeaders.size()==0) && searchColumnName.equalsIgnoreCase("*DEFAULT")){
			//get *DEFAULT
			//selectionColumnBean.createDefaultView();
			//transactionHeaders = dwColumnService.getColumnsByDataWindowName("TB_TRANSACTION_DETAIL", "*DEFAULT");
			
			transactionHeaders = selectionColumnBean.getDefaultView(); //
		}
		
		//Save last view
		User user = userService.findById(matrixCommonBean.getLoginBean().getUserDTO().getUserCode());
		user.setLastPurchView(selectedColumn);
		userService.saveOrUpdate(user);
		

		taxMatrixBean.setSearchColumnNameLocal(searchColumnName);
		taxMatrixBean.setSelectedUserNameLocal(selectedUserName);
		taxMatrixBean.setSelectedColumn(selectedColumn);
		taxMatrixBean.setViewChanged(Boolean.TRUE);
		
		locationMatrixBean.setSearchColumnNameLocal(searchColumnName);
		locationMatrixBean.setSelectedUserNameLocal(selectedUserName);
		locationMatrixBean.setSelectedColumn(selectedColumn);
		locationMatrixBean.setViewChanged(Boolean.TRUE);
		
		int columnCount = matrixCommonBean.createViewTransactionDetailTable(trDetailTable,
				"transactionDetailBean", "transactionDetailBean.transactionDataModel", searchColumnName, true, selectedUserName, null);
		
		String rowStr = (columnCount>100)? matrixCommonBean.getMatrixDTO().getSysFullThreshold() : 
			matrixCommonBean.getMatrixDTO().getSysLimitedThreshold();
		int rowpage = 0;
		if(rowStr==null || rowStr.length()==0){
			//rowpage = (displayAllFields)? 25:100; 
			rowpage = (columnCount>100)? 25:100; 
		}
		else{
			rowpage = Integer.parseInt(rowStr);
		}
		
		String muitipleStr = (columnCount>100)? matrixCommonBean.getMatrixDTO().getSysFullThresholdPages() : 
			matrixCommonBean.getMatrixDTO().getSysLimitedThresholdPages();
		int muitiplepage = 0;
		if(muitipleStr==null || muitipleStr.length()==0){
			muitiplepage = (columnCount>100)? 4:4; 
		}
		else{
			muitiplepage = Integer.parseInt(muitipleStr);
		}
		
		System.out.println("Select");
		
		transactionDataModel.setPageSize(rowpage);
		transactionDataModel.setDbFetchMultiple(muitiplepage);
		setSelectedColumn(selectedColumn);
		setSelectedUserName(selectedUserName);
		setLastViewColumn(selectedColumn);
		//resetForHeaderRearrangeTemp();
		
		LogMemory.executeGC();
	}
	
	public String getLastView() {
		String lastView = optionService.getUserOption(matrixCommonBean.getLoginBean().getUserDTO().getUserCode(), "SAVEGRIDCHANGES");
		if (lastView!=null && lastView.equalsIgnoreCase("*LASTVIEW")) {
			//TODO: retrieve last view from tb_user.last_purch_view
			User user = userService.findById(matrixCommonBean.getLoginBean().getUserDTO().getUserCode().toUpperCase());
			return user.getLastPurchView();
		}
		else if(lastView!=null && lastView.length()>0){
			return lastView;
		}
		else{
			return "";
		}

	}
	
	public String viewEditAction() {
		List<DwFilter> dwFilterList = new ArrayList<DwFilter>();
		
		//xxxxxxxxxxxxxx
		
		selectionColumnBean.setUserDTO(matrixCommonBean.getLoginBean().getUserDTO());
		selectionColumnBean.setScreenName("TB_PURCHTRANS");
		//selectionColumnBean.setDwColumnList(dwFilterList);
		selectionColumnBean.setCurrentColumnName(selectedColumn);
		selectionColumnBean.setallbackBeanC(this);
		

		setLastViewColumn(selectedColumn.substring(selectedColumn.indexOf("/") + 1));
		/*
		
		//Create filters column, values map
		Map<String, String> columnValuesMap = new HashMap<String, String>();
		
		String value = "";
		String transactionColumnname = "";
		String name = "";
		Map<String,DriverNames> taxDriversMap =  getMatrixCommonBean().getCacheManager().getMatrixColDriverNamesMap(TaxMatrix.DRIVER_CODE);
		for (String matrixColumnName : taxDriversMap.keySet()) {
			DriverNames driverNames = taxDriversMap.get(matrixColumnName);
			transactionColumnname = driverNames.getTransDtlColName();
			value = (String) Util.getProperty(filterSelectionTaxMatrix, Util.columnToProperty(matrixColumnName));			
			if(value!=null && value.length()>0){
				name = Util.columnToProperty(transactionColumnname);
				if(columnValuesMap.get(name)==null){
					columnValuesMap.put(name, value);
					
					DwFilter dwFilter = new DwFilter(new DwFilterPK("","","",name));
					dwFilter.setFilterValue(value);
					dwFilterList.add(dwFilter);
				}
			}
		}
		
		Map<String,DriverNames> locationDriversMap =  getMatrixCommonBean().getCacheManager().getMatrixColDriverNamesMap(LocationMatrix.DRIVER_CODE);
		for (String matrixColumnName : locationDriversMap.keySet()) {
			DriverNames driverNames = locationDriversMap.get(matrixColumnName);
			transactionColumnname = driverNames.getTransDtlColName();
			value = (String) Util.getProperty(filterSelectionLocationMatrix, Util.columnToProperty(matrixColumnName));	
			if(value!=null && value.length()>0){
				name = Util.columnToProperty(transactionColumnname);
				if(columnValuesMap.get(name)==null){
					columnValuesMap.put(name, value);
					
					DwFilter dwFilter = new DwFilter(new DwFilterPK("","","",name));
					dwFilter.setFilterValue(value);
					dwFilterList.add(dwFilter);
				}
			}
		}
		
		
		String status = (transactionStatus == null)? "":transactionStatus;
		if(status.length() > 0) {
			columnValuesMap.put("transactionStatus", status);
			
			DwFilter dwFilter = new DwFilter(new DwFilterPK("","","","transactionStatus"));
			dwFilter.setFilterValue(status);
			dwFilterList.add(dwFilter);
		}
		
		if ((transactionId != null) && (transactionId > 0L)) {
			columnValuesMap.put("transactionDetailId", transactionId.toString());		
			DwFilter dwFilter = new DwFilter(new DwFilterPK("","","","transactionDetailId"));
			dwFilter.setFilterValue(transactionId.toString());
			dwFilterList.add(dwFilter);
		}
		
		
		selectionColumnBean.setUserDTO(matrixCommonBean.getLoginBean().getUserDTO());
		selectionColumnBean.setScreenName("TB_TRANSACTION_DETAIL");
		selectionColumnBean.setDwColumnList(dwFilterList);
		selectionColumnBean.setCurrentColumnName(selectedColumn);
		selectionColumnBean.setallbackBeanC(this);

		for (int i=0; i<dwFilterList.size() ; i++) {
			DwFilter dwFilter = dwFilterList.get(i);
			dwFilter.setWindowName("TB_TRANSACTION_DETAIL");
			dwFilter.setUserCode(matrixCommonBean.getLoginBean().getUserDTO().getUserCode());
		}
		*/
		
		return "column_update";
	}

	public String listTransactions() {         
		logger.info(" --------------------- enter getTransactionsList() --------------------------------- ");
		
		boolean includeUnprocessed = false;
		boolean glExtractFlagIsNull = false;
		//String taxcodeCode = null;

		this.indicateAllSelected = false; //reset the all selected check, since they all won't be
		
        PurchaseTransaction trDetail = new PurchaseTransaction();

		String state = selectedState;
		if(state==null || state.length()==0 || state.equalsIgnoreCase("*ALL")) {
			trDetail.setTransactionStateCode("");
		}
		else if ((state != null) && !state.equals("")) {
			trDetail.setTransactionStateCode(state);
		}
		

		String country = selectedCountry;
		if(country==null || country.length()==0 || country.equalsIgnoreCase("*ALL")) {
			trDetail.setTransactionCountryCode("");
		}
		else if ((country != null) && !country.equals("")) {
			trDetail.setTransactionCountryCode(country);
		}
		
		// Now, map the drivers to transaction detail properties
		matrixCommonBean.setTransactionProperties(filterSelectionTaxMatrix, trDetail);
		matrixCommonBean.setTransactionProperties(filterSelectionLocationMatrix, trDetail);
		
		if (!getTaxMatrixIdDisabled() && (taxMatrixId != null) && (taxMatrixId > 0L)) {
			trDetail.setTaxMatrixId(taxMatrixId);
		}
		
		if (!getLocationMatrixIdDisabled() && (locationMatrixId != null) && (locationMatrixId > 0L)) {
			trDetail.setShiptoLocnMatrixId(locationMatrixId);
		}
		
		if ((jurisdictionId != null) && (jurisdictionId > 0L)) {
			trDetail.setShiptoJurisdictionId(jurisdictionId);
		}
		
		if ((purchtransId != null) && (purchtransId > 0L)) {
			trDetail.setPurchtransId(purchtransId);
		}
		
		if ((glExtractId != null) && (glExtractId > 0L)) {
			trDetail.setGlExtractBatchNo(glExtractId);
		}
		
		if ((processBatchId != null) && (processBatchId > 0L)) {
			trDetail.setProcessBatchNo(processBatchId);
		}
		
		if(multiTransCode != null && multiTransCode.length() > 0) {
			trDetail.setMultiTransCode(multiTransCode);
		}
		
		if ((subTransId != null) && (subTransId > 0L)) {
			trDetail.setSplitSubtransId(subTransId);
		}

		// Set taxcode filters, if enabled
		if (!getTaxCodeDisabled()) {
			String taxcodeCode = filterTaxCodeHandler.getTaxcodeCode();
			
			if ((taxcodeCode != null) && !taxcodeCode.equals("")) {
				trDetail.setTaxcodeCode(taxcodeCode);
			}
			
			//CCH			String cchCat = filterTaxCodeHandler.getCchTaxCat();
			//CCH			String cchGroup = filterTaxCodeHandler.getCchGroup();
			//CCH			if ((cchCat != null) && !cchCat.equals("")) {
			//CCH				trDetail.setCchTaxcatCode(cchCat);
			//CCH			}
			//CCH			if ((cchGroup != null) && !cchGroup.equals("")) {
			//CCH				trDetail.setCchGroupCode(cchGroup);
			//CCH			}
		}
		
		String advancedFilter = advancedFilterBean.getAdvancedFilter();

		if (advancedFilter == null )
			advancedFilter = "";
		
		String status = (transactionStatus == null)? "":transactionStatus;
		if (status.equalsIgnoreCase("P")) {	 // All - Processed Transactions
			trDetail.setTransactionInd("P");
		} else if (status.equalsIgnoreCase("PO")) {	// Processed - Open Transactions   // MBF:PP-234
			trDetail.setTransactionInd("P");
			glExtractFlagIsNull = true;
			if(advancedFilter == null || advancedFilter.length() == 0) {
				advancedFilter = "(STATE_TAXCODE_TYPE_CODE='T' OR COUNTY_TAXCODE_TYPE_CODE='T' OR CITY_TAXCODE_TYPE_CODE='T' " +
						"OR (COUNTRY_TAXCODE_TYPE_CODE IS NOT NULL AND COUNTRY_TAXCODE_TYPE_CODE='T') " +
						"OR (STJ1_TAXCODE_TYPE_CODE IS NOT NULL AND STJ1_TAXCODE_TYPE_CODE='T') " +
						"OR (STJ2_TAXCODE_TYPE_CODE IS NOT NULL AND STJ2_TAXCODE_TYPE_CODE='T') " +
						"OR (STJ3_TAXCODE_TYPE_CODE IS NOT NULL AND STJ3_TAXCODE_TYPE_CODE='T') " +
						"OR (STJ4_TAXCODE_TYPE_CODE IS NOT NULL AND STJ4_TAXCODE_TYPE_CODE='T') " +
						"OR (STJ5_TAXCODE_TYPE_CODE IS NOT NULL AND STJ5_TAXCODE_TYPE_CODE='T') " +
						"OR (STJ6_TAXCODE_TYPE_CODE IS NOT NULL AND STJ6_TAXCODE_TYPE_CODE='T') " +
						"OR (STJ7_TAXCODE_TYPE_CODE IS NOT NULL AND STJ7_TAXCODE_TYPE_CODE='T') " +
						"OR (STJ8_TAXCODE_TYPE_CODE IS NOT NULL AND STJ8_TAXCODE_TYPE_CODE='T') " +
						"OR (STJ9_TAXCODE_TYPE_CODE IS NOT NULL AND STJ9_TAXCODE_TYPE_CODE='T') " +
						"OR (STJ10_TAXCODE_TYPE_CODE IS NOT NULL AND STJ10_TAXCODE_TYPE_CODE='T'))";
			}
			else {
				advancedFilter += " AND (STATE_TAXCODE_TYPE_CODE='T' OR COUNTY_TAXCODE_TYPE_CODE='T' OR CITY_TAXCODE_TYPE_CODE='T' " +
						"OR (COUNTRY_TAXCODE_TYPE_CODE IS NOT NULL AND COUNTRY_TAXCODE_TYPE_CODE='T') " +
						"OR (STJ1_TAXCODE_TYPE_CODE IS NOT NULL AND STJ1_TAXCODE_TYPE_CODE='T') " +
						"OR (STJ2_TAXCODE_TYPE_CODE IS NOT NULL AND STJ2_TAXCODE_TYPE_CODE='T') " +
						"OR (STJ3_TAXCODE_TYPE_CODE IS NOT NULL AND STJ3_TAXCODE_TYPE_CODE='T') " +
						"OR (STJ4_TAXCODE_TYPE_CODE IS NOT NULL AND STJ4_TAXCODE_TYPE_CODE='T') " +
						"OR (STJ5_TAXCODE_TYPE_CODE IS NOT NULL AND STJ5_TAXCODE_TYPE_CODE='T') " +
						"OR (STJ6_TAXCODE_TYPE_CODE IS NOT NULL AND STJ6_TAXCODE_TYPE_CODE='T') " +
						"OR (STJ7_TAXCODE_TYPE_CODE IS NOT NULL AND STJ7_TAXCODE_TYPE_CODE='T') " +
						"OR (STJ8_TAXCODE_TYPE_CODE IS NOT NULL AND STJ8_TAXCODE_TYPE_CODE='T') " +
						"OR (STJ9_TAXCODE_TYPE_CODE IS NOT NULL AND STJ9_TAXCODE_TYPE_CODE='T') " +
						"OR (STJ10_TAXCODE_TYPE_CODE IS NOT NULL AND STJ10_TAXCODE_TYPE_CODE='T'))";
			}
		} else if (status.equalsIgnoreCase("PE")) {	// Processed - Exempt Transactions   // MBF:PP-234
			trDetail.setTransactionInd("P");
			glExtractFlagIsNull = true;
			if(advancedFilter == null || advancedFilter.length() == 0) {
				advancedFilter = "(STATE_TAXCODE_TYPE_CODE='E' AND COUNTY_TAXCODE_TYPE_CODE='E' AND CITY_TAXCODE_TYPE_CODE='E' " +
						"AND (COUNTRY_TAXCODE_TYPE_CODE IS NULL OR COUNTRY_TAXCODE_TYPE_CODE='E') " +
						"AND (STJ1_TAXCODE_TYPE_CODE IS NULL OR STJ1_TAXCODE_TYPE_CODE='E') " +
						"AND (STJ2_TAXCODE_TYPE_CODE IS NULL OR STJ2_TAXCODE_TYPE_CODE='E') " +
						"AND (STJ3_TAXCODE_TYPE_CODE IS NULL OR STJ3_TAXCODE_TYPE_CODE='E') " +
						"AND (STJ4_TAXCODE_TYPE_CODE IS NULL OR STJ4_TAXCODE_TYPE_CODE='E') " +
						"AND (STJ5_TAXCODE_TYPE_CODE IS NULL OR STJ5_TAXCODE_TYPE_CODE='E') " +
						"AND (STJ6_TAXCODE_TYPE_CODE IS NULL OR STJ6_TAXCODE_TYPE_CODE='E') " +
						"AND (STJ7_TAXCODE_TYPE_CODE IS NULL OR STJ7_TAXCODE_TYPE_CODE='E') " +
						"AND (STJ8_TAXCODE_TYPE_CODE IS NULL OR STJ8_TAXCODE_TYPE_CODE='E') " +
						"AND (STJ9_TAXCODE_TYPE_CODE IS NULL OR STJ9_TAXCODE_TYPE_CODE='E') " +
						"AND (STJ10_TAXCODE_TYPE_CODE IS NULL OR STJ10_TAXCODE_TYPE_CODE='E'))";
			}
			else {
				advancedFilter += " AND (STATE_TAXCODE_TYPE_CODE='E' AND COUNTY_TAXCODE_TYPE_CODE='E' AND CITY_TAXCODE_TYPE_CODE='E'" +
						"AND (COUNTRY_TAXCODE_TYPE_CODE IS NULL OR COUNTRY_TAXCODE_TYPE_CODE='E') " +
						"AND (STJ1_TAXCODE_TYPE_CODE IS NULL OR STJ1_TAXCODE_TYPE_CODE='E') " +
						"AND (STJ2_TAXCODE_TYPE_CODE IS NULL OR STJ2_TAXCODE_TYPE_CODE='E') " +
						"AND (STJ3_TAXCODE_TYPE_CODE IS NULL OR STJ3_TAXCODE_TYPE_CODE='E') " +
						"AND (STJ4_TAXCODE_TYPE_CODE IS NULL OR STJ4_TAXCODE_TYPE_CODE='E') " +
						"AND (STJ5_TAXCODE_TYPE_CODE IS NULL OR STJ5_TAXCODE_TYPE_CODE='E') " +
						"AND (STJ6_TAXCODE_TYPE_CODE IS NULL OR STJ6_TAXCODE_TYPE_CODE='E') " +
						"AND (STJ7_TAXCODE_TYPE_CODE IS NULL OR STJ7_TAXCODE_TYPE_CODE='E') " +
						"AND (STJ8_TAXCODE_TYPE_CODE IS NULL OR STJ8_TAXCODE_TYPE_CODE='E') " +
						"AND (STJ9_TAXCODE_TYPE_CODE IS NULL OR STJ9_TAXCODE_TYPE_CODE='E') " +
						"AND (STJ10_TAXCODE_TYPE_CODE IS NULL OR STJ10_TAXCODE_TYPE_CODE='E'))";
			}
		} else if (status.equalsIgnoreCase("PC")) {	// Processed - Closed Transactions   // MBF:PP-234
			trDetail.setTransactionInd("P");
			trDetail.setGlExtractFlag("1");
		} else if (status.equalsIgnoreCase("PR")) {	// Processed - Original Transactions   // MBF:PP-234
			if(advancedFilter == null || advancedFilter.length() == 0) {
				advancedFilter = "(MULTI_TRANS_CODE LIKE 'O%')";
			}
			else {
				advancedFilter += " AND (MULTI_TRANS_CODE LIKE 'O%')";
			}
		} else if (status.equalsIgnoreCase("U")) {	// All Unprocessed Transactions
			includeUnprocessed = true;
		} else if (status.equalsIgnoreCase("US")) {	// Unprocessed - Suspended Transactions
			trDetail.setTransactionInd("S");
		} else if(status.equalsIgnoreCase("UST")) {	// Unprocessed - Suspended - Tax Matrix Transactions
			trDetail.setTransactionInd("S");
			trDetail.setSuspendInd("T");
		} else if(status.equalsIgnoreCase("USL")) {	// Unprocessed - Suspended - Location Matrix Transactions
			trDetail.setTransactionInd("S");
			trDetail.setSuspendInd("L");
		} else if(status.equalsIgnoreCase("USD")) {	// Unprocessed - Suspended - No TaxCode Rule
			trDetail.setTransactionInd("S");
			trDetail.setSuspendInd("D");
		} else if(status.equalsIgnoreCase("USR")) {	// Unprocessed - Suspended - Tax Rate Transactions
			trDetail.setTransactionInd("S");
			trDetail.setSuspendInd("R");
		} else if(status.equalsIgnoreCase("UH")) {	// Unprocessed - Held Transactions
			trDetail.setTransactionInd("H");
		} else if(status.equalsIgnoreCase("UHN")) {	// Unprocessed - Held Normal Transactions
			trDetail.setTransactionInd("H");
			if(advancedFilter == null || advancedFilter.length() == 0) {
				advancedFilter = "(MULTI_TRANS_CODE is null OR MULTI_TRANS_CODE = '')";
			}
			else {
				advancedFilter += " AND (MULTI_TRANS_CODE is null OR MULTI_TRANS_CODE = '')";
			}
		} else if(status.equalsIgnoreCase("FS")) {	// Unprocessed - Future Split Transactions
			trDetail.setTransactionInd("H");
			trDetail.setMultiTransCode("FS");
		}



		   if(selectedTaxabilityCode!=null && !selectedTaxabilityCode.equals("") && !getTaxableTypeDisabled()){
        	if(advancedFilter == null || advancedFilter.length() == 0) {
        		advancedFilter = " (STATE_TAXCODE_TYPE_CODE ='"+selectedTaxabilityCode+"' OR COUNTY_TAXCODE_TYPE_CODE ='"+selectedTaxabilityCode+"' OR CITY_TAXCODE_TYPE_CODE ='"+selectedTaxabilityCode+"' OR "
						+ "COUNTRY_TAXCODE_TYPE_CODE = '"+selectedTaxabilityCode+"' OR STJ1_TAXCODE_TYPE_CODE = '"+selectedTaxabilityCode+"' OR  STJ2_TAXCODE_TYPE_CODE = '"+selectedTaxabilityCode+"' OR STJ3_TAXCODE_TYPE_CODE = '"+selectedTaxabilityCode+"'"
								+ " OR STJ4_TAXCODE_TYPE_CODE = '"+selectedTaxabilityCode+"' OR STJ5_TAXCODE_TYPE_CODE = '"+selectedTaxabilityCode+"' OR STJ6_TAXCODE_TYPE_CODE = '"+selectedTaxabilityCode+"' OR STJ7_TAXCODE_TYPE_CODE = '"+selectedTaxabilityCode+"'"
										+ " OR STJ8_TAXCODE_TYPE_CODE = '"+selectedTaxabilityCode+"' OR STJ9_TAXCODE_TYPE_CODE = '"+selectedTaxabilityCode+"' OR STJ10_TAXCODE_TYPE_CODE = '"+selectedTaxabilityCode+"' )";
			}
			else {
				advancedFilter += " AND (STATE_TAXCODE_TYPE_CODE ='"+selectedTaxabilityCode+"' OR COUNTY_TAXCODE_TYPE_CODE ='"+selectedTaxabilityCode+"' OR CITY_TAXCODE_TYPE_CODE ='"+selectedTaxabilityCode+"' OR "
						+ "COUNTRY_TAXCODE_TYPE_CODE = '"+selectedTaxabilityCode+"' OR STJ1_TAXCODE_TYPE_CODE = '"+selectedTaxabilityCode+"' OR  STJ2_TAXCODE_TYPE_CODE = '"+selectedTaxabilityCode+"' OR STJ3_TAXCODE_TYPE_CODE = '"+selectedTaxabilityCode+"'"
								+ " OR STJ4_TAXCODE_TYPE_CODE = '"+selectedTaxabilityCode+"' OR STJ5_TAXCODE_TYPE_CODE = '"+selectedTaxabilityCode+"' OR STJ6_TAXCODE_TYPE_CODE = '"+selectedTaxabilityCode+"' OR STJ7_TAXCODE_TYPE_CODE = '"+selectedTaxabilityCode+"'"
										+ " OR STJ8_TAXCODE_TYPE_CODE = '"+selectedTaxabilityCode+"' OR STJ9_TAXCODE_TYPE_CODE = '"+selectedTaxabilityCode+"' OR STJ10_TAXCODE_TYPE_CODE = '"+selectedTaxabilityCode+"' )";
			}
        }
		   transactionDataModel.setFilterCriteriaAdvanced(trDetail, fromGLDate,
				toGLDate, includeUnprocessed, false, null, glExtractFlagIsNull,
				advancedFilter);		
		resetSelection();
		setDispSaveAs((transactionDataModel.getRowCount() > 0));
		
		//Reset if number of pages changes.
		resetDetailTable();
		
		//Reset sub total
		selectionTotals = new HashMap<String,Double>();
		selectedIds = null;
		multiTypeSelected = "";
		
		return null;
    }	

	public String advancedFilterAction() {
		advancedFilterBean.setFromScreenName(ADVANCE_FILTER_TRANSACTION_SCREEN); 
		advancedFilterBean.initAdvancedFilter();
		return "advanced_filter";
	}

	public String advancedSortAction() {
		advancedSortBean.initAdvancedSort(getTransactionSortMap(),getSortOrder());
		advancedSortBean.setCallback(this);
		return "advanced_sort";
	}
	
	public boolean getIsAdvancedFilterEnabled() {
		return advancedFilterBean.isAdvancedFilterEnabled();
	}
	
	
	public void selectedTransactionChanged (ActionEvent e) throws AbortProcessingException {
		UIComponent uiComponent = e.getComponent().getParent();
		UIDataAdaptor table = (UIDataAdaptor) uiComponent;
		selectedTransaction = (PurchaseTransaction) table.getRowData();
	    selectedRowIndex = table.getRowIndex();
	    selectedTransactionTemp = selectedTransaction;
	    selectedIds = null;
	    getSelectedIds();
	    currentUser = userService.findById(matrixCommonBean.getLoginBean().getUserDTO().getUserCode().toUpperCase());
	}
	
	public void selectedTransactionLogChanged(ActionEvent e) throws AbortProcessingException {
		HtmlDataTable table = (HtmlDataTable) ((HtmlAjaxSupport) e.getComponent()).getParent();
		this.selectedTransactionLog = (PurchaseTransactionLog) table.getRowData();
		this.selectedTransactionLogIndex = table.getRowIndex();
		
	}
	
	public String displayTransactionLogViewAction() {
		transactionViewBean.setOkAction("trans_history");
		transactionViewBean.setSelectedTransactionLog(selectedTransactionLog);
		return "trans_log_entry_view";
	}
	
	public void selectedSplitTransactionChanged (ActionEvent e) throws AbortProcessingException {
		UIComponent uiComponent = e.getComponent().getParent();
		UIDataAdaptor table = (UIDataAdaptor) uiComponent;
		selectedSplitTransaction = (PurchaseTransaction) table.getRowData();
	    selectedSplitRowIndex = table.getRowIndex();
	    
	    selectedSplitTransactionTemp = selectedSplitTransaction;
	}
	
	public void multiSelectionChanged(ActionEvent e) {
	    updateFieldSum();
	}
	
	private void resetSelection() {
		currentTransactionList = null;
		currentTransactionIndex = 0;
		selectedTransaction = null;
		selectedTransactionTemp = null;
		selectedRowIndex = -1;
	}
	
	public String doubleClickAction() {
		return displayProcessAction();
	}
	
	public String getComments() {
		return comments;
	}
	
	public void setComments(String comments) {
		this.comments = comments;
	}
	
	public boolean isSuspendTax() {
		return suspendTax;
	}
	
	public void setSuspendTax(boolean suspendTax) {
		this.suspendTax = suspendTax;
	}
	
	public boolean isSuspendLocation() {
		return suspendLocation;
	}
	
	public void setSuspendLocation(boolean suspendLocation) {
		this.suspendLocation = suspendLocation;
	}
	
	public boolean isSuspendTaxcode() {
		return suspendTaxcode;
	}
	
	public void setSuspendTaxcode(boolean suspendTaxcode) {
		this.suspendTaxcode = suspendTaxcode;
	}
	
	public boolean isSuspendTaxrate() {
		return suspendTaxrate;
	}
	
	public void setSuspendTaxrate(boolean suspendTaxrate) {
		this.suspendTaxrate = suspendTaxrate;
	}

	public TransactionDataModel getTransactionDataModel() {
		return transactionDataModel;
	}

	public void setTransactionDataModel(TransactionDataModel transactionDataModel) {
		this.transactionDataModel = transactionDataModel;
		
		String rowStr = (displayAllFields)? matrixCommonBean.getMatrixDTO().getSysFullThreshold() : 
			matrixCommonBean.getMatrixDTO().getSysLimitedThreshold();
		int rowpage = 0;
		if(rowStr==null || rowStr.length()==0){
			rowpage = (displayAllFields)? 25:100; 
		}
		else{
			rowpage = Integer.parseInt(rowStr);
		}
		
		String muitipleStr = (displayAllFields)? matrixCommonBean.getMatrixDTO().getSysFullThresholdPages() : 
			matrixCommonBean.getMatrixDTO().getSysLimitedThresholdPages();
		int muitiplepage = 0;
		if(muitipleStr==null || muitipleStr.length()==0){
			muitiplepage = (displayAllFields)? 4:4; 
		}
		else{
			muitiplepage = Integer.parseInt(muitipleStr);
		}
		
		transactionDataModel.setPageSize(rowpage);
		transactionDataModel.setDbFetchMultiple(muitiplepage);
	}
    
	public void sortAction(ActionEvent e) {
		transactionDataModel.toggleSortOrder(e.getComponent().getId().replace("s_trans_", ""));
	}
	
	public String findJurisdictionIdAction() {
		Jurisdiction jurisdictionFilter = new Jurisdiction();
		jurisdictionFilter.setJurisdictionId(jurisdictionId);
		jurisdictionFilter.setCountry(this.selectedCountry);
		jurisdictionFilter.setState(this.selectedState);
		taxJurisdictionBean.findId(this, "jurisdictionId", "trans_main", jurisdictionFilter);
		taxJurisdictionBean.setSearchCountry(this.selectedCountry);
		taxJurisdictionBean.setSearchstate(this.selectedState);
		return "maintanence_tax_rates";
	}
	
	public String findProcessBatchIdAction() {
		String batchType = "IP";
		
		Option isPCO = optionService.findByPK(new OptionCodePK("PCO", "ADMIN", "ADMIN"));
		if (isPCO!=null && isPCO.getValue()!=null && "1".equals(isPCO.getValue())) {
			batchType = "PCO";
		}
		
		batchMaintenanceBean.findId(this, "processBatchId", "trans_main", batchType);
		return "data_utility_batch";
	}
	
	public String findGlExtractBatchIdAction() {
		batchMaintenanceBean.findId(this, "glExtractId", "trans_main", "GE");
		return "data_utility_batch";
	}
	
	public String findTaxMatrixIdAction() {
		taxMatrixBean.findId(this, "taxMatrixId", "trans_main");
		return "tax_matrix_main";
	}
	
	public String findLocationMatrixIdAction() {
		LocationMatrix locationMatrix=new LocationMatrix();
		Jurisdiction jurisdictionFilter = new Jurisdiction();
		jurisdictionFilter.setCountry(this.selectedCountry);
		jurisdictionFilter.setState(this.selectedState);
		locationMatrix.setJurisdiction(jurisdictionFilter);
		locationMatrixBean.findId(this, "locationMatrixId", "trans_main");
		locationMatrixBean.setFilterCriteria(locationMatrix);
		locationMatrixBean.getFilterHandler().setCountry(this.selectedCountry);
		locationMatrixBean.getFilterHandler().setState(this.selectedState);
		return "location_matrix_main";
	}
	
	public void findIdCallback(Long id, String context) {
		if (context.equalsIgnoreCase("taxMatrixId")) {
			taxMatrixId = id;
		} else if (context.equalsIgnoreCase("locationMatrixId")) {
			locationMatrixId = id;
		} else if (context.equalsIgnoreCase("processBatchId")) {
			processBatchId = id;
		} else if (context.equalsIgnoreCase("glExtractId")) {
			glExtractId = id;
		} else if (context.equalsIgnoreCase("jurisdictionId")) {
			jurisdictionId = id;
		
		} else if (context.equalsIgnoreCase("compareToMatrixId")) {
			compareToMatrixId = id;
		}
	}
	
	public void searchLocationMatrixAction() {
		locationMatrixBean.search(this, "search");
	}
	
	public void searchTaxMatrixAction() {
		taxMatrixBean.search(this, "search");
	}
	
	public void searchTaxCodeAction() {
		taxCodeBackingBean.search(this, "search");
	}
	
	public void searchCallback(String context) {
		if (context.equalsIgnoreCase("search")) {
			listTransactions();
		}
	}
	
	public void sortCallback(String context) {
		if (context.equalsIgnoreCase("sort")) {
			// Refresh data, but we can keep the count
			transactionDataModel.refreshData(true);
		}
    }
	
	public void batchIdSelectedListener(ActionEvent e) {
		processBatchId = batchIdHandler.getSelectedItemKey();
	}
	
	public void glExtractIdSelectedListener(ActionEvent e) {
		glExtractId = batchIdHandler.getSelectedItemKey();
	}

	public void minimumTaxDriversActionListener(ActionEvent e) {
		matrixCommonBean.prepareTaxDriverPanel(filterSelectionPanel);
	}
	
	public void defaultImportMapProcessListener(ActionEvent e){
		batchIdHandler.setDefaultSearch("IP");
		batchIdHandler.reset();
	}
	
	public void defaultGLExtractListener(ActionEvent e){
		batchIdHandler.setDefaultSearch("GE");
		batchIdHandler.reset();
	}
	
	public void popupSearchAction(ActionEvent e) {
		// pass event to handler
		jurisdictionHandler.reset();
		jurisdictionHandler.setCallback(this);
		jurisdictionHandler.popupSearchAction(e);
	}
	
	public void searchJurisdictionCallback() {
		Long id = jurisdictionHandler.getSearchJurisdictionResultId();
		jurisdictionId = id;
	}
	
	public long getTotalSelected() {
		return totalSelected;
	}

	public void setTotalSelected(long totalSelected) {
		this.totalSelected = totalSelected;
	}

	public PurchaseTransaction getTransactionDetail() {
		return transactionDetail;
	}

	public void setTransactionDetail(PurchaseTransaction transactionDetail) {
		this.transactionDetail = transactionDetail;
	}

	public List<PurchaseTransaction> getTraList() {
		return traList;
	}

	public void setTraList(List<PurchaseTransaction> traList) {
		this.traList = traList;
	}

	public boolean isDispSaveAs() {
		return dispSaveAs;
	}

	public void setDispSaveAs(boolean dispSaveAs) {
		this.dispSaveAs = dispSaveAs;
	}

	public OptionService getOptionService() {
		return optionService;
	}

	public void setOptionService(OptionService optionService) {
		this.optionService = optionService;
	}

	public Boolean getRenderOptionPanel() {
		return renderOptionPanel;
	}

	public void setRenderOptionPanel(Boolean renderOptionPanel) {
		this.renderOptionPanel = renderOptionPanel;
	}

	public boolean isSaveNow() {
		return saveNow;
	}

	public void setSaveNow(boolean saveNow) {
		this.saveNow = saveNow;
	}

	public boolean isShowOkButton() {
		return showOkButton;
	}

	public void setShowOkButton(boolean showOkButton) {
		this.showOkButton = showOkButton;
	}
	
	public void searchCountryChanged(ValueChangeEvent e){
		String newCountry = (String)e.getNewValue();
		if(newCountry == null) {
			selectedCountry = "*ALL";
		}
		else {
			selectedCountry = newCountry;
		}
	
		if(stateMenuItems!=null) stateMenuItems.clear();

		stateMenuItems = getMatrixCommonBean().getCacheManager().createStateItems(selectedCountry);
		selectedState = "*ALL";
    }
	
	public void searchStateChanged(ValueChangeEvent e){
		String newState = (String)e.getNewValue();
		if(newState == null) {
			selectedState = "*ALL";
		}
		else {
			selectedState = newState;
			transState = newState;
		}
    }
	
	public void viewStateChanged(ValueChangeEvent e){
		String newState = (String)e.getNewValue();
		if(newState == null) {
			selectedState = "";
		}
		else {
			selectedState = transState;
		}
    }

	private List<SelectItem> countryMenuItems;
	public List<SelectItem> getCountryMenuItems() {
		if(countryMenuItems==null){
			selectedCountry = "";
		}
		countryMenuItems = getMatrixCommonBean().getCacheManager().createCountryItems();   
		return countryMenuItems;
	}

	private List<SelectItem> stateMenuItems;
	public List<SelectItem> getStateMenuItems() {
		if (stateMenuItems == null) {
		   stateMenuItems = getMatrixCommonBean().getCacheManager().createStateItems(selectedCountry);
		   selectedState = "";
		}
		return stateMenuItems;
	}
	
	public List<SelectItem> getEditTransStateMenuItems(String countryCode) {
		String country = countryCode;
		   stateMenuItems = getMatrixCommonBean().getCacheManager().createStateItems(country);
		   selectedState = "";
		return stateMenuItems;
	}
	
	public List<SelectItem> getStateMenuItemsModifyOriginal() { 
		return getMatrixCommonBean().getCacheManager().createStateItems(getCurrentTransaction().getTransactionCountryCode());
	}
	
	public List<SelectItem> getStateJurisMenuItemsModifyOriginal() { 
		return getMatrixCommonBean().getCacheManager().createStateItems((getCurrentJurisdiction()==null) ? "" : getCurrentJurisdiction().getCountry());
	}

	public String splitAction() {
		this.transactionSplittingToolBean.setTransactionDetail(this.selectedTransaction);
		this.transactionSplittingToolBean.setTransactionDetailBean(this);

		if(this.currentSplittingToolBean == null) {
			this.currentSplittingToolBean = new ArrayList<TransactionSplittingToolBean<?>>();
		}
		else {
			this.currentSplittingToolBean.clear();
		}
		this.currentSplittingToolBean.add(this.transactionSplittingToolBean);
		return "split_transactions";
	}

	public List<TransactionSplittingToolBean<?>> getCurrentSplittingToolBean() {
		return currentSplittingToolBean;
	}

	public void setCurrentSplittingToolBean(
			List<TransactionSplittingToolBean<?>> currentSplittingToolBean) {
		this.currentSplittingToolBean = currentSplittingToolBean;
	}

	public TransactionSplittingToolBean<PurchaseTransaction> getTransactionSplittingToolBean() {
		return transactionSplittingToolBean;
	}

	public void setTransactionSplittingToolBean(
			TransactionSplittingToolBean<PurchaseTransaction> transactionSplittingToolBean) {
		this.transactionSplittingToolBean = transactionSplittingToolBean;
	}

	public String getMultiTransCode() {
		return multiTransCode;
	}

	public void setMultiTransCode(String multiTransCode) {
		this.multiTransCode = multiTransCode;
	}

	public Long getSubTransId() {
		return subTransId;
	}

	public void setSubTransId(Long subTransId) {
		this.subTransId = subTransId;
	}

	public TransactionDataModel getSplitGroupDataModel() {
		return splitGroupDataModel;
	}

	public void setSplitGroupDataModel(TransactionDataModel splitGroupDataModel) {
		this.splitGroupDataModel = splitGroupDataModel;
	}

	public HtmlDataTable getTrDetailSplitTable() {
		if (trDetailSplitTable == null) {
			trDetailSplitTable = new HtmlDataTable();
			trDetailSplitTable.setId("aMDetailList");
			trDetailSplitTable.setVar("trdetail");
			matrixCommonBean.createTransactionDetailTable(trDetailSplitTable,
					"transactionDetailBean", "transactionDetailBean.splitGroupDataModel", 
					matrixCommonBean.getTransactionPropertyMap(), false, displayAllViewSplitFields, false, false, true, null);
		}
		return trDetailSplitTable;
	}

	public void setTrDetailSplitTable(HtmlDataTable trDetailSplitTable) {
		this.trDetailSplitTable = trDetailSplitTable;
	}

	public int getSelectedSplitRowIndex() {
		return selectedSplitRowIndex;
	}

	public void setSelectedSplitRowIndex(int selectedSplitRowIndex) {
		this.selectedSplitRowIndex = selectedSplitRowIndex;
	}

	public boolean isDisplayAllViewSplitFields() {
		return displayAllViewSplitFields;
	}

	public void setDisplayAllViewSplitFields(boolean displayAllViewSplitFields) {
		this.displayAllViewSplitFields = displayAllViewSplitFields;
	}

	public String getSelectedCountry() {
		return selectedCountry;
	}

	public void setSelectedCountry(String selectedCountry) {
		this.selectedCountry = selectedCountry;
	}

	public String getSelectedState() {
		return selectedState;
	}

	public void setSelectedState(String selectedState) {
		this.selectedState = selectedState;
	}
	
	public String getFilterFormState() {
		return filterFormState;
	}

	public void setFilterFormState(String filterFormState) {
		this.filterFormState = filterFormState;
	}
	
	public void filterFormStateChanged(ValueChangeEvent e) {
		String v = (String) e.getNewValue();
		this.filterFormState = v;
	}

	public HtmlPanelGrid getFilterTaxDriverSelectionPanel() {

		filterTaxDriverSelectionPanel = MatrixCommonBean.createDriverPanel(
					TaxMatrix.DRIVER_CODE, matrixCommonBean.getCacheManager(), 
					matrixCommonBean.getUserEntityService().getEntityItemDTO(),
					"filterPanel", 
					"transactionDetailBean", "filterSelectionTaxMatrix", "matrixCommonBean", 
					false, false, false, true, true); 
				matrixCommonBean.prepareTaxDriverPanel(filterTaxDriverSelectionPanel);
		return filterTaxDriverSelectionPanel;
	}

	public void setFilterTaxDriverSelectionPanel(
			HtmlPanelGrid filterTaxDriverSelectionPanel) {
		this.filterTaxDriverSelectionPanel = filterTaxDriverSelectionPanel;
	}

	public HtmlPanelGrid getFilterLocDriverSelectionPanel() {
		filterLocDriverSelectionPanel = MatrixCommonBean.createDriverPanel(
				LocationMatrix.DRIVER_CODE, matrixCommonBean.getCacheManager(), 
					matrixCommonBean.getUserEntityService().getEntityItemDTO(),
					"filterPanel", 
					"transactionDetailBean", "filterSelectionLocationMatrix", "matrixCommonBean", 
					false, false, false, true, true); 
		matrixCommonBean.prepareTaxDriverPanel(filterLocDriverSelectionPanel);
		return filterLocDriverSelectionPanel;
	}

	public void setFilterLocDriverSelectionPanel(
			HtmlPanelGrid filterLocDriverSelectionPanel) {
		this.filterLocDriverSelectionPanel = filterLocDriverSelectionPanel;
	}

	public TogglePanelController getTogglePanelController() {
		if (togglePanelController == null) {
			togglePanelController = new TogglePanelController();
			togglePanelController.addTogglePanel("miscInformation", true);
			togglePanelController.addTogglePanel("taxDriversInformation", false);
			togglePanelController.addTogglePanel("locationDriversInformation", false);
		}
		return togglePanelController;
	}

	public void setTogglePanelController(TogglePanelController togglePanelController) {
		this.togglePanelController = togglePanelController;
	}

	public String getSelectedTaxabilityCode() {
		return selectedTaxabilityCode;
	}

	public void setSelectedTaxabilityCode(String selectedTaxabilityCode) {
		this.selectedTaxabilityCode = selectedTaxabilityCode;
	}

	public List<SelectItem> getTaxabilityCodeMenuItems() {
		if(taxabilityCodeMenuItems==null){
			selectedTaxabilityCode = "";
			taxabilityCodeMenuItems = getMatrixCommonBean().getCacheManager().createTaxabilityCodeItems();
		}
		return taxabilityCodeMenuItems;
	}

	public void setTaxabilityCodeMenuItems(List<SelectItem> taxabilityCodeMenuItems) {
		this.taxabilityCodeMenuItems = taxabilityCodeMenuItems;
	}
	
	public boolean getTaxableTypeDisabled() {
		return (!(transactionStatus.equalsIgnoreCase("*ALL") || transactionStatus.equalsIgnoreCase("P")|| transactionStatus.equalsIgnoreCase("PO")
				|| transactionStatus.equalsIgnoreCase("PC") || transactionStatus.equalsIgnoreCase("UH") || transactionStatus.equalsIgnoreCase("UHN") || transactionStatus.equalsIgnoreCase("FS")));
	}
	public String displayBatchView() {
		String fromview = (String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("fromview");
		String batchId = (String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("viewBatchNo");
		String nextPage =  batchMaintenanceBean.displayBatchView(Long.valueOf(batchId),fromview);
		if(nextPage == null) {
			transactionViewBean.displayErrorMessage("BatchID not found"); 
		}
		return nextPage;
		
	}
	
	public String viewJurisdiction() {
		String fromview = (String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("command");
		String JurisdId = (String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("transLogJurisdId");
		return taxJurisdictionBean.viewJurisdiction(Long.valueOf(JurisdId),fromview);
    }
	
	public String displayViewSplitAction() {
		String fromview = (String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("fromview");
		String splitSubTransId = (String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("taxAllocSubtransID");
		String errormessage = (String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("message");
		String nextPage =  displayViewSplitAction(Long.valueOf(splitSubTransId),fromview);
		if(nextPage == null){
    		transactionViewBean.displayErrorMessage(errormessage); 
    		return null;
    	}
    	return nextPage;
		
	}
		
	public String displayViewSplitAction(Long splitSubTransId,String fromview) {
		if(fromview.equals("viewFromTransaction")){
			returnTransUrl = "trans_view";
		}
		else {
			returnTransUrl = "trans_log_entry_view";
		}
		setReturnTransUrl(returnTransUrl);
		selectedSplitRowIndex = -1;
		selectedSplitTransaction = null;
		selectedSplitTransactionTemp = null;
		PurchaseTransaction purchTrans = new PurchaseTransaction();
		if(splitSubTransId != null && splitSubTransId > 0L) {
			purchTrans.setSplitSubtransId(splitSubTransId);
		}
		else {
			purchTrans.setPurchtransId(this.selectedTransaction.getPurchtransId());
		}
		splitGroupDataModel.setFilterCriteria(purchTrans, null, null, null, null, null, null);
		showSplitGroupViewSplitButton = false;
		return "view_split_set";
	}
	
	public String closeSplitSubTransIdAction() {
		if("trans_view".equals(getReturnTransUrl())){
			return "trans_view";
		}
		else if("trans_log_entry_view".equals(getReturnTransUrl())){
			return "trans_log_entry_view";
		}
		
		else {
			return "transactions_process";
		}
		
	}
	public String jurisdictionHistory(String City,String county,String stateCode,String countryCode) { 
		StringBuffer jurStrBuffer = new StringBuffer();
		if(City !=null && StringUtils.isNotEmpty(City)){
			jurStrBuffer.append(City).append(", ");
		}
		if(county !=null && StringUtils.isNotEmpty(county)){
			jurStrBuffer.append(county).append(", ");
		}
		if(stateCode !=null && StringUtils.isNotEmpty(stateCode)){
			jurStrBuffer.append(stateCode).append(", ");;
		}
		if(countryCode !=null && StringUtils.isNotEmpty(countryCode)){
			jurStrBuffer.append(countryCode).append(", ");
		}
		
		String jurisdiction = jurStrBuffer.toString();
		if(jurisdiction.endsWith(", ")){
			return jurisdiction.substring(0, jurisdiction.length()-2);
		}
		return jurStrBuffer.toString();
	}
	
	public String okAction() {
		return "trans_history";
	}
	
	public Jurisdiction getJurisdictionDetails(Long jurisdictionId){
		currentJurisdiction = jurisdictionService.findById(jurisdictionId);
		return currentJurisdiction;
	}
	
	public String getLogSource(String logSource) {
		if("sp_batch_process".equalsIgnoreCase(logSource)) {
			return "Batch Process";
		}
		else if("sp_tb_jurisdiction_taxrate_a_i".equalsIgnoreCase(logSource)) {
			return "Tax Rate added from Transaction";
		}
		else if("sp_tb_location_matrix_a_i".equalsIgnoreCase(logSource)) {
			return "Location Matrix added from Transaction";
		}
		else if("sp_tb_tax_matrix_a_i".equalsIgnoreCase(logSource)) {
			return "Goods/Services Matrix added from Transaction";
		}
		else if("sp_tb_taxcode_detail_a_i".equalsIgnoreCase(logSource)){
			return "TaxCode Rule added from Transaction";
		}
		else if("sp_tb_transaction_detail_b_u".equalsIgnoreCase(logSource)){
			return "Updated";
		}
		else {
			return logSource;
		}
	}
	
	public String getStatus(String transDesc,String suspendDesc) {
		
		if(suspendDesc != null && !suspendDesc.isEmpty())
			return transDesc + " for " + suspendDesc;
		else
			return transDesc;
		
	}
	
	public String getActionText() {
		return currentAction.getActionText();
	}
	
	public boolean getIsViewScreen() {
		return ("View".equals(getActionText()) || "ViewSplit".equals(getActionText()) || "ViewFromStats".equals(getActionText()) );
	}
	
	public String cancelAction() {
		transactionViewBean.setSelectedDisplay("taxDriver");
		trDetailTable = null;
		if("ViewSplit".equals(getActionText())) {
			return "view_split_set";
		}
		
		if("ViewFromStats".equals(getActionText())) {
			return "viewDbTrStatisticsAction";
		}
		return "trans_main";
	}
	
	public String findTaxCodeFilterAction() {
		taxCodeBackingBean.findCode(this, "TaxCodeFilter", "trans_main", filterTaxCodeHandler.getTaxcodeCode());	
		return "taxability_codes";
	}
	
	public String findTaxCodeApplyAction() {
		taxCodeBackingBean.findCode(this, "TaxCodeApply", "apply_taxcode", newTaxCodeHandler.getTaxcodeCode());	
		return "taxability_codes";
	}
	
	public void findCodeCallback(String code, String context) {
		if(context.equalsIgnoreCase("TaxCodeFilter")) {
			filterTaxCodeHandler.setTaxcodeCode(code);
		}
		else if(context.equalsIgnoreCase("TaxCodeApply")) {
			newTaxCodeHandler.setTaxcodeCode(code);
		}
	}
	public boolean getApplyMatrixDisabled() {
		for (PurchaseTransaction purchtrans : transactionDataModel.getItems(getSelectedIds())) {
			String status = purchtrans.getTransactionInd();
			if ((status == null) || !status.equalsIgnoreCase("S")) {
				return true;
			}
			
			String suspend = purchtrans.getSuspendInd();
			if ((suspend == null) || (!suspend.equalsIgnoreCase("T") && !suspend.equalsIgnoreCase("L"))) {
				return true;
			}
		}

		return (getSelectionCount() == 0);
	}
	
	public User getCurrentUser() {
		return currentUser;
	}

	public void checkSelectedVal(ValueChangeEvent event) {
		setSuspendFlag(true);
	}
	
	public String displayComparisonTransViewAction() {
		transactionViewBean.setSelectedTransaction(getCurrentTransaction());
		transactionViewBean.setOkAction("matrix_analysis");
		return "trans_view";
	}
	
	public String findComparedMatrixIdAction() {
		if(this.compareToMatrixType!=null && compareToMatrixType.contentEquals("T")){
			taxMatrixBean.findId(this, "compareToMatrixId", "matrix_analysis");
			return "tax_matrix_main";		
		}
		else if(this.compareToMatrixType!=null && compareToMatrixType.contentEquals("L")){	
			locationMatrixBean.findId(this, "compareToMatrixId", "matrix_analysis");
			return "location_matrix_main";
		}

		return null;
	}
	
	public String displayViewTaxMatrixAction(){	
		//View Tax or Location matrix
		if(selectedTransaction!=null && this.selectedTransaction.getTaxMatrixId()!=null){
			String nextPage = taxMatrixBean.viewFromMatrixComparison(selectedTransaction.getTaxMatrixId(), "tax_matrix_view");
			if(nextPage == null) {
				FacesMessage message = new FacesMessage("Goods & Services Matrix ID not found");
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage(null, message);
				return null;
			}
			else {
				return nextPage;
			}
		}
		else{
			return null;
		}
	}
	
	public String displayViewLocationMatrixAction(){	
		//View Tax or Location matrix
		if(selectedTransaction!=null && this.selectedTransaction.getShiptoLocnMatrixId()!=null){	
			//TransactionDetail trdetail = getCurrentTransaction();
			
			String nextPage = locationMatrixBean.viewFromMatrixComparison(this.selectedTransaction.getShiptoLocnMatrixId(), "loc_matrix_view");
			if(nextPage == null) {
				FacesMessage message = new FacesMessage("Location Matrix ID not found");
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage(null, message);
				return null;
			}
			else {
				return nextPage;
			}
		}
		else{
			return null;
		}
	}
	
	public String displayComparedViewAction(){	
		//View Tax or Location matrix
		if(compareToMatrixId!=null && compareToMatrixId.longValue()>0L && this.compareToMatrixType!=null && compareToMatrixType.contentEquals("T")){
			//TransactionDetail trdetail = getCurrentTransaction();
			
			String nextPage = taxMatrixBean.viewFromMatrixComparison(compareToMatrixId, "tax_matrix_view");
			if(nextPage == null) {
				FacesMessage message = new FacesMessage("Goods & Services Matrix ID not found.");
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage(null, message);
				return null;
			}
			else {
				return nextPage;
			}
		}
		else if(compareToMatrixId!=null && compareToMatrixId.longValue()>0L && this.compareToMatrixType!=null && compareToMatrixType.contentEquals("L")){	
			//TransactionDetail trdetail = getCurrentTransaction();
			
			String nextPage = locationMatrixBean.viewFromMatrixComparison(compareToMatrixId, "loc_matrix_view");
			if(nextPage == null) {
				FacesMessage message = new FacesMessage("Location Matrix ID not found.");
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage(null, message);
				return null;
			}
			else {
				return nextPage;
			}
		}
		else{
			FacesMessage message = new FacesMessage("Please enter valid Matrix ID.");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
			return null;
		}
	}
	
	public String displayComparedMatrixViewAction(){	
		//View Tax or Location matrix
		if(selectedCompareTaxMatrix!=null && selectedCompareTaxMatrixIndex >-1){
			String nextPage = taxMatrixBean.viewFromMatrixComparison(selectedCompareTaxMatrix.getTaxMatrixId(), "tax_matrix_view");
			if(nextPage == null) {
				FacesMessage message = new FacesMessage("Goods & Services Matrix ID not found");
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage(null, message);
				return null;
			}
			else {
				return nextPage;
			}
		}
		else if(selectedCompareLocationMatrix!=null && selectedCompareLocationMatrixIndex>-1){	
			String nextPage = locationMatrixBean.viewFromMatrixComparison(selectedCompareLocationMatrix.getLocationMatrixId(), "loc_matrix_view");
			if(nextPage == null) {
				FacesMessage message = new FacesMessage("Location Matrix ID not found");
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage(null, message);
				return null;
			}
			else {
				return nextPage;
			}
		}
		else{
			return null;
		}
	}
	
	public String displayComparedMatrixCompareAction(){	
		if(selectedCompareTaxMatrix!=null && selectedCompareTaxMatrixIndex >-1){
			compareToMatrixId = selectedCompareTaxMatrix.getTaxMatrixId();	
		}
		else if(selectedCompareLocationMatrix!=null && selectedCompareLocationMatrixIndex>-1){	
			compareToMatrixId = selectedCompareLocationMatrix.getLocationMatrixId();
		}

		matchingLocationMatrixList = null;
		matchingTaxMatrixList = null;
		
		selectedCompareTaxMatrixIndex = -1;
		selectedCompareLocationMatrixIndex = -1;
		
		displayTransMatrixAction();
		
		return "matrix_analysis";
	}
	
	public boolean isValidComparedMatrixSelection() {
		if(selectedCompareTaxMatrix!=null && selectedCompareTaxMatrixIndex >-1){
			return true;
		}
		else if(selectedCompareLocationMatrix!=null && selectedCompareLocationMatrixIndex>-1){
			return true;
		}
		else{
			return false;
		}
	}
	
	public boolean getMatrixAnalysis() {
		return this.selectedTransaction != null && 
		(this.selectedTransaction.getTaxMatrixId() != null  || this.selectedTransaction.getShiptoLocnMatrixId() !=null);
	}
	
	public String displayMatrixAnalysisAction() {
		transactionMatrixList = null;
		PurchaseTransaction trdetail = getCurrentTransaction();
		if(trdetail.getTaxMatrix() != null && trdetail.getShiptoLocnMatrixId() != null) {
			compareToMatrixType = "T"; //default If both are present
			compareToMatrixId = trdetail.getTaxMatrixId();
		}
		if(trdetail.getTaxMatrix() != null && trdetail.getShiptoLocnMatrixId() == null) {
			compareToMatrixType = "T"; //default If Tax Matrix Id only present
			compareToMatrixId = trdetail.getTaxMatrixId();
		}
		if(trdetail.getTaxMatrix() == null && trdetail.getShiptoLocnMatrixId() != null) {
			compareToMatrixType = "L"; //default If Location Matrix Id present
			compareToMatrixId = trdetail.getShiptoLocnMatrixId();
		}
		
		matchingLocationMatrixList = null;
		matchingTaxMatrixList = null;
		
		selectedCompareTaxMatrixIndex = -1;
		selectedCompareLocationMatrixIndex = -1;
		
		displayTransMatrixAction();
		
		return "matrix_analysis";
	}
	
	public boolean getIsTaxMatrixSelected() {
    	return (TaxMatrix.DRIVER_CODE.equals(compareToMatrixType));
    }
	
	public boolean getIsLocationMatrixSelected() {
    	return (LocationMatrix.DRIVER_CODE.equals(compareToMatrixType));
    }
	
	public int getSelectedCompareTaxMatrixIndex() {
		return selectedCompareTaxMatrixIndex;
	}
	
	public int getSelectedCompareLocationMatrixIndex() {
		return selectedCompareLocationMatrixIndex;
	}
	
	public void selectedTaxMatrixChanged(ActionEvent e) throws AbortProcessingException { 
		HtmlDataTable table = (HtmlDataTable) ((HtmlAjaxSupport) e.getComponent()).getParent();
		TaxMatrix aTaxMatrix = (TaxMatrix) table.getRowData();
		selectedCompareTaxMatrix = new TaxMatrix();
		BeanUtils.copyProperties(aTaxMatrix, selectedCompareTaxMatrix);
		this.selectedCompareTaxMatrixIndex = table.getRowIndex();
		selectedCompareLocationMatrixIndex = -1;
		selectedCompareLocationMatrix=null;
	}
	
	public void selectedLocationMatrixChanged(ActionEvent e) throws AbortProcessingException { 
		HtmlDataTable table = (HtmlDataTable) ((HtmlAjaxSupport) e.getComponent()).getParent();
		LocationMatrix aLocationMatrix = (LocationMatrix) table.getRowData();
		selectedCompareLocationMatrix = new LocationMatrix();
		BeanUtils.copyProperties(aLocationMatrix, selectedCompareLocationMatrix);
		this.selectedCompareLocationMatrixIndex = table.getRowIndex();
		selectedCompareTaxMatrixIndex = -1;
		selectedCompareTaxMatrix=null;
	}
	
	public List<TaxMatrix> getMatchingTaxMatrixList() {
		return matchingTaxMatrixList;
	}

	public void setMatchingTaxMatrixList(List<TaxMatrix> matchingTaxMatrixList) {
		this.matchingTaxMatrixList = matchingTaxMatrixList;
	}

	public List<LocationMatrix> getMatchingLocationMatrixList() {
		return matchingLocationMatrixList;
	}

	public void setMatchingLocationMatrixList(List<LocationMatrix> matchingLocationMatrixList) {
		this.matchingLocationMatrixList = matchingLocationMatrixList;
	}
	
	public void matrixTypeValueChanged(ValueChangeEvent e){
		compareToMatrixType = (String)e.getNewValue();
		
		PurchaseTransaction trdetail = getCurrentTransaction();
		if(compareToMatrixType.contentEquals("T")) {
			compareToMatrixId = trdetail.getTaxMatrixId();
		}
		else if(compareToMatrixType.contentEquals("L")) {
			compareToMatrixId = trdetail.getShiptoLocnMatrixId();
		}
		
		matchingLocationMatrixList = null;
		matchingTaxMatrixList = null;
		
		selectedCompareTaxMatrixIndex = -1;
		selectedCompareLocationMatrixIndex = -1;
		
		displayTransMatrixAction();
	}
	
	public Map<Long, String> getEntityMap() {
		if (entityMap == null) {
			entityMap = new LinkedHashMap<Long, String>();
			List<EntityItem> eList = entityItemService.findAllEntityItems();//source list   
    	    if(eList != null && eList.size() > 0){
    	    	for(EntityItem entityItem:eList){
    	    		entityMap.put(entityItem.getEntityId(),entityItem.getEntityCode());
    	    	}
    	    }
		}
		return entityMap;
	}
	
	public Map<String, String> getModuleMap() {
		if (moduleMap == null) {
			moduleMap = new LinkedHashMap<String, String>();
			moduleMap.put("PURCHUSE",getModulecodeDescription("PURCHUSE"));
			moduleMap.put("BILLSALE",getModulecodeDescription("BILLSALE"));
		}
		return moduleMap;
	}
	
	public void displayTransMatrixAction() {
		
		//Retrieve matching..
		PurchaseTransaction trdetail = getCurrentTransaction();
		if(TaxMatrix.DRIVER_CODE.equals(compareToMatrixType)) {
			matchingTaxMatrixList = taxMatrixBean.getMatrixService().getMatchingTaxMatrixLines(trdetail);
			matchingLocationMatrixList = null;
		}
		
		if(LocationMatrix.DRIVER_CODE.equals(compareToMatrixType)) {
			matchingLocationMatrixList = locationMatrixBean.getMatrixService().getMatchingLocationMatrixLines(trdetail);
			matchingTaxMatrixList = null;
		}
		
		//Retrieve transaction information
		transactionMatrixList = null;
		transactionMatrixList = createTransactionMatrixList();
		
		
		compareToMatrixTypeSearched = compareToMatrixType;
		
		selectedCompareLocationMatrix=null;
		selectedCompareLocationMatrixIndex = -1;
		selectedCompareTaxMatrix=null;
		selectedCompareTaxMatrixIndex = -1;
	}
	
	public List<TransactionMatrixCompareDTO> createTransactionMatrixList() {
		transactionMatrixList = new ArrayList<TransactionMatrixCompareDTO>();

		System.out.println("Selected dropdown Matrix type was  :   "+compareToMatrixType); 
		//Validations
		if(compareToMatrixId==null || compareToMatrixId.longValue()<1L ){
			FacesMessage message = new FacesMessage("Please enter valid Matrix ID.");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
			return null;
		}
		
		if(TaxMatrix.DRIVER_CODE.equals(compareToMatrixType)) {
			TaxMatrix taxmatrix = null;
			if(compareToMatrixId != null){
				taxmatrix = getTaxMatrixBean().getMatrixService().findById(compareToMatrixId);
				if(taxmatrix == null){
					FacesMessage message = new FacesMessage(" You have selected Goods and Service from drop down. Please enter valid Tax Matrix ID.");
					message.setSeverity(FacesMessage.SEVERITY_ERROR);
					FacesContext.getCurrentInstance().addMessage(null, message);
					return null;
				}
			}
		}
		
		if(LocationMatrix.DRIVER_CODE.equals(compareToMatrixType)) {
			LocationMatrix locationmatrix = null;
			if(compareToMatrixId != null){
				locationmatrix = locationMatrixService.findById(compareToMatrixId);
				if(locationmatrix == null){
					FacesMessage message = new FacesMessage(" You have selected Location Matrix from drop down. Please enter valid Location Matrix ID.");
					message.setSeverity(FacesMessage.SEVERITY_ERROR);
					FacesContext.getCurrentInstance().addMessage(null, message);
					return null;
				}
			}
		}
		transactionMatrixList = new ArrayList<TransactionMatrixCompareDTO>();
		TransactionMatrixCompareDTO transactioninfo = createTransactionInfo();  //Transaction Row Data.First Row
		TransactionMatrixCompareDTO matrixinfo = createMatrixInfo();            //Matrix Data.Second Row    
		transactionMatrixList.add(transactioninfo); 
		transactionMatrixList.add(matrixinfo);
		TransactionMatrixCompareDTO compareinfo = createCompareTxnandMatrixInfo(); //Compare Data.Third Row
		transactionMatrixList.add(compareinfo);
		return transactionMatrixList;
	}
	
	public TransactionMatrixCompareDTO createTransactionInfo() {
		TransactionMatrixCompareDTO transactionInfo = new TransactionMatrixCompareDTO();
		Map<String,DriverNames> transactionDriverNamesMap = null;
		PurchaseTransaction selectedTransaction = getCurrentTransaction();
		if(TaxMatrix.DRIVER_CODE.equals(compareToMatrixType)) {
			transactionDriverNamesMap = matrixCommonBean.getCacheManager().getTransactionPropertyDriverNamesMap("T"); //  
			transactionInfo.setMatrixId(selectedTransaction.getTaxMatrixId().toString()); 
			
			Long matrixId = selectedTransaction.getTaxMatrixId();
			if(matrixId != null){
				TaxMatrix taxmatrix = getTaxMatrixBean().getMatrixService().findById(matrixId);
				if(taxmatrix!=null){
					transactionInfo.setBinaryWeight(taxmatrix.getBinaryWeight().toString());
				}
			}

		}
		if(LocationMatrix.DRIVER_CODE.equals(compareToMatrixType)) {
		    transactionDriverNamesMap = matrixCommonBean.getCacheManager().getTransactionPropertyDriverNamesMap("L"); //  
			transactionInfo.setMatrixId(selectedTransaction.getShiptoLocnMatrixId().toString());
			
			Long matrixId = selectedTransaction.getShiptoLocnMatrixId();
			if(matrixId != null){
				LocationMatrix locationMatrix = locationMatrixService.findById(matrixId);
				if(locationMatrix!=null){
					transactionInfo.setBinaryWeight(locationMatrix.getBinaryWt().toString());
				}
			}
		}
		transactionInfo.setComparemodule(COMPARE_TRANSACTION_LABEL);
		transactionInfo.setModulecode(getModuleMap().get(TRANS_MODULE_CODE));
		transactionInfo.setEntitycode(selectedTransaction.getEntityCode()); 
		transactionInfo.setEffectiveDate(DateFormatUtils.format(selectedTransaction.getGlDate(), "MM/dd/yyyy"));
		transactionInfo.setActiveFlag("n/a");
		
		for (String property : transactionDriverNamesMap.keySet()) {
			DriverNames driverNames = transactionDriverNamesMap.get(property);
			String driver = driverNames.getMatrixColName();
			String value = Util.getPropertyAsString(selectedTransaction, property); 
			if ((value != null) && !value.equals("")) {
				//System.out.println("  createTransactionObject  setting property '"+property+"' to value '"+value+"' from " + driver);
				Util.setProperty(transactionInfo, Util.columnToProperty(driver), value.toUpperCase(), String.class);
			}
		}
		
		
		//Best Matrix		
		if(TaxMatrix.DRIVER_CODE.equals(compareToMatrixType) && matchingTaxMatrixList!=null && matchingTaxMatrixList.size()>0) {
			if(selectedTransaction.getTaxMatrixId()!=null){
				Long matchId =  selectedTransaction.getTaxMatrixId();
				Long matrixId = matchingTaxMatrixList.get(0).getTaxMatrixId();
				if (matrixId.equals(matchId)){
					transactionInfo.setBestMatrix("Best Match");
				}
				else{
					boolean higherMatchFound = false;
					for(int i=0; i<matchingTaxMatrixList.size(); i++){
						matrixId = matchingTaxMatrixList.get(i).getTaxMatrixId();
						if (matrixId.equals(matchId)){
							transactionInfo.setBestMatrix("Higher Match Exists");
							higherMatchFound = true;
							break;
						}
					}
					if(!higherMatchFound){
						transactionInfo.setBestMatrix("No Match");
					}
				}
			}
		}		
		else if(LocationMatrix.DRIVER_CODE.equals(compareToMatrixType) && matchingLocationMatrixList!=null && matchingLocationMatrixList.size()>0) {
			if(selectedTransaction.getShiptoLocnMatrixId()!=null){
				Long matchId =  selectedTransaction.getShiptoLocnMatrixId();
				Long matrixId = matchingLocationMatrixList.get(0).getLocationMatrixId();
				if (matrixId.equals(matchId)){
					transactionInfo.setBestMatrix("Best Match");
				}
				else{
					boolean higherMatchFound = false;
					for(int i=0; i<matchingLocationMatrixList.size(); i++){
						matrixId = matchingLocationMatrixList.get(i).getLocationMatrixId();
						if (matrixId.equals(matchId)){
							transactionInfo.setBestMatrix("Higher Match Exists");
							higherMatchFound = true;
							break;
						}
					}
					if(!higherMatchFound){
						transactionInfo.setBestMatrix("No Match");
					}
				}
			}
		}

		return transactionInfo;
		
	}
	
	// this method has to return MatrixType either Location matrix or Goods Service matrix
	public TransactionMatrixCompareDTO createMatrixInfo() {
		TransactionMatrixCompareDTO comparematrixInfo = new TransactionMatrixCompareDTO();
		if(TaxMatrix.DRIVER_CODE.equals(compareToMatrixType)) {
			TaxMatrix taxmatrix = null;
			if(compareToMatrixId != null){
				taxmatrix = getTaxMatrixBean().getMatrixService().findById(compareToMatrixId);
			}
					
			if(taxmatrix != null) {
				Map<String,DriverNames> matrixDriverNamesMap = matrixCommonBean.getCacheManager().getMatrixColDriverNamesMap(TaxMatrix.DRIVER_CODE);
				for (String colname : matrixDriverNamesMap.keySet()) {
					String drivername =  Util.columnToProperty(colname);
					String value = Util.getPropertyAsString(taxmatrix, drivername); 
					if ((value != null) && !value.equals("")) {
						Util.setProperty(comparematrixInfo, drivername, value.toUpperCase(), String.class);
					}
				}
				EntityItem aEntityItem = entityItemService.findById(taxmatrix.getEntityId());
				comparematrixInfo.setMatrixId(compareToMatrixId.toString());
				comparematrixInfo.setModulecode(getModuleMap().get(taxmatrix.getModuleCode()));
				if(aEntityItem != null){
					comparematrixInfo.setEntitycode(aEntityItem.getEntityCode()); 
				}
				comparematrixInfo.setEffectiveDate(DateFormatUtils.format(taxmatrix.getEffectiveDate(), "MM/dd/yyyy"));
				comparematrixInfo.setExpirationDate(DateFormatUtils.format(taxmatrix.getExpirationDate(), "MM/dd/yyyy")); 
				comparematrixInfo.setBinaryWeight(taxmatrix.getBinaryWeight().toString());
				if(taxmatrix.getActiveFlag()!=null && taxmatrix.getActiveFlag().equals("1")){
					comparematrixInfo.setActiveFlag("Yes");
				}
				else{
					comparematrixInfo.setActiveFlag("No");
				}
			}
		}
		if(LocationMatrix.DRIVER_CODE.equals(compareToMatrixType)) {
			LocationMatrix locationMatrix = null;
			if(compareToMatrixId != null){
				locationMatrix = locationMatrixService.findById(compareToMatrixId);
				// TODO need to change  after glass search icons functionality implemented besides matrix id button
			}
				
			if(locationMatrix !=null) {
				Map<String,DriverNames> matrixDriverNamesMap = matrixCommonBean.getCacheManager().getMatrixColDriverNamesMap(LocationMatrix.DRIVER_CODE);
				for (String colname : matrixDriverNamesMap.keySet()) {
					String drivername =  Util.columnToProperty(colname);
					String value = Util.getPropertyAsString(locationMatrix, drivername); 
					if ((value != null) && !value.equals("")) {
						Util.setProperty(comparematrixInfo, drivername, value.toUpperCase(), String.class);
					}
				}
				EntityItem aEntityItem = entityItemService.findById(locationMatrix.getEntityId());
				comparematrixInfo.setMatrixId(compareToMatrixId.toString());
				comparematrixInfo.setEntitycode(aEntityItem.getEntityCode()); 
				comparematrixInfo.setEffectiveDate(DateFormatUtils.format(locationMatrix.getEffectiveDate(), "MM/dd/yyyy"));
				comparematrixInfo.setExpirationDate(DateFormatUtils.format(locationMatrix.getExpirationDate(), "MM/dd/yyyy")); 
				comparematrixInfo.setBinaryWeight(locationMatrix.getBinaryWt().toString());
				
			}
		}
		comparematrixInfo.setComparemodule(COMPARE_MATRIX_LABEL);
		
		//Best Matrix		
		if(TaxMatrix.DRIVER_CODE.equals(compareToMatrixType) && matchingTaxMatrixList!=null && matchingTaxMatrixList.size()>0) {
			if(compareToMatrixId!=null){
				Long matchId =  compareToMatrixId;
				Long matrixId = matchingTaxMatrixList.get(0).getTaxMatrixId();
				if (matrixId.equals(matchId)){
					comparematrixInfo.setBestMatrix("Best Match");
				}
				else{
					boolean higherMatchFound = false;
					for(int i=0; i<matchingTaxMatrixList.size(); i++){
						matrixId = matchingTaxMatrixList.get(i).getTaxMatrixId();
						if (matrixId.equals(matchId)){
							comparematrixInfo.setBestMatrix("Higher Match Exists");
							higherMatchFound = true;
							break;
						}
					}
					if(!higherMatchFound){
						comparematrixInfo.setBestMatrix("No Match");
					}
				}
			}
		}		
		else if(LocationMatrix.DRIVER_CODE.equals(compareToMatrixType) && matchingLocationMatrixList!=null && matchingLocationMatrixList.size()>0) {
			if(compareToMatrixId!=null){
				Long matchId =  compareToMatrixId;
				Long matrixId = matchingLocationMatrixList.get(0).getLocationMatrixId();
				if (matrixId.equals(matchId)){
					comparematrixInfo.setBestMatrix("Best Match");
				}
				else{
					boolean higherMatchFound = false;
					for(int i=0; i<matchingLocationMatrixList.size(); i++){
						matrixId = matchingLocationMatrixList.get(i).getLocationMatrixId();
						if (matrixId.equals(matchId)){
							comparematrixInfo.setBestMatrix("Higher Match Exists");
							higherMatchFound = true;
							break;
						}
					}
					if(!higherMatchFound){
						comparematrixInfo.setBestMatrix("No Match");
					}
				}
			}
		}
		
		
		return comparematrixInfo;   
	}
	
	public TransactionMatrixCompareDTO createCompareTxnandMatrixInfo() {
		TransactionMatrixCompareDTO comparetransmatrixInfo = new TransactionMatrixCompareDTO();
		comparetransmatrixInfo.setComparemodule(COMPARE_RESULT_LABEL);
		if(TaxMatrix.DRIVER_CODE.equals(compareToMatrixType)) {
			 
			if(StringUtils.equals(transactionMatrixList.get(0).getModulecode(), transactionMatrixList.get(1).getModulecode())) {
				comparetransmatrixInfo.setModulecode("Match");
			}
			else {
				comparetransmatrixInfo.setModulecode("No Match");
				 
			}
			if("Yes".equals(transactionMatrixList.get(1).getActiveFlag())) {
				comparetransmatrixInfo.setActiveFlag("Match");
			}
			else {
				comparetransmatrixInfo.setActiveFlag("No Match");
				 
			}	
			
			driverComparision(TaxMatrix.DRIVER_COUNT,comparetransmatrixInfo);
		}
		
		if(LocationMatrix.DRIVER_CODE.equals(compareToMatrixType)) {
			driverComparision(LocationMatrix.DRIVER_COUNT, comparetransmatrixInfo);
		}
		
		if(StringUtils.equals(transactionMatrixList.get(0).getEntitycode(), transactionMatrixList.get(1).getEntitycode())) {
			comparetransmatrixInfo.setEntitycode("Match");
		}
		else {
			comparetransmatrixInfo.setEntitycode("No Match");
			 
		}
		
		try{
			if(transactionMatrixList.get(0).getEffectiveDate() == null || transactionMatrixList.get(1).getEffectiveDate() == null) {
				comparetransmatrixInfo.setExpirationDate("No Match");
			}
			else {
				Date transactioneffdate = new SimpleDateFormat("MM/dd/yyyy").parse(transactionMatrixList.get(0).getEffectiveDate());  
				Date comparematrixeffdate = new SimpleDateFormat("MM/dd/yyyy").parse(transactionMatrixList.get(1).getEffectiveDate());  
				if(comparematrixeffdate.before(transactioneffdate) || comparematrixeffdate.equals(transactioneffdate)) { 
					comparetransmatrixInfo.setEffectiveDate("Match");
				}
				else {
					comparetransmatrixInfo.setEffectiveDate("No Match");
					 
				}
			}
		}catch(ParseException parserException) {
			logger.debug("Exception occured while parsting dates in the method createCompareTxnandMatrixInfo in the class TransactionDetailBean  " ); 
			System.out.println("E xception occured...");
		}
		
		try{
			if(transactionMatrixList.get(0).getEffectiveDate() == null || transactionMatrixList.get(1).getExpirationDate() == null) {
				comparetransmatrixInfo.setExpirationDate("No Match");
			}
			else{
				Date transactioneffdate = new SimpleDateFormat("MM/dd/yyyy").parse(transactionMatrixList.get(0).getEffectiveDate());  
				Date comparematrixexpdate = new SimpleDateFormat("MM/dd/yyyy").parse(transactionMatrixList.get(1).getExpirationDate());  
				if(comparematrixexpdate.after(transactioneffdate) || comparematrixexpdate.equals(transactioneffdate)) { 
					comparetransmatrixInfo.setExpirationDate("Match");
				}
				else {
					comparetransmatrixInfo.setExpirationDate("No Match");
					 
				}
			}
			
		}catch(ParseException parserException) {
			logger.debug("Exception occured while parsting dates in the method createCompareTxnandMatrixInfo in the class TransactionDetailBean  " ); 
			System.out.println(" exception occured...");
		}
		
		comparetransmatrixInfo.setBinaryWeight("");

		return comparetransmatrixInfo;
	}
	
	public String getModulecodeDescription(String modulecode) {
		String description = "";
		Map<String, ListCodes> listCodeList =  matrixCommonBean.getCacheManager().getListCodesMapByType("MODULE");
		if(listCodeList.get(modulecode)!=null){
			description = listCodeList.get(modulecode).getDescription();
		}
		
		if(description==null || description.trim().length()==0) {
			description = modulecode;
		}
		
		return description;
	}
	
	
	public void driverComparision(long driverCount, TransactionMatrixCompareDTO comparetransmatrixInfo) {
		
		Map<String,DriverNames> matrixDriverMap = null;	
		if(compareToMatrixType.contentEquals("T")) {
			matrixDriverMap = matrixCommonBean.getTaxMatrixDriverMap();
		}
		else if(compareToMatrixType.contentEquals("L")) {
			matrixDriverMap = matrixCommonBean.getLocationMatrixDriverMap();
		}
		
		DriverNames driverNames = null;	
		for (int i = 1; i <= driverCount; i++) {
			//System.out.println(" string driver is : "+String.format("driver%02d", i)); 
			String columndriver = String.format("driver%02d", i);
			String transdriver = (String) Util.getProperty(transactionMatrixList.get(0), String.format("driver%02d", i));
			String matrixdriver = (String) Util.getProperty(transactionMatrixList.get(1), String.format("driver%02d", i));
			if(matrixdriver == null) {
				break;
			}
			
			String namedriver = "DRIVER_" + String.format("%02d", i);	
			driverNames = matrixDriverMap.get(namedriver);
			if(driverNames == null) {
				break;
			}
			
			//else is off, then Result=blank
			if(driverNames.getActiveFlag()==null || !driverNames.getActiveFlag().equals("1")){
				Util.setProperty(comparetransmatrixInfo, columndriver, "Inactive", String.class);
				continue;
			}
			
			if("*ALL".equals(matrixdriver)) {
					Util.setProperty(comparetransmatrixInfo, columndriver, "Match", String.class);
					continue;
			}
			if(matrixdriver.equalsIgnoreCase(transdriver)){
				Util.setProperty(comparetransmatrixInfo, columndriver, "Match", String.class);
			}
			if(!matrixdriver.equalsIgnoreCase(transdriver)){
				Util.setProperty(comparetransmatrixInfo, columndriver, "No Match", String.class);
			}

			if(driverNames.getNullDriverFlag()!=null && driverNames.getNullDriverFlag().equals("1")){
				if("*NULL".equalsIgnoreCase(matrixdriver) && transdriver == null) {
						Util.setProperty(comparetransmatrixInfo, columndriver, "Match", String.class);
				}
				if("*NULL".equalsIgnoreCase(matrixdriver) && transdriver != null) {
					Util.setProperty(comparetransmatrixInfo, columndriver, "No Match", String.class);
			    }
			}
			
			if(driverNames.getWildcardFlag()!=null && driverNames.getWildcardFlag().equals("1")){
				if(matrixdriver.endsWith("%")) {
					if(transdriver == null){
						Util.setProperty(comparetransmatrixInfo, columndriver, "No Match", String.class);
					}
					else if (matrixdriver.length() >= 2){
						String subValue = matrixdriver.substring(0, matrixdriver.length()-1);
						if(transdriver.startsWith(subValue)){
							Util.setProperty(comparetransmatrixInfo, columndriver, "Match", String.class);
						}
						else {
							Util.setProperty(comparetransmatrixInfo, columndriver, "No Match", String.class);
						}
						
					}
				}
			}
	    }
	}
	
	public boolean getValidMatrixAnalysisSelection() {
		if(selectedTransaction !=null) {
			return (getSelectionCount() > 0 && (getCurrentTransaction().getTaxMatrixId() != null || getCurrentTransaction().getShiptoLocnMatrixId() !=null));
		}
		return false;
	}
	
	public String cancelMatrixAnalysisAction() {
		return  "trans_main";
	}
	
	
}
