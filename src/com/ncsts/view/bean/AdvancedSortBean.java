package com.ncsts.view.bean;

import java.util.ArrayList;
import java.util.List;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.Collection;
import java.util.Iterator;

import javax.faces.event.AbortProcessingException;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.ajax4jsf.component.html.HtmlAjaxSupport;
import org.apache.log4j.Logger;
import org.richfaces.component.html.HtmlDataTable;

import com.ncsts.common.LoggerFactory;
import com.ncsts.common.Util;
import com.ncsts.jsf.model.TransactionDataModel;
import com.ncsts.services.DataDefinitionService;
import com.ncsts.domain.DataDefinitionColumn;
import com.ncsts.jsf.model.BaseExtendedDataModel;
import com.ncsts.dto.ColumnItem;

public class AdvancedSortBean {

	static private Logger logger = LoggerFactory.getInstance().getLogger(
			AdvancedSortBean.class);
	
	public interface SortCallback {
		void sortCallback(String context);
	}

	private DataDefinitionService dataDefinitionService;
	private List<DataDefinitionColumn> dataDefinitionColList;
	private TransactionDataModel transactionDataModel;
	
	private Map<String,String> transactionSortMap;
	private Map<String,String> sortOrder;
	
	private ArrayList<ColumnItem> columnItemList;
	private ArrayList<ColumnItem> sortItemList;
	
	private Collection<ColumnItem> selectedColumnItemList;
	private Collection<ColumnItem> selectedSortItemList;
	
	private int selectedColumnRowIndex = -1;
	private int selectedSortRowIndex = -1;
	
	private ColumnItem selectedColumnItem;
	private ColumnItem selectedSortItem;
	
	private SortCallback callback;
	
	public Collection<ColumnItem> getSelectedColumnItemList() {
		return selectedColumnItemList;
	}
	
	public void setSelectedColumnItemList(Collection<ColumnItem> selectedColumnItemList) {
		this.selectedColumnItemList = selectedColumnItemList;
	}
	
	
	public Collection<ColumnItem> getSelectedSortItemList() {
		return selectedSortItemList;
	}
	
	public void setSelectedSortItemList(Collection<ColumnItem> selectedSortItemList) {
		this.selectedSortItemList = selectedSortItemList;
	}
	
	public AdvancedSortBean() {
	}
	
	public void setCallback(SortCallback callback) {
		this.callback = callback;
	}
	
	public ArrayList<ColumnItem> getColumnItemList() {
		if(columnItemList==null){
			columnItemList = new ArrayList<ColumnItem>();
			for (String field : transactionSortMap.keySet()) {
				columnItemList.add(new ColumnItem(field, transactionSortMap.get(field)));
			}
		}
		
		List<ColumnItem> mapList = getSortItemList();
		for(ColumnItem columnObj : mapList){
			this.columnItemList.remove(columnObj);
		}
		return columnItemList;
	}

	public void setColumnItemList(ArrayList<ColumnItem> columnItemList) {
		this.columnItemList = columnItemList;
	}	
	
	public ArrayList<ColumnItem> getSortItemList() {
		if(sortItemList==null){
			sortItemList = new ArrayList<ColumnItem>();
			for (String field : sortOrder.keySet()) {
				if (!BaseExtendedDataModel.SORT_NONE.equals(sortOrder.get(field))) {
					if(BaseExtendedDataModel.SORT_ASCENDING.equals(sortOrder.get(field))){
						sortItemList.add(new ColumnItem(field, transactionSortMap.get(field), true));
					}
					else{
						sortItemList.add(new ColumnItem(field, transactionSortMap.get(field), false));
					}
				}
			}
		}

		return sortItemList;
	}

	public void setSortItemList(ArrayList<ColumnItem> sortItemList) {
		this.sortItemList = sortItemList;
	}	
	
	public Map<String,String> getTransactionSortMap(){
		return transactionSortMap;
	}
	
	public Map<String,String> getSortOrder(){
		return sortOrder;
	}

	public void setTransactionSortMap(Map<String,String> transactionSortMap){
		this.transactionSortMap = transactionSortMap;
	}
	
	public void setSortOrder(Map<String,String> sortOrder){
		this.sortOrder = sortOrder;
	}

	public int getSelectedColumnRowIndex() {
		return selectedColumnRowIndex;
	}

	public void setSelectedColumnRowIndex(int selectedColumnRowIndex) {
		this.selectedColumnRowIndex = selectedColumnRowIndex;
	}
	
	public int getSelectedSortRowIndex() {
		return selectedSortRowIndex;
	}

	public void setSelectedSortRowIndex(int selectedSortRowIndex) {
		this.selectedSortRowIndex = selectedSortRowIndex;
	}

	public TransactionDataModel getTransactionDataModel() {
		return transactionDataModel;
	}

	public void setTransactionDataModel(
			TransactionDataModel transactionDataModel) {
		this.transactionDataModel = transactionDataModel;
	}

	public List<DataDefinitionColumn> getDataDefinitionColList() {
		if (dataDefinitionColList == null) {
			dataDefinitionColList = dataDefinitionService.getAllDataDefinitionColumnByTable("TB_PURCHTRANS");
		}

		return dataDefinitionColList;
	}

	public void setDataDefinitionColList(
			List<DataDefinitionColumn> dataDefinitionColList) {
		this.dataDefinitionColList = dataDefinitionColList;
	}

	public DataDefinitionService getDataDefinitionService() {
		return dataDefinitionService;
	}

	public void setDataDefinitionService(
			DataDefinitionService dataDefinitionService) {
		this.dataDefinitionService = dataDefinitionService;
	}

	public void initAdvancedSort(Map<String,String> transactionSortMap, Map<String,String> sortOrder) {
		this.transactionSortMap = transactionSortMap;
		this.sortOrder = sortOrder;
		this.columnItemList = null;
		this.sortItemList = null;
		
		selectedColumnRowIndex = -1;
		selectedSortRowIndex = -1;
		selectedColumnItem = null;
		selectedSortItem = null;
	}

	public void selectedColumnChanged(ActionEvent e) throws AbortProcessingException {
		HtmlDataTable table = (HtmlDataTable) (((HtmlAjaxSupport) e.getComponent()).getParent());
		selectedColumnRowIndex = table.getRowIndex();
		selectedColumnItem = (ColumnItem) table.getRowData();
	}
	
	public void selectedSortChanged(ActionEvent e) throws AbortProcessingException {
		HtmlDataTable table = (HtmlDataTable) (((HtmlAjaxSupport) e.getComponent()).getParent());
		selectedSortRowIndex = table.getRowIndex();
		selectedSortItem = (ColumnItem) table.getRowData();
	}

	public void selectedDblColumnChanged(ActionEvent e) throws AbortProcessingException {
		HtmlDataTable table = (HtmlDataTable) (((HtmlAjaxSupport) e.getComponent()).getParent());
		selectedColumnRowIndex = table.getRowIndex();
		selectedColumnItem = (ColumnItem) table.getRowData();
		
		selectedColumnItem.setAscending(!selectedColumnItem.getAscending());
	}
	
	public void selectedDblSortChanged(ActionEvent e) throws AbortProcessingException {
		HtmlDataTable table = (HtmlDataTable) (((HtmlAjaxSupport) e.getComponent()).getParent());
		selectedSortRowIndex = table.getRowIndex();
		selectedSortItem = (ColumnItem) table.getRowData();
		
		selectedSortItem.setAscending(!selectedSortItem.getAscending());
	}
	
	public String assignSortColumn(){	
		if(selectedColumnItem!=null){
	        this.sortItemList.add(selectedColumnItem);	
	        this.columnItemList.remove(selectedColumnItem);
		}
		
		selectedColumnRowIndex = -1;
		selectedSortRowIndex = -1;
		selectedColumnItem = null;
		selectedSortItem = null;
		
		return "";
	}
	
	public String removeSortColumn(){
		if(selectedSortItem!=null){	
			this.columnItemList.add(selectedSortItem);
	        this.sortItemList.remove(selectedSortItem);	
		}
	
		selectedColumnRowIndex = -1;
		selectedSortRowIndex = -1;
		selectedColumnItem = null;
		selectedSortItem = null;
		
		return "";
	}
	
	public String removeAllSortColumn(){
		for(Iterator<ColumnItem> itr = sortItemList.iterator(); itr.hasNext();){
			ColumnItem columnItem = (ColumnItem)itr.next();	
			
			//this.sortItemList.remove(columnItem);	
	        this.columnItemList.add(columnItem);
		}
		
		this.sortItemList.clear();
		
		return "";
	}
	
	public String okAdvancedSortAction() {
		//Reset all sorting
		Map<String,String> tempMap = new HashMap(sortOrder);
		
		for (String key : tempMap.keySet()) {
			tempMap.put(key, BaseExtendedDataModel.SORT_NONE);
		}
		
		//Since it is a Map That Retains Order-of-Insertion
		sortOrder.clear();
		
		//Step 1: set order
		for(Iterator<ColumnItem> itr = sortItemList.iterator(); itr.hasNext();){
			ColumnItem columnItem = itr.next();	
			if(columnItem.getAscending()){
				sortOrder.put(columnItem.getField(), BaseExtendedDataModel.SORT_ASCENDING);
			}
			else{
				sortOrder.put(columnItem.getField(), BaseExtendedDataModel.SORT_DESCENDING);
			}
		}
		
		//Step 2: get all columns
		for (String key : tempMap.keySet()) {
			sortOrder.put(key, BaseExtendedDataModel.SORT_NONE);
		}
		
		//Step 3: reset value
		for(Iterator<ColumnItem> itr = sortItemList.iterator(); itr.hasNext();){
			ColumnItem columnItem = itr.next();	
			if(columnItem.getAscending()){
				sortOrder.put(columnItem.getField(), BaseExtendedDataModel.SORT_ASCENDING);
			}
			else{
				sortOrder.put(columnItem.getField(), BaseExtendedDataModel.SORT_DESCENDING);
			}
		}
		
		callback.sortCallback("sort");
		return "transactions_process";
	}

	public String cancelAdvancedSortAction() {
		return "transactions_process";
	}
}