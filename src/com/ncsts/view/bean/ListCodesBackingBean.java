package com.ncsts.view.bean;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.component.UISelectItems;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.ajax4jsf.component.html.HtmlAjaxSupport;
import org.apache.log4j.Logger;
import org.richfaces.component.html.HtmlDataTable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;

import com.ncsts.common.CacheManager;
import com.ncsts.domain.ListCodes;
import com.ncsts.domain.ListCodesPK;
import com.ncsts.domain.User;
import com.ncsts.dto.ListCodesDTO;
import com.ncsts.services.ListCodesService;

/**
 * @author Muneer
 *
 */
public class ListCodesBackingBean /*extends JsfHelper*/ {
	
	private Logger logger = Logger.getLogger(ListCodesBackingBean.class);
	
	private EditAction currentAction = EditAction.VIEW;
	
	private HtmlSelectOneMenu codeCodeMenu; //= new HtmlSelectOneMenu();
	
	private String codeTypeCode = ""; //this is declared for search
	private List<ListCodesDTO> listCodesList = new ArrayList<ListCodesDTO>(); // this list is declared for displaying the table rows
	private ListCodesDTO selectedListCodes; //this is declared for capturing one selected row from the table i.e by default 0th index of selected rows
	private ListCodesDTO selectedListCodesOri; //Keep the original selected
	private ListCodesDTO selectedDefListCodes; //keep selected list code definition
	
	private int selectedRowIndex = -1;
	
	private ListCodesService listCodesService;
	private CacheManager cacheManager;
	private String completeAction;
	
	@Autowired
	private loginBean loginBean;
	

	public ListCodesService getListCodesService() {
		return listCodesService;
	}

	public void setListCodesService(ListCodesService listCodesService) {
		this.listCodesService = listCodesService;
	}

	public CacheManager getCacheManager() {
		return cacheManager;
	}

	public void setCacheManager(CacheManager cacheManager) {
		this.cacheManager = cacheManager;
	}

	public HtmlSelectOneMenu getCodeCodeMenu() {				
		if (codeCodeMenu == null){			
			codeCodeMenu = new HtmlSelectOneMenu();			
			final Collection<SelectItem> codeCodeList = new ArrayList<SelectItem>();		
			codeCodeList.add(new SelectItem("","Select Codes"));
			codeCodeList.add(new SelectItem("*DEF","*DEF - List Code Definitions"));
					
			List<ListCodes> listCodeList = listCodesService.getAllListCodes();		
			for (ListCodes listcodes : listCodeList) {			
				if(listcodes != null && listcodes.getCodeCode()!=null && listcodes.getDescription()!=null){				
					String tem=listcodes.getCodeCode()+" - "+listcodes.getDescription();
					logger.debug("Adding: "+tem);
					
					codeCodeList.add(new SelectItem(listcodes.getCodeCode(),tem));
				}
			}
			final UISelectItems items = new UISelectItems();
			items.setValue(codeCodeList);
			//items.setId("batchTypeId");//sets an unique id.... Check this out
			codeCodeMenu.getChildren().add(items);
		}	
		
		return codeCodeMenu;
	}
	
	public void setCodeCodeMenu(HtmlSelectOneMenu codeCodeMenu) {
		this.codeCodeMenu = codeCodeMenu;
	}

	/**
	 * @return the listCodesList
	 */
	public List<ListCodesDTO> getListCodesList() {
		return listCodesList;		
	}

	public String getCodeTypeCode() {
		return codeTypeCode;
	}

	public void setCodeTypeCode(String codeTypeCode) {
		this.codeTypeCode = codeTypeCode;
	}

	public String retrieveListCodes() {
		logger.debug("  From ListCodesBackingBean::retrieveListCodes::codeTypeCode="+codeTypeCode);
		listCodesList = listCodesService.getListCodesByCodeTypeCodeAsDTO(codeTypeCode);
		selectedListCodes = null;
		selectedListCodesOri = null;
		selectedRowIndex = -1;
			
		if (!isListCodeDefinition()) {
			if(codeTypeCode==null || codeTypeCode.length()==0){
				selectedDefListCodes = null;
			}
			else{
				ListCodesPK pk = new ListCodesPK("*DEF", codeTypeCode);
				selectedDefListCodes = (ListCodesDTO)listCodesService.findByPK(pk).getListCodesDTO();
			}
		}
		else{
			selectedDefListCodes = null;
		}
		
		return null;
	}
	
	public boolean isListCodeDefinition(){
		if (codeTypeCode!=null && codeTypeCode.length()!=0 && codeTypeCode.equalsIgnoreCase("*DEF")){
			return true;
		}
		else{
			return false;
		}
	}
	
	public boolean getDeleteAction() {
		return currentAction.equals(EditAction.DELETE);
	}
	
	public boolean getViewAction() {
		return currentAction.equals(EditAction.VIEW);
	}

	public boolean getUpdateAction() {
		return currentAction.equals(EditAction.UPDATE);
	}

	public String getActionText() {
		return currentAction.getActionText();
	}
	
	/**
	 * @return the selectedListCodes
	 */
	public ListCodesDTO getSelectedListCodes() {
		return selectedListCodes;
	}
	
	public ListCodesDTO getSelectedListCodesOri() {
		return selectedListCodesOri;
	}
	
	public ListCodesDTO getSelectedDefListCodes() {
		return selectedDefListCodes;
	}

	public int getSelectedRowIndex() {
		return selectedRowIndex;
	}

	public void setSelectedRowIndex(int selectedRowIndex) {
		this.selectedRowIndex = selectedRowIndex;
	}

	public void selectedRowChanged(ActionEvent e) throws AbortProcessingException { 
		HtmlDataTable table = (HtmlDataTable) ((HtmlAjaxSupport) e.getComponent()).getParent();
		this.selectedListCodes = (ListCodesDTO) table.getRowData();
		this.selectedListCodesOri = selectedListCodes;
		this.selectedRowIndex = table.getRowIndex();
	}

	public String viewAction() {
		if(selectedListCodesOri!=null){
			selectedListCodes = selectedListCodesOri;
		}
		else{
			selectedListCodes = null;
		}
	    return completeAction;
	}	
	
	public String displayUpdateAction() {
		selectedListCodes = selectedListCodesOri;
		currentAction = EditAction.UPDATE;
		completeAction = "list_codes_main";
		
		if(isListCodeDefinition()){
			return "list_codes_definition_update";
		}
		else{
			return "list_codes_update";
		}
	}
	
	public String externalUpdateAction(ListCodesDTO selectedListCodes, String completeAction) {
		currentAction = EditAction.UPDATE;
		ListCodesPK pk = new ListCodesPK("*DEF", selectedListCodes.getCodeTypeCode());
		this.selectedDefListCodes = listCodesService.findByPK(pk).getListCodesDTO();
		this.selectedListCodes = selectedListCodes;
		this.completeAction = completeAction;
		return "list_codes_update";
	}
	
	public String externalViewAction(ListCodesDTO selectedListCodes, String completeAction) {
		currentAction = EditAction.VIEW;
		ListCodesPK pk = new ListCodesPK("*DEF", selectedListCodes.getCodeTypeCode());
		this.selectedDefListCodes = listCodesService.findByPK(pk).getListCodesDTO();
		this.selectedListCodes = selectedListCodes;
		this.completeAction = completeAction;
		return "list_codes_update";
	}
	
	public String displayDeleteAction() {
		selectedListCodes = selectedListCodesOri;;
		currentAction = EditAction.DELETE;
		completeAction = "list_codes_main";
		
		if(isListCodeDefinition()){
			return "list_codes_definition_delete";
		}
		else{
			return "list_codes_delete";
		}
	}	

	public String externalDeleteAction(ListCodesDTO selectedListCodes, String completeAction) {
		currentAction = EditAction.DELETE;
		ListCodesPK pk = new ListCodesPK("*DEF", selectedListCodes.getCodeTypeCode());
		this.selectedDefListCodes = listCodesService.findByPK(pk).getListCodesDTO();
		this.selectedListCodes = selectedListCodes;
		this.completeAction = completeAction;
		return "list_codes_delete";
	}
	
	public String displayAddAction() {
		currentAction = EditAction.ADD;
		completeAction = "list_codes_main";

		selectedListCodes =  new ListCodesDTO();
		ListCodesPK pk = new ListCodesPK();
		pk.setCodeTypeCode(codeTypeCode);
		selectedListCodes.setListCodesPK(pk);
		
		if(isListCodeDefinition()){
			return "list_codes_definition_add";
		}
		else{
			return "list_codes_add";
		}
	}
	
	public String externalAddAction(String codeType, String completeAction) {
		currentAction = EditAction.ADD;
		this.completeAction = completeAction;

		ListCodesPK pk = new ListCodesPK("*DEF", codeType);
		this.selectedDefListCodes = listCodesService.findByPK(pk).getListCodesDTO();
		selectedListCodes =  new ListCodesDTO();
		pk = new ListCodesPK();
		pk.setCodeTypeCode(codeType);
		selectedListCodes.setListCodesPK(pk);
		
		return "list_codes_add";
	}
	
	public String okAction() {
		String result = null;

		if (selectedListCodes != null) {
			cacheManager.flushListCodesByType(selectedListCodes.getCodeTypeCode());
		}
		
		switch (currentAction) {
			case ADD:
				result = addAction();
				break;
				
			case DELETE:
				result = removeAction();
				break;
				
			case UPDATE:
				result = updateAction();
				break;
				
			case VIEW:
				 return completeAction;
		}
		
		return result;
	}
	
	public String okDefinitionAction() {
		String result = null;

		switch (currentAction) {
			case ADD:
				result = addDefinitionAction();
				break;
				
			case DELETE:
				result = removeDefinitionAction();
				break;
				
			case UPDATE:
				result = updateDefinitionAction();
				break;
		}
		
		return result;
	}

	public String removeAction() {
		logger.info("start removeAction for ListCodes Backing Bean");
		ListCodesPK lpk = new ListCodesPK();
		lpk.setCodeTypeCode(selectedListCodes.getCodeTypeCode());
		lpk.setCodeCode(selectedListCodes.getCodeCode());
		listCodesService.delete(lpk);
		retrieveListCodes();
		logger.info("end removeAction");	    
	    return completeAction;
	  }

	public String updateAction() {
		logger.info("start updateAction for List Codes from Backing Bean"); 
		listCodesService.update(selectedListCodes);
		retrieveListCodes();
		logger.info("end updateAction");	    
	    return completeAction;
	}
	
	public String addAction() {
		logger.info("start addAction for ListCodes "); 
		logger.debug("**********listCodesDTONew values for Add are ***********");		
		
		logger.debug(selectedListCodes.toString());
		
		try {
			selectedListCodes.getListCodesPK().setCodeCode(selectedListCodes.getCodeCode());
			listCodesService.addListCodeRecord(selectedListCodes);
		} catch (DataIntegrityViolationException ex) {
			// TODO: How to communicate duplicate??
			ex.printStackTrace();
			logger.error("Error adding code");
			return "list_codes_add";
		}
		
		retrieveListCodes();
		logger.info("end addAction for List Codes ");
	    return completeAction;
	}
	
	public String removeDefinitionAction() {
		logger.info("start removeDefinitionAction for ListCodes Backing Bean");
		ListCodesPK lpk = new ListCodesPK();
		lpk.setCodeTypeCode(selectedListCodes.getCodeTypeCode());
		lpk.setCodeCode(selectedListCodes.getCodeCode());
		listCodesService.delete(lpk);
		codeCodeMenu = null;
		retrieveListCodes();
		logger.info("end removeDefinitionAction");	    
	    return "list_codes_main";
	  }

	public String updateDefinitionAction() {
		logger.info("start updateDefinitionAction for List Codes from Backing Bean"); 
		listCodesService.update(selectedListCodes);
		codeCodeMenu = null;
		retrieveListCodes();
		logger.info("end updateDefinitionAction");	    
	    return "list_codes_main";
	}
	
	public String addDefinitionAction() {
		logger.info("start addDefinitionAction for ListCodes Definition"); 

		logger.debug(selectedListCodes.toString());
		
		try {
			selectedListCodes.getListCodesPK().setCodeCode(selectedListCodes.getCodeCode());
			listCodesService.addListCodeRecord(selectedListCodes);
		} catch (DataIntegrityViolationException ex) {
			ex.printStackTrace();
			logger.error("Error adding definition");
			return "list_codes_definition_add";
		}
		
		codeCodeMenu = null;
		retrieveListCodes();
		logger.info("end addDefinitionAction for List Codes Definition ");
		return "list_codes_main";
	}
	
	public void validateCode(FacesContext context, UIComponent toValidate, Object value) {
	    // Ensure table name is unique
		String code = (String) value;
		ListCodesPK pk = new ListCodesPK(selectedListCodes.getCodeTypeCode(), code);
		if (currentAction.isAddAction() && (listCodesService.findByPK(pk) != null)) {
			((UIInput)toValidate).setValid(false);
			String errorInfo = "";
			if(isListCodeDefinition()){
				errorInfo = "List Code Definition already exists.";
			}
			else{
				errorInfo = "List Code already exists.";
			}
			
			FacesMessage message = new FacesMessage(errorInfo);
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage(toValidate.getClientId(context), message);
	    }
	}
	
	public List<ListCodes> filterListCodes(String typeCode,String code){
		return listCodesService.getListCodesByTypeAndCode(typeCode, code);
	}
	public User getCurrentUser() {
		return loginBean.getUser();
   }
}
