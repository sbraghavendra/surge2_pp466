package com.ncsts.view.bean;


import java.nio.charset.Charset;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;

import com.ncsts.dto.DbPropertiesDTO;
import com.ncsts.management.SQLiteConnectionPool;



//
//Here are the dbcp-specific classes.
//Note that they are only used in the setupDataSource
//method. In normal use, your classes interact
//only with the standard JDBC API
//
import org.apache.commons.pool.ObjectPool;
import org.apache.commons.pool.impl.GenericObjectPool;
import org.apache.commons.dbcp.BasicDataSource;
import org.apache.commons.dbcp.ConnectionFactory;
import org.apache.commons.dbcp.PoolingDataSource;
import org.apache.commons.dbcp.PoolableConnectionFactory;
import org.apache.commons.dbcp.DriverManagerConnectionFactory;

import javax.sql.DataSource;

import java.sql.SQLException;

public class RoutingDataSource extends AbstractRoutingDataSource{
	//private Map targetDataSources;
	//private Object defaultTargetDataSource;
	private String selectedDataSource;
	private String connectionURL = null;
	private String driverClassName = null;
	private String connectionUserName = null;
	private String connectionPassword = null;
	
	private SQLiteConnectionPool connectionPool = null;
	
	private String defaultDatabaseName = null;
	private String securityDbFile = null;
	
	@Override
	protected Object determineCurrentLookupKey() {
    	    logger.info("RoutingDataSource.determineCurrentLookupKey() - enter");		
		    if (ChangeContextHolder.getDataBaseType() != null) {
		    	logger.info("ChangeContextHolder.getDataBaseType() != null");
				return ChangeContextHolder.getDataBaseType();		    	
		    } else {	
		    	logger.info("ChangeContextHolder.getDataBaseType() == null, getting ds from session");
				return "";			    	
		    }
	}
	
	public void setProperties(String file) {
		this.securityDbFile = file;
	  	 Map<Object, Object> map = new HashMap<Object, Object>();
		 setTargetDataSources(map);
		 logger.info("enter RoutingDataSource.setProperties()");
	
	  	 try {		 
	  		if(file != null && file.indexOf("security_db.db")>=0){
	  			connectionURL = "jdbc:sqlite:" + file; 
		  		driverClassName = "org.sqlite.JDBC";
		  		connectionPool = new SQLiteConnectionPool("SQLite", connectionURL, driverClassName, connectionUserName, connectionPassword, 5);		
	  		}
	  		else if(file != null && file.toLowerCase().indexOf("jdbc:postgresql")>=0){
	  			// File format: "jdbc:postgresql://127.0.0.1:5432/postgres;postgres;admin";
	  			String[] tokens = file.split(";");
	  			if(tokens.length==3){
	  				driverClassName = "org.postgresql.Driver"; 
	  				connectionURL = tokens[0];
			  		connectionUserName = tokens[1];
			  		connectionPassword = tokens[2];
			  		connectionPool = new SQLiteConnectionPool("postgresql", connectionURL, driverClassName, connectionUserName, connectionPassword, 5);
	  			}
	  			else{
	  				return;
	  			}
	  		}
	  		else if(file != null && file.toLowerCase().indexOf("jdbc:oracle")>=0){
	  			// File format: "jdbc:oracle:thin:@10.1.106.14:1521:TESTDB4;stscorp;STSCORP";
	  			String[] tokens = file.split(";");
	  			if(tokens.length==3){
	  				driverClassName = "oracle.jdbc.driver.OracleDriver"; 
	  				connectionURL = tokens[0];
			  		connectionUserName = tokens[1];
			  		connectionPassword = tokens[2];
			  		connectionPool = new SQLiteConnectionPool("oracle", connectionURL, driverClassName, connectionUserName, connectionPassword, 5);
	  			}
	  			else{
	  				return;
	  			}
	  		}
	  		else{
	  			return;
	  		}

	  		System.out.println("############ RoutingDataSource: connectionURL: " + connectionURL + ", schema: " + connectionUserName);

	  		Connection conn = CreateSQLiteConnection();
	  		List<DbPropertiesDTO> result = new ArrayList<DbPropertiesDTO>();
			try{
				Statement stat = conn.createStatement();
		    	ResultSet rs = stat.executeQuery("select * from TB_DB_PROPERTIES order by DB_ID desc");
		    	while (rs.next()) {
		    		DbPropertiesDTO dbPropertiesDTO = new DbPropertiesDTO(); 
		    		dbPropertiesDTO.setDbId(rs.getInt("DB_ID"));
		    		dbPropertiesDTO.setUserName(rs.getString("USER_NAME"));
		    		dbPropertiesDTO.setPassword(rs.getString(RoutingDataSource.getColumnHeader(conn)+"PASSWORD"));
		    		dbPropertiesDTO.setDatabaseName(rs.getString("DATABASE_NAME"));
		    		dbPropertiesDTO.setDriverClassName(rs.getString("DRIVER_CLASS_NAME"));
		    		dbPropertiesDTO.setUrl(rs.getString("URL"));
		    		result.add(dbPropertiesDTO);   
		    	}
		    	rs.close();	
			}
			catch (Exception e){
				e.printStackTrace();
				return;
			}
			CloseSQLiteConnection(conn);
			
			List<DbPropertiesDTO> dbPropertiesList  = result;
	  		int i = 0;
	  		for (DbPropertiesDTO dbPropertiesDTO : dbPropertiesList) {			
	  			i++;
				
	  			/*
	  			DriverManagerDataSource ds = new DriverManagerDataSource();
	  			ds.setDriverClassName(dbPropertiesDTO.getDriverClassName());
	  			ds.setUrl(dbPropertiesDTO.getUrl());
	  			logger.info("url secu= " + dbPropertiesDTO.getUrl());
	  			 
	  			ds.setUsername(dbPropertiesDTO.getUserName());
	  			ds.setPassword(dbPropertiesDTO.getPassword());
	  			*/
	  			
	  			DataSource ds = null;
	  			try{
	  				ds = setupDataSource(dbPropertiesDTO.getDriverClassName(),dbPropertiesDTO.getUrl(),dbPropertiesDTO.getUserName(),
	  						dbPropertiesDTO.getPassword(),5,30);
	  		    }
	  		    catch(Exception e){
	  		    	e.printStackTrace();
	  		    	break;
	  		    }	
	  		    System.out.println("############ Pooling DataSource: " + dbPropertiesDTO.getDatabaseName());
	  			
	  		    map.put(dbPropertiesDTO.getDatabaseName(), ds);
	  			if (i == 1) {
	  				setDefaultTargetDataSource(ds); 
	  				setDefaultDatabaseName(dbPropertiesDTO.getDatabaseName());
	  				System.out.println("############ default database: " + dbPropertiesDTO.getDatabaseName());
	  			}
			} 
	  	 } catch (Exception e) {
	  		 e.printStackTrace();
	  		 logger.error(e);
	  	 }
	  	 
	  	 System.out.println("############ Charset.defaultCharset(): " + Charset.defaultCharset());
	  	
	  	 logger.info("exit RoutingDataSource.setProperties()");  	 
    }

    public String getSelectedDataSource() {
	    return selectedDataSource;
    }
   
    public void setSelectedDataSource(String selectedDataSource) {
	    this.selectedDataSource = selectedDataSource;
    }
   
    public Connection CreateSQLiteConnection(){
	    Connection conn = connectionPool.getConnection();
	    return conn;
    }
  
    public void CloseSQLiteConnection(Connection conn){
	    connectionPool.freeConnection(conn);
    }
   
    public Connection RenewSQLiteConnection(Connection conn){
	    return connectionPool.renewConnection(conn);
    }

	public String getDefaultDatabaseName() {
		return defaultDatabaseName;
	}
	
	public void setDefaultDatabaseName(String defaultDatabaseName) {
		this.defaultDatabaseName = defaultDatabaseName;
	}
	
	public String getSecurityDbFile() {
		return securityDbFile;
	}
	
	public void setSecurityDbFile(String securityDbFile) {
		this.securityDbFile = securityDbFile;
	}
	
	public void connectionValidation(){
	    connectionPool.connectionValidation();
    }

	private DataSource setupDataSourceXX(String driverClassName, String connectURI, String username, String password, 
			int minIdle, int maxActive) throws Exception{
		Class.forName(driverClassName);
	
	    // Create a ObjectPool that serves as the actual pool of connections.
	    GenericObjectPool connectionPool = new GenericObjectPool(null);
	    connectionPool.setMinIdle(minIdle);
	    connectionPool.setMaxActive(maxActive);
	
	    // Create a ConnectionFactory that the pool will use to create Connections.
	    ConnectionFactory connectionFactory = new DriverManagerConnectionFactory(connectURI,username, password);
	
	    PoolableConnectionFactory poolableConnectionFactory =
	        new PoolableConnectionFactory(connectionFactory,connectionPool,null,null,false,true);
	
	    PoolingDataSource dataSource = new PoolingDataSource(connectionPool);
	
	    return dataSource;
	}
	
	private DataSource setupDataSource(String driverClassName, String connectURI, String username, String password, 
			int minIdle, int maxActive) throws Exception{
	
		BasicDataSource dataSource = new BasicDataSource();
		dataSource.setDriverClassName(driverClassName);
		dataSource.setUsername(username);
		dataSource.setPassword(password);
		dataSource.setUrl(connectURI);
		dataSource.setMaxWait(1000 * 10);
		dataSource.setMinIdle(minIdle);
		dataSource.setMaxActive(maxActive);

		dataSource.setMinEvictableIdleTimeMillis(1000 * 60 * 30);          
       dataSource.setTimeBetweenEvictionRunsMillis(1000 * 60 * 30);         
       dataSource.setNumTestsPerEvictionRun(3);          
       dataSource.setTestOnBorrow(true);   
       dataSource.setRemoveAbandoned(true);
       dataSource.setValidationQuery("SELECT 1 FROM DUAL");

	    return dataSource;
	}

	/** 
	* Returns the root logger to meet Java 1.7 DataSource interface 
	* specifications. 
	*/ 
	public Logger getParentLogger(){ 
		return Logger.getLogger(Logger.GLOBAL_LOGGER_NAME); 
	} 

	public static void main(String[] args){
		String url = "jdbc:postgresql://127.0.0.1:5432/postgres;postgres;admin";
		
		String[] tokens = url.split(";");
		if(tokens.length==3){
			String connectionURL = tokens[0];
			String connectionUserName = tokens[1];
			String connectionPassword = tokens[2];
			
			System.out.println("connectionURL : " + connectionURL);
			System.out.println("connectionUserName : " + connectionUserName);
			System.out.println("connectionPassword : " + connectionPassword);
		}
		else{
			return;
		}

	}
	
	public static String getColumnHeader(java.sql.Connection con) throws SQLException {
		if (con.getMetaData().getDatabaseProductName().equalsIgnoreCase("Oracle")){
			return "USER_";
		}
		else{
			return "";
		}
	}
}

