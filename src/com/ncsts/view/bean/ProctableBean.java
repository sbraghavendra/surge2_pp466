package com.ncsts.view.bean;

import java.lang.reflect.Method;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.ajax4jsf.component.html.HtmlAjaxSupport;
import org.apache.log4j.Logger;
import org.richfaces.component.html.HtmlDataTable;
import org.richfaces.model.selection.SimpleSelection;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.ncsts.common.CacheManager;
import com.ncsts.common.Util;
import com.ncsts.domain.DataDefinitionColumn;
import com.ncsts.domain.Proctable;
import com.ncsts.domain.ProctableDetail;
import com.ncsts.domain.User;
import com.ncsts.jsf.model.ProctableDataModel;
import com.ncsts.services.ProctableService;
import com.ncsts.view.util.TogglePanelController;

public class ProctableBean {	
	private Logger logger = Logger.getLogger(ProctableBean.class);
	
	@Autowired
	private loginBean loginBean;
	
	@Autowired
	ProcruleBean procruleBean;
	
	private MatrixCommonBean matrixCommonBean;

	private Proctable filterProctable = new Proctable();
	private Proctable selProctable = new Proctable();
	private ProctableDetail selProcDetailtable = new ProctableDetail();
	private Proctable updateProctable = new Proctable();
	
	private CacheManager cacheManager;
	private ProctableService proctableService;
	private ProctableDataModel proctableDataModel;

	private SimpleSelection selection = new SimpleSelection();
	private int selectedProctableRowIndex = -1;
	private int selectedProctableDetailRowIndex = -1;
	private EditAction currentAction = EditAction.VIEW;
	private TogglePanelController togglePanelController = null;
	
	private List<SelectItem> filterActiveTablesMenuItems;
	private List<SelectItem> ruleTypeMenuItems;
	private List<InColumnObject> inColumnObjectItems= new ArrayList<InColumnObject>();
	private List<OutColumnObject> outColumnObjectItems= new ArrayList<OutColumnObject>();
	private List<SelectItem> operatorItems;
	private List<SelectItem> operationGroupItems;
	private List<SelectItem> operationGroupSalesItems;
	
	private List<SelectItem> allTransactionItems;
	private List<SelectItem> allSaleTransItems;
	
	private List<SelectItem> valuelocationInColumnItems;
	private List<SelectItem> valuelocationInColumnSalesItems;
	
	private List<SelectItem> valuelocationOutColumnItems;
	private List<SelectItem> valuePopulateItems;
	private List<SelectItem> valuePopulateSalesItems;
	
	private List<SelectItem> relationItems;
	private List<SelectItem> basicTranbsactionItems;
	private List<SelectItem> customerTranbsactionItems;
	private List<SelectItem> invoiceTranbsactionItems;
	private List<SelectItem> locationTranbsactionItems;
	private List<SelectItem> entityTranbsactionItems;
	private List<SelectItem> userTranbsactionItems;
	private List<Proctable> reorderProctableArray= new ArrayList<Proctable>();
	
	private List<PopulateObject> inoutcolumnsList = null;
	private List<PopulateObject> incolumnsList = null;
	private List<PopulateObject> outcolumnsList = null;
	private int entriesPageSize = 50;
	private int entriesPageNumber; 
	
	public int getEntriesPageSize() {
		return entriesPageSize;
	}

	public void setEntriesPageSize(int entriesPageSize) {
		this.entriesPageSize = entriesPageSize;
	}

	public int getEntriesPageNumber() {
		return entriesPageNumber;
	}

	public void setEntriesPageNumber(int entriesPageNumber) {
		this.entriesPageNumber = entriesPageNumber;
	}

	public List<PopulateObject> getIncolumnsList() {
		return incolumnsList;
	}

	public void setIncolumnsList(List<PopulateObject> incolumnsList) {
		this.incolumnsList = incolumnsList;
	}

	public List<PopulateObject> getOutcolumnsList() {
		return outcolumnsList;
	}

	public void setOutcolumnsList(List<PopulateObject> outcolumnsList) {
		this.outcolumnsList = outcolumnsList;
	}

	public List<PopulateObject> getInoutcolumnsList() {
		return inoutcolumnsList;
	}

	public void setInoutcolumnsList(List<PopulateObject> inoutcolumnsList) {
		this.inoutcolumnsList = inoutcolumnsList;
	}

	public  TogglePanelController getTogglePanelController(){
		if(togglePanelController==null){
			togglePanelController = new TogglePanelController();
			togglePanelController.addTogglePanel("ProctablePanel", true);
		}
		return togglePanelController;
	}
	
	public void setTogglePanelController(TogglePanelController togglePanelController){
		this.togglePanelController = togglePanelController;;
	}
	
	public Proctable getSelProctable(){
		return selProctable;
	}
	
	public Proctable getUpdateProctable(){
		return updateProctable;
	}
	
	public MatrixCommonBean getMatrixCommonBean() {
		return matrixCommonBean;
	}

	public void setMatrixCommonBean(MatrixCommonBean matrixCommonBean) {
		this.matrixCommonBean = matrixCommonBean;
	}
	
	public List<SelectItem> getFilterActiveTablesMenuItems() {
		if(filterActiveTablesMenuItems==null){
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
	    	selectItems.add(new SelectItem("","All Tables"));
	    	selectItems.add(new SelectItem("1","Active Only"));
	    	selectItems.add(new SelectItem("0","Inactive Only"));

	    	filterActiveTablesMenuItems = selectItems;
	    	
			filterProctable.setActiveFlag("");
		}
 
		return filterActiveTablesMenuItems;
	}
	
	public List<SelectItem> getRuleTypeMenuItems() {
		if(ruleTypeMenuItems==null){
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
	    	selectItems.add(new SelectItem("","Select a Rule Type"));
	    	selectItems.add(new SelectItem("PRE","Pre-Process"));
	    	selectItems.add(new SelectItem("POST","Post-Process"));

	    	ruleTypeMenuItems = selectItems;
		}
 
		return ruleTypeMenuItems;
	}

	public ProctableService getProctableService() {
		return proctableService;
	}

	public void setProctableService(ProctableService proctableService) {
		this.proctableService = proctableService;
	}
	
	public SimpleSelection getSelection() {
		return selection;
	}
	
	public void setSelection(SimpleSelection selection) {
		this.selection = selection;
	}
	
	public void setProctableDataModel(ProctableDataModel proctableDataModel) {
		this.proctableDataModel = proctableDataModel;
	}
	
	public ProctableDataModel getProctableDataModel() {
		return proctableDataModel;
	}
	
	public int getSelectedProctableRowIndex() {
		return selectedProctableRowIndex;
	}
	
	private boolean currentPurchasingSelected = false;
	
	public String navigateAction() {
		if(getIsPurchasingMenu() != currentPurchasingSelected){
			currentPurchasingSelected = getIsPurchasingMenu();
			
			//Need refresh
			resetFilterSearchAction();
			retrieveFilterProctable();
		}

		return "proctable_main";
	}
	
	public boolean getIsPurchasingMenu(){
		return loginBean.getIsPurchasingSelected();
	}
	
	public String getCurrentModuleMenu(){
		if(getIsPurchasingMenu()){
			return "PURCHUSE";
		}
		else{
			return "BILLSALE";
		}
	}
	
	public void retrieveFilterProctable() {
		selectedProctableRowIndex = -1;
		selectedProctableDetails = null;
		
		selProctable = null;
		
		Proctable aProctable = new Proctable();
		aProctable.setModuleCode(getCurrentModuleMenu());
		
		//Proctable filter
		if(filterProctable.getProctableName()!=null && filterProctable.getProctableName().trim().length()>0){
			aProctable.setProctableName(filterProctable.getProctableName().trim());
		}
		if(filterProctable.getDescription()!=null && filterProctable.getDescription().trim().length()>0){
			aProctable.setDescription(filterProctable.getDescription().trim());
		}
		if(filterProctable.getActiveFlag()!=null && filterProctable.getActiveFlag().length()>0){
			if(filterProctable.getActiveFlag().equals("1")){
				aProctable.setActiveFlag("1");
			}
			else{
				aProctable.setActiveFlag("0");
			}
		}
		
		proctableDataModel.setCriteria(aProctable);
	}
	
	public void retrieveFilterProctableDetail() {
		selectedProctableDetails = null;
		
		if(selProctable==null || selProctable.getProctableId()==null || (selectedProctableRowIndex == -1)){
			return; //Not selected
		}
		
		selectedProctableDetails = proctableService.findProctableDetail(selProctable.getProctableId());
	}
	
	List<ProctableDetail> selectedProctableDetails = null;
	
	public List<ProctableDetail> getSelectedProctableDetails() {
		return selectedProctableDetails;
	}

	public void selectedProctableRowChanged(ActionEvent e) throws AbortProcessingException {
		HtmlDataTable table = (HtmlDataTable) ((HtmlAjaxSupport) e.getComponent()).getParent(); 
		setSelectedProctableRowIndex(table.getRowIndex());

		selProctable = new Proctable();
		selProctable = (Proctable)table.getRowData();
		
		retrieveFilterProctableDetail();
		entriesPageNumber = 1; 
	}
	
	public void selectedProcDetailtableRowChanged(ActionEvent e) throws AbortProcessingException {
		HtmlDataTable table = (HtmlDataTable) ((HtmlAjaxSupport) e.getComponent()).getParent(); 
		setSelectedProctableDetailRowIndex(table.getRowIndex());
		selProcDetailtable = (ProctableDetail)table.getRowData();
	}
	
	public boolean getIsSelectedProctableDetailsExist(){

		if(selectedProctableRowIndex == -1){
			return false; //Not selected
		}
		
		if(selectedProctableDetails==null || selectedProctableDetails.size()==0){
			return false;
		}
		else{
			return true;
		}
	}
	
	public String getActionText() {
		if(currentAction.equals(EditAction.ADD)){
			return "Add ";
		}
		else if(currentAction.equals(EditAction.UPDATE)){
			return "Update";
		}
		else if(currentAction.equals(EditAction.DELETE)){
			return "Delete";
		}
		else if(currentAction.equals(EditAction.VIEW)){
			return "View";
		}

		return "";
	}
	
	public Boolean getDisplayCopyAddAction() {
		return (currentAction == EditAction.COPY_ADD);
	}
	
	public Boolean getDisplayAddAction() {
		return (currentAction == EditAction.ADD);
	}
	
	public Boolean getDisplayDeleteAction() {
		return (currentAction == EditAction.DELETE);
	}
	
	public Boolean getDisplayUpdateAction() {
		return (currentAction == EditAction.UPDATE);
	}
	
	public Boolean getDisplayViewAction() {
		return (currentAction == EditAction.VIEW);
	}
	
	public String displayAddProctableAction() {
		this.currentAction = EditAction.ADD;
		updateChangedArrayList= new ArrayList<ArrayList>();

		updateProctable= new Proctable();		
		updateProctable.setActiveBooleanFlag(true);
		updateProctable.setModuleCode(getCurrentModuleMenu());

		
		//For In column 
		inColumnObjectItems= new ArrayList<InColumnObject>();
		InColumnObject aInColumnObject = new InColumnObject();
		aInColumnObject.setOperation("ALL");
		inColumnObjectItems.add(aInColumnObject);
		
		//For Out column
		outColumnObjectItems= new ArrayList<OutColumnObject>();
		OutColumnObject aOutColumnObject = new OutColumnObject();
		outColumnObjectItems.add(aOutColumnObject);

		return "proctable_update";
	}
	
	public String updateProctabledetailAction() {
		this.currentAction = EditAction.UPDATE;
		updateChangedArrayList= new ArrayList<ArrayList>();

		updateProctable.setActiveBooleanFlag(true);
		updateProctable.setModuleCode(getCurrentModuleMenu());
		
		updateProctable= new Proctable();		
		if (selProctable != null) {
			BeanUtils.copyProperties(selProctable, updateProctable);
		}
		getProctabledetailData(updateProctable.getProctableId());
		return "proctabledetail_update";
	}
	
	private ArrayList updateChangedArrayList= new ArrayList<ArrayList>();
	private ArrayList populateObjectArrayList= new ArrayList<ArrayList>();
	public List<ArrayList> getPopulateObjectArrayList() {
	 	return populateObjectArrayList;
	}
	
	public String displayPopulateProctableFromUpdateAction() {
		//Should be EditAction.ADD or EditAction.UPDATE;
		
		//Should save updateProctable & delete all associated ProctableDetails first
		if(this.currentAction.equals(EditAction.ADD)){
			if(addProcessTableAction().length()>0){
			
				//Retrieve Proctable object
				updateProctable = this.proctableService.findById(updateProctable.getProctableId());
				reloadPopulateData(updateProctable.getProctableId());
				
				return "proctable_populate";
			}
		}
		else if(this.currentAction.equals(EditAction.UPDATE)){
			if(updateProcessTableAction().length()>0){
				reloadPopulateData(updateProctable.getProctableId());
				
				return "proctable_populate";
			}

			return "proctable_populate";
		}
		
		return "";
	}
	
	public String displayPopulateProctableFromMainAction() {
		this.currentAction = EditAction.POPULATE;
		
		if(selProctable==null){
			return "proctable_main";
		}
		
		updateProctable= new Proctable();		
		if (selProctable != null) {
			BeanUtils.copyProperties(selProctable, updateProctable);
		}
		
		reloadPopulateData(updateProctable.getProctableId());
		
		return "proctable_populate";
	}
	
	public String okPopulateTableAction() {
		String result = null;
		try {			
			if(validateProctableDetailLine()){
				return "";
			}
			
			List<ProctableDetail> aProctableDetailList = buildPopulateProctableDetail();
			this.proctableService.populateProcruleDetailList(updateProctable, aProctableDetailList);

			//Refresh & Retrieve
			retrieveFilterProctable();
			
		    result = "proctable_main";		
		} catch (Exception ex) {
			ex.printStackTrace();
			result = "";
		}
		   
        return result;
	}
	
	boolean validateProctableDetailLine(){
		boolean errorMessage = false;
		
		List<PopulateObject> populateObjectItems = null;
		PopulateObject aPopulateObject = null;
		for(int jj=0; jj<populateObjectArrayList.size(); jj++){
			populateObjectItems = (List<PopulateObject>)populateObjectArrayList.get(jj);

			int inCount = 0;
			int outCount = 0;
			boolean errorLine = false;
			for(int i=0; i<populateObjectItems.size(); i++){
				aPopulateObject = (PopulateObject) populateObjectItems.get(i);
				
				try {
					if(aPopulateObject.getIsTextSearchType()){
						inCount++;
						if(aPopulateObject.getTextSearchName()==null || aPopulateObject.getTextSearchName().length()==0){
							errorLine = true;
						}
					}
					else if(aPopulateObject.getIsColumnSearchType()){
						inCount++;
						if(aPopulateObject.getColumnSearchName()==null || aPopulateObject.getColumnSearchName().length()==0){
							errorLine = true;	
						}
					}
					else if(aPopulateObject.getIsComboSearchType()){
						outCount++;
						if(aPopulateObject.getOperation()!=null && aPopulateObject.getOperation().equalsIgnoreCase("VALUE")){
							if(aPopulateObject.getFieldText()==null || aPopulateObject.getFieldText().length()==0){
								errorLine = true;	
							}
						}
						else if(aPopulateObject.getOperation()!=null && aPopulateObject.getOperation().equalsIgnoreCase("BLANK")){
							//Do nothing
						}
						else{
							//Column name
							if(aPopulateObject.getFieldColumn()==null || aPopulateObject.getFieldColumn().length()==0){
								errorLine = true;	
							}
						}
					}
					
				}
				catch(Exception e){	
					e.printStackTrace();
				}	
			}
			
			if(errorLine){
				errorMessage = true;
				FacesMessage message = new FacesMessage("Line " + (jj+1) + ": columns and result cannot be empty.");
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage(null, message);
			}
		}
	
		return errorMessage;
	}  
	
	
	boolean validateEntries(){
		boolean errorMessage = false;
		boolean errorLine = false;
		PopulateObject aPopulateObject = null;
		for(int i=0; i<inoutcolumnsList.size(); i++){
			aPopulateObject = (PopulateObject) inoutcolumnsList.get(i);
			
			try {
				if(aPopulateObject.getIsTextSearchType()){
					if(aPopulateObject.getTextSearchName()==null || aPopulateObject.getTextSearchName().length()==0){
						errorLine = true;
					}
				}
				else if(aPopulateObject.getIsColumnSearchType()){
					if(aPopulateObject.getColumnSearchName()==null || aPopulateObject.getColumnSearchName().length()==0){
						errorLine = true;	
					}
				}
				else if(aPopulateObject.getIsComboSearchType()){
					if(aPopulateObject.getOperation()!=null && aPopulateObject.getOperation().equalsIgnoreCase("VALUE")){
						if(aPopulateObject.getFieldText()==null || aPopulateObject.getFieldText().length()==0){
							errorLine = true;	
						}
					}
					else if(aPopulateObject.getOperation()!=null && aPopulateObject.getOperation().equalsIgnoreCase("BLANK")){
						//Do nothing
					}
					else{
						//Column name
						if(aPopulateObject.getFieldColumn()==null || aPopulateObject.getFieldColumn().length()==0){
							errorLine = true;	
						}
					}
				}
				
			}
			catch(Exception e){	
				e.printStackTrace();
			}	
		}
		
		if(errorLine){
			errorMessage = true;
			FacesMessage message = new FacesMessage("columns and result cannot be empty.");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
		}
	
		return errorMessage;
	}
	
	private List<ProctableDetail> buildPopulateProctableDetail(){
		List<ProctableDetail> aProctableDetailList= new ArrayList<ProctableDetail>();
		ProctableDetail aProctableDetail = null;
		List<PopulateObject> populateObjectItems = null;
		for(int jj=0; jj<populateObjectArrayList.size(); jj++){
			populateObjectItems = (List<PopulateObject>)populateObjectArrayList.get(jj);
			aProctableDetail = new ProctableDetail();
			aProctableDetail =  setProcDetailData(aProctableDetail, populateObjectItems);
			aProctableDetailList.add(aProctableDetail);
		}
		return aProctableDetailList;	
	}  
 	
	private ProctableDetail buildProctableDetail(){
		ProctableDetail aProctableDetail = null;
		if(this.currentAction.equals(EditAction.ADD)) {
			aProctableDetail = new ProctableDetail();
		}
		//for update
		if(this.currentAction.equals(EditAction.UPDATE)) {
			aProctableDetail = this.proctableService.findProctableDetailById(selProcDetailtable.getProctableDetailId());
		}
		aProctableDetail = setProcDetailData(aProctableDetail,inoutcolumnsList);
		return aProctableDetail;	
	}
	
	private ProctableDetail setProcDetailData(ProctableDetail aProctableDetail, List<PopulateObject> columnsList) {
		PopulateObject aPopulateObject = null;
		Class<?> cls = ProctableDetail.class;
		NumberFormat formatter = new DecimalFormat("00");
		String methodName = null;
		Method m = null;
		
		int inCount = 0;
		int outCount = 0;
		for(int i=0; i<columnsList.size(); i++){
			aPopulateObject = (PopulateObject) columnsList.get(i);
			
			try {
				if(aPopulateObject.getIsTextSearchType()){
					inCount++;
					methodName = "setInColumn" + formatter.format(inCount); 
			        m = cls.getMethod(methodName, new Class[] {String.class});
					if (m != null) {
			            m.invoke(aProctableDetail, new Object[] {new String(aPopulateObject.getTextSearchName())});//For TEXT
					}
				}
				else if(aPopulateObject.getIsColumnSearchType()){
					inCount++;
					methodName = "setInColumn" + formatter.format(inCount); 
			        m = cls.getMethod(methodName, new Class[] {String.class});
					if (m != null) {
			            m.invoke(aProctableDetail, new Object[] {new String(aPopulateObject.getColumnSearchName())});//For TEXT
					}
				}
				else{
					//Must be combo search
					if(aPopulateObject.getOperation().equalsIgnoreCase("VALUE")){
						outCount++;
						methodName = "setOutColumnName" + formatter.format(outCount); 
				        m = cls.getMethod(methodName, new Class[] {String.class});
						if (m != null) {
				            m.invoke(aProctableDetail, new Object[] {new String(aPopulateObject.getFieldText())});//For TEXT
						}
						
						methodName = "setOutColumnType" + formatter.format(outCount); 
				        m = cls.getMethod(methodName, new Class[] {String.class});
						if (m != null) {
				            m.invoke(aProctableDetail, new Object[] {new String("V")});//For TEXT
						}

					}
					else if(aPopulateObject.getOperation().equalsIgnoreCase("BLANK")){
						outCount++;
						methodName = "setOutColumnName" + formatter.format(outCount); 
				        m = cls.getMethod(methodName, new Class[] {String.class});
						if (m != null) {
				            m.invoke(aProctableDetail, new Object[] {new String("")});//For TEXT
						}
						
						methodName = "setOutColumnType" + formatter.format(outCount); 
				        m = cls.getMethod(methodName, new Class[] {String.class});
						if (m != null) {
				            m.invoke(aProctableDetail, new Object[] {new String("V")});//For TEXT
						}

					}
					else{
						outCount++;
						methodName = "setOutColumnName" + formatter.format(outCount); 
				        m = cls.getMethod(methodName, new Class[] {String.class});
						if (m != null) {
				            m.invoke(aProctableDetail, new Object[] {new String(aPopulateObject.getFieldColumn())});//For COLUMN
						}
						
						methodName = "setOutColumnType" + formatter.format(outCount); 
				        m = cls.getMethod(methodName, new Class[] {String.class});
						if (m != null) {
				            m.invoke(aProctableDetail, new Object[] {new String("F")});//For TEXT
						}
					}
				}
			}
			catch(Exception e){	
				e.printStackTrace();
			}	
		}
		return aProctableDetail;
	}
	
	private ArrayList<PopulateObject> createPopulateObjectItems(){
		ArrayList<PopulateObject> populateObjectItems= new ArrayList<PopulateObject>();
		InColumnObject aInColumnObject = null;
		OutColumnObject aOutColumnObject = null;
		PopulateObject aPopulateObject = null;
		incolumnsList = new ArrayList<PopulateObject>();
		outcolumnsList = new ArrayList<PopulateObject>();
		for(int i=0; i< inColumnObjectItems.size(); i++){
			aInColumnObject = inColumnObjectItems.get(i);		
			aPopulateObject = new PopulateObject();
			if(aInColumnObject.getOperation().equalsIgnoreCase("TEXT")){
				aPopulateObject.setSearchType("TEXT");
			}
			else{
				aPopulateObject.setSearchType("COLUMN");
			}	
			aPopulateObject.setColumnheading(aInColumnObject.getColumnName()); 
			incolumnsList.add(aPopulateObject);
			populateObjectItems.add(aPopulateObject);
		}
		
		for(int i=0; i< outColumnObjectItems.size(); i++){
			aOutColumnObject = outColumnObjectItems.get(i);	
			aPopulateObject = new PopulateObject();
			aPopulateObject.setSearchType("COMBO");
			aPopulateObject.setColumnheading(aOutColumnObject.getColumnName());
			outcolumnsList.add(aPopulateObject);
			populateObjectItems.add(aPopulateObject);
		}	
		
		return populateObjectItems;
	}
	
	private void reloadPopulateData(Long proctableId){
		
		if(!buildInOutColumnObjects(proctableId)){
			return; 
		}
	
		if(inColumnObjectItems==null || inColumnObjectItems.size()==0 || outColumnObjectItems==null || outColumnObjectItems.size()==0){
			return; 
		}
		
		List<ProctableDetail> proctableDetailList = this.proctableService.findProctableDetail(proctableId);
		populateObjectArrayList= new ArrayList<ArrayList>();	
		List<PopulateObject> populateObjectItems = null;
		if(proctableDetailList==null || proctableDetailList.size()==0){
			//Add first new line
			populateObjectItems = createPopulateObjectItems();
			populateObjectArrayList.add(populateObjectItems);
		}
		else{
			ProctableDetail  aProctableDetail = null;
			for(int jj=0; jj< proctableDetailList.size(); jj++){
				aProctableDetail = proctableDetailList.get(jj);
			
				//Reload line
				populateObjectItems= new ArrayList<PopulateObject>();
				populateObjectArrayList.add(populateObjectItems);
				createProctableDetail(aProctableDetail, populateObjectItems, null, null);
			}  
		}
		
	}
	
	public String displayDeleteProctableAction() {
		displayUpdateProctableAction();
		this.currentAction = EditAction.DELETE;
		
		return "proctable_update";
	}
	
	public String displayViewProctableAction(){
		displayUpdateProctableAction();
		this.currentAction = EditAction.VIEW;
		
		return "proctable_update";
	}
	
	private boolean buildInOutColumnObjects(Long proctableId){
		Proctable aProctable = this.proctableService.findById(proctableId);
		if(aProctable==null || aProctable.getProctableId()==null){
			return false; //Not selected
		}
		
		//For when 
		inColumnObjectItems= new ArrayList<InColumnObject>();
		InColumnObject aInColumnObject = null;

		//For Set
		outColumnObjectItems= new ArrayList<OutColumnObject>();
		OutColumnObject aOutColumnObject = null;
		
		Class<?> cls = Proctable.class;
		NumberFormat formatter = new DecimalFormat("00");
		String methodName = null;
		Method m = null;
		
		try {
			for(int i=0; i<20; i++){
				aInColumnObject = new InColumnObject();

				methodName = "getInColumnType" + formatter.format(i+1); 
				m = cls.getMethod(methodName, new Class<?> [] {});
				if (m != null) {
					Object val = m.invoke(aProctable, new Object [] {});
					if (val != null) {
						String columnType = (String)val;
						if(columnType.equalsIgnoreCase("V")){
							aInColumnObject.setOperation("TEXT");
						}
						else{//"F" for column
							aInColumnObject.setOperation("ALL");
						}
					}
					else{
						break;
					}
				}
				
				methodName = "getInColumnName" + formatter.format(i+1); 
				m = cls.getMethod(methodName, new Class<?> [] {});
				if (m != null) {
		           // m.invoke(aProctable, new Object[] {new String(aInColumnObject.getFieldText())});
					Object val = m.invoke(aProctable, new Object [] {});
					if (val != null) {
						String columnName = (String)val;
						if(aInColumnObject.getOperation().equalsIgnoreCase("TEXT")){
							aInColumnObject.setFieldText(columnName);
						}
						else{//"F" for column
							aInColumnObject.setFieldColumn(columnName);
						}
					aInColumnObject.setColumnName(columnName);
					}
					else{
						break;
					}
				}
				
				inColumnObjectItems.add(aInColumnObject);		
			}
			
			for(int i=0; i<10; i++){
				aOutColumnObject = new OutColumnObject();

				methodName = "getOutColumnType" + formatter.format(i+1); 
				m = cls.getMethod(methodName, new Class<?> [] {});
				if (m != null) {
					Object val = m.invoke(aProctable, new Object [] {});
					if (val != null) {
						String columnType = (String)val;
						if(columnType.equalsIgnoreCase("V")){
							aOutColumnObject.setOperation("TEXT");
						}
						else if(columnType.equalsIgnoreCase("N")){
							aOutColumnObject.setOperation("NUMBER");
						}
						else if(columnType.equalsIgnoreCase("D")){
							aOutColumnObject.setOperation("DATE");
						}
					}
					else{
						break;
					}
				}
				
				methodName = "getOutColumnName" + formatter.format(i+1); 
				m = cls.getMethod(methodName, new Class<?> [] {});
				if (m != null) {
					Object val = m.invoke(aProctable, new Object [] {});
					if (val != null) {
						String columnName = (String)val;
						aOutColumnObject.setFieldText(columnName);
						aOutColumnObject.setColumnName(columnName);
					}
					else{
						break;
					}
				}
				
				outColumnObjectItems.add(aOutColumnObject);		
			}

		} catch (Exception ex) {
			ex.printStackTrace();
			return false;
		}
		
		return true;
	}
	
	public String displayUpdateProctableAction() {
		this.currentAction = EditAction.UPDATE;
		updateChangedArrayList= new ArrayList<ArrayList>();
		
		if(selProctable==null){
			return "proctable_main";
		}
		
		updateProctable= new Proctable();		
		if (selProctable != null) {
			BeanUtils.copyProperties(selProctable, updateProctable);
		}
		
		if(!buildInOutColumnObjects(selProctable.getProctableId())){
			return ""; 
		}
	
		return "proctable_update";
	}
	
	public String okProcessTableAction() {
		String result = null;
		switch (currentAction) {
			case DELETE:
				result = deleteProcessTableAction();
				break;
				
			case UPDATE:
				result = updateProcessTableAction();
				break;
				
			case REORDER:
				result = reorderProcessRuleAction();
				break;
				
			case ADD:
				result = addProcessTableAction();
				break;
				
			case VIEW:
				result = "proctable_main";
				break;
				
			default:
				result = cancelAction();
				break;
		}
		return result;
	}
	
	public String okProcdetailTableAction() {
		String result = null;
		switch (currentAction) {
			case DELETE:
				result = deleteProcdetailTableAction();
				break;
				
			case UPDATE:
				result = addorUpdateProcdetailTableAction();
				break;
				
			case ADD:
				result = addorUpdateProcdetailTableAction();
				break;
				
			case VIEW:
				result = "proctable_main";
				break;
				
			default:
				result = cancelAction();
				break;
		}
		return result;
	}
	
	public String okAddProcdetailTableAction() {
		String result = addorUpdateProcdetailTableAction();
		if(result==null || result.trim().length()==0){
			return result;
		}
		
		return addProctabledetailAction();
	}
	
	private List<ProctableDetail> buildUpdateProctableDetail(){
		List<ProctableDetail> aProctableEntityList= new ArrayList<ProctableDetail>();
		
		ProctableDetail aProctableDetail = null;
		int lineNo = 1;
		if(inColumnObjectItems!=null && inColumnObjectItems.size()>0){
			aProctableDetail = new ProctableDetail();
			aProctableEntityList.add(aProctableDetail);
			
			InColumnObject aInColumnObject = null;
			for(int i=0; i<inColumnObjectItems.size(); i++){
				aInColumnObject = (InColumnObject)inColumnObjectItems.get(i);
				aProctableDetail = new ProctableDetail();
				aProctableEntityList.add(aProctableDetail);
			}
		}	
		
		if(outColumnObjectItems!=null && outColumnObjectItems.size()>0){
			aProctableDetail = new ProctableDetail();
			aProctableEntityList.add(aProctableDetail);

			OutColumnObject aOutColumnObject = null;
			for(int i=0; i<outColumnObjectItems.size(); i++){
				aOutColumnObject = (OutColumnObject)outColumnObjectItems.get(i);
				aProctableDetail = new ProctableDetail();
				aProctableEntityList.add(aProctableDetail);
			}
		}
		
		return aProctableEntityList;
		
	}
	
	boolean validateTable(){
		boolean errorMessage = false;
		
		//Validate name, type
		if(updateProctable.getProctableName()==null || updateProctable.getProctableName().trim().length()==0){
			errorMessage = true;
			
			FacesMessage message = new FacesMessage("The Table Name is mandatory.");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
		}
		
		return errorMessage;
	}
	
	boolean validateInOutColumns(){
		boolean errorMessage = false;
		
		//Verify In column
		InColumnObject aInColumnObject = null;
		for(int i=0; i<inColumnObjectItems.size(); i++){
			aInColumnObject = (InColumnObject)inColumnObjectItems.get(i);		
			if((aInColumnObject.getFieldColumn()==null || aInColumnObject.getFieldColumn().length()==0) &&
					(aInColumnObject.getFieldText()==null || aInColumnObject.getFieldText().length()==0)){
				errorMessage = true;
				
				FacesMessage message = new FacesMessage("Input Column " + (i+1) + ": The Field/Value cannot be empty.");
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage(null, message);
			}
		}
		
		//Verify In column
		OutColumnObject aOutColumnObject = null;
		for(int i=0; i<outColumnObjectItems.size(); i++){
			aOutColumnObject = (OutColumnObject)outColumnObjectItems.get(i);		
			if(aOutColumnObject.getFieldText()==null || aOutColumnObject.getFieldText().length()==0){
				errorMessage = true;
				
				FacesMessage message = new FacesMessage("Output Column " + (i+1) + ": The Name cannot be empty.");
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage(null, message);
			}
		}
		
		return errorMessage;
	}
	
	private boolean fillInOutColumn(Proctable aProctable){
		boolean errorMessage = false;

		try {
			Class<?> cls = Proctable.class;
			NumberFormat formatter = new DecimalFormat("00");
	
			//Fill In column
			InColumnObject aInColumnObject = null;
			String methodName = null;
			Method m = null;
			for(int i=0; i<inColumnObjectItems.size(); i++){
				aInColumnObject = (InColumnObject)inColumnObjectItems.get(i);		
				if(aInColumnObject.getOperation().equalsIgnoreCase("TEXT")){
					methodName = "setInColumnType" + formatter.format(i+1); 
			        m = cls.getMethod(methodName, new Class[] {String.class});
					if (m != null) {
			            m.invoke(aProctable, new Object[] {new String("V")});//For TEXT
					}
					
					methodName = "setInColumnName" + formatter.format(i+1); 
			        m = cls.getMethod(methodName, new Class[] {String.class});
					if (m != null) {
			            m.invoke(aProctable, new Object[] {new String(aInColumnObject.getFieldText())});
					}
				}
				else{
					methodName = "setInColumnType" + formatter.format(i+1); 
			        m = cls.getMethod(methodName, new Class[] {String.class});
					if (m != null) {
			            m.invoke(aProctable, new Object[] {new String("F")}); //For column
					}
					
					methodName = "setInColumnName" + formatter.format(i+1); 
			        m = cls.getMethod(methodName, new Class[] {String.class});
					if (m != null) {
			            m.invoke(aProctable, new Object[] {new String(aInColumnObject.getFieldColumn())});
					}
				}
			}
			
			//Fill out column
			OutColumnObject aOutColumnObject = null;
			for(int i=0; i<outColumnObjectItems.size(); i++){
				aOutColumnObject = (OutColumnObject)outColumnObjectItems.get(i);		
				methodName = "setOutColumnType" + formatter.format(i+1); 
		        m = cls.getMethod(methodName, new Class[] {String.class});
				if (m != null) {
					if(aOutColumnObject.getOperation().equalsIgnoreCase("TEXT")){
						m.invoke(aProctable, new Object[] {new String("V")});//For TEXT
					}
					else if(aOutColumnObject.getOperation().equalsIgnoreCase("NUMBER")){
						m.invoke(aProctable, new Object[] {new String("N")});
					}
					else if(aOutColumnObject.getOperation().equalsIgnoreCase("DATE")){
						m.invoke(aProctable, new Object[] {new String("D")});
					}	            
				}
				
				methodName = "setOutColumnName" + formatter.format(i+1); 
		        m = cls.getMethod(methodName, new Class[] {String.class});
				if (m != null) {
		            m.invoke(aProctable, new Object[] {new String(aOutColumnObject.getFieldText())});
				}
			}
			
		} catch (Exception ex) {
			ex.printStackTrace();
			errorMessage = true;
			
			FacesMessage message = new FacesMessage("Update error: " + ex.getMessage());
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
		}
		   
        return errorMessage;
	}
	
	public String addProcessTableAction() {
		String result = null;
		try {
			if(validateTable()){
				return "";
			}
			
			if(validateInOutColumns()){
				return "";
			}
			
			//Update CustLocn
			Proctable aProctable = new Proctable();
			aProctable.setModuleCode(updateProctable.getModuleCode());
			aProctable.setProctableName(updateProctable.getProctableName());
			aProctable.setDescription(updateProctable.getDescription());
			aProctable.setActiveFlag(updateProctable.getActiveFlag());
			
			if(fillInOutColumn(aProctable)){
				return "";
			}

			this.proctableService.save(aProctable);
			
			//For Populate use if needed
			updateProctable.setProctableId(aProctable.getProctableId());

			//Refresh & Retrieve
			retrieveFilterProctable();
			
		    result = "proctable_main";		
		} catch (Exception ex) {
			ex.printStackTrace();
			result = "";
		}
		   
        return result;
	}
	
	public String updateProcessTableAction() {
		String result = null;
		try {
			if(validateTable()){
				return "";
			}
			
			if(validateInOutColumns()){
				return "";
			}
			
			//Update CustLocn
			Proctable aProctable = new Proctable();
			aProctable.setModuleCode(updateProctable.getModuleCode());
			aProctable.setProctableId(this.selProctable.getProctableId());//For update
			aProctable.setProctableName(updateProctable.getProctableName());
			aProctable.setDescription(updateProctable.getDescription());
			aProctable.setActiveFlag(updateProctable.getActiveFlag());
			
			if(fillInOutColumn(aProctable)){
				return "";
			}
			
			//Check if column has been changed
			if(updateChangedArrayList!=null && updateChangedArrayList.size()>0){
				//Column has been added, removed, changed.
				
				//Do Not change active flag
				//aProctable.setActiveBooleanFlag(false);
			}
			
			//findProctableDetail
			proctableService.updateProctableDetail(aProctable, updateChangedArrayList);

			//Refresh & Retrieve
			retrieveFilterProctable();
			
		    result = "proctable_main";		
		} catch (Exception ex) {
			ex.printStackTrace();
			result = "";
		}
		   
        return result;
	}
	
	public Boolean getIsReorderProcessRuleEmpty() {
		if(reorderProctableArray==null || reorderProctableArray.size()==0){
	    	return true;
	    }
		else{
			return false;
		}	
	}
	
	public String reorderProcessRuleAction() {
		String result = null;
		try {
			if(reorderProctableArray==null || reorderProctableArray.size()==0){
		    	return null;//Impossible
		    }
		    
		    List<Proctable> updateProctableList= new ArrayList<Proctable>();
		    Proctable aProctable = null;
		    for(int i=0; i<reorderProctableArray.size(); i++ ){
		    	aProctable = (Proctable)reorderProctableArray.get(i);
		    	//ac 		    	aProctable.setOrderNo(new Long(i+1));
		    	
		    	updateProctableList.add(aProctable);
		    }

			this.proctableService.reorderProctableList(updateProctableList);

			//Refresh & Retrieve
			retrieveFilterProctable();
			
		    result = "proctable_main";		
		} catch (Exception ex) {
			ex.printStackTrace();
			result = "";
		}
		   
        return result;
	}
	
	public String deleteProcessTableAction() {
		String result = null;
		logger.info("start addAction"); 
		try {
			
			//Update CustLocn
			Proctable aProctable = new Proctable();
			aProctable.setModuleCode(updateProctable.getModuleCode());
			aProctable.setProctableId(this.selProctable.getProctableId());//For update
			aProctable.setProctableName(updateProctable.getProctableName());
			aProctable.setDescription(updateProctable.getDescription());
			aProctable.setActiveFlag(updateProctable.getActiveFlag());
		
			this.proctableService.deleteProctable(selProctable);
			//Refresh & Retrieve
			retrieveFilterProctable();
			selectedProctableRowIndex= -1;//reset the table row index
			return "proctable_main";	
        
		} catch (Exception ex) {
			ex.printStackTrace();
			result = "";
		}
		   
        return result;
	}
	
	public String cancelAction() {
		return "proctable_main";
	}
	
	public Boolean getIsViewAction() {
		return currentAction.equals(EditAction.VIEW);
	}
	public Boolean getIsUpdateAction() {
		return currentAction.equals(EditAction.UPDATE);
	}

	public Boolean getDisplayProctableButtons() {
		return (selectedProctableRowIndex!=-1);
	}
	
	public Boolean getDisplayProctabledetailButtons() {
		return (selectedProctableDetailRowIndex!=-1);
	}
	
	public Boolean getDisplayProctableDeleteButtons() {
		return (selectedProctableRowIndex!=-1);
	}
	
	private String isCustomerInformationOpen = "true";
	private String isLocationInformationOpen = "false";
	private Boolean isHideSearchPanel = false;
	
	public Boolean getIsHideSearchPanel() {
		return isHideSearchPanel;
	}
	
	public void toggleHideSearchPanel(ActionEvent event){
		isHideSearchPanel = !isHideSearchPanel;
	}
	
	public String toggleHideSearchPanel(){
		isHideSearchPanel = !isHideSearchPanel;
		return null;
	}
	
	public String getIsCustomerInformationOpen() {
		return isCustomerInformationOpen;
	}
	
	public void toggleCollapseAll(ActionEvent event) {
		isCustomerInformationOpen = "false";
		isLocationInformationOpen = "false";
	}
	
	public String getIsLocationInformationOpen() {
		return isLocationInformationOpen;
	}
	
	public void toggleExpandAll(ActionEvent event) {
		isCustomerInformationOpen = "true";
		isLocationInformationOpen = "true";
	}
	
	public void toggleCustomerInformation(ActionEvent event) {
		if(Boolean.valueOf(isCustomerInformationOpen)){
			isCustomerInformationOpen = "false";	
		}
		else{
			isCustomerInformationOpen = "true";
		}
	}
	
	public void toggleLocationInformation(ActionEvent event) {
		if(Boolean.valueOf(isLocationInformationOpen)){
			isLocationInformationOpen = "false";	
		}
		else{
			isLocationInformationOpen = "true";
		}
	}

	public void oncollapseCustomerInformation(ActionEvent e) throws AbortProcessingException {
		isCustomerInformationOpen = "false";
	}
	
	public void onexpandCustomerInformation(ActionEvent e) throws AbortProcessingException {
		isCustomerInformationOpen = "true";
	}
	
	public Boolean getDisplayCustLocnButtons() {
		return ((selectedProctableRowIndex != -1) && (selectedProctableDetailRowIndex != -1));
	}
	
	public String resetFilterSearchAction(){
		filterProctable = new Proctable();
			
		return null;
	}

	public CacheManager getCacheManager() {
		return this.cacheManager;
	}

	public void setCacheManager(CacheManager cacheManager) {
		this.cacheManager = cacheManager;
	}   

	public Proctable getFilterProctable() {
		return filterProctable;
	}

	public void setFilterProctable(Proctable filterProctable) {
		this.filterProctable = filterProctable;
	}

	public void setSelectedProctableRowIndex(int selectedProctableRowIndex) {
		this.selectedProctableRowIndex = selectedProctableRowIndex;
	}
	
	public int getSelectedProctableDetailRowIndex() {
		return selectedProctableDetailRowIndex;
	}

	public void setSelectedProctableDetailRowIndex(int selectedProctableDetailRowIndex) {
		this.selectedProctableDetailRowIndex = selectedProctableDetailRowIndex;
	}
	
	public void sortAction(ActionEvent e) {
		proctableDataModel.toggleSortOrder(e.getComponent().getParent().getId());
	}

	public List<Proctable> getReorderProctableArray() {
		 return reorderProctableArray;
	}
	
	public List<InColumnObject> getInColumnObjectItems() {
	 	return inColumnObjectItems;
	}
	
	public List<OutColumnObject> getOutColumnObjectItems() {
	 	return outColumnObjectItems;
	}
	
	public List<SelectItem> getAllSaleTransItems() {
		if(getIsPurchasingMenu()){
			if(allTransactionItems==null){
				
				Map<String,DataDefinitionColumn> propertyMap = cacheManager.getDataDefinitionPropertyMap("TB_TRANSACTION_DETAIL");
				List<SelectItem> selectItems = new ArrayList<SelectItem>();
		    	selectItems.add(new SelectItem("","Select a Field"));
		    	
				for (String transProperty : propertyMap.keySet()) {
					DataDefinitionColumn driverNames = propertyMap.get(transProperty);
					selectItems.add(new SelectItem(driverNames.getColumnName(),driverNames.getDescription()));
				}

				allTransactionItems = selectItems;
			}
			return allTransactionItems;
		}
		else{
			if(allSaleTransItems==null){
				
				Map<String,DataDefinitionColumn> propertyMap = cacheManager.getDataDefinitionPropertyMap("TB_SALETRANS");
				List<SelectItem> selectItems = new ArrayList<SelectItem>();
		    	selectItems.add(new SelectItem("","Select a Field"));
		    	
				for (String transProperty : propertyMap.keySet()) {
					DataDefinitionColumn driverNames = propertyMap.get(transProperty);
					selectItems.add(new SelectItem(driverNames.getColumnName(),driverNames.getDescription()));
				}

				allSaleTransItems = selectItems;
			}
			return allSaleTransItems;
		}
	}
	
	public List<SelectItem> getBasicTranbsactionItems() {
		if(basicTranbsactionItems==null){
			Map<String,DataDefinitionColumn> propertyMap = cacheManager.getDataDefinitionPropertyMap("TB_SALETRANS");
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
	    	selectItems.add(new SelectItem("","Select a Field"));

	    	String[] BASIC_COLUMNS = { 
	    			"TRANSACTION_TYPE","RATETYPE","METHOD_DELIVERY","GL_DATE","COMMENTS","CREDIT_IND","EXEMPT_IND","EXEMPT_REASON",
	    			"TAX_PAID_TO_VENDOR_FLAG","DO_NOT_CALC_FLAG","VENDOR_TAX_AMT","GEO_RECON_CODE","DISCOUNT_TYPE"
	    	};
	    	
	    	for(int i=0; i<BASIC_COLUMNS.length; i++){
	    		DataDefinitionColumn driverNames = propertyMap.get(Util.columnToProperty(BASIC_COLUMNS[i]));
	    		if(driverNames!=null){
	    			selectItems.add(new SelectItem(driverNames.getColumnName(),driverNames.getDescription()));
	    		}
	    	}

			basicTranbsactionItems = selectItems;
		}
		return basicTranbsactionItems;
	}
	
	public List<SelectItem> getCustomerTranbsactionItems() {
		if(customerTranbsactionItems==null){
			Map<String,DataDefinitionColumn> propertyMap = cacheManager.getDataDefinitionPropertyMap("TB_SALETRANS");
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
	    	selectItems.add(new SelectItem("","Select a Field"));

	    	String[] CUSTOMER_COLUMNS = { 
	    			"CUST_NBR","CUST_NAME","CUST_LOCN_CODE","LOCATION_NAME","CUST_ENTITY_ID","CUST_ENTITY_CODE","CUST_JURISDICTION_ID","CUST_GEOCODE",
					"CUST_ADDRESS_LINE_1","CUST_ADDRESS_LINE_2","CUST_CITY","CUST_COUNTY","CUST_STATE_CODE","CUST_ZIP","CUST_ZIPPLUS4","CUST_COUNTRY_CODE",
					"CERT_NBR","CERT_TYPE","CERT_SIGN_DATE","CERT_EXPIRATION_DATE","CERT_IMAGE"
	    	};
	    	
	    	for(int i=0; i<CUSTOMER_COLUMNS.length; i++){
	    		DataDefinitionColumn driverNames = propertyMap.get(Util.columnToProperty(CUSTOMER_COLUMNS[i]));
	    		if(driverNames!=null){
	    			selectItems.add(new SelectItem(driverNames.getColumnName(),driverNames.getDescription()));
	    		}
	    	}

			customerTranbsactionItems = selectItems;
		}
		return customerTranbsactionItems;
	}
	
	public List<SelectItem> getInvoiceTranbsactionItems() {
		if(invoiceTranbsactionItems==null){
			Map<String,DataDefinitionColumn> propertyMap = cacheManager.getDataDefinitionPropertyMap("TB_SALETRANS");
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
	    	selectItems.add(new SelectItem("","Select a Field"));

	    	String[] INVOICE_COLUMNS = { 
				"INVOICE_DATE","INVOICE_NBR","INVOICE_DESC","INVOICE_GROSS_AMT","INVOICE_FREIGHT_AMT","INVOICE_DISC_AMT","INVOICE_DISC_TYPE_CODE",
				"INVOICE_TAX_PAID_AMT","INVOICE_LINE_NBR","INVOICE_LINE_DESC","INVOICE_LINE_GROSS_AMT","INVOICE_LINE_ITEM-COUNT","INVOICE_LINE_ITEM_AMT",
				"INVOICE_LINE_FREIGHT_AMT","INVOICE_LINE_DISC_AMT","INVOICE_LINE_DISC_TYPE_CODE","INVOICE_LINE_TAX_PAID_AMT","INVOICE_LINE_PRODUCT_CODE"
	    	};
	    	
	    	for(int i=0; i<INVOICE_COLUMNS.length; i++){
	    		DataDefinitionColumn driverNames = propertyMap.get(Util.columnToProperty(INVOICE_COLUMNS[i]));
	    		if(driverNames!=null){
	    			selectItems.add(new SelectItem(driverNames.getColumnName(),driverNames.getDescription()));
	    		}
	    	}

			invoiceTranbsactionItems = selectItems;
		}
		return invoiceTranbsactionItems;
	}
	
	public List<SelectItem> getLocationTranbsactionItems() {
		if(locationTranbsactionItems==null){
			Map<String,DataDefinitionColumn> propertyMap = cacheManager.getDataDefinitionPropertyMap("TB_SALETRANS");
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
	    	selectItems.add(new SelectItem("","Select a Field"));

	    	String[] LOCATION_COLUMNS = { 
	    			"SHIPTO_ENTITY_ID","SHIPTO_ENTITY_CODE","SHIPTO_JURISDICTION_ID","SHIPTO_GEOCODE","SHIPTO_ADDRESS_LINE_1","SHIPTO_ADDRESS_LINE_2",
	    			"SHIPTO_CITY","SHIPTO_COUNTY","SHIPTO_STATE_CODE","SHIPTO_ZIP","SHIPTO_ZIPPLUS4","SHIPTO_COUNTRY_CODE","SHIPTO_STJ1_NAME",
	    			"SHIPTO_STJ2_NAME","SHIPTO_STJ3_NAME","SHIPTO_STJ4_NAME","SHIPTO_STJ5_NAME","SHIPFROM_ENTITY_ID","SHIPFROM_ENTITY_CODE",
	    			"SHIPFROM_JURISDICTION_ID","SHIPFROM_GEOCODE","SHIPFROM_ADDRESS_LINE_1","SHIPFROM_ADDRESS_LINE_2",
	    			"SHIPFROM_CITY","SHIPFROM_COUNTY","SHIPFROM_STATE_CODE","SHIPFROM_ZIP","SHIPFROM_ZIPPLUS4","SHIPFROM_COUNTRY_CODE",
	    			"SHIPFROM_STJ1_NAME","SHIPFROM_STJ2_NAME","SHIPFROM_STJ3_NAME","SHIPFROM_STJ4_NAME","SHIPFROM_STJ5_NAME","ORDRACPT_ENTITY_ID",
	    			"ORDRACPT_ENTITY_CODE","ORDRACPT_JURISDICTION_ID","ORDRACPT_GEOCODE","ORDRACPT_ADDRESS_LINE_1","ORDRACPT_ADDRESS_LINE_2","ORDRACPT_CITY",
	    			"ORDRACPT_COUNTY","ORDRACPT_STATE_CODE","ORDRACPT_ZIP","ORDRACPT_ZIPPLUS4","ORDRACPT_COUNTRY_CODE","ORDRACPT_STJ1_NAME",
	    			"ORDRACPT_STJ2_NAME","ORDRACPT_STJ3_NAME","ORDRACPT_STJ4_NAME","ORDRACPT_STJ5_NAME","ORDRORGN_ENTITY_ID","ORDRORGN_ENTITY_CODE",
	    			"ORDRORGN_JURISDICTION_ID","ORDRORGN_GEOCODE","ORDRORGN_ADDRESS_LINE_1","ORDRORGN_ADDRESS_LINE_2","ORDRORGN_CITY","ORDRORGN_COUNTY",
	    			"ORDRORGN_STATE_CODE","ORDRORGN_ZIP","ORDRORGN_ZIPPLUS4","ORDRORGN_COUNTRY_CODE","ORDRORGN_STJ1_NAME","ORDRORGN_STJ2_NAME",
	    			"ORDRORGN_STJ3_NAME","ORDRORGN_STJ4_NAME","ORDRORGN_STJ5_NAME","FIRSTUSE_ENTITY_ID","FIRSTUSE_ENTITY_CODE","FIRSTUSE_JURISDICTION_ID",
	    			"FIRSTUSE_GEOCODE","FIRSTUSE_ADDRESS_LINE_1","FIRSTUSE_ADDRESS_LINE_2","FIRSTUSE_CITY","FIRSTUSE_COUNTY","FIRSTUSE_STATE_CODE",
	    			"FIRSTUSE_ZIP","FIRSTUSE_ZIPPLUS4","FIRSTUSE_COUNTRY_CODE","FIRSTUSE_STJ1_NAME","FIRSTUSE_STJ2_NAME","FIRSTUSE_STJ3_NAME",
	    			"FIRSTUSE_STJ4_NAME","FIRSTUSE_STJ5_NAME","BILLTO_ENTITY_ID","BILLTO_ENTITY_CODE","BILLTO_JURISDICTION_ID","BILLTO_GEOCODE",
	    			"BILLTO_ADDRESS_LINE_1","BILLTO_ADDRESS_LINE_2","BILLTO_CITY","BILLTO_COUNTY","BILLTO_STATE_CODE","BILLTO_ZIP","BILLTO_ZIPPLUS4",
	    			"BILLTO_COUNTRY_CODE","BILLTO_STJ1_NAME","BILLTO_STJ2_NAME","BILLTO_STJ3_NAME","BILLTO_STJ4_NAME","BILLTO_STJ5_NAME"
	    	};
	    	
	    	for(int i=0; i<LOCATION_COLUMNS.length; i++){
	    		DataDefinitionColumn driverNames = propertyMap.get(Util.columnToProperty(LOCATION_COLUMNS[i]));
	    		if(driverNames!=null){
	    			selectItems.add(new SelectItem(driverNames.getColumnName(),driverNames.getDescription()));
	    		}
	    	}

			locationTranbsactionItems = selectItems;
		}
		return locationTranbsactionItems;
	}
	
	public List<SelectItem> getEntityTranbsactionItems() {
		if(entityTranbsactionItems==null){
			Map<String,DataDefinitionColumn> propertyMap = cacheManager.getDataDefinitionPropertyMap("TB_SALETRANS");
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
	    	selectItems.add(new SelectItem("","Select a Field"));

	    	String[] ENTITY_COLUMNS = { 
	    			"ENTITY_ID","ENTITY_CODE","COMPANY_NBR","COMPANY_DESC","DIVISION_NBR","DIVISION_DESC"
	    	};
	    	
	    	for(int i=0; i<ENTITY_COLUMNS.length; i++){
	    		DataDefinitionColumn driverNames = propertyMap.get(Util.columnToProperty(ENTITY_COLUMNS[i]));
	    		if(driverNames!=null){
	    			selectItems.add(new SelectItem(driverNames.getColumnName(),driverNames.getDescription()));
	    		}
	    	}

			entityTranbsactionItems = selectItems;
		}
		return entityTranbsactionItems;
	}
	
	public List<SelectItem> getUserTranbsactionItems() {
		if(userTranbsactionItems==null){
			Map<String,DataDefinitionColumn> propertyMap = cacheManager.getDataDefinitionPropertyMap("TB_SALETRANS");
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
	    	selectItems.add(new SelectItem("","Select a Field"));

	    	String[] USER_COLUMNS = { 
	    			"USER_TEXT_01","USER_TEXT_02","USER_TEXT_03","USER_TEXT_04","USER_TEXT_05","USER_TEXT_06","USER_TEXT_07","USER_TEXT_08","USER_TEXT_09","USER_TEXT_10",
	    			"USER_TEXT_11","USER_TEXT_12","USER_TEXT_13","USER_TEXT_14","USER_TEXT_15","USER_TEXT_16","USER_TEXT_17","USER_TEXT_18","USER_TEXT_19","USER_TEXT_20",
	    			"USER_TEXT_21","USER_TEXT_22","USER_TEXT_23","USER_TEXT_24","USER_TEXT_25","USER_TEXT_26","USER_TEXT_27","USER_TEXT_28","USER_TEXT_29","USER_TEXT_30",
	    			"USER_TEXT_31","USER_TEXT_32","USER_TEXT_33","USER_TEXT_34","USER_TEXT_35","USER_TEXT_36","USER_TEXT_37","USER_TEXT_38","USER_TEXT_39","USER_TEXT_40",
	    			"USER_TEXT_41","USER_TEXT_42","USER_TEXT_43","USER_TEXT_44","USER_TEXT_45","USER_TEXT_46","USER_TEXT_47","USER_TEXT_48","USER_TEXT_49","USER_TEXT_50",
	    			"USER_TEXT_51","USER_TEXT_52","USER_TEXT_53","USER_TEXT_54","USER_TEXT_55","USER_TEXT_56","USER_TEXT_57","USER_TEXT_58","USER_TEXT_59","USER_TEXT_60",
	    			"USER_TEXT_61","USER_TEXT_62","USER_TEXT_63","USER_TEXT_64","USER_TEXT_65","USER_TEXT_66","USER_TEXT_67","USER_TEXT_68","USER_TEXT_69","USER_TEXT_70",
	    			"USER_TEXT_71","USER_TEXT_72","USER_TEXT_73","USER_TEXT_74","USER_TEXT_75","USER_TEXT_76","USER_TEXT_77","USER_TEXT_78","USER_TEXT_79","USER_TEXT_80",
	    			"USER_TEXT_81","USER_TEXT_82","USER_TEXT_83","USER_TEXT_84","USER_TEXT_85","USER_TEXT_86","USER_TEXT_87","USER_TEXT_88","USER_TEXT_89","USER_TEXT_90",
	    			"USER_TEXT_91","USER_TEXT_92","USER_TEXT_93","USER_TEXT_94","USER_TEXT_95","USER_TEXT_96","USER_TEXT_97","USER_TEXT_98","USER_TEXT_99","USER_TEXT_100",
	    			"USER_NUMBER_01","USER_NUMBER_02","USER_NUMBER_03","USER_NUMBER_04","USER_NUMBER_05","USER_NUMBER_06","USER_NUMBER_07","USER_NUMBER_08","USER_NUMBER_09","USER_NUMBER_10",
	    			"USER_NUMBER_11","USER_NUMBER_12","USER_NUMBER_13","USER_NUMBER_14","USER_NUMBER_15","USER_NUMBER_16","USER_NUMBER_17","USER_NUMBER_18","USER_NUMBER_19","USER_NUMBER_20",
	    			"USER_NUMBER_21","USER_NUMBER_22","USER_NUMBER_23","USER_NUMBER_24","USER_NUMBER_25",
	    			"USER_DATE_01","USER_DATE_02","USER_DATE_03","USER_DATE_04","USER_DATE_05","USER_DATE_06","USER_DATE_07","USER_DATE_08","USER_DATE_09","USER_DATE_10",
	    			"USER_DATE_11","USER_DATE_12","USER_DATE_13","USER_DATE_14","USER_DATE_15","USER_DATE_16","USER_DATE_17","USER_DATE_18","USER_DATE_19","USER_DATE_20","USER_DATE_21",
	    			"USER_DATE_22","USER_DATE_23","USER_DATE_24","USER_DATE_25",
	    	};
	    	
	    	for(int i=0; i<USER_COLUMNS.length; i++){
	    		DataDefinitionColumn driverNames = propertyMap.get(Util.columnToProperty(USER_COLUMNS[i]));
	    		if(driverNames!=null){
	    			selectItems.add(new SelectItem(driverNames.getColumnName(),driverNames.getDescription()));
	    		}
	    	}

			userTranbsactionItems = selectItems;
		}
		return userTranbsactionItems;
	}
	
	public List<SelectItem> getOperationGroupItems() {	
		if(getIsPurchasingMenu()){
			if(operationGroupItems==null){
				List<SelectItem> selectItems = new ArrayList<SelectItem>();
				selectItems.add(new SelectItem("ALL","All"));			
				selectItems.add(new SelectItem("TRANS_AUDIT","Audit"));
				selectItems.add(new SelectItem("TRANS_GENERALLEDGER","General Ledger"));
				selectItems.add(new SelectItem("TRANS_INVENTORY","Inventory"));
				selectItems.add(new SelectItem("TRANS_INVOICE","Invoice"));
				selectItems.add(new SelectItem("TRANS_LOCATION","Location"));				
				selectItems.add(new SelectItem("TRANS_OTHER","Other"));				
				selectItems.add(new SelectItem("TRANS_PAYMENTINFO","Payment Info"));
				selectItems.add(new SelectItem("TRANS_PROJECT","Project"));
				selectItems.add(new SelectItem("TRANS_PURCHASEORDER","Purchase Order"));
				selectItems.add(new SelectItem("TRANS_TRANSACTION","Transaction"));
				selectItems.add(new SelectItem("TRANS_USER","User"));
				selectItems.add(new SelectItem("TRANS_VENDOR","Vendor"));
				selectItems.add(new SelectItem("TRANS_WORKORDER","Work Order"));			
		    	operationGroupItems = selectItems;
			}
			return operationGroupItems;
		}
		else{
			if(operationGroupSalesItems==null){
				List<SelectItem> selectItems = new ArrayList<SelectItem>();
				selectItems.add(new SelectItem("ALL","All"));
		    	selectItems.add(new SelectItem("SALE_BASIC","Basic"));
		    	selectItems.add(new SelectItem("SALE_CUSTOMER","Customer"));
		    	selectItems.add(new SelectItem("SALE_INVOICE","Invoice"));
		    	selectItems.add(new SelectItem("SALE_LOCATION","Location"));
		    	selectItems.add(new SelectItem("SALE_ENTITY","Entity"));
		    	selectItems.add(new SelectItem("SALE_USER","User"));
		    	operationGroupSalesItems = selectItems;
			}
			return operationGroupSalesItems;
		}
	}
	
	public List<SelectItem> getOperatorItems() {
		if(operatorItems==null){
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
	    	selectItems.add(new SelectItem("AND","And"));
	    	selectItems.add(new SelectItem("OR","Or"));
	    	operatorItems = selectItems;
		}
		return operatorItems;
	}
	
	public List<SelectItem> getRelationItems() {
		if(relationItems==null){
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
			selectItems.add(new SelectItem("=","="));
	    	selectItems.add(new SelectItem("<","<"));
	    	selectItems.add(new SelectItem(">",">"));
	    	selectItems.add(new SelectItem("<=","<="));
	    	selectItems.add(new SelectItem(">=",">="));
	    	selectItems.add(new SelectItem("<>","<>"));
	    	selectItems.add(new SelectItem("StartsWith","Starts With"));
	    	selectItems.add(new SelectItem("EndsWith","Ends With"));
	    	selectItems.add(new SelectItem("Contains","Contains"));
	    	relationItems = selectItems;
		}
		return relationItems;
	}
	
	public List<SelectItem> getValuelocationInColumnItems() {	
		if(getIsPurchasingMenu()){
			if(valuelocationInColumnItems==null){
				List<SelectItem> selectItems = new ArrayList<SelectItem>();
				selectItems.add(new SelectItem("ALL","All"));			
				selectItems.add(new SelectItem("TRANS_AUDIT","Audit"));
				selectItems.add(new SelectItem("TRANS_GENERALLEDGER","General Ledger"));
				selectItems.add(new SelectItem("TRANS_INVENTORY","Inventory"));
				selectItems.add(new SelectItem("TRANS_INVOICE","Invoice"));
				selectItems.add(new SelectItem("TRANS_LOCATION","Location"));
				selectItems.add(new SelectItem("TRANS_PAYMENTINFO","Payment Info"));
				selectItems.add(new SelectItem("TRANS_PROJECT","Project"));
				selectItems.add(new SelectItem("TRANS_PURCHASEORDER","Purchase Order"));
				selectItems.add(new SelectItem("TRANS_TRANSACTION","Transaction"));
				selectItems.add(new SelectItem("TRANS_USER","User"));
				selectItems.add(new SelectItem("TRANS_VENDOR","Vendor"));
				selectItems.add(new SelectItem("TRANS_WORKORDER","Work Order"));	
				selectItems.add(new SelectItem("TEXT","Text"));
				valuelocationInColumnItems = selectItems;
			}
			return valuelocationInColumnItems;
		}
		else{
			if(valuelocationInColumnSalesItems==null){
				List<SelectItem> selectItems = new ArrayList<SelectItem>();
				selectItems.add(new SelectItem("ALL","All"));
		    	selectItems.add(new SelectItem("SALE_BASIC","Basic"));
		    	selectItems.add(new SelectItem("SALE_CUSTOMER","Customer"));
		    	selectItems.add(new SelectItem("SALE_INVOICE","Invoice"));
		    	selectItems.add(new SelectItem("SALE_LOCATION","Location"));
		    	selectItems.add(new SelectItem("SALE_ENTITY","Entity"));
		    	selectItems.add(new SelectItem("SALE_USER","User"));
		    	selectItems.add(new SelectItem("TEXT","Text"));
		    	valuelocationInColumnSalesItems = selectItems;
			}
			return valuelocationInColumnSalesItems;
		}
	}
	
	public List<SelectItem> getValuePopulateItems() {	
		if(getIsPurchasingMenu()){
			if(valuePopulateItems==null){
				List<SelectItem> selectItems = new ArrayList<SelectItem>();
				selectItems.add(new SelectItem("VALUE","Value"));
				selectItems.add(new SelectItem("BLANK","Blank"));
				selectItems.add(new SelectItem("ALL","All"));			
				selectItems.add(new SelectItem("TRANS_AUDIT","Audit"));
				selectItems.add(new SelectItem("TRANS_GENERALLEDGER","General Ledger"));
				selectItems.add(new SelectItem("TRANS_INVENTORY","Inventory"));
				selectItems.add(new SelectItem("TRANS_INVOICE","Invoice"));
				selectItems.add(new SelectItem("TRANS_LOCATION","Location"));
				selectItems.add(new SelectItem("TRANS_PAYMENTINFO","Payment Info"));
				selectItems.add(new SelectItem("TRANS_PROJECT","Project"));
				selectItems.add(new SelectItem("TRANS_PURCHASEORDER","Purchase Order"));
				selectItems.add(new SelectItem("TRANS_TRANSACTION","Transaction"));
				selectItems.add(new SelectItem("TRANS_USER","User"));
				selectItems.add(new SelectItem("TRANS_VENDOR","Vendor"));
				selectItems.add(new SelectItem("TRANS_WORKORDER","Work Order"));			
				valuePopulateItems = selectItems;
			}
			return valuePopulateItems;
		}
		else{
			if(valuePopulateSalesItems==null){
				List<SelectItem> selectItems = new ArrayList<SelectItem>();
				selectItems.add(new SelectItem("VALUE","Value"));
				selectItems.add(new SelectItem("BLANK","Blank"));
				selectItems.add(new SelectItem("ALL","All"));
		    	selectItems.add(new SelectItem("SALE_BASIC","Basic"));
		    	selectItems.add(new SelectItem("SALE_CUSTOMER","Customer"));
		    	selectItems.add(new SelectItem("SALE_INVOICE","Invoice"));
		    	selectItems.add(new SelectItem("SALE_LOCATION","Location"));
		    	selectItems.add(new SelectItem("SALE_ENTITY","Entity"));
		    	selectItems.add(new SelectItem("SALE_USER","User"));
		    	valuePopulateSalesItems = selectItems;
			}
			return valuePopulateSalesItems;
		}
	}
	
	public List<SelectItem> getValuelocationOutColumnItems() {
		
		if(valuelocationOutColumnItems==null){
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
	    	selectItems.add(new SelectItem("TEXT","Text"));
	    	selectItems.add(new SelectItem("NUMBER","Number"));
	    	selectItems.add(new SelectItem("DATE","Date"));
	    	valuelocationOutColumnItems = selectItems;
		}
		return valuelocationOutColumnItems;
	}
	
	public Integer getFirstInColumnObject() {
		if(inColumnObjectItems!=null && inColumnObjectItems.size()>=1){
			return new Integer(0);
		}
		else{
			return new Integer(-1);
		}
	}
	
	public Integer getLastInColumnObject() {
		if(inColumnObjectItems!=null && inColumnObjectItems.size()>=1){
			return new Integer(inColumnObjectItems.size() - 1);
		}
		else{
			return new Integer(-1);
		}
	}
	
	public Integer getFirstOutColumnObject() {
		if(outColumnObjectItems!=null && outColumnObjectItems.size()>=1){
			return new Integer(0);
		}
		else{
			return new Integer(-1);
		}
	}
	
	public Integer getLastOutColumnObject() {
		if(outColumnObjectItems!=null && outColumnObjectItems.size()>=1){
			return new Integer(outColumnObjectItems.size() - 1);
		}
		else{
			return new Integer(-1);
		}
	}
	
	public void processAddInColumnCommand(ActionEvent event) {
	    String command = (String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("command");
	    
	    updateChangedArrayList.add(new String[]{"ADD", "IN", command});

		InColumnObject aInColumnObject = new InColumnObject();
		aInColumnObject.setOperation("ALL");
		inColumnObjectItems.add(aInColumnObject);

	    return;
	}
	
	public void processRemoveInColumnCommand(ActionEvent event) {
	    String command = (String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("command");
	    
	    updateChangedArrayList.add(new String[]{"REMOVE", "IN", command});
	    
	    int commandInt = 0;
	    try{
	    	commandInt = Integer.parseInt(command);
	    }
	    catch(Exception e){
	    	return;
	    }
	    
	    inColumnObjectItems.remove(commandInt);

	    return;
	}
	
	public void processAddOutColumnCommand(ActionEvent event) {
	    String command = (String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("command");
	    
	    updateChangedArrayList.add(new String[]{"ADD", "OUT", command});
	    
	    OutColumnObject aOutColumnObject = new OutColumnObject();
		outColumnObjectItems.add(aOutColumnObject);

	    return;
	}
	
	public void processRemoveOutColumnCommand(ActionEvent event) {
	    String command = (String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("command");
	    
	    updateChangedArrayList.add(new String[]{"REMOVE", "OUT", command});
	    
	    int commandInt = 0;
	    try{
	    	commandInt = Integer.parseInt(command);
	    }
	    catch(Exception e){
	    	return;
	    }
	    
	    outColumnObjectItems.remove(commandInt);

	    return;
	}
	
	public String valuelocationInColumnChange() {
		String command = (String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("command");

		updateChangedArrayList.add(new String[]{"CHANGE", "IN", command});
	    
	    int commandInt = 0;
	    try{
	    	commandInt = Integer.parseInt(command);
	    }
	    catch(Exception e){
	    	return null;
	    }
	    
	    if(inColumnObjectItems==null || inColumnObjectItems.size()==0){
	    	return null;//Impossible
	    }
	    
	    InColumnObject  aInColumnObject = (InColumnObject)inColumnObjectItems.get(commandInt);
	    aInColumnObject.setFieldColumn("");
	    aInColumnObject.setFieldText("");

	    return null;
	}
	
	public String valuelocationOutColumnChange() {
		String command = (String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("command");
		
		System.out.println("*********   CHANGE Out Column : " +  command);
		updateChangedArrayList.add(new String[]{"CHANGE", "OUT", command});
	    
	    int commandInt = 0;
	    try{
	    	commandInt = Integer.parseInt(command);
	    }
	    catch(Exception e){
	    	return null;
	    }
	    
	    if(outColumnObjectItems==null || outColumnObjectItems.size()==0){
	    	return null;//Impossible
	    }
	    
	    OutColumnObject  aOutColumnObject = (OutColumnObject)outColumnObjectItems.get(commandInt);
	    aOutColumnObject.setFieldColumn("");
	    aOutColumnObject.setFieldText("");

	    return null;
	}
	
	//populate table
	public Integer getLastPopulateObject() {
		if(populateObjectArrayList!=null && populateObjectArrayList.size()>=1){
			return new Integer(populateObjectArrayList.size() - 1);
		}
		else{
			return new Integer(-1);
		}
	}
	
	public Integer getFirstPopulateObject() {
		if(populateObjectArrayList!=null && populateObjectArrayList.size()>=1){
			return new Integer(0);
		}
		else{
			return new Integer(-1);
		}
	}
	
	public void processAddPopulateCommand(ActionEvent event) {
	    String command = (String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("command");
	    
	    ArrayList<PopulateObject> populateObjectItems = createPopulateObjectItems();
		populateObjectArrayList.add(populateObjectItems);

	    return;
	}
	
	public void processRemovePopulateCommand(ActionEvent event) {
	    String command = (String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("command");
	    
	    int commandInt = 0;
	    try{
	    	commandInt = Integer.parseInt(command);
	    }
	    catch(Exception e){
	    	return;
	    }
	    
	    populateObjectArrayList.remove(commandInt);

	    return;
	}
	
	public String valuePopulateObjectColumnChange() {
		String command = (String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("command");
		String innercommand = (String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("innercommand");
	    
	    int commandInt = 0;
	    int innercommandInt = 0;
	    try{
	    	commandInt = Integer.parseInt(command);
	    	innercommandInt = Integer.parseInt(innercommand);
	    }
	    catch(Exception e){
	    	return null;
	    }
	    
	    if(populateObjectArrayList==null || populateObjectArrayList.size()==0){
	    	return null;//Impossible
	    }
	    
	    List<PopulateObject> aPopulateObjectItems = (List<PopulateObject>) populateObjectArrayList.get(commandInt);
	    
	    PopulateObject aPopulateObject = (PopulateObject) aPopulateObjectItems.get(innercommandInt);
	    aPopulateObject.setFieldColumn("");
	    aPopulateObject.setFieldText("");

	    return null;
	}
	
	public String valueoutObjectColumnChange() {
		String command = (String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("command");
	    int commandInt = 0;
	    try{
	    	commandInt = Integer.parseInt(command);
	    }
	    catch(Exception e){
	    	return null;
	    }
	    
	    PopulateObject aPopulateObject = (PopulateObject) outcolumnsList.get(commandInt);
	    aPopulateObject.setFieldColumn("");
	    aPopulateObject.setFieldText("");

	    return null;
	}
	
	public class InColumnObject{
		private String operation = "SALE_BASIC";
		private String fieldColumn = "";
		private String fieldText = "";
		private String columnName = ""; 
		
		public String getFieldColumn() {
			return this.fieldColumn;
		}

		public void setFieldColumn(String fieldColumn) {
			this.fieldColumn = fieldColumn;
		}

		public String getOperation() {
			return this.operation;
		}

		public void setOperation(String operation) {
			this.operation = operation;
		}
		
		public String getFieldText() {
			return this.fieldText;
		}

		public void setFieldText(String fieldText) {
			this.fieldText = fieldText;
		}

		public String getColumnName() {
			return columnName;
		}

		public void setColumnName(String columnName) {
			this.columnName = columnName;
		}
		
	}	
	
	public class OutColumnObject{
		private String operation = "TEXT";
		private String fieldColumn = "";
		private String fieldText = "";
		private String columnName = ""; 
		
		public String getFieldColumn() {
			return this.fieldColumn;
		}

		public void setFieldColumn(String fieldColumn) {
			this.fieldColumn = fieldColumn;
		}

		public String getOperation() {
			return this.operation;
		}

		public void setOperation(String operation) {
			this.operation = operation;
		}
		
		public String getFieldText() {
			return this.fieldText;
		}

		public void setFieldText(String fieldText) {
			this.fieldText = fieldText;
		}
		public String getColumnName() {
			return columnName;
		}

		public void setColumnName(String columnName) {
			this.columnName = columnName;
		}
	}
	
	public class PopulateObject{
		private String searchType = "TEXT"; //"TEXT", "COLUMN", & "COMBO"

		//For column lookup
		private String columnSearchName = "";
		
		//For text lookup
		private String textSearchName = "";
		
		//For combo box
		private String operation = "VALUE";
		private String fieldColumn = "";
		private String fieldText = "";
		private String columnheading = "";
		
		public String getSearchType() {
			return searchType;
		}

		public String getColumnheading() {
			return columnheading;
		}

		public void setColumnheading(String columnheading) {
			this.columnheading = columnheading;
		}

		public void setSearchType(String searchType) {
			this.searchType = searchType;
		}
		
		public void setTextSearchType() {
			this.searchType = "TEXT";
		}
		
		public void setColumnSearchType() {
			this.searchType = "COLUMN";
		}
		
		public void setComboSearchType() {
			this.searchType = "COMBO";
		}
		
		public String getColumnSearchName() {
			return this.columnSearchName;
		}

		public void setColumnSearchName(String columnSearchName) {
			this.columnSearchName = columnSearchName;
		}
		
		public String getTextSearchName() {
			return this.textSearchName;
		}

		public void setTextSearchName(String textSearchName) {
			this.textSearchName = textSearchName;
		}
		
		public Boolean getIsTextSearchType(){
			return (searchType.equalsIgnoreCase("TEXT")) ? true : false;
		}
		
		public Boolean getIsColumnSearchType(){
			return (searchType.equalsIgnoreCase("COLUMN")) ? true : false;
		}
		
		public Boolean getIsComboSearchType(){
			return (searchType.equalsIgnoreCase("COMBO")) ? true : false;
		}
		
		public String getFieldColumn() {
			return this.fieldColumn;
		}

		public void setFieldColumn(String fieldColumn) {
			this.fieldColumn = fieldColumn;
		}

		public String getOperation() {
			return this.operation;
		}

		public void setOperation(String operation) {
			this.operation = operation;
		}
		
		public String getFieldText() {
			return this.fieldText;
		}

		public void setFieldText(String fieldText) {
			this.fieldText = fieldText;
		}
	}	
	
    private void getProctabledetailData(Long proctableId){
		if(!buildInOutColumnObjects(proctableId)){
			return; 
		}
		if(inColumnObjectItems==null || inColumnObjectItems.size()==0 || outColumnObjectItems==null || outColumnObjectItems.size()==0){
			return; 
		}
		ProctableDetail aProctableDetail = this.proctableService.findProctableDetailById(selProcDetailtable.getProctableDetailId());
		inoutcolumnsList= new ArrayList<PopulateObject>();
		incolumnsList = new ArrayList<PopulateObject>();
		outcolumnsList = new ArrayList<PopulateObject>();
		
		createProctableDetail(aProctableDetail, inoutcolumnsList, incolumnsList, outcolumnsList);
     }
    
    private void createProctableDetail(ProctableDetail  aProctableDetail, List<PopulateObject> inoutcolumnsList, List<PopulateObject> incolumnList, List<PopulateObject> outColumnList) {
    	
    	InColumnObject aInColumnObject = null;
		OutColumnObject aOutColumnObject = null;
		PopulateObject aPopulateObject = null;
    	Class<?> cls = ProctableDetail.class;
		NumberFormat formatter = new DecimalFormat("00");
		String methodName = null;
		Method m = null;
		String columnValue = null;
		String columnType = null;
		for(int i=0; i< inColumnObjectItems.size(); i++){
			aInColumnObject = inColumnObjectItems.get(i);
			aPopulateObject = new PopulateObject();
			
			try {
				methodName = "getInColumn" + formatter.format(i+1); 
				m = cls.getMethod(methodName, new Class<?> [] {});					
				if (m != null) {
					Object val = m.invoke(aProctableDetail, new Object [] {});							
					if (val != null) {
						columnValue = (String)val;
						if(aInColumnObject.getOperation().equalsIgnoreCase("TEXT")){
							aPopulateObject.setSearchType("TEXT");
							aPopulateObject.setTextSearchName(columnValue);
						}
						else{
							aPopulateObject.setSearchType("COLUMN");
							aPopulateObject.setColumnSearchName(columnValue);
						}
					}
					aPopulateObject.setColumnheading(aInColumnObject.getColumnName()); 
				}
			}
			catch(Exception e){
				
			}
			inoutcolumnsList.add(aPopulateObject);
			if(incolumnList != null){
				incolumnList.add(aPopulateObject);
			}
			
		}
		
		for(int i=0; i< outColumnObjectItems.size(); i++){
			aOutColumnObject = outColumnObjectItems.get(i);
			aPopulateObject = new PopulateObject();
			aPopulateObject.setSearchType("COMBO");
			
			try {
				columnType = null;
				columnValue = null;
				methodName = "getOutColumnName" + formatter.format(i+1); 
				m = cls.getMethod(methodName, new Class<?> [] {});					
				if (m != null) {
					Object val = m.invoke(aProctableDetail, new Object [] {});							
					if (val != null) {
						columnValue = (String)val;
						//Check value later
					}
					aPopulateObject.setColumnheading(aOutColumnObject.getColumnName()); 
				}
				
				methodName = "getOutColumnType" + formatter.format(i+1); 
				m = cls.getMethod(methodName, new Class<?> [] {});					
				if (m != null) {
					Object val = m.invoke(aProctableDetail, new Object [] {});							
					if (val != null) {
						columnType = (String)val;
						
						if(columnType.equalsIgnoreCase("V")){
							if(columnValue!=null && columnValue.length()>0){
								aPopulateObject.setOperation("VALUE");
								aPopulateObject.setFieldText(columnValue);
							}
							else{
								aPopulateObject.setOperation("BLANK");
								aPopulateObject.setFieldText("");
							}
						}
						else{
							aPopulateObject.setOperation("ALL");
							aPopulateObject.setFieldColumn(columnValue);
						}
					}
				}
			}
			catch(Exception e){
				
			}
			inoutcolumnsList.add(aPopulateObject);
			if(outColumnList != null) {
				outColumnList.add(aPopulateObject);
			}
		}
    }
    
    public String addProctabledetailAction() {
    	this.currentAction = EditAction.ADD;
    	if(!buildInOutColumnObjects(selProctable.getProctableId())){
			return null; 
		}
    	updateProctable= new Proctable();		
		if (selProctable != null) {
			BeanUtils.copyProperties(selProctable, updateProctable);
		}
	
		if(inColumnObjectItems==null || inColumnObjectItems.size()==0 || outColumnObjectItems==null || outColumnObjectItems.size()==0){
			return null; 
		}
    	inoutcolumnsList = createPopulateObjectItems();
	    logger.info("Created empty records for Add process detail with input and output columns size : "+inoutcolumnsList.size()); 
	    return "proctabledetail_update";
	}
    
    public String addorUpdateProcdetailTableAction() {
		String result = null;
		try {
			
			if(validateEntries()){
				return "";
			}
			
			updateProctable= new Proctable();		
			if (selProctable != null) {
				BeanUtils.copyProperties(selProctable, updateProctable);
			}
			ProctableDetail proctableDetail  = buildProctableDetail();
			this.proctableService.addProctableDetail(updateProctable, proctableDetail);

			//Refresh & Retrieve
			retrieveFilterProctableDetail();
			selectedProctableDetailRowIndex = -1;//reset the table row index
			
		    result = "proctable_main";		
		} catch (Exception ex) {
			ex.printStackTrace();
			result = "";
		}
		   
        return result;
	}
    
    public String deleteProctabledetailAction() {
    	updateProctabledetailAction();
		this.currentAction = EditAction.DELETE;
		
		return "proctabledetail_update";
	}
	
	public String viewProctabledetailAction(){
		updateProctabledetailAction();
		this.currentAction = EditAction.VIEW;
		
		return "proctabledetail_update";
	}
	
	public String deleteProcdetailTableAction() {
		String result = null;
		try {
			this.proctableService.deleteProctableDetail(selProcDetailtable);
			//Refresh & Retrieve
			retrieveFilterProctableDetail();
			selectedProctableDetailRowIndex = -1;//reset the table row index
			result = "proctable_main";	
		} catch (Exception ex) {
			ex.printStackTrace();
			result = "";
		}
		   
        return result;
	}
    
    public String getPageDescription() {
		int rows = selectedProctableDetails == null ? 0 : selectedProctableDetails.size();
		int start = rows == 0 ? 0 : ((entriesPageNumber - 1) * entriesPageSize ) + 1;
		int end = entriesPageNumber * entriesPageSize ;
		if(end > rows) {
			end = rows;
		}
		
		String desc = String.format("Displaying rows %d through %d (%d matching rows)", start, end, rows);
		return desc;
	}
    
    public User getCurrentUser() {
		return loginBean.getUser();
   }
		
}
