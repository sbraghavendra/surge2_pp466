package com.ncsts.view.bean;

import java.beans.PropertyDescriptor;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileFilter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.TreeMap;

import javax.faces.component.UIComponent;
import javax.faces.component.UISelectItems;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.persistence.Column;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.servlet.http.HttpSession;

import org.ajax4jsf.component.UIDataAdaptor;
import org.apache.log4j.Logger;
import org.hibernate.CacheMode;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTemplate;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;

import com.ncsts.common.CacheManager;
import com.ncsts.common.DatabaseUtil;
import com.ncsts.common.Util;
import com.ncsts.dao.BatchMaintenanceDAO;
import com.ncsts.domain.AuditListener;
import com.ncsts.domain.Auditable;
import com.ncsts.domain.BCPBillTransaction;
import com.ncsts.domain.BCPPurchaseTransaction;
import com.ncsts.domain.BatchErrorLog;
import com.ncsts.domain.BatchMaintenance;
import com.ncsts.domain.BatchMaintenance.ErrorState;
import com.ncsts.domain.DataDefinitionColumn;
import com.ncsts.domain.DataDefinitionColumnPK;
import com.ncsts.domain.DriverNames;
import com.ncsts.domain.DriverNamesPK;
import com.ncsts.domain.ImportConnection;
import com.ncsts.domain.ImportDefinition;
import com.ncsts.domain.ImportDefinitionDetail;
import com.ncsts.domain.ImportSpec;
import com.ncsts.domain.ImportSpecPK;
import com.ncsts.domain.ListCodes;
import com.ncsts.domain.ListCodesPK;
import com.ncsts.domain.Option;
import com.ncsts.domain.OptionCodePK;
import com.ncsts.domain.TempTransactionDetail;
import com.ncsts.dto.DbPropertiesDTO;
import com.ncsts.dto.ImportMapProcessPreferenceDTO;
import com.ncsts.dto.ImportSpecDTO;
import com.ncsts.dto.SmtpPropertiesDTO;
import com.ncsts.dto.ImportMapProcessPreferenceDTO.DuplicationRestriction;
import com.ncsts.exception.DateFormatException;
import com.ncsts.management.DbPropertiesBean;
import com.ncsts.script.ScriptRunner;
import com.ncsts.services.ImportDefAndSpecsService;
import com.ncsts.services.ImportDefinitionService;
import com.ncsts.services.ImportMapService;
import com.ncsts.services.OptionService;
import com.ncsts.view.util.BCPTransaction;
import com.ncsts.view.util.FacesUtils;
import com.ncsts.view.util.ImportMapDatabaseParser;
import com.ncsts.view.util.ImportMapUploadParser;
import com.ncsts.view.util.JsfHelper;

public class ImportMapAddBean {

	private Logger logger = Logger.getLogger(ImportMapAddBean.class);

	private enum AddState {
		SELECT_SPEC, NEED_FILE, HAVE_FILE, PROCESSING, COMPLETE
	}

	private AddState state = AddState.SELECT_SPEC;

	private String backgroundError;
	private Thread background;

	private int selectedRowIndex = -1;
	private ImportSpec selectedImportSpec;
	private ImportDefinition selectedDefinition;
	private List<ImportSpec> importSpecList;
	private HtmlSelectOneMenu selectMenu = new HtmlSelectOneMenu();
	private Collection<SelectItem> fileItems;
	private File selectedFile;
	private ImportMapUploadParser parser;
	private List<BatchMaintenance> currentBatches;
	private List<ImportDefinitionDetail> mappedColumns;

	private long bytesInFile;
	private long estimatedRows;
	private BatchMaintenance newBatch;

	private BatchMaintenanceDAO batchMaintenanceDAO;
	private FilePreferenceBackingBean filePreferenceBean;
	private ImportMapProcessPreferenceDTO preferences;

	private CacheManager cacheManager;
	private DbPropertiesBean dbPropertiesBean;

	private ImportMapService importMapService;
	private ImportDefAndSpecsService importDefAndSpecsService;
	private ImportDefinitionService importDefinitionService;
	private ImportMapBean importMapBean;
	
	private String verificationMessage = "";
	private boolean allowDatabaseImport = false;

	@Autowired
	private OptionService optionService;
	@Autowired
	private loginBean loginservice;

	private DbPropertiesDTO dbPropertiesDTO = null;

	private boolean powerBuilderVersion = false;
	private Properties aidProperties = null;
	private StringBuffer emailContent = null;
	long startTime = 0;

	public void setAidProperties(Properties aidProperties) {
		this.aidProperties = aidProperties;
	}
	
	public void setPowerBuilderVersion(boolean powerBuilderVersion) {
		this.powerBuilderVersion = powerBuilderVersion;
	}

	public void clear(ImportMapBean bean) {
		this.importMapBean = bean;
		this.batchMaintenanceDAO = bean.getBatchMaintenanceDAO();
		clear();
	}

	public void clear() {
		this.selectedRowIndex = -1;
		this.selectedDefinition = null;
		this.importSpecList = null;
		this.selectedFile = null;
		preferences = filePreferenceBean.getImportMapProcessPreferenceDTO();
		currentBatches = null;
		mappedColumns = null;
		fileItems = null;
		background = null;
		state = AddState.SELECT_SPEC;
	}

	public void addAction() {
		try {
			ImportDefinition def = new ImportDefinition(selectedDefinition);
			def.setSampleFileName(selectedImportSpec.getDefaultDirectory()
					+ File.separator + selectedFile.getName());
			parser = new ImportMapUploadParser(def, preferences);
			parser.setCacheManager(cacheManager);
		} catch (DateFormatException dfe) {
			JsfHelper.addError(dfe.getMessage());
		}

		parser.setImportDefinitionDetail(mappedColumns);

		newBatch = new BatchMaintenance();
		// since this runs in a background thread, it has no access to a
		// FacesContext, so we need to get it now in foreground
		newBatch.setUpdateUserId(Auditable.currentUserCode());
		newBatch.setEntryTimestamp(new Date());
		newBatch.setVc02(selectedFile.getName());
		newBatch.setBatchTypeCode(selectedImportSpec.getImportSpecType());
		newBatch.setBatchStatusCode("IX");
		newBatch.setHeldFlag("0");
		newBatch.setTotalBytes(parser.getFileSize());
		batchMaintenanceDAO.save(newBatch);

		// now do the heavy lifting
		parser.setColumnProperties(batchMaintenanceDAO
				.getColumnProperties(BCPPurchaseTransaction.class));
		parser.setColumnPropertiesSale(batchMaintenanceDAO
				.getColumnProperties(BCPBillTransaction.class));
		processUpdateInBackground(newBatch);
	}

	public String getVerificationMessage(){
		return verificationMessage;
	}
	
	public boolean getAllowDatabaseImport(){
		return allowDatabaseImport;
	}
	
	public void verifyImportConnection() {
		state = AddState.HAVE_FILE;
		newBatch = null;
		
		try {
			ImportDefinition def = new ImportDefinition(selectedDefinition);
			def.setSampleFileName("");

			parser = new ImportMapDatabaseParser(def, preferences);
			parser.setCacheManager(cacheManager);
		} catch (DateFormatException dfe) {
			JsfHelper.addError(dfe.getMessage());
		}
	
		//Create headers list from mapping
		Map<String, String> result = new LinkedHashMap<String, String>();	
		for (ImportDefinitionDetail detail : getMappedColumns()) {
			result.put(detail.getImportColumnName(), detail.getImportColumnName());
		}
		
		List<String> headerNames = new ArrayList<String>();
		for (String headerName : result.keySet()) {
			headerNames.add(headerName);
		}
		parser.setHeaderNames(headerNames);

		List<ImportConnection> importConnections = importDefAndSpecsService
				.getConnectionsByCode(selectedDefinition.getImportConnectionCode()); // ORACLE_AC
		if (importConnections == null || importConnections.size() == 0) {
			verificationMessage = selectedDefinition.getImportConnectionCode() + " not found";
			return;
		}
		((ImportMapDatabaseParser) parser).setImportConnection(importConnections.get(0));
		((ImportMapDatabaseParser) parser).resetDatabaseRows();

		parser.setImportDefinitionDetail(mappedColumns);
		parser.setImportSpec(selectedImportSpec);

		allowDatabaseImport = ((ImportMapDatabaseParser) parser).verifyImportConnection();
		
		verificationMessage = ((ImportMapDatabaseParser) parser).getVerificationMessage();
	}
	
	public boolean automatedInitVerification(Connection connection, String specStr) {
		this.conn = connection;

		selectedImportSpec = getImportSpecAID(specStr);
		if (selectedImportSpec == null) {
			return false;
		}

		selectedDefinition = getImportDefinitionAID(selectedImportSpec.getImportDefinitionCode());
		if (selectedDefinition == null) {
			return false;
		}

		return true;
	}
	
	public boolean automatedDatabaseAction(final BatchMaintenance batch){
		try {
			ImportDefinition def = new ImportDefinition(selectedDefinition);
			def.setSampleFileName("");
			parser = new ImportMapDatabaseParser(def, preferences);
		} catch (DateFormatException dfe) {
			return false;
		}
	
		//Create headers list from mapping
		Map<String, String> result = new LinkedHashMap<String, String>();	
		for (ImportDefinitionDetail detail : getMappedColumnsAID()) {
			result.put(detail.getImportColumnName(), detail.getImportColumnName());
		}
		
		List<String> headerNames = new ArrayList<String>();
		for (String headerName : result.keySet()) {
			headerNames.add(headerName);
		}
		parser.setHeaderNames(headerNames);
		
		ImportConnection importConnection = getImportConnectionAID(selectedDefinition.getImportConnectionCode());
		if(importConnection==null){
			return false;
		}

		((ImportMapDatabaseParser) parser).setImportConnection(importConnection);
		((ImportMapDatabaseParser) parser).resetDatabaseRows();

		parser.setImportDefinitionDetail(mappedColumns);
		parser.setColumnProperties(getBCPTransactionDetailColumnProperties(BCPPurchaseTransaction.class));
		parser.setColumnPropertiesSale(getBCPTransactionDetailColumnProperties(BCPBillTransaction.class));
		automatedProcessUpdateInBackground(batch, conn, false);
	
		return true;
	}
	
	private void automatedProcessUpdateInBackground(BatchMaintenance batch, Connection conn, boolean autoProcess) {
		try {
			parser.setTransactionColumnDriverNamesMap(getTransactionColumnDriverNamesMapAID());
			parser.setBCPTransactionsColumnMap(getBCPTransactionsColumnMapAID("TB_PURCHTRANS"));
			parser.setBCPTransactionsColumnMapSale(getBCPTransactionsColumnMapAID("TB_BILLTRANS"));
			parser.setErrorListCodes(getErrorListCodesAID());
			parser.setImportSpec(selectedImportSpec);
			
			getMappedColumnsAID();

			state = AddState.PROCESSING;
			backgroundError = null;
			parser.setCompleted(false);
			conn.setAutoCommit(false);

			batch.setVc01(selectedImportSpec.getImportSpecCode());
			batch.setEntryTimestamp(new Date());

			// Import file
			importMapUpdatesUsingJDBC(batch, parser, conn);

			// force completion
			state = AddState.COMPLETE;
			if (parser.getErrorCount() > 0) {
				batch.setHeldFlag("1");
			}

			if (parser.getMaxErrorSeen() < 10) {
				// Perfect, no error.
			} else if (parser.getMaxErrorSeen() == 10) {
				batch.setErrorSevCode(BatchMaintenance.ErrorState.Warning);
			} else if (parser.getMaxErrorSeen() == 20) {
				batch.setErrorSevCode(BatchMaintenance.ErrorState.Error);
			} else if (parser.getMaxErrorSeen() >= 30) {
				batch.setErrorSevCode(BatchMaintenance.ErrorState.Terminal);
			}

			batch.setNu01(parser.getComputedTotalAmount().doubleValue());
			batch.setVc01(selectedImportSpec.getImportSpecCode());

			if (parser.wasAborted()) {
				batch.setBatchStatusCode("ABI");
			} else {
				batch.setBatchStatusCode("I");
			}

			batch.setTotalRows(new Long(parser.getTotalRows()));
			batch.setStatusUpdateTimestamp(new Date());
			batch.setStatusUpdateUserId(batch.getUpdateUserId());
			batch.setUpdateUserId(batch.getUpdateUserId());
			batch.setUpdateTimestamp(new Date());

			updateBatchUsingJDBC(batch, conn);
			
			// final clean up
			if (batch.getBatchStatusCode() != null
					&& batch.getBatchStatusCode().equalsIgnoreCase("I")) {
				parser.postImport();
			}

			parser.setCompleted(true);

			// Get latest from DB in case batch is updated from post scripts
			syncBatchUsingJDBC(batch, conn);

			if (autoProcess && (batch.getHeldFlag() == null || !batch.getHeldFlag().equalsIgnoreCase("1"))
					&& batch.getBatchStatusCode().equalsIgnoreCase("I")) {
				batch.setBatchStatusCode("FP");
				batch.setSchedStartTimestamp(new Date());
				batch.setUpdateTimestamp(new Date());
				batch.setStatusUpdateTimestamp(new Date());
				batch.setStatusUpdateUserId("STSCORP");
				updateBatchUsingJDBC(batch, conn);
			}
		} catch (Throwable e) {
			e.printStackTrace();
			parser.setCompleted(true);
			try {
				batch.setBatchStatusCode("ABI");
				batch.setStatusUpdateTimestamp(new Date());
				batch.setStatusUpdateUserId("STSCORP");
				batch.setUpdateTimestamp(new Date());
				updateBatchUsingJDBC(batch, conn);
			} catch (Exception ee) {
			}

		} finally {
		}
	}
	
	public void addDatabaseAction() {
		try {
			ImportDefinition def = new ImportDefinition(selectedDefinition);
			def.setSampleFileName("");

			parser = new ImportMapDatabaseParser(def, preferences);
			parser.setCacheManager(cacheManager);
		} catch (DateFormatException dfe) {
			JsfHelper.addError(dfe.getMessage());
		}
	
		//Create headers list from mapping
		Map<String, String> result = new LinkedHashMap<String, String>();	
		for (ImportDefinitionDetail detail : getMappedColumns()) {
			result.put(detail.getImportColumnName(), detail.getImportColumnName());
		}
		
		List<String> headerNames = new ArrayList<String>();
		for (String headerName : result.keySet()) {
			headerNames.add(headerName);
		}
		parser.setHeaderNames(headerNames);

		List<ImportConnection> importConnections = importDefAndSpecsService
				.getConnectionsByCode(selectedDefinition.getImportConnectionCode()); // ORACLE_AC
		if (importConnections == null || importConnections.size() == 0) {
			return;
		}
		((ImportMapDatabaseParser) parser).setImportConnection(importConnections.get(0));
		((ImportMapDatabaseParser) parser).resetDatabaseRows();

		parser.setImportDefinitionDetail(mappedColumns);

		newBatch = new BatchMaintenance();
		// since this runs in a background thread, it has no access to a
		// FacesContext, so we need to get it now in foreground
		newBatch.setUpdateUserId(Auditable.currentUserCode());
		newBatch.setEntryTimestamp(new Date());
		newBatch.setVc02("");// No file name
		newBatch.setBatchTypeCode(selectedImportSpec.getImportSpecType());
		newBatch.setBatchStatusCode("IX");
		newBatch.setHeldFlag("0");
		newBatch.setHeldFlag("0");
		batchMaintenanceDAO.save(newBatch);

		// now do the heavy lifting
		parser.setColumnProperties(batchMaintenanceDAO.getColumnProperties(BCPPurchaseTransaction.class));
		parser.setColumnPropertiesSale(batchMaintenanceDAO.getColumnProperties(BCPBillTransaction.class));
		processUpdateInBackground(newBatch);
	}

	public String closeAction() {
		background = null;

		// a new batch has been created, so clear selection
		if (importMapBean != null) {
			importMapBean.getBatchMaintenanceDataModel().refreshData(false);
			importMapBean.setSelectedRowIndex(-1);
		}
		newBatch = null;
		return "importmap_main";
	}

	public String cancelAction() {
		if (parser != null) {
			parser.setAbort(true);
			try {
				int secCount = 0;
				while (!parser.wasCompleted() && secCount < 50) {
					Thread.sleep(1000);
					secCount++;
				}

			} catch (Exception ex) {
			}
		}

		return closeAction();
	}

	private void processUpdateInBackground(final BatchMaintenance batch) {
		String selectedDb = ChangeContextHolder.getDataBaseType();
		dbPropertiesDTO = dbPropertiesBean.getDbPropertiesDTO(selectedDb);

		parser.setTransactionColumnDriverNamesMap(cacheManager
				.getTransactionColumnDriverNamesMap(null));
		parser.setBCPTransactionsColumnMap(cacheManager
				.getTransactionsColumnMap());// Use TB_TRANSACTION_DETAIL
		parser.setBCPTransactionsColumnMapSale(cacheManager
				.getSaleTransactionsColumnMap());// Use TB_SALETRANS

		parser.setErrorListCodes(cacheManager.getErrorListCodes());
		parser.setImportSpec(selectedImportSpec);

		// 5427 Convert to upper for import headers
		// Just want to get these data before running Thread
		getMappedColumns();

		state = AddState.PROCESSING;
		backgroundError = null;
		background = new Thread(new Runnable() {
			public void run() {
				System.out.println("############ processUpdateInBackground for Import File using URL: " + dbPropertiesDTO.getUrl());
				try {
					Class.forName(dbPropertiesDTO.getDriverClassName());
				} catch (ClassNotFoundException e) {
					System.out.println("xxxxxxxxxxxxxx Can't load class: "
							+ dbPropertiesDTO.getDriverClassName());
					return;
				}
				Connection conn = null;

				try {
					parser.setCompleted(false);

					// Standalone connection
					conn = DriverManager.getConnection(
							dbPropertiesDTO.getUrl(),
							dbPropertiesDTO.getUserName(),
							dbPropertiesDTO.getPassword());
					conn.setAutoCommit(false);

					batch.setVc01(selectedImportSpec.getImportSpecCode());
					batch.setEntryTimestamp(new Date());

					// Import file
					importMapUpdatesUsingJDBC(batch, parser, conn);
					if (parser.getErrorCount() > 0) {
						batch.setHeldFlag("1");
					}

					if (parser.getMaxErrorSeen() < 10) {
						// Perfect, no error.
					} else if (parser.getMaxErrorSeen() == 10) {
						batch.setErrorSevCode(BatchMaintenance.ErrorState.Warning);
					} else if (parser.getMaxErrorSeen() == 20) {
						batch.setErrorSevCode(BatchMaintenance.ErrorState.Error);
					} else if (parser.getMaxErrorSeen() >= 30) {
						batch.setErrorSevCode(BatchMaintenance.ErrorState.Terminal);
					}
					batch.setNu01(parser.getComputedTotalAmount().doubleValue());
					batch.setVc01(selectedImportSpec.getImportSpecCode());

					if (parser.wasAborted()) {
						batch.setBatchStatusCode("ABI");
					} else {
						batch.setBatchStatusCode("I");
					}
					batch.setTotalRows(new Long(parser.getTotalRows()));
					batch.setStatusUpdateTimestamp(new Date());
					batch.setStatusUpdateUserId(batch.getUpdateUserId());
					batch.setUpdateUserId(batch.getUpdateUserId());
					batch.setUpdateTimestamp(new Date());
					updateBatchUsingJDBC(batch, conn);
					// final clean up
					if (batch.getBatchStatusCode() != null && batch.getBatchStatusCode().equalsIgnoreCase("I")) {
						parser.postImport();
					}

					parser.setCompleted(true);
				} catch (Throwable e) {
					e.printStackTrace();

					if (e.getCause() != null) {
						e.getCause().printStackTrace();
					}

					parser.setCompleted(true);
					logger.debug(e, e);
					backgroundError = "Unable to save batch: " + e.getMessage();
				} finally {
					// 0005203: To prevent returning quickly for small file.
					// force completion
					state = AddState.COMPLETE;
					
					if (conn != null)
						try {
							conn.close();
						} catch (SQLException ignore) {
						}
				}
			}
		});
		background.start();
		try {
			Thread.sleep(1000);
		} catch (Exception ex) {
		}
		logger.debug("Background started");
	}

	protected void updateBatchUsingJDBC(BatchMaintenance batchMaintenance,
			Connection conn) throws Exception {
		String sql = batchMaintenance.getRawUpdateStatement();
		PreparedStatement prep = conn.prepareStatement(sql);
		batchMaintenance.populateUpdatePreparedStatement(conn, prep);
		prep.addBatch();

		prep.executeBatch();
		conn.commit();
		prep.clearBatch();

		return;
	}

	protected void syncBatchUsingJDBC(BatchMaintenance batchMaintenance,
			Connection conn) throws Exception {
		String sql = "SELECT * FROM TB_BATCH WHERE BATCH_ID = ?";
		PreparedStatement prep = conn.prepareStatement(sql);
		prep.setLong(1, batchMaintenance.getBatchId());

		ResultSet rs = prep.executeQuery();
		if (rs != null && rs.next()) {
			batchMaintenance.setBatchTypeCode(rs.getString("BATCH_TYPE_CODE"));
			batchMaintenance.setEntryTimestamp(rs
					.getTimestamp("ENTRY_TIMESTAMP"));
			batchMaintenance.setBatchStatusCode(rs
					.getString("BATCH_STATUS_CODE"));
			batchMaintenance.setHeldFlag(rs.getString("HELD_FLAG"));
			batchMaintenance.setErrorSevCode(rs.getString("ERROR_SEV_CODE"));
			batchMaintenance.setTotalBytes(rs.getLong("TOTAL_BYTES"));
			batchMaintenance.setTotalRows(rs.getLong("TOTAL_ROWS"));
			batchMaintenance.setStartRow(rs.getLong("START_ROW"));
			batchMaintenance.setProcessedBytes(rs.getLong("PROCESSED_BYTES"));
			batchMaintenance.setProcessedRows(rs.getLong("PROCESSED_ROWS"));
			batchMaintenance.setSchedStartTimestamp(rs
					.getTimestamp("SCHED_START_TIMESTAMP"));
			batchMaintenance.setActualStartTimestamp(rs
					.getTimestamp("ACTUAL_START_TIMESTAMP"));
			batchMaintenance.setActualEndTimestamp(rs
					.getTimestamp("ACTUAL_END_TIMESTAMP"));
			batchMaintenance.setVc01(rs.getString("VC01"));
			batchMaintenance.setVc02(rs.getString("VC02"));
			batchMaintenance.setVc03(rs.getString("VC03"));
			batchMaintenance.setVc04(rs.getString("VC04"));
			batchMaintenance.setVc05(rs.getString("VC05"));
			batchMaintenance.setVc06(rs.getString("VC06"));
			batchMaintenance.setVc07(rs.getString("VC07"));
			batchMaintenance.setVc08(rs.getString("VC08"));
			batchMaintenance.setVc09(rs.getString("VC09"));
			batchMaintenance.setVc10(rs.getString("VC10"));
			batchMaintenance.setNu01(rs.getDouble("NU01"));
			batchMaintenance.setNu02(rs.getDouble("NU02"));
			batchMaintenance.setNu03(rs.getDouble("NU03"));
			batchMaintenance.setNu04(rs.getDouble("NU04"));
			batchMaintenance.setNu05(rs.getDouble("NU05"));
			batchMaintenance.setNu06(rs.getDouble("NU06"));
			batchMaintenance.setNu07(rs.getDouble("NU07"));
			batchMaintenance.setNu08(rs.getDouble("NU08"));
			batchMaintenance.setNu09(rs.getDouble("NU09"));
			batchMaintenance.setNu10(rs.getDouble("NU10"));
			batchMaintenance.setTs01(rs.getDate("TS01"));
			batchMaintenance.setTs02(rs.getDate("TS02"));
			batchMaintenance.setTs03(rs.getDate("TS03"));
			batchMaintenance.setTs04(rs.getDate("TS04"));
			batchMaintenance.setTs05(rs.getDate("TS05"));
			batchMaintenance.setTs06(rs.getDate("TS06"));
			batchMaintenance.setTs07(rs.getDate("TS07"));
			batchMaintenance.setTs08(rs.getDate("TS08"));
			batchMaintenance.setTs09(rs.getDate("TS09"));
			batchMaintenance.setTs10(rs.getDate("TS10"));
			batchMaintenance.setStatusUpdateUserId(rs
					.getString("STATUS_UPDATE_USER_ID"));
			batchMaintenance.setStatusUpdateTimestamp(rs
					.getTimestamp("STATUS_UPDATE_TIMESTAMP"));
			batchMaintenance.setUpdateUserId(rs.getString("UPDATE_USER_ID"));
			batchMaintenance.setUpdateTimestamp(rs
					.getTimestamp("UPDATE_TIMESTAMP"));
		}

		prep.close();
	}

	protected BatchMaintenance importMapUpdatesUsingJDBC(
			BatchMaintenance batchMaintenance, ImportMapUploadParser parser,
			Connection conn) throws Exception {

		Iterator<BCPTransaction> truIter = parser.iterator();
		if (outAID != null) {
			writeLogAID("   ... importMapUpdatesUsingJDBC");
		}
		
		//"The Definition has mapped columns that are not in the import file."
		List<String> headerNames = new ArrayList<String>(); 
		for (String s : parser.getHeaders()) {
			headerNames.add(s == null ? "" : s.toUpperCase());
		}
		for (ImportDefinitionDetail detail : getMappedColumns()) {
			if (!headerNames.contains(detail.getImportColumnName().toUpperCase())) {
				//parser.addError(new BatchErrorLog("IP13", 1, "", "", detail.getImportColumnName().toUpperCase()));
				BatchErrorLog aBatchErrorLog = new BatchErrorLog("IP13", 0, "", "", null);	
				aBatchErrorLog.setImportHeaderColumn(detail.getImportColumnName().toUpperCase());
				aBatchErrorLog.setTransDtlColumnName(detail.getTransDtlColumnName());
				parser.addError(aBatchErrorLog);
				
				if (outAID != null) {
					writeLogAID("   ... Column mapped in Definition not in the Import File - " + detail.getImportColumnName().toUpperCase() + ", " + detail.getTransDtlColumnName());
				}
			}
		}
		
		//Import file columns not mapped in the Definition.
		List<ImportDefinitionDetail> mappedColumns = getMappedColumns();
		Set<String> localMappedColumns = new HashSet<String>();
		for (ImportDefinitionDetail detail : mappedColumns) {
			localMappedColumns.add(detail.getImportColumnName().toUpperCase());
		}

		for (String headerName : parser.getHeaders()) {
			if (!localMappedColumns.contains(headerName.toUpperCase())) {
				BatchErrorLog aBatchErrorLog = new BatchErrorLog("IP9", 0, "", "", null);	
				aBatchErrorLog.setImportHeaderColumn(headerName.toUpperCase());
				parser.addError(aBatchErrorLog);
				
				if (outAID != null) {
					writeLogAID("   ... Import file columns not mapped in the Definition - " + headerName.toUpperCase());
				}
			}
		}

		String sql = null;
		PreparedStatement prep = null;
		Statement stat = conn.createStatement();

		String sqlSale = null;
		PreparedStatement prepSale = null;
		Statement statSale = conn.createStatement();

		if (outAID != null) {
			writeLogAID("   ... before loop");
		}

		long nextBcpId = 0;
		boolean usingBcpTransactionId = true;
		if (batchMaintenance.getBatchTypeCode().equalsIgnoreCase("IP")
				|| batchMaintenance.getBatchTypeCode().equalsIgnoreCase("PCO")) {
			usingBcpTransactionId = true;
		} else {
			usingBcpTransactionId = false;
		}

		int i = 1;
		while (truIter.hasNext()) {
			BCPTransaction bcpTransaction = truIter.next();
			if (bcpTransaction == null) {
				continue;
			}

			BCPPurchaseTransaction td = bcpTransaction.getBcpPurchaseTransaction();
			BCPBillTransaction tdSale = bcpTransaction.getBcpSaleTransaction();
			if (td != null || tdSale != null) {
				if (usingBcpTransactionId) {
					nextBcpId = getNextBcpTransactionId(stat);
				} else {
					nextBcpId = getNextBcpSaletransId(statSale);
				}

				if (td != null) {
					if (prep == null) {
						sql = td.getRawInsertStatement();
						prep = conn.prepareStatement(sql);
					}

					td.setProcessBatchNo(batchMaintenance.getbatchId());		
					td.setBcpPurchtransId(nextBcpId);

					// populate data to Prepared Statement
					td.populatePreparedStatement(conn, prep);
					prep.addBatch();
				}

				if (tdSale != null) {
					if (prepSale == null) {
						sqlSale = tdSale.getRawInsertStatement();
						prepSale = conn.prepareStatement(sqlSale);
					}

					//tdSale.setBilltransId(null); // 0005203, remove lineNumber
					tdSale.setProcessBatchNo(batchMaintenance.getbatchId());

					// 0005203
					tdSale.setBcpBilltransId(nextBcpId);

					// populate data to Prepared Statement
					tdSale.populatePreparedStatement(conn, prepSale);
					prepSale.addBatch();

				}

				if ((i++ % 25) == 0) {
					logger.debug("Begin Flush: " + (i - 1));

					if (outAID != null) {
						writeLogAID("   ... Begin Flush: record " + (i - 1));
					}

					if (prep != null)
						prep.executeBatch();
					if (prepSale != null)
						prepSale.executeBatch();
					conn.commit();
					if (prep != null)
						prep.clearBatch();
					if (prepSale != null)
						prepSale.clearBatch();

					logger.debug("End Flush");

					if (outAID != null) {
						writeLogAID("   ... End Flush");
					}

					parser.setLinesWritten(i - 1);
				}
				td = null;
				tdSale = null;
			}

			// 0001701: Import errors make the system crash
			if (parser.getBatchErrorLogs().size() >= 50) {
				// flush every 50+ errors
				flushErrorLog(conn, batchMaintenance);
			}
		}

		if (prep != null || prepSale != null) {
			if (outAID != null) {
				writeLogAID("   ... Begin Final Flush: record " + (i - 1));
			}

			if (prep != null)
				prep.executeBatch();
			if (prepSale != null)
				prepSale.executeBatch();
			conn.commit();

			if (prep != null)
				prep.clearBatch();
			if (prepSale != null)
				prepSale.clearBatch();

			if (outAID != null) {
				writeLogAID("   ... End Final Flush");
			}

			try {
				if (prep != null)
					prep.close();
				if (prepSale != null)
					prepSale.close();
			} catch (Exception ex) {
			}
		}
		if (outAID != null) {
			writeLogAID("   ### Total Rows Imported: " + (i - 1));
		}

		// perform the checksum and generate any more ErrorLogs
		parser.checksum();

		parser.setLinesWritten(i - 1);

		// 0001701: Import errors make the system crash
		flushErrorLog(conn, batchMaintenance);

		if (outAID != null) {
			writeLogAID("   ### Total Non-Warning Errors: "
					+ parser.getErrorCount());
		}
		if (outAID != null) {
			writeLogAID("   ### Total Error Count: "
					+ parser.getErrorCountTotal());
		}

		if (stat != null || statSale != null) {
			try {
				if (stat != null)
					stat.close();
				if (statSale != null)
					statSale.close();
			} catch (Exception ex) {
			}
		}

		return batchMaintenance;
	}

	void flushErrorLog(Connection conn, BatchMaintenance batchMaintenance)
			throws Exception {
		// 0001701: Import errors make the system crash
		// Save error if needed
		
		Map<String, ListCodes> errorListCodes = parser.getErrorListCodes();
		ListCodes listCode = null;
		
		String sqlError = null;
		PreparedStatement prepError = null;

		Statement statErrorNext = null;
		int ie = 1;
		for (BatchErrorLog error : parser.getBatchErrorLogs()) {
			if (prepError == null) {
				sqlError = error.getRawInsertStatement();
				prepError = conn.prepareStatement(sqlError);
				statErrorNext = conn.createStatement();
			}
			
			error.truncateValues();
			error.setBatchErrorLogId(getNextBatchErrorLogId(statErrorNext));
			error.setProcessType("I");
			error.setProcessTimestamp(batchMaintenance.getEntryTimestamp());
			error.setBatchId(batchMaintenance.getBatchId());
			error.populatePreparedStatement(conn, prepError);
			prepError.addBatch();

			if ((ie++ % 50) == 0) {
				prepError.executeBatch();
				conn.commit();
				prepError.clearBatch();
			}

			if (outAID != null) {
				listCode = errorListCodes.get(error.getErrorDefCode());
				
				String errorStr = "   ... Error: RowNo: " + error.getRowNo()
						+ ", Column No: " + error.getColumnNo()
						+ ", Error Def Code: " + error.getErrorDefCode() 
						+ "; " + ((listCode!=null) ? listCode.getDescription() : "Error code not found"  )
						+ ", Import Header Column: "
						+ error.getImportHeaderColumn()
						+ ", Import Header Value: "
						+ error.getImportColumnValue()
						+ ", Trans Dtl Column Name: "
						+ error.getTransDtlColumnName();
				writeLogAID(errorStr);
			}
		}
		if (prepError != null) {
			prepError.executeBatch();
			conn.commit();
			prepError.clearBatch();

			try {
				prepError.close();
			} catch (Exception ex) {
			}
		}

		if (statErrorNext != null) {
			try {
				statErrorNext.close();
			} catch (Exception ex) {
			}
		}

		parser.resetBatchErrorLogs();

	}

	private long getNextBcpTransactionId(Statement stat) {
		long nextId = -1;
		try {
			String sql = DatabaseUtil.getSequenceQuery(
					DatabaseUtil.getDatabaseProductName(stat),
					"sq_tb_bcp_purchtrans_id", "NEXTID");
			ResultSet rs = stat.executeQuery(sql);
			if (rs.next()) {
				nextId = rs.getLong("NEXTID");
			}
			rs.close();
		} catch (Exception e) {
			logger.error("############ getNextBcpTransactionId() failed: "
					+ e.toString());
		}

		return nextId;
	}

	private long getNextBcpSaletransId(Statement stat) {
		long nextId = -1;
		try {
			String sql = DatabaseUtil.getSequenceQuery(
					DatabaseUtil.getDatabaseProductName(stat),
					"SQ_TB_BCP_BILLTRANS_ID", "NEXTID");
			ResultSet rs = stat.executeQuery(sql);
			if (rs.next()) {
				nextId = rs.getLong("NEXTID");
			}
			rs.close();
		} catch (Exception e) {
			logger.error("############ getNextBcpSaletransId() failed: "
					+ e.toString());
		}

		return nextId;
	}

	private long getNextBatchErrorLogId(Statement statErrorNext) {
		long nextId = -1;
		try {
			String sql = DatabaseUtil.getSequenceQuery(
					DatabaseUtil.getDatabaseProductName(statErrorNext),
					"sq_tb_batch_error_log_id", "NEXTID");

			// Statement stat = conn.createStatement();
			ResultSet rs = statErrorNext.executeQuery(sql);
			if (rs.next()) {
				nextId = rs.getLong("NEXTID");
			}
			rs.close();
		} catch (Exception e) {
			logger.error("############ getNextBatchErrorLogId() failed: "
					+ e.toString());
		} finally {
		}

		return nextId;
	}

	/*
	 * private void processUpdateInBackground(final BatchMaintenance batch) {
	 * state=AddState.PROCESSING; backgroundError = null; background = new
	 * Thread(new Runnable() { public void run() { try {
	 * parser.setCompleted(false);
	 * 
	 * batch.setVc01(selectedImportSpec.getImportSpecCode());
	 * batch.setVc02(selectedFile.getName()); batch.setEntryTimestamp(new
	 * Date()); Iterator<BCPTransactionDetail> iter = parser.iterator();
	 * batchMaintenanceDAO.importMapUpdates(batch, parser);
	 * 
	 * // force completion state=AddState.COMPLETE; if (parser.getErrorCount() >
	 * 0) { batch.setHeldFlag("1");
	 * batch.setErrorSevCode(BatchMaintenance.ErrorState.Error); }
	 * batch.setNu01(parser.getComputedTotalAmount());
	 * batch.setVc01(selectedImportSpec.getImportSpecCode());
	 * 
	 * if (parser.wasAborted()) { batch.setBatchStatusCode("ABI"); } else {
	 * batch.setBatchStatusCode("I"); } batch.setTotalRows(new
	 * Long(parser.getTotalRows())); batch.setStatusUpdateTimestamp(new Date());
	 * batch.setStatusUpdateUserId(batch.getUpdateUserId());
	 * batch.setUpdateUserId(batch.getUpdateUserId());
	 * batch.setUpdateTimestamp(new Date()); batchMaintenanceDAO.update(batch);
	 * 
	 * parser.setCompleted(true); } catch (Throwable e) {
	 * parser.setCompleted(true); logger.debug(e, e); backgroundError =
	 * "Unable to save batch: " + e.getMessage(); } } }); background.start();
	 * logger.debug("Background started"); }
	 */

	public Map<String, PropertyDescriptor> getBCPTransactionDetailColumnProperties(
			Class<?> clazz) {
		HashMap<String, PropertyDescriptor> map = new HashMap<String, PropertyDescriptor>();
		Field[] fields = clazz.getDeclaredFields();
		for (Field f : fields) {
			Column col = f.getAnnotation(Column.class);
			if (col != null) {
				try {
					String s = f.getName();
					PropertyDescriptor propertyDescriptor = BeanUtils
							.getPropertyDescriptor(clazz, s);

					map.put(col.name(), propertyDescriptor);

				} catch (Exception e) {
				}
			}
		}

		// Need to include AuditListener.class
		try {
			String s = "updateUserId";
			PropertyDescriptor propertyDescriptor = BeanUtils
					.getPropertyDescriptor(clazz, "updateUserId"); // Use
																	// BCPTransactionDetail.class
																	// here.
			map.put("UPDATE_USER_ID", propertyDescriptor);

			s = "updateTimestamp";
			propertyDescriptor = BeanUtils.getPropertyDescriptor(clazz,
					"updateTimestamp"); // Use BCPTransactionDetail.class here.

			map.put("UPDATE_TIMESTAMP", propertyDescriptor);

		} catch (Exception e) {
		}

		return map;
	}

	public static BatchMaintenance addBatchUsingJDBC(
			BatchMaintenance batchMaintenance, Connection conn) {
		// Get next batch Id
		long nextId = -1;
		Statement stat = null;
		try {
			stat = conn.createStatement();

			String sql = DatabaseUtil.getSequenceQuery(
					DatabaseUtil.getDatabaseProductName(stat),
					"sq_tb_batch_id", "NEXTID");

			ResultSet rs = stat.executeQuery(sql);
			if (rs.next()) {
				nextId = rs.getLong("NEXTID");
			}
			rs.close();
		} catch (Exception e) {
			return null;
		} finally {
			if (stat != null) {
				try {
					stat.close();
				} catch (Exception ex) {
				}
			}
		}

		// Set new batch id
		batchMaintenance.setBatchId(nextId);
		PreparedStatement prep = null;
		try {
			String sql = batchMaintenance.getRawAddStatement();
			prep = conn.prepareStatement(sql);
			batchMaintenance.populateAddPreparedStatement(conn, prep);
			prep.addBatch();
			prep.executeBatch();
			conn.commit();
		} catch (Exception e) {
			return null;
		} finally {
			if (prep != null) {
				try {
					prep.close();
				} catch (Exception ex) {
				}
			}
		}

		return batchMaintenance;
	}

	public boolean getIsDatabaseImport() {
		if (selectedDefinition != null
				&& selectedDefinition.getImportFileTypeCode() != null
				&& selectedDefinition.getImportFileTypeCode().equalsIgnoreCase(
						"DB")) {
			return true;
		} else {
			return false;
		}
	}

	public void selectedRowChanged(ActionEvent e)
			throws AbortProcessingException {
		UIComponent uiComponent = e.getComponent().getParent();
		UIDataAdaptor table = (UIDataAdaptor) uiComponent;
		this.selectedImportSpec = (ImportSpec) table.getRowData();
		this.selectedRowIndex = table.getRowIndex();
		this.selectedFile = null;
		selectedDefinition = importDefAndSpecsService.find(selectedImportSpec
				.getImportDefinitionCode());
		if (selectedDefinition == null) {
			JsfHelper.addError("Could not find definition for "
					+ selectedImportSpec.getImportDefinitionCode());
			return;
		}

		if (getIsDatabaseImport()) {
			verificationMessage = "";
			allowDatabaseImport = false;
			state = AddState.HAVE_FILE; // Upload from other database
			return;
		}

		try {
			selectMenu.setValue("");
			fileItems = new ArrayList<SelectItem>();
			fileItems.add(new SelectItem("", "Select File"));
			File file = new File(selectedImportSpec.getDefaultDirectory());
			List<String> files = new ArrayList<String>();
			for (File f : file.listFiles()) {
				files.add(f.getName());
			}
			Collections.sort(files);
			for (String f : files) {
				fileItems.add(new SelectItem(f, f));
			}
			final UISelectItems items = new UISelectItems();
			items.setValue(fileItems);
			selectMenu.getChildren().clear();
			selectMenu.getChildren().add(items);

		} catch (Exception ex) {
			fileItems = null;
			JsfHelper.addError("Default directory not available: "
					+ selectedImportSpec.getDefaultDirectory());
		}
		state = AddState.NEED_FILE;
	}

	public void getFileDetails(ActionEvent e) {
		HtmlSelectOneMenu selectItems = (HtmlSelectOneMenu) e.getComponent()
				.getParent();
		String fileName = (String) selectItems.getValue();
		this.selectedFile = null;
		this.mappedColumns = null;
		// in case this process fails and we no longer have a valid selected
		// file.
		String msg = "";
		
		state = AddState.NEED_FILE;
		if ((fileName == null) || (fileName.trim().length() == 0))
			return;
		try {
			ImportDefinition def = new ImportDefinition(selectedDefinition);
			def.setSampleFileName(selectedImportSpec.getDefaultDirectory()
					+ File.separator + fileName);
			parser = new ImportMapUploadParser(def, preferences);
			parser.setImportSpec(selectedImportSpec);
			
			//The Definition has mapped column(s) that are not in the import file
			Set<String> headerNames = new HashSet<String>();
			for (String s : parser.getHeaders()) {
				headerNames.add(s == null ? "" : s.toUpperCase());
			}

			String mapError = "";
			for (ImportDefinitionDetail detail : getMappedColumns()) {
				if (!headerNames.contains(detail.getImportColumnName().toUpperCase())) {
					if(mapError.length()>0){
						mapError = mapError + ", ";
					}
					mapError = mapError + detail.getImportColumnName().toUpperCase();
				}
			}
			if(mapError.length()>0){
				msg = "The Definition has mapped column(s) that are not in the import file: " + mapError;
			}

			if (preferences == null) {
				preferences = filePreferenceBean
						.getImportMapProcessPreferenceDTO();
			}
			BatchMaintenance nameBatch = null;

			if (DuplicationRestriction.get(selectedImportSpec
					.getFilenamedupres()) != (DuplicationRestriction.None)) {
				nameBatch = validateFileNames(fileName, getCurrentBatches());
			}
			BatchMaintenance sizeBatch = null;
			if (DuplicationRestriction.get(selectedImportSpec
					.getFilesizedupres()) != (DuplicationRestriction.None)) {
				sizeBatch = validateFileLengths(fileName, getCurrentBatches());
			}
			
			if ((sizeBatch != null) && (nameBatch != null)) {
				if(msg.length()>0){
					msg = msg + "; ";
				}
				msg = msg + "Potential duplicate file name with Batch Id: "
						+ nameBatch.getBatchId() + ", size: "
						+ sizeBatch.getBatchId();
			} else if (nameBatch != null) {
				if(msg.length()>0){
					msg = msg + "; ";
				}
				msg = msg + "Potential duplicate file name with Batch Id: "
						+ nameBatch.getBatchId();
			} else if (sizeBatch != null) {
				if(msg.length()>0){
					msg = msg + "; ";
				}
				msg = msg + "Potential duplicate file size with Batch Id: "
						+ sizeBatch.getBatchId();
			}
			
			if ((nameBatch != null)
					&& (DuplicationRestriction.get(selectedImportSpec
							.getFilenamedupres()) == (DuplicationRestriction.Full))) {
				JsfHelper.addError(selectItems, msg);
				return;
			} else if ((sizeBatch != null)
					&& (DuplicationRestriction.get(selectedImportSpec
							.getFilesizedupres()) == (DuplicationRestriction.Full))) {
				JsfHelper.addError(selectItems, msg);
				return;
			}
			if (msg!=null && msg.length()>0) {
				JsfHelper.addWarning(selectItems, msg);
			}
			
			parser.parseFile();
			this.bytesInFile = parser.getFileSize();
			this.estimatedRows = parser.getEstimatedRows();
			// enable the OK button
			this.selectedFile = new File(fileName);
			state = AddState.HAVE_FILE;
		} catch (Exception ex) {
			logger.error(ex);
			JsfHelper.addError(selectItems,
					"Could not read file: " + ex.getMessage());
		}
	}

	private boolean isFileAllColumnsMapped() {
		boolean returnCode = true;

		List<ImportDefinitionDetail> mappedColumns = getMappedColumns();
		Set<String> localMappedColumns = new HashSet<String>();
		for (ImportDefinitionDetail detail : mappedColumns) {
			localMappedColumns.add(detail.getImportColumnName().toUpperCase());
		}

		List<String> headerNames = parser.getHeaders();
		for (String headerName : headerNames) {
			if (!localMappedColumns.contains(headerName.toUpperCase())) {
				returnCode = false;
				break;
			}
		}

		return returnCode;
	}
	
	private void logMissingColumns() {
		List<ImportDefinitionDetail> mappedColumns = getMappedColumns();
		Set<String> localMappedColumns = new HashSet<String>();
		for (ImportDefinitionDetail detail : mappedColumns) {
			localMappedColumns.add(detail.getImportColumnName().toUpperCase());
		}

		List<String> headerNames = parser.getHeaders();
		for (String headerName : headerNames) {
			if (!localMappedColumns.contains(headerName.toUpperCase())) {
				writeLogAID("          " + headerName.toUpperCase());
			}
		}
	}

	private BatchMaintenance validateFileNames(String fileName,
			List<BatchMaintenance> list) {
		for (BatchMaintenance bm : list) {
			if ((!bm.getBatchStatusCode().startsWith("AB"))
					&& (bm.getVc02() != null)
					&& (bm.getVc02().trim().length() > 0)) {
				if (fileName.equalsIgnoreCase(bm.getVc02())) {
					return bm;
				}
			}
		}
		return null;
	}

	private BatchMaintenance validateFileLengths(String fileName,
			List<BatchMaintenance> list) {
		for (BatchMaintenance bm : list) {
			try {
				if ((!bm.getBatchStatusCode().equalsIgnoreCase("ABI"))
						&& (bm.getVc02() != null)
						&& (bm.getVc02().trim().length() > 0)
						&& (bm.getTotalBytes() == parser.getFileSize())) {
					logger.debug("Size match: " + fileName + ", "
							+ bm.getVc02());
					return bm;
				}
			} catch (Exception e) {
				logger.error(e);
			}
		}
		return null;
	}

	// expensive, so do as little as often
	private List<BatchMaintenance> getCurrentBatches() {
		if (currentBatches == null) {
			Criteria criteria = batchMaintenanceDAO.createCriteria();

			if (selectedImportSpec.getImportSpecType() != null
					&& selectedImportSpec.getImportSpecType().equalsIgnoreCase(
							"IP")) {
				criteria.add(Expression.eq("batchTypeCode", "IP"));
			} else if (selectedImportSpec.getImportSpecType() != null
					&& selectedImportSpec.getImportSpecType().equalsIgnoreCase(
							"IPS")) {
				criteria.add(Expression.eq("batchTypeCode", "IPS"));
			} else {
				criteria.add(Expression.eq("batchTypeCode", "PCO"));
			}

			// get all but deleted batches
			criteria.add(Expression.ne("batchStatusCode", "D"));
			currentBatches = batchMaintenanceDAO.find(criteria);
		}
		return currentBatches;
	}

	private List<ImportDefinitionDetail> getMappedColumns() {
		if ((this.mappedColumns == null) && (selectedDefinition != null)) {
			this.mappedColumns = importDefinitionService
					.getAllHeaders(selectedDefinition.getImportDefinitionCode());
		}
		return mappedColumns;
	}

	public boolean getDisplayFileSelector() {
		return (fileItems != null) && (state != AddState.SELECT_SPEC);
	}

	public boolean getEnableFileSelector() {
		return (state == AddState.NEED_FILE) && (fileItems != null);
	}

	// public boolean getDisplayFileSelectorButton() {
	// return (this.selectedFile != null);
	// }

	public boolean getDisplayOk() {
		return (state == AddState.HAVE_FILE);
	}

	public boolean getDisplayDatabaseOk() {
		return (state == AddState.HAVE_FILE && selectedDefinition != null
				&& selectedDefinition.getImportFileTypeCode() != null && getIsDatabaseImport());
	}

	public boolean getDisplayComplete() {
		return (state == AddState.COMPLETE);
	}

	public boolean getDisplayProgress() {
		return state == AddState.PROCESSING;
	}

	public boolean getDisplayBatch() {
		if (state == AddState.PROCESSING) {
			return true;
		} else if (state == AddState.COMPLETE) {
			return true;
		} else {
			return false;
		}
	}

	public String getProgress() {
		if (state == AddState.COMPLETE) {
			return "100.0";
		} else if ((parser == null) || (state != AddState.PROCESSING)) {
			return "0.0";
		} else if (backgroundError != null) {
			JsfHelper.addError(backgroundError);
			background = null;
			state = AddState.COMPLETE;
		}
		NumberFormat nf = NumberFormat.getNumberInstance();
		nf.setMaximumFractionDigits(1);
		String s = "0.0";
		Double scale = parser.getProgress() * 99.0;

		if (!parser.wasAborted()) {
			if (scale >= 99.0) {
				// 0005203: To prevent returning quickly for small file.
				scale = new Double(98.9);
			}
		}

		if (scale >= 0.01) {
			s = nf.format(scale);
		}
		logger.debug("Progress at: " + s);
		return s;
	}

	public long getBytesInFile() {
		return bytesInFile;
	}

	public long getEstimatedRows() {
		return estimatedRows;
	}

	public BatchMaintenance getNewBatch() {
		if (this.newBatch == null) {
			return new BatchMaintenance();
		}
		return this.newBatch;
	}

	public ImportMapService getImportMapService() {
		return importMapService;
	}

	public void setImportMapService(ImportMapService importMapService) {
		this.importMapService = importMapService;
	}

	public ImportDefAndSpecsService getImportDefAndSpecsService() {
		return importDefAndSpecsService;
	}

	public void setImportDefAndSpecsService(
			ImportDefAndSpecsService importDefAndSpecsService) {
		this.importDefAndSpecsService = importDefAndSpecsService;
	}

	public ImportDefinitionService getImportDefinitionService() {
		return importDefinitionService;
	}

	public void setImportDefinitionService(
			ImportDefinitionService importDefinitionService) {
		this.importDefinitionService = importDefinitionService;
	}

	public List<ImportSpec> getImportSpecList() {
		if (this.importSpecList == null) {
			if (importMapBean.getIsPurchasingMenu()) {
				this.importSpecList = importDefAndSpecsService
						.getAllForUserByType("IP");
			} else {
				this.importSpecList = importDefAndSpecsService
						.getAllForUserByType("IPS");
			}

			Option option = optionService.findByPK(new OptionCodePK("PCO",
					"ADMIN", "ADMIN"));
			String option_pco = (option == null || option.getValue() == null) ? "0"
					: option.getValue();
			boolean option_ip = loginservice.getHasPurchaseLicense();
			if (option_ip && option_pco.equals("1")) {
				List<ImportSpec> importSpecListPco = importDefAndSpecsService
						.getAllForUserByType("PCO");
				if (importSpecListPco != null && importSpecListPco.size() > 0) {
					importSpecList.addAll(importSpecListPco);
				}
			}
		}
		return this.importSpecList;
	}

	public ImportSpec getSelectedImportSpec() {
		return selectedImportSpec;
	}

	public int getSelectedRowIndex() {
		return this.selectedRowIndex;
	}

	public ImportDefinition getSelectedDefinition() {
		return this.selectedDefinition;
	}

	public BatchMaintenanceDAO getBatchMaintenanceDAO() {
		return this.batchMaintenanceDAO;
	}

	public void setBatchMaintenanceDAO(BatchMaintenanceDAO batchMaintenanceDAO) {
		this.batchMaintenanceDAO = batchMaintenanceDAO;
	}

	public File getSelectedFile() {
		return this.selectedFile;
	}

	public FilePreferenceBackingBean getFilePreferenceBean() {
		return this.filePreferenceBean;
	}

	public void setFilePreferenceBean(
			FilePreferenceBackingBean filePreferenceBean) {
		this.filePreferenceBean = filePreferenceBean;
	}

	public HtmlSelectOneMenu getSelectMenu() {
		return this.selectMenu;
	}

	public void setSelectMenu(HtmlSelectOneMenu selectMenu) {
		this.selectMenu = selectMenu;
	}

	public Collection<SelectItem> getFileItems() {
		return this.fileItems;
	}

	public ImportMapBean getImportMapBean() {
		return this.importMapBean;
	}

	public void setImportMapBean(ImportMapBean importMapBean) {
		this.importMapBean = importMapBean;
	}

	public void setCacheManager(CacheManager cacheManager) {
		this.cacheManager = cacheManager;
	}

	public void setDbPropertiesBean(DbPropertiesBean dbPropertiesBean) {
		this.dbPropertiesBean = dbPropertiesBean;
	}

	public ImportSpec getImportSpecAID(String specCode)
			throws DataAccessException {
		ImportSpec importSpec = null;
		Statement stat = null;
		try {
			stat = conn.createStatement();
			ResultSet rs = stat.executeQuery("select * from TB_IMPORT_SPEC where IMPORT_SPEC_TYPE in ('IP','PCO','IPS') and UPPER(IMPORT_SPEC_CODE)='"
							+ specCode.toUpperCase() + "'");
			if (rs.next()) {
				importSpec = new ImportSpec();
				ImportSpecPK importSpecPK = new ImportSpecPK();
				importSpecPK.setImportSpecType(rs.getString("IMPORT_SPEC_TYPE"));
				importSpecPK.setImportSpecCode(rs.getString("IMPORT_SPEC_CODE"));
				importSpec.setImportSpecPK(importSpecPK);

				importSpec.setDescription(rs.getString("DESCRIPTION"));
				importSpec.setImportDefinitionCode(rs.getString("IMPORT_DEFINITION_CODE"));
				importSpec.setDefaultDirectory(rs.getString("DEFAULT_DIRECTORY"));
				importSpec.setLastImportFileName(rs.getString("LAST_IMPORT_FILE_NAME"));
				importSpec.setWhereClause(rs.getString("WHERE_CLAUSE"));
				importSpec.setComments(rs.getString("COMMENTS"));
				importSpec.setFilesizedupres(rs.getString("FILESIZEDUPRES"));
				importSpec.setFilenamedupres(rs.getString("FILENAMEDUPRES"));
				importSpec.setImportdatemap(rs.getString("IMPORTDATEMAP"));
				importSpec.setDonotimport(rs.getString("DONOTIMPORT"));
				importSpec.setReplacewith(rs.getString("REPLACEWITH"));
				importSpec.setBatcheofmarker(rs.getString("BATCHEOFMARKER"));
				importSpec.setBatchcountmarker(rs.getString("BATCHCOUNTMARKER"));
				importSpec.setBatchtotalmarker(rs.getString("BATCHTOTALMARKER"));
				importSpec.setDelete0amtflag(rs.getString("DELETE0AMTFLAG"));
				importSpec.setHoldtransflag(rs.getString("HOLDTRANSFLAG"));
				importSpec.setHoldtransamt(rs.getLong("HOLDTRANSAMT"));
				importSpec.setAutoprocflag(rs.getString("AUTOPROCFLAG"));
				importSpec.setAutoprocamt(rs.getLong("AUTOPROCAMT"));
				importSpec.setAutoprocid(rs.getString("AUTOPROCID"));
			}
			rs.close();
		} catch (Exception e) {
			System.out.println("e.toString() : " + e.toString());
		} finally {
			if (stat != null) {
				try {
					stat.close();
				} catch (Exception ex) {
				}
			}
		}

		return importSpec;
	}

	public ImportDefinition getImportDefinitionAID(String definitionCode)
			throws DataAccessException {
		ImportDefinition importDefinition = null;
		Statement stat = null;
		try {
			stat = conn.createStatement();
			ResultSet rs = stat
					.executeQuery("select * from TB_IMPORT_DEFINITION where UPPER(IMPORT_DEFINITION_CODE)='"
							+ definitionCode.toUpperCase() + "'");
			if (rs.next()) {
				importDefinition = new ImportDefinition();
				importDefinition.setImportDefinitionCode(rs
						.getString("IMPORT_DEFINITION_CODE"));
				importDefinition.setDescription(rs.getString("DESCRIPTION"));
				importDefinition.setImportFileTypeCode(rs
						.getString("IMPORT_FILE_TYPE_CODE"));
				importDefinition.setFlatDelChar(rs.getString("FLAT_DEL_CHAR"));
				importDefinition.setFlatDelTextQual(rs
						.getString("FLAT_DEL_TEXT_QUAL"));
				importDefinition.setFlatDelLine1HeadFlag(rs
						.getString("FLAT_DEL_LINE1_HEAD_FLAG"));
				importDefinition.setComments(rs.getString("COMMENTS"));
				importDefinition.setSampleFileName(rs
						.getString("SAMPLE_FILE_NAME"));	
				importDefinition.setImportConnectionCode(rs.getString("IMPORT_CONNECTION_CODE"));
				importDefinition.setDbTableName(rs.getString("DB_TABLE_NAME")); 	
				importDefinition.setEncoding(rs.getString("ENCODING"));
			}
			rs.close();
		} catch (Exception e) {
			// logger.error("############ getImportDefinitionAID() failed: " +
			// e.toString());
		} finally {
			if (stat != null) {
				try {
					stat.close();
				} catch (Exception ex) {
				}
			}
		}

		return importDefinition;
	}
	
	public ImportConnection getImportConnectionAID(String connectionCode) throws DataAccessException {
		ImportConnection importConnection = null;
		Statement stat = null;
		try {
			stat = conn.createStatement();
			ResultSet rs = stat.executeQuery("select * from TB_IMPORT_CONNECTION where UPPER(IMPORT_CONNECTION_CODE)='"
							+ connectionCode.toUpperCase() + "'");
			if (rs.next()) {
				importConnection = new ImportConnection();
				importConnection.setImportConnectionCode(rs.getString("IMPORT_CONNECTION_CODE"));
				importConnection.setImportConnectionType(rs.getString("IMPORT_CONNECTION_TYPE"));
				importConnection.setDbDriverClassName(rs.getString("DB_DRIVER_CLASS_NAME"));
				importConnection.setDbUrl(rs.getString("DB_URL"));
				importConnection.setDbUserName(rs.getString("DB_USER_NAME"));
				importConnection.setDbPassword(rs.getString("DB_PASSWORD"));
			}
			rs.close();
		} catch (Exception e) {
			System.out.println("e.toString() : " + e.toString());
		} finally {
			if (stat != null) {
				try {
					stat.close();
				} catch (Exception ex) {
				}
			}
		}

		return importConnection;
	}

	public static DateFormat logDateformat = new SimpleDateFormat(
			"yyyy-MM-dd HH:mm:ss.SSS");
	private Connection conn = null;
	private BufferedWriter outAID = null;

	public boolean initVerification(Connection connection, BufferedWriter out,
			String specStr) {
		this.conn = connection;
		this.outAID = out;
		
		startTime = System.currentTimeMillis();
		writeLogAID("************ Time counted ..... ************");

		writeLogAID(" ");
		selectedImportSpec = getImportSpecAID(specStr);
		if (selectedImportSpec == null) {
			// No ImportSpec found
			writeLogAID("Error: Cannot find spec for " + specStr);
			return false;
		}

		selectedDefinition = getImportDefinitionAID(selectedImportSpec
				.getImportDefinitionCode());
		if (selectedDefinition == null) {
			// No ImportDefinition found
			writeLogAID("Error: Cannot find definition for "
					+ selectedImportSpec.getImportDefinitionCode());
			return false;
		}

		writeLogAID("Import Spec Code:  "
				+ selectedImportSpec.getImportSpecCode());
		writeLogAID("Import Definition: "
				+ selectedImportSpec.getImportDefinitionCode());
		try {
			writeLogAID("Database URL:      " + conn.getMetaData().getURL());
		} catch (Exception e) {
		}

		return true;
	}

	public void writeLogAID(String message) {
		writeLogAID(message, true);
	}

	public void writeLogAID(String message, boolean email) {
		Date d = new Date(System.currentTimeMillis());
		String currentDateTime = logDateformat.format(d);
		try {
			outAID.write("[" + currentDateTime + "] " + message);
			outAID.newLine();
			outAID.flush();
		} catch (Exception e) {
		}

		if (email) {
			if (emailContent == null) {
				emailContent = new StringBuffer();
			}
			emailContent.append("[" + currentDateTime + "] " + message + "\n");
		}
	}

	public Long addActionAID(String fileName, String filePath,
			boolean autoProcess, String folderPath) {
		if (preferences == null) {
			preferences = getImportMapProcessPreferenceDTOAID();
		}
		
		writeLogAID("File Imported:    " + filePath); // We actually use this
														// file to import.
		writeLogAID(" ");
		writeLogAID("** Started import procedure");

		// Send email notification when a file import begins
		String emailStart = preferences.getEmailStart(); // 0 = do not send a start email, 1 = send an email when the import starts.
		if (emailStart!=null && emailStart.equals("1")) {
			String subject = "PinPoint/AID: " + aidProperties.getProperty("original_file") + " Received";
			this.sendEmail(subject);
		}
		
		try {
			if (!getFileDetailsAID(filePath, fileName)) {
				// Verification error
				sendErrorEmail();
				return -1L;
			}

			ImportDefinition def = new ImportDefinition(selectedDefinition);
			def.setSampleFileName(filePath);

			parser = new ImportMapUploadParser(def, preferences);
		} catch (DateFormatException dfe) {
			writeLogAID("Error: Importing stopped with error, " + dfe.getMessage());
			
			sendErrorEmail();
			return -2L;
		}

		parser.setImportDefinitionDetail(mappedColumns);

		newBatch = new BatchMaintenance();
		newBatch.setUpdateUserId("STSCORP");
		newBatch.setEntryTimestamp(new Date());
		newBatch.setVc02(fileName);
		newBatch.setVc03("API");
		// newBatch.setBatchTypeCode("IP");
		newBatch.setBatchTypeCode(selectedImportSpec.getImportSpecType());
		newBatch.setBatchStatusCode("AX");//AX = AID Import Running
		newBatch.setHeldFlag("0");
		newBatch.setTotalBytes(parser.getFileSize());
		BatchMaintenance batchMaintenance = addBatchUsingJDBC(newBatch, conn);
		if (batchMaintenance == null) {
			// Cannot create a new batch
			writeLogAID("Error: Cannot create new Batch Id. Importing stopped.");

			sendErrorEmail();
			return -3L;
		}
		writeLogAID("Batch Id: " + batchMaintenance.getBatchId() + " created, Batch Status Code: " + newBatch.getBatchStatusCode());

		// now do the heavy lifting
		parser.setColumnProperties(getBCPTransactionDetailColumnProperties(BCPPurchaseTransaction.class));
		parser.setColumnPropertiesSale(getBCPTransactionDetailColumnProperties(BCPBillTransaction.class));
		processUpdateInBackgroundAID(batchMaintenance, conn, autoProcess,
				folderPath);

		// flush any custom script errors
		try {
			writeLogAID("flushing Error Log");
			flushErrorLog(conn, batchMaintenance);
			writeLogAID("flushing Error Log completed");
		} catch (Exception e) {
			e.printStackTrace();
			writeLogAID("flushErrorLog: " + getStackTrace(e));
		}
		
		// Send complete notification
		writeLogAID(getTimeElapsed(startTime));
		String emailStop = preferences.getEmailStop(); // 0 = do not send a completion email, 99 = Select the �Always�, o 20 = Select the �On Error Only�
		if (emailStop!=null && (emailStop.equals("99") || emailStop.equals("20"))) {
			String subject = "";
			
			Map<String, ListCodes> errorSeverityCodes = parser.getErrorSeverityCodes();
			ListCodes errorSeverityObj = (ListCodes)errorSeverityCodes.get(batchMaintenance.getErrorSevCode());
			String errorSeverityDesc = "";
			if(errorSeverityObj!=null){
				errorSeverityDesc = errorSeverityObj.getDescription();
			}
			
			boolean errorFound = true;
			if((batchMaintenance.getHeldFlag()==null || batchMaintenance.getHeldFlag().length()==0 || batchMaintenance.getHeldFlag().equals("0")) && (batchMaintenance.getErrorSevCode()==null || batchMaintenance.getErrorSevCode().length()==0)){
				//No error
				errorFound = false;
			}
	
			if(emailStop.equals("99") && !errorFound){
				errorSeverityDesc = "No";
			}
		
			if(errorSeverityDesc==null || errorSeverityDesc.length()==0){
				//Cannot decide error message
				errorSeverityDesc = "";			
			}
			
			subject = "PinPoint/AID: " + aidProperties.getProperty("original_file")
					+ " Imported with " + errorSeverityDesc + " Errors - Batch Id: " + batchMaintenance.getBatchId();
			
			if(emailStop.equals("99")){
				sendEmail(subject);
			}
			else if(emailStop.equals("20") && errorFound){
				sendEmail(subject);
			}
		}
		
		if(batchMaintenance!=null && batchMaintenance.getBatchStatusCode()!=null && 
			(batchMaintenance.getBatchStatusCode().equalsIgnoreCase("AX") || batchMaintenance.getBatchStatusCode().equalsIgnoreCase("ASX"))){
				//"AX": AID Import Running, "ASX": AID Script Running
			writeLogAID("Batch Id: " + batchMaintenance.getBatchId() + " left, Batch Status Code: " + newBatch.getBatchStatusCode());
			return batchMaintenance.getBatchId();
		}
		else{
			return 0L; //Return normally
		}
	}

	private void processUpdateInBackgroundAID(BatchMaintenance batch,
			Connection conn, boolean autoProcess, String folderPath) {
		try {
			writeLogAID("Retrieving mapping from database....");

			parser.setTransactionColumnDriverNamesMap(getTransactionColumnDriverNamesMapAID());
			parser.setBCPTransactionsColumnMap(getBCPTransactionsColumnMapAID("TB_PURCHTRANS"));
			parser.setBCPTransactionsColumnMapSale(getBCPTransactionsColumnMapAID("TB_BILLTRANS"));
			parser.setErrorListCodes(getErrorListCodesAID());
			parser.setErrorSeverityCodes(getErrorSeverityCodesAID());
			parser.setImportSpec(selectedImportSpec);

			// 5427 Convert to upper for import headers
			// Just want to get these data before running Thread
			getMappedColumnsAID();

			writeLogAID("Retrieving mapping from database.... done");

			writeLogAID("Run pre import scripts....");
			Map<String, String> scriptReplaceValues = new HashMap<String, String>();
			scriptReplaceValues.put(ScriptRunner.BATCHID, batch.getbatchId()
					+ "");
			boolean scriptRunStatus = runCustomScripts(conn, folderPath, "pre",
					parser, scriptReplaceValues);
			writeLogAID("Run pre import scripts.... done. Status: "
					+ scriptRunStatus);

			// Get latest from DB in case batch is updated from pre scripts
			syncBatchUsingJDBC(batch, conn);

			if (!scriptRunStatus) {
				try {
					batch.setBatchStatusCode("ABAS");//ABAS = AID Script Aborted
					batch.setStatusUpdateTimestamp(new Date());
					batch.setStatusUpdateUserId("STSCORP");
					batch.setUpdateTimestamp(new Date());
					batch.setErrorSevCode(ErrorState.Terminal);
					updateBatchUsingJDBC(batch, conn);
				} catch (Exception ee) {
				}

				writeLogAID("** Exiting batch import as pre import scripts failed to run");
				writeLogAID(" ");
				return;
			}

			state = AddState.PROCESSING;
			backgroundError = null;
			parser.setCompleted(false);
			conn.setAutoCommit(false);

			batch.setVc01(selectedImportSpec.getImportSpecCode());
			batch.setEntryTimestamp(new Date());

			// Import file
			writeLogAID("Importing data file....");
			importMapUpdatesUsingJDBC(batch, parser, conn);
			writeLogAID("Importing data file.... done");

			// force completion
			state = AddState.COMPLETE;
			if (parser.getErrorCount() > 0) {
				batch.setHeldFlag("1");
			}

			if (parser.getMaxErrorSeen() < 10) {
				// Perfect, no error.
			} else if (parser.getMaxErrorSeen() == 10) {
				batch.setErrorSevCode(BatchMaintenance.ErrorState.Warning);
			} else if (parser.getMaxErrorSeen() == 20) {
				batch.setErrorSevCode(BatchMaintenance.ErrorState.Error);
			} else if (parser.getMaxErrorSeen() >= 30) {
				batch.setErrorSevCode(BatchMaintenance.ErrorState.Terminal);
			}

			batch.setNu01(parser.getComputedTotalAmount().doubleValue());
			batch.setVc01(selectedImportSpec.getImportSpecCode());

			if (parser.wasAborted()) {
	  	  		batch.setBatchStatusCode("ABA");//ABA = AID Import Aborted
	  	  	} else {
	  	  		//batch.setBatchStatusCode("I"); //It is still importing when running scripts.
	  	  	}

			batch.setTotalRows(new Long(parser.getTotalRows()));
			batch.setStatusUpdateTimestamp(new Date());
			batch.setStatusUpdateUserId(batch.getUpdateUserId());
			batch.setUpdateUserId(batch.getUpdateUserId());
			batch.setUpdateTimestamp(new Date());

			updateBatchUsingJDBC(batch, conn);

			parser.setCompleted(true);
			parser.closeReader();

			writeLogAID("Batch Id: " + batch.getBatchId() + " updated, Held Flag: " + batch.getHeldFlag());		
			writeLogAID("** Finished import procedure");
			writeLogAID(" ");

			if(batch.getBatchStatusCode().equalsIgnoreCase("ABA")){//AID Import Aborted
				return;
			}

			writeLogAID("Run post import scripts....");
			
			//AID Script Running
			batch.setBatchStatusCode("XAS");//XAS = AID Script Running
			batch.setStatusUpdateTimestamp(new Date());
		  	batch.setUpdateTimestamp(new Date());
		  	updateBatchUsingJDBC(batch, conn);
		  	writeLogAID("Batch Id: " + batch.getBatchId() + " updated, Batch Status Code: " + batch.getBatchStatusCode());
		  	
		  	scriptReplaceValues.clear();
			scriptReplaceValues.put(ScriptRunner.BATCHID, batch.getbatchId() + "");
			scriptReplaceValues.put(ScriptRunner.BATCHSTAT, batch.getBatchStatusCode());
			scriptReplaceValues.put(ScriptRunner.ERRORSEV, batch.getErrorSevCode());
			scriptRunStatus = runCustomScripts(conn, folderPath, "post", parser, scriptReplaceValues);
			writeLogAID("Run post import scripts.... done. Status: " + scriptRunStatus);

			// Get latest from DB in case batch is updated from post scripts
			syncBatchUsingJDBC(batch, conn);
			
			//Done with import & scripts
			batch.setBatchStatusCode("IA");//IA = AID Imported 
			batch.setStatusUpdateTimestamp(new Date());
		  	batch.setUpdateTimestamp(new Date());
		  	updateBatchUsingJDBC(batch, conn);
		  	writeLogAID("Batch Id: " + batch.getBatchId() + " updated, Batch Status Code: " + batch.getBatchStatusCode());

			if (!scriptRunStatus) {
				try {
					batch.setBatchStatusCode("ABAS");//ABAS = AID Script Aborted 
					batch.setStatusUpdateTimestamp(new Date());
					batch.setStatusUpdateUserId("STSCORP");
					batch.setUpdateTimestamp(new Date());
					batch.setErrorSevCode(ErrorState.Warning);
					batch.setHeldFlag("1");
					updateBatchUsingJDBC(batch, conn);
				} catch (Exception ee) {
				}

				writeLogAID("** AutoProcess is not called as post import scripts failed to run");
				writeLogAID(" ");
			} else if (!autoProcess) {
				writeLogAID("** AutoProcess is turned off");
				writeLogAID(" ");
			}
			//Continue to process the batch when HeldFlag is not 1 and BatchStatusCode is 'IA'
			else if((batch.getHeldFlag()==null || !batch.getHeldFlag().equalsIgnoreCase("1")) && batch.getBatchStatusCode().equalsIgnoreCase("IA")){
				writeLogAID("** Started 'Flagged for Process' procedure for Batch Id, " + batch.getbatchId());

				batch.setBatchStatusCode("FP");
				// Control already done with the timezone conversion, UTC
				batch.setSchedStartTimestamp(new Date());
				batch.setUpdateTimestamp(new Date());
				batch.setStatusUpdateTimestamp(new Date());
				batch.setStatusUpdateUserId("STSCORP");
				updateBatchUsingJDBC(batch, conn);

				writeLogAID("Set Batch Status Code to FP");
				writeLogAID("** Finished 'Flagged for Process' procedure");
				writeLogAID(" ");
			} else {
				writeLogAID("** No 'Flagged for Process' procedure needed");
				writeLogAID(" ");
			}
		} catch (Throwable e) {
			e.printStackTrace();
			parser.setCompleted(true);

			writeLogAID("Error: Importing aborted with error, "
					+ e.getMessage());

			try {
				batch.setBatchStatusCode("ABA");
				batch.setStatusUpdateTimestamp(new Date());
				batch.setStatusUpdateUserId("STSCORP");
				batch.setUpdateTimestamp(new Date());
				updateBatchUsingJDBC(batch, conn);
			} catch (Exception ee) {
			}

		} finally {
		}
	}

	private boolean runCustomScripts(Connection conn, String folderPath,
			String type, ImportMapUploadParser parser,
			Map<String, String> replaceValues) {
		boolean result = true;

		if (folderPath != null) {
			File scriptsRootFolder = new File(folderPath + File.separator
					+ "Scripts");
			if (scriptsRootFolder.exists()) {
				File scriptsFolder = null;
				if ("pre".equalsIgnoreCase(type)) {
					scriptsFolder = new File(scriptsRootFolder, "pre-import");
				} else if ("post".equalsIgnoreCase(type)) {
					scriptsFolder = new File(scriptsRootFolder, "post-import");
				}

				if (scriptsFolder != null && scriptsFolder.exists()) {
					File[] scripts = scriptsFolder.listFiles(new FileFilter() {
						@Override
						public boolean accept(File pathname) {
							return pathname.isFile();
						}
					});

					if (scripts != null && scripts.length > 0) {
						Map<Integer, File> map = new TreeMap<Integer, File>();
						for (File f : scripts) {
							if (f.canRead()) {
								int index = f.getName().indexOf("_");
								if (index > -1) {
									try {
										int num = Integer.parseInt(f.getName()
												.substring(0, index));
										map.put(num, f);
									} catch (Exception e) {
									}
								}
							}
						}

						if (map.size() > 0) {
							for (File f : map.values()) {
								try {
									writeLogAID("** Running script: "
											+ f.getAbsolutePath());
									replaceValues.put(ScriptRunner.FILENAME,
											f.getAbsolutePath());
									ScriptRunner scriptRunner = new ScriptRunner(
											conn, false, true);
									scriptRunner.setLogWriter(new PrintWriter(
											outAID));
									scriptRunner
											.setErrorLogWriter(new PrintWriter(
													outAID));
									scriptRunner
											.setReplaceValues(replaceValues);
									scriptRunner.runScript(new FileReader(f));
									writeLogAID("** Running script: "
											+ f.getAbsolutePath() + ". Done.");
								} catch (Exception e) {
									writeLogAID("** Error running script: "
											+ f.getAbsolutePath());
									writeLogAID(e.getMessage());
									writeLogAID("Stopping further script executions");

									try {
										if (parser.getBatchErrorLogs() == null) {
											parser.initVariables();
										}

										String value = f.getAbsolutePath();
										if (value != null
												&& value.length() > 100) {
											value = value.substring(value
													.length() - 100);
										}
										if ("pre".equalsIgnoreCase(type)) {
											parser.addError(new BatchErrorLog(
													"IP10", 0, "",
													"Script Name", value));
										} else if ("post"
												.equalsIgnoreCase(type)) {
											parser.addError(new BatchErrorLog(
													"IP11", 0, "",
													"Script Name", value));
										}
									} catch (Exception ee) {
										ee.printStackTrace();
										writeLogAID("parser.addError: "
												+ getStackTrace(ee));
									}

									result = false;
									break;
								}
							}
						}
					}
				}
			}
		}

		return result;
	}

	public boolean getFileDetailsAID(String filePath, String fileName) {
		this.selectedFile = null;
		this.mappedColumns = null;
		// in case this process fails and we no longer have a valid selected
		// file.
		state = AddState.NEED_FILE;
		if ((fileName == null) || (fileName.trim().length() == 0))
			return false;
		try {
			ImportDefinition def = new ImportDefinition(selectedDefinition);
			def.setSampleFileName(filePath);

			parser = new ImportMapUploadParser(def, preferences);
			parser.setImportSpec(selectedImportSpec);
			Set<String> headerNames = new HashSet<String>();
			for (String s : parser.getHeaders()) {
				headerNames.add(s == null ? "" : s.toUpperCase());
			}

			String mapError = "";
			for (ImportDefinitionDetail detail : getMappedColumnsAID()) {
				// 5427 Convert to upper for import headers. They should be
				// upper in db, but just make sure.
				if (!headerNames.contains(detail.getImportColumnName().toUpperCase())) {
					if (!headerNames.contains(detail.getImportColumnName().toUpperCase())) {
						if(mapError.length()>0){
							mapError = mapError + ", ";
						}
						mapError = mapError + detail.getImportColumnName().toUpperCase();
					}
				}
			}
			if(mapError.length()>0){
				writeLogAID("Error: The Definition has mapped column(s) that are not in the import file: " + mapError);
			}

			BatchMaintenance nameBatch = null;

			if (DuplicationRestriction.get(selectedImportSpec
					.getFilenamedupres()) != (DuplicationRestriction.None)) {
				nameBatch = validateFileNamesAID(fileName);
			}
			BatchMaintenance sizeBatch = null;
			if (DuplicationRestriction.get(selectedImportSpec
					.getFilesizedupres()) != (DuplicationRestriction.None)) {
				sizeBatch = validateFileLengthsAID(fileName);
			}
			String msg = null;
			if ((sizeBatch != null) && (nameBatch != null)) {
				msg = "Potential duplicate file name with Batch Id: "
						+ nameBatch.getBatchId() + ", size: "
						+ sizeBatch.getBatchId();
			} else if (nameBatch != null) {
				msg = "Potential duplicate file name with Batch Id: "
						+ nameBatch.getBatchId();
			} else if (sizeBatch != null) {
				msg = "Potential duplicate file size with Batch Id: "
						+ sizeBatch.getBatchId();
			}
			
			if (msg!=null){
				writeLogAID(msg);
			}

			if ((nameBatch != null) && (DuplicationRestriction.get(selectedImportSpec.getFilenamedupres()) == (DuplicationRestriction.Full))) {
				return false;
			} else if ((sizeBatch != null) && (DuplicationRestriction.get(selectedImportSpec.getFilesizedupres()) == (DuplicationRestriction.Full))) {
				return false;
			}
			
			parser.parseFile();
			this.bytesInFile = parser.getFileSize();
			this.estimatedRows = parser.getEstimatedRows();
			// enable the OK button
			this.selectedFile = new File(fileName);
			state = AddState.HAVE_FILE;
		} catch (Exception ex) {
			writeLogAID("Could not read file: " + ex.getMessage());
			return false;
		}

		return true;
	}

	public static void main(String[] args) {
		System.out.println("############ main for Automatic Import of Data");

		String fileName = "pco_test_import.txt";
		String filePath = "C:\\AP_IMPORT\\STSTEST\\pco_test_import.txt";
		String specStr = "MBF_PCO_TEST";
		String folderPath = "C:\\AP_IMPORT\\STSTEST\\";
		String logFile = "C:\\AP_IMPORT\\STSTEST\\pco_test_import.log";

		BufferedWriter out = null;
		try {
			out = new BufferedWriter(new FileWriter(logFile));
			// out.write("aString");
		} catch (Exception e) {
		}

		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
		} catch (ClassNotFoundException e) {
			System.out
					.println("xxxxxxxxxxxxxx Can't load class: oracle.jdbc.driver.OracleDriver");
			return;
		}

		Connection conn = null;
		try {
			// Standalone connection
			conn = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:stscorp", "stscorp","STSCORP");
			conn.setAutoCommit(false);

			ImportMapAddBean importMapAddBean = new ImportMapAddBean();
			if (importMapAddBean.initVerification(conn, out, specStr)) {
				// Continue the process
				importMapAddBean.setPowerBuilderVersion(false);
				importMapAddBean.addActionAID(fileName, filePath, false,
						folderPath);
			} else {
				System.out.println("Error: Stop importing file, " + filePath);
			}

		} catch (Throwable e) {
			e.printStackTrace();
		} finally {
			if (conn != null)
				try {
					conn.close();
				} catch (SQLException ignore) {
				}
		}

		try {
			out.close();
		} catch (Exception e) {
		}
	}

	public Map<String, String> getSystemPreferenceMapAID() {
		logger.debug(" getSystemPreferenceMapAID ");
		Map<String, String> systemPreferenceMap = new HashMap<String, String>();
		Statement stat = null;
		try {
			stat = conn.createStatement();
			ResultSet rs = stat
					.executeQuery("select * from TB_OPTION where OPTION_TYPE_CODE='SYSTEM' and USER_CODE='SYSTEM'");
			while (rs.next()) {
				systemPreferenceMap.put(rs.getString("OPTION_CODE"),
						rs.getString("VALUE"));
			}
			rs.close();
		} catch (Exception e) {
			// logger.error("############ getSystemPreferenceMapAID() failed: "
			// + e.toString());
		} finally {
			if (stat != null) {
				try {
					stat.close();
				} catch (Exception ex) {
				}
			}
		}

		logger.debug("exit getSystemPreferenceMap");
		return systemPreferenceMap;
	}

	public Map<String, String> getAdminPreferenceMapAID() {
		Map<String, String> adminPreferenceMap = new HashMap<String, String>();
		Statement stat = null;
		try {
			stat = conn.createStatement();
			ResultSet rs = stat
					.executeQuery("select * from TB_OPTION where OPTION_TYPE_CODE='ADMIN' and USER_CODE='ADMIN'");
			while (rs.next()) {
				adminPreferenceMap.put(rs.getString("OPTION_CODE"),
						rs.getString("VALUE"));
			}
			rs.close();
		} catch (Exception e) {
			// logger.error("############ getSystemPreferenceMapAID() failed: "
			// + e.toString());
		} finally {
			if (stat != null) {
				try {
					stat.close();
				} catch (Exception ex) {
				}
			}
		}

		return adminPreferenceMap;
	}

	public ImportMapProcessPreferenceDTO getImportMapProcessPreferenceDTOAID() {
		ImportMapProcessPreferenceDTO importMapProcessPreferenceDTO = new ImportMapProcessPreferenceDTO();
		Map<String, String> map = new HashMap<String, String>();
		map.putAll(getSystemPreferenceMapAID());
		map.putAll(getAdminPreferenceMapAID());

		// System - Import/Map/Process
		String fileNameDuplicateRestriction = (String) map
				.get("FILENAMEDUPRES");
		importMapProcessPreferenceDTO
				.setFileNameDuplicateRestriction(fileNameDuplicateRestriction);
		String fileSizeDuplicateRestriction = (String) map
				.get("FILESIZEDUPRES");
		importMapProcessPreferenceDTO
				.setFileSizeDuplicateRestriction(fileSizeDuplicateRestriction);
		String terminalErrorsBeforeAbortImport = (String) map.get("ERRORROWS");
		importMapProcessPreferenceDTO
				.setTerminalErrorsBeforeAbortImport(terminalErrorsBeforeAbortImport);
		String defaultImportDateMap = (String) map.get("IMPORTDATEMAP");
		importMapProcessPreferenceDTO
				.setDefaultImportDateMap(defaultImportDateMap);
		String rowsToImportMapBeforeSave = (String) map.get("MAPROWS");
		importMapProcessPreferenceDTO
				.setRowsToImportMapBeforeSave(rowsToImportMapBeforeSave);
		String endOfFileMarker = (String) map.get("BATCHEOFMARKER");
		importMapProcessPreferenceDTO.setEndOfFileMarker(endOfFileMarker);
		String batchCountMarker = (String) map.get("BATCHCOUNTMARKER");
		importMapProcessPreferenceDTO.setBatchCountMarker(batchCountMarker);
		String batchTotalMarker = (String) map.get("BATCHTOTALMARKER");
		importMapProcessPreferenceDTO.setBatchTotalMarker(batchTotalMarker);
		String deleteTransactionWithZeroAmountFlag = (String) map
				.get("DELETE0AMTFLAG");
		if ("1".equalsIgnoreCase(deleteTransactionWithZeroAmountFlag)) {
			importMapProcessPreferenceDTO
					.setDeleteTransactionWithZeroAmountFlag(true);
		} else {
			importMapProcessPreferenceDTO
					.setDeleteTransactionWithZeroAmountFlag(false);
		}
		String holdTransactionAmountFlag = (String) map.get("HOLDTRANSFLAG");
		if ("1".equalsIgnoreCase(holdTransactionAmountFlag)) {
			importMapProcessPreferenceDTO.setHoldTransactionAmountFlag(true);
		} else {
			importMapProcessPreferenceDTO.setHoldTransactionAmountFlag(false);
		}
		String processTransactionAmountFlag = (String) map.get("AUTOPROCFLAG");
		if ("1".equalsIgnoreCase(processTransactionAmountFlag)) {
			importMapProcessPreferenceDTO.setProcessTransactionAmountFlag(true);
		} else {
			importMapProcessPreferenceDTO
					.setProcessTransactionAmountFlag(false);
		}
		String killProcessingProcedureCountFlag = (String) map
				.get("KILLPROCFLAG");
		if ("1".equalsIgnoreCase(killProcessingProcedureCountFlag)) {
			importMapProcessPreferenceDTO
					.setKillProcessingProcedureCountFlag(true);
		} else {
			importMapProcessPreferenceDTO
					.setKillProcessingProcedureCountFlag(false);
		}
		String holdTransactionAmount = (String) map.get("HOLDTRANSAMT");
		importMapProcessPreferenceDTO
				.setHoldTransactionAmount(holdTransactionAmount);
		String processTransactionAmount = (String) map.get("AUTOPROCAMT");
		importMapProcessPreferenceDTO
				.setProcessTransactionAmount(processTransactionAmount);
		String applyTaxCodeAmount = (String) map.get("AUTOPROCID");
		importMapProcessPreferenceDTO.setApplyTaxCodeAmount(applyTaxCodeAmount);
		String killProcessingProcedureCount = (String) map.get("KILLPROCCOUNT");
		importMapProcessPreferenceDTO
				.setKillProcessingProcedureCount(killProcessingProcedureCount);
		String doNotImportMarkers = (String) map.get("DONOTIMPORT");
		importMapProcessPreferenceDTO.setDoNotImportMarkers(doNotImportMarkers);
		String replaceWithMarkers = (String) map.get("REPLACEWITH");
		importMapProcessPreferenceDTO.setReplaceWithMarkers(replaceWithMarkers);

		// Admin - Import/Map/Process
		String allocationsEnabled = (String) map.get("ALLOCATIONSENABLED");
		if ("1".equalsIgnoreCase(allocationsEnabled)) {
			importMapProcessPreferenceDTO.setAllocationsEnabled(true);
		} else {
			importMapProcessPreferenceDTO.setAllocationsEnabled(false);
		}

		// System - GL Extract
		String glExtractFilePath = (String) map.get("GLEXTRACTFILEPATH");
		importMapProcessPreferenceDTO.setGlExtractFilePath(glExtractFilePath);
		String glExtractFilePrefix = (String) map.get("GLEXTRACTPREFIX");
		importMapProcessPreferenceDTO
				.setGlExtractFilePrefix(glExtractFilePrefix);
		String glExtractFileExtension = (String) map.get("GLEXTRACTEXT");
		importMapProcessPreferenceDTO
				.setGlExtractFileExtension(glExtractFileExtension);

		// Admin - GL Extract
		String glExtractEnabled = (String) map.get("GLEXTRACTENABLED");
		if ("1".equalsIgnoreCase(glExtractEnabled)) {
			importMapProcessPreferenceDTO.setGlExtractEnabled(true);
		} else {
			importMapProcessPreferenceDTO.setGlExtractEnabled(false);
		}
		String glExtractWindowName = (String) map.get("GLEXTRACTWIN");
		importMapProcessPreferenceDTO
				.setGlExtractWindowName(glExtractWindowName);
		String glExtractSetMark = (String) map.get("GLEXTRACTMARK");
		if ("1".equalsIgnoreCase(glExtractSetMark)) {
			importMapProcessPreferenceDTO.setGlExtractSetMark(true);
		} else {
			importMapProcessPreferenceDTO.setGlExtractSetMark(false);
		}

		// System - Security
		String pwdExpirationWarningDays = (String) map.get("PWEXPDAYS");
		importMapProcessPreferenceDTO
				.setPwdExpirationWarningDays(pwdExpirationWarningDays);
		String chgPassword = (String) map.get("PWUSERCHG");
		if ("1".equalsIgnoreCase(chgPassword)) {
			importMapProcessPreferenceDTO.setChgPassword(true);
		} else {
			importMapProcessPreferenceDTO.setChgPassword(false);
		}
		String denyPasswordChangeMessage = (String) map.get("PWDENYMSG");
		importMapProcessPreferenceDTO
				.setDenyPasswordChangeMessage(denyPasswordChangeMessage);

		// Admin - Security
		String activateExtendedSecurity = (String) map.get("EXTENDSEC");
		if ("1".equalsIgnoreCase(activateExtendedSecurity)) {
			importMapProcessPreferenceDTO.setActivateExtendedSecurity(true);
		} else {
			importMapProcessPreferenceDTO.setActivateExtendedSecurity(false);
		}
		String activateDebugExtendedSecurity = (String) map.get("DEBUGSEC");
		if ("1".equalsIgnoreCase(activateDebugExtendedSecurity)) {
			importMapProcessPreferenceDTO
					.setActivateDebugExtendedSecurity(true);
		} else {
			importMapProcessPreferenceDTO
					.setActivateDebugExtendedSecurity(false);
		}

		// System - Directories
		String sqlFilePath = (String) map.get("SQLFILEPATH");
		importMapProcessPreferenceDTO.setSqlFilePath(sqlFilePath);
		String sqlFileExt = (String) map.get("SQLFILEEXT");
		importMapProcessPreferenceDTO.setSqlFileExt(sqlFileExt);
		String impExpFilePath = (String) map.get("IMPEXPFILEPATH");
		importMapProcessPreferenceDTO.setImpExpFilePath(impExpFilePath);
		String impExpFileExt = (String) map.get("IMPEXPFILEEXT");
		importMapProcessPreferenceDTO.setImpExpFileExt(impExpFileExt);
		String refDocFilePath = (String) map.get("REFDOCFILEPATH");
		importMapProcessPreferenceDTO.setRefDocFilePath(refDocFilePath);
		String refDocFileExt = (String) map.get("REFDOCFILEEXT");
		importMapProcessPreferenceDTO.setRefDocFileExt(refDocFileExt);
		String defaultDirCrystal = (String) map.get("DEFAULTDIRCRYSTAL");
		importMapProcessPreferenceDTO.setDefaultDirCrystal(defaultDirCrystal);

		// User - Directories
		String importFilePath = (String) map.get("IMPORTFILEPATH");
		importMapProcessPreferenceDTO.setImportFilePath(importFilePath);
		String importFileExt = (String) map.get("IMPORTFILEEXT");
		importMapProcessPreferenceDTO.setImportFileExt(importFileExt);
		String userDirCrystal = (String) map.get("USERDIRCRYSTAL");
		importMapProcessPreferenceDTO.setUserDirCrystal(userDirCrystal);

		// System - Tax Updates
		String lastTaxRateDateFull = (String) map.get("LASTTAXRATEDATEFULL");
		importMapProcessPreferenceDTO
				.setLastTaxRateDateFull(lastTaxRateDateFull);
		String lastTaxRateRelFull = (String) map.get("LASTTAXRATERELFULL");
		importMapProcessPreferenceDTO.setLastTaxRateRelFull(lastTaxRateRelFull);
		String lastTaxRateDateUpdate = (String) map
				.get("LASTTAXRATEDATEUPDATE");
		importMapProcessPreferenceDTO
				.setLastTaxRateDateUpdate(lastTaxRateDateUpdate);
		String lastTaxRateRelUpdate = (String) map.get("LASTTAXRATERELUPDATE");
		importMapProcessPreferenceDTO
				.setLastTaxRateRelUpdate(lastTaxRateRelUpdate);
		String taxWarnRate = (String) map.get("TAXWARNRATE");
		importMapProcessPreferenceDTO.setTaxWarnRate(taxWarnRate);

		// System - Compliance
		String defCCHTaxCatCode = (String) map.get("DEFCCHTAXCATCODE");
		importMapProcessPreferenceDTO.setDefCCHTaxCatCode(defCCHTaxCatCode);
		String defCCHGroupCode = (String) map.get("DEFCCHGROUPCODE");
		importMapProcessPreferenceDTO.setDefCCHGroupCode(defCCHGroupCode);
		String tpExtractFilePath = (String) map.get("TPEXTRACTEFILEPATH");
		importMapProcessPreferenceDTO.setTpExtractFilePath(tpExtractFilePath);
		String tpExtractFilePrefix = (String) map.get("TPEXTRACTPREFIX");
		importMapProcessPreferenceDTO
				.setTpExtractFilePrefix(tpExtractFilePrefix);
		String tpExtractExt = (String) map.get("TPEXTRACTEXT");
		importMapProcessPreferenceDTO.setTpExtractExt(tpExtractExt);

		// Admin - Compliance
		String tpExtractEnabled = (String) map.get("TPEXTRACTENABLED");
		if ("1".equalsIgnoreCase(tpExtractEnabled)) {
			importMapProcessPreferenceDTO.setTpExtractEnabled(true);
		} else {
			importMapProcessPreferenceDTO.setTpExtractEnabled(false);
		}
		
		// For AID email notification
		String emailHost = (String) map.get("EMAILHOST");
		importMapProcessPreferenceDTO.setEmailHost(emailHost);
		String emailPort = (String) map.get("EMAILPORT");
		importMapProcessPreferenceDTO.setEmailPort(emailPort);
		String emailStart = (String) map.get("EMAILSTART");
		importMapProcessPreferenceDTO.setEmailStart(emailStart);
		String emailStop = (String) map.get("EMAILSTOP");
		importMapProcessPreferenceDTO.setEmailStop(emailStop);
		String emailTo = (String) map.get("EMAILTO");
		importMapProcessPreferenceDTO.setEmailTo(emailTo);
		String emailCc = (String) map.get("EMAILCC");
		importMapProcessPreferenceDTO.setEmailCc(emailCc);
		String emailFrom = (String) map.get("EMAILFROM");
		importMapProcessPreferenceDTO.setEmailFrom(emailFrom);

		return importMapProcessPreferenceDTO;
	}

	private BatchMaintenance validateFileNamesAID(String fileName) {
		BatchMaintenance batchMaintenance = null;
		Statement stat = null;
		try {
			stat = conn.createStatement();
			ResultSet rs = stat
					.executeQuery("SELECT * FROM TB_BATCH WHERE BATCH_STATUS_CODE not like 'AB%' and upper(VC02)='"
							+ fileName.toUpperCase() + "'");
			if (rs.next()) {
				batchMaintenance = new BatchMaintenance();
				batchMaintenance.setBatchId(rs.getLong("BATCH_ID"));
			}
			rs.close();
		} catch (Exception e) {
			// logger.error("############ validateFileNamesAID() failed: " +
			// e.toString());
		} finally {
			if (stat != null) {
				try {
					stat.close();
				} catch (Exception ex) {
				}
			}
		}

		return batchMaintenance;
	}

	private BatchMaintenance validateFileLengthsAID(String fileName) {
		BatchMaintenance batchMaintenance = null;
		Statement stat = null;
		try {
			stat = conn.createStatement();
			ResultSet rs = stat
					.executeQuery("SELECT * FROM TB_BATCH WHERE BATCH_STATUS_CODE not like 'AB%' and TOTAL_BYTES="
							+ parser.getFileSize());
			if (rs.next()) {
				batchMaintenance = new BatchMaintenance();
				batchMaintenance.setBatchId(rs.getLong("BATCH_ID"));
			}
			rs.close();
		} catch (Exception e) {
			// logger.error("############ validateFileLengthsAID() failed: " +
			// e.toString());
		} finally {
			if (stat != null) {
				try {
					stat.close();
				} catch (Exception ex) {
				}
			}
		}

		return batchMaintenance;
	}

	private List<ImportDefinitionDetail> getMappedColumnsAID() {
		if ((this.mappedColumns == null) && (selectedDefinition != null)) {
			mappedColumns = new ArrayList<ImportDefinitionDetail>();
			Statement stat = null;
			try {
				stat = conn.createStatement();
				ResultSet rs = stat
						.executeQuery("select * from TB_IMPORT_DEFINITION_DETAIL where IMPORT_DEFINITION_CODE='"
								+ selectedDefinition.getImportDefinitionCode()
								+ "'");
				while (rs.next()) {
					ImportDefinitionDetail importDefinitionDetail = new ImportDefinitionDetail();

					importDefinitionDetail.setTableName(rs
							.getString("TABLE_NAME"));
					importDefinitionDetail.setImportDefinitionCode(rs
							.getString("IMPORT_DEFINITION_CODE"));
					importDefinitionDetail.setTransDtlColumnName(rs
							.getString("TRANS_DTL_COLUMN_NAME"));
					importDefinitionDetail.setImportColumnName(rs
							.getString("IMPORT_COLUMN_NAME"));
					importDefinitionDetail.setSelectClause(rs
							.getString("SELECT_CLAUSE"));
					mappedColumns.add(importDefinitionDetail);
				}
				rs.close();
			} catch (Exception e) {
				// logger.error("############ getMappedColumnsAID() failed: " +
				// e.toString());
			} finally {
				if (stat != null) {
					try {
						stat.close();
					} catch (Exception ex) {
					}
				}
			}
		}
		return mappedColumns;
	}

	private Map<String, DriverNames> getTransactionColumnDriverNamesMapAID() {
		Map<String, DriverNames> result = new LinkedHashMap<String, DriverNames>();
		Statement stat = null;
		try {
			stat = conn.createStatement();
			ResultSet rs = stat
					.executeQuery("select * from TB_DRIVER_NAMES order by DRIVER_ID");
			while (rs.next()) {
				DriverNames driverNames = new DriverNames();

				DriverNamesPK driverNamesPK = new DriverNamesPK(
						rs.getString("DRIVER_NAMES_CODE"),
						rs.getLong("DRIVER_ID"));
				driverNames.setDriverNamesPK(driverNamesPK);

				driverNames.setTransDtlColName(rs
						.getString("TRANS_DTL_COLUMN_NAME"));
				driverNames
						.setMatrixColName(rs.getString("MATRIX_COLUMN_NAME"));
				driverNames.setBusinessUnitFlag(rs
						.getString("BUSINESS_UNIT_FLAG"));
				driverNames.setMandatoryFlag(rs.getString("MANDATORY_FLAG"));
				driverNames.setNullDriverFlag(rs.getString("NULL_DRIVER_FLAG"));
				driverNames.setWildcardFlag(rs.getString("WILDCARD_FLAG"));
				driverNames.setRangeFlag(rs.getString("RANGE_FLAG"));
				driverNames.setToNumberFlag(rs.getString("TO_NUMBER_FLAG"));
				driverNames.setActiveFlag(rs.getString("ACTIVE_FLAG"));

				result.put(driverNames.getTransDtlColName(), driverNames);
			}
			rs.close();
		} catch (Exception e) {
			// logger.error("############ getTransactionColumnDriverNamesMapAID() failed: "
			// + e.toString());
		} finally {
			if (stat != null) {
				try {
					stat.close();
				} catch (Exception ex) {
				}
			}
		}

		return result;
	}

	private Map<String, DataDefinitionColumn> getBCPTransactionsColumnMapAID(
			String tableName) {
		Map<String, DataDefinitionColumn> result = new LinkedHashMap<String, DataDefinitionColumn>();

		Statement stat = null;
		try {
			stat = conn.createStatement();
			ResultSet rs = stat
					.executeQuery("select * from TB_DATA_DEF_COLUMN where TABLE_NAME='"
							+ tableName + "' order by COLUMN_NAME");
			while (rs.next()) {
				DataDefinitionColumn dataDefinitionColumn = new DataDefinitionColumn();

				DataDefinitionColumnPK ataDefinitionColumnPK = new DataDefinitionColumnPK(
						rs.getString("TABLE_NAME"), rs.getString("COLUMN_NAME"));
				dataDefinitionColumn
						.setDataDefinitionColumnPK(ataDefinitionColumnPK);

				dataDefinitionColumn.setDataType(rs.getString("DATA_TYPE"));
				dataDefinitionColumn.setDatalength(rs.getLong("DATA_LENGTH"));
				dataDefinitionColumn
						.setDescription(rs.getString("DESCRIPTION"));
				dataDefinitionColumn.setAbbrDesc(rs.getString("ABBR_DESC"));
				dataDefinitionColumn.setDescColumnName(rs
						.getString("DESC_COLUMN_NAME"));
				dataDefinitionColumn.setDefinition(rs.getString("DEFINITION"));
				dataDefinitionColumn.setMapFlag(rs.getString("MAP_FLAG"));
				dataDefinitionColumn.setMinimumViewFlag(rs
						.getString("MINIMUM_VIEW_FLAG"));

				// Do some fixups to make sure AbbrDesc and Desc always exist!
				String property = Util.columnToProperty(dataDefinitionColumn
						.getColumnName());
				String abbr = dataDefinitionColumn.getAbbrDesc();
				if ((abbr == null) || abbr.equals("")) {
					// jmw - This is trying to set value to something > the max
					// field size
					// rather than try to get the size for tb_data_def_column
					// from tb_data_def_column
					// I am hardcoding for now
					String s = Util.propertyToLabel(property);
					dataDefinitionColumn.setAbbrDesc(s.substring(0,
							Math.min(20, s.length()) - 1));
				}
				String desc = dataDefinitionColumn.getDescription();
				if ((desc == null) || desc.equals("")) {
					String s = Util.propertyToLabel(property);
					dataDefinitionColumn.setDescription(s.substring(0,
							Math.min(40, s.length()) - 1));
				}

				result.put(dataDefinitionColumn.getColumnName(),
						dataDefinitionColumn);
			}
			rs.close();
		} catch (Exception e) {
			// logger.error("############ getBCPTransactionsColumnMapAID() failed: "
			// + e.toString());
		} finally {
			if (stat != null) {
				try {
					stat.close();
				} catch (Exception ex) {
				}
			}
		}

		return result;
	}

	private Map<String, ListCodes> getErrorListCodesAID() {
		Map<String, ListCodes> result = new LinkedHashMap<String, ListCodes>();
		Statement stat = null;
		try {
			stat = conn.createStatement();
			ResultSet rs = stat
					.executeQuery("select * from TB_LIST_CODE where CODE_TYPE_CODE='ERRORCODE' order by DESCRIPTION");
			while (rs.next()) {
				ListCodes listCodes = new ListCodes();

				ListCodesPK listCodesPK = new ListCodesPK(
						rs.getString("CODE_TYPE_CODE"),
						rs.getString("CODE_CODE"));
				listCodes.setListCodesPK(listCodesPK);

				listCodes.setDescription(rs.getString("DESCRIPTION"));
				listCodes.setMessage(rs.getString("MESSAGE"));
				listCodes.setExplanation(rs.getString("EXPLANATION"));
				listCodes.setSeverityLevel(rs.getString("SEVERITY_LEVEL"));
				listCodes.setWriteImportLineFlag(rs
						.getString("WRITE_IMPORT_LINE_FLAG"));
				listCodes.setAbortImportFlag(rs.getString("ABORT_IMPORT_FLAG"));

				result.put(listCodes.getCodeCode(), listCodes);
			}
			rs.close();
		} catch (Exception e) {
		} finally {
			if (stat != null) {
				try {
					stat.close();
				} catch (Exception ex) {
				}
			}
		}

		return result;
	}

	private Map<String, ListCodes> getErrorSeverityCodesAID() {
		Map<String, ListCodes> result = new LinkedHashMap<String, ListCodes>();
		Statement stat = null;
		try {
			stat = conn.createStatement();
			ResultSet rs = stat
					.executeQuery("select * from TB_LIST_CODE where CODE_TYPE_CODE='ERRORSEV' order by DESCRIPTION");
			while (rs.next()) {
				ListCodes listCodes = new ListCodes();

				ListCodesPK listCodesPK = new ListCodesPK(
						rs.getString("CODE_TYPE_CODE"),
						rs.getString("CODE_CODE"));
				listCodes.setListCodesPK(listCodesPK);

				listCodes.setDescription(rs.getString("DESCRIPTION"));
				listCodes.setMessage(rs.getString("MESSAGE"));
				listCodes.setExplanation(rs.getString("EXPLANATION"));
				listCodes.setSeverityLevel(rs.getString("SEVERITY_LEVEL"));
				listCodes.setWriteImportLineFlag(rs
						.getString("WRITE_IMPORT_LINE_FLAG"));
				listCodes.setAbortImportFlag(rs.getString("ABORT_IMPORT_FLAG"));

				result.put(listCodes.getCodeCode(), listCodes);
			}
			rs.close();
		} catch (Exception e) {
		} finally {
			if (stat != null) {
				try {
					stat.close();
				} catch (Exception ex) {
				}
			}
		}

		return result;
	}

	public static String getStackTrace(Throwable aThrowable) {
		final Writer result = new StringWriter();
		final PrintWriter printWriter = new PrintWriter(result);
		aThrowable.printStackTrace(printWriter);
		return result.toString();
	}
	
	public String getTimeElapsed(long startTime) {
		long totalTime = System.currentTimeMillis() - startTime;
		long hours = totalTime / (60 * 60 * 1000);
		long minutes = (totalTime - (60 * 60 * 1000) * hours) / (60 * 1000);
		long seconds = (totalTime - (60 * 60 * 1000) * hours - (60 * 1000)
				* minutes) / 1000;
		long milliseconds = totalTime - (60 * 60 * 1000) * hours - (60 * 1000)
				* minutes - seconds * 1000;

		return ("************ Time elapsed : " + hours + " hour(s) " + minutes
				+ " minute(s) " + seconds + "." + milliseconds + " second(s) ************");
	}
	
	public void sendErrorEmail() {
		//0007628, send email
		writeLogAID(getTimeElapsed(startTime));
		
		String emailStop = preferences.getEmailStop();
		String subject = "PinPoint/AID: " + aidProperties.getProperty("original_file") + " Not Imported with Errors";						
		if (emailStop != null && (emailStop.equals("99") || emailStop.equals("20"))) {
			sendEmail(subject);
		}
	}

	public void sendEmail(String subject) {
		// Get from Preferences
		String emailTo = preferences.getEmailTo();
		String emailCc = preferences.getEmailCc();
		String emailHost = preferences.getEmailHost();
		String emailPort = preferences.getEmailPort();
		String emailFrom = preferences.getEmailFrom();

		// Check at AID setting
		if (emailHost == null || emailHost.length() == 0 || emailPort == null
				|| emailPort.length() == 0) {
			writeLogAID("SMTP Settings must be configured");
			return;
		}

		if (emailTo == null || emailTo.length() == 0) {
			writeLogAID("At least one email address must be entered");
			return;
		}
		
		if (emailFrom == null || emailFrom.length() == 0) {
			emailFrom = "pinpoint.support@ryan.com";
		}
		String content = emailContent.toString();
		try {
			sendEmail(emailHost, emailPort, emailFrom, emailTo, emailCc, subject, content);
			writeLogAID("Email notification sent succeeded");
		} catch (Exception e) {
			writeLogAID("Email notification sent failed: " + e.getMessage());
		}
	}

	public void sendEmail(String smtpHost, String smtpPort, String from,
			String to, String cc, String subject, String content)
			throws AddressException, MessagingException {

		// Create a mail session
		java.util.Properties props = new java.util.Properties();
		props.put("mail.smtp.host", smtpHost);
		props.put("mail.smtp.port", smtpPort);
		javax.mail.Session session = javax.mail.Session.getInstance(props, null);

		// Construct the message
		Message msg = new MimeMessage(session);
		msg.setFrom(new InternetAddress(from));

		InternetAddress[] addressTo = InternetAddress.parse(
				to.replaceAll(";", ","), true);
		msg.setRecipients(Message.RecipientType.TO, addressTo);

		if (cc != null && cc.length() > 0) {
			InternetAddress[] addressCc = InternetAddress.parse(
					cc.replaceAll(";", ","), true);
			msg.setRecipients(Message.RecipientType.CC, addressCc);
		}
		msg.setSubject(subject);
		msg.setText(content);

		// Send the message
		Transport.send(msg);
	}
}
