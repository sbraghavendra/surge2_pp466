package com.ncsts.view.bean;

import javax.faces.application.Application;
import javax.faces.application.ViewHandler;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;

import com.ncsts.web.ContextLoaderListener;

public class StartupErrorBean extends loginBean {
	private PageViewHandler viewHandler;
	public void init() {
		Application app = FacesContext.getCurrentInstance().getApplication();
		ViewHandler vh = app.getViewHandler();
		if (!(vh instanceof PageViewHandler)) {
			viewHandler = new PageViewHandler(vh);
			app.setViewHandler(viewHandler);
		} else {
			viewHandler = (PageViewHandler)vh;
		}
		
		ServletContext sc = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
		String errorMessage = "Application failed to start due to Database Error. Error is: ";
		if(sc != null) {
			Object error = sc.getAttribute(ContextLoaderListener.ERROR_KEY);
			if(error != null) {
				errorMessage = errorMessage + (String) error;
			}
		}
		errorMessage += ". Please correct error and restart application.";
		setAppStartupError(errorMessage);
	}
}
