package com.ncsts.view.bean;

public class SitusUserDriverBackingBean extends SitusBaseDriverBackingBean{

	public void init(){
		setActionRole("user");
		setMaintenanceScreen("situs_user_drivers_main");
		setUpdateScreen("situs_user_drivers_update");
	}
	
	public boolean getIsCustomSitus() {
		if(getSelectedSitusDriver()!=null && getSelectedSitusDriver().getCustomBooleanFlag()){
			return true;
		}
		else{
			return false;
		}
	}
	
	public Boolean getIsAllowDelete() {
		return (getDisplayButtons() && getIsCustomSitus());
	}
}
