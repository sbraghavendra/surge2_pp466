package com.ncsts.view.bean;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;
import org.ajax4jsf.component.html.HtmlAjaxSupport;
import org.richfaces.component.html.HtmlDataTable;
import com.ncsts.common.CacheManager;
import com.ncsts.domain.ListCodes;
import com.ncsts.domain.SitusDriver;
import com.ncsts.domain.SitusDriverDetail;
import com.ncsts.dto.SitusDriverDTO;
import com.ncsts.dto.SitusDriverDetailDTO;
import com.ncsts.jsf.model.SitusDriverDataModel;
import com.ncsts.services.ListCodesService;
import com.ncsts.services.SitusDriverService;
import com.ncsts.services.OptionService;

public abstract class SitusBaseDriverBackingBean {
	private boolean initialized = false;
	
	private CacheManager cacheManager;
	private SitusDriverService situsDriverService;
	private FilePreferenceBackingBean filePreferenceBean;
	private ListCodesService listCodesService;
	private OptionService optionService;
	private SitusDriverDataModel situsDriverDataModel;
	
	private EditAction currentAction = EditAction.VIEW;
	private SitusDriver selectedSitusDriver = new SitusDriver();
	private SitusDriverDTO selectedSitusDriverDTO = new SitusDriverDTO();
	
	private SitusDriver filterSitusDriver = new SitusDriver();
	private int selectedRowIndex = -1;
	
	private List<SelectItem> filterTransactionTypeCodeItems;
	private List<SelectItem> filterRateTypeCodeItems;
	private List<SelectItem> filterMethodOfDeliveryItems;
	
	private List<SelectItem> updateTransactionTypeCodeItems;
	private List<SelectItem> updateRateTypeCodeItems;
	private List<SelectItem> updateMethodOfDeliveryItems;
	
	private Map<String, String > driverNameMap = null;
	
	private Map<String, String > transactionTypeMap = null;
	private Map<String, String > ratetypeMap = null;
	private Map<String, String > methodDeliveryMap = null;
	
	private List<SitusDriverDetail> situsDriverDetailList = new ArrayList<SitusDriverDetail>();	
	private List<SitusDriverDetailDTO> situsDriverDetailDTOList = new ArrayList<SitusDriverDetailDTO>();	
	
	private List<SelectItem> driverSelectItems;
	
	private String actionRole = "";
	private String maintenanceScreen = "";
	private String updateScreen = "";
	
	public SitusBaseDriverBackingBean(){
	}
	
	public void setMaintenanceScreen(String screen){
		this.maintenanceScreen = screen;
	}
	
	public void setUpdateScreen(String screen){
		this.updateScreen = screen;
	}
	
	public void setActionRole(String actionRole) {
		this.actionRole = actionRole;		
	}
	
	public boolean isAdminActionRole(){
		return (this.actionRole.equalsIgnoreCase("admin"));
	}
	
	public boolean isUserActionRole(){
		return (this.actionRole.equalsIgnoreCase("user"));
	}
	
	public String getActionRole() {
		return this.actionRole;		
	}
	
	public List<SelectItem> getDriverSelectItems() { 
	   if (driverSelectItems == null) {
		   List<SelectItem> selectItems = new ArrayList<SelectItem>();
		   selectItems.add(new SelectItem("",""));
			
		   Map<String, String > localDriverNameMap = getDriverNameMap();
		   int i=1;
		   for (String driverCode : localDriverNameMap.keySet()) {
			   selectItems.add(new SelectItem(new Long(i), "" + i));
			   i++;
		   }
		   	
		   driverSelectItems = selectItems;
	   }	    
	   return driverSelectItems;
	}
	
	public String getActionText() {
		if(currentAction.equals(EditAction.ADD)){
			return "Add ";
		}
		else if(currentAction.equals(EditAction.UPDATE)){
			return "Update";
		}
		else if(currentAction.equals(EditAction.DELETE)){
			return "Delete";
		}

		return "";
	}
	
	private String verifyData(){
		String errMessage = "";
		
		if(selectedSitusDriverDTO.getTransactionTypeCode()==null || selectedSitusDriverDTO.getTransactionTypeCode().length()==0){
			errMessage = errMessage + "Transaction Type is mandatory. ";
		}
		if(selectedSitusDriverDTO.getRatetypeCode()==null || selectedSitusDriverDTO.getRatetypeCode().length()==0){
			errMessage = errMessage + "Rate Type is mandatory. ";
		}
		if(selectedSitusDriverDTO.getMethodDeliveryCode()==null || selectedSitusDriverDTO.getMethodDeliveryCode().length()==0){
			errMessage = errMessage + "Method of Delivery is mandatory. ";
		}
		
		SitusDriver situsDriver = new SitusDriver();
		situsDriver.setTransactionTypeCode(selectedSitusDriverDTO.getTransactionTypeCode());
		situsDriver.setRatetypeCode(selectedSitusDriverDTO.getRatetypeCode());
		situsDriver.setMethodDeliveryCode(selectedSitusDriverDTO.getMethodDeliveryCode());
		Long situsCount = situsDriverService.count(situsDriver);
		if(situsCount.intValue()>0){	
			errMessage = errMessage + "Transaction Type, Rate Type, and Method of Delivery combination must be unique. ";
		}
		
		if(situsDriverDetailDTOList!=null && situsDriverDetailDTOList.size()>0){
			int driverSize = situsDriverDetailDTOList.size();
			
			Map<Long, Long> driverIDArray = new LinkedHashMap<Long, Long>();
			boolean emptyDriver = false;
			for(SitusDriverDetailDTO situsDriverDetailDTO:situsDriverDetailDTOList){
				
				if(situsDriverDetailDTO.getDriverId()==null || situsDriverDetailDTO.getDriverId().intValue()==0){
					errMessage = errMessage + "Driver ID cannot be empty. ";
					emptyDriver = true;
					break;
				}
				
				driverIDArray.put(situsDriverDetailDTO.getDriverId(), situsDriverDetailDTO.getDriverId());
	    	}
			
			if(!emptyDriver && driverIDArray.size()<driverSize){
				errMessage = errMessage + "Each selected Driver ID must be unique.";
			}
		}

		return errMessage;
	}
	
	public String okDriverAction(){
		if(currentAction == EditAction.ADD){
			String errMessage = verifyData();
			if (errMessage!=null && errMessage.length()>0) {
				FacesMessage message = new FacesMessage(errMessage);
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage(null, message);
				return null;
			}
			
			SitusDriver situsDriver = new SitusDriver();
			situsDriver.setSitusDriverId(selectedSitusDriverDTO.getSitusDriverId());
			situsDriver.setTransactionTypeCode(selectedSitusDriverDTO.getTransactionTypeCode());
			situsDriver.setRatetypeCode(selectedSitusDriverDTO.getRatetypeCode());
			situsDriver.setMethodDeliveryCode(selectedSitusDriverDTO.getMethodDeliveryCode());
			situsDriver.setCustomFlag(selectedSitusDriverDTO.getCustomFlag());
			
			List<SitusDriverDetail> localSitusDriverDetailList = new ArrayList<SitusDriverDetail>();
			
			if(situsDriverDetailDTOList!=null && situsDriverDetailDTOList.size()>0){
				for(SitusDriverDetailDTO situsDriverDetailDTO:situsDriverDetailDTOList){
					SitusDriverDetail situsDriverDetail = new SitusDriverDetail();
					
					situsDriverDetail.setSitusDriverId(situsDriverDetailDTO.getSitusDriverId());
					situsDriverDetail.setSitusDriverDetailId(situsDriverDetailDTO.getSitusDriverDetailId());
					situsDriverDetail.setLocationTypeCode(situsDriverDetailDTO.getLocationTypeCode());
					situsDriverDetail.setDriverId(situsDriverDetailDTO.getDriverId());
					situsDriverDetail.setMandatoryFlag(situsDriverDetailDTO.getMandatoryFlag());
					if(isAdminActionRole()){
						situsDriverDetail.setCustomMandatoryFlag(situsDriverDetailDTO.getMandatoryFlag());
					}
					else{
						situsDriverDetail.setCustomMandatoryFlag(situsDriverDetailDTO.getCustomMandatoryFlag());
					}
					localSitusDriverDetailList.add(situsDriverDetail);
		    	}
			}
			
			situsDriverService.addSitusDriver(situsDriver, localSitusDriverDetailList);
		}
		else if(currentAction == EditAction.UPDATE){
			SitusDriver situsDriver = new SitusDriver();
			situsDriver.setSitusDriverId(selectedSitusDriverDTO.getSitusDriverId());
			situsDriver.setTransactionTypeCode(selectedSitusDriverDTO.getTransactionTypeCode());
			situsDriver.setRatetypeCode(selectedSitusDriverDTO.getRatetypeCode());
			situsDriver.setMethodDeliveryCode(selectedSitusDriverDTO.getMethodDeliveryCode());
			situsDriver.setCustomFlag(selectedSitusDriverDTO.getCustomFlag());
			
			List<SitusDriverDetail> localSitusDriverDetailList = new ArrayList<SitusDriverDetail>();
			
			if(situsDriverDetailDTOList!=null && situsDriverDetailDTOList.size()>0){
				for(SitusDriverDetailDTO situsDriverDetailDTO:situsDriverDetailDTOList){
					SitusDriverDetail situsDriverDetail = new SitusDriverDetail();
					
					situsDriverDetail.setSitusDriverId(situsDriverDetailDTO.getSitusDriverId());
					situsDriverDetail.setSitusDriverDetailId(situsDriverDetailDTO.getSitusDriverDetailId());
					situsDriverDetail.setLocationTypeCode(situsDriverDetailDTO.getLocationTypeCode());
					situsDriverDetail.setDriverId(situsDriverDetailDTO.getDriverId());
					situsDriverDetail.setMandatoryFlag(situsDriverDetailDTO.getMandatoryFlag());
					if(isAdminActionRole()){
						situsDriverDetail.setCustomMandatoryFlag(situsDriverDetailDTO.getMandatoryFlag());
					}
					else{
						situsDriverDetail.setCustomMandatoryFlag(situsDriverDetailDTO.getCustomMandatoryFlag());
					}
					localSitusDriverDetailList.add(situsDriverDetail);
		    	}
			}
			
			situsDriverService.updateSitusDriver(situsDriver, localSitusDriverDetailList);
		}
		else if(currentAction == EditAction.DELETE){
			SitusDriver situsDriver = new SitusDriver();
			situsDriver.setSitusDriverId(selectedSitusDriverDTO.getSitusDriverId());
			situsDriver.setTransactionTypeCode(selectedSitusDriverDTO.getTransactionTypeCode());
			situsDriver.setRatetypeCode(selectedSitusDriverDTO.getRatetypeCode());
			situsDriver.setMethodDeliveryCode(selectedSitusDriverDTO.getMethodDeliveryCode());
			situsDriver.setCustomFlag(selectedSitusDriverDTO.getCustomFlag());

			situsDriverService.deleteSitusDriver(situsDriver);
		}
		
		retrieveFilterSitusDriver();
	
		return maintenanceScreen;
	}
	
	public void sortAction(ActionEvent e) {
		situsDriverDataModel.toggleSortOrder(e.getComponent().getParent().getId());
	}
	
	public String displayAddDriverAction() {
		currentAction = EditAction.ADD;
		selectedSitusDriverDTO = selectedSitusDriver.getSitusDriverDTO();
		selectedSitusDriverDTO = new SitusDriverDTO();
		
		if(isAdminActionRole()){
			selectedSitusDriverDTO.setCustomFlag("0");
		}
		else{
			selectedSitusDriverDTO.setCustomFlag("1");
		}
		
		selectedSitusDriverDTO.setTransactionTypeCode(filterSitusDriver.getTransactionTypeCode());
		selectedSitusDriverDTO.setRatetypeCode(filterSitusDriver.getRatetypeCode());
		selectedSitusDriverDTO.setMethodDeliveryCode(filterSitusDriver.getMethodDeliveryCode());
		
		if(situsDriverDetailDTOList!=null){
			situsDriverDetailDTOList.clear();
		}
		situsDriverDetailDTOList = new ArrayList<SitusDriverDetailDTO>();	
		
		Map<String, String > localDriverNameMap = getDriverNameMap();
		int i=1;
    	for (String driverCode : localDriverNameMap.keySet()) {
    		SitusDriverDetailDTO aSitusDriverDetailDTO = new SitusDriverDetailDTO();
    		aSitusDriverDetailDTO.setLocationTypeCode(driverCode);
    		//0002460: Add screen - Mandatory field
    		aSitusDriverDetailDTO.setMandatoryFlag("0");
    		
    		situsDriverDetailDTOList.add(aSitusDriverDetailDTO);
    	}
	    
		return updateScreen;
	}
	
	public String displayUpdateDriverAction() {
		if(selectedSitusDriver==null){
			return "";
		}

		currentAction = EditAction.UPDATE;
		selectedSitusDriverDTO = selectedSitusDriver.getSitusDriverDTO();
		
		if(situsDriverDetailDTOList!=null){
			situsDriverDetailDTOList.clear();
		}
		situsDriverDetailDTOList = new ArrayList<SitusDriverDetailDTO>();
		
		List<SitusDriverDetail> list = situsDriverService.findAllSitusDriverDetailBySitusDriverId(selectedSitusDriverDTO.getSitusDriverId());
	    if(list != null && list.size() > 0){
	    	for(SitusDriverDetail situsDriverDetail:list){
	    		situsDriverDetailDTOList.add(situsDriverDetail.getSitusDriverDetailDTO());
	    	}
	    } 	
	    
		return updateScreen;
	}
	
	public String displayDeleteDriverAction() {	
		String retPage = displayUpdateDriverAction();
		if(retPage==null || retPage.length()==0){
			return "";
		}
		
		currentAction = EditAction.DELETE;
	    
		return updateScreen;
	}
	
	public Map<String, String > getTransactionTypeMap(){
    	if(transactionTypeMap==null){
    		transactionTypeMap = new LinkedHashMap<String, String >();
    		
    		List<ListCodes> listCodes = cacheManager.getListCodesByType("TRANSTYPE");
 		   	if(listCodes != null) {
 			   for(ListCodes lc : listCodes) {
 				  transactionTypeMap.put(lc.getCodeCode(), lc.getDescription());
 			   }
 		   	}
    	}
    	return transactionTypeMap;
    }
	
	public Map<String, String > getRatetypeMap(){
    	if(ratetypeMap==null){
    		ratetypeMap = new LinkedHashMap<String, String >();
    		
    		List<ListCodes> listCodes = cacheManager.getListCodesByType("RATETYPE");
 		   	if(listCodes != null) {
 			   for(ListCodes lc : listCodes) {
 				  ratetypeMap.put(lc.getCodeCode(), lc.getDescription());
 			   }
 		   	}
    	}
    	return ratetypeMap;
    }
	
	public Map<String, String > getMethodDeliveryMap(){
    	if(methodDeliveryMap==null){
    		methodDeliveryMap = new LinkedHashMap<String, String >();
    		
    		List<ListCodes> listCodes = cacheManager.getListCodesByType("MOD");
 		   	if(listCodes != null) {
 			   for(ListCodes lc : listCodes) {
 				  methodDeliveryMap.put(lc.getCodeCode(), lc.getDescription());
 			   }
 		   	}
    	}
    	return methodDeliveryMap;
    }
	
	public Map<String, String > getDriverNameMap(){
    	if(driverNameMap==null){
    		driverNameMap = new LinkedHashMap<String, String >();
    		
    		List<ListCodes> listCodes = cacheManager.getListCodesByType("LOCNTYPE");
 		   	if(listCodes != null) {
 			   for(ListCodes lc : listCodes) {
 				  driverNameMap.put(lc.getCodeCode(), lc.getDescription());
 			   }
 		   	}
    	}
    	return driverNameMap;
    }
	
	public void retrieveFilterSitusDriver() {
		selectedRowIndex = -1;
		selectedSitusDriver = new SitusDriver();
		
		SitusDriver aSitusDriver = new SitusDriver();
		if(filterSitusDriver.getTransactionTypeCode() != null){
			aSitusDriver.setTransactionTypeCode(filterSitusDriver.getTransactionTypeCode());
		}
		if(filterSitusDriver.getRatetypeCode() != null){
			aSitusDriver.setRatetypeCode(filterSitusDriver.getRatetypeCode());
		}
		if(filterSitusDriver.getMethodDeliveryCode()!= null){
			aSitusDriver.setMethodDeliveryCode(filterSitusDriver.getMethodDeliveryCode());
		}

		situsDriverDataModel.setCriteria(aSitusDriver);
		
		displayDriverDetailAction();
	}
	
	public SitusDriver getFilterSitusDriver(){
		return filterSitusDriver;
	}
	
	public SitusDriverDTO getSelectedSitusDriverDTO(){
		return selectedSitusDriverDTO;
	}
	
	public SitusDriver getSelectedSitusDriver(){
		return selectedSitusDriver;
	}
	
	public String resetFilterSearchAction(){
		filterSitusDriver = new SitusDriver();
		
		return null;
	}
	
	public List<SelectItem> getFilterMethodOfDeliveryItems() { 
	   if (filterMethodOfDeliveryItems == null) {
		   List<SelectItem> selectItems = new ArrayList<SelectItem>();
			
		   selectItems.add(new SelectItem("","All Methods of Delivery"));
		   List<ListCodes> listCodes = cacheManager.getListCodesByType("MOD");
		   if(listCodes != null) {
			   for(ListCodes lc : listCodes) {
				   selectItems.add(new SelectItem(lc.getCodeCode(), lc.getDescription()));
			   }
		   }
			
		   filterMethodOfDeliveryItems = selectItems;
	   }	    
	   return filterMethodOfDeliveryItems;
	}
	
	public List<SelectItem> getFilterRateTypeCodeItems() { 
	   if (filterRateTypeCodeItems == null) {
		   List<SelectItem> selectItems = new ArrayList<SelectItem>();
			
		   selectItems.add(new SelectItem("","All Rate Types"));
		   List<ListCodes> listCodes = cacheManager.getListCodesByType("RATETYPE");
		   if(listCodes != null) {
			   for(ListCodes lc : listCodes) {
				   selectItems.add(new SelectItem(lc.getCodeCode(), lc.getDescription()));
			   }
		   }
			
		   filterRateTypeCodeItems = selectItems;
	   }	    
	   return filterRateTypeCodeItems;
	}
	
	public List<SelectItem> getFilterTransactionTypeCodeItems() { 
	   if (filterTransactionTypeCodeItems == null) {
		   List<SelectItem> selectItems = new ArrayList<SelectItem>();
			
		   selectItems.add(new SelectItem("","All Transaction Types"));
		   List<ListCodes> listCodes = cacheManager.getListCodesByType("TRANSTYPE");
		   if(listCodes != null) {
			   for(ListCodes lc : listCodes) {
				   selectItems.add(new SelectItem(lc.getCodeCode(), lc.getDescription()));
			   }
		   }
			
		   filterTransactionTypeCodeItems = selectItems;
	   }	    
	   return filterTransactionTypeCodeItems;
	}

	public List<SelectItem> getUpdateMethodOfDeliveryItems() { 
	   if (updateMethodOfDeliveryItems == null) {
		   List<SelectItem> selectItems = new ArrayList<SelectItem>();
			
		   selectItems.add(new SelectItem("","Select a Method of Delivery"));
		   List<ListCodes> listCodes = cacheManager.getListCodesByType("MOD");
		   if(listCodes != null) {
			   for(ListCodes lc : listCodes) {
				   selectItems.add(new SelectItem(lc.getCodeCode(), lc.getDescription()));
			   }
		   }
			
		   updateMethodOfDeliveryItems = selectItems;
	   }	    
	   return updateMethodOfDeliveryItems;
	}
	
	public List<SelectItem> getUpdateRateTypeCodeItems() { 
	   if (updateRateTypeCodeItems == null) {
		   List<SelectItem> selectItems = new ArrayList<SelectItem>();
			
		   selectItems.add(new SelectItem("","Select a Sales Type"));
		   List<ListCodes> listCodes = cacheManager.getListCodesByType("RATETYPE");
		   if(listCodes != null) {
			   for(ListCodes lc : listCodes) {
				   selectItems.add(new SelectItem(lc.getCodeCode(), lc.getDescription()));
			   }
		   }
			
		   updateRateTypeCodeItems = selectItems;
	   }	    
	   return updateRateTypeCodeItems;
	}
	
	public List<SelectItem> getUpdateTransactionTypeCodeItems() { 
	   if (updateTransactionTypeCodeItems == null) {
		   List<SelectItem> selectItems = new ArrayList<SelectItem>();
			
		   selectItems.add(new SelectItem("","Select a Transaction Type"));
		   List<ListCodes> listCodes = cacheManager.getListCodesByType("TRANSTYPE");
		   if(listCodes != null) {
			   for(ListCodes lc : listCodes) {
				   selectItems.add(new SelectItem(lc.getCodeCode(), lc.getDescription()));
			   }
		   }
			
		   updateTransactionTypeCodeItems = selectItems;
	   }	    
	   return updateTransactionTypeCodeItems;
	}
	
	public void selectedRowChanged(ActionEvent e) throws AbortProcessingException {
		HtmlDataTable table = (HtmlDataTable) ((HtmlAjaxSupport) e.getComponent()).getParent(); 
		setSelectedRowIndex(table.getRowIndex());
		selectedSitusDriver = new SitusDriver();
		selectedSitusDriver = (SitusDriver)table.getRowData();
		
		displayDriverDetailAction();
	}
	
	public List<SitusDriverDetailDTO> getSitusDriverDetailDTOList() {
		return situsDriverDetailDTOList ;
	}
	
	public void setSitusDriverDetailList(List<SitusDriverDetail> situsDriverDetailList) {
		this.situsDriverDetailList = situsDriverDetailList;
	}	
	
	public List<SitusDriverDetail> getSitusDriverDetailList() {
		return situsDriverDetailList ;
	}
	
	public void displayDriverDetailAction() {
		if(selectedSitusDriver==null || selectedSitusDriver.getSitusDriverId()==null){
			this.setSitusDriverDetailList(null);
			return;
		}
		
		this.setSitusDriverDetailList(null);
		List<SitusDriverDetail> list = situsDriverService.findAllSitusDriverDetailBySitusDriverId(selectedSitusDriver.getSitusDriverId());
		this.setSitusDriverDetailList(list);
	}
	
	public boolean getDeleteAction() {
		return currentAction.equals(EditAction.DELETE);
	}
	
	public boolean getUpdateAction() {
		return currentAction.equals(EditAction.UPDATE);
	}
	
	public Boolean getDisplayButtons() {
		return selectedRowIndex != -1;
	}
	
	public int getSelectedRowIndex() {
		return selectedRowIndex;
	}

	public void setSelectedRowIndex(int selectedRowIndex) {
		this.selectedRowIndex = selectedRowIndex;
	}	
	
	public CacheManager getCacheManager() {
		return this.cacheManager;
	}

	public void setCacheManager(CacheManager cacheManager) {
		this.cacheManager = cacheManager;
	} 
	
	public SitusDriverService getSitusDriverService() {
		return situsDriverService;
	}

	public void setSitusDriverService(SitusDriverService situsDriverService) {
		this.situsDriverService = situsDriverService;
	}
	
	public FilePreferenceBackingBean getFilePreferenceBean() {
		return this.filePreferenceBean;
	}

	public void setFilePreferenceBean(FilePreferenceBackingBean filePreferenceBean) {
		this.filePreferenceBean = filePreferenceBean;
	}
	
	public ListCodesService getListCodesService() {
		return listCodesService;
	}

	public void setListCodesService(ListCodesService listCodesService) {
		this.listCodesService = listCodesService;
	}
	
	public OptionService getOptionService() {
		return optionService;
	}

	public void setOptionService(OptionService optionService) {
		this.optionService = optionService;
	}
	
	public SitusDriverDataModel getSitusDriverDataModel() {
		if(!initialized) {
			retrieveFilterSitusDriver();
			initialized = true;
		}
		return situsDriverDataModel;
	}

	public void setSitusDriverDataModel(SitusDriverDataModel situsDriverDataModel) {
		this.situsDriverDataModel = situsDriverDataModel;
	}
}
