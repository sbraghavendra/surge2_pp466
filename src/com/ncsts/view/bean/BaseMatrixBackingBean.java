package com.ncsts.view.bean;

import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlPanelGrid;
import javax.faces.component.html.HtmlSelectBooleanCheckbox;
import javax.faces.context.FacesContext;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.ajax4jsf.component.html.HtmlAjaxSupport;
import org.apache.log4j.Logger;
import org.richfaces.component.html.HtmlCalendar;
import org.richfaces.component.html.HtmlDataTable;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.ncsts.common.LoggerFactory;
import com.ncsts.common.ObjectCloner;
import com.ncsts.common.Util;
import com.ncsts.common.constants.PurchaseTransactionServiceConstants;
import com.ncsts.common.constants.PurchaseTransactionServiceConstants.SuspendReason;
import com.ncsts.domain.BillTransaction;
import com.ncsts.domain.DataDefinitionColumn;
import com.ncsts.domain.DriverNames;
import com.ncsts.domain.DriverReference;
import com.ncsts.domain.EntityItem;
import com.ncsts.domain.GSBMatrix;
import com.ncsts.domain.GlExtractMap;
import com.ncsts.domain.LocationMatrix;
import com.ncsts.domain.Matrix;
import com.ncsts.domain.PurchaseTransaction;
import com.ncsts.domain.TaxAllocationMatrix;
import com.ncsts.domain.TaxMatrix;
import com.ncsts.domain.TransactionHeader;
import com.ncsts.domain.User;
import com.ncsts.exception.PurchaseTransactionProcessSuspendedException;
import com.ncsts.exception.PurchaseTransactionProcessingException;
import com.ncsts.jsf.model.BaseExtendedDataModel;
import com.ncsts.jsf.model.TransactionDataModel;
import com.ncsts.jsf.model.TransactionSaleDataModel;
import com.ncsts.services.DriverReferenceService;
import com.ncsts.services.EntityItemService;
import com.ncsts.services.MatrixService;
import com.ncsts.services.PurchaseTransactionService;
import com.ncsts.view.util.FacesUtils;
import com.ncsts.view.util.JsfHelper;
import com.ncsts.ws.message.ProcessSuspendedTransactionDocument;
import com.ncsts.ws.message.PurchaseTransactionDocument;
import com.seconddecimal.billing.domain.SaleTransaction;

/**
 * @author Paul Govindan
 *
 */

public abstract class BaseMatrixBackingBean<T extends Matrix, S extends MatrixService<T>>  {
	
    static private Logger logger = LoggerFactory.getInstance().getLogger(BaseMatrixBackingBean.class);
	
	public interface FindIdCallback {
		void findIdCallback(Long id, String action);
	}
	
	public interface SearchCallback {
		void searchCallback(String context);
	}

    private Class<T> clazz;
    
    protected S matrixService;
    
    private HtmlCalendar effectiveDateCalendar;
    private HtmlCalendar effectiveDateLocationCalendar;//bug0003951
    
	protected EditAction currentAction = EditAction.VIEW;
	
	private boolean driverGlobalFlag= false;//bug0003803
	
	private HtmlPanelGrid viewPanel;
	protected HtmlPanelGrid copyAddPanel;
	private HtmlPanelGrid filterSelectionPanel;
	private boolean filterSelectionPanelReadOnly=false;
	
	protected T filterSelectionMatrix;
	
	protected MatrixCommonBean matrixCommonBean;
	private TransactionViewBean transactionViewBean;
    
	protected T selectedMatrixSource;
	protected T selectedMatrix;
	protected T selectedMatrixTempCalc;
	private int selectedRowIndex = -1;
	private PurchaseTransaction selectedTransaction;
	protected PurchaseTransaction sourceTransaction;
	private int selectedTransactionIndex = -1;
	private loginBean loginBean;
	private SearchDriverHandler driverHandler = new SearchDriverHandler();
	
	private BaseExtendedDataModel<T,Long> matrixDataModel;
	
	// Flags that modify view transactions request
	private Boolean processedTransactions;
	private Boolean suspendedTransactions;
	
	private TransactionDataModel transactionDataModel;
	private TransactionSaleDataModel transactionSaleDataModel;
	private HtmlDataTable trDetailTable;
	private boolean displayAllFields = false;
	
	private FindIdCallback findIdCallback = null;
	private String findIdAction;
	private String findIdContext;
	
	private SearchCallback  searchCallback  = null;
	private String searchContext;
	
	protected boolean displayWarning = false;	
	private Long displayBackdateWarning = 0L;

	private long driverCount;
	
	protected Long matrixId;
	
	// ABSTRACT METHODS
	public abstract String getBeanName();
	public abstract boolean getIncludeDescFields();
	public abstract String getMainPageAction();
	public abstract String getDetailFormName();
	
	@Autowired
	private DriverHandlerBean driverHandlerBean;
	
@Autowired
	protected PurchaseTransactionService purchaseTransactionService;	String searchColumnNameLocal = "";
	String selectedUserNameLocal = "";
	private boolean navigatedTransactionProcessScreen = false;
	private  Map<String,String> whatIfMap;
	
	public Map<String, String> getWhatIfMap() {
		return whatIfMap;
	}
	public void setWhatIfMap(Map<String, String> whatIfMap) {
		this.whatIfMap = whatIfMap;
	}
	
	public boolean getIsNavigatedTransactionProcessScreen() {
		return navigatedTransactionProcessScreen;
	}
	
	public void setNavigatedTransactionProcessScreen(
			boolean navigatedTransactionProcessScreen) {
		this.navigatedTransactionProcessScreen = navigatedTransactionProcessScreen;
	}
	
	public String getSearchColumnNameLocal(){
		return searchColumnNameLocal;
	}
	
	public String getSelectedUserNameLocal(){
		return selectedUserNameLocal;
	}
	
	public void setSearchColumnNameLocal(String searchColumnNameLocal) {
		this.searchColumnNameLocal = searchColumnNameLocal;
	}
	public void setSelectedUserNameLocal(String selectedUserNameLocal) {
		this.selectedUserNameLocal = selectedUserNameLocal;
	}
	@SuppressWarnings("unchecked")
	public BaseMatrixBackingBean() {
        this.clazz = (Class<T>) ((ParameterizedType) getClass()
                .getGenericSuperclass()).getActualTypeArguments()[0];
        filterSelectionMatrix = newMatrix();
	}

	protected T newMatrix() {
		try {
			return clazz.newInstance();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

    public S getMatrixService() {
		return matrixService;
	}

	public void setMatrixService(S matrixService) {
		this.matrixService = matrixService;
	}
	
	public Long getDisplayBackdateWarning() {
		return displayBackdateWarning;
	}

	public void setDisplayBackdateWarning(Long displayBackdateWarning) {
		this.displayBackdateWarning = displayBackdateWarning;
	}

	public MatrixCommonBean getMatrixCommonBean() {
		return matrixCommonBean;
	}

	public void setMatrixCommonBean(MatrixCommonBean matrixCommonBean) {
		this.matrixCommonBean = matrixCommonBean;
		driverHandler.setCacheManager(matrixCommonBean.getCacheManager());
		driverHandler.setDriverReferenceService(matrixCommonBean.getDriverReferenceService());
	}

	public TransactionViewBean getTransactionViewBean() {
		return transactionViewBean;
	}
	
	public void setTransactionViewBean(TransactionViewBean transactionViewBean) {
		this.transactionViewBean = transactionViewBean;
	}
	
	public HtmlCalendar getEffectiveDateCalendar() {
		if (effectiveDateCalendar == null) {
			// Create calendar and add validator here as when it was assigned in the jsp
			// multiple validators were being accumulated - one for each page access!
			effectiveDateCalendar = new HtmlCalendar();
			
			effectiveDateCalendar.addValidator(
				//JsfHelper.createValidatorExpression("#{" + getBeanName() + ".validateEffectiveDate}"));
				JsfHelper.createValidatorExpression("#{" + getBeanName() + ".validateEffectiveDate}"));
		}
		return effectiveDateCalendar;
	}

	public void setEffectiveDateCalendar(HtmlCalendar effectiveDateCalendar) {
		this.effectiveDateCalendar = effectiveDateCalendar;
	}

	public HtmlPanelGrid getCopyAddPanel() {
		
			T matrix = newMatrix();
			copyAddPanel = MatrixCommonBean.createDriverPanel(
					matrix.getDriverCode(),
					matrixCommonBean.getCacheManager(),
					matrixCommonBean.getEntityItemDTO(),
					"copyAddPanel", 
					getBeanName(), "selectedMatrix", "matrixCommonBean", 
					false, true, getIncludeDescFields(), true, false); 
		

			if(matrix.getDriverCode().equals("GSB")){
				 matrixCommonBean.prepareTaxDriverPanelGSB(filterSelectionPanel);
		     
			}else{
				matrixCommonBean.prepareTaxDriverPanel(filterSelectionPanel);
			}
		
		return copyAddPanel;
	}

	public void setCopyAddPanel(HtmlPanelGrid copyAddPanel) {
		this.copyAddPanel = copyAddPanel;
	}

	public HtmlPanelGrid getViewPanel() {
		T matrix = newMatrix();
		if (viewPanel == null) {
			
			viewPanel = MatrixCommonBean.createDriverPanel(
					matrix.getDriverCode(), matrixCommonBean.getCacheManager(),
					matrixCommonBean.getEntityItemDTO(),
					"viewPanel", 
					getBeanName(), "selectedMatrix", "matrixCommonBean",
					true, false, getIncludeDescFields(), true, false); 
		}
		if(matrix.getDriverCode().equals("GSB")){
			 matrixCommonBean.prepareTaxDriverPanelGSB(filterSelectionPanel);
	     
		}else{
			matrixCommonBean.prepareTaxDriverPanel(filterSelectionPanel);
		}
		return viewPanel;
	}

	public void setViewPanel(HtmlPanelGrid viewPanel) {
		this.viewPanel = viewPanel;
	}
	
	public HtmlPanelGrid getFilterSelectionPanel() {
		return getFilterSelectionPanel(false);
	}
	
	public HtmlPanelGrid getFilterSelectionPanel(boolean state) {
		// not sure what's the use of this if, there is no other 
		//condition in which it will skip this condition 		
	//	if ((filterSelectionPanel == null) || (state != filterSelectionPanelReadOnly)) {
			T matrix = newMatrix();
			filterSelectionPanel = MatrixCommonBean.createDriverPanel(
					matrix.getDriverCode(), matrixCommonBean.getCacheManager(), 
					matrixCommonBean.getEntityItemDTO(),
					"filterPanel", 
					getBeanName(), "filterSelection", "matrixCommonBean", 
					state, false, false, true, true);
			filterSelectionPanelReadOnly = state;
			
				//	}
			if(matrix.getDriverCode().equals("GSB")){
				 matrixCommonBean.prepareTaxDriverPanelGSB(filterSelectionPanel);
		     
			}else{
				matrixCommonBean.prepareTaxDriverPanel(filterSelectionPanel);
			}
		return filterSelectionPanel;
	}
	
	public void setFilterSelectionPanel(HtmlPanelGrid filterSelectionPanel) {
		this.filterSelectionPanel = filterSelectionPanel;
	}

	public SearchDriverHandler getDriverHandler() {
		return driverHandler;
	}
	
	public void refreshWhatIfDataGrid(String searchColumnName, String selectedUserName){
		if (trDetailTable == null) {
			trDetailTable = new HtmlDataTable();
			trDetailTable.setId("aMDetailList");
			trDetailTable.setVar("trdetail");
		}	
		
		if(selectedMatrix != null && selectedMatrix.getDriverCode().equals("GSB")){
			matrixCommonBean.createSaleTransactionDetailTable(trDetailTable,
					getBeanName(), getBeanName() + ".transactionSaleDataModel", 
					matrixCommonBean.getSaleTransactionPropertyMap(), false, displayAllFields, false, false, false);
		}
		
		if(getIsNavigatedTransactionProcessScreen() == Boolean.TRUE){
			searchColumnNameLocal = searchColumnName;
			selectedUserNameLocal = selectedUserName;
			matrixCommonBean.createWhatIfTransactionDetailTable(trDetailTable,
					getBeanName(), getBeanName() + ".transactionDataModel", 
					false, false, true, searchColumnNameLocal, selectedUserName);
		}
		
		else{
			String gridLayout = getMatrixCommonBean().getUserPreferenceDTO().getGridLayout();
			
			if(gridLayout==null || gridLayout.length()==0){
				searchColumnNameLocal = "*DEFAULT";
				selectedUserNameLocal = "*GLOBAL";
			}
			else{
				if(whatIfMap == null ){
					createWhatIfMap();
				}
				if(gridLayout.equalsIgnoreCase("*LASTVIEW")){
					 matrixCommonBean.getCacheManager().flushAllUsersMap();
					 searchColumnNameLocal = getCurrentUser().getLastPurchView();
					if(searchColumnNameLocal != null && !searchColumnNameLocal.isEmpty() && searchColumnNameLocal.indexOf("/") != -1) {
						selectedUserNameLocal = searchColumnNameLocal.substring(0, searchColumnNameLocal.indexOf("/"));
						searchColumnNameLocal = searchColumnNameLocal.substring(searchColumnNameLocal.indexOf("/") + 1);
						
						if(selectedUserNameLocal == null || searchColumnNameLocal == null || searchColumnNameLocal.isEmpty() || selectedUserNameLocal.isEmpty()) {
							searchColumnNameLocal = "*DEFAULT";
							selectedUserNameLocal = "*GLOBAL";
						}
					}
					else {
						searchColumnNameLocal = "*DEFAULT";
						selectedUserNameLocal = "*GLOBAL";
					}
					
				}
				else {
					 searchColumnNameLocal = gridLayout;	
					 selectedUserNameLocal = whatIfMap.get(gridLayout);
					 
					 if(searchColumnNameLocal == null || selectedUserNameLocal == null) { 
						 searchColumnNameLocal = "*DEFAULT";
						 selectedUserNameLocal = "*GLOBAL";
					 }
				}
			}
			
			matrixCommonBean.createWhatIfTransactionDetailTable(trDetailTable,
					getBeanName(), getBeanName() + ".transactionDataModel", 
					false, false, true, searchColumnNameLocal, selectedUserName);
		}
		
		String rowStr = (displayAllFields)? matrixCommonBean.getMatrixDTO().getSysFullThreshold() : 
			matrixCommonBean.getMatrixDTO().getSysLimitedThreshold();
		int rowpage = 0;
		if(rowStr==null || rowStr.length()==0){
			rowpage = (displayAllFields)? 25:100; 
		}
		else{
			rowpage = Integer.parseInt(rowStr);
		}
		
		String muitipleStr = (displayAllFields)? matrixCommonBean.getMatrixDTO().getSysFullThresholdPages() : 
			matrixCommonBean.getMatrixDTO().getSysLimitedThresholdPages();
		int muitiplepage = 0;
		if(muitipleStr==null || muitipleStr.length()==0){
			muitiplepage = (displayAllFields)? 4:4; 
		}
		else{
			muitiplepage = Integer.parseInt(muitipleStr);
		}
	
		if(selectedMatrix != null && selectedMatrix.getDriverCode().equals("GSB")){
			transactionSaleDataModel.setPageSize(rowpage);
			transactionSaleDataModel.setDbFetchMultiple(muitiplepage);
		}
		else{
			transactionDataModel.setPageSize(rowpage);
			transactionDataModel.setDbFetchMultiple(muitiplepage);
		}
	}
	
	public HtmlDataTable getTrDetailTable() {
		if (trDetailTable == null) {
			trDetailTable = new HtmlDataTable();
			trDetailTable.setId("aMDetailList");
			trDetailTable.setVar("trdetail");
			
			if(selectedMatrix.getDriverCode().equals("GSB")){
				matrixCommonBean.createSaleTransactionDetailTable(trDetailTable,
						getBeanName(), getBeanName() + ".transactionSaleDataModel", 
						matrixCommonBean.getSaleTransactionPropertyMap(), false, displayAllFields, false, false, false);
			}
			else{
				matrixCommonBean.createTransactionDetailTable(trDetailTable,
						getBeanName(), getBeanName() + ".transactionDataModel", 
						matrixCommonBean.getTransactionPropertyMap(), false, displayAllFields, false, false, false);
			}
			
			String rowStr = (displayAllFields)? matrixCommonBean.getMatrixDTO().getSysFullThreshold() : 
				matrixCommonBean.getMatrixDTO().getSysLimitedThreshold();
			int rowpage = 0;
			if(rowStr==null || rowStr.length()==0){
				rowpage = (displayAllFields)? 25:100; 
			}
			else{
				rowpage = Integer.parseInt(rowStr);
			}
			
			String muitipleStr = (displayAllFields)? matrixCommonBean.getMatrixDTO().getSysFullThresholdPages() : 
				matrixCommonBean.getMatrixDTO().getSysLimitedThresholdPages();
			int muitiplepage = 0;
			if(muitipleStr==null || muitipleStr.length()==0){
				muitiplepage = (displayAllFields)? 4:4; 
			}
			else{
				muitiplepage = Integer.parseInt(muitipleStr);
			}
		
			if(selectedMatrix.getDriverCode().equals("GSB")){
				transactionSaleDataModel.setPageSize(rowpage);
				transactionSaleDataModel.setDbFetchMultiple(muitiplepage);
			}
			else{
				transactionDataModel.setPageSize(rowpage);
				transactionDataModel.setDbFetchMultiple(muitiplepage);
			}
		}
		return trDetailTable;
	}

	public void setTrDetailTable(HtmlDataTable trDetailTable) {
		this.trDetailTable = trDetailTable;
	}

	public boolean isDisplayAllFields() {
		return displayAllFields;
	}
	
	public void setDisplayAllFields(boolean displayAllFields) {
		this.displayAllFields = displayAllFields;
	}
	
	public void displayChange(ActionEvent e) {
		HtmlSelectBooleanCheckbox check = (HtmlSelectBooleanCheckbox) ((HtmlAjaxSupport) e.getComponent()).getParent();
	    displayAllFields = (check.getSubmittedValue() == null)? 
	    		((check.getValue() == null)? false:(Boolean) check.getValue()):Boolean.parseBoolean((String) check.getSubmittedValue());
		// Rebuild the table
		logger.debug("Rebuilding table - all fields: " + displayAllFields);
		
		if(selectedMatrix.getDriverCode().equals("GSB")){
			matrixCommonBean.createSaleTransactionDetailTable(trDetailTable,
					getBeanName(), getBeanName() + ".transactionSaleDataModel", 
					matrixCommonBean.getSaleTransactionPropertyMap(), false, displayAllFields, false, false, false);
		}
		else{
			matrixCommonBean.createTransactionDetailTable(trDetailTable,
					getBeanName(), getBeanName() + ".transactionDataModel", 
					matrixCommonBean.getTransactionPropertyMap(), false, displayAllFields, false, false, false);
		}
		
		String rowStr = (displayAllFields)? matrixCommonBean.getMatrixDTO().getSysFullThreshold() : 
			matrixCommonBean.getMatrixDTO().getSysLimitedThreshold();
		int rowpage = 0;
		if(rowStr==null || rowStr.length()==0){
			rowpage = (displayAllFields)? 25:100; 
		}
		else{
			rowpage = Integer.parseInt(rowStr);
		}

		String muitipleStr = (displayAllFields)? matrixCommonBean.getMatrixDTO().getSysFullThresholdPages() : 
			matrixCommonBean.getMatrixDTO().getSysLimitedThresholdPages();
		int muitiplepage = 0;
		if(muitipleStr==null || muitipleStr.length()==0){
			muitiplepage = (displayAllFields)? 4:4; 
		}
		else{
			muitiplepage = Integer.parseInt(muitipleStr);
		}
		
		if(selectedMatrix.getDriverCode().equals("GSB")){
			transactionSaleDataModel.setPageSize(rowpage);
			transactionSaleDataModel.setDbFetchMultiple(muitiplepage);
		}
		else{
			transactionDataModel.setPageSize(rowpage);
			transactionDataModel.setDbFetchMultiple(muitiplepage);
		}
	}

	public void selectedTransactionChanged (ActionEvent e) throws AbortProcessingException { 
	    HtmlDataTable table = (HtmlDataTable) (((HtmlAjaxSupport) e.getComponent()).getParent());
	    
	    
	    if(selectedMatrix.getDriverCode().equals("GSB")){
		}
		else{
			selectedTransaction = (PurchaseTransaction) table.getRowData();
		    selectedTransactionIndex = table.getRowIndex();
		}
	}
	
	public int getSelectedTransactionIndex() {
		return selectedTransactionIndex;
	}
	
	public void setSelectedTransactionIndex(int selectedTransactionIndex) {
		this.selectedTransactionIndex = selectedTransactionIndex;
	}
	
	public T getFilterSelection() {
		return filterSelectionMatrix;
	}

	public T getSelectedMatrix() {
		return (selectedMatrix == null)? selectedMatrixSource:selectedMatrix;
	}
	
	public T setSelectedMatrix(T matrix) {
		return selectedMatrix = matrix;
	}
	
	public int getSelectedRowIndex() {
		return selectedRowIndex;
	}
	
	public void setSelectedRowIndex(int selectedRowIndex) {
		this.selectedRowIndex = selectedRowIndex;
	}
	
	public String getMatrixId() {
		Long id = (selectedMatrix == null)? null:selectedMatrix.getId();
		return ((id == null) || (id <= 0L))? "*NEW":id.toString();
	}

	public boolean getReadOnlyAction() {
		return currentAction.isReadOnlyAction();
	}

	public boolean getViewAction() {
		return currentAction.equals(EditAction.VIEW) || currentAction.equals(EditAction.VIEW_FROM_TRANSACTION) || currentAction.equals(EditAction.VIEW_FROM_TRANSACTION_LOG_HISTORY)
				|| currentAction.equals(EditAction.VIEW_FROM_TRANSACTION_LOG_ENTRY) || 
				currentAction.equals(EditAction.VIEW_FROM_TRANSACTION_LOG_HISTORY) || currentAction.equals(EditAction.VIEW_FROM_MATRIX_COMPARISON);
	}

	public boolean getDeleteAction() {
		return currentAction.equals(EditAction.DELETE);
	}

	public boolean getUpdateAction() {
		return currentAction.equals(EditAction.UPDATE);
	}
	
	public boolean getBackdateAction() {
		return currentAction.equals(EditAction.BACKDATE);
	}
	
	public boolean getAddAction() {
		return currentAction.isAddAction();
	}

	public boolean getAddFromTransactionAction() {
		return currentAction.equals(EditAction.ADD_FROM_TRANSACTION);
	}

	public boolean getReadOnlyDrivers() {
		return (!currentAction.equals(EditAction.ADD) &&
				!currentAction.equals(EditAction.COPY_ADD) &&
				!currentAction.equals(EditAction.COPY_ADD_FROM_TRANSACTION));
	}

	public String getActionText() {
		return currentAction.getActionText();
	}
	
	public String getActionTextSuffix() {
		return currentAction.getActionTextSuffix();
	}
	
	public Boolean getValidSelection() {
		Long id = (selectedMatrixSource == null)? null:selectedMatrixSource.getId();
		return ((id != null) && (id > 0L));
	}
	
	public Boolean getEffectivedateSelection() {
		Long id = (selectedMatrixSource == null)? null:selectedMatrixSource.getId();
		if((id != null) && (id > 0L)){
			if(matrixCommonBean.getLoginBean().getUser().getAdminFlag()!=null && 
					(matrixCommonBean.getLoginBean().getUser().getAdminFlag().equals("1") || 
							matrixCommonBean.getLoginBean().getUser().getAdminFlag().equals("2"))){
				return true;
			}
			else{
				return false;
			}
		}
		else{
			return false;
		}
	}
	
	public Boolean getValidTransactionSelection() {
		Long id = (selectedTransaction == null)? null:selectedTransaction.getId();
		return ((id != null) && (id > 0L));
	}
	
	public String resetFilter() {
		filterSelectionMatrix = newMatrix();
		return null;
	}
	
	public Boolean getProcessedTransactions() {
		return processedTransactions;
	}

	public void setProcessedTransactions(Boolean processedTransactions) {
		this.processedTransactions = processedTransactions;
	}

	public Boolean getSuspendedTransactions() {
		return suspendedTransactions;
	}

	public void setSuspendedTransactions(Boolean suspendedTransactions) {
		this.suspendedTransactions = suspendedTransactions;
	}
	
	protected void updateTransactionDetailFilter(PurchaseTransaction trDetail) {
		// Hook to allow special handling of PurchaseTransaction example
	}
	
	public void findId(FindIdCallback callback, String context, String action) {
		findIdCallback = callback;
		findIdContext = context;
		findIdAction = action;
		resetFilter();
		resetSelection();
	}
	
	public String findAction() {
		findIdCallback.findIdCallback(selectedMatrixSource.getId(), findIdContext);
		exitFindMode();
		return findIdAction;
	}
	
	public String cancelFindAction() {
		exitFindMode();
		return findIdAction;
	}
	
	public boolean getFindMode() {
		return (findIdCallback != null);
	}
	
	public void exitFindMode() {
		if (findIdCallback != null) {
			findIdCallback = null;
			resetFilter();
			resetSelection();
		}
	}
	
	public void filterSaveAction(ActionEvent e) {
    }
	
	public void search(SearchCallback callback, String context) {
		searchCallback = callback;
		searchContext = context;
	}
	
	public void exitSearchMode() {
		if (searchCallback != null) {
			searchCallback = null;
			searchContext = "";
		}
	}
	
	public void searchAction() {
		if (searchCallback != null){
			searchCallback.searchCallback(searchContext);
		}
	}
	
	public String navigateAction() {
		exitFindMode();
		return getMainPageAction();
	}
	
	private void recalculateEffectiveDate(){
		if (!currentAction.equals(EditAction.ADD_FROM_TRANSACTION)){
			//3921
			if(selectedMatrix.getDriverCode().equals("L")){
				selectedMatrix.setEffectiveDate((Date) effectiveDateLocationCalendar.getValue());
			} else if(selectedMatrix.getDriverCode().equals("GSB")){
				selectedMatrix.setEffectiveDate((Date) effectiveDateCalendar.getValue());
			}else if(selectedMatrix.getDriverCode().equals("T")){
				selectedMatrix.setEffectiveDate((Date) effectiveDateCalendar.getValue());
			}else{
				selectedMatrix.setEffectiveDate((Date) effectiveDateCalendar.getValue());
			}
			return;
		}
		
		logger.debug("recalculateEffectiveDate");
		
		PurchaseTransaction trDetail = new PurchaseTransaction();
			
		// Now, map the drivers to transaction detail properties
		matrixCommonBean.setTransactionProperties(selectedMatrix, trDetail);
		
		//3921
		if (currentAction.equals(EditAction.ADD_FROM_TRANSACTION)) {
			// Handle any fields that aren't checked!
			for (String property : matrixCommonBean.findUncheckedDrivers(getViewPanel(), "TRANSPROP")) {
				Util.setProperty(trDetail, property, null, String.class);
			}
		}
		
		//Make sure unchecked item not included
		if (currentAction.equals(EditAction.ADD_FROM_TRANSACTION)) {
			Map<String,DriverNames> transactionDriverNamesMap = 
				matrixCommonBean.getCacheManager().getTransactionPropertyDriverNamesMap(selectedMatrix.getDriverCode());
			for (String property : transactionDriverNamesMap.keySet()) {
				DriverNames driverNames = transactionDriverNamesMap.get(property);
				String driver = driverNames.getMatrixColName();
				if (!getFlagValue(driverNames.getActiveFlag())) {
					Util.setProperty(trDetail, property, null, String.class);
					logger.debug("Setting unchecked property '" + property + "' to null again");
				} 
			}
		}
		
		updateTransactionDetailFilter(trDetail);
		
		transactionDataModel.setFilterCriteria(trDetail, 
				null, selectedMatrix.getExpirationDate(), 
				false, processedTransactions, 
				suspendedTransactions != null && suspendedTransactions ? selectedMatrix.getDriverCode():null, 
				false);
		
		selectedMatrix.setEffectiveDate(transactionDataModel.minGLDate());
	}
	
	public void displayTransactionsAction(ActionEvent e) {
		getTransactionsAction();
	}

	public void getTransactionsAction(){
		logger.debug("getTransactionsAction");
		selectedTransactionIndex = -1;
		selectedTransaction = null;
		recalculateEffectiveDate();
		
		try {
			PurchaseTransaction trDetail = new PurchaseTransaction();
			
			//4620
			EntityItemService entityItemService = this.matrixCommonBean.entityItemService();
			
			EntityItem entityItem =  null;
			if(selectedMatrix.getDriverCode().equals("L")){
				entityItem =  this.matrixCommonBean.entityItemService().findById(((LocationMatrix)selectedMatrix).getEntityId());
			} else if(selectedMatrix.getDriverCode().equals("GSB")){
				entityItem =  this.matrixCommonBean.entityItemService().findById(((GSBMatrix)selectedMatrix).getEntityId());
			}else if(selectedMatrix.getDriverCode().equals("T")){
				entityItem =  this.matrixCommonBean.entityItemService().findById(((TaxMatrix)selectedMatrix).getEntityId());
			}else{
			}
			
			if(entityItem!=null){
				if(entityItem.getEntityCode().equalsIgnoreCase("*ALL")){
					trDetail.setEntityCode(null);
				}
				else{
					trDetail.setEntityCode(entityItem.getEntityCode());
				}
			}

			// Now, map the drivers to transaction detail properties
			matrixCommonBean.setTransactionProperties(selectedMatrix, trDetail);
			if(selectedMatrix instanceof LocationMatrix) {
				Util.setProperty(trDetail, "entityId", ((LocationMatrix) selectedMatrix).getEntityId(), Long.class);
			}
			else if(selectedMatrix instanceof TaxMatrix) {
				Util.setProperty(trDetail, "entityId", ((TaxMatrix) selectedMatrix).getEntityId(), Long.class);
			}
			if (currentAction.equals(EditAction.ADD_FROM_TRANSACTION)) {
				// Handle any fields that aren't checked!
				for (String property : matrixCommonBean.findUncheckedDrivers(getViewPanel(), "TRANSPROP")) {
					logger.debug("Setting unchecked property '" + property + "' to null");
					Util.setProperty(trDetail, property, null, String.class);
				}
			}
			
			//Make sure unchecked item not included
			if (currentAction.equals(EditAction.ADD_FROM_TRANSACTION)) {
				Map<String,DriverNames> transactionDriverNamesMap = 
					matrixCommonBean.getCacheManager().getTransactionPropertyDriverNamesMap(selectedMatrix.getDriverCode());
				for (String property : transactionDriverNamesMap.keySet()) {
					DriverNames driverNames = transactionDriverNamesMap.get(property);
					String driver = driverNames.getMatrixColName();
					if (!getFlagValue(driverNames.getActiveFlag())) {
						Util.setProperty(trDetail, property, null, String.class);
						logger.debug("Setting unchecked property '" + property + "' to null again");
					} 
				}
			}
			
			updateTransactionDetailFilter(trDetail);
			
			// Set filter parameters on data model - ignore effective date per #2788
			transactionDataModel.setFilterCriteria(trDetail, 
					null, selectedMatrix.getExpirationDate(), 
					false, processedTransactions, 
					suspendedTransactions? selectedMatrix.getDriverCode():null, 
					false);
			
			//Test variables
			Long test1 = transactionDataModel.count();
			String test2 = transactionDataModel.getIncludeSuspended();
			List<PurchaseTransaction> test3 = transactionDataModel.fetchData(0, test1.intValue());
			//transactionDataModel.
			System.out.println("test");
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e);
		}
	}
	
	public void getSaleTransactionsAction(){
		logger.debug("getSaleTransactionsAction");
		selectedTransactionIndex = -1;
		selectedTransaction = null;
		recalculateEffectiveDate();
		
		try {
			//PurchaseTransaction trDetail = new PurchaseTransaction();
			BillTransaction trDetail = new BillTransaction();
			//4620
			Long entityId = ((GSBMatrix)selectedMatrix).getEntityId();
			if(entityId!=null && entityId>0L){
				trDetail.setEntityId(entityId);
			}
			else{
				trDetail.setEntityId(null);
			}
			
			// Now, map the drivers to transaction detail properties
			matrixCommonBean.setSaleTransactionProperties(selectedMatrix, trDetail);

			if (currentAction.equals(EditAction.ADD_FROM_TRANSACTION)) {
				// Handle any fields that aren't checked!
				for (String property : matrixCommonBean.findUncheckedDrivers(getViewPanel(), "TRANSPROP")) {
					logger.debug("Setting unchecked property '" + property + "' to null");
					Util.setProperty(trDetail, property, null, String.class);
				}
			}
			
			//Make sure unchecked item not included
			if (currentAction.equals(EditAction.ADD_FROM_TRANSACTION)) {
				Map<String,DriverNames> transactionDriverNamesMap = 
					matrixCommonBean.getCacheManager().getTransactionPropertyDriverNamesMap(selectedMatrix.getDriverCode());
				for (String property : transactionDriverNamesMap.keySet()) {
					DriverNames driverNames = transactionDriverNamesMap.get(property);
					String driver = driverNames.getMatrixColName();
					if (!getFlagValue(driverNames.getActiveFlag())) {
						Util.setProperty(trDetail, property, null, String.class);
						logger.debug("Setting unchecked property '" + property + "' to null again");
					} 
				}
			}

			//updateTransactionDetailFilter(trDetail);
			
			// Set filter parameters on data model - ignore effective date per #2788
			//transactionSaleDataModel.setFilterCriteria(trDetail);
			
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e);
		}
	}
	
	public void searchDriver(ActionEvent e) {
		String id = (String) e.getComponent().getAttributes().get("ID");
		logger.info("LLLLLLLLLLLLLLLLLLLLLLLLLLLLLL"+id);
		
		T m=null;
		 boolean panel=false;
	     m = (id.equals("filterPanel")? filterSelectionMatrix:selectedMatrix);
	     if (m == null) m = newMatrix();
	     driverHandlerBean.initSearch(m, (HtmlInputText) e.getComponent().getParent().getChildren().get(0), e.getComponent());
	     driverHandlerBean.setCallBackScreen(null);
	}
	
	public DriverHandlerBean getDriverHandlerBean(){
		return driverHandlerBean;
	}
	
	protected void updateMatrixFilter(T matrixFilter) {
		// Hook to modify matrix filter example
	}
	
	public void updateMatrixList (ActionEvent e) {         
		T matrix = newMatrix();
		BeanUtils.copyProperties(filterSelectionMatrix, matrix);
		
		// Fix the driver values
		matrix.adjustDriverFields();
		matrix.clearDriverDesc();
		
		// NULLs get converted to 0, change back to null
		Long id = matrix.getId();
		if ((id != null) && (id <= 0L)) {
			matrix.setId(null);
		}
		
		updateMatrixFilter(matrix);

	  	matrixDataModel.setExampleInstance(matrix);
	  	resetSelection();
    }
	
	protected void prepareDetailDisplay(EditAction action, PurchaseTransaction detail) {
		// Hook to provide class specific updates
		displayWarning = false;

		if (!action.isReadOnlyAction()) {
			// Make sure defaults are established
			// if we're not in a read only situation
			selectedMatrix.initializeDefaults();
			
			// Clear any inactive drivers
			Map<String,DriverNames> matrixDriverNamesMap = 
				getMatrixCommonBean().getCacheManager().getMatrixColDriverNamesMap(selectedMatrix.getDriverCode());
			
			//3371: show current values for inactive drivers
			// I hope this is right thing to disable the below: JMW
//			for (DriverNames driverName : matrixDriverNamesMap.values()) {
//				if (!getFlagValue(driverName.getActiveFlag())) {
//					String driver = Util.columnToProperty(driverName.getMatrixColName());
//					Util.setProperty(selectedMatrix, driver, null, String.class);
//					if (selectedMatrix.getHasDescriptions()) {
//						Util.setProperty(selectedMatrix, driver + "Desc", null, String.class);
//					}
//				}
//			}
			
			//3566: Values entered into inactive fields in the selection filter should not be defaulted onto the Add screen.
			if (currentAction.equals(EditAction.ADD)){
				for (DriverNames driverName : matrixDriverNamesMap.values()) {
					String driver = Util.columnToProperty(driverName.getMatrixColName());
					if (!getFlagValue(driverName.getActiveFlag())) {
						Util.setProperty(selectedMatrix, driver, null, String.class);
					}
					else{
						String curValue = Util.getPropertyAsString(selectedMatrix, driver);
						if(curValue==null || curValue.length()==0){
							Util.setProperty(selectedMatrix, driver, "*ALL", String.class);
						}
					}
				}
			}		

			// Reset what-if display
			if (transactionDataModel != null) {
				transactionDataModel.resetFilterCriteria();
				selectedTransaction = null;
				processedTransactions = false;
				suspendedTransactions = true;
			}

			// Refresh form controls
			FacesUtils.refreshComponentTree(copyAddPanel);
		}
		
		// Traverse the driver panel turn off all checkboxes
		matrixCommonBean.initDriverPanelCheckboxes(viewPanel, null);
		
		// Always reset the calendar
		//3921
		if(selectedMatrix!=null && selectedMatrix.getDriverCode()!=null && selectedMatrix.getDriverCode().equals("L")){
			FacesUtils.refreshComponent(effectiveDateLocationCalendar);
		}
		else{
			FacesUtils.refreshComponent(effectiveDateCalendar);
		}
	}
	
	@SuppressWarnings("unchecked")
	protected T copyMatrix(T source) {
		try {
			return (T)ObjectCloner.deepCopy(source);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public String displayViewAction() {
		currentAction = EditAction.VIEW;
		selectedMatrix = selectedMatrixSource;
		prepareDetailDisplay(currentAction, null);
	    return "matrix_view";
	}
	
	public String viewFromMatrixComparison(Long id, String view) {
		currentAction = EditAction.VIEW_FROM_MATRIX_COMPARISON;
		selectedMatrix = matrixService.findById(id);
		if(selectedMatrix !=null) {
			prepareDetailDisplay(currentAction, null);
		}
		else {
			return null;
		}
		
	    return view;
	}
	
	public String viewFromTransaction(Long id, String view) {
		currentAction = EditAction.VIEW_FROM_TRANSACTION;
		selectedMatrix = matrixService.findById(id);
		if(selectedMatrix == null){
			return null;
		}
		prepareDetailDisplay(currentAction, null);
	    return view;
	}
	
	public String displayUpdateAction() {
		currentAction = EditAction.UPDATE;
		// Make a copy
		selectedMatrix = copyMatrix(selectedMatrixSource);

		prepareDetailDisplay(currentAction, null);
	    return "matrix_update";
	}
	
	public String displayBackDateAction(){
		currentAction = EditAction.BACKDATE;
		// Make a copy
		selectedMatrix = copyMatrix(selectedMatrixSource);

		prepareDetailDisplay(currentAction, null);
	    return "matrix_backdate";
	}

	public String displayCopyAddAction() {
		currentAction = EditAction.COPY_ADD;
		// Make copy and reset ID
		selectedMatrix = copyMatrix(selectedMatrixSource);
		selectedMatrix.setId(null);
		selectedMatrix.setEffectiveDate(null);
		selectedMatrix.setExpirationDate(null);

		prepareDetailDisplay(currentAction, null);
	    return "matrix_copy_add";
	}	
	
	public String displayAddAction() {
		currentAction = EditAction.ADD;
		selectedMatrix = newMatrix();

		// Start with current filter selection
		selectedMatrix = copyMatrix(filterSelectionMatrix);
		selectedMatrix.setId(null);
		selectedMatrix.adjustDriverFields();
		
		// Now, map entity level value to the driver properties
		matrixCommonBean.setDriverEntityProperties(selectedMatrix);
		
		if(selectedMatrix instanceof TaxMatrix) {
			((TaxMatrix) selectedMatrix).setActiveFlag("1");
		}
		
		if(selectedMatrix instanceof GSBMatrix){
			((GSBMatrix) selectedMatrix).setActiveFlag("1");
		}

		
		prepareDetailDisplay(currentAction, null);
	    return "matrix_add";
	}	
	
	public void addFromTransaction(PurchaseTransaction detail) {
		sourceTransaction = detail;
		currentAction = EditAction.ADD_FROM_TRANSACTION;
		viewPanel = null; // avoid caching problems
		selectedMatrix = newMatrix();
		matrixCommonBean.setMatrixProperties(detail, selectedMatrix);
		
		// Now, map entity level value to the driver properties
		matrixCommonBean.setDriverEntityProperties(selectedMatrix);
		
		if(selectedMatrix instanceof TaxMatrix) {
			((TaxMatrix) selectedMatrix).setActiveFlag("1");
		}
		if(selectedMatrix instanceof GSBMatrix) {
			((GSBMatrix) selectedMatrix).setActiveFlag("1");
		}
		prepareDetailDisplay(currentAction, detail);
		
		// Now, init checkboxes based on transaction
		matrixCommonBean.initDriverPanelCheckboxes(getViewPanel(), selectedMatrix);
	}
	
	public void addFromTransaction(T matrix) {
		currentAction = EditAction.COPY_ADD_FROM_TRANSACTION;

		selectedMatrix = copyMatrix(matrix);
		selectedMatrix.setId(null);
		selectedMatrix.setEffectiveDate(null);
		selectedMatrix.setExpirationDate(null);
		
		prepareDetailDisplay(currentAction, null);
	}
	
	public String displayDeleteAction() {
		currentAction = EditAction.DELETE;
		selectedMatrix = selectedMatrixSource;

		prepareDetailDisplay(currentAction, null);
	    return "matrix_delete";
	}		
	
	public void copyUpdateListener(ActionEvent e) {
		if (currentAction.equals(EditAction.UPDATE)) {
			// Change to a copy update action
			currentAction = EditAction.COPY_UPDATE;
			
			// Make a copy of the current matrix
			T matrix = newMatrix();
			BeanUtils.copyProperties(selectedMatrix, matrix);
			matrix.setId(null);
			matrix.setEffectiveDate(new com.ncsts.view.util.DateConverter().getUserLocalDate());
			FacesUtils.refreshComponentTree(effectiveDateCalendar);
			selectedMatrix = matrix;
		}
	}
	
	public String viewTransactionAction() {
		transactionViewBean.setSelectedTransaction(selectedTransaction);
		transactionViewBean.setOkAction(getMainPageAction().replace("main", "detail"));
		return "trans_view";
	}

	public String viewSourceTransactionAction() {
		transactionViewBean.setSelectedTransaction(sourceTransaction);
		transactionViewBean.setOkAction(getMainPageAction().replace("main", "detail"));
		transactionViewBean.setCurrentAction(EditAction.VIEW_FROM_TRANSACTION);
		return "trans_view";
	}

	public boolean validateDriverSel(UIComponent toValidate) {
		boolean valid = true;
		Long id = selectedMatrix.getId();
		if (currentAction.equals(EditAction.ADD_FROM_TRANSACTION) && 
			((id == null) || (id <= 0L))) {
			String code = selectedMatrix.getDriverCode();
			
			Long minTaxDriverCount = getMatrixCommonBean().getMatrixDTO().getSysMinDriver();
			Long minLocationDriverCount = getMatrixCommonBean().getMatrixDTO().getSysMinLocationDriver();
			
			driverCount = 0L;
			findCheckedDrivers(getViewPanel(), "TRANSPROP");
			logger.debug("driverCount "+driverCount);
			// Ensure sufficient tax drivers supplied
			String txtMessage = "";
			if ((code.equals("T")||code.equals("GSB"))&& (driverCount < minTaxDriverCount)) {
				txtMessage = String.format("At least %d drivers must be entered.", minTaxDriverCount);
			} else if (code.equals("L") && (driverCount < minLocationDriverCount)) {
				txtMessage = String.format("At least %d drivers must be entered.", minLocationDriverCount);
			} 
			if (txtMessage.length() > 0) {
				FacesMessage message = new FacesMessage(txtMessage);
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage(null, message);
				valid = false;
			}
		}
		
		return valid;
	}
	
	private void findCheckedDrivers(UIComponent parent, String attribute) {
		if ((parent != null) && (parent.getChildCount() > 0)) {
			for (UIComponent child : parent.getChildren()) {
				if (child instanceof HtmlSelectBooleanCheckbox) {
					HtmlSelectBooleanCheckbox input = (HtmlSelectBooleanCheckbox) child;
					Boolean value = (Boolean) input.getValue();
					if(value != null && value == true){
						logger.debug("trues " + value);
						driverCount++;
					}
				}
				
				// recurse
				findCheckedDrivers(child, attribute);
			}
		}
	}
	
	public long CalcBinaryWeight() {
		selectedMatrixTempCalc = copyMatrix(selectedMatrix);
		if (currentAction.equals(EditAction.ADD_FROM_TRANSACTION)){
			for (String property : matrixCommonBean.findUncheckedDrivers(getViewPanel(), "DRIVERPROP")) {
				logger.debug("Setting unchecked property in CalcBinaryWeight()'" + property + "' to *ALL");
				Util.setProperty(selectedMatrixTempCalc, property, "*ALL", String.class);
			}
		}

		// Calculate Binary Weight
		long binaryWeight = 0;
		
		for(Long x = 1l,y = selectedMatrixTempCalc.getDriverCount()-1; x<=selectedMatrixTempCalc.getDriverCount()& y >= 0; x++, y--) {
			if(selectedMatrixTempCalc.getDriver(x) == null || "".equalsIgnoreCase(selectedMatrixTempCalc.getDriver(x))) {
				selectedMatrixTempCalc.setDriver(x, "*ALL");
			}
			if(!"*ALL".equals(selectedMatrixTempCalc.getDriver(x))) {
				logger.debug("::::" + selectedMatrixTempCalc.getDriver(x) + ":" + x + ":" +y);
				Double mult = Math.pow(2, y);
				binaryWeight = binaryWeight + mult.longValue();
			}
		}	
		
		selectedMatrixTempCalc = null;
		return binaryWeight;
	}
	
	
	public String CalcSignificantDigits() {
		selectedMatrixTempCalc = copyMatrix(selectedMatrix);
		if (currentAction.equals(EditAction.ADD_FROM_TRANSACTION)){
			for (String property : matrixCommonBean.findUncheckedDrivers(getViewPanel(), "DRIVERPROP")) {
				logger.debug("Setting unchecked property in CalcSignificantDigits()'" + property + "' to *ALL");
				Util.setProperty(selectedMatrixTempCalc, property, "*ALL", String.class);
			}
		}
		
		// Build Significant Digits
		String significantDigits = "";
		// x Drivers
		for(Long x = 1l; x <= selectedMatrixTempCalc.getDriverCount(); x++) {
			if(selectedMatrixTempCalc.getDriver(x) == null || "".equalsIgnoreCase(selectedMatrixTempCalc.getDriver(x))) {
				selectedMatrixTempCalc.setDriver(x, "*ALL");
			}
			
			if(selectedMatrixTempCalc.getDriver(x).contains("%")) {
				int y = selectedMatrixTempCalc.getDriver(x).indexOf("%");//the position that '%' is found in driver(x);
				significantDigits = significantDigits + String.format("%03d", y);
			} else {
				if("*ALL".equals(selectedMatrixTempCalc.getDriver(x))) {
					significantDigits = significantDigits + "000";
				} else {
					significantDigits = significantDigits + "999";
				}
			}
			if(x < selectedMatrixTempCalc.getDriverCount())
				significantDigits = significantDigits + ".";
		}
		
		selectedMatrixTempCalc = null;
		return significantDigits;
	}
	
	public String saveAction() {
		System.out.println("In Save Action");
		recalculateEffectiveDate();
		
		displayWarning = false;
		selectedMatrix.initNullDriverFields();
		
		
		if (currentAction.equals(EditAction.UPDATE)) {
			logger.debug("Updating existing matrix: " + selectedMatrix.getId());
			matrixService.update(selectedMatrix);
		} else {
			if (currentAction.equals(EditAction.ADD_FROM_TRANSACTION)) {
				logger.debug("Adding from transaction");
				boolean valid = true;
				
				//0001650: Wildcards with What if
				valid = validateCheckedWildDrivers(FacesContext.getCurrentInstance());
				
				if (!valid) {
					return null;
				}
				
				//Fix for 3371: 2. Write *ALL and no description to the Tax/Location Matrix.  
				//Handle fields that inactive!
				Map<String,DriverNames> transactionDriverNamesMap = 
					matrixCommonBean.getCacheManager().getTransactionPropertyDriverNamesMap(selectedMatrix.getDriverCode());
				for (String property : transactionDriverNamesMap.keySet()) {
					DriverNames driverNames = transactionDriverNamesMap.get(property);
					String driver = driverNames.getMatrixColName();
					if (!getFlagValue(driverNames.getActiveFlag())) {
						logger.debug("Setting inactive '"+driver+"' value to '"+"*ALL"+"' ("+ property + ")");
						try {
							Util.setProperty(selectedMatrix, Util.columnToProperty(driver), "*ALL", String.class);
							if(!(selectedMatrix instanceof TaxAllocationMatrix)){//No driver description
								Util.setProperty(selectedMatrix, Util.columnToProperty(driver)+"Desc", null, String.class);
							}
						} catch (RuntimeException e) {
							logger.error("Could not set '"+driver+"' value to '"+"*ALL"+"' from " + property);
						}
					} 
				}
				
				// Handle any fields that aren't checked!
				for (String property : matrixCommonBean.findUncheckedDrivers(getViewPanel(), "DRIVERPROP")) {
					logger.debug("Setting unchecked property '" + property + "' to *ALL");
					Util.setProperty(selectedMatrix, property, "*ALL", String.class);
					if(!(selectedMatrix instanceof TaxAllocationMatrix)){//No driver description
						Util.setProperty(selectedMatrix, property+"Desc", null, String.class);
					}
				}
	
			} else if (currentAction.equals(EditAction.COPY_UPDATE)) {
				if (selectedMatrix.getEffectiveDate().equals(selectedMatrixSource.getEffectiveDate())) {
					FacesMessage message = new FacesMessage("Effective date cannot be same as original matrix.");
					message.setSeverity(FacesMessage.SEVERITY_ERROR);
					FacesContext.getCurrentInstance().addMessage(null, message);
					return null;
				}
			} else if (matrixService.matrixExists(selectedMatrix, false)) {
				FacesMessage message = new FacesMessage("Driver combination already exists.");
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage(null, message);
				return null;
			}
			
			logger.debug("Creating new matrix");
			System.out.println("Creating New Matrix");

			matrixService.save(selectedMatrix);	
			matrixId = selectedMatrix.getId();
			System.out.println("Created New Matrix: " + matrixId);
		}
        matrixDataModel.refreshData(false);
        resetSelection();
        
        //Search on calling page if needed
        if (currentAction.equals(EditAction.ADD_FROM_TRANSACTION) ||
			currentAction.equals(EditAction.COPY_ADD_FROM_TRANSACTION)) {
        	searchAction();
		}
        
	    return mainPageAction();
	}
	
	public String refreshAction() {
	    matrixDataModel.refreshData(false);
	    resetSelection();

	    return mainPageAction();
	}
	
	public String deleteAction() {
		if (currentAction.equals(EditAction.DELETE)) {
			logger.info("Deleting matrixId: " + selectedMatrix.getId());	    	
	        matrixService.remove(selectedMatrix); 
	
	        matrixDataModel.refreshData(false);
	        resetSelection();
		}
	    return mainPageAction();
	}	
	
	public boolean verifyData(){
		boolean errMessage = false; 
		
		Date effectTS = getSelectedMatrix().getEffectiveDate();
		Date exprTS = getSelectedMatrix().getExpirationDate();

		boolean hasDateIssue = false;
		if(effectTS==null || exprTS==null){
			if (effectTS==null) {  
				errMessage = true;
				FacesMessage message = new FacesMessage("Effective Date may not be blank.");
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage(null, message);
				hasDateIssue = true;
			}
			if (exprTS==null) {
				errMessage = true;
				FacesMessage message = new FacesMessage("Expiration Date may not be blank.");
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage(null, message);
				hasDateIssue = true;
			}
		}
	
		if(hasDateIssue){
			return errMessage;
		}
	
		if (effectTS!=null && exprTS!=null && exprTS.before(effectTS)) {
			errMessage = true;
			FacesMessage message = new FacesMessage("Expiration Date may not be earlier than Effective Date.");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
		} 

		return errMessage;
	}
	
	public boolean goEffectiveDate(){
		Date effectTS = getSelectedMatrix().getEffectiveDate();
		
		GregorianCalendar yesterday = new GregorianCalendar();
		yesterday.add(GregorianCalendar.DATE, -1);
		if (!effectTS.after(yesterday.getTime())) {
			return false;
		}

		return true;
	}
	
	public String backdateAction() {
		if(getDisplayBackdateWarning() == 0L) {
			if (verifyData()) {
				return null;
			}
			
			//Check driver combination
			if(matrixService.matrixExists(selectedMatrix, true)){
				FacesMessage message = new FacesMessage("New Effective date must not already exist for the driver combination.");
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage(null, message);	
				return null;
			}
			
			//Get min effective date
			Date minDate = matrixService.minEffectiveDate(selectedMatrix);
			if(minDate==null){
				FacesMessage message = new FacesMessage("Matrix not exist.");
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage(null, message);	
				return null;
			}
			
			Long effectiveDateCount = matrixService.matrixEffectiveDateCount(selectedMatrix);
			
			//Compare min effective date and new effective date
			if (effectiveDateCount > 1L && selectedMatrix.getEffectiveDate().compareTo(minDate)<0){
				setDisplayBackdateWarning(1L);
				return null;
			}

			if(!goEffectiveDate()){
				setDisplayBackdateWarning(2L);
				return null;
			}	
		}
		else {
			setDisplayBackdateWarning(0L);
		}
		
		if (currentAction.equals(EditAction.BACKDATE)) {   	
	        matrixService.save(selectedMatrix); 
	
	        matrixDataModel.refreshData(false);
	        resetSelection();
		}
	    return mainPageAction();
	}
	
	public String cancelAction() {
		selectedMatrix = null;
	    return mainPageAction();
	}
	
	public String mainPageAction() {
		if (currentAction.equals(EditAction.ADD_FROM_TRANSACTION) ||
			currentAction.equals(EditAction.COPY_ADD_FROM_TRANSACTION)) {
			
			exitSearchMode();
			return "trans_main";
		}
		else if(currentAction.equals(EditAction.VIEW_FROM_TRANSACTION)) {
			return "trans_view";
		}
		
		else if(currentAction.equals(EditAction.VIEW_FROM_TRANSACTION_LOG_HISTORY)) {
			return "trans_history";
		}
		else if(currentAction.equals(EditAction.VIEW_FROM_TRANSACTION_LOG_ENTRY)) {
			return "trans_log_entry_view";
		}
		else if(currentAction.equals(EditAction.VIEW_FROM_MATRIX_COMPARISON)) {
			return "matrix_analysis";
		}
		
		return getMainPageAction();
	}
	
	@SuppressWarnings("unchecked")
	public void selectedRowChanged(ActionEvent e) throws AbortProcessingException { 
	    HtmlDataTable table = (HtmlDataTable) ((HtmlAjaxSupport) e.getComponent()).getParent(); 
		selectedRowIndex = table.getRowIndex();
	    selectedMatrixSource = ((T) table.getRowData());
		logger.debug("Selected row: " + selectedRowIndex + 
				"  Matrix ID: " + ((selectedMatrixSource == null)? "NULL":selectedMatrixSource.getId()));		
	}
	
	public void resetSelection() {
		selectedRowIndex = -1;
		selectedMatrixSource = null;
		selectedMatrix = null;
	}

	public BaseExtendedDataModel<T, Long> getMatrixDataModel() {
		return matrixDataModel;
	}

	public void setMatrixDataModel(BaseExtendedDataModel<T, Long> matrixDataModel) {
		this.matrixDataModel = matrixDataModel;
	}

	public TransactionDataModel getTransactionDataModel() {
		return transactionDataModel;
	}

	public void setTransactionDataModel(TransactionDataModel transactionDataModel) {
		this.transactionDataModel = transactionDataModel;
		
		/* xx
		String rowStr = (displayAllFields)? matrixCommonBean.getUserPreferenceDTO().getUserThreshHold() : 
			matrixCommonBean.getUserPreferenceDTO().getLimitedUserThreshHold();
		int rowpage = 0;
		if(rowStr==null || rowStr.length()==0){
			rowpage = (displayAllFields)? 25:100; 
		}
		else{
			rowpage = Integer.parseInt(rowStr);
		}
		this.transactionDataModel.setPageSize(rowpage);
		this.transactionDataModel.setDbFetchMultiple((displayAllFields)? 20:5);*/
		
		String rowStr = (displayAllFields)? matrixCommonBean.getMatrixDTO().getSysFullThreshold() : 
			matrixCommonBean.getMatrixDTO().getSysLimitedThreshold();
		int rowpage = 0;
		if(rowStr==null || rowStr.length()==0){
			rowpage = (displayAllFields)? 25:100; 
		}
		else{
			rowpage = Integer.parseInt(rowStr);
		}
		
		String muitipleStr = (displayAllFields)? matrixCommonBean.getMatrixDTO().getSysFullThresholdPages() : 
			matrixCommonBean.getMatrixDTO().getSysLimitedThresholdPages();
		int muitiplepage = 0;
		if(muitipleStr==null || muitipleStr.length()==0){
			muitiplepage = (displayAllFields)? 4:4; 
		}
		else{
			muitiplepage = Integer.parseInt(muitipleStr);
		}
		
		transactionDataModel.setPageSize(rowpage);
		transactionDataModel.setDbFetchMultiple(muitiplepage);
	}
	
	//////////
	public TransactionSaleDataModel getTransactionSaleDataModel() {
		return transactionSaleDataModel;
	}

	public void setTransactionSaleDataModel(TransactionSaleDataModel transactionSaleDataModel) {
		this.transactionSaleDataModel = transactionSaleDataModel;
		
		String rowStr = (displayAllFields)? matrixCommonBean.getMatrixDTO().getSysFullThreshold() : 
			matrixCommonBean.getMatrixDTO().getSysLimitedThreshold();
		int rowpage = 0;
		if(rowStr==null || rowStr.length()==0){
			rowpage = (displayAllFields)? 25:100; 
		}
		else{
			rowpage = Integer.parseInt(rowStr);
		}
		
		String muitipleStr = (displayAllFields)? matrixCommonBean.getMatrixDTO().getSysFullThresholdPages() : 
			matrixCommonBean.getMatrixDTO().getSysLimitedThresholdPages();
		int muitiplepage = 0;
		if(muitipleStr==null || muitipleStr.length()==0){
			muitiplepage = (displayAllFields)? 4:4; 
		}
		else{
			muitiplepage = Integer.parseInt(muitipleStr);
		}
		
		transactionSaleDataModel.setPageSize(rowpage);
		transactionSaleDataModel.setDbFetchMultiple(muitiplepage);
	}

	public void sortAction(ActionEvent e) {
		matrixDataModel.toggleSortOrder(e.getComponent().getParent().getId());
	}
	
	public void sortDetailAction(ActionEvent e) {
		matrixDataModel.toggleSortDetailOrder(e.getComponent().getParent().getId());
	}
	
	public Long getDisplayWarning() {
		return (displayWarning)? 1L:0L;
	}
	
	public void setDisplayWarning(Long val) {
		this.displayWarning = (val == 1L);	
	}
	
	public void cancelWarning(ActionEvent e) {
		displayWarning = false;
		displayBackdateWarning = 0L;
	}
	/*####################################################################################
	 * Changes made from line 998 to 1023 to fix the bug 0003951
	 *####################################################################################
	 */
	public HtmlCalendar getEffectiveDateLocationCalendar() {
		//3921
		if (effectiveDateLocationCalendar == null) {
			// Create calendar and add validator here as when it was assigned in the jsp
			// multiple validators were being accumulated - one for each page access!
			effectiveDateLocationCalendar = new HtmlCalendar();
			effectiveDateLocationCalendar.addValidator(
					//JsfHelper.createValidatorExpression("#{" + getBeanName() + ".validateEffectiveDate}"));
					JsfHelper.createValidatorExpression("#{" + getBeanName() + ".validateLocationMatrixEffectiveDate}"));
		}
		return effectiveDateLocationCalendar;
	}

	public void setEffectiveDateLocationCalendar(HtmlCalendar effectiveDateLocationCalendar) {
			this.effectiveDateLocationCalendar = effectiveDateLocationCalendar;
		}

	public void validateLocationMatrixEffectiveDate(FacesContext context, UIComponent toValidate, Object value) {
		Long id = selectedMatrix.getId();
		// Don't validate saved records
		if ((id == null) || (id <= 0L)) {
		    // Ensure date >= today
			//Date effDate = (Date) value;
			GregorianCalendar yesterday = new GregorianCalendar();
			yesterday.add(GregorianCalendar.DATE, -1);
		}
	}
	
	public void validateEffectiveDate(FacesContext context, UIComponent toValidate, Object value) {
		//No validation needed for Add a Tax Matrix Line from a Transactions 
		if(getAddFromTransactionAction()){	
			return;
		}

		Long id = selectedMatrix.getId();
	
		// Don't validate saved records
		if ((id == null) || (id <= 0L)) {
		    // Ensure date >= today
			Date effDate = (Date) value;		
			Date todayDate = new com.ncsts.view.util.DateConverter().getUserLocalDate();	
			if (effDate.before(todayDate)) {
				((UIInput)toValidate).setValid(false);
				FacesMessage message = new FacesMessage("Effective date must be on or after today.");
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				context.addMessage(toValidate.getClientId(context), message);
		    }
		}
	}

	public void validateExpirationDate(FacesContext context, UIComponent toValidate, Object value) {
	    // Ensure date > effectiveDate
		Date expDate = (Date) value;
		Date effDate = (Date) effectiveDateCalendar.getValue();
		if ((effDate != null) && ((expDate == null) || !expDate.after(effDate))) {
			((UIInput)toValidate).setValid(false);
			FacesMessage message = new FacesMessage("Expiration date must be later than effective date.");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage(toValidate.getClientId(context), message);
	    }
	}
	
	//Bug # - 4088
	public void validateExpirationDateForLocation(FacesContext context, UIComponent toValidate, Object value) {
	    // Ensure date > effectiveDate
		Date expDate = (Date) value;
		Date effDate = (Date) effectiveDateLocationCalendar.getValue();
		if ((effDate != null) && ((expDate == null) || !expDate.after(effDate))) {
			((UIInput)toValidate).setValid(false);
			FacesMessage message = new FacesMessage("Expiration date must be later than effective date.");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage(toValidate.getClientId(context), message);
	    }
	}
	
	public void validateDrivers(FacesContext context, UIComponent toValidate, Object objValue) {
		validateDrivers(context, toValidate);
	}

	public boolean getDriverGlobalFlag(){
		return driverGlobalFlag;
	}
	public void setDriverGlobalFlag(boolean driverGlobalFlag){
		this.driverGlobalFlag=driverGlobalFlag;
	}
	
	protected boolean validateDrivers(FacesContext context, UIComponent toValidate) {
		return validateDrivers(context, toValidate, false);
	}
	
	protected boolean validateDrivers(FacesContext context, UIComponent toValidate, boolean forceValidate) {
		//0001650: Wildcards with What if
	    String skipValidator = context.getExternalContext().getRequestParameterMap().get("skipValidator");
	    
		if (skipValidator == null || skipValidator.equals("true")) {
		if(!validateCheckedWildDrivers(context)){
			return false;
		}
		
		if(driverGlobalFlag){
			String globalMandatoryColName = getGlobalMandatoryColName();
		
			
			if(globalMandatoryColName==null || globalMandatoryColName.length()==0){
				String cId = (toValidate == null)? null:toValidate.getClientId(context);
				
				FacesMessage message = new FacesMessage(String.format("Global Business Unit Driver not found."));
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				context.addMessage(cId, message);
				return false;
			}
			
			T m = selectedMatrix;
			if (m == null) m = newMatrix();
			driverHandler.initUpdate(m);
		
			driverHandler.updateBU(globalMandatoryColName);	
		}
		
		// If displayWarning is true, then the user is confirming override
		boolean overrideWarning = displayWarning;
		displayWarning = false;
		boolean valid = true;
		Long id = selectedMatrix.getId();
		// Don't validate saved records
		if ((forceValidate ||
			(!currentAction.equals(EditAction.COPY_UPDATE) && 
			!currentAction.equals(EditAction.ADD_FROM_TRANSACTION))) && 
			((id == null) || (id <= 0L))) {
			boolean requiredFieldMessage = false;
			boolean mandatoryGlobalAll = false;
			UIComponent panel = copyAddPanel;
			String code = selectedMatrix.getDriverCode();
			String clientId = (toValidate == null)? null:toValidate.getClientId(context);
			
			Map<String,DriverNames> driverNamesMap = 
					getMatrixCommonBean().getCacheManager().getMatrixColDriverNamesMap(code);
			Map<String,DataDefinitionColumn> transactionPropertyMap = null;
			if(code.equals("GSB"))
				transactionPropertyMap=	getMatrixCommonBean().getCacheManager().getDataDefinitionPropertyMap("TB_BILLTRANS");
			else{
				transactionPropertyMap=	getMatrixCommonBean().getCacheManager().getDataDefinitionPropertyMap("TB_PURCHTRANS");
			}
	
			String restriction = getMatrixCommonBean().getMatrixDTO().getSysDriverRef();
			Long minTaxDriverCount = getMatrixCommonBean().getMatrixDTO().getSysMinDriver();
			Long minLocationDriverCount = getMatrixCommonBean().getMatrixDTO().getSysMinLocationDriver();
			Long minAllocationDriverCount = getMatrixCommonBean().getMatrixDTO().getSysMinAllocationDriver();
			// There is not a Minimum Extract Drivers option yet, so this will always be 1.
			Long minExtractDriverCount = 1l;
			Long driverCount = 0L;
			
			DriverReferenceService driverReferenceService = getMatrixCommonBean().getDriverReferenceService();
			
			for (HtmlInputText input : findActiveInputControls(panel, new ArrayList<HtmlInputText>())) {
				String driver = (String)input.getAttributes().get("DRIVER");
				DriverNames driverName = driverNamesMap.get(driver);
				String value = (String) input.getValue();
				String driverProperty = Util.columnToProperty(driver);
				
				// Check if value was supplied
				if ((value == null) || value.trim().equals("") || value.equalsIgnoreCase("*ALL")) {
	
					// Ensure all required drivers have a value
					if (code.equals(GSBMatrix.DRIVER_CODE)) {
						
							if(requiredFieldMessage || matrixCommonBean.getMinimumTaxDriversMapGSB().get(driverProperty)){
								if((!getDriverGlobalFlag() && driverName.isBusinessUnit())
										||(getDriverGlobalFlag() && !driverName.isBusinessUnit()) 
										||(!getDriverGlobalFlag() && !driverName.isBusinessUnit())){
									requiredFieldMessage = requiredFieldMessage || matrixCommonBean.getMinimumTaxDriversMap().get(driverProperty);
									
								}
							}
					} else {
				
						logger.debug("Inside setting mandatory flag" + (driverName.isMandatory() && driverName.isActive()));
						requiredFieldMessage = (requiredFieldMessage || (driverName.isMandatory() && driverName.isActive()));
						

					}
					
					if (code.equals(TaxMatrix.DRIVER_CODE)) {
						
						if(requiredFieldMessage || matrixCommonBean.getMinimumTaxDriversMap().get(driverProperty)){
							if((!getDriverGlobalFlag() && driverName.isBusinessUnit())
									||(getDriverGlobalFlag() && !driverName.isBusinessUnit()) 
									||(!getDriverGlobalFlag() && !driverName.isBusinessUnit())){
								requiredFieldMessage = requiredFieldMessage || matrixCommonBean.getMinimumTaxDriversMapGSB().get(driverProperty);
								
							}
						}
				} else {
			
					logger.debug("Inside setting mandatory flag" + (driverName.isMandatory() && driverName.isActive()));
					requiredFieldMessage = (requiredFieldMessage || (driverName.isMandatory() && driverName.isActive()));
					

				}
	
				} else {
					driverCount++;
					String desc = transactionPropertyMap.get(Util.columnToProperty(driverName.getTransDtlColName())).getAbbrDesc();
					
					if (selectedMatrix.getHasDescriptions()) {
						Util.setProperty(selectedMatrix, driverProperty+"Desc", null, String.class);
					}
					
					if (value.equalsIgnoreCase("*NULL")) {
						if (!getFlagValue(driverName.getNullDriverFlag())) {
							FacesMessage message = new FacesMessage("Null not allowed for " + desc);
							message.setSeverity(FacesMessage.SEVERITY_ERROR);
							context.addMessage(clientId, message);
							valid = false;
						}
					
					} else if (value.indexOf("%")>=0) {
						if (!getFlagValue(driverName.getWildcardFlag())) {
							FacesMessage message = new FacesMessage("Wildcard not allowed for " + desc);  
							message.setSeverity(FacesMessage.SEVERITY_ERROR);
							context.addMessage(clientId, message);
							valid = false;
						}
						
					} else {
						DriverReference dr = driverReferenceService.getDriverReference(driverName.getTransDtlColName(), value);
						
						if (dr != null) {
							if (selectedMatrix.getHasDescriptions()) {
								Util.setProperty(selectedMatrix, driverProperty+"Desc", dr.getDriverDesc(), String.class);
							}
						} else {
							// -1 = FULL
							// 0 = PARTIAL
							// 1 = ANY VALUE
							// But not for GlExtract drivers!!
							if ((!code.equalsIgnoreCase(GlExtractMap.DRIVER_CODE)) && ((restriction.equals("-1") || (restriction.equals("0") && !overrideWarning)))) {
								valid = (valid && restriction.equals("0"));
								FacesMessage message = new FacesMessage("Value '" + value + "' not found for " + desc); 
								message.setSeverity(restriction.equals("-1")? FacesMessage.SEVERITY_ERROR:FacesMessage.SEVERITY_INFO);
								context.addMessage(clientId, message);
								displayWarning = restriction.equals("0");
							}
						}
							
					}
				}

			}
			
			// Ensure sufficient tax drivers supplied
			if(driverGlobalFlag)
			{driverCount= driverCount+1;}
			String txtMessage = "";
			if ((code.equals("T")||code.equals("GSB")) && (driverCount < minTaxDriverCount)) {
				txtMessage = String.format("At least %d drivers must be entered.", minTaxDriverCount);
			} else if (code.equals("L") && (driverCount < minLocationDriverCount)) {
				txtMessage = String.format("At least %d drivers must be entered.", minLocationDriverCount);
			} else if (code.equals("A") && (driverCount < minAllocationDriverCount)) {
				txtMessage = String.format("At least %d drivers must be entered.", minAllocationDriverCount);
			} else if (code.equals("E") && (driverCount < minExtractDriverCount)) {
				txtMessage = String.format("At least %d drivers must be entered.", minExtractDriverCount);
			}
			if (txtMessage.length() > 0) {
				FacesMessage message = new FacesMessage(txtMessage);
				/* FacesMessage message = new FacesMessage(String.format("At least %d drivers must be entered.", minTaxDriverCount)); */
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				context.addMessage(clientId, message);
				valid = false;
			}
			if (requiredFieldMessage ){
					FacesMessage message = new FacesMessage(String.format("Drivers in red are mandatory and must be entered."));
						message.setSeverity(FacesMessage.SEVERITY_ERROR);
						context.addMessage(clientId, message);
						valid = false;
			}
			FacesMessage.Severity severity = context.getMaximumSeverity();
			if (((severity == null) || (severity.compareTo(FacesMessage.SEVERITY_INFO) <= 0)) && displayWarning) {
				// Warnings are all that's wrong, so we keep warning flag set
				valid = false;
			} else {
				// Don't bother with warning as there are more severe issues
				displayWarning = false;
			}
			
			if (!valid && (toValidate != null)) {
				((UIInput)toValidate).setValid(false);
			}
		}
		
		return valid;
		} else {
			return true;
		}
	}
	
	//0001650: Wildcards with What if
	protected boolean validateCheckedWildDrivers(FacesContext context) {
		boolean valid = true;
		boolean validCheck = true;
		Long id = selectedMatrix.getId();
		if (currentAction.equals(EditAction.ADD_FROM_TRANSACTION) && 
			((id == null) || (id <= 0L))) {
			for (HtmlSelectBooleanCheckbox check : matrixCommonBean.findWildcardDrivers(getViewPanel())) {
				String property = (String) check.getAttributes().get("DRIVERPROP");
				String origValue = (String) check.getAttributes().get("ORIGVALUE");
				String curValue = Util.getPropertyAsString(selectedMatrix, property);
				validCheck = true;
				if (curValue == null || curValue.length()==0){
					validCheck = false;
				}
				else if(!curValue.equals(origValue)){	
					boolean startWild = curValue.startsWith("%");
					boolean endWild = curValue.endsWith("%");
					String subValue = curValue;
					if(startWild && endWild && (curValue.length() >= 3)){ //LIke $12345%
						subValue = curValue.substring(1, curValue.length()-1);
						if(!origValue.contains(subValue)){ //LIke 01234567 not contain 12345
							validCheck = false;
						}
					}
					else if(startWild && !endWild && (curValue.length() >= 2)){ //LIke $34567
						subValue = curValue.substring(1, curValue.length());
						if(!origValue.endsWith(subValue)){ //LIke 01234567 not end with 34567
							validCheck = false;
						}
					}
					else if(!startWild && endWild && (curValue.length() >= 2)){ //LIke 12345%
						subValue = curValue.substring(0, curValue.length()-1);
						if(!origValue.startsWith(subValue)){ //LIke 01234567 not start with 12345
							validCheck = false;
						}
					}
					else{
						
						if ((curValue != null) && !curValue.equals("") && curValue.equals("*NULL")){
								// Active & null enabled
							Boolean nullEnabled = (Boolean) check.getAttributes().get("NULLS");
							if ((nullEnabled != null) && nullEnabled) {				
							}
							else{
								validCheck = false;
							}		
						}
						else{					
							validCheck = false;
						}
					}
				}

				if (!validCheck) {
					FacesMessage message = new FacesMessage(check.getLabel() + " value has been modified improperly.");
					message.setSeverity(FacesMessage.SEVERITY_ERROR);
					context.addMessage(null, message);
					valid = false;
				}
			}	
		}
		
		return valid;
	}
	
	static private boolean getFlagValue(String flag) {
		return ((flag != null) && flag.equals("1"));
	}
	
	private List<HtmlInputText> findActiveInputControls(UIComponent parent, List<HtmlInputText> list) {
		if ((parent != null) && (parent.getChildCount() > 0)) {
			for (UIComponent child : parent.getChildren()) {
				if (child instanceof HtmlInputText) {
					HtmlInputText input = (HtmlInputText) child;
					String driver = (String)input.getAttributes().get("DRIVER");
					if (!input.isDisabled() && (driver != null) && !driver.equals("")) {
						list.add(input);
					}
				}
				findActiveInputControls(child,list);
			}
		}
		return list;
	}
	
	public Boolean getShowDefaultLine() {
		return (currentAction != EditAction.ADD_FROM_TRANSACTION) &&
			matrixCommonBean.getUserDefaultMatrixLineFlag();
	}
	
	public loginBean getLoginBean() {
		return loginBean;
	}
	public void setLoginBean(loginBean loginBean) {
		this.loginBean = loginBean;
	}
	public String getGlobalMandatoryColName(){
		Map<String,DriverNames> transactionDriverNamesMap=null;
		if(selectedMatrix instanceof TaxMatrix) {
			 transactionDriverNamesMap = getMatrixCommonBean().getCacheManager().getTransactionPropertyDriverNamesMap(TaxMatrix.DRIVER_CODE);
		}else if(selectedMatrix instanceof GSBMatrix ){
			 transactionDriverNamesMap = getMatrixCommonBean().getCacheManager().getTransactionPropertyDriverNamesMap(GSBMatrix.DRIVER_CODE);
		}
		
		//Map<String,DataDefinitionColumn> transactionPropertyMap = getMatrixCommonBean().getCacheManager().getDataDefinitionPropertyMap("TB_TRANSACTION_DETAIL");
		String temp = null;
		for (String property : transactionDriverNamesMap.keySet()) {
			DriverNames driverNames = transactionDriverNamesMap.get(property);
	
			if((driverNames.getBusinessUnitFlag() != null) && driverNames.getBusinessUnitFlag().equals("1")){	
				temp = driverNames.getMatrixColName();
				break;
			}
		}
		return temp;
		
	}
	
	
	//bug 0003803
	//FOLLOWUP: THIS CODE SEEMS TO NEVER ACTUALLY BE CALLED... THERE'S A actionListener="#{TaxMatrixViewBean.valueChange}" in TaxMatrixAdd.jsp
	//BUT I DIDN'T SEE IT HITTING THIS
	public void valueChange(ActionEvent e){
		HtmlSelectBooleanCheckbox check = (HtmlSelectBooleanCheckbox) ((HtmlAjaxSupport) e.getComponent()).getParent();
	    driverGlobalFlag = (check.getSubmittedValue() == null)? 
	    		((check.getValue() == null)? false:(Boolean) check.getValue()):Boolean.parseBoolean((String) check.getSubmittedValue());
		//System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!! now driverGlobalFlag is "+driverGlobalFlag);
	}
	
	public User getCurrentUser() {
		return matrixCommonBean.getLoginBean().getUser();
   }
	public ProcessSuspendedTransactionDocument processSuspendedTransactions(SuspendReason reason) throws PurchaseTransactionProcessingException {
		System.out.println("Processing All Suspended Transactions");
		if(currentAction.equals(EditAction.COPY_ADD) || currentAction.equals(EditAction.ADD) || currentAction.equals(EditAction.COPY_UPDATE) || currentAction.equals(EditAction.ADD_FROM_TRANSACTION))
			return purchaseTransactionService.processSuspendedTransactions(matrixId, reason, true);
		else
			return null;
	}	
	
	public void createWhatIfMap() {
		List<TransactionHeader> transactionHeaders = getMatrixCommonBean().getDwColumnService().getFilterNamesByUserCode("TB_PURCHTRANS", matrixCommonBean.getLoginBean().getUserDTO().getUserCode());
		whatIfMap = new HashMap<String, String>();
		whatIfMap.put("*DEFAULT", "*GLOBAL");
		for(TransactionHeader transactionHeader : transactionHeaders){
			if(!transactionHeader.getDataWindowName().equalsIgnoreCase("*DEFAULT")){
				whatIfMap.put(transactionHeader.getDataWindowName(), transactionHeader.getUserCode()) ;
			}
		}
	}
}
