package com.ncsts.view.bean;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

public class UpdateFilesBackingBean {
    private Connection conn = null;
	private BufferedWriter outAID = null;
	public static DateFormat logDateformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
	
	public void writeLogAID(String message) {
		Date d = new Date(System.currentTimeMillis());
		String currentDateTime = logDateformat.format(d);
		try {
			if(outAID!=null){
			outAID.write("[" + currentDateTime + "] " + message);
			outAID.newLine();
			outAID.flush();
			}
		} catch (Exception e) {
		}
	}

	//0004930
	public String doUpdateFilesAction(Connection connection, BufferedWriter out, String fileName, String filePath, String folderPath){
		this.conn = connection;
		this.outAID = out;
		
		String typeUpdate = null;
		
		//Open the file
		try{
			FileInputStream fstream = new FileInputStream(filePath);
			BufferedReader br = new BufferedReader(new InputStreamReader(fstream));
			String firstLine;
	
			//Read first line
			if((firstLine = br.readLine()) != null)   {
				typeUpdate = getHeaderValueOfType(firstLine, FILE_VALUE);
			}
			
			br.close();
		}
		catch(Exception e){	
			writeLogAID("Cannot read " + filePath + ": " + e.getMessage());
			return null;
		}
		
		if(typeUpdate==null){
			writeLogAID("\"File:\" Parameter: not found - TaxRate: Assumed"); 
			typeUpdate = "TaxRate";
		}
		else{
			writeLogAID("\"File:\" Parameter: " + typeUpdate); 
		}
		
		//File: TaxRate, 
		if(typeUpdate.equalsIgnoreCase("TaxRate")){	
			DataUtilityTaxRateUpdatesBackingBean importBean = new DataUtilityTaxRateUpdatesBackingBean();
			if (importBean.initVerification(conn, outAID)) {
				importBean.addActionAID(fileName, filePath, folderPath);
			} else {
				writeLogAID("** Error: Stop updating files, " + filePath);
			}
		}
		//File: TaxCode, 
		else if(typeUpdate.equalsIgnoreCase("TaxCode")){	
			TaxCodeRulesBackingBean importBean = new TaxCodeRulesBackingBean();
			importBean.setFileTypeCode(typeUpdate); 
			if (importBean.initVerification(conn, outAID)) {
				importBean.addActionAID(fileName, filePath, folderPath);
			} else {
				writeLogAID("** Error: Stop updating files, " + filePath);
			}
		}
		
		else if(typeUpdate.equalsIgnoreCase("RefDoc")){	
			TaxCodeRulesBackingBean importBean = new TaxCodeRulesBackingBean();
			importBean.setFileTypeCode(typeUpdate);
			if (importBean.initVerification(conn, outAID)) {
				importBean.addActionAID(fileName, filePath, folderPath);
			} else {
				writeLogAID("** Error: Stop updating files, " + filePath);
			}
		}
		//Add more types here...
		else{
			writeLogAID("** Invalid Update File Type - Import Aborted **");
			
			//Move to Rejected folder
			File originalFile = new File(filePath);
			String importedPath = originalFile.getParent();			
			String rejectedPath = importedPath.substring(0, importedPath.lastIndexOf("Imported")) + "Rejected";
	    	File rejectedDirectory = new File(rejectedPath);
	    	if (rejectedDirectory.exists()) {
	    	} else {
	    		if ((new File(rejectedPath)).mkdirs()) {
	    			writeLogAID("** " + rejectedPath + " folder - Created **");
	    		}
	    	}
	    	
	    	if (originalFile.renameTo(new File(rejectedDirectory, fileName))) {
	    		writeLogAID("** " + fileName + " moved to " + rejectedPath + " - succeeded **");
	    	}
	    	else{
	    		writeLogAID("** " + fileName + " moved to " + rejectedPath + " - failed **");
	    	}
		}

		return null;
	}
	
	public static Map<String,String> getErrorListCodes(Connection conn) {
		Map<String,String> result = new LinkedHashMap<String,String>();
		try {		
			PreparedStatement prep = conn.prepareStatement("select code_code, description from tb_list_code where code_type_code = 'ERRORCODE'");
			ResultSet rs = prep.executeQuery();
			while(rs!=null && rs.next()){
				result.put(rs.getString(1), rs.getString(2));
			}
			prep.close();
  		} 
		catch (Exception ex) {
		}
		
		return result;
	}
	
	public static String TYPE_VALUE = "TYPE";
	public static String DATE_VALUE = "DATE";
	public static String REL_VALUE = "REL";
	public static String FILE_VALUE = "FILE";
	public static String COLONSIGN = ":";
	
	public static String getHeaderValueOfType(String header, String type) {
		if(header==null || header.trim().length()==0) {
			return null;
		}
		String upperHeader = header.trim().toUpperCase();
		
		if(type==null || type.trim().length()==0) {
			return null;
		}
		String upperType = type.trim().toUpperCase();
		
		int beginIndex = upperHeader.indexOf(upperType+COLONSIGN);
		if(beginIndex<0) {
			if(upperType.equals(FILE_VALUE)) {
				return "TAXRATE";
			}
			else {			
				return null;
			}
		}
		
		String remainHeader = upperHeader.substring(beginIndex+(upperType+COLONSIGN).length(), upperHeader.length());
		if(remainHeader==null || remainHeader.trim().length()==0) {
			return null;
		}
		remainHeader = remainHeader.trim();
		
		int endIndex = remainHeader.indexOf(COLONSIGN);
		String subHeader = "";
		if(endIndex<0) {
			subHeader = remainHeader;
		}
		else {
			subHeader = remainHeader.substring(0, endIndex);
		}
		
		if(subHeader==null || subHeader.trim().length()==0) {
			return null;
		}
		subHeader = subHeader.trim();
		
		//Start working with specific type
		String value = null;	
		if(upperType.equals(TYPE_VALUE)) {
			if(subHeader.indexOf("UPDATE")>=0) {
				value = "UPDATE";
			}
			else if(subHeader.indexOf("FULL")>=0) {
				value = "FULL";
			}
			else {
				value = subHeader;
			}
		}
		else if(upperType.equals(FILE_VALUE)) {
			if(subHeader.indexOf("TAXRATE")>=0) {
				value = "TAXRATE";
			}
			else if(subHeader.indexOf("TAXCODE")>=0) {
				value = "TAXCODE";
			}
			else if(subHeader.indexOf("REFDOC")>=0) {
				value = "REFDOC";
			}
			else if(subHeader.indexOf("SITUS")>=0) {
				value = "SITUS";
			}
			else {
				value = subHeader;
			}
		}
		else if(upperType.equals(REL_VALUE)) {
			boolean foundNumber = false;
			String numString = "";
			for(int i=0; i<subHeader.length();i++){
		        char ch = subHeader.charAt(i);
		        Boolean flag = Character.isDigit(subHeader.charAt(i));
		         if(flag) {
		            foundNumber = true;
		            numString = numString + subHeader.charAt(i);
		         }
		         else {
		            if(foundNumber) {
		            	break;
		            }
		         }
		     } 

			 Integer numInt = null;
			 if(numString.length()>0) {
				 try {
					 numInt = Integer.valueOf(numString);
					 if(numInt!=null) {
						 value = numInt.toString();
					 }
				 }
				 catch(Exception e) {		  
				 }
			 }	
		}
		else if(upperType.equals(DATE_VALUE)) {
			//xxxxxx
			subHeader = subHeader.replaceAll("-", "_").replaceAll("/", "_");
			
			int underlineIndex = subHeader.indexOf("_"); //AA2019_02BB
			if(underlineIndex<0 || underlineIndex<4) {		
				return null;
			}
			
			String dateString = "";
			for(int i=0; i<7;i++){
				if(subHeader.length()>(underlineIndex-4 + i)) {
					if(i==4) {
						dateString = dateString + "_"; //2019_
					}
					else {				
				        char ch = subHeader.charAt(underlineIndex-4 + i);//2
				        Boolean flag = Character.isDigit(ch);
				         if(flag) {
				            dateString = dateString + ch;
				         }
				         else {
				            break;
				         }
					}
				}
		    }
			
			if(dateString.length()==7) {
				char ch5 = subHeader.charAt(5);
				char ch6 = subHeader.charAt(6);
				
				if(ch5==48) {
					if(ch6==48) {
						return null;//MM=00
					}
				}
				else if(ch5==49) {
					if(ch6>50) {
						return null;//MM=13
					}
				}
				else {
					return null;
				}
			}
			else if(dateString.length()==6) {
				char ch5 = subHeader.charAt(5);
				
				if(ch5==48) {
					return null;//M=0, 2019_0
				}
			}
			
			if(dateString.length()>0){
				SimpleDateFormat formatMM = new SimpleDateFormat("yyyy_MM");
				//format of 2020_02
				Date date = null;
				try {
					date = formatMM.parse(dateString);	
					value = formatMM.format(date);
				}
				catch(Exception e) {
					//e.printStackTrace();
				}
			}
		}
		
		return value;
	}
	
	public static void mainBK(String[] args) {
		String[] headerArray = {
				"TYPE: UPDATE   DATE: 2019_02   REL: 1   FILE: TAXRATE",
				"  DaTE: AA2019_2BB   REL: 01  FILE: TAXRATE , TYPE:UPDATE",
				"DATE: 2019/13  TyPE:update , REL: 001FILE:TAXCODE ",
				" DATE: 19/12  TyPE:update , REL: 001FILE:TAXCODE ",
				"tYPE:UPDATE  : DATE: 2019-A2   REL: REL1  ",
				" tYPE:UPDATE  : DATEd: 2019-09   REL: REL1  ",
				"TYPE: FULL   DATE: 20BB_02   irREL: 01   FILE: SITUS",
				"  DATE: 2019_0   REL: 2   FILE: taxrate , TYPe: full",
				"FILE: ReFDOCDATE: 2019_29  TYPE: michael fuller , rel:1k ",
				"REL: 01A,TYPE: FuLL   DATE: 2019-2   FILE: TAXRATE ",
				"REL: 001,TYPE: FuLL   DATE: 2019/02   FILE: TAXRATE",
				"Type: UPDATE, Date: 2019_10, Rel: 2"
		};
		
		for(int i=0; i<headerArray.length; i++) {
			System.out.print(headerArray[i] + " -> ");
			System.out.print(TYPE_VALUE + ":" + getHeaderValueOfType(headerArray[i], TYPE_VALUE) + " ");
			System.out.print(DATE_VALUE + ":" + getHeaderValueOfType(headerArray[i], DATE_VALUE) + " ");
			System.out.print(REL_VALUE + ":" + getHeaderValueOfType(headerArray[i], REL_VALUE) + " ");
			System.out.print(FILE_VALUE + ":" + getHeaderValueOfType(headerArray[i], FILE_VALUE) + " ");
			System.out.println("");
		}
		
		//for(int i=0; i<headerArray.length; i++) {
		//	System.out.println(getHeaderValueOfType(headerArray[i], REL_VALUE));
		//}

	}
	
	public static void main(String[] args) {
		System.out.println("############ main for Automatic Import of Data");

		String name = "TCR_UPDATE_2019_02_v6_bad+partial+footer+2";
		String fileName = name + ".txt";
		String filePath = "C:\\File Updates - Test\\" + name + ".txt";
		String folderPath = "C:\\File Updates - Test\\";
		String logFile = "C:\\File Updates - Test\\" + name + ".log";
		
		BufferedWriter out = null;
		try {
			out = new BufferedWriter(new FileWriter(logFile));
			// out.write("aString");
		} catch (Exception e) {
		}

		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
		} catch (ClassNotFoundException e) {
			System.out
					.println("xxxxxxxxxxxxxx Can't load class: oracle.jdbc.driver.OracleDriver");
			return;
		}

		Connection conn = null;
		try {
			// Standalone connection
			conn = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe", "testdb4_login","STSCORP");
			conn.setAutoCommit(false);

			UpdateFilesBackingBean importTaxRateBean = new UpdateFilesBackingBean();		
			importTaxRateBean.doUpdateFilesAction(conn, out, fileName, filePath, folderPath);
			
		} catch (Throwable e) {
			e.printStackTrace();
		} finally {
			if (conn != null)
				try {
					conn.close();
				} catch (SQLException ignore) {
				}
		}

		try {
			out.close();
		} catch (Exception e) {
		}
	}
}
