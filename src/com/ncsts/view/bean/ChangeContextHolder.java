package com.ncsts.view.bean;

import org.springframework.util.Assert;

public class ChangeContextHolder {
	
	      private static final ThreadLocal<String> contextHolder =
          new ThreadLocal<String>();
	  
	      public static void setDataBaseType(String dataBaseType) {
		     Assert.notNull(dataBaseType, "DataBaseType cannot be null");
		     contextHolder.set(dataBaseType);
		  }

		  public static String getDataBaseType() {			 
			     return (String)contextHolder.get();				 
		  }

		  public static void clearDataBase() {
		     contextHolder.remove();
		  }	  

}
