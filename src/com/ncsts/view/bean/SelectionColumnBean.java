package com.ncsts.view.bean;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.ajax4jsf.component.UIDataAdaptor;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.ncsts.common.LoggerFactory;
import com.ncsts.common.Util;
import com.ncsts.services.DwColumnService;
import com.ncsts.services.UserService;
import com.ncsts.view.bean.ImportDefinitionMappingBean.ImportColumn;
import com.ncsts.view.bean.ImportDefinitionMappingBean.MappedColumn;
import com.ncsts.domain.DataDefinitionColumn;
import com.ncsts.domain.DriverNames;
import com.ncsts.domain.DwFilter;
import com.ncsts.domain.DwFilterPK;
import com.ncsts.domain.ImportDefinitionDetail;
import com.ncsts.domain.LocationMatrix;
import com.ncsts.domain.TaxMatrix;
import com.ncsts.domain.TransactionDetail;
import com.ncsts.domain.TransactionHeader;
import com.ncsts.domain.User;
import com.ncsts.dto.DbPropertiesDTO;
import com.ncsts.dto.UserDTO;

@Component
@Scope("session")
public class SelectionColumnBean implements InitializingBean {

	static private Logger logger = LoggerFactory.getInstance().getLogger(
			SelectionColumnBean.class);
	
	@Autowired
	private DwColumnService dwColumnService;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private MatrixCommonBean matrixCommonBean;

	private UserDTO userDTO;
	public String NEW_VIEW = "*NEWVIEW";
	private List<DwFilter> dwHiddenList = new ArrayList<DwFilter>();
	private List<DwFilter> dwFilterList = new ArrayList<DwFilter>();
	private String screenName = "";
	private String selectedUserName = "";
	private String selectedColumnName = "";
	private String createdColumnName = "";
	private String currentColumnName = "";
	private boolean initialStateChanged = false;
	private TransactionDetailBean transactionDetailBean = null;
	private List<SelectItem> userNameList = null;
	private List<SelectItem> columnNameList = null;
	private int selectedHiddenIndex = -1;
	private int selectedDisplayedIndex = -1;
	private boolean dirty;
	private DwFilter selectedHiddenColumn = null;
	private DwFilter selectedDisplayedColumn = null;
	private  String windowName = "TB_PURCHTRANS";
	
	
	@Override
	public void afterPropertiesSet() throws Exception {
	}
	
	public SelectionColumnBean() {
	}

	public void userNameChangeListener(ActionEvent e) {	
		initialStateChanged = true;
		columnNameList = null;
		selectedColumnName = "";
	}
	
	public Boolean isGlobalNoPermission(){
		return ((userDTO.getGlobalViewFlag()==null || !userDTO.getGlobalViewFlag().equals("1")) && 
				(this.selectedUserName!=null && this.selectedUserName.equalsIgnoreCase("*GLOBAL")));
		//DO NOT add "*NEW Filter" selection, disable add, update, delete buttons when "*GLOBAL" selected, but no global permission
	}
	
	public Boolean isGlobalUser(){
		return (userDTO.getGlobalViewFlag()!=null && userDTO.getGlobalViewFlag().equals("1"));
	}
	
	public Boolean isAdminlUser(){
		return (userDTO.getAdminFlag()!=null && (userDTO.getAdminFlag().equals("1") || userDTO.getAdminFlag().equals("2")));
	}
	
	public Boolean isGlobalSelected(){
		return (selectedUserName!=null && selectedUserName.equalsIgnoreCase("*GLOBAL"));
	}
	
	public Boolean isDefaultSelected(){
		return (selectedColumnName!=null && selectedColumnName.equalsIgnoreCase("*DEFAULT"));
	}
	
	public Boolean isNewViewSelected(){
		return (selectedColumnName!=null && selectedColumnName.equalsIgnoreCase(NEW_VIEW));
	}
	
	public Boolean isExistingViewSelected(){
		return (selectedColumnName!=null && selectedColumnName.length()>0 && !selectedColumnName.equalsIgnoreCase(NEW_VIEW));
	}
	
	public Boolean isCurrentUserSelected(){
		return (selectedUserName!=null && selectedUserName.equalsIgnoreCase(userDTO.getUserCode().toUpperCase()));
	}
	
	public Boolean getDisplayUpdateButton() {
		//return (!initialStateChanged && !selectedColumnName.equalsIgnoreCase(NEW_VIEW)) && !isGlobalNoPermission();
		if(isGlobalUser() && isGlobalSelected() && !isDefaultSelected() && isExistingViewSelected()){
			return true;
		}
		
		if(isCurrentUserSelected() && isExistingViewSelected()){
			return true;
		}
		
		return false;
	}
	
	public Boolean getDisplayAddButton() {
		//return (selectedColumnName.equalsIgnoreCase(NEW_VIEW)) && !isGlobalNoPermission();
		if(isGlobalUser() && isNewViewSelected()){
			return true;
		}
		
		if(isCurrentUserSelected() && isNewViewSelected()){
			return true;
		}
		
		return false;
	}
	
	public Boolean getDisplayDeleteButton() {
		//return !(selectedColumnName.equalsIgnoreCase(NEW_VIEW) || selectedColumnName.length()==0)  && !isGlobalNoPermission();
		if(isGlobalUser() && !isDefaultSelected() && isExistingViewSelected()){
			return true;
		}
		
		if(isCurrentUserSelected() && isExistingViewSelected()){
			return true;
		}
		
		if(isAdminlUser() && isExistingViewSelected() && !isGlobalSelected()){
			return true;
		}
		
		return false;
	}
	
	public Boolean getDisplayEditButton() {
		if(getDisplayAddButton() || getDisplayUpdateButton()){
			return true;
		}
		
		return false;
	}
	
	public List<DwFilter> convertTransactionHeaderToDwFilter(List<TransactionHeader> transactionHeaders){
		Map<String,DataDefinitionColumn> transactionPropertyMap = matrixCommonBean.getCacheManager().getTransactionPropertyMap(); 
		DataDefinitionColumn dataDefinitionColumn = null;
		List<DwFilter> dwFilterList = new ArrayList<DwFilter>();
		for(TransactionHeader transactionHeader : transactionHeaders){
			if(transactionPropertyMap.get(transactionHeader.getColumnName())!=null){
				dataDefinitionColumn = transactionPropertyMap.get(transactionHeader.getColumnName());
				
				DwFilter dwFilter = new DwFilter(new DwFilterPK("","","",dataDefinitionColumn.getColumnName()));
				dwFilter.setFilterValue(dataDefinitionColumn.getAbbrDesc());
				dwFilterList.add(dwFilter);
			}
			else if(transactionHeader.getColumnName().equalsIgnoreCase("processStatus")){
				DwFilter dwFilter = new DwFilter(new DwFilterPK("","","","PROCESSSTATUS"));
				dwFilter.setFilterValue("Process Status");
				dwFilterList.add(dwFilter);
			}
		}
		return dwFilterList;
	}
	
	public List<TransactionHeader> convertDwFilterToTransactionHeader(List<DwFilter> dwFilterList, String columnName){
		List<TransactionHeader> transactionHeaders = new ArrayList<TransactionHeader>();
		TransactionHeader transactionHeader = null;
		String windowName = screenName;
		String dataWindowName = columnName;
		String userCode = selectedUserName;
	
		int order = 1;
		for(DwFilter dwFilter : dwFilterList ){
			transactionHeader = new TransactionHeader();
			transactionHeader.setWindowName(windowName);
			transactionHeader.setDataWindowName(dataWindowName);
			transactionHeader.setUserCode(userCode);
			transactionHeader.setColumnId(order++);
			if(dwFilter.getColumnName().equals("PROCESSSTATUS")){
				transactionHeader.setColumnName("processStatus");
			}
			else{
				transactionHeader.setColumnName(Util.columnToProperty(dwFilter.getColumnName()));//like glCompanyNbr
			}
			transactionHeaders.add(transactionHeader);
		}
		
		return transactionHeaders;
	}

	public void columnNameChangeListener(ActionEvent e) {
		initialStateChanged = true;
		if(selectedColumnName.length()==0 || selectedColumnName.equalsIgnoreCase(NEW_VIEW)){	
		}
		else{
			List<TransactionHeader> transactionHeaders = null;
			if(selectedColumnName!=null && selectedColumnName.equalsIgnoreCase("*DEFAULT")){
				transactionHeaders = getDefaultView();
			}
			else{
				transactionHeaders = dwColumnService.getColumnsByUserandDataWindowName(windowName, this.selectedColumnName, this.selectedUserName);	
			}
			
			dwFilterList = convertTransactionHeaderToDwFilter(transactionHeaders);

			//Build hidden list
			dwHiddenList = createHiddenList(dwFilterList);
		}
	}
	
	public final List<SelectItem> getUserNameList() {
		if(userNameList==null){
			userNameList = new ArrayList<SelectItem>();
			
			userNameList.add(new SelectItem("*GLOBAL", "*GLOBAL"));
			if(isAdminlUser()){//Display all users
				List<User> users = userService.findAllUsers();				
				if(users != null && users.size() > 0){
	    	    	for(User user : users){
	    	    		userNameList.add(new SelectItem(user.getUserCode(), user.getUserCode()));		
	    	    	}
	    	    } 	
			}
			else{//Display logged on user
				userNameList.add(new SelectItem(userDTO.getUserCode().toUpperCase(), userDTO.getUserCode().toUpperCase()));
			}
		}
		return userNameList;
	}
	
	public final List<SelectItem> getColumnNameList() {
		if (columnNameList == null) {
			List<String> filterNames = dwColumnService.getFilterNamesPerUserCode(windowName, this.selectedUserName);
			
			columnNameList = new ArrayList<SelectItem>();
			columnNameList.add(new SelectItem("","Select a View Name"));	
			if((isGlobalUser() && isGlobalSelected()) || (isCurrentUserSelected())){
				columnNameList.add(new SelectItem(NEW_VIEW, "*New View"));	
			}
			
			if(isGlobalSelected()){
				columnNameList.add(new SelectItem("*DEFAULT", "*DEFAULT"));
			}
			
			for(String filtername : filterNames){
				columnNameList.add(new SelectItem(filtername, filtername));
			}
		}
		return columnNameList;
	}
	
	public List<DwFilter> getDwFilterList() {
		return dwFilterList;
	}

	public void setDwFilterList(List<DwFilter> dwFilterList) {
		this.dwFilterList = dwFilterList;
	}
	
	public List<DwFilter> getDwHiddenList() {
		return dwHiddenList;
	}

	public void setDwHiddenList(List<DwFilter> dwHiddenList) {
		this.dwHiddenList = dwHiddenList;
	}
	
	public String getSelectedUserName() {
		return selectedUserName;
	}

	public void setSelectedUserName(String selectedUserName) {
		this.selectedUserName = selectedUserName;
	}
	
	public String getScreenName() {
		return screenName;
	}

	public void setUserDTO(UserDTO userDTO){
		this.userDTO = userDTO;
	}

	public void setScreenName(String screenName) {
		this.screenName = screenName;
	}
	
	public String getScreenNameDescription() {
		if(screenName!=null && screenName.equalsIgnoreCase(windowName)){
			return "Purchase Transactions";
		}
		
		return "";
	}
	
	public String getSelectedColumnName() {
		return selectedColumnName;
	}
	
	public void setCurrentColumnName(String selectedColumnName) {
		initialStateChanged = false;
		currentColumnName = selectedColumnName;
		createdColumnName = "";
		List<TransactionHeader> transactionHeaders ; 
		if(selectedColumnName==null || selectedColumnName.length()==0 || selectedColumnName.equalsIgnoreCase("*DEFAULT")){
			this.selectedColumnName = "*DEFAULT"; //if this pass in from transaction maintenance
			//AC this.selectedUserName = userDTO.getUserCode();
			this.selectedUserName = "*GLOBAL";
		}
		else{
			this.selectedUserName = selectedColumnName.substring(0, selectedColumnName.indexOf("/"));
			this.selectedColumnName = selectedColumnName.substring(selectedColumnName.indexOf("/") + 1 ,selectedColumnName.length());
		}
		
		if("*GLOBAL".equals(this.selectedUserName)) {
			transactionHeaders = dwColumnService.getColumnsByUserandDataWindowName("TB_TRANSACTION_DETAIL", this.selectedColumnName, this.selectedUserName);
		}
		else {
			transactionHeaders = dwColumnService.getColumnsByUserandDataWindowName("TB_TRANSACTION_DETAIL", this.selectedColumnName, userDTO.getUserCode().toUpperCase());
		}
		
		//TODO: retrieve columns
	   transactionHeaders = dwColumnService.getColumnsByUserandDataWindowName(windowName, this.selectedColumnName, this.selectedUserName);
		if((transactionHeaders==null || transactionHeaders.size()==0) && this.selectedColumnName.equalsIgnoreCase("*DEFAULT")){
			//Create *DEFAULT if needed
			//createDefaultView();
			//transactionHeaders = dwColumnService.getColumnsByDataWindowName("TB_TRANSACTION_DETAIL", "*DEFAULT");
			
			//get *DEFAULT
			transactionHeaders = getDefaultView();
		}
		
		dwFilterList = convertTransactionHeaderToDwFilter(transactionHeaders);	

		//Build hidden list
		dwHiddenList = createHiddenList(dwFilterList);
		
		columnNameList = null;
	}
	
	public List<DwFilter> createHiddenList(List<DwFilter> dwFilterList){
		Map<String, String> displayedMap = new HashMap<String, String>();
		for(DwFilter dwFilter : dwFilterList){
			displayedMap.put(dwFilter.getColumnName(), dwFilter.getColumnName());
		}
		
		DwFilter dwFilter = null;
		List<DwFilter> dwHiddenList = new ArrayList<DwFilter>();
		
		if(displayedMap.get("PROCESSSTATUS")==null){
			dwFilter = new DwFilter(new DwFilterPK("","","","PROCESSSTATUS"));
			dwFilter.setFilterValue("Process Status");
			dwHiddenList.add(dwFilter);
		}
				
		Map<String,DataDefinitionColumn> transactionPropertyMap = matrixCommonBean.getCacheManager().getTransactionPropertyMap(); 
		DataDefinitionColumn dataDefinitionColumn = null;
		for (String transProperty : transactionPropertyMap.keySet()) {
			dataDefinitionColumn = transactionPropertyMap.get(transProperty);
			if(displayedMap.get(dataDefinitionColumn.getColumnName())==null){
				dwFilter = new DwFilter(new DwFilterPK("","","",dataDefinitionColumn.getColumnName()));
				dwFilter.setFilterValue(dataDefinitionColumn.getAbbrDesc());
				dwHiddenList.add(dwFilter);
			}
		}
		
		return dwHiddenList;
	}
	
	public List<TransactionHeader> getDefaultView(){
		//Create *DEFAULT
		List<TransactionHeader> transactionHeaders = new ArrayList<TransactionHeader>();
		TransactionHeader transactionHeader = null;
		//String transactionColumnname = "";
		//String name = "";
		String dataWindowName = "*DEFAULT";
		String userCode = "*GLOBAL";
		
		Map<String, String> columnValuesMap = new HashMap<String, String>();
		
		int order = 1;
		
		Set<String> defaultViewColumns = matrixCommonBean.getDefaultViewColumns();
		
		for (String propertyname : defaultViewColumns){
			columnValuesMap.put(propertyname, propertyname);
			transactionHeader = new TransactionHeader();
			transactionHeader.setWindowName(windowName);
			transactionHeader.setDataWindowName(dataWindowName);
			transactionHeader.setUserCode(userCode);
			transactionHeader.setColumnId(order++);
			transactionHeader.setColumnName(propertyname);
			transactionHeaders.add(transactionHeader);
		}
		
		/*
		name = "processStatus";
		columnValuesMap.put(name, name);
		transactionHeader = new TransactionHeader();
		transactionHeader.setWindowName(windowName);
		transactionHeader.setDataWindowName(dataWindowName);
		transactionHeader.setUserCode(userCode);
		transactionHeader.setColumnId(order++);
		transactionHeader.setColumnName(name);
		transactionHeaders.add(transactionHeader);
		
		name = "transactionDetailId";
		columnValuesMap.put(name, name);
		transactionHeader = new TransactionHeader();
		transactionHeader.setWindowName(windowName);
		transactionHeader.setDataWindowName(dataWindowName);
		transactionHeader.setUserCode(userCode);
		transactionHeader.setColumnId(order++);
		transactionHeader.setColumnName(name);
		transactionHeaders.add(transactionHeader);	

		Map<String,DriverNames> taxDriversMap =  matrixCommonBean.getCacheManager().getMatrixColDriverNamesMap(TaxMatrix.DRIVER_CODE);
		for (String matrixColumnName : taxDriversMap.keySet()) {
			DriverNames driverNames = taxDriversMap.get(matrixColumnName);
			transactionColumnname = driverNames.getTransDtlColName();			
			name = Util.columnToProperty(transactionColumnname);// like glCompanyNbr	
			
			if(columnValuesMap.get(name)==null){
				columnValuesMap.put(name, name);
				transactionHeader = new TransactionHeader();
				transactionHeader.setWindowName(windowName);
				transactionHeader.setDataWindowName(dataWindowName);
				transactionHeader.setUserCode(userCode);
				transactionHeader.setColumnId(order++);
				transactionHeader.setColumnName(name);
				transactionHeaders.add(transactionHeader);
			}
		}
		
		Map<String,DriverNames> locationDriversMap =  matrixCommonBean.getCacheManager().getMatrixColDriverNamesMap(LocationMatrix.DRIVER_CODE);
		for (String matrixColumnName : locationDriversMap.keySet()) {
			DriverNames driverNames = locationDriversMap.get(matrixColumnName);
			transactionColumnname = driverNames.getTransDtlColName();			
			name = Util.columnToProperty(transactionColumnname);// like glCompanyNbr	
			
			if(columnValuesMap.get(name)==null){
				columnValuesMap.put(name, name);
				transactionHeader = new TransactionHeader();
				transactionHeader.setWindowName(windowName);
				transactionHeader.setDataWindowName(dataWindowName);
				transactionHeader.setUserCode(userCode);
				transactionHeader.setColumnId(order++);
				transactionHeader.setColumnName(name);
				transactionHeaders.add(transactionHeader);
			}
		}
		
		//dwColumnService.addColumnsByFilterName(windowName, dataWindowName, transactionHeaders);
		*/
		return transactionHeaders;
	}
	
	public void createDefaultView(){
		//Create *DEFAULT
		List<TransactionHeader> transactionHeaders = new ArrayList<TransactionHeader>();
		TransactionHeader transactionHeader = null;
		String transactionColumnname = "";
		String name = "";
		String dataWindowName = "*DEFAULT";
		String userCode = "*GLOBAL";
		
		Map<String, String> columnValuesMap = new HashMap<String, String>();
		
		int order = 1;
		name = "processStatus";
		columnValuesMap.put(name, name);
		transactionHeader = new TransactionHeader();
		transactionHeader.setWindowName(windowName);
		transactionHeader.setDataWindowName(dataWindowName);
		transactionHeader.setUserCode(userCode);
		transactionHeader.setColumnId(order++);
		transactionHeader.setColumnName(name);
		transactionHeaders.add(transactionHeader);
		
		name = "transactionDetailId";
		columnValuesMap.put(name, name);
		transactionHeader = new TransactionHeader();
		transactionHeader.setWindowName(windowName);
		transactionHeader.setDataWindowName(dataWindowName);
		transactionHeader.setUserCode(userCode);
		transactionHeader.setColumnId(order++);
		transactionHeader.setColumnName(name);
		transactionHeaders.add(transactionHeader);	

		Map<String,DriverNames> taxDriversMap =  matrixCommonBean.getCacheManager().getMatrixColDriverNamesMap(TaxMatrix.DRIVER_CODE);
		for (String matrixColumnName : taxDriversMap.keySet()) {
			DriverNames driverNames = taxDriversMap.get(matrixColumnName);
			transactionColumnname = driverNames.getTransDtlColName();			
			name = Util.columnToProperty(transactionColumnname);// like glCompanyNbr	
			
			if(columnValuesMap.get(name)==null){
				columnValuesMap.put(name, name);
				transactionHeader = new TransactionHeader();
				transactionHeader.setWindowName(windowName);
				transactionHeader.setDataWindowName(dataWindowName);
				transactionHeader.setUserCode(userCode);
				transactionHeader.setColumnId(order++);
				transactionHeader.setColumnName(name);
				transactionHeaders.add(transactionHeader);
			}
		}
		
		Map<String,DriverNames> locationDriversMap =  matrixCommonBean.getCacheManager().getMatrixColDriverNamesMap(LocationMatrix.DRIVER_CODE);
		for (String matrixColumnName : locationDriversMap.keySet()) {
			DriverNames driverNames = locationDriversMap.get(matrixColumnName);
			transactionColumnname = driverNames.getTransDtlColName();			
			name = Util.columnToProperty(transactionColumnname);// like glCompanyNbr	
			
			if(columnValuesMap.get(name)==null){
				columnValuesMap.put(name, name);
				transactionHeader = new TransactionHeader();
				transactionHeader.setWindowName(windowName);
				transactionHeader.setDataWindowName(dataWindowName);
				transactionHeader.setUserCode(userCode);
				transactionHeader.setColumnId(order++);
				transactionHeader.setColumnName(name);
				transactionHeaders.add(transactionHeader);
			}
		}
		
		dwColumnService.addColumnsByUserAndFilterName(windowName, dataWindowName, userCode, transactionHeaders);
	}
	
	public void setSelectedColumnName(String selectedColumnName) {
		this.selectedColumnName = selectedColumnName;
	}

	public String getCreatedColumnName() {
		return createdColumnName;
	}

	public void setCreatedColumnName(String createdColumnName) {
		this.createdColumnName = createdColumnName;
	}
	
	public String okAddAction() {	
		if(createdColumnName==null || createdColumnName.trim().length()==0){
			FacesMessage msg = new FacesMessage("Column Name is mandatory.");
	        msg.setSeverity(FacesMessage.SEVERITY_ERROR);
	        FacesContext.getCurrentInstance().addMessage(null, msg);
	        return null;
		}
		
		if(dwFilterList !=null){
			List<TransactionHeader> transactionHeaders = convertDwFilterToTransactionHeader(dwFilterList, createdColumnName);
			dwColumnService.addColumnsByUserAndFilterName(screenName, createdColumnName, selectedUserName, transactionHeaders);
		}
		
		//TODO: refresh filter combo
		if(transactionDetailBean!=null){
			transactionDetailBean.setViewItems(null);
		}
		
		if(userDTO.getUserCode().equalsIgnoreCase(selectedUserName) || selectedUserName.equalsIgnoreCase("*GLOBAL")){
			if(transactionDetailBean!=null){
				transactionDetailBean.setSelectedColumn(selectedUserName + "/" + createdColumnName);
				transactionDetailBean.setLastViewColumn(createdColumnName);
				transactionDetailBean.viewChangeListener(null);
			}
		}
	
		return "transactions_process";
	}
	
	private void clear() {
		dirty = false;
		selectedDisplayedIndex = -1;
		selectedHiddenIndex = -1;
		selectedHiddenColumn = null;
		selectedDisplayedColumn = null;
	}
	
	public int getSelectedHiddenIndex() {
		return this.selectedHiddenIndex;
	}
	
	public int getSelectedDisplayedIndex() {
		return this.selectedDisplayedIndex;
	}
	
	public void importColumnChanged(ActionEvent e) throws AbortProcessingException { 
		UIComponent uiComponent = e.getComponent().getParent();
		UIDataAdaptor table = (UIDataAdaptor) uiComponent;
		selectedHiddenIndex = table.getRowIndex();
		selectedHiddenColumn = (DwFilter)table.getRowData();
	}
	
	public void mappingColumnChanged(ActionEvent e) throws AbortProcessingException { 
		UIComponent uiComponent = e.getComponent().getParent();
		UIDataAdaptor table = (UIDataAdaptor) uiComponent;
		selectedDisplayedIndex = table.getRowIndex();
		selectedDisplayedColumn = (DwFilter)table.getRowData();
	}
	
	public void assignAction() {	
		DwFilter hiddenColumn = null;
		if(selectedHiddenIndex!=-1 && selectedHiddenColumn!=null){	
			for(int i=0; i< dwHiddenList.size(); i++){
				hiddenColumn = (DwFilter)dwHiddenList.get(i);
				if(hiddenColumn.getColumnName().equals(selectedHiddenColumn.getColumnName())){
					dwHiddenList.remove(i);
					break;				
				}
			}
		}
			
		if(hiddenColumn==null){
			return;
		}
		
		DwFilter displayedColumn = null;
		if(selectedDisplayedIndex!=-1 && selectedDisplayedColumn!=null){
			for(int i=0; i< dwFilterList.size(); i++){
				displayedColumn = (DwFilter)dwFilterList.get(i);
				if(displayedColumn.getColumnName().equals(selectedDisplayedColumn.getColumnName())){
					if((i+1)<dwFilterList.size()){
						dwFilterList.add((i+1), hiddenColumn);//Insert at i+ 1
					}
					else{
						dwFilterList.add(hiddenColumn);//Add to very end
					}
					break;				
				}
			}	
		}
		else{
			dwFilterList.add(hiddenColumn);//Add to very end
		}
		
		selectedHiddenIndex=-1;
		selectedHiddenColumn=null;
		selectedDisplayedIndex=-1;
		selectedDisplayedColumn=null;
		
		dirty=true;
	}
	
	public void assignAllAction() throws IOException {
		DwFilter hiddenColumn = null;
		DwFilter displayedColumn = null;
		if(selectedDisplayedIndex!=-1 && selectedDisplayedColumn!=null){
			for(int i=0; i< dwFilterList.size(); i++){
				displayedColumn = (DwFilter)dwFilterList.get(i);
				if(displayedColumn.getColumnName().equals(selectedDisplayedColumn.getColumnName())){
					if((i+1)<dwFilterList.size()){
						for(int j=0; j< dwHiddenList.size(); j++){
							hiddenColumn = (DwFilter)dwHiddenList.get(j);	
							dwFilterList.add((i+1)+j, hiddenColumn);//Insert at i+ 1
						}
						dwHiddenList.clear();
					}
					else{
						dwFilterList.addAll(dwHiddenList);
						dwHiddenList.clear();
					}
					break;				
				}
			}	
		}
		else{
			dwFilterList.addAll(dwHiddenList);//Add to very end
			dwHiddenList.clear();
		}
		
		selectedHiddenIndex=-1;
		selectedHiddenColumn=null;
		selectedDisplayedIndex=-1;
		selectedDisplayedColumn=null;
		
		dirty=true;
	}

	public void removeAction() {
		DwFilter displayedColumn = null;
		if(selectedDisplayedIndex!=-1 && selectedDisplayedColumn!=null){	
			for(int i=0; i< dwFilterList.size(); i++){
				displayedColumn = (DwFilter)dwFilterList.get(i);
				if(displayedColumn.getColumnName().equals(selectedDisplayedColumn.getColumnName()) ){
					dwFilterList.remove(i);
					dwHiddenList.add(displayedColumn);//Add to very end
					break;				
				}
			}
		}
			
		if(displayedColumn==null){
			return;
		}

		selectedHiddenIndex=-1;
		selectedHiddenColumn=null;
		selectedDisplayedIndex=-1;
		selectedDisplayedColumn=null;
		
		dirty=true;
	}
	
	public void removeAllAction() {
		dwHiddenList.addAll(dwFilterList);//Add to very end
		dwFilterList.clear();
		
		selectedHiddenIndex=-1;
		selectedHiddenColumn=null;
		selectedDisplayedIndex=-1;
		selectedDisplayedColumn=null;
		
		dirty=true;
	}
	
	public int getHiddenColumnCount(){
		if(dwHiddenList==null){
			return 0;
		}
		else{
			return dwHiddenList.size();
		}
	}
	
	public int getDisplayedColumnCount(){
		if(dwFilterList==null){
			return 0;
		}
		else{
			return dwFilterList.size();
		}
	}
	
	public void processUpUpReorderCommand(ActionEvent event) {
		if(selectedDisplayedColumn==null){
			return;
		}
		
		DwFilter displayedColumn = null;
		for(int i=0; i< dwFilterList.size(); i++){
			displayedColumn = (DwFilter)dwFilterList.get(i);
			if(displayedColumn.getColumnName().equals(selectedDisplayedColumn.getColumnName()) ){
				if(i!=0){
					dwFilterList.remove(i);
					dwFilterList.add(0,displayedColumn);//Move to very top
				}
				break;				
			}
		}
	}
	
	public void processUpReorderCommand(ActionEvent event) {
		if(selectedDisplayedColumn==null){
			return;
		}
		
		DwFilter displayedColumn = null;
		for(int i=0; i< dwFilterList.size(); i++){
			displayedColumn = (DwFilter)dwFilterList.get(i);
			if(displayedColumn.getColumnName().equals(selectedDisplayedColumn.getColumnName()) ){
				if(i!=0){
					dwFilterList.remove(i);
					dwFilterList.add(i-1,displayedColumn);//Move one up
				}
				break;				
			}
		}
	}
	
	public void processDownReorderCommand(ActionEvent event) {
		if(selectedDisplayedColumn==null){
			return;
		}
		
		DwFilter displayedColumn = null;
		for(int i=0; i< dwFilterList.size(); i++){
			displayedColumn = (DwFilter)dwFilterList.get(i);
			if(displayedColumn.getColumnName().equals(selectedDisplayedColumn.getColumnName()) ){
				if(i!=(dwFilterList.size()-1)){
					dwFilterList.remove(i);
					dwFilterList.add(i+1,displayedColumn);//Move one down
				}
				break;				
			}
		}
	}
	
	public void processDownDownReorderCommand(ActionEvent event) {
		if(selectedDisplayedColumn==null){
			return;
		}
		
		DwFilter displayedColumn = null;
		for(int i=0; i< dwFilterList.size(); i++){
			displayedColumn = (DwFilter)dwFilterList.get(i);
			if(displayedColumn.getColumnName().equals(selectedDisplayedColumn.getColumnName()) ){
				if(i!=(dwFilterList.size()-1)){
					dwFilterList.remove(i);
					dwFilterList.add(displayedColumn);//Move to very bottom
				}
				break;				
			}
		}
	}
	
	public String okUpdateAction() {
		List<DwFilter> filterList = new ArrayList<DwFilter>();	
		if(dwFilterList!=null){
			List<TransactionHeader> transactionHeaders = convertDwFilterToTransactionHeader(dwFilterList, selectedColumnName);
			dwColumnService.updateColumnsByUserAndFilterName(screenName, selectedColumnName, selectedUserName, transactionHeaders);		
		}
		
		//TODO: refresh filter combo
		if(transactionDetailBean!=null){
			transactionDetailBean.setViewItems(null);
			
			transactionDetailBean.setSelectedColumn(selectedUserName + "/" + selectedColumnName);
			transactionDetailBean.viewChangeListener(null);
		}
		
		return "transactions_process";
	}
	
	public boolean getIsSelectUserNameEnabled(){
		return true;
	}
	
	public String deleteAction() {	
		dwColumnService.deleteColumnsByUserAndFilterName(screenName, selectedColumnName, selectedUserName);
		
		//TODO: refresh Column combo
		if(transactionDetailBean!=null){
			transactionDetailBean.setViewItems(null);
		}
		
		if(currentColumnName.equalsIgnoreCase(selectedColumnName)){
			//TODO: reset to "*NOFILTER"
			if(transactionDetailBean!=null){
				transactionDetailBean.setSelectedColumn("");
				transactionDetailBean.viewChangeListener(null);
			}
		}
		
		return "transactions_process";
	}
	
	public String cancelAction() {
		if(transactionDetailBean!=null){
			transactionDetailBean.setViewItems(null);
			
			transactionDetailBean.setSelectedColumn(selectedUserName + "/" + selectedColumnName);
			transactionDetailBean.viewChangeListener(null);
		}
		return "transactions_process";
	}
	
	public void setallbackBeanC(TransactionDetailBean transactionDetailBean){
		this.transactionDetailBean = transactionDetailBean;
	}
}