package com.ncsts.view.bean;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import javax.faces.component.UISelectItems;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.ajax4jsf.component.html.HtmlAjaxSupport;
import org.apache.log4j.Logger;
import org.richfaces.component.html.HtmlScrollableDataTable;
import org.richfaces.model.selection.Selection;
import org.richfaces.model.selection.SimpleSelection;

import com.ncsts.common.CacheManager;
import com.ncsts.domain.Jurisdiction;
import com.ncsts.domain.JurisdictionTaxrate;
import com.ncsts.dto.TaxCodeStateDTO;
import com.ncsts.services.JurisdictionService;
import com.ncsts.services.TaxCodeStateService;

public class JurisdictionBean {
	
	private Logger logger = Logger.getLogger(JurisdictionBean.class);

  protected CacheManager cacheManager;
  protected JurisdictionService jurisdictionService;	
  protected TaxCodeStateService taxCodeStateService;
	
  private String geoCode;
  private Long jurisdictionId;
  private Date effectiveDate;
  private String city;
  private Long jurisdictionTaxRateId;
  private Date expireDate;
  private String county;
  protected HtmlSelectOneMenu stateMenu = new HtmlSelectOneMenu();
  protected HtmlSelectOneMenu taxRateTypeMenu = new HtmlSelectOneMenu();
  private String clientGeoCode;
  private boolean custom;
  private boolean modified;
  private String zipCode;
  private String compGeoCode;
  protected SimpleSelection selection = new SimpleSelection();
  private List<Jurisdiction> jurisdictionList = new ArrayList<Jurisdiction>();
  private List<JurisdictionTaxrate> jurisdictionTaxrateList = new ArrayList<JurisdictionTaxrate>();
  
  private List<Jurisdiction> selectedJurisdictionList;
  private Jurisdiction selectedJurisdiction;
  private JurisdictionTaxrate selectedJurisdictionTaxrate;
  
  private int rowCount=0;
  
  
  
  private Locale locale = Locale.US;
  
  
  public void loadStateCombo(){
	  List<SelectItem> states = new ArrayList<SelectItem>();
	  // Only add states that are active
	  List<TaxCodeStateDTO> taxCodeState = this.taxCodeStateService.getActiveTaxCodeState();
	  for (TaxCodeStateDTO tcs : taxCodeState) {
		  states.add(new SelectItem(tcs.getTaxCodeState(), tcs.getName()));
	  }

	  UISelectItems items = new UISelectItems();
	  items.setValue(states);
	  stateMenu.getChildren().add(items);
		
	 items = new UISelectItems();
	 List<SelectItem> taxRateTypeList = new ArrayList<SelectItem>();
	 taxRateTypeList.add(new SelectItem("Jurisdiction","Jurisdiction"));
	 taxRateTypeList.add(new SelectItem("TaxRatesOnly","TaxRatesOnly"));
	 items.setValue(taxRateTypeList);
	 taxRateTypeMenu.getChildren().add(items);
  }
  
  public void fetchJurisdictionListList(){
	  jurisdictionList = new ArrayList<Jurisdiction>();
	  Jurisdiction jurisdiction = new Jurisdiction();
	  jurisdiction.setGeocode((this.geoCode!=null && !this.geoCode.trim().equals(""))?this.geoCode:null);
	  jurisdiction.setJurisdictionId((this.jurisdictionId!=null)?this.jurisdictionId:null);
	  //set effective date
	  //set expiry date
	  jurisdiction.setCity((this.city!=null && !this.city.trim().equals(""))?this.city:null);
	  //jur.tax rate id
	  jurisdiction.setCounty((this.county!=null && !this.county.trim().equals(""))?this.county:null);
	  jurisdiction.setState(!((String)this.stateMenu.getValue()).trim().equals("-1")?(String) this.stateMenu.getValue():null);
	  jurisdiction.setClientGeocode((this.clientGeoCode!=null && !this.clientGeoCode.trim().equals(""))?this.clientGeoCode:null);
	  jurisdiction.setCustomFlag((this.custom)?"1":"0");
	  jurisdiction.setZip((this.zipCode!=null && !this.zipCode.trim().equals(""))?this.zipCode:null);
	  jurisdiction.setCompGeocode((this.compGeoCode!=null && !this.compGeoCode.trim().equals(""))?this.compGeoCode:null);
	  //modified flag
	  
	  jurisdictionList=this.jurisdictionService.findByExample(jurisdiction, 200);
	  //not setting jurisdiction in getAllJurisdiction??? !!!!! 
	  
	  if(jurisdictionList!=null)
		  rowCount = jurisdictionList.size();
	  
  }
  
  public void selectedRowChanged(ActionEvent e) throws AbortProcessingException { 
		HtmlScrollableDataTable table = (HtmlScrollableDataTable) ((HtmlAjaxSupport) e.getComponent()).getParent();
		Selection sel = table.getSelection();
		List<Jurisdiction> selected = new ArrayList<Jurisdiction>(); 
		for (Iterator<Object> iter = sel.getKeys(); iter.hasNext();) { 
			Object next = iter.next(); 
			table.setRowKey(next); 
			selected.add((Jurisdiction)table.getRowData()); 
			
		}
		this.selectedJurisdictionList = selected; 
		if(getSelectedJurisdictionList().size() > 0){
			this.selectedJurisdiction = getSelectedJurisdictionList().get(0);
		}	
		
		Long jusrisdictionId= selectedJurisdiction.getJurisdictionId();
		
		JurisdictionTaxrate jurisdictionTaxrate = new JurisdictionTaxrate();
		jurisdictionTaxrate.setJurisdiction(new Jurisdiction(jusrisdictionId));
		
		
		jurisdictionTaxrateList=this.jurisdictionService.getAllJurisdictionTaxrate(jurisdictionTaxrate, 200);
		logger.debug("jurisdictionTaxrateList.size:"+jurisdictionTaxrateList.size());
		
		if(jurisdictionTaxrateList!=null && jurisdictionTaxrateList.size()>0)
			selectedJurisdictionTaxrate = jurisdictionTaxrateList.get(0);
		
		
	}

public CacheManager getCacheManager() {
	return cacheManager;
}

public void setCacheManager(CacheManager cacheManager) {
	this.cacheManager = cacheManager;
}

public JurisdictionService getJurisdictionService() {
	return jurisdictionService;
}

public void setJurisdictionService(JurisdictionService jurisdictionService) {
	this.jurisdictionService = jurisdictionService;
	loadStateCombo();
}

public TaxCodeStateService getTaxCodeStateService() {
	return taxCodeStateService;
}

public void setTaxCodeStateService(TaxCodeStateService taxCodeStateService) {
	this.taxCodeStateService = taxCodeStateService;
}
  
public String getGeoCode() {
	return geoCode;
}

public void setGeoCode(String geoCode) {
	this.geoCode = geoCode;
}

public Long getJurisdictionId() {
	return jurisdictionId;
}

public void setJurisdictionId(Long jurisdictionId) {
	this.jurisdictionId = jurisdictionId;
}

public Date getEffectiveDate() {
	return effectiveDate;
}

public void setEffectiveDate(Date effectiveDate) {
	this.effectiveDate = effectiveDate;
}

public String getCity() {
	return city;
}

public void setCity(String city) {
	this.city = city;
}

public Long getJurisdictionTaxRateId() {
	return jurisdictionTaxRateId;
}

public void setJurisdictionTaxRateId(Long jurisdictionTaxRateId) {
	this.jurisdictionTaxRateId = jurisdictionTaxRateId;
}

public Date getExpireDate() {
	return expireDate;
}

public void setExpireDate(Date expireDate) {
	this.expireDate = expireDate;
}

public String getCounty() {
	return county;
}

public void setCounty(String county) {
	this.county = county;
}

public HtmlSelectOneMenu getStateMenu() {
	return stateMenu;
}

public void setStateMenu(HtmlSelectOneMenu stateMenu) {
	this.stateMenu = stateMenu;
}

public String getClientGeoCode() {
	return clientGeoCode;
}

public void setClientGeoCode(String clientGeoCode) {
	this.clientGeoCode = clientGeoCode;
}

public boolean isCustom() {
	return custom;
}

public void setCustom(boolean custom) {
	this.custom = custom;
}

public boolean isModified() {
	return modified;
}

public void setModified(boolean modified) {
	this.modified = modified;
}

public String getZipCode() {
	return zipCode;
}

public void setZipCode(String zipCode) {
	this.zipCode = zipCode;
}

public String getCompGeoCode() {
	return compGeoCode;
}

public void setCompGeoCode(String compGeoCode) {
	this.compGeoCode = compGeoCode;
}

public List<Jurisdiction> getJurisdictionList() {
	return jurisdictionList;
}

public void setJurisdictionList(List<Jurisdiction> jurisdictionList) {
	this.jurisdictionList = jurisdictionList;
}

public List<JurisdictionTaxrate> getJurisdictionTaxrateList() {
	return jurisdictionTaxrateList;
}

public void setJurisdictionTaxrateList(
		List<JurisdictionTaxrate> jurisdictionTaxrateList) {
	this.jurisdictionTaxrateList = jurisdictionTaxrateList;
}

public Locale getLocale() {
	return locale;
}

public void setLocale(Locale locale) {
	this.locale = locale;
}

public SimpleSelection getSelection() {
	return selection;
}

public void setSelection(SimpleSelection selection) {
	this.selection = selection;
}

public List<Jurisdiction> getSelectedJurisdictionList() {
	return selectedJurisdictionList;
}

public void setSelectedJurisdictionList(
		List<Jurisdiction> selectedJurisdictionList) {
	this.selectedJurisdictionList = selectedJurisdictionList;
}

public Jurisdiction getSelectedJurisdiction() {
	return selectedJurisdiction;
}

public void setSelectedJurisdiction(Jurisdiction selectedJurisdiction) {
	this.selectedJurisdiction = selectedJurisdiction;
}

public int getRowCount() {
	return rowCount;
}

public void setRowCount(int rowCount) {
	this.rowCount = rowCount;
}

public HtmlSelectOneMenu getTaxRateTypeMenu() {
	return taxRateTypeMenu;
}

public void setTaxRateTypeMenu(HtmlSelectOneMenu taxRateTypeMenu) {
	this.taxRateTypeMenu = taxRateTypeMenu;
}

public JurisdictionTaxrate getSelectedJurisdictionTaxrate() {
	return selectedJurisdictionTaxrate;
}

public void setSelectedJurisdictionTaxrate(
		JurisdictionTaxrate selectedJurisdictionTaxrate) {
	this.selectedJurisdictionTaxrate = selectedJurisdictionTaxrate;
}

}
