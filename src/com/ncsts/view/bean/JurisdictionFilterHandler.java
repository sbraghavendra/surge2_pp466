package com.ncsts.view.bean;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import javax.faces.component.UIInput;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.ajax4jsf.component.html.HtmlAjaxSupport;
import org.springframework.util.StringUtils;

import com.ncsts.common.CacheManager;
import com.ncsts.domain.Jurisdiction;
import com.ncsts.domain.TaxCodeStatePK;
import com.ncsts.dto.NexusJurisdictionItemDTO;
import com.ncsts.dto.TaxCodeStateDTO;
import com.ncsts.services.JurisdictionService;
import com.ncsts.services.TaxCodeStateService;

public class JurisdictionFilterHandler {
	private CacheManager cacheManager;
	private JurisdictionService jurisdictionService;
	private TaxCodeStateService taxCodeStateService;
	
	private HtmlSelectOneMenu countryInput = new HtmlSelectOneMenu();
	private HtmlSelectOneMenu stateInput = new HtmlSelectOneMenu();
	private HtmlSelectOneMenu countyInput = new HtmlSelectOneMenu();	
	private HtmlSelectOneMenu cityInput = new HtmlSelectOneMenu();
	private HtmlSelectOneMenu stjInput = new HtmlSelectOneMenu();
	
	private String stj;
	private String city;
   	private String county;
   	private String state;
   	private String country;
   	
   	List<SelectItem> countyItems;
   	List<SelectItem> cityItems;
   	List<SelectItem> stjItems;

	public HtmlSelectOneMenu getCountryInput() {
		return countryInput;
	}

	public void setCountryInput(HtmlSelectOneMenu countryInput) {
		this.countryInput = countryInput;
	}

	public HtmlSelectOneMenu getStateInput() {
		return stateInput;
	}

	public void setStateInput(HtmlSelectOneMenu stateInput) {
		this.stateInput = stateInput;
	}

	public HtmlSelectOneMenu getCountyInput() {
		return countyInput;
	}

	public void setCountyInput(HtmlSelectOneMenu countyInput) {
		this.countyInput = countyInput;
	}

	public HtmlSelectOneMenu getCityInput() {
		return cityInput;
	}

	public void setCityInput(HtmlSelectOneMenu cityInput) {
		this.cityInput = cityInput;
	}
	
	public HtmlSelectOneMenu getStjInput() {
		return stjInput;
	}

	public void setStjInput(HtmlSelectOneMenu stjInput) {
		this.stjInput = stjInput;
	}
	
	public String getStj() {
		return stj;
	}

	public void setStj(String stj) {
		this.stj = stj;
		setInputValue(stjInput, stj);
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
		setInputValue(cityInput, city);
	}

	public String getCounty() {
		return county;
	}

	public void setCounty(String county) {
		this.county = county;
		setInputValue(countyInput, county);
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
		setInputValue(stateInput, state);
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
		setInputValue(countryInput, country);
	}
	
	public void countrySelected(ActionEvent e) {
		String country = (String) ((HtmlSelectOneMenu) ((HtmlAjaxSupport) e.getComponent()).getParent()).getSubmittedValue();
		
		setCountry(country);
		setState("");
		setCounty("");
		setCity("");
		setStj("");
		clearCountyCityItems();
		clearStjItems();
	}
	
	public void stateSelected(ActionEvent e) {
		String state = (String) ((HtmlSelectOneMenu) ((HtmlAjaxSupport) e.getComponent()).getParent()).getSubmittedValue();
		
		setState(state);
		setCounty("");
		setCity("");
		setStj("");
		createCountyCityItems();
		createStjItems();
	}
	
	public void countySelected(ActionEvent e) {
		String county = (String) ((HtmlSelectOneMenu) ((HtmlAjaxSupport) e.getComponent()).getParent()).getSubmittedValue();		
		setCounty(county);
		if(StringUtils.hasText(county) && !"*ALL".equals(county)) {
			setCity("*ALL");
			setStj("*ALL");
		}
	}
	
	public void citySelected(ActionEvent e) {
		String city = (String) ((HtmlSelectOneMenu) ((HtmlAjaxSupport) e.getComponent()).getParent()).getSubmittedValue();
		if(StringUtils.hasText(city) && !"*ALL".equals(city)) {
			setCounty("*ALL");
			setStj("*ALL");
		}
		setCity(city);
	}
	
	public void stjSelected(ActionEvent e) {
		String stj = (String) ((HtmlSelectOneMenu) ((HtmlAjaxSupport) e.getComponent()).getParent()).getSubmittedValue();
		if(StringUtils.hasText(stj) && !"*ALL".equals(stj)) {
			setCounty("*ALL");
			setCity("*ALL");
		}
		setStj(stj);
	}
	
	private void clearCountyCityItems() {
		countyItems = new ArrayList<SelectItem>();
		cityItems = new ArrayList<SelectItem>();
		countyItems.add(new SelectItem("","Select a County"));
		cityItems.add(new SelectItem("","Select a City"));
	}
	
	private void clearStjItems(){
		stjItems = new ArrayList<SelectItem>();
		stjItems.add(new SelectItem("","Select an STJ"));
	}
	
	private void createStjItems(){
		Jurisdiction jurisdiction = new Jurisdiction();
		jurisdiction.setCountry(country);
		jurisdiction.setState(state);
		
		TaxCodeStatePK pk = new TaxCodeStatePK();
		pk.setCountry(country);
		pk.setTaxcodeStateCode(state);
		
		List<String>  listObject = null;
		boolean hasLocal = false;
		
		if(StringUtils.hasText(country) && StringUtils.hasText(state)) {
			TaxCodeStateDTO tcs = taxCodeStateService.findByPK(pk);
			if(tcs != null && "L".equalsIgnoreCase(tcs.getLocalTaxabilityCode())) {
				hasLocal = true;
			}
			
			if(hasLocal) {
			//	list = jurisdictionService.searchByExample(jurisdiction, true);
				
				
				listObject = jurisdictionService.findCountryStateStj(country, state);
			}
		}
		
		stjItems = new ArrayList<SelectItem>();
		stjItems.add(new SelectItem("","Select an STJ"));
		stjItems.add(new SelectItem("*ALL", "*ALL"));
		
		Set<String> stjsSet = new TreeSet<String>();
		String stjName = null;
		if(listObject != null) {
			//if(listObject.size() > 0 || !hasLocal) {
			//	stjItems.add(new SelectItem("*ALL", "*ALL"));
			//}
			
			for(String n : listObject) {
				stjName = (String) n;
				stjItems.add(new SelectItem(stjName, stjName));
			}
		}	
	}
	
	private void createCountyCityItems() {
		Jurisdiction jurisdiction = new Jurisdiction();
		jurisdiction.setCountry(country);
		jurisdiction.setState(state);
		
		TaxCodeStatePK pk = new TaxCodeStatePK();
		pk.setCountry(country);
		pk.setTaxcodeStateCode(state);
		
		List<Jurisdiction> list = null;
		boolean hasLocal = false;
		
		if(StringUtils.hasText(country) && StringUtils.hasText(state)) {
			TaxCodeStateDTO tcs = taxCodeStateService.findByPK(pk);
			if(tcs != null && "L".equalsIgnoreCase(tcs.getLocalTaxabilityCode())) {
				hasLocal = true;
			}
			
			if(hasLocal) {
				list = jurisdictionService.searchByExample(jurisdiction, true);
			}
		}
		
		countyItems = new ArrayList<SelectItem>();
		cityItems = new ArrayList<SelectItem>();
		countyItems.add(new SelectItem("","Select a County"));
		countyItems.add(new SelectItem("*ALL", "*ALL"));
		
		cityItems.add(new SelectItem("","Select a City"));
		cityItems.add(new SelectItem("*ALL", "*ALL"));
		
		Set<String> countiesSet = new TreeSet<String>();
		Set<String> citiesSet = new TreeSet<String>();
		
		if(list != null || !hasLocal) {
			if(hasLocal) {
				for(Jurisdiction j : list) {
					countiesSet.add(j.getCounty());
					citiesSet.add(j.getCity());
				}
			}
		
			for(String s : countiesSet) {
				countyItems.add(new SelectItem(s, s));
			}
			
			for(String s : citiesSet) {
				cityItems.add(new SelectItem(s, s));
			}
		}		
	}

	private void setInputValue(UIInput input, String value) {
		input.setLocalValueSet(false);
		input.setSubmittedValue(null);
		input.setValue(value);
	}
	
	private String getInputValue(UIInput input) {
		String value = (String) input.getSubmittedValue();
		return (value != null)? value:(String) input.getValue();
	}

	public CacheManager getCacheManager() {
		return cacheManager;
	}

	public void setCacheManager(CacheManager cacheManager) {
		this.cacheManager = cacheManager;
	}
	
	public List<SelectItem> getCountryItems() {
		return cacheManager.createCountryItems();
	}
	
	public List<SelectItem> getStateItems() {
		String strCountry = country;
		List<SelectItem> selectItems = new ArrayList<SelectItem>();
		selectItems.add(new SelectItem("","Select a State"));
		selectItems.add(new SelectItem("*ALL", "*ALL"));
		Collection<TaxCodeStateDTO> taxCodeStateDTOList = cacheManager.getTaxCodeStateMap().values();
		if(strCountry!=null && strCountry.length()>0){  
			for (TaxCodeStateDTO taxCodeStateDTO : taxCodeStateDTOList) {
			  if(strCountry.equalsIgnoreCase(taxCodeStateDTO.getCountry()) && ((taxCodeStateDTO.getActiveFlag() != null) && taxCodeStateDTO.getActiveFlag().equals("1")) ){
				  selectItems.add(new SelectItem(taxCodeStateDTO.getTaxCodeState(), 
						  taxCodeStateDTO.getName() + " (" + taxCodeStateDTO.getTaxCodeState() + ")"));
			  }
		  }
		}
	  
		return selectItems;
	}
	
	public JurisdictionService getJurisdictionService() {
		return jurisdictionService;
	}

	public void setJurisdictionService(JurisdictionService jurisdictionService) {
		this.jurisdictionService = jurisdictionService;
	}

	public List<SelectItem> getCountyItems() {
		if(countyItems == null) {
			clearCountyCityItems();
		}
		return countyItems;
	}

	public void setCountyItems(List<SelectItem> countyItems) {
		this.countyItems = countyItems;
	}

	public List<SelectItem> getCityItems() {
		if(cityItems == null) {
			clearCountyCityItems();
		}
		
		return cityItems;
	}

	public void setCityItems(List<SelectItem> cityItems) {
		this.cityItems = cityItems;
	}
	
	public List<SelectItem> getStjItems() {
		if(stjItems == null) {
			clearStjItems();
		}
		
		return stjItems;
	}

	public void setStjItems(List<SelectItem> stjItems) {
		this.stjItems = stjItems;
	}
	
	public void reset() {
		setCountry("");
		setState("");
		setCounty("");
		setCity("");
		setStj("");
		clearCountyCityItems();
		clearStjItems();
	}

	public TaxCodeStateService getTaxCodeStateService() {
		return taxCodeStateService;
	}

	public void setTaxCodeStateService(TaxCodeStateService taxCodeStateService) {
		this.taxCodeStateService = taxCodeStateService;
	}
}
