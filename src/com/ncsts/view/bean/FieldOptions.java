package com.ncsts.view.bean;

public class FieldOptions {
	private boolean readOnly;
	private boolean disabled;
	
	// Default constructor is private
	FieldOptions(boolean readOnly, boolean disabled) {
		this.readOnly = readOnly;	// NO LONGER USED!
		this.disabled = (readOnly || disabled);
	}
	
	@SuppressWarnings("unused")
	private FieldOptions() {
		// Do nothing
	}

	public boolean getReadOnly() {
		return readOnly;
	}
	
	public boolean getDisabled() {
		return disabled;
	}
}
