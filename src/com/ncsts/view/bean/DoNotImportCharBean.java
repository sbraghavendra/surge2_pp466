package com.ncsts.view.bean;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

import javax.persistence.Column;

import com.ncsts.domain.BatchErrorLog;
import com.ncsts.view.util.JsfHelper;

public class DoNotImportCharBean implements java.io.Serializable{
	private String dni1 = "";
    private String dni2 = "";
    private String dni3 = "";
    private String dni4 = "";
    private String dni5 = "";
    private String dni6 = "";
    private String dni7 = "";
    private String dni8 = "";
    private String dni9 = "";
    private String dni10 = "";
    private String dni11 = "";
    private String dni12 = "";
    private String dni13 = "";
    private String dni14 = "";
    private String dni15 = "";
    private String dni16 = "";
    private String dni17 = "";
    private String dni18 = "";
    private String dni19 = "";
    private String dni20 = "";
    private String dni21 = "";
    private String dni22 = "";
    private String dni23 = "";
    private String dni24 = "";
    private String dni25 = "";
    private String dni26 = "";
    private String dni27 = "";
    private String dni28 = "";
    private String dni29 = "";
    private String dni30 = "";
    private String dni31 = "";
    private String dni32 = "";
    private String dni33 = "";
    private String dni34 = "";
    private String dni35 = "";
    private String dni36 = "";
    private String dni37 = "";
    private String dni38 = "";
    private String dni39 = "";
    private String dni40 = "";
    private String dni41 = "";
    private String dni42 = "";
    private String dni43 = "";
    private String dni44 = "";
    private String dni45 = "";
    private String dni46 = "";
    private String dni47 = "";
    private String dni48 = "";
    private String dni49 = "";
    private String dni50 = "";
    
    private String rw1 = "";
    private String rw2 = "";
    private String rw3 = "";
    private String rw4 = "";
    private String rw5 = "";
    private String rw6 = "";
    private String rw7 = "";
    private String rw8 = "";
    private String rw9 = "";
    private String rw10 = "";
    private String rw11 = "";
    private String rw12 = "";
    private String rw13 = "";
    private String rw14 = "";
    private String rw15 = "";
    private String rw16 = "";
    private String rw17 = "";
    private String rw18 = "";
    private String rw19 = "";
    private String rw20 = "";
    private String rw21 = "";
    private String rw22 = "";
    private String rw23 = "";
    private String rw24 = "";
    private String rw25 = "";
    private String rw26 = "";
    private String rw27 = "";
    private String rw28 = "";
    private String rw29 = "";
    private String rw30 = "";
    private String rw31 = "";
    private String rw32 = "";
    private String rw33 = "";
    private String rw34 = "";
    private String rw35 = "";
    private String rw36 = "";
    private String rw37 = "";
    private String rw38 = "";
    private String rw39 = "";
    private String rw40 = "";
    private String rw41 = "";
    private String rw42 = "";
    private String rw43 = "";
    private String rw44 = "";
    private String rw45 = "";
    private String rw46 = "";
    private String rw47 = "";
    private String rw48 = "";
    private String rw49 = "";
    private String rw50 = "";
    
	private String doNotImportMarkers = "";
	private String replaceWithMarkers = "";
	private String errorMessage = "";
	
	public String getDoNotImportMarkers(){
		return doNotImportMarkers;
	}
	
	public void setDoNotImportMarkers(String doNotImportMarkers){
		this.doNotImportMarkers = doNotImportMarkers;
	}
	
	public String getReplaceWithMarkers(){
		return replaceWithMarkers;
	}
	
	public void setReplaceWithMarkers(String replaceWithMarkers){
		this.replaceWithMarkers = replaceWithMarkers;
	}
    
	public String getErrorMessage(){
		return errorMessage;
	}
	
	public void setErrorMessage(String errorMessage){
		this.errorMessage = errorMessage;
	}
	
	
    public String getDni1(){
        return dni1;
    }

    public void setDni1(String newDni){
        this.dni1 = newDni;
    }

    public String getDni2(){
        return dni2;
    }

    public void setDni2(String newDni){
        this.dni2 = newDni;
    }

    public String getDni3(){
        return dni3;
    }

    public void setDni3(String newDni){
        this.dni3 = newDni;
    }

    public String getDni4(){
        return dni4;
    }

    public void setDni4(String newDni){
        this.dni4 = newDni;
    }

    public String getDni5(){
        return dni5;
    }

    public void setDni5(String newDni){
        this.dni5 = newDni;
    }

    public String getDni6(){
        return dni6;
    }

    public void setDni6(String newDni){
        this.dni6 = newDni;
    }

    public String getDni7(){
        return dni7;
    }

    public void setDni7(String newDni){
        this.dni7 = newDni;
    }

    public String getDni8(){
        return dni8;
    }

    public void setDni8(String newDni){
        this.dni8 = newDni;
    }

    public String getDni9(){
        return dni9;
    }

    public void setDni9(String newDni){
        this.dni9 = newDni;
    }

    public String getDni10(){
        return dni10;
    }

    public void setDni10(String newDni){
        this.dni10 = newDni;
    }

    public String getDni11(){
        return dni11;
    }

    public void setDni11(String newDni){
        this.dni11 = newDni;
    }

    public String getDni12(){
        return dni12;
    }

    public void setDni12(String newDni){
        this.dni12 = newDni;
    }

    public String getDni13(){
        return dni13;
    }

    public void setDni13(String newDni){
        this.dni13 = newDni;
    }

    public String getDni14(){
        return dni14;
    }

    public void setDni14(String newDni){
        this.dni14 = newDni;
    }

    public String getDni15(){
        return dni15;
    }

    public void setDni15(String newDni){
        this.dni15 = newDni;
    }

    public String getDni16(){
        return dni16;
    }

    public void setDni16(String newDni){
        this.dni16 = newDni;
    }

    public String getDni17(){
        return dni17;
    }

    public void setDni17(String newDni){
        this.dni17 = newDni;
    }

    public String getDni18(){
        return dni18;
    }

    public void setDni18(String newDni){
        this.dni18 = newDni;
    }

    public String getDni19(){
        return dni19;
    }

    public void setDni19(String newDni){
        this.dni19 = newDni;
    }

    public String getDni20(){
        return dni20;
    }

    public void setDni20(String newDni){
        this.dni20 = newDni;
    }

    public String getDni21(){
        return dni21;
    }

    public void setDni21(String newDni){
        this.dni21 = newDni;
    }

    public String getDni22(){
        return dni22;
    }

    public void setDni22(String newDni){
        this.dni22 = newDni;
    }

    public String getDni23(){
        return dni23;
    }

    public void setDni23(String newDni){
        this.dni23 = newDni;
    }

    public String getDni24(){
        return dni24;
    }

    public void setDni24(String newDni){
        this.dni24 = newDni;
    }

    public String getDni25(){
        return dni25;
    }

    public void setDni25(String newDni){
        this.dni25 = newDni;
    }

    public String getDni26(){
        return dni26;
    }

    public void setDni26(String newDni){
        this.dni26 = newDni;
    }

    public String getDni27(){
        return dni27;
    }

    public void setDni27(String newDni){
        this.dni27 = newDni;
    }

    public String getDni28(){
        return dni28;
    }

    public void setDni28(String newDni){
        this.dni28 = newDni;
    }

    public String getDni29(){
        return dni29;
    }

    public void setDni29(String newDni){
        this.dni29 = newDni;
    }

    public String getDni30(){
        return dni30;
    }

    public void setDni30(String newDni){
        this.dni30 = newDni;
    }

    public String getDni31(){
        return dni31;
    }

    public void setDni31(String newDni){
        this.dni31 = newDni;
    }

    public String getDni32(){
        return dni32;
    }

    public void setDni32(String newDni){
        this.dni32 = newDni;
    }

    public String getDni33(){
        return dni33;
    }

    public void setDni33(String newDni){
        this.dni33 = newDni;
    }

    public String getDni34(){
        return dni34;
    }

    public void setDni34(String newDni){
        this.dni34 = newDni;
    }

    public String getDni35(){
        return dni35;
    }

    public void setDni35(String newDni){
        this.dni35 = newDni;
    }

    public String getDni36(){
        return dni36;
    }

    public void setDni36(String newDni){
        this.dni36 = newDni;
    }

    public String getDni37(){
        return dni37;
    }

    public void setDni37(String newDni){
        this.dni37 = newDni;
    }

    public String getDni38(){
        return dni38;
    }

    public void setDni38(String newDni){
        this.dni38 = newDni;
    }

    public String getDni39(){
        return dni39;
    }

    public void setDni39(String newDni){
        this.dni39 = newDni;
    }

    public String getDni40(){
        return dni40;
    }

    public void setDni40(String newDni){
        this.dni40 = newDni;
    }

    public String getDni41(){
        return dni41;
    }

    public void setDni41(String newDni){
        this.dni41 = newDni;
    }

    public String getDni42(){
        return dni42;
    }

    public void setDni42(String newDni){
        this.dni42 = newDni;
    }

    public String getDni43(){
        return dni43;
    }

    public void setDni43(String newDni){
        this.dni43 = newDni;
    }

    public String getDni44(){
        return dni44;
    }

    public void setDni44(String newDni){
        this.dni44 = newDni;
    }

    public String getDni45(){
        return dni45;
    }

    public void setDni45(String newDni){
        this.dni45 = newDni;
    }

    public String getDni46(){
        return dni46;
    }

    public void setDni46(String newDni){
        this.dni46 = newDni;
    }

    public String getDni47(){
        return dni47;
    }

    public void setDni47(String newDni){
        this.dni47 = newDni;
    }

    public String getDni48(){
        return dni48;
    }

    public void setDni48(String newDni){
        this.dni48 = newDni;
    }

    public String getDni49(){
        return dni49;
    }

    public void setDni49(String newDni){
        this.dni49 = newDni;
    }

    public String getDni50(){
        return dni50;
    }

    public void setDni50(String newDni){
        this.dni50 = newDni;
    }

    public String getRw1(){
        return rw1;
    }

    public void setRw1(String newRw){
        this.rw1 = newRw;
    }

    public String getRw2(){
        return rw2;
    }

    public void setRw2(String newRw){
        this.rw2 = newRw;
    }

    public String getRw3(){
        return rw3;
    }

    public void setRw3(String newRw){
        this.rw3 = newRw;
    }

    public String getRw4(){
        return rw4;
    }

    public void setRw4(String newRw){
        this.rw4 = newRw;
    }

    public String getRw5(){
        return rw5;
    }

    public void setRw5(String newRw){
        this.rw5 = newRw;
    }

    public String getRw6(){
        return rw6;
    }

    public void setRw6(String newRw){
        this.rw6 = newRw;
    }

    public String getRw7(){
        return rw7;
    }

    public void setRw7(String newRw){
        this.rw7 = newRw;
    }

    public String getRw8(){
        return rw8;
    }

    public void setRw8(String newRw){
        this.rw8 = newRw;
    }

    public String getRw9(){
        return rw9;
    }

    public void setRw9(String newRw){
        this.rw9 = newRw;
    }

    public String getRw10(){
        return rw10;
    }

    public void setRw10(String newRw){
        this.rw10 = newRw;
    }

    public String getRw11(){
        return rw11;
    }

    public void setRw11(String newRw){
        this.rw11 = newRw;
    }

    public String getRw12(){
        return rw12;
    }

    public void setRw12(String newRw){
        this.rw12 = newRw;
    }

    public String getRw13(){
        return rw13;
    }

    public void setRw13(String newRw){
        this.rw13 = newRw;
    }

    public String getRw14(){
        return rw14;
    }

    public void setRw14(String newRw){
        this.rw14 = newRw;
    }

    public String getRw15(){
        return rw15;
    }

    public void setRw15(String newRw){
        this.rw15 = newRw;
    }

    public String getRw16(){
        return rw16;
    }

    public void setRw16(String newRw){
        this.rw16 = newRw;
    }

    public String getRw17(){
        return rw17;
    }

    public void setRw17(String newRw){
        this.rw17 = newRw;
    }

    public String getRw18(){
        return rw18;
    }

    public void setRw18(String newRw){
        this.rw18 = newRw;
    }

    public String getRw19(){
        return rw19;
    }

    public void setRw19(String newRw){
        this.rw19 = newRw;
    }

    public String getRw20(){
        return rw20;
    }

    public void setRw20(String newRw){
        this.rw20 = newRw;
    }

    public String getRw21(){
        return rw21;
    }

    public void setRw21(String newRw){
        this.rw21 = newRw;
    }

    public String getRw22(){
        return rw22;
    }

    public void setRw22(String newRw){
        this.rw22 = newRw;
    }

    public String getRw23(){
        return rw23;
    }

    public void setRw23(String newRw){
        this.rw23 = newRw;
    }

    public String getRw24(){
        return rw24;
    }

    public void setRw24(String newRw){
        this.rw24 = newRw;
    }

    public String getRw25(){
        return rw25;
    }

    public void setRw25(String newRw){
        this.rw25 = newRw;
    }

    public String getRw26(){
        return rw26;
    }

    public void setRw26(String newRw){
        this.rw26 = newRw;
    }

    public String getRw27(){
        return rw27;
    }

    public void setRw27(String newRw){
        this.rw27 = newRw;
    }

    public String getRw28(){
        return rw28;
    }

    public void setRw28(String newRw){
        this.rw28 = newRw;
    }

    public String getRw29(){
        return rw29;
    }

    public void setRw29(String newRw){
        this.rw29 = newRw;
    }

    public String getRw30(){
        return rw30;
    }

    public void setRw30(String newRw){
        this.rw30 = newRw;
    }

    public String getRw31(){
        return rw31;
    }

    public void setRw31(String newRw){
        this.rw31 = newRw;
    }

    public String getRw32(){
        return rw32;
    }

    public void setRw32(String newRw){
        this.rw32 = newRw;
    }

    public String getRw33(){
        return rw33;
    }

    public void setRw33(String newRw){
        this.rw33 = newRw;
    }

    public String getRw34(){
        return rw34;
    }

    public void setRw34(String newRw){
        this.rw34 = newRw;
    }

    public String getRw35(){
        return rw35;
    }

    public void setRw35(String newRw){
        this.rw35 = newRw;
    }

    public String getRw36(){
        return rw36;
    }

    public void setRw36(String newRw){
        this.rw36 = newRw;
    }

    public String getRw37(){
        return rw37;
    }

    public void setRw37(String newRw){
        this.rw37 = newRw;
    }

    public String getRw38(){
        return rw38;
    }

    public void setRw38(String newRw){
        this.rw38 = newRw;
    }

    public String getRw39(){
        return rw39;
    }

    public void setRw39(String newRw){
        this.rw39 = newRw;
    }

    public String getRw40(){
        return rw40;
    }

    public void setRw40(String newRw){
        this.rw40 = newRw;
    }

    public String getRw41(){
        return rw41;
    }

    public void setRw41(String newRw){
        this.rw41 = newRw;
    }

    public String getRw42(){
        return rw42;
    }

    public void setRw42(String newRw){
        this.rw42 = newRw;
    }

    public String getRw43(){
        return rw43;
    }

    public void setRw43(String newRw){
        this.rw43 = newRw;
    }

    public String getRw44(){
        return rw44;
    }

    public void setRw44(String newRw){
        this.rw44 = newRw;
    }

    public String getRw45(){
        return rw45;
    }

    public void setRw45(String newRw){
        this.rw45 = newRw;
    }

    public String getRw46(){
        return rw46;
    }

    public void setRw46(String newRw){
        this.rw46 = newRw;
    }

    public String getRw47(){
        return rw47;
    }

    public void setRw47(String newRw){
        this.rw47 = newRw;
    }

    public String getRw48(){
        return rw48;
    }

    public void setRw48(String newRw){
        this.rw48 = newRw;
    }

    public String getRw49(){
        return rw49;
    }

    public void setRw49(String newRw){
        this.rw49 = newRw;
    }

    public String getRw50(){
        return rw50;
    }

    public void setRw50(String newRw){
        this.rw50 = newRw;
    }
    
    public String getDNIValues(){
		String strDNI = "";
		
		Class<?> cls = DoNotImportCharBean.class;
		String strTempDNI = "";

		for(int i=1; i<=50; i++){
			strTempDNI = "";
	    	try {
				Method mDNI = cls.getMethod("getDni"+i, new Class<?> [] {});
				if (mDNI != null) {
					Object val = mDNI.invoke(this, new Object [] {});	
					strTempDNI = (String)val;
					strTempDNI = strTempDNI.trim();
				}
    	    } catch (Exception e) {}
    	    
    	    if(strTempDNI==null || strTempDNI.length()==0){
    	    	strTempDNI = " "; //1 character
    	    }
    	    
    	    strDNI = strDNI + strTempDNI; 
        }
		
		System.out.println("strDNI = " + strDNI); 
		System.out.println("strDNI.length() = " + strDNI.length()); 
    		
		return strDNI;
    }
    
    public String getRWValues(){
		String strRW = "";
		
		Class<?> cls = DoNotImportCharBean.class;
		String strTempRW = "";

		for(int i=1; i<=50; i++){
			strTempRW = "";
	    	try {
				Method mRW = cls.getMethod("getRw"+i, new Class<?> [] {});
				if (mRW != null) {
					Object val = mRW.invoke(this, new Object [] {});	
					strTempRW = (String)val;
					strTempRW = strTempRW.trim();
				}
    	    } catch (Exception e) {}
    	    
    	    if(strTempRW==null || strTempRW.length()==0){
    	    	strTempRW = "  "; //2 characters
    	    }
    	    else if(strTempRW.length()==1){
    	    	strTempRW = strTempRW + " "; //2 characters
    	    }
    	    
    	    strRW = strRW + strTempRW; 
        }
		
		System.out.println("strRW = " + strRW); 
		System.out.println("strRW.length() = " + strRW.length()); 
    		
		return strRW;
    }

    public void parseValuesToEditControls(String strDNI, String strRW){
    	Class<?> cls = DoNotImportCharBean.class;
    	Field [] fields = cls.getDeclaredFields();

    	// Clear all previous values
    	this.setDoNotImportMarkers("");
    	this.setReplaceWithMarkers("");
    	this.setErrorMessage("");
    	
    	try{
    	  for (Field f : fields) {
			try {
				String methodName = "set" + f.getName().substring(0, 1).toUpperCase() + f.getName().substring(1); 
				Method m = cls.getMethod(methodName, new Class<?> [] {String.class});
				if (m != null) {
					Object val = m.invoke(this, new Object [] {""});	
				}
			} catch (Exception e) {}
		  }
    	} catch (Exception e) {
    		e.printStackTrace();
    		
    	}
 
    	int DniCount = strDNI.length();
    	String methodDNIName = "";
    	String methodRWName = "";

    	String methodDNIRoot = "setDni";
    	String methodRWRoot = "setRw";
    	
    	for(int i=1; i<=strDNI.length(); i++){
    		methodDNIName = methodDNIRoot + i; 
    		methodRWName = methodRWRoot + i; 
    		try {			
				if(i*2 <= strRW.length()){ //2 chars array for RW
					Method mDNI = cls.getMethod(methodDNIName, new Class<?> [] {String.class});
					int startIndex = i;
					
					if (mDNI != null) {Object val = mDNI.invoke(this, new Object [] {strDNI.substring(startIndex-1, startIndex).trim()});}
				
					Method mRW = cls.getMethod(methodRWName, new Class<?> [] {String.class});
					if (mRW != null) {Object val = mRW.invoke(this, new Object [] {strRW.substring((startIndex-1)*2, startIndex*2).trim()});}
				}
				else{
					//Not enough RW chars
					break;
				}
    		} catch (Exception e) {
    			e.printStackTrace();
    		}
    	}	
	}

}
