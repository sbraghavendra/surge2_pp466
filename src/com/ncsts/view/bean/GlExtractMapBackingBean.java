package com.ncsts.view.bean;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.component.UISelectItems;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlPanelGrid;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.ajax4jsf.component.UIDataAdaptor;
import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;

import com.ncsts.common.LoggerFactory;
import com.ncsts.common.Util;
import com.ncsts.domain.DriverNames;
import com.ncsts.domain.GlExtractMap;
import com.ncsts.domain.ListCodes;
import com.ncsts.domain.TaxCodeDetail;
import com.ncsts.dto.TaxCodeStateDTO;
import com.ncsts.jsf.model.GlExtractMapDataModel;
import com.ncsts.services.GlExtractMapService;
//import com.ncsts.services.ListCodesService;
import com.ncsts.services.TaxCodeDetailService;
import com.ncsts.services.TaxCodeStateService;
import com.ncsts.view.util.FacesUtils;
import com.ncsts.view.util.JsfHelper;

public class GlExtractMapBackingBean extends BaseMatrixBackingBean<GlExtractMap, GlExtractMapService> implements ViewTaxCodeBean.TaxCodeInfoCallback {

	private Logger logger = LoggerFactory.getInstance().getLogger(GlExtractMapBackingBean.class);

	private GlExtractMap selectedGlExtract = new GlExtractMap();
	private TaxCodeDetailService taxCodeDetailService;
	private TaxCodeStateService taxCodeStateService;
	private GlExtractMapService glExtractMapService;
//	private ListCodesService listCodesService;
	private List<SelectItem> stateComboItems;
	private List<SelectItem> listCodeComboItems;
	private List<SelectItem> taxCodeComboItems;
	private Map<String, String> juristypeCodeCodeToDescriptionMap;
	private int selectedRowIndex = -1;
	private GlExtractMapDataModel glExtractMapDataModel;
	private FilePreferenceBackingBean filePreferenceBean;

	// For tax code look up
	private TaxCodeSearchHandler filterTaxCodeHandler;

	private ViewTaxCodeBean viewTaxCodeBean;
	private TaxCodeHandler taxCodeHandler;
	private HtmlSelectOneMenu taxJurisTypeCodeInput;
	private HtmlSelectOneMenu showTaxJurisTypeCodeInput;
	private HtmlInputText comments = new HtmlInputText();
	
	private String currentAction;

	// true if last validation had errors
	private boolean hasErrors;;
	
	public FilePreferenceBackingBean getFilePreferenceBean() {
	  return this.filePreferenceBean;
	}
	  
	public void setFilePreferenceBean(FilePreferenceBackingBean filePreferenceBean) {
	  this.filePreferenceBean = filePreferenceBean;
	}
	  
	public GlExtractMapBackingBean() {
		filterTaxCodeHandler = new TaxCodeSearchHandler(this, "mtns_glextract_main");
		taxCodeHandler = new TaxCodeHandler(this, "mtns_glextract_show");
	}

	public void searchTaxCodeInfoCallback(String taxcodeCountry, String taxcodeState, String taxcodeType,
			String taxcodeCode, String callingPage) {
		viewTaxCodeBean.setCallingPage(callingPage);
		viewTaxCodeBean.setTaxCode(taxcodeCountry, taxcodeState, taxcodeType, taxcodeCode);
	}

	@Override
	public void selectedRowChanged(ActionEvent e) throws AbortProcessingException {
		UIComponent uiComponent = e.getComponent().getParent();
		if (!(uiComponent instanceof UIDataAdaptor)) {
			logger.info("Invalid class to event listener: " + uiComponent.getClass().getName());
			return;
		}

		UIDataAdaptor table = (UIDataAdaptor) uiComponent;

		// Reset selection, if necessary
		if (table.getRowData() != null) {
			this.selectedGlExtract = (GlExtractMap) table.getRowData();
			selectedRowIndex = table.getRowIndex();
		}
	}

	public String gotoAddAction() {
		GlExtractMap glNew = new GlExtractMap();
		// propagate all active values from search filter
	 	Map<String,DriverNames> transactionDriverNamesMap = matrixCommonBean.getCacheManager().getTransactionPropertyDriverNamesMap(glNew.getDriverCode());
		BeanUtils.copyProperties(filterSelectionMatrix, glNew);
		for (DriverNames driverNames: transactionDriverNamesMap.values()) {
			if (! driverNames.isActive()) {
				Util.setProperty(glNew, Util.columnToProperty(driverNames.getMatrixColName()), null, String.class);
			}
		}
		glNew.setTaxcodeCountryCode(filterTaxCodeHandler.getTaxcodeCountry());
		glNew.setTaxCodeStateCode(filterTaxCodeHandler.getTaxcodeState());
		glNew.setTaxCodeTypeCode(filterTaxCodeHandler.getTaxcodeType());
		glNew.setTaxCodeCode(filterTaxCodeHandler.getTaxcodeCode());
		glNew.setTaxJurisTypeCode((String)taxJurisTypeCodeInput.getValue());
		return gotoShow("Add", glNew);
	}
	public String gotoUpdateAction() {
		return gotoShow("Update", selectedGlExtract);
	}
	public String gotoDeleteAction() {
		return gotoShow("Delete", selectedGlExtract);
	}
	public String gotoShow(String action, GlExtractMap glExtractMap) {
		displayWarning = false;
		selectedMatrix = glExtractMap;
		this.currentAction = action;
		
		if(glExtractMap.getTaxcodeCountryCode()==null || glExtractMap.getTaxcodeCountryCode().equals("")){
			taxCodeHandler.setTaxcodeCountry(filePreferenceBean.getUserPreferenceDTO().getUserCountry());
			taxCodeHandler.setTaxcodeState("");
			taxCodeHandler.setTaxcodeType("");
			taxCodeHandler.setTaxcodeCode("");
			taxCodeHandler.rebuildMenus();
		}
		else{
			taxCodeHandler.setTaxcodeCountry(glExtractMap.getTaxcodeCountryCode());
			taxCodeHandler.setTaxcodeState(glExtractMap.getTaxCodeStateCode());
			taxCodeHandler.setTaxcodeType(glExtractMap.getTaxCodeTypeCode());
			taxCodeHandler.setTaxcodeCode(glExtractMap.getTaxCodeCode());
			taxCodeHandler.rebuildMenus();
		}
		
		getShowTaxJurisTypeCodeInput().setValue(glExtractMap.getTaxJurisTypeCode());
		getShowTaxJurisTypeCodeInput().setLocalValueSet(false);
		getShowTaxJurisTypeCodeInput().setSubmittedValue(null);
		comments.setValue(glExtractMap.getComments());
		comments.setSubmittedValue(glExtractMap.getComments());
		return "mtns_glextract_show";
	}


	public void searchGlExtract() {
		glExtractMapDataModel.refreshData(false);
		GlExtractMap glExtractMap = new GlExtractMap();
		BeanUtils.copyProperties(getFilterSelection(), glExtractMap);
		glExtractMap.adjustDriverFields();
		Long id = glExtractMap.getId();
		if ((id != null) && (id <= 0L)) {
			glExtractMap.setId(null);
		}
		if (filterTaxCodeHandler.getTaxcodeState() != "" && filterTaxCodeHandler.getTaxcodeState() != null) {
			glExtractMap.setTaxCodeStateCode(filterTaxCodeHandler.getTaxcodeState());
		}
		if (filterTaxCodeHandler.getTaxcodeCountry() != "" && filterTaxCodeHandler.getTaxcodeCountry() != null) {
			glExtractMap.setTaxcodeCountryCode(filterTaxCodeHandler.getTaxcodeCountry());
		}
		if (filterTaxCodeHandler.getTaxcodeType() != "" && filterTaxCodeHandler.getTaxcodeType() != null) {
			glExtractMap.setTaxCodeTypeCode(filterTaxCodeHandler.getTaxcodeType());
		}
		if (filterTaxCodeHandler.getTaxcodeCode() != "" && filterTaxCodeHandler.getTaxcodeCode() != null) {
			glExtractMap.setTaxCodeCode(filterTaxCodeHandler.getTaxcodeCode());
		}
		if ((taxJurisTypeCodeInput.getValue() != null) && (! "-1".equals(taxJurisTypeCodeInput.getValue().toString()))) {
			glExtractMap.setTaxJurisTypeCode(taxJurisTypeCodeInput.getValue().toString());
		}
		updateMatrixFilter(glExtractMap);
		glExtractMapDataModel.setFilterCriteria(glExtractMap);
		resetSelection();
		selectedMatrix = glExtractMap;
		selectedRowIndex = -1;
	}

	public String deleteAction() {
		try {
			Long id = selectedGlExtract.getGlExtractMapId();
			glExtractMapService.remove(id);
			searchGlExtract();
			selectedRowIndex = -1;
		} catch (Exception e) {
			logger.error("Exception during G/L Extract Map delete:", e);
		}
		return "mtns_glextract_main";
	}
 //4091 Babith
	public String addAction() {
		try {
			validateDrivers(FacesContext.getCurrentInstance());
			if (hasErrors)
				return "mtns_glextract_show";
			GlExtractMap glExtractMap = build();
			// Avoid foreign key constraint violations
			glExtractMapService.save(glExtractMap);
			searchGlExtract();
			selectedRowIndex = -1;
		} catch (Exception e) {
			logger.error("Exception during G/L Extract Map save:", e);
		}
		return "mtns_glextract_main";
	}

	public String updateAction() {
		try {
			validateDrivers(FacesContext.getCurrentInstance());
			if (hasErrors)
				return "mtns_glextract_show";
			GlExtractMap glExtractMap = build();
			glExtractMap.setGlExtractMapId(selectedGlExtract.getGlExtractMapId());
			glExtractMapService.update(glExtractMap);
			glExtractMapDataModel.refreshItem(glExtractMap.getGlExtractMapId());
			selectedGlExtract = glExtractMap;
			searchGlExtract();
		} catch (Exception e) {
			logger.error("Exception during G/L Extract Map delete:", e);
		}
		return "mtns_glextract_main";
	}
	
	public String cancelAction() {
//		filterTaxCodeHandler.reset();
//		resetSelection();
//		resetFilter();
		return "mtns_glextract_main";
	}
	
//	private void resetAll() {
//		resetFilter();
//		selectedRowIndex = -1;
//		searchGlExtract();
//	}
//	
	public String resetFilter(){
		resetSelection();
		filterTaxCodeHandler.reset();
		taxCodeHandler.reset();
		selectedRowIndex = -1;
		super.setFilterSelectionPanel(null);
		super.resetFilter();
//		glExtractMapDataModel.refreshData(false);
		FacesUtils.refreshView();
		return "mtns_glextract_main_redirect";
	}
	
	private GlExtractMap build() {
		GlExtractMap glExtractMap = new GlExtractMap();
		BeanUtils.copyProperties(selectedMatrix, glExtractMap);

		String tCode = taxCodeHandler.getTaxcodeState();
		String newCode = !"".equals(tCode) && (tCode != null) ? tCode : null;
		glExtractMap.setTaxCodeStateCode(newCode);
		
		
		tCode = taxCodeHandler.getTaxcodeCountry();
		newCode = !"".equals(tCode) && (tCode != null) ? tCode : null;
		glExtractMap.setTaxcodeCountryCode(newCode);
		
		
		tCode = taxCodeHandler.getTaxcodeType();
		newCode = !"".equals(tCode) && (tCode != null) ? tCode : null;
		glExtractMap.setTaxCodeTypeCode(newCode);
				
		tCode = taxCodeHandler.getTaxcodeCode();
		newCode = !"".equals(tCode) && (tCode != null) ? tCode : null;
		glExtractMap.setTaxCodeCode(newCode);

		if ((showTaxJurisTypeCodeInput.getValue() != null) && (! "-1".equals(showTaxJurisTypeCodeInput.getValue().toString()))) {
			glExtractMap.setTaxJurisTypeCode(showTaxJurisTypeCodeInput.getValue().toString());
		}
		if (!"".equals(comments.getValue().toString()) && comments.getValue() != null) {
			glExtractMap.setComments(comments.getValue().toString());
		}
		return glExtractMap;
	}
	
	public void validateDrivers(FacesContext context) {
		if ((taxCodeHandler.getTaxcodeState() != null) && ! "".equals(taxCodeHandler.getTaxcodeState()) &&
				((taxCodeHandler.getTaxcodeCode() == null) || "".equals(taxCodeHandler.getTaxcodeCode())) &&
				((taxCodeHandler.getTaxcodeType() == null) || "".equals(taxCodeHandler.getTaxcodeType()))) {
				if ((showTaxJurisTypeCodeInput.getValue() != null) && !"*All".equalsIgnoreCase((String)showTaxJurisTypeCodeInput.getValue())) {
					JsfHelper.addError("Tax Jurisdiction Type must be All when only a state is selected.");
				}
		} else {
			if ("".equals(taxCodeHandler.getTaxcodeState()) || (taxCodeHandler.getTaxcodeState() == null))
				JsfHelper.addError("State is required.");
			if ((showTaxJurisTypeCodeInput.getValue() == null) || "-1".equals(showTaxJurisTypeCodeInput.getValue().toString()))
				JsfHelper.addError("Tax Jurisdiction Type is required.");
			if ((taxCodeHandler.getTaxcodeType() == null) || "".equals(taxCodeHandler.getTaxcodeType())) 
				JsfHelper.addError("Tax Type is required.");
			if ((taxCodeHandler.getTaxcodeCode() == null) || "".equals(taxCodeHandler.getTaxcodeCode())) 
				JsfHelper.addError("Tax Code is required.");
		}
		
		hasErrors = FacesContext.getCurrentInstance().getMessages().hasNext();
		// override validation of super class. It sets invalid if just warnings...
		super.validateDrivers(context, null);
	}
	
	public boolean getHasJustWarnings() {
		return !hasErrors && displayWarning;
	}

	/**
	 * For the specified CODE_TYPE_CODE in the table TB_LIST_CODE, create a map of
	 * CODE_CODE to DESCRPTION.
	 * 
	 * @param codeTypeCode The CODE_TYPE_CODE to filter on
	 * @return For the specified CODE_TYPE_CODE, a map of CODE_CODE to
	 *         DESCRIPTION.
	 */
	private Map<String, String> getCodeCodeToDescriptionMap(String codeTypeCode) {
		Map<String, String> map = new HashMap<String, String>();
		//List<ListCodes> listCodes = listCodesService.getListCodesByCodeTypeCode(codeTypeCode);
		List<ListCodes> listCodes = matrixCommonBean.getCacheManager().getListCodesByType(codeTypeCode);

		for (ListCodes lc : listCodes) {
			map.put(lc.getCodeCode(), lc.getDescription());
		}
		return map;
	}

	public Map<String, String> getJuristypeCodeCodeToDescriptionMap() {
		juristypeCodeCodeToDescriptionMap = new HashMap<String, String>();
			juristypeCodeCodeToDescriptionMap = getCodeCodeToDescriptionMap("JURISTYPE");
		

		return juristypeCodeCodeToDescriptionMap;
	}

	public void setMatrixCommonBean(MatrixCommonBean matrixCommonBean) {
		super.setMatrixCommonBean(matrixCommonBean);
		filterTaxCodeHandler.setTaxCodeDetailService(matrixCommonBean.getTaxCodeDetailService());
		taxCodeHandler.setTaxCodeDetailService(matrixCommonBean.getTaxCodeDetailService());
		
		filterTaxCodeHandler.setCacheManager(matrixCommonBean.getCacheManager());
		taxCodeHandler.setCacheManager(matrixCommonBean.getCacheManager());
	}

	@Override
	public String getBeanName() {
		return "glExtractmapBackingBean";
	}

	@Override
	public boolean getIncludeDescFields() {
		return false;
	}

	@Override
	public String getMainPageAction() {
		return "mtns_glextract_main";
	}

	@Override
	public String getDetailFormName() {
		return "glExtractForm";
	}

	public List<SelectItem> getStateComboItems() {
			stateComboItems = new ArrayList<SelectItem>();
			for (TaxCodeStateDTO taxCodeState : taxCodeStateService.getActiveTaxCodeState()) {
				stateComboItems.add(new SelectItem(taxCodeState.getTaxCodeState(), taxCodeState.getName()));
			}
		return stateComboItems;
	}

	public List<SelectItem> getListCodeComboItems() {
			listCodeComboItems = new ArrayList<SelectItem>();
			for (ListCodes lcd : matrixCommonBean.getCacheManager().getListCodesByType("TCTYPE")) {
				listCodeComboItems.add(new SelectItem(lcd.getCodeCode(), lcd.getDescription()));
			}
		return listCodeComboItems;
	}

	public List<SelectItem> getTaxCodeComboItems() {
			taxCodeComboItems = new ArrayList<SelectItem>();
			for (TaxCodeDetail txd : taxCodeDetailService.findAllSorted("taxcodeCode", true)) {
				// TODO this wont work, there are multiple
				// labels for each value!!
				taxCodeComboItems.add(new SelectItem(txd.getTaxcodeCode(), txd.getTaxcodeLabel()));
			}
		
		return taxCodeComboItems;
	}

	public HtmlInputText getComments() {
		return comments;
	}

	public void setComments(HtmlInputText comments) {
		this.comments = comments;
	}

	public GlExtractMapDataModel getGlExtractMapDataModel() {
		return glExtractMapDataModel;
	}

	public void setGlExtractMapDataModel(GlExtractMapDataModel glExtractMapDataModel) {
		this.glExtractMapDataModel = glExtractMapDataModel;
	}

	public boolean isShowButtons() {
		return selectedRowIndex != -1;
	}

	public void sortAction(ActionEvent e) {
		glExtractMapDataModel.toggleSortOrder(e.getComponent().getParent().getId());
	}

	public TaxCodeDetailService getTaxCodeDetailService() {
		return taxCodeDetailService;
	}

	public void setTaxCodeDetailService(TaxCodeDetailService taxCodeDetailService) {
		this.taxCodeDetailService = taxCodeDetailService;
	}

	public TaxCodeStateService getTaxCodeStateService() {
		return taxCodeStateService;
	}

	public void setTaxCodeStateService(TaxCodeStateService taxCodeStateService) {
		this.taxCodeStateService = taxCodeStateService;
	}

	public GlExtractMapService getGlExtractMapService() {
		return glExtractMapService;
	}

	public void setGlExtractMapService(GlExtractMapService glExtractMapService) {
		this.glExtractMapService = glExtractMapService;
	}

	public int getSelectedRowIndex() {
		return selectedRowIndex;
	}

	public void setSelectedRowIndex(int selectedRowIndex) {
		this.selectedRowIndex = selectedRowIndex;
	}

	public TaxCodeSearchHandler getFilterTaxCodeHandler() {
		return filterTaxCodeHandler;
	}

	public TaxCodeHandler getTaxCodeHandler() {
		return taxCodeHandler;
	}

	public GlExtractMap getSelectedGlExtract() {
		return this.selectedGlExtract;
	}

	public HtmlSelectOneMenu getTaxJurisTypeCodeInput() {
		
			taxJurisTypeCodeInput = new HtmlSelectOneMenu();
			taxJurisTypeCodeInput.getChildren().add(getTaxJurisSelections());
		
		return taxJurisTypeCodeInput;
	}

	private UISelectItems getTaxJurisSelections() {
		final Collection<SelectItem> selectItems = new ArrayList<SelectItem>();
		selectItems.add(new SelectItem(-1,"Select a Tax Jurisdiction Type"));
		for (ListCodes lc : matrixCommonBean.getCacheManager().getListCodesByType("JURISTYPE")) {
			selectItems.add(new SelectItem(lc.getCodeCode(), lc.getDescription()));
		}
		UISelectItems items = new UISelectItems();
		items.setValue(selectItems);
		return items;
	}

	public HtmlSelectOneMenu getShowTaxJurisTypeCodeInput() {
		if (showTaxJurisTypeCodeInput == null) {
			showTaxJurisTypeCodeInput = new HtmlSelectOneMenu();
			showTaxJurisTypeCodeInput.getChildren().add(getTaxJurisSelections());
		}
		return showTaxJurisTypeCodeInput;
		
	}

	public void setTaxJurisTypeCodeInput(HtmlSelectOneMenu taxJurisTypeCodeInput) {
		this.taxJurisTypeCodeInput = taxJurisTypeCodeInput;
	}
	public void setShowTaxJurisTypeCodeInput(HtmlSelectOneMenu taxJurisTypeCodeInput) {
		this.showTaxJurisTypeCodeInput = taxJurisTypeCodeInput;
	}

	public String getCurrentAction() {
		return this.currentAction;
	}
	
	public String getCurrentActionButton() {
		return currentAction.toLowerCase() + "Action";
	}

	public boolean getIsAddAction() {
		return "Add".equalsIgnoreCase(currentAction);
	}
	public boolean getIsUpdateAction() {
		return "Update".equalsIgnoreCase(currentAction);
	}
	public boolean getIsDeleteAction() {
		return "Delete".equalsIgnoreCase(currentAction);
	}
	
	public HtmlPanelGrid getShowMatrixPanel() {
		copyAddPanel = MatrixCommonBean.createDriverPanel(
				selectedGlExtract.getDriverCode(),
				matrixCommonBean.getCacheManager(), 
				matrixCommonBean.getEntityItemDTO(),
				"showPanel", 
				getBeanName(), 
				"selectedMatrix", 
				"matrixCommonBean", 
				getIsDeleteAction(), false, false, true, false);
		
		matrixCommonBean.prepareTaxDriverPanel(copyAddPanel);
		return copyAddPanel;
	}
	
	public void setShowMatrixPanel(HtmlPanelGrid panel) {
		copyAddPanel = panel;
	}

	public ViewTaxCodeBean getViewTaxCodeBean() {
		return this.viewTaxCodeBean;
	}

	public void setViewTaxCodeBean(ViewTaxCodeBean viewTaxCodeBean) {
		this.viewTaxCodeBean = viewTaxCodeBean;
	}




}
