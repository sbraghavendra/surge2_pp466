package com.ncsts.view.bean;

import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.GregorianCalendar;
import java.util.Hashtable;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIInput;
import javax.faces.component.html.HtmlSelectBooleanCheckbox;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import com.ncsts.services.JurisdictionService;

import org.ajax4jsf.component.html.HtmlAjaxSupport;
import org.apache.log4j.Logger;
import org.richfaces.component.html.HtmlDataTable;
import org.richfaces.component.html.HtmlSimpleTogglePanel;
import org.richfaces.model.selection.SimpleSelection;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;

import com.ncsts.common.CacheManager;
import com.ncsts.domain.CustEntity;
import com.ncsts.domain.CustEntityPK;
import com.ncsts.domain.EntityItem;
import com.ncsts.domain.EntityLevel;
import com.ncsts.domain.Jurisdiction;
import com.ncsts.domain.Cust;
import com.ncsts.domain.CustLocn;
import com.ncsts.domain.User;
import com.ncsts.dto.EntityItemDTO;
import com.ncsts.jsf.model.CustDataModel;
import com.ncsts.jsf.model.CustLocnDataModel;
import com.ncsts.jsf.model.EntityDataModel;
import com.ncsts.services.CustService;
import com.ncsts.services.CustLocnService;
import com.ncsts.services.DriverReferenceService;
import com.ncsts.services.EntityItemService;
import com.ncsts.services.EntityLevelService;
import com.ncsts.services.ListCodesService;
import com.ncsts.services.OptionService;
import com.ncsts.view.util.TogglePanelController;

import javax.faces.event.ValueChangeEvent;
import javax.persistence.Transient;

public class CustomerLocationBean {	
	private Logger logger = Logger.getLogger(CustomerLocationBean.class);

	private Cust filterCust = new Cust();
	private CustLocn filterCustLocn = new CustLocn();
	private Cust selCust = new Cust();
	private CustLocn selCustLocn = new CustLocn();
	private Cust updateCust = new Cust();
	private CustLocn updateCustLocn = new CustLocn();
	private boolean isEntityCheck=false;
	private List<SelectItem> filterStateMenuItems;
	private List<SelectItem> filterCountryMenuItems;
	private List<SelectItem> filterActiveCustomerMenuItems;
	private List<SelectItem> filterActiveLocationMenuItems;
	private List<SelectItem> filterEntityMenuItems;
	
	private String filterJurisdictionHolidayCode;
	private String filterJurisdictionCountry;
	private String filterJurisdictionState;
	private String filterJurisdictionTaxCode;
	private String filterJurisdictionCounty;
	private String filterJurisdictionCity;
	public String entityCode;
	private CommonCallBack commonCallBack;
	
	private List<SelectItem> entityLevelList = null;
	private Map<Long, String > entityLevelMap = null;
	boolean selectAll = false;
	private loginBean loginBean;
	private ExemptionCertificateBean exemptionCertificateBean;
	
	private List<EntityItem> retrieveEntityItemList= new ArrayList<EntityItem>();
	private List<SelectItem> filterJurisdictionCountyMenuItems;
	private List<SelectItem> filterJurisdictionCityMenuItems;
	private CacheManager cacheManager;
	
	private MatrixCommonBean matrixCommonBean;
	
	private CustDataModel custDataModel;
	private EntityDataModel entityDataModel;
	private CustLocnDataModel custLocnDataModel;
	
	private SimpleSelection selection = new SimpleSelection();
	private int selectedCustRowIndex = -1;
	private int selectedCustLocnRowIndex = -1;
	private EditAction currentAction = EditAction.VIEW;
	static final int MAX_SELECT_ALL = 10000;
	private FilePreferenceBackingBean filePreferenceBean;
	private CustService custService;
	private CustLocnService custLocnService;
	private EntityItemService entityItemService; 
	private ListCodesService listCodesService;
	private OptionService optionService;
	private JurisdictionService jurisdictionService;
	protected EntityLevelService entityLevelService;	
	private EntityItemDTO filterEntityItem = null;
	private String lookupFlag;
	private String showhide="";
	private SearchJurisdictionHandler filterHandler = new SearchJurisdictionHandler();
	private HtmlSelectOneMenu countyInput = new HtmlSelectOneMenu();	
	private HtmlSelectOneMenu cityInput = new HtmlSelectOneMenu();
	private Hashtable<Long,CustEntity> updateCustEntityListChecked = new Hashtable<Long,CustEntity>();
	private TogglePanelController togglePanelController = null;
	private TaxJurisdictionBackingBean taxJurisdictionBean;
	
	public TaxJurisdictionBackingBean getTaxJurisdictionBean() {
		return taxJurisdictionBean;
	}

	public void setTaxJurisdictionBean(
			TaxJurisdictionBackingBean taxJurisdictionBean) {
		this.taxJurisdictionBean = taxJurisdictionBean;
	}
	
	public  TogglePanelController getTogglePanelController(){
		if(togglePanelController==null){
			togglePanelController = new TogglePanelController();
			togglePanelController.addTogglePanel("CustomerInformationPanel", true);
			togglePanelController.addTogglePanel("LocationInformationPanel", false);
		}
		return togglePanelController;
	}
	
	public void setTogglePanelController(TogglePanelController togglePanelController){
		this.togglePanelController = togglePanelController;;
	}
	
	@Autowired
	private DriverReferenceService driverReferenceService;
	
	public Cust getSelCust(){
		return selCust;
	}
	
	public String getLookupFlag() {
		return this.lookupFlag;
	}

	public void setLookupFlag(String lookupFlag) {
		this.lookupFlag = lookupFlag;
	}
	
	public CustLocn getSelCustLocn(){
		return selCustLocn;
	}
	
    public void setMatrixCommonBean(MatrixCommonBean matrixCommonBean) {
    	this.matrixCommonBean = matrixCommonBean;
		filterHandler.setJurisdictionService(matrixCommonBean.getJurisdictionService());
		filterHandler.setCacheManager(matrixCommonBean.getCacheManager());
		filterHandler.setTaxJurisdictionBean(taxJurisdictionBean);
		filterHandler.setCallbackScreen("custlocn_update");
	}
	
	public List<SelectItem> getFilterCountryMenuItems() {
		if(filterCountryMenuItems==null){
			filterCustLocn.setCountry("");
		}
		filterCountryMenuItems = getCacheManager().createCountryItems();   
		return filterCountryMenuItems;
	}
	
	public void filterCountryChanged(ValueChangeEvent e){
		String newCountry = (String)e.getNewValue();
		if(newCountry==null) filterCustLocn.setCountry("");
		else filterCustLocn.setCountry(newCountry);
	
		if(filterStateMenuItems!=null) filterStateMenuItems.clear();

		filterStateMenuItems = getCacheManager().createStateItems(filterCustLocn.getCountry());
		filterCustLocn.setState("");
    }
	
	public List<SelectItem> getFilterStateMenuItems() { 
	   if (filterStateMenuItems == null) {
		   filterStateMenuItems = getCacheManager().createStateItems(filterCustLocn.getCountry());
		   filterCustLocn.setState("");
	   }	    
	   return filterStateMenuItems;
	}
	
	public List<SelectItem> getFilterEntityMenuItems() {
		if(filterEntityMenuItems==null){
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
	    	selectItems.add(new SelectItem(new Long(0L),"Select an Entity"));
	    	
	    	List<EntityItem> eList = entityItemService.findAllEntityItems();
    	    if(eList != null && eList.size() > 0){
    	    	for(EntityItem entityItem:eList){
    	    		if(entityItem.getEntityId().intValue()!=0){
    	    			selectItems.add(new SelectItem(entityItem.getEntityId(), entityItem.getEntityCode() + " - " + entityItem.getEntityName()));		
    	    		}
    	    	}
    	    } 	
	    	    
	    	filterEntityMenuItems = selectItems;
		}
 
		return filterEntityMenuItems;
	}
	
	public List<SelectItem> getFilterActiveCustomerMenuItems() {
		if(filterActiveCustomerMenuItems==null){
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
	    	selectItems.add(new SelectItem("","All Customers"));
	    	selectItems.add(new SelectItem("1","Active Only"));
	    	selectItems.add(new SelectItem("0","Inactive Only"));
	   
	    	filterActiveCustomerMenuItems = selectItems;
	    	
			filterCust.setActiveFlag("");
		}
 
		return filterActiveCustomerMenuItems;
	}
	
	public List<SelectItem> getFilterActiveLocationMenuItems() {
		if(filterActiveLocationMenuItems==null){
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
	    	selectItems.add(new SelectItem("","All Locations"));
	    	selectItems.add(new SelectItem("1","Active Only"));
	    	selectItems.add(new SelectItem("0","Inactive Only"));
	   
	    	filterActiveLocationMenuItems = selectItems;
	    	
			filterCustLocn.setActiveFlag("");
		}
 
		return filterActiveLocationMenuItems;
	}
	
	public JurisdictionService getJurisdictionService() {
		return jurisdictionService;
	}

	public void setJurisdictionService(JurisdictionService jurisdictionService) {
		this.jurisdictionService = jurisdictionService;
	}	
	
	public void setFilterJurisdictionHolidayCode(String filterJurisdictionHolidayCode) {
		this.filterJurisdictionHolidayCode = filterJurisdictionHolidayCode;
	}
	
	public String getFilterJurisdictionHolidayCode() {
		return filterJurisdictionHolidayCode;
	}
	
	public void setFilterJurisdictionCountry(String filterJurisdictionCountry) {
		this.filterJurisdictionCountry = filterJurisdictionCountry;
	}
	
	public String getFilterJurisdictionCountry() {
		return filterJurisdictionCountry;
	}
	
	public void setFilterJurisdictionState(String filterJurisdictionState) {
		this.filterJurisdictionState = filterJurisdictionState;
	}
	
	public String getFilterJurisdictionState() {
		return filterJurisdictionState;
	}
	
	public void setFilterJurisdictionCounty(String filterJurisdictionCounty) {
		this.filterJurisdictionCounty = filterJurisdictionCounty;
	}
	
	public String getFilterJurisdictionCounty() {
		return filterJurisdictionCounty;
	}
	
	public void setFilterJurisdictionCity(String filterJurisdictionCity) {
		this.filterJurisdictionCity = filterJurisdictionCity;
	}
	
	public String getFilterJurisdictionCity() {
		return filterJurisdictionCity;
	}

	public void setFilterJurisdictionTaxCode(String filterJurisdictionTaxCode) {
		this.filterJurisdictionTaxCode = filterJurisdictionTaxCode;
	}
	
	public String getFilterJurisdictionTaxCode() {
		return filterJurisdictionTaxCode;
	}
	
	public EntityItemService getEntityItemService() {
    	return entityItemService;
    }
    
    public void setEntityItemService(EntityItemService entityItemService) {
    	this.entityItemService = entityItemService;
    }  
    
    public OptionService getOptionService() {
		return optionService;
	}

	public void setOptionService(OptionService optionService) {
		this.optionService = optionService;
	}

	public CustService getCustService() {
		return custService;
	}

	public void setCustService(CustService custService) {
		this.custService = custService;
	}
	
	public CustLocnService getCustLocnService() {
		return custLocnService;
	}

	public void setCustLocnService(CustLocnService custLocnService) {
		this.custLocnService = custLocnService;
	}

	public ListCodesService getListCodesService() {
		return listCodesService;
	}

	public void setListCodesService(ListCodesService listCodesService) {
		this.listCodesService = listCodesService;
	}
	
	public SimpleSelection getSelection() {
		return selection;
	}
	
	public void setSelection(SimpleSelection selection) {
		this.selection = selection;
	}
	
	public EntityLevelService getEntityLevelService() {
		return entityLevelService;
	}

	public void setFilterEntityItem(EntityItemDTO filterEntityItem){
		this.filterEntityItem = filterEntityItem;
	}
	
	public EntityItemDTO getFilterEntityItem(){
		return this.filterEntityItem;
	}

	public void setEntityLevelService(EntityLevelService entityLevelService) {
		this.entityLevelService = entityLevelService;
	}
	
	public void setCustDataModel(CustDataModel custDataModel) {
		this.custDataModel = custDataModel;
	}
	
	public CustDataModel getCustDataModel() {
		return custDataModel;
	}
	
	public CustLocnDataModel getCustLocnDataModel() {
		return custLocnDataModel;
	}

	public void setCustLocnDataModel(
			CustLocnDataModel custLocnDataModel) {
		this.custLocnDataModel = custLocnDataModel;
	}
	
	public loginBean getLoginBean() {
		return loginBean;
	}
	
	public void setLoginBean(loginBean loginBean) {
		this.loginBean = loginBean;
	}
	
	public ExemptionCertificateBean getExemptionCertificateBean() {
		return exemptionCertificateBean;
	}
	
	public void setExemptionCertificateBean(ExemptionCertificateBean exemptionCertificateBean) {
		this.exemptionCertificateBean = exemptionCertificateBean;
	}

	public int getSelectedCustRowIndex() {
		return selectedCustRowIndex;
	}
	
	public List<SelectItem> getFilterJurisdictionCountyMenuItems() { 
	   if (filterJurisdictionCountyMenuItems == null) {
		   filterJurisdictionCountyMenuItems = new ArrayList<SelectItem>();
		   filterJurisdictionCountyMenuItems.add(new SelectItem("",""));
		   List<String> listCounty = jurisdictionService.findTaxCodeCounty(filterJurisdictionCountry, filterJurisdictionState);
		   for (String countyname : listCounty) {
			   if(countyname!=null && countyname.length()>0){
				   filterJurisdictionCountyMenuItems.add(new SelectItem(countyname,countyname));
			   }
		   }  
	   }	    
	   return filterJurisdictionCountyMenuItems;
	}
		
	public List<SelectItem> getFilterJurisdictionCityMenuItems() { 
	   if (filterJurisdictionCityMenuItems == null) {		   
		   filterJurisdictionCityMenuItems = new ArrayList<SelectItem>();
		   filterJurisdictionCityMenuItems.add(new SelectItem("",""));
		   List<String> listCity = jurisdictionService.findTaxCodeCity(filterJurisdictionCountry, filterJurisdictionState);
		   for (String cityname : listCity) {
			   if(cityname!=null && cityname.length()>0){
				   filterJurisdictionCityMenuItems.add(new SelectItem(cityname,cityname));
			   }
		   }  
	   }	    
	   return filterJurisdictionCityMenuItems;
	}
    
    public void selectAllCheckboxChecked(ActionEvent e) throws AbortProcessingException {
		HtmlSelectBooleanCheckbox chk = (HtmlSelectBooleanCheckbox) ((HtmlAjaxSupport) e.getComponent()).getParent();
		if((Boolean) chk.getValue()) {
			selectAll = true;
		}
		else {
			selectAll = false;
		}
		
		for (EntityItem entityItem : retrieveEntityItemList) {
			entityItem.setSelectBooleanFlag(selectAll);
			if(selectAll){
				CustEntityPK aCustEntityPK = new CustEntityPK(0L, entityItem.getEntityId());
				CustEntity aCustEntity = new CustEntity();
				aCustEntity.setId(aCustEntityPK);
				updateCustEntityListChecked.put(aCustEntity.getId().getEntityId(),aCustEntity);
			}else {
				updateCustEntityListChecked.remove(entityItem.getEntityId());
			}
			}
	}
	//Fixed for 0005825: Customer Issues
	public void displayChangeChecked(ActionEvent e) {
		HtmlSelectBooleanCheckbox check = (HtmlSelectBooleanCheckbox) ((HtmlAjaxSupport) e
				.getComponent()).getParent();
		Boolean value = (Boolean) check.getValue();
		HtmlDataTable table = (HtmlDataTable) check.getParent().getParent();
		EntityItem tcd = (EntityItem) table.getRowData();
		if (value != null && value == false) {
		updateCustEntityListChecked.remove(tcd.getEntityId());
		}
	}


	public void retrieveFilterEntity() {
		//Retrieve entities
		List<EntityItem> list = entityItemService.findAllEntityItemsWithFilter(filterEntityItem.getEntityLevelId(), 
		filterEntityItem.getEntityCode().trim(), filterEntityItem.getEntityName().trim());
		selectAll = false;	
		EntityItem filter = new EntityItem();
		filter.setEntityLevelId(filterEntityItem.getEntityLevelId());
		filter.setEntityCode(filterEntityItem.getEntityCode().trim());
		filter.setEntityName(filterEntityItem.getEntityName().trim());
		entityDataModel.setCriteria(filter, buildWhereClause());
		boolean foundOne = false;
				//Fixed for 0005825: Customer Issues
			
		 for (EntityItem entityItem :retrieveEntityItemList) {
			if(entityItem.getSelectBooleanFlag()){
				foundOne = true;
				CustEntityPK aCustEntityPK = new CustEntityPK(0L, entityItem.getEntityId());
				CustEntity aCustEntity = new CustEntity();
				aCustEntity.setId(aCustEntityPK);
				updateCustEntityListChecked.put(aCustEntity.getId().getEntityId(),aCustEntity);
			}
		}
	
		for (EntityItem aEntityItem : list){
			if(updateCustEntityListChecked.get(aEntityItem.getEntityId())!=null){
				aEntityItem.setSelectBooleanFlag(true);
			}
		 }
		
				
	retrieveEntityItemList = list;
			
	}
	

	private String buildWhereClause() {
		
	String whereClause = "ENTITY_ID >= 0";
			  
		  if(filterEntityItem.getEntityLevelId()!=null && filterEntityItem.getEntityLevelId() >0 ){
			  whereClause = whereClause + " and ENTITY_LEVEL_ID = " + filterEntityItem.getEntityLevelId();
		  }
		  
		  if(filterEntityItem.getEntityCode().trim()!=null && filterEntityItem.getEntityCode().trim().length()>0){
			  whereClause = whereClause + "  and UPPER(ENTITY_CODE) like '" + filterEntityItem.getEntityCode().trim().toUpperCase() + "%'";
		  }
		  
		  if(filterEntityItem.getEntityName().trim()!=null && filterEntityItem.getEntityName().trim().length()>0){
			  whereClause = whereClause + "  and UPPER(ENTITY_NAME) like '" + filterEntityItem.getEntityName().trim().toUpperCase() + "%'";
		  }
		  
		
		return whereClause;
	}

	public void retrieveFilterCust() {
		selectedCustRowIndex = -1;
		selCust = new Cust();
		
		Cust aCust = new Cust();
		
		//Cust filter
		if(filterCust.getCustNbr()!=null && filterCust.getCustNbr().trim().length()>0){
			aCust.setCustNbr(filterCust.getCustNbr().trim());
		}
		if(filterCust.getCustName()!=null && filterCust.getCustName().trim().length()>0){
			aCust.setCustName(filterCust.getCustName().trim());
		}
		if(filterCust.getActiveFlag()!=null && filterCust.getActiveFlag().length()>0){
			if(filterCust.getActiveFlag().equals("1")){
				aCust.setActiveFlag("1");
			}
			else{
				aCust.setActiveFlag("0");
			}
		}
		
		EntityItem entityItem = entityItemService.findByCode(entityCode);
		
		//Entity filter
		if(entityItem != null && entityItem.getEntityId()!=null && entityItem.getEntityId().intValue()!=0){
			aCust.setEntityId(entityItem.getEntityId());
		}
		
		//CustLocn filter
		if(filterCustLocn.getCustLocnCode()!=null && filterCustLocn.getCustLocnCode().trim().length()>0){
			aCust.setCustLocnCode(filterCustLocn.getCustLocnCode().trim());
		}
		if(filterCustLocn.getLocationName()!=null && filterCustLocn.getLocationName().trim().length()>0){
			aCust.setCustLocnName(filterCustLocn.getLocationName().trim());
		}
		if(filterCustLocn.getActiveFlag()!=null && filterCustLocn.getActiveFlag().trim().length()>0){
			aCust.setCustLocnActiveFlag(filterCustLocn.getActiveFlag().trim());
		}
		if(filterCustLocn.getCountry()!=null && filterCustLocn.getCountry().trim().length()>0){
			aCust.setCustLocnCountry(filterCustLocn.getCountry().trim());
		}
		if(filterCustLocn.getState()!=null && filterCustLocn.getState().trim().length()>0){
			aCust.setCustLocnState(filterCustLocn.getState().trim());
		}	
		
		aCust.setLookupFlag(getLookupFlag());
		custDataModel.setCriteria(aCust);
		
		//Reset custLocn
		selectedCustLocnRowIndex = -1;
		selCustLocn = new CustLocn();
		CustLocn bNull = null;
		custLocnDataModel.setCriteria(bNull );
	}
	
	public void retrieveFilterCustLocn() {
		if(selCust==null || selCust.getCustId()==null || (selectedCustRowIndex == -1)){
			return; //Not selected
		}
		
		selectedCustLocnRowIndex = -1;
		selCustLocn = new CustLocn();
		
		CustLocn aCustLocn = new CustLocn();
		
		aCustLocn.setCustId(selCust.getCustId());
		
		if(filterCustLocn.getCustLocnCode()!=null && filterCustLocn.getCustLocnCode().trim().length()>0){
			aCustLocn.setCustLocnCode(filterCustLocn.getCustLocnCode().trim());
		}
		if(filterCustLocn.getLocationName()!=null && filterCustLocn.getLocationName().trim().length()>0){
			aCustLocn.setLocationName(filterCustLocn.getLocationName().trim());
		}
		if(filterCustLocn.getActiveFlag()!=null && filterCustLocn.getActiveFlag().trim().length()>0){
			aCustLocn.setActiveFlag(filterCustLocn.getActiveFlag().trim());
		}
		if(filterCustLocn.getCountry()!=null && filterCustLocn.getCountry().trim().length()>0){
			aCustLocn.setCountry(filterCustLocn.getCountry().trim());
		}
		if(filterCustLocn.getState()!=null && filterCustLocn.getState().trim().length()>0){
			aCustLocn.setState(filterCustLocn.getState().trim());
		}

		custLocnDataModel.setCriteria(aCustLocn);
	}

	public void selectedCustRowChanged(ActionEvent e) throws AbortProcessingException {
		HtmlDataTable table = (HtmlDataTable) ((HtmlAjaxSupport) e.getComponent()).getParent(); 
		setSelectedCustRowIndex(table.getRowIndex());

		selCust = new Cust();
		selCust = (Cust)table.getRowData();
		
		retrieveFilterCustLocn();
	}
	
	public void selectedCustLocnRowChanged(ActionEvent e) throws AbortProcessingException {
		HtmlDataTable table = (HtmlDataTable) ((HtmlAjaxSupport) e.getComponent()).getParent(); 
		setSelectedCustLocnRowIndex(table.getRowIndex());

		selCustLocn = new CustLocn();
		selCustLocn = (CustLocn)table.getRowData();
	}
	
	public String getActionText() {
		if(currentAction.equals(EditAction.ADD)){
			return "Add ";
		}
		else if(currentAction.equals(EditAction.COPY_ADD)){
			return "Copy/Add";
		}
		else if(currentAction.equals(EditAction.UPDATE)){
			return "Update";
		}
		else if(currentAction.equals(EditAction.DELETE)){
			return "Delete";
		}
		else if(currentAction.equals(EditAction.VIEW)){
			return "View";
		}

		return "";
	}
	
	public Boolean getDisplayCopyAddAction() {
		return (currentAction == EditAction.COPY_ADD);
	}
	
	public Boolean getDisplayAddAction() {
		return (currentAction == EditAction.ADD);
	}
	
	public Boolean getDisplayDeleteAction() {
		return (currentAction == EditAction.DELETE);
	}
	
	public Boolean getDisplayUpdateAction() {
		return (currentAction == EditAction.UPDATE);
	}
	
	public Boolean getDisplayViewAction() {
		return (currentAction == EditAction.VIEW);
	}
	
	public Map<Long, String >  getEntityLevelMap(){
		if(entityLevelMap==null){
			entityLevelMap = new LinkedHashMap<Long, String >();

		    List<EntityLevel> aList = entityLevelService.findAllEntityLevels();
			for (EntityLevel entityLevel : aList) {
				entityLevelMap.put(entityLevel.getEntityLevelId(), entityLevel.getDescription());
			}
	    }
		return entityLevelMap;
	}

	public List<EntityItem> getRetrieveEntityItemList() {
	 	return retrieveEntityItemList;
	 }
	
	public List<SelectItem> getEntityLevelList(){
		if(entityLevelList==null){
			entityLevelList = new ArrayList<SelectItem>();
		    List<EntityLevel> aList = entityLevelService.findAllEntityLevels();
			for (EntityLevel entityLevel : aList) {
				entityLevelList.add(new SelectItem(entityLevel.getEntityLevelId(), entityLevel.getDescription()));
			}
	    }
		return entityLevelList;
	}
	
	public SearchJurisdictionHandler getFilterHandler() {
		return filterHandler;
	}
	
	public void resetFilter() {
		filterHandler.reset();
		filterHandler.setCountry("");
		filterHandler.setState("");
	}
	
	public String okCustAction() {
		String result = null;
		switch (currentAction) {
			case DELETE:
				result = deleteCustAction();
				break;
				
			case UPDATE:
				result = updateCustAction();
				break;
				
			case ADD:
				result = addCustAction();
				break;
				
			case VIEW:
				result = "customerLocation_main";
				break;
				
			default:
				result = cancelAction();
				break;
		}
		return result;
	}
	
	public String okCustAndAddAction(){
		String result =  addCustAction();
		if(result==null || result.length()==0){
			return result;
		}
		
		this.currentAction = EditAction.ADD;
		
		updateCustLocn = new CustLocn();
		updateCustLocn.setCustId(updateCust.getCustId());
		updateCustLocn.setActiveBooleanFlag(true);
		
		//Set Jurisdiction filter
		filterHandler.setJurisdictionService(getJurisdictionService());
		filterHandler.setCacheManager(getCacheManager());
		resetFilter();
		
		return "custlocn_update";
	}
	
	public String addCustAction() {
		String result = null;
		try {
			boolean errorMessage = false;
			
			//Validate code, name
			if(updateCust.getCustNbr()==null || updateCust.getCustNbr().trim().length()==0){
				errorMessage = true;
				
				FacesMessage msg = new FacesMessage("The Customer Number is mandatory.");
		        msg.setSeverity(FacesMessage.SEVERITY_ERROR);
		        FacesContext.getCurrentInstance().addMessage(null, msg);
			}
			if(updateCust.getCustName()==null || updateCust.getCustName().trim().length()==0){
				errorMessage = true;
				
				FacesMessage msg = new FacesMessage("The Customer Name is mandatory.");
		        msg.setSeverity(FacesMessage.SEVERITY_ERROR);
		        FacesContext.getCurrentInstance().addMessage(null, msg);
			}
			
			//Check The Customer Number must be unique.
			if(updateCust.getCustNbr()!=null || updateCust.getCustNbr().trim().length()>0){
				Cust findCust = custService.getCustByNumber(updateCust.getCustNbr().trim());
				if(findCust!=null){
					errorMessage = true;
					
					FacesMessage msg = new FacesMessage("The Customer Number must be unique.");
			        msg.setSeverity(FacesMessage.SEVERITY_ERROR);
			        FacesContext.getCurrentInstance().addMessage(null, msg);
				}
			}
			
		
			//CustEntity
			List<CustEntity> updateCustEntityList = new ArrayList<CustEntity>();
			
			//At least one entity must be checked
			 List<EntityItem> list = entityItemService.findAllEntityItems();
				boolean foundOne = false;
							//Fixed for 0005825: Customer Issues
			for (EntityItem entityItem : retrieveEntityItemList) {
				if(entityItem.getSelectBooleanFlag())
				{
					foundOne = true;
					CustEntityPK aCustEntityPK = new CustEntityPK(0L, entityItem.getEntityId());
					CustEntity aCustEntity = new CustEntity();
					aCustEntity.setId(aCustEntityPK);
					updateCustEntityListChecked.put(aCustEntity.getId().getEntityId(),aCustEntity);
				}
				
				}
				for (EntityItem aEntityItem : list){
					if(updateCustEntityListChecked.get(aEntityItem.getEntityId())!=null){
						aEntityItem.setSelectBooleanFlag(true);
					    CustEntityPK aCustEntityPK = new CustEntityPK(0L, aEntityItem.getEntityId());
					    CustEntity aCustEntity = new CustEntity();
					    aCustEntity.setId(aCustEntityPK);
					    updateCustEntityList.add(aCustEntity);
					}
					
				 }
				if(!foundOne){
				errorMessage = true;
				
				FacesMessage msg = new FacesMessage("At least one Entity must be checked.");
		        msg.setSeverity(FacesMessage.SEVERITY_ERROR);
		        FacesContext.getCurrentInstance().addMessage(null, msg);
			}

			if(errorMessage){
				return "";
			}
			
			//Update Cust and CustEntity
			custService.addCustEntityList(updateCust, updateCustEntityList);

			//Refresh & Retrieve
			retrieveFilterCust();
			
		    result = "customerLocation_main";		
		} catch (Exception ex) {
			ex.printStackTrace();
			result = "";
		}
		   
        return result;
	}
	
	public String updateCustAction() {
		String result = null;
		try {
			boolean errorMessage = false;
			
			//Validate code, name
			if(updateCust.getCustNbr()==null || updateCust.getCustNbr().trim().length()==0){
				errorMessage = true;
				
				FacesMessage message = new FacesMessage("The Customer Number is mandatory.");
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage(null, message);
			}
			if(updateCust.getCustName()==null || updateCust.getCustName().trim().length()==0){
				errorMessage = true;
				
				FacesMessage message = new FacesMessage("The Customer Name is mandatory.");
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage(null, message);
			}
			

			//CustEntity
			List<CustEntity> updateCustEntityList = new ArrayList<CustEntity>();
			
			//At least one entity must be checked
			List<EntityItem> list = entityItemService.findAllEntityItems();
				boolean foundOne = false;
							//Fixed for 0005825: Customer Issues
			for (EntityItem entityItem : retrieveEntityItemList) {
				if(entityItem.getSelectBooleanFlag()){
					foundOne = true;
					CustEntityPK aCustEntityPK = new CustEntityPK(0L, entityItem.getEntityId());
					CustEntity aCustEntity = new CustEntity();
					aCustEntity.setId(aCustEntityPK);
					updateCustEntityListChecked.put(aCustEntity.getId().getEntityId(),aCustEntity);
					}
				
				}
				for (EntityItem aEntityItem : list){
					if(updateCustEntityListChecked.get(aEntityItem.getEntityId())!=null){
						aEntityItem.setSelectBooleanFlag(true);
						 CustEntityPK aCustEntityPK = new CustEntityPK(0L, aEntityItem.getEntityId());
						 CustEntity aCustEntity = new CustEntity();
						 aCustEntity.setId(aCustEntityPK);
						 updateCustEntityList.add(aCustEntity);
					}
				 }
				
			if(!foundOne){
				errorMessage = true;
				
				FacesMessage message = new FacesMessage("At least one Entity must be checked.");
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage(null, message);
			}

			if(errorMessage){
				return "";
			}

			//Update Cust
			custService.updateCustEntityList(updateCust, updateCustEntityList);

			//Refresh & Retrieve
			retrieveFilterCust();
			
		    result = "customerLocation_main";		
		} catch (Exception ex) {
			ex.printStackTrace();
			result = "";
		}
		   
        return result;
	}
	
	public String deleteCustAction() {
		String result = null;
		logger.info("start addAction"); 
		try {
	
			if(isAllowDeleteCustomer){
				//Update Cust and CustEntity
				custService.deleteCustEntityList(updateCust);
			}
			else{
				//Set cust to inactive
				custService.disableActiveCustomer(updateCust.getCustNbr());
			}
			
			isAllowDeleteCustomer = false;

			//Refresh & Retrieve
			retrieveFilterCust();
			
		    result = "customerLocation_main";		
		} catch (Exception ex) {
			ex.printStackTrace();
			result = "";
		}
		   
        return result;
	}
	
	boolean isAdminRole(){
		return (loginBean.getRole().getRoleCode().equalsIgnoreCase("ADMIN"));
	}
	
	public boolean getSelectAll(){
		return selectAll;
	}
	
	public void setSelectAll(boolean selectAll){
		this.selectAll = selectAll;
	}
	
	public String displayAddCustAction() {
		this.currentAction = EditAction.ADD;
		updateCustEntityListChecked.clear();
		retrieveEntityItemList=new ArrayList<EntityItem>();
		filterEntityItem = new EntityItemDTO();
		filterEntityItem.setEntityLevelId(0l);
		filterEntityItem.setEntityCode("");
		filterEntityItem.setEntityName("");
		
		updateCust= new Cust();		
		updateCust.setActiveBooleanFlag(true);
		
		retrieveFilterEntity();
		
		selectAll = false;

		return "cust_update";
	}
	
	public String displayViewCustAction(){
		displayUpdateCustAction();
		this.currentAction = EditAction.VIEW;
		
		return "cust_update";
	}
	
	public String displayUpdateCustAction() {
		this.currentAction = EditAction.UPDATE;
		updateCustEntityListChecked.clear();
		retrieveEntityItemList=new ArrayList<EntityItem>();
		filterEntityItem = new EntityItemDTO();
		filterEntityItem.setEntityLevelId(0l);
		filterEntityItem.setEntityCode("");
		filterEntityItem.setEntityName("");
		
		updateCust= new Cust();
		updateCust.setCustId(selCust.getCustId());
		updateCust.setCustName(selCust.getCustName());
		updateCust.setCustNbr(selCust.getCustNbr());
		updateCust.setActiveFlag(selCust.getActiveFlag());
		updateCust.setUpdateUserId(selCust.getUpdateUserId());
		updateCust.setUpdateTimestamp(selCust.getUpdateTimestamp());
		
		updateCustLocn = new CustLocn();
		//updateCustLocn.setCustId(selCust.getCustId());
		//updateCustLocn.setActiveBooleanFlag(true);
		
		BeanUtils.copyProperties(selCustLocn, updateCustLocn);
		
		//Set Jurisdiction filter
		filterHandler.setJurisdictionService(getJurisdictionService());
		filterHandler.setCacheManager(getCacheManager());
		resetFilter();
		filterHandler.setGeocode(updateCustLocn.getGeocode());
		filterHandler.setCountry(updateCustLocn.getCountry());
		filterHandler.setState(updateCustLocn.getState());
		filterHandler.setCounty(updateCustLocn.getCounty());
		filterHandler.setCity(updateCustLocn.getCity());
		filterHandler.setZip(updateCustLocn.getZip());
		updateCustLocn.setZipplus4("");
		//Set selected entities
		retrieveFilterEntity();
		List<CustEntity> custEntityList = custService.findCustEntity(updateCust.getCustId());
		
		Hashtable<Long, CustEntity> custEntityHashtable = new Hashtable<Long, CustEntity>();
		for(CustEntity aCustEntity : custEntityList) {
			custEntityHashtable.put(aCustEntity.getId().getEntityId(), aCustEntity);
		}
		
		for (EntityItem aEntityItem : retrieveEntityItemList){
			if(custEntityHashtable.get(aEntityItem.getEntityId())!=null){
				aEntityItem.setSelectBooleanFlag(true);
			}
		}
		
		return "cust_update";
	}
	
	private Boolean isAllowDeleteCustomer = false;
	
	public Boolean getIsAllowDeleteCustomer(){
		return this.isAllowDeleteCustomer;
	}
	
	public String displayDeleteCustAction() {
		displayUpdateCustAction();
		this.currentAction = EditAction.DELETE;
		
		isAllowDeleteCustomer = false;
		
		//check tb_saletrans for any cust_nbr which has been used.
		isAllowDeleteCustomer = custService.isAllowDeleteCustomer(updateCust.getCustNbr());
		
		return "cust_update";
	}
	
	public String okCustLocnAction() {
		String result = null;
		switch (currentAction) {
			case DELETE:
				result = deleteCustLocnAction();
				break;
				
			case UPDATE:
				result = updateCustLocnAction();
				break;
				
			case ADD:
				result = addCustLocnAction();
				break;
				
			case VIEW:
				result = "customerLocation_main";
				break;
				
			default:
				result = cancelAction();
				break;
		}
		return result;
	}
	
	public String displayAddCustLocnAction() {
		this.currentAction = EditAction.ADD;
		//this.nextAccepted = false;
		
		updateCust= new Cust();
		updateCust.setCustId(selCust.getCustId());
		updateCust.setCustName(selCust.getCustName());
		updateCust.setCustNbr(selCust.getCustNbr());
		
		//BeanUtils.copyProperties(selCust, updateCust);
		//updateSitusMatrixRule.setSitusMatrixId(null);
		
		updateCustLocn = new CustLocn();
		updateCustLocn.setCustId(selCust.getCustId());
		updateCustLocn.setActiveBooleanFlag(true);
		
		//Set Jurisdiction filter
		filterHandler.setJurisdictionService(getJurisdictionService());
		filterHandler.setCacheManager(getCacheManager());
		resetFilter();	
		filterHandler.setCountry(filePreferenceBean.getUserPreferenceDTO().getUserCountry());
		filterHandler.setState("");

		return "custlocn_update";
	}
	
	public String displayViewCustLocnAction(){
		displayUpdateCustLocnAction();
		this.currentAction = EditAction.VIEW;
		
		return "custlocn_update";
	}
	
	public String displayExempCertAction(){
		
		Long custId = (selCust!=null) ? selCust.getCustId() : null;
		Long custLocnId = (selCustLocn!=null) ? selCustLocn.getCustLocnId() : null;
		
		return exemptionCertificateBean.launchExemptCertFromLocation(custId, custLocnId);
	}
	
	public String displayUpdateCustLocnAction() {
		this.currentAction = EditAction.UPDATE;
		
		updateCust= new Cust();
		updateCust.setCustId(selCust.getCustId());
		updateCust.setCustName(selCust.getCustName());
		updateCust.setCustNbr(selCust.getCustNbr());
		
		updateCustLocn = new CustLocn();
		//updateCustLocn.setCustId(selCust.getCustId());
		//updateCustLocn.setActiveBooleanFlag(true);
		
		BeanUtils.copyProperties(selCustLocn, updateCustLocn);
		
		//Set Jurisdiction filter
		filterHandler.setJurisdictionService(getJurisdictionService());
		filterHandler.setCacheManager(getCacheManager());
		resetFilter();
		filterHandler.setGeocode(updateCustLocn.getGeocode());
		filterHandler.setCountry(updateCustLocn.getCountry());
		filterHandler.setState(updateCustLocn.getState());
		filterHandler.setCounty(updateCustLocn.getCounty());
		filterHandler.setCity(updateCustLocn.getCity());
		filterHandler.setZip(updateCustLocn.getZip());
		updateCustLocn.setZipplus4("");
		
		return "custlocn_update";
	}
	
	public String displayDeleteCustLocnAction() {
		displayUpdateCustLocnAction();
		this.currentAction = EditAction.DELETE;
		
		return "custlocn_update";
	}
	
	
	public String addCustLocnExAction(){
		String result = addCustLocnAction();
		if(result==null || result.length()==0){
			return result;
		}
		
		return exemptionCertificateBean.addExempFromLocation(updateCustLocn.getCustId(), updateCustLocn.getCustLocnId());

	}
	
	public String addCustLocnAction() {
		String result = null;
		logger.info("start addAction"); 
		try {
			boolean errorMessage = false;
			
			//Validate code, name
			if(updateCustLocn.getCustLocnCode()==null || updateCustLocn.getCustLocnCode().trim().length()==0){
				errorMessage = true;
				
				FacesMessage message = new FacesMessage("The Location Code is mandatory.");
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage(null, message);
			}
			if(updateCustLocn.getLocationName()==null || updateCustLocn.getLocationName().trim().length()==0){
				errorMessage = true;
				
				FacesMessage message = new FacesMessage("The Location Name is mandatory.");
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage(null, message);
			}
				
			//Validate full and partial Jurisdiction		
			boolean validJurisdiction = false;
			Jurisdiction aJurisdiction = null;
			if(filterHandler.getHasSomeInput()){
				aJurisdiction = filterHandler.getJurisdictionFromDB();
				if(aJurisdiction!=null){
					validJurisdiction = true;
				}
				else{
					validJurisdiction = filterHandler.getIsValidPartialJurisdiction();
				}
				
				if(!validJurisdiction){
					errorMessage = true;
					
					FacesMessage message = new FacesMessage("An Invalid Jurisdiction was entered.");
					message.setSeverity(FacesMessage.SEVERITY_ERROR);
					FacesContext.getCurrentInstance().addMessage(null, message);
				}
			}
			
			if(errorMessage){
				return "";
			}
			
			//Update CustLocn
			updateCustLocn.setGeocode(filterHandler.getGeocode());		
			updateCustLocn.setCountry(filterHandler.getCountry());
			updateCustLocn.setState(filterHandler.getState());
			updateCustLocn.setCity(filterHandler.getCity());
			updateCustLocn.setCounty(filterHandler.getCounty());
			updateCustLocn.setZip(filterHandler.getZip());
				
			custLocnService.save(updateCustLocn);

			//Refresh & Retrieve
			retrieveFilterCustLocn();
			
		    result = "customerLocation_main";		
		} catch (Exception ex) {
			ex.printStackTrace();
			result = "";
		}
		   
        return result;
	}
	
	public String updateCustLocnAction() {
		String result = null;
		logger.info("start addAction"); 
		try {
			boolean errorMessage = false;
			
			//Validate code, name
			if(updateCustLocn.getCustLocnCode()==null || updateCustLocn.getCustLocnCode().trim().length()==0){
				errorMessage = true;
				
				FacesMessage message = new FacesMessage("The Location Code is mandatory.");
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage(null, message);
			}
			if(updateCustLocn.getLocationName()==null || updateCustLocn.getLocationName().trim().length()==0){
				errorMessage = true;
				
				FacesMessage message = new FacesMessage("The Location Name is mandatory.");
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage(null, message);
			}
			
			//Validate full and partial Jurisdiction		
			boolean validJurisdiction = false;
			Jurisdiction aJurisdiction = null;
			if(filterHandler.getHasSomeInput()){
				aJurisdiction = filterHandler.getJurisdictionFromDB();
				if(aJurisdiction!=null){
					validJurisdiction = true;
				}
				else{
					validJurisdiction = filterHandler.getIsValidPartialJurisdiction();
				}
				
				if(!validJurisdiction){
					errorMessage = true;
					
					FacesMessage message = new FacesMessage("An Invalid Jurisdiction was entered.");
					message.setSeverity(FacesMessage.SEVERITY_ERROR);
					FacesContext.getCurrentInstance().addMessage(null, message);
				}
			}
			
			if(errorMessage){
				
				return "";
			}
			
			//Update CustLocn
			updateCustLocn.setGeocode(filterHandler.getGeocode());		
			updateCustLocn.setCountry(filterHandler.getCountry());
			updateCustLocn.setState(filterHandler.getState());
			updateCustLocn.setCity(filterHandler.getCity());
			updateCustLocn.setCounty(filterHandler.getCounty());
			updateCustLocn.setZip(filterHandler.getZip());
				
			custLocnService.update(updateCustLocn);

			//Refresh & Retrieve
			retrieveFilterCustLocn();
			
		    result = "customerLocation_main";		
		} catch (Exception ex) {
			ex.printStackTrace();
			result = "";
		}
		   
        return result;
	}
	
	public String deleteCustLocnAction() {
		String result = null;
		logger.info("start addAction"); 
		try {

			custLocnService.remove(updateCustLocn);

			//Refresh & Retrieve
			retrieveFilterCustLocn();
			
		    result = "customerLocation_main";		
		} catch (Exception ex) {
			ex.printStackTrace();
			result = "";
		}
		   
        return result;
	}
	
	public String resetJurisdictionSearchAction(){
		filterJurisdictionCounty = "";
		filterJurisdictionCity = "";

		return null;
	}
	
	public String cancelAction() {
		return "customerLocation_main";
	}
	
	public Boolean getIsViewAction() {
		return currentAction.equals(EditAction.VIEW);
	}
	public Boolean getIsUpdateAction() {
		return currentAction.equals(EditAction.UPDATE);
	}

	public Boolean getDisplayCustButtons() {
		return (selectedCustRowIndex!=-1);
	}
	
	public Boolean getDisplayCustDeleteButtons() {
		return (selectedCustRowIndex!=-1 && isAdminRole());
	}
	
	public Boolean getDisplayCustLocnDeleteButtons() {
		return (selectedCustLocnRowIndex!=-1 && isAdminRole());
	}
	
	private String isCustomerInformationOpen = "true";
	private String isLocationInformationOpen = "false";
	private Boolean isHideSearchPanel = false;
	
	public Boolean getIsHideSearchPanel() {
		return isHideSearchPanel;
	}
	
	public void toggleHideSearchPanel(ActionEvent event){
		isHideSearchPanel = !isHideSearchPanel;
	}
	
	public String toggleHideSearchPanel(){
		isHideSearchPanel = !isHideSearchPanel;
		return null;
	}
	
	public String getIsCustomerInformationOpen() {
		return isCustomerInformationOpen;
	}
	
	public void toggleCollapseAll(ActionEvent event) {
		isCustomerInformationOpen = "false";
		isLocationInformationOpen = "false";
	}
	
	public String getIsLocationInformationOpen() {
		return isLocationInformationOpen;
	}
	
	public void toggleExpandAll(ActionEvent event) {
		isCustomerInformationOpen = "true";
		isLocationInformationOpen = "true";
	}
	
	public void toggleCustomerInformation(ActionEvent event) {
		if(Boolean.valueOf(isCustomerInformationOpen)){
			isCustomerInformationOpen = "false";	
		}
		else{
			isCustomerInformationOpen = "true";
		}
	}
	
	public void toggleLocationInformation(ActionEvent event) {
		if(Boolean.valueOf(isLocationInformationOpen)){
			isLocationInformationOpen = "false";	
		}
		else{
			isLocationInformationOpen = "true";
		}
	}

	public void oncollapseCustomerInformation(ActionEvent e) throws AbortProcessingException {
		isCustomerInformationOpen = "false";
	}
	
	public void onexpandCustomerInformation(ActionEvent e) throws AbortProcessingException {
		isCustomerInformationOpen = "true";
	}
	
	public Boolean getDisplayCustLocnButtons() {
		return ((selectedCustRowIndex != -1) && (selectedCustLocnRowIndex != -1));
	}
	
	public String resetFilterEntitySearchAction(){
		filterEntityItem = new EntityItemDTO();
		filterEntityItem.setEntityLevel("0");
		return null;
	}
	
	
	public String resetFilterSearchAction(){
		//clear filter on maintenance
		filterCust = new Cust();
		filterCustLocn = new CustLocn();
		
		if(filterStateMenuItems!=null) filterStateMenuItems.clear();

		filterStateMenuItems = getCacheManager().createStateItems(filterCustLocn.getCountry());
		filterCustLocn.setState("");
		entityCode=null;		
		return null;
	}

	public CacheManager getCacheManager() {
		return this.cacheManager;
	}

	public void setCacheManager(CacheManager cacheManager) {
		this.cacheManager = cacheManager;
	}   

	public FilePreferenceBackingBean getFilePreferenceBean() {
		return this.filePreferenceBean;
	}

	public void setFilePreferenceBean(FilePreferenceBackingBean filePreferenceBean) {
		this.filePreferenceBean = filePreferenceBean;
	}

	public Cust getFilterCust() {
		return filterCust;
	}

	public void setFilterCust(Cust filterCust) {
		this.filterCust = filterCust;
	}
	
	public CustLocn getFilterCustLocn() {
		return filterCustLocn;
	}

	public void setFilterCustLocn(CustLocn filterCustLocn) {
		this.filterCustLocn = filterCustLocn;
	}
	
	public Cust getUpdateCust() {
		return updateCust;
	}

	public void setUpdateCust(Cust updateCust) {
		this.updateCust = updateCust;
	}
	
	public CustLocn getUpdateCustLocn() {
		return updateCustLocn;
	}

	public void setUpdateCustLocn(CustLocn updateCustLocn) {
		this.updateCustLocn = updateCustLocn;
	}

	public void setSelectedCustRowIndex(int selectedCustRowIndex) {
		this.selectedCustRowIndex = selectedCustRowIndex;
	}
	
	public int getSelectedCustLocnRowIndex() {
		return selectedCustLocnRowIndex;
	}

	public void setSelectedCustLocnRowIndex(int selectedCustLocnRowIndex) {
		this.selectedCustLocnRowIndex = selectedCustLocnRowIndex;
	}
	
	public void sortAction(ActionEvent e) {
		custDataModel.toggleSortOrder(e.getComponent().getParent().getId());
	}
	
	public void sortLocnAction(ActionEvent e) {
		custLocnDataModel.toggleSortOrder(e.getComponent().getParent().getId());
	}
	
	public void countySelected(ActionEvent e) {
		String value = (String) ((HtmlSelectOneMenu) ((HtmlAjaxSupport) e.getComponent()).getParent()).getSubmittedValue();
		if(StringUtils.hasText(value)) {
			filterJurisdictionCity = "";
			setInputValue(cityInput, "");
		}
	}
	
	public void citySelected(ActionEvent e) {
		String value = (String) ((HtmlSelectOneMenu) ((HtmlAjaxSupport) e.getComponent()).getParent()).getSubmittedValue();
		if(StringUtils.hasText(value)) {
			filterJurisdictionCounty = "";
			setInputValue(countyInput, "");
		}
	}

	public HtmlSelectOneMenu getCountyInput() {
		return countyInput;
	}

	public void setCountyInput(HtmlSelectOneMenu countyInput) {
		this.countyInput = countyInput;
	}

	public HtmlSelectOneMenu getCityInput() {
		return cityInput;
	}

	public void setCityInput(HtmlSelectOneMenu cityInput) {
		this.cityInput = cityInput;
	}
	
	private void setInputValue(UIInput input, String value) {
		input.setLocalValueSet(false);
		input.setSubmittedValue(null);
		input.setValue(value);
	}
	public boolean getIsEntityCheck() {
		return isEntityCheck;
	}

	public void setEntityCheck(boolean isEntityCheck) {
	this.isEntityCheck = isEntityCheck;
	}

	public String getShowhide() {
		return showhide;
	}

	public void setShowhide(String showhide) {
		this.showhide = showhide;
	}

	public EntityDataModel getEntityDataModel() {
		return entityDataModel;
	}

	public void setEntityDataModel(EntityDataModel entityDataModel) {
		this.entityDataModel = entityDataModel;
	}
	
	public String getEntityCode() {
		return entityCode;
	}

	public void setEntityCode(String entityCode) {
		this.entityCode = entityCode;
	}
	public CommonCallBack getCommonCallBack() {
		return commonCallBack;
	}
	public void setCommonCallBack(CommonCallBack commonCallBack) {
		this.commonCallBack = commonCallBack;
	}
	public String getSelectAllCaption(){
		if(entityDataModel.getRowCount() <= MAX_SELECT_ALL){
			return "Select All";
		} else {
			return "Select First "+MAX_SELECT_ALL;
		}
	}
	
	public User getCurrentUser() {
		return loginBean.getUser();
   }
}
