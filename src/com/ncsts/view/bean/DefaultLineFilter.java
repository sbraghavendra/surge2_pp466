package com.ncsts.view.bean;

import java.util.List;
import javax.faces.model.SelectItem;

public interface DefaultLineFilter {
	// Default Line menu values and labels
	public static final String DEFAULT_LINE_ALL_VALUE = "2";
	public static final String DEFAULT_LINE_ALL_LABEL = "ALL matrix";
	public static final String DEFAULT_LINE_DEFAULT_VALUE = "1";
	public static final String DEFAULT_LINE_DEFAULT_LABEL = "DEFAULT matrix";
	public static final String DEFAULT_LINE_NON_DEFAULT_VALUE = "0";
	public static final String DEFAULT_LINE_NON_DEFAULT_LABEL = "NON-DEFAULT matrix";
	
	// Get menu items
	public List<SelectItem> getDefaultLineMenuItems();	
	// Get/set value
	public String getDefaultLineFilter();

	public void setDefaultLineFilter(String defaultLineFilter);
}
