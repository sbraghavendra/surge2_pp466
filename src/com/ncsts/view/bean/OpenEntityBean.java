package com.ncsts.view.bean;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.faces.context.FacesContext;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.ActionEvent;
import javax.servlet.http.HttpSession;

import org.ajax4jsf.component.html.HtmlAjaxSupport;
import org.apache.log4j.Logger;
import org.richfaces.component.html.HtmlDataTable;

import com.ncsts.common.LoggerFactory;
import com.ncsts.domain.EntityItem;
import com.ncsts.domain.EntityLevel;
import com.ncsts.domain.User;
import com.ncsts.domain.UserEntityPK;
import com.ncsts.dto.EntityItemDTO;
import com.ncsts.dto.UserDTO;
import com.ncsts.dto.UserEntityDTO;
import com.ncsts.services.EntityLevelService;
import com.ncsts.services.UserEntityService;
import com.ncsts.services.UserService;

public class OpenEntityBean {
	
	private Logger logger = LoggerFactory.getInstance().getLogger(
			OpenEntityBean.class);

	private UserEntityDTO selectedUserEntity;

	private loginBean loginBean;

	private List<UserEntityDTO> userEntityMapList = new ArrayList<UserEntityDTO>();
	private UserEntityService userEntityService;
	private UserService userService;

	private EntityLevelService entityLevelService;
	private String buDescription;
	private String tableHeader;
	
	private int selectedRowIndex = -1;
	
	private int userselectedRowIndex = -1;
	
    private boolean openEntityAction = false;
	 
	public boolean getOpenEntityAction() {
		return openEntityAction;
	}

	public List<UserEntityDTO> getUserEntityMapList() {
		HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
		UserDTO user = (UserDTO) session.getAttribute("user");
		User lastusedentityIdUser = userService.findById(user.getUserCode());
		if(userEntityMapList == null || userEntityMapList.size() == 0) {
			List<Object[]> objList = userEntityService.findForUserCode(user.getUserCode());
			int rowIndex = 0;
			if(objList !=null){
				for(Iterator<Object[]> pairs  = objList.iterator(); pairs.hasNext();){
					Object[] pair = (Object[]) pairs.next();
					UserEntityDTO userEntity = new UserEntityDTO();
					UserEntityPK userEntityPK = new UserEntityPK();
					userEntityPK.setUserCode(pair[0].toString());
					userEntity.setUserEntityPK(userEntityPK);
					userEntity.setUserName(pair[1].toString());
					userEntity.setEntity((EntityItem)pair[2]);
					userEntity.setEntityLevel((EntityLevel)pair[3]);
					userEntity.setRoleCode(pair[4].toString());
					userEntity.setRoleName(pair[5].toString());
					if(lastusedentityIdUser.getLastUsedEntityId() !=null){
						if(((EntityItem)pair[2]).getEntityId().equals(new Long(lastusedentityIdUser.getLastUsedEntityId()))) {
							userselectedRowIndex = rowIndex;
						}
					}
					userEntityMapList.add(userEntity);
					rowIndex++;
				}
			selectedRowIndex = userselectedRowIndex;
			}
		}
		return userEntityMapList;
	}
	
	public void setUserSelecteRowIndex() {
		HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
	    UserDTO user = (UserDTO) session.getAttribute("user");
		User lastusedentityIdUser = userService.findById(user.getUserCode());
		List<Object[]> objList = userEntityService.findForUserCode(user.getUserCode());
		int rowIndex = 0;
		boolean found = false;
		if(objList !=null){
			for(Iterator<Object[]> userEntityObjects  = objList.iterator(); userEntityObjects.hasNext();){
				Object[] object = (Object[]) userEntityObjects.next();
				if(lastusedentityIdUser.getLastUsedEntityId() !=null){
					if(((EntityItem)object[2]).getEntityId().equals(new Long(lastusedentityIdUser.getLastUsedEntityId()))) {
						userselectedRowIndex = rowIndex;
						found = true;
						break;
					}
				}
				rowIndex++;
			}
			if(!found){
				selectedRowIndex = -1;
			}
			else {
				selectedRowIndex = userselectedRowIndex;
			}
		
		}
	}
	
	public int getSelectedRowIndex() {
		return selectedRowIndex;
	}
	
	public void setSelectedRowIndex(int selectedRowIndex) {
		this.selectedRowIndex = selectedRowIndex;
	}

	public UserEntityDTO getSelectedUserEntity() {
		return selectedUserEntity;
	}

	public void setUserEntityMapList(List<UserEntityDTO> userEntityMapList) {
		this.userEntityMapList = userEntityMapList;
	}

	public void setSelectedUserEntity(UserEntityDTO selectedUserEntity) {
		this.selectedUserEntity = selectedUserEntity;
	}
	
	public UserService getUserService() {
    	return userService;
    }
    
    public void setUserService(UserService userService) {
    	this.userService = userService;
    }

	public UserEntityService getUserEntityService() {
    	return userEntityService;
    }
    
    public void setUserEntityService(UserEntityService userEntityService) {
    	this.userEntityService = userEntityService;
    }  
    
    public loginBean getLoginBean() {
		return loginBean;
	}

	public void setLoginBean(loginBean loginBean) {
		this.loginBean = loginBean;
	}
	
	public void selectedUserEntityRowChanged(ActionEvent event) throws AbortProcessingException { 
		logger.info("enter selectedUserEntityRowChanged");
		
		HtmlDataTable table = (HtmlDataTable) (((HtmlAjaxSupport) event.getComponent()).getParent());
		selectedUserEntity = (UserEntityDTO) table.getRowData();
		selectedRowIndex = table.getRowIndex();
		
		logger.info("selectedRowIndex: " + selectedRowIndex);
	}	
	
	public boolean isShowButtons() {
		logger.info("selectedRowIndex != -1: " + (selectedRowIndex != -1));
		return (selectedRowIndex != -1);
	}

	public String setDynamicMenus(){
		if(selectedUserEntity != null) {
			User user = userService.findById(selectedUserEntity.getUserCode());
			 user.setLastUsedEntityId(selectedUserEntity.getEntity().getEntityId());
			 userService.saveOrUpdate(user);
			 //loginBean.resetFlags);
			 loginBean.setEntityItem(selectedUserEntity.getEntity().getEntityId());
			 loginBean.setRole(selectedUserEntity.getRoleCode());
			 loginBean.setDynamicMenus(selectedUserEntity.getUserCode(), selectedUserEntity.getRoleCode(), selectedUserEntity.getEntity().getEntityId());
		}
		 return "login_success";
	 }

	public String getTableHeader() {
		/*
		buDescription = "";
		tableHeader = "";
	//	SELECT MIN(tb_entity_level.description)
	//	  FROM tb_entity, tb_entity_level, tb_driver_names
	//	 WHERE tb_entity.entity_level_id = tb_entity_level.entity_level_id
	//	   AND tb_entity_level.trans_dtl_column_name = tb_driver_names.trans_dtl_column_name
	//	   AND tb_driver_names.driver_names_code = 'T'
	//	   AND tb_driver_names.business_unit_flag = '1'
		//---> buDescription = entityLevelService.findByBU();
		if (buDescription == "") {
			buDescription = "Business Unit";
		}
		tableHeader = "Select " +  buDescription + " Entity:";
			return tableHeader;*/
		
		HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
		UserDTO user = (UserDTO) session.getAttribute("user");
		User u = userService.findById(user.getUserCode());
		
		String returnStr = "Select Entity: ";
		if(u.getLastUsedEntityId()!=null){
			EntityItemDTO entityItemDTO = userEntityService.getEntityItemDTO();

			if(entityItemDTO!=null){
				returnStr = returnStr + entityItemDTO.getEntityCode() + " - " + entityItemDTO.getEntityName();
			}
		}
		
		return returnStr;
	}
	
	public int getUserEntityMapListSize() {
		if(userEntityMapList == null || userEntityMapList.size() == 0) {
			return getUserEntityMapList().size();
		}
		return userEntityMapList.size();
	}
		
	public String getLastuserentityId() {
		HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
		UserDTO userDTO = (UserDTO) session.getAttribute("user");
		User user = userService.findById(userDTO.getUserCode());
		return user.getLastUsedEntityId().toString();
	}
	
	public String navigateAction() {
		openEntityAction = true;
		setUserSelecteRowIndex();
		return "security_user_entity";
	}
		
		
}
