/*
 * Author: Jim M. Wilson
 * Created: Aug 12, 2008
 * $Date: 2008-08-18 10:24:05 -0500 (Mon, 18 Aug 2008) $: - $Revision: 1812 $: 
 */
package com.ncsts.view.bean;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.faces.context.FacesContext;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.ActionEvent;

import org.ajax4jsf.component.html.HtmlAjaxSupport;
import org.apache.log4j.Logger;
import org.richfaces.component.html.HtmlDataTable;
import org.springframework.beans.BeanUtils;

import com.ncsts.common.CacheManager;
import com.ncsts.common.Util;
import com.ncsts.domain.BatchErrorLog;
import com.ncsts.domain.BatchMaintenance;
import com.ncsts.domain.DataDefinitionColumn;
import com.ncsts.domain.ListCodes;
import com.ncsts.dto.BatchDTO;
import com.ncsts.dto.BatchMaintenanceDTO;
import com.ncsts.dto.ErrorLogDTO;
import com.ncsts.services.BatchMaintenanceService;
import com.ncsts.jsf.model.BatchErrorDataModel;

/**
 * Manage display of batch errors
 * 
 */
public class BatchErrorsBackingBean {

  private Logger logger = Logger.getLogger(BatchErrorsBackingBean.class);
    
  private List<ErrorLogDTO> processList = new ArrayList<ErrorLogDTO>();
  private List<ErrorLogDTO> errorList = new ArrayList<ErrorLogDTO>();

  private ErrorLogDTO selectedProcess = new ErrorLogDTO();
  private ErrorLogDTO selectedError = new ErrorLogDTO();

  private BatchMaintenanceService batchMaintenanceService;
  private BatchErrorDataModel batchErrorDataModel;

  private BatchMaintenance batchMaintenance;
  private BatchErrorLog batchErrorLog;
  
  private CacheManager cacheManager;
	
  private String returnView;
	
  private int selectedProcessRowIndex = -1;
  private int selectedErrorRowIndex = -1;
  private int errorListSize=0;
  
  public BatchErrorDataModel getBatchErrorDataModel() {
	return this.batchErrorDataModel;
  }

  public void setBatchErrorDataModel(BatchErrorDataModel model) {
	this.batchErrorDataModel = model;
  }

  public void selectedProcessRowChanged(ActionEvent e) throws AbortProcessingException {
    HtmlDataTable table = (HtmlDataTable) ((HtmlAjaxSupport) e.getComponent()).getParent();
    selectedProcess = (ErrorLogDTO) table.getRowData();
    selectedProcessRowIndex = table.getRowIndex();
    //selectErrors();
    setBatchErrorLog(selectedProcess);
  }

  public void selectedErrorRowChanged(ActionEvent e) throws AbortProcessingException {
    HtmlDataTable table = (HtmlDataTable) ((HtmlAjaxSupport) e.getComponent()).getParent();
    selectedError = (ErrorLogDTO) table.getRowData();
    selectedErrorRowIndex = table.getRowIndex();
  }

  protected void selectErrors() {
  	if ((batchMaintenanceService != null) && (batchMaintenance != null)) {
  		this.errorList = batchMaintenanceService.getBatchErrors(selectedProcess);
  		// need the desc mappings for both display and saveAs
			for (ErrorLogDTO err : errorList) {
				try {
					String prop = Util.columnToProperty(err
							.getTransDtlColumnName());
					DataDefinitionColumn c = cacheManager
							.getTransactionPropertyMap().get(prop);
					if (c == null) {
						err.setTransDtlColumnDesc(err.getTransDtlColumnName());
					} else {
						err.setTransDtlColumnDesc(c.getDescription());
					}
					Map<String, ListCodes> map = cacheManager.getListCodeMap()
							.get("ERRORCODE");
					if (err.getErrorDefCode() != null) {
						err.setErrorDefDesc(map.get(err.getErrorDefCode())
								.getDescription());
						err.setErrorDefExplanation(map.get(
								err.getErrorDefCode()).getExplanation());
						err.setSevLevel(map.get(err.getErrorDefCode())
								.getSeverityLevel());
					}
					map = cacheManager.getListCodeMap().get("ERRORSEV");
					if (err.getSevLevel() != null) {
						err.setSevLevelDesc(map.get(err.getSevLevel())
								.getDescription());
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
  		selectedErrorRowIndex = -1;
  	}
  }
  
  public Boolean getViewEnabled() {
    return selectedErrorRowIndex != -1;
  }
  public Boolean getSaveAsEnabled() {
    return selectedProcessRowIndex != -1;
  }

  public String viewAction() {
    return "batch_errors_view";
  }

  public String cancelAction() {
    return returnView;
  }

  public String saveAsAction() {
    try{
      if (errorList.size() > 0) { 
        Util.writeToFile(saveAs(),FacesContext.getCurrentInstance());
      }
    }catch(IOException io){
      logger.debug("Error in saving file");
    }
    return "";
  }

  public StringBuffer saveAs() throws IOException{
    StringBuffer fos = new StringBuffer();
    String[] header = {"BATCH_ERROR_LOG_ID","BATCH_ID","PROCESS_TYPE","PROCESS_TIMESTAMP",
                       "ROW_NO", "COLUMN_NO", "ERROR_DEF_CODE", "ERROR_DEF_DESC", "IMPORT_HEADER_COLUMN",
                       "IMPORT_COLUMN_VALUE", "TRANS_DTL_COLUMN_NAME", "TRANS_DTL_COLUMN_DESC", 
                       "TRANS_DTL_DATATYPE", "IMPORT_ROW"};

    for (String s : header) {
      fos.append(s); fos.append("\t");
    }
    fos.append("\n");
			 
    for(ErrorLogDTO detail: errorList){
      fos.append(detail.toString());
      fos.append("\n");
    }
    return fos;
  }
  /**
   * @return the errorList
   */
  public List<ErrorLogDTO> getErrorList() {
	  if((errorList==null || errorList.size()==0) && selectedProcess!=null){
		  selectErrors();
	  }
	  errorListSize=errorList.size();
    return this.errorList;
  }

  /**
   * @param errorList the errorList to set
   */
  public void setErrorList(List<ErrorLogDTO> errorList) {
    this.errorList = errorList;
  }

  /**
   * @return the processList
   */
  public List<ErrorLogDTO> getProcessList() {
    return this.processList;
  }

  /**
   * @param processList the processList to set
   */
  public void setProcessList(List<ErrorLogDTO> processList) {
    this.processList = processList;
  }

  /**
   * @return the selectedError
   */
  public ErrorLogDTO getSelectedError() {
    return this.selectedError;
  }

  /**
   * @param selectedError the selectedError to set
   */
  public void setSelectedError(ErrorLogDTO selectedError) {
    this.selectedError = selectedError;
  }

  /**
   * @return the batchMaintenanceService
   */
  public BatchMaintenanceService getBatchMaintenanceService() {
    return this.batchMaintenanceService;
  }

  /**
   * @param batchMaintenanceService the batchMaintenanceService to set
   */
  public void setBatchMaintenanceService(BatchMaintenanceService batchMaintenanceService) {
    this.batchMaintenanceService = batchMaintenanceService;
  }

  /**
   * @return the returnView
   */
  public String getReturnView() {
    return this.returnView;
  }

  /**
   * @param returnView the returnView to set
   */
  public void setReturnView(String returnView) {
    this.returnView = returnView;
  }

  /**
   * @return the batchMaintenance
   */
  public BatchMaintenance getBatchMaintenance() {
    return this.batchMaintenance;
  }

  /**
   * @param batchMaintenance the batchMaintenance to set
   */
  public void setBatchMaintenance(BatchDTO batch) {
  	BatchMaintenance bm = new BatchMaintenance();
  	BeanUtils.copyProperties(batch, bm);
  	setBatchMaintenance(bm);
  }
  
  public void setBatchMaintenance(BatchMaintenance batchMaintenance) {
    this.batchMaintenance = batchMaintenance;
    if ((batchMaintenanceService != null) && (batchMaintenance != null)) {
      this.processList= batchMaintenanceService.getBatchProcesses(batchMaintenance.getBatchId());
      if ((processList != null) && (processList.size() > 0)) {
      	selectedProcess = processList.get(0);
      	
      	//0001701: Import errors make the system crash
      	//Data Model already handle it.
      	selectedErrorRowIndex = -1;
      	//selectErrors();
        selectedProcessRowIndex = 0;
        this.errorList=null;
        setBatchErrorLog(selectedProcess);
      } else {
        selectedProcessRowIndex = -1;
        selectedErrorRowIndex = -1;

        //PP-155
        selectedProcess = new ErrorLogDTO();
        selectedProcess.setBatchId(0L);
        this.errorList=null;
        setBatchErrorLog(selectedProcess);
      }
    }
  }
  public void setBatchMaintenance(BatchMaintenanceDTO batchMaintenance) {
  	BatchMaintenance bm = new BatchMaintenance();
  	BeanUtils.copyProperties(batchMaintenance, bm);
  	setBatchMaintenance(bm);
  }

	/**
	 * @return the selectedProcessRowIndex
	 */
	public int getSelectedProcessRowIndex() {
		return this.selectedProcessRowIndex;
	}

	/**
	 * @param selectedProcessRowIndex the selectedProcessRowIndex to set
	 */
	public void setSelectedProcessRowIndex(int selectedProcessRowIndex) {
		this.selectedProcessRowIndex = selectedProcessRowIndex;
	}

	/**
	 * @return the selectedErrorRowIndex
	 */
	public int getSelectedErrorRowIndex() {
		return this.selectedErrorRowIndex;
	}

	/**
	 * @param selectedErrorRowIndex the selectedErrorRowIndex to set
	 */
	public void setSelectedErrorRowIndex(int selectedErrorRowIndex) {
		this.selectedErrorRowIndex = selectedErrorRowIndex;
	}

	/**
	 * @return the cacheManager
	 */
	public CacheManager getCacheManager() {
		return this.cacheManager;
	}

	/**
	 * @param cacheManager the cacheManager to set
	 */
	public void setCacheManager(CacheManager cacheManager) {
		this.cacheManager = cacheManager;
	}

	public int getErrorListSize() {
		return errorListSize;
	}

	public void setErrorListSize(int errorListSize) {
		this.errorListSize = errorListSize;
	}

	public BatchErrorLog getBatchErrorLog() {
		return batchErrorLog;
	}

	public void setBatchErrorLog(BatchErrorLog batchErrorLog) {
		this.batchErrorLog = batchErrorLog;
	}

	public void setBatchErrorLog(ErrorLogDTO errorLogDto) {
		BatchErrorLog batchErrorLog = new BatchErrorLog();
		BeanUtils.copyProperties(errorLogDto, batchErrorLog);
		//setBatchErrorLog(batchErrorLog);
		batchErrorDataModel.setBatchErrorLog(batchErrorLog);
	}
	

}
