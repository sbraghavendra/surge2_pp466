package com.ncsts.view.bean;

import java.text.DateFormatSymbols;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.faces.application.FacesMessage;
import javax.faces.component.html.HtmlSelectBooleanCheckbox;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.ajax4jsf.component.html.HtmlAjaxSupport;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.richfaces.component.html.HtmlDataTable;

import com.ncsts.common.CacheManager;
import com.ncsts.domain.FisYear;
import com.ncsts.domain.User;
import com.ncsts.jsf.model.FisYearDataModel;
import com.ncsts.services.FisYearService;
import com.ncsts.view.util.TogglePanelController;

public class FiscalYearBean {

	private Logger logger = Logger.getLogger(FiscalYearBean.class);

	private TogglePanelController togglePanelController = null;
	private List<SelectItem> filterYearPeriodMenuItems;
	private List<SelectItem> firstMonthItems;
	private List<SelectItem> firstYearItems;
	private HtmlSelectOneMenu closeInput = new HtmlSelectOneMenu();
	private FisYear filterFisYear = new FisYear();
	private FisYear updateFisYear = new FisYear();
	private FisYear selFisYear = new FisYear();
	private FisYear selPeriodYear = new FisYear();
	private FisYearService fisYearService;
	private boolean closeFlagCheck = false;
	private CacheManager cacheManager;
	private int checkCount = 0;
	private int uncheckCount = 0;
	private MatrixCommonBean matrixCommonBean;
	private FisYearDataModel fisYearDataModel;
	private int selectedfisYearRowIndex = -1;
	private int selectedyearPeriodsRowIndex = -1;
	private List<FisYear> selectedFisYearList = null;
	private List<FisYear> retrievePeriodList = null;
	private List<FisYear> selectedFisYearListChecked = null;
	private boolean displayAllFields = false;
	private EditAction currentAction = EditAction.VIEW;
	private SimpleDateFormat origalFormat = new SimpleDateFormat("MMMM,yyyy");
	private SimpleDateFormat monthFormat = new SimpleDateFormat("MMMM");
	private SimpleDateFormat monFormat = new SimpleDateFormat("MMM");
	private SimpleDateFormat mmFormat = new SimpleDateFormat("MM");
	private SimpleDateFormat yyyyFormat = new SimpleDateFormat("yyyy");
	private SimpleDateFormat yyFormat = new SimpleDateFormat("yy");

	public TogglePanelController getTogglePanelController() {
		if (togglePanelController == null) {
			togglePanelController = new TogglePanelController();
			togglePanelController.addTogglePanel("FilterPanel", true);
			// togglePanelController.addTogglePanel("TaxPanel", false);
		}
		return togglePanelController;
	}

	public void setTogglePanelController(
			TogglePanelController togglePanelController) {
		this.togglePanelController = togglePanelController;
	}

	public String resetFilter() {

		return "fiscal_years";
	}

	public String updateMatrixList() {

		return "fiscal_years";
	}

	public CacheManager getCacheManager() {
		return this.cacheManager;
	}

	public void setCacheManager(CacheManager cacheManager) {
		this.cacheManager = cacheManager;
	}

	public List<SelectItem> getFilterYearPeriodMenuItems() {
		if (filterYearPeriodMenuItems == null) {
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
			selectItems.add(new SelectItem("", "All Year & Periods"));
			selectItems.add(new SelectItem("0", "Only Open"));
			selectItems.add(new SelectItem("1", "Only Closed"));
			filterYearPeriodMenuItems = selectItems;
			filterFisYear.setCloseFlag("");
		}

		return filterYearPeriodMenuItems;
	}

	public List<SelectItem> getFirstMonthItems() {
		if (firstMonthItems == null) {
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
			selectItems.add(new SelectItem("0", "January"));
			selectItems.add(new SelectItem("1", "February"));
			selectItems.add(new SelectItem("2", "March"));
			selectItems.add(new SelectItem("3", "April"));
			selectItems.add(new SelectItem("4", "May"));
			selectItems.add(new SelectItem("5", "June"));
			selectItems.add(new SelectItem("6", "July"));
			selectItems.add(new SelectItem("7", "August"));
			selectItems.add(new SelectItem("8", "September"));
			selectItems.add(new SelectItem("9", "October"));
			selectItems.add(new SelectItem("10", "November"));
			selectItems.add(new SelectItem("11", "December"));
			firstMonthItems = selectItems;
			updateFisYear.setFirstMonth("0");
		}

		return firstMonthItems;
	}
	
	public List<SelectItem> getFirstYearItems() {
		if (firstYearItems == null) {
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
			selectItems.add(new SelectItem("0", "Fiscal Year"));
			selectItems.add(new SelectItem("1", "Fiscal Year - 1"));
			firstYearItems = selectItems;
			updateFisYear.setFirstYear("0");
		}

		return firstYearItems;
	}

	public void selectedfisYearRowChanged(ActionEvent e)
			throws AbortProcessingException {
		HtmlDataTable table = (HtmlDataTable) ((HtmlAjaxSupport) e
				.getComponent()).getParent();
		setSelectedfisYearRowIndex(table.getRowIndex());
		selFisYear = new FisYear();
		selFisYear = (FisYear) table.getRowData();
		retrieveFilterPeriodDetail();
	}

	public void selectedyearPeriodsRowIndex(ActionEvent e)
			throws AbortProcessingException {
		HtmlDataTable table = (HtmlDataTable) ((HtmlAjaxSupport) e
				.getComponent()).getParent();
		setSelectedyearPeriodsRowIndex(table.getRowIndex());
		selPeriodYear = new FisYear();
		selPeriodYear = (FisYear) table.getRowData();

	}

	public boolean isDisplayAllFields() {
		return displayAllFields;
	}

	public void setDisplayAllFields(boolean displayAllFields) {
		this.displayAllFields = displayAllFields;
	}

	public void retrieveFilterPeriodDetail() {

		retrievePeriodList = null;
		selectedFisYearListChecked = new ArrayList<FisYear>();
		checkCount = 0;
		uncheckCount = 0;
		selectedFisYearList = new ArrayList<FisYear>();
		if (selFisYear == null || selFisYear.getFiscalYear() == null
				|| (selectedfisYearRowIndex == -1)) {
			return; // Not selected
		}

		List<FisYear> actualRowsList = new ArrayList<FisYear>();
		actualRowsList = fisYearService.getAllYearPeriod(selFisYear
				.getFiscalYear());

		FisYear aperiods = null;
		for (int i = 0; i < actualRowsList.size(); i++) {
			aperiods = (FisYear) actualRowsList.get(i);
			if (aperiods.getCloseFlag().equals("0")
					&& selFisYear.getSelectedYear()) {
				aperiods.setSelectedPeriod(true);
				checkCount++;
				selectedFisYearListChecked.add(aperiods);
			}

			selectedFisYearList.add(aperiods);

		}
	}
	
	public Integer getLastYearResultObject() {
		if(retrievePeriodList!=null && retrievePeriodList.size()>=1){
			return new Integer(retrievePeriodList.size() - 1);
		}
		else{
			return new Integer(-1);
		}
	}
	
	public void processAddYearResultCommand(ActionEvent event) {
		// Validate Year
		if (retrievePeriodList.size()==0 && updateFisYear.getFiscalYear() == null) {
			return;
		}
		else if (retrievePeriodList.size()==0 && updateFisYear.getFiscalYear() != null) {
			//If Fiscal Year - 1
			Long fiscalYearFilter = updateFisYear.getFiscalYear();
			if(updateFisYear.getFirstYear()!=null && updateFisYear.getFirstYear().equals("1")){
				fiscalYearFilter = fiscalYearFilter - 1;
			}
					
			FisYear aFisYear = new FisYear();
			aFisYear.setYearPeriod(fiscalYearFilter + "_01");//First one
		    retrievePeriodList.add(aFisYear);
		}
		else{
			//Get next Year/Period
			FisYear lastFisYear = retrievePeriodList.get(retrievePeriodList.size()-1);//Get the last one
			FisYear aFisYear = new FisYear();	
			
			String aYear = lastFisYear.getYearPeriod().substring(0,4);	
			String aMonth = lastFisYear.getYearPeriod().substring(5,7);	
			NumberFormat numberFormat = new DecimalFormat("00");		
			try{
				Number oldMonth = numberFormat.parse(aMonth);	
				aFisYear.setYearPeriod(aYear + "_" + numberFormat.format(oldMonth.intValue()+1));
				aFisYear.setFiscalYear(Long.valueOf(aYear));
				aFisYear.setCloseFlag("0");
			}
	    	catch(Exception e) {
	    	}
		    retrievePeriodList.add(aFisYear);    
		}

	    return;
	}

	public void processRemoveYearResultCommand(ActionEvent event) {
	    String command = (String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("command");
	    int commandInt = 0;
	    try{
	    	commandInt = Integer.parseInt(command);
	    }
	    catch(Exception e){
	    	return;
	    }
	    
	    retrievePeriodList.remove(commandInt);

	    return;
	}

	public void sortAction(ActionEvent e) {
		fisYearDataModel.toggleSortOrder(e.getComponent().getParent().getId());
	}

	public String displayAddFisYearAction() {
		this.currentAction = EditAction.ADD;

		updateFisYear = new FisYear();
		updateFisYear.setCloseFlag("0");
		updateFisYear.setFirstMonth("0");
		updateFisYear.setFirstYear("0");
		
		retrievePeriodList = new ArrayList<FisYear>();	

		return "fiscal_update";
	}

	public String displayOpenAction() {
		this.currentAction = EditAction.OPEN;
		getAllPeriodDescOpen();
		return "fiscal_update";
	}

	public void getAllPeriodDesc() {
		retrievePeriodList = new ArrayList<FisYear>();
		List<FisYear> actualRowsList = new ArrayList<FisYear>();
		actualRowsList = fisYearService.getAllPeriodDesc(selFisYear
				.getFiscalYear());
		FisYear aperiods = null;

		for (int i = 0; i < actualRowsList.size(); i++) {
			aperiods = (FisYear) actualRowsList.get(i);

			retrievePeriodList.add(aperiods);
		}

	}

	public void getAllPeriodDescClose() {
		retrievePeriodList = new ArrayList<FisYear>();
		FisYear aperiods = null;
		for (int i = 0; i < selectedFisYearListChecked.size(); i++) {
			aperiods = (FisYear) selectedFisYearListChecked.get(i);
			if(aperiods.getCloseFlag().equals("0"))
			retrievePeriodList.add(aperiods);
		}

	}
	
	public void getAllPeriodDescOpen() {
		retrievePeriodList = new ArrayList<FisYear>();
		FisYear aperiods = null;
		FisYear currentHighest = null;
		Number currentHighestMonth = null;
		Number currentMonth = null;
		for (int i = 0; i < selectedFisYearListChecked.size(); i++) {
			aperiods = (FisYear) selectedFisYearListChecked.get(i);
			if(aperiods.getCloseFlag().equals("1")){
				//Select only the highest YEAR_PERIOD
				String aMonth = aperiods.getYearPeriod().substring(5,7);	
				NumberFormat numberFormat = new DecimalFormat("00");		
				try{
					currentMonth = numberFormat.parse(aMonth);						
					if(currentHighestMonth==null || currentMonth.intValue()>currentHighestMonth.intValue()){
						currentHighestMonth = currentMonth;
						currentHighest = aperiods;
					}		
				}
		    	catch(Exception e) {
		    	}
			}
		}
		
		if(currentHighest!=null){
			retrievePeriodList.add(currentHighest);
		}
	}

	public void displayChange(ActionEvent e) {
		HtmlSelectBooleanCheckbox check = (HtmlSelectBooleanCheckbox) ((HtmlAjaxSupport) e
				.getComponent()).getParent();
		Boolean value = (Boolean) check.getValue();
		HtmlDataTable table = (HtmlDataTable) check.getParent().getParent();
		FisYear tcd = (FisYear) table.getRowData();
		if (value != null && value == true) {

			selectedFisYearListChecked.add(tcd);

			if (tcd.getCloseFlag().equals("0"))
				checkCount++;
			if (tcd.getCloseFlag().equals("1"))
				uncheckCount++;

		} else {

			selectedFisYearListChecked.remove(tcd);

			if (tcd.getCloseFlag().equals("0"))
				checkCount--;
			if (tcd.getCloseFlag().equals("1"))
				uncheckCount--;

		}

	}

	public void displayChangeFirst(ActionEvent e) {
		HtmlSelectBooleanCheckbox check1 = (HtmlSelectBooleanCheckbox) ((HtmlAjaxSupport) e
				.getComponent()).getParent();
		Boolean value = (Boolean) check1.getValue();
		if (value != null && value == false) {
			checkCount = 0;
		}

	}

	public String displayUpdateFisYearAction() {
		this.currentAction = EditAction.UPDATE;
		updateFisYear = new FisYear();
		updateFisYear.setFiscalYear(selFisYear.getFiscalYear());
		updateFisYear.setCloseFlag("0");
		updateFisYear.setDescription(selFisYear.getDescription());
		getAllPeriodDesc();
		return "fiscal_update";
	}

	public String displayDeleteFisYearAction() {
		this.currentAction = EditAction.DELETE;
		getAllPeriodDesc();
		return "fiscal_update";
	}

	public String displayCloseAction() {
		this.currentAction = EditAction.CLOSE;
		getAllPeriodDescClose();
		return "fiscal_update";
	}

	public FisYearService getFisYearService() {
		return fisYearService;
	}

	public void setFisYearService(FisYearService fisYearService) {
		this.fisYearService = fisYearService;
	}

	public HtmlSelectOneMenu getCloseInput() {
		return closeInput;
	}

	public void setCloseInput(HtmlSelectOneMenu closeInput) {
		this.closeInput = closeInput;
	}

	public void retrieveFilterfiscalYear() {
		selectedfisYearRowIndex = -1;
		checkCount = 0;
		uncheckCount = 0;
		selectedFisYearList = null;
		this.selPeriodYear.setCloseFlag("");
		this.selPeriodYear.setSelectedPeriod(false);
		FisYear afiscalYear = new FisYear();

		// fiscal year filter
		if (filterFisYear.getFiscalYear() != null
				&& filterFisYear.getFiscalYear() > 0) {
			afiscalYear.setFiscalYear(filterFisYear.getFiscalYear());

		}

		if (filterFisYear.getCloseFlag() != null
				&& filterFisYear.getCloseFlag().length() > 0) {
			if (filterFisYear.getCloseFlag().equals("1")) {
				afiscalYear.setCloseFlag("1");
			} else {
				afiscalYear.setCloseFlag("0");
			}
		}
		fisYearDataModel.setCriteria(afiscalYear);
	}

	public List<FisYear> getSelectedFisYearList() {
		return selectedFisYearList;
	}

	public List<FisYear> getRetrievePeriodList() {
		return retrievePeriodList;
	}

	public List<FisYear> getSelectedFisYearListChecked() {
		return selectedFisYearListChecked;
	}

	public FisYear getSelFisYear() {
		return selFisYear;
	}

	public void setSelFisYear(FisYear selFisYear) {
		this.selFisYear = selFisYear;
	}

	public int getSelectedfisYearRowIndex() {
		return selectedfisYearRowIndex;
	}

	public void setSelectedfisYearRowIndex(int selectedfisYearRowIndex) {
		this.selectedfisYearRowIndex = selectedfisYearRowIndex;
	}

	public int getSelectedyearPeriodsRowIndex() {
		return selectedyearPeriodsRowIndex;
	}

	public int getCheckCount() {
		return checkCount;
	}

	public void setCheckCount(int checkCount) {
		this.checkCount = checkCount;
	}

	public int getUncheckCount() {
		return uncheckCount;
	}

	public void setUncheckCount(int uncheckCount) {
		this.uncheckCount = uncheckCount;
	}

	public void setSelectedyearPeriodsRowIndex(int selectedyearPeriodsRowIndex) {
		this.selectedyearPeriodsRowIndex = selectedyearPeriodsRowIndex;
	}

	public Boolean getDisplayProcruleButtons() {
		return (selectedfisYearRowIndex != -1);
	}

	public Boolean getDisplayProcruleDeleteButtons() {
		return (selectedfisYearRowIndex != -1);
	}

	public FisYear getFilterFisYear() {
		return filterFisYear;
	}

	public void setFilterFisYear(FisYear filterFisYear) {
		this.filterFisYear = filterFisYear;
	}

	public String displayFormatupdate(String description, String period,
			int calenderMonth, int fiscalYear) {
		String descriptionPattern = this.updateFisYear.getDescription();
		Date dateR = null;
		try {
			dateR = origalFormat.parse(description);

		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String strMonth = monthFormat.format(dateR);
		String strMon = monFormat.format(dateR);
		String strMM = mmFormat.format(dateR);
		String stryyyy = yyyyFormat.format(dateR);
		String stryy = yyFormat.format(dateR);
		List<String> tokens = new ArrayList<String>();
		tokens.add("MONTH");
		tokens.add("MON");
		tokens.add("PD");
		tokens.add("YR");
		tokens.add("YEAR");
		tokens.add("YYYY");
		tokens.add("YY");
		tokens.add("MM");
		StringTokenizer st = new StringTokenizer(descriptionPattern);
		String patternString = "\\b(" + StringUtils.join(tokens, "|") + ")\\b";
		Pattern pattern = Pattern.compile(patternString);
		String token = "";
		String test = "";
		while (st.hasMoreTokens()) {
			token = st.nextToken();
			Matcher matcher = pattern.matcher(token);
			while (matcher.find()) {
				String element = matcher.group(1);
				if (element.equals("MONTH"))
					token = token.replaceFirst(element, strMonth);
				if (element.equals("MON"))
					token = token.replaceFirst(element, strMon);
				if (element.equals("MM"))
					token = token.replaceFirst(element, strMM);
				if (element.equals("YYYY"))
					token = token.replaceFirst(element, String.valueOf(fiscalYear));//Fiscal Year
				if (element.equals("YY")){
					String fiscalYearStr  = String.valueOf(fiscalYear);
					if(fiscalYearStr.length()>2){
						fiscalYearStr = fiscalYearStr.substring(fiscalYearStr.length()-2, fiscalYearStr.length());
					}
					token = token.replaceFirst(element, fiscalYearStr);//Two digit Fiscal Year					
				}

				if (element.equals("PD"))
					token = token.replaceFirst(element, period);
				if (element.equals("YEAR") && calenderMonth < 12
						&& calenderMonth != -1) {
					int year = Integer.parseInt(stryyyy) + 1;
					token = token.replaceFirst(element, Integer.toString(year));
				} else if (element.equals("YEAR")) {
					token = token.replaceFirst(element, stryyyy);
				}
				
				if (element.equals("YR") && calenderMonth < 12
						&& calenderMonth != -1) {
					int year = Integer.parseInt(stryy) + 1;
					token = token.replaceFirst(element, Integer.toString(year));
				} else if (element.equals("YR")) {
					token = token.replaceFirst(element, stryy);
				}
			}
			test = test + token + " ";

		}
		return test;

	}

	public String okFisAndAddAction() {
		String result = addCustAction();
		if (result == null || result.length() == 0) {
			return result;
		}
		
		retrievePeriodList = new ArrayList<FisYear>();	
		if (this.currentAction.equals(EditAction.ADD)
				|| this.currentAction.equals(EditAction.UPDATE)) {

			String[] months = new DateFormatSymbols().getMonths();
			NumberFormat formatter = new DecimalFormat("00");
			Long fiscalYear = this.updateFisYear.getFiscalYear();
			String firstMonth = this.updateFisYear.getFirstMonth();
			int i = 0;
			if (firstMonth == "" || firstMonth == null) {
				i = 0;
			} else {
				i = Integer.parseInt(firstMonth);
			}
			
			//If Fiscal Year - 1
			Long fiscalYearFilter = fiscalYear;
			if(updateFisYear.getFirstYear()!=null && updateFisYear.getFirstYear().equals("1")){
				fiscalYearFilter = fiscalYearFilter - 1;
			}

			int period = 0;
			int count = 0;
			int calenderMonth = -1;
			for (int mon = i; mon < months.length - 1; mon++) {
				String month = months[mon];
				FisYear aperiods = new FisYear();
				aperiods.setFiscalYear(fiscalYear);
				aperiods.setYearPeriod(fiscalYear + "_"
						+ formatter.format(++period));
				aperiods.setDescription(displayFormatupdate(month + ","
						+ fiscalYearFilter, formatter.format(period), calenderMonth, fiscalYear.intValue()));
				aperiods.setCloseFlag("0");
				retrievePeriodList.add(aperiods);

			}
			
			while (period < 12) {
				String month = months[count];
				calenderMonth = period;
				count++;
				FisYear aperiods = new FisYear();

				aperiods.setFiscalYear(fiscalYear);
				aperiods.setYearPeriod(fiscalYear + "_"
						+ formatter.format(++period));
				aperiods.setDescription(displayFormatupdate(month + ","
						+ fiscalYearFilter, formatter.format(period),
						calenderMonth, fiscalYear.intValue()));
				aperiods.setCloseFlag("0");
				retrievePeriodList.add(aperiods);
			}
			return "fiscal_update";

		}

		return "";
	}

	public String addCustAction() {
		String result = null;
		try {
			boolean errorMessage = false;

			// Validate Year, Description
			if (updateFisYear.getFiscalYear() == null) {
				errorMessage = true;

				FacesMessage msg = new FacesMessage(
						"The Fiscal Year is mandatory.");
				msg.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage(null, msg);
			}
			if (updateFisYear.getDescription() == null
					|| updateFisYear.getDescription().trim().length() == 0) {
				errorMessage = true;

				FacesMessage msg = new FacesMessage(
						"The Description is mandatory on Populate.");
				msg.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage(null, msg);
			}

			if (errorMessage) {
				return "";
			}

			result = "fiscal_years";
		} catch (Exception ex) {
			ex.printStackTrace();
			result = "";
		}

		return result;
	}

	public MatrixCommonBean getMatrixCommonBean() {
		return matrixCommonBean;
	}

	public void setMatrixCommonBean(MatrixCommonBean matrixCommonBean) {
		this.matrixCommonBean = matrixCommonBean;
	}

	public FisYearDataModel getFisYearDataModel() {
		return fisYearDataModel;
	}

	public void setFisYearDataModel(FisYearDataModel fisYearDataModel) {
		this.fisYearDataModel = fisYearDataModel;
	}

	public String okProcessTableAction() {
		String result = null;
		switch (currentAction) {
		case DELETE:
			result = deleteProcessTableAction();
			break;

		case UPDATE:
			result = updateProcessTableAction();
			break;

		case ADD:
			result = addProcessTableAction();
			break;

		case OPEN:
			result = openProcessTableAction();
			break;
		case CLOSE:
			result = closeProcessTableAction();
			break;

		default:
			result = cancelAction();
			break;
		}
		return result;
	}

	public String cancelAction() {
		return "fiscal_years";
	}

	public String openProcessTableAction() {
		String result = null;
		logger.info("start OpenAction");
		try {

			if(retrievePeriodList!=null && retrievePeriodList.size()>0){
				FisYear aFisYear = retrievePeriodList.get(0);
			
				// Update Close Period to Open Period;
				FisYear aselPeriod = new FisYear();
				aselPeriod.setId(aFisYear.getId());// For update
				aselPeriod.setFiscalYear(aFisYear.getFiscalYear());
				if (aFisYear.getCloseFlag().equals("1")) {
					aselPeriod.setCloseFlag("0");
				}
				aselPeriod.setDescription(aFisYear.getDescription());
				aselPeriod.setSelectedYear(aFisYear.getSelectedYear());
				this.fisYearService.update(aselPeriod);
			
			}
			
			// Refresh & Retrieve
			retrieveFilterfiscalYear();
			retrieveFilterPeriodDetail();

			result = "fiscal_years";
		} catch (Exception ex) {
			ex.printStackTrace();
			result = "";
		}

		return result;
	}

	public String closeProcessTableAction() {
		String result = null;
		logger.info("start CloseAction");
		try {

			// Update Open Period to Close Period;

			FisYear aselPeriod = null;
			for (int i = 0; i < retrievePeriodList.size(); i++) {
				aselPeriod = (FisYear) retrievePeriodList.get(i);
				if (aselPeriod.getCloseFlag().equals("0")) {
					aselPeriod.setCloseFlag("1");
				}
				this.fisYearService.update(aselPeriod);
			}
			// Refresh & Retrieve
			retrieveFilterfiscalYear();
			retrieveFilterPeriodDetail();
			result = "fiscal_years";

		} catch (Exception ex) {
			ex.printStackTrace();
			result = "";
		}

		return result;
	}

	boolean validateTable() {
		boolean errorMessage = false;
		
		FisYear aselPeriod = null;
		for (int i = 0; i < retrievePeriodList.size(); i++) {
			aselPeriod = (FisYear) retrievePeriodList.get(i);
			if (aselPeriod.getDescription()==null || aselPeriod.getDescription().length()==0) {
				errorMessage = true;
				FacesMessage message = new FacesMessage("A Description is mandatory.");
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage(null, message);
				return errorMessage;
			}
		}

		return errorMessage;
	}

	public String addProcessTableAction() {
		String result = null;
		try {
			if (validateTable()) {
				return "";
			}

			FisYear aselPeriod = null;
			Long fiscalYear = -1L;
			for (int i = 0; i < retrievePeriodList.size(); i++) {
				aselPeriod = (FisYear) retrievePeriodList.get(i);
				if (fisYearService.findById(aselPeriod.getYearPeriod()) != null) {
					FacesMessage message = new FacesMessage(
							"Fiscal Year already exists.");
					message.setSeverity(FacesMessage.SEVERITY_ERROR);
					FacesContext.getCurrentInstance().addMessage(null, message);
					return null;
				}
				
				//Need to get FiscalYear from Year_Period
				if(fiscalYear == -1L){
					String fiscalYearString = aselPeriod.getYearPeriod().substring(0,4);
					
					try{
						fiscalYear = Long.parseLong(fiscalYearString);
					}
			    	catch(Exception e) {
			    		FacesMessage message = new FacesMessage("Fiscal Year is not valid.");
						message.setSeverity(FacesMessage.SEVERITY_ERROR);
						FacesContext.getCurrentInstance().addMessage(null, message);
						return null;
			    	}
				}

				aselPeriod.setFiscalYear(fiscalYear);
				aselPeriod.setCloseFlag(updateFisYear.getCloseFlag());
				this.fisYearService.save(aselPeriod);
			}
			retrieveFilterfiscalYear();
			// Refresh & Retrieve
			retrieveFilterPeriodDetail();

			result = "fiscal_years";
		} catch (Exception ex) {
			ex.printStackTrace();
			result = "";
		}

		return result;
	}

	public String updateProcessTableAction() {
		String result = null;
		try {
			if (validateTable()) {
				return "";
			}

			/*
			FisYear aselPeriod = null;
			for (int i = 0; i < retrievePeriodList.size(); i++) {
				aselPeriod = (FisYear) retrievePeriodList.get(i);
				aselPeriod.setId(aselPeriod.getYearPeriod());// For update
				aselPeriod.setFiscalYear(updateFisYear.getFiscalYear());
				aselPeriod.setCloseFlag(updateFisYear.getCloseFlag());
				this.fisYearService.update(aselPeriod);
			}
			*/
			
			this.fisYearService.updateFisYear(updateFisYear.getFiscalYear(), retrievePeriodList);
			
			
			
			// Refresh & Retrieve
			retrieveFilterfiscalYear();
			retrieveFilterPeriodDetail();

			result = "fiscal_years";
		} catch (Exception ex) {
			ex.printStackTrace();
			result = "";
		}

		return result;

	}

	public String deleteProcessTableAction() {
		String result = null;
		try {

			FisYear aselPeriod = null;
			for (int i = 0; i < retrievePeriodList.size(); i++) {
				aselPeriod = (FisYear) retrievePeriodList.get(i);
				aselPeriod.setId(aselPeriod.getYearPeriod());// For update
				aselPeriod.setFiscalYear(updateFisYear.getFiscalYear());
				aselPeriod.setCloseFlag(updateFisYear.getCloseFlag());
				this.fisYearService.remove(aselPeriod);
			}
			// Refresh & Retrieve
			retrieveFilterfiscalYear();
			retrieveFilterPeriodDetail();

			result = "fiscal_years";
		} catch (Exception ex) {
			ex.printStackTrace();
			result = "";
		}

		return result;
	}

	public FisYear getSelPeriodYear() {
		return selPeriodYear;
	}

	public void setSelPeriodYear(FisYear selPeriodYear) {
		this.selPeriodYear = selPeriodYear;
	}

	public String resetFilterSearchAction() {
		filterFisYear = new FisYear();

		return null;
	}

	public String getActionText() {
		if (currentAction.equals(EditAction.ADD)) {
			return "Add";
		} else if (currentAction.equals(EditAction.UPDATE)) {
			return "Update";
		} else if (currentAction.equals(EditAction.DELETE)) {
			return "Delete";
		} else if (currentAction.equals(EditAction.VIEW)) {
			return "View";

		} else if (currentAction.equals(EditAction.OPEN)) {
			return "Open";

		} else if (currentAction.equals(EditAction.CLOSE)) {
			return "Close";
		}

		return "";
	}
	
	public String getActionOpenText() {
		//Display the highest month only
		String description = "";
		if(retrievePeriodList!=null && retrievePeriodList.size()>0){
			FisYear aFisYear = retrievePeriodList.get(0);
			
			SimpleDateFormat fullMonthFormat = new SimpleDateFormat("yyyy_MM");
			SimpleDateFormat MONTH = new SimpleDateFormat("MMMM, yyyy");
			try{
				Date date = fullMonthFormat.parse(aFisYear.getYearPeriod());
				description = MONTH.format(date);
			}
	    	catch(Exception e) {
	    	}
		}
		
		return description;
	}

	public FisYear getUpdateFisYear() {
		return updateFisYear;
	}

	public Boolean getDisplayAddAction() {
		return (currentAction == EditAction.ADD);
	}

	public Boolean getDisplayDeleteAction() {
		return (currentAction == EditAction.DELETE);
	}

	public Boolean getDisplayUpdateAction() {
		return (currentAction == EditAction.UPDATE);
	}

	public Boolean getDisplayOpenAction() {
		return (currentAction == EditAction.OPEN);
	}

	public Boolean getDisplayCloseAction() {
		return (currentAction == EditAction.CLOSE);
	}

	public boolean getDisableOnCheckedOpen() {
		boolean disable = false;
		if (checkCount > 0 && uncheckCount > 0) {
			disable = true;
		}
		if (uncheckCount > 0) {
			disable = true;
		} else {
			disable = false;
		}

		return disable;
	}

	public boolean getDisableOnCheckedClosed() {
		boolean disable = false;
		if (checkCount > 0 && uncheckCount > 0) {
			disable = true;
		}
		if (checkCount > 0) {
			disable = true;
		} else {
			disable = false;
		}

		return disable;
	}

	public boolean isCloseFlagCheck() {
		return closeFlagCheck;
	}

	public void setCloseFlagCheck(boolean closeFlagCheck) {
		this.closeFlagCheck = closeFlagCheck;
	}
	
	public User getCurrentUser() {
		return matrixCommonBean.getLoginBean().getUser();
   }

}