package com.ncsts.view.bean;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.ActionEvent;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;

import org.ajax4jsf.component.html.HtmlAjaxSupport;
import org.apache.log4j.Logger;
import org.richfaces.component.html.HtmlDataTable;

import com.ncsts.common.CacheManager;
import com.ncsts.domain.Jurisdiction;
import com.ncsts.domain.ReferenceDocument;
import com.ncsts.domain.TaxCode;
import com.ncsts.domain.TaxCodeDetail;
import com.ncsts.domain.TaxCodePK;
import com.ncsts.services.JurisdictionService;
import com.ncsts.services.ReferenceDocumentService;
import com.ncsts.services.TaxCodeDetailService;
import com.ncsts.services.TaxCodeService;
import com.ncsts.view.util.StateObservable;

public class ViewTaxCodeBean implements Observer, HttpSessionBindingListener{

	private Logger logger = Logger.getLogger(ViewTaxCodeBean.class);
	
	private StateObservable ov = null;
	private List<ReferenceDocument> docList;
	private TaxCodeDetailService taxCodeDetailService;
	private TaxCodeService taxCodeService;
	private ReferenceDocumentService referenceDocumentService;
	private JurisdictionService jurisdictionService;
	private CacheManager cacheManager;
	
	private TaxCodeDetail taxCodeDetail = null;
	private TaxCode taxCode = null;
	private ReferenceDocument selectedDocRefSource;
	private int selectedDocRefIndex = -1;
	private SearchJurisdictionHandler currentJurisdictionHandler = new SearchJurisdictionHandler();
	private Jurisdiction currentJurisdiction;
	private String callingPage = "";
	private String taxcodeCountry;
	private String taxcodeState;
	private String taxcodeType;
	private String taxcodeCode;
	private String description;
	
	public interface TaxCodeInfoCallback {
		void searchTaxCodeInfoCallback(String taxcodeCountry, String taxcodeState, String taxcodeType, String taxcodeCode, String callingPage);
	}

	public TaxCodeDetail findTaxcodeDetail() {
		TaxCodeDetail result = null;
		List<TaxCodeDetail> detailList =
			taxCodeDetailService.findTaxCodeDetailList(taxcodeCountry, taxcodeState, taxcodeType, taxcodeCode);
		if ((detailList != null) && (detailList.size() > 0)) {
			if (detailList.size() == 1) {
				result = detailList.get(0);
			} else {
				result = detailList.get(0);
			}
		} 
		else {
		}
		return result;
	}

	public TaxCode findTaxcode() {
		if(taxCodeDetail==null){
			return null;
		}

		TaxCodePK id = new TaxCodePK();
		id.setTaxcodeCode(taxcodeCode);

		TaxCode result = taxCodeDetailService.findTaxCode(id);

		return result;
	}

	public void searchJurisdiction() {
		if(taxCodeDetail==null){
			currentJurisdictionHandler.setJurisdiction(new Jurisdiction());
			return;
		}

		currentJurisdictionHandler.setJurisdictionService(getJurisdictionService());
		currentJurisdictionHandler.setCacheManager(getCacheManager());
		Long id = null;
		if (currentJurisdiction == null) {
			currentJurisdiction = new Jurisdiction();
		}

		currentJurisdictionHandler.setJurisdiction(currentJurisdiction);
	}
	
	public void update(Observable obs, Object obj) {
		if (obs == ov) {
			currentJurisdictionHandler.setChanged();
		}
	}
	
	public void valueBound(HttpSessionBindingEvent event){
		StateObservable ov = StateObservable.getInstance();
		this.ov = ov;
		this.ov.addObserver(this);
	}

	public void valueUnbound(HttpSessionBindingEvent event){	
		if(ov!=null){
			ov.deleteObserver(this);
		}
	}

	public SearchJurisdictionHandler getCurrentJurisdictionHandler() {
		return currentJurisdictionHandler;
	}

	public void setCallingPage(String callingPage){
		this.callingPage = callingPage;
	}
	
	public String getCallingPage(){
		return callingPage;
	}

	public void reset(){
		callingPage="";
		selectedDocRefIndex = -1;
		selectedDocRefSource = null;
		taxcodeCountry = "";
		taxcodeState = "";
		taxcodeType = "";
		taxcodeCode = "";
		taxCodeDetail = null;
		taxCode = null;
		currentJurisdiction = null;
	}

	public String cancelAction() {
	    return callingPage;
	}
	
	public String getTaxcodeCountry() {
		return taxcodeCountry;
	}

	public void setTaxcodeCountry(String taxcodeCountry) {
		this.taxcodeCountry = taxcodeCountry;
	}

	public String getTaxcodeState() {
		return taxcodeState;
	}

	public void setTaxcodeState(String taxcodeState) {
		this.taxcodeState = taxcodeState;
	}

	public String getTaxcodeType() {
		return taxcodeType;
	}

	public void setTaxcodeType(String taxcodeType) {
		this.taxcodeType = taxcodeType;
	}

	public String getTaxcodeCode() {
		return taxcodeCode;
	}

	public void setTaxcodeCode(String taxcodeCode) {
		this.taxcodeCode = taxcodeCode;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setTaxCode(String taxcodeCountry, String taxcodeState, String taxcodeType, String taxcodeCode){
		this.taxcodeCountry = taxcodeCountry;
		this.taxcodeState = taxcodeState;
		this.taxcodeType = taxcodeType;
		this.taxcodeCode = taxcodeCode;

		this.taxCodeDetail = findTaxcodeDetail();
		this.taxCode = findTaxcode();
		searchJurisdiction();
	}

	public TaxCodeDetail getTaxCodeDetail() {
		return taxCodeDetail;
	}

	public void setTaxCodeDetail(TaxCodeDetail taxCodeDetail) {
		this.taxCodeDetail = taxCodeDetail;
	}

	public TaxCode getTaxCode() {
		return taxCode;
	}

	public void setTaxCode(TaxCode taxCode) {
		this.taxCode = taxCode;
	}
	
	public List<ReferenceDocument> getDocList() {
		if(taxCodeDetail==null){
			return null;
		}

		Long id = taxCodeDetail.getTaxcodeDetailId();
		docList = taxCodeDetailService.getDocumentReferencesByTaxCodeId(id);

		return docList;
	}

	/* service getter and setter methods */
	public TaxCodeDetailService getTaxCodeDetailService() {
		return taxCodeDetailService;
	}

	public void setTaxCodeDetailService(TaxCodeDetailService taxCodeDetailService) {
		this.taxCodeDetailService = taxCodeDetailService;
	}

	public TaxCodeService getTaxCodeService() {
		return taxCodeService;
	}

	public void setTaxCodeService(TaxCodeService taxCodeService) {
		this.taxCodeService = taxCodeService;
	}

	public ReferenceDocumentService getReferenceDocumentService() {
		return referenceDocumentService;
	}

	public void setReferenceDocumentService(ReferenceDocumentService referenceDocumentService) {
		this.referenceDocumentService = referenceDocumentService;
	}

	public JurisdictionService getJurisdictionService() {
		return jurisdictionService;
	}

	public void setJurisdictionService(JurisdictionService jurisdictionService) {
		this.jurisdictionService = jurisdictionService;
	}

	public CacheManager getCacheManager() {
		return cacheManager;
	}

	public void setCacheManager(CacheManager cacheManager) {
		this.cacheManager = cacheManager;
	}

	public void selectedDocRefChanged(ActionEvent e) throws AbortProcessingException {
		HtmlDataTable table = (HtmlDataTable) ((HtmlAjaxSupport) e.getComponent()).getParent();
		this.selectedDocRefSource = (ReferenceDocument) table.getRowData();
		this.selectedDocRefIndex = table.getRowIndex();
	}

	public int getSelectedDocRefIndex() {
		return selectedDocRefIndex;
	}
	
	public boolean getViewEnabled() {
		return (selectedDocRefSource != null);
	}
	
	public void displayUpdateAction() {
	}

	public void displayViewAction() {

		String path = selectedDocRefSource.getUrl() + "\\" +selectedDocRefSource.getDocName() ;
		logger.debug("File path : " + path);
		logger.debug("File name : " + selectedDocRefSource.getDocName());
		try{

		File file = new File(path);
		FacesContext context = FacesContext.getCurrentInstance();
		HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();

		FileInputStream istr = new FileInputStream(path);
		BufferedInputStream bstr = new BufferedInputStream( istr );
		int contentLength = (int) file.length(); // get the file size (in bytes)
		byte[] data = new byte[contentLength]; // allocate byte array of right size
		bstr.read( data, 0, contentLength ); // read into byte array
		bstr.close();
		istr.close();

        response.setContentType("application/octet-stream");
        response.setContentLength(contentLength);
        response.setHeader("Content-disposition", "attachment; filename=\"" + selectedDocRefSource.getDocName() + "\"");
        ServletOutputStream out = response.getOutputStream();
        out.write(data);
        out.flush();
        out.close();
        context.responseComplete();
		}catch(FileNotFoundException fe){
			logger.debug("File not found");
			FacesMessage msg = new FacesMessage("Path not found");
			msg.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, msg);
		}catch(IOException e){
			logger.debug("Error reading file");
		}
	}
}
