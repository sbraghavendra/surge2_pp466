package com.ncsts.view.bean;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIInput;
import javax.faces.component.html.HtmlSelectBooleanCheckbox;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;

import org.ajax4jsf.component.html.HtmlAjaxSupport;
import org.apache.log4j.Logger;
import org.richfaces.component.html.HtmlDataTable;
import org.richfaces.model.selection.SimpleSelection;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.ncsts.common.CacheManager;
import com.ncsts.domain.ListCodes;
import com.ncsts.domain.TaxHoliday;
import com.ncsts.domain.TaxHolidayDetail;
import com.ncsts.domain.User;
import com.ncsts.dto.TaxHolidayDTO;
import com.ncsts.jsf.model.TaxHolidayDataModel;
import com.ncsts.services.JurisdictionService;
import com.ncsts.services.ListCodesService;
import com.ncsts.services.OptionService;
import com.ncsts.services.TaxCodeDetailService;
import com.ncsts.services.TaxCodeStateService;
import com.ncsts.services.TaxHolidayService;
import com.ncsts.view.util.ArithmeticUtils;

public class TaxHolidayBean /*extends JsfHelper*/ {	
	private Logger logger = Logger.getLogger(TaxHolidayBean.class);

	private TaxHoliday filterTaxHoliday = new TaxHoliday();
	private List<SelectItem> filterStateMenuItems;
	private List<SelectItem> filterCountryMenuItems;
	
	private TaxHoliday updateTaxHoliday = new TaxHoliday();
	private List<SelectItem> updateStateMenuItems;
	private List<SelectItem> updateCountryMenuItems;
	
	private String filterTaxCodeCode;
	private String filterTaxCodeDescription;
	private String filterTaxCodeDetailCountry;
	private String filterTaxCodeDetailCity;
	private String filterTaxCodeDetailStj;
	
	private List<SelectItem> filterCountyMenuItems;
	private List<SelectItem> filterCityMenuItems;
	private List<SelectItem> filterStjMenuItems;
	
	private List<SelectItem> taxabilityMenuItems;
	private List<SelectItem> taxtypeMenuItems;
	private List<SelectItem> ratetypeMenuItems;

	
	private String filterJurisdictionHolidayCode;
	private String filterJurisdictionCountry;
	private String filterJurisdictionState;
	private String filterJurisdictionTaxCode;
	private String filterJurisdictionCounty;
	private String filterJurisdictionCity;
	private String filterJurisdictionStj;
	
	private List<SelectItem> filterJurisdictionCountyMenuItems;
	private List<SelectItem> filterJurisdictionCityMenuItems;
	private List<SelectItem> filterJurisdictionStjMenuItems;
	
	private CacheManager cacheManager;
	private TaxHolidayDTO taxHolidayDTO = new TaxHolidayDTO(); //this is declared for search

	private TaxHolidayDTO taxHolidayDTONew = new TaxHolidayDTO(); //this is declared for updating location matrix line
	private List<TaxHolidayDTO> taxHolidayList = new ArrayList<TaxHolidayDTO>(); // this list is declared for displaying the table rows
	private List<TaxHolidayDTO> selectedTaxHolidayList = new ArrayList<TaxHolidayDTO>(); // this list is declared for capturing selected rows from the table
	
	private TaxHolidayDataModel taxHolidayDataModel;
	private TaxHoliday selTaxHoliday = new TaxHoliday();
	private TaxHolidayDTO selTaxHolidayDTO = new TaxHolidayDTO();

	private SimpleSelection selection = new SimpleSelection();
	private int selectedRowIndex = -1;
	private EditAction currentAction = EditAction.VIEW;
   
	private FilePreferenceBackingBean filePreferenceBean;
	private TaxHolidayService taxHolidayService;
	private ListCodesService listCodesService;
	private OptionService optionService;
	private JurisdictionService jurisdictionService;
	private TaxCodeStateService taxCodeStateService;
	private TaxCodeDetailService taxCodeDetailService;
	private Hashtable<String, TaxHolidayDetail> selectedTaxCodeHashtable = new Hashtable<String, TaxHolidayDetail>();
	private Hashtable<String, TaxHolidayDetail> selectedTaxCodeHashtableBackup = new Hashtable<String, TaxHolidayDetail>();
	
	private Hashtable<String, TaxHolidayDetail> selectedJurisdictionHashtable = new Hashtable<String, TaxHolidayDetail>();
	
	private List<TaxHolidayDetail> listTaxHolidayDetailByTaxCode = new ArrayList<TaxHolidayDetail>(); 
	private List<TaxHolidayDetail> listTaxHolidayDetailByJurisdiction = new ArrayList<TaxHolidayDetail>(); 
	
	private Boolean nextAccepted = false;
	
	private TaxHolidayDetail selectedJurisdictionDetail=null;
	private int selectedJurisdictionDetailRowIndex = -1;
	
	private boolean initialized = false;
	private HtmlSelectOneMenu countyInput = new HtmlSelectOneMenu();	
	private HtmlSelectOneMenu cityInput = new HtmlSelectOneMenu();
	private HtmlSelectOneMenu stjInput = new HtmlSelectOneMenu();
	
	@Autowired
	private loginBean loginBean;
	public TaxCodeStateService getTaxCodeStateService() {
		return taxCodeStateService;
	}

	public void setTaxCodeStateService(TaxCodeStateService taxCodeStateService) {
		this.taxCodeStateService = taxCodeStateService;
	}
	
	public JurisdictionService getJurisdictionService() {
		return jurisdictionService;
	}

	public void setJurisdictionService(JurisdictionService jurisdictionService) {
		this.jurisdictionService = jurisdictionService;
	}	

	public TaxCodeDetailService getTaxCodeDetailService() {
		return taxCodeDetailService;
	}

	public void setTaxCodeDetailService(TaxCodeDetailService taxCodeDetailService) {
		this.taxCodeDetailService = taxCodeDetailService;
	}

	public void setFilterTaxCodeCode(String filterTaxCodeCode) {
		this.filterTaxCodeCode = filterTaxCodeCode;
	}
	
	public String getFilterTaxCodeCode() {
		return filterTaxCodeCode;
	}
	
	public void setFilterTaxCodeDescription(String filterTaxCodeDescription) {
		this.filterTaxCodeDescription = filterTaxCodeDescription;
	}
	
	public String getFilterTaxCodeDescription() {
		return filterTaxCodeDescription;
	}
	
	public void setFilterTaxCodeDetailCountry(String filterTaxCodeDetailCountry) {
		this.filterTaxCodeDetailCountry = filterTaxCodeDetailCountry;
	}
	
	public String getFilterTaxCodeDetailCountry() {
		return filterTaxCodeDetailCountry;
	}
	
	public void setFilterTaxCodeDetailCity(String filterTaxCodeDetailCity) {
		this.filterTaxCodeDetailCity = filterTaxCodeDetailCity;
	}
	
	public String getFilterTaxCodeDetailCity() {
		return filterTaxCodeDetailCity;
	}
	
	public void setFilterTaxCodeDetailStj(String filterTaxCodeDetailStj) {
		this.filterTaxCodeDetailStj = filterTaxCodeDetailStj;
	}
	
	public String getFilterTaxCodeDetailStj() {
		return filterTaxCodeDetailStj;
	}
	
	public void setFilterJurisdictionHolidayCode(String filterJurisdictionHolidayCode) {
		this.filterJurisdictionHolidayCode = filterJurisdictionHolidayCode;
	}
	
	public String getFilterJurisdictionHolidayCode() {
		return filterJurisdictionHolidayCode;
	}
	
	public void setFilterJurisdictionCountry(String filterJurisdictionCountry) {
		this.filterJurisdictionCountry = filterJurisdictionCountry;
	}
	
	public String getFilterJurisdictionCountry() {
		return filterJurisdictionCountry;
	}
	
	public void setFilterJurisdictionState(String filterJurisdictionState) {
		this.filterJurisdictionState = filterJurisdictionState;
	}
	
	public String getFilterJurisdictionState() {
		return filterJurisdictionState;
	}
	
	public void setFilterJurisdictionCounty(String filterJurisdictionCounty) {
		this.filterJurisdictionCounty = filterJurisdictionCounty;
	}
	
	public String getFilterJurisdictionCounty() {
		return filterJurisdictionCounty;
	}
	
	public void setFilterJurisdictionCity(String filterJurisdictionCity) {
		this.filterJurisdictionCity = filterJurisdictionCity;
	}
	
	public String getFilterJurisdictionCity() {
		return filterJurisdictionCity;
	}

	public void setFilterJurisdictionStj(String filterJurisdictionStj) {
		this.filterJurisdictionStj = filterJurisdictionStj;
	}
	
	public String getFilterJurisdictionStj() {
		return filterJurisdictionStj;
	}
	
	public void setFilterJurisdictionTaxCode(String filterJurisdictionTaxCode) {
		this.filterJurisdictionTaxCode = filterJurisdictionTaxCode;
	}
	
	public String getFilterJurisdictionTaxCode() {
		return filterJurisdictionTaxCode;
	}
	
	public List<SelectItem> getTaxabilityMenuItems() { 
	   if (taxabilityMenuItems == null) {
		   taxabilityMenuItems = new ArrayList<SelectItem>();
		   taxabilityMenuItems.add(new SelectItem("",""));
		   
		   
		   List<ListCodes> listCodeList = cacheManager.getListCodesByType("TCTYPE");
			for (ListCodes listcodes : listCodeList) {
				if(listcodes != null && listcodes.getCodeCode()!=null && listcodes.getDescription()!=null){
					taxabilityMenuItems.add(new SelectItem(listcodes.getCodeCode().trim(), listcodes.getDescription().trim()));				
				}
			}
	   }	    
	   return taxabilityMenuItems;
	}
	
	public List<SelectItem> getTaxtypeMenuItems() { 
	   if (taxtypeMenuItems == null) {
		   taxtypeMenuItems = new ArrayList<SelectItem>();
		   taxtypeMenuItems.add(new SelectItem("",""));
		   
		   
		   List<ListCodes> listCodeList = cacheManager.getListCodesByType("TAXTYPE");
			for (ListCodes listcodes : listCodeList) {
				if(listcodes != null && listcodes.getCodeCode()!=null && listcodes.getDescription()!=null){
					taxtypeMenuItems.add(new SelectItem(listcodes.getCodeCode().trim(), listcodes.getDescription().trim()));				
				}
			}
	   }	    
	   return taxtypeMenuItems;
	}
	
	public List<SelectItem> getRatetypeMenuItems() { 
	   if (ratetypeMenuItems == null) {
		   ratetypeMenuItems = new ArrayList<SelectItem>();
		   ratetypeMenuItems.add(new SelectItem("",""));
		   
		   
		   List<ListCodes> listCodeList = cacheManager.getListCodesByType("RATETYPE");
			for (ListCodes listcodes : listCodeList) {
				if(listcodes != null && listcodes.getCodeCode()!=null && listcodes.getDescription()!=null){
					ratetypeMenuItems.add(new SelectItem(listcodes.getCodeCode().trim(), listcodes.getDescription().trim()));				
				}
			}
	   }	    
	   return ratetypeMenuItems;
	}
	
	public List<SelectItem> getFilterCountyMenuItems() { 
	   if (filterCountyMenuItems == null) {
		   filterCountyMenuItems = new ArrayList<SelectItem>();
		   filterCountyMenuItems.add(new SelectItem("",""));
		   
		   List<String> listCounty = taxCodeDetailService.findTaxCodeCounty(updateTaxHoliday.getTaxcodeCountryCode(), updateTaxHoliday.getTaxcodeStateCode());
		   for (String countyname : listCounty) {
			   if(countyname!=null && countyname.length()>0){
				   filterCountyMenuItems.add(new SelectItem(countyname,countyname));
			   }
		   }  
	   }	    
	   return filterCountyMenuItems;
	}
	
	public List<SelectItem> getFilterCityMenuItems() { 
	   if (filterCityMenuItems == null) {
		   filterCityMenuItems = new ArrayList<SelectItem>();
		   filterCityMenuItems.add(new SelectItem("",""));
		   List<String> listCity = taxCodeDetailService.findTaxCodeCity(updateTaxHoliday.getTaxcodeCountryCode(), updateTaxHoliday.getTaxcodeStateCode());
		   for (String cityname : listCity) {
			   if(cityname!=null && cityname.length()>0){
				   filterCityMenuItems.add(new SelectItem(cityname,cityname));
			   }
		   }  
	   }	    
	   return filterCityMenuItems;
	}
	
	public List<SelectItem> getFilterStjMenuItems() { 
	   if (filterStjMenuItems == null) {
		   filterStjMenuItems = new ArrayList<SelectItem>();
		   filterStjMenuItems.add(new SelectItem("",""));
		   List<String> listStj = taxCodeDetailService.findTaxCodeStj(updateTaxHoliday.getTaxcodeCountryCode(), updateTaxHoliday.getTaxcodeStateCode());
		   for (String stjname : listStj) {
			   if(stjname!=null && stjname.length()>0){
				   filterStjMenuItems.add(new SelectItem(stjname,stjname));
			   }
		   }  
	   }	    
	   return filterStjMenuItems;
	}
	
	public List<SelectItem> getFilterJurisdictionCountyMenuItems() { 
	   if (filterJurisdictionCountyMenuItems == null) {
		   filterJurisdictionCountyMenuItems = new ArrayList<SelectItem>();
		   filterJurisdictionCountyMenuItems.add(new SelectItem("",""));
		   List<String> listCounty = jurisdictionService.findTaxCodeCounty(filterJurisdictionCountry, filterJurisdictionState);
		   for (String countyname : listCounty) {
			   if(countyname!=null && countyname.length()>0){
				   filterJurisdictionCountyMenuItems.add(new SelectItem(countyname,countyname));
			   }
		   }  
	   }	    
	   return filterJurisdictionCountyMenuItems;
	}
		
	public List<SelectItem> getFilterJurisdictionCityMenuItems() { 
	   if (filterJurisdictionCityMenuItems == null) {		   
		   filterJurisdictionCityMenuItems = new ArrayList<SelectItem>();
		   filterJurisdictionCityMenuItems.add(new SelectItem("",""));
		   List<String> listCity = jurisdictionService.findTaxCodeCity(filterJurisdictionCountry, filterJurisdictionState);
		   for (String cityname : listCity) {
			   if(cityname!=null && cityname.length()>0){
				   filterJurisdictionCityMenuItems.add(new SelectItem(cityname,cityname));
			   }
		   }  
	   }	    
	   return filterJurisdictionCityMenuItems;
	}
	
	public List<SelectItem> getFilterJurisdictionStjMenuItems() { 
		   if (filterJurisdictionStjMenuItems == null) {		   
			   filterJurisdictionStjMenuItems = new ArrayList<SelectItem>();
			   filterJurisdictionStjMenuItems.add(new SelectItem("",""));
			   List<String> listCity = jurisdictionService.findLocalStj(filterJurisdictionCountry, filterJurisdictionState, null, null);
			   for (String cityname : listCity) {
				   if(cityname!=null && cityname.length()>0){
					   filterJurisdictionStjMenuItems.add(new SelectItem(cityname,cityname));
				   }
			   }  
		   }	    
		   return filterJurisdictionStjMenuItems;
		}
	
	public void filterCountryChanged(ValueChangeEvent e){
		String newCountry = (String)e.getNewValue();
		if(newCountry==null) filterTaxHoliday.setTaxcodeCountryCode("");
		else filterTaxHoliday.setTaxcodeCountryCode(newCountry);
	
		if(filterStateMenuItems!=null) filterStateMenuItems.clear();

		filterStateMenuItems = getCacheManager().createStateItems(filterTaxHoliday.getTaxcodeCountryCode());
		filterTaxHoliday.setTaxcodeStateCode("");
    }

	public List<SelectItem> getFilterCountryMenuItems() {
		if(filterCountryMenuItems==null){
			filterTaxHoliday.setTaxcodeCountryCode("");
		}
		filterCountryMenuItems = getCacheManager().createCountryItems();   
		return filterCountryMenuItems;
	}

	public List<SelectItem> getFilterStateMenuItems() { 
	   if (filterStateMenuItems == null) {
		   filterStateMenuItems = getCacheManager().createStateItems(filterTaxHoliday.getTaxcodeCountryCode());
		   filterTaxHoliday.setTaxcodeStateCode("");
	   }	    
	   return filterStateMenuItems;
	}
	
	public void updateCountryChanged(ValueChangeEvent e){
		String newCountry = (String)e.getNewValue();
		if(newCountry==null) updateTaxHoliday.setTaxcodeCountryCode("");
		else updateTaxHoliday.setTaxcodeCountryCode(newCountry);
	
		if(updateStateMenuItems!=null) updateStateMenuItems.clear();

		updateStateMenuItems = getCacheManager().createStateItems(updateTaxHoliday.getTaxcodeCountryCode());
		updateTaxHoliday.setTaxcodeStateCode("");
    }

	public List<SelectItem> getUpdateCountryMenuItems() {
		if(updateCountryMenuItems==null){
			updateTaxHoliday.setTaxcodeCountryCode("");
		}
		updateCountryMenuItems = getCacheManager().createCountryItems();   
		return updateCountryMenuItems;
	}

	public List<SelectItem> getUpdateStateMenuItems() { 
	   if (updateStateMenuItems == null) {
		   updateStateMenuItems = getCacheManager().createStateItems(updateTaxHoliday.getTaxcodeCountryCode());
		   updateTaxHoliday.setTaxcodeStateCode("");
	   }	    
	   return updateStateMenuItems;
	}
	
	public void retrieveFilterTaxHoliday() {
		selectedRowIndex = -1;
		selTaxHoliday = new TaxHoliday();
		
		TaxHoliday aTaxHoliday = new TaxHoliday();
		if(filterTaxHoliday.getEffectiveDate() != null){
			aTaxHoliday.setEffectiveDate(filterTaxHoliday.getEffectiveDate());
		}
		if(filterTaxHoliday.getExpirationDate() != null){
			aTaxHoliday.setExpirationDate(filterTaxHoliday.getExpirationDate());
		}
		if(filterTaxHoliday.getTaxHolidayCode()!=null && filterTaxHoliday.getTaxHolidayCode().trim().length()>0){
			aTaxHoliday.setTaxHolidayCode(filterTaxHoliday.getTaxHolidayCode().trim());
		}
		if(filterTaxHoliday.getDescription()!=null && filterTaxHoliday.getDescription().trim().length()>0){
			aTaxHoliday.setDescription(filterTaxHoliday.getDescription().trim());
		}
		if(filterTaxHoliday.getTaxcodeCountryCode()!=null && filterTaxHoliday.getTaxcodeCountryCode().trim().length()>0){
			aTaxHoliday.setTaxcodeCountryCode(filterTaxHoliday.getTaxcodeCountryCode().trim());
		}
		if(filterTaxHoliday.getTaxcodeStateCode()!=null && filterTaxHoliday.getTaxcodeStateCode().trim().length()>0){
			aTaxHoliday.setTaxcodeStateCode(filterTaxHoliday.getTaxcodeStateCode().trim());
		}

		taxHolidayDataModel.setCriteria(aTaxHoliday);
	}
	
	public List<TaxHolidayDetail> getListTaxHolidayDetailByTaxCode(){
		return listTaxHolidayDetailByTaxCode;
	}
	
	public List<TaxHolidayDetail> getListTaxHolidayDetailByJurisdiction(){
		return listTaxHolidayDetailByJurisdiction;
	}
	
	public String getPageTaxCodeDescription() {
		if(listTaxHolidayDetailByTaxCode.size()>0){
			return String.format("Displaying rows %d through %d (%d matching rows)", 1, listTaxHolidayDetailByTaxCode.size(), listTaxHolidayDetailByTaxCode.size());
		}
		else{
			return "";
		}
	}
	
	public String getPageJurisdictionDescription() {
		if(listTaxHolidayDetailByJurisdiction.size()>0){
			return String.format("Displaying rows %d through %d (%d matching rows)", 1, listTaxHolidayDetailByJurisdiction.size(), listTaxHolidayDetailByJurisdiction.size());
		}
		else{
			return "";
		}
	}

	public void selectedRowChanged(ActionEvent e) throws AbortProcessingException {
		HtmlDataTable table = (HtmlDataTable) ((HtmlAjaxSupport) e.getComponent()).getParent(); 
		setSelectedRowIndex(table.getRowIndex());
		selTaxHoliday = new TaxHoliday();
		
		//0003181
		//selTaxHoliday = (TaxHoliday)table.getRowData();
		selTaxHoliday.setTaxHolidayCode(new String(((TaxHoliday)table.getRowData()).getTaxHolidayCode()));
		selTaxHolidayDTO.setTaxHolidayCode(new String(selTaxHoliday.getTaxHolidayCode()));
		
		selTaxHoliday.setTaxcodeCountryCode(((TaxHoliday)table.getRowData()).getTaxcodeCountryCode());  
		selTaxHoliday.setTaxcodeStateCode(((TaxHoliday)table.getRowData()).getTaxcodeStateCode());
		selTaxHoliday.setDescription(((TaxHoliday)table.getRowData()).getDescription());  
		selTaxHoliday.setNotes(((TaxHoliday)table.getRowData()).getNotes());    
		selTaxHoliday.setEffectiveDate(((TaxHoliday)table.getRowData()).getEffectiveDate());
		selTaxHoliday.setExpirationDate(((TaxHoliday)table.getRowData()).getExpirationDate());
		selTaxHoliday.setTaxcodeTypeCode(((TaxHoliday)table.getRowData()).getTaxcodeTypeCode());  
		selTaxHoliday.setOverrideTaxtypeCode(((TaxHoliday)table.getRowData()).getOverrideTaxtypeCode()); 
		selTaxHoliday.setRatetypeCode(((TaxHoliday)table.getRowData()).getRatetypeCode());  
		selTaxHoliday.setTaxableThresholdAmt(((TaxHoliday)table.getRowData()).getTaxableThresholdAmt());
		selTaxHoliday.setTaxLimitationAmt(((TaxHoliday)table.getRowData()).getTaxLimitationAmt());
		selTaxHoliday.setCapAmt(((TaxHoliday)table.getRowData()).getCapAmt());
		selTaxHoliday.setBaseChangePct(((TaxHoliday)table.getRowData()).getBaseChangePct());
		selTaxHoliday.setSpecialRate(((TaxHoliday)table.getRowData()).getSpecialRate());
		selTaxHoliday.setExecuteFlag(((TaxHoliday)table.getRowData()).getExecuteFlag());
	}
	
	public String getActionText() {
		if(currentAction.equals(EditAction.ADD)){
			return "Add ";
		}
		else if(currentAction.equals(EditAction.COPY_ADD)){
			return "Copy/Add";
		}
		else if(currentAction.equals(EditAction.UPDATE)){
			return "Update";
		}
		else if(currentAction.equals(EditAction.DELETE)){
			return "Delete";
		}
		else if(currentAction.equals(EditAction.EXECUTE)){
			return "Execute";
		}
		else if(currentAction.equals(EditAction.VIEW)){
			return "View";
		}

		return "";
	}
	
	public Boolean isNextAccepted() {
		return nextAccepted;
	}
	
	public Boolean getDisplayAddActionAccepted() {
		return ((currentAction == EditAction.ADD) && nextAccepted);
	}
	
	public Boolean getDisplayCopyAddActionAccepted() {
		return ((currentAction == EditAction.COPY_ADD) && nextAccepted);
	}
	
	public Boolean getDisplayAddActionNotAccepted() {
		return ((currentAction == EditAction.ADD) && !nextAccepted);
	}
	
	public Boolean getDisplayExceptionAction() {
		
		return (selectedTaxCodeDetail!=null && 
				selectedTaxCodeDetail.getTaxcodeCounty()!=null && selectedTaxCodeDetail.getTaxcodeCounty().equalsIgnoreCase("*ALL") && 
				selectedTaxCodeDetail.getTaxcodeCity()!=null && selectedTaxCodeDetail.getTaxcodeCity().equalsIgnoreCase("*ALL") &&
				selectedTaxCodeDetail.getTaxcodeStj()!=null && selectedTaxCodeDetail.getTaxcodeStj().equalsIgnoreCase("*ALL") &&
				selectedTaxCodeDetail.getJurLevel()!=null && selectedTaxCodeDetail.getJurLevel().equalsIgnoreCase("*STATE") &&
				selectedTaxCodeDetail.getSelected());
	}
	
	public Boolean getDisplayCopyAddActionNotAccepted() {
		return ((currentAction == EditAction.COPY_ADD) && !nextAccepted);
	}
	
	public Boolean getDisplayCopyAddAction() {
		return (currentAction == EditAction.COPY_ADD);
	}
	
	public Boolean getDisplayDeleteAction() {
		return (currentAction == EditAction.DELETE);
	}
	
	public Boolean getDisplayUpdateAction() {
		return (currentAction == EditAction.UPDATE);
	}
	
	public Boolean getDisplayViewAction() {
		return (currentAction == EditAction.VIEW);
	}
	
	public String addAction() {
		this.currentAction = EditAction.ADD;
		this.nextAccepted = false;
		clearSelectedTaxCodeDetail();
		
		if(filterCountyMenuItems!=null)filterCountyMenuItems.clear();
		filterCountyMenuItems=null;
		
		if(filterCityMenuItems!=null)filterCityMenuItems.clear();
		filterCityMenuItems=null;
		
		if(filterStjMenuItems!=null)filterStjMenuItems.clear();
		filterStjMenuItems=null;
		
		updateTaxHoliday= new TaxHoliday();
		
		updateTaxHoliday.setEffectiveDate(new com.ncsts.view.util.DateConverter().getUserLocalDate());
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
		try {
			updateTaxHoliday.setExpirationDate(sdf.parse("12/31/9999"));
		}
		catch(ParseException e){}
		
		//Update country combo
		updateCountryMenuItems = getCacheManager().createCountryItems();   
		
		//Update state combo
		if(updateStateMenuItems!=null) updateStateMenuItems.clear();
		updateStateMenuItems = getCacheManager().createStateItems(updateTaxHoliday.getTaxcodeCountryCode());
		
		//Load all taxCodeDetail per taxHoliday code
		selectedTaxCodeHashtable.clear();
		selectedTaxCodeHashtable = new Hashtable<String, TaxHolidayDetail>();
		
		selectedTaxCodeHashtableBackup.clear();
		selectedTaxCodeHashtableBackup = new Hashtable<String, TaxHolidayDetail>();
		
		listTaxHolidayDetailByTaxCode.clear();
		listTaxHolidayDetailByTaxCode = new ArrayList<TaxHolidayDetail>();
		
		return "taxholiday_update";
	}
	
	public String copyaddAction() {
		updateAction();
		
		if(filterCountyMenuItems!=null)filterCountyMenuItems.clear();
		filterCountyMenuItems=null;
		
		if(filterCityMenuItems!=null)filterCityMenuItems.clear();
		filterCityMenuItems=null;
		
		if(filterStjMenuItems!=null)filterStjMenuItems.clear();
		filterStjMenuItems=null;

		this.currentAction = EditAction.COPY_ADD;
		this.nextAccepted = false;
		updateTaxHoliday.setTaxHolidayCode(""); // new Tax Holiday

		return "taxholiday_update";
	}
	
	public String viewAction(){
		updateAction();
		this.currentAction = EditAction.VIEW;
		return "taxholiday_update";
		
	}
	
	public String updateAction(){	 
		this.currentAction = EditAction.UPDATE;
		clearSelectedTaxCodeDetail();
		
		if(filterCountyMenuItems!=null)filterCountyMenuItems.clear();
		filterCountyMenuItems=null;
		
		if(filterCityMenuItems!=null)filterCityMenuItems.clear();
		filterCityMenuItems=null;
		
		if(filterStjMenuItems!=null)filterStjMenuItems.clear();
		filterStjMenuItems=null;
		
		updateTaxHoliday= new TaxHoliday();
		BeanUtils.copyProperties(selTaxHoliday, updateTaxHoliday);
		
		//Update country combo
		updateCountryMenuItems = getCacheManager().createCountryItems();  
		
		
		//Update state combo
		if(updateStateMenuItems!=null) updateStateMenuItems.clear();
		updateStateMenuItems = getCacheManager().createStateItems(updateTaxHoliday.getTaxcodeCountryCode());
		
		//Load all taxCodeDetail per taxHoliday code
		selectedTaxCodeHashtable.clear();
		selectedTaxCodeHashtable = new Hashtable<String, TaxHolidayDetail>();
		List<TaxHolidayDetail> storedList= taxHolidayService.findTaxHolidayDetailByTaxHolidayCode(updateTaxHoliday.getTaxHolidayCode(), "0", null);
		for (TaxHolidayDetail thd : storedList) {
			updateTaxCodeHashtable(thd);
		}
		
		//Load all taxCode per country/state
		listTaxHolidayDetailByTaxCode.clear();
		listTaxHolidayDetailByTaxCode = taxHolidayService.findTaxHolidayDetailFromTaxCode(
				updateTaxHoliday.getTaxcodeCountryCode(), updateTaxHoliday.getTaxcodeStateCode(), "", "", "", "", "");
		
		//Check if TaxHolidayDetail has been selected
		for (TaxHolidayDetail thd : listTaxHolidayDetailByTaxCode) {
			if(isSelectedTaxCodeDetail(thd)){
				thd.setSelected(true);
			}
		}

		return "taxholiday_update";
	}
	
	public void clearSelectedTaxCodeDetail(){
		selectedTaxCodeDetail=null;
		selectedTaxCodeDetailRowIndex = -1;
	}
	
	public String mapAction() {
		this.currentAction = EditAction.MAP;
		clearSelectedTaxCodeDetail();
		
		if(filterCountyMenuItems!=null)filterCountyMenuItems.clear();
		filterCountyMenuItems=null;
		
		if(filterCityMenuItems!=null)filterCityMenuItems.clear();
		filterCityMenuItems=null;
		
		if(filterStjMenuItems!=null)filterStjMenuItems.clear();
		filterStjMenuItems=null;
		
		updateTaxHoliday= new TaxHoliday();
		BeanUtils.copyProperties(selTaxHoliday, updateTaxHoliday);
		
		//Update country combo
		updateCountryMenuItems = getCacheManager().createCountryItems();   
		
		//Update state combo
		if(updateStateMenuItems!=null) updateStateMenuItems.clear();
		updateStateMenuItems = getCacheManager().createStateItems(updateTaxHoliday.getTaxcodeCountryCode());
		
		//Load all taxCodeDetail per taxHoliday code
		selectedTaxCodeHashtable.clear();
		selectedTaxCodeHashtable = new Hashtable<String, TaxHolidayDetail>();
		selectedTaxCodeHashtableBackup.clear();
		selectedTaxCodeHashtableBackup = new Hashtable<String, TaxHolidayDetail>();
		
		List<TaxHolidayDetail> storedList= taxHolidayService.findTaxHolidayDetailByTaxHolidayCode(updateTaxHoliday.getTaxHolidayCode(), "0", null);
		for (TaxHolidayDetail thd : storedList) {
			updateTaxCodeHashtable(thd);
			updateTaxCodeHashtableBackup(thd);
		}
		
		//Load all taxCode per country/state
		listTaxHolidayDetailByTaxCode.clear();
		listTaxHolidayDetailByTaxCode = taxHolidayService.findTaxHolidayDetailFromTaxCode(
				updateTaxHoliday.getTaxcodeCountryCode(), updateTaxHoliday.getTaxcodeStateCode(), "", "", "", "", "");
		
		//Check if TaxHolidayDetail has been selected
		for (TaxHolidayDetail thd : listTaxHolidayDetailByTaxCode) {
			if(isSelectedTaxCodeDetail(thd)){
				thd.setSelected(true);
			}
		}

		return "taxholiday_map";
	}

	public String deleteAction() {
		this.currentAction = EditAction.DELETE;
		clearSelectedTaxCodeDetail();
		
		if(filterCountyMenuItems!=null)filterCountyMenuItems.clear();
		filterCountyMenuItems=null;
		
		if(filterCityMenuItems!=null)filterCityMenuItems.clear();
		filterCityMenuItems=null;
		
		if(filterStjMenuItems!=null)filterStjMenuItems.clear();
		filterStjMenuItems=null;
		
		updateTaxHoliday= new TaxHoliday();
		BeanUtils.copyProperties(selTaxHoliday, updateTaxHoliday);
		
		//Update country combo
		updateCountryMenuItems = getCacheManager().createCountryItems();   
		
		//Update state combo
		if(updateStateMenuItems!=null) updateStateMenuItems.clear();
		updateStateMenuItems = getCacheManager().createStateItems(updateTaxHoliday.getTaxcodeCountryCode());
		
		//Load all taxCodeDetail per taxHoliday code
		selectedTaxCodeHashtable.clear();
		selectedTaxCodeHashtable = new Hashtable<String, TaxHolidayDetail>();
		List<TaxHolidayDetail> storedList= taxHolidayService.findTaxHolidayDetailByTaxHolidayCode(updateTaxHoliday.getTaxHolidayCode(), "0", null);
		for (TaxHolidayDetail thd : storedList) {
			updateTaxCodeHashtable(thd);
		}
		
		//Load all taxCode per country/state
		listTaxHolidayDetailByTaxCode.clear();
		listTaxHolidayDetailByTaxCode = taxHolidayService.findTaxHolidayDetailFromTaxCode(
				updateTaxHoliday.getTaxcodeCountryCode(), updateTaxHoliday.getTaxcodeStateCode(), "", "", "", "", "");
		
		//Check if TaxHolidayDetail has been selected
		for (TaxHolidayDetail thd : listTaxHolidayDetailByTaxCode) {
			if(isSelectedTaxCodeDetail(thd)){
				thd.setSelected(true);
			}
		}

		return "taxholiday_update";
	}
	
	public String executeAction() {
		this.currentAction = EditAction.EXECUTE;
		taxHolidayService.executeTaxHolidayDetails(selTaxHolidayDTO.getTaxHolidayCode());
		
		//Refresh & Retrieve
		refreshAndRetrieve();
		
		return "taxholiday_maintenance";	
	}
	
	public String resetTaxCodeSearchAction(){
		filterTaxCodeCode = "";
		filterTaxCodeDescription = "";
		filterTaxCodeDetailCountry = "";
		filterTaxCodeDetailCity = "";
		filterTaxCodeDetailStj = "";
		
		clearSelectedTaxCodeDetail();

		return null;
	}
	
	public String resetJurisdictionSearchAction(){
		filterJurisdictionCounty = "";
		filterJurisdictionCity = "";
		filterJurisdictionStj = "";

		return null;
	}
	
	public void retrieveTaxHolidayDetailByTaxCode() {
		clearSelectedTaxCodeDetail();
		
		String country = "";
		String state = "";
		String taxCodeCode = "";
		String taxCodeCodeDescription = "";
		String county = "";
		String city = "";
		String stj = "";
		
		if(updateTaxHoliday.getTaxcodeCountryCode()!=null && updateTaxHoliday.getTaxcodeCountryCode().trim().length()>0){
			country = updateTaxHoliday.getTaxcodeCountryCode().trim();
		}
		if(updateTaxHoliday.getTaxcodeStateCode()!=null && updateTaxHoliday.getTaxcodeStateCode().trim().length()>0){
			state = updateTaxHoliday.getTaxcodeStateCode().trim();
		}
		if(filterTaxCodeCode!=null && filterTaxCodeCode.trim().length()>0){
			taxCodeCode = filterTaxCodeCode.trim();
		}
		if(filterTaxCodeDescription!=null && filterTaxCodeDescription.trim().length()>0){
			taxCodeCodeDescription = filterTaxCodeDescription.trim();
		}
		if(filterTaxCodeDetailCountry!=null && filterTaxCodeDetailCountry.trim().length()>0){
			county = filterTaxCodeDetailCountry.trim();
		}
		if(filterTaxCodeDetailCity!=null && filterTaxCodeDetailCity.trim().length()>0){
			city = filterTaxCodeDetailCity.trim();
		}
		if(filterTaxCodeDetailStj!=null && filterTaxCodeDetailStj.trim().length()>0){
			stj = filterTaxCodeDetailStj.trim();
		}
		
		//Load all taxCode per country/state
		listTaxHolidayDetailByTaxCode.clear();
		listTaxHolidayDetailByTaxCode = taxHolidayService.findTaxHolidayDetailFromTaxCode(
				country, state, taxCodeCode, taxCodeCodeDescription, county, city, stj);
		
		//Check if TaxHolidayDetail has been selected
		for (TaxHolidayDetail thd : listTaxHolidayDetailByTaxCode) {
			if(isSelectedTaxCodeDetail(thd)){
				thd.setSelected(true);
			}
		}
	}
	
	public void retrieveTaxHolidayDetailByJurisdiction() {
		String country = filterJurisdictionCountry;
		String state = filterJurisdictionState;
		String taxCodeCode = filterJurisdictionTaxCode;
		String county = "";
		String city = "";
		String stj = "";
		
	
		if(filterJurisdictionCounty!=null && filterJurisdictionCounty.trim().length()>0){
			county = filterJurisdictionCounty.trim();
		}
		if(filterJurisdictionCity!=null && filterJurisdictionCity.trim().length()>0){
			city = filterJurisdictionCity.trim();
		}
		if(filterJurisdictionCounty!=null && filterJurisdictionCounty.trim().length()>0){
			county = filterJurisdictionCounty.trim();
		}
		if(filterJurisdictionStj!=null && filterJurisdictionStj.trim().length()>0){
			stj = filterJurisdictionStj.trim();
		}
		
		//Load all Jurisdiction per country/state
		listTaxHolidayDetailByJurisdiction.clear();
		listTaxHolidayDetailByJurisdiction = taxHolidayService.findTaxHolidayDetailFromJurisdiction(country, state, taxCodeCode, county, city, stj);
		
		//Check if Jurisdiction has been selected
		for (TaxHolidayDetail thd : listTaxHolidayDetailByJurisdiction) {
			if(isSelectedJurisdiction(thd)){
				thd.setSelected(true);
			}
		}
	}
	
	public void viewSelectedTaxCodeAction() {
		clearSelectedTaxCodeDetail();
		listTaxHolidayDetailByTaxCode = getSelectedTaxCodeDetail();
	}
	
	public List<TaxHolidayDetail> getSelectedTaxCodeDetail() {
		List<TaxHolidayDetail> list = new ArrayList<TaxHolidayDetail>();
		
		String key; 
		TaxHolidayDetail taxHolidayDetail = null;
		Enumeration keys = selectedTaxCodeHashtable.keys(); 
		while(keys.hasMoreElements()) { 
			key = (String) keys.nextElement(); 
			taxHolidayDetail = (TaxHolidayDetail)selectedTaxCodeHashtable.get(key);
			taxHolidayDetail.setSelected(true);
			list.add(taxHolidayDetail); 
		} 
		
		return list;
	}
	
	public void viewSelectedJurisdictionAction() {
		filterJurisdictionCounty = "";
		filterJurisdictionCity = "";
		filterJurisdictionStj = "";
		setInputValue(countyInput, "");
		setInputValue(cityInput, "");
		setInputValue(stjInput, "");
		
		listTaxHolidayDetailByJurisdiction = getSelectedJurisdictionDetail();
	}
	
	public List<TaxHolidayDetail> getSelectedJurisdictionDetail() {
		List<TaxHolidayDetail> list = new ArrayList<TaxHolidayDetail>();
		
		String key = ""; 
		TaxHolidayDetail taxHolidayDetail = null;
		Enumeration keys = selectedJurisdictionHashtable.keys(); 
		while(keys.hasMoreElements()) { 
			key = (String) keys.nextElement();
			taxHolidayDetail = (TaxHolidayDetail)selectedJurisdictionHashtable.get(key);
			taxHolidayDetail.setSelected(true);
			list.add(taxHolidayDetail); 
		} 
		
		return list;
	}
	
	public void selectedTaxCodeNoneAction() {
		clearSelectedTaxCodeDetail();
		
		//Remove select from current displayed Tax record
		for (TaxHolidayDetail thd : listTaxHolidayDetailByTaxCode) {
				thd.setSelected(false);
				updateTaxCodeHashtable(thd);
		}
	}
	public void selectedTaxCodeAllAction() {
		clearSelectedTaxCodeDetail();;
		
		//Add select from current displayed Tax record
		for (TaxHolidayDetail thd : listTaxHolidayDetailByTaxCode) {
				thd.setSelected(true);
				updateTaxCodeHashtable(thd);
		}
	}
	
	public void selectedJurisdictionNoneAction() {
		//Remove select from current displayed Tax record
		for (TaxHolidayDetail thd : listTaxHolidayDetailByJurisdiction) {
				thd.setSelected(false);
				updateJurisdictionHashtable(thd);
		}
	}
	public void selectedJurisdictionAllAction() {
		//Add select from current displayed Tax record
		for (TaxHolidayDetail thd : listTaxHolidayDetailByJurisdiction) {
				thd.setSelected(true);
				updateJurisdictionHashtable(thd);
		}
	}
	
	public boolean getDisplayAllCounties() {
		if(listTaxHolidayDetailByJurisdiction != null) {
			for (TaxHolidayDetail thd : listTaxHolidayDetailByJurisdiction) {
				if(!"*ALL".equalsIgnoreCase(thd.getTaxcodeCounty())) {
					return true;
				}
			}
		}
		
		return false;
	}
	
	public boolean getDisplayAllCities() {
		if(listTaxHolidayDetailByJurisdiction != null) {
			for (TaxHolidayDetail thd : listTaxHolidayDetailByJurisdiction) {
				if(!"*ALL".equalsIgnoreCase(thd.getTaxcodeCity())) {
					return true;
				}
			}
		}
		
		return false;
	}
	
	public boolean getDisplayAllStjs() {
		if(listTaxHolidayDetailByJurisdiction != null) {
			for (TaxHolidayDetail thd : listTaxHolidayDetailByJurisdiction) {
				if(!"*ALL".equalsIgnoreCase(thd.getTaxcodeStj())) {
					return true;
				}
			}
		}
		
		return false;
	}
	
	public void allCountiesAction() {
		if(listTaxHolidayDetailByJurisdiction != null) {
			for (TaxHolidayDetail thd : listTaxHolidayDetailByJurisdiction) {
				if(!"*ALL".equalsIgnoreCase(thd.getTaxcodeCounty())) {
					thd.setSelected(true);
					updateJurisdictionHashtable(thd);
				}
			}
		}
	}
	
	public void allCitiesAction() {
		if(listTaxHolidayDetailByJurisdiction != null) {
			for (TaxHolidayDetail thd : listTaxHolidayDetailByJurisdiction) {
				if(!"*ALL".equalsIgnoreCase(thd.getTaxcodeCity())) {
					thd.setSelected(true);
					updateJurisdictionHashtable(thd);
				}
			}
		}
	}
	
	public void allStjsAction() {
		if(listTaxHolidayDetailByJurisdiction != null) {
			for (TaxHolidayDetail thd : listTaxHolidayDetailByJurisdiction) {
				if(!"*ALL".equalsIgnoreCase(thd.getTaxcodeStj())) {
					thd.setSelected(true);
					updateJurisdictionHashtable(thd);
				}
			}
		}
	}
	
	public String nextAction(){
		String errMessage = ""; 
		if (updateTaxHoliday.getTaxHolidayCode()==null || updateTaxHoliday.getTaxHolidayCode().trim().length()==0) {
			errMessage = errMessage + "Holiday Code cannot be empty. ";
		}
		
		if (updateTaxHoliday.getTaxcodeCountryCode()==null || updateTaxHoliday.getTaxcodeCountryCode().trim().length()==0) {	
			errMessage = errMessage + "Country cannot be empty. ";
		}
		
		if (updateTaxHoliday.getTaxcodeStateCode()==null || updateTaxHoliday.getTaxcodeStateCode().trim().length()==0) {	
			errMessage = errMessage + "State cannot be empty. ";
		}
		
		if ( (currentAction.equals(EditAction.ADD) || currentAction.equals(EditAction.COPY_ADD)) && 
			(updateTaxHoliday.getTaxHolidayCode()!=null && updateTaxHoliday.getTaxHolidayCode().trim().length()>0)) {
			TaxHoliday newTaxHoliday = taxHolidayService.getTaxHolidayData(updateTaxHoliday.getTaxHolidayCode().trim().toUpperCase());
			if(newTaxHoliday!=null){
				errMessage = errMessage + "Holiday Code has already existed. ";
			}
		}
		
		if (errMessage.length()>0) {
			FacesMessage message = new FacesMessage(errMessage);
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
			return null;
		}
		
		//taxHolidayService.update(updateTaxHoliday); until OK clicked
		nextAccepted = true;
		
		if(this.currentAction == EditAction.ADD){
			//Load all taxCode per country/state
			listTaxHolidayDetailByTaxCode.clear();
			listTaxHolidayDetailByTaxCode = taxHolidayService.findTaxHolidayDetailFromTaxCode(
				updateTaxHoliday.getTaxcodeCountryCode(), updateTaxHoliday.getTaxcodeStateCode(), "", "", "", "", "");
		
			selectedTaxCodeHashtable.clear();
			selectedTaxCodeHashtable = new Hashtable<String, TaxHolidayDetail>();
		}
		else if(this.currentAction == EditAction.COPY_ADD){
			//Data already loaded.
		}
		
		//load county and city data
		if(filterCountyMenuItems!=null)filterCountyMenuItems.clear();
		filterCountyMenuItems=null;
		
		if(filterCityMenuItems!=null)filterCityMenuItems.clear();
		filterCityMenuItems=null;
		
		if(filterStjMenuItems!=null)filterStjMenuItems.clear();
		filterStjMenuItems=null;
		
		getFilterCountyMenuItems();
		getFilterCityMenuItems();
		getFilterStjMenuItems();
		
		return "taxholiday_update";
	}
	public String previousAction(){
		nextAccepted = false;
		
		if(this.currentAction == EditAction.ADD){
			listTaxHolidayDetailByTaxCode.clear();
			listTaxHolidayDetailByTaxCode = new ArrayList<TaxHolidayDetail>(); 
		
			selectedTaxCodeHashtable.clear();
			selectedTaxCodeHashtable = new Hashtable<String, TaxHolidayDetail>();
		}
		else if(this.currentAction == EditAction.COPY_ADD){
			//Load all taxCodeDetail per taxHoliday code
			selectedTaxCodeHashtable.clear();
			selectedTaxCodeHashtable = new Hashtable<String, TaxHolidayDetail>();
			List<TaxHolidayDetail> storedList= taxHolidayService.findTaxHolidayDetailByTaxHolidayCode(updateTaxHoliday.getTaxHolidayCode(), "0", null);
			for (TaxHolidayDetail thd : storedList) {
				updateTaxCodeHashtable(thd);
			}
			
			//Load all taxCode per country/state
			listTaxHolidayDetailByTaxCode.clear();
			listTaxHolidayDetailByTaxCode = taxHolidayService.findTaxHolidayDetailFromTaxCode(
					updateTaxHoliday.getTaxcodeCountryCode(), updateTaxHoliday.getTaxcodeStateCode(), "", "", "", "", "");
			
			//Check if TaxHolidayDetail has been selected
			for (TaxHolidayDetail thd : listTaxHolidayDetailByTaxCode) {
				if(isSelectedTaxCodeDetail(thd)){
					thd.setSelected(true);
				}
			}
		}
		
		return "taxholiday_update";
	}
	
	public String exceptionsAction(){
		filterJurisdictionHolidayCode = updateTaxHoliday.getTaxHolidayCode();
		filterJurisdictionCountry = updateTaxHoliday.getTaxcodeCountryCode();
		filterJurisdictionState = updateTaxHoliday.getTaxcodeStateCode();
		filterJurisdictionTaxCode = selectedTaxCodeDetail.getTaxcodeCode();
		filterJurisdictionCounty = "";
		filterJurisdictionCity = "";
		filterJurisdictionStj = "";
		setInputValue(countyInput, "");
		setInputValue(cityInput, "");
		setInputValue(stjInput, "");

		//Load selected Jurisdiction
		selectedJurisdictionHashtable.clear();
		selectedJurisdictionHashtable = new Hashtable<String, TaxHolidayDetail>();
		List<TaxHolidayDetail> storedList= taxHolidayService.findTaxHolidayDetailByTaxHolidayCode(updateTaxHoliday.getTaxHolidayCode(), "1", selectedTaxCodeDetail.getTaxcodeCode());
		for (TaxHolidayDetail thd : storedList) {
			updateJurisdictionHashtable(thd);
		}
		
		retrieveTaxHolidayDetailByJurisdiction();
		
		if(filterJurisdictionCountyMenuItems!=null)filterJurisdictionCountyMenuItems.clear();
		filterJurisdictionCountyMenuItems=null;
		
		if(filterJurisdictionCityMenuItems!=null)filterJurisdictionCityMenuItems.clear();
		filterJurisdictionCityMenuItems=null;
		
		if(filterJurisdictionStjMenuItems!=null)filterJurisdictionStjMenuItems.clear();
		filterJurisdictionStjMenuItems=null;
		
		return "taxholiday_exception";
	}
	
	public boolean verifyData(){
		boolean errMessage = false; 
		
		if (updateTaxHoliday.getTaxHolidayCode()==null || updateTaxHoliday.getTaxHolidayCode().trim().length()==0) {
			errMessage = true;
			FacesMessage message = new FacesMessage("Holiday Code cannot be empty.");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
		}
		
		if (updateTaxHoliday.getTaxcodeCountryCode()==null || updateTaxHoliday.getTaxcodeCountryCode().trim().length()==0) {	
			errMessage = true;
			FacesMessage message = new FacesMessage("Country cannot be empty.");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
		}
		
		if (updateTaxHoliday.getTaxcodeStateCode()==null || updateTaxHoliday.getTaxcodeStateCode().trim().length()==0) {	
			errMessage = true;
			FacesMessage message = new FacesMessage("State cannot be empty.");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
		}
		
		if ( (currentAction.equals(EditAction.ADD) || currentAction.equals(EditAction.COPY_ADD)) && 
			(updateTaxHoliday.getTaxHolidayCode()!=null && updateTaxHoliday.getTaxHolidayCode().trim().length()>0)) {
			TaxHoliday newTaxHoliday = taxHolidayService.getTaxHolidayData(updateTaxHoliday.getTaxHolidayCode().trim().toUpperCase());
			if(newTaxHoliday!=null){
				errMessage = true;
				FacesMessage message = new FacesMessage("Holiday Code has already existed.");
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage(null, message);
			}
		}
		
		Date effectTS = (Date)updateTaxHoliday.getEffectiveDate();
		Date exprTS = (Date)updateTaxHoliday.getExpirationDate();
		
		boolean hasDateIssue = false;
		if(effectTS==null || exprTS==null){
			if (effectTS==null) {  
				errMessage = true;
				FacesMessage message = new FacesMessage("Effective Date may not be blank.");
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage(null, message);
				hasDateIssue = true;
			}
			if (exprTS==null) {
				errMessage = true;
				FacesMessage message = new FacesMessage("Expiration Date may not be blank.");
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage(null, message);
				hasDateIssue = true;
			}
		}
	
		if(hasDateIssue){
			return errMessage;
		}
	
		Date todayDate = new com.ncsts.view.util.DateConverter().getUserLocalDate();	
		if (effectTS.before(todayDate)) {
			errMessage = true;
			FacesMessage message = new FacesMessage("Effective date must be on or after today.");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
		}
	
		if (effectTS!=null && exprTS!=null && exprTS.before(effectTS)) {
			errMessage = true;
			FacesMessage message = new FacesMessage("Expiration Date may not be earlier than Effective Date.");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
		} 
		
		updateTaxHoliday.setBaseChangePct(ArithmeticUtils.decimalFormat(updateTaxHoliday.getBaseChangePct()));
		updateTaxHoliday.setCapAmt(ArithmeticUtils.decimalFormat(updateTaxHoliday.getCapAmt()));
		updateTaxHoliday.setSpecialRate(ArithmeticUtils.decimalFormat(updateTaxHoliday.getSpecialRate()));
		updateTaxHoliday.setTaxableThresholdAmt(ArithmeticUtils.decimalFormat(updateTaxHoliday.getTaxableThresholdAmt()));
		updateTaxHoliday.setTaxLimitationAmt(ArithmeticUtils.decimalFormat(updateTaxHoliday.getTaxLimitationAmt()));
		
		return errMessage;
	}
	
	public String okMapAction(){
		if((currentAction == EditAction.MAP)){
			Hashtable<String, TaxHolidayDetail> unselectedTaxCodeHashtable = new Hashtable<String, TaxHolidayDetail>();
			for (String key : selectedTaxCodeHashtableBackup.keySet()) {
				TaxHolidayDetail aTaxHolidayDetail  = (TaxHolidayDetail)selectedTaxCodeHashtableBackup.get(key);
				unselectedTaxCodeHashtable.put(key, aTaxHolidayDetail);
	    	}
			
			//update TaxHolidayDetail
			List<TaxHolidayDetail> list =  getSelectedTaxCodeDetail();
			for (TaxHolidayDetail thd : list) {
				thd.setTaxHolidayCode(updateTaxHoliday.getTaxHolidayCode().trim());
				thd.setExceptInd("0");
				
				//Remove TaxHolidayDetail that doesn't need to be deleted.
				if(unselectedTaxCodeHashtable.get(thd.getKey())!=null){
					unselectedTaxCodeHashtable.remove(thd.getKey());
				}		
			}
			
			List<TaxHolidayDetail> unselectedList = new ArrayList<TaxHolidayDetail>();
			for (String key : unselectedTaxCodeHashtable.keySet()) {
				TaxHolidayDetail aTaxHolidayDetail  = (TaxHolidayDetail)unselectedTaxCodeHashtable.get(key);
				unselectedList.add(aTaxHolidayDetail);
	    	}
			
			taxHolidayService.updateTaxHolidayMapDetails(updateTaxHoliday, list, "0", null, unselectedList);
					
			//Refresh & Retrieve
			refreshAndRetrieve();
			
			return "taxholiday_maintenance";	
		}
	
		return null;
	}
	
	public String okAction(){
		if((currentAction == EditAction.ADD || currentAction == EditAction.COPY_ADD)){
			if (verifyData()) {
				return null;
			}
			
			List<TaxHolidayDetail> list = null;
			
			//Add TaxHolidayDetail
			if(currentAction == EditAction.ADD){
				//No TaxHolidayDetails
				list = new ArrayList<TaxHolidayDetail>();
			}
			else{
				//May have some TaxHolidayDetails
				list =  getSelectedTaxCodeDetail();
				for (TaxHolidayDetail thd : list) {
					thd.setTaxHolidayCode(updateTaxHoliday.getTaxHolidayCode().trim());
				}
			}
			
			boolean copyMapFlag = updateTaxHoliday.getCopyMapFlag();
			taxHolidayService.addTaxHolidayDetails(updateTaxHoliday, list, copyMapFlag);
			
			//Refresh & Retrieve
			refreshAndRetrieve();
			
			return "taxholiday_maintenance";	
		}
		else if((currentAction == EditAction.UPDATE)){
			if (verifyData()) {
				return null;
			}
			
			//update TaxHoliday
			taxHolidayService.updateTaxHoliday(updateTaxHoliday);
			
			//Refresh & Retrieve
			refreshAndRetrieve();
			
			return "taxholiday_maintenance";	
	
		}
		else if((currentAction == EditAction.DELETE)){
			//delete TaxHolidayDetail
			taxHolidayService.deleteTaxHoliday(updateTaxHoliday);
			
			//Refresh & Retrieve
			refreshAndRetrieve();
			
			return "taxholiday_maintenance";	
		}
		
		else if((currentAction == EditAction.VIEW)){
			return "taxholiday_maintenance";
		}
	
		return null;
	}
	
	protected void refreshAndRetrieve(){
		//Retrieve & Refresh
		selectedTaxCodeHashtable.clear();
		selectedTaxCodeHashtable = new Hashtable<String, TaxHolidayDetail>();
		
		nextAccepted = false;
		
		retrieveFilterTaxHoliday();
		
		if(listTaxHolidayDetailByTaxCode!=null)listTaxHolidayDetailByTaxCode.clear();
		if(listTaxHolidayDetailByJurisdiction!=null)listTaxHolidayDetailByJurisdiction.clear();
	}
	
	public String cancelAction() {
		return "taxholiday_maintenance";
	}
	
	public String okExceptionAction(){
		//update TaxHolidayDetail
		List<TaxHolidayDetail> list =  getSelectedJurisdictionDetail();
		for (TaxHolidayDetail thd : list) {
			thd.setTaxHolidayCode(updateTaxHoliday.getTaxHolidayCode().trim());
			if(thd.getExceptInd()==null){
				thd.setExceptInd("1");//*State has been set to 0
			}
		}
		taxHolidayService.updateTaxHolidayExceptionDetails(updateTaxHoliday, list, "1", selectedTaxCodeDetail);
		
		
		return "taxholiday_map";
	}
	
	public String cancelExceptionAction() {
		return "taxholiday_map";
	}
	
	public Boolean getIsViewAction() {
		return currentAction.equals(EditAction.VIEW);
	}
	public Boolean getIsUpdateAction() {
		return currentAction.equals(EditAction.UPDATE);
	}

	public Boolean getDisplayDelete() {
		if(selTaxHoliday == null){
			return false;
		}
		return false;
	}

	public Boolean getDisplayButtons() {
		return selectedRowIndex != -1;
	}
	
	public Boolean getIsExecuted() {
		return (selectedRowIndex != -1) && (selTaxHoliday.getExecuteBooleanFlag()!=null) && (selTaxHoliday.getExecuteBooleanFlag().booleanValue());
	}
	
	public Boolean getDisplayExecuteButton() {
		Long count = taxHolidayService.findTaxHolidayDetailCount(selTaxHoliday.getTaxHolidayCode());
		return (count>0L) && (selectedRowIndex != -1) && (selTaxHoliday.getExecuteBooleanFlag()==null || !selTaxHoliday.getExecuteBooleanFlag().booleanValue());
	}
	
	public Hashtable<String,String> getBatchTypes(){
        Hashtable<String,String> ht = new Hashtable<String,String>();
		//List<ListCodes> listCodeList = listCodesService.getListCodesByCodeTypeCode("BATCHTYPE");
		List<ListCodes> listCodeList = cacheManager.getListCodesByType("BATCHTYPE");
		for (ListCodes listcodes : listCodeList) {
			if(listcodes != null && listcodes.getCodeTypeCode()!=null && listcodes.getDescription()!=null){
				ht.put(listcodes.getCodeCode().trim(), listcodes.getDescription().trim());				
			}
		}
		return ht;
	}
	
	public void updateTaxCodeHashtableBackup(TaxHolidayDetail taxHolidayDetail){
		//key: TaxCode&#58;County&#58;City&#58;Stj, example TAXCODEA&#58;HARRIS&#58;HOUSTON&#58;STJ
		String key = (taxHolidayDetail.getKey());
		TaxHolidayDetail aTemp = selectedTaxCodeHashtable.get(key);
		selectedTaxCodeHashtableBackup.put(key, taxHolidayDetail);
	}

	public void updateTaxCodeHashtable(TaxHolidayDetail taxHolidayDetail){
		//key: TaxCode&#58;County&#58;City&#58;Stj, example TAXCODEA&#58;HARRIS&#58;HOUSTON&#58;STJ
		String key = (taxHolidayDetail.getKey());
		TaxHolidayDetail aTemp = selectedTaxCodeHashtable.get(key);
		
		if(aTemp==null && taxHolidayDetail.getSelected()){
			selectedTaxCodeHashtable.put(key, taxHolidayDetail);
		}
		else if(aTemp!=null && !taxHolidayDetail.getSelected()){
			selectedTaxCodeHashtable.remove(key);
		}
	}
	
	public void updateJurisdictionHashtable(TaxHolidayDetail taxHolidayDetail){
		//key: TaxCode&#58;County&#58;City&#58;Stj, example TAXCODEA&#58;HARRIS&#58;HOUSTON&#58;STJ
		String key = (taxHolidayDetail.getKey());
		TaxHolidayDetail aTemp = selectedJurisdictionHashtable.get(key);
		
		if(aTemp==null && taxHolidayDetail.getSelected()){
			selectedJurisdictionHashtable.put(key, taxHolidayDetail);
		}
		else if(aTemp!=null && !taxHolidayDetail.getSelected()){
			selectedJurisdictionHashtable.remove(key);
		}
	}
	
	public void exceptionCheckboxSelected(ActionEvent e) throws AbortProcessingException {
		HtmlSelectBooleanCheckbox chk = (HtmlSelectBooleanCheckbox) ((HtmlAjaxSupport) e.getComponent()).getParent();
		HtmlDataTable table = (HtmlDataTable) chk.getParent().getParent();
		TaxHolidayDetail td = (TaxHolidayDetail) table.getRowData();	
		if((Boolean) chk.getValue()) {
			updateJurisdictionHashtable(td);
		}
		else {
			updateJurisdictionHashtable(td);
		}		
	}
	
	public void mapCheckboxSelected(ActionEvent e) throws AbortProcessingException {
		HtmlSelectBooleanCheckbox chk = (HtmlSelectBooleanCheckbox) ((HtmlAjaxSupport) e.getComponent()).getParent();
		HtmlDataTable table = (HtmlDataTable) chk.getParent().getParent();
		TaxHolidayDetail td = (TaxHolidayDetail) table.getRowData();	
		if((Boolean) chk.getValue()) {
			updateTaxCodeHashtable(td);
		}
		else {
			updateTaxCodeHashtable(td);
		}		
	}

	public boolean isSelectedTaxCodeDetail(TaxHolidayDetail taxHolidayDetail){
		//key: TaxCode&#58;County&#58;City&#58;Stj, example TAXCODEA&#58;HARRIS&#58;HOUSTON&#58;STJ
		String key = (taxHolidayDetail.getKey());
		TaxHolidayDetail aTemp = selectedTaxCodeHashtable.get(key);
		
		if(aTemp==null){
			return false;
		}
		else{
			return true;
		}
	}
	
	public boolean isSelectedJurisdiction(TaxHolidayDetail taxHolidayDetail){
		//key: TaxCode&#58;County&#58;City&#58;Stj, example TAXCODEA&#58;HARRIS&#58;HOUSTON&#58;STJ
		String key = (taxHolidayDetail.getKey());
		TaxHolidayDetail aTemp = selectedJurisdictionHashtable.get(key);
		
		if(aTemp==null){
			return false;
		}
		else{
			return true;
		}
	}
	
	public String resetFilterSearchAction(){
		//clear filter on maintenance
		filterTaxHoliday = new TaxHoliday();
		
		if(filterStateMenuItems!=null) filterStateMenuItems.clear();
		filterStateMenuItems = getCacheManager().createStateItems("");

		return null;
	}
	
	private TaxHolidayDetail selectedTaxCodeDetail=null;
	private int selectedTaxCodeDetailRowIndex = -1;
	
	public int getSelectedTaxCodeDetailRowIndex() {
		return selectedTaxCodeDetailRowIndex;
	}
	
	public void setSelectedTaxCodeDetailRowIndex(int selectedTaxCodeDetailRowIndex) {
		this.selectedTaxCodeDetailRowIndex = selectedTaxCodeDetailRowIndex;
	}

	public void selectedTaxCodeDetailChanged (ActionEvent e) throws AbortProcessingException {
		HtmlDataTable table = (HtmlDataTable) (((HtmlAjaxSupport) e.getComponent()).getParent());
 		selectedTaxCodeDetail = (TaxHolidayDetail) table.getRowData();
		selectedTaxCodeDetailRowIndex = table.getRowIndex();
		selectedTaxCodeDetail.getTaxcodeCode();
		
		updateTaxCodeHashtable(selectedTaxCodeDetail);
	}
	
	public int getSelectedJurisdictionDetailRowIndex() {
		return selectedJurisdictionDetailRowIndex;
	}
	
	public void setSelectedJurisdictionDetailRowIndex(int selectedJurisdictionDetailRowIndex) {
		this.selectedJurisdictionDetailRowIndex = selectedJurisdictionDetailRowIndex;
	}
	
	public void selectedJurisdictionDetailChanged (ActionEvent e) throws AbortProcessingException {
		HtmlDataTable table = (HtmlDataTable) (((HtmlAjaxSupport) e.getComponent()).getParent());
		selectedJurisdictionDetail = (TaxHolidayDetail) table.getRowData();
		selectedJurisdictionDetailRowIndex = table.getRowIndex();
		selectedJurisdictionDetail.getTaxcodeCode();
		
		//updateJurisdictionHashtable(selectedJurisdictionDetail);
	}

	public CacheManager getCacheManager() {
		return this.cacheManager;
	}

	public void setCacheManager(CacheManager cacheManager) {
		this.cacheManager = cacheManager;
	}   
	private void addError(String s) {
		addMessage(s, FacesMessage.SEVERITY_ERROR);
	}
	private void addInfo(String s) {
		addMessage(s, FacesMessage.SEVERITY_INFO);
	}
	
	private void addMessage(String s, FacesMessage.Severity severity) {
		FacesContext.getCurrentInstance().addMessage(null, 
				new FacesMessage(severity, s, null));
	}

	public FilePreferenceBackingBean getFilePreferenceBean() {
		return this.filePreferenceBean;
	}

	public void setFilePreferenceBean(FilePreferenceBackingBean filePreferenceBean) {
		this.filePreferenceBean = filePreferenceBean;
	}

	public TaxHoliday getSelTaxHoliday() {
		return selTaxHoliday;
	}

	public void setSelTaxHoliday(TaxHoliday selTaxHoliday) {
		this.selTaxHoliday = selTaxHoliday;
	}
	
	public TaxHoliday getFilterTaxHoliday() {
		return filterTaxHoliday;
	}

	public void setFilterTaxHoliday(TaxHoliday filterTaxHoliday) {
		this.filterTaxHoliday = filterTaxHoliday;
	}
	
	public TaxHoliday getUpdateTaxHoliday() {
		return updateTaxHoliday;
	}

	public void setUpdateTaxHoliday(TaxHoliday updateTaxHoliday) {
		this.updateTaxHoliday = updateTaxHoliday;
	}

	public TaxHolidayDataModel getTaxHolidayDataModel() {
		if(!initialized) {
			retrieveFilterTaxHoliday();
			initialized = true;
		}
		return taxHolidayDataModel;
	}

	public void setTaxHolidayDataModel(
			TaxHolidayDataModel taxHolidayDataModel) {
		this.taxHolidayDataModel = taxHolidayDataModel;
	}

	public int getSelectedRowIndex() {
		return selectedRowIndex;
	}

	public void setSelectedRowIndex(int selectedRowIndex) {
		this.selectedRowIndex = selectedRowIndex;
	}

	public OptionService getOptionService() {
		return optionService;
	}

	public void setOptionService(OptionService optionService) {
		this.optionService = optionService;
	}

	public TaxHolidayService getTaxHolidayService() {
		return taxHolidayService;
	}

	public void setTaxHolidayService(
			TaxHolidayService taxHolidayService) {
		this.taxHolidayService = taxHolidayService;
	}

	public ListCodesService getListCodesService() {
		return listCodesService;
	}

	public void setListCodesService(ListCodesService listCodesService) {
		this.listCodesService = listCodesService;
	}

	public List<TaxHolidayDTO> getTaxHolidayList() {
		return taxHolidayList;		
	}

	public void setTaxHolidayList(List<TaxHolidayDTO> taxHolidayList) {
		this.taxHolidayList = taxHolidayList;
	}
	
	public TaxHolidayDTO getTaxHolidayDTO() {
		return taxHolidayDTO;
	}

	public void setTaxHolidayDTO(TaxHolidayDTO taxHolidayDTO) {
		this.taxHolidayDTO = taxHolidayDTO;
	}
	public SimpleSelection getSelection() {
		return selection;
	}
	
	public void setSelection(SimpleSelection selection) {
		this.selection = selection;
	}
	
	public List<TaxHolidayDTO> getSelectedTaxHolidayList() {
		return selectedTaxHolidayList;
	}

	public void setSelectedTaxHolidayList(
			List<TaxHolidayDTO> selectedTaxHolidayList) {
		this.selectedTaxHolidayList = selectedTaxHolidayList;
	}
	
	public TaxHolidayDTO getTaxHolidayDTONew() {
		return taxHolidayDTONew;
	}

	public void setTaxHolidayDTONew(TaxHolidayDTO taxHolidayDTONew) {
		this.taxHolidayDTONew = taxHolidayDTONew;
	}

	public void sortAction(ActionEvent e) {
		taxHolidayDataModel.toggleSortOrder(e.getComponent().getParent().getId());
	}
	
	/*
	public void countySelected(ActionEvent e) {
		String value = (String) ((HtmlSelectOneMenu) ((HtmlAjaxSupport) e.getComponent()).getParent()).getSubmittedValue();
		if(StringUtils.hasText(value)) {
			filterJurisdictionCity = "";
			setInputValue(cityInput, "");
		}
	}
	
	public void citySelected(ActionEvent e) {
		String value = (String) ((HtmlSelectOneMenu) ((HtmlAjaxSupport) e.getComponent()).getParent()).getSubmittedValue();
		if(StringUtils.hasText(value)) {
			filterJurisdictionCounty = "";
			setInputValue(countyInput, "");
		}
	}*/

	public HtmlSelectOneMenu getCountyInput() {
		return countyInput;
	}

	public void setCountyInput(HtmlSelectOneMenu countyInput) {
		this.countyInput = countyInput;
	}

	public HtmlSelectOneMenu getCityInput() {
		return cityInput;
	}

	public void setCityInput(HtmlSelectOneMenu cityInput) {
		this.cityInput = cityInput;
	}
	
	public HtmlSelectOneMenu getStjInput() {
		return stjInput;
	}

	public void setStjInput(HtmlSelectOneMenu stjInput) {
		this.stjInput = stjInput;
	}
	
	private void setInputValue(UIInput input, String value) {
		input.setLocalValueSet(false);
		input.setSubmittedValue(null);
		input.setValue(value);
	}
	
	public User getCurrentUser() {
		return loginBean.getUser();
   }
}
