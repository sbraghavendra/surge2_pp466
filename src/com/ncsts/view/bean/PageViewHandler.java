package com.ncsts.view.bean;

import javax.faces.application.ViewHandler;
import javax.faces.application.ViewHandlerWrapper;
import javax.faces.component.UIViewRoot;
import javax.faces.context.FacesContext;

public class PageViewHandler extends ViewHandlerWrapper {

	private ViewHandler handler;
	private String page;
	
	public PageViewHandler(ViewHandler handler) {
		this.handler = handler;
	}
	@Override
	protected ViewHandler getWrapped() {
		return this.handler;
	}

	@Override
	public UIViewRoot createView(FacesContext context, java.lang.String viewId) {
		page = viewId;
		return handler.createView(context, viewId);
	}
	
	public String getPage() {
		return page;
	}
	
	public void setPage(String page) {
		this.page = page;
	}

}
