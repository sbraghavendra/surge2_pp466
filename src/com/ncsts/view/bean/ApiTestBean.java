package com.ncsts.view.bean;

import java.io.IOException;
import java.net.URI;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.faces.context.FacesContext;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLException;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.HttpEntity;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.conn.ssl.X509HostnameVerifier;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("session")
public class ApiTestBean {
	private static final Log logger = LogFactory.getLog(ApiTestBean.class);
	
	private static final String SAMPLE_REQUEST = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n<mes:SaleTransactionProcessRequest xmlns:mes=\"http://www.seconddecimal.com/ncsts/schemas/messages\">\n\t<saleTransaction>\n\t<vendorTaxAmt></vendorTaxAmt>\n\t<userText30></userText30>\n\t<userText29></userText29>\n\t<userText28></userText28>\n\t<userText27></userText27>\n\t<userText26></userText26>\n\t<userText25></userText25>\n\t<userText24></userText24>\n\t<userText23></userText23>\n\t<userText22></userText22>\n\t<userText21></userText21>\n\t<userText20></userText20>\n\t<userText19></userText19>\n\t<userText18></userText18>\n\t<userText17></userText17>\n\t<userText16></userText16>\n\t<userText15></userText15>\n\t<userText14></userText14>\n\t<userText13></userText13>\n\t<userText12></userText12>\n\t<userText11></userText11>\n\t<userText10></userText10>\n\t<userText09></userText09>\n\t<userText08></userText08>\n\t<userText07></userText07>\n\t<userText06></userText06>\n\t<userText05></userText05>\n\t<userText04></userText04>\n\t<userText03></userText03>\n\t<userText02></userText02>\n\t<userText01></userText01>\n\t<userNbr10></userNbr10>\n\t<userNbr09></userNbr09>\n\t<userNbr08></userNbr08>\n\t<userNbr07></userNbr07>\n\t<userNbr06></userNbr06>\n\t<userNbr05></userNbr05>\n\t<userNbr04></userNbr04>\n\t<userNbr03></userNbr03>\n\t<userNbr02></userNbr02>\n\t<userNbr01></userNbr01>\n\t<userDate10></userDate10>\n\t<userDate09></userDate09>\n\t<userDate08></userDate08>\n\t<userDate07></userDate07>\n\t<userDate06></userDate06>\n\t<userDate05></userDate05>\n\t<userDate04></userDate04>\n\t<userDate03></userDate03>\n\t<userDate02></userDate02>\n\t<userDate01></userDate01>\n\t<taxPaidToVendorFlag></taxPaidToVendorFlag>\n\t<locationName></locationName>\n\t<divisionDesc></divisionDesc>\n\t<custZipplus4></custZipplus4>\n\t<custZip></custZip>\n\t<custStateCode></custStateCode>\n\t<custLocnCode></custLocnCode>\n\t<custJurisdictionId></custJurisdictionId>\n\t<custGeocode></custGeocode>\n\t<custEntityId></custEntityId>\n\t<custEntityCode></custEntityCode>\n\t<custCounty></custCounty>\n\t<custCountryCode></custCountryCode>\n\t<custCity></custCity>\n\t<custAddressLine2></custAddressLine2>\n\t<custAddressLine1></custAddressLine1>\n\t<companyDesc></companyDesc>\n\t<stateFreightDetailId></stateFreightDetailId>\n\t<updateTimestamp></updateTimestamp>\n\t<updateUserId></updateUserId>\n\t<doNotCalcFlag></doNotCalcFlag>\n\t<stj10TaxAmt></stj10TaxAmt>\n\t<stj9TaxAmt></stj9TaxAmt>\n\t<stj8TaxAmt></stj8TaxAmt>\n\t<stj7TaxAmt></stj7TaxAmt>\n\t<stj6TaxAmt></stj6TaxAmt>\n\t<stj5TaxAmt></stj5TaxAmt>\n\t<stj4TaxAmt></stj4TaxAmt>\n\t<stj3TaxAmt></stj3TaxAmt>\n\t<stj2TaxAmt></stj2TaxAmt>\n\t<stj1TaxAmt></stj1TaxAmt>\n\t<cityTaxAmt></cityTaxAmt>\n\t<countyTaxAmt></countyTaxAmt>\n\t<stateTier3TaxAmt></stateTier3TaxAmt>\n\t<stateTier2TaxAmt></stateTier2TaxAmt>\n\t<stateTier1TaxAmt></stateTier1TaxAmt>\n\t<stateTaxAmt></stateTaxAmt>\n\t<countryTaxAmt></countryTaxAmt>\n\t<taxAmt></taxAmt>\n\t<stj10TaxableAmt></stj10TaxableAmt>\n\t<stj9TaxableAmt></stj9TaxableAmt>\n\t<stj8TaxableAmt></stj8TaxableAmt>\n\t<stj7TaxableAmt></stj7TaxableAmt>\n\t<stj6TaxableAmt></stj6TaxableAmt>\n\t<stj5TaxableAmt></stj5TaxableAmt>\n\t<stj4TaxableAmt></stj4TaxableAmt>\n\t<stj3TaxableAmt></stj3TaxableAmt>\n\t<stj2TaxableAmt></stj2TaxableAmt>\n\t<stj1TaxableAmt></stj1TaxableAmt>\n\t<cityTaxableAmt></cityTaxableAmt>\n\t<countyTaxableAmt></countyTaxableAmt>\n\t<stateTier3TaxableAmt></stateTier3TaxableAmt>\n\t<stateTier2TaxableAmt></stateTier2TaxableAmt>\n\t<stateTier1TaxableAmt></stateTier1TaxableAmt>\n\t<stateTaxableAmt></stateTaxableAmt>\n\t<countryTaxableAmt></countryTaxableAmt>\n\t<taxableAmt></taxableAmt>\n\t<taxcodeCode></taxcodeCode>\n\t<invoiceLineProductCode></invoiceLineProductCode>\n\t<invoiceLineDiscTypeCode></invoiceLineDiscTypeCode>\n\t<invoiceLineDiscAmt></invoiceLineDiscAmt>\n\t<invoiceLineFreightAmt></invoiceLineFreightAmt>\n\t<invoiceLineGrossAmt></invoiceLineGrossAmt>\n\t<invoiceLineItemAmt></invoiceLineItemAmt>\n\t<invoiceLineItemCount></invoiceLineItemCount>\n\t<invoiceLineDesc></invoiceLineDesc>\n\t<invoiceLineNbr></invoiceLineNbr>\n\t<invoiceDiscTypeCode></invoiceDiscTypeCode>\n\t<invoiceDiscAmt></invoiceDiscAmt>\n\t<invoiceFreightAmt></invoiceFreightAmt>\n\t<invoiceGrossAmt></invoiceGrossAmt>\n\t<invoiceDesc></invoiceDesc>\n\t<invoiceNbr></invoiceNbr>\n\t<invoiceDate></invoiceDate>\n\t<exclusionAmt></exclusionAmt>\n\t<exemptAmt></exemptAmt>\n\t<exemptCode></exemptCode>\n\t<exemptReason></exemptReason>\n\t<certImage></certImage>\n\t<certExpirationDate></certExpirationDate>\n\t<certSignDate></certSignDate>\n\t<certType></certType>\n\t<certNbr></certNbr>\n\t<exemptInd></exemptInd>\n\t<creditInd></creditInd>\n\t<stj10TaxrateId></stj10TaxrateId>\n\t<stj9TaxrateId></stj9TaxrateId>\n\t<stj8TaxrateId></stj8TaxrateId>\n\t<stj7TaxrateId></stj7TaxrateId>\n\t<stj6TaxrateId></stj6TaxrateId>\n\t<stj5TaxrateId></stj5TaxrateId>\n\t<stj4TaxrateId></stj4TaxrateId>\n\t<stj3TaxrateId></stj3TaxrateId>\n\t<stj2TaxrateId></stj2TaxrateId>\n\t<stj1TaxrateId></stj1TaxrateId>\n\t<cityTaxrateId></cityTaxrateId>\n\t<countyTaxrateId></countyTaxrateId>\n\t<stateTaxrateId></stateTaxrateId>\n\t<countryTaxrateId></countryTaxrateId>\n\t<stj10TaxcodeDetailId></stj10TaxcodeDetailId>\n\t<stj9TaxcodeDetailId></stj9TaxcodeDetailId>\n\t<stj8TaxcodeDetailId></stj8TaxcodeDetailId>\n\t<stj7TaxcodeDetailId></stj7TaxcodeDetailId>\n\t<stj6TaxcodeDetailId></stj6TaxcodeDetailId>\n\t<stj5TaxcodeDetailId></stj5TaxcodeDetailId>\n\t<stj4TaxcodeDetailId></stj4TaxcodeDetailId>\n\t<stj3TaxcodeDetailId></stj3TaxcodeDetailId>\n\t<stj2TaxcodeDetailId></stj2TaxcodeDetailId>\n\t<stj1TaxcodeDetailId></stj1TaxcodeDetailId>\n\t<cityTaxcodeDetailId></cityTaxcodeDetailId>\n\t<countyTaxcodeDetailId></countyTaxcodeDetailId>\n\t<stateTaxcodeDetailId></stateTaxcodeDetailId>\n\t<countryTaxcodeDetailId></countryTaxcodeDetailId>\n\t<stj10NexusDefDetailId></stj10NexusDefDetailId>\n\t<stj9NexusDefDetailId></stj9NexusDefDetailId>\n\t<stj8NexusDefDetailId></stj8NexusDefDetailId>\n\t<stj7NexusDefDetailId></stj7NexusDefDetailId>\n\t<stj6NexusDefDetailId></stj6NexusDefDetailId>\n\t<stj5NexusDefDetailId></stj5NexusDefDetailId>\n\t<stj4NexusDefDetailId></stj4NexusDefDetailId>\n\t<stj3NexusDefDetailId></stj3NexusDefDetailId>\n\t<stj2NexusDefDetailId></stj2NexusDefDetailId>\n\t<stj1NexusDefDetailId></stj1NexusDefDetailId>\n\t<cityNexusDefDetailId></cityNexusDefDetailId>\n\t<countyNexusDefDetailId></countyNexusDefDetailId>\n\t<stateNexusDefDetailId></stateNexusDefDetailId>\n\t<countryNexusDefDetailId></countryNexusDefDetailId>\n\t<stj5SitusMatrixId></stj5SitusMatrixId>\n\t<stj4SitusMatrixId></stj4SitusMatrixId>\n\t<stj3SitusMatrixId></stj3SitusMatrixId>\n\t<stj2SitusMatrixId></stj2SitusMatrixId>\n\t<stj1SitusMatrixId></stj1SitusMatrixId>\n\t<citySitusMatrixId></citySitusMatrixId>\n\t<countySitusMatrixId></countySitusMatrixId>\n\t<stateSitusMatrixId></stateSitusMatrixId>\n\t<countrySitusMatrixId></countrySitusMatrixId>\n\t<entityLocnSetDtlId></entityLocnSetDtlId>\n\t<processMethod></processMethod>\n\t<comments></comments>\n\t<glDate></glDate>\n\t<billtoStj5Name></billtoStj5Name>\n\t<billtoStj4Name></billtoStj4Name>\n\t<billtoStj3Name></billtoStj3Name>\n\t<billtoStj2Name></billtoStj2Name>\n\t<billtoStj1Name></billtoStj1Name>\n\t<billtoCountryCode></billtoCountryCode>\n\t<billtoZipplus4></billtoZipplus4>\n\t<billtoZip></billtoZip>\n\t<billtoStateCode></billtoStateCode>\n\t<billtoCounty></billtoCounty>\n\t<billtoCity></billtoCity>\n\t<billtoAddressLine2></billtoAddressLine2>\n\t<billtoAddressLine1></billtoAddressLine1>\n\t<billtoGeocode></billtoGeocode>\n\t<billtoJurisdictionId></billtoJurisdictionId>\n\t<billtoEntityCode></billtoEntityCode>\n\t<billtoEntityId></billtoEntityId>\n\t<firstuseStj5Name></firstuseStj5Name>\n\t<firstuseStj4Name></firstuseStj4Name>\n\t<firstuseStj3Name></firstuseStj3Name>\n\t<firstuseStj2Name></firstuseStj2Name>\n\t<firstuseStj1Name></firstuseStj1Name>\n\t<firstuseCountryCode></firstuseCountryCode>\n\t<firstuseZipplus4></firstuseZipplus4>\n\t<firstuseZip></firstuseZip>\n\t<firstuseStateCode></firstuseStateCode>\n\t<firstuseCounty></firstuseCounty>\n\t<firstuseCity></firstuseCity>\n\t<firstuseAddressLine2></firstuseAddressLine2>\n\t<firstuseAddressLine1></firstuseAddressLine1>\n\t<firstuseGeocode></firstuseGeocode>\n\t<firstuseJurisdictionId></firstuseJurisdictionId>\n\t<firstuseEntityCode></firstuseEntityCode>\n\t<firstuseEntityId></firstuseEntityId>\n\t<ordrorgnStj5Name></ordrorgnStj5Name>\n\t<ordrorgnStj4Name></ordrorgnStj4Name>\n\t<ordrorgnStj3Name></ordrorgnStj3Name>\n\t<ordrorgnStj2Name></ordrorgnStj2Name>\n\t<ordrorgnStj1Name></ordrorgnStj1Name>\n\t<ordrorgnCountryCode></ordrorgnCountryCode>\n\t<ordrorgnZipplus4></ordrorgnZipplus4>\n\t<ordrorgnZip></ordrorgnZip>\n\t<ordrorgnStateCode></ordrorgnStateCode>\n\t<ordrorgnCounty></ordrorgnCounty>\n\t<ordrorgnCity></ordrorgnCity>\n\t<ordrorgnAddressLine2></ordrorgnAddressLine2>\n\t<ordrorgnAddressLine1></ordrorgnAddressLine1>\n\t<ordrorgnGeocode></ordrorgnGeocode>\n\t<ordrorgnJurisdictionId></ordrorgnJurisdictionId>\n\t<ordrorgnEntityCode></ordrorgnEntityCode>\n\t<ordrorgnEntityId></ordrorgnEntityId>\n\t<ordracptStj5Name></ordracptStj5Name>\n\t<ordracptStj4Name></ordracptStj4Name>\n\t<ordracptStj3Name></ordracptStj3Name>\n\t<ordracptStj2Name></ordracptStj2Name>\n\t<ordracptStj1Name></ordracptStj1Name>\n\t<ordracptCountryCode></ordracptCountryCode>\n\t<ordracptZipplus4></ordracptZipplus4>\n\t<ordracptZip></ordracptZip>\n\t<ordracptStateCode></ordracptStateCode>\n\t<ordracptCounty></ordracptCounty>\n\t<ordracptCity></ordracptCity>\n\t<ordracptAddressLine2></ordracptAddressLine2>\n\t<ordracptAddressLine1></ordracptAddressLine1>\n\t<ordracptGeocode></ordracptGeocode>\n\t<ordracptJurisdictionId></ordracptJurisdictionId>\n\t<ordracptEntityCode></ordracptEntityCode>\n\t<ordracptEntityId></ordracptEntityId>\n\t<shiptoStj5Name></shiptoStj5Name>\n\t<shiptoStj4Name></shiptoStj4Name>\n\t<shiptoStj3Name></shiptoStj3Name>\n\t<shiptoStj2Name></shiptoStj2Name>\n\t<shiptoStj1Name></shiptoStj1Name>\n\t<shiptoCountryCode></shiptoCountryCode>\n\t<shiptoZipplus4></shiptoZipplus4>\n\t<shiptoZip></shiptoZip>\n\t<shiptoStateCode></shiptoStateCode>\n\t<shiptoCounty></shiptoCounty>\n\t<shiptoCity></shiptoCity>\n\t<shiptoAddressLine2></shiptoAddressLine2>\n\t<shiptoAddressLine1></shiptoAddressLine1>\n\t<shiptoGeocode></shiptoGeocode>\n\t<shiptoJurisdictionId></shiptoJurisdictionId>\n\t<shiptoEntityCode></shiptoEntityCode>\n\t<shiptoEntityId></shiptoEntityId>\n\t<custName></custName>\n\t<custNbr></custNbr>\n\t<shipfromStj5Name></shipfromStj5Name>\n\t<shipfromStj4Name></shipfromStj4Name>\n\t<shipfromStj3Name></shipfromStj3Name>\n\t<shipfromStj2Name></shipfromStj2Name>\n\t<shipfromStj1Name></shipfromStj1Name>\n\t<shipfromCountryCode></shipfromCountryCode>\n\t<shipfromZipplus4></shipfromZipplus4>\n\t<shipfromZip></shipfromZip>\n\t<shipfromStateCode></shipfromStateCode>\n\t<shipfromCounty></shipfromCounty>\n\t<shipfromCity></shipfromCity>\n\t<shipfromAddressLine2></shipfromAddressLine2>\n\t<shipfromAddressLine1></shipfromAddressLine1>\n\t<shipfromGeocode></shipfromGeocode>\n\t<shipfromJurisdictionId></shipfromJurisdictionId>\n\t<shipfromEntityCode></shipfromEntityCode>\n\t<shipfromEntityId></shipfromEntityId>\n\t<divisionNbr></divisionNbr>\n\t<companyNbr></companyNbr>\n\t<discountItemFlag></discountItemFlag>\n\t<freightItemFlag></freightItemFlag>\n\t<discountType></discountType>\n\t<geoReconCode></geoReconCode>\n\t<methodDeliveryCode></methodDeliveryCode>\n\t<methodDelivery></methodDelivery>\n\t<ratetypeCode></ratetypeCode>\n\t<ratetype></ratetype>\n\t<entityCode></entityCode>\n\t<entityId></entityId>\n\t<transactionTypeCode></transactionTypeCode>\n\t<transactionType></transactionType>\n\t<enteredDate></enteredDate>\n\t<processBatchNo></processBatchNo>\n\t<saletransId></saletransId>\n\t</saleTransaction>\n</mes:SaleTransactionProcessRequest>";

	private String endPoint;
	private String username;
	private String password;
	private String request;
	private String response;
	
	public String getEndPoint() {
		if (endPoint == null) {
			HttpServletRequest servletRequest = (HttpServletRequest) FacesContext
					.getCurrentInstance().getExternalContext().getRequest();
			endPoint = String.format(
					"%s://%s:%d/ncsts/rest/saleTransactionProcess",
					servletRequest.getScheme(), servletRequest.getServerName(),
					servletRequest.getServerPort());
		}
		return endPoint;
	}

	public void setEndPoint(String endPoint) {
		this.endPoint = endPoint;
	}

	public String getRequest() {
		if(request == null) {
			request = SAMPLE_REQUEST;
		}
		return request;
	}

	public void setRequest(String request) {
		this.request = request;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void submitAction() {
		response = post();
	}

	private String post() {
		String responseBody = null;
		DefaultHttpClient httpclient = wrapClient(new DefaultHttpClient());
		
		try {
			URI uri = new URI(getEndPoint());
			
			httpclient.getCredentialsProvider().setCredentials(
                    new AuthScope(uri.getHost(), uri.getPort()),
                    new UsernamePasswordCredentials(username, password));
			
			HttpPost httppost = new HttpPost(getEndPoint());
			httppost.setHeader("Content-Type", "application/xml");

			HttpEntity entity = new StringEntity(request, "UTF-8");
			httppost.setEntity(entity);

			if (logger.isDebugEnabled()) {
				logger.warn("executing request " + httppost.getURI());
			}

			ResponseHandler<String> responseHandler = new BasicResponseHandler();
			responseBody = httpclient.execute(httppost, responseHandler);

			if (logger.isDebugEnabled()) {
				logger.warn(responseBody);
			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			responseBody = e.getMessage();
		} finally {
			httpclient.getConnectionManager().shutdown();
		}

		return responseBody;
	}
	
	public static DefaultHttpClient wrapClient(HttpClient base) {
		try {
			SSLContext ctx = SSLContext.getInstance("TLS");
			
			X509TrustManager tm = new X509TrustManager() {

				public void checkClientTrusted(X509Certificate[] xcs,
						String string) throws CertificateException {
				}

				public void checkServerTrusted(X509Certificate[] xcs,
						String string) throws CertificateException {
				}

				public X509Certificate[] getAcceptedIssuers() {
					return null;
				}
			};
			
			X509HostnameVerifier verifier = new X509HostnameVerifier() {
				@Override
				public void verify(String string, SSLSocket ssls)
						throws IOException {
				}

				@Override
				public void verify(String string, X509Certificate xc)
						throws SSLException {
				}

				@Override
				public void verify(String string, String[] strings,
						String[] strings1) throws SSLException {
				}

				@Override
				public boolean verify(String string, SSLSession ssls) {
					return true;
				}
			};
			
			ctx.init(null, new TrustManager[] { tm }, null);
			SSLSocketFactory ssf = new SSLSocketFactory(ctx, verifier);
			ClientConnectionManager ccm = base.getConnectionManager();
			SchemeRegistry sr = ccm.getSchemeRegistry();
			sr.register(new Scheme("https", 443, ssf));
			return new DefaultHttpClient(ccm, base.getParams());
		} catch (Exception ex) {
			ex.printStackTrace();
			return null;
		}
	}
}
