package com.ncsts.view.bean;

import java.util.ArrayList;
import java.util.List;

import javax.faces.event.AbortProcessingException;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.ajax4jsf.component.html.HtmlAjaxSupport;
import org.apache.log4j.Logger;
import org.richfaces.component.html.HtmlDataTable;

import com.ncsts.common.LoggerFactory;
import com.ncsts.common.Util;
import com.ncsts.jsf.model.TransactionDataModel;
import com.ncsts.services.DataDefinitionService;
import com.ncsts.domain.DataDefinitionColumn;

public class AdvancedFilterBean {

	static private Logger logger = LoggerFactory.getInstance().getLogger(
			AdvancedFilterBean.class);

	private List<SelectItem> operatorItems;
	private int selectedAdvancedFilterRowIndex = -1;
	private DataDefinitionService dataDefinitionService;
	private List<DataDefinitionColumn> dataDefinitionColList;
	private TransactionDataModel transactionDataModel;
	
	private String advancedFilter = "";
	private String advancedFilterTemp = "";
	private String advancedFilterMessage = "";
	
	private String fromScreenName;
	private static final String ADVANCE_FILTER_DBSTATITCIS_SCREEN = "dbstatitics";
	private static final String ADVANCE_FILTER_TRANSACTION_SCREEN = "transactionProcess";

	public String getFromScreenName() {
		return fromScreenName;
	}

	public void setFromScreenName(String fromScreenName) {
		this.fromScreenName = fromScreenName;
	}

	
	public AdvancedFilterBean() {
	}
	
	public int getSelectedAdvancedFilterRowIndex() {
		return selectedAdvancedFilterRowIndex;
	}

	public void setSelectedAdvancedFilterRowIndex(int selectedAdvancedFilterRowIndex) {
		this.selectedAdvancedFilterRowIndex = selectedAdvancedFilterRowIndex;
	}

	public TransactionDataModel getTransactionDataModel() {
		return transactionDataModel;
	}

	public void setTransactionDataModel(
			TransactionDataModel transactionDataModel) {
		this.transactionDataModel = transactionDataModel;
	}

	public List<DataDefinitionColumn> getDataDefinitionColList() {
		if (dataDefinitionColList == null) {
			dataDefinitionColList = dataDefinitionService.getAllDataDefinitionColumnByTable("TB_PURCHTRANS");
		}

		return dataDefinitionColList;
	}

	public void setDataDefinitionColList(
			List<DataDefinitionColumn> dataDefinitionColList) {
		this.dataDefinitionColList = dataDefinitionColList;
	}

	public DataDefinitionService getDataDefinitionService() {
		return dataDefinitionService;
	}

	public void setDataDefinitionService(
			DataDefinitionService dataDefinitionService) {
		this.dataDefinitionService = dataDefinitionService;
	}

	public List<SelectItem> getOperatorItems() {
		if (operatorItems == null) {
			operatorItems = new ArrayList<SelectItem>();
			operatorItems.add(new SelectItem("(", "("));
			operatorItems.add(new SelectItem(")", ")"));
			operatorItems.add(new SelectItem("<", " < "));
			operatorItems.add(new SelectItem("<=", " <= "));
			operatorItems.add(new SelectItem("<>", " <> "));
			operatorItems.add(new SelectItem("=", " = "));
			operatorItems.add(new SelectItem(">", " > "));
			operatorItems.add(new SelectItem(">=", " >= "));
			operatorItems.add(new SelectItem("and", " and "));
			operatorItems.add(new SelectItem("or", " or "));
			operatorItems.add(new SelectItem("'", "'"));
			operatorItems.add(new SelectItem("in", " in "));
			operatorItems.add(new SelectItem(" like ", "like"));
			operatorItems.add(new SelectItem(" not like ", "notlike"));
			operatorItems.add(new SelectItem(" is null ", "isnull"));
			operatorItems.add(new SelectItem(" is not null ", "isnotnull"));
		}
		return operatorItems;
	}

	public String getAdvancedFilterMessage() {
		return advancedFilterMessage;
	}

	public String getAdvancedFilterTemp() {
		return advancedFilterTemp;
	}

	public void setAdvancedFilterTemp(String advancedFilterTemp) {
		this.advancedFilterTemp = advancedFilterTemp;
	}

	public String getAdvancedFilter() {
		return advancedFilter;
	}

	public void setAdvancedFilter(String advancedFilter) {
		this.advancedFilter = advancedFilter;
	}

	public boolean getIsAdvancedFilterOn() {
		return (advancedFilter != null && advancedFilter.length() > 0);
	}

	public boolean isValidAdvancedFilter(String filter) {
		return transactionDataModel.isValidAdvancedFilter(filter);
	}

	public void initAdvancedFilter() {
		advancedFilterMessage = "";

		if (advancedFilter != null && advancedFilter.length() > 0) {
			advancedFilterTemp = advancedFilter;
		} 
		else {
			advancedFilterTemp = "";
		}
	}
	
	public boolean isAdvancedFilterEnabled() {
		if (advancedFilter != null && advancedFilter.length() > 0) {
			return true;
		} 
		else {
			return false;
		}
	}
	
	public void selectedOperatorChanged(ActionEvent e) throws AbortProcessingException {
		HtmlDataTable table = (HtmlDataTable) (((HtmlAjaxSupport) e.getComponent()).getParent());
		SelectItem selectedOperator = (SelectItem) table.getRowData();
		advancedFilterTemp = advancedFilterTemp + selectedOperator.getValue();
	}

	public void selectedColumnChanged(ActionEvent e) throws AbortProcessingException {
		HtmlDataTable table = (HtmlDataTable) (((HtmlAjaxSupport) e.getComponent()).getParent());
		DataDefinitionColumn selectedDataDefinitionColumn = (DataDefinitionColumn) table.getRowData();

		advancedFilterTemp = advancedFilterTemp + selectedDataDefinitionColumn.getDataDefinitionColumnPK().getColumnName();
	}

	public String okAdvancedFilterAction() {
		advancedFilterTemp = advancedFilterTemp.trim();
		if (advancedFilterTemp != null && advancedFilterTemp.length() > 0) {
			if (isValidAdvancedFilter(advancedFilterTemp)) {
				advancedFilter = advancedFilterTemp;
				advancedFilterTemp = "";
				advancedFilterMessage = "";

				if(ADVANCE_FILTER_TRANSACTION_SCREEN.equals(getFromScreenName())) {
					 return "transactions_process";
				}
				else {
					return "transactions_db_stats";
				}
			} 
			else {

				advancedFilterMessage = "The Filter Expression is not valid!";
				return "advanced_filter";
			}
		} 
		else {
			advancedFilterMessage = "";
			advancedFilterTemp = "";
			advancedFilter = "";

			if(ADVANCE_FILTER_TRANSACTION_SCREEN.equals(getFromScreenName())) {
				 return "transactions_process";
			}
			else {
				return "transactions_db_stats";
			}
		}
	}

	public String clearAdvancedFilterAction() {
		advancedFilterTemp = "";
		advancedFilterMessage = "";

		if(ADVANCE_FILTER_TRANSACTION_SCREEN.equals(getFromScreenName())) {
			 return "advanced_filter";
		}
		else {
			return "advanced_filter";
		}
	}

	public String cancelAdvancedFilterAction() {
		advancedFilterTemp = "";
		advancedFilterMessage = "";

		return "transactions_process";
	}
}