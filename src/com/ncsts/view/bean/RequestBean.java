package com.ncsts.view.bean;

import com.ncsts.dao.PreferenceDAO.AdminPreference;
import com.ncsts.services.PreferenceService;

public class RequestBean {

	private PreferenceService preferenceService;
	
	private Boolean taxPartner;
	
	/** End **/
	
	
	public RequestBean() {
	}
	
	public PreferenceService getPreferenceService() {
		return preferenceService;
	}

	public void setPreferenceService(PreferenceService preferenceService) {
		this.preferenceService = preferenceService;
	}

	public Boolean getTaxPartner() {
		if (taxPartner == null) {
			String partner = preferenceService.getAdminPreference(AdminPreference.TPEXTRACTENABLED, "0");
			taxPartner = partner.equals("1");
		}
		
		return taxPartner;
	}
}
