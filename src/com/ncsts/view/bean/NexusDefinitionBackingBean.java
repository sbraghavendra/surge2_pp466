package com.ncsts.view.bean;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.component.html.HtmlSelectBooleanCheckbox;
import javax.faces.context.FacesContext;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.ajax4jsf.component.html.HtmlAjaxSupport;
import org.richfaces.component.html.HtmlDataTable;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.ncsts.common.CacheManager;
import com.ncsts.domain.EntityItem;
import com.ncsts.domain.NexusDefinition;
import com.ncsts.domain.NexusDefinitionDetail;
import com.ncsts.domain.Option;
import com.ncsts.domain.OptionCodePK;
import com.ncsts.domain.Registration;
import com.ncsts.domain.RegistrationDetail;
import com.ncsts.dto.NexusJurisdictionDTO;
import com.ncsts.dto.NexusJurisdictionItemDTO;
import com.ncsts.dto.TaxCodeStateDTO;
import com.ncsts.services.EntityItemService;
import com.ncsts.services.JurisdictionService;
import com.ncsts.services.NexusDefinitionDetailService;
import com.ncsts.services.RegistrationService;
import com.ncsts.services.RegistrationDetailService;
import com.ncsts.services.NexusDefinitionService;
import com.ncsts.services.OptionService;
import com.ncsts.services.TaxCodeStateService;

@Component
@Scope("session")
public class NexusDefinitionBackingBean implements InitializingBean {
	@Autowired
	private MatrixCommonBean matrixCommonBean;
	
	@Autowired
	private EntityItemService entityItemService;
	
	@Autowired
	private TaxCodeStateService taxCodeStateService;
	
	@Autowired
	private JurisdictionService jurisdictionService;
	
	@Autowired
	private NexusDefinitionService nexusDefinitionService;
	
	@Autowired
	private RegistrationService registrationService;

	@Autowired
	private NexusDefinitionDetailService nexusDefinitionDetailService;
	
	@Autowired
	private RegistrationDetailService registrationDetailService;
	
	@Autowired
	private CacheManager cacheManager;
	
	@Autowired
	private OptionService optionService;
	
	public enum TaskAction {
		REGISTRATION,
		DEFINITION;
	}
	
	private TaskAction taskAction = TaskAction.DEFINITION;
	
	private String filterEntityCode;
	private String filterEntityName;
	private String filterActive;
	private String filterStatesActive;
	
	private SearchJurisdictionHandler filterHandler = new SearchJurisdictionHandler();
	private SearchJurisdictionHandler viewFilterHandler;
	private SearchJurisdictionHandler statesFilterHandler;
	
	private List<EntityItem> entityItemList;
	private List<TaxCodeStateDTO> stateList;
	private List<NexusDefinitionDetail> nexusDefDetailList;
	private List<RegistrationDetail> registrationDetailList;
	
	
	private List<NexusJurisdictionItemDTO> countyList;
	private List<NexusJurisdictionItemDTO> cityList;
	private List<NexusJurisdictionItemDTO> stjList;
	private List<NexusJurisdictionItemDTO> levelList;
	
	private EntityItem selectedEntityItem;
	private int selectedEntityIndex = -1;
	private TaxCodeStateDTO selectedState;
	private int selectedStateIndex = -1;
	private NexusDefinitionDetail selectedDefinitionDetail;
	private RegistrationDetail selectedRegistrationDetail;
	private int selectedDefinitionDetailIndex = -1;
	private int selectedRegistrationDetailIndex = -1;
	private NexusJurisdictionItemDTO selectedLevel;
	private int selectedLevelIndex = -1;
	private NexusJurisdictionItemDTO selectedCounty;
	private int selectedCountyIndex = -1;
	private NexusJurisdictionItemDTO selectedCity;
	private int selectedCityIndex = -1;
	private NexusJurisdictionItemDTO selectedStj;
	private int selectedStjIndex = -1;
	private NexusJurisdictionDTO selectedJurisdiction;
	private NexusDefinitionDetail selectedNexusDefDetail;
	private int selectedNexusDefDetailIndex = -1;
	private NexusDefinition selectedNexusDefinition;
	private Registration selectedRegistration;
	
	private Map<String, NexusJurisdictionItemDTO> selectedLevels = new HashMap<String, NexusJurisdictionItemDTO>();
	private Map<String, NexusJurisdictionItemDTO> selectedCounties = new HashMap<String, NexusJurisdictionItemDTO>();
	private Map<String, NexusJurisdictionItemDTO> selectedCities = new HashMap<String, NexusJurisdictionItemDTO>();
	private Map<String, NexusJurisdictionItemDTO> selectedStjs = new HashMap<String, NexusJurisdictionItemDTO>();
	
	private String nexusDefStatesReturnView;
	
	private EditAction currentAction = EditAction.VIEW;
	
	private List<SelectItem> nexusTypeItems = null;
	private List<SelectItem> regTypeItems = null;

	private SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
	
	private Set<Integer> levelRowsToUpdate;
	
	private boolean bulkExpire = false;
	
	public SearchJurisdictionHandler getFilterHandler() {
		return filterHandler;
	}

	public void setFilterHandler(SearchJurisdictionHandler filterHandler) {
		this.filterHandler = filterHandler;
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		filterHandler.setJurisdictionService(matrixCommonBean.getJurisdictionService());
		filterHandler.setCacheManager(matrixCommonBean.getCacheManager());
	}

	public int getSelectedEntityIndex() {
		return selectedEntityIndex;
	}

	public void setSelectedEntityIndex(int selectedEntityIndex) {
		this.selectedEntityIndex = selectedEntityIndex;
	}

	public List<EntityItem> getEntityItemList() {
		if(this.entityItemList == null) {
			this.entityItemList = this.entityItemService.findAllEntityItemsByLevel(1L);
		}
		return entityItemList;
	}

	public void setEntityItemList(List<EntityItem> entityItemList) {
		this.entityItemList = entityItemList;
	}
	
	public void selectedEntityChanged(ActionEvent e) {
		HtmlDataTable table = (HtmlDataTable) ((HtmlAjaxSupport) e.getComponent()).getParent();
		this.selectedEntityItem = (EntityItem) table.getRowData();
		this.selectedEntityIndex = table.getRowIndex();
	}
	
	public void selectedStateChanged(ActionEvent e) {
		HtmlDataTable table = (HtmlDataTable) ((HtmlAjaxSupport) e.getComponent()).getParent();
		this.selectedState = (TaxCodeStateDTO) table.getRowData();
		this.selectedStateIndex = table.getRowIndex();
	}
	
	public void selectedNexusDefDetailChanged(ActionEvent e) {
		HtmlDataTable table = (HtmlDataTable) ((HtmlAjaxSupport) e.getComponent()).getParent();
		this.selectedDefinitionDetail = (NexusDefinitionDetail) table.getRowData();
		this.selectedDefinitionDetailIndex = table.getRowIndex();
	}
	
	//selectedRegistrationDetailIndex
	public void selectedRegistrationDetailChanged(ActionEvent e) {
		HtmlDataTable table = (HtmlDataTable) ((HtmlAjaxSupport) e.getComponent()).getParent();
		this.selectedRegistrationDetail = (RegistrationDetail) table.getRowData();
		this.selectedRegistrationDetailIndex = table.getRowIndex();
	}
	
	public boolean getValidSelection() {
		return this.selectedEntityItem != null;
	}
	
	public boolean getValidStateSelection() {
		return this.selectedState != null;
	}

	public EntityItem getSelectedEntityItem() {
		return selectedEntityItem;
	}

	public void setSelectedEntityItem(EntityItem selectedEntityItem) {
		this.selectedEntityItem = selectedEntityItem;
	}
	
	public boolean getRegistrationTask() {
		return (this.taskAction == TaskAction.REGISTRATION);
	}
	
	public void setSelectedEntityItemFromRegistration(EntityItem selectedEntityItem){
		setSelectedEntityItemNesuxMaintenance(selectedEntityItem, TaskAction.REGISTRATION);
	}
	
	public void setSelectedEntityItemFromDefinition(EntityItem selectedEntityItem){
		setSelectedEntityItemNesuxMaintenance(selectedEntityItem, TaskAction.DEFINITION);
	}
	
	public void setSelectedEntityItemNesuxMaintenance(EntityItem selectedEntityItem, TaskAction aTaskAction){
		this.selectedEntityItem = selectedEntityItem;
		
		taskAction = aTaskAction;
		
		if(viewFilterHandler == null) {
			viewFilterHandler = new SearchJurisdictionHandler();
		}
		else {
			viewFilterHandler.reset();
		}
		
		viewFilterHandler.setGeocode(this.selectedEntityItem.getGeocode());
		viewFilterHandler.setCountry(this.selectedEntityItem.getCountry());
		viewFilterHandler.setState(this.selectedEntityItem.getState());
		viewFilterHandler.setCounty(this.selectedEntityItem.getCounty());
		viewFilterHandler.setCity(this.selectedEntityItem.getCity());
		viewFilterHandler.setZip(this.selectedEntityItem.getZip());
		viewFilterHandler.setZipPlus4(this.selectedEntityItem.getZipPlus4());
		
		nexusDefStatesReturnView = "entity_main";
		
		this.selectedState = null;
		this.selectedStateIndex = -1;
	}
	
	public String displayUpdateAction() {
		taskAction = TaskAction.DEFINITION;
		
		if(viewFilterHandler == null) {
			viewFilterHandler = new SearchJurisdictionHandler();
		}
		else {
			viewFilterHandler.reset();
		}
		viewFilterHandler.setGeocode(this.selectedEntityItem.getGeocode());
		viewFilterHandler.setCountry(this.selectedEntityItem.getCountry());
		viewFilterHandler.setState(this.selectedEntityItem.getState());
		viewFilterHandler.setCounty(this.selectedEntityItem.getCounty());
		viewFilterHandler.setCity(this.selectedEntityItem.getCity());
		viewFilterHandler.setZip(this.selectedEntityItem.getZip());
		viewFilterHandler.setZipPlus4(this.selectedEntityItem.getZipPlus4());
		
		nexusDefStatesReturnView = "nexus_def_main";
		
		return "nexus_def_states";
	}
	
	public String nexusDefResetFilter() {
		this.filterActive = "-1";
		this.filterEntityCode = "";
		this.filterEntityName = "";
		this.filterHandler.reset();
		
		return null;
	}
	
	public String nexusDefSearchFilter() {
		this.selectedEntityIndex = -1;
		this.selectedEntityItem = null;
		
		EntityItem example = new EntityItem();
		example.setEntityLevelId(1L);
		
		if(StringUtils.hasText(this.filterEntityCode)) {
			example.setEntityCode(this.filterEntityCode);
		}
		
		if(StringUtils.hasText(this.filterEntityName)) {
			example.setEntityName(this.filterEntityName);
		}
		
		if(StringUtils.hasText(this.filterHandler.getGeocode())) {
			example.setGeocode(this.filterHandler.getGeocode());
		}
		
		if(StringUtils.hasText(this.filterHandler.getCountry())) {
			example.setCountry(this.filterHandler.getCountry());
		}
		
		if(StringUtils.hasText(this.filterHandler.getState())) {
			example.setState(this.filterHandler.getState());
		}
		
		if(StringUtils.hasText(this.filterHandler.getCounty())) {
			example.setCounty(this.filterHandler.getCounty());
		}
		
		if(StringUtils.hasText(this.filterHandler.getCity())) {
			example.setCity(this.filterHandler.getCity());
		}
		
		if(StringUtils.hasText(this.filterHandler.getZip())) {
			example.setZip(this.filterHandler.getZip());
		}
		
		if(StringUtils.hasText(this.filterHandler.getZipPlus4())) {
			example.setZipPlus4(this.filterHandler.getZipPlus4());
		}
		
		if(!"-1".equals(this.filterActive)) {
			example.setActiveFlag(this.filterActive);
		}
		
		this.entityItemList = entityItemService.findByExample(example, 0, new String[]{});
		
		return null;
	}

	public String getFilterEntityCode() {
		return filterEntityCode;
	}

	public void setFilterEntityCode(String filterEntityCode) {
		this.filterEntityCode = filterEntityCode;
	}

	public String getFilterEntityName() {
		return filterEntityName;
	}

	public void setFilterEntityName(String filterEntityName) {
		this.filterEntityName = filterEntityName;
	}

	public String getFilterActive() {
		return filterActive;
	}

	public void setFilterActive(String filterActive) {
		this.filterActive = filterActive;
	}

	public SearchJurisdictionHandler getViewFilterHandler() {
		return viewFilterHandler;
	}

	public void setViewFilterHandler(SearchJurisdictionHandler viewFilterHandler) {
		this.viewFilterHandler = viewFilterHandler;
	}

	public SearchJurisdictionHandler getStatesFilterHandler() {
		if(statesFilterHandler == null) {
			statesFilterHandler = new SearchJurisdictionHandler();
			statesFilterHandler.setJurisdictionService(matrixCommonBean.getJurisdictionService());
			statesFilterHandler.setCacheManager(matrixCommonBean.getCacheManager());
		}
		return statesFilterHandler;
	}

	public void setStatesFilterHandler(SearchJurisdictionHandler statesFilterHandler) {
		this.statesFilterHandler = statesFilterHandler;
	}

	public String getFilterStatesActive() {
		return filterStatesActive;
	}

	public void setFilterStatesActive(String filterStatesActive) {
		this.filterStatesActive = filterStatesActive;
	}
	
	public String nexusDefStatesResetFilter() {
		statesFilterHandler.reset();
		filterStatesActive = "";
		return null;
	}
	
	public String nexusDefStatesSearchFilter() {
		String country = null;
		String state = null;
		String activeFlag = null;
		
		if(StringUtils.hasText(statesFilterHandler.getCountry())) {
			country = statesFilterHandler.getCountry();
		}
		
		if(StringUtils.hasText(statesFilterHandler.getState()) && !"*ALL".equals(statesFilterHandler.getState())) {
			state = statesFilterHandler.getState();
		}
		
		if(!"-1".equals(filterStatesActive)) {
			activeFlag = filterStatesActive;
		}
		
		stateList = taxCodeStateService.findByCountryStateActive(country, state, activeFlag);
		stateList = removeStarAll(stateList);
		

		this.selectedState = null;
		this.selectedStateIndex = -1;
		
		return null;
	}

	public String getNexusDefStatesReturnView() {
		return nexusDefStatesReturnView;
	}

	public void setNexusDefStatesReturnView(String nexusDefStatesReturnView) {
		this.nexusDefStatesReturnView = nexusDefStatesReturnView;
	}

	public List<TaxCodeStateDTO> getStateList() {
		if(stateList == null) {
			stateList = taxCodeStateService.findByCountryStateActive(null, null, null);
			stateList = removeStarAll(stateList);
		}
		return stateList;
	}

	private List<TaxCodeStateDTO> removeStarAll(List<TaxCodeStateDTO> stateList) {
		Set<TaxCodeStateDTO> removeList = new HashSet<TaxCodeStateDTO>();
		for(TaxCodeStateDTO s : stateList) {
			if("*ALL".equals(s.getTaxCodeState())) {
				removeList.add(s);
			}
		}
		
		for(TaxCodeStateDTO s : removeList) {
			stateList.remove(s);
		}
		
		return stateList;
	}

	public void setStateList(List<TaxCodeStateDTO> stateList) {
		this.stateList = stateList;
	}

	public TaxCodeStateDTO getSelectedState() {
		return selectedState;
	}

	public void setSelectedState(TaxCodeStateDTO selectedState) {
		this.selectedState = selectedState;
	}

	public int getSelectedStateIndex() {
		return selectedStateIndex;
	}

	public void setSelectedStateIndex(int selectedStateIndex) {
		this.selectedStateIndex = selectedStateIndex;
	}
	
	public String statesCloseAction() {
		return nexusDefStatesReturnView;
	}
	
	private List<NexusJurisdictionItemDTO> buildList(List<Object[]> list) {
		List<NexusJurisdictionItemDTO> result = null;
		if(list != null) {
			result = new ArrayList<NexusJurisdictionItemDTO>();
			for(Object[] n : list) {
				NexusJurisdictionItemDTO sj = new NexusJurisdictionItemDTO();
				sj.setName((String) n[0]);
				if("1".equals(n[1] + "")) {
					sj.setHasNexus(true);
				}
				result.add(sj);
			}
		}
		
		return result;
	}
	
	private List<NexusJurisdictionItemDTO> buildRegList(List<Object[]> list) {
		List<NexusJurisdictionItemDTO> result = null;
		if(list != null) {
			result = new ArrayList<NexusJurisdictionItemDTO>();
			for(Object[] n : list) {
				NexusJurisdictionItemDTO sj = new NexusJurisdictionItemDTO();
				sj.setName((String) n[0]);
				String aFlag = (n[1]==null) ? "" : (String)n[1];
				if(aFlag.length()>0) {
					sj.setHasNexus(true);
				}
				result.add(sj);
			}
		}
		
		return result;
	}
	
	public String statesUpdateAction() {
		if(getRegistrationTask()){
			getJurisdictions();
			
			resetSelections();
			resetSelectionMaps();
			disableAllJurisdictionCheck();
			
			registrationDetailList = null;
			
			return "nexus_def_juris";
		}
		
		getJurisdictions();
		
		resetSelections();
		resetSelectionMaps();
		disableAllJurisdictionCheck();
		
		nexusDefDetailList = null;
		
		return "nexus_def_juris";
	}

	private void resetSelectionMaps() {
		this.selectedLevels.clear();
		this.selectedCounties.clear();
		this.selectedCities.clear();
		this.selectedStjs.clear();
	}

	private void getJurisdictions() {
		if(getRegistrationTask()){
			List<Object[]> list = jurisdictionService.findRegistrationJurisdiction(selectedEntityItem.getEntityId(), this.selectedState.getCountry(), this.selectedState.getTaxCodeState(), "I", "level");
			levelList = buildRegList(list);
			
			list = jurisdictionService.findRegistrationJurisdiction(selectedEntityItem.getEntityId(), this.selectedState.getCountry(), this.selectedState.getTaxCodeState(), "I", "county");
			countyList = buildRegList(list);
			
			list = jurisdictionService.findRegistrationJurisdiction(selectedEntityItem.getEntityId(), this.selectedState.getCountry(), this.selectedState.getTaxCodeState(), "I", "city");
			cityList = buildRegList(list);
			
			list = jurisdictionService.findRegistrationJurisdiction(selectedEntityItem.getEntityId(), this.selectedState.getCountry(), this.selectedState.getTaxCodeState(), "I", "stj");
			stjList = buildRegList(list);
		}
		else{
			List<Object[]> list = jurisdictionService.findNexusJurisdiction(selectedEntityItem.getEntityId(), this.selectedState.getCountry(), this.selectedState.getTaxCodeState(), "I", "level");
			levelList = buildList(list);
		
			list = jurisdictionService.findNexusJurisdiction(selectedEntityItem.getEntityId(), this.selectedState.getCountry(), this.selectedState.getTaxCodeState(), "I", "county");
			countyList = buildList(list);
		
			list = jurisdictionService.findNexusJurisdiction(selectedEntityItem.getEntityId(), this.selectedState.getCountry(), this.selectedState.getTaxCodeState(), "I", "city");
			cityList = buildList(list);
		
			list = jurisdictionService.findNexusJurisdiction(selectedEntityItem.getEntityId(), this.selectedState.getCountry(), this.selectedState.getTaxCodeState(), "I", "stj");
			stjList = buildList(list);
		}
	}
	
	public String statesDeleteAction() {
		return bulkExpireAction();
	}

	public NexusDefinitionDetail getSelectedDefinitionDetail() {
		return selectedDefinitionDetail;
	}

	public void setSelectedRegistrationDetail(
			RegistrationDetail selectedRegistrationDetail) {
		this.selectedRegistrationDetail = selectedRegistrationDetail;
	}
	
	public RegistrationDetail getSelectedRegistrationDetail() {
		return selectedRegistrationDetail;
	}

	public void setSelectedDefinitionDetail(
			NexusDefinitionDetail selectedDefinitionDetail) {
		this.selectedDefinitionDetail = selectedDefinitionDetail;
	}

	public int getSelectedDefinitionDetailIndex() {
		return selectedDefinitionDetailIndex;
	}

	public void setSelectedDefinitionDetailIndex(int selectedDefinitionDetailIndex) {
		this.selectedDefinitionDetailIndex = selectedDefinitionDetailIndex;
	}
	
	public int getSelectedRegistrationDetailIndex() {
		return selectedRegistrationDetailIndex;
	}

	public void setSelectedRegistrationDetailIndex(int selectedRegistrationDetailIndex) {
		this.selectedRegistrationDetailIndex = selectedRegistrationDetailIndex;
	}

	public List<NexusDefinitionDetail> getNexusDefDetailList() {
		return nexusDefDetailList;
	}

	public void setNexusDefDetailList(List<NexusDefinitionDetail> nexusDefDetailList) {
		this.nexusDefDetailList = nexusDefDetailList;
	}
	
	public List<RegistrationDetail> getRegistrationDetailList() {
		return registrationDetailList;
	}

	public void setRegistrationDetailList(List<RegistrationDetail> registrationDetailList) {
		this.registrationDetailList = registrationDetailList;
	}

	public List<NexusJurisdictionItemDTO> getCountyList() {
		return countyList;
	}

	public void setCountyList(List<NexusJurisdictionItemDTO> countyList) {
		this.countyList = countyList;
	}

	public List<NexusJurisdictionItemDTO> getCityList() {
		return cityList;
	}

	public void setCityList(List<NexusJurisdictionItemDTO> cityList) {
		this.cityList = cityList;
	}

	public List<NexusJurisdictionItemDTO> getStjList() {
		return stjList;
	}

	public void setStjList(List<NexusJurisdictionItemDTO> stjList) {
		this.stjList = stjList;
	}
	
	public void levelCheckboxSelected(ActionEvent e) throws AbortProcessingException {
		HtmlSelectBooleanCheckbox chk = (HtmlSelectBooleanCheckbox) ((HtmlAjaxSupport) e.getComponent()).getParent();
		HtmlDataTable table = (HtmlDataTable) chk.getParent().getParent();
		NexusJurisdictionItemDTO dto = (NexusJurisdictionItemDTO) table.getRowData();
		
		if((Boolean) chk.getValue()) {
			this.selectedLevels.put(dto.getName(), dto);
			
			if("All State".equalsIgnoreCase(dto.getName())) {
				for(NexusJurisdictionItemDTO nj : levelList) {
					nj.setSelected(true);
					nj.setEnabled(true);
					this.selectedLevels.put(nj.getName(), nj);
				}
				
				if(countyList != null) {
					for(NexusJurisdictionItemDTO nj : countyList) {
						nj.setSelected(true);
						nj.setEnabled(true);
						this.selectedCounties.put(nj.getName(), nj);
					}
				}
				
				if(cityList != null) {
					for(NexusJurisdictionItemDTO nj : cityList) {
						nj.setSelected(true);
						nj.setEnabled(true);
						this.selectedCities.put(nj.getName(), nj);
					}
				}
				
				if(stjList != null) {
					for(NexusJurisdictionItemDTO nj : stjList) {
						nj.setSelected(true);
						nj.setEnabled(true);
						this.selectedStjs.put(nj.getName(), nj);
					}
				}
			}
			else if("All Counties".equalsIgnoreCase(dto.getName())) {
				if(countyList != null) {
					for(NexusJurisdictionItemDTO nj : countyList) {
						nj.setSelected(true);
						this.selectedCounties.put(nj.getName(), nj);
					}
				}
			}
			else if("All Cities".equalsIgnoreCase(dto.getName())) {
				if(cityList != null) {
					for(NexusJurisdictionItemDTO nj : cityList) {
						nj.setSelected(true);
						this.selectedCities.put(nj.getName(), nj);
					}
				}
			}
			else if("All Stjs".equalsIgnoreCase(dto.getName())) {
				if(stjList != null) {
					for(NexusJurisdictionItemDTO nj : stjList) {
						nj.setSelected(true);
						this.selectedStjs.put(nj.getName(), nj);
					}
				}
			}
		}
		else {
			this.selectedLevels.remove(dto.getName());
			
			//Uncheck all if All State uncheck
			if("All State".equalsIgnoreCase(dto.getName())) {
				disableAllJurisdictionCheck();
			}
		}
	}
	
	private void disableAllJurisdictionCheck(){
		for(NexusJurisdictionItemDTO nj : levelList) {
			nj.setSelected(false);
			if("All State".equalsIgnoreCase(nj.getName())){
				nj.setEnabled(true); //Only exception
			}
			else{
				nj.setEnabled(false);
			}
			this.selectedLevels.remove(nj.getName());
		}
		
		if(countyList != null) {
			for(NexusJurisdictionItemDTO nj : countyList) {
				nj.setSelected(false);
				nj.setEnabled(false);
				this.selectedCounties.remove(nj.getName());
			}
		}
		
		if(cityList != null) {
			for(NexusJurisdictionItemDTO nj : cityList) {
				nj.setSelected(false);
				nj.setEnabled(false);
				this.selectedCities.remove(nj.getName());
			}
		}
		
		if(stjList != null) {
			for(NexusJurisdictionItemDTO nj : stjList) {
				nj.setSelected(false);
				nj.setEnabled(false);
				this.selectedStjs.remove(nj.getName());
			}
		}
	}
	
	public void countyCheckboxSelected(ActionEvent e) throws AbortProcessingException {
		HtmlSelectBooleanCheckbox chk = (HtmlSelectBooleanCheckbox) ((HtmlAjaxSupport) e.getComponent()).getParent();
		HtmlDataTable table = (HtmlDataTable) chk.getParent().getParent();
		NexusJurisdictionItemDTO dto = (NexusJurisdictionItemDTO) table.getRowData();
		
		if((Boolean) chk.getValue()) {
			this.selectedCounties.put(dto.getName(), dto);
		}
		else {
			//levelList.get(0).setSelected(false);
			levelList.get(1).setSelected(false);
			
			//this.selectedLevels.remove("All State");
			this.selectedLevels.remove("All Counties");
			this.selectedCounties.remove(dto.getName());
		}		
	}
	
	public void cityCheckboxSelected(ActionEvent e) throws AbortProcessingException {
		HtmlSelectBooleanCheckbox chk = (HtmlSelectBooleanCheckbox) ((HtmlAjaxSupport) e.getComponent()).getParent();
		HtmlDataTable table = (HtmlDataTable) chk.getParent().getParent();
		NexusJurisdictionItemDTO dto = (NexusJurisdictionItemDTO) table.getRowData();
		
		if((Boolean) chk.getValue()) {
			this.selectedCities.put(dto.getName(), dto);
		}
		else {
			//levelList.get(0).setSelected(false);
			levelList.get(2).setSelected(false);
			
			//this.selectedLevels.remove("All State");
			this.selectedLevels.remove("All Cities");
			this.selectedCities.remove(dto.getName());
		}		
	}
	
	public void stjCheckboxSelected(ActionEvent e) throws AbortProcessingException {
		HtmlSelectBooleanCheckbox chk = (HtmlSelectBooleanCheckbox) ((HtmlAjaxSupport) e.getComponent()).getParent();
		HtmlDataTable table = (HtmlDataTable) chk.getParent().getParent();
		NexusJurisdictionItemDTO dto = (NexusJurisdictionItemDTO) table.getRowData();
		
		if((Boolean) chk.getValue()) {
			this.selectedStjs.put(dto.getName(), dto);
		}
		else {
			//levelList.get(0).setSelected(false);
			levelList.get(3).setSelected(false);
			
			//this.selectedLevels.remove("All State");
			this.selectedLevels.remove("All Stjs");
			this.selectedStjs.remove(dto.getName());
		}		
	}

	public List<NexusJurisdictionItemDTO> getLevelList() {
		return levelList;
	}

	public void setLevelList(List<NexusJurisdictionItemDTO> levelList) {
		this.levelList = levelList;
	}
	
	public void resetSelections() {
		this.selectedLevel = null;
		this.selectedLevelIndex = -1;
		this.selectedCounty = null;
		this.selectedCountyIndex = -1;
		this.selectedCity = null;
		this.selectedCityIndex = -1;
		this.selectedStj = null;
		this.selectedStjIndex = -1;
		this.selectedDefinitionDetail = null;
		this.selectedDefinitionDetailIndex = -1;
		this.selectedRegistrationDetail = null;
		this.selectedRegistrationDetailIndex = -1;
	}
	
	public void selectedLevelChanged(ActionEvent e) {
		resetSelections();
		HtmlDataTable table = (HtmlDataTable) ((HtmlAjaxSupport) e.getComponent()).getParent();
		this.selectedLevel = (NexusJurisdictionItemDTO) table.getRowData();
		this.selectedLevelIndex = table.getRowIndex();
		
		if(getRegistrationTask()){
			switch(this.selectedLevelIndex) {
			case 0:
				registrationDetailList  = registrationDetailService.findAllRegistrationDetails(selectedEntityItem.getEntityId(), this.selectedState.getCountry(), 
						this.selectedState.getTaxCodeState(), "*STATE", "*STATE", "*STATE", true);
				break;
			case 1:
				registrationDetailList  = registrationDetailService.findAllRegistrationDetails(selectedEntityItem.getEntityId(), this.selectedState.getCountry(), 
						this.selectedState.getTaxCodeState(), "*ALL", "*COUNTY", "*COUNTY", true);
				break;
			case 2:
				registrationDetailList  = registrationDetailService.findAllRegistrationDetails(selectedEntityItem.getEntityId(), this.selectedState.getCountry(), 
						this.selectedState.getTaxCodeState(), "*CITY", "*ALL", "*CITY", true);
				break;
			case 3:
				registrationDetailList  = registrationDetailService.findAllRegistrationDetails(selectedEntityItem.getEntityId(), this.selectedState.getCountry(), 
						this.selectedState.getTaxCodeState(), "*STJ", "*STJ", "*ALL", true);
				break;
			}		
			
			return;
		}

		
		switch(this.selectedLevelIndex) {
		case 0:
			nexusDefDetailList = nexusDefinitionDetailService.findAllNexusDefinitionDetails(selectedEntityItem.getEntityId(), this.selectedState.getCountry(), 
					this.selectedState.getTaxCodeState(), "*STATE", "*STATE", "*STATE", true);
			break;
		case 1:
			nexusDefDetailList = nexusDefinitionDetailService.findAllNexusDefinitionDetails(selectedEntityItem.getEntityId(), this.selectedState.getCountry(), 
					this.selectedState.getTaxCodeState(), "*ALL", "*COUNTY", "*COUNTY", true);
			break;
		case 2:
			nexusDefDetailList = nexusDefinitionDetailService.findAllNexusDefinitionDetails(selectedEntityItem.getEntityId(), this.selectedState.getCountry(), 
					this.selectedState.getTaxCodeState(), "*CITY", "*ALL", "*CITY", true);
			break;
		case 3:
			nexusDefDetailList = nexusDefinitionDetailService.findAllNexusDefinitionDetails(selectedEntityItem.getEntityId(), this.selectedState.getCountry(), 
					this.selectedState.getTaxCodeState(), "*STJ", "*STJ", "*ALL", true);
			break;
		}		
	}
	
	public void selectedCountyChanged(ActionEvent e) {
		resetSelections();
		HtmlDataTable table = (HtmlDataTable) ((HtmlAjaxSupport) e.getComponent()).getParent();
		this.selectedCounty = (NexusJurisdictionItemDTO) table.getRowData();
		this.selectedCountyIndex = table.getRowIndex();
		
		if(getRegistrationTask()){
			registrationDetailList = registrationDetailService.findAllRegistrationDetails(selectedEntityItem.getEntityId(), this.selectedState.getCountry(), 
					this.selectedState.getTaxCodeState(), this.selectedCounty.getName(), "*COUNTY", "*COUNTY", true);
			return; 
		}
		
		nexusDefDetailList = nexusDefinitionDetailService.findAllNexusDefinitionDetails(selectedEntityItem.getEntityId(), this.selectedState.getCountry(), 
				this.selectedState.getTaxCodeState(), this.selectedCounty.getName(), "*COUNTY", "*COUNTY", true);
	}

	public void selectedCityChanged(ActionEvent e) {
		resetSelections();
		HtmlDataTable table = (HtmlDataTable) ((HtmlAjaxSupport) e.getComponent()).getParent();
		this.selectedCity = (NexusJurisdictionItemDTO) table.getRowData();
		this.selectedCityIndex = table.getRowIndex();
		
		if(getRegistrationTask()){
			registrationDetailList = registrationDetailService.findAllRegistrationDetails(selectedEntityItem.getEntityId(), this.selectedState.getCountry(), 
					this.selectedState.getTaxCodeState(), "*CITY", this.selectedCity.getName(), "*CITY", true);
			return; 
		}
		
		nexusDefDetailList = nexusDefinitionDetailService.findAllNexusDefinitionDetails(selectedEntityItem.getEntityId(), this.selectedState.getCountry(), 
				this.selectedState.getTaxCodeState(), "*CITY", this.selectedCity.getName(), "*CITY", true);
	}
	
	public void selectedStjChanged(ActionEvent e) {
		resetSelections();
		HtmlDataTable table = (HtmlDataTable) ((HtmlAjaxSupport) e.getComponent()).getParent();
		this.selectedStj = (NexusJurisdictionItemDTO) table.getRowData();
		this.selectedStjIndex = table.getRowIndex();
		
		if(getRegistrationTask()){
			registrationDetailList = registrationDetailService.findAllRegistrationDetails(selectedEntityItem.getEntityId(), this.selectedState.getCountry(), 
					this.selectedState.getTaxCodeState(), "*STJ", "*STJ", this.selectedStj.getName(), true);
			return; 
		}
		
		nexusDefDetailList = nexusDefinitionDetailService.findAllNexusDefinitionDetails(selectedEntityItem.getEntityId(), this.selectedState.getCountry(), 
				this.selectedState.getTaxCodeState(), "*STJ", "*STJ", this.selectedStj.getName(), true);
	}

	public NexusJurisdictionItemDTO getSelectedLevel() {
		return selectedLevel;
	}

	public void setSelectedLevel(NexusJurisdictionItemDTO selectedLevel) {
		this.selectedLevel = selectedLevel;
	}

	public int getSelectedLevelIndex() {
		return selectedLevelIndex;
	}

	public void setSelectedLevelIndex(int selectedLevelIndex) {
		this.selectedLevelIndex = selectedLevelIndex;
	}

	public NexusJurisdictionItemDTO getSelectedCounty() {
		return selectedCounty;
	}

	public void setSelectedCounty(NexusJurisdictionItemDTO selectedCounty) {
		this.selectedCounty = selectedCounty;
	}

	public int getSelectedCountyIndex() {
		return selectedCountyIndex;
	}

	public void setSelectedCountyIndex(int selectedCountyIndex) {
		this.selectedCountyIndex = selectedCountyIndex;
	}

	public NexusJurisdictionItemDTO getSelectedCity() {
		return selectedCity;
	}

	public void setSelectedCity(NexusJurisdictionItemDTO selectedCity) {
		this.selectedCity = selectedCity;
	}

	public int getSelectedCityIndex() {
		return selectedCityIndex;
	}

	public void setSelectedCityIndex(int selectedCityIndex) {
		this.selectedCityIndex = selectedCityIndex;
	}

	public NexusJurisdictionItemDTO getSelectedStj() {
		return selectedStj;
	}

	public void setSelectedStj(NexusJurisdictionItemDTO selectedStj) {
		this.selectedStj = selectedStj;
	}

	public int getSelectedStjIndex() {
		return selectedStjIndex;
	}

	public void setSelectedStjIndex(int selectedStjIndex) {
		this.selectedStjIndex = selectedStjIndex;
	}
	
	public boolean getDisplayAdd() {
		return this.selectedLevel != null || this.selectedCounty != null || this.selectedCity != null || this.selectedStj != null || getJurisdictionChecked();
	}
	
	public String displayAddAction() {
		this.currentAction = EditAction.ADD;
		this.bulkExpire = false;
		
		this.selectedJurisdiction = new NexusJurisdictionDTO();
		this.selectedJurisdiction.setCountryCode(this.selectedState.getCountry());
		this.selectedJurisdiction.setCountryName(null);
		this.selectedJurisdiction.setStateCode(this.selectedState.getTaxCodeState());
		this.selectedJurisdiction.setStateName(this.selectedState.getName());
		if(this.selectedLevel != null) {
			switch(this.selectedLevelIndex) {
			case 0:
				this.selectedJurisdiction.setCounty("*STATE");
				this.selectedJurisdiction.setCity("*STATE");
				this.selectedJurisdiction.setStj("*STATE");
				break;
			case 1:
				this.selectedJurisdiction.setCounty("*ALL");
				this.selectedJurisdiction.setCity("*COUNTY");
				this.selectedJurisdiction.setStj("*COUNTY");
				break;
			case 2:
				this.selectedJurisdiction.setCounty("*CITY");
				this.selectedJurisdiction.setCity("*ALL");
				this.selectedJurisdiction.setStj("*CITY");
				break;
			case 3:
				this.selectedJurisdiction.setCounty("*STJ");
				this.selectedJurisdiction.setCity("*STJ");
				this.selectedJurisdiction.setStj("*ALL");
				break;
			}
		}
		else {
			if(this.selectedCounty != null) {
				this.selectedJurisdiction.setCounty(this.selectedCounty.getName());
				this.selectedJurisdiction.setCity("*COUNTY");
				this.selectedJurisdiction.setStj("*COUNTY");
			}
			else if(this.selectedCity != null) {
				this.selectedJurisdiction.setCounty("*CITY");
				this.selectedJurisdiction.setCity(this.selectedCity.getName());
				this.selectedJurisdiction.setStj("*CITY");				
			}
			else if(this.selectedStj != null) {
				this.selectedJurisdiction.setCounty("*STJ");
				this.selectedJurisdiction.setCity("*STJ");
				this.selectedJurisdiction.setStj(this.selectedStj.getName());
			}
		}
		
		if(getJurisdictionChecked()) {
			return displayCheckedAddAction();
		}
		
		if(getRegistrationTask()){
			Registration nd = new Registration();
			nd.setEntityId(this.selectedEntityItem.getEntityId());
			nd.setRegCountryCode(this.selectedJurisdiction.getCountryCode());
			nd.setRegStateCode(this.selectedJurisdiction.getStateCode());
			nd.setRegCounty(this.selectedJurisdiction.getCounty());
			nd.setRegCity(this.selectedJurisdiction.getCity());
			nd.setRegStj(this.selectedJurisdiction.getStj());
			
			List<Registration> nl = registrationService.findByExample(nd, 0);
			if(nl != null && nl.size() > 0) {
				this.selectedRegistration = nl.get(0);
			}
			else {
				this.selectedRegistration = nd;
			}
			
			this.selectedRegistrationDetail = new RegistrationDetail();
			this.selectedRegistrationDetail.setEffectiveDate(new com.ncsts.view.util.DateConverter().getUserLocalDate());
	//		this.selectedRegistrationDetail.setRegTypeCode(getDefNexusType());
			try {
				this.selectedRegistrationDetail.setExpirationDate(sdf.parse("12/31/9999"));
			} catch (ParseException e) {}
			this.selectedRegistrationDetail.setActiveFlag("1");
			
			return "nexus_def_add";
		}
		
	
		NexusDefinition nd = new NexusDefinition();
		nd.setEntityId(this.selectedEntityItem.getEntityId());
		nd.setNexusCountryCode(this.selectedJurisdiction.getCountryCode());
		nd.setNexusStateCode(this.selectedJurisdiction.getStateCode());
		nd.setNexusCounty(this.selectedJurisdiction.getCounty());
		nd.setNexusCity(this.selectedJurisdiction.getCity());
		nd.setNexusStj(this.selectedJurisdiction.getStj());
		
		List<NexusDefinition> nl = nexusDefinitionService.findByExample(nd, 0);
		if(nl != null && nl.size() > 0) {
			this.selectedNexusDefinition = nl.get(0);
		}
		else {
			this.selectedNexusDefinition = nd;
		}
		
		this.selectedNexusDefDetail = new NexusDefinitionDetail();
		this.selectedNexusDefDetail.setEffectiveDate(new com.ncsts.view.util.DateConverter().getUserLocalDate());
		this.selectedNexusDefDetail.setNexusTypeCode(getDefNexusType());
		try {
			this.selectedNexusDefDetail.setExpirationDate(sdf.parse("12/31/9999"));
		} catch (ParseException e) {}
		this.selectedNexusDefDetail.setActiveFlag("1");
		
		return "nexus_def_add";
	}

	private String displayCheckedAddAction() {
		if(getRegistrationTask()){
			this.selectedRegistrationDetail = new RegistrationDetail();
			this.selectedRegistrationDetail.setEffectiveDate(new com.ncsts.view.util.DateConverter().getUserLocalDate());
		//	this.selectedRegistrationDetail.setRegTypeCode(getDefNexusType());
			try {
				this.selectedRegistrationDetail.setExpirationDate(sdf.parse("12/31/9999"));
			} catch (ParseException e) {}
			this.selectedRegistrationDetail.setActiveFlag("1");
			
			return "nexus_def_add";
		}
		
		this.selectedNexusDefDetail = new NexusDefinitionDetail();
		this.selectedNexusDefDetail.setEffectiveDate(new com.ncsts.view.util.DateConverter().getUserLocalDate());
		this.selectedNexusDefDetail.setNexusTypeCode(getDefNexusType());
		try {
			this.selectedNexusDefDetail.setExpirationDate(sdf.parse("12/31/9999"));
		} catch (ParseException e) {}
		this.selectedNexusDefDetail.setActiveFlag("1");
		
		return "nexus_def_add";
	}

	public EditAction getCurrentAction() {
		return currentAction;
	}

	public void setCurrentAction(EditAction currentAction) {
		this.currentAction = currentAction;
	}

	public NexusJurisdictionDTO getSelectedJurisdiction() {
		return selectedJurisdiction;
	}

	public void setSelectedJurisdiction(NexusJurisdictionDTO selectedJurisdiction) {
		this.selectedJurisdiction = selectedJurisdiction;
	}
	
	public boolean getReadOnlyAction() {
		return this.currentAction.isReadOnlyAction();
	}

	public NexusDefinitionDetail getSelectedNexusDefDetail() {
		return selectedNexusDefDetail;
	}

	public void setSelectedNexusDefDetail(
			NexusDefinitionDetail selectedNexusDefDetail) {
		this.selectedNexusDefDetail = selectedNexusDefDetail;
	}

	public int getSelectedNexusDefDetailIndex() {
		return selectedNexusDefDetailIndex;
	}

	public void setSelectedNexusDefDetailIndex(int selectedNexusDefDetailIndex) {
		this.selectedNexusDefDetailIndex = selectedNexusDefDetailIndex;
	}
	
	public List<SelectItem> getNexusTypeItems() {
		if(nexusTypeItems == null) {
			nexusTypeItems = cacheManager.createNexusTypeItems();
		}
		
		return nexusTypeItems;
	}
	
	public List<SelectItem> getRegTypeItems() {
		if(regTypeItems == null) {
			regTypeItems = cacheManager.createRegistrationTypeItems();
		}
		
		return regTypeItems;
	}
	
	public String getDefNexusType() {
		Option option = optionService.findByPK(new OptionCodePK ("DEFAULTNEXUSTYPE", "SYSTEM", "SYSTEM"));
		return option.getValue();
	}
	
	private SimpleDateFormat maxDate = new SimpleDateFormat("MM/dd/yyyy");
	
	public void validateExpirationDate(FacesContext context, UIComponent toValidate, Object value) {
	    // Ensure date > effectiveDate
		Date expDate = (Date) value;

		Date maxExpDate = null;
		try {
			maxExpDate = maxDate.parse("12/31/9999");
		} catch (ParseException e) {}

		if (expDate.after(maxExpDate)) {
			((UIInput)toValidate).setValid(false);
			FacesMessage message = new FacesMessage("Expiration Date: could not be understood as a date.");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage(toValidate.getClientId(context), message);
	    }
	}
	
	public void validateEffectiveDate(FacesContext context, UIComponent toValidate, Object value) {
	    // Ensure date > effectiveDate
		Date expDate = (Date) value;

		Date maxExpDate = null;
		try {
			maxExpDate = maxDate.parse("12/31/9999");
		} catch (ParseException e) {}

		if (expDate.after(maxExpDate)) {
			((UIInput)toValidate).setValid(false);
			FacesMessage message = new FacesMessage("Effective Date: could not be understood as a date.");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage(toValidate.getClientId(context), message);
	    }
	}
	
	public String okAddRegistrationAction() {
		if(this.currentAction.equals(EditAction.DELETE)) {
			if(this.bulkExpire) {
				return okBulkExpireRegistrationAction();
			}
			else {
				return okDeleteRegistrationAction();
			}
		}
		
		if(this.selectedRegistrationDetail.getEffectiveDate() == null) {
			FacesMessage message = new FacesMessage("Effective Date is required.");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
			return null;
		}
		else if(this.selectedRegistrationDetail.getExpirationDate() == null) {
			FacesMessage message = new FacesMessage("Expiration Date is required.");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
			return null;
		}
		else if(this.selectedRegistrationDetail.getExpirationDate().getTime() < this.selectedRegistrationDetail.getEffectiveDate().getTime()) {
			FacesMessage message = new FacesMessage("Expiration Date must be equal or greater than the Effective Date.");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
			return null;
		}
		
		if(getJurisdictionChecked()) {
			return okCheckedAddRegistrationAction();
		}
		
		if(this.selectedRegistration.getRegistrationId() == null) {
			//this.selectedRegistration.setNexusFlag("1");
			try {
				registrationService.save(selectedRegistration);
			}
			catch(Exception e) {
				FacesMessage message = new FacesMessage("Error saving Registration: " + e.getMessage());
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage(null, message);
				return null;
			}
		}
		else {
		
			//this.selectedRegistration.setNexusFlag("1");
			try {
				registrationService.update(selectedRegistration);
			}
			catch(Exception e) {
				FacesMessage message = new FacesMessage("Error updating Nexus Definition: " + e.getMessage());
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage(null, message);
				return null;
			}	
		}
		
		this.selectedRegistrationDetail.setRegistrationId(this.selectedRegistration.getRegistrationId());
		try {
			registrationDetailService.save(selectedRegistrationDetail);
		}
		catch(Exception e) {
			FacesMessage message = new FacesMessage("Error saving Registration Detail: " + e.getMessage());
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
			return null;
		}
		
		getJurisdictions();
		
		registrationDetailList = registrationDetailService.findAllRegistrationDetails(selectedEntityItem.getEntityId(), this.selectedJurisdiction.getCountryCode(), 
				this.selectedJurisdiction.getStateCode(), this.selectedJurisdiction.getCounty(), this.selectedJurisdiction.getCity(), 
				this.selectedJurisdiction.getStj(), true);
		
		this.selectedRegistrationDetail = null;
		this.selectedRegistrationDetailIndex = -1;
		
		resetSelectionMaps();
		disableAllJurisdictionCheck();
		
		return "nexus_def_juris";
	}
	
	public String okAddAction() {
		if(getRegistrationTask()){
			return okAddRegistrationAction();
		}
			
		if(this.currentAction.equals(EditAction.DELETE)) {
			if(this.bulkExpire) {
				return okBulkExpireAction();
			}
			else {
				return okDeleteAction();
			}
		}
		
		if(this.selectedNexusDefDetail.getEffectiveDate() == null) {
			FacesMessage message = new FacesMessage("Effective Date is required.");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
			return null;
		}
		else if(this.selectedNexusDefDetail.getExpirationDate() == null) {
			FacesMessage message = new FacesMessage("Expiration Date is required.");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
			return null;
		}
		else if(this.selectedNexusDefDetail.getExpirationDate().getTime() < this.selectedNexusDefDetail.getEffectiveDate().getTime()) {
			FacesMessage message = new FacesMessage("Expiration Date must be equal or greater than the Effective Date.");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
			return null;
		}
		
		if(getJurisdictionChecked()) {
			return okCheckedAddAction();
		}
		
		if(this.selectedNexusDefinition.getNexusDefId() == null) {
			this.selectedNexusDefinition.setNexusFlag("1");
			try {
				nexusDefinitionService.save(selectedNexusDefinition);
			}
			catch(Exception e) {
				FacesMessage message = new FacesMessage("Error saving Nexus Definition: " + e.getMessage());
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage(null, message);
				return null;
			}
		}
		else {
			if(!"1".equals(this.selectedNexusDefinition.getNexusFlag())) {
				this.selectedNexusDefinition.setNexusFlag("1");
				try {
					nexusDefinitionService.update(selectedNexusDefinition);
				}
				catch(Exception e) {
					FacesMessage message = new FacesMessage("Error updating Nexus Definition: " + e.getMessage());
					message.setSeverity(FacesMessage.SEVERITY_ERROR);
					FacesContext.getCurrentInstance().addMessage(null, message);
					return null;
				}
			}
		}
		
		this.selectedNexusDefDetail.setNexusDefId(this.selectedNexusDefinition.getNexusDefId());
		try {
			nexusDefinitionDetailService.save(selectedNexusDefDetail);
		}
		catch(Exception e) {
			FacesMessage message = new FacesMessage("Error saving Nexus Definition Detail: " + e.getMessage());
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
			return null;
		}
		
		getJurisdictions();
		
		nexusDefDetailList = nexusDefinitionDetailService.findAllNexusDefinitionDetails(selectedEntityItem.getEntityId(), this.selectedJurisdiction.getCountryCode(), 
				this.selectedJurisdiction.getStateCode(), this.selectedJurisdiction.getCounty(), this.selectedJurisdiction.getCity(), 
				this.selectedJurisdiction.getStj(), true);
		
		resetSelectionMaps();
		disableAllJurisdictionCheck();
		
		return "nexus_def_juris";
	}
	
	private String okBulkExpireAction() {
		if(this.selectedNexusDefDetail.getExpirationDate() == null) {
			FacesMessage message = new FacesMessage("Expiration Date is required.");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
			return null;
		}
		
		try {
			nexusDefinitionDetailService.bulkExpireNexusDefinitionDetail(this.selectedJurisdiction.getCountryCode(), 
					this.selectedJurisdiction.getStateCode(), this.selectedNexusDefDetail.getExpirationDate());
		}
		catch(Exception e) {
			FacesMessage message = new FacesMessage("Error bulk expiring Nexus Definitions: " + e.getMessage());
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
			return null;
		}
		
		return "nexus_def_states";
	}
	
	private String okBulkExpireRegistrationAction() {
		if(this.selectedRegistrationDetail.getExpirationDate() == null) {
			FacesMessage message = new FacesMessage("Expiration Date is required.");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
			return null;
		}
		
		try {
			nexusDefinitionDetailService.bulkExpireRegistrationDetail(this.selectedJurisdiction.getCountryCode(), 
					this.selectedJurisdiction.getStateCode(), this.selectedRegistrationDetail.getExpirationDate());
		}
		catch(Exception e) {
			FacesMessage message = new FacesMessage("Error bulk expiring Registrations: " + e.getMessage());
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
			return null;
		}
		
		return "nexus_def_states";
	}

	private String okDeleteAction() {
		this.selectedNexusDefDetail.setActiveFlag("0");
		try {
			nexusDefinitionDetailService.update(selectedNexusDefDetail);
		}
		catch(Exception e) {
			FacesMessage message = new FacesMessage("Error updating Nexus Definition Detail: " + e.getMessage());
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
			return null;
		}
		
		NexusDefinitionDetail ndd = new NexusDefinitionDetail();
		ndd.setNexusDefId(this.selectedNexusDefDetail.getNexusDefId());
		ndd.setActiveFlag("1");
		
		List<NexusDefinitionDetail> list = nexusDefinitionDetailService.findByExample(ndd, 0);
		if(list == null || list.size() == 0) {
			NexusDefinition nd = nexusDefinitionService.findById(this.selectedNexusDefDetail.getNexusDefId());
			if(nd != null) {
				nd.setNexusFlag("0");
				try {
					nexusDefinitionService.update(nd);
				}
				catch(Exception e) {
					FacesMessage message = new FacesMessage("Error updating Nexus Definition: " + e.getMessage());
					message.setSeverity(FacesMessage.SEVERITY_ERROR);
					FacesContext.getCurrentInstance().addMessage(null, message);
					return null;
				}
			}
		}
		
		getJurisdictions();
		
		nexusDefDetailList = nexusDefinitionDetailService.findAllNexusDefinitionDetails(selectedEntityItem.getEntityId(), this.selectedJurisdiction.getCountryCode(), 
				this.selectedJurisdiction.getStateCode(), this.selectedJurisdiction.getCounty(), this.selectedJurisdiction.getCity(), 
				this.selectedJurisdiction.getStj(), true);
		
		resetSelectionMaps();
		disableAllJurisdictionCheck();
		
		return "nexus_def_juris";
	}
	
	private String okDeleteRegistrationAction() {
		this.selectedRegistrationDetail.setActiveFlag("0");
		try {
			registrationDetailService.remove(selectedRegistrationDetail);
		}
		catch(Exception e) {
			FacesMessage message = new FacesMessage("Error updating Registration Detail: " + e.getMessage());
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
			return null;
		}
		
		RegistrationDetail ndd = new RegistrationDetail();
		ndd.setRegistrationDetailId(this.selectedRegistrationDetail.getRegistrationDetailId());
		ndd.setActiveFlag("1");
		
		List<RegistrationDetail> list = registrationDetailService.findByExample(ndd, 0);
		if(list == null || list.size() == 0) {
			Registration nd = registrationService.findById(this.selectedRegistrationDetail.getRegistrationId());
			if(nd != null) {
				//nd.setNexusFlag("0");
				try {
					registrationService.update(nd);
				}
				catch(Exception e) {
					FacesMessage message = new FacesMessage("Error updating Registration: " + e.getMessage());
					message.setSeverity(FacesMessage.SEVERITY_ERROR);
					FacesContext.getCurrentInstance().addMessage(null, message);
					return null;
				}
			}
		}
		
		getJurisdictions();
		
		registrationDetailList = registrationDetailService.findAllRegistrationDetails(selectedEntityItem.getEntityId(), this.selectedJurisdiction.getCountryCode(), 
				this.selectedJurisdiction.getStateCode(), this.selectedJurisdiction.getCounty(), this.selectedJurisdiction.getCity(), 
				this.selectedJurisdiction.getStj(), true);
		
		this.selectedRegistrationDetail = null;
		this.selectedRegistrationDetailIndex = -1;
		
		resetSelectionMaps();
		disableAllJurisdictionCheck();
		
		return "nexus_def_juris";
	}

	private String okCheckedAddAction() {
		boolean hasStateAll = false;
		boolean hasCountiesAll = false;
		boolean hasCitiesAll = false;
		boolean hasStjsAll = false;
		
		if(this.selectedLevels.size() > 0) {
			for(Map.Entry<String, NexusJurisdictionItemDTO> e : this.selectedLevels.entrySet()) {
				NexusDefinition nd = new NexusDefinition();
				
				if("All State".equals(e.getKey())) {
					nd.setEntityId(this.selectedEntityItem.getEntityId());
					nd.setNexusCountryCode(this.selectedJurisdiction.getCountryCode());
					nd.setNexusStateCode(this.selectedJurisdiction.getStateCode());
					nd.setNexusCounty("*STATE");
					nd.setNexusCity("*STATE");
					nd.setNexusStj("*STATE");
					
					hasStateAll = false;
				}
				else if("All Counties".equals(e.getKey())) {
					nd.setEntityId(this.selectedEntityItem.getEntityId());
					nd.setNexusCountryCode(this.selectedJurisdiction.getCountryCode());
					nd.setNexusStateCode(this.selectedJurisdiction.getStateCode());
					nd.setNexusCounty("*ALL");
					nd.setNexusCity("*COUNTY");
					nd.setNexusStj("*COUNTY");
					
					hasCountiesAll = true;
				}
				else if("All Cities".equals(e.getKey())) {
					nd.setEntityId(this.selectedEntityItem.getEntityId());
					nd.setNexusCountryCode(this.selectedJurisdiction.getCountryCode());
					nd.setNexusStateCode(this.selectedJurisdiction.getStateCode());
					nd.setNexusCounty("*CITY");
					nd.setNexusCity("*ALL");
					nd.setNexusStj("*CITY");
					
					hasCitiesAll = true;
				}
				else if("All Stjs".equals(e.getKey())) {
					nd.setEntityId(this.selectedEntityItem.getEntityId());
					nd.setNexusCountryCode(this.selectedJurisdiction.getCountryCode());
					nd.setNexusStateCode(this.selectedJurisdiction.getStateCode());
					nd.setNexusCounty("*STJ");
					nd.setNexusCity("*STJ");
					nd.setNexusStj("*ALL");
					
					hasStjsAll = true;
				}
				else {
					continue;
				}
					
				String result = saveNexusDefitionDetail(nd);
				if(result == null) {
					return null;
				}
			}
		}
		
		if(!hasStateAll && !hasCountiesAll && this.selectedCounties.size() > 0) {
			for(Map.Entry<String, NexusJurisdictionItemDTO> e : this.selectedCounties.entrySet()) {
				NexusDefinition nd = new NexusDefinition();
				nd.setEntityId(this.selectedEntityItem.getEntityId());
				nd.setNexusCountryCode(this.selectedJurisdiction.getCountryCode());
				nd.setNexusStateCode(this.selectedJurisdiction.getStateCode());
				nd.setNexusCounty(e.getValue().getName());
				nd.setNexusCity("*COUNTY");
				nd.setNexusStj("*COUNTY");
				
				String result = saveNexusDefitionDetail(nd);
				if(result == null) {
					return null;
				}
			}
		}
		
		if(!hasStateAll && !hasCitiesAll && this.selectedCities.size() > 0) {
			for(Map.Entry<String, NexusJurisdictionItemDTO> e : this.selectedCities.entrySet()) {
				NexusDefinition nd = new NexusDefinition();
				nd.setEntityId(this.selectedEntityItem.getEntityId());
				nd.setNexusCountryCode(this.selectedJurisdiction.getCountryCode());
				nd.setNexusStateCode(this.selectedJurisdiction.getStateCode());
				nd.setNexusCounty("*CITY");
				nd.setNexusCity(e.getValue().getName());
				nd.setNexusStj("*CITY");
				
				String result = saveNexusDefitionDetail(nd);
				if(result == null) {
					return null;
				}
			}
		}
		
		if(!hasStateAll && !hasStjsAll && this.selectedStjs.size() > 0) {
			for(Map.Entry<String, NexusJurisdictionItemDTO> e : this.selectedStjs.entrySet()) {
				NexusDefinition nd = new NexusDefinition();
				nd.setEntityId(this.selectedEntityItem.getEntityId());
				nd.setNexusCountryCode(this.selectedJurisdiction.getCountryCode());
				nd.setNexusStateCode(this.selectedJurisdiction.getStateCode());
				nd.setNexusCounty("*STJ");
				nd.setNexusCity("*STJ");
				nd.setNexusStj(e.getValue().getName());
				
				String result = saveNexusDefitionDetail(nd);
				if(result == null) {
					return null;
				}
			}
		}
		
		getJurisdictions();
		
		nexusDefDetailList = nexusDefinitionDetailService.findAllNexusDefinitionDetails(selectedEntityItem.getEntityId(), this.selectedJurisdiction.getCountryCode(), 
				this.selectedJurisdiction.getStateCode(), this.selectedJurisdiction.getCounty(), this.selectedJurisdiction.getCity(), 
				this.selectedJurisdiction.getStj(), true);
		
		resetSelectionMaps();
		disableAllJurisdictionCheck();
		
		return "nexus_def_juris";
	}
	
	private String okCheckedAddRegistrationAction() {
		boolean hasStateAll = false;
		boolean hasCountiesAll = false;
		boolean hasCitiesAll = false;
		boolean hasStjsAll = false;
		
		if(this.selectedLevels.size() > 0) {
			for(Map.Entry<String, NexusJurisdictionItemDTO> e : this.selectedLevels.entrySet()) {
				Registration nd = new Registration();
				
				if("All State".equals(e.getKey())) {
					nd.setEntityId(this.selectedEntityItem.getEntityId());
					nd.setRegCountryCode(this.selectedJurisdiction.getCountryCode());
					nd.setRegStateCode(this.selectedJurisdiction.getStateCode());
					nd.setRegCounty("*STATE");
					nd.setRegCity("*STATE");
					nd.setRegStj("*STATE");
					
					hasStateAll = false;
				}
				else if("All Counties".equals(e.getKey())) {
					nd.setEntityId(this.selectedEntityItem.getEntityId());
					nd.setRegCountryCode(this.selectedJurisdiction.getCountryCode());
					nd.setRegStateCode(this.selectedJurisdiction.getStateCode());
					nd.setRegCounty("*ALL");
					nd.setRegCity("*COUNTY");
					nd.setRegStj("*COUNTY");
					
					hasCountiesAll = true;
				}
				else if("All Cities".equals(e.getKey())) {
					nd.setEntityId(this.selectedEntityItem.getEntityId());
					nd.setRegCountryCode(this.selectedJurisdiction.getCountryCode());
					nd.setRegStateCode(this.selectedJurisdiction.getStateCode());
					nd.setRegCounty("*CITY");
					nd.setRegCity("*ALL");
					nd.setRegStj("*CITY");
					
					hasCitiesAll = true;
				}
				else if("All Stjs".equals(e.getKey())) {
					nd.setEntityId(this.selectedEntityItem.getEntityId());
					nd.setRegCountryCode(this.selectedJurisdiction.getCountryCode());
					nd.setRegStateCode(this.selectedJurisdiction.getStateCode());
					nd.setRegCounty("*STJ");
					nd.setRegCity("*STJ");
					nd.setRegStj("*ALL");
					
					hasStjsAll = true;
				}
				else {
					continue;
				}
					
				String result = saveRegistrationDetail(nd);
				if(result == null) {
					return null;
				}
			}
		}
		
		if(!hasStateAll && !hasCountiesAll && this.selectedCounties.size() > 0) {
			for(Map.Entry<String, NexusJurisdictionItemDTO> e : this.selectedCounties.entrySet()) {
				Registration nd = new Registration();
				nd.setEntityId(this.selectedEntityItem.getEntityId());
				nd.setRegCountryCode(this.selectedJurisdiction.getCountryCode());
				nd.setRegStateCode(this.selectedJurisdiction.getStateCode());
				nd.setRegCounty(e.getValue().getName());
				nd.setRegCity("*COUNTY");
				nd.setRegStj("*COUNTY");
				
				String result = saveRegistrationDetail(nd);
				if(result == null) {
					return null;
				}
			}
		}
		
		if(!hasStateAll && !hasCitiesAll && this.selectedCities.size() > 0) {
			for(Map.Entry<String, NexusJurisdictionItemDTO> e : this.selectedCities.entrySet()) {
				Registration nd = new Registration();
				nd.setEntityId(this.selectedEntityItem.getEntityId());
				nd.setRegCountryCode(this.selectedJurisdiction.getCountryCode());
				nd.setRegStateCode(this.selectedJurisdiction.getStateCode());
				nd.setRegCounty("*CITY");
				nd.setRegCity(e.getValue().getName());
				nd.setRegStj("*CITY");
				
				String result = saveRegistrationDetail(nd);
				if(result == null) {
					return null;
				}
			}
		}
		
		if(!hasStateAll && !hasStjsAll && this.selectedStjs.size() > 0) {
			for(Map.Entry<String, NexusJurisdictionItemDTO> e : this.selectedStjs.entrySet()) {
				Registration nd = new Registration();
				nd.setEntityId(this.selectedEntityItem.getEntityId());
				nd.setRegCountryCode(this.selectedJurisdiction.getCountryCode());
				nd.setRegStateCode(this.selectedJurisdiction.getStateCode());
				nd.setRegCounty("*STJ");
				nd.setRegCity("*STJ");
				nd.setRegStj(e.getValue().getName());
				
				String result = saveRegistrationDetail(nd);
				if(result == null) {
					return null;
				}
			}
		}
		
		getJurisdictions();
		
		registrationDetailList = registrationDetailService.findAllRegistrationDetails(selectedEntityItem.getEntityId(), this.selectedJurisdiction.getCountryCode(), 
				this.selectedJurisdiction.getStateCode(), this.selectedJurisdiction.getCounty(), this.selectedJurisdiction.getCity(), 
				this.selectedJurisdiction.getStj(), true);
		
		this.selectedRegistrationDetail = null;
		this.selectedRegistrationDetailIndex = -1;
		
		resetSelectionMaps();
		disableAllJurisdictionCheck();
		
		return "nexus_def_juris";
	}
	
	private String saveNexusDefitionDetail(NexusDefinition nd) {
		List<NexusDefinition> nl = nexusDefinitionService.findByExample(nd, 0);
		if(nl != null && nl.size() > 0) {
			nd = nl.get(0);
			nd.setNexusFlag("1");
			try {
				nexusDefinitionService.update(nd);
			}
			catch(Exception ex) {
				FacesMessage message = new FacesMessage("Error updating Nexus Definition: " + ex.getMessage());
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage(null, message);
				return null;
			}
		}
		else {
			nd.setNexusFlag("1");
			try {
				nexusDefinitionService.save(nd);
			}
			catch(Exception ex) {
				FacesMessage message = new FacesMessage("Error saving Nexus Definition: " + ex.getMessage());
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage(null, message);
				return null;
			}
		}
		
		NexusDefinitionDetail ndd = new NexusDefinitionDetail();
		BeanUtils.copyProperties(this.selectedNexusDefDetail, ndd);
		ndd.setNexusDefId(nd.getNexusDefId());
		
		try {
			nexusDefinitionDetailService.save(ndd);
		}
		catch(Exception ex) {
			FacesMessage message = new FacesMessage("Error saving Nexus Definition Detail: " + ex.getMessage());
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
			return null;
		}
		
		return "saved";
	}
	
	private String saveRegistrationDetail(Registration nd) {
		List<Registration> nl = registrationService.findByExample(nd, 0);
		if(nl != null && nl.size() > 0) {
			nd = nl.get(0);
		//	nd.setNexusFlag("1");
			try {
				registrationService.update(nd);
			}
			catch(Exception ex) {
				FacesMessage message = new FacesMessage("Error updating Registration: " + ex.getMessage());
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage(null, message);
				return null;
			}
		}
		else {
		//	nd.setNexusFlag("1");
			try {
				registrationService.save(nd);
			}
			catch(Exception ex) {
				FacesMessage message = new FacesMessage("Error saving Nexus Registration: " + ex.getMessage());
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage(null, message);
				return null;
			}
		}
		
		RegistrationDetail ndd = new RegistrationDetail();
		BeanUtils.copyProperties(this.selectedRegistrationDetail, ndd);
		ndd.setRegistrationId(nd.getRegistrationId());
		
		try {
			registrationDetailService.save(ndd);
		}
		catch(Exception ex) {
			FacesMessage message = new FacesMessage("Error saving Registration Detail: " + ex.getMessage());
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
			return null;
		}
		
		return "saved";
	}

	public String cancelAction() {
		return "nexus_def_juris";
	}
	
	public String jurisCloseAction() {
		return "nexus_def_states";
	}

	public Set<Integer> getLevelRowsToUpdate() {
		if(levelRowsToUpdate == null) {
			levelRowsToUpdate = new HashSet<Integer>(4);
			levelRowsToUpdate.add(0);
			levelRowsToUpdate.add(1);
			levelRowsToUpdate.add(2);
			levelRowsToUpdate.add(3);
		}
		return levelRowsToUpdate;
	}
	
	public boolean getJurisdictionChecked() {
		return this.selectedLevels.size() > 0 
			|| this.selectedCounties.size() > 0 
			|| this.selectedCities.size() > 0 
			|| this.selectedStjs.size() > 0;
	}
	
	public boolean getDisplayDelete() {
		if(getRegistrationTask()){
			return this.selectedRegistrationDetail != null;
		}
		
		return this.selectedDefinitionDetail != null;
	}
	
	public String displayDeleteAction() {
		this.currentAction = EditAction.DELETE;
		this.bulkExpire = false;
		
		this.selectedJurisdiction = new NexusJurisdictionDTO();
		this.selectedJurisdiction.setCountryCode(this.selectedState.getCountry());
		this.selectedJurisdiction.setCountryName(null);
		this.selectedJurisdiction.setStateCode(this.selectedState.getTaxCodeState());
		this.selectedJurisdiction.setStateName(this.selectedState.getName());
		if(this.selectedLevel != null) {
			switch(this.selectedLevelIndex) {
			case 0:
				this.selectedJurisdiction.setCounty("*STATE");
				this.selectedJurisdiction.setCity("*STATE");
				this.selectedJurisdiction.setStj("*STATE");
				break;
			case 1:
				this.selectedJurisdiction.setCounty("*ALL");
				this.selectedJurisdiction.setCity("*COUNTY");
				this.selectedJurisdiction.setStj("*COUNTY");
				break;
			case 2:
				this.selectedJurisdiction.setCounty("*CITY");
				this.selectedJurisdiction.setCity("*ALL");
				this.selectedJurisdiction.setStj("*CITY");
				break;
			case 3:
				this.selectedJurisdiction.setCounty("*STJ");
				this.selectedJurisdiction.setCity("*STJ");
				this.selectedJurisdiction.setStj("*ALL");
				break;
			}
		}
		else {
			if(this.selectedCounty != null) {
				this.selectedJurisdiction.setCounty(this.selectedCounty.getName());
				this.selectedJurisdiction.setCity("*COUNTY");
				this.selectedJurisdiction.setStj("*COUNTY");
			}
			else if(this.selectedCity != null) {
				this.selectedJurisdiction.setCounty("*CITY");
				this.selectedJurisdiction.setCity(this.selectedCity.getName());
				this.selectedJurisdiction.setStj("*CITY");				
			}
			else if(this.selectedStj != null) {
				this.selectedJurisdiction.setCounty("*STJ");
				this.selectedJurisdiction.setCity("*STJ");
				this.selectedJurisdiction.setStj(this.selectedStj.getName());
			}
		}

		this.selectedNexusDefDetail = this.selectedDefinitionDetail;
		return "nexus_def_add";
	}
	
	public String bulkExpireAction() {
		this.currentAction = EditAction.DELETE;
		this.bulkExpire = true;

		this.selectedJurisdiction = new NexusJurisdictionDTO();
		this.selectedJurisdiction.setCountryCode(this.selectedState.getCountry());
		this.selectedJurisdiction.setCountryName(null);
		this.selectedJurisdiction.setStateCode(this.selectedState.getTaxCodeState());
		this.selectedJurisdiction.setStateName(this.selectedState.getName());
		
		if(getRegistrationTask()){
			this.selectedRegistrationDetail = new RegistrationDetail();
			try {
				this.selectedRegistrationDetail.setExpirationDate(sdf.parse("12/31/9999"));
			} catch (ParseException e) {}
		}
		else{
			this.selectedNexusDefDetail = new NexusDefinitionDetail();
			try {
				this.selectedNexusDefDetail.setExpirationDate(sdf.parse("12/31/9999"));
			} catch (ParseException e) {}
		}
		
		return "nexus_def_add";
	}

	public boolean isBulkExpire() {
		return bulkExpire;
	}
	
	public String bulkExpireCancelAction() {
		return "nexus_def_states";
	}
}
