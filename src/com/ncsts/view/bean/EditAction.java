package com.ncsts.view.bean;

import com.ncsts.common.Util;

public enum EditAction {
	ADD,
	COPY_ADD,
	ADD_NEW,
	COPY_UPDATE,
	ADD_FROM_TRANSACTION,
	COPY_ADD_FROM_TRANSACTION,
	UPDATE,
	UPDATE_PROC_DETAILS,
	BACKDATE,
	VIEW,
	DELETE,
	START,
	IN_PROCESS,
	MAP,
	RESET,
	UNLOCK,
	LOCK,
	SEND,
	EXECUTE,
	FINISHED,
	VIEW_FROM_TRANSACTION,
	VIEW_FROM_TRANSACTION_LOG_HISTORY,
	VIEW_FROM_TRANSACTION_LOG_ENTRY,
	COPY_TO,
	DEFAULT,
	DELETE_ALL,
	REORDER,
	POPULATE,
	OPEN,
	CLOSE,
	EDIT,
	VIEW_SPLIT,
	VIEW_FROM_MATRIX_COMPARISON,
	VIEW_FROM_STATITICS;
	
	public boolean isAddAction() {
		return (this.equals(ADD) || 
				this.equals(COPY_ADD) || 
				this.equals(COPY_UPDATE) ||
				this.equals(ADD_FROM_TRANSACTION) ||
				this.equals(COPY_ADD_FROM_TRANSACTION));
	}
	
	public boolean isReadOnlyAction() {
		return (this.equals(VIEW) || this.equals(DELETE) || this.equals(VIEW_FROM_TRANSACTION) || this.equals(VIEW_FROM_TRANSACTION_LOG_HISTORY) || this.equals(VIEW_FROM_TRANSACTION_LOG_ENTRY)
				|| this.equals(VIEW_FROM_MATRIX_COMPARISON));
	}
	
	public String getActionText() {
		if (this.equals(COPY_ADD) ||
			this.equals(COPY_ADD_FROM_TRANSACTION)) {
			return "Copy/Add";
		}
		
		if (this.equals(COPY_UPDATE)) {
			return "Copy/Update";
		}
		
		if (this.equals(ADD_FROM_TRANSACTION)) {
			return "Add";
		}
		
		if (this.equals(VIEW_FROM_TRANSACTION)|| this.equals(VIEW_FROM_TRANSACTION_LOG_HISTORY) || this.equals(VIEW_FROM_TRANSACTION_LOG_ENTRY)
				|| this.equals(EditAction.VIEW_FROM_MATRIX_COMPARISON)){
			return "View";
		}
		
		if(this.equals(BACKDATE)){
			return "Change Effective Date on";
		}
		
		if(this.equals(EDIT)){
			return "Edit";
		}
		
		if(this.equals(VIEW_SPLIT)){
			return "ViewSplit";
		}
		
		if(this.equals(VIEW_FROM_STATITICS)){
			return "ViewFromStats";
		}
		
		return Util.makeProper(this.name());
	}
	
	public String getActionTextSuffix() {
		if (this.equals(ADD_FROM_TRANSACTION) ||
			this.equals(COPY_ADD_FROM_TRANSACTION)) {
			return " from a Transaction";
		}
		
		return "";
	}
}
