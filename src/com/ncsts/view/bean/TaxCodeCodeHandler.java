package com.ncsts.view.bean;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import javax.faces.component.UIInput;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.ajax4jsf.component.html.HtmlAjaxSupport;
import org.apache.log4j.Logger;
import org.richfaces.component.html.HtmlDataTable;

import com.ncsts.common.CacheManager;
import com.ncsts.common.LoggerFactory;
import com.ncsts.domain.TaxCode;
import com.ncsts.domain.TaxCodeDetail;
import com.ncsts.domain.TransactionDetail;
import com.ncsts.dto.TaxCodeDetailDTO;
import com.ncsts.services.TaxCodeDetailService;

public class TaxCodeCodeHandler {

	static private Logger logger = LoggerFactory.getInstance().getLogger(TaxCodeCodeHandler.class);
	
	public interface TaxCodeCodeHandlerCallback {
		void searchTaxCodeCodeCallback(TaxCodeCodeHandler taxCodeCodeHandler);
	}
	
	private TaxCodeCodeHandlerCallback callback;

	public TaxCodeCodeHandlerCallback getCallback() {
		return callback;
	}

	public void setCallback(TaxCodeCodeHandlerCallback callback) {
		this.callback = callback;
	}

	private CacheManager cacheManager;	
	private HtmlSelectOneMenu taxcodeMenu = new HtmlSelectOneMenu();
	private HtmlInputText taxcodeDetailId = new HtmlInputText();

	private List<SelectItem> taxCodeItems; 
	private String taxcodeCode;
	
	private Long taxcodeId;
	private Integer size;
	private String id;
	
	private Long searchTaxCodeResultId;
	private TaxCodeDetailService taxCodeDetailService;
	private List<TaxCodeDetailDTO> taxCodeDTOList;
	
	//0004440
	private String returnRuleUrl = null;
	private TaxCodeBackingBean taxCodeBackingBean = null;

	public void setReturnRuleUrl(String returnRuleUrl){
		this.returnRuleUrl = returnRuleUrl;
	}
	
	public String getReturnRuleUrl(){
		return returnRuleUrl;
	}
	
	public void setTaxCodeBackingBean(TaxCodeBackingBean taxCodeBackingBean){
		this.taxCodeBackingBean = taxCodeBackingBean;
	}
	
	public TaxCodeBackingBean getTaxCodeBackingBean(){
		return taxCodeBackingBean;
	}
	
	public String displayRulesView(){
		taxCodeBackingBean.setReturnRuleUrl(returnRuleUrl);
		taxCodeBackingBean.setViewTaxcodeCode(taxcodeCode);	
		return taxCodeBackingBean.displayRulesView();
	}

	public void setCacheManager(CacheManager cacheManager) {
		this.cacheManager = cacheManager;
	}

	public List<SelectItem> getCountryItems() {
		return cacheManager.createCountryItems();
    }
	
	public List<TaxCodeDetailDTO> getTaxCodeDTOList() {
		return taxCodeDTOList;
	}
	
	public int getSize() {
		size = taxCodeDTOList.size();
		return size;
	}
	
	public TaxCodeCodeHandler() {
	}
	
	public TaxCodeCodeHandler(String id) {
		this.id = id;
	}

	public TaxCodeDetailService getTaxCodeDetailService() {
		return taxCodeDetailService;
	}
	
	public void setTaxCodeDetailService(TaxCodeDetailService taxCodeDetailService) {
		this.taxCodeDetailService = taxCodeDetailService;
		rebuildMenus();
	}

	public String getTaxcodeCode() {
		return taxcodeCode;
	}

	public void setTaxcodeCode(String taxcodeCode) {
		this.taxcodeCode = taxcodeCode;
		setMenuValue(taxcodeMenu, this.taxcodeCode);
	}
	
	public Long getTaxcodeId() {
		return taxcodeId;
	}

	public void setTaxcodeId(Long taxcodeId) {
		this.taxcodeId = taxcodeId;
		setMenuValue(taxcodeDetailId, this.taxcodeId);
	}
	
	public void setTaxcodeDetail(TaxCodeDetail tcDetail) {
		setTaxcodeCode((tcDetail == null)? null:tcDetail.getTaxcodeCode());

		// Rebuild menu items
		rebuildMenus();
	}

	public void setTransaction(TransactionDetail trDetail) {
		setTaxcodeCode((trDetail == null)? null:trDetail.getTaxcodeCode());
		
		// Rebuild menus
		rebuildMenus();
	}
	
	public HtmlSelectOneMenu getTaxcodeMenu() {
		//return null; //taxcodeMenu;
		return taxcodeMenu;
	}
	
	public void setTaxcodeMenu(HtmlSelectOneMenu taxcodeMenu) {
		this.taxcodeMenu = taxcodeMenu;
	}
	
	public HtmlInputText getTaxcodeDetailId() {
		return taxcodeDetailId;
	}

	public void setTaxcodeDetailId(HtmlInputText taxcodeDetailId) {
		this.taxcodeDetailId = taxcodeDetailId;
	}
	
	public void reset() {
		setTaxcodeCode(null);
		setTaxcodeDetailId(null);
		taxcodeCode = null;
		
		taxcodeId = null;
		
		taxCodeDTOList = null;
		searchTaxCodeResultId = null;

		// Rebuild menu items
		rebuildMenus();
	}
	
	private String getMenuValue(HtmlSelectOneMenu menu) {
		String result = (menu == null)? null:(String) menu.getSubmittedValue();
		return ((result == null) && (menu != null))? (String) menu.getValue():result;
	}
	
	private void setMenuValue(HtmlSelectOneMenu menu, String value) {
		if (menu != null) {
			menu.setLocalValueSet(false);
			menu.setSubmittedValue(null);
			menu.setValue(value);
		}
	}
	
	private String getMenuValue(UIInput menu) {
		String result = (String) menu.getSubmittedValue();
		return (result == null)? (String) menu.getValue():result;
	}
	
	private void setMenuValue(UIInput menu, Object value) {
		menu.setLocalValueSet(false);
		menu.setSubmittedValue(null);
		menu.setValue(value);
	}
	
	public void taxCodeSearchAction(ActionEvent e) {
		if(getMenuValue(taxcodeDetailId) != null && !getMenuValue(taxcodeDetailId).equals(""))
			taxcodeId = Long.valueOf(getMenuValue(taxcodeDetailId));
		initializeSearch();
	}
	
	public void taxCodeClearAction(ActionEvent e) {
		reset();
	}
	
	protected void initializeSearch() {
		taxCodeDTOList = performSearch();
	}
	
	private List<TaxCodeDetailDTO> performSearch() {
		taxcodeCode = getMenuValue(taxcodeMenu);
		return taxCodeDetailService.getTaxcodeDetails("", "", taxcodeCode, "", taxcodeId);
	}
	
	public void setTaxCode(TaxCodeDetail taxCode) {
		setTaxcodeCode((taxCode == null)? null:taxCode.getTaxcodeCode());
		setTaxcodeId((taxCode == null)? null:taxCode.getTaxcodeDetailId());
	}
	
	public boolean getValidSearchTaxCodeResult() {
		return (searchTaxCodeResultId != null);
	}

	public void selectTaxCode(ActionEvent e) { 
	    HtmlDataTable table = (HtmlDataTable) ((HtmlAjaxSupport) e.getComponent()).getParent(); 
	    searchTaxCodeResultId = ((TaxCodeDetailDTO) table.getRowData()).getTaxcodeDetailId();
		logger.debug("Selected Jurisdiction ID: " + ((searchTaxCodeResultId == null)? "NULL":searchTaxCodeResultId));		
	}

	public Long getSearchTaxCodeResultId() {
		return searchTaxCodeResultId;
	}

	public void setSearchTaxCodeResultId(Long searchTaxCodeResultId) {
		this.searchTaxCodeResultId = searchTaxCodeResultId;
	}

	public TaxCodeDetail findTaxcodeDetail() {
		TaxCodeDetail result = null;
		List<TaxCodeDetail> detailList = 
			taxCodeDetailService.findTaxCodeDetailListByTaxCode(taxcodeCode);
		if ((detailList != null) && (detailList.size() > 0)) {
			if (detailList.size() == 1) {
				logger.debug("Found one matching TaxCodeDetail: " + 
						String.format("%s - %s - %s", "", "", taxcodeCode));
				result = detailList.get(0);
			} else {
				logger.warn("Found multiple matching TaxCodeDetail: " +
						String.format("%s - %s - %s", "", "", taxcodeCode));
				result = detailList.get(0);
			}
		} else {
			logger.debug("Found no matching TaxCodeDetail: " + 
					String.format("%s - %s - %s", "", "", taxcodeCode));
		}
		return result;
	}
	
	public TaxCode findTaxcode() {
		TaxCode result = null;
		List<TaxCode> detailList = 
			taxCodeDetailService.findTaxCodeListByTaxCode(taxcodeCode);//
		if ((detailList != null) && (detailList.size() > 0)) {
			if (detailList.size() == 1) {
				result = detailList.get(0);
			} else {
				logger.warn("Found multiple matching TaxCode: " + String.format("%s - %s - %s", "", "", taxcodeCode));
				result = detailList.get(0);
			}
		} else {
			logger.debug("Found no matching TaxCode: " + String.format("%s - %s - %s", "", "", taxcodeCode));
		}
		return result;
	}
	
	public void rebuildMenus() {
		rebuildCodeMenu("", "", "", this.taxcodeCode);
	}
	
	public List<SelectItem> getTaxCodeItems() {
		return taxCodeItems;
	}
	
	public void taxcodeCodeSelected(ActionEvent e) {
		taxcodeCode = getMenuValue(taxcodeMenu);
		
		if (callback != null) {
			callback.searchTaxCodeCodeCallback(this);
		}
	}
	
	private void rebuildCodeMenu(String countryCode, String stateCode, String typeCode, String taxCode) {
		boolean taxCodeExists = false;
		logger.debug("Update tax code from state: " + stateCode + " type: " + typeCode);
		if(taxCodeItems!=null){
			taxCodeItems.clear();
			taxCodeItems = null;
		}
		
		taxCodeItems = new ArrayList<SelectItem>();
		taxCodeItems.add(new SelectItem("","Select a TaxCode"));
		List<TaxCode> codes = taxCodeDetailService.getAllTaxCode();
		

		if (codes != null) {
			for (TaxCode code : codes) {
				String value = code.getTaxCodePK().getTaxcodeCode();
				taxCodeItems.add(new SelectItem(value, value));
				// Check if previously selected code still exists
				if ((taxCode != null) && code.getTaxCodePK().getTaxcodeCode().equals(taxCode)) {
					taxCodeExists = true;
				}
			}
		}
	
		if (!taxCodeExists) {
			logger.debug("Selected code not found, resetting selection");
			setTaxcodeCode(null);
		}
	}
}
