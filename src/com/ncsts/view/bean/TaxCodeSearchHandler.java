package com.ncsts.view.bean;


import java.util.ArrayList;
import java.util.List;

import javax.faces.component.UIInput;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.ajax4jsf.component.html.HtmlAjaxSupport;
import org.apache.log4j.Logger;
import org.richfaces.component.html.HtmlDataTable;

import com.ncsts.common.CacheManager;
import com.ncsts.common.LoggerFactory;

import com.ncsts.domain.Jurisdiction;
import com.ncsts.domain.TaxCode;
import com.ncsts.domain.TaxCodeDetail;
import com.ncsts.dto.TaxCodeDetailDTO;
import com.ncsts.services.TaxCodeDetailService;
import com.ncsts.view.bean.ViewTaxCodeBean.TaxCodeInfoCallback;

public class TaxCodeSearchHandler {

	static private Logger logger = LoggerFactory.getInstance().getLogger(TaxCodeSearchHandler.class);

	private HtmlSelectOneMenu taxcodeCountryMenu = new HtmlSelectOneMenu();
	private HtmlSelectOneMenu taxcodeStateMenu = new HtmlSelectOneMenu();
	private HtmlSelectOneMenu taxcodeTypeMenu = new HtmlSelectOneMenu();
	private HtmlInputText taxcodeCodeText = new HtmlInputText();
	private HtmlInputText taxcodeDetailId = new HtmlInputText();
	private HtmlInputText taxcodeDesc = new HtmlInputText();
	private List<SelectItem> taxCodeItems; 
	
	private String taxcodeCountry;
	private String taxcodeState;
	private String taxcodeType;
	private String taxcodeCode;
	private String description;
	
	private Long taxcodeId;
	private Integer size;
	
	private String cchGroup;
	private String cchTaxCat;
	
	private Long searchTaxCodeResultId;
		
	private TaxCodeDetailService taxCodeDetailService;
	private TaxCodeSearchHandler popupHandler ;
	private CacheManager cacheManager;

	private List<TaxCodeDetailDTO> taxCodeDTOList;
	
	private TaxCodeInfoCallback taxCodeInfoCallback;
	private String callingPage = "";
	
	public void setCacheManager(CacheManager cacheManager) {
		this.cacheManager = cacheManager;
	}
	
	public List<SelectItem> getTaxCodeStateItems() {
		return cacheManager.createStateItems(taxcodeCountry);
    }
	
	public List<SelectItem> getCountryItems() {
		return cacheManager.createCountryItems();
    }
	
	public TaxCodeSearchHandler(TaxCodeInfoCallback taxCodeInfoCallback, String callingPage) {
		this.taxCodeInfoCallback = taxCodeInfoCallback;
		this.callingPage = callingPage;
	}
	
	public void setCallingPage(String callingPage){
		this.callingPage = callingPage;
	}
	
	public String getCallingPage(){
		return callingPage;
	}
	
	public List<TaxCodeDetailDTO> getTaxCodeDTOList() {
		return taxCodeDTOList;
	}
	
	public int getSize() {
		size = taxCodeDTOList.size();
		return size;
	}
	
	public TaxCodeSearchHandler() {
	}
	
	public String viewTaxCodeAction() {
		taxCodeInfoCallback.searchTaxCodeInfoCallback(getMenuValue(taxcodeCountryMenu),getMenuValue(taxcodeStateMenu), 
					getMenuValue(taxcodeTypeMenu), getMenuValue(taxcodeCodeText), callingPage);
		return "view_taxcode";
	}
	
	public boolean getIsCallingPageExist(){
		return (taxCodeInfoCallback!=null && callingPage!=null && callingPage.length()>0);
	}
	
	public TaxCodeDetailService getTaxCodeDetailService() {
		return taxCodeDetailService;
	}
	
	public void setTaxCodeDetailService(TaxCodeDetailService taxCodeDetailService) {
		this.taxCodeDetailService = taxCodeDetailService;
	}
	
	public String getTaxcodeCountry() {
		return taxcodeCountry;
	}

	public void setTaxcodeCountry(String taxcodeCountry) {
		this.taxcodeCountry = taxcodeCountry;
		setMenuValue(taxcodeCountryMenu, this.taxcodeCountry);
	}
	
	public String getTaxcodeState() {
		return taxcodeState;
	}

	public void setTaxcodeState(String taxcodeState) {
		this.taxcodeState = taxcodeState;
		setMenuValue(taxcodeStateMenu, this.taxcodeState);
	}

	public String getTaxcodeType() {
		return taxcodeType;
	}

	public void setTaxcodeType(String taxcodeType) {
		this.taxcodeType = taxcodeType;
		setMenuValue(taxcodeTypeMenu, this.taxcodeType);
	}

	public String getTaxcodeCode() {
		return taxcodeCode;
	}

	public void setTaxcodeCode(String taxcodeCode) {
		this.taxcodeCode = taxcodeCode;
		setMenuValue(taxcodeCodeText, this.taxcodeCode);
	}
	
	public Long getTaxcodeId() {
		return taxcodeId;
	}

	public void setTaxcodeId(Long taxcodeId) {
		this.taxcodeId = taxcodeId;
		setMenuValue(taxcodeDetailId, this.taxcodeId);
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
		setMenuValue(taxcodeDesc, this.description);
	}
	
	public String getCchGroup() {
		return cchGroup;
	}

	public void setCchGroup(String cchGroup) {
		this.cchGroup = cchGroup;
	}

	public String getCchTaxCat() {
		return cchTaxCat;
	}

	public void setCchTaxCat(String cchTaxCat) {
		this.cchTaxCat = cchTaxCat;
	}
	
	public HtmlSelectOneMenu getTaxcodeCountryMenu() {
		return taxcodeCountryMenu;
	}
	
	public void setTaxcodeCountryMenu(HtmlSelectOneMenu taxcodeCountryMenu) {
		this.taxcodeCountryMenu = taxcodeCountryMenu;
	}

	public HtmlSelectOneMenu getTaxcodeStateMenu() {
		return taxcodeStateMenu;
	}
	
	public void setTaxcodeStateMenu(HtmlSelectOneMenu taxcodeStateMenu) {
		this.taxcodeStateMenu = taxcodeStateMenu;
	}
	
	public HtmlSelectOneMenu getTaxcodeTypeMenu() {
		return taxcodeTypeMenu;
	}
	
	public void setTaxcodeTypeMenu(HtmlSelectOneMenu taxcodeTypeMenu) {
		this.taxcodeTypeMenu = taxcodeTypeMenu;
	}
	
	public HtmlInputText getTaxcodeCodeText() {
		return taxcodeCodeText;
	}
	
	public void setTaxcodeCodeText(HtmlInputText taxcodeCodeText) {
		this.taxcodeCodeText = taxcodeCodeText;
	}
	
	public HtmlInputText getTaxcodeDetailId() {
		return taxcodeDetailId;
	}

	public HtmlInputText getTaxcodeDesc() {
		return taxcodeDesc;
	}

	public void setTaxcodeDetailId(HtmlInputText taxcodeDetailId) {
		this.taxcodeDetailId = taxcodeDetailId;
	}

	public void setTaxcodeDesc(HtmlInputText taxcodeDesc) {
		this.taxcodeDesc = taxcodeDesc;
	}

	public void reset() {
		setTaxcodeCountry(null);
		setTaxcodeState(null);
		setTaxcodeType(null);
		setTaxcodeCode(null);
		
		setCchGroup(null);
		setCchTaxCat(null);
		
		setTaxcodeDetailId(null);
		setTaxcodeDesc(null);
		
		taxcodeCountry = null;
		taxcodeState = null;
		taxcodeType = null;
		taxcodeCode = null;
		
		taxcodeId = null;
		description = null;
		
		taxCodeDTOList = null;
		searchTaxCodeResultId = null;

	}


	private String getMenuValue(UIInput menu) {
		String result = (String) menu.getSubmittedValue();
		return (result == null)? (String) menu.getValue():result;
	}
	
	private void setMenuValue(UIInput menu, Object value) {
		menu.setLocalValueSet(false);
		menu.setSubmittedValue(null);
		menu.setValue(value);
	}
	
	

	public void popupSearchAction(ActionEvent e) {
		TaxCodeSearchHandler popupHandler = getPopupHandler();
		popupHandler.setTaxcodeCountry(getMenuValue(taxcodeCountryMenu));	
		popupHandler.setTaxcodeState(getMenuValue(taxcodeStateMenu));	
		popupHandler.setTaxcodeType(getMenuValue(taxcodeTypeMenu));
		popupHandler.setTaxcodeCode(getMenuValue(taxcodeCodeText));
		popupHandler.initializeSearch();
	}
	
	public void taxCodeSearchAction(ActionEvent e) {
		if(getMenuValue(taxcodeDesc) != null && !getMenuValue(taxcodeDesc).equals(""))
			description = getMenuValue(taxcodeDesc);
		if(getMenuValue(taxcodeDetailId) != null && !getMenuValue(taxcodeDetailId).equals(""))
			taxcodeId = Long.valueOf(getMenuValue(taxcodeDetailId));
		initializeSearch();
	}
	
	public void taxCodeClearAction(ActionEvent e) {
		reset();
	}
	
	protected void initializeSearch() {
		taxCodeDTOList = performSearch();
	}
	
	private List<TaxCodeDetailDTO> performSearch() {
		taxcodeCountry = getMenuValue(taxcodeCountryMenu);
		taxcodeState = getMenuValue(taxcodeStateMenu);
		taxcodeType = getMenuValue(taxcodeTypeMenu);
		taxcodeCode = getMenuValue(taxcodeCodeText);
		logger.debug("taxCodeDetailService   : " + taxCodeDetailService + "-" + taxcodeState + "-" + taxcodeType + "-" + taxcodeCode+ "-" + description+ "-"+ taxcodeId);
		return taxCodeDetailService.getTaxcodeDetailsByCountry(taxcodeCountry, taxcodeState, taxcodeType, taxcodeCode, description, taxcodeId);
	}
	
	public void setTaxCode(TaxCodeDetail taxCode) {
		setTaxcodeCountry((taxCode == null)? null:taxCode.getTaxcodeCountryCode());
		setTaxcodeState((taxCode == null)? null:taxCode.getTaxcodeStateCode());
		setTaxcodeType((taxCode == null)? null:taxCode.getTaxcodeTypeCode());
		setTaxcodeCode((taxCode == null)? null:taxCode.getTaxcodeCode());
		setTaxcodeId((taxCode == null)? null:taxCode.getTaxcodeDetailId());
		
	}
	
	public void updateTaxCodeListener(ActionEvent e) {
		if (popupHandler != null) {
			searchTaxCodeResultId = popupHandler.getSearchTaxCodeResultId();
			// TODO we could optimize by getting object from list in popup handler
			setTaxCode(taxCodeDetailService.findById(searchTaxCodeResultId));
			popupHandler.reset();
		}
	}
	
	public TaxCodeSearchHandler getPopupHandler() {
		if (popupHandler == null) {
			popupHandler = new TaxCodeSearchHandler();
			popupHandler.setTaxCodeDetailService(taxCodeDetailService);
			popupHandler.setCacheManager(cacheManager);
		}
		return popupHandler;
	}
	
	 public boolean getValidSearchTaxCodeResult() {
			return (searchTaxCodeResultId != null);
		}

		public void selectTaxCode(ActionEvent e) { 
		    HtmlDataTable table = (HtmlDataTable) ((HtmlAjaxSupport) e.getComponent()).getParent(); 
		    searchTaxCodeResultId = ((TaxCodeDetailDTO) table.getRowData()).getTaxcodeDetailId();
			logger.debug("Selected Jurisdiction ID: " + ((searchTaxCodeResultId == null)? "NULL":searchTaxCodeResultId));		
		}

		public Long getSearchTaxCodeResultId() {
			return searchTaxCodeResultId;
		}

		public void setSearchTaxCodeResultId(Long searchTaxCodeResultId) {
			this.searchTaxCodeResultId = searchTaxCodeResultId;
		}
		
		public TaxCodeDetail findTaxcodeDetail() {
			TaxCodeDetail result = null;
			List<TaxCodeDetail> detailList = 
				taxCodeDetailService.findTaxCodeDetailList(taxcodeCountry, taxcodeState, taxcodeType, taxcodeCode);
			if ((detailList != null) && (detailList.size() > 0)) {
				if (detailList.size() == 1) {
					logger.debug("Found one matching TaxCodeDetail: " + 
							String.format("%s - %s - %s", taxcodeState, taxcodeType, taxcodeCode));
					result = detailList.get(0);
				} else {
					logger.warn("Found multiple matching TaxCodeDetail: " +
							String.format("%s - %s - %s", taxcodeState, taxcodeType, taxcodeCode));
					result = detailList.get(0);
				}
			} else {
				logger.debug("Found no matching TaxCodeDetail: " + 
						String.format("%s - %s - %s", taxcodeState, taxcodeType, taxcodeCode));
			}
			return result;
		}
		
		public void setTaxcodeDetail(TaxCodeDetail tcDetail) {
			setTaxcodeCountry((tcDetail == null)? null:tcDetail.getTaxcodeCountryCode());
			setTaxcodeState((tcDetail == null)? null:tcDetail.getTaxcodeStateCode());
			setTaxcodeType((tcDetail == null)? null:tcDetail.getTaxcodeTypeCode());
			setTaxcodeCode((tcDetail == null)? null:tcDetail.getTaxcodeCode());

			// Rebuild menu items
			rebuildMenus();
		}
		
		public void taxcodeCountrySelected(ActionEvent e) {
			// Since the controls are not contained within their own
			// form, we need to use immediate=true which means
			// we need to get values from the submittedValue area!!
			taxcodeCountry = getMenuValue(taxcodeCountryMenu);
			taxcodeState = getMenuValue(taxcodeStateMenu);
			taxcodeType = getMenuValue(taxcodeTypeMenu);
			setMenuValue(taxcodeCountryMenu, taxcodeCountry);
			
			//Reset state only
			//setTaxcodeCountry(null);
			setTaxcodeState(null);
			//setTaxcodeType(null);
			//taxcodeCountry = null;
			taxcodeState = null;
			//taxcodeType = null;

			rebuildCodeMenu(taxcodeCountry, taxcodeState, taxcodeType, taxcodeCode);
		}
		
		public void rebuildMenus() {
			rebuildCodeMenu(this.taxcodeCountry, this.taxcodeState, this.taxcodeType, this.taxcodeCode);
		}
		
		public List<SelectItem> getTaxCodeItems() {
			return taxCodeItems;
		}
		
		private void rebuildCodeMenu(String countryCode, String stateCode, String typeCode, String taxCode) {
			boolean taxCodeExists = false;
			logger.debug("Update tax code from state: " + stateCode + " type: " + typeCode);
			taxCodeItems = new ArrayList<SelectItem>();
			taxCodeItems.add(new SelectItem("","Tax Code"));
			List<TaxCode> codes = ((countryCode == null) || countryCode.equals("") || (stateCode == null) || stateCode.equals("") || (typeCode == null) || typeCode.equals(""))? 
					null:taxCodeDetailService.findTaxCodeListByCountry(countryCode, stateCode, typeCode, null);
			if (codes != null) {
				for (TaxCode code : codes) {
					String value = code.getTaxCodePK().getTaxcodeCode() + " - " + code.getDescription();
					if (value.length() > 80) {
						value = value.substring(0, 80) + "...";
					}
					taxCodeItems.add(new SelectItem(code.getTaxCodePK().getTaxcodeCode(), value));
					// Check if previously selected code still exists
					if ((taxCode != null) && code.getTaxCodePK().getTaxcodeCode().equals(taxCode)) {
						taxCodeExists = true;
					}
				}
			}
			/*
			UISelectItems items = new UISelectItems();
			items.setValue(itemList);
			items.setId(id + "_code_items");
			taxcodeMenu.getChildren().clear();
			taxcodeMenu.getChildren().add(items);
			 */
			if (!taxCodeExists) {
				logger.debug("Selected code not found, resetting selection");
				setTaxcodeCode(null);
			}
		}
		
}
