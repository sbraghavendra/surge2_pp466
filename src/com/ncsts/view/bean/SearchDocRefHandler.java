package com.ncsts.view.bean;

import java.util.List;

import javax.faces.component.UIInput;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.ajax4jsf.component.html.HtmlAjaxSupport;
import org.apache.log4j.Logger;
import org.richfaces.component.html.HtmlDataTable;

import com.ncsts.common.CacheManager;
import com.ncsts.common.LoggerFactory;
import com.ncsts.domain.ReferenceDocument;
import com.ncsts.jsf.model.RefDocLookupDataModel;
import com.ncsts.services.ReferenceDocumentService;
import com.ncsts.view.util.TogglePanelController;

public class SearchDocRefHandler {

   	static private Logger logger = LoggerFactory.getInstance().getLogger(SearchDocRefHandler.class);
   	
   	private HtmlInputText refDocCodeInput = new HtmlInputText();
   	private HtmlSelectOneMenu refTypeCodeInput = new HtmlSelectOneMenu();
   	private HtmlInputText descriptionInput = new HtmlInputText();
   	private HtmlInputText keyWordsInput = new HtmlInputText();
   	private HtmlInputText docNameInput = new HtmlInputText();
   	private HtmlInputText urlInput = new HtmlInputText();
    private TogglePanelController togglePanelController = null;
   	
	private ReferenceDocument selectedItemKey;
	private int selectedRefDocIndex = -1;
	
	private CacheManager cacheManager;
	private ReferenceDocumentService referenceDocumentService;
	
	private RefDocLookupDataModel refDocLookupDataModel;
	
	private String urlLookup = null;
	

	public RefDocLookupDataModel getRefDocLookupDataModel() {
		return refDocLookupDataModel;
	}

	public void setRefDocLookupDataModel(RefDocLookupDataModel refDocLookupDataModel) {
		this.refDocLookupDataModel = refDocLookupDataModel;
	}

	public HtmlInputText getRefDocCodeInput() {
		return refDocCodeInput;
	}

	public void setRefDocCodeInput(HtmlInputText refDocCodeInput) {
		this.refDocCodeInput = refDocCodeInput;
	}

	public HtmlSelectOneMenu getRefTypeCodeInput() {
		return refTypeCodeInput;
	}

	public void setRefTypeCodeInput(HtmlSelectOneMenu refTypeCodeInput) {
		this.refTypeCodeInput = refTypeCodeInput;
	}

	public HtmlInputText getDescriptionInput() {
		return descriptionInput;
	}

	public void setDescriptionInput(HtmlInputText descriptionInput) {
		this.descriptionInput = descriptionInput;
	}

	public HtmlInputText getKeyWordsInput() {
		return keyWordsInput;
	}

	public void setKeyWordsInput(HtmlInputText keyWordsInput) {
		this.keyWordsInput = keyWordsInput;
	}

	public HtmlInputText getDocNameInput() {
		return docNameInput;
	}

	public void setDocNameInput(HtmlInputText docNameInput) {
		this.docNameInput = docNameInput;
	}

	public HtmlInputText getUrlInput() {
		return urlInput;
	}

	public void setUrlInput(HtmlInputText urlInput) {
		this.urlInput = urlInput;
	}

	public String getRefDocCode() {
		return getInputValue(refDocCodeInput);
	}

	public void setRefDocCode(String refDocCode) {
		setInputValue(refDocCodeInput, refDocCode);
	}

	public String getRefTypeCode() {
		return getInputValue(refTypeCodeInput);
	}

	public void setRefTypeCode(String refTypeCode) {
		setInputValue(refTypeCodeInput, refTypeCode);
	}

	public String getDescription() {
		return getInputValue(descriptionInput);
	}

	public void setDescription(String description) {
		setInputValue(descriptionInput, description);
	}

	public String getKeyWords() {
		return getInputValue(keyWordsInput);
	}

	public void setKeyWords(String keyWords) {
		setInputValue(keyWordsInput, keyWords);
	}

	public String getDocName() {
		return getInputValue(docNameInput);
	}

	public void setDocName(String docName) {
		setInputValue(docNameInput, docName);
	}

	public String getUrl() {
		return getInputValue(urlInput);
	}

	public void setUrl(String url) {
		setInputValue(urlInput, url);
	}

	public void setRefDoc(ReferenceDocument refDoc) {
		setRefDocCode((refDoc == null)? null:refDoc.getRefDocCode());
		setRefTypeCode((refDoc == null)? null:refDoc.getRefTypeCode());
		setDescription((refDoc == null)? null:refDoc.getDescription());
		setKeyWords((refDoc == null)? null:refDoc.getKeyWords());
		setDocName((refDoc == null)? null:refDoc.getDocName());
		setUrl((refDoc == null)? null:refDoc.getUrl());
	}
	
	public void getRefDoc(ReferenceDocument refDoc) {
		refDoc.setRefDocCode(getRefDocCode());
		refDoc.setRefTypeCode(getRefTypeCode());
		refDoc.setDescription(getDescription());
		refDoc.setKeyWords(getKeyWords());
		refDoc.setDocName(getDocName());
		refDoc.setUrl(getUrl());
	}
	
	public void reset() {
		setRefDocCode(null);
		setRefTypeCode(null);
		setDescription(null);
		setKeyWords(null);
		setDocName(null);
		setUrl(null);
		selectedItemKey = null;
	}
	
	public ReferenceDocumentService getReferenceDocumentService() {
		return referenceDocumentService;
	}

	public void setReferenceDocumentService(ReferenceDocumentService referenceDocumentService) {
		this.referenceDocumentService = referenceDocumentService;
	}

	public ReferenceDocument getSelectedItemKey() {
		return selectedItemKey;
	}

	public void setSelectedItemKey(ReferenceDocument selectedItemKey) {
		this.selectedItemKey = selectedItemKey;
	}

	protected void initializeSearch() {
		performSearch();
		selectedItemKey = null;
		this.selectedRefDocIndex = -1;
	}
	
	public void clearAction(ActionEvent e) {
		reset();
	}
	
	public String searchAction() {
		initializeSearch();
		return  "taxcodes_lookup_ref_docs";
		
	}
	
	public boolean getValidSearchResult() {
		return (selectedItemKey != null);
	}

	public void selectItem(ActionEvent e) { 
	    HtmlDataTable table = (HtmlDataTable) ((HtmlAjaxSupport) e.getComponent()).getParent(); 
	    selectedItemKey = (ReferenceDocument)table.getRowData();
		logger.debug("Selected ID: " + ((selectedItemKey == null)? "NULL":selectedItemKey));
		this.selectedRefDocIndex = table.getRowIndex();
	}
	
	public boolean isRefDocSelected() {
		return this.selectedRefDocIndex > -1;
	}

	public ReferenceDocument getSelectedItem() {
		if(selectedItemKey !=null){
			return selectedItemKey;
		}
		return null;
	}
	
	private void performSearch() {
		ReferenceDocument example = new ReferenceDocument();
		example.setRefDocCode(getRefDocCode());
		example.setRefTypeCode(getRefTypeCode());
		example.setDescription(getDescription());
		example.setKeyWords(getKeyWords());
		example.setDocName(getDocName());
		example.setUrl(getUrl());
		refDocLookupDataModel.setFilterCriteria(example);
	}
	
	public void refCodeLookup() {
		selectedItemKey = null;
		this.selectedRefDocIndex = -1;
		ReferenceDocument example = new ReferenceDocument();
		example.setRefDocCode(getRefDocCode());
        refDocLookupDataModel.setFilterCriteria(example);
	}
	
	public void setInputValue(UIInput input, String value) {
		input.setLocalValueSet(false);
		input.setSubmittedValue(null);
		input.setValue(value);
	}

	private String getInputValue(UIInput input) {
		String value = (String) input.getSubmittedValue();
		return (value != null)? value:(String) input.getValue();
	}

	public CacheManager getCacheManager() {
		return cacheManager;
	}

	public void setCacheManager(CacheManager cacheManager) {
		this.cacheManager = cacheManager;
	} 
	
	public List<SelectItem> getRefTypeItems() {
		return cacheManager.createRefTypeItems();
	}
	
	public int getSelectedRefDocIndex() {
		return selectedRefDocIndex;
	}

	public void setSelectedRefDocIndex(int selectedRefDocIndex) {
		this.selectedRefDocIndex = selectedRefDocIndex;
	}

	public  TogglePanelController getTogglePanelController(){
		if(togglePanelController==null){
			togglePanelController = new TogglePanelController();
			togglePanelController.addTogglePanel("ReferenceInformationPanel", true);
			togglePanelController.addTogglePanel("DocumentInformationPanel", true);
		}
		return togglePanelController;
	}
	
	public void setTogglePanelController(TogglePanelController togglePanelController){
		this.togglePanelController = togglePanelController;;
	}
	
	public String getUrlLookup() {
		return urlLookup;
	}

	public void setUrlLookup(String urlLookup) {
		this.urlLookup = urlLookup;
	}

	public void urlLookupResetAction(ActionEvent e) {
		urlLookup = null;
    }
	
	public String cancelAction() {
		reset();
		return "taxability_codes_ref_doc_add";
	}
	
	public String lookuprefDocOkAction() {
		reset();
		return "taxability_codes_ref_doc_add";
	}
}

