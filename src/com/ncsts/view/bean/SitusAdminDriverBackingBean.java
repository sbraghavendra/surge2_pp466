package com.ncsts.view.bean;

public class SitusAdminDriverBackingBean extends SitusBaseDriverBackingBean{

	public void init(){
		setActionRole("admin");
		setMaintenanceScreen("situs_admin_drivers_main");
		setUpdateScreen("situs_admin_drivers_update");
	}
}
