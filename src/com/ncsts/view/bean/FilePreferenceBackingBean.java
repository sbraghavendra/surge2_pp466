package com.ncsts.view.bean;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import javax.faces.application.FacesMessage;
import javax.faces.component.html.HtmlInputText;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;

import org.ajax4jsf.component.UIAjaxCommandButton;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.params.HttpClientParams;
import org.apache.log4j.Logger;
import org.apache.myfaces.custom.fileupload.UploadedFile;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;


import antlr.StringUtils;

/*
import sun.misc.IOUtils;
import sun.nio.cs.StandardCharsets;
*/
import com.ncsts.common.CacheManager;
import com.ncsts.common.LoggerFactory;
import com.ncsts.domain.CCHCode;
import com.ncsts.domain.CCHTaxMatrix;
import com.ncsts.domain.Jurisdiction;
import com.ncsts.domain.License;
import com.ncsts.domain.ListCodes;
import com.ncsts.domain.Option;
import com.ncsts.domain.OptionCodePK;
import com.ncsts.domain.ReferenceDocument;
import com.ncsts.domain.TransactionHeader;
import com.ncsts.domain.User;
import com.ncsts.dto.BatchPreferenceDTO;
import com.ncsts.dto.CompanyDTO;
import com.ncsts.dto.ImportMapProcessPreferenceDTO;
import com.ncsts.dto.ListCodesDTO;
import com.ncsts.dto.MatrixDTO;
import com.ncsts.dto.TaxCodeStateDTO;
import com.ncsts.dto.UserDTO;
import com.ncsts.dto.UserPreferenceDTO;
import com.ncsts.services.DwColumnService;
import com.ncsts.services.JurisdictionService;
import com.ncsts.services.OptionService;
import com.ncsts.services.PreferenceService;
import com.ncsts.services.ReferenceDocumentService;
import com.ncsts.services.TaxCodeDetailService;
import com.ncsts.services.TaxCodeService;
import com.ncsts.services.TaxCodeStateService;
import com.ncsts.services.UserService;
import com.ncsts.view.util.JsfHelper;
import com.ncsts.view.validator.TimeValidator;

@Aspect
public class FilePreferenceBackingBean implements SearchJurisdictionHandler.SearchJurisdictionCallback, TaxCodeCodeHandler.TaxCodeCodeHandlerCallback, BaseMatrixBackingBean.FindIdCallback {
	private Logger logger = LoggerFactory.getInstance().getLogger(FilePreferenceBackingBean.class);
	
	private static final List<String> REPLACE_WITH_MARKERS = 
		new ArrayList<String>(Arrays.asList(new String[] {"~a", "~b", "~c", "~d", "~s"}));
	
	@Autowired
	private TaxCodeBackingBean taxCodeBackingBean;
	
	@Autowired
	private DwColumnService dwColumnService;
	
	@Autowired
	private loginBean loginservice;
	private TaxCodeCodeHandler filterTaxCodeHandler = new TaxCodeCodeHandler();
	private TaxCodeCodeHandler  filterTaxCodeHandlerDefault = new TaxCodeCodeHandler();
	private String selectedRole="User";
	private String selectedPreference="G";
	private List<SelectItem> matrixUserTransIndList; /*= new ArrayList<SelectItem>();*/
	private List<SelectItem> matrixUserTaxCodeList = new ArrayList<SelectItem>();
	private List<SelectItem> matrixUserTaxTypeList = new ArrayList<SelectItem>();
	private List<SelectItem> matrixUserRateTypeList = new ArrayList<SelectItem>();
	private List<SelectItem> matrixCountryList = new ArrayList<SelectItem>();
	private List<SelectItem> timeZoneList = new ArrayList<SelectItem>();
	private List<SelectItem> viewItemsList = new ArrayList<SelectItem>();
	private List<SelectItem> moduleList = new ArrayList<SelectItem>();
	private List<SelectItem> genericLookupList = null;
	private List<SelectItem> roleItems = new ArrayList<SelectItem>();
	private List<SelectItem> preferencesItems = new ArrayList<SelectItem>();
	private Hashtable<String, String> roleHt = new Hashtable<String, String>();
	private Hashtable<String, String> preferenceHt = new Hashtable<String, String>();
	private CompanyDTO companyDTO;
	private BatchPreferenceDTO batchPreferenceDTO; 
	private MatrixDTO matrixDTO;
	private ImportMapProcessPreferenceDTO importMapProcessPreferenceDTO;
	private DoNotImportCharBean doNotImportCharBean = new DoNotImportCharBean();
	private UserPreferenceDTO userPreferenceDTO;
	private PreferenceService preferenceService;
	//new code that reads from the database instead of external file
	private OptionService optionService;
	private Map<String,String> defaultPreferenceMap;
	private Map<String,String> systemPreferenceMap;
	private Map<String,String> adminPreferenceMap;
	private Map<String,String> userPreferenceMap;
	private boolean systemBatchesFieldsProtected = false;
	private boolean systemBatchesRecalcFieldsProtected = false;	
	private CacheManager cacheManager;	
	private List<SelectItem> cchTaxCatItems;
	private List<SelectItem> cchTaxGrpItems;
	private UploadedFile fileSrc; 
	private boolean displayButton = false;
	private String errorMessage;
	
	private JurisdictionService jurisdictionService; 
	
	private String navigationString = "";
	
	private SearchBatchIdHandler batchIdHandler = new SearchBatchIdHandler();	
	
	private String companyName;
	private boolean displayUpload = false;

	private String lookUpStatus = "";
	private HtmlInputText processTransactionAmountInput = new HtmlInputText();
	private HtmlInputText holdTransactionAmount = new HtmlInputText();
	private HtmlInputText killProcessingProcedureInput = new HtmlInputText();
	private boolean displaySearch = false;
	private UIAjaxCommandButton searchButton ;
	private boolean displayBackButton = false;
	private boolean applyButtonClicked;
	
	private TaxCodeStateService taxCodeStateService;
	private TaxCodeDetailService taxCodeDetailService;
	private loginBean loginBean;
	private boolean controlToValuesSucceed = true;
	
	private String aidHostname;
	private String aidPort;
	private String aidStatus;
	private String aidHostname2;
	private String aidPort2;
	private String aidStatus2;
	private String aidInstallation;
	private String daysToKeep;
	private String emailHost;
	private String emailPort;
	
	private boolean emailStart;
	private String emailStop;
	private String emailTo;
	private String emailCc;
	private String emailFrom;
	
	private boolean allowAidAddFile;
	private boolean allowAidReimportFile;
	private boolean allowAidDeleteFile;
	
	private List<SelectItem> defaultNexusTypes;
	
	private String licenseKey;
	private boolean showLicenseWarning;
	
	@Autowired
	private TaxJurisdictionBackingBean taxJurisdictionBean;
	
	@Autowired
	private TaxCodeService taxCodeService;
	
	private List<SelectItem> maxdecimalplacesList = new ArrayList<SelectItem>();
	
	private String maxdecimalplace;
	
	private String currentmaxdecplace;
	
	private boolean maxdecimalvalChanged = false;
	
	private static String DEFAULT_MAX_DECIMAL_PLACE = "11";
	
	private boolean addStatetoDirEnabled;
	
	private String adminDirPath;
	
	private String urlpath;
	
	private boolean taxCodeCountOverThreshold = false;
	
	private List<SelectItem> matrixEntityList = new ArrayList<SelectItem>();
	
	private List<SelectItem> matrixStateList = new ArrayList<SelectItem>();
	
	private List<SelectItem> postJurisdictiontoSalesAddressItems;
	
	private String lonLatLookup;
	private String genJurLookup;
	private String streetLookup;
	
	public String getAdminDirPath() {
		Option option = optionService.findByPK(new OptionCodePK("REFDOCDIR", "ADMIN", "ADMIN"));
		adminDirPath = option.getValue();
		return adminDirPath;
	}

	public void setAdminDirPath(String adminDirPath) {
		this.adminDirPath = adminDirPath;
	}

	public String getUrlpath() {
		Option option = optionService.findByPK(new OptionCodePK("ADDLDTLDIR", "ADMIN", "ADMIN"));
		urlpath = option.getValue();
		return urlpath;
	}

	public void setUrlpath(String urlpath) {
		this.urlpath = urlpath;
	}

	public boolean getAddStatetoDirEnabled() {
		Option option = optionService.findByPK(new OptionCodePK("REFDOCDIRADDSTATE", "ADMIN", "ADMIN")); 
		String refdocdirState  = option.getValue();
		if ("1".equalsIgnoreCase(refdocdirState)) {
			this.addStatetoDirEnabled = true;
		} else {
			this.addStatetoDirEnabled = false;
		}
		return addStatetoDirEnabled;
	}

	public void setAddStatetoDirEnabled(boolean addStatetoDirEnabled) {
		this.addStatetoDirEnabled = addStatetoDirEnabled;
	}

	public boolean isMaxdecimalvalChanged() {
		return maxdecimalvalChanged;
	}

	public void setMaxdecimalvalChanged(boolean maxdecimalvalChanged) {
		this.maxdecimalvalChanged = maxdecimalvalChanged;
	}

	public String getCurrentmaxdecplace() {
		return currentmaxdecplace;
	}

	public void setCurrentmaxdecplace(String currentmaxdecplace) {
		this.currentmaxdecplace = currentmaxdecplace;
	}

	public String getMaxdecimalplace() {
		if(maxdecimalplace == null) {
			HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
			UserDTO userDTO = (UserDTO) session.getAttribute("user");
	        Option option = optionService.findByPK(new OptionCodePK ("MAXDECIMALPLACE", "USER", userDTO.getUserCode())); 
	        if(option.getValue() == null) {
	        	maxdecimalplace = DEFAULT_MAX_DECIMAL_PLACE;
	        }
	        else{
	        	maxdecimalplace = option.getValue();
	        }
	        return maxdecimalplace;
		}
		return maxdecimalplace;
	}

	public void setMaxdecimalplace(String maxdecimalplace) {
		userPreferenceDTO.setMaxdecimalplace(maxdecimalplace);
	}

	public List<SelectItem> getMaxdecimalplacesList() {
		return maxdecimalplacesList;
	}

	public void setMaxdecimalplacesList(List<SelectItem> maxdecimalplacesList) {
		this.maxdecimalplacesList = maxdecimalplacesList;
	}

	public loginBean getLoginBean() {
		return loginBean;
	}

	public void setLoginBean(loginBean loginBean) {
		this.loginBean = loginBean;
	}

	private UserService userService;
	
	private ReferenceDocumentService referenceDocumentService;
	
	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	public boolean isDisplayUpload() {
		return displayUpload;
	}

	public void setDisplayUpload(boolean displayUpload) {
		this.displayUpload = displayUpload;
	}
	
	public final List<SelectItem> getMatrixEntityList() {
		matrixEntityList.clear();
		matrixEntityList.add(new SelectItem("","Select Entity"));
		Map<String,String> entityMap = cacheManager.getEntityCodeMap();
		for (String entityCode : entityMap.keySet()) {
    		String entityName = entityMap.get(entityCode);
    		matrixEntityList.add(new SelectItem(entityCode, entityCode + " - " + entityName));
    	}

		return matrixEntityList;
	}
	
	public final List<SelectItem> getMatrixStateList() {
		matrixStateList.clear();		
		matrixStateList = cacheManager.createCountryStateItems(userPreferenceDTO.getUserCountry());

		return matrixStateList;
	}
	
	public void init() {
		filterTaxCodeHandlerDefault.setTaxcodeCode(getImportMapProcessPreferenceDTO().getRatepointDefaultTaxCode());
		filterTaxCodeHandler.setTaxcodeCode(getImportMapProcessPreferenceDTO().getApplyTaxCodeAmount());
	}

	public FilePreferenceBackingBean() {
		roleHt.put("User", "User");
		roleHt.put("System", "System");
		roleHt.put("Admin", "Admin");
		preferenceHt.put("G", "General");
		preferenceHt.put("M", "Matrix");
		preferenceHt.put("B", "Batches");
		preferenceHt.put("I", "Import/Map/Process");
		preferenceHt.put("GL", "G/L Extract");
		//preferenceHt.put("S", "Security");
		preferenceHt.put("D", "Directories");
		preferenceHt.put("T", "Update Files");
		//preferenceHt.put("TCR", "TaxCode Rules");
		preferenceHt.put("AID", "AID Service");
		//preferenceHt.put("C", "Compliance");		
		//filterTaxCodeHandler.setTaxcodeType("E");
	}

	/*
	 * Preferences item values hard coded, since no table exist in the db which is having all these preferences
	 * To get the value db has to be modified or can get from properties file or xml file
	 */
	public final List<SelectItem> getPreferencesItems() {
		preferencesItems.clear();
		preferencesItems.add(new SelectItem("G","General"));
		preferencesItems.add(new SelectItem("M","Matrix"));
		preferencesItems.add(new SelectItem("B","Batches"));
		preferencesItems.add(new SelectItem("I","Import/Map/Process"));
		preferencesItems.add(new SelectItem("GL","G/L Extract"));
		//preferencesItems.add(new SelectItem("S","Security"));
		preferencesItems.add(new SelectItem("D","Directories"));
		preferencesItems.add(new SelectItem("T","Update Files"));
		//preferencesItems.add(new SelectItem("TCR","TaxCode Rules"));
		//preferencesItems.add(new SelectItem("C","Compliance"));
		preferencesItems.add(new SelectItem("AID", "AID Service"));
		preferencesItems.add(new SelectItem("R", "RatePoint"));
		preferencesItems.add(new SelectItem("CP", "ControlPoint"));
		return preferencesItems;
	}
	
	public void setPreferencesItems(List<SelectItem> preferencesItems) {
		this.preferencesItems = preferencesItems;
	}
	
	public final List<SelectItem> getJurisdictiontoSalesAddressItems() {
		postJurisdictiontoSalesAddressItems =  new ArrayList<SelectItem>();
		Map<String, ListCodes> listCodeList = 	cacheManager.getListCodesMapByType("LOCNTYPE");
		for (ListCodes listcodes : listCodeList.values()) {			
			if(listcodes != null && listcodes.getCodeTypeCode()!=null && listcodes.getDescription()!=null){				
				postJurisdictiontoSalesAddressItems.add(new SelectItem(listcodes.getCodeCode(),listcodes.getCodeCode()+"-"+listcodes.getDescription()));
			}
		}
		return postJurisdictiontoSalesAddressItems;
		
	}
	
	public void setRoleItems(List<SelectItem> roleItems) {
		this.roleItems = roleItems;
	}
	
	/*
	 * Role Items are hard coded since no table exist in the db having all these roles..
	 */
	public final List<SelectItem> getRoleItems() {
		
		HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
		UserDTO userDTO = (UserDTO) session.getAttribute("user");
		String userCode = userDTO.getUserCode();
		
		User user = userService.findById(userCode);
		boolean userAdminBooleanFlag = user.getAdminBooleanFlag();

		roleItems.clear();
		
		// User Role
		roleItems.add(new SelectItem("User","User"));
		
		// System Role
		// 3912
		if (user.getAdminFlag().equals("1") || user.getAdminFlag().equals("2") || userCode.equals(user.getUserSTSUserCode())) {
			roleItems.add(new SelectItem("System", "System"));
		}

		if (user.getAdminFlag().equals("2") || userCode.equals(user.getUserSTSUserCode())) {
			roleItems.add(new SelectItem("Admin", "Admin"));
		}
		
		return roleItems;
	}
	
	public String getSelectedRole() {
		return selectedRole;
	}

	public void setSelectedRole(String selectedRole) {
		this.selectedRole = selectedRole;
	}

	public String getSelectedPreference() {
		return selectedPreference;
	}

	public void setSelectedPreference(String selectedPreference) {
		this.selectedPreference = selectedPreference;
	}
		
    /*public void roleChanged(ValueChangeEvent event) {
    	logger.debug("before role chnage event fire.. role== " + selectedRole + "preference == " + selectedPreference);
    	selectedRole = (String) event.getNewValue();
        logger.debug("after role change event fire...role== " + selectedRole + "preference == " + selectedPreference);
    }
    
    public void preferenceChanged(ValueChangeEvent event) {
    	logger.debug("before preference change event fire.. role== " + selectedRole + "preference == " + selectedPreference);
       	selectedPreference = (String) event.getNewValue();
    }*/
    
/*    public final CompanyDTO getCompanyDTO() {
    	if (companyDTO == null) {
    		companyDTO = preferenceService.getCompany();
    	}
    	return companyDTO;
	}*/
	
	public TaxCodeCodeHandler getFilterTaxCodeHandler() {
		return filterTaxCodeHandler;
	}
	
	public TaxCodeCodeHandler getFilterTaxCodeHandlerDefault() {
		return filterTaxCodeHandlerDefault;
	}
	
	public TaxCodeDetailService getTaxCodeDetailService() {
		return this.taxCodeDetailService;
	}
	
	public void setTaxCodeDetailService(TaxCodeDetailService taxCodeDetailService) {
		this.taxCodeDetailService = taxCodeDetailService;
		filterTaxCodeHandler.setTaxCodeDetailService(taxCodeDetailService);
		filterTaxCodeHandler.setCallback(this);
		
		filterTaxCodeHandlerDefault.setTaxCodeDetailService(taxCodeDetailService);
		filterTaxCodeHandlerDefault.setCallback(this);

		//ac filterTaxCodeHandlerDefault.getTaxcodeDetailId().setDisabled(false);
	}

	public final List<SelectItem> getTimeZoneList() {
		timeZoneList.clear();
		List<ListCodesDTO> list = preferenceService.getTimeZoneList();
		for(ListCodesDTO lc : list){
			timeZoneList.add(new SelectItem(lc.getCodeCode(),lc.getDescription()));
		}
		return timeZoneList;
	}
	
	public void setTimeZoneList(List<SelectItem> timeZoneList) {
		this.timeZoneList = timeZoneList;
	}
	
	public final List<SelectItem> getViewItemsList() {
		viewItemsList.clear();
		
		List<TransactionHeader> transactionHeaders = dwColumnService.getFilterNamesByUserCode("TB_PURCHTRANS", loginservice.getUsername().toUpperCase());
		
		viewItemsList = new ArrayList<SelectItem>();
		viewItemsList.add(new SelectItem("*LASTVIEW", "*Last View"));
		viewItemsList.add(new SelectItem("", "*GLOBAL/*DEFAULT"));
		for(TransactionHeader transactionHeader : transactionHeaders){
			if(!transactionHeader.getDataWindowName().equalsIgnoreCase("*DEFAULT")){
				viewItemsList.add(new SelectItem(transactionHeader.getUserCode()  + "/" + transactionHeader.getDataWindowName(), transactionHeader.getUserCode()  + "/" +  transactionHeader.getDataWindowName()));
			}
		}

		return viewItemsList;
	}
	
	public String getSelectedModule() {
		String lastModule = optionService.getUserOption(loginservice.getUsername().toUpperCase(), "LASTMODULE");
		if (lastModule!=null) {
			return lastModule;
		} else if (loginservice.getHasBillingLicense() ) {
			return "BILLSALE";
		} else return "PURCHUSE";

	}

	public void setSelectedModule(String selectedModule) {
		userPreferenceDTO.setModule(selectedModule);
	}
	
	public final List<SelectItem> getModuleList() {
		moduleList.clear();
		boolean option_purchase =loginservice.getHasPurchaseLicense();
		boolean option_billing= loginservice.getHasBillingLicense();
		List<SelectItem> selectItems = new ArrayList<SelectItem>();
		selectItems.add(new SelectItem("LASTMODULE", "Last Module"));
		if(option_purchase)
			selectItems.add(new SelectItem("PURCHUSE", "Purchasing"));
		if(option_billing)
			selectItems.add(new SelectItem("BILLSALE", "Billing"));
		moduleList = selectItems;
		return moduleList;
	}
	
	public void setModuleList(List<SelectItem> moduleList) {
		this.moduleList = moduleList;
	}

	public final List<SelectItem> getGenericLookupList() {
		if(genericLookupList==null){
			genericLookupList = new ArrayList<SelectItem>();
			genericLookupList.add(new SelectItem("0", "Off"));
			genericLookupList.add(new SelectItem("H", "Highest Rate"));
			genericLookupList.add(new SelectItem("L", "Lowest Rate"));
		}
		return genericLookupList;
	}
	
	public final List<SelectItem> getMatrixUserTransIndList() {
		if (matrixUserTransIndList == null) {
			matrixUserTransIndList = new ArrayList<SelectItem>();
			matrixUserTransIndList.add(new SelectItem("", "Select Transaction Status"));
			matrixUserTransIndList.add(new SelectItem("*ALL", "All"));
			matrixUserTransIndList.add(new SelectItem("P", "Processed"));
			matrixUserTransIndList.add(new SelectItem("PO", "Open"));
			matrixUserTransIndList.add(new SelectItem("PC", "Closed"));
			matrixUserTransIndList.add(new SelectItem("U", "Unprocessed"));
			matrixUserTransIndList.add(new SelectItem("US", "Suspended"));
			matrixUserTransIndList.add(new SelectItem("UST", "No Tax Matrix"));
			matrixUserTransIndList.add(new SelectItem("USL", "No Location Matrix"));
			matrixUserTransIndList.add(new SelectItem("USD", "No TaxCode Rule"));
			matrixUserTransIndList.add(new SelectItem("USR", "No Tax Rate"));
			matrixUserTransIndList.add(new SelectItem("UH", "Held"));
/*			for (SelectItem item : statusItems) {
				item.setEscape(false);
			}*/
		}
		return matrixUserTransIndList;		
	}		
	
	public void setMatrixUserTransIndList(List<SelectItem> matrixUserTransIndList) {
		this.matrixUserTransIndList = matrixUserTransIndList;
	}
	
	public final List<SelectItem> getMatrixUserTaxCodeList() {
		matrixUserTaxCodeList.clear();
		List<ListCodesDTO> list = preferenceService.getDefTaxCodeList();
		for(ListCodesDTO lc : list){
			matrixUserTaxCodeList.add(new SelectItem(lc.getCodeCode(),lc.getDescription()));
		}
		return matrixUserTaxCodeList;
	}
	
	public final List<SelectItem> getMatrixUserTaxTypeList() {
		matrixUserTaxTypeList.clear();
		List<ListCodesDTO> list = preferenceService.getDefTaxTypeList();
		matrixUserTaxTypeList.add(new SelectItem("","Select Tax Type"));
		for(ListCodesDTO lc : list){
			matrixUserTaxTypeList.add(new SelectItem(lc.getCodeCode(),lc.getDescription()));
		}
		return matrixUserTaxTypeList;
	}
	
	public final List<SelectItem> getMatrixUserRateTypeList() {
		matrixUserRateTypeList.clear();
		List<ListCodesDTO> list = preferenceService.getDefRateTypeList();
		matrixUserRateTypeList.add(new SelectItem("","Select Rate Type"));
		for(ListCodesDTO lc : list){
			matrixUserRateTypeList.add(new SelectItem(lc.getCodeCode(),lc.getDescription()));
		}
		return matrixUserRateTypeList;
	}
	
	public void setMatrixUserTaxCodeList(List<SelectItem> matrixUserTaxCodeList) {
		this.matrixUserTaxCodeList = matrixUserTaxCodeList;
	}
	
	public final List<SelectItem> getMatrixCountryList() {
		
		//public List<SelectItem> getCountryMenuItems() {
		//	  return getMatrixCommonBean().getCacheManager().createCountryItems();
		//}
		
		
		matrixCountryList.clear();
		
		matrixCountryList = cacheManager.createCountryItems();
		//List<ListCodesDTO> list = preferenceService.getDefTaxCodeList();
		//for(ListCodesDTO lc : list){
		////	matrixCountryList.add(new SelectItem(lc.getCodeCode(),lc.getDescription()));
		//}
		return matrixCountryList;
	}
	
	public void setMatrixCountryList(List<SelectItem> matrixCountryList) {
		this.matrixCountryList = matrixCountryList;
	}
	
	public PreferenceService getPreferenceService() {
		return preferenceService;
	}
	
	public void setPreferenceService(PreferenceService preferenceService) {
		this.preferenceService = preferenceService;
	}
	
	public OptionService getOptionService() {
		return optionService;
	}
	
	public void setOptionService(OptionService optionService) {
		this.optionService = optionService;
	}	
	
	public String roleSubmit() {
		return navigateAction();
	}
	
	public String preferenceSubmit() {
		return navigateAction();
	}
	
	public String navigateAction() {
		
		refreshAction();
		
		logger.debug("selected role == " + selectedRole);
    	logger.debug("selected preference == " + selectedPreference);
    	this.setErrorMessage(null);
    	setDisplayButton(false);
/*    	if (this.isDisplayButton()) {
    		String message = "Data has been changed on this page, do you want to Save? Update/Cancel.";
    		this.setErrorMessage(message);
    		System.out.println("--->" + message);
    		System.out.println("-- GETTING ->" + this.getNavigationString());    		
    	    return this.getNavigationString();	
    	} else {
    		System.out.println(" no change ");    		
    	}*/
    	String urlString = "";
        if(selectedRole.equalsIgnoreCase("System")){
            if(selectedPreference.equalsIgnoreCase("G")){
            urlString = "filepref_system_general";
            }else if(selectedPreference.equalsIgnoreCase("M")){
            urlString = "filepref_system_matrix";
            }else if(selectedPreference.equalsIgnoreCase("B")){
            urlString = "filepref_system_batch";
            }else if(selectedPreference.equalsIgnoreCase("I")){
            urlString = "filepref_system_import";
            }else if(selectedPreference.equalsIgnoreCase("GL")){
            urlString = "filepref_system_glextract";
            }else if(selectedPreference.equalsIgnoreCase("S")){
            urlString = "filepref_system_security";
            }else if(selectedPreference.equalsIgnoreCase("D")){
            urlString = "filepref_system_directory";
            }else if(selectedPreference.equalsIgnoreCase("T")){
            urlString = "filepref_system_tax";
            }else if (selectedPreference.equalsIgnoreCase("AID")) {
    		urlString = "filepref_system_aid";
    		//}else if(selectedPreference.equalsIgnoreCase("TCR")){
            //urlString = "filepref_system_taxcode_rules";
            } else if (selectedPreference.equalsIgnoreCase("R")) {
				urlString = "filepref_system_ratepoint";
            }else if (selectedPreference.equalsIgnoreCase("CP")) {
				urlString = "filepref_system_controlpoint";
            }else{
            	//urlString = "filepref_system_compliance";
            }
        }
        if(selectedRole.equalsIgnoreCase("Admin")){
        	if(selectedPreference.equalsIgnoreCase("G")){
                urlString = "filepref_admin_general";
                }else if(selectedPreference.equalsIgnoreCase("M")){
                urlString = "filepref_admin_matrix";
                }else if(selectedPreference.equalsIgnoreCase("B")){
                urlString = "filepref_admin_batch";
                }else if(selectedPreference.equalsIgnoreCase("I")){
                urlString = "filepref_admin_import";
                }else if(selectedPreference.equalsIgnoreCase("GL")){
                urlString = "filepref_admin_glextract";
                }else if(selectedPreference.equalsIgnoreCase("S")){
                urlString = "filepref_admin_security";
                }else if(selectedPreference.equalsIgnoreCase("D")){
                urlString = "filepref_admin_directory";
                }else if(selectedPreference.equalsIgnoreCase("T")){
                urlString = "filepref_admin_tax";
                }else if (selectedPreference.equalsIgnoreCase("AID")) {
        		urlString = "filepref_admin_aid";
        		//}else if(selectedPreference.equalsIgnoreCase("TCR")){
                //urlString = "filepref_admin_taxcode_rules";
                } else if (selectedPreference.equalsIgnoreCase("R")) {
    				urlString = "filepref_admin_ratepoint";
                }else if (selectedPreference.equalsIgnoreCase("CP")) {
    				urlString = "filepref_admin_controlpoint";
                }else{
                	//urlString = "filepref_admin_compliance";
                }
        }
        if(selectedRole.equalsIgnoreCase("User")){
        	if(selectedPreference.equalsIgnoreCase("G")){
                urlString = "filepref_user_general";
                }else if(selectedPreference.equalsIgnoreCase("M")){
                urlString = "filepref_user_matrix";
                }else if(selectedPreference.equalsIgnoreCase("B")){
                urlString = "filepref_user_batch";
                }else if(selectedPreference.equalsIgnoreCase("I")){
                urlString = "filepref_user_import";
                }else if(selectedPreference.equalsIgnoreCase("GL")){
                urlString = "filepref_user_glextract";
                }else if(selectedPreference.equalsIgnoreCase("S")){
                urlString = "filepref_user_security";
                }else if(selectedPreference.equalsIgnoreCase("D")){
                urlString = "filepref_user_directory";
                }else if(selectedPreference.equalsIgnoreCase("T")){
                urlString = "filepref_user_tax";
                }else if (selectedPreference.equalsIgnoreCase("AID")) {
        		urlString = "filepref_user_aid";
        		//}else if(selectedPreference.equalsIgnoreCase("TCR")){
                //urlString = "filepref_user_taxcode_rules";
                } else if (selectedPreference.equalsIgnoreCase("R")) {
    				urlString = "filepref_blank";
                }else if (selectedPreference.equalsIgnoreCase("CP")) {
    				urlString = "filepref_user_controlpoint";
                }else{
                	//urlString = "filepref_user_compliance";
                }
        }
        this.setNavigationString(urlString);       
        return urlString;
    }
	
	public String refreshAction() {
		this.displayUpload = false;
		this.setApplyButtonClicked(false);
		licenseKey = getLicense();
		
		//defaultPreferenceMap=null;
		systemPreferenceMap=null;
		adminPreferenceMap=null;
		userPreferenceMap=null;

		matrixDTO = null;
		matrixDTO = getMatrixDTO();
		companyDTO = null;
		companyDTO = getCompanyDTO();
		batchPreferenceDTO = null;
		batchPreferenceDTO = getBatchPreferenceDTO();
		importMapProcessPreferenceDTO = null;
		importMapProcessPreferenceDTO = getImportMapProcessPreferenceDTO();
		userPreferenceDTO = null;
		userPreferenceDTO = getUserPreferenceDTO();
		
		refreshSystemAIDAction();
		refreshAdminAIDAction();
		
		filterTaxCodeHandler.setTaxcodeCode(importMapProcessPreferenceDTO.getApplyTaxCodeAmount());
		filterTaxCodeHandlerDefault.setTaxcodeCode(importMapProcessPreferenceDTO.getRatepointDefaultTaxCode());

		this.setDisplayButton(false);
    	this.setErrorMessage(""); 
    	
	    return null; 
	}
    
    public String updateSystemGeneralAction() {
    	System.out.println("enter updateAction"); 		
       	Option option = optionService.findByPK(new OptionCodePK ("COMPANYNAME", "SYSTEM", "SYSTEM"));
    	option.setValue(companyDTO.getName());
    	optionService.saveOrUpdate(option);
    	logger.debug("saved name");
    	
    	option = optionService.findByPK(new OptionCodePK ("LOGO", "SYSTEM", "SYSTEM"));
    	option.setValue(companyDTO.getLogoUrl());
    	optionService.saveOrUpdate(option); 
    	logger.debug("saved logourl");   

    	option = optionService.findByPK(new OptionCodePK ("AUTOADDNEXUS", "SYSTEM", "SYSTEM"));
    	option.setValue(companyDTO.getAutoAddNexus());
    	optionService.saveOrUpdate(option);
    	
    	option = optionService.findByPK(new OptionCodePK ("AUTOADDNEXUSNORIGHTS", "SYSTEM", "SYSTEM"));
    	option.setValue(companyDTO.getAutoAddNexusNoRights());
    	optionService.saveOrUpdate(option);
    	
    	option = optionService.findByPK(new OptionCodePK ("DEFAULTNEXUSTYPE", "SYSTEM", "SYSTEM"));
    	option.setValue(companyDTO.getDefaultNexusType());
    	optionService.saveOrUpdate(option);

    	option = optionService.findByPK(new OptionCodePK ("SYSTEMTIMEZONE", "SYSTEM", "SYSTEM"));
    	option.setValue(companyDTO.getStsTimeZone());
    	optionService.saveOrUpdate(option);
    	    	
    	//Update time zone
    	String changedTimeZone = getChangedTimeZone();
    	
    	option = optionService.findByPK(new OptionCodePK ("SYSTEMDST", "SYSTEM", "SYSTEM"));
    	if (companyDTO.isStsAdminDST()) {
    		option.setValue("1");
    	} else {
    		option.setValue("0");
    	}   	
    	optionService.saveOrUpdate(option);
    	
    	option = optionService.findByPK(new OptionCodePK ("DEFUSERTIMEZONE", "SYSTEM", "SYSTEM"));
    	option.setValue(companyDTO.getDefaultTimeZone());
    	optionService.saveOrUpdate(option);     	
    	
    	option = optionService.findByPK(new OptionCodePK ("DEFUSERDST", "SYSTEM", "SYSTEM"));
    	if (companyDTO.isStsDST()) {
    		option.setValue("1");
    	} else {
    		option.setValue("0");
    	}     	
    	optionService.saveOrUpdate(option);
    	
    	//Save license key
    	License oldLicense = new License(getLicense());
    	License newLicense = new License(licenseKey);
    	option = optionService.findByPK(new OptionCodePK ("LICENSEKEY", "SYSTEM", "SYSTEM"));
    	option.setValue(licenseKey);
    	optionService.saveOrUpdate(option);
    	 
        this.setDisplayButton(false); 
        this.setErrorMessage(null);
    	logger.debug("exit updateAction");    	
    	
    	if(oldLicense.isValidLicense() != newLicense.isValidLicense()) {
    		this.setShowLicenseWarning(true);
    		this.setErrorMessage("License changes will be reflected only in next login. Please logout and login back again.");
    	}
    	else {
    		this.setShowLicenseWarning(false);
    	}
    	
		return null;
    }
    
    /*
    //0000005: The Do Not Import. Validate it at dialog box level
    public boolean validateMarkers() {
    	if (importMapProcessPreferenceDTO.getReplaceWithMarkers() != null) {
    		String s = importMapProcessPreferenceDTO.getReplaceWithMarkers();
    		for (int i=0; i < s.length(); i +=2) {
    			if ((s.substring(i, i+2).trim().length() == 2) && (!REPLACE_WITH_MARKERS.contains(s.substring(i, i+2)))) {
    				JsfHelper.addError("fpSIForm:replaceWithMarkers", "Invalid replace with marker");
    				return false;
    			}
    		}
    	}
    	return true;
    }
	*/
    
    public void parseValuesToEditControls() {
    	String doNotImportMarkers = getImportMapProcessPreferenceDTO().getDoNotImportMarkers();
    	String replaceWithMarkers = getImportMapProcessPreferenceDTO().getReplaceWithMarkers();
    	
    	if(doNotImportMarkers==null){
    		doNotImportMarkers = "";
    	}
    	if(replaceWithMarkers==null){
    		replaceWithMarkers = "";
    	}
    	doNotImportCharBean.parseValuesToEditControls(doNotImportMarkers, replaceWithMarkers);
	}
    
    public void moveEditControlToValuesListener(ActionEvent event){
    	String strDNIValues = doNotImportCharBean.getDNIValues();
    	String strRWValues = doNotImportCharBean.getRWValues();
 
    	String returnDNIValues = "";
    	String returnRWValues = "";
    	
    	String strTempDNI = "";
    	String strTempRW = "";

    	boolean bError = false;
    	for(int i=1; i<=50; i++){
    		
    		int startIndex = i;
    		strTempDNI = strDNIValues.substring(startIndex-1, startIndex);
    		strTempRW = strRWValues.substring((startIndex-1)*2, startIndex*2);
    		
    		if(strTempDNI.trim().length()==0 && strTempRW.trim().length()==0){	
    			//fine
    		}
    		else if(strTempRW.trim().length()==0){
    			//0000005: 
    			//Add: It is valid to have a DNI character without a RW character.
				returnDNIValues = returnDNIValues + strTempDNI.trim();
				returnRWValues = returnRWValues + "  ";	
    		}
    		else if(strTempDNI.trim().length()==0){
    			bError = true;
    			JsfHelper.addError("doNotImportReplaceWithForm:msg",i + " - Invalid \"Do Not Import\" value");
    		}
    		else{
    			String tempStr = strTempRW.trim();
    			if ((tempStr.length() == 2) && !(tempStr.substring(0,1).equalsIgnoreCase("~"))) {
    				bError = true;
    				JsfHelper.addError("doNotImportReplaceWithForm:msg",i + " - Invalid \"Replace With\" value");
    			}
    			else if ((tempStr.length() == 2) && (!REPLACE_WITH_MARKERS.contains(tempStr))) {  		
    				bError = true;
    				JsfHelper.addError("doNotImportReplaceWithForm:msg",i + " - Invalid Special \"Replace With\" value");
    			}
    			else{
    				//Valid character
    				returnDNIValues = returnDNIValues + strTempDNI;
    		    	returnRWValues = returnRWValues + strTempRW;
    			}
    		}
    	}
    	
    	if(bError){
    		controlToValuesSucceed = false;
    		return;
    	}
    	
    	controlToValuesSucceed = true;
    	getImportMapProcessPreferenceDTO().setDoNotImportMarkers(returnDNIValues);
    	getImportMapProcessPreferenceDTO().setReplaceWithMarkers(returnRWValues);
    	
    	updateButtonAction(null);
    }
    
    public String applyTaxMatrixAction() {
    	return null;
    }
    
    public boolean getControlToValuesSucceed() {
		return controlToValuesSucceed;
	}	
    
    public void updateButtonAction(ValueChangeEvent event) {
    	String message = "Data has been changed on this page.  Please Update if you want to save changes.";
    	this.setErrorMessage(message);
    	this.setDisplayButton(true);
    }
    
    public void updateButtonMaxDecAction(ValueChangeEvent event) {
    	currentmaxdecplace = (String)event.getOldValue();
    	String message = "Data has been changed on this page.  Please Update if you want to save changes.";
    	this.setErrorMessage(message);
    	this.setDisplayButton(true);
    }
    
    public void updateButtonDefaultEntityAction(ValueChangeEvent event) {
    	String defaultEntity = (String)event.getNewValue(); 
		if(defaultEntity==null || defaultEntity.length()==0){
    		userPreferenceDTO.setUserEntityFilter(false);	
    	}
		
    	updateButtonAction(null);
    }
    
    public void updateButtonFilterCountryAction(ValueChangeEvent event) {
    	Boolean filterCountryBoolean= (Boolean)event.getNewValue(); 
    	boolean filterCountry= filterCountryBoolean.booleanValue();
		if(!filterCountry){
			userPreferenceDTO.setUserStateFilter(false);
    	}
    	
    	updateButtonAction(null);
    }
    
    public String cancelAdminDirectoriesAction() {
    	this.setDisplayButton(false);
    	this.setErrorMessage(""); 
    	return "filepref_admin_directory";
    }
    
    
    public void updateButtonDefaultStateAction(ValueChangeEvent event) {
    	String defaultState = (String)event.getNewValue(); 
		if(defaultState==null || defaultState.length()==0){
    		userPreferenceDTO.setUserStateFilter(false);	
    	}
		
    	updateButtonAction(null);
    }
    
    public void updateButtonDefaultCountryAction(ValueChangeEvent event) {
    	String defaultCountry = (String)event.getNewValue(); 
		if(defaultCountry==null || defaultCountry.length()==0){
    		userPreferenceDTO.setUserCountryFilter(false);
    		userPreferenceDTO.setUserStateFilter(false);
    		userPreferenceDTO.setUserState("");
    	}
			
    	updateButtonAction(null);
    }
    
    public void updateMaxDecimalButtonAction(ValueChangeEvent event) {
    	currentmaxdecplace = (String)event.getOldValue();
    	if(!loginservice.getUser().getViewOnlyBooleanFlag()){
	    	String message = "Data has been changed on this page.  Please Update if you want to save changes.";
	    	this.setErrorMessage(message);
	    	this.setDisplayButton(true);
    	}
    }
    
//fixed for issue 0004541
	public void lookupAction(ActionEvent event) {
		String message = "Data has been changed on this page.  Please Update if you want to save changes.";
		this.setErrorMessage(message);
		this.setDisplayButton(true);
	}
    public void updateButtonActionListener(ActionEvent event) {
    	updateButtonAction(null);
    }
    
    public void updateButtonActionLookUp() {
    	updateButtonAction(null);
    }
    
    private SearchJurisdictionHandler jurisdictionHandler = new SearchJurisdictionHandler();
	
	public void searchJurisdictionCallback() {
		Long id = jurisdictionHandler.getSearchJurisdictionResultId();
		getImportMapProcessPreferenceDTO().setRatepointDefaultJurisdiction(id.toString());
		
		updateButtonAction(null);
	}
	
	public void popupSearchAction(ActionEvent e) {
		// pass event to handler
		jurisdictionHandler.reset();
		jurisdictionHandler.setCallback(this);
		jurisdictionHandler.popupSearchAction(e);
	}
	
	public String findJurisdictionIdAction() {
		Jurisdiction jurisdictionFilter = new Jurisdiction();
		
		Long jurisdictionId = null;
		if(importMapProcessPreferenceDTO.getRatepointDefaultJurisdiction()!=null && 
				importMapProcessPreferenceDTO.getRatepointDefaultJurisdiction().trim().length()>0){
			try{
				jurisdictionId = Long.parseLong(importMapProcessPreferenceDTO.getRatepointDefaultJurisdiction().trim());
			}
			catch (Exception e){				
			}		
		}
		jurisdictionFilter.setJurisdictionId(jurisdictionId);
		
		taxJurisdictionBean.findId(this, "jurisdictionId", "filepref_system_ratepoint", jurisdictionFilter);
		return "maintanence_tax_rates";
	}
	
	public void findIdCallback(Long id, String context) {
		if (context.equalsIgnoreCase("jurisdictionId")) {
			importMapProcessPreferenceDTO.setRatepointDefaultJurisdiction(id.toString());
			updateButtonAction(null);
		}
	}
	
	public SearchJurisdictionHandler getJurisdictionHandler() {
		return jurisdictionHandler;
	}
    
    public String updateUserGeneralAction() {
    	logger.debug("enter updateUserGeneralAction");  
		HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
		UserDTO userDTO = (UserDTO) session.getAttribute("user");
		String userCode = userDTO.getUserCode();   
    	
        Option option = optionService.findByPK(new OptionCodePK ("SAVEGRIDCHANGES", "USER", userCode));    	
    	option.setValue(userPreferenceDTO.getGridLayout());
    	optionService.saveOrUpdate(option);  
    	
    	option = optionService.findByPK(new OptionCodePK ("USERTIMEZONE", "USER", userCode));    	
    	option.setValue(userPreferenceDTO.getUserTimeZoneID());
    	optionService.saveOrUpdate(option);  
    	
    	
       	option = optionService.findByPK(new OptionCodePK ("LASTMODULE", "USER", userCode));    	
    	option.setValue(userPreferenceDTO.getModule());
      	optionService.saveOrUpdate(option);  
		maxdecimalplace = userPreferenceDTO.getMaxdecimalplace();
      	option = optionService.findByPK(new OptionCodePK ("MAXDECIMALPLACE", "USER", userCode));    	
    	option.setValue(maxdecimalplace);
      	optionService.saveOrUpdate(option);
		if((currentmaxdecplace!=null && maxdecimalplace!=null) && !currentmaxdecplace.equals(maxdecimalplace)){
    		setMaxdecimalvalChanged(true);
    	}
		option = optionService.findByPK(new OptionCodePK ("USETAXCODELOOKUP", "USER", userCode));
    	if (option == null) {
    		option = new Option();
    		option.setCodePK(new OptionCodePK ("USETAXCODELOOKUP", "USER", userCode));
    	}
    	option.setValue(userPreferenceDTO.getUseTaxCodeLookup() ? "1" : "0");
    	optionService.saveOrUpdate(option);	
    	//Update time zone
    	String changedTimeZone = getChangedTimeZone();
    	
    	 this.setDisplayButton(false); 
         this.setErrorMessage(null);
    	logger.debug("exit updateUserGeneralAction");    	
    	return null;
    }
    
    public String updateAdminAIDAction() {
    	logger.debug("enter updateAdminAIDAction");

		Option option = optionService.findByPK(new OptionCodePK("AIDHOSTNAME","ADMIN", "ADMIN"));
		option.setValue(aidHostname.trim() +  (aidHostname2.trim().length()>0 ? ";" + aidHostname2.trim() : ""));
		optionService.saveOrUpdate(option);

		option = optionService.findByPK(new OptionCodePK("AIDPORT", "ADMIN","ADMIN"));
		option.setValue(aidPort.trim() +  (aidPort2.trim().length()>0 ? ";" + aidPort2.trim() : ""));
		optionService.saveOrUpdate(option);
		
		option = optionService.findByPK(new OptionCodePK("AIDSETTINGPATH",
				"ADMIN", "ADMIN"));
		option.setValue(aidInstallation);
		optionService.saveOrUpdate(option);		
		this.loginBean.resetAidMenuEnabled();
		
		//PP395 Days of files to keep
		option = optionService.findByPK(new OptionCodePK("AIDPURGEDAYS",
				"ADMIN", "ADMIN"));
		option.setValue(daysToKeep);
		optionService.saveOrUpdate(option);
		
		option = optionService.findByPK(new OptionCodePK("EMAILHOST", "ADMIN", "ADMIN"));
		option.setValue(emailHost);
		optionService.saveOrUpdate(option);		
		
		option = optionService.findByPK(new OptionCodePK("EMAILPORT", "ADMIN", "ADMIN"));
		option.setValue(emailPort);
		optionService.saveOrUpdate(option);	
		
		option = optionService.findByPK(new OptionCodePK("EMAILFROM", "ADMIN", "ADMIN"));
		option.setValue(emailFrom);
		optionService.saveOrUpdate(option);	

		this.setDisplayButton(false);
		this.setErrorMessage(null);

		aidStatus = checkAIDStatus(aidHostname, aidPort);
		aidStatus2 = checkAIDStatus(aidHostname2, aidPort2);

		logger.debug("exit updateAdminAIDAction");
		return null;
	}
    
    public String updateAdminDirectoriesAction() {
    	
    	logger.debug("enter updateAdminDirectoriesAction");
    	Option option = optionService.findByPK(new OptionCodePK("REFDOCDIR", "ADMIN", "ADMIN"));
    	option.setValue(this.adminDirPath);
    	optionService.saveOrUpdate(option);
    	
    	option = optionService.findByPK(new OptionCodePK("REFDOCDIRADDSTATE", "ADMIN", "ADMIN"));
		if(this.addStatetoDirEnabled){
			option.setValue("1");
		}
		else {
			option.setValue("0");
		}
		optionService.saveOrUpdate(option);
		
		option = optionService.findByPK(new OptionCodePK("ADDLDTLDIR", "ADMIN", "ADMIN"));
    	option.setValue(this.urlpath);
    	optionService.saveOrUpdate(option);
    	
		this.setDisplayButton(false);
		this.setErrorMessage(null);
		return null;
    }
    
    public String updateSystemAIDAction() {
		logger.debug("enter updateSystemAIDAction");
		
		Option option = optionService.findByPK(new OptionCodePK ("AIDUSERADDFILE", "SYSTEM", "SYSTEM"));   
        if (allowAidAddFile) {
            option.setValue("1");	
        } else {
            option.setValue("0");	        	
        }
    	optionService.saveOrUpdate(option);
    	
    	option = optionService.findByPK(new OptionCodePK ("AIDUSERREIMPORTFILE", "SYSTEM", "SYSTEM"));   
        if (allowAidReimportFile) {
            option.setValue("1");	
        } else {
            option.setValue("0");	        	
        }
    	optionService.saveOrUpdate(option);

    	option = optionService.findByPK(new OptionCodePK ("AIDUSERDELETEFILE", "SYSTEM", "SYSTEM"));   
        if (allowAidDeleteFile) {
            option.setValue("1");	
        } else {
            option.setValue("0");	        	
        }
    	optionService.saveOrUpdate(option);
    	
    	option = optionService.findByPK(new OptionCodePK("EMAILSTART", "SYSTEM", "SYSTEM"));
    	if (emailStart) {
            option.setValue("1");	
        } else {
            option.setValue("0");	        	
        }
		optionService.saveOrUpdate(option);	
		
		option = optionService.findByPK(new OptionCodePK("EMAILSTOP", "SYSTEM", "SYSTEM"));
		option.setValue(emailStop);
		optionService.saveOrUpdate(option);	
		
		option = optionService.findByPK(new OptionCodePK("EMAILTO", "SYSTEM", "SYSTEM"));
		option.setValue(emailTo);
		optionService.saveOrUpdate(option);	
		
		option = optionService.findByPK(new OptionCodePK("EMAILCC", "SYSTEM", "SYSTEM"));
		option.setValue(emailCc);
		optionService.saveOrUpdate(option);	

		this.setDisplayButton(false);
		this.setErrorMessage(null);

		logger.debug("exit updateSystemAIDAction");
		return null;
	}
    
    public String updateAdminRatepointAction() {
		logger.debug("enter updateAdminGeneralAction");

		Option option = optionService.findByPK(new OptionCodePK(
				"RATEPOINTENABLED", "ADMIN", "ADMIN"));
		if (companyDTO.isRatepointEnabled()) {
			option.setValue("1");
		} else {
			option.setValue("0");
		}
		optionService.saveOrUpdate(option);

		option = optionService.findByPK(new OptionCodePK("RATEPOINTSERVER",
				"ADMIN", "ADMIN"));
		option.setValue(companyDTO.getRatepointServer());
		optionService.saveOrUpdate(option);

		option = optionService.findByPK(new OptionCodePK("RATEPOINTUSERID",
				"ADMIN", "ADMIN"));
		option.setValue(companyDTO.getRatepointUserId());
		optionService.saveOrUpdate(option);

		option = optionService.findByPK(new OptionCodePK("RATEPOINTPASSWORD",
				"ADMIN", "ADMIN"));
		option.setValue(companyDTO.getRatepointPassword());
		optionService.saveOrUpdate(option);
		
		option = optionService.findByPK(new OptionCodePK("LATLONLOOKUPURL", "SYSTEM", "SYSTEM"));
		option.setValue(this.getLonLatLookup());
		optionService.saveOrUpdate(option);

		option = optionService.findByPK(new OptionCodePK("GENERICJURAPIURL", "SYSTEM", "SYSTEM"));
		option.setValue(this.getGenJurLookup());
		optionService.saveOrUpdate(option);

		option = optionService.findByPK(new OptionCodePK("STREETLEVELURL", "SYSTEM", "SYSTEM"));
		option.setValue(this.getStreetLookup()); 
		optionService.saveOrUpdate(option);

		
		this.setDisplayButton(false);
		this.setErrorMessage(null);
		logger.debug("exit updateAdminratepointAction");
		return null;
	}
    public String updateAdminControlPointAction() {
		logger.debug("enter updateAdminControlPointAction");

		Option option = optionService.findByPK(new OptionCodePK(
				"CONTROLPOINTENABLED", "ADMIN", "ADMIN"));
		if (companyDTO.isControlPointEnabled()){
			option.setValue("1");
		} else {
			option.setValue("0");
		}
		optionService.saveOrUpdate(option);

		option = optionService.findByPK(new OptionCodePK("CONTROLPOINTSERVER",
				"ADMIN", "ADMIN"));
		option.setValue(companyDTO.getControlPointServer());
		optionService.saveOrUpdate(option);

		option = optionService.findByPK(new OptionCodePK("CONTROLPOINTUSERID",
				"ADMIN", "ADMIN"));
		option.setValue(companyDTO.getControlPointUserId());
		optionService.saveOrUpdate(option);

		option = optionService.findByPK(new OptionCodePK("CONTROLPOINTPASSWORD",
				"ADMIN", "ADMIN"));
		option.setValue(companyDTO.getControlPointPassword());
		optionService.saveOrUpdate(option);

		this.setDisplayButton(false);
		this.setErrorMessage(null);
		logger.debug("exit updateAdminControlPointAction");
		return null;
	}
    
    public String updateAdminGeneralAction() {
    	logger.debug("enter updateAdminGeneralAction");   
    	
        Option option = optionService.findByPK(new OptionCodePK ("VERSION", "ADMIN", "ADMIN"));    	
    	option.setValue(companyDTO.getVersion());
    	optionService.saveOrUpdate(option); 
    	
        option = optionService.findByPK(new OptionCodePK ("ASP", "ADMIN", "ADMIN"));
       	if (companyDTO.isAspCustomer()) {
        	option.setValue("1");       		
       	} else {
        	option.setValue("0");         		
       	}
    	optionService.saveOrUpdate(option);   
    	
    	 option = optionService.findByPK(new OptionCodePK ("PCO", "ADMIN", "ADMIN"));
        	if (companyDTO.isPcoCustomer()) {
         	option.setValue("1");       		
        	} else {
         	option.setValue("0");         		
        	}
     	optionService.saveOrUpdate(option);  
    	
        option = optionService.findByPK(new OptionCodePK ("STSCORPTIMEZONE", "ADMIN", "ADMIN"));    	
    	option.setValue(companyDTO.getStsAdminTimeZone());
    	optionService.saveOrUpdate(option);    
    	
        option = optionService.findByPK(new OptionCodePK ("STSCORPDST", "ADMIN", "ADMIN"));
       	if (companyDTO.isStsAdminDST()) {
        	option.setValue("1");       		
       	} else {
        	option.setValue("0");         		
       	}
    	optionService.saveOrUpdate(option);     
    	
        option = optionService.findByPK(new OptionCodePK ("INDEX_TABLESPACE", "ADMIN", "ADMIN"));    	
    	option.setValue(companyDTO.getTbSpace());
    	optionService.saveOrUpdate(option);   
    	
    	option = optionService.findByPK(new OptionCodePK ("TAXCODEDROPDOWNTHRESHOLD", "ADMIN", "ADMIN"));    	
    	option.setValue(matrixDTO.getSysTaxCodeDropDownThreshold());
    	optionService.saveOrUpdate(option); 
    	
    	 this.setDisplayButton(false); 
         this.setErrorMessage(null);
    	logger.debug("exit updateAdminGeneralAction");     	
    	return null;
    }
    
    public String updateSystemMatrixAction() {
    	logger.debug("enter updateSystemMatrixAction");  
    	
    	Option option = optionService.findByPK(new OptionCodePK ("MAXTOTALFULL", "ADMIN", "ADMIN"));    	
    	int maxTotalFull = 100;
    	if(option!=null){
    		try{
    			maxTotalFull = Integer.parseInt(option.getValue());
			}
			catch(Exception e){
			}
    	}
    	
    	option = optionService.findByPK(new OptionCodePK ("MAXTOTALLIMITED", "ADMIN", "ADMIN"));    	
    	int maxTotalLimited = 400;
    	if(option!=null){
    		try{
    			maxTotalLimited = Integer.parseInt(option.getValue());
			}
			catch(Exception e){
			}
    	}
    	
    	boolean errorFullFlag = false;	
    	boolean errorLimitedFlag = false;	
    	int fullThreshold = 0;
		try{
			fullThreshold = Integer.parseInt(matrixDTO.getSysFullThreshold());
		}
		catch(Exception e){
		}
    	if(fullThreshold<=0){
    		FacesMessage message = new FacesMessage("Full Threshold must be postive integers.");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
			errorFullFlag = true;
    	}
    	
    	int limitedThreshold = 0;
		try{
			limitedThreshold = Integer.parseInt(matrixDTO.getSysLimitedThreshold());
		}
		catch(Exception e){
		}
		if(limitedThreshold<=0){
    		FacesMessage message = new FacesMessage("Limited Threshold must be postive integers.");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
			errorLimitedFlag = true;
    	}
    	  	
    	int fullThresholdPages = 0;
		try{
			fullThresholdPages = Integer.parseInt(matrixDTO.getSysFullThresholdPages());
		}
		catch(Exception e){
		}
		if(fullThresholdPages<=0){
    		FacesMessage message = new FacesMessage("Full Threshold Pages must be postive integers.");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
			errorFullFlag = true;
    	}
    	  	
    	int limitedThresholdPages = 0;
		try{
			limitedThresholdPages = Integer.parseInt(matrixDTO.getSysLimitedThresholdPages());
		}
		catch(Exception e){
		}
		if(limitedThresholdPages<=0){
    		FacesMessage message = new FacesMessage("Limited Threshold Pages must be postive integers.");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
			errorLimitedFlag = true;
    	}
		
		if(errorFullFlag && errorLimitedFlag){
			return null;
		}
		
		if(!errorFullFlag && fullThreshold*fullThresholdPages > maxTotalFull){
			FacesMessage message = new FacesMessage("Full Threshold * Full Threshold Pages can not be greater than the Maximum Total Full Rows (" + maxTotalFull+ ").");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
			errorFullFlag = true;
		}
		
		if(!errorLimitedFlag && limitedThreshold*limitedThresholdPages > maxTotalLimited){
			FacesMessage message = new FacesMessage("Limited Threshold * Limited Threshold Pages can not be greater than the Maximum Total Limited Rows (" + maxTotalLimited + ").");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
			errorLimitedFlag = true;
		}
		
		if(errorFullFlag || errorLimitedFlag){
			return null;
		}
    	
        option = optionService.findByPK(new OptionCodePK ("DRIVERREFVAL", "SYSTEM", "SYSTEM"));    	
    	option.setValue(matrixDTO.getSysDriverRef().toString());
    	optionService.saveOrUpdate(option);    
    	
    	option = optionService.findByPK(new OptionCodePK ("FULLTHRESHOLD", "SYSTEM", "SYSTEM"));    	
    	option.setValue(matrixDTO.getSysFullThreshold());
    	optionService.saveOrUpdate(option);
    	
    	option = optionService.findByPK(new OptionCodePK ("FULLTHRESHOLDPAGES", "SYSTEM", "SYSTEM"));    	
    	option.setValue(matrixDTO.getSysFullThresholdPages());
    	optionService.saveOrUpdate(option); 
    	
    	option = optionService.findByPK(new OptionCodePK ("LIMITEDTHRESHOLD", "SYSTEM", "SYSTEM"));    	
    	option.setValue(matrixDTO.getSysLimitedThreshold());
    	optionService.saveOrUpdate(option);
    	
    	option = optionService.findByPK(new OptionCodePK ("LIMITEDTHRESHOLDPAGES", "SYSTEM", "SYSTEM"));    	
    	option.setValue(matrixDTO.getSysLimitedThresholdPages());
    	optionService.saveOrUpdate(option); 
    	
        option = optionService.findByPK(new OptionCodePK ("MINIMUMTAXDRIVERS", "SYSTEM", "SYSTEM"));    	
    	option.setValue(matrixDTO.getSysMinDriver().toString());
    	optionService.saveOrUpdate(option);     	
    	
        option = optionService.findByPK(new OptionCodePK ("MINIMUMLOCATIONDRIVERS", "SYSTEM", "SYSTEM"));    	
    	option.setValue(matrixDTO.getSysMinLocationDriver().toString());
    	optionService.saveOrUpdate(option);      
    	
        option = optionService.findByPK(new OptionCodePK ("MINIMUMALLOCATIONDRIVERS", "SYSTEM", "SYSTEM"));    	
    	option.setValue(matrixDTO.getSysMinAllocationDriver().toString());
    	optionService.saveOrUpdate(option);      	
    	 this.setDisplayButton(false); 
         this.setErrorMessage(null);
    	logger.debug("exit updateSystemMatrixAction");     	
    	return null;
    }
    
    public String updateUserMatrixAction() {
    	logger.debug("enter updateUserMatrixAction");  
		HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
		UserDTO userDTO = (UserDTO) session.getAttribute("user");
		String userCode = userDTO.getUserCode();    	

        Option option = optionService.findByPK(new OptionCodePK ("DEFTRANSIND", "USER", userCode));    	
    	option.setValue(userPreferenceDTO.getUserTransInd());
    	optionService.saveOrUpdate(option);    
    	
        option = optionService.findByPK(new OptionCodePK ("DEFTAXCODETYPE", "USER", userCode));    	
    	option.setValue(userPreferenceDTO.getUserTaxCode());
    	optionService.saveOrUpdate(option); 
    	
    	option = optionService.findByPK(new OptionCodePK ("DEFTAXTYPECODE", "USER", userCode));    	
    	option.setValue(userPreferenceDTO.getUserTaxType());
    	optionService.saveOrUpdate(option);
    	
    	option = optionService.findByPK(new OptionCodePK ("DEFRATETYPECODE", "USER", userCode));    	
    	option.setValue(userPreferenceDTO.getUserRateType());
    	optionService.saveOrUpdate(option);
    	
    	//Default country
    	option = optionService.findByPK(new OptionCodePK ("DEFUSERCOUNTRY", "USER", userCode));    	
    	option.setValue(userPreferenceDTO.getUserCountry());
    	optionService.saveOrUpdate(option);
    	
    	option = optionService.findByPK(new OptionCodePK ("DEFUSERCOUNTRYFILTER", "USER", userCode));  
    	if(userPreferenceDTO.getUserCountryFilter()) {
    		option.setValue("1");	
        }
    	else {
            option.setValue("0");	        	
        }
    	optionService.saveOrUpdate(option);
    	
    	//Default state
    	option = optionService.findByPK(new OptionCodePK ("DEFUSERSTATE", "USER", userCode));    	
    	option.setValue(userPreferenceDTO.getUserState());
    	optionService.saveOrUpdate(option);
    	
    	option = optionService.findByPK(new OptionCodePK ("DEFUSERSTATEFILTER", "USER", userCode));  
    	if(userPreferenceDTO.getUserStateFilter()) {
    		option.setValue("1");	
        }
    	else {
            option.setValue("0");	        	
        }
    	optionService.saveOrUpdate(option);
    	
    	//Default entity
    	option = optionService.findByPK(new OptionCodePK ("DEFUSERENTITY", "USER", userCode));    	
    	option.setValue(userPreferenceDTO.getUserEntity());
    	optionService.saveOrUpdate(option);
    	
    	option = optionService.findByPK(new OptionCodePK ("DEFUSERENTITYFILTER", "USER", userCode));  
    	if(userPreferenceDTO.getUserEntityFilter()) {
    		option.setValue("1");	
        }
    	else {
            option.setValue("0");	        	
        }
    	optionService.saveOrUpdate(option);
    	
    	 this.setDisplayButton(false); 
         this.setErrorMessage(null);
    	logger.debug("exit updateUserMatrixAction");     	
    	return null;
    }    
    
    private String getChangedTimeZone() {
    	String timeZoneStr= "";
  	
    	if(userPreferenceDTO == null) {
    		userPreferenceDTO = getUserPreferenceDTO();
    	}
    	
    	String uTZ = userPreferenceDTO.getUserTimeZoneID();
    	String sTZ = companyDTO.getDefaultTimeZone();
    	String sysTZ = companyDTO.getStsTimeZone();
    	
    	try {
    		if ((uTZ != null) && (uTZ.length() != 0)) {
    			timeZoneStr = uTZ;
    		} else if ((sTZ != null) && (sTZ.length() != 0)) {
    			timeZoneStr = sTZ;
    		} else if ((sysTZ != null) && (sysTZ.length() != 0)) {
    			timeZoneStr = sysTZ;
    		}
    	} catch (Exception e) {
    		logger.error(e);
    	}
    	if (timeZoneStr.length() == 0) timeZoneStr = TimeZone.getDefault().getID();
    	
    	//Change user time zone in session
    	TimeZone timeZone= null;
    	try {
    		timeZone = TimeZone.getTimeZone(timeZoneStr);	
    	} catch (Exception e) {
    		logger.error(e);
    	}
    	if (timeZone == null) timeZone = TimeZone.getDefault();
    	HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
  		session.setAttribute("timezone", timeZone);
  		logger.info("Setting user timeZone to " + timeZone.getDisplayName());
    	
  		return timeZoneStr;
    }
    
    private Date formatTime(String s) {
		if ((s == null) || (s.trim().length() == 0)) return null;
		return new TimeValidator().parse(s);
	}
    
    public String updateSystemBatchesAction() {
    	logger.debug("enter updateSystemBatchesAction");    

    	//4916
		Option option = optionService.findByPK(new OptionCodePK(
				"EXPORTDELIMITER", "SYSTEM", "SYSTEM"));
		String delimiterChar = batchPreferenceDTO.getDelimiter();
		boolean delimiterError = false;
		if(delimiterChar==null){
			delimiterChar = "";
		}
		else{
			delimiterChar = delimiterChar.trim();
			if(delimiterChar.length()==2){
				if(!(delimiterChar.equalsIgnoreCase("~t") || delimiterChar.equalsIgnoreCase("~n") || delimiterChar.equalsIgnoreCase("~v") || delimiterChar.equalsIgnoreCase("~r") || 
						delimiterChar.equalsIgnoreCase("~f") || delimiterChar.equalsIgnoreCase("~b"))){
					delimiterError = true;
				}			
			}
			else if(delimiterChar.length()>2){
				delimiterError = true;
			}
		}
		if(delimiterError){
			FacesMessage message = new FacesMessage("The delimiter must be either a single character or a two-character special code that starts with a tilde (~). The valid special values are: ~t : tab, ~n : new line, ~v : vertical tab, ~r : carriage return, ~f : form feed, ~b :backspace ");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
			return null;
		}
		option.setValue(delimiterChar);
		optionService.saveOrUpdate(option);
		
		option = optionService.findByPK(new OptionCodePK ("EXPORTFILEEXT", "SYSTEM", "SYSTEM"));    	
    	option.setValue(batchPreferenceDTO.getExportFileExt());
    	optionService.saveOrUpdate(option); 
    	
    	option = optionService.findByPK(new OptionCodePK ("EXPORTTEXTQUAL", "SYSTEM", "SYSTEM"));    	
    	option.setValue(batchPreferenceDTO.getTextQualifier());
    	optionService.saveOrUpdate(option); 
    	
    	option = optionService.findByPK(new OptionCodePK ("BATCHAUTOCHECKFLAG", "SYSTEM", "SYSTEM"));   
        if (batchPreferenceDTO.isSystemAutoCheck()) {
            option.setValue("1");	
        } else {
            option.setValue("0");	        	
        }
    	optionService.saveOrUpdate(option);    
    	
        option = optionService.findByPK(new OptionCodePK ("BATCHAUTOCHECKSEC", "SYSTEM", "SYSTEM"));    	
    	option.setValue(batchPreferenceDTO.getSystemAutoCheckTime());
    	optionService.saveOrUpdate(option); 

    	//Need to change to UTC (System) time zone
    	String systemDefTime = batchPreferenceDTO.getSystemDefTime();
    	if (systemDefTime != null && systemDefTime.trim().length() > 0) {
    		if(!systemDefTime.equalsIgnoreCase("*NOW")){
	    		String changedTimeZone = getChangedTimeZone();
				Date date = formatTime(systemDefTime);
				if (date != null) {
					long systemTimeInMillis = com.ncsts.view.util.DateHelper.getSystemTimeInMillis(changedTimeZone, date.getTime());
					
					Date d = new Date(systemTimeInMillis);
				    DateFormat df = new SimpleDateFormat("hh:mm:ss a");
				    systemDefTime = df.format(d); //System time zone
				    
				    batchPreferenceDTO.setSystemDefTime(df.format(date));//User time zone
				}
				else{
					systemDefTime = "";
				}
    		}
		} 
    	else{
    		systemDefTime = "";
    	}
    	
    	option = optionService.findByPK(new OptionCodePK ("BATCHDEFSCHEDTIME", "SYSTEM", "SYSTEM"));    	
    	option.setValue(systemDefTime);
    	optionService.saveOrUpdate(option); 
    	
        option = optionService.findByPK(new OptionCodePK ("BATCHUSERCHGTIMEFLAG", "SYSTEM", "SYSTEM"));   
        if (batchPreferenceDTO.isSystemUserFlag()) {
            option.setValue("1");	
        } else {
            option.setValue("0");	        	
        }
    	optionService.saveOrUpdate(option);    	
    	
        option = optionService.findByPK(new OptionCodePK ("SYSTEMSTATUSRECALCSECS", "SYSTEM", "SYSTEM"));    	
    	option.setValue(batchPreferenceDTO.getSystemRecalTime());
    	optionService.saveOrUpdate(option);     	
    	 this.setDisplayButton(false); 
         this.setErrorMessage(null);
    	logger.debug("exit updateSystemBatchesAction");     	
    	return null;
    }     
    
    public String updateUserBatchesAction() {
    	logger.debug("enter updateUserBatchesAction");  
		HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
		UserDTO userDTO = (UserDTO) session.getAttribute("user");
		String userCode = userDTO.getUserCode();       	

        Option option = optionService.findByPK(new OptionCodePK ("AOLENABLED", "USER", userCode));   
        if (userPreferenceDTO.isAolEnabled()) {
            option.setValue("1");	
        } else {
            option.setValue("0");	        	
        }
    	optionService.saveOrUpdate(option); 
    	
        option = optionService.findByPK(new OptionCodePK ("AOLWHEN", "USER", userCode));   
        option.setValue(userPreferenceDTO.getAolWhen());
    	optionService.saveOrUpdate(option);     	
    	
        option = optionService.findByPK(new OptionCodePK ("AOLWHICH", "USER", userCode));   
        option.setValue(userPreferenceDTO.getAolWhich());
    	optionService.saveOrUpdate(option);       	
    	 this.setDisplayButton(false); 
         this.setErrorMessage(null);
    	logger.debug("exit updateUserBatchesAction");     	
    	return null;
    } 
    
    public String updateAdminBatchesAction() {
    	logger.debug("enter updateAdminBatchesAction");  
    	
    	//Need to change to UTC (System) time zone
    	String adminDefTime = batchPreferenceDTO.getAdminDefTime();
    	if (adminDefTime != null && adminDefTime.trim().length() > 0) {
    		if(!adminDefTime.equalsIgnoreCase("*NOW")){
	    		String changedTimeZone = getChangedTimeZone();
				Date date = formatTime(adminDefTime);
				if (date != null) {
					long systemTimeInMillis = com.ncsts.view.util.DateHelper.getSystemTimeInMillis(changedTimeZone, date.getTime());
					
					Date d = new Date(systemTimeInMillis);
				    DateFormat df = new SimpleDateFormat("hh:mm:ss a");
				    adminDefTime = df.format(d); //System time zone
				    
				    batchPreferenceDTO.setAdminDefTime(df.format(date));//User time zone
				}
				else{
					adminDefTime = "";
				}
    		}
		} 
    	else{
    		adminDefTime = "";
    	}
    	
    	Option option = optionService.findByPK(new OptionCodePK ("ADMINDEFSCHEDTIME", "ADMIN", "ADMIN"));    	
    	option.setValue(adminDefTime);
    	optionService.saveOrUpdate(option); 
    	

        option = optionService.findByPK(new OptionCodePK ("ADMINSTATUSRECALCSECS", "ADMIN", "ADMIN"));
    	option.setValue(batchPreferenceDTO.getAdminRecalTime());        
    	optionService.saveOrUpdate(option);   
    	
    	option = optionService.findByPK(new OptionCodePK ("BATCHPROCESSINTERVALSECS", "SYSTEM", "SYSTEM"));
    	option.setValue(batchPreferenceDTO.getAdminIntervalTime());        
    	optionService.saveOrUpdate(option);
    	
        option = optionService.findByPK(new OptionCodePK ("ADMINUSERCHGRECALCSECSFLAG", "ADMIN", "ADMIN"));
       	if (batchPreferenceDTO.isAdminRecalUserFlag()) {
       		this.setSystemBatchesRecalcFieldsProtected(false);       		
        	option.setValue("1");       		
       	} else {
       		this.setSystemBatchesRecalcFieldsProtected(true);         		
        	option.setValue("0");         		
       	}
    	optionService.saveOrUpdate(option);     
    	
        option = optionService.findByPK(new OptionCodePK ("ADMINUSERCHGTIMEFLAG", "ADMIN", "ADMIN"));
       	if (batchPreferenceDTO.isAdminUserFlag()) {
    		logger.debug("updateAdminBatchesAction():adminUserFlag = " + batchPreferenceDTO.isAdminUserFlag());       		
       		this.setSystemBatchesFieldsProtected(false);
        	option.setValue("1");       		
       	} else {
    		logger.debug("updateAdminBatchesAction():adminUserFlag = " + batchPreferenceDTO.isAdminUserFlag());       		
       		this.setSystemBatchesFieldsProtected(true);       		
        	option.setValue("0");          		
       	}
    	optionService.saveOrUpdate(option);   
    	 this.setDisplayButton(false); 
         this.setErrorMessage(null);
    	logger.debug("exit updateAdminBatchesAction");      	
    	return null;
    }    
    
    public String updateSystemImportMapProcessAction() {
    	//0000005: The Do Not Import. Validate it at dialog box level
    	//if (!validateMarkers()) return null;
    	
    	logger.debug("enter updateSystemImportMapProcessAction");
    	
    	Option option = null;
      	
      	//0001701: Import errors make the system crash
      	if(importMapProcessPreferenceDTO.getTerminalErrorsBeforeAbortImport()!=null && importMapProcessPreferenceDTO.getTerminalErrorsBeforeAbortImport().length()>0){
	      	int maxError = 100;
			try{
				maxError = Integer.parseInt(importMapProcessPreferenceDTO.getTerminalErrorsBeforeAbortImport());
			}
			catch(Exception e){
				FacesMessage message = new FacesMessage("'Number of terminal errors before abort import' is invalid.");
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage(null, message);
				return null;
			}
			
	    	if(maxError>100){
	    		FacesMessage message = new FacesMessage("'Number of terminal errors before abort import' must be less than or equal to 100.");
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage(null, message);
				return null;
	    	}
	    	
	    	if(maxError<1){
	    		FacesMessage message = new FacesMessage("'Number of terminal errors before abort import' must be greater than or equal to 1.");
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage(null, message);
				return null;
	    	}
	
	    	option = optionService.findByPK(new OptionCodePK ("ERRORROWS", "SYSTEM", "SYSTEM")); 
	        option.setValue(Integer.toString(maxError));
	      	optionService.saveOrUpdate(option);
      	}
      	else{
      		option = optionService.findByPK(new OptionCodePK ("ERRORROWS", "SYSTEM", "SYSTEM")); 
	        option.setValue("");
	      	optionService.saveOrUpdate(option);
      	}
      	
      	option = optionService.findByPK(new OptionCodePK("BATCHTOTALTOLERANCE","SYSTEM", "SYSTEM"));
		option.setValue(importMapProcessPreferenceDTO.getBatchTotalTolerance());
		optionService.saveOrUpdate(option);
      	
      	//0007519
      	
        option = optionService.findByPK(new OptionCodePK ("KILLPROCFLAG", "SYSTEM", "SYSTEM"));   
        if (importMapProcessPreferenceDTO.isKillProcessingProcedureCountFlag()) {
            option.setValue("1");	
        } else {
            option.setValue("0");	        	
        }
    	optionService.saveOrUpdate(option);  

		option = optionService.findByPK(new OptionCodePK ("VENDORNEXUSFLAG", "SYSTEM", "SYSTEM"));   
        if (importMapProcessPreferenceDTO.isLookupVendorNexusFlag()) {
            option.setValue("1");	
        } else {
            option.setValue("0");	        	
        }
    	optionService.saveOrUpdate(option); 
    	
    	option = optionService.findByPK(new OptionCodePK ("EMAILPROC", "SYSTEM", "SYSTEM"));   
    	if (importMapProcessPreferenceDTO.isEmailProcessStatusFlag()) {
    		option.setValue("1");	
    	} else {
    		option.setValue("0");	        	
    	}
    	optionService.saveOrUpdate(option);
    	
    	option = optionService.findByPK(new OptionCodePK ("HELDPROCRULE", "SYSTEM", "SYSTEM"));   
    	if (importMapProcessPreferenceDTO.isRunPostProcessrulesForheldTransFlag()) {
    		option.setValue("1");	
    	} else {
    		option.setValue("0");	        	
    	}
    	optionService.saveOrUpdate(option);
    	
    	
        option = optionService.findByPK(new OptionCodePK ("KILLPROCCOUNT", "SYSTEM", "SYSTEM")); 
        option.setValue(importMapProcessPreferenceDTO.getKillProcessingProcedureCount());
      	optionService.saveOrUpdate(option); 
      	
      //Moved back to Preference
        option = optionService.findByPK(new OptionCodePK ("HOLDTRANSAMT", "SYSTEM", "SYSTEM"));   
        option.setValue(importMapProcessPreferenceDTO.getHoldTransactionAmount());
    	optionService.saveOrUpdate(option);
    	
        option = optionService.findByPK(new OptionCodePK ("AUTOPROCAMT", "SYSTEM", "SYSTEM")); 
        option.setValue(importMapProcessPreferenceDTO.getProcessTransactionAmount());
      	optionService.saveOrUpdate(option); 
      	
        option = optionService.findByPK(new OptionCodePK ("AUTOPROCID", "SYSTEM", "SYSTEM")); 
        option.setValue(filterTaxCodeHandler.getTaxcodeCode().trim());
      	optionService.saveOrUpdate(option);  

      	option = optionService.findByPK(new OptionCodePK ("DELETE0AMTFLAG", "SYSTEM", "SYSTEM"));   
        if (importMapProcessPreferenceDTO.isDeleteTransactionWithZeroAmountFlag()) {
            option.setValue("1");	
        } else {
            option.setValue("0");	        	
        }
    	optionService.saveOrUpdate(option);    	
    	
        option = optionService.findByPK(new OptionCodePK ("HOLDTRANSFLAG", "SYSTEM", "SYSTEM"));   
        if (importMapProcessPreferenceDTO.isHoldTransactionAmountFlag()) {
            option.setValue("1");	
        } else {
            option.setValue("0");	        	
        }
    	optionService.saveOrUpdate(option);    
    	
        option = optionService.findByPK(new OptionCodePK ("AUTOPROCFLAG", "SYSTEM", "SYSTEM"));   
        if (importMapProcessPreferenceDTO.isProcessTransactionAmountFlag()) {
            option.setValue("1");	
        } else {
            option.setValue("0");	        	
        }
    	optionService.saveOrUpdate(option);
    	
    	option = optionService.findByPK(new OptionCodePK ("CUSTOMTCOR", "SYSTEM", "SYSTEM"));   
	    if(importMapProcessPreferenceDTO.isCustomTaxCodeOverrideTaxionaryFlag()) {
	    	option.setValue("1");
	    }else {
	    	option.setValue("0");
	    }
	 	optionService.saveOrUpdate(option);
	 	
	 	option = optionService.findByPK(new OptionCodePK ("ALLOCJUR", "SYSTEM", "SYSTEM"));   
	    if (importMapProcessPreferenceDTO.isPostJurisdtoSalesAddressEnabled()) {
	        option.setValue(importMapProcessPreferenceDTO.getPostJurisdictiontoSalesAddress());	
	    }
	 	optionService.saveOrUpdate(option); 

    	logger.debug("exit updateSystemImportMapProcessAction");  
    	this.setDisplayButton(false); 
        this.setErrorMessage(null);
    	return null;
    }  
    
    public String updateAdminImportMapProcessAction() {
    	logger.debug("enter updateAdminImportMapProcessAction");  
    	
        Option option = optionService.findByPK(new OptionCodePK ("ALLOCATIONSENABLED", "ADMIN", "ADMIN"));
       	if (importMapProcessPreferenceDTO.isAllocationsEnabled()) {
        	option.setValue("1");       		
       	} else {
        	option.setValue("0");         		
       	}
    	optionService.saveOrUpdate(option); 
    	
      logger.debug("enter updateAdminImportMapProcessAction");  
    	
        option = optionService.findByPK(new OptionCodePK ("PROCRULEENABLED", "ADMIN", "ADMIN"));
       	if (importMapProcessPreferenceDTO.isProcessrulesEnabled()) {
        	option.setValue("1");       		
       	} else {
        	option.setValue("0");         		
       	}
    	optionService.saveOrUpdate(option); 
    	
    	option = optionService.findByPK(new OptionCodePK ("TAXALLOCENABLED", "ADMIN", "ADMIN"));
       	if (importMapProcessPreferenceDTO.isTaxCodeAllocationsEnabled()) {
        	option.setValue("1");       		
       	} else {
        	option.setValue("0");         		
       	}
    	optionService.saveOrUpdate(option); 
    	
    	option = optionService.findByPK(new OptionCodePK ("SALEAPIDATABASE", "ADMIN", "ADMIN"));
    	option.setValue(importMapProcessPreferenceDTO.getSaleApiDatabase());
        optionService.saveOrUpdate(option); 
        logger.debug("enter updateAdminImportMapProcessAction ::SALEAPIDATABASE"); 
        
        option = optionService.findByPK(new OptionCodePK ("SALEAPISERVER", "ADMIN", "ADMIN"));
    	option.setValue(importMapProcessPreferenceDTO.getSaleApiServer());
        optionService.saveOrUpdate(option); 
        logger.debug("enter updateAdminImportMapProcessAction ::SALEAPISERVER");
        
        option = optionService.findByPK(new OptionCodePK ("SALEAPIUSERID", "ADMIN", "ADMIN"));
    	option.setValue(importMapProcessPreferenceDTO.getSaleApiUserId());
        optionService.saveOrUpdate(option); 
        logger.debug("enter updateAdminImportMapProcessAction ::SALEAPIUSERID"); 
        
        option = optionService.findByPK(new OptionCodePK ("SALEAPIPASSWORD", "ADMIN", "ADMIN"));
    	option.setValue(importMapProcessPreferenceDTO.getSaleApiPassword());
        optionService.saveOrUpdate(option); 
        logger.debug("enter updateAdminImportMapProcessAction ::SALEAPIPASSWORD"); 

    	 this.setDisplayButton(false); 
         this.setErrorMessage(null);   	
    	logger.debug("exit updateAdminImportMapProcessAction");     	
    	return null;
    }  
    
    public String updateSystemGLExtractAction() {
    	logger.debug("enter updateSystemGLExtractAction");  
    	
        Option option = optionService.findByPK(new OptionCodePK ("GLEXTRACTEXT", "SYSTEM", "SYSTEM"));    	
    	option.setValue(importMapProcessPreferenceDTO.getGlExtractFileExtension());
    	optionService.saveOrUpdate(option);    	
    	
        option = optionService.findByPK(new OptionCodePK ("GLEXTRACTFILEPATH", "SYSTEM", "SYSTEM"));    	
    	option.setValue(importMapProcessPreferenceDTO.getGlExtractFilePath());
    	optionService.saveOrUpdate(option);    	
    	
        option = optionService.findByPK(new OptionCodePK ("GLEXTRACTPREFIX", "SYSTEM", "SYSTEM"));    	
    	option.setValue(importMapProcessPreferenceDTO.getGlExtractFilePrefix());
    	optionService.saveOrUpdate(option);       	
    	
    	logger.debug("exit updateSystemGLExtractAction"); 
    	 this.setDisplayButton(false); 
         this.setErrorMessage(null);
    	return null; 
    }
    
    public String updateAdminGLExtractAction() {
    	logger.debug("enter updateAdminGLExtractAction");  
        Option option = optionService.findByPK(new OptionCodePK ("GLEXTRACTENABLED", "ADMIN", "ADMIN"));  
        if (importMapProcessPreferenceDTO.isGlExtractEnabled()) {
            option.setValue("1");	
        } else {
            option.setValue("0");	
        }
    	optionService.saveOrUpdate(option);    	
    	
        option = optionService.findByPK(new OptionCodePK ("GLEXTRACTMARK", "ADMIN", "ADMIN")); 
        if (importMapProcessPreferenceDTO.isGlExtractSetMark()) {
            option.setValue("1");	        	
        } else {
            option.setValue("0");	
        }
    	optionService.saveOrUpdate(option);    	
    	
        option = optionService.findByPK(new OptionCodePK ("GLEXTRACTWIN", "ADMIN", "ADMIN"));    	
    	option.setValue(importMapProcessPreferenceDTO.getGlExtractWindowName());
    	optionService.saveOrUpdate(option);       	
    	 this.setDisplayButton(false); 
         this.setErrorMessage(null);
    	logger.debug("exit updateAdminGLExtractAction"); 
    	return null;
    }   
    
    public String updateSystemSecurityAction() {
    	logger.debug("enter updateSystemSecurityAction");  
        Option option = optionService.findByPK(new OptionCodePK ("PWEXPDAYS", "SYSTEM", "SYSTEM"));    	
    	option.setValue(importMapProcessPreferenceDTO.getPwdExpirationWarningDays());
    	optionService.saveOrUpdate(option);    	
    	
        option = optionService.findByPK(new OptionCodePK ("PWDENYMSG", "SYSTEM", "SYSTEM"));    	
    	option.setValue(importMapProcessPreferenceDTO.getDenyPasswordChangeMessage());
    	optionService.saveOrUpdate(option);    	
    	
        option = optionService.findByPK(new OptionCodePK ("PWUSERCHG", "SYSTEM", "SYSTEM"));
        if (importMapProcessPreferenceDTO.isChgPassword()) {
        	option.setValue("1");        	
        } else {
        	option.setValue("0");
        }
    	optionService.saveOrUpdate(option);  
    	 this.setDisplayButton(false); 
         this.setErrorMessage(null);
    	logger.debug("exit updateSystemSecurityAction"); 
    	return null;
    }    
    
    public String updateAdminSecurityAction() {
    	logger.debug("enter updateAdminSecurityAction");  
        Option option = optionService.findByPK(new OptionCodePK ("EXTENDSEC", "ADMIN", "ADMIN"));  
        if (importMapProcessPreferenceDTO.isActivateExtendedSecurity()) {
            option.setValue("1");	
        } else {
            option.setValue("0");	
        }
    	optionService.saveOrUpdate(option);    	
    	
        option = optionService.findByPK(new OptionCodePK ("DEBUGSEC", "ADMIN", "ADMIN")); 
        if (importMapProcessPreferenceDTO.isActivateDebugExtendedSecurity()) {
            option.setValue("1");	        	
        } else {
            option.setValue("0");	
        }
    	optionService.saveOrUpdate(option);    	      	
    	 this.setDisplayButton(false); 
         this.setErrorMessage(null);
    	logger.debug("exit updateAdminSecurityAction");
    	return null;
    }   
    
    public String updateSystemDirectoriesAction() {
    this.logger.debug("enter updateSystemDirectoriesAction");
    Option option = this.optionService.findByPK(new OptionCodePK("DEFAULTDIRCRYSTAL", "SYSTEM", "SYSTEM"));
    option.setValue(this.importMapProcessPreferenceDTO.getDefaultDirCrystal());
    this.optionService.saveOrUpdate(option);
    
    option = this.optionService.findByPK(new OptionCodePK("IMPEXPFILEEXT", "SYSTEM", "SYSTEM"));
    option.setValue(this.importMapProcessPreferenceDTO.getImpExpFileExt());
    this.optionService.saveOrUpdate(option);
    
    option = this.optionService.findByPK(new OptionCodePK("IMPEXPFILEPATH", "SYSTEM", "SYSTEM"));
    option.setValue(this.importMapProcessPreferenceDTO.getImpExpFilePath());
    this.optionService.saveOrUpdate(option);
    
    option = this.optionService.findByPK(new OptionCodePK("REFDOCFILEEXT", "SYSTEM", "SYSTEM"));
    option.setValue(this.importMapProcessPreferenceDTO.getRefDocFileExt());
    this.optionService.saveOrUpdate(option);
    
    option = this.optionService.findByPK(new OptionCodePK("DOWNLOADPATH", "SYSTEM", "SYSTEM"));
    option.setValue(this.importMapProcessPreferenceDTO.getDownloadFilePath());
    this.optionService.saveOrUpdate(option);
    String downloadUserId = this.importMapProcessPreferenceDTO.getDownloadUserId();
    String downloadPassword = this.importMapProcessPreferenceDTO.getDownloadPassword();
    if ((downloadUserId != null) && (!downloadUserId.trim().equals("")) && ((downloadPassword == null) || (downloadPassword.trim().equals(""))))
    {
      setErrorMessage("Please enter the Password");
      return null;
    }
    if ((downloadPassword != null) && (!downloadPassword.trim().equals("")) && ((downloadUserId == null) || (downloadUserId.trim().equals(""))))
    {
      setErrorMessage("Please enter the Username");
      return null;
    }
    option = this.optionService.findByPK(new OptionCodePK("DOWNLOADUSERID", "SYSTEM", "SYSTEM"));
    option.setValue(this.importMapProcessPreferenceDTO.getDownloadUserId());
    this.optionService.saveOrUpdate(option);
    
    option = this.optionService.findByPK(new OptionCodePK("DOWNLOADPASSWORD", "SYSTEM", "SYSTEM"));
    option.setValue(this.importMapProcessPreferenceDTO.getDownloadPassword());
    this.optionService.saveOrUpdate(option);
    
    option = this.optionService.findByPK(new OptionCodePK("REFDOCFILEPATH", "SYSTEM", "SYSTEM"));
    String fullPath = this.importMapProcessPreferenceDTO.getRefDocFilePath();
    String path = null;
    String tempDocName = null;
    String validDoc = null;
    fullPath = this.importMapProcessPreferenceDTO.getRefDocFilePath();
    tempDocName = fullPath.substring(fullPath.lastIndexOf("\\") + 1);
    validDoc = tempDocName.substring(tempDocName.lastIndexOf(".") + 1);
    if (((tempDocName.contains(".")) && (validDoc.length() == 4)) || (validDoc.length() == 3))
    {
      path = fullPath.substring(0, fullPath.lastIndexOf("\\"));
      option.setValue(path);
    }
    else
    {
      option.setValue(this.importMapProcessPreferenceDTO.getRefDocFilePath());
    }
    this.optionService.saveOrUpdate(option);
    
    option = this.optionService.findByPK(new OptionCodePK("SQLFILEEXT", "SYSTEM", "SYSTEM"));
    option.setValue(this.importMapProcessPreferenceDTO.getSqlFileExt());
    this.optionService.saveOrUpdate(option);
    
    option = this.optionService.findByPK(new OptionCodePK("SQLFILEPATH", "SYSTEM", "SYSTEM"));
    option.setValue(this.importMapProcessPreferenceDTO.getSqlFilePath());
    this.optionService.saveOrUpdate(option);
    setDisplayButton(false);
    setErrorMessage(null);
    if (this.applyButtonClicked) {
      try
      {
        this.logger.info("filePreferenceBackingBean line 951.  Before calling applyAction()");
        applyAction();
        this.logger.info("filePreferenceBackingBean line 953.  Before calling applyAction()");
      }
      catch (Exception e)
      {
        this.logger.info("filePreferenceBackingBean line 955. exception e = " + e.getMessage());
      }
    }
    setApplyButtonClicked(false);
    this.logger.debug("exit updateSystemDirectoriesAction");
    return null;}    
    
    public String updateSystemRatePointAction() {

		Option option = optionService.findByPK(new OptionCodePK(
				"LATLONGLOOKUP", "SYSTEM", "SYSTEM"));
		if (importMapProcessPreferenceDTO.isRatepointLatlongLookup()) {
			option.setValue("1");
		} else {
			option.setValue("0");
		}
		optionService.saveOrUpdate(option);

		option = optionService.findByPK(new OptionCodePK("DEFAULTJURISDICTION",
				"SYSTEM", "SYSTEM"));
		option.setValue(importMapProcessPreferenceDTO
				.getRatepointDefaultJurisdiction());
		optionService.saveOrUpdate(option);

		option = optionService.findByPK(new OptionCodePK("DEFAULTTAXCODE",
				"SYSTEM", "SYSTEM"));
		//option.setValue(importMapProcessPreferenceDTO
		//		.getRatepointDefaultTaxCode());
		
		//filterTaxCodeHandlerDefault.setTaxcodeCode(importMapProcessPreferenceDTO.getRatepointDefaultTaxCode());
		
		importMapProcessPreferenceDTO.setRatepointDefaultTaxCode(filterTaxCodeHandlerDefault.getTaxcodeCode());
		option.setValue(filterTaxCodeHandlerDefault.getTaxcodeCode());	
		optionService.saveOrUpdate(option);
		
		//0004426
		option = optionService.findByPK(new OptionCodePK("GENERICJURAPI",
				"SYSTEM", "SYSTEM"));
		option.setValue(importMapProcessPreferenceDTO.getGenericJurApi());
		optionService.saveOrUpdate(option);

		this.setDisplayButton(false);
		this.setErrorMessage(null);
		logger.debug("exit updateSystemRatePointAction");
		return null;
	}
    
    public String cancelSystemRatePointAction() {
		logger.debug("enter cancelSystemRatePointAction");
		matrixDTO = null;
		matrixDTO = getMatrixDTO();
		companyDTO = null;
		companyDTO = getCompanyDTO();
		batchPreferenceDTO = null;
		batchPreferenceDTO = getBatchPreferenceDTO();
		importMapProcessPreferenceDTO = null;
		importMapProcessPreferenceDTO = getImportMapProcessPreferenceDTO();
		userPreferenceDTO = null;
		userPreferenceDTO = getUserPreferenceDTO();
		
		filterTaxCodeHandlerDefault.setTaxcodeCode(importMapProcessPreferenceDTO.getRatepointDefaultTaxCode());
		
		this.setDisplayButton(false);
		this.setErrorMessage("");
		logger.debug("exit cancelSystemRatePointAction");
		return "filepref_system_ratepoint";
	}
    
    public String updateUserDirectoriesAction() {
    	logger.debug("enter updateUserDirectoriesAction");    
    	
        Option option = optionService.findByPK(new OptionCodePK ("IMPORTFILEEXT", "USER", "STSCORP"));   
    	option.setValue(importMapProcessPreferenceDTO.getImportFileExt());
    	optionService.saveOrUpdate(option);
    	
        option = optionService.findByPK(new OptionCodePK ("IMPORTFILEPATH", "USER", "STSCORP"));   
    	option.setValue(importMapProcessPreferenceDTO.getImportFilePath());
    	optionService.saveOrUpdate(option);    	
    	
        option = optionService.findByPK(new OptionCodePK ("USERDIRCRYSTAL", "USER", "STSCORP"));   
    	option.setValue(importMapProcessPreferenceDTO.getUserDirCrystal());
    	optionService.saveOrUpdate(option);     	
    	 this.setDisplayButton(false); 
         this.setErrorMessage(null);
    	logger.debug("exit updateUserDirectoriesAction");     	
    	return null;
    } 
    
    public String updateSystemTaxUpdatesAction() {
    	logger.debug("enter updateSystemTaxUpdatesAction");  
    	
        Option option = optionService.findByPK(new OptionCodePK ("LASTTAXRATEDATEFULL", "SYSTEM", "SYSTEM"));    	
    	option.setValue(importMapProcessPreferenceDTO.getLastTaxRateDateFull());
    	optionService.saveOrUpdate(option);    	
    	
        option = optionService.findByPK(new OptionCodePK ("LASTTAXRATEDATEUPDATE", "SYSTEM", "SYSTEM"));    	
    	option.setValue(importMapProcessPreferenceDTO.getLastTaxRateDateUpdate());
    	optionService.saveOrUpdate(option);    	
    	
        option = optionService.findByPK(new OptionCodePK ("LASTTAXRATERELFULL", "SYSTEM", "SYSTEM"));
    	option.setValue(importMapProcessPreferenceDTO.getLastTaxRateRelFull());
    	optionService.saveOrUpdate(option);  
    	
        option = optionService.findByPK(new OptionCodePK ("LASTTAXRATERELUPDATE", "SYSTEM", "SYSTEM"));
    	option.setValue(importMapProcessPreferenceDTO.getLastTaxRateRelUpdate());
    	optionService.saveOrUpdate(option);     
    	
        option = optionService.findByPK(new OptionCodePK ("TAXWARNRATE", "SYSTEM", "SYSTEM"));
    	option.setValue(importMapProcessPreferenceDTO.getTaxWarnRate());
    	optionService.saveOrUpdate(option); 
    	
    	option = optionService.findByPK(new OptionCodePK ("LASTRULEDATEUPDATE", "SYSTEM", "SYSTEM"));
    	option.setValue(importMapProcessPreferenceDTO.getLastRuleDateUpdate());
    	optionService.saveOrUpdate(option); 
    	
    	option = optionService.findByPK(new OptionCodePK ("LASTRULERELUPDATE", "SYSTEM", "SYSTEM"));
    	option.setValue(importMapProcessPreferenceDTO.getLastRuleRelUpdate());
    	optionService.saveOrUpdate(option); 
    	
    	option = optionService.findByPK(new OptionCodePK ("LASTSITUSDATEUPDATE", "SYSTEM", "SYSTEM"));
    	option.setValue(importMapProcessPreferenceDTO.getLastSitusDateUpdate());
    	optionService.saveOrUpdate(option); 
    	
    	option = optionService.findByPK(new OptionCodePK ("LASTSITUSRELUPDATE", "SYSTEM", "SYSTEM"));
    	option.setValue(importMapProcessPreferenceDTO.getLastSitusRelUpdate());
    	optionService.saveOrUpdate(option); 
    	
    	 this.setDisplayButton(false); 
         this.setErrorMessage(null);
    	logger.debug("exit updateSystemTaxUpdatesAction"); 
    	return null;
    }
    
    public String updateSystemTaxCodeRulesUpdatesAction() {
    	logger.debug("enter updateSystemTaxCodeRulesUpdatesAction");  
    	
        Option option = optionService.findByPK(new OptionCodePK ("LASTRULEDATEFULL", "SYSTEM", "SYSTEM"));    	
    	option.setValue(importMapProcessPreferenceDTO.getLastTaxCodeRulesDateFull());
    	optionService.saveOrUpdate(option);    	
    	
        option = optionService.findByPK(new OptionCodePK ("LASTRULEDATEUPDATE", "SYSTEM", "SYSTEM"));    	
    	option.setValue(importMapProcessPreferenceDTO.getLastTaxCodeRulesDateUpdate());
    	optionService.saveOrUpdate(option);    	
    	
        option = optionService.findByPK(new OptionCodePK ("LASTRULERELFULL", "SYSTEM", "SYSTEM"));
    	option.setValue(importMapProcessPreferenceDTO.getLastTaxCodeRulesRelFull());
    	optionService.saveOrUpdate(option);  
    	
        option = optionService.findByPK(new OptionCodePK ("LASTRULERELUPDATE", "SYSTEM", "SYSTEM"));
    	option.setValue(importMapProcessPreferenceDTO.getLastTaxCodeRulesRelUpdate());
    	optionService.saveOrUpdate(option);     
    	
    	this.setDisplayButton(false); 
    	this.setErrorMessage(null);
    	logger.debug("exit updateSystemTaxCodeRulesUpdatesAction"); 
    	return null;
    }
    
    public String updateSystemSitusRulesUpdatesAction() {
    	logger.debug("enter updateSystemSitusRulesUpdatesAction");  
    	
        Option option = optionService.findByPK(new OptionCodePK ("LASTSITUSRULEDATEFULL", "SYSTEM", "SYSTEM"));    	
    	option.setValue(importMapProcessPreferenceDTO.getLastSitusRulesDateFull());
    	optionService.saveOrUpdate(option);    	
    	
        option = optionService.findByPK(new OptionCodePK ("LASTSITUSRULEDATEUPDATE", "SYSTEM", "SYSTEM"));    	
    	option.setValue(importMapProcessPreferenceDTO.getLastSitusRulesDateUpdate());
    	optionService.saveOrUpdate(option);    	
    	
        option = optionService.findByPK(new OptionCodePK ("LASTSITUSRULERELFULL", "SYSTEM", "SYSTEM"));
    	option.setValue(importMapProcessPreferenceDTO.getLastSitusRulesRelFull());
    	optionService.saveOrUpdate(option);  
    	
        option = optionService.findByPK(new OptionCodePK ("LASTSITUSRULERELUPDATE", "SYSTEM", "SYSTEM"));
    	option.setValue(importMapProcessPreferenceDTO.getLastSitusRulesRelUpdate());
    	optionService.saveOrUpdate(option);     
    	
    	this.setDisplayButton(false); 
    	this.setErrorMessage(null);
    	logger.debug("exit updateSystemSitusRulesUpdatesAction"); 
    	return null;
    }
    
    public String updateSystemComplianceAction() {
    	logger.debug("enter updateSystemComplianceAction");  
    	logger.debug("getDefCCHTaxCatCode() = " + importMapProcessPreferenceDTO.getDefCCHTaxCatCode()); 
    	logger.debug("getDefCCHGroupCode() = " + importMapProcessPreferenceDTO.getDefCCHGroupCode()); 
    	logger.debug("getTpExtractExt() = " + importMapProcessPreferenceDTO.getTpExtractExt()); 
    	logger.debug("getTpExtractFilePath() = " + importMapProcessPreferenceDTO.getTpExtractFilePath()); 
    	logger.debug("getTpExtractFilePrefix() = " + importMapProcessPreferenceDTO.getTpExtractFilePrefix());
    	
        Option option = optionService.findByPK(new OptionCodePK ("DEFCCHTAXCATCODE", "SYSTEM", "SYSTEM"));    	
    	option.setValue(importMapProcessPreferenceDTO.getDefCCHTaxCatCode());
    	optionService.saveOrUpdate(option);    	
    	
        option = optionService.findByPK(new OptionCodePK ("DEFCCHGROUPCODE", "SYSTEM", "SYSTEM"));    	
    	option.setValue(importMapProcessPreferenceDTO.getDefCCHGroupCode());
    	optionService.saveOrUpdate(option);    	
    	
        option = optionService.findByPK(new OptionCodePK ("TPEXTRACTEXT", "SYSTEM", "SYSTEM"));
    	option.setValue(importMapProcessPreferenceDTO.getTpExtractExt());
    	optionService.saveOrUpdate(option);  
    	    	
    	option = optionService.findByPK(new OptionCodePK ("TPEXTRACTEFILEPATH", "SYSTEM", "SYSTEM"));   		
    	option.setValue(importMapProcessPreferenceDTO.getTpExtractFilePath());    

    	option = optionService.findByPK(new OptionCodePK ("TPEXTRACTPREFIX", "SYSTEM", "SYSTEM"));
    	option.setValue(importMapProcessPreferenceDTO.getTpExtractFilePrefix());
    	optionService.saveOrUpdate(option);  	
    	 this.setDisplayButton(false); 
         this.setErrorMessage(null);
    	logger.debug("exit updateSystemComplianceAction"); 
    	return null;
    }    
    
    public String updateAdminComplianceAction() {
    	logger.debug("enter updateAdminComplianceAction");  

        Option option = optionService.findByPK(new OptionCodePK ("TPEXTRACTENABLED", "ADMIN", "ADMIN"));  
        if (importMapProcessPreferenceDTO.isTpExtractEnabled()) {
            option.setValue("1");	
        } else {
            option.setValue("0");	
        }
    	optionService.saveOrUpdate(option);    	 	      	
    	 this.setDisplayButton(false); 
         this.setErrorMessage(null);
    	logger.debug("exit updateAdminComplianceAction");
    	return null;
    }    

	public String cancelSystemGeneralAction() {
		logger.debug("enter cancelSystemGeneralAction");
		matrixDTO = null;
		matrixDTO = getMatrixDTO();
		companyDTO = null;
		companyDTO = getCompanyDTO();
		batchPreferenceDTO = null;
		batchPreferenceDTO = getBatchPreferenceDTO();
		importMapProcessPreferenceDTO = null;
		importMapProcessPreferenceDTO = getImportMapProcessPreferenceDTO();
		userPreferenceDTO = null;
		userPreferenceDTO = getUserPreferenceDTO();
		licenseKey = getLicense();
		this.setDisplayButton(false);
    	this.setErrorMessage("");    		
		logger.debug("exit cancelSystemGeneralAction");		
	    return "filepref_system_general"; 
	}

	private String getLicense() {
		String lk = null;
		Option option = optionService.findByPK(new OptionCodePK("LICENSEKEY", "SYSTEM", "SYSTEM"));
    	if(option != null && option.getValue() != null) {
    		lk = option.getValue();
    	}
		
		return lk;
	}

	public String cancelAdminBatchesAction() {
		logger.debug("enter cancelAdminBatchesAction");
		matrixDTO = null;
		matrixDTO = getMatrixDTO();
		companyDTO = null;
		companyDTO = getCompanyDTO();
		batchPreferenceDTO = null;
		batchPreferenceDTO = getBatchPreferenceDTO();
		importMapProcessPreferenceDTO = null;
		importMapProcessPreferenceDTO = getImportMapProcessPreferenceDTO();
		userPreferenceDTO = null;
		userPreferenceDTO = getUserPreferenceDTO();
		this.setDisplayButton(false);
    	this.setErrorMessage(""); 
		logger.debug("exit cancelAdminBatchesAction");		
	    return "filepref_admin_batch"; 
	}	
	
	public String cancelAdminComplianceAction() {
		logger.debug("enter cancelAdminComplianceAction");
		matrixDTO = null;
		matrixDTO = getMatrixDTO();
		companyDTO = null;
		companyDTO = getCompanyDTO();
		batchPreferenceDTO = null;
		batchPreferenceDTO = getBatchPreferenceDTO();
		importMapProcessPreferenceDTO = null;
		importMapProcessPreferenceDTO = getImportMapProcessPreferenceDTO();
		userPreferenceDTO = null;
		userPreferenceDTO = getUserPreferenceDTO();
		this.setDisplayButton(false);
    	this.setErrorMessage(""); 
		logger.debug("exit cancelAdminComplianceAction");		
	    return "filepref_admin_compliance"; 
	}	
	
	public String cancelAdminGeneralAction() {
		logger.debug("enter cancelAdminGeneralAction");
		matrixDTO = null;
		matrixDTO = getMatrixDTO();
		companyDTO = null;
		companyDTO = getCompanyDTO();
		batchPreferenceDTO = null;
		batchPreferenceDTO = getBatchPreferenceDTO();
		importMapProcessPreferenceDTO = null;
		importMapProcessPreferenceDTO = getImportMapProcessPreferenceDTO();
		userPreferenceDTO = null;
		userPreferenceDTO = getUserPreferenceDTO();
		this.setDisplayButton(false);
    	this.setErrorMessage(""); 
		logger.debug("exit cancelAdminGeneralAction");		
	    return "filepref_admin_general"; 
	}	
	
	public String cancelAdminRatepointAction() {
		logger.debug("enter cancelAdminRatepointAction");
		matrixDTO = null;
		matrixDTO = getMatrixDTO();
		companyDTO = null;
		companyDTO = getCompanyDTO();
		batchPreferenceDTO = null;
		batchPreferenceDTO = getBatchPreferenceDTO();
		importMapProcessPreferenceDTO = null;
		importMapProcessPreferenceDTO = getImportMapProcessPreferenceDTO();
		userPreferenceDTO = null;
		userPreferenceDTO = getUserPreferenceDTO();
		this.setDisplayButton(false);
		this.setErrorMessage("");
		logger.debug("exit cancelAdminRatepointAction");
		return "filepref_admin_ratepoint";
	}
	
	public String cancelAdminAIDAction() {
		logger.debug("enter cancelAdminAIDAction");
		
		adminPreferenceMap = null;
		refreshAdminAIDAction();

		
		this.setDisplayButton(false);
		this.setErrorMessage("");
		logger.debug("exit cancelAdminAIDAction");
		
		return "filepref_admin_aid";
	}
	
	public void refreshAdminAIDAction() {
		Map<String,String> map = getAdminPreferenceMap();

		String aidHostnames = map.get("AIDHOSTNAME");
		this.aidHostname = "";
		this.aidHostname2 = "";
    	if(aidHostnames!=null && aidHostnames.length()>0){
	    	String[] split = aidHostnames.split(";");	  
	    	if(split.length>0){aidHostname = split[0];}
	    	if(split.length>1){aidHostname2 = split[1];}
    	}

		this.aidInstallation = map.get("AIDSETTINGPATH");
		this.loginBean.resetAidMenuEnabled();
		
		this.daysToKeep = map.get("AIDPURGEDAYS");

		String aidPorts = map.get("AIDPORT");
		this.aidPort = "";
		this.aidPort2 = "";
    	if(aidPorts!=null && aidPorts.length()>0){
	    	String[] split = aidPorts.split(";");	  
	    	if(split.length>0){aidPort = split[0];}
	    	if(split.length>1){aidPort2 = split[1];}
    	}

		this.emailHost = map.get("EMAILHOST");
		this.emailPort = map.get("EMAILPORT");
		this.emailFrom = map.get("EMAILFROM");
	}
	
	public boolean isAidSettingFileExist(String path){
		if(path!=null && path.length()>0){
			File aFile = new File(path);
			if(aFile.isDirectory()){
				if(new File(path + aFile.separator + "conf" + aFile.separator + "AIDSetting.xml").isFile()){
					return true;
				}
			}
		}
		
		return false;
	}
	
	public String cancelSystemAIDAction() {
		logger.debug("enter cancelSystemAIDAction");
		
		systemPreferenceMap = null;
		refreshSystemAIDAction();
		
		
		this.setDisplayButton(false);
		this.setErrorMessage("");
		logger.debug("exit cancelSystemAIDAction");
		return "filepref_system_aid";
	}
	
	public void refreshSystemAIDAction() {
		Map<String,String> map = getSystemPreferenceMap();

		if ("1".equalsIgnoreCase(map.get("AIDUSERADDFILE"))) {
			this.setAllowAidAddFile(true);
		} else {
			this.setAllowAidAddFile(false);				
		}  
			
		if ("1".equalsIgnoreCase(map.get("AIDUSERREIMPORTFILE"))) {
			this.setAllowAidReimportFile(true);
		} else {
			this.setAllowAidReimportFile(false);				
		}  
		
		if ("1".equalsIgnoreCase(map.get("AIDUSERDELETEFILE"))) {
			this.setAllowAidDeleteFile(true);
		} else {
			this.setAllowAidDeleteFile(false);				
		}
		
		if ("1".equalsIgnoreCase(map.get("EMAILSTART"))) {
			this.setEmailStart(true);
		} else {
			this.setEmailStart(false);				
		}  
		
		this.emailStop = map.get("EMAILSTOP");
		this.emailTo = map.get("EMAILTO");
		this.emailCc = map.get("EMAILCC"); 
	}
	
	public String cancelAdminControlPointAction() {
		logger.debug("enter cancelAdminControlpointAction");
		matrixDTO = null;
		matrixDTO = getMatrixDTO();
		companyDTO = null;
		companyDTO = getCompanyDTO();
		batchPreferenceDTO = null;
		batchPreferenceDTO = getBatchPreferenceDTO();
		importMapProcessPreferenceDTO = null;
		importMapProcessPreferenceDTO = getImportMapProcessPreferenceDTO();
		userPreferenceDTO = null;
		userPreferenceDTO = getUserPreferenceDTO();
		this.setDisplayButton(false);
		this.setErrorMessage("");
		logger.debug("exit cancelAdminControlpointAction");
		return "filepref_admin_controlpoint";
	}
	
	public String cancelAdminGLExtractAction() {
		logger.debug("enter cancelAdminGLExtractAction");
		matrixDTO = null;
		matrixDTO = getMatrixDTO();
		companyDTO = null;
		companyDTO = getCompanyDTO();
		batchPreferenceDTO = null;
		batchPreferenceDTO = getBatchPreferenceDTO();
		importMapProcessPreferenceDTO = null;
		importMapProcessPreferenceDTO = getImportMapProcessPreferenceDTO();
		userPreferenceDTO = null;
		userPreferenceDTO = getUserPreferenceDTO();
		this.setDisplayButton(false);
    	this.setErrorMessage(""); 
		logger.debug("exit cancelAdminGLExtractAction");		
	    return "filepref_admin_glextract"; 
	}	
	
	public String cancelAdminImportMapProcessAction() {
		logger.debug("enter cancelAdminImportMapProcessAction");
		matrixDTO = null;
		matrixDTO = getMatrixDTO();
		companyDTO = null;
		companyDTO = getCompanyDTO();
		batchPreferenceDTO = null;
		batchPreferenceDTO = getBatchPreferenceDTO();
		importMapProcessPreferenceDTO = null;
		importMapProcessPreferenceDTO = getImportMapProcessPreferenceDTO();
		userPreferenceDTO = null;
		userPreferenceDTO = getUserPreferenceDTO();
		this.setDisplayButton(false);
    	this.setErrorMessage(""); 
		logger.debug("exit cancelAdminImportMapProcessAction");		
	    return "filepref_admin_import"; 
	}	
	
	public String cancelAdminSecurityAction() {
		logger.debug("enter cancelAdminSecurityAction");
		matrixDTO = null;
		matrixDTO = getMatrixDTO();
		companyDTO = null;
		companyDTO = getCompanyDTO();
		batchPreferenceDTO = null;
		batchPreferenceDTO = getBatchPreferenceDTO();
		importMapProcessPreferenceDTO = null;
		importMapProcessPreferenceDTO = getImportMapProcessPreferenceDTO();
		userPreferenceDTO = null;
		userPreferenceDTO = getUserPreferenceDTO();
		this.setDisplayButton(false);
    	this.setErrorMessage(""); 
		logger.debug("exit cancelAdminSecurityAction");		
	    return "filepref_admin_security"; 
	}	
	
	public String cancelSystemBatchesAction() {
		logger.debug("enter cancelSystemBatchesAction");
		matrixDTO = null;
		matrixDTO = getMatrixDTO();
		companyDTO = null;
		companyDTO = getCompanyDTO();
		batchPreferenceDTO = null;
		batchPreferenceDTO = getBatchPreferenceDTO();
		importMapProcessPreferenceDTO = null;
		importMapProcessPreferenceDTO = getImportMapProcessPreferenceDTO();
		userPreferenceDTO = null;
		userPreferenceDTO = getUserPreferenceDTO();
		this.setDisplayButton(false);
    	this.setErrorMessage(""); 
		logger.debug("exit cancelSystemBatchesAction");		
	    return "filepref_system_batch"; 
	}	
	
	public String cancelSystemComplianceAction() {
		logger.debug("enter cancelSystemComplianceAction");
		matrixDTO = null;
		matrixDTO = getMatrixDTO();
		companyDTO = null;
		companyDTO = getCompanyDTO();
		batchPreferenceDTO = null;
		batchPreferenceDTO = getBatchPreferenceDTO();
		importMapProcessPreferenceDTO = null;
		importMapProcessPreferenceDTO = getImportMapProcessPreferenceDTO();
		userPreferenceDTO = null;
		userPreferenceDTO = getUserPreferenceDTO();
		this.setDisplayButton(false);
    	this.setErrorMessage(""); 
		this.displayUpload = false;
		logger.debug("exit cancelSystemComplianceAction");		
	    return "filepref_system_compliance"; 
	}	
	
	public String cancelSystemDirectoriesAction() {
		logger.debug("enter cancelSystemDirectoriesAction");
		matrixDTO = null;
		matrixDTO = getMatrixDTO();
		companyDTO = null;
		companyDTO = getCompanyDTO();
		batchPreferenceDTO = null;
		batchPreferenceDTO = getBatchPreferenceDTO();
		importMapProcessPreferenceDTO = null;
		importMapProcessPreferenceDTO = getImportMapProcessPreferenceDTO();
		userPreferenceDTO = null;
		userPreferenceDTO = getUserPreferenceDTO();
		this.setDisplayButton(false);
    	this.setErrorMessage(""); 
    	this.setApplyButtonClicked(false);
		logger.debug("exit cancelSystemDirectoriesAction");		
	    return "filepref_system_directory"; 
	}	
	
	public String cancelSystemGLExtractAction() {
		logger.debug("enter cancelSystemGLExtractAction");
		matrixDTO = null;
		matrixDTO = getMatrixDTO();
		companyDTO = null;
		companyDTO = getCompanyDTO();
		batchPreferenceDTO = null;
		batchPreferenceDTO = getBatchPreferenceDTO();
		importMapProcessPreferenceDTO = null;
		importMapProcessPreferenceDTO = getImportMapProcessPreferenceDTO();
		userPreferenceDTO = null;
		userPreferenceDTO = getUserPreferenceDTO();
		this.setDisplayButton(false);
    	this.setErrorMessage(""); 
		logger.debug("exit cancelSystemGLExtractAction");		
	    return "filepref_system_glextract"; 
	}	
	
	public String cancelSystemImportMapProcessAction() {
		logger.debug("enter cancelSystemImportMapProcessAction");
		matrixDTO = null;
		matrixDTO = getMatrixDTO();
		companyDTO = null;
		companyDTO = getCompanyDTO();
		batchPreferenceDTO = null;
		batchPreferenceDTO = getBatchPreferenceDTO();
		importMapProcessPreferenceDTO = null;
		importMapProcessPreferenceDTO = getImportMapProcessPreferenceDTO();
		userPreferenceDTO = null;
		userPreferenceDTO = getUserPreferenceDTO();
		
		filterTaxCodeHandler.setTaxcodeCode(importMapProcessPreferenceDTO.getApplyTaxCodeAmount());
		
		this.setDisplayButton(false);
    	this.setErrorMessage(""); 
		logger.debug("exit cancelSystemImportMapProcessAction");		
	    return "filepref_system_import"; 
	}
	/*matrixDTO = null;
	matrixDTO = getMatrixDTO();
	companyDTO = null;
	companyDTO = getCompanyDTO();
	batchPreferenceDTO = null;
	batchPreferenceDTO = getBatchPreferenceDTO();
	importMapProcessPreferenceDTO = null;
	importMapProcessPreferenceDTO = getImportMapProcessPreferenceDTO();*/
	
	public String cancelSystemMatrixAction() {
		logger.debug("enter cancelSystemMatrixAction");
		matrixDTO = null;
		matrixDTO = getMatrixDTO();
		companyDTO = null;
		companyDTO = getCompanyDTO();
		batchPreferenceDTO = null;
		batchPreferenceDTO = getBatchPreferenceDTO();
		importMapProcessPreferenceDTO = null;
		importMapProcessPreferenceDTO = getImportMapProcessPreferenceDTO();
		userPreferenceDTO = null;
		userPreferenceDTO = getUserPreferenceDTO();
		this.setDisplayButton(false);
    	this.setErrorMessage(""); 
		logger.debug("exit cancelSystemMatrixAction");		
	    return "filepref_system_matrix"; 
	}	
	
	public String cancelSystemSecurityAction() {
		logger.debug("enter cancelSystemSecurityAction");
		matrixDTO = null;
		matrixDTO = getMatrixDTO();
		companyDTO = null;
		companyDTO = getCompanyDTO();
		batchPreferenceDTO = null;
		batchPreferenceDTO = getBatchPreferenceDTO();
		importMapProcessPreferenceDTO = null;
		importMapProcessPreferenceDTO = getImportMapProcessPreferenceDTO();
		userPreferenceDTO = null;
		userPreferenceDTO = getUserPreferenceDTO();
		this.setDisplayButton(false);
    	this.setErrorMessage(""); 
		logger.debug("exit cancelSystemSecurityAction");		
	    return "filepref_system_security"; 
	}	
	
	public String cancelSystemTaxUpdatesAction() {
		logger.debug("enter cancelSystemTaxUpdatesAction");
		matrixDTO = null;
		matrixDTO = getMatrixDTO();
		companyDTO = null;
		companyDTO = getCompanyDTO();
		batchPreferenceDTO = null;
		batchPreferenceDTO = getBatchPreferenceDTO();
		importMapProcessPreferenceDTO = null;
		importMapProcessPreferenceDTO = getImportMapProcessPreferenceDTO();
		userPreferenceDTO = null;
		userPreferenceDTO = getUserPreferenceDTO();
		this.setDisplayButton(false);
    	this.setErrorMessage(""); 
		logger.debug("exit cancelSystemTaxUpdatesAction");		
	    return "filepref_system_tax"; 
	}
	
	public String cancelSystemTaxCodeRulesUpdatesAction() {
		logger.debug("enter cancelSystemTaxCodeRulesUpdatesAction");
		matrixDTO = null;
		matrixDTO = getMatrixDTO();
		companyDTO = null;
		companyDTO = getCompanyDTO();
		batchPreferenceDTO = null;
		batchPreferenceDTO = getBatchPreferenceDTO();
		systemPreferenceMap = null;
		importMapProcessPreferenceDTO = null;
		importMapProcessPreferenceDTO = getImportMapProcessPreferenceDTO();
		userPreferenceDTO = null;
		userPreferenceDTO = getUserPreferenceDTO();
		this.setDisplayButton(false);
    	this.setErrorMessage(""); 
		logger.debug("exit cancelSystemTaxCodeRulesUpdatesAction");		
	    return "filepref_system_taxcode_rules"; 
	}
	
	public String cancelUserBatchesAction() {
		logger.debug("enter cancelUserBatchesAction");
		matrixDTO = null;
		matrixDTO = getMatrixDTO();
		companyDTO = null;
		companyDTO = getCompanyDTO();
		batchPreferenceDTO = null;
		batchPreferenceDTO = getBatchPreferenceDTO();
		importMapProcessPreferenceDTO = null;
		importMapProcessPreferenceDTO = getImportMapProcessPreferenceDTO();
		userPreferenceDTO = null;
		userPreferenceDTO = getUserPreferenceDTO();
		this.setDisplayButton(false);
    	this.setErrorMessage(""); 
		logger.debug("exit cancelUserBatchesAction");		
	    return "filepref_user_batch"; 
	}	
	
	public String cancelUserDirectoriesAction() {
		logger.debug("enter cancelUserDirectoriesAction");
		matrixDTO = null;
		matrixDTO = getMatrixDTO();
		companyDTO = null;
		companyDTO = getCompanyDTO();
		batchPreferenceDTO = null;
		batchPreferenceDTO = getBatchPreferenceDTO();
		importMapProcessPreferenceDTO = null;
		importMapProcessPreferenceDTO = getImportMapProcessPreferenceDTO();
		userPreferenceDTO = null;
		userPreferenceDTO = getUserPreferenceDTO();
		this.setDisplayButton(false);
    	this.setErrorMessage(""); 
		logger.debug("exit cancelUserDirectoriesAction");		
	    return "filepref_user_directory"; 
	}	
	
	public String cancelUserGeneralAction() {
		logger.debug("enter cancelUserGeneralAction");
		matrixDTO = null;
		matrixDTO = getMatrixDTO();
		companyDTO = null;
		companyDTO = getCompanyDTO();
		batchPreferenceDTO = null;
		batchPreferenceDTO = getBatchPreferenceDTO();
		importMapProcessPreferenceDTO = null;
		importMapProcessPreferenceDTO = getImportMapProcessPreferenceDTO();
		userPreferenceDTO = null;
		userPreferenceDTO = getUserPreferenceDTO();
        this.setDisplayButton(false); 
    	this.setErrorMessage("");        
		logger.debug("exit cancelUserGeneralAction");		
	    return "filepref_user_general"; 
	}
	
	public String cancelUserMatrixAction() {
		logger.debug("enter cancelUserMatrixAction");
		matrixDTO = null;
		matrixDTO = getMatrixDTO();
		companyDTO = null;
		companyDTO = getCompanyDTO();
		batchPreferenceDTO = null;
		batchPreferenceDTO = getBatchPreferenceDTO();
		importMapProcessPreferenceDTO = null;
		importMapProcessPreferenceDTO = getImportMapProcessPreferenceDTO();
		userPreferenceDTO = null;
		userPreferenceDTO = getUserPreferenceDTO();
		this.setDisplayButton(false);
    	this.setErrorMessage(""); 
		logger.debug("exit cancelUserMatrixAction");		
	    return "filepref_user_matrix"; 
	}	
	
	public String showUploadControl() {
		System.out.println("enter showUploadControl");
		this.setDisplayUpload(true);
		System.out.println("exit showUploadControl");		
		return null;
	}
	
	public Map<String,String> getDefaultPreferenceMap(){ 
    	logger.debug(" getDefaultPreferenceMap ");
    	if(defaultPreferenceMap == null) {
    		defaultPreferenceMap = new HashMap<String,String>();
    		defaultPreferenceMap.put("AUTOADDNEXUS", "Never");
    		defaultPreferenceMap.put("AUTOADDNEXUSNORIGHTS", "Never");
    	}
    	logger.debug("exit getDefaultPreferenceMap");       	
        return defaultPreferenceMap;
	}
	
    public Map<String,String> getSystemPreferenceMap(){ 
    	logger.debug(" getSystemPreferenceMap ");
    	if (systemPreferenceMap == null) {
    		List<Option> list = null;
        	list = optionService.getSystemPreferences();  
            logger.debug("list size = " + list.size()); 
            systemPreferenceMap = new HashMap<String,String>();
            for (Option opt : list) {
            	systemPreferenceMap.put(opt.getCodePK().getOptionCode(), opt.getValue());	
            }  		
    	}
        logger.debug("exit getSystemPreferenceMap");       	
        return systemPreferenceMap;
    }  
    
    public void setSystemPreferenceMap (Map<String,String> map) {
    	this.systemPreferenceMap = map;
    }
    
    public Map<String,String> getAdminPreferenceMap(){ 
    	logger.debug(" getAdminPreferenceMap ");
    	if (adminPreferenceMap == null) {
    		List<Option> list = null;
        	list = optionService.getAdminPreferences();  
            logger.debug("list size = " + list.size()); 
            adminPreferenceMap = new HashMap<String,String>();
            for (Option opt : list) {
            	adminPreferenceMap.put(opt.getCodePK().getOptionCode(), opt.getValue());	
            } 
    	}
        logger.debug("exit getAdminPreferenceMap");
        return adminPreferenceMap;
    } 
    
    public void setAdminPreferenceMap (Map<String,String> map) {
    	this.adminPreferenceMap = map;
    }    
    
    public Map<String,String> getUserPreferenceMap(){ 
    	logger.debug(" getUserPreferenceMap ");
    	if (userPreferenceMap == null) {
    		List<Option> list = null;
        	list = optionService.getUserPreferences();  
            logger.debug("list size = " + list.size()); 
            userPreferenceMap = new HashMap<String,String>();
            for (Option opt : list) {
            	userPreferenceMap.put(opt.getCodePK().getOptionCode(), opt.getValue());	
            }     		
    	}
        logger.debug("exit getUserPreferenceMap");
        return userPreferenceMap;
    }    
    
    public void setUserPreferenceMap (Map<String,String> map) {
    	this.userPreferenceMap = map;
    }     
    
	public CompanyDTO getCompanyDTO(){
		if (companyDTO == null) { 
			logger.debug ("*************enter new getCompanyDTO()********************");
			companyDTO = new CompanyDTO();
			Map<String,String> map = new HashMap<String,String>();
			map.putAll(getDefaultPreferenceMap());
			map.putAll(getSystemPreferenceMap());
			map.putAll(getAdminPreferenceMap());		
			map.putAll(getUserPreferenceMap());	
			logger.debug("map size = " + map.size());
			companyDTO.setName((String)map.get("COMPANYNAME"));
			companyDTO.setLogoUrl((String)map.get("LOGO"));
			
			companyDTO.setAutoAddNexus((String)map.get("AUTOADDNEXUS"));
			companyDTO.setAutoAddNexusNoRights((String)map.get("AUTOADDNEXUSNORIGHTS"));
			companyDTO.setDefaultNexusType((String)map.get("DEFAULTNEXUSTYPE"));
			
			String stsTimeZone = (String)map.get("SYSTEMTIMEZONE");		
			companyDTO.setStsTimeZone(stsTimeZone);
			logger.debug("stsTimeZone = " + stsTimeZone);
			companyDTO.setDefaultTimeZone((String)map.get("DEFUSERTIMEZONE"));
			String systemdst = (String)map.get("SYSTEMDST");
			if(("1").equals(systemdst)){
				companyDTO.setStsAdminDST(true);	
			}	
			String defuserdst = (String)map.get("DEFUSERDST");
			if(("1").equals(defuserdst)){
				companyDTO.setStsDST(true);	
			}	
			
			String splash = (String)map.get("SPLASH"); 
			logger.debug ("***splash = " + splash);			
			if ("1".equalsIgnoreCase(splash)) {
			    companyDTO.setSplashscreen(true);
			} else {
			    companyDTO.setSplashscreen(false);				
			}
			String sounds = (String)map.get("SOUNDS");
			logger.debug ("***sounds = " + sounds);			
			if ("1".equalsIgnoreCase(sounds)) {
			    companyDTO.setSound(true);
			} else {
			    companyDTO.setSound(false);			
			}
			String usertimezone = (String)map.get("USERTIMEZONE");	
			logger.debug ("***usertimezone = " + usertimezone);			
			companyDTO.setUserTimeZone(usertimezone);
			String userdst = (String)map.get("USERDST");
			logger.debug ("***userdst = " + userdst);
			if ("1".equalsIgnoreCase(userdst)) {
			    companyDTO.setUserDST(true);
			} else {
			    companyDTO.setUserDST(false);		
			}			
			String saveGridChanges = (String)map.get("SAVEGRIDCHANGES");
			logger.debug ("***saveGridChanges = " + saveGridChanges);
			companyDTO.setGridLayout(saveGridChanges);
			
			String version = (String)map.get("VERSION");
			logger.debug ("***version = " + version);
			companyDTO.setVersion(version);
			String aspCustomer = (String)map.get("ASP");
			logger.debug ("**aspCustomer = " + aspCustomer);
			if ("1".equalsIgnoreCase(aspCustomer)) {
				companyDTO.setAspCustomer(true);	
			} else {
				companyDTO.setAspCustomer(false);					
			}
			String pcoCustomer = (String)map.get("PCO");
			logger.debug ("**pcoCustomer = " + pcoCustomer);
			if ("1".equalsIgnoreCase(pcoCustomer)) {
				companyDTO.setPcoCustomer(true);	
			} else {
				companyDTO.setPcoCustomer(false);					
			}
			String stsAdminTimeZone = (String)map.get("STSCORPTIMEZONE");
			logger.debug ("***stsAdminTimeZone  = " + stsAdminTimeZone);
			companyDTO.setStsAdminTimeZone(stsAdminTimeZone);		
			String stsAdminDST = (String)map.get("STSCORPDST");
			logger.debug ("***stsAdminDST  = " + stsAdminDST);
			if ("1".equalsIgnoreCase(stsAdminDST)) {
				companyDTO.setStsAdminDST(true);	
			} else {
				companyDTO.setStsAdminDST(false);						
			}	
			
			String ratepointEnabled = (String) map.get("RATEPOINTENABLED");
			if ("1".equalsIgnoreCase(ratepointEnabled)) {
				companyDTO.setRatepointEnabled(true);
			} else {
				companyDTO.setRatepointEnabled(false);
			}

			String ratepointServer = (String) map.get("RATEPOINTSERVER");
			companyDTO.setRatepointServer(ratepointServer);

			String ratepointUserId = (String) map.get("RATEPOINTUSERID");
			companyDTO.setRatepointUserId(ratepointUserId);

			String ratepointPassword = (String) map.get("RATEPOINTPASSWORD");
			companyDTO.setRatepointPassword(ratepointPassword);

			String tbSpace = (String)map.get("INDEX_TABLESPACE");		
			logger.debug ("***tbSpace  = " + tbSpace);
			companyDTO.setTbSpace(tbSpace);
			
			//License
			licenseKey = (String)map.get("LICENSEKEY");
			String controlPointEnabled = (String) map.get("CONTROLPOINTENABLED");
			if ("1".equalsIgnoreCase(controlPointEnabled)) {
				companyDTO.setControlPointEnabled(true);
			} else {
				companyDTO.setControlPointEnabled(false);
			}
            //ControlPoint
			String controlPointServer = (String) map.get("CONTROLPOINTSERVER");
			companyDTO.setControlPointServer(controlPointServer);

			String controlPointUserId = (String) map.get("CONTROLPOINTUSERID");
			companyDTO.setControlPointUserId(controlPointUserId);

			String controlPointPassword = (String) map.get("CONTROLPOINTPASSWORD");
			companyDTO.setControlPointPassword(controlPointPassword);
			
			logger.debug ("*************exit new getCompanyDTO()********************");					
		}
		return companyDTO;
	}    
	
	public MatrixDTO getMatrixDTO() {
		if (matrixDTO == null) {
			matrixDTO = new MatrixDTO();
			Map<String,String> map = new HashMap<String,String>();	
			map.putAll(getSystemPreferenceMap());
			map.putAll(getAdminPreferenceMap());		
			map.putAll(getUserPreferenceMap());	
			matrixDTO.setSysDriverRef((String)map.get("DRIVERREFVAL"));
			if (map.get("MINIMUMTAXDRIVERS")  != null) {
				matrixDTO.setSysMinDriver(new Long((String)map.get("MINIMUMTAXDRIVERS")));				
			}
            if (map.get("MINIMUMLOCATIONDRIVERS") != null) {
    			matrixDTO.setSysMinLocationDriver(new Long((String)map.get("MINIMUMLOCATIONDRIVERS")));            	
            }
            if (map.get("MINIMUMALLOCATIONDRIVERS") != null) {
            	matrixDTO.setSysMinAllocationDriver(new Long ((String)map.get("MINIMUMALLOCATIONDRIVERS")));			
            }
            
            if (!(map.get("TAXCODEDROPDOWNTHRESHOLD") == null || map.get("TAXCODEDROPDOWNTHRESHOLD").isEmpty())) {
            	matrixDTO.setSysTaxCodeDropDownThreshold(map.get("TAXCODEDROPDOWNTHRESHOLD"));
            }

			matrixDTO.setUserTaxCode((String)map.get("DEFTAXCODETYPE"));
			matrixDTO.setUserCountry((String)map.get("DEFUSERCOUNTRY"));
			matrixDTO.setUserTransInd((String)map.get("DEFTRANSIND"));	
			matrixDTO.setSysFullThreshold((String)map.get("FULLTHRESHOLD"));
			matrixDTO.setSysLimitedThreshold((String)map.get("LIMITEDTHRESHOLD"));
			matrixDTO.setSysFullThresholdPages((String)map.get("FULLTHRESHOLDPAGES"));
			matrixDTO.setSysLimitedThresholdPages((String)map.get("LIMITEDTHRESHOLDPAGES"));
		}
		return matrixDTO;
	}	

	public UserPreferenceDTO getUserPreferenceDTO() {
      logger.debug("enter getUserPreferenceDTO");		
	  if (userPreferenceDTO == null) {
		logger.debug("userPreferenceDTO == null");
		userPreferenceDTO = new UserPreferenceDTO();
		HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
		UserDTO userDTO = (UserDTO) session.getAttribute("user");
		String userCode = userDTO.getUserCode();
		logger.debug("userCode = " + userCode);	
		logger.debug("call option service SAVEGRIDCHANGES");		
        Option option = optionService.findByPK(new OptionCodePK ("SAVEGRIDCHANGES", "USER", userCode));
		logger.debug("end option service SAVEGRIDCHANGES; VALUE = " + option.getValue());	
		if(option.getValue() == null) {
			  userPreferenceDTO.setGridLayout("");
		}
		else {
			 userPreferenceDTO.setGridLayout(option.getValue());
		}
       
		logger.debug("setGridLayout called");		        
        option = optionService.findByPK(new OptionCodePK ("DEFTRANSIND", "USER", userCode));	
        userPreferenceDTO.setUserTransInd(option.getValue());  
		logger.debug("setUserTransInd called");	        
        option = optionService.findByPK(new OptionCodePK ("DEFTAXCODETYPE", "USER", userCode));	
        userPreferenceDTO.setUserTaxCode(option.getValue());  
		logger.debug("setUserTaxCode called");
		
		option = optionService.findByPK(new OptionCodePK ("DEFTAXTYPECODE", "USER", userCode));	
        userPreferenceDTO.setUserTaxType(option.getValue());  
		logger.debug("setUserTaxCode called");
		
		option = optionService.findByPK(new OptionCodePK ("DEFRATETYPECODE", "USER", userCode));	
        userPreferenceDTO.setUserRateType(option.getValue());  
		logger.debug("setUserTaxCode called");
		
		option = optionService.findByPK(new OptionCodePK ("DEFUSERCOUNTRY", "USER", userCode));	
        userPreferenceDTO.setUserCountry(option.getValue());  
		logger.debug("setUserCountry called");  
		
		option = optionService.findByPK(new OptionCodePK ("USERTIMEZONE", "USER", userCode));	
        userPreferenceDTO.setUserTimeZoneID(option.getValue()); 
        
      //Default entity
  		option = optionService.findByPK(new OptionCodePK ("DEFUSERENTITY", "USER", userCode));	
        userPreferenceDTO.setUserEntity(option.getValue());  
  		logger.debug("setUserEntity called");
        
        option = optionService.findByPK(new OptionCodePK ("DEFUSERENTITYFILTER", "USER", userCode));	 
		if("1".equalsIgnoreCase(option.getValue())) {
			userPreferenceDTO.setUserEntityFilter(true);
		}
		else {
			userPreferenceDTO.setUserEntityFilter(false);				
		}  
		
		option = optionService.findByPK(new OptionCodePK ("DEFUSERCOUNTRYFILTER", "USER", userCode));	 
		if("1".equalsIgnoreCase(option.getValue())) {
			userPreferenceDTO.setUserCountryFilter(true);
		}
		else {
			userPreferenceDTO.setUserCountryFilter(false);				
		} 
		
		//Default country
		option = optionService.findByPK(new OptionCodePK ("DEFUSERSTATE", "USER", userCode));	
        userPreferenceDTO.setUserState(option.getValue());  
		logger.debug("setUserState called");  
		
		option = optionService.findByPK(new OptionCodePK ("DEFUSERSTATEFILTER", "USER", userCode));	 
		if("1".equalsIgnoreCase(option.getValue())) {
			userPreferenceDTO.setUserStateFilter(true);
		}
		else {
			userPreferenceDTO.setUserStateFilter(false);				
		} 
		   
        option = optionService.findByPK(new OptionCodePK ("LASTMODULE", "USER", userCode));	
        userPreferenceDTO.setModule(option.getValue()); 
		
//xxx        option = optionService.findByPK(new OptionCodePK ("THRESHHOLD", "USER", userCode));	
 //       userPreferenceDTO.setUserThreshHold(option.getValue());  
//		logger.debug("setUserThreshHold called");    
		
 //       option = optionService.findByPK(new OptionCodePK ("LIMITEDTHRESHHOLD", "USER", userCode));	
 //       userPreferenceDTO.setLimitedUserThreshHold(option.getValue()); 
//		logger.debug("setLimitedUserThreshHold called");   
		
        option = optionService.findByPK(new OptionCodePK ("USETAXCODELOOKUP", "USER", userCode));
		if (option == null || option.getValue() == null || option.getValue().isEmpty()) {
			userPreferenceDTO.setUseTaxCodeLookup(false);
		} else {
			if (option.getValue().compareTo("0") == 0) {
				userPreferenceDTO.setUseTaxCodeLookup(false);
			} else {
				userPreferenceDTO.setUseTaxCodeLookup(true);
			}
		}
        option = optionService.findByPK(new OptionCodePK ("AOLENABLED", "USER", userCode));	
		if ("1".equalsIgnoreCase(option.getValue())) {
			userPreferenceDTO.setAolEnabled(true);
		} else {
			userPreferenceDTO.setAolEnabled(false);				
		}        
		logger.debug("setAolEnabled called");  
        option = optionService.findByPK(new OptionCodePK ("AOLWHEN", "USER", userCode));	
        userPreferenceDTO.setAolWhen(option.getValue()); 
		logger.debug("setAolWhen called");  
        option = optionService.findByPK(new OptionCodePK ("AOLWHICH", "USER", userCode));	
        userPreferenceDTO.setAolWhich(option.getValue()); 
		logger.debug("setAolWhich called");  
		logger.debug("exit getUserPreferenceDTO");   
	  } else {
	    logger.debug("userPreferenceDTO != null");		  
	  }
	  return userPreferenceDTO; 
	}
	
	public BatchPreferenceDTO getBatchPreferenceDTO() {
		if (batchPreferenceDTO == null) {
			batchPreferenceDTO = new BatchPreferenceDTO();
			Map<String,String> map = new HashMap<String,String>();	
			map.putAll(getSystemPreferenceMap());
			map.putAll(getAdminPreferenceMap());		
			map.putAll(getUserPreferenceMap());	

		    //System - Batch Options
			String batchAutoCheckFlag = (String)map.get("BATCHAUTOCHECKFLAG");
			String batchAutoCheckSec = (String)map.get("BATCHAUTOCHECKSEC");
			String batchDefSchedTime = (String)map.get("BATCHDEFSCHEDTIME");
			String batchUserChgTimeFlag = (String)map.get("BATCHUSERCHGTIMEFLAG");
			String systemStatusRecalcSecs = (String)map.get("SYSTEMSTATUSRECALCSECS");			
		    if ("1".equalsIgnoreCase(batchAutoCheckFlag)) {
				batchPreferenceDTO.setSystemAutoCheck(true); 
		    } else {
				batchPreferenceDTO.setSystemAutoCheck(false);	    	
		    }
			batchPreferenceDTO.setSystemAutoCheckTime(batchAutoCheckSec); 
			
			String delimiter = (String) map.get("EXPORTDELIMITER");
			batchPreferenceDTO.setDelimiter(delimiter);
			
			//Need to change to user time zone
	    	if (batchDefSchedTime != null && batchDefSchedTime.trim().length() > 0) {
	    		if(!batchDefSchedTime.equalsIgnoreCase("*NOW")){
		    		HttpSession session = (HttpSession)FacesContext.getCurrentInstance().getExternalContext().getSession(false);
					TimeZone timeZone = null;
					if (session.getAttribute("timezone") != null) {
						timeZone= (TimeZone)session.getAttribute("timezone");
					}
					
		    		String userTimeZone = timeZone.getID();//getChangedTimeZone();
					Date date = formatTime(batchDefSchedTime);
					if (date != null) {
						long userTimeInMillis = com.ncsts.view.util.DateHelper.getUserTimeInMillis(userTimeZone, date.getTime());
						
						Date d = new Date(userTimeInMillis);
					    DateFormat df = new SimpleDateFormat("hh:mm:ss a");
					    batchDefSchedTime = df.format(d); //Conver to user time zone
					}
					else{
						batchDefSchedTime = "";
					}
	    		}
			} 
			batchPreferenceDTO.setSystemDefTime(batchDefSchedTime);		
			
			
			if ("1".equalsIgnoreCase(batchUserChgTimeFlag)) {
				batchPreferenceDTO.setSystemUserFlag(true);	
			} else {
				batchPreferenceDTO.setSystemUserFlag(false);					
			}
			batchPreferenceDTO.setSystemRecalTime(systemStatusRecalcSecs);
			
			String exportFileExt = (String) map.get("EXPORTFILEEXT");
			batchPreferenceDTO.setExportFileExt(exportFileExt );
			
			String textQualifier = (String) map.get("EXPORTTEXTQUAL");
			batchPreferenceDTO.setTextQualifier(textQualifier );
			
			//Admin - Batch options 
			String adminDefTime =  (String)map.get("ADMINDEFSCHEDTIME");
			
			//Need to change to user time zone
	    	if (adminDefTime != null && adminDefTime.trim().length() > 0) {
	    		if(!adminDefTime.equalsIgnoreCase("*NOW")){
		    		HttpSession session = (HttpSession)FacesContext.getCurrentInstance().getExternalContext().getSession(false);
					TimeZone timeZone = null;
					if (session.getAttribute("timezone") != null) {
						timeZone= (TimeZone)session.getAttribute("timezone");
					}
					
		    		String userTimeZone = timeZone.getID();//getChangedTimeZone();
					Date date = formatTime(adminDefTime);
					if (date != null) {
						long userTimeInMillis = com.ncsts.view.util.DateHelper.getUserTimeInMillis(userTimeZone, date.getTime());
						
						Date d = new Date(userTimeInMillis);
					    DateFormat df = new SimpleDateFormat("hh:mm:ss a");
					    adminDefTime = df.format(d); //Convert to user time zone
					}
					else{
						adminDefTime = "";
					}
	    		}
			} 
			batchPreferenceDTO.setAdminDefTime(adminDefTime);	
			
			String adminIntervalTime =  (String)map.get("BATCHPROCESSINTERVALSECS");	
			batchPreferenceDTO.setAdminIntervalTime(adminIntervalTime);
			
			String adminRecalTime =  (String)map.get("ADMINSTATUSRECALCSECS");	
			batchPreferenceDTO.setAdminRecalTime(adminRecalTime);
			String adminUserFlag = (String)map.get("ADMINUSERCHGTIMEFLAG");
		    if ("1".equalsIgnoreCase(adminUserFlag)) {
				batchPreferenceDTO.setAdminUserFlag(true);
		    } else {
				batchPreferenceDTO.setAdminUserFlag(false);	  
		    }			
			String adminRecalUserFlag = (String)map.get("ADMINUSERCHGRECALCSECSFLAG");
		    if ("1".equalsIgnoreCase(adminRecalUserFlag)) {
				batchPreferenceDTO.setAdminRecalUserFlag(true); 
				batchPreferenceDTO.setShowRecalc(true);	
				logger.debug("showRecalc true = " + batchPreferenceDTO.isShowRecalc());				
		    } else {
				batchPreferenceDTO.setAdminRecalUserFlag(false);	
				batchPreferenceDTO.setShowRecalc(false);	
				logger.debug("showRecalc false = " + batchPreferenceDTO.isShowRecalc());					
		    }			

			//User - Batch options
			String userCompletionPopUp = (String)map.get("DISPLAYPOPUPS");
		    if ("1".equalsIgnoreCase(userCompletionPopUp)) {
				batchPreferenceDTO.setUserCompletionPopUp(true); 
		    } else {
				batchPreferenceDTO.setUserCompletionPopUp(false);	    	
		    }	
		    
		    HttpSession session = (HttpSession)FacesContext.getCurrentInstance().getExternalContext().getSession(false);
			if (session.getAttribute("timezone") != null) {
				TimeZone timeZone= (TimeZone)session.getAttribute("timezone");
				batchPreferenceDTO.setTimeZoneName(timeZone.getID());
			}
			
		}
		return batchPreferenceDTO;
	}

	public ImportMapProcessPreferenceDTO getImportMapProcessPreferenceDTO() {
		if (importMapProcessPreferenceDTO == null) {
			importMapProcessPreferenceDTO = new ImportMapProcessPreferenceDTO();
			Map<String,String> map = new HashMap<String,String>();	
			map.putAll(getSystemPreferenceMap());
			map.putAll(getAdminPreferenceMap());		
			map.putAll(getUserPreferenceMap());	
			
			//System - Import/Map/Process
			String fileNameDuplicateRestriction =  (String)map.get("FILENAMEDUPRES");				
			importMapProcessPreferenceDTO.setFileNameDuplicateRestriction(fileNameDuplicateRestriction);
			String fileSizeDuplicateRestriction =  (String)map.get("FILESIZEDUPRES");				
			importMapProcessPreferenceDTO.setFileSizeDuplicateRestriction(fileSizeDuplicateRestriction);
			String terminalErrorsBeforeAbortImport =  (String)map.get("ERRORROWS");				
			importMapProcessPreferenceDTO.setTerminalErrorsBeforeAbortImport(terminalErrorsBeforeAbortImport);		
			String defaultImportDateMap =  (String)map.get("IMPORTDATEMAP");				
			importMapProcessPreferenceDTO.setDefaultImportDateMap(defaultImportDateMap);
			String rowsToImportMapBeforeSave =  (String)map.get("MAPROWS");	
			importMapProcessPreferenceDTO.setRowsToImportMapBeforeSave(rowsToImportMapBeforeSave);
			String endOfFileMarker =  (String)map.get("BATCHEOFMARKER");				
			importMapProcessPreferenceDTO.setEndOfFileMarker(endOfFileMarker);	
			String batchCountMarker =  (String)map.get("BATCHCOUNTMARKER");				
			importMapProcessPreferenceDTO.setBatchCountMarker(batchCountMarker);
			String batchTotalMarker =  (String)map.get("BATCHTOTALMARKER");				
			importMapProcessPreferenceDTO.setBatchTotalMarker(batchTotalMarker);
			
			String emailProcessStatusFlag =  (String)map.get("EMAILPROC");
			if ("1".equalsIgnoreCase(emailProcessStatusFlag)) {
				importMapProcessPreferenceDTO.setEmailProcessStatusFlag(true);	
			} else {
				importMapProcessPreferenceDTO.setEmailProcessStatusFlag(false);					
			}	
			String deleteTransactionWithZeroAmountFlag =  (String)map.get("DELETE0AMTFLAG");
			if ("1".equalsIgnoreCase(deleteTransactionWithZeroAmountFlag)) {
				importMapProcessPreferenceDTO.setDeleteTransactionWithZeroAmountFlag(true);	
			} else {
				importMapProcessPreferenceDTO.setDeleteTransactionWithZeroAmountFlag(false);					
			}		
			String holdTransactionAmountFlag =  (String)map.get("HOLDTRANSFLAG");
			if ("1".equalsIgnoreCase(holdTransactionAmountFlag)) {
				importMapProcessPreferenceDTO.setHoldTransactionAmountFlag(true);
			} else {
				importMapProcessPreferenceDTO.setHoldTransactionAmountFlag(false);				
			}			
			String processTransactionAmountFlag =  (String)map.get("AUTOPROCFLAG");	
			if ("1".equalsIgnoreCase(processTransactionAmountFlag)) {
				importMapProcessPreferenceDTO.setProcessTransactionAmountFlag(true);
			} else {
				importMapProcessPreferenceDTO.setProcessTransactionAmountFlag(false);				
			}			
			String killProcessingProcedureCountFlag =  (String)map.get("KILLPROCFLAG");	
			if ("1".equalsIgnoreCase(killProcessingProcedureCountFlag)) {
				importMapProcessPreferenceDTO.setKillProcessingProcedureCountFlag(true);
			} else {
				importMapProcessPreferenceDTO.setKillProcessingProcedureCountFlag(false);				
			}
			
			String customTaxCodeOverrideTaxionaryFlag =  (String)map.get("CUSTOMTCOR");
			if ("1".equalsIgnoreCase(customTaxCodeOverrideTaxionaryFlag)) {
				importMapProcessPreferenceDTO.setCustomTaxCodeOverrideTaxionaryFlag(true);
			} else {
				importMapProcessPreferenceDTO.setCustomTaxCodeOverrideTaxionaryFlag(false);				
			}
					
			String lookupVendorNexusFlag =  (String)map.get("VENDORNEXUSFLAG");	
			if ("1".equalsIgnoreCase(lookupVendorNexusFlag)) {
				importMapProcessPreferenceDTO.setLookupVendorNexusFlag(true);
			} else {
				importMapProcessPreferenceDTO.setLookupVendorNexusFlag(false);				
			}		
			
			String holdTransactionAmount =  (String)map.get("HOLDTRANSAMT");				
			importMapProcessPreferenceDTO.setHoldTransactionAmount(holdTransactionAmount);		
			String processTransactionAmount =  (String)map.get("AUTOPROCAMT");				
			importMapProcessPreferenceDTO.setProcessTransactionAmount(processTransactionAmount);
			String applyTaxCodeAmount =  (String)map.get("AUTOPROCID");				
			importMapProcessPreferenceDTO.setApplyTaxCodeAmount(applyTaxCodeAmount);
			String killProcessingProcedureCount =  (String)map.get("KILLPROCCOUNT");				
			importMapProcessPreferenceDTO.setKillProcessingProcedureCount(killProcessingProcedureCount);
			String doNotImportMarkers = (String)map.get("DONOTIMPORT");
			importMapProcessPreferenceDTO.setDoNotImportMarkers(doNotImportMarkers);
			String replaceWithMarkers = (String)map.get("REPLACEWITH");
			importMapProcessPreferenceDTO.setReplaceWithMarkers(replaceWithMarkers);
			
			//Admin - Import/Map/Process			
			String allocationsEnabled =  (String)map.get("ALLOCATIONSENABLED");	
			if ("1".equalsIgnoreCase(allocationsEnabled)) {
				importMapProcessPreferenceDTO.setAllocationsEnabled(true);					
			} else {
				importMapProcessPreferenceDTO.setAllocationsEnabled(false);					
			}
			String processrulesEnabled =  (String)map.get("PROCRULEENABLED");	
			if ("1".equalsIgnoreCase(processrulesEnabled)) {
				importMapProcessPreferenceDTO.setProcessrulesEnabled(true);					
			} else {
				importMapProcessPreferenceDTO.setProcessrulesEnabled(false);					
			}
			String taxCodeAllocationsEnabled =  (String)map.get("TAXALLOCENABLED");	
			if ("1".equalsIgnoreCase(taxCodeAllocationsEnabled)) {
				importMapProcessPreferenceDTO.setTaxCodeAllocationsEnabled(true);					
			} else {
				importMapProcessPreferenceDTO.setTaxCodeAllocationsEnabled(false);					
			}
						
			String saleApiDatabase =  (String)map.get("SALEAPIDATABASE");	
			importMapProcessPreferenceDTO.setSaleApiDatabase(saleApiDatabase);
			String saleApiServer =  (String)map.get("SALEAPISERVER");	
			importMapProcessPreferenceDTO.setSaleApiServer(saleApiServer);
			String saleApiUserId =  (String)map.get("SALEAPIUSERID");	
			importMapProcessPreferenceDTO.setSaleApiUserId(saleApiUserId);
			String saleApiPassword =  (String)map.get("SALEAPIPASSWORD");	
			importMapProcessPreferenceDTO.setSaleApiPassword(saleApiPassword);
						//System - GL Extract
		    String glExtractFilePath = (String)map.get("GLEXTRACTFILEPATH");
		    importMapProcessPreferenceDTO.setGlExtractFilePath(glExtractFilePath);
		    String glExtractFilePrefix = (String)map.get("GLEXTRACTPREFIX");
		    importMapProcessPreferenceDTO.setGlExtractFilePrefix(glExtractFilePrefix);
		    String glExtractFileExtension = (String)map.get("GLEXTRACTEXT"); 
		    importMapProcessPreferenceDTO.setGlExtractFileExtension(glExtractFileExtension);
		    
			//Admin - GL Extract		    
		    String glExtractEnabled = (String)map.get("GLEXTRACTENABLED");
		    if ("1".equalsIgnoreCase(glExtractEnabled)) {
		    	importMapProcessPreferenceDTO.setGlExtractEnabled(true);
		    } else {
		    	importMapProcessPreferenceDTO.setGlExtractEnabled(false);		    	
		    }
		    String glExtractWindowName = (String)map.get("GLEXTRACTWIN");
		    importMapProcessPreferenceDTO.setGlExtractWindowName(glExtractWindowName);
		    String glExtractSetMark = (String)map.get("GLEXTRACTMARK");	
		    if ("1".equalsIgnoreCase(glExtractSetMark)) {
		    	importMapProcessPreferenceDTO.setGlExtractSetMark(true);
		    } else {
		    	importMapProcessPreferenceDTO.setGlExtractSetMark(false);	    	
		    }	
		    
		    //System - Security
		    String pwdExpirationWarningDays = (String)map.get("PWEXPDAYS"); 
		    importMapProcessPreferenceDTO.setPwdExpirationWarningDays(pwdExpirationWarningDays);
		    String chgPassword = (String)map.get("PWUSERCHG");
		    if ("1".equalsIgnoreCase(chgPassword)) {
			    importMapProcessPreferenceDTO.setChgPassword(true);		    	
		    } else {
			    importMapProcessPreferenceDTO.setChgPassword(false);			    	
		    }		    
		    String denyPasswordChangeMessage = (String)map.get("PWDENYMSG");
		    importMapProcessPreferenceDTO.setDenyPasswordChangeMessage(denyPasswordChangeMessage);
		    
		    //Admin - Security
		    String activateExtendedSecurity = (String)map.get("EXTENDSEC");
		    if ("1".equalsIgnoreCase(activateExtendedSecurity)) {
			    importMapProcessPreferenceDTO.setActivateExtendedSecurity(true);		    	
		    } else {
			    importMapProcessPreferenceDTO.setActivateExtendedSecurity(false);		    	
		    }
		    String activateDebugExtendedSecurity = (String)map.get("DEBUGSEC");	    
		    if ("1".equalsIgnoreCase(activateDebugExtendedSecurity)) {
			    importMapProcessPreferenceDTO.setActivateDebugExtendedSecurity(true);		    	
		    } else {
			    importMapProcessPreferenceDTO.setActivateDebugExtendedSecurity(false);			    	
		    }	
		    
		    //System - Directories
		    String sqlFilePath = (String)map.get("SQLFILEPATH");
		    importMapProcessPreferenceDTO.setSqlFilePath(sqlFilePath);
		    String sqlFileExt = (String)map.get("SQLFILEEXT");
		    importMapProcessPreferenceDTO.setSqlFileExt(sqlFileExt);
		    String impExpFilePath = (String)map.get("IMPEXPFILEPATH");
		    importMapProcessPreferenceDTO.setImpExpFilePath(impExpFilePath);
		    String impExpFileExt = (String)map.get("IMPEXPFILEEXT");
		    importMapProcessPreferenceDTO.setImpExpFileExt(impExpFileExt);
		    String refDocFilePath = (String)map.get("REFDOCFILEPATH");
		    importMapProcessPreferenceDTO.setRefDocFilePath(refDocFilePath);
		    String refDocFileExt = (String)map.get("REFDOCFILEEXT");
		    importMapProcessPreferenceDTO.setRefDocFileExt(refDocFileExt);
		    String defaultDirCrystal = (String)map.get("DEFAULTDIRCRYSTAL");
		    importMapProcessPreferenceDTO.setDefaultDirCrystal(defaultDirCrystal);

            //User - Directories
		    String importFilePath = (String)map.get("IMPORTFILEPATH");
		    importMapProcessPreferenceDTO.setImportFilePath(importFilePath);
		    String importFileExt = (String)map.get("IMPORTFILEEXT");
		    importMapProcessPreferenceDTO.setImportFileExt(importFileExt);
		    String userDirCrystal  = (String)map.get("USERDIRCRYSTAL");	
		    importMapProcessPreferenceDTO.setUserDirCrystal(userDirCrystal);
		    
		    // System - Tax Updates
		    String lastTaxRateDateFull = (String)map.get("LASTTAXRATEDATEFULL");
		    importMapProcessPreferenceDTO.setLastTaxRateDateFull(lastTaxRateDateFull);
		    String lastTaxRateRelFull = (String)map.get("LASTTAXRATERELFULL");
		    importMapProcessPreferenceDTO.setLastTaxRateRelFull(lastTaxRateRelFull);
		    String lastTaxRateDateUpdate = (String)map.get("LASTTAXRATEDATEUPDATE");
		    importMapProcessPreferenceDTO.setLastTaxRateDateUpdate(lastTaxRateDateUpdate);
		    String lastTaxRateRelUpdate = (String)map.get("LASTTAXRATERELUPDATE");
		    importMapProcessPreferenceDTO.setLastTaxRateRelUpdate(lastTaxRateRelUpdate);
		    String taxWarnRate = (String)map.get("TAXWARNRATE");
		    importMapProcessPreferenceDTO.setTaxWarnRate(taxWarnRate);
		    String lastRuleDateUpdate = (String)map.get("LASTRULEDATEUPDATE");
		    importMapProcessPreferenceDTO.setLastRuleDateUpdate(lastRuleDateUpdate);
		    String lastRuleRelUpdate = (String)map.get("LASTRULERELUPDATE");
		    importMapProcessPreferenceDTO.setLastRuleRelUpdate(lastRuleRelUpdate);
		    String lastSitusDateUpdate = (String)map.get("LASTSITUSDATEUPDATE");
		    importMapProcessPreferenceDTO.setLastSitusDateUpdate(lastSitusDateUpdate);
		    String lastSitusRelUpdate = (String)map.get("LASTSITUSRELUPDATE");
		    importMapProcessPreferenceDTO.setLastSitusRelUpdate(lastSitusRelUpdate);
		   
		    
		    // System - TaxCode Rules
		    String lastTaxCodeRulesDateFull = (String)map.get("LASTRULEDATEFULL");
		    importMapProcessPreferenceDTO.setLastTaxCodeRulesDateFull(lastTaxCodeRulesDateFull);
		    String lastTaxCodeRulesRelFull = (String)map.get("LASTRULERELFULL");
		    importMapProcessPreferenceDTO.setLastTaxCodeRulesRelFull(lastTaxCodeRulesRelFull);
		    String lastTaxCodeRulesDateUpdate = (String)map.get("LASTRULEDATEUPDATE");
		    importMapProcessPreferenceDTO.setLastTaxCodeRulesDateUpdate(lastTaxCodeRulesDateUpdate);
		    String lastTaxCodeRulesRelUpdate = (String)map.get("LASTRULERELUPDATE");
		    importMapProcessPreferenceDTO.setLastTaxCodeRulesRelUpdate(lastTaxCodeRulesRelUpdate);
		    
		    // System - Compliance
		    String defCCHTaxCatCode = (String)map.get("DEFCCHTAXCATCODE");
		    importMapProcessPreferenceDTO.setDefCCHTaxCatCode(defCCHTaxCatCode);
		    String defCCHGroupCode = (String)map.get("DEFCCHGROUPCODE");
		    importMapProcessPreferenceDTO.setDefCCHGroupCode(defCCHGroupCode);
		    String tpExtractFilePath = (String)map.get("TPEXTRACTEFILEPATH");
		    importMapProcessPreferenceDTO.setTpExtractFilePath(tpExtractFilePath);            
		    String tpExtractFilePrefix  = (String)map.get("TPEXTRACTPREFIX");
		    importMapProcessPreferenceDTO.setTpExtractFilePrefix(tpExtractFilePrefix);
		    String tpExtractExt  = (String)map.get("TPEXTRACTEXT");
		    importMapProcessPreferenceDTO.setTpExtractExt(tpExtractExt);

		    // Admin - Compliance
		    String tpExtractEnabled = (String)map.get("TPEXTRACTENABLED");	
		    if ("1".equalsIgnoreCase(tpExtractEnabled)) {
		    	importMapProcessPreferenceDTO.setTpExtractEnabled(true);
		    } else {
		    	importMapProcessPreferenceDTO.setTpExtractEnabled(false);
		    }
		    
		    // System - RatePoint
			String ratepointLatlongLookup = (String) map.get("LATLONGLOOKUP");
			if ("1".equalsIgnoreCase(ratepointLatlongLookup)) {
				importMapProcessPreferenceDTO.setRatepointLatlongLookup(true);
			} else {
				importMapProcessPreferenceDTO.setRatepointLatlongLookup(false);
			}
			
			String runPostProcessrulesForheldTransFlag =  (String)map.get("HELDPROCRULE");
			if ("1".equalsIgnoreCase(runPostProcessrulesForheldTransFlag)) {
				importMapProcessPreferenceDTO.setRunPostProcessrulesForheldTransFlag(true);
			} else {
				importMapProcessPreferenceDTO.setDeleteTransactionWithZeroAmountFlag(false);					
			}	
	
			String ratepointDefaultJurisdiction = (String) map.get("DEFAULTJURISDICTION");
			importMapProcessPreferenceDTO.setRatepointDefaultJurisdiction(ratepointDefaultJurisdiction);
	
			String ratepointDefaultTaxCode = (String) map.get("DEFAULTTAXCODE");
			importMapProcessPreferenceDTO.setRatepointDefaultTaxCode(ratepointDefaultTaxCode);
			String genericJurApi = (String) map.get("GENERICJURAPI");
			importMapProcessPreferenceDTO.setGenericJurApi(genericJurApi);
			
			String allocjuval = (String)map.get("ALLOCJUR");
		    importMapProcessPreferenceDTO.setPostJurisdictiontoSalesAddress(allocjuval);
		    
		    String pcoadmin = (String)map.get("PCO");	
		    if ("1".equalsIgnoreCase(pcoadmin)) {
		    	importMapProcessPreferenceDTO.setPostJurisdtoSalesAddressEnabled(true);
		    	
		    } else {
		    	importMapProcessPreferenceDTO.setPostJurisdtoSalesAddressEnabled(false);
		    }
			
			// Admin - AID Service //0007645
			String aidHostnames = (String) map.get("AIDHOSTNAME");		
			this.aidHostname = "";
			this.aidHostname2 = "";
	    	if(aidHostnames!=null && aidHostnames.length()>0){
		    	String[] split = aidHostnames.split(";");	  
		    	if(split.length>0){aidHostname = split[0];}
		    	if(split.length>1){aidHostname2 = split[1];}
	    	}
			
			String aidPorts = (String) map.get("AIDPORT");
			this.aidPort = "";
			this.aidPort2 = "";
	    	if(aidPorts!=null && aidPorts.length()>0){
		    	String[] split = aidPorts.split(";");	  
		    	if(split.length>0){aidPort = split[0];}
		    	if(split.length>1){aidPort2 = split[1];}
	    	}
				    	
			this.aidInstallation = (String) map.get("AIDSETTINGPATH");
			this.loginBean.resetAidMenuEnabled();
			
			this.daysToKeep = (String) map.get("AIDPURGEDAYS");
			
			this.emailHost = (String) map.get("EMAILHOST");
			this.emailPort = (String) map.get("EMAILPORT");
			this.emailFrom = (String) map.get("EMAILFROM");

			String emailStart = (String) map.get("EMAILSTART");
			if ("1".equalsIgnoreCase(emailStart)) {
				this.setEmailStart(true);
			} else {
				this.setEmailStart(false);
			}
			
			this.emailStop = (String) map.get("EMAILSTOP");
			this.emailTo = (String) map.get("EMAILTO");
			this.emailCc = (String) map.get("EMAILCC");
			
			String addAllow = (String) map.get("AIDUSERADDFILE");
			if ("1".equalsIgnoreCase(addAllow)) {
				this.setAllowAidAddFile(true);
			} else {
				this.setAllowAidAddFile(false);
			}
			
			String reimportAllow = (String) map.get("AIDUSERREIMPORTFILE");
			if ("1".equalsIgnoreCase(reimportAllow)) {
				this.setAllowAidReimportFile(true);
			} else {
				this.setAllowAidReimportFile(false);
			}
			
			String deleteAllow = (String) map.get("AIDUSERDELETEFILE");
			if ("1".equalsIgnoreCase(deleteAllow)) {
				this.setAllowAidDeleteFile(true);
			} else {
				this.setAllowAidDeleteFile(false);
			}
		}		
		return importMapProcessPreferenceDTO;
	}

	public void setImportMapProcessPreferenceDTO(
			ImportMapProcessPreferenceDTO importMapProcessPreferenceDTO) {
		this.importMapProcessPreferenceDTO = importMapProcessPreferenceDTO;
	}
	
	public DoNotImportCharBean getDoNotImportCharBean(){
		return doNotImportCharBean;
	}
	
	public void setUserPreferenceDTO(
			UserPreferenceDTO userPreferenceDTO) {
		this.userPreferenceDTO = userPreferenceDTO;
	}	

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	
	public boolean  getAllowAidAddFile() {
		return allowAidAddFile;
	}

	public void setAllowAidAddFile(boolean  allowAidAddFile) {
		this.allowAidAddFile = allowAidAddFile;
	}
	
	public boolean  getAllowAidReimportFile() {
		return allowAidReimportFile;
	}

	public void setAllowAidReimportFile(boolean  allowAidReimportFile) {
		this.allowAidReimportFile = allowAidReimportFile;
	}
	
	public boolean  getAllowAidDeleteFile() {
		return allowAidDeleteFile;
	}

	public void setAllowAidDeleteFile(boolean  allowAidDeleteFile) {
		this.allowAidDeleteFile = allowAidDeleteFile;
	}

	public boolean isSystemBatchesFieldsProtected() {
		logger.debug("isSystemBatchesFieldsProtected():systemBatchesFieldsProtected = " + systemBatchesFieldsProtected);
		return systemBatchesFieldsProtected;
	}

	public void setSystemBatchesFieldsProtected(boolean systemBatchesFieldsProtected) {
		logger.debug("setSystemBatchesFieldsProtected():systemBatchesFieldsProtected = " + systemBatchesFieldsProtected);		
		this.systemBatchesFieldsProtected = systemBatchesFieldsProtected;
	}

	public boolean isSystemBatchesRecalcFieldsProtected() {
		return systemBatchesRecalcFieldsProtected;
	}

	public void setSystemBatchesRecalcFieldsProtected(
			boolean systemBatchesRecalcFieldsProtected) {
		this.systemBatchesRecalcFieldsProtected = systemBatchesRecalcFieldsProtected;
	}

	public CacheManager getCacheManager() {
		return cacheManager;
	}

	public void setCacheManager(CacheManager cacheManager) {
		this.cacheManager = cacheManager;
		
		jurisdictionHandler.setCacheManager(cacheManager);
		filterTaxCodeHandlerDefault.setCacheManager(cacheManager);
		
		filterTaxCodeHandlerDefault.setTaxCodeBackingBean(taxCodeBackingBean);
		filterTaxCodeHandlerDefault.setReturnRuleUrl("filepref_system_ratepoint");
		
		filterTaxCodeHandler.setCacheManager(cacheManager);
		filterTaxCodeHandler.setTaxCodeBackingBean(taxCodeBackingBean);
		filterTaxCodeHandler.setReturnRuleUrl("filepref_system_import");
	}
	
	public JurisdictionService getJurisdictionService() {
		return jurisdictionService;
	}
	
	public void setJurisdictionService(JurisdictionService jurisdictionService) {
		this.jurisdictionService = jurisdictionService;
		
		jurisdictionHandler.setJurisdictionService(jurisdictionService);
	}
	
	public void searchTaxCodeCodeCallback(TaxCodeCodeHandler taxCodeCodeHandler) {
		if(taxCodeCodeHandler == filterTaxCodeHandlerDefault){
			//selectedTaxcodeCode = filterTaxCodeHandler.getTaxcodeCode();
			
			//filterTaxCodeHandler1.setTaxcodeCode(selectedTaxcodeCode);
			//filterTaxCodeHandler2.setTaxcodeCode(selectedTaxcodeCode);
			updateButtonAction(null);	
		}
		else if(taxCodeCodeHandler == filterTaxCodeHandler){
			updateButtonAction(null);	
		}
	}

	public void setCchTaxCatItems(List<SelectItem> cchTaxCatItems) {
		this.cchTaxCatItems = cchTaxCatItems;
	}	
	
	public List<SelectItem> getCchTaxCatItems() {
		if (cchTaxCatItems == null) {
			cchTaxCatItems = new ArrayList<SelectItem>();
			cchTaxCatItems.add(new SelectItem("","CCH Tax Category"));
			for (CCHCode code : cacheManager.getCchCodeMap().values()) {
				cchTaxCatItems.add(new SelectItem(code.getCodePK().getCode(), 
						code.getCodePK().getCode() + " - " + code.getDescription()));
			}
		}
		  
		return cchTaxCatItems;
	}
	
	public void setCchTaxGrpItems(List<SelectItem> cchTaxGrpItems) {
		this.cchTaxGrpItems = cchTaxGrpItems;
	}	

	public List<SelectItem> getCchTaxGrpItems() {
		if (cchTaxGrpItems == null) {
			cchTaxGrpItems = new ArrayList<SelectItem>();
			cchTaxGrpItems.add(new SelectItem("","CCH Tax Group"));
			for (CCHTaxMatrix code : cacheManager.getCchGroupMap().values()) {
				cchTaxGrpItems.add(new SelectItem(code.getCchTaxMatrixPK().getGroupCode(), 
						code.getCchTaxMatrixPK().getGroupCode() + " - " + code.getGroupDesc()));
			}
		}
		
		return cchTaxGrpItems;
	}

	public UploadedFile getFileSrc() {
		return fileSrc;
	}

	public void setFileSrc(UploadedFile fileSrc) {
		this.fileSrc = fileSrc;
	}	

	public boolean isDisplayButton() {
		return displayButton;
	}

	public void setDisplayButton(boolean displayButton) {
		this.displayButton = displayButton;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	
	public String getNavigationString() {
		return navigationString;
	}

	public void setNavigationString(String navigationString) {
		this.navigationString = navigationString;
	}
	
	public void vclTransactionAmount(ValueChangeEvent event){
		updateButtonAction(event);
		if(event.getNewValue().equals(true)){
			holdTransactionAmount.setDisabled(false);
		}else{
			holdTransactionAmount.setDisabled(true);
			//holdTransactionAmount.setValue("");
		}
	}
	
	public void vclApplyTaxCode(ValueChangeEvent event){
		updateButtonAction(event);
		if(event.getNewValue().equals(true)){
		filterTaxCodeHandler.getTaxcodeDetailId().setDisabled(false);
		processTransactionAmountInput.setDisabled(false);
		setDisplaySearch(true);
				
		
		}else{
			filterTaxCodeHandler.getTaxcodeDetailId().setDisabled(true);
			processTransactionAmountInput.setDisabled(true);
			setDisplaySearch(false);
			//processTransactionAmountInput.setValue("");
		}
		
	}
	
	public void vclProcessingProcedure(ValueChangeEvent event){
		updateButtonAction(event);
		if(event.getNewValue().equals(true)){
			killProcessingProcedureInput.setDisabled(false);
		}else {
			killProcessingProcedureInput.setDisabled(true);
			//killProcessingProcedureInput.setValue("");
		}
	}
		public HtmlInputText getProcessTransactionAmountInput() {
			return processTransactionAmountInput;
		}

		public void setProcessTransactionAmountInput(
				HtmlInputText processTransactionAmountInput) {
			this.processTransactionAmountInput = processTransactionAmountInput;
		}

		public boolean isDisplaySearch() {
			return displaySearch;
		}

		public void setDisplaySearch(boolean displaySearch) {
			this.displaySearch = displaySearch;
		}

		public UIAjaxCommandButton getSearchButton() {
			return searchButton;
		}

		public void setSearchButton(UIAjaxCommandButton searchButton) {
			this.searchButton = searchButton;
		}

		public HtmlInputText getHoldTransactionAmount() {
			return holdTransactionAmount;
		}

		public void setHoldTransactionAmount(HtmlInputText holdTransactionAmount) {
			this.holdTransactionAmount = holdTransactionAmount;
		}

		public HtmlInputText getKillProcessingProcedureInput() {
			return killProcessingProcedureInput;
		}

		public void setKillProcessingProcedureInput(
				HtmlInputText killProcessingProcedureInput) {
			this.killProcessingProcedureInput = killProcessingProcedureInput;
		}

		public String getLookUpStatus() {
			return lookUpStatus;
		}

		public void setLookUpStatus(String lookUpStatus) {
			this.lookUpStatus = lookUpStatus;
		}
			
			public void defaultDirectoryProcessListener(ActionEvent e){
				logger.info("enter defaultDirectoryProcessListener");
				//batchIdHandler.setDefaultSearch("IP");
				batchIdHandler.reset();
				logger.info("exit defaultDirectoryProcessListener"); 				
			}
			
			public void applyAction() throws Exception {
				if(!"".equalsIgnoreCase(importMapProcessPreferenceDTO.getRefDocFilePath()) &&
						importMapProcessPreferenceDTO.getRefDocFilePath()!=null){
				logger.info("Enter apply action");
				String path =null;
				List<ReferenceDocument> refDocList = referenceDocumentService.findAll();
				if(importMapProcessPreferenceDTO.getRefDocFilePath().endsWith("\\")){
					path = importMapProcessPreferenceDTO.getRefDocFilePath().
					substring(0, importMapProcessPreferenceDTO.getRefDocFilePath( ).lastIndexOf("\\"));
				}else {
					path = importMapProcessPreferenceDTO.getRefDocFilePath(); 
				}
				for(ReferenceDocument referenceDocument : refDocList){
					int count =0;
					String documentNameFromUrl = null;
					String isValidDoc = null;
					String pathOnly = null;
					if(!"".equals(referenceDocument.getUrl()) && referenceDocument.getUrl()!=null ){
						for(int i = 0; i < referenceDocument.getUrl().length(); i++) {
						     if(referenceDocument.getUrl().charAt(i) == '.') {
						          count++;
						     }
						}
						
					documentNameFromUrl = referenceDocument.getUrl().substring(referenceDocument.getUrl().lastIndexOf("\\")+1);
					isValidDoc = documentNameFromUrl.substring(documentNameFromUrl.lastIndexOf(".")+1);
					if(documentNameFromUrl.contains(".") && count==1 && isValidDoc.length()==3 || isValidDoc.length() ==4){
					pathOnly = referenceDocument.getUrl().substring(0, referenceDocument.getUrl().lastIndexOf("\\"));
					}else{
						pathOnly = referenceDocument.getUrl();
					}
						}
					// new url with *STATE
					if(path.contains("\\*STATE")){
						String tempState = null;
						// check url exists in current row
						if(!"".equals(referenceDocument.getUrl()) && referenceDocument.getUrl()!=null 
								&& referenceDocument.getUrl().contains("www") || count >=2){
							logger.info("Contains WWW or .......");
							referenceDocument.setUrl(referenceDocument.getUrl());
						}else{
						String tempPath = path.substring(0, path.lastIndexOf("\\*STATE"));
						List<TaxCodeStateDTO> listStateName = new ArrayList<TaxCodeStateDTO>();
						List<TaxCodeStateDTO> listStateCode = new ArrayList<TaxCodeStateDTO>();
						if(!"".equals(referenceDocument.getUrl()) && referenceDocument.getUrl()!=null){
							if(documentNameFromUrl.contains(".") && count==1 && isValidDoc.length()==3 || isValidDoc.length() ==4){
								tempState = pathOnly.substring(pathOnly.lastIndexOf("\\")+1);
								logger.info("IIIIIIIIIIIIIIIII "+tempState);
								
							}else {
								tempState = referenceDocument.getUrl().substring(referenceDocument.getUrl().lastIndexOf("\\")+1);
								logger.info("EEEEEEEEEEEEEEEEE "+tempState);
							}
					
						
						if(tempState.length() >2){
							listStateName = taxCodeStateService.findByStateName(tempState);
						}
						 if(tempState.length()==2 && !tempState.endsWith(":")){
							 tempState.toUpperCase();
							 listStateCode = taxCodeStateService.findByStateCode(tempState);
						 }
							if(!listStateCode.isEmpty() || !listStateName.isEmpty()){
								
								logger.info("############# STATE CODE OR STATE NAME ###################### "+tempState);
									if(documentNameFromUrl.contains(".") && count==1 && isValidDoc.length()==3 || isValidDoc.length() ==4){
											logger.info("|||||||||||||Doc name  = null and refdoc url has a file "+documentNameFromUrl);
										
										if("".equals(referenceDocument.getDocName()) || referenceDocument.getDocName()==null){
											logger.info("|||||||||||||| its a valid doc  "+documentNameFromUrl);
											referenceDocument.setDocName(documentNameFromUrl);
											referenceDocument.setUrl(tempPath.concat("\\"+tempState));
										}else {
											//referenceDocument.setDocName(documentNameFromUrl);
										referenceDocument.setUrl(tempPath);
										}
									}else {
										if("".equals(referenceDocument.getDocName()) || referenceDocument.getDocName()==null) {
											referenceDocument.setUrl(referenceDocument.getUrl());
										}else {
										logger.info("|||||||||||||| Inside Else ");
										//referenceDocument.setDocName("");
										referenceDocument.setUrl(tempPath.concat("\\"+tempState));
										}
									}
								}
							else{
								logger.info("||||||||||||| NO STATE CODE OR STATE NAME ||||||||||||||||");
								isValidDoc = documentNameFromUrl.substring(documentNameFromUrl.lastIndexOf(".")+1);
								if(documentNameFromUrl.contains(".")){
									pathOnly = referenceDocument.getUrl().substring(0, referenceDocument.getUrl().lastIndexOf("\\"));
								}else {
									pathOnly = referenceDocument.getUrl();
								}
										
									if(documentNameFromUrl.contains(".") && count==1 && isValidDoc.length()==3 || isValidDoc.length() ==4){
										if( "".equals(referenceDocument.getDocName()) || referenceDocument.getDocName()==null ){
											logger.info("No state code or statename "+documentNameFromUrl);
											referenceDocument.setUrl(referenceDocument.getUrl().substring(0, referenceDocument.getUrl().lastIndexOf("\\")));
											referenceDocument.setDocName(documentNameFromUrl);
										}else {
											logger.info("|||||||||||||");
											referenceDocument.setUrl(tempPath);
											//referenceDocument.setDocName(documentNameFromUrl);
										}
									}else{
										logger.info("|||||||||||||");
										referenceDocument.setUrl(referenceDocument.getUrl());
									}
								}
							}
						logger.info("line 2209.  Before update(referenceDocument)");
						referenceDocumentService.update(referenceDocument);
						logger.info("line 2211.  After update(referenceDocument)");						
					}
						}
					else{
							if(!"".equals(referenceDocument.getUrl()) && referenceDocument.getUrl()!=null 
									&& referenceDocument.getUrl().contains("www")|| count >=2){
								logger.info("||||||||||||||| Is a Web URL  "+referenceDocument.getUrl());
								referenceDocument.setUrl(referenceDocument.getUrl());
								referenceDocument.setDocName("");
							}else {
								isValidDoc = documentNameFromUrl.substring(documentNameFromUrl.lastIndexOf(".")+1);
						if(documentNameFromUrl.contains(".") && count==1 && isValidDoc.length()==3 || isValidDoc.length() ==4){
							logger.info("|||||||||||||Doc name  = null and refdoc url has a file "+documentNameFromUrl);
							
							if("".equals(referenceDocument.getDocName()) || referenceDocument.getDocName()==null){
								logger.info("|||||||||||||| its a valid doc  "+documentNameFromUrl);
								referenceDocument.setDocName(documentNameFromUrl);
								referenceDocument.setUrl(path);
							}else {
								//referenceDocument.setDocName(documentNameFromUrl);
								referenceDocument.setUrl(path);
							}
						}else {
							if("".equals(referenceDocument.getDocName()) || referenceDocument.getDocName()==null) {
								referenceDocument.setUrl(referenceDocument.getUrl());
							}else {
							logger.info("|||||||||||||| Inside Else ");
							//referenceDocument.setDocName("");
							referenceDocument.setUrl(path);
							}
						}
						}
						logger.info("line 2243.  Before update(referenceDocument)");
						referenceDocumentService.update(referenceDocument);
						logger.info("line 2245.  After update(referenceDocument)");							
				}
				}
				}
				logger.info("Exit apply action");
			}
			

			public void applyActionListener(ActionEvent e){
				logger.info("enter applyActionListener"); 
				setApplyButtonClicked(true); 
            	/*String message = "Data has been changed on this page.  Please Update if you want to save changes.";
            	this.setErrorMessage(message);
                this.setDisplayButton(true);  */               
				logger.info("exit applyActionListener");
			}			
			
			public SearchBatchIdHandler getBatchIdHandler() {
				return batchIdHandler;
			}

			public void setBatchIdHandler(SearchBatchIdHandler batchIdHandler) {
				this.batchIdHandler = batchIdHandler;
			}


			public ReferenceDocumentService getReferenceDocumentService() {
				return referenceDocumentService;
			}

			public void setReferenceDocumentService(
					ReferenceDocumentService referenceDocumentService) {
				this.referenceDocumentService = referenceDocumentService;
			}

			public boolean isDisplayBackButton() {
				return displayBackButton;
			}

			public void setDisplayBackButton(boolean displayBackButton) {
				this.displayBackButton = displayBackButton;
			}
			//Method to set directory path from Directory browser Backing Bean
			public void setDirectoryPath(String selectedDirectory) {
				importMapProcessPreferenceDTO.setRefDocFilePath(selectedDirectory);
				
				
			}

			public void setCrystalReportPath(String selectedDirectory) {
				importMapProcessPreferenceDTO.setDefaultDirCrystal(selectedDirectory);
				
			}
			
			public void setGlExtractFilePath(String selectedDirectory){
				importMapProcessPreferenceDTO.setGlExtractFilePath(selectedDirectory);
			}
			
			

			public TaxCodeStateService getTaxCodeStateService() {
				return taxCodeStateService;
			}

			public void setTaxCodeStateService(TaxCodeStateService taxCodeStateService) {
				this.taxCodeStateService = taxCodeStateService;
			}

			public boolean isApplyButtonClicked() {
				return applyButtonClicked;
			}

			public void setApplyButtonClicked(boolean applyButtonClicked) {
				this.applyButtonClicked = applyButtonClicked;
			}
	
	public String getAidHostname() {
		return aidHostname;
	}

	public void setAidHostname(String aidHostname) {
		this.aidHostname = aidHostname;
	}
	
	public String getAidHostname2() {
		return aidHostname2;
	}

	public void setAidHostname2(String aidHostname2) {
		this.aidHostname2 = aidHostname2;
	}
	
	public String getAidInstallation() {
		return aidInstallation;
	}

	public void setAidInstallation(String aidInstallation) {
		this.aidInstallation = aidInstallation;
	}
	
	public String getDaysToKeep() {
		return daysToKeep;
	}

	public void setDaysToKeep(String daysToKeep) {
		this.daysToKeep = daysToKeep;
	}
	
	public String getEmailHost() {
		return emailHost;
	}

	public void setEmailHost(String emailHost) {
		this.emailHost = emailHost;
	}
	
	public String getEmailPort() {
		return emailPort;
	}

	public void setEmailPort(String emailPort) {
		this.emailPort = emailPort;
	}
	
	public void setEmailFrom(String emailFrom) {
		this.emailFrom = emailFrom;
	}
	
	public String getEmailFrom() {
		return emailFrom;
	}

	public String getAidPort() {
		return aidPort;
	}

	public void setAidPort(String aidPort) {
		this.aidPort = aidPort;
	}
	
	public String getAidPort2() {
		return aidPort2;
	}

	public void setAidPort2(String aidPort2) {
		this.aidPort2 = aidPort2;
	}
	
	public boolean getEmailStart() {
		return emailStart;
	}

	public void setEmailStart(boolean emailStart) {
		this.emailStart = emailStart;
	}
	
	public String getEmailStop() {
		return emailStop;
	}

	public void setEmailStop(String emailStop) {
		this.emailStop = emailStop;
	}
	
	public String getEmailTo() {
		return emailTo;
	}

	public void setEmailTo(String emailTo) {
		this.emailTo = emailTo;
	}
	
	public String getEmailCc() {
		return emailCc;
	}

	public void setEmailCc(String emailCc) {
		this.emailCc = emailCc;
	}

	public String getAidStatus() {
		if (aidStatus == null) {
			aidStatus = checkAIDStatus(aidHostname, aidPort);
		}
		return aidStatus;
	}

	public void setAidStatus(String aidStatus) {
		this.aidStatus = aidStatus;
	}
	
	public String getAidStatus2() {
		if (aidStatus2 == null) {
			aidStatus2 = checkAIDStatus(aidHostname2, aidPort2);
		}
		return aidStatus2;
	}

	public void setAidStatus2(String aidStatus2) {
		this.aidStatus2 = aidStatus2;
	}
	
	private final String uri = "/InvokeAction//com.ncsts.aid.jmx%3Aname%3DRemoteControlBean";
	
	public String executeAIDStatus(String action, String hostName, String port) {
		StringBuilder content = new StringBuilder();
		if (hostName != null && hostName.trim().length() > 0
				&& port != null && port.trim().length() > 0) {
			try {
				URL url = new URL(
						"http://" + hostName + ":" + port + uri + "/action=" + action + "?action=" + action);
				URLConnection conn = url.openConnection();
				conn.setConnectTimeout(2 * 1000);
				conn.setReadTimeout(5 * 1000);
				BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
				
				String line;
				while ((line = reader.readLine()) != null) {
					content.append(line);
				}
				reader.close();
			} catch (Exception e) {
				content.append(e.getMessage());
				System.out.println("hostName: " + hostName + ", port: " + port + " : " + e.getMessage());
			}
		}
		return content.toString();
	}
	
	public String toggleAIDStatus(String action, String hostName, String port) {
		String status = "";

		String content = executeAIDStatus(action, hostName, port);
		if (content.length() > 0 && content.toString().indexOf(action + " Successful") > -1) {
			status = checkAIDStatus(hostName, port);
		}
		else{
			status = content;
		}

		return status;
	}
	
	public String checkAIDStatus(String hostName, String port) {
		String status = "";
		
		String content = executeAIDStatus("status", hostName, port);

		if (content.length() > 0 && content.toString().indexOf("status Successful") > -1) {
			if (content.toString().indexOf("Running") > -1) {
				status = "Running";
			} else if (content.toString().indexOf("Suspended") > -1) {
				status = "Suspended";
			}
		}
		else{
			status = content;
		}

		return status;
	}
	
	public boolean getSuspendButtonEnabled() {
		if ((aidHostname!=null && aidHostname.trim().length()>0 && aidPort != null && aidPort.trim().length() > 0 && "running".equalsIgnoreCase(getAidStatus())) || 
				(aidHostname2!=null && aidHostname2.trim().length()>0 && aidPort2 != null && aidPort2.trim().length() > 0 && "running".equalsIgnoreCase(getAidStatus2()))) {
			return true;
		} 
		else{
			return false;
		}
	}
	
	public boolean getResumeButtonEnabled() {
		if ((aidHostname!=null && aidHostname.trim().length()>0 && aidPort != null && aidPort.trim().length() > 0 && !"running".equalsIgnoreCase(getAidStatus())) || 
				(aidHostname2!=null && aidHostname2.trim().length()>0 && aidPort2 != null && aidPort2.trim().length() > 0 && !"running".equalsIgnoreCase(getAidStatus2()))) {
			return true;
		} 
		else{
			return false;
		}
	}
	
	public void aidResumeActionListener(ActionEvent e) {
		aidStatus = toggleAIDStatus("resume", aidHostname, aidPort);
		aidStatus2 = toggleAIDStatus("resume", aidHostname2, aidPort2);
	}
	
	public void aidSuspendActionListener(ActionEvent e) {
		aidStatus = toggleAIDStatus("suspend", aidHostname, aidPort);
		aidStatus2 = toggleAIDStatus("suspend", aidHostname2, aidPort2);
	}
			
	public List<SelectItem> getDefaultNexusTypes() {
		if(this.defaultNexusTypes == null) {
			this.defaultNexusTypes = cacheManager.createNexusTypeItems();
		}
		
		return this.defaultNexusTypes;
	}

	public void setDefaultNexusTypes(List<SelectItem> defaultNexusTypes) {
		this.defaultNexusTypes = defaultNexusTypes;
	}

	public String getLicenseKey() {
		return licenseKey;
	}

	public void setLicenseKey(String licenseKey) {
		this.licenseKey = licenseKey;
	}

	public boolean isShowLicenseWarning() {
		return showLicenseWarning;
	}

	public void setShowLicenseWarning(boolean showLicenseWarning) {
		this.showLicenseWarning = showLicenseWarning;
	}

	public loginBean getLoginservice() {
		return loginservice;
	}

	public void setLoginservice(loginBean loginservice) {
		this.loginservice = loginservice;
	}
	
	private static final String VERIFY_PINPOINT_API_REQUEST = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><mes:StatusRequest xmlns:mes=\"http://www.seconddecimal.com/ncsts/schemas/messages\"></mes:StatusRequest>";
		
	public void verifyPinPointAPIActionListener(ActionEvent e) {
		ApiTestBean apiTestBean = new ApiTestBean();
		apiTestBean.setRequest(VERIFY_PINPOINT_API_REQUEST);
		apiTestBean.setEndPoint(importMapProcessPreferenceDTO.getSaleApiServer() + "/ncsts/rest/status"); //Like, https://10.1.106.18:8225/ncsts/rest/ping
		
		apiTestBean.setUsername(importMapProcessPreferenceDTO.getSaleApiUserId() + "\\" + importMapProcessPreferenceDTO.getSaleApiDatabase());
		apiTestBean.setPassword(importMapProcessPreferenceDTO.getSaleApiPassword());
		apiTestBean.submitAction();
		String response = apiTestBean.getResponse();
		
		if(response!=null && response.indexOf("<applicationVersion>PinPoint")>=0){
			this.setErrorMessage("Connection succeeded.");
		}
		else{
			this.setErrorMessage(response);
		}
	}
	
	public void verifyControlPointAPIActionListener(ActionEvent e) {
		String response = "";
		try {
            // Create SOAP Connection
        	javax.xml.soap.SOAPConnectionFactory soapConnectionFactory = javax.xml.soap.SOAPConnectionFactory.newInstance();
        	javax.xml.soap.SOAPConnection soapConnection = soapConnectionFactory.createConnection();

            // Send SOAP Message to SOAP Server
        	//Example: http://10.1.107.22/CertiSoftDispatchService/CertiSoftDispatchService.asmx 
            String url = companyDTO.getControlPointServer() + "/CertiSoftDispatchService/CertiSoftDispatchService.asmx";
            javax.xml.soap.SOAPMessage soapResponse = soapConnection.call(createSOAPRequest(), url);

            // Process the SOAP Response
            System.out.print("\nResponse SOAP Message = ");
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            soapResponse.writeTo(out);
            response = new String(out.toByteArray());
            System.out.print(response);
            
            String beginMessage = "<Message>";
        	String endMessage = "</Message>";
        	
        	int beginIndex =  response.indexOf(beginMessage);
        	int endIndex =  response.indexOf(endMessage);
        	if(beginIndex>=0 && endIndex>=0){
        		response = response.substring(beginIndex+beginMessage.length(), endIndex);
        	}
        	
            soapConnection.close();
        } catch (Exception ee) {
            System.err.println("Error occurred while sending SOAP Request to Server");
            ee.printStackTrace();
            response = ee.getMessage();
        }
		
		if(response!=null && response.indexOf("Unknown method")>=0){//We logged into and found no method.
			this.setErrorMessage("Connection succeeded.");
		}
		else{
			this.setErrorMessage(response);
		}
	}
	
    private javax.xml.soap.SOAPMessage createSOAPRequest() throws Exception {
    	javax.xml.soap.MessageFactory messageFactory = javax.xml.soap.MessageFactory.newInstance();
    	javax.xml.soap.SOAPMessage soapMessage = messageFactory.createMessage();
    	
    	javax.xml.soap.MimeHeaders headers = soapMessage.getMimeHeaders();
        headers.addHeader("SOAPAction", "http://integration.workflow.ncsts.com/CertiSoftDispatchService/Dispatch");
    	javax.xml.soap.SOAPPart soapPart = soapMessage.getSOAPPart();
    	
        // SOAP Envelope
        javax.xml.soap.SOAPEnvelope envelope = soapPart.getEnvelope();
        envelope.addNamespaceDeclaration("xsi", "http://www.w3.org/2001/XMLSchema-instance");
        envelope.addNamespaceDeclaration("xsd", "http://www.w3.org/2001/XMLSchema");

        /*
        <SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
        	<SOAP-ENV:Header/>
        	<SOAP-ENV:Body>
        		<Dispatch xmlns="http://integration.workflow.ncsts.com/">
        			<RequestXml>
        				<![CDATA[<?xml version="1.0" encoding="utf-8" ?>
        				<request><arguments>
        					<item><key>CertiSoft_Username</key><value>admin</value></item>
        					<item><key>CertiSoft_Password</key><value>Admin01!</value></item>
        					<item><key>method</key><value>Verification</value></item>
        				</arguments></request>
        				]]>
        			</RequestXml>
        		</Dispatch>
        	</SOAP-ENV:Body>
        </SOAP-ENV:Envelope>
        */

        // SOAP Body
        javax.xml.soap.SOAPBody soapBody = envelope.getBody();
        javax.xml.namespace.QName bodyName = new javax.xml.namespace.QName("http://integration.workflow.ncsts.com/", "Dispatch" );
        javax.xml.soap.SOAPBodyElement soapBodyElem = soapBody.addBodyElement(bodyName);


        javax.xml.namespace.QName name = new javax.xml.namespace.QName("RequestXml");
        
        String requestXml = 
        		"<request><arguments>" +
				"<item><key>CertiSoft_Username</key><value>" + companyDTO.getControlPointUserId() + "</value></item>" + 
				"<item><key>CertiSoft_Password</key><value>" + companyDTO.getControlPointPassword() + "</value></item>" + 
				"<item><key>method</key><value>Verification</value></item>" + 
				"</arguments></request>";

        javax.xml.soap.SOAPElement soapBodyElem1 = soapBodyElem.addChildElement(name);
        String textNode = 	"<![CDATA[<?xml version=\"1.0\" encoding=\"utf-8\" ?>" + requestXml + "]]>";
        
        soapBodyElem1.addTextNode(textNode);
        soapMessage.saveChanges();

        /* Print the request message */
        System.out.print("Request SOAP Message = ");
        soapMessage.writeTo(System.out);
        System.out.println();

        return soapMessage;
    }
    
    public static void main(String[] args) throws FileNotFoundException, IOException {
		try {
            // Create SOAP Connection
        	javax.xml.soap.SOAPConnectionFactory soapConnectionFactory = javax.xml.soap.SOAPConnectionFactory.newInstance();
        	javax.xml.soap.SOAPConnection soapConnection = soapConnectionFactory.createConnection();

            // Send SOAP Message to SOAP Server
        	String url = "http://10.1.107.22/CertiSoftDispatchService/CertiSoftDispatchService.asmx"; 
            //String url = "http://localhost:57148/CertiSoftDispatchService.asmx";
            javax.xml.soap.SOAPMessage soapResponse = soapConnection.call(createSOAPRequestTest(), url);

            // Process the SOAP Response
            processResponse(soapResponse);     
            soapConnection.close();
        } catch (Exception ee) {
            System.err.println("Error occurred while sending SOAP Request to Server");
            ee.printStackTrace();
        }
    }
    
    public static javax.xml.soap.SOAPMessage createSOAPRequestTest() throws Exception {
    	javax.xml.soap.MessageFactory messageFactory = javax.xml.soap.MessageFactory.newInstance();
    	javax.xml.soap.SOAPMessage soapMessage = messageFactory.createMessage();
    	
    	javax.xml.soap.MimeHeaders headers = soapMessage.getMimeHeaders();
        headers.addHeader("SOAPAction", "http://integration.workflow.ncsts.com/CertiSoftDispatchService/Dispatch");
    	javax.xml.soap.SOAPPart soapPart = soapMessage.getSOAPPart();
    	
        // SOAP Envelope
        javax.xml.soap.SOAPEnvelope envelope = soapPart.getEnvelope();
        envelope.addNamespaceDeclaration("xsi", "http://www.w3.org/2001/XMLSchema-instance");
        envelope.addNamespaceDeclaration("xsd", "http://www.w3.org/2001/XMLSchema");

        // SOAP Body
        javax.xml.soap.SOAPBody soapBody = envelope.getBody();
        javax.xml.namespace.QName bodyName = new javax.xml.namespace.QName("http://integration.workflow.ncsts.com/", "Dispatch" );
        javax.xml.soap.SOAPBodyElement soapBodyElem = soapBody.addBodyElement(bodyName);

        javax.xml.namespace.QName name = new javax.xml.namespace.QName("RequestXml");
        javax.xml.soap.SOAPElement soapBodyElem1 = soapBodyElem.addChildElement(name);
        
        soapBodyElem1.addTextNode(createContent());
        soapMessage.saveChanges();

        /* Print the request message */
        System.out.print("Request SOAP Message = ");
        soapMessage.writeTo(System.out);
        System.out.println();

        return soapMessage;
    }
    
    public static String createContent() throws Exception {
    	//File file = new File("C:\\ControlPoint\\CERTIFICATE_SAVED\\UploadCertificate.pdf");//ArchTest.pdf
    	File file = new File("C:\\ControlPoint\\CERTIFICATE_SAVED\\Test.pdf");
    	FileInputStream fis = new FileInputStream(file);
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        byte[] buf = new byte[1024];
        int totalBytes = 0;
        try {
            for (int readNum; (readNum = fis.read(buf)) != -1;) {
                bos.write(buf, 0, readNum); //no doubt here is 0
                totalBytes = totalBytes + readNum;
            }
        } catch (IOException ex) {
           ex.printStackTrace();
        }      
        System.out.println("read " + totalBytes + " bytes,");
        byte[] bytes = bos.toByteArray();
        String base64String = new sun.misc.BASE64Encoder().encode(bytes);
        String documentName = "ArchTest.pdf";
        
        String requestXml = 
        		"<request><arguments>" +
				"<item><key>CertiSoft_Username</key><value>admin</value></item>" + 
				"<item><key>CertiSoft_Password</key><value>Admin01!</value></item>" + 
				"<item><key>method</key><value>UploadCertificate</value></item>" +
				"<item><key>Document64String</key><value>" + base64String + "</value></item>" +
				"<item><key>DocumentName</key><value>" + documentName + "</value></item>" +
				"</arguments></request>";

        return 	"<![CDATA[<?xml version=\"1.0\" encoding=\"utf-8\" ?>" + requestXml + "]]>";
    }
    
    public static void processResponse1(javax.xml.soap.SOAPMessage soapResponse) throws Exception {
    	String response = "";
		try {
            System.out.print("\nResponse SOAP Message = ");
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            soapResponse.writeTo(out);
            response = new String(out.toByteArray());
            System.out.print(response);
            
            String beginMessage = "<Message>";
        	String endMessage = "</Message>";
        	
        	int beginIndex =  response.indexOf(beginMessage);
        	int endIndex =  response.indexOf(endMessage);
        	if(beginIndex>=0 && endIndex>=0){
        		response = response.substring(beginIndex+beginMessage.length(), endIndex);
        	}
        } catch (Exception ee) {
            System.err.println("Error occurred while sending SOAP Request to Server");
            ee.printStackTrace();
            response = ee.getMessage();
        }
		
		if(response!=null && response.indexOf("Unknown method")>=0){//We logged into and found no method.
			System.out.println("Connection succeeded.");
		}
		else{
			System.out.println(response);
		}
    }
    
    public static void processResponse(javax.xml.soap.SOAPMessage soapResponse) throws Exception {
    	String response = "";
		try {
            System.out.print("\nResponse SOAP Message = ");
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            soapResponse.writeTo(out);
            response = new String(out.toByteArray());
            System.out.print(response);

            String beginMessage = "<Message>";
        	String endMessage = "</Message>";
        	
        	int beginIndex =  response.indexOf(beginMessage);
        	int endIndex =  response.indexOf(endMessage);
        	if(beginIndex>=0 && endIndex>=0){
        		response = response.substring(beginIndex+beginMessage.length(), endIndex);
        	}
        } catch (Exception ee) {
            System.err.println("Error occurred while sending SOAP Request to Server");
            ee.printStackTrace();
            response = ee.getMessage();
        }
		
		if(response!=null && response.indexOf("Unknown method")>=0){//We logged into and found no method.
			System.out.println("Connection succeeded.");
		}
		else{
			System.out.println(response);
		}
    }
    
    public void verifyRatePointAPIActionListener(ActionEvent e) {
    	String response = "";
    	
		String userName = companyDTO.getRatepointUserId();//"superuser";
		String password = companyDTO.getRatepointPassword();//"Second1*";
		String endPoint = companyDTO.getRatepointServer();
		
		if(userName==null || userName.length()==0){
			this.setErrorMessage("User ID cannot be empty.");
			return;
		}
		
		if(password==null || password.length()==0){
			this.setErrorMessage("Password cannot be empty.");
			return;
		}
		
		if(endPoint==null || endPoint.length()==0){
			this.setErrorMessage("Server cannot be empty.");
			return;
		}
		
		if(endPoint.endsWith("/")){
			this.setErrorMessage("Server cannot end with '/'.");
			return;
		}	
		
		endPoint = endPoint + "/ratepoint";//"http://ratepoint.sd-apps.com:80/ratepoint";

		String requestXml = "<request><username>" + userName + "</username><password>" + password + "</password>" +
							"<zip>ZIP</zip><taxType>TAXTYPE</taxType><state>STATE</state>" +
							"<county>COUNTY</county><city>CITY</city><vertical>VERTICAL</vertical></request>";
		
    	try{
		    PostMethod method = new PostMethod(endPoint + "/api/getRate");
			method.addParameter("request", requestXml);
			
			HttpClientParams params = new HttpClientParams();
			params.setParameter(HttpClientParams.CONNECTION_MANAGER_TIMEOUT, (long)2*60*1000);
			params.setParameter(HttpClientParams.SO_TIMEOUT, (int) (long)2*60*1000);
			
			HttpClient client = new HttpClient(params);
			client.executeMethod(method);
			
			// <?xml version="1.0" encoding="UTF-8"?><response><message>No rates found</message></response>
			response = method.getResponseBodyAsString();
			
			String beginMessage = "<Message>";
        	String endMessage = "</Message>";	
        	int beginIndex =  response.indexOf(beginMessage);
        	int endIndex =  response.indexOf(endMessage);
        	if(beginIndex>=0 && endIndex>=0){
        		response = response.substring(beginIndex+beginMessage.length(), endIndex);
        	}

    	}
    	catch(Exception ee){
    		ee.printStackTrace();
    		response = ee.getMessage();
    	}
    	
    	if(response!=null && response.indexOf("No rates found")>=0){//We logged into and found no rate.
			this.setErrorMessage("Connection succeeded.");
		}
		else{
			this.setErrorMessage(response);
		}
    }
    public final List<SelectItem> getMaxDecimalListItems() {
		maxdecimalplacesList.clear();
		SelectItem[] maxdecimalplaceItemArray = new SelectItem[10];
		for (int i = 2; i <= 11; i++) {
			maxdecimalplaceItemArray[i-2] = new SelectItem(String.valueOf(i),String.valueOf(i));
			maxdecimalplacesList.add(maxdecimalplaceItemArray[i-2]);
		}
		return maxdecimalplacesList;
	}
    
    public User getCurrentUser() {
		return loginBean.getUser();
   }
    
   public String getLonLatLookup(){
	   if(lonLatLookup == null)
		   this.lonLatLookup = optionService.findByPK(new OptionCodePK("LATLONLOOKUPURL", "SYSTEM", "SYSTEM")).getValue();
	   return lonLatLookup;
   }
   
   public String getGenJurLookup(){
	   if(genJurLookup == null)
		   this.genJurLookup = optionService.findByPK(new OptionCodePK("GENERICJURAPIURL", "SYSTEM", "SYSTEM")).getValue();
	   return genJurLookup;
   }
   
   public String getStreetLookup(){
	   if(streetLookup == null)
		   this.streetLookup = optionService.findByPK(new OptionCodePK("STREETLEVELURL", "SYSTEM", "SYSTEM")).getValue();
	   return streetLookup;
   }

	public void setLonLatLookup(String lonLatLookup) {
		this.lonLatLookup = lonLatLookup;
	}
	
	public void setGenJurLookup(String genJurLookup) {
		this.genJurLookup = genJurLookup;
	}
	
	public void setStreetLookup(String streetLookup) {
		this.streetLookup = streetLookup;
	}
	   
   
}
	