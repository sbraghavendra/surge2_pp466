package com.ncsts.view.bean;

import java.util.concurrent.Future;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Component;

import com.ncsts.task.BatchProcessTask;

@Component
@Scope("session")
public class BatchProcessBackingBean extends BatchProcessTask {
	private static final Log logger = LogFactory.getLog(BatchProcessBackingBean.class);
	
	private volatile boolean running = false;
	
	public String runBatchProcess() {
		String message = "";
		if(running) {
			logger.info("Polling is already running");
			message = "Batch process is already running";
		}
		else {
			message = "Batch process started";
			pollAsync();
		}
		
		return message;
	}
	
	@Async
	public Future<String> pollAsync() {
		logger.info("Polling for batches");
		running = true;
		try {
			poll();
		}
		catch(Exception e){
			logger.error(e.getMessage(), e);
		}
		running = false;
		logger.info("Polling for batches completed");
		
		return new AsyncResult<String>("Batch process completed");
	}
}
