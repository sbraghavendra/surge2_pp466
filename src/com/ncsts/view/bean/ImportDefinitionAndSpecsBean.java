package com.ncsts.view.bean;

import java.io.BufferedReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.component.UISelectItems;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.event.ValueChangeListener;
import javax.faces.model.SelectItem;

import org.ajax4jsf.component.UIDataAdaptor;
import org.apache.log4j.Logger;
import org.apache.myfaces.custom.fileupload.HtmlInputFileUpload;
import org.aspectj.lang.annotation.Aspect;
import org.richfaces.component.html.HtmlDataTable;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.ncsts.common.CacheManager;
import com.ncsts.domain.DataDefinitionColumn;
import com.ncsts.domain.ImportConnection;
import com.ncsts.domain.ImportDefinition;
import com.ncsts.domain.ImportDefinitionDetail;
import com.ncsts.domain.ImportSpec;
import com.ncsts.domain.ImportSpecPK;
import com.ncsts.domain.ListCodes;
import com.ncsts.domain.Option;
import com.ncsts.domain.OptionCodePK;
import com.ncsts.domain.User;
import com.ncsts.dto.ImportDefDTO;
import com.ncsts.dto.ImportMapProcessPreferenceDTO;
import com.ncsts.dto.ImportSpecDTO;
import com.ncsts.dto.UserDTO;
import com.ncsts.jsf.model.BaseExtendedDataModel;
import com.ncsts.jsf.model.ImportConnectionModel;
import com.ncsts.jsf.model.ImportDefinitionModel;
import com.ncsts.jsf.model.ImportSpecModel;
import com.ncsts.services.DataDefinitionService;
import com.ncsts.services.ImportDefAndSpecsService;
import com.ncsts.services.ImportDefinitionService;
import com.ncsts.services.OptionService;
import com.ncsts.services.TaxCodeDetailService;
import com.ncsts.services.UserService;
import com.ncsts.view.util.AbstractFileUploader;
import com.ncsts.view.util.JsfHelper;
import com.ncsts.view.util.PathResolver;
import com.ncsts.view.util.TogglePanelController;
@Aspect
public class ImportDefinitionAndSpecsBean extends AbstractFileUploader implements ValueChangeListener, TaxCodeCodeHandler.TaxCodeCodeHandlerCallback  {
	public enum ScreenType {
		Definition,
		Spec,
		Connection
	}
	
	public enum ImportDefsAndSpecsAction {
		// MapDef action text depends on type of def (FD, DB, FF)
		AddSpec("Add an Import Spec"),
		copyAddSpec("Copy/Add an Import Spec"),
		UpdateSpec("Update an Import Spec"),
		DeleteSpec("Delete an Import Spec"),
		ViewSpec("View an Import Spec"),
		MapSpec("Map Import Spec Users"),
		AddDef("Add an Import Definition"),
		copyAddDef("Copy/Add an Import Definition"),
		UpdateDef("Update an Import Definition"),
		DeleteDef("Delete an Import Definition"),
		ViewDef("View an Import Definition"),
		MapDef("Map Import File"),
		AddConn("Add an Import Connection"),
		UpdateConn("Update an Import Connection"),
//		CopyAddConn("Copy/Add a Connection"),
		DeleteConn("Delete a Connection"),
		MapConn("Map a Connection"),
		ViewConn("View a Connection");

		private String actionText;

		ImportDefsAndSpecsAction(String actionText) {
			this.actionText = actionText;
		}

		public String getActionText() {
			return actionText;
		}
	}
	
	private Logger logger = Logger.getLogger(ImportDefinitionAndSpecsBean.class);
	private static final List<String> REPLACE_WITH_MARKERS = 
			new ArrayList<String>(Arrays.asList(new String[] {"~a", "~b", "~c", "~d", "~s"}));
	private ImportDefsAndSpecsAction currentAction;
	
	private List<SelectItem> screenTypeMenuItems;
	private List<SelectItem> allUsersMenuItems;
	
	// Drop down for Add page
	private HtmlSelectOneMenu importSpecTypeMenu;
	private HtmlSelectOneMenu importDefinitionMenu;
	
	@Autowired
	private TaxCodeBackingBean taxCodeBackingBean;
	
	private TaxCodeCodeHandler filterTaxCodeHandler = new TaxCodeCodeHandler();
	
	@Autowired
	private OptionService optionService;
	@Autowired
	private loginBean loginservice;
	@Autowired
	protected DataDefinitionService dataDefinitionService;	
	
	private HtmlInputFileUpload uploadFile;
	private HtmlInputText processTransactionAmountInput = new HtmlInputText();
	private List<ImportDefDTO> importDefList; // this list is declared for displaying the table rows
	private List<ImportSpecDTO> importSpecList; // Main Page Resuts
	private List<UserDTO> userList;
	private List<String> userCodeList;
	private ImportDefDTO selectedImportDef;
	private ImportDefDTO selectedImportDefOri;
	private ImportSpecDTO selectedImportSpec;
	private ImportSpecDTO selectedImportSpecOri;
	private List<ImportSpecDTO> selectedImportSpecList;
	private ImportMapProcessPreferenceDTO importMapProcessPreferenceDTO;
	private HtmlInputText holdTransactionAmount = new HtmlInputText();
	private CacheManager cacheManager;
	private boolean displayButton = false;
	private String errorMessage;
	private int selectedDefIndex = -1;
	private int selectedSpecIndex = -1;
	private int selectedConnectionIndex = -1;
	private int selectedDefUsingConnIndex = -1;
	private int selectedSpecUsingDefIndex = -1;
	
	private ScreenType screenType = ScreenType.Definition;
	
	private ImportSpecModel importSpecModel;
	private ImportConnectionModel importConnectionModel;
	private ImportDefinitionModel importDefinitionModel;
	
	private ImportSpec selectedImportSpecComponent;
	private ImportDefinition selectedImportDefinitionComponent;
	private ImportConnection selectedImportConnectionComponent;
	private ImportConnection indexImportConnectionComponent;
	
	private List<ImportDefDTO> definitionsUsingConnection;
	
	private HtmlDataTable importSpecTable;
	private HtmlDataTable importConnectionTable;
	private HtmlDataTable importDefinitionTable;
	
	private LinkedHashSet<String> importSpecColumns;
	private LinkedHashSet<String> importConnectionColumns;
	private LinkedHashSet<String> importDefinitionColumns;
	
	private MatrixCommonBean matrixCommonBean;
	
	private ImportDefAndSpecsService importDefAndSpecsService;
	private UserService userService;
	private ImportDefinitionMappingBean importDefinitionMappingBean;
	private String sampleFileName;
	private String sampleFileNameOri;
	private Map<String,String> defaultPreferenceMap;
	private Map<String,String> fileDupMap;
	private TaxCodeDetailService taxCodeDetailService;
	private boolean displayBackButton = false;
	private boolean displaySearch = false;
	private DoNotImportCharBean doNotImportCharBean = new DoNotImportCharBean();
	List<SelectItem> importDefTypeItems = null;
	List<SelectItem> importSpecTypeItems = null;
	List<SelectItem> importFileTypeItems = null;
	List<SelectItem> encodingItems = null;
	List<SelectItem> importConnectionItems = null;
	List<SelectItem> importTableItems = null;
	
	List<SelectItem> importDefSpecTypeItems = null;
	
	private String importDefSpecType;
	private String defSpecCode;
	private String defDescriptionText;
	private String defCommentsText;
	private String connectionType;
	private Map<String, String> administrativeMap = null;
	
	private String verificationErrorMessage;
	
	private boolean controlToValuesSucceed = true;
	
	private TogglePanelController togglePanelController = null;
	
	@Autowired
	private FilePreferenceBackingBean filepreferenceBean;
	
	@Autowired
	private ImportDefinitionService importDefinitionService;
	
  	public ImportDefinitionAndSpecsBean() {
		super(PathResolver.getSampleFileUploadPath());
		filterTaxCodeHandler = new TaxCodeCodeHandler();
		
		importSpecColumns = new LinkedHashSet<String>();
		importSpecColumns.add("importSpecCode");
		importSpecColumns.add("importSpecType");
		importSpecColumns.add("description");
		importSpecColumns.add("importDefinitionCode");
		importSpecColumns.add("defaultDirectory");
		importSpecColumns.add("comments");
		importSpecColumns.add("lastImportFileName");
		importSpecColumns.add("whereClause");
		importSpecColumns.add("filesizedupres");
		importSpecColumns.add("filenamedupres");
		importSpecColumns.add("importdatemap");
		importSpecColumns.add("donotimport");
		importSpecColumns.add("replacewith");
		importSpecColumns.add("batcheofmarker");
		importSpecColumns.add("batchcountmarker");
		importSpecColumns.add("batchtotalmarker");
		importSpecColumns.add("delete0amtflag");
		importSpecColumns.add("holdtransflag");
		importSpecColumns.add("holdtransamt");
		importSpecColumns.add("autoprocflag");
		importSpecColumns.add("autoprocamt");
		importSpecColumns.add("autoprocid");
		importSpecColumns.add("autoprocessFlag");
		
		importConnectionColumns = new LinkedHashSet<String>();
		importConnectionColumns.add("importConnectionCode");
		importConnectionColumns.add("importConnectionType");
		importConnectionColumns.add("description");
		importConnectionColumns.add("dbDriverClassName");
		importConnectionColumns.add("dbUrl");
		importConnectionColumns.add("dbUserName");
		importConnectionColumns.add("dbPassword");
		importConnectionColumns.add("comments");		
		
		importDefinitionColumns = new LinkedHashSet<String>();
		importDefinitionColumns.add("importDefinitionCode");
		importDefinitionColumns.add("importDefinitionType");
		importDefinitionColumns.add("description");
		importDefinitionColumns.add("importFileTypeCode");
		importDefinitionColumns.add("flatDelChar");
		importDefinitionColumns.add("flatDelTextQual");
		importDefinitionColumns.add("flatDelLine1HeadFlag");
		importDefinitionColumns.add("comments");
		importDefinitionColumns.add("sampleFileName");
		importDefinitionColumns.add("mapUpdateUserId");
		importDefinitionColumns.add("mapUpdateTimestamp");
			
	}
	public void init() {
		//filterTaxCodeHandlerDefault.setTaxcodeCode(getImportMapProcessPreferenceDTO().getRatepointDefaultTaxCode());
	   filterTaxCodeHandler.setTaxcodeCode(getImportMapProcessPreferenceDTO().getApplyTaxCodeAmount());
	}
	
	public ImportMapProcessPreferenceDTO getImportMapProcessPreferenceDTO() {
		if (importMapProcessPreferenceDTO == null) {
			importMapProcessPreferenceDTO = new ImportMapProcessPreferenceDTO();
			Map<String,String> map = new HashMap<String,String>();	
			//map.putAll(getSystemPreferenceMap());
			//map.putAll(getAdminPreferenceMap());		
			//map.putAll(getUserPreferenceMap());	
			
			//System - Import/Map/Process
			String fileNameDuplicateRestriction =  (String)map.get("FILENAMEDUPRES");				
			importMapProcessPreferenceDTO.setFileNameDuplicateRestriction(fileNameDuplicateRestriction);
			String fileSizeDuplicateRestriction =  (String)map.get("FILESIZEDUPRES");				
			importMapProcessPreferenceDTO.setFileSizeDuplicateRestriction(fileSizeDuplicateRestriction);
			String terminalErrorsBeforeAbortImport =  (String)map.get("ERRORROWS");				
			importMapProcessPreferenceDTO.setTerminalErrorsBeforeAbortImport(terminalErrorsBeforeAbortImport);		
			String defaultImportDateMap =  (String)map.get("IMPORTDATEMAP");				
			importMapProcessPreferenceDTO.setDefaultImportDateMap(defaultImportDateMap);
			String rowsToImportMapBeforeSave =  (String)map.get("MAPROWS");	
			importMapProcessPreferenceDTO.setRowsToImportMapBeforeSave(rowsToImportMapBeforeSave);
			String endOfFileMarker =  (String)map.get("BATCHEOFMARKER");				
			importMapProcessPreferenceDTO.setEndOfFileMarker(endOfFileMarker);	
			String batchCountMarker =  (String)map.get("BATCHCOUNTMARKER");				
			importMapProcessPreferenceDTO.setBatchCountMarker(batchCountMarker);
			String batchTotalMarker =  (String)map.get("BATCHTOTALMARKER");				
			importMapProcessPreferenceDTO.setBatchTotalMarker(batchTotalMarker);	
			String deleteTransactionWithZeroAmountFlag =  (String)map.get("DELETE0AMTFLAG");
			if ("1".equalsIgnoreCase(deleteTransactionWithZeroAmountFlag)) {
				importMapProcessPreferenceDTO.setDeleteTransactionWithZeroAmountFlag(true);	
			} else {
				importMapProcessPreferenceDTO.setDeleteTransactionWithZeroAmountFlag(false);					
			}		
			String holdTransactionAmountFlag =  (String)map.get("HOLDTRANSFLAG");
			if ("1".equalsIgnoreCase(holdTransactionAmountFlag)) {
				importMapProcessPreferenceDTO.setHoldTransactionAmountFlag(true);
			} else {
				importMapProcessPreferenceDTO.setHoldTransactionAmountFlag(false);				
			}			
			String processTransactionAmountFlag =  (String)map.get("AUTOPROCFLAG");	
			if ("1".equalsIgnoreCase(processTransactionAmountFlag)) {
				importMapProcessPreferenceDTO.setProcessTransactionAmountFlag(true);
			} else {
				importMapProcessPreferenceDTO.setProcessTransactionAmountFlag(false);				
			}			
			/*String killProcessingProcedureCountFlag =  (String)map.get("KILLPROCFLAG");	
			if ("1".equalsIgnoreCase(killProcessingProcedureCountFlag)) {
				importMapProcessPreferenceDTO.setKillProcessingProcedureCountFlag(true);
			} else {
				importMapProcessPreferenceDTO.setKillProcessingProcedureCountFlag(false);				
			}*/		
			String holdTransactionAmount =  (String)map.get("HOLDTRANSAMT");				
			importMapProcessPreferenceDTO.setHoldTransactionAmount(holdTransactionAmount);		
			String processTransactionAmount =  (String)map.get("AUTOPROCAMT");				
			importMapProcessPreferenceDTO.setProcessTransactionAmount(processTransactionAmount);
			String applyTaxCodeAmount =  (String)map.get("AUTOPROCID");		
			filterTaxCodeHandler.setTaxcodeCode(applyTaxCodeAmount);
			
	    //	String killProcessingProcedureCount =  (String)map.get("KILLPROCCOUNT");				
			//importMapProcessPreferenceDTO.setKillProcessingProcedureCount(killProcessingProcedureCount);
			String doNotImportMarkers = (String)map.get("DONOTIMPORT");
			importMapProcessPreferenceDTO.setDoNotImportMarkers(doNotImportMarkers);
			String replaceWithMarkers = (String)map.get("REPLACEWITH");
			importMapProcessPreferenceDTO.setReplaceWithMarkers(replaceWithMarkers);
			
			//Admin - Import/Map/Process			
			String allocationsEnabled =  (String)map.get("ALLOCATIONSENABLED");	
			if ("1".equalsIgnoreCase(allocationsEnabled)) {
				importMapProcessPreferenceDTO.setAllocationsEnabled(true);					
			} else {
				importMapProcessPreferenceDTO.setAllocationsEnabled(false);					
			}

			//System - GL Extract
		    String glExtractFilePath = (String)map.get("GLEXTRACTFILEPATH");
		    importMapProcessPreferenceDTO.setGlExtractFilePath(glExtractFilePath);
		    String glExtractFilePrefix = (String)map.get("GLEXTRACTPREFIX");
		    importMapProcessPreferenceDTO.setGlExtractFilePrefix(glExtractFilePrefix);
		    String glExtractFileExtension = (String)map.get("GLEXTRACTEXT"); 
		    importMapProcessPreferenceDTO.setGlExtractFileExtension(glExtractFileExtension);
		    
			//Admin - GL Extract		    
		    String glExtractEnabled = (String)map.get("GLEXTRACTENABLED");
		    if ("1".equalsIgnoreCase(glExtractEnabled)) {
		    	importMapProcessPreferenceDTO.setGlExtractEnabled(true);
		    } else {
		    	importMapProcessPreferenceDTO.setGlExtractEnabled(false);		    	
		    }
		    String glExtractWindowName = (String)map.get("GLEXTRACTWIN");
		    importMapProcessPreferenceDTO.setGlExtractWindowName(glExtractWindowName);
		    String glExtractSetMark = (String)map.get("GLEXTRACTMARK");	
		    if ("1".equalsIgnoreCase(glExtractSetMark)) {
		    	importMapProcessPreferenceDTO.setGlExtractSetMark(true);
		    } else {
		    	importMapProcessPreferenceDTO.setGlExtractSetMark(false);	    	
		    }	
		    
		    //System - Security
		    String pwdExpirationWarningDays = (String)map.get("PWEXPDAYS"); 
		    importMapProcessPreferenceDTO.setPwdExpirationWarningDays(pwdExpirationWarningDays);
		    String chgPassword = (String)map.get("PWUSERCHG");
		    if ("1".equalsIgnoreCase(chgPassword)) {
			    importMapProcessPreferenceDTO.setChgPassword(true);		    	
		    } else {
			    importMapProcessPreferenceDTO.setChgPassword(false);			    	
		    }		    
		    String denyPasswordChangeMessage = (String)map.get("PWDENYMSG");
		    importMapProcessPreferenceDTO.setDenyPasswordChangeMessage(denyPasswordChangeMessage);
		    
		    //Admin - Security
		    String activateExtendedSecurity = (String)map.get("EXTENDSEC");
		    if ("1".equalsIgnoreCase(activateExtendedSecurity)) {
			    importMapProcessPreferenceDTO.setActivateExtendedSecurity(true);		    	
		    } else {
			    importMapProcessPreferenceDTO.setActivateExtendedSecurity(false);		    	
		    }
		    String activateDebugExtendedSecurity = (String)map.get("DEBUGSEC");	    
		    if ("1".equalsIgnoreCase(activateDebugExtendedSecurity)) {
			    importMapProcessPreferenceDTO.setActivateDebugExtendedSecurity(true);		    	
		    } else {
			    importMapProcessPreferenceDTO.setActivateDebugExtendedSecurity(false);			    	
		    }	
		    
		    //System - Directories
		    String sqlFilePath = (String)map.get("SQLFILEPATH");
		    importMapProcessPreferenceDTO.setSqlFilePath(sqlFilePath);
		    String sqlFileExt = (String)map.get("SQLFILEEXT");
		    importMapProcessPreferenceDTO.setSqlFileExt(sqlFileExt);
		    String impExpFilePath = (String)map.get("IMPEXPFILEPATH");
		    importMapProcessPreferenceDTO.setImpExpFilePath(impExpFilePath);
		    String impExpFileExt = (String)map.get("IMPEXPFILEEXT");
		    importMapProcessPreferenceDTO.setImpExpFileExt(impExpFileExt);
		    String refDocFilePath = (String)map.get("REFDOCFILEPATH");
		    importMapProcessPreferenceDTO.setRefDocFilePath(refDocFilePath);
		    String refDocFileExt = (String)map.get("REFDOCFILEEXT");
		    importMapProcessPreferenceDTO.setRefDocFileExt(refDocFileExt);
		    String defaultDirCrystal = (String)map.get("DEFAULTDIRCRYSTAL");
		    importMapProcessPreferenceDTO.setDefaultDirCrystal(defaultDirCrystal);

            //User - Directories
		    String importFilePath = (String)map.get("IMPORTFILEPATH");
		    importMapProcessPreferenceDTO.setImportFilePath(importFilePath);
		    String importFileExt = (String)map.get("IMPORTFILEEXT");
		    importMapProcessPreferenceDTO.setImportFileExt(importFileExt);
		    String userDirCrystal  = (String)map.get("USERDIRCRYSTAL");	
		    importMapProcessPreferenceDTO.setUserDirCrystal(userDirCrystal);
		    
		    // System - Tax Updates
		    String lastTaxRateDateFull = (String)map.get("LASTTAXRATEDATEFULL");
		    importMapProcessPreferenceDTO.setLastTaxRateDateFull(lastTaxRateDateFull);
		    String lastTaxRateRelFull = (String)map.get("LASTTAXRATERELFULL");
		    importMapProcessPreferenceDTO.setLastTaxRateRelFull(lastTaxRateRelFull);
		    String lastTaxRateDateUpdate = (String)map.get("LASTTAXRATEDATEUPDATE");
		    importMapProcessPreferenceDTO.setLastTaxRateDateUpdate(lastTaxRateDateUpdate);
		    String lastTaxRateRelUpdate = (String)map.get("LASTTAXRATERELUPDATE");
		    importMapProcessPreferenceDTO.setLastTaxRateRelUpdate(lastTaxRateRelUpdate);
		    String taxWarnRate = (String)map.get("TAXWARNRATE");
		    importMapProcessPreferenceDTO.setTaxWarnRate(taxWarnRate);
		    
		    // System - TaxCode Rules
		    String lastTaxCodeRulesDateFull = (String)map.get("LASTRULEDATEFULL");
		    importMapProcessPreferenceDTO.setLastTaxCodeRulesDateFull(lastTaxCodeRulesDateFull);
		    String lastTaxCodeRulesRelFull = (String)map.get("LASTRULERELFULL");
		    importMapProcessPreferenceDTO.setLastTaxCodeRulesRelFull(lastTaxCodeRulesRelFull);
		    String lastTaxCodeRulesDateUpdate = (String)map.get("LASTRULEDATEUPDATE");
		    importMapProcessPreferenceDTO.setLastTaxCodeRulesDateUpdate(lastTaxCodeRulesDateUpdate);
		    String lastTaxCodeRulesRelUpdate = (String)map.get("LASTRULERELUPDATE");
		    importMapProcessPreferenceDTO.setLastTaxCodeRulesRelUpdate(lastTaxCodeRulesRelUpdate);
		    
		    // System - Compliance
		    String defCCHTaxCatCode = (String)map.get("DEFCCHTAXCATCODE");
		    importMapProcessPreferenceDTO.setDefCCHTaxCatCode(defCCHTaxCatCode);
		    String defCCHGroupCode = (String)map.get("DEFCCHGROUPCODE");
		    importMapProcessPreferenceDTO.setDefCCHGroupCode(defCCHGroupCode);
		    String tpExtractFilePath = (String)map.get("TPEXTRACTEFILEPATH");
		    importMapProcessPreferenceDTO.setTpExtractFilePath(tpExtractFilePath);            
		    String tpExtractFilePrefix  = (String)map.get("TPEXTRACTPREFIX");
		    importMapProcessPreferenceDTO.setTpExtractFilePrefix(tpExtractFilePrefix);
		    String tpExtractExt  = (String)map.get("TPEXTRACTEXT");
		    importMapProcessPreferenceDTO.setTpExtractExt(tpExtractExt);

		    // Admin - Compliance
		    String tpExtractEnabled = (String)map.get("TPEXTRACTENABLED");	
		    if ("1".equalsIgnoreCase(tpExtractEnabled)) {
		    	importMapProcessPreferenceDTO.setTpExtractEnabled(true);
		    } else {
		    	importMapProcessPreferenceDTO.setTpExtractEnabled(false);
		    }
		    
		    // System - RatePoint
			String ratepointLatlongLookup = (String) map.get("LATLONGLOOKUP");
			if ("1".equalsIgnoreCase(ratepointLatlongLookup)) {
				importMapProcessPreferenceDTO.setRatepointLatlongLookup(true);
			} else {
				importMapProcessPreferenceDTO.setRatepointLatlongLookup(false);
			}
	
			//String ratepointDefaultJurisdiction = (String) map.get("DEFAULTJURISDICTION");
			//importMapProcessPreferenceDTO.setRatepointDefaultJurisdiction(ratepointDefaultJurisdiction);
	
			//String ratepointDefaultTaxCode = (String) map.get("DEFAULTTAXCODE");
			//importMapProcessPreferenceDTO.setRatepointDefaultTaxCode(ratepointDefaultTaxCode);
			

		}		
		return importMapProcessPreferenceDTO;
	}

	public Map<String,String> getFileDupMap(){ 
    	if(fileDupMap == null) {
    		fileDupMap = new HashMap<String,String>();
    		fileDupMap.put("0", "Full");
    		fileDupMap.put("1", "Partial");
    		fileDupMap.put("2", "None");
    	}      	
        return fileDupMap;
	}
	
	public Map<String,String> getDefaultPreferenceMap(){ 
    	logger.debug(" getDefaultPreferenceMap ");
    	if(defaultPreferenceMap == null) {
    		defaultPreferenceMap = new HashMap<String,String>();
    		defaultPreferenceMap.put("AUTOADDNEXUS", "Never");
    		defaultPreferenceMap.put("AUTOADDNEXUSNORIGHTS", "Never");
    	}
    	logger.debug("exit getDefaultPreferenceMap");       	
        return defaultPreferenceMap;
	}
	 
	  public void parseValuesToEditControls() {
	    	String doNotImportMarkers = getImportMapProcessPreferenceDTO().getDoNotImportMarkers();
	    	String replaceWithMarkers = getImportMapProcessPreferenceDTO().getReplaceWithMarkers();
	    	
	    	if(doNotImportMarkers==null){
	    		doNotImportMarkers = "";
	    	}
	    	if(replaceWithMarkers==null){
	    		replaceWithMarkers = "";
	    	}
	    	doNotImportCharBean.parseValuesToEditControls(doNotImportMarkers, replaceWithMarkers);
		}
		  
	  public DoNotImportCharBean getDoNotImportCharBean(){
			return doNotImportCharBean;
		}
	  
	public boolean getAddAction() {
		return currentAction.equals(ImportDefsAndSpecsAction.AddSpec) || currentAction.equals(ImportDefsAndSpecsAction.AddDef);
	}
	
	public boolean getDeleteAction() {
		return currentAction.equals(ImportDefsAndSpecsAction.DeleteSpec) || currentAction.equals(ImportDefsAndSpecsAction.DeleteDef);
	}

	public boolean getUpdateAction() {
		return currentAction.equals(ImportDefsAndSpecsAction.UpdateSpec) || currentAction.equals(ImportDefsAndSpecsAction.UpdateDef);
	}
	
	public boolean getReadOnlyAction() {
		return (currentAction.equals(ImportDefsAndSpecsAction.DeleteSpec) || currentAction.equals(ImportDefsAndSpecsAction.DeleteDef) ||
				currentAction.equals(ImportDefsAndSpecsAction.ViewSpec) || currentAction.equals(ImportDefsAndSpecsAction.ViewDef) ||
				currentAction.equals(ImportDefsAndSpecsAction.ViewConn) || currentAction.equals(ImportDefsAndSpecsAction.DeleteConn));
	}
	
	public boolean getIsAddDefaultAction() {
		return (defaultSetting && (currentAction.equals(ImportDefsAndSpecsAction.AddDef) || currentAction.equals(ImportDefsAndSpecsAction.AddSpec)));
	}
	
	public boolean getMapAction() {
		return currentAction.equals(ImportDefsAndSpecsAction.MapSpec) || currentAction.equals(ImportDefsAndSpecsAction.MapDef);
	}
	
	public boolean getIsSpecUpdatePcoAllow() {
		if(!getValidSpecSelection()){
			return false;
		}
		Option option = optionService.findByPK(new OptionCodePK("PCO", "ADMIN", "ADMIN"));
		String option_pco = (option == null || option.getValue() == null) ? "0" : option.getValue();
		boolean isPco = (option_pco!=null && option_pco.equals("1"))? true : false;
		
		if(!isPco && selectedImportSpec.getImportSpecType().equalsIgnoreCase("PCO")){
			return false;
		}		
		return true;
	}
	
	public boolean getIsDefUpdatePcoAllow() {
		if(!getValidDefSelection()){
			return false;
		}
		Option option = optionService.findByPK(new OptionCodePK("PCO", "ADMIN", "ADMIN"));
		String option_pco = (option == null || option.getValue() == null) ? "0" : option.getValue();
		boolean isPco = (option_pco!=null && option_pco.equals("1"))? true : false;
		
		if(!isPco && selectedImportDef.getImportDefinitionType().equalsIgnoreCase("PCO")){
			return false;
		}		
		return true;
	}
	
	public TaxCodeDetailService getTaxCodeDetailService() {
		return this.taxCodeDetailService;
	}
	public void setTaxCodeDetailService(TaxCodeDetailService taxCodeDetailService) {
		this.taxCodeDetailService = taxCodeDetailService;
		filterTaxCodeHandler.setTaxCodeDetailService(taxCodeDetailService);
		filterTaxCodeHandler.setCallback(this);
	}
	
	private void clear() {
		//definition:
		selectedImportDef = null;
		selectedImportDefOri = null;
		selectedDefIndex = -1;
		selectedImportSpecList = null;
		selectedSpecUsingDefIndex = -1;

		//connection:
		selectedConnectionIndex = -1;
		definitionsUsingConnection = null;
		selectedDefUsingConnIndex = -1;
		indexImportConnectionComponent = null;
			
		//spec:
		selectedImportSpec = null;
		selectedImportSpecOri = null;
		selectedSpecIndex = -1;
		userList = null;
		userCodeList =null;
	}

	public List<SelectItem> getScreenTypeMenuItems() {
		if (screenTypeMenuItems == null) {
			screenTypeMenuItems = new ArrayList<SelectItem>();
			for (ScreenType st : ScreenType.values()) {
				screenTypeMenuItems.add(new SelectItem(st.toString(), st.toString()));
			}
		}	
		
		return screenTypeMenuItems;
	}

	private void updateImportDefList() {
		//importDefList = importDefAndSpecsService.getAllImportDefinition();
		
		ImportDefinition importDefinition = new ImportDefinition();	
		importDefinition.setImportDefinitionCode(defSpecCode);
		importDefinition.setImportDefinitionType(importDefSpecType);	
		importDefinition.setDescription(defDescriptionText);
		importDefinition.setComments(defCommentsText);
		importDefinitionModel.setExampleInstance(importDefinition);	
	
		clear();
	}
	public boolean isDisplayButton() {
		return displayButton;
	}

	public void setDisplayButton(boolean displayButton) {
		this.displayButton = displayButton;
	}
	 public void updateButtonAction(ValueChangeEvent event) {
	    	String message = "Data has been changed on this page.  Please Update if you want to save changes.";
	    	this.setErrorMessage(message);
	    	this.setDisplayButton(true);
	    }
	 public String getErrorMessage() {
			return errorMessage;
		}

		public void setErrorMessage(String errorMessage) {
			this.errorMessage = errorMessage;
		}
	/**
	 * @return the listCodesList
	 */
	public List<ImportDefDTO> getImportDefList() {
		if (importDefList == null) {
			updateImportDefList();
		}

		return importDefList;
	}
	
	private void selectedDefRowChanged(ActionEvent e) throws AbortProcessingException { 
		UIComponent uiComponent = e.getComponent().getParent();
		if (!(uiComponent instanceof UIDataAdaptor)) {
			logger.info("Invalid class to event listener: " + uiComponent.getClass().getName());
			
			return;
		}

		UIDataAdaptor table = (UIDataAdaptor) uiComponent;
		ImportDefinition importDef = (ImportDefinition) table.getRowData();
		
		selectedImportDef = new ImportDefDTO();
	  	BeanUtils.copyProperties(importDef, selectedImportDef);
	  	
	  	selectedImportDefOri = new ImportDefDTO();
	  	BeanUtils.copyProperties(importDef, selectedImportDefOri);
	  	
		this.selectedDefIndex = table.getRowIndex();
		
		selectedImportSpecList = importDefAndSpecsService.getImportSpecByDefCode(selectedImportDef.getImportDefinitionCode());
		
		//Reset index for Spec Using Definition
		selectedSpecUsingDefIndex = -1;
	}

	public void selectedDefUsingConnRowChanged(ActionEvent e) throws AbortProcessingException { 
		UIComponent uiComponent = e.getComponent().getParent();
		if (!(uiComponent instanceof UIDataAdaptor)) {
			logger.info("Invalid class to event listener: " + uiComponent.getClass().getName());
			
			return;
		}

		UIDataAdaptor table = (UIDataAdaptor) uiComponent;
		ImportDefDTO importDef = (ImportDefDTO) table.getRowData();
		
		selectedImportDef = new ImportDefDTO();
		selectedImportDefOri = new ImportDefDTO();
		BeanUtils.copyProperties(importDef, selectedImportDef);
	  	BeanUtils.copyProperties(importDef, selectedImportDefOri);

		this.selectedDefUsingConnIndex = table.getRowIndex();
	}
	
	public void selectedSpecUsingDefRowChanged(ActionEvent e) throws AbortProcessingException { 
		UIComponent uiComponent = e.getComponent().getParent();
		if (!(uiComponent instanceof UIDataAdaptor)) {
			logger.info("Invalid class to event listener: " + uiComponent.getClass().getName());
			
			return;
		}

		UIDataAdaptor table = (UIDataAdaptor) uiComponent;
		ImportSpecDTO importSpec = (ImportSpecDTO) table.getRowData();
		
		selectedImportSpec = new ImportSpecDTO();
		BeanUtils.copyProperties(importSpec, selectedImportSpec);
		
		selectedImportSpecOri = new ImportSpecDTO();
	  	BeanUtils.copyProperties(importSpec, selectedImportSpecOri);

		this.selectedSpecUsingDefIndex = table.getRowIndex();
		
		userList = this.importDefAndSpecsService.getImportSpecUserBySpec(selectedImportSpec);
		userCodeList = new ArrayList<String>();
		for (UserDTO u : userList) {
			userCodeList.add(u.getUserCode());
		}
	}
	
	public Boolean getDisplayUpdateDefUsingConn() {
		return (selectedDefUsingConnIndex != -1);
	}
	
	public Boolean getDisplayUpdateSpecUsingDef() {
		return (selectedSpecUsingDefIndex != -1);
	}
	
	
	public boolean defaultSetting = false;
	
	public Boolean getDefaultSetting() {
		return defaultSetting;
	}
	
	
	///displayDefUsingConnAddAction
	//xxxxxxxxxxxxxxxxxxxx
	public String displayDefUsingConnAddAction(){
		defaultSetting = true;
		selectedImportDef = new ImportDefDTO();
		
		if(indexImportConnectionComponent!=null){
			selectedImportDef.setImportFileTypeCode("DB");
			selectedImportDef.setImportConnectionCode(indexImportConnectionComponent.getImportConnectionCode());
		}
		
		//selectedImportDef = selectedImportDefOri;
		sampleFileName = "";
		sampleFileNameOri = "";
		
		initImportFileType();

		currentAction = ImportDefsAndSpecsAction.AddDef;
		this.uploadFile = new HtmlInputFileUpload();
		return "import_def_add";
	}
	
	public String displayDefUsingConnCopyAddAction() {	
		defaultSetting = true;
		selectedImportDef = new ImportDefDTO();
		BeanUtils.copyProperties(selectedImportDefOri, selectedImportDef);

		sampleFileName = "";
		sampleFileNameOri = "";
		selectedImportDef.setImportDefinitionCode("");
		selectedImportDef.setUpdateUserId("");
		selectedImportDef.setUpdateTimestamp(null);
		
		initImportFileType();

		currentAction = ImportDefsAndSpecsAction.AddDef;
		this.uploadFile = new HtmlInputFileUpload();
		return "import_def_add";
	}
	
	public String displayDefUsingConnUpdateAction(){	
		//selectedImportDef = selectedImportDefOri;
		selectedImportDef = new ImportDefDTO();
		BeanUtils.copyProperties(selectedImportDefOri, selectedImportDef);
		
		sampleFileName = "";
		sampleFileNameOri = "";
		
		initImportFileType();

		currentAction = ImportDefsAndSpecsAction.UpdateDef;
		this.uploadFile = new HtmlInputFileUpload();
		return "import_def_update";
	}
	
	public String displayDefUsingConnViewAction() {
		//selectedImportDef = selectedImportDefOri;
		selectedImportDef = new ImportDefDTO();
		BeanUtils.copyProperties(selectedImportDefOri, selectedImportDef);
		
		sampleFileName = "";
		sampleFileNameOri = "";
		
		initImportFileType();

		currentAction = ImportDefsAndSpecsAction.ViewDef;
		this.uploadFile = new HtmlInputFileUpload();
		return "import_def_view"; 
	}
	
	public String displayDefUsingConnDeleteAction(){	
		//selectedImportDef = selectedImportDefOri;
		selectedImportDef = new ImportDefDTO();
		BeanUtils.copyProperties(selectedImportDefOri, selectedImportDef);
		
		sampleFileName = "";
		sampleFileNameOri = "";
		
		initImportFileType();

		currentAction = ImportDefsAndSpecsAction.DeleteDef;
		this.uploadFile = new HtmlInputFileUpload();
		return "import_def_update";
	}
	
	public String displayDefUsingConnMapAction(){
		selectedImportDef = new ImportDefDTO();
		BeanUtils.copyProperties(selectedImportDefOri, selectedImportDef);
		
		if(selectedImportDef.getImportFileTypeCode().equals("FD")){
			return importDefinitionMappingBean.displayMapFDAction(selectedImportDef, this);
		}
		else if(selectedImportDef.getImportFileTypeCode().equals("DB")){
			return importDefinitionMappingBean.displayMapDBAction(selectedImportDef, this);
		}
		return null;
	}
	
	public void selectedConnectionRowChanged(ActionEvent e) throws AbortProcessingException {	
		UIComponent uiComponent = e.getComponent().getParent();
		if (!(uiComponent instanceof UIDataAdaptor)) {
			logger.info("Invalid class to event listener: " + uiComponent.getClass().getName());
			
			return;
		}

		UIDataAdaptor table = (UIDataAdaptor) uiComponent;
		
		//0003314
		ImportConnection iConn  = (ImportConnection) table.getRowData();
		this.selectedConnectionIndex = table.getRowIndex(); 
		indexImportConnectionComponent = new ImportConnection(); 
		BeanUtils.copyProperties(iConn, indexImportConnectionComponent);
		selectedImportConnectionComponent = null;  
		
		definitionsUsingConnection = this.importDefAndSpecsService.getDefinitionsByConnection(iConn.getImportConnectionCode());

		//Reset index for Definitions Using Connection
		selectedDefUsingConnIndex = -1;
	}
	
	public String displayImportDefMapAction(){
		selectedImportDef = new ImportDefDTO();
		BeanUtils.copyProperties(selectedImportDefOri, selectedImportDef);
		
		if(selectedImportDef.getImportFileTypeCode().equals("FD")){
			return importDefinitionMappingBean.displayMapFDAction(selectedImportDef, this);
		}
		else if(selectedImportDef.getImportFileTypeCode().equals("DB")){
			return importDefinitionMappingBean.displayMapDBAction(selectedImportDef, this);
		}
		return null;
	}
	
	public TaxCodeCodeHandler getFilterTaxCodeHandler() {
		return filterTaxCodeHandler;
	}
	

	public Boolean getCanDeleteDef() {
		return (selectedDefIndex != -1) && (selectedImportSpecList.size() == 0);
	}
	
	//bug 5063
	public Boolean getCanMapDef() {
		if((selectedDefIndex == -1) || (selectedImportDefOri == null)){
			return false;
		}
		
		if(selectedImportDefOri.getImportFileTypeCode()!=null && 
		   selectedImportDefOri.getImportFileTypeCode().equalsIgnoreCase("DB") && 
			(selectedImportDefOri.getDbTableName()!=null) && selectedImportDefOri.getDbTableName().trim().length()>0){
			return true;
		}
				
				
		if(selectedImportDefOri.getImportFileTypeCode()!=null &&
		   selectedImportDefOri.getImportFileTypeCode().equalsIgnoreCase("FD") &&	
		    (selectedImportDefOri.getSampleFileName()!=null) && selectedImportDefOri.getSampleFileName().trim().length()>0){
			return true;
		}
				
		return false;
	}
	
	public void moveEditControlToValuesListener(ActionEvent event){
    	String strDNIValues = doNotImportCharBean.getDNIValues();
    	String strRWValues = doNotImportCharBean.getRWValues();
 
    	String returnDNIValues = "";
    	String returnRWValues = "";
    	
    	String strTempDNI = "";
    	String strTempRW = "";

    	boolean bError = false;
    	for(int i=1; i<=50; i++){
    		
    		int startIndex = i;
    		strTempDNI = strDNIValues.substring(startIndex-1, startIndex);
    		strTempRW = strRWValues.substring((startIndex-1)*2, startIndex*2);
    		
    		if(strTempDNI.trim().length()==0 && strTempRW.trim().length()==0){	
    			//fine
    		}
    		else if(strTempRW.trim().length()==0){
    			//0000005: 
    			//Add: It is valid to have a DNI character without a RW character.
				returnDNIValues = returnDNIValues + strTempDNI.trim();
				returnRWValues = returnRWValues + "  ";	
    		}
    		else if(strTempDNI.trim().length()==0){
    			bError = true;
    			JsfHelper.addError("doNotImportReplaceWithForm:msg",i + " - Invalid \"Do Not Import\" value");
    		}
    		else{
    			String tempStr = strTempRW.trim();
    			if ((tempStr.length() == 2) && !(tempStr.substring(0,1).equalsIgnoreCase("~"))) {
    				bError = true;
    				JsfHelper.addError("doNotImportReplaceWithForm:msg",i + " - Invalid \"Replace With\" value");
    			}
    			else if ((tempStr.length() == 2) && (!REPLACE_WITH_MARKERS.contains(tempStr))) {  		
    				bError = true;
    				JsfHelper.addError("doNotImportReplaceWithForm:msg",i + " - Invalid Special \"Replace With\" value");
    			}
    			else{
    				//Valid character
    				returnDNIValues = returnDNIValues + strTempDNI;
    		    	returnRWValues = returnRWValues + strTempRW;
    			}
    		}
    	}
    	
    	if(bError){
    		controlToValuesSucceed = false;
    		return;
    	}
    	
    	controlToValuesSucceed = true;
    	getImportMapProcessPreferenceDTO().setDoNotImportMarkers(returnDNIValues);
    	getImportMapProcessPreferenceDTO().setReplaceWithMarkers(returnRWValues);
    	
    	updateButtonAction(null);
    }
	 public boolean getControlToValuesSucceed() {
			return controlToValuesSucceed;
		}	

	public void initImportFileType(){
		flatDelCharEntered = "";
		flatDelCharSelected = "OTHER";
		
		String dbChar = selectedImportDef.getFlatDelChar();		
		if(this.getIsFlatDelimitedType()){
			if(dbChar!=null && dbChar.length()>0){
				if(dbChar.equalsIgnoreCase("~t") || dbChar.equalsIgnoreCase("~n") ||
						dbChar.equalsIgnoreCase("~f") || dbChar.equalsIgnoreCase("~b") ||
						dbChar.equalsIgnoreCase("�")){
					flatDelCharSelected = dbChar;
					flatDelCharEntered = "";
				}
				else{
					flatDelCharSelected = "OTHER";
					flatDelCharEntered = dbChar;
				}
			}
			else{
				flatDelCharSelected = "OTHER";
				flatDelCharEntered = "";
			}	
			
			return;
		}
		
		if(this.getIsDatabaseType()){
			importConnectionItems = null;
			getImportConnectionItems();
			
			importTableItems = null;
			getImportTableItems() ;
			
			return;
		}
		
		return;
	}
	
	public String displayImportDefUpdateAction(){	
		//selectedImportDef = selectedImportDefOri;
		selectedImportDef = new ImportDefDTO();
		BeanUtils.copyProperties(selectedImportDefOri, selectedImportDef);
		
		sampleFileName = selectedImportDef.getSampleFileName();
		sampleFileNameOri = selectedImportDef.getSampleFileName();
		
		initImportFileType();

		currentAction = ImportDefsAndSpecsAction.UpdateDef;
		this.uploadFile = new HtmlInputFileUpload();
		importDefTypeItems = null;
		return "import_def_update";
	}
	
	public String displayImportDefDeleteAction() {
		//selectedImportDef = selectedImportDefOri;
		selectedImportDef = new ImportDefDTO();
		BeanUtils.copyProperties(selectedImportDefOri, selectedImportDef);
		
		sampleFileName = selectedImportDef.getSampleFileName();
		sampleFileNameOri = selectedImportDef.getSampleFileName();
		
		initImportFileType();
		
		currentAction = ImportDefsAndSpecsAction.DeleteDef;
		this.uploadFile = new HtmlInputFileUpload();
		importDefTypeItems = null;
		return "import_def_delete"; 
	}
	
	public String displayImportDefViewAction() {
		//selectedImportDef = selectedImportDefOri;
		selectedImportDef = new ImportDefDTO();
		BeanUtils.copyProperties(selectedImportDefOri, selectedImportDef);
		
		sampleFileName = selectedImportDef.getSampleFileName();
		sampleFileNameOri = selectedImportDef.getSampleFileName();
		
		initImportFileType();
		
		currentAction = ImportDefsAndSpecsAction.ViewDef;
		this.uploadFile = new HtmlInputFileUpload();
		importDefTypeItems = null;
		return "import_def_view"; 
	}
	
	public String displayImportDefAddAction(){
		defaultSetting = false;
		this.sampleFileName = "";
		this.sampleFileNameOri = "";
		this.uploadFileName="";
		
		flatDelCharEntered = "";
		flatDelCharSelected = "OTHER";
		
		currentAction = ImportDefsAndSpecsAction.AddDef;
	 	selectedImportDef = new ImportDefDTO();
	 	
	 	//PP-164
        Option option = optionService.findByPK(new OptionCodePK("PCO", "ADMIN", "ADMIN"));
        String option_pco = (option == null || option.getValue() == null) ? "0" : option.getValue();
        boolean isPco = (option_pco!=null && option_pco.equals("1"))? true : false;   
        boolean option_ip = loginservice.getHasPurchaseLicense();
        boolean option_ips = loginservice.getHasBillingLicense();   
        if(isPco){
        	selectedImportDef.setImportDefinitionType("PCO");
        }
        else if(option_ip){
        	selectedImportDef.setImportDefinitionType("IP");
        }
        else if(option_ips){
        	selectedImportDef.setImportDefinitionType("IPS");
        }
	 	
	 	this.uploadFile = new HtmlInputFileUpload();
	 	
	 	importConnectionItems = null;
		importTableItems = null;
		importDefTypeItems = null;
		return "import_def_add";
	}
	
	public String displayImportDefCopyAddAction(){
		defaultSetting = false;
		selectedImportDef = new ImportDefDTO();
		BeanUtils.copyProperties(selectedImportDefOri, selectedImportDef);
		
		sampleFileName = selectedImportDef.getSampleFileName();
		sampleFileNameOri = selectedImportDef.getSampleFileName();
		selectedImportDef.setImportDefinitionCode("");
		selectedImportDef.setUpdateUserId("");
		selectedImportDef.setUpdateTimestamp(null);
		
		initImportFileType();

		currentAction = ImportDefsAndSpecsAction.copyAddDef;
		this.uploadFile = new HtmlInputFileUpload();
		importDefTypeItems = null;
		
		return "import_def_add";
	}
	
	public String retrieveListCodes() {
		
		if (this.uploadFile.getUploadedFile() != null) {
			sampleFileName = this.uploadFile.getUploadedFile().getName();
  		}
		
		return null;
	}
	
	public void processValueChange(ValueChangeEvent event) throws AbortProcessingException {
        // do something with uploadedFile
        if (this.uploadFile.getUploadedFile() != null) {
			sampleFileName = this.uploadFile.getUploadedFile().getName();
  		}
	}
	
	public void fileChangeActionListener(ActionEvent event) {	
		if(uploadFile!=null){
			UIInput uii = (UIInput)event.getComponent().findComponent("importDefAddForm:uploadFileControl");
			if (uii != null) {
				String s = (String)uii.getSubmittedValue();
				this.sampleFileName = s;
			}	
		}
			
		if (this.uploadFile.getUploadedFile() != null) {
			sampleFileName = this.uploadFile.getUploadedFile().getName();
  		}	
    }
	
	//--------------- File Upload logic ---------------------//
	// required by super class, but nothing to do for us
	public void processHeader(BufferedReader reader) throws IOException {
	}
  private boolean fileUpload() {
  	try {
  		if (this.uploadFile.getUploadedFile() != null) {
  			super.upload(this.uploadFile.getUploadedFile().getName(), this.uploadFile.getUploadedFile().getInputStream());
  		}
  		return true;
  	} catch (Exception e) {
  		JsfHelper.addError(e.getMessage());
  	}
  	return false;
  }

  	public boolean validateImportFileType(){
  		boolean delimiterError = false;
  		
  		if(selectedImportDef.getImportDefinitionCode()==null || selectedImportDef.getImportDefinitionCode().length()==0){
  			FacesMessage message = new FacesMessage("Definition Code is required. ");
  			message.setSeverity(FacesMessage.SEVERITY_ERROR);
  			FacesContext.getCurrentInstance().addMessage(null, message);
  			delimiterError = true;
  		}
  		
  		if(selectedImportDef.getDescription()==null || selectedImportDef.getDescription().length()==0){
  			FacesMessage message = new FacesMessage("Description is required. ");
  			message.setSeverity(FacesMessage.SEVERITY_ERROR);
  			FacesContext.getCurrentInstance().addMessage(null, message);
  			delimiterError = true;
  		}
  		
  		if(selectedImportDef.getImportFileTypeCode()==null || selectedImportDef.getImportFileTypeCode().length()==0){
  			FacesMessage message = new FacesMessage("Import File Type is required. ");
  			message.setSeverity(FacesMessage.SEVERITY_ERROR);
  			FacesContext.getCurrentInstance().addMessage(null, message);
  			delimiterError = true;
  		}
  		
  		if(delimiterError){
			return true;
		}
  		
  		if(getIsFlatDelimitedType()) {
  			String delimiterChar = "";	
  	  		if(!flatDelCharSelected.equalsIgnoreCase("OTHER")){//Selected from radio buttons
  	  			delimiterChar = flatDelCharSelected;
  			}
  			else{//user entered.
  				delimiterChar = flatDelCharEntered.trim();
  			}

  			delimiterError = false;
  			if(delimiterChar==null || delimiterChar.length()==0){
  				delimiterError = true;
  			}
  			else{
  				delimiterChar = delimiterChar.trim();
  				if(delimiterChar.length()==2){
  					if(!(delimiterChar.equalsIgnoreCase("~t") || delimiterChar.equalsIgnoreCase("~n") || 
  							delimiterChar.equalsIgnoreCase("~f") || delimiterChar.equalsIgnoreCase("~b"))){
  						delimiterError = true;
  					}			
  				}
  				else if(delimiterChar.length()>2){
  					delimiterError = true;
  				}
  			}
  			if(delimiterError){
  				FacesMessage message = new FacesMessage("The delimiter must be either a single character or a two-character special code that starts with a tilde (~). The valid special values are: ~t : tab, ~n : new line, ~f : form feed, ~b :backspace ");
  				message.setSeverity(FacesMessage.SEVERITY_ERROR);
  				FacesContext.getCurrentInstance().addMessage(null, message);
  			}
  			else{
  				selectedImportDef.setFlatDelChar(delimiterChar);		
  				selectedImportDef.setImportConnectionCode("");
  				selectedImportDef.setDbTableName("");
  			}
  			
  			return delimiterError;		
  		}
  		
  		if(getIsDatabaseType()){
  			delimiterError = false;
  			if(selectedImportDef.getImportConnectionCode()==null || selectedImportDef.getImportConnectionCode().length()==0){
  				FacesMessage message = new FacesMessage("Connection is required. ");
  				message.setSeverity(FacesMessage.SEVERITY_ERROR);
  				FacesContext.getCurrentInstance().addMessage(null, message);
  				delimiterError = true;
  			}
  			if(selectedImportDef.getDbTableName()==null || selectedImportDef.getDbTableName().length()==0){
  				FacesMessage message = new FacesMessage("Table Name is required. ");
  				message.setSeverity(FacesMessage.SEVERITY_ERROR);
  				FacesContext.getCurrentInstance().addMessage(null, message);
  				delimiterError = true;
  			}
  			
  			if(!delimiterError){
  				selectedImportDef.setFlatDelChar("");
  			}
  			
  			return delimiterError;	
  		}
  		
  		return true;
  	}
  	
	public String importDefOkAction() {
		switch (currentAction) {
			case AddDef:
			case copyAddDef:
				if (importDefAndSpecsService.find(selectedImportDef.getImportDefinitionCode()) != null) {
					JsfHelper.addError(selectedImportDef.getImportDefinitionCode() + " is already defined");
					return null;
				}
				
				if(validateImportFileType()){//Validation failed.
					return null;
				}
				
				if(selectedImportDef.getImportFileTypeCode().equalsIgnoreCase("FD")){
					if(currentAction.equals(ImportDefsAndSpecsAction.copyAddDef)) {
						selectedImportDef.setSampleFileName(selectedImportDef.getSampleFileName());
					}
					else if (fileUpload()) {
						selectedImportDef.setSampleFileName(this.uploadFileName);
					}
					selectedImportDef.setImportFileTypeCode("FD");
					importDefAndSpecsService.addImportDefRecord(selectedImportDef);
				}
				else {
					importDefAndSpecsService.addImportDefRecord(selectedImportDef);
				}
				if(currentAction.equals(ImportDefsAndSpecsAction.copyAddDef) && selectedImportDef.getCopyMapFlag()){
					
					for (ImportDefinitionDetail detail : 	importDefinitionService.getAllHeaders(selectedImportDefOri.getImportDefinitionCode())) {
						ImportDefinitionDetail copyAddDetail =  new ImportDefinitionDetail();
						copyAddDetail.setImportDefinitionCode(selectedImportDef.getImportDefinitionCode());
						copyAddDetail.setTransDtlColumnName(detail.getTransDtlColumnName());
						copyAddDetail.setImportColumnName(detail.getImportColumnName());
						copyAddDetail.setSelectClause(detail.getSelectClause());
						copyAddDetail.setTableName(detail.getTableName());
						importDefinitionService.addImportDefinitionDetail(copyAddDetail); 
					}
				}

				break;
				
			case DeleteDef:
				importDefAndSpecsService.delete(selectedImportDef.getImportDefinitionCode());
				break;
				
			case UpdateDef:
				if(validateImportFileType()){//Validation failed.
					return null;
				}
				
				if(selectedImportDef.getImportFileTypeCode().equalsIgnoreCase("FD")){
					if (fileUpload()) {
						// if user does not provide name, then do not update.
						if (this.uploadFileName != null) {
							selectedImportDef.setSampleFileName(this.uploadFileName);
						}
						importDefAndSpecsService.update(selectedImportDef);
					}
				}
				else {
					importDefAndSpecsService.update(selectedImportDef);
				}
				break;
		}
		
		importDefinitionMenu = null;
		
		updateImportDefList();
		return "import_def_main";
	}

	public boolean validateConnProperties(){
  		boolean verifyError = false;
  		
  		verifyConnProperties();		
  		if(!(verificationErrorMessage==null || verificationErrorMessage.length()==0) && !(verificationErrorMessage.equalsIgnoreCase("Connection succeeded"))){
  			verifyError = true;
  		}

  		return verifyError;
  	}
	
	public String importConnOkAction() {
		if (selectedImportConnectionComponent==null) {
			return "import_def_main";
		}		
		
		
		//JsfHelper.addError("You can't save me!");
		switch (currentAction) {
		case AddConn:
			System.out.println("AddConn");
			if (selectedImportConnectionComponent.getImportConnectionCode() == null 
				|| "".equalsIgnoreCase(selectedImportConnectionComponent.getImportConnectionCode())) {
				JsfHelper.addError("Connection Code is required!");
				return null;
			}

			if (selectedImportConnectionComponent.getImportConnectionType() == null
				|| "".equalsIgnoreCase(selectedImportConnectionComponent.getImportConnectionType())) {
				JsfHelper.addError("Connection Type is required!");
				return null;				
			}
			
			if(validateConnProperties()){//Validation failed.
				return null;
			}
			
			importDefAndSpecsService.addConnection(selectedImportConnectionComponent);
			indexImportConnectionComponent = null;
			selectedImportConnectionComponent = null;
			selectedConnectionIndex = -1;
			break;

/*		case CopyAddConn:
			break;*/
		case DeleteConn:
			importDefAndSpecsService.deleteConnection(selectedImportConnectionComponent.getImportConnectionCode());		
			indexImportConnectionComponent = null;
			selectedImportConnectionComponent = null;
			selectedConnectionIndex = -1;
			break;
		case UpdateConn:
			System.out.println("UpdateConn");
			if (selectedImportConnectionComponent.getImportConnectionCode() == null 
					|| "".equalsIgnoreCase(selectedImportConnectionComponent.getImportConnectionCode())) {
					JsfHelper.addError("Connection Code is required!");
					return null;
				}

				if (selectedImportConnectionComponent.getImportConnectionType() == null
					|| "".equalsIgnoreCase(selectedImportConnectionComponent.getImportConnectionType())) {
					JsfHelper.addError("Connection Type is required!");
					return null;				
				}
				if(validateConnProperties()){//Validation failed.
					return null;
				}
				
				importDefAndSpecsService.updateConnection(selectedImportConnectionComponent);
				indexImportConnectionComponent = null;
				selectedImportConnectionComponent = null;
				selectedConnectionIndex = -1;
			break;
		case ViewConn:
			break;
			
		}
		
		ImportConnection importConnection = new ImportConnection();
		importConnection.setImportConnectionType(connectionType);
		importConnection.setImportConnectionCode(defSpecCode);
		importConnection.setDescription(defDescriptionText);
		importConnection.setComments(defCommentsText);
		importConnectionModel.setExampleInstance(importConnection);
		return "import_def_main";
	}
	
	public String importConnCancelAction() {
		//selectedImportConnectionComponent = null;
		//selectedConnectionIndex = -1;
		return "import_def_main";
	}
	
	public void setScreenType(String screenType) {
		try {
			this.screenType = ScreenType.valueOf(screenType);
		} catch (IllegalArgumentException e) {
			logger.warn("Invalid screen type: " + screenType);
		} catch (NullPointerException e) {
			logger.warn("Null screen type");
		}
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	///////////////////////////  Methods related to Import Specs goes here ////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	private void selectedSpecRowChanged(ActionEvent e) throws AbortProcessingException { 
		UIComponent uiComponent = e.getComponent().getParent();
		if (!(uiComponent instanceof UIDataAdaptor)) {
			logger.info("Invalid class to event listener: " + uiComponent.getClass().getName());
			
			return;
		}

		UIDataAdaptor table = (UIDataAdaptor) uiComponent;
		
		//0003314
		ImportSpec iSpec  = (ImportSpec) table.getRowData();
		this.selectedSpecIndex = table.getRowIndex();
		
		selectedImportSpecOri = new ImportSpecDTO();
		BeanUtils.copyProperties(iSpec, selectedImportSpecOri);

		this.selectedImportSpec = new ImportSpecDTO();
    	BeanUtils.copyProperties(selectedImportSpecOri,selectedImportSpec);
		
		userList = this.importDefAndSpecsService.getImportSpecUserBySpec(selectedImportSpec);
		userCodeList = new ArrayList<String>();
		for (UserDTO u : userList) {
			userCodeList.add(u.getUserCode());
		}
	}

	private void updateImportSpecList() {
		//importSpecList = importDefAndSpecsService.getAllImportSpec();
		
		ImportSpec importSpec = new ImportSpec();	
		ImportSpecPK ImportSpecPK = new ImportSpecPK();
		ImportSpecPK.setImportSpecCode(defSpecCode);
		ImportSpecPK.setImportSpecType(importDefSpecType);
		importSpec.setImportSpecPK(ImportSpecPK);
		importSpec.setDescription(defDescriptionText);
		importSpec.setComments(defCommentsText);
		importSpecModel.setExampleInstance(importSpec);	
	
		clear();
	}
	
	public List<ImportSpecDTO> getImportSpecList() {
		if (importSpecList == null) {
			updateImportSpecList();
		}
		return importSpecList;
	}

	/**
	 * Following method will return the Import Spec Type with code and 
	 * discription to populate the drop down
	 */
	public HtmlSelectOneMenu getImportSpecTypeMenu() {				
		if (importSpecTypeMenu == null){			
			importSpecTypeMenu = new HtmlSelectOneMenu();			
			final Collection<SelectItem> batchTypeList = new ArrayList<SelectItem>();		
			batchTypeList.add(new SelectItem("",""));		
			//batchTypeList.add(new SelectItem("*ALL","*ALL"));		
			Map<String, ListCodes> listCodeList = 	cacheManager.getListCodesMapByType("BATCHTYPE");
			for (ListCodes listcodes : listCodeList.values()) {			
				if(listcodes != null && listcodes.getCodeTypeCode()!=null && listcodes.getDescription()!=null){				
					batchTypeList.add(new SelectItem(listcodes.getCodeCode(),listcodes.getCodeCode()+"-"+listcodes.getDescription()));
				}
			}
			final UISelectItems items = new UISelectItems();
			items.setValue(batchTypeList);		
			importSpecTypeMenu.getChildren().add(items);
		}	
		
		return importSpecTypeMenu;
	}

	public HtmlSelectOneMenu getImportDefinitionMenu() {
		
		if (importDefinitionMenu == null){			
			importDefinitionMenu = new HtmlSelectOneMenu();			
			final Collection<SelectItem> importDefDropdownList = new ArrayList<SelectItem>();		
			importDefDropdownList.add(new SelectItem("",""));
			List<ImportDefDTO> objList = importDefAndSpecsService.getAllImportDefinition();			
			
			for (ImportDefDTO ojImportDef : objList) {			
				if(ojImportDef != null && ojImportDef.getImportDefinitionCode()!=null ){				
					importDefDropdownList.add(new SelectItem(ojImportDef.getImportDefinitionCode(),ojImportDef.getImportDefinitionCode()));
				}
			}
			final UISelectItems items = new UISelectItems();
			items.setValue(importDefDropdownList);		
			importDefinitionMenu.getChildren().add(items); 
		}	
		
		return importDefinitionMenu;
	}

	////xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
	public String displaySpecUsingDefUpdateAction(){	
		//selectedImportDef = selectedImportDefOri;
		selectedImportSpec = new ImportSpecDTO();
		BeanUtils.copyProperties(selectedImportSpecOri, selectedImportSpec);
		setSelectedSpecFileTypeCode(selectedImportSpec.getImportDefinitionCode());

		currentAction = ImportDefsAndSpecsAction.UpdateSpec;
		copyBeanforSpecRevertUpdate();
		setInputValue(importDefinitionMenu, selectedImportSpec.getImportDefinitionCode());
		verificationErrorMessage = "";
		return "import_spec_update";
	}
	
	public String displaySpecUsingDefDeleteAction() {
		selectedImportSpec = new ImportSpecDTO();
		BeanUtils.copyProperties(selectedImportSpecOri, selectedImportSpec);
		setSelectedSpecFileTypeCode(selectedImportSpec.getImportDefinitionCode());
		
		currentAction = ImportDefsAndSpecsAction.DeleteSpec;
		setInputValue(importDefinitionMenu, selectedImportSpec.getImportDefinitionCode());
		verificationErrorMessage = "";
	    return "import_spec_delete"; 
	}	
	
	
	public String displaySpecUsingDefAddAction(){
		defaultSetting = true;
		selectedImportSpec = new ImportSpecDTO();
		selectedImportSpec.setImportDefinitionCode(selectedImportDef.getImportDefinitionCode());	
		selectedImportSpec.setImportSpecType(selectedImportDef.getImportDefinitionType());
		selectedImportSpec.setFilesizedupres("2");
		selectedImportSpec.setFilenamedupres("2");
		setSelectedSpecFileTypeCode(selectedImportDef.getImportDefinitionCode());//default of selected definition

		currentAction = ImportDefsAndSpecsAction.AddSpec;
		copyBeanforSpecRevertAdd();
		setInputValue(importDefinitionMenu, "");
		verificationErrorMessage = "";
		return "import_spec_add";
	}
	
	public String displaySpecUsingDefCopyAddAction(){
		defaultSetting = true;
		selectedImportSpec = new ImportSpecDTO();
		BeanUtils.copyProperties(selectedImportSpecOri, selectedImportSpec);
		setSelectedSpecFileTypeCode(selectedImportSpec.getImportDefinitionCode());
		selectedImportSpec.setImportSpecCode("");
		
		currentAction = ImportDefsAndSpecsAction.copyAddSpec;
		copyBeanforSpecRevertAdd();
		setInputValue(importDefinitionMenu, "");
		verificationErrorMessage = "";
		return "import_spec_add";
	}
	
	public String displaySpecUsingDefMapAction() {
		selectedImportSpec = new ImportSpecDTO();
		BeanUtils.copyProperties(selectedImportSpecOri, selectedImportSpec);
		
		currentAction = ImportDefsAndSpecsAction.MapSpec;
		setInputValue(importDefinitionMenu, selectedImportSpec.getImportDefinitionCode());
		verificationErrorMessage = "";
		return "import_spec_map"; 
	}
	
	public String displaySpecUsingDefViewAction(){
		selectedImportSpec = new ImportSpecDTO();
		BeanUtils.copyProperties(selectedImportSpecOri, selectedImportSpec);
		setSelectedSpecFileTypeCode(selectedImportSpec.getImportDefinitionCode());
		
		currentAction = ImportDefsAndSpecsAction.ViewSpec;
		copyBeanforSpecRevertAdd();
		setInputValue(importDefinitionMenu, "");
		verificationErrorMessage = "";
		return "import_spec_add";
	}
	

	private String selectedSpecFileTypeCode = "";
	
	public String getSelectedSpecFileTypeCode() {
		return selectedSpecFileTypeCode;			
	}
	
	public void setSelectedSpecFileTypeCode(String definitionCode) {
		if(definitionCode==null || definitionCode.length()==0){
			selectedSpecFileTypeCode = "";
		}
		else{
			ImportDefinition importDefinition = importDefAndSpecsService.find(definitionCode);
			if(importDefinition!=null && importDefinition.getImportFileTypeCode()!=null){
				selectedSpecFileTypeCode = importDefinition.getImportFileTypeCode();// "DB" or "FD"
			}
			else{
				selectedSpecFileTypeCode = "";
			}
		}		
	}
	
	public void onImportDefinitionChange(ValueChangeEvent event) {
		
		String importDefinitionCode = "";
		try {
			importDefinitionCode = (String) event.getNewValue();
		} catch (Exception e) {
			logger.warn("Failed to process new screen type: " + e);
		}
		
		setSelectedSpecFileTypeCode(importDefinitionCode);
	}
	
	
	
////xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
	
	public String displayImportSpecUpdateAction(){	 
		selectedImportSpec = new ImportSpecDTO();
		BeanUtils.copyProperties(selectedImportSpecOri, selectedImportSpec);
		setSelectedSpecFileTypeCode(selectedImportSpec.getImportDefinitionCode());

		currentAction = ImportDefsAndSpecsAction.UpdateSpec;
		copyBeanforSpecRevertUpdate();
		setInputValue(importDefinitionMenu, selectedImportSpec.getImportDefinitionCode());
		verificationErrorMessage = "";
		importSpecTypeItems = null;
		return "import_spec_update";
	}
	
	public String displayImportSpecViewAction(){	 
		selectedImportSpec = new ImportSpecDTO();
		BeanUtils.copyProperties(selectedImportSpecOri, selectedImportSpec);
		setSelectedSpecFileTypeCode(selectedImportSpec.getImportDefinitionCode());

		currentAction = ImportDefsAndSpecsAction.ViewSpec;
		copyBeanforSpecRevertUpdate();
		setInputValue(importDefinitionMenu, selectedImportSpec.getImportDefinitionCode());
		verificationErrorMessage = "";
		importSpecTypeItems = null;
		return "import_spec_update";
	}
	
	public String displayImportSpecCopyAddAction(){	 
		defaultSetting = false;
		selectedImportSpec = new ImportSpecDTO();
		BeanUtils.copyProperties(selectedImportSpecOri, selectedImportSpec);
		setSelectedSpecFileTypeCode(selectedImportSpec.getImportDefinitionCode());
		selectedImportSpec.setImportSpecCode("");

		currentAction = ImportDefsAndSpecsAction.copyAddSpec;
		copyBeanforSpecRevertUpdate();
		setInputValue(importDefinitionMenu, selectedImportSpec.getImportDefinitionCode());
		verificationErrorMessage = "";
		importSpecTypeItems = null;
		return "import_spec_add";
	}
	
	public String displayImportSpecDeleteAction() {
		selectedImportSpec = new ImportSpecDTO();
		BeanUtils.copyProperties(selectedImportSpecOri, selectedImportSpec);
		setSelectedSpecFileTypeCode(selectedImportSpec.getImportDefinitionCode());
		
		currentAction = ImportDefsAndSpecsAction.DeleteSpec;
		setInputValue(importDefinitionMenu, selectedImportSpec.getImportDefinitionCode());
		verificationErrorMessage = "";
		importSpecTypeItems = null;
	    return "import_spec_delete"; 
	}	
	
	
	public String displayImportSpecAddAction(){
		defaultSetting = false;
		currentAction = ImportDefsAndSpecsAction.AddSpec;
		selectedImportSpec = new ImportSpecDTO();
		
		//PP-164
        Option option = optionService.findByPK(new OptionCodePK("PCO", "ADMIN", "ADMIN"));
        String option_pco = (option == null || option.getValue() == null) ? "0" : option.getValue();
        boolean isPco = (option_pco!=null && option_pco.equals("1"))? true : false;   
        boolean option_ip = loginservice.getHasPurchaseLicense();
        boolean option_ips = loginservice.getHasBillingLicense();   
        if(isPco){
        	selectedImportSpec.setImportSpecType("PCO");
        }
        else if(option_ip){
        	selectedImportSpec.setImportSpecType("IP");
        }
        else if(option_ips){
        	selectedImportSpec.setImportSpecType("IPS");
        }
		
		//  raghu selectedImportSpec.setImportSpecType("PCO");//default
		selectedImportSpec.setFilesizedupres("2");
		selectedImportSpec.setFilenamedupres("2");
		setSelectedSpecFileTypeCode("");
		copyBeanforSpecRevertAdd();
		setInputValue(importDefinitionMenu, "");
		verificationErrorMessage = "";
		importSpecTypeItems = null;
		return "import_spec_add";
	}
	
	public String displayImportSpecMapAction() {
		currentAction = ImportDefsAndSpecsAction.MapSpec;
		setInputValue(importDefinitionMenu, selectedImportSpec.getImportDefinitionCode());
		verificationErrorMessage = "";
		return "import_spec_map"; 
	}	

	public String displayImportConnectionAddAction() {
		verificationErrorMessage = ""; 
		currentAction = ImportDefsAndSpecsAction.AddConn;
		selectedImportConnectionComponent = new ImportConnection();
		return "import_conn_add";
	}
	
	public String displayImportConnectionUpdateAction() {
		if(indexImportConnectionComponent==null){
			return null;
		}
		selectedImportConnectionComponent= new ImportConnection(); 
		BeanUtils.copyProperties(indexImportConnectionComponent, selectedImportConnectionComponent);

		verificationErrorMessage = "";
		currentAction = ImportDefsAndSpecsAction.UpdateConn;
		return "import_conn_add";
	}
		
	public String displayImportConnectionDeleteAction() {
		if(indexImportConnectionComponent==null){
			return null;
		}
		selectedImportConnectionComponent= new ImportConnection(); 
		BeanUtils.copyProperties(indexImportConnectionComponent, selectedImportConnectionComponent);
		
		verificationErrorMessage = "";
		currentAction = ImportDefsAndSpecsAction.DeleteConn;
		return "import_conn_add";
	}
	
	public String displayImportConnectionViewAction() {
		if(indexImportConnectionComponent==null){
			return null;
		}
		selectedImportConnectionComponent= new ImportConnection(); 
		BeanUtils.copyProperties(indexImportConnectionComponent, selectedImportConnectionComponent);
		
		verificationErrorMessage = "";
		currentAction = ImportDefsAndSpecsAction.ViewConn;
		return "import_conn_add";
	}
	
	public String displayImportConnectionCopyAddAction() {
		if(indexImportConnectionComponent==null){
			return null;
		}
		selectedImportConnectionComponent= new ImportConnection(); 
		BeanUtils.copyProperties(indexImportConnectionComponent, selectedImportConnectionComponent);
		
		verificationErrorMessage = "";
		
		selectedImportConnectionComponent.setImportConnectionCode(null);
		selectedImportConnectionComponent.setUpdateTimestamp(null);
		selectedImportConnectionComponent.setUpdateUserId(null);
		currentAction = ImportDefsAndSpecsAction.AddConn;
		return "import_conn_add";
	}
	
	private void setInputValue(UIInput input, String value) {
		if(input!=null){
			input.setLocalValueSet(false);
			input.setSubmittedValue(null);
			input.setValue(value);
		}
	}

	public void vclTransactionAmount(ValueChangeEvent event){
		if(event.getNewValue().equals(true)){
			holdTransactionAmount.setDisabled(false);
		}else{
			holdTransactionAmount.setDisabled(true);
			//holdTransactionAmount.setValue("");
		}
	}
	private void copyBeanforSpecRevertAdd(){
		getImportMapProcessPreferenceDTO().setFileSizeDuplicateRestriction(selectedImportSpec.getFilesizedupres());
		getImportMapProcessPreferenceDTO().setFileNameDuplicateRestriction(selectedImportSpec.getFilenamedupres());
		getImportMapProcessPreferenceDTO().setDefaultImportDateMap(selectedImportSpec.getImportdatemap());
		getImportMapProcessPreferenceDTO().setDoNotImportMarkers(selectedImportSpec.getDonotimport());
		getImportMapProcessPreferenceDTO().setReplaceWithMarkers(selectedImportSpec.getReplacewith());
		getImportMapProcessPreferenceDTO().setEndOfFileMarker(selectedImportSpec.getBatcheofmarker());
		getImportMapProcessPreferenceDTO().setBatchCountMarker(selectedImportSpec.getBatchcountmarker());
		getImportMapProcessPreferenceDTO().setBatchTotalMarker(selectedImportSpec.getBatchtotalmarker());
		getImportMapProcessPreferenceDTO().setDeleteTransactionWithZeroAmountFlag(false);
		getImportMapProcessPreferenceDTO().setHoldTransactionAmountFlag(false);
		getImportMapProcessPreferenceDTO().setHoldTransactionAmount("");
		getImportMapProcessPreferenceDTO().setProcessTransactionAmountFlag(false);
		getImportMapProcessPreferenceDTO().setProcessTransactionAmount("");
		
		this.processTransactionAmountInput=null;
		filterTaxCodeHandler.setTaxcodeCode("");
	}

	private void copyBeanforSpecRevertUpdate() {
		getImportMapProcessPreferenceDTO().setFileSizeDuplicateRestriction(
				selectedImportSpec.getFilesizedupres());
		getImportMapProcessPreferenceDTO().setFileNameDuplicateRestriction(
				selectedImportSpec.getFilenamedupres());
		getImportMapProcessPreferenceDTO().setDefaultImportDateMap(
				selectedImportSpec.getImportdatemap());
		getImportMapProcessPreferenceDTO().setDoNotImportMarkers(
				selectedImportSpec.getDonotimport());
		getImportMapProcessPreferenceDTO().setReplaceWithMarkers(
				selectedImportSpec.getReplacewith());
		getImportMapProcessPreferenceDTO().setEndOfFileMarker(
				selectedImportSpec.getBatcheofmarker());
		getImportMapProcessPreferenceDTO().setBatchCountMarker(
				selectedImportSpec.getBatchcountmarker());
		getImportMapProcessPreferenceDTO().setBatchTotalMarker(
				selectedImportSpec.getBatchtotalmarker());

		String Delete0amtflag = selectedImportSpec.getDelete0amtflag();
		if ("1".equalsIgnoreCase(Delete0amtflag)) {
			getImportMapProcessPreferenceDTO()
					.setDeleteTransactionWithZeroAmountFlag(true);
		} else {
			getImportMapProcessPreferenceDTO()
					.setDeleteTransactionWithZeroAmountFlag(false);
		}
		String Holdtransflag = selectedImportSpec.getHoldtransflag();
		if ("1".equalsIgnoreCase(Holdtransflag)) {
			getImportMapProcessPreferenceDTO().setHoldTransactionAmountFlag(
					true);
		} else {
			getImportMapProcessPreferenceDTO().setHoldTransactionAmountFlag(
					false);
		}
		try {
			String Holdtransamt = String.valueOf(selectedImportSpec
					.getHoldtransamt());
			if (Holdtransamt.equalsIgnoreCase("null")) {
				getImportMapProcessPreferenceDTO().setHoldTransactionAmount("");
			} else {
				getImportMapProcessPreferenceDTO().setHoldTransactionAmount(
						Holdtransamt);
			}
		} catch (Exception e) {
		}
		String Autoprocflag = selectedImportSpec.getAutoprocflag();
		if ("1".equalsIgnoreCase(Autoprocflag)) {
			getImportMapProcessPreferenceDTO().setProcessTransactionAmountFlag(
					true);
		} else {
			getImportMapProcessPreferenceDTO().setProcessTransactionAmountFlag(
					false);
		}

		try {
			String Autoproamt = String.valueOf(selectedImportSpec
					.getAutoprocamt());
			if (Autoproamt.equalsIgnoreCase("null")) {
				getImportMapProcessPreferenceDTO().setProcessTransactionAmount(
						"");
			} else {
				getImportMapProcessPreferenceDTO().setProcessTransactionAmount(
						Autoproamt);
			}

		} catch (Exception e) {
		}
		filterTaxCodeHandler.setTaxcodeCode(selectedImportSpec.getAutoprocid());
	}

	private void copyBeanforSpec() {
		selectedImportSpec.setFilesizedupres(getImportMapProcessPreferenceDTO()
				.getFileSizeDuplicateRestriction());
		selectedImportSpec.setFilenamedupres(getImportMapProcessPreferenceDTO()
				.getFileNameDuplicateRestriction());
		selectedImportSpec.setImportdatemap(getImportMapProcessPreferenceDTO()
				.getDefaultImportDateMap());
		selectedImportSpec.setDonotimport(getImportMapProcessPreferenceDTO()
				.getDoNotImportMarkers());
		selectedImportSpec.setReplacewith(getImportMapProcessPreferenceDTO()
				.getReplaceWithMarkers());
		selectedImportSpec.setBatcheofmarker(getImportMapProcessPreferenceDTO()
				.getEndOfFileMarker());
		selectedImportSpec
				.setBatchcountmarker(getImportMapProcessPreferenceDTO()
						.getBatchCountMarker());
		selectedImportSpec
				.setBatchtotalmarker(getImportMapProcessPreferenceDTO()
						.getBatchTotalMarker());
		String Delete0amtflag = String
				.valueOf(getImportMapProcessPreferenceDTO()
						.isDeleteTransactionWithZeroAmountFlag());
		if (Delete0amtflag.equalsIgnoreCase("true"))
			selectedImportSpec.setDelete0amtflag("1");
		else if (Delete0amtflag.equalsIgnoreCase("false")) {
			selectedImportSpec.setDelete0amtflag("0");
		}
		String Holdtransflag = String
				.valueOf(getImportMapProcessPreferenceDTO()
						.isHoldTransactionAmountFlag());
		if (Holdtransflag.equalsIgnoreCase("true"))
			selectedImportSpec.setHoldtransflag("1");
		else if (Holdtransflag.equalsIgnoreCase("false")) {
			selectedImportSpec.setHoldtransflag("0");
		}
		try {
			long Holdtransamt = Long
					.parseLong(getImportMapProcessPreferenceDTO()
							.getHoldTransactionAmount().toString());

			selectedImportSpec.setHoldtransamt(Holdtransamt);
		} catch (Exception e) {
		}
		String Autoprocflag = String.valueOf(getImportMapProcessPreferenceDTO()
				.isProcessTransactionAmountFlag());
		if (Autoprocflag.equalsIgnoreCase("true"))
			selectedImportSpec.setAutoprocflag("1");
		else if (Autoprocflag.equalsIgnoreCase("false")) {
			selectedImportSpec.setAutoprocflag("0");
		}

		try {
			long Autoproamt = Long.parseLong(getImportMapProcessPreferenceDTO()
					.getProcessTransactionAmount().toString());
			selectedImportSpec.setAutoprocamt(Autoproamt);

		} catch (Exception e) {
		}
		selectedImportSpec.setAutoprocid(filterTaxCodeHandler.getTaxcodeCode());

	}
	
	public boolean validateImportSpec(){
  		boolean verifyError = false;
  		
  		if(selectedImportSpec.getImportSpecCode()==null || selectedImportSpec.getImportSpecCode().length()==0){
  			FacesMessage message = new FacesMessage("Spec Code is required. ");
  			message.setSeverity(FacesMessage.SEVERITY_ERROR);
  			FacesContext.getCurrentInstance().addMessage(null, message);
  			verifyError = true;
  		}
  		
  		if(selectedImportSpec.getDescription()==null || selectedImportSpec.getDescription().length()==0){
  			FacesMessage message = new FacesMessage("Description is required. ");
  			message.setSeverity(FacesMessage.SEVERITY_ERROR);
  			FacesContext.getCurrentInstance().addMessage(null, message);
  			verifyError = true;
  		}
  		
  		if(selectedImportSpec.getImportDefinitionCode()==null || selectedImportSpec.getImportDefinitionCode().length()==0){
  			FacesMessage message = new FacesMessage("Import Definition is required. ");
  			message.setSeverity(FacesMessage.SEVERITY_ERROR);
  			FacesContext.getCurrentInstance().addMessage(null, message);
  			verifyError = true;
  		}
  		
  		if(verifyError){
			return true;
		}
  		
  		return false;
  	}
	
	public boolean validateWhereClause(){
  		boolean verifyError = false;
  		
  		verifyWhereClause();		
  		if(!(verificationErrorMessage==null || verificationErrorMessage.length()==0) && !(verificationErrorMessage.equalsIgnoreCase("Verification succeeded"))){
  			verifyError = true;
  		}

  		return verifyError;
  	}
	
	public String importSpecOkAction() {
 		switch (currentAction) {
			case AddSpec:
				if(validateImportSpec()){//Validation failed.
					return null;
				}
				if(validateWhereClause()){//Validation failed.
					return null;
				}
				copyBeanforSpec();
				importDefAndSpecsService.addImportSpecRecord(selectedImportSpec);
				break;
				
			case DeleteSpec:
				ImportSpecPK lpk = new ImportSpecPK();
				lpk.setImportSpecType(selectedImportSpec.getImportSpecType());
				lpk.setImportSpecCode(selectedImportSpec.getImportSpecCode());
				importDefAndSpecsService.deleteImportSpec(lpk);
				break;
				
			case UpdateSpec:
				if(validateImportSpec()){//Validation failed.
					return null;
				}
				if(validateWhereClause()){//Validation failed.
					return null;
				}
				copyBeanforSpec();
				importDefAndSpecsService.updateImportSpec(selectedImportSpec);
				
				break;

			case MapSpec:
				logger.debug("userCodeList.size(): " + userCodeList.size());
				importDefAndSpecsService.updateImportSpecUserBySpec(selectedImportSpec, userCodeList);

				break;
		}
		updateImportSpecList();
		return "import_specs_main";
	}
	
	
	public String goToMainAction(){
		// Reset selections
		//updateImportDefList();
		//updateImportSpecList();

		sampleFileName = "";
		sampleFileNameOri = "";
		
		return "import_specs_main";
}

	public void validateSpecCode(FacesContext context, UIComponent toValidate, Object value) {
	    // Ensure code is unique
		String code = (String) value; 
	    String type = selectedImportSpec.getImportSpecType();
	    ImportSpecPK key = new ImportSpecPK(type, code);
	    if (importDefAndSpecsService.getImportSpec(key) != null) {
			((UIInput)toValidate).setValid(false);
			FacesMessage message = new FacesMessage("Spec code already exists.");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage(toValidate.getClientId(context), message);
	    }
	}
	
	public void onScreenTypeChange(ValueChangeEvent event) {
		logger.debug("Caught value change from screen type drop down");
		
		try {
			screenType = ScreenType.valueOf((String) event.getNewValue());
		} catch (Exception e) {
			logger.warn("Failed to process new screen type: " + e);
		}
		// Reset selections
		updateImportDefList();
		updateImportSpecList();
	}
	
	//"FD"  FlatDelimitedType, "DB" DatabaseType
	
	public Boolean getIsFlatDelimitedType() {
		return (selectedImportDef.getImportFileTypeCode() !=null && selectedImportDef.getImportFileTypeCode().equalsIgnoreCase("FD"));			
	}
	
	public Boolean getIsDatabaseType() {
		return (selectedImportDef.getImportFileTypeCode() !=null && selectedImportDef.getImportFileTypeCode().equalsIgnoreCase("DB"));			
	}
	
	public void onImportFileTypeChange(ValueChangeEvent event) {
		String importFileType = "";
		try {
			importFileType = (String) event.getNewValue();
		} catch (Exception e) {
			logger.warn("Failed to process new screen type: " + e);
		}
		
		selectedImportDef.setImportFileTypeCode(importFileType);;
	}
	
	public void onConnectionChange(ValueChangeEvent event) {
		logger.debug("Caught value change from screen type drop down");
		
		String connectionCode = "";
		try {
			connectionCode = (String) event.getNewValue();
		} catch (Exception e) {
			logger.warn("Failed to process new screen type: " + e);
		}
		
		selectedImportDef.setImportConnectionCode(connectionCode);
		importTableItems = null;
	}

	
	public List<SelectItem> getAllUsersMenuItems() {
		if (allUsersMenuItems == null) {
			allUsersMenuItems = new ArrayList<SelectItem>();
			for (User user : userService.findAllUsers()) {
				String userCode = user.getUserCode();
				String userName = user.getUserName();
				allUsersMenuItems.add(new SelectItem(userCode, userCode + " (" + userName + ")"));
			}
		}
		return allUsersMenuItems;
	}
	
	public List<String> getUserCodeList() {
		return userCodeList;
	}
	
	public void setUserCodeList(List<String> userCodeList) {
		this.userCodeList = userCodeList;
	}

	public String getSampleFileName() {
		return this.sampleFileName;
	}
	
	public String getSampleFileNameOri() {
		return this.sampleFileNameOri;
	}

	public void setSampleFileName(String sampleFileName) {
		this.sampleFileName = sampleFileName;
	}
	public void setImportDefinitionMenu(HtmlSelectOneMenu importDefinitionMenu) {
		this.importDefinitionMenu = importDefinitionMenu;
	}
	
	public void setImportSpecTypeMenu(HtmlSelectOneMenu importSpecTypeMenu) {
		this.importSpecTypeMenu = importSpecTypeMenu;
	}

	public UserService getUserService() {
		return userService;
	}
	
	public void setUserService(UserService userService) {
		this.userService = userService;
	}
	
	public ImportDefAndSpecsService getImportDefAndSpecsService() {
		return importDefAndSpecsService;
	}
	
	public void setImportDefAndSpecsService(ImportDefAndSpecsService importDefAndSpecsService) {
		this.importDefAndSpecsService = importDefAndSpecsService;
	}
	
	public int getSelectedDefUsingConnIndex() {
		return selectedDefUsingConnIndex;
	}

	public void setSelectedDefUsingConnIndex(int selectedDefUsingConnIndex) {
		this.selectedDefUsingConnIndex = selectedDefUsingConnIndex;
	}

	public int getSelectedSpecUsingDefIndex() {
		return selectedSpecUsingDefIndex;
	}

	public void setSelectedSpecUsingDefIndex(int selectedSpecUsingDefIndex) {
		this.selectedSpecUsingDefIndex = selectedSpecUsingDefIndex;
	}
	
	public int getSelectedDefIndex() {
		return selectedDefIndex;
	}

	public void setSelectedDefIndex(int selectedDefIndex) {
		this.selectedDefIndex = selectedDefIndex;
	}

	public int getSelectedSpecIndex() {
		return selectedSpecIndex;
	}

	public void setSelectedSpecIndex(int selectedSpecIndex) {
		this.selectedSpecIndex = selectedSpecIndex;
	}
	
	public boolean getValidSpecSelection() {
		return (selectedImportSpec != null);
	}

	public boolean getValidDefSelection() {
		return (selectedImportDef != null);
	}

	public String getActionText() {
		return currentAction.getActionText();
	}
	
	public Boolean getIsDefinitionScreen() {
		return (screenType == ScreenType.Definition);
	}
	
	public Boolean getIsSpecScreen() {
		return (screenType == ScreenType.Spec);
	}
	
	public Boolean getIsConnectionScreen() {
		return (screenType == ScreenType.Connection);				
	}
	
	public ImportDefDTO getSelectedImportDef() {
		return selectedImportDef;
	}
	
	public String getScreenType() {
		return screenType.toString();
	}
	
	public List<UserDTO> getUserList() {
		return userList;
	}
	
	public ImportSpecDTO getSelectedImportSpec() {
		return selectedImportSpec;
	}
	
	public ImportSpecDTO getSelectedImportSpecOri() {
		return selectedImportSpecOri;
	}
	

	/**
	 * @return the selectedImportSpecList
	 */
	public List<ImportSpecDTO> getSelectedImportSpecList() {
		return this.selectedImportSpecList;
	}

	/**
	 * @param selectedImportSpecList the selectedImportSpecList to set
	 */
	public void setSelectedImportSpecList(List<ImportSpecDTO> selectedImportSpecList) {
		this.selectedImportSpecList = selectedImportSpecList;
	}
	public CacheManager getCacheManager() {
		return this.cacheManager;
	}
	public void setCacheManager(CacheManager cacheManager) {
         this.cacheManager = cacheManager;
		
		//jurisdictionHandler.setCacheManager(cacheManager);
		filterTaxCodeHandler.setCacheManager(cacheManager);
		filterTaxCodeHandler.setTaxCodeBackingBean(taxCodeBackingBean);
		filterTaxCodeHandler.setReturnRuleUrl("import_spec_add");
	}
	public void searchTaxCodeCodeCallback(TaxCodeCodeHandler taxCodeCodeHandler) {
	 if(taxCodeCodeHandler == filterTaxCodeHandler){
			updateButtonAction(null);	
		}
	}
	public ImportDefinitionMappingBean getImportDefinitionMappingBean() {
		return this.importDefinitionMappingBean;
	}
	public void setImportDefinitionMappingBean(ImportDefinitionMappingBean importDefinitionMappingBean) {
		this.importDefinitionMappingBean = importDefinitionMappingBean;
	}

	public HtmlInputFileUpload getUploadFile() {
		return this.uploadFile;
	}

	public void setUploadFile(HtmlInputFileUpload uploadFile) {
		this.uploadFile = uploadFile;
	}
	public void vclApplyTaxCode(ValueChangeEvent event){
		if(event.getNewValue().equals(true)){
		filterTaxCodeHandler.getTaxcodeDetailId().setDisabled(false);
		processTransactionAmountInput.setDisabled(false);
		setDisplaySearch(true);
				
		
		}else{
			filterTaxCodeHandler.getTaxcodeDetailId().setDisabled(true);
			processTransactionAmountInput.setDisabled(true);
			setDisplaySearch(false);
			//processTransactionAmountInput.setValue("");
		}
		
	}
	public HtmlInputText getProcessTransactionAmountInput() {
		return processTransactionAmountInput;
	}

	public void setProcessTransactionAmountInput(
			HtmlInputText processTransactionAmountInput) {
		this.processTransactionAmountInput = processTransactionAmountInput;
	}
	public boolean isDisplayBackButton() {
		return displayBackButton;
	}
	public boolean isDisplaySearch() {
		return displaySearch;
	}
	public void setDisplaySearch(boolean displaySearch) {
		this.displaySearch = displaySearch;
	}
	public void setDisplayBackButton(boolean displayBackButton) {
		this.displayBackButton = displayBackButton;
	}
//Method to set directory path from Directory browser Backing Bean
	public void setDirectoryPath(String selectedDirectory2) {
		selectedImportSpec.setDefaultDirectory(selectedDirectory2);
		
	}
	
	public void copySampleFile(ActionEvent e) throws AbortProcessingException {
		UIInput uii = (UIInput)e.getComponent().findComponent("impFileForm:sampleFileName");
		if (uii != null) {
			String s = (String)uii.getSubmittedValue();
			this.uploadFileName = this.sampleFileName = s;
		}
	}
	
	public List<SelectItem> getImportSpecTypeItems() {	
		if (importSpecTypeItems == null) {
			importSpecTypeItems = new ArrayList<SelectItem>();
			List<ListCodes> listCodes = cacheManager.getListCodesByType("IMPDEFTYPE");
			
			if(currentAction.equals(ImportDefsAndSpecsAction.AddSpec)){
				Option option = optionService.findByPK(new OptionCodePK("PCO",
						"ADMIN", "ADMIN"));
				String option_pco = (option == null || option.getValue() == null) ? "0"
						: option.getValue();
				boolean isPco = (option_pco!=null && option_pco.equals("1"))? true : false;
				
				boolean option_ip = loginservice.getHasPurchaseLicense();
				boolean option_ips = loginservice.getHasBillingLicense();
				if (listCodes != null) {
					for (ListCodes lc : listCodes) {
						if (lc.getCodeCode() != null) {
							if ((lc.getCodeCode().equals("PCO")
									&& isPco)
									|| (lc.getCodeCode().equals("IP") && option_ip)
									|| (lc.getCodeCode().equals("IPS") && option_ips)) {
								importSpecTypeItems.add(new SelectItem(lc.getCodeCode(), lc.getDescription()));
							}
						}
					}
				}
			}
			else{
				if (listCodes != null) {
					for (ListCodes lc : listCodes) {
						if (lc.getCodeCode() != null) {
							if (lc.getCodeCode().equals("PCO")
									|| lc.getCodeCode().equals("IP") 
									|| lc.getCodeCode().equals("IPS")) {
								importSpecTypeItems.add(new SelectItem(lc.getCodeCode(), lc.getDescription()));
							}
						}
					}
				}
			}

		}
		return importSpecTypeItems;
	}

	public List<SelectItem> getImportConnectionItems() {	
		if (importConnectionItems == null) {
			importConnectionItems = new ArrayList<SelectItem>();
			importConnectionItems.add(new SelectItem("","Select a Connection"));
			
			List<ImportConnection> importConnections = importDefAndSpecsService.getAllImportConnection();
			if (importConnections != null) {
				for (ImportConnection lc : importConnections) {
					importConnectionItems.add(new SelectItem(lc.getImportConnectionCode(), 
						lc.getImportConnectionCode() + " - " + lc.getDescription()));
				}
			}
		}
		return importConnectionItems;
	}
	
	public List<SelectItem> getImportTableItems() {	
		if (importTableItems == null) {
			importTableItems = new ArrayList<SelectItem>();

			importTableItems.add(new SelectItem("","Select a Table Name"));
			
			if(selectedImportDef.getImportConnectionCode()==null || selectedImportDef.getImportConnectionCode().length()==0){
				return importTableItems;
			}
				
			//Make JDBC connection to get all table names
			//SELECT TABLE_NAME FROM TB_VIEW_TABLE_NAMES ORDER BY TABLE_NAME			
			List<ImportConnection> importConnections = importDefAndSpecsService.getConnectionsByCode(selectedImportDef.getImportConnectionCode());		
			if(importConnections==null || importConnections.size()==0){
				return importTableItems;
			}
				
			ImportConnection importConnection = importConnections.get(0);
				
			try {
				Class.forName(importConnection.getDbDriverClassName());
		    } catch (ClassNotFoundException e) {
		    	e.printStackTrace();
		    	return importTableItems;
		    }

		    Connection conn = null;
		    Statement stmt = null;
		    ResultSet rset = null;
		    try {
		    	conn = DriverManager.getConnection(importConnection.getDbUrl(), importConnection.getDbUserName(), importConnection.getDbPassword());
		    	stmt = conn.createStatement();
		    	rset = stmt.executeQuery("SELECT table_name FROM user_tables order by table_name");		    	
		    	String tableName = "";
				while (rset.next()) {
					tableName = rset.getString("table_name");				
					importTableItems.add(new SelectItem(tableName, tableName));
				}

		    	rset.close();rset = null;
		    	stmt.close();stmt = null;
		    	conn.close();conn = null;
		    } catch (SQLException e) {
		    	e.printStackTrace();
		    } finally {
		      if (rset != null) try {rset.close();} catch (SQLException ignore) {}
		      if (stmt != null) try {stmt.close();} catch (SQLException ignore) {}
		      if (conn != null) try { conn.close();} catch (SQLException ignore) {}
		    }
		}
		return importTableItems;
	}
	
	public List<SelectItem> getEncodingItems() {	
		if (encodingItems == null) {
			encodingItems = new ArrayList<SelectItem>();
			encodingItems.add(new SelectItem("","Select an Encoding Type"));//default: Windows-1252
			encodingItems.add(new SelectItem("Windows-1252", "Windows-1252 - ANSI"));
			encodingItems.add(new SelectItem("UTF-8", "UTF-8"));
			encodingItems.add(new SelectItem("UTF-16", "UTF-16"));
			encodingItems.add(new SelectItem("US-ASCII", "US-ASCII"));
		}
		return encodingItems;
	}

	public List<SelectItem> getImportFileTypeItems() {	
		if (importFileTypeItems == null) {
			importFileTypeItems = new ArrayList<SelectItem>();
			List<ListCodes> listCodes = cacheManager.getListCodesByType("IMPFILETYP");

			importFileTypeItems.add(new SelectItem("","Select an Import File Type"));
			if (listCodes != null) {
				for (ListCodes lc : listCodes) {
					importFileTypeItems.add(new SelectItem(lc.getCodeCode(), lc.getDescription()));
				}
			}
		}
		return importFileTypeItems;
	}

	public List<SelectItem> getImportDefTypeItems() {
		if (importDefTypeItems == null) {
			importDefTypeItems = new ArrayList<SelectItem>();
			List<ListCodes> listCodes = cacheManager.getListCodesByType("IMPDEFTYPE");
			
			if(currentAction.equals(ImportDefsAndSpecsAction.AddDef)){
				Option option = optionService.findByPK(new OptionCodePK("PCO",
						"ADMIN", "ADMIN"));
				String option_pco = (option == null || option.getValue() == null) ? "0"
						: option.getValue();
				boolean isPco = (option_pco!=null && option_pco.equals("1"))? true : false;
				
				boolean option_ip = loginservice.getHasPurchaseLicense();
				boolean option_ips = loginservice.getHasBillingLicense();
				if (listCodes != null) {
					for (ListCodes lc : listCodes) {
						if (lc.getCodeCode() != null) {
							if ((lc.getCodeCode().equals("PCO")
									&& isPco)
									|| (lc.getCodeCode().equals("IP") && option_ip)
									|| (lc.getCodeCode().equals("IPS") && option_ips)) {
								importDefTypeItems.add(new SelectItem(lc.getCodeCode(), lc.getDescription()));
							}
						}
					}
				}
			}
			else{
				if (listCodes != null) {
					for (ListCodes lc : listCodes) {
						if (lc.getCodeCode() != null) {
							if (lc.getCodeCode().equals("PCO")
									|| lc.getCodeCode().equals("IP") 
									|| lc.getCodeCode().equals("IPS")) {
								importDefTypeItems.add(new SelectItem(lc.getCodeCode(), lc.getDescription()));
							}
						}
					}
				}
			}

		}
		return importDefTypeItems;
	}
	
	public List<SelectItem> getImportDefinitionCodeItems() {
		final List<SelectItem> importDefDropdownList = new ArrayList<SelectItem>();		
		importDefDropdownList.add(new SelectItem("","Select an Import Definition"));
		if(selectedImportSpec.getImportSpecType()==null)
		{
			List<SelectItem> batchTypeList = getImportDefSpecTypeItems();
			
			String getvalue=batchTypeList.get(0).getValue().toString();
			List<ImportDefDTO> objList = importDefAndSpecsService.getAllImportDefinition(getvalue);
			for (ImportDefDTO ojImportDef : objList) {			
				if(ojImportDef != null && ojImportDef.getImportDefinitionCode()!=null ){				
					importDefDropdownList.add(new SelectItem(ojImportDef.getImportDefinitionCode(),ojImportDef.getImportDefinitionCode()));
				}
		  }
		}else if(selectedImportSpec.getImportSpecType() != null) {
			List<ImportDefDTO> objList = importDefAndSpecsService.getAllImportDefinition(selectedImportSpec.getImportSpecType());			
			
			for (ImportDefDTO ojImportDef : objList) {			
				if(ojImportDef != null && ojImportDef.getImportDefinitionCode()!=null ){				
					importDefDropdownList.add(new SelectItem(ojImportDef.getImportDefinitionCode(),ojImportDef.getImportDefinitionCode()));
				}
			}
		}
		
		return importDefDropdownList;
	}
	
	public List<SelectItem> getConnectionTypeCodeItems() {
		List<SelectItem> connectionTypeCodeItems = new ArrayList<SelectItem>();
		
		connectionTypeCodeItems.add(new SelectItem("","Select a Connection Type"));
		
		List <ListCodes> connectionTypes = importDefAndSpecsService.getConnectionTypeCodes();
		for (ListCodes lc : connectionTypes) {
			connectionTypeCodeItems.add(new SelectItem(lc.getCodeCode(), lc.getDescription()));			
		}
		return connectionTypeCodeItems;
	}
	
	public String listDefAndSpecs() {
		
		System.out.println("listDefAndSpecs");
		
		String result = null;
		if (screenType.equals(ScreenType.Connection)) {
		
			ImportConnection importConnection = new ImportConnection();
			importConnection.setImportConnectionType(connectionType);
			importConnection.setImportConnectionCode(defSpecCode);
			importConnection.setDescription(defDescriptionText);
			importConnection.setComments(defCommentsText);
			importConnectionModel.setExampleInstance(importConnection);	
		} else if (screenType.equals(ScreenType.Spec)) {
			ImportSpec importSpec = new ImportSpec();	
			ImportSpecPK ImportSpecPK = new ImportSpecPK();
			ImportSpecPK.setImportSpecCode(defSpecCode);
			ImportSpecPK.setImportSpecType(importDefSpecType);
			importSpec.setImportSpecPK(ImportSpecPK);
			importSpec.setDescription(defDescriptionText);
			importSpec.setComments(defCommentsText);
			importSpecModel.setExampleInstance(importSpec);	
			
		} else if (screenType.equals(ScreenType.Definition)) {
			ImportDefinition importDefinition = new ImportDefinition();	
			importDefinition.setImportDefinitionCode(defSpecCode);
			importDefinition.setImportDefinitionType(importDefSpecType);	
			importDefinition.setDescription(defDescriptionText);
			importDefinition.setComments(defCommentsText);
			importDefinitionModel.setExampleInstance(importDefinition);	
		}
		
		clear();
		
		return "import_def_main";
	}
	
	public void sortConnectionAction(ActionEvent e) {
		importConnectionModel.toggleSortOrder(e.getComponent().getParent().getId());
	}
	
	public void sortDefinitionAction(ActionEvent e) {
		importDefinitionModel.toggleSortOrder(e.getComponent().getParent().getId());
	}
	
	public void sortSpecAction(ActionEvent e) {
		importSpecModel.toggleSortOrder(e.getComponent().getParent().getId());
	}

	public String resetSearchFilter() {
		System.out.println("resetSearchFilter");
		//screenType = ScreenType.Definition;
		connectionType = null;
		importDefSpecType = null;
		defSpecCode = null;
		defDescriptionText = null;
		defCommentsText = null;
		
		return null;
	}
	
	public OptionService getOptionService() {
		return optionService;
	}
	public void setOptionService(OptionService optionService) {
		this.optionService = optionService;
	}
	public loginBean getLoginservice() {
		return loginservice;
	}
	public void setLoginservice(loginBean loginservice) {
		this.loginservice = loginservice;
	}
	
	public List<SelectItem> getImportDefSpecTypeItems() {
		
		if (importDefSpecTypeItems == null) {
			importDefSpecTypeItems = new ArrayList<SelectItem>();
			List<ListCodes> listCodes = cacheManager.getListCodesByType("IMPDEFTYPE");
			Option option = optionService.findByPK(new OptionCodePK("PCO","ADMIN", "ADMIN"));
			String option_pco = (option == null || option.getValue() == null) ? "0": option.getValue();
			boolean isPco = (option_pco != null && option_pco.equals("1")) ? true: false;
			boolean option_ip = loginservice.getHasPurchaseLicense();
			boolean option_ips = loginservice.getHasBillingLicense();
			
			importDefSpecTypeItems.add(new SelectItem("","Select a Type"));
			if (listCodes != null) {
				for (ListCodes lc : listCodes) {
					if (lc.getCodeCode() != null) {
						if ((lc.getCodeCode().equals("PCO") && isPco)
								|| (lc.getCodeCode().equals("IP") && option_ip)
								|| (lc.getCodeCode().equals("IPS") && option_ips)) {
							importDefSpecTypeItems.add(new SelectItem(lc.getCodeCode(), lc.getDescription()));
						}
					}
				}
			}

		}
		return importDefSpecTypeItems;
	}
	
    public HtmlInputText getHoldTransactionAmount() {
		return holdTransactionAmount;
	}
	public void setHoldTransactionAmount(HtmlInputText holdTransactionAmount) {
		this.holdTransactionAmount = holdTransactionAmount;
	}
	public TogglePanelController getTogglePanelController() {
		if (togglePanelController == null) {
			togglePanelController = new TogglePanelController();
			togglePanelController.addTogglePanel("selectConfigComponent", true);
			togglePanelController.addTogglePanel("defSpecInformation", true);
		}
		
		
		return togglePanelController;
	}
	public void setTogglePanelController(TogglePanelController togglePanelController) {
		this.togglePanelController = togglePanelController;
	}

	public String getConnectionType() {
		return connectionType;
	}
	public void setConnectionType(String connectionType) {
		this.connectionType = connectionType;
	}
	public String getImportDefSpecType() {
		return importDefSpecType;
	}
	public void setImportDefSpecType(String importDefSpecType) {
		this.importDefSpecType = importDefSpecType;
	}
	public String getDefSpecCode() {
		return defSpecCode;
	}
	public void setDefSpecCode(String defSpecCode) {
		this.defSpecCode = defSpecCode;
	}
	public String getDefDescriptionText() {
		return defDescriptionText;
	}
	public void setDefDescriptionText(String defDescriptionText) {
		this.defDescriptionText = defDescriptionText;
	}
	public String getDefCommentsText() {
		return defCommentsText;
	}
	public void setDefCommentsText(String defCommentsText) {
		this.defCommentsText = defCommentsText;
	}
	public ImportSpecModel getImportSpecModel() {
		return importSpecModel;
	}
	public void setImportSpecModel(ImportSpecModel importSpecModel) {
		this.importSpecModel = importSpecModel;
	}
	public ImportConnectionModel getImportConnectionModel() {
		return importConnectionModel;
	}
	public void setImportConnectionModel(ImportConnectionModel importConnectionModel) {
		this.importConnectionModel = importConnectionModel;
	}
	public ImportDefinitionModel getImportDefinitionModel() {
		return importDefinitionModel;
	}
	public void setImportDefinitionModel(ImportDefinitionModel importDefinitionModel) {
		this.importDefinitionModel = importDefinitionModel;
	}
	public HtmlDataTable getImportSpecTable() {
		System.out.println("getImportSpecTable");
		if (importSpecTable == null) {
			
			importSpecTable = new HtmlDataTable();
			importSpecTable.setId("importSpecList");
			importSpecTable.setVar("currrow");
			importSpecTable.setRowClasses("odd-row,even-row");
			
			Map<String,DataDefinitionColumn> propertyMap =  this.getMatrixCommonBean().getImportSpecPropertyMap();
			String maxdecimalplace = filepreferenceBean.getMaxdecimalplace();
			MatrixCommonBean.createDynamicTable(ImportSpec.class, importSpecTable, 
					"importDefinitionAndSpecsBean", "importDefinitionAndSpecsBean.importSpecModel", 
					importSpecColumns, new LinkedHashSet<String>(), new LinkedHashSet<String>(), new LinkedHashSet<String>(), 
					propertyMap, false, false, maxdecimalplace);
		}
		
		return importSpecTable;
	}
	public void setImportSpecTable(HtmlDataTable importSpecTable) {
		this.importSpecTable = importSpecTable;
	}
	public HtmlDataTable getImportConnectionTable() {
		System.out.println("getImportConnectionTable");
		if (importConnectionTable == null) {
			
			importConnectionTable = new HtmlDataTable();
			importConnectionTable.setId("importConnectionList");
			importConnectionTable.setVar("currrow");
			importConnectionTable.setRowClasses("odd-row,even-row");
			
			Map<String,DataDefinitionColumn> propertyMap =  this.getMatrixCommonBean().getImportConnectionPropertyMap();
			String maxdecimalplace = filepreferenceBean.getMaxdecimalplace();
			MatrixCommonBean.createDynamicTable(ImportConnection.class, importConnectionTable, 
					"importDefinitionAndSpecsBean", "importDefinitionAndSpecsBean.importConnectionModel", 
					importConnectionColumns, new LinkedHashSet<String>(), new LinkedHashSet<String>(), new LinkedHashSet<String>(), 
					propertyMap, false, false, maxdecimalplace);
		}
		return importConnectionTable;
	}
	public void setImportConnectionTable(HtmlDataTable importConnectionTable) {
		this.importConnectionTable = importConnectionTable;
	}
	
	public HtmlDataTable getImportDefinitionTable() {
		System.out.println("getImportDefinitionTable");
		if (importDefinitionTable == null) {
			
			importDefinitionTable = new HtmlDataTable();
			importDefinitionTable.setId("importDefinitionList");
			importDefinitionTable.setVar("currrow");
			importDefinitionTable.setRowClasses("odd-row,even-row");
			
			Map<String,DataDefinitionColumn> propertyMap =  this.getMatrixCommonBean().getImportDefinitionPropertyMap();
			String maxdecimalplace = filepreferenceBean.getMaxdecimalplace();
			MatrixCommonBean.createDynamicTable(ImportDefinition.class, importDefinitionTable, 
					"importDefinitionAndSpecsBean", "importDefinitionAndSpecsBean.importDefinitionModel", 
					importDefinitionColumns, new LinkedHashSet<String>(), new LinkedHashSet<String>(), new LinkedHashSet<String>(), 
					propertyMap, false, false, maxdecimalplace);
			
		}
		return importDefinitionTable;
	}
	

	
	public BaseExtendedDataModel getComponentDetailsDataModel() {
		return null;
	}
	
	public void setComponentDetailsDataModel(BaseExtendedDataModel model) {
		
	}
	
	
	public void setImportDefinitionTable(HtmlDataTable importDefinitionTable) {
		this.importDefinitionTable = importDefinitionTable;
	}
	public MatrixCommonBean getMatrixCommonBean() {
		return matrixCommonBean;
	}
	public void setMatrixCommonBean(MatrixCommonBean matrixCommonBean) {
		this.matrixCommonBean = matrixCommonBean;
	}
	
	public void selectedItemChanged(ActionEvent e) {
		UIComponent uiComponent = e.getComponent().getParent();
		UIDataAdaptor table = (UIDataAdaptor) uiComponent;		
		Object selectedRow =  table.getRowData();	
		
		if (selectedRow instanceof ImportSpec) {
			selectedSpecRowChanged(e);
		} else if (selectedRow instanceof ImportDefinition) {
			selectedDefRowChanged(e);
		} else if (selectedRow instanceof ImportConnection) {
			selectedConnectionRowChanged(e);
		}
	}
	
	public String getUsageNUsersFormCaption() {
		String result = null;
		if (screenType.equals(ScreenType.Connection)) {
			result = "Definitions Using Connection";
		} else if (screenType.equals(ScreenType.Spec)) {
			result = "Authorized Users For Spec";
		} else if (screenType.equals(ScreenType.Definition)) {
			result = "Specs Using Definition";
		}
		return result;
	}
	public List<ImportDefDTO> getDefinitionsUsingConnection() {
		return definitionsUsingConnection;
	}
	public void setDefinitionsUsingConnection(
			List<ImportDefDTO> definitionsUsingConnection) {
		this.definitionsUsingConnection = definitionsUsingConnection;
	}
	public ImportConnection getSelectedImportConnectionComponent() {
		return selectedImportConnectionComponent;
	}
	public ImportConnection getIndexImportConnectionComponent() {
		return indexImportConnectionComponent;
	}
	public void setSelectedImportConnectionComponent(
			ImportConnection selectedImportConnectionComponent) {
		this.selectedImportConnectionComponent = selectedImportConnectionComponent;
	}
	public boolean isDoShowConnectionPropertiesSubForm() {
		return (selectedImportConnectionComponent!=null && selectedImportConnectionComponent.getImportConnectionType()!=null && "ORACLE".equalsIgnoreCase(selectedImportConnectionComponent.getImportConnectionType()) );
	}

	public ImportDefsAndSpecsAction getCurrentAction() {
		return currentAction;
	}
	
	public String verifyWhereClause() {
		verificationErrorMessage = "";
		
		if(selectedImportSpec.getImportDefinitionCode()==null || selectedImportSpec.getImportDefinitionCode().length()==0){
			verificationErrorMessage = "Import Definition Code not found";
			return "import_spec_update"; 
		}
		
		ImportDefinition importDefinition = importDefAndSpecsService.find(selectedImportSpec.getImportDefinitionCode());
		if(importDefinition==null || importDefinition.getImportFileTypeCode()==null){
			verificationErrorMessage = "Import File Type Code not found";
			return "import_spec_update";
		}
		else if(!importDefinition.getImportFileTypeCode().equalsIgnoreCase("DB")){
			return "";
		}
		
		List<ImportConnection> importConnections = importDefAndSpecsService.getConnectionsByCode(importDefinition.getImportConnectionCode());		
		if(importConnections==null || importConnections.size()==0){
			verificationErrorMessage = "Import Connection Code not found";
			return "import_spec_update";
		}
			
		ImportConnection importConnection = importConnections.get(0);
		
		String tableName = importDefinition.getDbTableName();
    	String whereClause = selectedImportSpec.getWhereClause();
    	if(whereClause==null || whereClause.length()==0){
    		verificationErrorMessage = "";//No verification is needed
    		return "import_spec_update";
    	}

		try {
			Class.forName(importConnection.getDbDriverClassName());
	    } catch (ClassNotFoundException e) {
	    	verificationErrorMessage =  "Can't find class " + importConnection.getDbDriverClassName();
	    	return "import_spec_update";
	    }

	    Connection conn = null;
	    Statement stmt = null;
	    ResultSet rset = null;
	    try {
	    	conn = DriverManager.getConnection(importConnection.getDbUrl(), importConnection.getDbUserName(), importConnection.getDbPassword());

	    
	    	stmt = conn.createStatement();
	    	rset = stmt.executeQuery("select count(*) from " + tableName + " where " + whereClause);
	    	
	    	rset.close();rset = null;
	    	stmt.close();stmt = null;
	    	conn.close();conn = null;
	    	
	    	verificationErrorMessage = "Verification succeeded";
	    } catch (SQLException e) {
	    	verificationErrorMessage =  "SQL error: " + e.getMessage();
	    } finally {
	      if (rset != null)
	        try {
	          rset.close();
	        } catch (SQLException ignore) {
	        }
	      if (stmt != null)
	        try {
	          stmt.close();
	        } catch (SQLException ignore) {
	        }
	      if (conn != null)
	        try {
	          conn.close();
	        } catch (SQLException ignore) {
	        }
	    }
		return "import_spec_update";
	}

	public String verifyConnProperties() {
		verificationErrorMessage = "";
		try {
			Class.forName(selectedImportConnectionComponent.getDbDriverClassName());
	    } catch (ClassNotFoundException e) {
	    	verificationErrorMessage =  "Can't find class " + selectedImportConnectionComponent.getDbDriverClassName();
	    	return "import_conn_add";
	    }

	    Connection conn = null;
	    try {
	    	conn = DriverManager.getConnection(selectedImportConnectionComponent.getDbUrl(), selectedImportConnectionComponent.getDbUserName(), selectedImportConnectionComponent.getDbPassword());
	    	conn.close();conn = null;
	    	
	    	verificationErrorMessage = "Connection succeeded";
	    } catch (SQLException e) {
	    	verificationErrorMessage =  "SQL error: " + e.getMessage();
	    } finally {
	      if (conn != null)
	        try {
	          conn.close();
	        } catch (SQLException ignore) {
	        }
	    }
		return "import_conn_add";
	}
	public String getVerificationErrorMessage() {
		return verificationErrorMessage;
	}
	public void setVerificationErrorMessage(String verificationErrorMessage) {
		this.verificationErrorMessage = verificationErrorMessage;
	}
	
	private String flatDelCharSelected;
	private String flatDelCharEntered;

	public String getFlatDelCharSelected() {
		return flatDelCharSelected;
	}

	public void setFlatDelCharSelected(String flatDelCharSelected) {
		this.flatDelCharSelected = flatDelCharSelected;
	}
	
	public String getFlatDelCharEntered() {
		return flatDelCharEntered;
	}

	public void setFlatDelCharEntered(String flatDelCharEntered) {
		this.flatDelCharEntered = flatDelCharEntered;
	}

	public Map<String, String>  getAdministrativeMap(){
		if(administrativeMap==null){
			administrativeMap = new LinkedHashMap<String, String>();
			administrativeMap.put("0","Normal");
			administrativeMap.put("1","Administrative");
			administrativeMap.put("2","System");
	    }
		return administrativeMap;
	}
	
	/*
	 ~t : tab, 
	~n : new line, 
	~f : form feed, 
	~b :backspace 
	� : �
	Other:

	 */
	
	public User getCurrentUser() {
		return loginservice.getUser();
   }
}

