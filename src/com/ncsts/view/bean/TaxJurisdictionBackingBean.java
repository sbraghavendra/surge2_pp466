/**
 * This is the Backing Bean for the Tax Jurisdiction and Tax Rate Maintenance screens.
 * 
 * There are two main screens, the first is a Search and Selection screen where the user searches for Tax Jurisdictions or Tax Rates they want to manipulate. The second is the action page which
 * provides the user an interface to perform the manipulation.
 * 
 * All the actions (add, copy/add, update, delete and view) for both Tax Jurisdictions and Tax Rates are performed using the action page.
 * 
 * Several parameters are passed along to the JSP/JSF page which control layout and accessibility of widgets on the screen.
 * 
 * @author Jay Jungalwala
 * 
 * @version 1.0
 * 
 */
package com.ncsts.view.bean;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UISelectItems;
import javax.faces.component.html.HtmlDataTable;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlSelectBooleanCheckbox;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;

import org.ajax4jsf.component.UIDataAdaptor;
import org.ajax4jsf.component.html.HtmlAjaxSupport;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.myfaces.component.html.ext.HtmlOutputText;
import org.richfaces.component.html.HtmlCalendar;
import org.richfaces.component.html.HtmlModalPanel;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.ncsts.common.CacheManager;
import com.ncsts.common.LoggerFactory;
import com.ncsts.common.constants.PurchaseTransactionServiceConstants;
import com.ncsts.domain.Jurisdiction;
import com.ncsts.domain.JurisdictionTaxrate;
import com.ncsts.domain.ListCodes;
import com.ncsts.domain.Option;
import com.ncsts.domain.OptionCodePK;
import com.ncsts.domain.User;
import com.ncsts.dto.FlagValue;
import com.ncsts.dto.TaxCodeStateDTO;
import com.ncsts.dto.UserDTO;
import com.ncsts.exception.PurchaseTransactionProcessingException;
import com.ncsts.jsf.model.JurisdictionDataModel;
import com.ncsts.services.JurisdictionService;
import com.ncsts.services.JurisdictionTaxrateService;
import com.ncsts.services.ListCodesService;
import com.ncsts.services.OptionService;
import com.ncsts.services.PurchaseTransactionService;
import com.ncsts.services.TaxCodeStateService;
import com.ncsts.view.bean.BaseMatrixBackingBean.FindIdCallback;
import com.ncsts.view.util.ArithmeticUtils;
import com.ncsts.view.util.TogglePanelController;

@SuppressWarnings("unchecked")
public class TaxJurisdictionBackingBean {

  private static Logger logger = LoggerFactory.getInstance().getLogger(
      TaxJurisdictionBackingBean.class);

  private static final String PARAM_DISABLED = "disabled";

  private static final String VALUE_FALSE = "0";

  private static final String VALUE_TRUE = "1";

  private static final String MODE_VIEW = "view"; // TaxJurisdiction & Tax Rate
                                                  // Read Only

  private static final String MODE_EDIT = "edit"; // TaxJurisdiction & Tax Rate
                                                  // Editable

  private static final String MODE_EDITTR = "edittr"; // TaxJurisdiction
                                                      // Read-Only & Tax Rate
                                                      // Editable

  @SuppressWarnings("unused")
  private static final String ACTION_TYPE_JURISDICTION = "Jurisdiction";

  @SuppressWarnings("unused")
  private static final String ACTION_TYPE_TAXRATE = "TaxRate";

  private static final String UI_VISABLE = "display:block;";

  private static final String UI_HIDDEN = "display:none;";

  private static final String optionCode = "TAXWARNRATE";

  private static final String system = "SYSTEM";
  
  private static final String codeTypeCode = "RATETYPE";
  
  public static final Long MAX_AMOUNT = 999999999l;

  // Initialize Services
  protected CacheManager cacheManager;

  protected TaxCodeStateService taxCodeStateService;

  protected JurisdictionService jurisdictionService;

  protected JurisdictionTaxrateService taxRateService;

  protected OptionService optionService;
  
  protected ListCodesService listCodesService;
  
  protected TransactionDetailBean transactionDetailBean;
  
  @Autowired
  private PurchaseTransactionService purchaseTransactionService;

  // Search Fields
  private Long searchjurisdictionId;

  private String searchgeocode;

  private String searchstate;
  
  private String searchstateDetail;

  private String searchcounty;

  private String searchcity;// = "boston";

  private String searchzip;
  
  private String searchzipplus4;

  private String searchclientGeocode;

  private String searchcompGeocode;
  
  private String searchstj;
  
  private String searchratetype;

  private Long searchjurisdictionTaxrateId;

  private Date searcheffectiveDate;
 
  private Date searchexpirationDate;

  private boolean searchmodifiedFlag;

  private boolean showOkButton;
  // UI Items
  private HtmlSelectOneMenu stateMenu = new HtmlSelectOneMenu();

  private HtmlSelectOneMenu inOutMenu = new HtmlSelectOneMenu();

  private HtmlSelectOneMenu rateTypeCodeMenu = new HtmlSelectOneMenu();

  private List<JurisdictionTaxrate> taxRateList = new ArrayList<JurisdictionTaxrate>();

  private List<SelectItem> rateTypeCodeItems = null;
  private List<SelectItem> countyLocalItems = null;

  private List<SelectItem> stateMenuItems;
  private List<SelectItem> stateMenuItemsDetail;
  
  private boolean rateUsed = false;

  //PP-24
  //stateSplitAmountInputText = new HtmlInputText();
  
  //start of country
  //Country Tier 1
  private HtmlInputText countrySalesTier1RateInputText = new HtmlInputText();
  private HtmlInputText countrySalesTier1SetamtInputText = new HtmlInputText();
  private HtmlInputText countrySalesTier1MaxAmtInputText = new HtmlInputText();
  private HtmlSelectBooleanCheckbox countrySalesTier1SetToMaximumCheckBind = new HtmlSelectBooleanCheckbox();
  private HtmlSelectBooleanCheckbox countrySalesTier2SetToMaximumCheckBind = new HtmlSelectBooleanCheckbox();
  private HtmlSelectBooleanCheckbox countrySalesTier2TaxEntireAmountCheckBind = new HtmlSelectBooleanCheckbox();
  private HtmlSelectBooleanCheckbox countrySalesTier3TaxEntireAmountCheckBind = new HtmlSelectBooleanCheckbox();
  
  private HtmlInputText countryUseTier1RateInputText = new HtmlInputText();
  private HtmlInputText countryUseTier1SetamtInputText = new HtmlInputText(); //new
  private HtmlInputText countryUseTier1MaxAmtInputText = new HtmlInputText();
  private HtmlSelectBooleanCheckbox countryUseTier1SetToMaximumCheckBind = new HtmlSelectBooleanCheckbox();
  private HtmlSelectBooleanCheckbox countryUseTier2SetToMaximumCheckBind = new HtmlSelectBooleanCheckbox();
  private HtmlSelectBooleanCheckbox countryUseTier2TaxEntireAmountCheckBind = new HtmlSelectBooleanCheckbox();
  private HtmlSelectBooleanCheckbox countryUseTier3TaxEntireAmountCheckBind = new HtmlSelectBooleanCheckbox();
  
  //Country Tier 2
  private HtmlInputText countrySalesTier2MinAmtInputText = new HtmlInputText();
  private HtmlInputText countrySalesTier2RateInputText  = new HtmlInputText();
  private HtmlInputText countrySalesTier2SetamtInputText = new HtmlInputText(); 
  private HtmlInputText countrySalesTier2MaxAmtInputText = new HtmlInputText();
  
  private HtmlInputText countryUseTier2MinAmtInputText = new HtmlInputText();
  private HtmlInputText countryUseTier2RateInputText  = new HtmlInputText(); 
  private HtmlInputText countryUseTier2SetamtInputText = new HtmlInputText(); 
  private HtmlInputText countryUseTier2MaxAmtInputText = new HtmlInputText();
  
  //Country Tier 3
  private HtmlSelectBooleanCheckbox countrySameAsSalesRatesCheckBind = new HtmlSelectBooleanCheckbox();
  
  private HtmlInputText countrySalesTier3RateInputText  = new HtmlInputText(); 
  private HtmlInputText countrySalesTier3SetamtInputText = new HtmlInputText(); 
  private HtmlInputText countryUseTier3RateInputText  = new HtmlInputText(); 
  private HtmlInputText countryUseTier3SetamtInputText = new HtmlInputText();  
  private HtmlInputText countrySalesMaxtaxAmtInputText = new HtmlInputText();
  private HtmlInputText countryUseMaxtaxAmtInputText = new HtmlInputText();
  
  private boolean countrySalesTier1SetToMaximumCheck = false;
  private boolean countrySalesTier2SetToMaximumCheck = false;
  private boolean countryUseTier1SetToMaximumCheck = false;
  private boolean countryUseTier2SetToMaximumCheck = false; 
  private boolean countrySameAsSalesRatesCheck = false;
  
  private boolean countrySalesTier2TaxEntireAmountCheck = false;
  private boolean countrySalesTier3TaxEntireAmountCheck = false;
  private boolean countryUseTier2TaxEntireAmountCheck = false;
  private boolean countryUseTier3TaxEntireAmountCheck = false;
  //end of country

  //start of state
  //state Tier 1
  private HtmlInputText stateSalesTier1RateInputText = new HtmlInputText();
  private HtmlInputText stateSalesTier1SetamtInputText = new HtmlInputText();
  private HtmlInputText stateSalesTier1MaxAmtInputText = new HtmlInputText();
  private HtmlSelectBooleanCheckbox stateSalesTier1SetToMaximumCheckBind = new HtmlSelectBooleanCheckbox();
  private HtmlSelectBooleanCheckbox stateSalesTier2SetToMaximumCheckBind = new HtmlSelectBooleanCheckbox();
  private HtmlSelectBooleanCheckbox stateSalesTier2TaxEntireAmountCheckBind = new HtmlSelectBooleanCheckbox();
  private HtmlSelectBooleanCheckbox stateSalesTier3TaxEntireAmountCheckBind = new HtmlSelectBooleanCheckbox();
  
  private HtmlInputText stateUseTier1RateInputText = new HtmlInputText();
  private HtmlInputText stateUseTier1SetamtInputText = new HtmlInputText(); //new
  private HtmlInputText stateUseTier1MaxAmtInputText = new HtmlInputText();
  private HtmlSelectBooleanCheckbox stateUseTier1SetToMaximumCheckBind = new HtmlSelectBooleanCheckbox();
  private HtmlSelectBooleanCheckbox stateUseTier2SetToMaximumCheckBind = new HtmlSelectBooleanCheckbox();
  private HtmlSelectBooleanCheckbox stateUseTier2TaxEntireAmountCheckBind = new HtmlSelectBooleanCheckbox();
  private HtmlSelectBooleanCheckbox stateUseTier3TaxEntireAmountCheckBind = new HtmlSelectBooleanCheckbox();
  
  //state Tier 2
  private HtmlInputText stateSalesTier2MinAmtInputText = new HtmlInputText();
  private HtmlInputText stateSalesTier2RateInputText  = new HtmlInputText();
  private HtmlInputText stateSalesTier2SetamtInputText = new HtmlInputText(); 
  private HtmlInputText stateSalesTier2MaxAmtInputText = new HtmlInputText();
  
  private HtmlInputText stateUseTier2MinAmtInputText = new HtmlInputText();
  private HtmlInputText stateUseTier2RateInputText  = new HtmlInputText(); 
  private HtmlInputText stateUseTier2SetamtInputText = new HtmlInputText(); 
  private HtmlInputText stateUseTier2MaxAmtInputText = new HtmlInputText();
  
  //state Tier 3
  private HtmlSelectBooleanCheckbox stateSameAsSalesRatesCheckBind = new HtmlSelectBooleanCheckbox();
  
  private HtmlInputText stateSalesTier3RateInputText  = new HtmlInputText(); 
  private HtmlInputText stateSalesTier3SetamtInputText = new HtmlInputText(); 
  private HtmlInputText stateUseTier3RateInputText  = new HtmlInputText(); 
  private HtmlInputText stateUseTier3SetamtInputText = new HtmlInputText(); 
  private HtmlInputText stateSalesMaxtaxAmtInputText = new HtmlInputText();
  private HtmlInputText stateUseMaxtaxAmtInputText = new HtmlInputText();
  
  private boolean stateSalesTier1SetToMaximumCheck = false;
  private boolean stateSalesTier2SetToMaximumCheck = false;
  private boolean stateUseTier1SetToMaximumCheck = false;
  private boolean stateUseTier2SetToMaximumCheck = false; 
  private boolean stateSameAsSalesRatesCheck = false;
  
  private boolean stateSalesTier2TaxEntireAmountCheck = false;
  private boolean stateSalesTier3TaxEntireAmountCheck = false;
  private boolean stateUseTier2TaxEntireAmountCheck = false;
  private boolean stateUseTier3TaxEntireAmountCheck = false;
  //end of state
  
  //start of county
  //county Tier 1
  private HtmlInputText countySalesTier1RateInputText = new HtmlInputText();
  private HtmlInputText countySalesTier1SetamtInputText = new HtmlInputText();
  private HtmlInputText countySalesTier1MaxAmtInputText = new HtmlInputText();
  private HtmlSelectBooleanCheckbox countySalesTier1SetToMaximumCheckBind = new HtmlSelectBooleanCheckbox();
  private HtmlSelectBooleanCheckbox countySalesTier2SetToMaximumCheckBind = new HtmlSelectBooleanCheckbox();
  private HtmlSelectBooleanCheckbox countySalesTier2TaxEntireAmountCheckBind = new HtmlSelectBooleanCheckbox();
  private HtmlSelectBooleanCheckbox countySalesTier3TaxEntireAmountCheckBind = new HtmlSelectBooleanCheckbox();
  
  private HtmlInputText countyUseTier1RateInputText = new HtmlInputText();
  private HtmlInputText countyUseTier1SetamtInputText = new HtmlInputText(); //new
  private HtmlInputText countyUseTier1MaxAmtInputText = new HtmlInputText();
  private HtmlSelectBooleanCheckbox countyUseTier1SetToMaximumCheckBind = new HtmlSelectBooleanCheckbox();
  private HtmlSelectBooleanCheckbox countyUseTier2SetToMaximumCheckBind = new HtmlSelectBooleanCheckbox();
  private HtmlSelectBooleanCheckbox countyUseTier2TaxEntireAmountCheckBind = new HtmlSelectBooleanCheckbox();
  private HtmlSelectBooleanCheckbox countyUseTier3TaxEntireAmountCheckBind = new HtmlSelectBooleanCheckbox();
  
  //county Tier 2
  private HtmlInputText countySalesTier2MinAmtInputText = new HtmlInputText();
  private HtmlInputText countySalesTier2RateInputText  = new HtmlInputText();
  private HtmlInputText countySalesTier2SetamtInputText = new HtmlInputText(); 
  private HtmlInputText countySalesTier2MaxAmtInputText = new HtmlInputText();
  
  private HtmlInputText countyUseTier2MinAmtInputText = new HtmlInputText();
  private HtmlInputText countyUseTier2RateInputText  = new HtmlInputText(); 
  private HtmlInputText countyUseTier2SetamtInputText = new HtmlInputText(); 
  private HtmlInputText countyUseTier2MaxAmtInputText = new HtmlInputText();
  
  //county Tier 3
  private HtmlSelectBooleanCheckbox countySameAsSalesRatesCheckBind = new HtmlSelectBooleanCheckbox();
  
  private HtmlInputText countySalesTier3RateInputText  = new HtmlInputText(); 
  private HtmlInputText countySalesTier3SetamtInputText = new HtmlInputText(); 
  private HtmlInputText countyUseTier3RateInputText  = new HtmlInputText(); 
  private HtmlInputText countyUseTier3SetamtInputText = new HtmlInputText(); 
  private HtmlInputText countySalesMaxtaxAmtInputText = new HtmlInputText();
  private HtmlInputText countyUseMaxtaxAmtInputText = new HtmlInputText();
  
  private boolean countySalesTier1SetToMaximumCheck = false;
  private boolean countySalesTier2SetToMaximumCheck = false;
  private boolean countyUseTier1SetToMaximumCheck = false;
  private boolean countyUseTier2SetToMaximumCheck = false; 
  private boolean countySameAsSalesRatesCheck = false;
  
  private boolean countySalesTier2TaxEntireAmountCheck = false;
  private boolean countySalesTier3TaxEntireAmountCheck = false;
  private boolean countyUseTier2TaxEntireAmountCheck = false;
  private boolean countyUseTier3TaxEntireAmountCheck = false;
  //end of county
  
  //start of city
  //city Tier 1
  private HtmlInputText citySalesTier1RateInputText = new HtmlInputText();
  private HtmlInputText citySalesTier1SetamtInputText = new HtmlInputText();
  private HtmlInputText citySalesTier1MaxAmtInputText = new HtmlInputText();
  private HtmlSelectBooleanCheckbox citySalesTier1SetToMaximumCheckBind = new HtmlSelectBooleanCheckbox();
  private HtmlSelectBooleanCheckbox citySalesTier2SetToMaximumCheckBind = new HtmlSelectBooleanCheckbox();
  private HtmlSelectBooleanCheckbox citySalesTier2TaxEntireAmountCheckBind = new HtmlSelectBooleanCheckbox();
  private HtmlSelectBooleanCheckbox citySalesTier3TaxEntireAmountCheckBind = new HtmlSelectBooleanCheckbox();
  
  private HtmlInputText cityUseTier1RateInputText = new HtmlInputText();
  private HtmlInputText cityUseTier1SetamtInputText = new HtmlInputText(); //new
  private HtmlInputText cityUseTier1MaxAmtInputText = new HtmlInputText();
  private HtmlSelectBooleanCheckbox cityUseTier1SetToMaximumCheckBind = new HtmlSelectBooleanCheckbox();
  private HtmlSelectBooleanCheckbox cityUseTier2SetToMaximumCheckBind = new HtmlSelectBooleanCheckbox();
  private HtmlSelectBooleanCheckbox cityUseTier2TaxEntireAmountCheckBind = new HtmlSelectBooleanCheckbox();
  private HtmlSelectBooleanCheckbox cityUseTier3TaxEntireAmountCheckBind = new HtmlSelectBooleanCheckbox();
  
  //city Tier 2
  private HtmlInputText citySalesTier2MinAmtInputText = new HtmlInputText();
  private HtmlInputText citySalesTier2RateInputText  = new HtmlInputText();
  private HtmlInputText citySalesTier2SetamtInputText = new HtmlInputText(); 
  private HtmlInputText citySalesTier2MaxAmtInputText = new HtmlInputText();
  
  private HtmlInputText cityUseTier2MinAmtInputText = new HtmlInputText();
  private HtmlInputText cityUseTier2RateInputText  = new HtmlInputText(); 
  private HtmlInputText cityUseTier2SetamtInputText = new HtmlInputText(); 
  private HtmlInputText cityUseTier2MaxAmtInputText = new HtmlInputText();
  
  //city Tier 3
  private HtmlSelectBooleanCheckbox citySameAsSalesRatesCheckBind = new HtmlSelectBooleanCheckbox();
  
  private HtmlSelectBooleanCheckbox stjSameAsSalesRatesCheckBind = new HtmlSelectBooleanCheckbox();
  
  private HtmlInputText citySalesTier3RateInputText  = new HtmlInputText(); 
  private HtmlInputText citySalesTier3SetamtInputText = new HtmlInputText(); 
  private HtmlInputText cityUseTier3RateInputText  = new HtmlInputText(); 
  private HtmlInputText cityUseTier3SetamtInputText = new HtmlInputText(); 
  private HtmlInputText citySalesMaxtaxAmtInputText = new HtmlInputText();
  private HtmlInputText cityUseMaxtaxAmtInputText = new HtmlInputText();
  
  private boolean citySalesTier1SetToMaximumCheck = false;
  private boolean citySalesTier2SetToMaximumCheck = false;
  private boolean cityUseTier1SetToMaximumCheck = false;
  private boolean cityUseTier2SetToMaximumCheck = false; 
  private boolean citySameAsSalesRatesCheck = false;
  
  private boolean citySalesTier2TaxEntireAmountCheck = false;
  private boolean citySalesTier3TaxEntireAmountCheck = false;
  private boolean cityUseTier2TaxEntireAmountCheck = false;
  private boolean cityUseTier3TaxEntireAmountCheck = false;
  //end of city
  
  private HtmlInputText stj1SalesSetamtInputText  = new HtmlInputText();
  private HtmlInputText stj1UseSetamtInputText  = new HtmlInputText();
  private HtmlInputText stj2SalesSetamtInputText  = new HtmlInputText();
  private HtmlInputText stj2UseSetamtInputText  = new HtmlInputText();
  private HtmlInputText stj3SalesSetamtInputText  = new HtmlInputText();
  private HtmlInputText stj3UseSetamtInputText  = new HtmlInputText();
  private HtmlInputText stj4SalesSetamtInputText  = new HtmlInputText();
  private HtmlInputText stj4UseSetamtInputText  = new HtmlInputText();  
  private HtmlInputText stj5SalesSetamtInputText  = new HtmlInputText();
  private HtmlInputText stj5UseSetamtInputText  = new HtmlInputText();
  
  private boolean stjSameAsSalesRatesCheck = false;
  
  private HtmlInputText stj1UseRateInputText = new HtmlInputText();
  private HtmlInputText stj2UseRateInputText = new HtmlInputText();
  private HtmlInputText stj3UseRateInputText = new HtmlInputText();
  private HtmlInputText stj4UseRateInputText = new HtmlInputText();
  private HtmlInputText stj5UseRateInputText = new HtmlInputText();
  private HtmlInputText stj1SalesRateInputText = new HtmlInputText();
  private HtmlInputText stj2SalesRateInputText = new HtmlInputText();
  private HtmlInputText stj3SalesRateInputText = new HtmlInputText();
  private HtmlInputText stj4SalesRateInputText = new HtmlInputText();
  private HtmlInputText stj5SalesRateInputText = new HtmlInputText();

  private HtmlOutputText useWarningOutputText = new HtmlOutputText();
  private HtmlOutputText salesWarningOutputText = new HtmlOutputText();
  
  private HtmlInputText combinedUseRate = new HtmlInputText();

  private HtmlInputText combinedSalesRate = new HtmlInputText();
  
  private HtmlInputText stateMaxtaxAmount = new HtmlInputText();
  
  private HtmlInputText stateUseTier2RateInput = new HtmlInputText();
  private HtmlInputText stateSalesTier2RateInput = new HtmlInputText();
  
  private HtmlCalendar effectiveDate = new HtmlCalendar();
  
  private HtmlCalendar expirationDate = new HtmlCalendar();

  private String trShowSection; // Used by JSF to show/hide Tax Rate Section

  // Variables for Message/Errors handler
  private List<String> messages = new ArrayList<String>();

  private HtmlDataTable formMessagesDataTable = new HtmlDataTable();

  private HtmlModalPanel messagesModalPanel;

  private Boolean showCancelButton; // Stores whether to show the cancel button
                                    // or not

  // Tax Jurisdiction Selection(s)
  private Jurisdiction selectedJurisdiction; // JuridictionDTO for

  private Jurisdiction editJurisdiction; // JuridictionDTO to be
                                          // Created/Deleted/Modified

  @SuppressWarnings("unused")
  private Jurisdiction origJurisdiction; // JuridictionDTO for comparison

  @SuppressWarnings("unused")
  private List<Jurisdiction> searchStateCityZipCounty = new ArrayList<Jurisdiction>();

  private int selectedJurisdictionIndex = -1;

  // Tax Rate Selection(s)
  private JurisdictionTaxrate selectedTaxRate; // TaxRate for Selection

  private JurisdictionTaxrate editTaxRate; // TaxRate to be
                                            // Created/Deleted/Modified

  private JurisdictionTaxrate origTaxRate; // TaxRate for comparison

  private int selectedTaxRateIndex = -1;

  // Search elements
  private Jurisdiction srchJurisdiction = new Jurisdiction();

  private JurisdictionTaxrate srchTaxrate = new JurisdictionTaxrate();
  
  private Jurisdiction jurisdictionFilter = null;

  private JurisdictionTaxrate taxrateFilter = null;

  private int rowCount = 0;

  private Locale locale = Locale.US;


  // UI Elements and Attribute containers
  private Map<String, Map<String, Map<String, String>>> actionMap = new HashMap<String, Map<String, Map<String, String>>>();

  private Map<TaxJurisdictionAction, Map<String, FieldOptions>> newActionMap = new HashMap<TaxJurisdictionAction, Map<String, FieldOptions>>();

  private Map<String, Integer> fieldSizeMap = new HashMap<String, Integer>();

  private TaxJurisdictionAction currentAction;

  private String currentMode; // VIEW or EDIT

  private List<SelectItem> customFlagMenuItems;

  private String customFlagFilter;

  private List<SelectItem> modifiedFlagMenuItems;

  private String modifiedFlagFilter;

  private JurisdictionDataModel jurisdictionDataModel;
  
  
  private List<Jurisdiction> jurisdictionList = new ArrayList<Jurisdiction>();
  
  private List<ListCodes> listCodesList = new ArrayList<ListCodes>();

  private Map<String, String> rateTypeCodeToRateTypeMap;
  
  private Boolean useWarning = false;
  
  private Boolean salesWarning = false;
  
  private Boolean warningsShown = false;
  
  private String searchCountry="";
  private String searchCountryDetail="";
  
  private List<SelectItem> nexusIndItems;
  
  private SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
  
  private SearchJurisdictionHandler addressHandler;
  
  private String rateOptionFlag = "2";
  
	private FindIdCallback findIdCallback = null;
	private String findIdAction;
	private String findIdContext;
	private String findDefaultBatchType="";
	
	private TogglePanelController togglePanelController = null;
	
	@Autowired
	private loginBean loginBean;
	
	public  TogglePanelController getTogglePanelController(){
		if(togglePanelController==null){
			togglePanelController = new TogglePanelController();
			togglePanelController.addTogglePanel("TaxRatesPanel", true);
			togglePanelController.addTogglePanel("MiscInformationPanel", false);
		}
		return togglePanelController;
	}
	
	public void setTogglePanelController(TogglePanelController togglePanelController){
		this.togglePanelController = togglePanelController;;
	}
	
	public void findId(FindIdCallback callback, String context, String action, Jurisdiction jurisdictionFilter) {
		findIdCallback = callback;
		findIdContext = context;
		findIdAction = action;
		
		resetSearch();		
		if(jurisdictionFilter!=null){
			if(jurisdictionFilter.getJurisdictionId()!=null && jurisdictionFilter.getJurisdictionId()!=0L){
				searchjurisdictionId = jurisdictionFilter.getJurisdictionId();
			}
			if(jurisdictionFilter.getGeocode()!=null && jurisdictionFilter.getGeocode().trim().length()>0){
				searchgeocode = jurisdictionFilter.getGeocode().trim();
			}
			if(jurisdictionFilter.getCountry()!=null && jurisdictionFilter.getCountry().trim().length()>0){
				searchCountry = jurisdictionFilter.getCountry().trim();
			}
			if(jurisdictionFilter.getState()!=null && jurisdictionFilter.getState().trim().length()>0){
				searchstate = jurisdictionFilter.getState().trim();
			}
			if(jurisdictionFilter.getCounty()!=null && jurisdictionFilter.getCounty().trim().length()>0){
				searchcounty = jurisdictionFilter.getCounty().trim();
			}
			if(jurisdictionFilter.getCity()!=null && jurisdictionFilter.getCity().trim().length()>0){
				searchcity = jurisdictionFilter.getCity().trim();
			}
			if(jurisdictionFilter.getZip()!=null && jurisdictionFilter.getZip().trim().length()>0){
				searchzip = jurisdictionFilter.getZip().trim();
			}
			if(jurisdictionFilter.getZipplus4()!=null && jurisdictionFilter.getZipplus4().trim().length()>0){
				searchzipplus4 = jurisdictionFilter.getZipplus4().trim();
			}
			
			searchJurisdiction();
		}
	}
	
	public String findAction() {
		Long selectedJurisdictionId = 0L;
		if(selectedJurisdiction!=null && selectedJurisdiction.getJurisdictionId()>0L){
			selectedJurisdictionId = selectedJurisdiction.getJurisdictionId();
		}
		findIdCallback.findIdCallback(selectedJurisdictionId, findIdContext);
		exitFindMode();
		return findIdAction;
	}
	
	public String cancelFindAction() {
		exitFindMode();
		return findIdAction;
	}
	
	public boolean getFindMode() {
		return (findIdCallback != null);
	}
	
	public void exitFindMode() {
		if (findIdCallback != null) {
			findIdCallback = null;
		}
	}
	
	public void setShowOkButton(Boolean showOkButton) {
		this.showOkButton = showOkButton;
	}
	
	public Boolean getShowOkButton() {
		return this.showOkButton;
	}
	
	public String navigateAction() {
		exitFindMode();
		loadMaintenanceFilter();
		return "maintanence_tax_rates";
	}
  
  public String getRateOptionFlag() {
  	 return this.rateOptionFlag;
  }
  
  public void setRateOptionFlag(String rateOptionFlag) {
	 this.rateOptionFlag = rateOptionFlag;
  }

  public void optionChangeAction(ValueChangeEvent event) {
     String newValue = (String) event.getNewValue();
     String oldValue = (String) event.getOldValue();
  }

  public boolean getDisplaySalesRates() {
  	return (rateOptionFlag.equalsIgnoreCase("0") || rateOptionFlag.equalsIgnoreCase("2"));
  }
  
  public boolean getDisplayUseRates() {
  	return (rateOptionFlag.equalsIgnoreCase("1") || rateOptionFlag.equalsIgnoreCase("2"));
  }

  public String getSearchCountry() {
	return searchCountry;
  }

  public void setSearchCountry(String searchCountry) {
	this.searchCountry = searchCountry;
  }
  
  public String getSearchCountryDetail() {
	return searchCountryDetail;
  }

  public void setSearchCountryDetail(String searchCountryDetail) {
	this.searchCountryDetail = searchCountryDetail;
  }
  
  

  public Double getWarningLevel() {
    OptionCodePK optionCodePK = new OptionCodePK();
    Option option = new Option();
    optionCodePK.setOptionCode(optionCode);
    optionCodePK.setOptionTypeCode(system);
    optionCodePK.setUserCode(system);
    option = optionService.findByPK(optionCodePK);
    logger.info("Option code (warning level): " + option.getValue());
    return Double.parseDouble(option.getValue());
  }

  public TaxJurisdictionBackingBean() {
    generateFieldSizeMap(); // Create Map of Field Sizes
    generateFieldAndActionMaps(); // Create Map of Actions and Display
                                  // characteristics
    generateNewActionMap();
  }
  
  //0001894
  public void uninitializedControls(){
	  tabSelected = "statetab";
	  
	  //start of country	  
	  countrySalesTier1SetToMaximumCheck = false;
	  countrySalesTier1SetToMaximumCheckBind.resetValue();

	  countrySalesTier2SetToMaximumCheck = false;
	  countrySalesTier2SetToMaximumCheckBind.resetValue();

	  countrySalesTier2TaxEntireAmountCheck = false;
	  countrySalesTier2TaxEntireAmountCheckBind.resetValue();

	  countrySalesTier3TaxEntireAmountCheck = false;
	  countrySalesTier3TaxEntireAmountCheckBind.resetValue();
	  
	  countrySameAsSalesRatesCheck = false;
	  countrySameAsSalesRatesCheckBind.resetValue();
	  
	  countryUseTier1SetToMaximumCheck = false;
	  countryUseTier1SetToMaximumCheckBind.resetValue();
	  
	  countryUseTier2SetToMaximumCheck = false;
	  countryUseTier2SetToMaximumCheckBind.resetValue();
	  
	  countryUseTier2TaxEntireAmountCheck = false;
	  countryUseTier2TaxEntireAmountCheckBind.resetValue();

	  countryUseTier3TaxEntireAmountCheck = false;
	  countryUseTier3TaxEntireAmountCheckBind.resetValue();
	  //end of country
	  
	  //start of state
	  stateSalesTier1SetToMaximumCheck = false;
	  stateSalesTier1SetToMaximumCheckBind.resetValue();

	  stateSalesTier2SetToMaximumCheck = false;
	  stateSalesTier2SetToMaximumCheckBind.resetValue();

	  stateSalesTier2TaxEntireAmountCheck = false;
	  stateSalesTier2TaxEntireAmountCheckBind.resetValue();

	  stateSalesTier3TaxEntireAmountCheck = false;
	  stateSalesTier3TaxEntireAmountCheckBind.resetValue();
	  
	  stateSameAsSalesRatesCheck = false;
	  stateSameAsSalesRatesCheckBind.resetValue();
	  
	  stateUseTier1SetToMaximumCheck = false;
	  stateUseTier1SetToMaximumCheckBind.resetValue();
	  
	  stateUseTier2SetToMaximumCheck = false;
	  stateUseTier2SetToMaximumCheckBind.resetValue();
	  
	  stateUseTier2TaxEntireAmountCheck = false;
	  stateUseTier2TaxEntireAmountCheckBind.resetValue();

	  stateUseTier3TaxEntireAmountCheck = false;
	  stateUseTier3TaxEntireAmountCheckBind.resetValue();
	  //end of state
	  
	  //start of county
	  countySalesTier1SetToMaximumCheck = false;
	  countySalesTier1SetToMaximumCheckBind.resetValue();

	  countySalesTier2SetToMaximumCheck = false;
	  countySalesTier2SetToMaximumCheckBind.resetValue();

	  countySalesTier2TaxEntireAmountCheck = false;
	  countySalesTier2TaxEntireAmountCheckBind.resetValue();

	  countySalesTier3TaxEntireAmountCheck = false;
	  countySalesTier3TaxEntireAmountCheckBind.resetValue();
	  
	  countySameAsSalesRatesCheck = false;
	  countySameAsSalesRatesCheckBind.resetValue();
	  
	  countyUseTier1SetToMaximumCheck = false;
	  countyUseTier1SetToMaximumCheckBind.resetValue();
	  
	  countyUseTier2SetToMaximumCheck = false;
	  countyUseTier2SetToMaximumCheckBind.resetValue();
	  
	  countyUseTier2TaxEntireAmountCheck = false;
	  countyUseTier2TaxEntireAmountCheckBind.resetValue();

	  countyUseTier3TaxEntireAmountCheck = false;
	  countyUseTier3TaxEntireAmountCheckBind.resetValue();
	  //end of county
	  
	  //start of city
	  citySalesTier1SetToMaximumCheck = false;
	  citySalesTier1SetToMaximumCheckBind.resetValue();

	  citySalesTier2SetToMaximumCheck = false;
	  citySalesTier2SetToMaximumCheckBind.resetValue();

	  citySalesTier2TaxEntireAmountCheck = false;
	  citySalesTier2TaxEntireAmountCheckBind.resetValue();

	  citySalesTier3TaxEntireAmountCheck = false;
	  citySalesTier3TaxEntireAmountCheckBind.resetValue();
	  
	  citySameAsSalesRatesCheck = false;
	  citySameAsSalesRatesCheckBind.resetValue();
	  
	  cityUseTier1SetToMaximumCheck = false;
	  cityUseTier1SetToMaximumCheckBind.resetValue();
	  
	  cityUseTier2SetToMaximumCheck = false;
	  cityUseTier2SetToMaximumCheckBind.resetValue();
	  
	  cityUseTier2TaxEntireAmountCheck = false;
	  cityUseTier2TaxEntireAmountCheckBind.resetValue();

	  cityUseTier3TaxEntireAmountCheck = false;
	  cityUseTier3TaxEntireAmountCheckBind.resetValue();
	  //end of city
	  
	  countrySalesTier2MinAmtInputText.setDisabled(true);
	  countryUseTier2MinAmtInputText.setDisabled(true);  
	  stateSalesTier2MinAmtInputText.setDisabled(true);
	  stateUseTier2MinAmtInputText.setDisabled(true); 
	  countySalesTier2MinAmtInputText.setDisabled(true);
	  countyUseTier2MinAmtInputText.setDisabled(true);
	  citySalesTier2MinAmtInputText.setDisabled(true);
	  cityUseTier2MinAmtInputText.setDisabled(true);

	  stj1UseRateInputText.setValue("0");
	  stj2UseRateInputText.setValue("0");
	  stj3UseRateInputText.setValue("0");
	  stj4UseRateInputText.setValue("0");
	  stj5UseRateInputText.setValue("0");
	  stj1UseSetamtInputText.setValue("0");
	  stj2UseSetamtInputText.setValue("0");
	  stj3UseSetamtInputText.setValue("0");
	  stj4UseSetamtInputText.setValue("0");
	  stj5UseSetamtInputText.setValue("0");
	  
	  stj1SalesRateInputText.setValue("0");
	  stj2SalesRateInputText.setValue("0");
	  stj3SalesRateInputText.setValue("0");
	  stj4SalesRateInputText.setValue("0");
	  stj5SalesRateInputText.setValue("0");
	  stj1SalesSetamtInputText.setValue("0");
	  stj2SalesSetamtInputText.setValue("0");
	  stj3SalesSetamtInputText.setValue("0");
	  stj4SalesSetamtInputText.setValue("0");
	  stj5SalesSetamtInputText.setValue("0");

	  stj1UseRateInputText.setDisabled(false);
	  stj2UseRateInputText.setDisabled(false);
	  stj3UseRateInputText.setDisabled(false);
	  stj4UseRateInputText.setDisabled(false);
	  stj5UseRateInputText.setDisabled(false);
	  stj1UseSetamtInputText.setDisabled(false);
	  stj2UseSetamtInputText.setDisabled(false);
	  stj3UseSetamtInputText.setDisabled(false);
	  stj4UseSetamtInputText.setDisabled(false);
	  stj5UseSetamtInputText.setDisabled(false);
	  
	  stj1SalesRateInputText.setDisabled(false);
	  stj2SalesRateInputText.setDisabled(false);
	  stj3SalesRateInputText.setDisabled(false);
	  stj4SalesRateInputText.setDisabled(false);
	  stj5SalesRateInputText.setDisabled(false);
	  stj1SalesSetamtInputText.setDisabled(false);
	  stj2SalesSetamtInputText.setDisabled(false);
	  stj3SalesSetamtInputText.setDisabled(false);
	  stj4SalesSetamtInputText.setDisabled(false);
	  stj5SalesSetamtInputText.setDisabled(false);

	  combinedUseRate.setValue(0L);
	  combinedSalesRate.setValue(0L);

	  effectiveDate.resetValue();
	  expirationDate.resetValue();
  }
  
  	public void copyUpdateListener(ActionEvent e) {
  		if (currentAction == TaxJurisdictionAction.updateJurisdictionTaxrate) {
  			currentAction = TaxJurisdictionAction.copyUpdateJurisdictionTaxrate;
  			rateUsed = false;
  			
  			//editTaxRate = new JurisdictionTaxrate(selectedTaxRate);
  			editTaxRate.setId(null);

  	    	loadToControl(TaxJurisdictionAction.copyUpdateJurisdictionTaxrate);	
  		  	recalCombinedRates(null);

  		  	// Clear Out certain fields for new record
  		  	editTaxRate.setJurisdictionTaxrateIdString(JurisdictionTaxrate.NEW_JURISDICTION_TAXRATE_ID_STRING);
  		  	editTaxRate.setUpdateTimestamp(null);
  		  	editTaxRate.setUpdateUserId(null);
  		}
	}
  
  	public boolean getUpdateTaxrateAction() {
		return (currentAction == TaxJurisdictionAction.updateJurisdictionTaxrate);
	}
  
    public void loadToControl(TaxJurisdictionAction nextAction){
    	
    	if(TaxJurisdictionAction.copyAddJurisdictionTaxrate == nextAction ||
    			TaxJurisdictionAction.addJurisdictionTaxrate == nextAction){
    		rateTypeCodeMenu.setDisabled(false);
  	      	rateTypeCodeMenu.setReadonly(false);
  	      	effectiveDate.setDisabled(false);
  	      	expirationDate.setDisabled(false);
    	}
    	else if(TaxJurisdictionAction.copyUpdateJurisdictionTaxrate == nextAction){
    		rateTypeCodeMenu.setDisabled(true);
  	      	rateTypeCodeMenu.setReadonly(true);
  	      	effectiveDate.setDisabled(false);
  	      	expirationDate.setDisabled(false);
    	}
    	else if(TaxJurisdictionAction.updateJurisdictionTaxrate == nextAction){
    		rateTypeCodeMenu.setDisabled(true);
    	    rateTypeCodeMenu.setReadonly(true);
    	    effectiveDate.setDisabled(true);
    	    expirationDate.setDisabled(false);
    	}
    	else{
    		rateTypeCodeMenu.setDisabled(true);
    	    rateTypeCodeMenu.setReadonly(true);
    	    effectiveDate.setDisabled(true);
    	    expirationDate.setDisabled(true);
    	}

    	//start of country 
        //country tier 1
        if (new Long(JurisdictionTaxrate.MAX_AMOUNT).equals(editTaxRate.getCountrySalesTier1MaxAmt().longValue())) {
        	countrySalesTier1MaxAmtInputText.setValue(JurisdictionTaxrate.MAX_STRING);
        	countrySalesTier1MaxAmtInputText.setDisabled(true);
        	countrySalesTier2MinAmtInputText.setDisabled(false);
        	countrySalesTier2MinAmtInputText.setValue(""+editTaxRate.getCountrySalesTier2MinAmt());
        	countrySalesTier1SetToMaximumCheck = true;
        	countrySalesTier1SetToMaximumCheckBind.setSelected(countrySalesTier1SetToMaximumCheck);
        }
        else{
        	countrySalesTier1MaxAmtInputText.setDisabled(false);
        	countrySalesTier2MinAmtInputText.setDisabled(true);
        	countrySalesTier1SetToMaximumCheck = false;
        	countrySalesTier1SetToMaximumCheckBind.setSelected(countrySalesTier1SetToMaximumCheck);
        	countrySalesTier1MaxAmtInputText.setValue(""+editTaxRate.getCountrySalesTier1MaxAmt());
        	countrySalesTier2MinAmtInputText.setValue(""+editTaxRate.getCountrySalesTier2MinAmt());
        }
      
        if(new Long(JurisdictionTaxrate.MAX_AMOUNT).equals(editTaxRate.getCountryUseTier1MaxAmt().longValue())) {
        	countryUseTier1MaxAmtInputText.setValue(JurisdictionTaxrate.MAX_STRING);
        	countryUseTier1MaxAmtInputText.setDisabled(true);
        	countryUseTier2MinAmtInputText.setDisabled(false);
        	countryUseTier2MinAmtInputText.setValue(""+editTaxRate.getCountryUseTier2MinAmt());
        	countryUseTier1SetToMaximumCheck = true;
        	countryUseTier1SetToMaximumCheckBind.setSelected(countryUseTier1SetToMaximumCheck);
        }
        else{
        	countryUseTier1MaxAmtInputText.setDisabled(false);
        	countryUseTier2MinAmtInputText.setDisabled(true);
        	countryUseTier1SetToMaximumCheck = false;
        	countryUseTier1SetToMaximumCheckBind.setSelected(countryUseTier1SetToMaximumCheck);
        	countryUseTier1MaxAmtInputText.setValue(""+editTaxRate.getCountryUseTier1MaxAmt());
        	countryUseTier2MinAmtInputText.setValue(""+editTaxRate.getCountryUseTier2MinAmt());
        }
     
        //country tier 2
        if(new Long(JurisdictionTaxrate.MAX_AMOUNT).equals(editTaxRate.getCountrySalesTier2MaxAmt().longValue())) {
        	countrySalesTier2MaxAmtInputText.setValue(JurisdictionTaxrate.MAX_STRING);
        	countrySalesTier2MaxAmtInputText.setDisabled(true);
        	countrySalesTier2SetToMaximumCheck = true;
        	countrySalesTier2SetToMaximumCheckBind.setSelected(countrySalesTier2SetToMaximumCheck);
           
        	countrySalesTier3RateInputText.setDisabled(true);
        	countrySalesTier3SetamtInputText.setDisabled(true);
        	countrySalesTier3TaxEntireAmountCheckBind.setDisabled(true);
        } 
        else{
        	countrySalesTier2MaxAmtInputText.setDisabled(false);
        	countrySalesTier2MaxAmtInputText.setValue(""+editTaxRate.getCountrySalesTier2MaxAmt());
        	countrySalesTier2SetToMaximumCheck = false;
        	countrySalesTier2SetToMaximumCheckBind.setSelected(countrySalesTier2SetToMaximumCheck);
           
        	countrySalesTier3RateInputText.setDisabled(false);
        	countrySalesTier3SetamtInputText.setDisabled(false);
        	countrySalesTier3TaxEntireAmountCheckBind.setDisabled(false);
        }
      
        if (new Long(JurisdictionTaxrate.MAX_AMOUNT).equals(editTaxRate.getCountryUseTier2MaxAmt().longValue())) {
        	countryUseTier2MaxAmtInputText.setValue(JurisdictionTaxrate.MAX_STRING);
        	countryUseTier2MaxAmtInputText.setDisabled(true);
        	countryUseTier2SetToMaximumCheck = true;
        	countryUseTier2SetToMaximumCheckBind.setSelected(countryUseTier2SetToMaximumCheck);
          
        	countryUseTier3RateInputText.setDisabled(true);
        	countryUseTier3SetamtInputText.setDisabled(true);
        	countryUseTier3TaxEntireAmountCheckBind.setDisabled(true);
        } 
        else {
        	countryUseTier2MaxAmtInputText.setDisabled(false);
        	countryUseTier2MaxAmtInputText.setValue(""+editTaxRate.getCountryUseTier2MaxAmt());
        	countryUseTier2SetToMaximumCheck = false;
        	countryUseTier2SetToMaximumCheckBind.setSelected(countryUseTier2SetToMaximumCheck);
          
        	countryUseTier3RateInputText.setDisabled(false);
        	countryUseTier3SetamtInputText.setDisabled(false);
        	countryUseTier3TaxEntireAmountCheckBind.setDisabled(false);
        }
      
        countrySalesTier2TaxEntireAmountCheck = ((editTaxRate.getCountrySalesTier2EntamFlag()!=null && editTaxRate.getCountrySalesTier2EntamFlag().equals("1")) ? true : false);
        countrySalesTier3TaxEntireAmountCheck = ((editTaxRate.getCountrySalesTier3EntamFlag()!=null && editTaxRate.getCountrySalesTier3EntamFlag().equals("1")) ? true : false);
      
        countryUseTier2TaxEntireAmountCheck = ((editTaxRate.getCountryUseTier2EntamFlag()!=null && editTaxRate.getCountryUseTier2EntamFlag().equals("1")) ? true : false);
        countryUseTier3TaxEntireAmountCheck = ((editTaxRate.getCountryUseTier3EntamFlag()!=null && editTaxRate.getCountryUseTier3EntamFlag().equals("1")) ? true : false);
      
	    //country sales
	  	countrySalesTier1RateInputText.setValue(""+editTaxRate.getCountrySalesTier1Rate());
	  	countrySalesTier1SetamtInputText.setValue(""+editTaxRate.getCountrySalesTier1Setamt());
	  	  
	  	countrySalesTier2MinAmtInputText.setValue(""+editTaxRate.getCountrySalesTier2MinAmt());
	  	countrySalesTier2RateInputText.setValue(""+editTaxRate.getCountrySalesTier2Rate()); 
	  	countrySalesTier2SetamtInputText.setValue(""+editTaxRate.getCountrySalesTier2Setamt());
	  	  
	  	countrySalesTier3RateInputText.setValue(""+editTaxRate.getCountrySalesTier3Rate());
	  	countrySalesTier3SetamtInputText.setValue(""+editTaxRate.getCountrySalesTier3Setamt());
	  	
	  	countrySalesMaxtaxAmtInputText.setValue(""+editTaxRate.getCountrySalesMaxtaxAmt());

	  	//country Same As Sales Rates
	  	countrySameAsSalesRatesCheck = ((editTaxRate.getCountryUseSameSales()!=null && editTaxRate.getCountryUseSameSales().equals("1")) ? true : false);
	  	countrySameAsSalesRatesCheckBind.setSelected(countrySameAsSalesRatesCheck);
	  	
	  	//country use  	  
	  	countryUseTier1RateInputText.setValue(""+editTaxRate.getCountryUseTier1Rate());
	  	countryUseTier1SetamtInputText.setValue(""+editTaxRate.getCountryUseTier1Setamt());
	  	countryUseTier2MinAmtInputText.setValue(""+editTaxRate.getCountryUseTier2MinAmt()); 
	  	countryUseTier2RateInputText.setValue(""+editTaxRate.getCountryUseTier2Rate());
	  	countryUseTier2SetamtInputText.setValue(""+editTaxRate.getCountryUseTier2Setamt());

	  	countryUseTier3RateInputText.setValue(""+editTaxRate.getCountryUseTier3Rate());
	  	countryUseTier3SetamtInputText.setValue(""+editTaxRate.getCountryUseTier3Setamt());
	      
	  	countryUseMaxtaxAmtInputText.setValue(""+editTaxRate.getCountryUseMaxtaxAmt());
	  	
	    //for use controls
	    if(countrySameAsSalesRatesCheck || TaxJurisdictionAction.deleteJurisdictionTaxrate == nextAction 
	    								|| TaxJurisdictionAction.viewJurisdictionTaxrate == nextAction 
	    								|| TaxJurisdictionAction.viewJurisdictionTaxrateFromTransaction == nextAction 
	    								|| (TaxJurisdictionAction.updateJurisdictionTaxrate == nextAction && rateUsed)){
	    	//country tier 1	
			countryUseTier1RateInputText.setDisabled(true);
			countryUseTier1RateInputText.setReadonly(true);
			  
			countryUseTier1SetamtInputText.setDisabled(true);
			countryUseTier1SetamtInputText.setReadonly(true);
			  
		    countryUseTier1MaxAmtInputText.setDisabled(true);
		    countryUseTier1MaxAmtInputText.setReadonly(true);

		    //country tier 2
		    countryUseTier2MinAmtInputText.setDisabled(true);
		    countryUseTier2MinAmtInputText.setReadonly(true);
		    countryUseTier2RateInputText.setDisabled(true);
		    countryUseTier2RateInputText.setReadonly(true);
		    countryUseTier2SetamtInputText.setDisabled(true);
		    countryUseTier2SetamtInputText.setReadonly(true);
		    countryUseTier2MaxAmtInputText.setDisabled(true);
		    countryUseTier2MaxAmtInputText.setReadonly(true);
	 
		    //country tier 3
		    countryUseTier3RateInputText.setDisabled(true);
		    countryUseTier3RateInputText.setReadonly(true);
		    countryUseTier3SetamtInputText.setDisabled(true);
		    countryUseTier3SetamtInputText.setReadonly(true);
		    
		    countryUseTier1SetToMaximumCheckBind.setDisabled(true);
		    countryUseTier1SetToMaximumCheckBind.setDisabled(true);
		    
		    countryUseTier2SetToMaximumCheckBind.setDisabled(true);
		    countryUseTier2SetToMaximumCheckBind.setDisabled(true);
		     
		    countryUseTier2TaxEntireAmountCheckBind.setDisabled(true);
		    countryUseTier3TaxEntireAmountCheckBind.setReadonly(true);
		    
		    countryUseTier3TaxEntireAmountCheckBind.setDisabled(true);
		    countryUseTier3TaxEntireAmountCheckBind.setReadonly(true);
		    
		    countryUseMaxtaxAmtInputText.setDisabled(true);
		    countryUseMaxtaxAmtInputText.setReadonly(true);
	    }
	    else{
	    	//country tier 1	
			countryUseTier1RateInputText.setDisabled(false);
			countryUseTier1RateInputText.setReadonly(false);		  
			countryUseTier1SetamtInputText.setDisabled(false);
			countryUseTier1SetamtInputText.setReadonly(false);

		    //country tier 2
		    countryUseTier2RateInputText.setDisabled(false);
		    countryUseTier2RateInputText.setReadonly(false);
		    countryUseTier2SetamtInputText.setDisabled(false);
		    countryUseTier2SetamtInputText.setReadonly(false);
	 
		    countryUseTier1SetToMaximumCheckBind.setDisabled(false);
		    countryUseTier1SetToMaximumCheckBind.setDisabled(false);
		    
		    countryUseTier2SetToMaximumCheckBind.setDisabled(false);
		    countryUseTier2SetToMaximumCheckBind.setDisabled(false);
		     
		    countryUseTier2TaxEntireAmountCheckBind.setDisabled(false);
		    countryUseTier2TaxEntireAmountCheckBind.setReadonly(false);
		    
		    countryUseMaxtaxAmtInputText.setDisabled(false);
		    countryUseMaxtaxAmtInputText.setReadonly(false);
	    }
	    
	    //for sales controls
	    if(TaxJurisdictionAction.deleteJurisdictionTaxrate == nextAction 
	    		|| TaxJurisdictionAction.viewJurisdictionTaxrate == nextAction 
				|| TaxJurisdictionAction.viewJurisdictionTaxrateFromTransaction == nextAction 
				|| (TaxJurisdictionAction.updateJurisdictionTaxrate == nextAction && rateUsed)){
	    	//country tier 1	
			countrySalesTier1RateInputText.setDisabled(true);
			countrySalesTier1RateInputText.setReadonly(true);
			  
			countrySalesTier1SetamtInputText.setDisabled(true);
			countrySalesTier1SetamtInputText.setReadonly(true);
			  
		    countrySalesTier1MaxAmtInputText.setDisabled(true);
		    countrySalesTier1MaxAmtInputText.setReadonly(true);

		    //country tier 2
		    countrySalesTier2MinAmtInputText.setDisabled(true);
		    countrySalesTier2MinAmtInputText.setReadonly(true);
		    countrySalesTier2RateInputText.setDisabled(true);
		    countrySalesTier2RateInputText.setReadonly(true);
		    countrySalesTier2SetamtInputText.setDisabled(true);
		    countrySalesTier2SetamtInputText.setReadonly(true);
		    countrySalesTier2MaxAmtInputText.setDisabled(true);
		    countrySalesTier2MaxAmtInputText.setReadonly(true);
	 
		    //country tier 3
		    countrySalesTier3RateInputText.setDisabled(true);
		    countrySalesTier3RateInputText.setReadonly(true);
		    countrySalesTier3SetamtInputText.setDisabled(true);
		    countrySalesTier3SetamtInputText.setReadonly(true);
		    
		    countrySalesTier1SetToMaximumCheckBind.setDisabled(true);
		    countrySalesTier1SetToMaximumCheckBind.setDisabled(true);
		    
		    countrySalesTier2SetToMaximumCheckBind.setDisabled(true);
		    countrySalesTier2SetToMaximumCheckBind.setDisabled(true);
		     
		    countrySalesTier2TaxEntireAmountCheckBind.setDisabled(true);
		    countrySalesTier2TaxEntireAmountCheckBind.setReadonly(true);
		    
		    countrySalesTier3TaxEntireAmountCheckBind.setDisabled(true);
		    countrySalesTier3TaxEntireAmountCheckBind.setReadonly(true);
		    
		    countrySalesMaxtaxAmtInputText.setDisabled(true);
			countrySalesMaxtaxAmtInputText.setReadonly(true);
	    }
	    else{
	    	//country tier 1	
			countrySalesTier1RateInputText.setDisabled(false);
			countrySalesTier1RateInputText.setReadonly(false);		  
			countrySalesTier1SetamtInputText.setDisabled(false);
			countrySalesTier1SetamtInputText.setReadonly(false);

		    //country tier 2
		    countrySalesTier2RateInputText.setDisabled(false);
		    countrySalesTier2RateInputText.setReadonly(false);
		    countrySalesTier2SetamtInputText.setDisabled(false);
		    countrySalesTier2SetamtInputText.setReadonly(false);
	 
		    countrySalesTier1SetToMaximumCheckBind.setDisabled(false);
		    countrySalesTier1SetToMaximumCheckBind.setDisabled(false);
		    
		    countrySalesTier2SetToMaximumCheckBind.setDisabled(false);
		    countrySalesTier2SetToMaximumCheckBind.setDisabled(false);
		     
		    countrySalesTier2TaxEntireAmountCheckBind.setDisabled(false);
		    countrySalesTier2TaxEntireAmountCheckBind.setReadonly(false);
		    
		    countrySalesMaxtaxAmtInputText.setDisabled(false);
		    countrySalesMaxtaxAmtInputText.setReadonly(false);
	    }
	    
	    //Same As Sales Rates
	    if(TaxJurisdictionAction.deleteJurisdictionTaxrate == nextAction 
	    		|| TaxJurisdictionAction.viewJurisdictionTaxrate == nextAction 
				|| TaxJurisdictionAction.viewJurisdictionTaxrateFromTransaction == nextAction 
				|| (TaxJurisdictionAction.updateJurisdictionTaxrate == nextAction && rateUsed)){
	    	countrySameAsSalesRatesCheckBind.setDisabled(true);
	    	countrySameAsSalesRatesCheckBind.setReadonly(true); 
	    }
	    else{
	    	countrySameAsSalesRatesCheckBind.setDisabled(false);
	    	countrySameAsSalesRatesCheckBind.setReadonly(false); 
	    }
	    //end of country

	    //start of state
        //state tier 1
        if (new Long(JurisdictionTaxrate.MAX_AMOUNT).equals(editTaxRate.getStateSalesTier1MaxAmt().longValue())) {
        	stateSalesTier1MaxAmtInputText.setValue(JurisdictionTaxrate.MAX_STRING);
        	stateSalesTier1MaxAmtInputText.setDisabled(true);
        	stateSalesTier2MinAmtInputText.setDisabled(false);
        	stateSalesTier2MinAmtInputText.setValue(""+editTaxRate.getStateSalesTier2MinAmt());
        	stateSalesTier1SetToMaximumCheck = true;
        	stateSalesTier1SetToMaximumCheckBind.setSelected(true);
        }
        else{
        	stateSalesTier1MaxAmtInputText.setDisabled(false);
        	stateSalesTier2MinAmtInputText.setDisabled(true);
        	stateSalesTier1SetToMaximumCheck = false;
        	stateSalesTier1SetToMaximumCheckBind.setSelected(false);
        	stateSalesTier1MaxAmtInputText.setValue(""+editTaxRate.getStateSalesTier1MaxAmt());
        	stateSalesTier2MinAmtInputText.setValue(""+editTaxRate.getStateSalesTier2MinAmt());
        }
      
        if(new Long(JurisdictionTaxrate.MAX_AMOUNT).equals(editTaxRate.getStateUseTier1MaxAmt().longValue())) {
        	stateUseTier1MaxAmtInputText.setValue(JurisdictionTaxrate.MAX_STRING);
        	stateUseTier1MaxAmtInputText.setDisabled(true);
        	stateUseTier2MinAmtInputText.setDisabled(false);
        	stateUseTier2MinAmtInputText.setValue(""+editTaxRate.getStateUseTier2MinAmt());
        	stateUseTier1SetToMaximumCheck = true;
        	stateUseTier1SetToMaximumCheckBind.setSelected(true);
        }
        else{
        	stateUseTier1MaxAmtInputText.setDisabled(false);
        	stateUseTier2MinAmtInputText.setDisabled(true);
        	stateUseTier1SetToMaximumCheck = false;
        	stateUseTier1SetToMaximumCheckBind.setSelected(false);
        	stateUseTier1MaxAmtInputText.setValue(""+editTaxRate.getStateUseTier1MaxAmt());
        	stateUseTier2MinAmtInputText.setValue(""+editTaxRate.getStateUseTier2MinAmt());
        }
     
        //state tier 2
        if(new Long(JurisdictionTaxrate.MAX_AMOUNT).equals(editTaxRate.getStateSalesTier2MaxAmt().longValue())) {
        	stateSalesTier2MaxAmtInputText.setValue(JurisdictionTaxrate.MAX_STRING);
        	stateSalesTier2MaxAmtInputText.setDisabled(true);
        	stateSalesTier2SetToMaximumCheck = true;
        	stateSalesTier2SetToMaximumCheckBind.setSelected(true);
           
        	stateSalesTier3RateInputText.setDisabled(true);
        	stateSalesTier3SetamtInputText.setDisabled(true);
        	stateSalesTier3TaxEntireAmountCheckBind.setDisabled(true);
        } 
        else{
        	stateSalesTier2MaxAmtInputText.setDisabled(false);
        	stateSalesTier2MaxAmtInputText.setValue(""+editTaxRate.getStateSalesTier2MaxAmt());
        	stateSalesTier2SetToMaximumCheck = false;
        	stateSalesTier2SetToMaximumCheckBind.setSelected(false);
           
        	stateSalesTier3RateInputText.setDisabled(false);
        	stateSalesTier3SetamtInputText.setDisabled(false);
        	stateSalesTier3TaxEntireAmountCheckBind.setDisabled(false);
        }
      
        if (new Long(JurisdictionTaxrate.MAX_AMOUNT).equals(editTaxRate.getStateUseTier2MaxAmt().longValue())) {
        	stateUseTier2MaxAmtInputText.setValue(JurisdictionTaxrate.MAX_STRING);
        	stateUseTier2MaxAmtInputText.setDisabled(true);
        	stateUseTier2SetToMaximumCheck = true;
        	stateUseTier2SetToMaximumCheckBind.setSelected(true);
          
        	stateUseTier3RateInputText.setDisabled(true);
        	stateUseTier3SetamtInputText.setDisabled(true);
        	stateUseTier3TaxEntireAmountCheckBind.setDisabled(true);
        } 
        else {
        	stateUseTier2MaxAmtInputText.setDisabled(false);
        	stateUseTier2MaxAmtInputText.setValue(""+editTaxRate.getStateUseTier2MaxAmt());
        	stateUseTier2SetToMaximumCheck = false;
        	stateUseTier2SetToMaximumCheckBind.setSelected(false);
          
        	stateUseTier3RateInputText.setDisabled(false);
        	stateUseTier3SetamtInputText.setDisabled(false);
        	stateUseTier3TaxEntireAmountCheckBind.setDisabled(false);
        }
      
        stateSalesTier2TaxEntireAmountCheck = ((editTaxRate.getStateSalesTier2EntamFlag()!=null && editTaxRate.getStateSalesTier2EntamFlag().equals("1")) ? true : false);
        stateSalesTier2TaxEntireAmountCheckBind.setSelected(stateSalesTier2TaxEntireAmountCheck);
        stateSalesTier3TaxEntireAmountCheck = ((editTaxRate.getStateSalesTier3EntamFlag()!=null && editTaxRate.getStateSalesTier3EntamFlag().equals("1")) ? true : false);
        stateSalesTier3TaxEntireAmountCheckBind.setSelected(stateSalesTier3TaxEntireAmountCheck);
        
        stateUseTier2TaxEntireAmountCheck = ((editTaxRate.getStateUseTier2EntamFlag()!=null && editTaxRate.getStateUseTier2EntamFlag().equals("1")) ? true : false);
        stateUseTier2TaxEntireAmountCheckBind.setSelected(stateUseTier2TaxEntireAmountCheck);
        stateUseTier3TaxEntireAmountCheck = ((editTaxRate.getStateUseTier3EntamFlag()!=null && editTaxRate.getStateUseTier3EntamFlag().equals("1")) ? true : false);
        stateUseTier3TaxEntireAmountCheckBind.setSelected(stateUseTier3TaxEntireAmountCheck);
      
	    //state sales
	  	stateSalesTier1RateInputText.setValue(""+editTaxRate.getStateSalesTier1Rate());
	  	stateSalesTier1SetamtInputText.setValue(""+editTaxRate.getStateSalesTier1Setamt());
	  	  
	  	stateSalesTier2MinAmtInputText.setValue(""+editTaxRate.getStateSalesTier2MinAmt());
	  	stateSalesTier2RateInputText.setValue(""+editTaxRate.getStateSalesTier2Rate()); 
	  	stateSalesTier2SetamtInputText.setValue(""+editTaxRate.getStateSalesTier2Setamt());
	  	  
	  	stateSalesTier3RateInputText.setValue(""+editTaxRate.getStateSalesTier3Rate());
	  	stateSalesTier3SetamtInputText.setValue(""+editTaxRate.getStateSalesTier3Setamt());
	  	
	  	stateSalesMaxtaxAmtInputText.setValue(""+editTaxRate.getStateSalesMaxtaxAmt());

	  	//state Same As Sales Rates
	  	stateSameAsSalesRatesCheck = ((editTaxRate.getStateUseSameSales()!=null && editTaxRate.getStateUseSameSales().equals("1")) ? true : false);
	  	stateSameAsSalesRatesCheckBind.setSelected(stateSameAsSalesRatesCheck);
	  	
	  	//state use  	  
	  	stateUseTier1RateInputText.setValue(""+editTaxRate.getStateUseTier1Rate());
	  	stateUseTier1SetamtInputText.setValue(""+editTaxRate.getStateUseTier1Setamt());
	  	stateUseTier2MinAmtInputText.setValue(""+editTaxRate.getStateUseTier2MinAmt()); 
	  	stateUseTier2RateInputText.setValue(""+editTaxRate.getStateUseTier2Rate());
	  	stateUseTier2SetamtInputText.setValue(""+editTaxRate.getStateUseTier2Setamt());

	  	stateUseTier3RateInputText.setValue(""+editTaxRate.getStateUseTier3Rate());
	  	stateUseTier3SetamtInputText.setValue(""+editTaxRate.getStateUseTier3Setamt());
	  	
	  	stateUseMaxtaxAmtInputText.setValue(""+editTaxRate.getStateUseMaxtaxAmt());
	      
	    //for use controls
	    if(stateSameAsSalesRatesCheck || TaxJurisdictionAction.deleteJurisdictionTaxrate == nextAction 
	    								|| TaxJurisdictionAction.viewJurisdictionTaxrate == nextAction 
	    								|| TaxJurisdictionAction.viewJurisdictionTaxrateFromTransaction == nextAction
	    								|| (TaxJurisdictionAction.updateJurisdictionTaxrate == nextAction && rateUsed)){
	    	//state tier 1	
			stateUseTier1RateInputText.setDisabled(true);
			stateUseTier1RateInputText.setReadonly(true);
			  
			stateUseTier1SetamtInputText.setDisabled(true);
			stateUseTier1SetamtInputText.setReadonly(true);
			  
		    stateUseTier1MaxAmtInputText.setDisabled(true);
		    stateUseTier1MaxAmtInputText.setReadonly(true);

		    //state tier 2
		    stateUseTier2MinAmtInputText.setDisabled(true);
		    stateUseTier2MinAmtInputText.setReadonly(true);
		    stateUseTier2RateInputText.setDisabled(true);
		    stateUseTier2RateInputText.setReadonly(true);
		    stateUseTier2SetamtInputText.setDisabled(true);
		    stateUseTier2SetamtInputText.setReadonly(true);
		    stateUseTier2MaxAmtInputText.setDisabled(true);
		    stateUseTier2MaxAmtInputText.setReadonly(true);
	 
		    //state tier 3
		    stateUseTier3RateInputText.setDisabled(true);
		    stateUseTier3RateInputText.setReadonly(true);
		    stateUseTier3SetamtInputText.setDisabled(true);
		    stateUseTier3SetamtInputText.setReadonly(true);
		    
		    stateUseTier1SetToMaximumCheckBind.setDisabled(true);
		    stateUseTier1SetToMaximumCheckBind.setDisabled(true);
		    
		    stateUseTier2SetToMaximumCheckBind.setDisabled(true);
		    stateUseTier2SetToMaximumCheckBind.setDisabled(true);
		     
		    stateUseTier2TaxEntireAmountCheckBind.setDisabled(true);
		    stateUseTier3TaxEntireAmountCheckBind.setReadonly(true);
		    
		    stateUseTier3TaxEntireAmountCheckBind.setDisabled(true);
		    stateUseTier3TaxEntireAmountCheckBind.setReadonly(true);
		    
		    stateUseMaxtaxAmtInputText.setDisabled(true);
		    stateUseMaxtaxAmtInputText.setReadonly(true);
	    }
	    else{
	    	//state tier 1	
			stateUseTier1RateInputText.setDisabled(false);
			stateUseTier1RateInputText.setReadonly(false);		  
			stateUseTier1SetamtInputText.setDisabled(false);
			stateUseTier1SetamtInputText.setReadonly(false);

		    //state tier 2
		    stateUseTier2RateInputText.setDisabled(false);
		    stateUseTier2RateInputText.setReadonly(false);
		    stateUseTier2SetamtInputText.setDisabled(false);
		    stateUseTier2SetamtInputText.setReadonly(false);
	 
		    stateUseTier1SetToMaximumCheckBind.setDisabled(false);
		    stateUseTier1SetToMaximumCheckBind.setDisabled(false);
		    
		    stateUseTier2SetToMaximumCheckBind.setDisabled(false);
		    stateUseTier2SetToMaximumCheckBind.setDisabled(false);
		     
		    stateUseTier2TaxEntireAmountCheckBind.setDisabled(false);
		    stateUseTier2TaxEntireAmountCheckBind.setReadonly(false);
		    
		    stateUseMaxtaxAmtInputText.setDisabled(false);
		    stateUseMaxtaxAmtInputText.setReadonly(false);
	    }
	    
	    //for sales controls
	    if(TaxJurisdictionAction.deleteJurisdictionTaxrate == nextAction 
	    		|| TaxJurisdictionAction.viewJurisdictionTaxrate == nextAction 
				|| TaxJurisdictionAction.viewJurisdictionTaxrateFromTransaction == nextAction 
				|| (TaxJurisdictionAction.updateJurisdictionTaxrate == nextAction && rateUsed)){
	    	//state tier 1	
			stateSalesTier1RateInputText.setDisabled(true);
			stateSalesTier1RateInputText.setReadonly(true);
			  
			stateSalesTier1SetamtInputText.setDisabled(true);
			stateSalesTier1SetamtInputText.setReadonly(true);
			  
		    stateSalesTier1MaxAmtInputText.setDisabled(true);
		    stateSalesTier1MaxAmtInputText.setReadonly(true);

		    //state tier 2
		    stateSalesTier2MinAmtInputText.setDisabled(true);
		    stateSalesTier2MinAmtInputText.setReadonly(true);
		    stateSalesTier2RateInputText.setDisabled(true);
		    stateSalesTier2RateInputText.setReadonly(true);
		    stateSalesTier2SetamtInputText.setDisabled(true);
		    stateSalesTier2SetamtInputText.setReadonly(true);
		    stateSalesTier2MaxAmtInputText.setDisabled(true);
		    stateSalesTier2MaxAmtInputText.setReadonly(true);
	 
		    //state tier 3
		    stateSalesTier3RateInputText.setDisabled(true);
		    stateSalesTier3RateInputText.setReadonly(true);
		    stateSalesTier3SetamtInputText.setDisabled(true);
		    stateSalesTier3SetamtInputText.setReadonly(true);
		    
		    stateSalesTier1SetToMaximumCheckBind.setDisabled(true);
		    stateSalesTier1SetToMaximumCheckBind.setDisabled(true);
		    
		    stateSalesTier2SetToMaximumCheckBind.setDisabled(true);
		    stateSalesTier2SetToMaximumCheckBind.setDisabled(true);
		     
		    stateSalesTier2TaxEntireAmountCheckBind.setDisabled(true);
		    stateSalesTier2TaxEntireAmountCheckBind.setReadonly(true);
		    
		    stateSalesTier3TaxEntireAmountCheckBind.setDisabled(true);
		    stateSalesTier3TaxEntireAmountCheckBind.setReadonly(true);
		    
		    stateSalesMaxtaxAmtInputText.setDisabled(true);
		    stateSalesMaxtaxAmtInputText.setReadonly(true);
	    }
	    else{
	    	//state tier 1	
			stateSalesTier1RateInputText.setDisabled(false);
			stateSalesTier1RateInputText.setReadonly(false);		  
			stateSalesTier1SetamtInputText.setDisabled(false);
			stateSalesTier1SetamtInputText.setReadonly(false);

		    //state tier 2
		    stateSalesTier2RateInputText.setDisabled(false);
		    stateSalesTier2RateInputText.setReadonly(false);
		    stateSalesTier2SetamtInputText.setDisabled(false);
		    stateSalesTier2SetamtInputText.setReadonly(false);
	 
		    stateSalesTier1SetToMaximumCheckBind.setDisabled(false);
		    stateSalesTier1SetToMaximumCheckBind.setDisabled(false);
		    
		    stateSalesTier2SetToMaximumCheckBind.setDisabled(false);
		    stateSalesTier2SetToMaximumCheckBind.setDisabled(false);
		     
		    stateSalesTier2TaxEntireAmountCheckBind.setDisabled(false);
		    stateSalesTier2TaxEntireAmountCheckBind.setReadonly(false);
		    
		    stateSalesMaxtaxAmtInputText.setDisabled(false);
		    stateSalesMaxtaxAmtInputText.setReadonly(false);
	    }
	    
	    //Same As Sales Rates
	    if(TaxJurisdictionAction.deleteJurisdictionTaxrate == nextAction 
	    		|| TaxJurisdictionAction.viewJurisdictionTaxrate == nextAction 
				|| TaxJurisdictionAction.viewJurisdictionTaxrateFromTransaction == nextAction 
				|| (TaxJurisdictionAction.updateJurisdictionTaxrate == nextAction && rateUsed)){
	    	stateSameAsSalesRatesCheckBind.setDisabled(true);
	    	stateSameAsSalesRatesCheckBind.setReadonly(true); 
	    }
	    else{
	    	stateSameAsSalesRatesCheckBind.setDisabled(false);
	    	stateSameAsSalesRatesCheckBind.setReadonly(false); 
	    }
	    //end of state
	    
	    //start of county
        //county tier 1
        if (new Long(JurisdictionTaxrate.MAX_AMOUNT).equals(editTaxRate.getCountySalesTier1MaxAmt().longValue())) {
        	countySalesTier1MaxAmtInputText.setValue(JurisdictionTaxrate.MAX_STRING);
        	countySalesTier1MaxAmtInputText.setDisabled(true);
        	countySalesTier2MinAmtInputText.setDisabled(false);
        	countySalesTier2MinAmtInputText.setValue(""+editTaxRate.getCountySalesTier2MinAmt());
        	countySalesTier1SetToMaximumCheck = true;
        	countySalesTier1SetToMaximumCheckBind.setSelected(true);
        }
        else{
        	countySalesTier1MaxAmtInputText.setDisabled(false);
        	countySalesTier2MinAmtInputText.setDisabled(true);
        	countySalesTier1SetToMaximumCheck = false;
        	countySalesTier1SetToMaximumCheckBind.setSelected(false);
        	countySalesTier1MaxAmtInputText.setValue(""+editTaxRate.getCountySalesTier1MaxAmt());
        	countySalesTier2MinAmtInputText.setValue(""+editTaxRate.getCountySalesTier2MinAmt());
        }
      
        if(new Long(JurisdictionTaxrate.MAX_AMOUNT).equals(editTaxRate.getCountyUseTier1MaxAmt().longValue())) {
        	countyUseTier1MaxAmtInputText.setValue(JurisdictionTaxrate.MAX_STRING);
        	countyUseTier1MaxAmtInputText.setDisabled(true);
        	countyUseTier2MinAmtInputText.setDisabled(false);
        	countyUseTier2MinAmtInputText.setValue(""+editTaxRate.getCountyUseTier2MinAmt());
        	countyUseTier1SetToMaximumCheck = true;
        	countyUseTier1SetToMaximumCheckBind.setSelected(true);
        }
        else{
        	countyUseTier1MaxAmtInputText.setDisabled(false);
        	countyUseTier2MinAmtInputText.setDisabled(true);
        	countyUseTier1SetToMaximumCheck = false;
        	countyUseTier1SetToMaximumCheckBind.setSelected(false);
        	countyUseTier1MaxAmtInputText.setValue(""+editTaxRate.getCountyUseTier1MaxAmt());
        	countyUseTier2MinAmtInputText.setValue(""+editTaxRate.getCountyUseTier2MinAmt());
        }
     
        //county tier 2
        if(new Long(JurisdictionTaxrate.MAX_AMOUNT).equals(editTaxRate.getCountySalesTier2MaxAmt().longValue())) {
        	countySalesTier2MaxAmtInputText.setValue(JurisdictionTaxrate.MAX_STRING);
        	countySalesTier2MaxAmtInputText.setDisabled(true);
        	countySalesTier2SetToMaximumCheck = true;
        	countySalesTier2SetToMaximumCheckBind.setSelected(true);
           
        	countySalesTier3RateInputText.setDisabled(true);
        	countySalesTier3SetamtInputText.setDisabled(true);
        	countySalesTier3TaxEntireAmountCheckBind.setDisabled(true);
        } 
        else{
        	countySalesTier2MaxAmtInputText.setDisabled(false);
        	countySalesTier2MaxAmtInputText.setValue(""+editTaxRate.getCountySalesTier2MaxAmt());
        	countySalesTier2SetToMaximumCheck = false;
        	countySalesTier2SetToMaximumCheckBind.setSelected(false);
           
        	countySalesTier3RateInputText.setDisabled(false);
        	countySalesTier3SetamtInputText.setDisabled(false);
        	countySalesTier3TaxEntireAmountCheckBind.setDisabled(false);
        }
      
        if (new Long(JurisdictionTaxrate.MAX_AMOUNT).equals(editTaxRate.getCountyUseTier2MaxAmt().longValue())) {
        	countyUseTier2MaxAmtInputText.setValue(JurisdictionTaxrate.MAX_STRING);
        	countyUseTier2MaxAmtInputText.setDisabled(true);
        	countyUseTier2SetToMaximumCheck = true;
        	countyUseTier2SetToMaximumCheckBind.setSelected(true);
          
        	countyUseTier3RateInputText.setDisabled(true);
        	countyUseTier3SetamtInputText.setDisabled(true);
        	countyUseTier3TaxEntireAmountCheckBind.setDisabled(true);
        } 
        else {
        	countyUseTier2MaxAmtInputText.setDisabled(false);
        	countyUseTier2MaxAmtInputText.setValue(""+editTaxRate.getCountyUseTier2MaxAmt());
        	countyUseTier2SetToMaximumCheck = false;
        	countyUseTier2SetToMaximumCheckBind.setSelected(false);
          
        	countyUseTier3RateInputText.setDisabled(false);
        	countyUseTier3SetamtInputText.setDisabled(false);
        	countyUseTier3TaxEntireAmountCheckBind.setDisabled(false);
        }
      
        countySalesTier2TaxEntireAmountCheck = ((editTaxRate.getCountySalesTier2EntamFlag()!=null && editTaxRate.getCountySalesTier2EntamFlag().equals("1")) ? true : false);
        countySalesTier2TaxEntireAmountCheckBind.setSelected(countySalesTier2TaxEntireAmountCheck);
        countySalesTier3TaxEntireAmountCheck = ((editTaxRate.getCountySalesTier3EntamFlag()!=null && editTaxRate.getCountySalesTier3EntamFlag().equals("1")) ? true : false);
        countySalesTier3TaxEntireAmountCheckBind.setSelected(countySalesTier3TaxEntireAmountCheck);
        
        countyUseTier2TaxEntireAmountCheck = ((editTaxRate.getCountyUseTier2EntamFlag()!=null && editTaxRate.getCountyUseTier2EntamFlag().equals("1")) ? true : false);
        countyUseTier2TaxEntireAmountCheckBind.setSelected(countyUseTier2TaxEntireAmountCheck);
        countyUseTier3TaxEntireAmountCheck = ((editTaxRate.getCountyUseTier3EntamFlag()!=null && editTaxRate.getCountyUseTier3EntamFlag().equals("1")) ? true : false);
        countyUseTier3TaxEntireAmountCheckBind.setSelected(countyUseTier3TaxEntireAmountCheck);
      
	    //county sales
	  	countySalesTier1RateInputText.setValue(""+editTaxRate.getCountySalesTier1Rate());
	  	countySalesTier1SetamtInputText.setValue(""+editTaxRate.getCountySalesTier1Setamt());
	  	  
	  	countySalesTier2MinAmtInputText.setValue(""+editTaxRate.getCountySalesTier2MinAmt());
	  	countySalesTier2RateInputText.setValue(""+editTaxRate.getCountySalesTier2Rate()); 
	  	countySalesTier2SetamtInputText.setValue(""+editTaxRate.getCountySalesTier2Setamt());
	  	  
	  	countySalesTier3RateInputText.setValue(""+editTaxRate.getCountySalesTier3Rate());
	  	countySalesTier3SetamtInputText.setValue(""+editTaxRate.getCountySalesTier3Setamt());
	  	
	  	countySalesMaxtaxAmtInputText.setValue(""+editTaxRate.getCountySalesMaxtaxAmt());

	  	//county Same As Sales Rates
	  	countySameAsSalesRatesCheck = ((editTaxRate.getCountyUseSameSales()!=null && editTaxRate.getCountyUseSameSales().equals("1")) ? true : false);
	  	countySameAsSalesRatesCheckBind.setSelected(countySameAsSalesRatesCheck);
	  	
	  	//county use  	  
	  	countyUseTier1RateInputText.setValue(""+editTaxRate.getCountyUseTier1Rate());
	  	countyUseTier1SetamtInputText.setValue(""+editTaxRate.getCountyUseTier1Setamt());
	  	countyUseTier2MinAmtInputText.setValue(""+editTaxRate.getCountyUseTier2MinAmt()); 
	  	countyUseTier2RateInputText.setValue(""+editTaxRate.getCountyUseTier2Rate());
	  	countyUseTier2SetamtInputText.setValue(""+editTaxRate.getCountyUseTier2Setamt());

	  	countyUseTier3RateInputText.setValue(""+editTaxRate.getCountyUseTier3Rate());
	  	countyUseTier3SetamtInputText.setValue(""+editTaxRate.getCountyUseTier3Setamt());
	  	
	  	countyUseMaxtaxAmtInputText.setValue(""+editTaxRate.getCountyUseMaxtaxAmt());
	  	
	    //for use controls
	    if(countySameAsSalesRatesCheck || TaxJurisdictionAction.deleteJurisdictionTaxrate == nextAction 
	    								|| TaxJurisdictionAction.viewJurisdictionTaxrate == nextAction 
	    								|| TaxJurisdictionAction.viewJurisdictionTaxrateFromTransaction == nextAction 
	    								|| (TaxJurisdictionAction.updateJurisdictionTaxrate == nextAction && rateUsed)){
	    	//county tier 1	
			countyUseTier1RateInputText.setDisabled(true);
			countyUseTier1RateInputText.setReadonly(true);
			  
			countyUseTier1SetamtInputText.setDisabled(true);
			countyUseTier1SetamtInputText.setReadonly(true);
			  
		    countyUseTier1MaxAmtInputText.setDisabled(true);
		    countyUseTier1MaxAmtInputText.setReadonly(true);

		    //county tier 2
		    countyUseTier2MinAmtInputText.setDisabled(true);
		    countyUseTier2MinAmtInputText.setReadonly(true);
		    countyUseTier2RateInputText.setDisabled(true);
		    countyUseTier2RateInputText.setReadonly(true);
		    countyUseTier2SetamtInputText.setDisabled(true);
		    countyUseTier2SetamtInputText.setReadonly(true);
		    countyUseTier2MaxAmtInputText.setDisabled(true);
		    countyUseTier2MaxAmtInputText.setReadonly(true);
	 
		    //county tier 3
		    countyUseTier3RateInputText.setDisabled(true);
		    countyUseTier3RateInputText.setReadonly(true);
		    countyUseTier3SetamtInputText.setDisabled(true);
		    countyUseTier3SetamtInputText.setReadonly(true);
		    
		    countyUseTier1SetToMaximumCheckBind.setDisabled(true);
		    countyUseTier1SetToMaximumCheckBind.setDisabled(true);
		    
		    countyUseTier2SetToMaximumCheckBind.setDisabled(true);
		    countyUseTier2SetToMaximumCheckBind.setDisabled(true);
		     
		    countyUseTier2TaxEntireAmountCheckBind.setDisabled(true);
		    countyUseTier3TaxEntireAmountCheckBind.setReadonly(true);
		    
		    countyUseTier3TaxEntireAmountCheckBind.setDisabled(true);
		    countyUseTier3TaxEntireAmountCheckBind.setReadonly(true);
		    
		    countyUseMaxtaxAmtInputText.setDisabled(true);
		    countyUseMaxtaxAmtInputText.setReadonly(true);
	    }
	    else{
	    	//county tier 1	
			countyUseTier1RateInputText.setDisabled(false);
			countyUseTier1RateInputText.setReadonly(false);		  
			countyUseTier1SetamtInputText.setDisabled(false);
			countyUseTier1SetamtInputText.setReadonly(false);

		    //county tier 2
		    countyUseTier2RateInputText.setDisabled(false);
		    countyUseTier2RateInputText.setReadonly(false);
		    countyUseTier2SetamtInputText.setDisabled(false);
		    countyUseTier2SetamtInputText.setReadonly(false);
	 
		    countyUseTier1SetToMaximumCheckBind.setDisabled(false);
		    countyUseTier1SetToMaximumCheckBind.setDisabled(false);
		    
		    countyUseTier2SetToMaximumCheckBind.setDisabled(false);
		    countyUseTier2SetToMaximumCheckBind.setDisabled(false);
		     
		    countyUseTier2TaxEntireAmountCheckBind.setDisabled(false);
		    countyUseTier2TaxEntireAmountCheckBind.setReadonly(false);
		    
		    countyUseMaxtaxAmtInputText.setDisabled(false);
		    countyUseMaxtaxAmtInputText.setReadonly(false);
	    }
	    
	    //for sales controls
	    if(TaxJurisdictionAction.deleteJurisdictionTaxrate == nextAction 
	    		|| TaxJurisdictionAction.viewJurisdictionTaxrate == nextAction 
				|| TaxJurisdictionAction.viewJurisdictionTaxrateFromTransaction == nextAction 
				|| (TaxJurisdictionAction.updateJurisdictionTaxrate == nextAction && rateUsed)){
	    	//county tier 1	
			countySalesTier1RateInputText.setDisabled(true);
			countySalesTier1RateInputText.setReadonly(true);
			  
			countySalesTier1SetamtInputText.setDisabled(true);
			countySalesTier1SetamtInputText.setReadonly(true);
			  
		    countySalesTier1MaxAmtInputText.setDisabled(true);
		    countySalesTier1MaxAmtInputText.setReadonly(true);

		    //county tier 2
		    countySalesTier2MinAmtInputText.setDisabled(true);
		    countySalesTier2MinAmtInputText.setReadonly(true);
		    countySalesTier2RateInputText.setDisabled(true);
		    countySalesTier2RateInputText.setReadonly(true);
		    countySalesTier2SetamtInputText.setDisabled(true);
		    countySalesTier2SetamtInputText.setReadonly(true);
		    countySalesTier2MaxAmtInputText.setDisabled(true);
		    countySalesTier2MaxAmtInputText.setReadonly(true);
	 
		    //county tier 3
		    countySalesTier3RateInputText.setDisabled(true);
		    countySalesTier3RateInputText.setReadonly(true);
		    countySalesTier3SetamtInputText.setDisabled(true);
		    countySalesTier3SetamtInputText.setReadonly(true);
		    
		    countySalesTier1SetToMaximumCheckBind.setDisabled(true);
		    countySalesTier1SetToMaximumCheckBind.setDisabled(true);
		    
		    countySalesTier2SetToMaximumCheckBind.setDisabled(true);
		    countySalesTier2SetToMaximumCheckBind.setDisabled(true);
		     
		    countySalesTier2TaxEntireAmountCheckBind.setDisabled(true);
		    countySalesTier2TaxEntireAmountCheckBind.setReadonly(true);
		    
		    countySalesTier3TaxEntireAmountCheckBind.setDisabled(true);
		    countySalesTier3TaxEntireAmountCheckBind.setReadonly(true);
		    
		    countySalesMaxtaxAmtInputText.setDisabled(true);
		    countySalesMaxtaxAmtInputText.setReadonly(true);
	    }
	    else{
	    	//county tier 1	
			countySalesTier1RateInputText.setDisabled(false);
			countySalesTier1RateInputText.setReadonly(false);		  
			countySalesTier1SetamtInputText.setDisabled(false);
			countySalesTier1SetamtInputText.setReadonly(false);

		    //county tier 2
		    countySalesTier2RateInputText.setDisabled(false);
		    countySalesTier2RateInputText.setReadonly(false);
		    countySalesTier2SetamtInputText.setDisabled(false);
		    countySalesTier2SetamtInputText.setReadonly(false);
	 
		    countySalesTier1SetToMaximumCheckBind.setDisabled(false);
		    countySalesTier1SetToMaximumCheckBind.setDisabled(false);
		    
		    countySalesTier2SetToMaximumCheckBind.setDisabled(false);
		    countySalesTier2SetToMaximumCheckBind.setDisabled(false);
		     
		    countySalesTier2TaxEntireAmountCheckBind.setDisabled(false);
		    countySalesTier2TaxEntireAmountCheckBind.setReadonly(false);
		    
		    countySalesMaxtaxAmtInputText.setDisabled(false);
		    countySalesMaxtaxAmtInputText.setReadonly(false);
	    }
	    
	    //Same As Sales Rates
	    if(TaxJurisdictionAction.deleteJurisdictionTaxrate == nextAction 
	    		|| TaxJurisdictionAction.viewJurisdictionTaxrate == nextAction 
				|| TaxJurisdictionAction.viewJurisdictionTaxrateFromTransaction == nextAction 
				|| (TaxJurisdictionAction.updateJurisdictionTaxrate == nextAction && rateUsed)){
	    	countySameAsSalesRatesCheckBind.setDisabled(true);
	    	countySameAsSalesRatesCheckBind.setReadonly(true); 
	    }
	    else{
	    	countySameAsSalesRatesCheckBind.setDisabled(false);
	    	countySameAsSalesRatesCheckBind.setReadonly(false); 
	    }
	    //end of county
	    
	    //start of city
        //city tier 1
        if (new Long(JurisdictionTaxrate.MAX_AMOUNT).equals(editTaxRate.getCitySalesTier1MaxAmt().longValue())) {
        	citySalesTier1MaxAmtInputText.setValue(JurisdictionTaxrate.MAX_STRING);
        	citySalesTier1MaxAmtInputText.setDisabled(true);
        	citySalesTier2MinAmtInputText.setDisabled(false);
        	citySalesTier2MinAmtInputText.setValue(""+editTaxRate.getCitySalesTier2MinAmt());
        	citySalesTier1SetToMaximumCheck = true;
        	citySalesTier1SetToMaximumCheckBind.setSelected(true);
        }
        else{
        	citySalesTier1MaxAmtInputText.setDisabled(false);
        	citySalesTier2MinAmtInputText.setDisabled(true);
        	citySalesTier1SetToMaximumCheck = false;
        	citySalesTier1SetToMaximumCheckBind.setSelected(false);
        	citySalesTier1MaxAmtInputText.setValue(""+editTaxRate.getCitySalesTier1MaxAmt());
        	citySalesTier2MinAmtInputText.setValue(""+editTaxRate.getCitySalesTier2MinAmt());
        }
      
        if(new Long(JurisdictionTaxrate.MAX_AMOUNT).equals(editTaxRate.getCityUseTier1MaxAmt().longValue())) {
        	cityUseTier1MaxAmtInputText.setValue(JurisdictionTaxrate.MAX_STRING);
        	cityUseTier1MaxAmtInputText.setDisabled(true);
        	cityUseTier2MinAmtInputText.setDisabled(false);
        	cityUseTier2MinAmtInputText.setValue(""+editTaxRate.getCityUseTier2MinAmt());
        	cityUseTier1SetToMaximumCheck = true;
        	cityUseTier1SetToMaximumCheckBind.setSelected(true);
        }
        else{
        	cityUseTier1MaxAmtInputText.setDisabled(false);
        	cityUseTier2MinAmtInputText.setDisabled(true);
        	cityUseTier1SetToMaximumCheck = false;
        	cityUseTier1SetToMaximumCheckBind.setSelected(false);
        	cityUseTier1MaxAmtInputText.setValue(""+editTaxRate.getCityUseTier1MaxAmt());
        	cityUseTier2MinAmtInputText.setValue(""+editTaxRate.getCityUseTier2MinAmt());
        }
     
        //city tier 2
        if(new Long(JurisdictionTaxrate.MAX_AMOUNT).equals(editTaxRate.getCitySalesTier2MaxAmt().longValue())) {
        	citySalesTier2MaxAmtInputText.setValue(JurisdictionTaxrate.MAX_STRING);
        	citySalesTier2MaxAmtInputText.setDisabled(true);
        	citySalesTier2SetToMaximumCheck = true;
        	citySalesTier2SetToMaximumCheckBind.setSelected(true);
           
        	citySalesTier3RateInputText.setDisabled(true);
        	citySalesTier3SetamtInputText.setDisabled(true);
        	citySalesTier3TaxEntireAmountCheckBind.setDisabled(true);
        } 
        else{
        	citySalesTier2MaxAmtInputText.setDisabled(false);
        	citySalesTier2MaxAmtInputText.setValue(""+editTaxRate.getCitySalesTier2MaxAmt());
        	citySalesTier2SetToMaximumCheck = false;
        	citySalesTier2SetToMaximumCheckBind.setSelected(false);
           
        	citySalesTier3RateInputText.setDisabled(false);
        	citySalesTier3SetamtInputText.setDisabled(false);
        	citySalesTier3TaxEntireAmountCheckBind.setDisabled(false);
        }
      
        if (new Long(JurisdictionTaxrate.MAX_AMOUNT).equals(editTaxRate.getCityUseTier2MaxAmt().longValue())) {
        	cityUseTier2MaxAmtInputText.setValue(JurisdictionTaxrate.MAX_STRING);
        	cityUseTier2MaxAmtInputText.setDisabled(true);
        	cityUseTier2SetToMaximumCheck = true;
        	cityUseTier2SetToMaximumCheckBind.setSelected(true);
          
        	cityUseTier3RateInputText.setDisabled(true);
        	cityUseTier3SetamtInputText.setDisabled(true);
        	cityUseTier3TaxEntireAmountCheckBind.setDisabled(true);
        } 
        else {
        	cityUseTier2MaxAmtInputText.setDisabled(false);
        	cityUseTier2MaxAmtInputText.setValue(""+editTaxRate.getCityUseTier2MaxAmt());
        	cityUseTier2SetToMaximumCheck = false;
        	cityUseTier2SetToMaximumCheckBind.setSelected(false);
          
        	cityUseTier3RateInputText.setDisabled(false);
        	cityUseTier3SetamtInputText.setDisabled(false);
        	cityUseTier3TaxEntireAmountCheckBind.setDisabled(false);
        }
      
        citySalesTier2TaxEntireAmountCheck = ((editTaxRate.getCitySalesTier2EntamFlag()!=null && editTaxRate.getCitySalesTier2EntamFlag().equals("1")) ? true : false);
        citySalesTier2TaxEntireAmountCheckBind.setSelected(citySalesTier2TaxEntireAmountCheck);
        citySalesTier3TaxEntireAmountCheck = ((editTaxRate.getCitySalesTier3EntamFlag()!=null && editTaxRate.getCitySalesTier3EntamFlag().equals("1")) ? true : false);
        citySalesTier3TaxEntireAmountCheckBind.setSelected(citySalesTier3TaxEntireAmountCheck);
        
        cityUseTier2TaxEntireAmountCheck = ((editTaxRate.getCityUseTier2EntamFlag()!=null && editTaxRate.getCityUseTier2EntamFlag().equals("1")) ? true : false);
        cityUseTier2TaxEntireAmountCheckBind.setSelected(cityUseTier2TaxEntireAmountCheck);
        cityUseTier3TaxEntireAmountCheck = ((editTaxRate.getCityUseTier3EntamFlag()!=null && editTaxRate.getCityUseTier3EntamFlag().equals("1")) ? true : false);
        cityUseTier3TaxEntireAmountCheckBind.setSelected(cityUseTier3TaxEntireAmountCheck);
      
	    //city sales
	  	citySalesTier1RateInputText.setValue(""+editTaxRate.getCitySalesTier1Rate());
	  	citySalesTier1SetamtInputText.setValue(""+editTaxRate.getCitySalesTier1Setamt());
	  	  
	  	citySalesTier2MinAmtInputText.setValue(""+editTaxRate.getCitySalesTier2MinAmt());
	  	citySalesTier2RateInputText.setValue(""+editTaxRate.getCitySalesTier2Rate()); 
	  	citySalesTier2SetamtInputText.setValue(""+editTaxRate.getCitySalesTier2Setamt());
	  	  
	  	citySalesTier3RateInputText.setValue(""+editTaxRate.getCitySalesTier3Rate());
	  	citySalesTier3SetamtInputText.setValue(""+editTaxRate.getCitySalesTier3Setamt());
	  	
	  	citySalesMaxtaxAmtInputText.setValue(""+editTaxRate.getCitySalesMaxtaxAmt());

	  	//city Same As Sales Rates
	  	citySameAsSalesRatesCheck = ((editTaxRate.getCityUseSameSales()!=null && editTaxRate.getCityUseSameSales().equals("1")) ? true : false);
	  	citySameAsSalesRatesCheckBind.setSelected(citySameAsSalesRatesCheck);
	  	
	  	//city use  	  
	  	cityUseTier1RateInputText.setValue(""+editTaxRate.getCityUseTier1Rate());
	  	cityUseTier1SetamtInputText.setValue(""+editTaxRate.getCityUseTier1Setamt());
	  	cityUseTier2MinAmtInputText.setValue(""+editTaxRate.getCityUseTier2MinAmt()); 
	  	cityUseTier2RateInputText.setValue(""+editTaxRate.getCityUseTier2Rate());
	  	cityUseTier2SetamtInputText.setValue(""+editTaxRate.getCityUseTier2Setamt());

	  	cityUseTier3RateInputText.setValue(""+editTaxRate.getCityUseTier3Rate());
	  	cityUseTier3SetamtInputText.setValue(""+editTaxRate.getCityUseTier3Setamt());
	  	
	  	cityUseMaxtaxAmtInputText.setValue(""+editTaxRate.getCityUseMaxtaxAmt());
	      
	    //for use controls
	    if(citySameAsSalesRatesCheck || TaxJurisdictionAction.deleteJurisdictionTaxrate == nextAction 
	    								|| TaxJurisdictionAction.viewJurisdictionTaxrate == nextAction 
	    								|| TaxJurisdictionAction.viewJurisdictionTaxrateFromTransaction == nextAction 
	    								|| (TaxJurisdictionAction.updateJurisdictionTaxrate == nextAction && rateUsed)){
	    	//city tier 1	
			cityUseTier1RateInputText.setDisabled(true);
			cityUseTier1RateInputText.setReadonly(true);
			  
			cityUseTier1SetamtInputText.setDisabled(true);
			cityUseTier1SetamtInputText.setReadonly(true);
			  
		    cityUseTier1MaxAmtInputText.setDisabled(true);
		    cityUseTier1MaxAmtInputText.setReadonly(true);

		    //city tier 2
		    cityUseTier2MinAmtInputText.setDisabled(true);
		    cityUseTier2MinAmtInputText.setReadonly(true);
		    cityUseTier2RateInputText.setDisabled(true);
		    cityUseTier2RateInputText.setReadonly(true);
		    cityUseTier2SetamtInputText.setDisabled(true);
		    cityUseTier2SetamtInputText.setReadonly(true);
		    cityUseTier2MaxAmtInputText.setDisabled(true);
		    cityUseTier2MaxAmtInputText.setReadonly(true);
	 
		    //city tier 3
		    cityUseTier3RateInputText.setDisabled(true);
		    cityUseTier3RateInputText.setReadonly(true);
		    cityUseTier3SetamtInputText.setDisabled(true);
		    cityUseTier3SetamtInputText.setReadonly(true);
		    
		    cityUseTier1SetToMaximumCheckBind.setDisabled(true);
		    cityUseTier1SetToMaximumCheckBind.setDisabled(true);
		    
		    cityUseTier2SetToMaximumCheckBind.setDisabled(true);
		    cityUseTier2SetToMaximumCheckBind.setDisabled(true);
		     
		    cityUseTier2TaxEntireAmountCheckBind.setDisabled(true);
		    cityUseTier3TaxEntireAmountCheckBind.setReadonly(true);
		    
		    cityUseTier3TaxEntireAmountCheckBind.setDisabled(true);
		    cityUseTier3TaxEntireAmountCheckBind.setReadonly(true);
		    
		    cityUseMaxtaxAmtInputText.setDisabled(true);
		    cityUseMaxtaxAmtInputText.setReadonly(true);
	    }
	    else{
	    	//city tier 1	
			cityUseTier1RateInputText.setDisabled(false);
			cityUseTier1RateInputText.setReadonly(false);		  
			cityUseTier1SetamtInputText.setDisabled(false);
			cityUseTier1SetamtInputText.setReadonly(false);

		    //city tier 2
		    cityUseTier2RateInputText.setDisabled(false);
		    cityUseTier2RateInputText.setReadonly(false);
		    cityUseTier2SetamtInputText.setDisabled(false);
		    cityUseTier2SetamtInputText.setReadonly(false);
	 
		    cityUseTier1SetToMaximumCheckBind.setDisabled(false);
		    cityUseTier1SetToMaximumCheckBind.setDisabled(false);
		    
		    cityUseTier2SetToMaximumCheckBind.setDisabled(false);
		    cityUseTier2SetToMaximumCheckBind.setDisabled(false);
		     
		    cityUseTier2TaxEntireAmountCheckBind.setDisabled(false);
		    cityUseTier2TaxEntireAmountCheckBind.setReadonly(false);
		    
		    cityUseMaxtaxAmtInputText.setDisabled(false);
		    cityUseMaxtaxAmtInputText.setReadonly(false);
	    }
	    
	    //for sales controls
	    if(TaxJurisdictionAction.deleteJurisdictionTaxrate == nextAction 
	    		|| TaxJurisdictionAction.viewJurisdictionTaxrate == nextAction 
				|| TaxJurisdictionAction.viewJurisdictionTaxrateFromTransaction == nextAction 
				|| (TaxJurisdictionAction.updateJurisdictionTaxrate == nextAction && rateUsed)){
	    	//city tier 1	
			citySalesTier1RateInputText.setDisabled(true);
			citySalesTier1RateInputText.setReadonly(true);
			  
			citySalesTier1SetamtInputText.setDisabled(true);
			citySalesTier1SetamtInputText.setReadonly(true);
			  
		    citySalesTier1MaxAmtInputText.setDisabled(true);
		    citySalesTier1MaxAmtInputText.setReadonly(true);

		    //city tier 2
		    citySalesTier2MinAmtInputText.setDisabled(true);
		    citySalesTier2MinAmtInputText.setReadonly(true);
		    citySalesTier2RateInputText.setDisabled(true);
		    citySalesTier2RateInputText.setReadonly(true);
		    citySalesTier2SetamtInputText.setDisabled(true);
		    citySalesTier2SetamtInputText.setReadonly(true);
		    citySalesTier2MaxAmtInputText.setDisabled(true);
		    citySalesTier2MaxAmtInputText.setReadonly(true);
	 
		    //city tier 3
		    citySalesTier3RateInputText.setDisabled(true);
		    citySalesTier3RateInputText.setReadonly(true);
		    citySalesTier3SetamtInputText.setDisabled(true);
		    citySalesTier3SetamtInputText.setReadonly(true);
		    
		    citySalesTier1SetToMaximumCheckBind.setDisabled(true);
		    citySalesTier1SetToMaximumCheckBind.setDisabled(true);
		    
		    citySalesTier2SetToMaximumCheckBind.setDisabled(true);
		    citySalesTier2SetToMaximumCheckBind.setDisabled(true);
		     
		    citySalesTier2TaxEntireAmountCheckBind.setDisabled(true);
		    citySalesTier2TaxEntireAmountCheckBind.setReadonly(true);
		    
		    citySalesTier3TaxEntireAmountCheckBind.setDisabled(true);
		    citySalesTier3TaxEntireAmountCheckBind.setReadonly(true);
		    
		    citySalesMaxtaxAmtInputText.setDisabled(true);
		    citySalesMaxtaxAmtInputText.setReadonly(true);
	    }
	    else{
	    	//city tier 1	
			citySalesTier1RateInputText.setDisabled(false);
			citySalesTier1RateInputText.setReadonly(false);		  
			citySalesTier1SetamtInputText.setDisabled(false);
			citySalesTier1SetamtInputText.setReadonly(false);

		    //city tier 2
		    citySalesTier2RateInputText.setDisabled(false);
		    citySalesTier2RateInputText.setReadonly(false);
		    citySalesTier2SetamtInputText.setDisabled(false);
		    citySalesTier2SetamtInputText.setReadonly(false);
	 
		    citySalesTier1SetToMaximumCheckBind.setDisabled(false);
		    citySalesTier1SetToMaximumCheckBind.setDisabled(false);
		    
		    citySalesTier2SetToMaximumCheckBind.setDisabled(false);
		    citySalesTier2SetToMaximumCheckBind.setDisabled(false);
		     
		    citySalesTier2TaxEntireAmountCheckBind.setDisabled(false);
		    citySalesTier2TaxEntireAmountCheckBind.setReadonly(false);
		    
		    citySalesMaxtaxAmtInputText.setDisabled(false);
		    citySalesMaxtaxAmtInputText.setReadonly(false);
	    }
	    
	    //Same As Sales Rates
	    if(TaxJurisdictionAction.deleteJurisdictionTaxrate == nextAction 
	    		|| TaxJurisdictionAction.viewJurisdictionTaxrate == nextAction 
				|| TaxJurisdictionAction.viewJurisdictionTaxrateFromTransaction == nextAction 
				|| (TaxJurisdictionAction.updateJurisdictionTaxrate == nextAction && rateUsed)){
	    	citySameAsSalesRatesCheckBind.setDisabled(true);
	    	citySameAsSalesRatesCheckBind.setReadonly(true); 
	    }
	    else{
	    	citySameAsSalesRatesCheckBind.setDisabled(false);
	    	citySameAsSalesRatesCheckBind.setReadonly(false); 
	    }
	    //end of city
	    
	    //start of stj  
	    stj1UseRateInputText.setValue(""+editTaxRate.getStj1UseRate());
	    stj2UseRateInputText.setValue(""+editTaxRate.getStj2UseRate());
	    stj3UseRateInputText.setValue(""+editTaxRate.getStj3UseRate());
	    stj4UseRateInputText.setValue(""+editTaxRate.getStj4UseRate());
	    stj5UseRateInputText.setValue(""+editTaxRate.getStj5UseRate());    
	    stj1UseSetamtInputText.setValue(""+editTaxRate.getStj1UseSetamt());
		stj2UseSetamtInputText.setValue(""+editTaxRate.getStj2UseSetamt());
		stj3UseSetamtInputText.setValue(""+editTaxRate.getStj3UseSetamt());
		stj4UseSetamtInputText.setValue(""+editTaxRate.getStj4UseSetamt());
		stj5UseSetamtInputText.setValue(""+editTaxRate.getStj5UseSetamt());
		
		stj1SalesRateInputText.setValue(""+editTaxRate.getStj1SalesRate());
	    stj2SalesRateInputText.setValue(""+editTaxRate.getStj2SalesRate());
	    stj3SalesRateInputText.setValue(""+editTaxRate.getStj3SalesRate());
	    stj4SalesRateInputText.setValue(""+editTaxRate.getStj4SalesRate());
	    stj5SalesRateInputText.setValue(""+editTaxRate.getStj5SalesRate());    
	    stj1SalesSetamtInputText.setValue(""+editTaxRate.getStj1SalesSetamt());
		stj2SalesSetamtInputText.setValue(""+editTaxRate.getStj2SalesSetamt());
		stj3SalesSetamtInputText.setValue(""+editTaxRate.getStj3SalesSetamt());
		stj4SalesSetamtInputText.setValue(""+editTaxRate.getStj4SalesSetamt());
		stj5SalesSetamtInputText.setValue(""+editTaxRate.getStj5SalesSetamt());

	  	//city Same As Sales Rates
	  	stjSameAsSalesRatesCheck = ((editTaxRate.getStjUseSameSales()!=null && editTaxRate.getStjUseSameSales().equals("1")) ? true : false);
	  	stjSameAsSalesRatesCheckBind.setSelected(stjSameAsSalesRatesCheck);
	  	
	      
	    //for use controls
	    if(stjSameAsSalesRatesCheck || TaxJurisdictionAction.deleteJurisdictionTaxrate == nextAction 
	    								|| TaxJurisdictionAction.viewJurisdictionTaxrate == nextAction 
	    								|| TaxJurisdictionAction.viewJurisdictionTaxrateFromTransaction == nextAction 
	    								|| (TaxJurisdictionAction.updateJurisdictionTaxrate == nextAction && rateUsed)){
	    	stj1UseRateInputText.setDisabled(true);
		    stj2UseRateInputText.setDisabled(true);
		    stj3UseRateInputText.setDisabled(true);
		    stj4UseRateInputText.setDisabled(true);
		    stj5UseRateInputText.setDisabled(true);   
		    stj1UseSetamtInputText.setDisabled(true);
			stj2UseSetamtInputText.setDisabled(true);
			stj3UseSetamtInputText.setDisabled(true);
			stj4UseSetamtInputText.setDisabled(true);
			stj5UseSetamtInputText.setDisabled(true);
			
			stj1UseRateInputText.setReadonly(true);
		    stj2UseRateInputText.setReadonly(true);
		    stj3UseRateInputText.setReadonly(true);
		    stj4UseRateInputText.setReadonly(true);
		    stj5UseRateInputText.setReadonly(true);   
		    stj1UseSetamtInputText.setReadonly(true);
			stj2UseSetamtInputText.setReadonly(true);
			stj3UseSetamtInputText.setReadonly(true);
			stj4UseSetamtInputText.setReadonly(true);
			stj5UseSetamtInputText.setReadonly(true);
		   
	    }
	    else{
	    	stj1UseRateInputText.setDisabled(false);
		    stj2UseRateInputText.setDisabled(false);
		    stj3UseRateInputText.setDisabled(false);
		    stj4UseRateInputText.setDisabled(false);
		    stj5UseRateInputText.setDisabled(false);   
		    stj1UseSetamtInputText.setDisabled(false);
			stj2UseSetamtInputText.setDisabled(false);
			stj3UseSetamtInputText.setDisabled(false);
			stj4UseSetamtInputText.setDisabled(false);
			stj5UseSetamtInputText.setDisabled(false);
			
			stj1UseRateInputText.setReadonly(false);
		    stj2UseRateInputText.setReadonly(false);
		    stj3UseRateInputText.setReadonly(false);
		    stj4UseRateInputText.setReadonly(false);
		    stj5UseRateInputText.setReadonly(false);   
		    stj1UseSetamtInputText.setReadonly(false);
			stj2UseSetamtInputText.setReadonly(false);
			stj3UseSetamtInputText.setReadonly(false);
			stj4UseSetamtInputText.setReadonly(false);
			stj5UseSetamtInputText.setReadonly(false);
	    }
	    
	    //for sales controls
	    if(TaxJurisdictionAction.deleteJurisdictionTaxrate == nextAction 
	    		|| TaxJurisdictionAction.viewJurisdictionTaxrate == nextAction 
				|| TaxJurisdictionAction.viewJurisdictionTaxrateFromTransaction == nextAction 
				|| (TaxJurisdictionAction.updateJurisdictionTaxrate == nextAction && rateUsed)){

	    	stj1SalesRateInputText.setDisabled(true);
		    stj2SalesRateInputText.setDisabled(true);
		    stj3SalesRateInputText.setDisabled(true);
		    stj4SalesRateInputText.setDisabled(true);
		    stj5SalesRateInputText.setDisabled(true);   
		    stj1SalesSetamtInputText.setDisabled(true);
			stj2SalesSetamtInputText.setDisabled(true);
			stj3SalesSetamtInputText.setDisabled(true);
			stj4SalesSetamtInputText.setDisabled(true);
			stj5SalesSetamtInputText.setDisabled(true);
			
			stj1SalesRateInputText.setReadonly(true);
		    stj2SalesRateInputText.setReadonly(true);
		    stj3SalesRateInputText.setReadonly(true);
		    stj4SalesRateInputText.setReadonly(true);
		    stj5SalesRateInputText.setReadonly(true);   
		    stj1SalesSetamtInputText.setReadonly(true);
			stj2SalesSetamtInputText.setReadonly(true);
			stj3SalesSetamtInputText.setReadonly(true);
			stj4SalesSetamtInputText.setReadonly(true);
			stj5SalesSetamtInputText.setReadonly(true);
		   
	    }
	    else{
	    	stj1SalesRateInputText.setDisabled(false);
		    stj2SalesRateInputText.setDisabled(false);
		    stj3SalesRateInputText.setDisabled(false);
		    stj4SalesRateInputText.setDisabled(false);
		    stj5SalesRateInputText.setDisabled(false);   
		    stj1SalesSetamtInputText.setDisabled(false);
			stj2SalesSetamtInputText.setDisabled(false);
			stj3SalesSetamtInputText.setDisabled(false);
			stj4SalesSetamtInputText.setDisabled(false);
			stj5SalesSetamtInputText.setDisabled(false);
			
			stj1SalesRateInputText.setReadonly(false);
		    stj2SalesRateInputText.setReadonly(false);
		    stj3SalesRateInputText.setReadonly(false);
		    stj4SalesRateInputText.setReadonly(false);
		    stj5SalesRateInputText.setReadonly(false);   
		    stj1SalesSetamtInputText.setReadonly(false);
			stj2SalesSetamtInputText.setReadonly(false);
			stj3SalesSetamtInputText.setReadonly(false);
			stj4SalesSetamtInputText.setReadonly(false);
			stj5SalesSetamtInputText.setReadonly(false);
	    }
	    
	    //Same As Sales Rates
	    if(TaxJurisdictionAction.deleteJurisdictionTaxrate == nextAction 
	    		|| TaxJurisdictionAction.viewJurisdictionTaxrate == nextAction 
				|| TaxJurisdictionAction.viewJurisdictionTaxrateFromTransaction == nextAction 
				|| getIsSTJDisabled()
				|| (TaxJurisdictionAction.updateJurisdictionTaxrate == nextAction && rateUsed)){
	    	stjSameAsSalesRatesCheckBind.setDisabled(true);
	    	stjSameAsSalesRatesCheckBind.setReadonly(true); 
	    }
	    else{
	    	stjSameAsSalesRatesCheckBind.setDisabled(false);
	    	stjSameAsSalesRatesCheckBind.setReadonly(false); 
	    }
	    //end of stj
    }
  
  public void setDefaultControls(){
	  //start of country
	  if(countrySalesTier1MaxAmtInputText.getValue()==null) countrySalesTier1MaxAmtInputText.setValue("");
	  if(countryUseTier1MaxAmtInputText.getValue()==null) countryUseTier1MaxAmtInputText.setValue("");
	  
	  if(countrySalesTier2MaxAmtInputText.getValue()==null) countrySalesTier2MaxAmtInputText.setValue("");
	  if(countryUseTier2MaxAmtInputText.getValue()==null) countryUseTier2MaxAmtInputText.setValue("");

	  if(countrySalesTier1RateInputText.getValue()==null) countrySalesTier1RateInputText.setValue("");
	  if(countryUseTier1RateInputText.getValue()==null) countryUseTier1RateInputText.setValue("");
	  
	  if(stateUseTier3RateInputText.getValue()==null) stateUseTier3RateInputText.setValue("");
	  if(stateSalesTier3RateInputText.getValue()==null) stateSalesTier3RateInputText.setValue("");
	  
	  if(countrySalesTier2MinAmtInputText.getValue()==null) countrySalesTier2MinAmtInputText.setValue("");
	  if(countryUseTier2MinAmtInputText.getValue()==null) countryUseTier2MinAmtInputText.setValue("");

	  if(stj1UseRateInputText.getValue()==null) stj1UseRateInputText.setValue("");
	  if(stj2UseRateInputText.getValue()==null) stj2UseRateInputText.setValue("");
	  if(stj3UseRateInputText.getValue()==null) stj3UseRateInputText.setValue("");
	  if(stj4UseRateInputText.getValue()==null) stj4UseRateInputText.setValue("");
	  if(stj5UseRateInputText.getValue()==null) stj5UseRateInputText.setValue("");
	  
	  if(stj1SalesRateInputText.getValue()==null) stj1SalesRateInputText.setValue("");
	  if(stj2SalesRateInputText.getValue()==null) stj2SalesRateInputText.setValue("");
	  if(stj3SalesRateInputText.getValue()==null) stj3SalesRateInputText.setValue("");
	  if(stj4SalesRateInputText.getValue()==null) stj4SalesRateInputText.setValue("");
	  if(stj5SalesRateInputText.getValue()==null) stj5SalesRateInputText.setValue("");
	  
	  if(combinedUseRate.getValue()==null) combinedUseRate.setValue("");
	  if(combinedSalesRate.getValue()==null) combinedSalesRate.setValue("");
	  if(stateMaxtaxAmount.getValue()==null) stateMaxtaxAmount.setValue("");
	  if(stateUseTier2RateInput.getValue()==null) stateUseTier2RateInput.setValue("");
	  if(stateSalesTier2RateInput.getValue()==null) stateSalesTier2RateInput.setValue("");
  }
  
  public void stjNameChange(ValueChangeEvent vce) {
  }

  /**
   * This method is called from all other "actions" and sets up the loading of
   * Tax Jurisdiction / Tax Rate Detail page.
   * 
   * @return String to next bean
   */
  public String processJurisdictionSelection(TaxJurisdictionAction nextAction) {
    generateFieldAndActionMaps(); // Reset fields
    // logger.debug("stateTier2MinAmount.disabled=" +
    // getCurrentActionMap().get("STATETIER2MINAMOUNT").getDisabled());
    
    uninitializedControls();

    this.trShowSection = UI_VISABLE; // Default: display Tax Rate Section
    this.showCancelButton = true;

    editJurisdiction = null;
    editTaxRate = null;
    this.showOkButton = true;
    this.jurisdictionInUseMessage = null;
    String uniqueIdNumber = "";
    BigDecimal t3, t2;
    
    String view = "tax_jurisdiction_detail";
 
    switch (nextAction) {
    case viewJurisdictionTaxrate:
    case viewJurisdictionTaxrateFromTransaction:
    case viewJurisdictionTaxrateFromTransactionLog:
      if (this.selectedJurisdiction == null) {
        return null; // If there is no Jurisdiction selected, return back to
                      // main page.
      }
      this.currentMode = MODE_VIEW;
      this.showCancelButton = false;

      // Now copy the selected Jurisdictions and TaxRate to the ones that will
      // be edited.
      editJurisdiction = selectedJurisdiction;
      editJurisdiction.setJurisdictionIdString(editJurisdiction.getJurisdictionId().toString());
      
      searchCountryDetail = editJurisdiction.getCountry();
	  searchstateDetail = editJurisdiction.getState();
	  if(stateMenuItemsDetail!=null){
		  stateMenuItemsDetail.clear();
		  stateMenuItemsDetail=null;
	  }
      
      editTaxRate = selectedTaxRate;

      editTaxRate.setJurisdictionTaxrateIdString(selectedTaxRate.getJurisdictionTaxrateId().toString());
      loadToControl(TaxJurisdictionAction.viewJurisdictionTaxrate);
      resetAddressHandler();
	  
	  recalCombinedRates(null);
	  	
      break;
    case addJurisdiction:
      this.currentMode = MODE_EDIT;
      // Since we are adding, just create empty Jurisdiction and Tax Rate
      editJurisdiction = new Jurisdiction();
      countrySalesTier1SetToMaximumCheck = false;
      countrySalesTier2SetToMaximumCheck = false;
      countryUseTier1SetToMaximumCheck = false;
      countryUseTier2SetToMaximumCheck = false;
      
      editJurisdiction.setJurisdictionIdString(Jurisdiction.NEW_JURISDICTION_ID_STRING);
      
      //Set default country
      if(searchCountry==null || searchCountry.equals("")){
    	  HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
    	  UserDTO userDTO = (UserDTO) session.getAttribute("user");
  		  String userCode = userDTO.getUserCode();
  		  Option option = optionService.findByPK(new OptionCodePK ("DEFUSERCOUNTRY", "USER", userCode));
  		  String defaultUserCountry = "";
  		  if(option!=null && option.getValue()!=null){
  			  defaultUserCountry=option.getValue();
  		  }

    	  editJurisdiction.setCountry(defaultUserCountry);
    	  editJurisdiction.setState("");
      }
      else{
    	  editJurisdiction.setCountry(searchCountry);
    	  editJurisdiction.setState(searchstate);
      }
      
      if(searchcounty!=null){
    	  editJurisdiction.setCounty(searchcounty.trim().replace("%", ""));
      }
      if(searchcity!=null){
    	  editJurisdiction.setCity(searchcity.trim().replace("%", ""));
      }
      if(searchzip!=null){
    	  editJurisdiction.setZip(searchzip.trim().replace("%", ""));
      }
      if(searchzipplus4!=null){
    	  editJurisdiction.setZipplus4(searchzipplus4.trim().replace("%", ""));
      }
      if(searchstj!=null){
    	  editJurisdiction.setStj1Name(searchstj.trim().replace("%", ""));
      }
      if(searchgeocode!=null){
    	  editJurisdiction.setGeocode(searchgeocode.trim().replace("%", ""));
      }
      
      editJurisdiction.setCountryNexusindCode("S");
      editJurisdiction.setStateNexusindCode("S");
      editJurisdiction.setCountyNexusindCode("S");
      editJurisdiction.setCityNexusindCode("S");
      editJurisdiction.setStj1NexusindCode("S");
      editJurisdiction.setStj2NexusindCode("S");
      editJurisdiction.setStj3NexusindCode("S");
      editJurisdiction.setStj4NexusindCode("S");
      editJurisdiction.setStj5NexusindCode("S");
      
      editJurisdiction.setEffectiveDate(new com.ncsts.view.util.DateConverter().getUserLocalDate());
      try {
    	  editJurisdiction.setExpirationDate(sdf.parse("12/31/9999"));
      }
      catch(Exception e){}
      editJurisdiction.setModifiedFlag("0");
      
      searchCountryDetail = editJurisdiction.getCountry();
	  searchstateDetail = editJurisdiction.getState();
	  if(stateMenuItemsDetail!=null){
		  stateMenuItemsDetail.clear();
		  stateMenuItemsDetail=null;
	  }
	  
      editTaxRate = new JurisdictionTaxrate();
      
      //PP-24
      //stateSplitAmountInputText.setValue(0L);
      countrySalesTier1MaxAmtInputText.setValue("0");   
      countrySalesTier2MinAmtInputText.setValue("0");
      countrySalesTier2MaxAmtInputText.setValue("0");
      countryUseTier1MaxAmtInputText.setValue("0");   
      countryUseTier2MinAmtInputText.setValue("0");
      countryUseTier2MaxAmtInputText.setValue("0");
      
      stateUseTier3RateInputText.setValue(0.0);
      stateSalesTier3RateInputText.setValue(0.0);
      
      resetRates();
      recalCombinedRates(null);
      
      stateUseTier2RateInput.setValue(0.0);
      stateSalesTier2RateInput.setValue(0.0);
      stateMaxtaxAmount.setValue(0.0);
      
      //PP-24
      //stateSplitAmountInputText.setDisabled(false);
      countrySalesTier1MaxAmtInputText.setDisabled(false);    
      countrySalesTier2MaxAmtInputText.setDisabled(false);
      countryUseTier1MaxAmtInputText.setDisabled(false);    
      countryUseTier2MaxAmtInputText.setDisabled(false);
      
      stateUseTier3RateInputText.setDisabled(false);
      stateSalesTier3RateInputText.setDisabled(false);
      stateUseTier2RateInput.setDisabled(false);
      stateSalesTier2RateInput.setDisabled(false);
      stateMaxtaxAmount.setDisabled(false);
      rateTypeCodeMenu.setDisabled(false);
      rateTypeCodeMenu.setReadonly(false);
      effectiveDate.setDisabled(false);
      expirationDate.setDisabled(false);
      editTaxRate.init(); // Initialize taxRate DTO Object
      getSelectionsForBinaryWeight("-1");
      view = "tax_jurisdiction_add";
      break;
    case addJurisdictionTaxrate:
    case addJurisdictionTaxrateFromTransaction:
      if (this.selectedJurisdiction == null) {
        return null; // If there is no Jurisdiction selected, return back to
                      // main page.
      }
      this.currentMode = MODE_EDITTR;
      // Since we are adding, just create empty Tax Rate     
      editJurisdiction = new Jurisdiction(selectedJurisdiction);
      editJurisdiction.setJurisdictionId(selectedJurisdiction.getJurisdictionId());
      editJurisdiction.setJurisdictionIdString(selectedJurisdiction.getJurisdictionId().toString());
	  getSelectionsForBinaryWeight(editJurisdiction.getState());
	  
	  searchCountryDetail = editJurisdiction.getCountry();
	  searchstateDetail = editJurisdiction.getState();
	  if(stateMenuItemsDetail!=null){
		  stateMenuItemsDetail.clear();
		  stateMenuItemsDetail=null;
	  }
	  
      editTaxRate = new JurisdictionTaxrate();
      
      //Country sales
	  countrySalesTier1RateInputText.setValue("0");
	  countrySalesTier1RateInputText.setDisabled(false); 
	  countrySalesTier1SetamtInputText.setValue("0");
	  countrySalesTier1SetamtInputText.setDisabled(false); 
	  countrySalesTier1MaxAmtInputText.setValue("0");
	  countrySalesTier1MaxAmtInputText.setDisabled(false); 
	  countrySalesTier1MaxAmtInputText.setDisabled(false); 
	  countrySalesTier1SetToMaximumCheck = false;
	  countrySalesTier1SetToMaximumCheckBind.setDisabled(false);
	  
	  countrySalesTier2MinAmtInputText.setValue("0");
	  countrySalesTier2MinAmtInputText.setDisabled(true); 
	  countrySalesTier2RateInputText.setValue("0");
	  countrySalesTier2RateInputText.setDisabled(false); 
	  countrySalesTier2SetamtInputText.setValue("0");
	  countrySalesTier2SetamtInputText.setDisabled(false); 
	  countrySalesTier2MaxAmtInputText.setValue("0");
	  countrySalesTier2MaxAmtInputText.setDisabled(false); 
	  countrySalesTier2SetToMaximumCheck = false;
	  countrySalesTier2SetToMaximumCheckBind.setDisabled(false); 
	  countrySalesTier2TaxEntireAmountCheck = false;
	  countrySalesTier2TaxEntireAmountCheckBind.setDisabled(false); 
	  
	  countrySalesTier3RateInputText.setValue("0");
	  countrySalesTier3RateInputText.setDisabled(false); 
	  countrySalesTier3SetamtInputText.setValue("0");
	  countrySalesTier3SetamtInputText.setDisabled(false); 
	  countrySalesTier3TaxEntireAmountCheck = false;
	  countrySalesTier3TaxEntireAmountCheckBind.setDisabled(false); 
	  
	  countrySalesMaxtaxAmtInputText.setValue("0");
	  countrySalesMaxtaxAmtInputText.setDisabled(false); 
	  
	  //Country use
	  countrySameAsSalesRatesCheck = false;
	  countrySameAsSalesRatesCheckBind.setDisabled(false);  
	  
	  countryUseTier1RateInputText.setValue("0");
	  countryUseTier1RateInputText.setDisabled(false); 
	  countryUseTier1SetamtInputText.setValue("0");
	  countryUseTier1SetamtInputText.setDisabled(false); 
	  countryUseTier1MaxAmtInputText.setValue("0");
	  countryUseTier1MaxAmtInputText.setDisabled(false); 
	  countryUseTier1MaxAmtInputText.setDisabled(false); 
	  countryUseTier1SetToMaximumCheck = false;
	  countryUseTier1SetToMaximumCheckBind.setDisabled(false);
	  
	  countryUseTier2MinAmtInputText.setValue("0");
	  countryUseTier2MinAmtInputText.setDisabled(true); 
	  countryUseTier2RateInputText.setValue("0");
	  countryUseTier2RateInputText.setDisabled(false); 
	  countryUseTier2SetamtInputText.setValue("0");
	  countryUseTier2SetamtInputText.setDisabled(false); 
	  countryUseTier2MaxAmtInputText.setValue("0");
	  countryUseTier2MaxAmtInputText.setDisabled(false); 
	  countryUseTier2SetToMaximumCheck = false;
	  countryUseTier2SetToMaximumCheckBind.setDisabled(false); 
	  countryUseTier2TaxEntireAmountCheck = false;
	  countryUseTier2TaxEntireAmountCheckBind.setDisabled(false); 
	  
	  countryUseTier3RateInputText.setValue("0");
	  countryUseTier3RateInputText.setDisabled(false); 
	  countryUseTier3SetamtInputText.setValue("0");
	  countryUseTier3SetamtInputText.setDisabled(false); 
	  countryUseTier3TaxEntireAmountCheck = false;
	  stateUseTier3TaxEntireAmountCheckBind.setDisabled(false);
	  
	  countryUseMaxtaxAmtInputText.setValue("0");
	  countryUseMaxtaxAmtInputText.setDisabled(false);
	  
	  //state sales
	  stateSalesTier1RateInputText.setValue("0");
	  stateSalesTier1RateInputText.setDisabled(false); 
	  stateSalesTier1SetamtInputText.setValue("0");
	  stateSalesTier1SetamtInputText.setDisabled(false); 
	  stateSalesTier1MaxAmtInputText.setValue("0");
	  stateSalesTier1MaxAmtInputText.setDisabled(false); 
	  stateSalesTier1MaxAmtInputText.setDisabled(false); 
	  stateSalesTier1SetToMaximumCheck = false;
	  stateSalesTier1SetToMaximumCheckBind.setDisabled(false);
	  
	  stateSalesTier2MinAmtInputText.setValue("0");
	  stateSalesTier2MinAmtInputText.setDisabled(true); 
	  stateSalesTier2RateInputText.setValue("0");
	  stateSalesTier2RateInputText.setDisabled(false); 
	  stateSalesTier2SetamtInputText.setValue("0");
	  stateSalesTier2SetamtInputText.setDisabled(false); 
	  stateSalesTier2MaxAmtInputText.setValue("0");
	  stateSalesTier2MaxAmtInputText.setDisabled(false); 
	  stateSalesTier2SetToMaximumCheck = false;
	  stateSalesTier2SetToMaximumCheckBind.setDisabled(false); 
	  stateSalesTier2TaxEntireAmountCheck = false;
	  stateSalesTier2TaxEntireAmountCheckBind.setDisabled(false); 
	  
	  stateSalesTier3RateInputText.setValue("0");
	  stateSalesTier3RateInputText.setDisabled(false); 
	  stateSalesTier3SetamtInputText.setValue("0");
	  stateSalesTier3SetamtInputText.setDisabled(false); 
	  stateSalesTier3TaxEntireAmountCheck = false;
	  stateSalesTier3TaxEntireAmountCheckBind.setDisabled(false); 
	  
	  stateSalesMaxtaxAmtInputText.setValue("0");
	  stateSalesMaxtaxAmtInputText.setDisabled(false);
	  
	  //state use
	  stateSameAsSalesRatesCheck = false;
	  stateSameAsSalesRatesCheckBind.setDisabled(false);  
	  
	  stateUseTier1RateInputText.setValue("0");
	  stateUseTier1RateInputText.setDisabled(false); 
	  stateUseTier1SetamtInputText.setValue("0");
	  stateUseTier1SetamtInputText.setDisabled(false); 
	  stateUseTier1MaxAmtInputText.setValue("0");
	  stateUseTier1MaxAmtInputText.setDisabled(false); 
	  stateUseTier1MaxAmtInputText.setDisabled(false); 
	  stateUseTier1SetToMaximumCheck = false;
	  stateUseTier1SetToMaximumCheckBind.setDisabled(false);
	  
	  stateUseTier2MinAmtInputText.setValue("0");
	  stateUseTier2MinAmtInputText.setDisabled(true); 
	  stateUseTier2RateInputText.setValue("0");
	  stateUseTier2RateInputText.setDisabled(false); 
	  stateUseTier2SetamtInputText.setValue("0");
	  stateUseTier2SetamtInputText.setDisabled(false); 
	  stateUseTier2MaxAmtInputText.setValue("0");
	  stateUseTier2MaxAmtInputText.setDisabled(false); 
	  stateUseTier2SetToMaximumCheck = false;
	  stateUseTier2SetToMaximumCheckBind.setDisabled(false); 
	  stateUseTier2TaxEntireAmountCheck = false;
	  stateUseTier2TaxEntireAmountCheckBind.setDisabled(false); 
	  
	  stateUseTier3RateInputText.setValue("0");
	  stateUseTier3RateInputText.setDisabled(false); 
	  stateUseTier3SetamtInputText.setValue("0");
	  stateUseTier3SetamtInputText.setDisabled(false); 
	  stateUseTier3TaxEntireAmountCheck = false;
	  stateUseTier3TaxEntireAmountCheckBind.setDisabled(false);
	  
	  stateUseMaxtaxAmtInputText.setValue("0");
	  stateUseMaxtaxAmtInputText.setDisabled(false);
	  
	  //county sales
	  countySalesTier1RateInputText.setValue("0");
	  countySalesTier1RateInputText.setDisabled(false); 
	  countySalesTier1SetamtInputText.setValue("0");
	  countySalesTier1SetamtInputText.setDisabled(false); 
	  countySalesTier1MaxAmtInputText.setValue("0");
	  countySalesTier1MaxAmtInputText.setDisabled(false); 
	  countySalesTier1MaxAmtInputText.setDisabled(false); 
	  countySalesTier1SetToMaximumCheck = false;
	  countySalesTier1SetToMaximumCheckBind.setDisabled(false);
	  
	  countySalesTier2MinAmtInputText.setValue("0");
	  countySalesTier2MinAmtInputText.setDisabled(true); 
	  countySalesTier2RateInputText.setValue("0");
	  countySalesTier2RateInputText.setDisabled(false); 
	  countySalesTier2SetamtInputText.setValue("0");
	  countySalesTier2SetamtInputText.setDisabled(false); 
	  countySalesTier2MaxAmtInputText.setValue("0");
	  countySalesTier2MaxAmtInputText.setDisabled(false); 
	  countySalesTier2SetToMaximumCheck = false;
	  countySalesTier2SetToMaximumCheckBind.setDisabled(false); 
	  countySalesTier2TaxEntireAmountCheck = false;
	  countySalesTier2TaxEntireAmountCheckBind.setDisabled(false); 
	  
	  countySalesTier3RateInputText.setValue("0");
	  countySalesTier3RateInputText.setDisabled(false); 
	  countySalesTier3SetamtInputText.setValue("0");
	  countySalesTier3SetamtInputText.setDisabled(false); 
	  countySalesTier3TaxEntireAmountCheck = false;
	  countySalesTier3TaxEntireAmountCheckBind.setDisabled(false); 
	  
	  countySalesMaxtaxAmtInputText.setValue("0");
	  countySalesMaxtaxAmtInputText.setDisabled(false);
	  
	  //county use
	  countySameAsSalesRatesCheck = false;
	  countySameAsSalesRatesCheckBind.setDisabled(false);  
	  
	  countyUseTier1RateInputText.setValue("0");
	  countyUseTier1RateInputText.setDisabled(false); 
	  countyUseTier1SetamtInputText.setValue("0");
	  countyUseTier1SetamtInputText.setDisabled(false); 
	  countyUseTier1MaxAmtInputText.setValue("0");
	  countyUseTier1MaxAmtInputText.setDisabled(false); 
	  countyUseTier1MaxAmtInputText.setDisabled(false); 
	  countyUseTier1SetToMaximumCheck = false;
	  countyUseTier1SetToMaximumCheckBind.setDisabled(false);
	  
	  countyUseTier2MinAmtInputText.setValue("0");
	  countyUseTier2MinAmtInputText.setDisabled(true); 
	  countyUseTier2RateInputText.setValue("0");
	  countyUseTier2RateInputText.setDisabled(false); 
	  countyUseTier2SetamtInputText.setValue("0");
	  countyUseTier2SetamtInputText.setDisabled(false); 
	  countyUseTier2MaxAmtInputText.setValue("0");
	  countyUseTier2MaxAmtInputText.setDisabled(false); 
	  countyUseTier2SetToMaximumCheck = false;
	  countyUseTier2SetToMaximumCheckBind.setDisabled(false); 
	  countyUseTier2TaxEntireAmountCheck = false;
	  countyUseTier2TaxEntireAmountCheckBind.setDisabled(false); 
	  
	  countyUseTier3RateInputText.setValue("0");
	  countyUseTier3RateInputText.setDisabled(false); 
	  countyUseTier3SetamtInputText.setValue("0");
	  countyUseTier3SetamtInputText.setDisabled(false); 
	  countyUseTier3TaxEntireAmountCheck = false;
	  
	  countyUseMaxtaxAmtInputText.setValue("0");
	  countyUseMaxtaxAmtInputText.setDisabled(false);
	  
	  //city sales
	  citySalesTier1RateInputText.setValue("0");
	  citySalesTier1RateInputText.setDisabled(false); 
	  citySalesTier1SetamtInputText.setValue("0");
	  citySalesTier1SetamtInputText.setDisabled(false); 
	  citySalesTier1MaxAmtInputText.setValue("0");
	  citySalesTier1MaxAmtInputText.setDisabled(false); 
	  citySalesTier1MaxAmtInputText.setDisabled(false); 
	  citySalesTier1SetToMaximumCheck = false;
	  citySalesTier1SetToMaximumCheckBind.setDisabled(false);
	  
	  citySalesTier2MinAmtInputText.setValue("0");
	  citySalesTier2MinAmtInputText.setDisabled(true); 
	  citySalesTier2RateInputText.setValue("0");
	  citySalesTier2RateInputText.setDisabled(false); 
	  citySalesTier2SetamtInputText.setValue("0");
	  citySalesTier2SetamtInputText.setDisabled(false); 
	  citySalesTier2MaxAmtInputText.setValue("0");
	  citySalesTier2MaxAmtInputText.setDisabled(false); 
	  citySalesTier2SetToMaximumCheck = false;
	  citySalesTier2SetToMaximumCheckBind.setDisabled(false); 
	  citySalesTier2TaxEntireAmountCheck = false;
	  citySalesTier2TaxEntireAmountCheckBind.setDisabled(false); 
	  
	  citySalesTier3RateInputText.setValue("0");
	  citySalesTier3RateInputText.setDisabled(false); 
	  citySalesTier3SetamtInputText.setValue("0");
	  citySalesTier3SetamtInputText.setDisabled(false); 
	  citySalesTier3TaxEntireAmountCheck = false;
	  citySalesTier3TaxEntireAmountCheckBind.setDisabled(false); 
	  
	  citySalesMaxtaxAmtInputText.setValue("0");
	  citySalesMaxtaxAmtInputText.setDisabled(false);
	  
	  //city use
	  citySameAsSalesRatesCheck = false;
	  citySameAsSalesRatesCheckBind.setDisabled(false);  
	  
	  cityUseTier1RateInputText.setValue("0");
	  cityUseTier1RateInputText.setDisabled(false); 
	  cityUseTier1SetamtInputText.setValue("0");
	  cityUseTier1SetamtInputText.setDisabled(false); 
	  cityUseTier1MaxAmtInputText.setValue("0");
	  cityUseTier1MaxAmtInputText.setDisabled(false); 
	  cityUseTier1MaxAmtInputText.setDisabled(false); 
	  cityUseTier1SetToMaximumCheck = false;
	  cityUseTier1SetToMaximumCheckBind.setDisabled(false);
	  
	  cityUseTier2MinAmtInputText.setValue("0");
	  cityUseTier2MinAmtInputText.setDisabled(true); 
	  cityUseTier2RateInputText.setValue("0");
	  cityUseTier2RateInputText.setDisabled(false); 
	  cityUseTier2SetamtInputText.setValue("0");
	  cityUseTier2SetamtInputText.setDisabled(false); 
	  cityUseTier2MaxAmtInputText.setValue("0");
	  cityUseTier2MaxAmtInputText.setDisabled(false); 
	  cityUseTier2SetToMaximumCheck = false;
	  cityUseTier2SetToMaximumCheckBind.setDisabled(false); 
	  cityUseTier2TaxEntireAmountCheck = false;
	  cityUseTier2TaxEntireAmountCheckBind.setDisabled(false); 
	  
	  cityUseMaxtaxAmtInputText.setValue("0");
	  cityUseMaxtaxAmtInputText.setDisabled(false);
	  
	  cityUseTier3RateInputText.setValue("0");
	  cityUseTier3RateInputText.setDisabled(false); 
	  cityUseTier3SetamtInputText.setValue("0");
	  cityUseTier3SetamtInputText.setDisabled(false); 
	  cityUseTier3TaxEntireAmountCheck = false;
	  
	  stj1UseRateInputText.setValue("0");
	  stj2UseRateInputText.setValue("0");
	  stj3UseRateInputText.setValue("0");
	  stj4UseRateInputText.setValue("0");
	  stj5UseRateInputText.setValue("0");
	  stj1UseSetamtInputText.setValue("0");
	  stj2UseSetamtInputText.setValue("0");
	  stj3UseSetamtInputText.setValue("0");
	  stj4UseSetamtInputText.setValue("0");
	  stj5UseSetamtInputText.setValue("0");
	  
	  stj1SalesRateInputText.setValue("0");
	  stj2SalesRateInputText.setValue("0");
	  stj3SalesRateInputText.setValue("0");
	  stj4SalesRateInputText.setValue("0");
	  stj5SalesRateInputText.setValue("0");
	  stj1SalesSetamtInputText.setValue("0");
	  stj2SalesSetamtInputText.setValue("0");
	  stj3SalesSetamtInputText.setValue("0");
	  stj4SalesSetamtInputText.setValue("0");
	  stj5SalesSetamtInputText.setValue("0");
      
	  resetAddressHandler();
      recalCombinedRates(null);
      
      //PP-24
      effectiveDate.setDisabled(false);
      expirationDate.setDisabled(false);
  
      editTaxRate.init(); // Initialize taxRate DTO Object
      if(nextAction == TaxJurisdictionAction.addJurisdictionTaxrateFromTransaction){
    	  rateTypeCodeMenu.setDisabled(false);
          rateTypeCodeMenu.setReadonly(false);
          editTaxRate.setRatetypeCode((String)rateTypeCodeMenu.getValue());
      }else{
    	  rateTypeCodeMenu.setDisabled(false);
          rateTypeCodeMenu.setReadonly(false);
      }
      break;
    case copyAddJurisdiction:
      if (this.selectedJurisdiction == null) {
        return null; // If there is no Jurisdiction selected, return back to
                      // main page.
      }

      this.currentMode = MODE_EDIT;
      /*
      // Now copy the selected Jurisdictions and TaxRate to the ones that will
      // be edited.
      editJurisdiction = new Jurisdiction(selectedJurisdiction);
      origJurisdiction = new Jurisdiction(selectedJurisdiction);
      editTaxRate = new JurisdictionTaxrate(selectedTaxRate);
      // Clear Out certain fields for new record
      editJurisdiction.setJurisdictionIdString(Jurisdiction.NEW_JURISDICTION_ID_STRING);
      editJurisdiction.setUpdateTimestamp(null);
      editJurisdiction.setUpdateUserId(null);
      */
      
      // Now copy the selected Jurisdictions and TaxRate to the ones that will
      // be edited.
      origJurisdiction = new Jurisdiction(selectedJurisdiction);
      
      //4824
      editJurisdiction = new Jurisdiction();
      editJurisdiction.setJurisdictionIdString(Jurisdiction.NEW_JURISDICTION_ID_STRING);
      editJurisdiction.setGeocode(selectedJurisdiction.getGeocode());
      editJurisdiction.setState(selectedJurisdiction.getState());
      editJurisdiction.setCounty(selectedJurisdiction.getCounty());
      editJurisdiction.setCity(selectedJurisdiction.getCity());
      editJurisdiction.setZip(selectedJurisdiction.getZip());
      editJurisdiction.setZipplus4(selectedJurisdiction.getZipplus4());
      editJurisdiction.setInOut(selectedJurisdiction.getInOut());
      editJurisdiction.setCustomFlag(selectedJurisdiction.getCustomFlag());
      editJurisdiction.setDescription(selectedJurisdiction.getDescription());
      editJurisdiction.setClientGeocode(selectedJurisdiction.getClientGeocode());
      editJurisdiction.setCompGeocode(selectedJurisdiction.getCompGeocode());
      editJurisdiction.setUpdateTimestamp(null);
      editJurisdiction.setUpdateUserId(null);
      
      //3101 & 3098 
      editJurisdiction.setModifiedFlag("0");
      editJurisdiction.setReportingCity(selectedJurisdiction.getReportingCity());
      editJurisdiction.setReportingCityFips(selectedJurisdiction.getReportingCityFips());
      
      editJurisdiction.setCountry(selectedJurisdiction.getCountry());
	  editJurisdiction.setState(selectedJurisdiction.getState());
      
      searchCountryDetail = editJurisdiction.getCountry();
	  searchstateDetail = editJurisdiction.getState();
	  if(stateMenuItemsDetail!=null){
		  stateMenuItemsDetail.clear();
		  stateMenuItemsDetail=null;
	  }
	  
	  editJurisdiction.setEffectiveDate(selectedJurisdiction.getEffectiveDate());
	  editJurisdiction.setExpirationDate(selectedJurisdiction.getExpirationDate());
	  
	  editJurisdiction.setStj1Name(selectedJurisdiction.getStj1Name());
	  editJurisdiction.setStj2Name(selectedJurisdiction.getStj2Name());
	  editJurisdiction.setStj3Name(selectedJurisdiction.getStj3Name());
	  editJurisdiction.setStj4Name(selectedJurisdiction.getStj4Name());
	  editJurisdiction.setStj5Name(selectedJurisdiction.getStj5Name());
	  
	  editJurisdiction.setCountryNexusindCode(selectedJurisdiction.getCountryNexusindCode());
      editJurisdiction.setStateNexusindCode(selectedJurisdiction.getStateNexusindCode());
      editJurisdiction.setCountyNexusindCode(selectedJurisdiction.getCountyNexusindCode());
      editJurisdiction.setCityNexusindCode(selectedJurisdiction.getCityNexusindCode());
      editJurisdiction.setStj1NexusindCode(selectedJurisdiction.getStj1NexusindCode());
      editJurisdiction.setStj2NexusindCode(selectedJurisdiction.getStj2NexusindCode());
      editJurisdiction.setStj3NexusindCode(selectedJurisdiction.getStj3NexusindCode());
      editJurisdiction.setStj4NexusindCode(selectedJurisdiction.getStj4NexusindCode());
      editJurisdiction.setStj5NexusindCode(selectedJurisdiction.getStj5NexusindCode());
	  
      //TaxRate
      editTaxRate = new JurisdictionTaxrate(selectedTaxRate);
      editTaxRate.setJurisdiction(null);
      editTaxRate.setJurisdictionTaxrateIdString(JurisdictionTaxrate.NEW_JURISDICTION_TAXRATE_ID_STRING);
      
       //PP-24
       //Country Tier 1
       if (new Long(JurisdictionTaxrate.MAX_AMOUNT).equals(editTaxRate.getCountrySalesTier1MaxAmt())) {
    	   countrySalesTier1MaxAmtInputText.setValue(JurisdictionTaxrate.MAX_STRING);
    	   countrySalesTier1MaxAmtInputText.setDisabled(true);
           countrySalesTier2MinAmtInputText.setDisabled(false);
           countrySalesTier2MinAmtInputText.setValue(""+editTaxRate.getCountrySalesTier2MinAmt());
           countrySalesTier1SetToMaximumCheck = true;
       }
       else{
    	   countrySalesTier1MaxAmtInputText.setDisabled(false);
           countrySalesTier2MinAmtInputText.setDisabled(true);
           countrySalesTier1SetToMaximumCheck = false;
           countrySalesTier1MaxAmtInputText.setValue(""+editTaxRate.getCountrySalesTier1MaxAmt());
           countrySalesTier2MinAmtInputText.setValue(""+editTaxRate.getCountrySalesTier2MinAmt());
       }
       
       if (new Long(JurisdictionTaxrate.MAX_AMOUNT).equals(editTaxRate.getCountryUseTier1MaxAmt())) {
    	   countryUseTier1MaxAmtInputText.setValue(JurisdictionTaxrate.MAX_STRING);
    	   countryUseTier1MaxAmtInputText.setDisabled(true);
           countryUseTier2MinAmtInputText.setDisabled(false);
           countryUseTier2MinAmtInputText.setValue(""+editTaxRate.getCountryUseTier2MinAmt());
           countryUseTier1SetToMaximumCheck = true;
       }
       else{
    	   countryUseTier1MaxAmtInputText.setDisabled(false);
           countryUseTier2MinAmtInputText.setDisabled(true);
           countryUseTier1SetToMaximumCheck = false;
           countryUseTier1MaxAmtInputText.setValue(""+editTaxRate.getCountryUseTier1MaxAmt());
           countryUseTier2MinAmtInputText.setValue(""+editTaxRate.getCountryUseTier2MinAmt());
       }
      
       //Country Tier 2
       if (new Long(JurisdictionTaxrate.MAX_AMOUNT).equals(editTaxRate.getCountrySalesTier2MaxAmt())) {
            countrySalesTier2MaxAmtInputText.setValue(JurisdictionTaxrate.MAX_STRING);
            countrySalesTier2MaxAmtInputText.setDisabled(true);
            countrySalesTier2SetToMaximumCheck = true;
            
            countrySalesTier3RateInputText.setDisabled(true);
            countrySalesTier3SetamtInputText.setDisabled(true);
            countrySalesTier3TaxEntireAmountCheckBind.setDisabled(true);
       } 
       else {
            countrySalesTier2MaxAmtInputText.setDisabled(false);
            countrySalesTier2MaxAmtInputText.setValue(""+editTaxRate.getCountrySalesTier2MaxAmt());
            countrySalesTier2SetToMaximumCheck = false;
            
            countrySalesTier3RateInputText.setDisabled(false);
            countrySalesTier3SetamtInputText.setDisabled(false);
            countrySalesTier3TaxEntireAmountCheckBind.setDisabled(false);
       }
       
       if (new Long(JurisdictionTaxrate.MAX_AMOUNT).equals(editTaxRate.getCountryUseTier2MaxAmt())) {
           countryUseTier2MaxAmtInputText.setValue(JurisdictionTaxrate.MAX_STRING);
           countryUseTier2MaxAmtInputText.setDisabled(true);
           countryUseTier2SetToMaximumCheck = true;
           
           countryUseTier3RateInputText.setDisabled(true);
           countryUseTier3SetamtInputText.setDisabled(true);
           countryUseTier3TaxEntireAmountCheckBind.setDisabled(true);
      } 
      else {
           countryUseTier2MaxAmtInputText.setDisabled(false);
           countryUseTier2MaxAmtInputText.setValue(""+editTaxRate.getCountryUseTier2MaxAmt());
           countryUseTier2SetToMaximumCheck = false;
           
           countryUseTier3RateInputText.setDisabled(false);
           countryUseTier3SetamtInputText.setDisabled(false);
           countryUseTier3TaxEntireAmountCheckBind.setDisabled(false);
      }
       
      t3 = editTaxRate.getStateUseTier3Rate() == null ? ArithmeticUtils.ZERO : editTaxRate.getStateUseTier3Rate();
      t2 = editTaxRate.getStateUseTier2Rate() == null ? ArithmeticUtils.ZERO : editTaxRate.getStateUseTier2Rate();
      if((t2.compareTo(ArithmeticUtils.ZERO) > 0) || (t3.compareTo(ArithmeticUtils.ZERO) > 0)){
    	  stateMaxtaxAmount.setDisabled(true);
      }else{
    	  stateMaxtaxAmount.setDisabled(false);
      }
      rateTypeCodeMenu.setDisabled(false);
      rateTypeCodeMenu.setReadonly(false);
      effectiveDate.setDisabled(false);
      expirationDate.setDisabled(false);
      stateUseTier2RateInput.setDisabled(false);
      stateSalesTier2RateInput.setDisabled(false);
      stateUseTier2RateInput.setValue(""+editTaxRate.getStateUseTier2Rate());
      stateSalesTier2RateInput.setValue(""+editTaxRate.getStateSalesTier2Rate());    
      countrySalesTier2MinAmtInputText.setValue(""+editTaxRate.getCountrySalesTier2MinAmt());
      countryUseTier2MinAmtInputText.setValue(""+editTaxRate.getCountryUseTier2MinAmt()); 
      stateUseTier3RateInputText.setValue(""+editTaxRate.getStateUseTier3Rate());
      stateSalesTier3RateInputText.setValue(""+editTaxRate.getStateSalesTier3Rate());
      
	  stj1UseRateInputText.setValue(""+editTaxRate.getStj1UseRate());
	  stj2UseRateInputText.setValue(""+editTaxRate.getStj2UseRate());
	  stj3UseRateInputText.setValue(""+editTaxRate.getStj3UseRate());
	  stj4UseRateInputText.setValue(""+editTaxRate.getStj4UseRate());
	  stj5UseRateInputText.setValue(""+editTaxRate.getStj5UseRate());  
	  stj1SalesRateInputText.setValue(""+editTaxRate.getStj1SalesRate());
	  stj2SalesRateInputText.setValue(""+editTaxRate.getStj2SalesRate());
	  stj3SalesRateInputText.setValue(""+editTaxRate.getStj3SalesRate());
	  stj4SalesRateInputText.setValue(""+editTaxRate.getStj4SalesRate());
	  stj5SalesRateInputText.setValue(""+editTaxRate.getStj5SalesRate());
	  
	  recalCombinedRates(null);
      
      editTaxRate.setStateTier3MaxAmount(JurisdictionTaxrate.MAX_STRING);
      editTaxRate.setUpdateTimestamp(null);
      editTaxRate.setUpdateUserId(null);
      view = "tax_jurisdiction_add";
      break;
    case copyAddJurisdictionTaxrate:
    	//case copyAddJurisdictionTaxrateFromTransaction:
    	if ((this.selectedJurisdiction == null) || (this.selectedTaxRate == null)) {
    		return null; // If there is no Jurisdiction and/or Tax Rate selected, return back to main page.
    	}
    	this.currentMode = MODE_EDITTR;

    	// Now copy the selected Jurisdictions and TaxRate to the ones that will be edited.
    	editJurisdiction = new Jurisdiction(selectedJurisdiction);
      
    	//5133, the Id can not be copied over.
    	editJurisdiction.setJurisdictionId(selectedJurisdiction.getJurisdictionId());
      
    	searchCountryDetail = editJurisdiction.getCountry();
    	searchstateDetail = editJurisdiction.getState();
    	if(stateMenuItemsDetail!=null){
    		stateMenuItemsDetail.clear();
    		stateMenuItemsDetail=null;
    	}
      
    	editTaxRate = new JurisdictionTaxrate(selectedTaxRate);

    	loadToControl(TaxJurisdictionAction.copyAddJurisdictionTaxrate);
    	resetAddressHandler();
	  	recalCombinedRates(null);

	  	// Clear Out certain fields for new record
	  	editTaxRate.setJurisdictionTaxrateIdString(JurisdictionTaxrate.NEW_JURISDICTION_TAXRATE_ID_STRING);
	  	editTaxRate.setUpdateTimestamp(null);
	  	editTaxRate.setUpdateUserId(null);
	  	break;
    case updateJurisdiction:
    case viewJurisdiction:
    case viewJurisdictionFromTransaction:
    case viewJurisdictionFromTransactionLog:
    	if (this.selectedJurisdiction == null) {
            return null; // If there is no Jurisdiction selected, return back to
                          // main page.
          }
          if (nextAction == TaxJurisdictionAction.updateJurisdiction || nextAction == TaxJurisdictionAction.viewJurisdiction  || nextAction == TaxJurisdictionAction.viewJurisdictionFromTransaction
        		  || nextAction == TaxJurisdictionAction.viewJurisdictionFromTransactionLog) {
            this.currentMode = MODE_EDIT;
            this.trShowSection = UI_HIDDEN; // Hide the Tax Rate Section
            if (nextAction == TaxJurisdictionAction.viewJurisdiction || nextAction == TaxJurisdictionAction.viewJurisdictionFromTransaction
            		|| nextAction == TaxJurisdictionAction.viewJurisdictionFromTransactionLog) {
            	this.showCancelButton = false;
            }
          } else {
            if (this.selectedTaxRate == null) {
              return null; // If there is no Tax Rate selected, return back to main
                            // page.
            }
            this.currentMode = MODE_EDITTR;
          }
          
          //0003257
          //editJurisdiction = selectedJurisdiction;
          editJurisdiction = new Jurisdiction(selectedJurisdiction);
          editJurisdiction.setJurisdictionId(new Long(selectedJurisdiction.getJurisdictionId()));        
          editJurisdiction.setJurisdictionIdString(editJurisdiction.getJurisdictionId().toString());
          
          searchCountryDetail = editJurisdiction.getCountry();
    	  searchstateDetail = editJurisdiction.getState();
    	  if(stateMenuItemsDetail!=null){
    		  stateMenuItemsDetail.clear();
    		  stateMenuItemsDetail=null;
    	  }
    	  
          origJurisdiction = new Jurisdiction(editJurisdiction);
          view = "tax_jurisdiction_add";
          break;
    case updateJurisdictionTaxrate:
    	if (this.selectedJurisdiction == null) {
    		return null; // If there is no Jurisdiction selected, return back to main page.
    	}
    	
    	if (nextAction == TaxJurisdictionAction.updateJurisdiction) {
    		this.currentMode = MODE_EDIT;
    		this.trShowSection = UI_HIDDEN; // Hide the Tax Rate Section
    	}
    	else {
    		if (this.selectedTaxRate == null) {
    			return null; // If there is no Tax Rate selected, return back to main page.
    		}
    		this.currentMode = MODE_EDITTR;
    	}
    	
    	editJurisdiction = selectedJurisdiction;
    	editJurisdiction.setJurisdictionIdString(editJurisdiction.getJurisdictionId().toString());
      
    	searchCountryDetail = editJurisdiction.getCountry();
    	searchstateDetail = editJurisdiction.getState();
    	if(stateMenuItemsDetail!=null){
    		stateMenuItemsDetail.clear();
    		stateMenuItemsDetail=null;
    	}	  
    	origJurisdiction = new Jurisdiction(editJurisdiction);

    	// Now load the selected Jurisdictions and TaxRate to the ones that will be edited.
    	if (selectedTaxRate != null) {
    		if (selectedTaxRate.getJurisdictionTaxrateId() == null) {
    			// Trying to update a Tax Jurisdiction that has no tax rates.
    			// So just create a blank one.
    			editTaxRate = new JurisdictionTaxrate();
    			editTaxRate.init();
    		}
    		else {
    			editTaxRate = selectedTaxRate;
    			editTaxRate.setJurisdictionTaxrateIdString(selectedTaxRate.getJurisdictionTaxrateId().toString());
    		}
    		
    		long rateUsedCount = taxRateService.getTaxrateUsedCount(editTaxRate.getId());
  	  		if(rateUsedCount > 0) {
  	  			rateUsed=true;
  	  		}
        
    		loadToControl(TaxJurisdictionAction.updateJurisdictionTaxrate);	
    		resetAddressHandler();
  	  		recalCombinedRates(null);

  	  		origTaxRate = new JurisdictionTaxrate(editTaxRate);
    	}
    	break;
    	
    case deleteJurisdiction:
    case deleteJurisdictionTaxrate:
      if (this.selectedJurisdiction == null) {
        return null; // If there is no Jurisdiction selected, return back to
                      // main page.
      }
      if (nextAction == TaxJurisdictionAction.deleteJurisdiction) {
        this.currentMode = MODE_VIEW;
        this.trShowSection = UI_HIDDEN; // Hide the Tax Rate Section
        view = "tax_jurisdiction_add";
        logger.info("Inside IF " + nextAction);
      } else {
        if (this.selectedTaxRate == null) {
          logger.info("Inside Else " + nextAction);
          return null; // If there is no Tax Rate selected, return back to main
                        // page.

        }
      }
      
      if(checkJurisdictionUsage().size() > 0) {
    	 setShowOkButton(false); 
    	 
    	 //Need to hide the Ok button on the next screen, currently not working.
    	 //Maybe set a flag?
//    	 UIComponent formOkButton = currContext.getViewRoot().findComponent("jurisdictionViewForm").findComponent("t1ok");
//    	 formOkButton.getAttributes().put("rendered", false);
      }
      
      this.currentMode = MODE_VIEW;
      
      // Now load the selected Jurisdictions and TaxRate to the ones that will
      // be edited.
      editJurisdiction = selectedJurisdiction;
      
      //0002051: Jurisdiction ID not displayed on Delete after Update.
      editJurisdiction.setJurisdictionIdString(editJurisdiction.getJurisdictionId().toString());
      
      searchCountryDetail = editJurisdiction.getCountry();
	  searchstateDetail = editJurisdiction.getState();
	  if(stateMenuItemsDetail!=null){
		  stateMenuItemsDetail.clear();
		  stateMenuItemsDetail=null;
	  }
	  
      editTaxRate = selectedTaxRate;
      if (selectedTaxRate != null) {
    	  editTaxRate.setJurisdictionTaxrateIdString(selectedTaxRate.getJurisdictionTaxrateId().toString());
    	  
    	loadToControl(TaxJurisdictionAction.deleteJurisdictionTaxrate); 	
    	resetAddressHandler();
  	  	recalCombinedRates(null);

      }
      break;
    default:
      logger.error("Unhandled state: " + nextAction);
      return null;
    }
    
    setDefaultControls();

    // Build inOut Selection
    buildInOutSelection(uniqueIdNumber);

    // reset the state Menu
    stateMenu.setValue("-1");

    this.currentAction = nextAction;
    logger.debug("processJurisdictionSelection: " + nextAction);
    return view;
  }

    // reset controls back to state from selection (used on cancel)
    public void resetControls() {
      if (selectedTaxRate != null) {
    	  //CP-24
    	  countrySalesTier1MaxAmtInputText.setValue(selectedTaxRate.getCountrySalesTier1MaxAmt());
          countrySalesTier2MinAmtInputText.setValue(selectedTaxRate.getCountrySalesTier2MinAmt());
          countrySalesTier2MaxAmtInputText.setValue(selectedTaxRate.getCountrySalesTier2MaxAmt());
          
          countryUseTier1MaxAmtInputText.setValue(selectedTaxRate.getCountryUseTier1MaxAmt());
          countryUseTier2MinAmtInputText.setValue(selectedTaxRate.getCountryUseTier2MinAmt());
          countryUseTier2MaxAmtInputText.setValue(selectedTaxRate.getCountryUseTier2MaxAmt());
          
          stateUseTier3RateInputText.setValue(selectedTaxRate.getStateUseTier3Rate());
          stateSalesTier3RateInputText.setValue(selectedTaxRate.getStateSalesTier3Rate());
        
  	  		stj1UseRateInputText.setValue(selectedTaxRate.getStj1UseRate());
  	  		stj2UseRateInputText.setValue(selectedTaxRate.getStj2UseRate());
  	  		stj3UseRateInputText.setValue(selectedTaxRate.getStj3UseRate());
  	  		stj4UseRateInputText.setValue(selectedTaxRate.getStj4UseRate());
  	  		stj5UseRateInputText.setValue(selectedTaxRate.getStj5UseRate());
	  	
  	  		stj1SalesRateInputText.setValue(selectedTaxRate.getStj1SalesRate());
  	  		stj2SalesRateInputText.setValue(selectedTaxRate.getStj2SalesRate());
  	  		stj3SalesRateInputText.setValue(selectedTaxRate.getStj3SalesRate());
  	  		stj4SalesRateInputText.setValue(selectedTaxRate.getStj4SalesRate());
  	  		stj5SalesRateInputText.setValue(selectedTaxRate.getStj5SalesRate());
  	  	
  	  		recalCombinedRates(null);
        
  	  		stateUseTier2RateInput.setValue(selectedTaxRate.getStateUseTier2Rate());
  	  		stateSalesTier2RateInput.setValue(selectedTaxRate.getStateSalesTier2Rate());
      	}
    }

  /*
   * 
   * The ACTION PROCESS Methods
   * 
   */

  /**
   * This function performs the Addition of a Tax Jurisdiction to the database.
   * The function also supports the addition of a Tax Rate if one is available.
   * 
   * Note: if the Tax Rate insertion fails, the Tax Jurisdiction will be removed
   * also.
   * 
   * @return - Return boolean of whether the Add Jurisdiction was completed or
   *         not.
   * 
   * TODO: Need to add support for Exception Handling and User Interface
   * Interaction
   */
    
    private Boolean checkDuplicateJurisdiction(){
    	Jurisdiction searchCityCountyStateZip = new Jurisdiction();
        searchCityCountyStateZip.setCity(editJurisdiction.getCity());
        searchCityCountyStateZip.setCounty(editJurisdiction.getCounty());
        searchCityCountyStateZip.setGeocode(editJurisdiction.getGeocode());
        searchCityCountyStateZip.setZip(editJurisdiction.getZip());
        searchCityCountyStateZip.setState(editJurisdiction.getState());
        List<Jurisdiction> searchCityCountyStateZipList = jurisdictionService.findByExample(
            searchCityCountyStateZip, 0);

        if (searchCityCountyStateZipList.isEmpty()) {
          logger.info("Inside If condition list empty ");
     
          return true;
        } else {
          FacesMessage msg = new FacesMessage("Duplicate Jurisdiction exists.");
          msg.setSeverity(FacesMessage.SEVERITY_ERROR);
          FacesContext.getCurrentInstance().addMessage(null, msg);
          return false;
        }
    	
    	
    	
    }
    
    private List<Jurisdiction> getNewlyAddedJurisdiction(){
    	Jurisdiction searchCityCountyStateZip = new Jurisdiction();
        searchCityCountyStateZip.setCity(editJurisdiction.getCity());
        searchCityCountyStateZip.setCounty(editJurisdiction.getCounty());
        searchCityCountyStateZip.setGeocode(editJurisdiction.getGeocode());
        searchCityCountyStateZip.setZip(editJurisdiction.getZip());
        searchCityCountyStateZip.setState(editJurisdiction.getState());
        List<Jurisdiction> searchCityCountyStateZipList = jurisdictionService.findByExample(
            searchCityCountyStateZip, 0);
        if(searchCityCountyStateZipList.isEmpty()){
        	return new ArrayList<Jurisdiction>();
        }else {
			return searchCityCountyStateZipList;
		}
    	
    }
    

    private boolean processAddJurisdiction() {
    	logger.info("Enter processAddJurisdiction ");
    	
    	if(!checkDuplicateJurisdiction()){
        	return false;
    	}
    	    	
    	editJurisdiction.setCustomFlag("1"); // Any user added items are custom.
    	
    	if(selectedJurisdiction != null && selectedJurisdiction.getCopyRatesFlag()) {
    		jurisdictionService.save(editJurisdiction);
    		Long jurId = editJurisdiction.getJurisdictionId();
    		Jurisdiction copyaddjurisdiction = jurisdictionService.findById(jurId);
    		 for(JurisdictionTaxrate jurisdictionTaxrate : taxRateList) {
    	      	 JurisdictionTaxrate copyaddTaxRate = new JurisdictionTaxrate(jurisdictionTaxrate);
    	      	 copyaddTaxRate.setJurisdiction(copyaddjurisdiction);
    	      	 taxRateService.save(copyaddTaxRate);
    	      	  
    	     }
    	}
    	else {
    		jurisdictionService.save(editJurisdiction);
    	}
    
		try {
			purchaseTransactionService.processSuspendedTransactions(editJurisdiction.getId(), PurchaseTransactionServiceConstants.SuspendReason.SUSPEND_RATE, false);
		} catch (PurchaseTransactionProcessingException e) {
			e.printStackTrace();
		}

        return true;
    }
    
    public static BigDecimal toBigDecimal(Object o) {
    	BigDecimal bd = new BigDecimal ("0");
		if(o != null && o.toString().length()>0 && !o.toString().equalsIgnoreCase("null") ) {
			try {
				bd = new BigDecimal(o.toString());
			}
			catch(Exception e) {
				e.printStackTrace();
			}
		}
		
		return bd;
	}
    
    private void populateTaxRateValues(){
    	//start of country
    	//Country sales
    	editTaxRate.setCountrySalesTier1Rate(TaxJurisdictionBackingBean.toBigDecimal(countrySalesTier1RateInputText.getValue()));
    	editTaxRate.setCountrySalesTier1Setamt(TaxJurisdictionBackingBean.toBigDecimal(countrySalesTier1SetamtInputText.getValue()));
    	
    	if(countrySalesTier1MaxAmtInputText.getValue()!=null && "*MAX".equalsIgnoreCase(countrySalesTier1MaxAmtInputText.getValue().toString())){
    		editTaxRate.setCountrySalesTier1MaxAmt(TaxJurisdictionBackingBean.toBigDecimal(JurisdictionTaxrate.MAX_AMOUNT));
    	}
    	else{
    		editTaxRate.setCountrySalesTier1MaxAmt(TaxJurisdictionBackingBean.toBigDecimal(countrySalesTier1MaxAmtInputText.getValue()));
    	}
  	  	//countrySalesTier1SetToMaximumCheck = false;
    	
    	editTaxRate.setCountrySalesTier2MinAmt(TaxJurisdictionBackingBean.toBigDecimal(countrySalesTier2MinAmtInputText.getValue()));
    	editTaxRate.setCountrySalesTier2Rate(TaxJurisdictionBackingBean.toBigDecimal(countrySalesTier2RateInputText.getValue()));
    	editTaxRate.setCountrySalesTier2Setamt(TaxJurisdictionBackingBean.toBigDecimal(countrySalesTier2SetamtInputText.getValue()));
    	if (countrySalesTier2MaxAmtInputText.getValue()!=null && "*MAX".equalsIgnoreCase(countrySalesTier2MaxAmtInputText.getValue().toString())) {
	  		editTaxRate.setCountrySalesTier2MaxAmt(BigDecimal.valueOf(JurisdictionTaxrate.MAX_AMOUNT));
	  	} 
	  	else {
	  		editTaxRate.setCountrySalesTier2MaxAmt(TaxJurisdictionBackingBean.toBigDecimal(countrySalesTier2MaxAmtInputText.getValue()));
	  	}
  	  	//countrySalesTier2SetToMaximumCheck = false;
    	editTaxRate.setCountrySalesTier2EntamFlag((countrySalesTier2TaxEntireAmountCheck)? "1" : "0");

    	editTaxRate.setCountrySalesTier3Rate(TaxJurisdictionBackingBean.toBigDecimal(countrySalesTier3RateInputText.getValue()));
    	editTaxRate.setCountrySalesTier3Setamt(TaxJurisdictionBackingBean.toBigDecimal(countrySalesTier3SetamtInputText.getValue()));
    	editTaxRate.setCountrySalesTier3EntamFlag((countrySalesTier3TaxEntireAmountCheck)? "1" : "0");

    	editTaxRate.setCountrySalesMaxtaxAmt(TaxJurisdictionBackingBean.toBigDecimal(countrySalesMaxtaxAmtInputText.getValue()));
    	
  	  	//Country use
    	editTaxRate.setCountryUseSameSales((countrySameAsSalesRatesCheck)? "1" : "0");
    	if(countrySameAsSalesRatesCheck){
  	    	//Copy sales rates to use rates
    		editTaxRate.setCountryUseTier1Rate(editTaxRate.getCountrySalesTier1Rate());
	  	    editTaxRate.setCountryUseTier1Setamt(editTaxRate.getCountrySalesTier1Setamt());
	  	    editTaxRate.setCountryUseTier1MaxAmt(editTaxRate.getCountrySalesTier1MaxAmt());
	
	  	    //countryUseTier1SetToMaximumCheck = false;
	  	 
	  	    editTaxRate.setCountryUseTier2MinAmt(editTaxRate.getCountrySalesTier2MinAmt());
	  	    editTaxRate.setCountryUseTier2Rate(editTaxRate.getCountrySalesTier2Rate());
	  	    editTaxRate.setCountryUseTier2Setamt(editTaxRate.getCountrySalesTier2Setamt());
	  	    editTaxRate.setCountryUseTier2MaxAmt(editTaxRate.getCountrySalesTier2MaxAmt());
	  	    //countryUseTier2SetToMaximumCheck = false;
	  	    editTaxRate.setCountryUseTier2EntamFlag(editTaxRate.getCountrySalesTier2EntamFlag());
	  	  
	  	    editTaxRate.setCountryUseTier3Rate(editTaxRate.getCountrySalesTier3Rate());
	  	    editTaxRate.setCountryUseTier3Setamt(editTaxRate.getCountrySalesTier3Setamt());
	  	    editTaxRate.setCountryUseTier3EntamFlag(editTaxRate.getCountrySalesTier3EntamFlag());	
    	}
    	else{ 	  
	  	    editTaxRate.setCountryUseTier1Rate(TaxJurisdictionBackingBean.toBigDecimal(countryUseTier1RateInputText.getValue()));
	  	    editTaxRate.setCountryUseTier1Setamt(TaxJurisdictionBackingBean.toBigDecimal(countryUseTier1SetamtInputText.getValue()));
	  	    
	  	    if(countryUseTier1MaxAmtInputText.getValue()!=null && "*MAX".equalsIgnoreCase(countryUseTier1MaxAmtInputText.getValue().toString())){
	  	    	editTaxRate.setCountryUseTier1MaxAmt(TaxJurisdictionBackingBean.toBigDecimal(JurisdictionTaxrate.MAX_AMOUNT));
	  	    }
	  	    else{
	  	    	editTaxRate.setCountryUseTier1MaxAmt(TaxJurisdictionBackingBean.toBigDecimal(countryUseTier1MaxAmtInputText.getValue()));
	  	    }
	  	    //countryUseTier1SetToMaximumCheck = false;
	  	 
	  	    editTaxRate.setCountryUseTier2MinAmt(TaxJurisdictionBackingBean.toBigDecimal(countryUseTier2MinAmtInputText.getValue()));
	  	    editTaxRate.setCountryUseTier2Rate(TaxJurisdictionBackingBean.toBigDecimal(countryUseTier2RateInputText.getValue()));
	  	    editTaxRate.setCountryUseTier2Setamt(TaxJurisdictionBackingBean.toBigDecimal(countryUseTier2SetamtInputText.getValue()));
	  	    if (countryUseTier2MaxAmtInputText.getValue()!=null && "*MAX".equalsIgnoreCase(countryUseTier2MaxAmtInputText.getValue().toString())) {
		  		editTaxRate.setCountryUseTier2MaxAmt(BigDecimal.valueOf(JurisdictionTaxrate.MAX_AMOUNT));
		  	} 
		  	else {
		  		editTaxRate.setCountryUseTier2MaxAmt(TaxJurisdictionBackingBean.toBigDecimal(countryUseTier2MaxAmtInputText.getValue()));
		  	}
	  	    //countryUseTier2SetToMaximumCheck = false;
	  	    editTaxRate.setCountryUseTier2EntamFlag((countryUseTier2TaxEntireAmountCheck)? "1" : "0");
	  	  
	  	    editTaxRate.setCountryUseTier3Rate(TaxJurisdictionBackingBean.toBigDecimal(countryUseTier3RateInputText.getValue()));
	  	    editTaxRate.setCountryUseTier3Setamt(TaxJurisdictionBackingBean.toBigDecimal(countryUseTier3SetamtInputText.getValue()));
	  	    editTaxRate.setCountryUseTier3EntamFlag((countryUseTier3TaxEntireAmountCheck)? "1" : "0");
	  	    
	  	    editTaxRate.setCountryUseMaxtaxAmt(TaxJurisdictionBackingBean.toBigDecimal(countryUseMaxtaxAmtInputText.getValue()));
    	}
  	    //end of country
  	    
  	    //start of state
    	//State sales
    	editTaxRate.setStateSalesTier1Rate(TaxJurisdictionBackingBean.toBigDecimal(stateSalesTier1RateInputText.getValue()));
    	editTaxRate.setStateSalesTier1Setamt(TaxJurisdictionBackingBean.toBigDecimal(stateSalesTier1SetamtInputText.getValue()));
    	
    	if(stateSalesTier1MaxAmtInputText.getValue()!=null && "*MAX".equalsIgnoreCase(stateSalesTier1MaxAmtInputText.getValue().toString())){
    		editTaxRate.setStateSalesTier1MaxAmt(TaxJurisdictionBackingBean.toBigDecimal(JurisdictionTaxrate.MAX_AMOUNT));
    	}
    	else{
    		editTaxRate.setStateSalesTier1MaxAmt(TaxJurisdictionBackingBean.toBigDecimal(stateSalesTier1MaxAmtInputText.getValue()));
    	}
  	  	//stateSalesTier1SetToMaximumCheck = false;
    	
    	editTaxRate.setStateSalesTier2MinAmt(TaxJurisdictionBackingBean.toBigDecimal(stateSalesTier2MinAmtInputText.getValue()));
    	editTaxRate.setStateSalesTier2Rate(TaxJurisdictionBackingBean.toBigDecimal(stateSalesTier2RateInputText.getValue()));
    	editTaxRate.setStateSalesTier2Setamt(TaxJurisdictionBackingBean.toBigDecimal(stateSalesTier2SetamtInputText.getValue()));
    	if (stateSalesTier2MaxAmtInputText.getValue()!=null && "*MAX".equalsIgnoreCase(stateSalesTier2MaxAmtInputText.getValue().toString())) {
	  		editTaxRate.setStateSalesTier2MaxAmt(BigDecimal.valueOf(JurisdictionTaxrate.MAX_AMOUNT));
	  	} 
	  	else {
	  		editTaxRate.setStateSalesTier2MaxAmt(TaxJurisdictionBackingBean.toBigDecimal(stateSalesTier2MaxAmtInputText.getValue()));
	  	}
  	  	//stateSalesTier2SetToMaximumCheck = false;
    	editTaxRate.setStateSalesTier2EntamFlag((stateSalesTier2TaxEntireAmountCheck)? "1" : "0");

    	editTaxRate.setStateSalesTier3Rate(TaxJurisdictionBackingBean.toBigDecimal(stateSalesTier3RateInputText.getValue()));
    	editTaxRate.setStateSalesTier3Setamt(TaxJurisdictionBackingBean.toBigDecimal(stateSalesTier3SetamtInputText.getValue()));
    	editTaxRate.setStateSalesTier3EntamFlag((stateSalesTier3TaxEntireAmountCheck)? "1" : "0");
  	  
    	editTaxRate.setStateSalesMaxtaxAmt(TaxJurisdictionBackingBean.toBigDecimal(stateSalesMaxtaxAmtInputText.getValue()));
    	
  	  	//State use
  	    editTaxRate.setStateUseSameSales((stateSameAsSalesRatesCheck)? "1" : "0");
  	    if(stateSameAsSalesRatesCheck){
	    	//Copy sales rates to use rates
  	    	editTaxRate.setStateUseTier1Rate(editTaxRate.getStateSalesTier1Rate());
	  	    editTaxRate.setStateUseTier1Setamt(editTaxRate.getStateSalesTier1Setamt());
	  	    editTaxRate.setStateUseTier1MaxAmt(editTaxRate.getStateSalesTier1MaxAmt());
	
	  	    //stateUseTier1SetToMaximumCheck = false;
	  	 
	  	    editTaxRate.setStateUseTier2MinAmt(editTaxRate.getStateSalesTier2MinAmt());
	  	    editTaxRate.setStateUseTier2Rate(editTaxRate.getStateSalesTier2Rate());
	  	    editTaxRate.setStateUseTier2Setamt(editTaxRate.getStateSalesTier2Setamt());
	  	    editTaxRate.setStateUseTier2MaxAmt(editTaxRate.getStateSalesTier2MaxAmt());
	  	    //stateUseTier2SetToMaximumCheck = false;
	  	    editTaxRate.setStateUseTier2EntamFlag(editTaxRate.getStateSalesTier2EntamFlag());
	  	  
	  	    editTaxRate.setStateUseTier3Rate(editTaxRate.getStateSalesTier3Rate());
	  	    editTaxRate.setStateUseTier3Setamt(editTaxRate.getStateSalesTier3Setamt());
	  	    editTaxRate.setStateUseTier3EntamFlag(editTaxRate.getStateSalesTier3EntamFlag());	
  	    }
  	    else{   
	  	    editTaxRate.setStateUseTier1Rate(TaxJurisdictionBackingBean.toBigDecimal(stateUseTier1RateInputText.getValue()));
	  	    editTaxRate.setStateUseTier1Setamt(TaxJurisdictionBackingBean.toBigDecimal(stateUseTier1SetamtInputText.getValue()));
	  	    
	  	    if(stateUseTier1MaxAmtInputText.getValue()!=null && "*MAX".equalsIgnoreCase(stateUseTier1MaxAmtInputText.getValue().toString())){
	  	    	editTaxRate.setStateUseTier1MaxAmt(TaxJurisdictionBackingBean.toBigDecimal(JurisdictionTaxrate.MAX_AMOUNT));
	  	    }
	  	    else{
	  	    	editTaxRate.setStateUseTier1MaxAmt(TaxJurisdictionBackingBean.toBigDecimal(stateUseTier1MaxAmtInputText.getValue()));
	  	    }
	  	    //stateUseTier1SetToMaximumCheck = false;
	  	 
	  	    editTaxRate.setStateUseTier2MinAmt(TaxJurisdictionBackingBean.toBigDecimal(stateUseTier2MinAmtInputText.getValue()));
	  	    editTaxRate.setStateUseTier2Rate(TaxJurisdictionBackingBean.toBigDecimal(stateUseTier2RateInputText.getValue()));
	  	    editTaxRate.setStateUseTier2Setamt(TaxJurisdictionBackingBean.toBigDecimal(stateUseTier2SetamtInputText.getValue()));
	  	    if (stateUseTier2MaxAmtInputText.getValue()!=null && "*MAX".equalsIgnoreCase(stateUseTier2MaxAmtInputText.getValue().toString())) {
		  		editTaxRate.setStateUseTier2MaxAmt(BigDecimal.valueOf(JurisdictionTaxrate.MAX_AMOUNT));
		  	} 
		  	else {
		  		editTaxRate.setStateUseTier2MaxAmt(TaxJurisdictionBackingBean.toBigDecimal(stateUseTier2MaxAmtInputText.getValue()));
		  	}
	  	    //stateUseTier2SetToMaximumCheck = false;
	  	    editTaxRate.setStateUseTier2EntamFlag((stateUseTier2TaxEntireAmountCheck)? "1" : "0");
	  	  
	  	    editTaxRate.setStateUseTier3Rate(TaxJurisdictionBackingBean.toBigDecimal(stateUseTier3RateInputText.getValue()));
	  	    editTaxRate.setStateUseTier3Setamt(TaxJurisdictionBackingBean.toBigDecimal(stateUseTier3SetamtInputText.getValue()));
	  	    editTaxRate.setStateUseTier3EntamFlag((stateUseTier3TaxEntireAmountCheck)? "1" : "0");
	  	    
	  	    editTaxRate.setStateUseMaxtaxAmt(TaxJurisdictionBackingBean.toBigDecimal(stateUseMaxtaxAmtInputText.getValue()));
  	    }
  	    //end of state
  	    
  	    //start of county
    	//County sales
    	editTaxRate.setCountySalesTier1Rate(TaxJurisdictionBackingBean.toBigDecimal(countySalesTier1RateInputText.getValue()));
    	editTaxRate.setCountySalesTier1Setamt(TaxJurisdictionBackingBean.toBigDecimal(countySalesTier1SetamtInputText.getValue()));
    	
    	if(countySalesTier1MaxAmtInputText.getValue()!=null && "*MAX".equalsIgnoreCase(countySalesTier1MaxAmtInputText.getValue().toString())){
    		editTaxRate.setCountySalesTier1MaxAmt(TaxJurisdictionBackingBean.toBigDecimal(JurisdictionTaxrate.MAX_AMOUNT));
    	}
    	else{
    		editTaxRate.setCountySalesTier1MaxAmt(TaxJurisdictionBackingBean.toBigDecimal(countySalesTier1MaxAmtInputText.getValue()));
    	}
  	  	//countySalesTier1SetToMaximumCheck = false;
    	
    	editTaxRate.setCountySalesTier2MinAmt(TaxJurisdictionBackingBean.toBigDecimal(countySalesTier2MinAmtInputText.getValue()));
    	editTaxRate.setCountySalesTier2Rate(TaxJurisdictionBackingBean.toBigDecimal(countySalesTier2RateInputText.getValue()));
    	editTaxRate.setCountySalesTier2Setamt(TaxJurisdictionBackingBean.toBigDecimal(countySalesTier2SetamtInputText.getValue()));
    	if (countySalesTier2MaxAmtInputText.getValue()!=null && "*MAX".equalsIgnoreCase(countySalesTier2MaxAmtInputText.getValue().toString())) {
	  		editTaxRate.setCountySalesTier2MaxAmt(BigDecimal.valueOf(JurisdictionTaxrate.MAX_AMOUNT));
	  	} 
	  	else {
	  		editTaxRate.setCountySalesTier2MaxAmt(TaxJurisdictionBackingBean.toBigDecimal(countySalesTier2MaxAmtInputText.getValue()));
	  	}
  	  	//countySalesTier2SetToMaximumCheck = false;
    	editTaxRate.setCountySalesTier2EntamFlag((countySalesTier2TaxEntireAmountCheck)? "1" : "0");

    	editTaxRate.setCountySalesTier3Rate(TaxJurisdictionBackingBean.toBigDecimal(countySalesTier3RateInputText.getValue()));
    	editTaxRate.setCountySalesTier3Setamt(TaxJurisdictionBackingBean.toBigDecimal(countySalesTier3SetamtInputText.getValue()));
    	editTaxRate.setCountySalesTier3EntamFlag((countySalesTier3TaxEntireAmountCheck)? "1" : "0");
    	
    	editTaxRate.setCountySalesMaxtaxAmt(TaxJurisdictionBackingBean.toBigDecimal(countySalesMaxtaxAmtInputText.getValue()));
  	  
  	  	//County use
    	editTaxRate.setCountyUseSameSales((countySameAsSalesRatesCheck)? "1" : "0");
    	if(countySameAsSalesRatesCheck){
  	    	//Copy sales rates to use rates
    		editTaxRate.setCountyUseTier1Rate(editTaxRate.getCountySalesTier1Rate());
	  	    editTaxRate.setCountyUseTier1Setamt(editTaxRate.getCountySalesTier1Setamt());
	  	    editTaxRate.setCountyUseTier1MaxAmt(editTaxRate.getCountySalesTier1MaxAmt());
	
	  	    //countyUseTier1SetToMaximumCheck = false;
	  	 
	  	    editTaxRate.setCountyUseTier2MinAmt(editTaxRate.getCountySalesTier2MinAmt());
	  	    editTaxRate.setCountyUseTier2Rate(editTaxRate.getCountySalesTier2Rate());
	  	    editTaxRate.setCountyUseTier2Setamt(editTaxRate.getCountySalesTier2Setamt());
	  	    editTaxRate.setCountyUseTier2MaxAmt(editTaxRate.getCountySalesTier2MaxAmt());
	  	    //countyUseTier2SetToMaximumCheck = false;
	  	    editTaxRate.setCountyUseTier2EntamFlag(editTaxRate.getCountySalesTier2EntamFlag());
	  	  
	  	    editTaxRate.setCountyUseTier3Rate(editTaxRate.getCountySalesTier3Rate());
	  	    editTaxRate.setCountyUseTier3Setamt(editTaxRate.getCountySalesTier3Setamt());
	  	    editTaxRate.setCountyUseTier3EntamFlag(editTaxRate.getCountySalesTier3EntamFlag());	
    	}
    	else{ 
	  	    editTaxRate.setCountyUseSameSales((countySameAsSalesRatesCheck)? "1" : "0");
	  	  
	  	    editTaxRate.setCountyUseTier1Rate(TaxJurisdictionBackingBean.toBigDecimal(countyUseTier1RateInputText.getValue()));
	  	    editTaxRate.setCountyUseTier1Setamt(TaxJurisdictionBackingBean.toBigDecimal(countyUseTier1SetamtInputText.getValue()));
	  	    
	  	    if(countyUseTier1MaxAmtInputText.getValue()!=null && "*MAX".equalsIgnoreCase(countyUseTier1MaxAmtInputText.getValue().toString())){
	  	    	editTaxRate.setCountyUseTier1MaxAmt(TaxJurisdictionBackingBean.toBigDecimal(JurisdictionTaxrate.MAX_AMOUNT));
	  	    }
	  	    else{
	  	    	editTaxRate.setCountyUseTier1MaxAmt(TaxJurisdictionBackingBean.toBigDecimal(countyUseTier1MaxAmtInputText.getValue()));
	  	    }
	  	    //countyUseTier1SetToMaximumCheck = false;
	  	 
	  	    editTaxRate.setCountyUseTier2MinAmt(TaxJurisdictionBackingBean.toBigDecimal(countyUseTier2MinAmtInputText.getValue()));
	  	    editTaxRate.setCountyUseTier2Rate(TaxJurisdictionBackingBean.toBigDecimal(countyUseTier2RateInputText.getValue()));
	  	    editTaxRate.setCountyUseTier2Setamt(TaxJurisdictionBackingBean.toBigDecimal(countyUseTier2SetamtInputText.getValue()));
	  	    if (countyUseTier2MaxAmtInputText.getValue()!=null && "*MAX".equalsIgnoreCase(countyUseTier2MaxAmtInputText.getValue().toString())) {
		  		editTaxRate.setCountyUseTier2MaxAmt(BigDecimal.valueOf(JurisdictionTaxrate.MAX_AMOUNT));
		  	} 
		  	else {
		  		editTaxRate.setCountyUseTier2MaxAmt(TaxJurisdictionBackingBean.toBigDecimal(countyUseTier2MaxAmtInputText.getValue()));
		  	}
	  	    //countyUseTier2SetToMaximumCheck = false;
	  	    editTaxRate.setCountyUseTier2EntamFlag((countyUseTier2TaxEntireAmountCheck)? "1" : "0");
	  	  
	  	    editTaxRate.setCountyUseTier3Rate(TaxJurisdictionBackingBean.toBigDecimal(countyUseTier3RateInputText.getValue()));
	  	    editTaxRate.setCountyUseTier3Setamt(TaxJurisdictionBackingBean.toBigDecimal(countyUseTier3SetamtInputText.getValue()));
	  	    editTaxRate.setCountyUseTier3EntamFlag((countyUseTier3TaxEntireAmountCheck)? "1" : "0");
	  	    
	  	    editTaxRate.setCountyUseMaxtaxAmt(TaxJurisdictionBackingBean.toBigDecimal(countyUseMaxtaxAmtInputText.getValue()));
    	}   
  	    //end of County
  	    
  	    //start of city
    	//City sales
    	editTaxRate.setCitySalesTier1Rate(TaxJurisdictionBackingBean.toBigDecimal(citySalesTier1RateInputText.getValue()));
    	editTaxRate.setCitySalesTier1Setamt(TaxJurisdictionBackingBean.toBigDecimal(citySalesTier1SetamtInputText.getValue()));
    	
    	if(citySalesTier1MaxAmtInputText.getValue()!=null && "*MAX".equalsIgnoreCase(citySalesTier1MaxAmtInputText.getValue().toString())){
    		editTaxRate.setCitySalesTier1MaxAmt(TaxJurisdictionBackingBean.toBigDecimal(JurisdictionTaxrate.MAX_AMOUNT));
    	}
    	else{
    		editTaxRate.setCitySalesTier1MaxAmt(TaxJurisdictionBackingBean.toBigDecimal(citySalesTier1MaxAmtInputText.getValue()));
    	}
  	  	//citySalesTier1SetToMaximumCheck = false;
    	
    	editTaxRate.setCitySalesTier2MinAmt(TaxJurisdictionBackingBean.toBigDecimal(citySalesTier2MinAmtInputText.getValue()));
    	editTaxRate.setCitySalesTier2Rate(TaxJurisdictionBackingBean.toBigDecimal(citySalesTier2RateInputText.getValue()));
    	editTaxRate.setCitySalesTier2Setamt(TaxJurisdictionBackingBean.toBigDecimal(citySalesTier2SetamtInputText.getValue()));
    	if (citySalesTier2MaxAmtInputText.getValue()!=null && "*MAX".equalsIgnoreCase(citySalesTier2MaxAmtInputText.getValue().toString())) {
	  		editTaxRate.setCitySalesTier2MaxAmt(BigDecimal.valueOf(JurisdictionTaxrate.MAX_AMOUNT));
	  	} 
	  	else {
	  		editTaxRate.setCitySalesTier2MaxAmt(TaxJurisdictionBackingBean.toBigDecimal(citySalesTier2MaxAmtInputText.getValue()));
	  	}
  	  	//citySalesTier2SetToMaximumCheck = false;
    	editTaxRate.setCitySalesTier2EntamFlag((citySalesTier2TaxEntireAmountCheck)? "1" : "0");

    	editTaxRate.setCitySalesTier3Rate(TaxJurisdictionBackingBean.toBigDecimal(citySalesTier3RateInputText.getValue()));
    	editTaxRate.setCitySalesTier3Setamt(TaxJurisdictionBackingBean.toBigDecimal(citySalesTier3SetamtInputText.getValue()));
    	editTaxRate.setCitySalesTier3EntamFlag((citySalesTier3TaxEntireAmountCheck)? "1" : "0");
    	
    	editTaxRate.setCitySalesMaxtaxAmt(TaxJurisdictionBackingBean.toBigDecimal(citySalesMaxtaxAmtInputText.getValue()));
  	  
  	  	//City use
  	    editTaxRate.setCityUseSameSales((citySameAsSalesRatesCheck)? "1" : "0");
	  	if(citySameAsSalesRatesCheck){
		    //Copy sales rates to use rates
	  		editTaxRate.setCityUseTier1Rate(editTaxRate.getCitySalesTier1Rate());
		  	editTaxRate.setCityUseTier1Setamt(editTaxRate.getCitySalesTier1Setamt());
		  	editTaxRate.setCityUseTier1MaxAmt(editTaxRate.getCitySalesTier1MaxAmt());
		
	  	    //cityUseTier1SetToMaximumCheck = false;
	  	 
	  	    editTaxRate.setCityUseTier2MinAmt(editTaxRate.getCitySalesTier2MinAmt());
	  	    editTaxRate.setCityUseTier2Rate(editTaxRate.getCitySalesTier2Rate());
	  	    editTaxRate.setCityUseTier2Setamt(editTaxRate.getCitySalesTier2Setamt());
	  	    editTaxRate.setCityUseTier2MaxAmt(editTaxRate.getCitySalesTier2MaxAmt());
	  	    //cityUseTier2SetToMaximumCheck = false;
	  	    editTaxRate.setCityUseTier2EntamFlag(editTaxRate.getCitySalesTier2EntamFlag());
	  	  
	  	    editTaxRate.setCityUseTier3Rate(editTaxRate.getCitySalesTier3Rate());
	  	    editTaxRate.setCityUseTier3Setamt(editTaxRate.getCitySalesTier3Setamt());
	  	    editTaxRate.setCityUseTier3EntamFlag(editTaxRate.getCitySalesTier3EntamFlag());	
	  	}
	  	else{  	  
	  	    editTaxRate.setCityUseTier1Rate(TaxJurisdictionBackingBean.toBigDecimal(cityUseTier1RateInputText.getValue()));
	  	    editTaxRate.setCityUseTier1Setamt(TaxJurisdictionBackingBean.toBigDecimal(cityUseTier1SetamtInputText.getValue()));
	  	    
	  	    if(cityUseTier1MaxAmtInputText.getValue()!=null && "*MAX".equalsIgnoreCase(cityUseTier1MaxAmtInputText.getValue().toString())){
	  	    	editTaxRate.setCityUseTier1MaxAmt(TaxJurisdictionBackingBean.toBigDecimal(JurisdictionTaxrate.MAX_AMOUNT));
	  	    }
	  	    else{
	  	    	editTaxRate.setCityUseTier1MaxAmt(TaxJurisdictionBackingBean.toBigDecimal(cityUseTier1MaxAmtInputText.getValue()));
	  	    }
	  	    //cityUseTier1SetToMaximumCheck = false;
	  	 
	  	    editTaxRate.setCityUseTier2MinAmt(TaxJurisdictionBackingBean.toBigDecimal(cityUseTier2MinAmtInputText.getValue()));
	  	    editTaxRate.setCityUseTier2Rate(TaxJurisdictionBackingBean.toBigDecimal(cityUseTier2RateInputText.getValue()));
	  	    editTaxRate.setCityUseTier2Setamt(TaxJurisdictionBackingBean.toBigDecimal(cityUseTier2SetamtInputText.getValue()));
	  	    if (cityUseTier2MaxAmtInputText.getValue()!=null && "*MAX".equalsIgnoreCase(cityUseTier2MaxAmtInputText.getValue().toString())) {
		  		editTaxRate.setCityUseTier2MaxAmt(BigDecimal.valueOf(JurisdictionTaxrate.MAX_AMOUNT));
		  	} 
		  	else {
		  		editTaxRate.setCityUseTier2MaxAmt(TaxJurisdictionBackingBean.toBigDecimal(cityUseTier2MaxAmtInputText.getValue()));
		  	}
	  	    //cityUseTier2SetToMaximumCheck = false;
	  	    editTaxRate.setCityUseTier2EntamFlag((cityUseTier2TaxEntireAmountCheck)? "1" : "0");
	  	  
	  	    editTaxRate.setCityUseTier3Rate(TaxJurisdictionBackingBean.toBigDecimal(cityUseTier3RateInputText.getValue()));
	  	    editTaxRate.setCityUseTier3Setamt(TaxJurisdictionBackingBean.toBigDecimal(cityUseTier3SetamtInputText.getValue()));
	  	    editTaxRate.setCityUseTier3EntamFlag((cityUseTier3TaxEntireAmountCheck)? "1" : "0");
	  	    
	  	    editTaxRate.setCityUseMaxtaxAmt(TaxJurisdictionBackingBean.toBigDecimal(cityUseMaxtaxAmtInputText.getValue()));
	  	}    
  	    //end of city
  	    
  	    //start of stj
  	    editTaxRate.setStj1SalesRate(TaxJurisdictionBackingBean.toBigDecimal(stj1SalesRateInputText.getValue()));
	    editTaxRate.setStj2SalesRate(TaxJurisdictionBackingBean.toBigDecimal(stj2SalesRateInputText.getValue()));
	    editTaxRate.setStj3SalesRate(TaxJurisdictionBackingBean.toBigDecimal(stj3SalesRateInputText.getValue()));
	    editTaxRate.setStj4SalesRate(TaxJurisdictionBackingBean.toBigDecimal(stj4SalesRateInputText.getValue()));
	    editTaxRate.setStj5SalesRate(TaxJurisdictionBackingBean.toBigDecimal(stj5SalesRateInputText.getValue()));

	    editTaxRate.setStj1SalesSetamt(TaxJurisdictionBackingBean.toBigDecimal(stj1SalesSetamtInputText.getValue()));
	    editTaxRate.setStj2SalesSetamt(TaxJurisdictionBackingBean.toBigDecimal(stj2SalesSetamtInputText.getValue()));
	    editTaxRate.setStj3SalesSetamt(TaxJurisdictionBackingBean.toBigDecimal(stj3SalesSetamtInputText.getValue()));
	    editTaxRate.setStj4SalesSetamt(TaxJurisdictionBackingBean.toBigDecimal(stj4SalesSetamtInputText.getValue()));
	    editTaxRate.setStj5SalesSetamt(TaxJurisdictionBackingBean.toBigDecimal(stj5SalesSetamtInputText.getValue()));
  	      
	    //STJ use
  	    editTaxRate.setStjUseSameSales((stjSameAsSalesRatesCheck)? "1" : "0");
  	    if(stjSameAsSalesRatesCheck){
  	    	//Copy sales rates to use rates
  	    	editTaxRate.setStj1UseRate(editTaxRate.getStj1SalesRate());
	  	    editTaxRate.setStj2UseRate(editTaxRate.getStj2SalesRate());
	  	    editTaxRate.setStj3UseRate(editTaxRate.getStj3SalesRate());
	  	    editTaxRate.setStj4UseRate(editTaxRate.getStj4SalesRate());
	  	    editTaxRate.setStj5UseRate(editTaxRate.getStj5SalesRate());
	
		    editTaxRate.setStj1UseSetamt(editTaxRate.getStj1SalesSetamt());
		    editTaxRate.setStj2UseSetamt(editTaxRate.getStj2SalesSetamt());
		    editTaxRate.setStj3UseSetamt(editTaxRate.getStj3SalesSetamt());
		    editTaxRate.setStj4UseSetamt(editTaxRate.getStj4SalesSetamt());
		    editTaxRate.setStj5UseSetamt(editTaxRate.getStj5SalesSetamt());
  	    }
  	    else{   
	  	    editTaxRate.setStj1UseRate(TaxJurisdictionBackingBean.toBigDecimal(stj1UseRateInputText.getValue()));
	  	    editTaxRate.setStj2UseRate(TaxJurisdictionBackingBean.toBigDecimal(stj2UseRateInputText.getValue()));
	  	    editTaxRate.setStj3UseRate(TaxJurisdictionBackingBean.toBigDecimal(stj3UseRateInputText.getValue()));
	  	    editTaxRate.setStj4UseRate(TaxJurisdictionBackingBean.toBigDecimal(stj4UseRateInputText.getValue()));
	  	    editTaxRate.setStj5UseRate(TaxJurisdictionBackingBean.toBigDecimal(stj5UseRateInputText.getValue()));
	
		    editTaxRate.setStj1UseSetamt(TaxJurisdictionBackingBean.toBigDecimal(stj1UseSetamtInputText.getValue()));
		    editTaxRate.setStj2UseSetamt(TaxJurisdictionBackingBean.toBigDecimal(stj2UseSetamtInputText.getValue()));
		    editTaxRate.setStj3UseSetamt(TaxJurisdictionBackingBean.toBigDecimal(stj3UseSetamtInputText.getValue()));
		    editTaxRate.setStj4UseSetamt(TaxJurisdictionBackingBean.toBigDecimal(stj4UseSetamtInputText.getValue()));
		    editTaxRate.setStj5UseSetamt(TaxJurisdictionBackingBean.toBigDecimal(stj5UseSetamtInputText.getValue()));
  	    }    
  	    //end of stj
    }
    
    public boolean isRateSetamtMutuallyExclusive(Object rateString, Object setamtString){
  		BigDecimal rateDbl = TaxJurisdictionBackingBean.toBigDecimal(rateString);
  		BigDecimal setamtDbl = TaxJurisdictionBackingBean.toBigDecimal(setamtString);

  		if(rateDbl.doubleValue()>0.0 && setamtDbl.doubleValue()>0.0){
  			return false;
  		}
  		
  		return true;
  	}
    
    public boolean processSaveTaxRate(){
    	boolean allowAdd = true;
    	
    	if(!processValidateRateType()){
    		allowAdd = false;
		}
    	
    	if(!processValidateDates()){
    		allowAdd = false;
		}
    	
    	if(!allowAdd){
	  		return false;
	  	}
    	
	  	//start of country
    	if(((editTaxRate.getCountrySalesTier2Rate().compareTo(ArithmeticUtils.ZERO) > 0 || editTaxRate.getCountrySalesTier2Setamt().compareTo(ArithmeticUtils.ZERO) > 0) &&
	  		(editTaxRate.getCountrySalesTier1Rate().compareTo(ArithmeticUtils.ZERO) == 0 && editTaxRate.getCountrySalesTier1Setamt().compareTo(ArithmeticUtils.ZERO) == 0))  ||
	  	   ((editTaxRate.getCountryUseTier2Rate().compareTo(ArithmeticUtils.ZERO) > 0 || editTaxRate.getCountryUseTier2Setamt().compareTo(ArithmeticUtils.ZERO) > 0) &&
	  		(editTaxRate.getCountryUseTier1Rate().compareTo(ArithmeticUtils.ZERO) == 0 && editTaxRate.getCountryUseTier1Setamt().compareTo(ArithmeticUtils.ZERO) == 0))){
		  	FacesMessage msg = new FacesMessage("Country Tier 2 Rate/Set Amount may not be entered unless a Tier 1 Rate/Set Amount is entered.");
		  	msg.setSeverity(FacesMessage.SEVERITY_ERROR);
		  	FacesContext.getCurrentInstance().addMessage(null, msg);
		  	allowAdd = false;
		}
	
	  	if(((editTaxRate.getCountrySalesTier3Rate().compareTo(ArithmeticUtils.ZERO) > 0 || editTaxRate.getCountrySalesTier3Setamt().compareTo(ArithmeticUtils.ZERO) > 0) &&
	  		(editTaxRate.getCountrySalesTier2Rate().compareTo(ArithmeticUtils.ZERO) == 0 && editTaxRate.getCountrySalesTier2Setamt().compareTo(ArithmeticUtils.ZERO) == 0))  ||
	  	   ((editTaxRate.getCountryUseTier3Rate().compareTo(ArithmeticUtils.ZERO) > 0 || editTaxRate.getCountryUseTier3Setamt().compareTo(ArithmeticUtils.ZERO) > 0) &&
	  		(editTaxRate.getCountryUseTier2Rate().compareTo(ArithmeticUtils.ZERO) == 0 && editTaxRate.getCountryUseTier2Setamt().compareTo(ArithmeticUtils.ZERO) == 0))){
		  	FacesMessage msg = new FacesMessage("Country Tier 3 Rate/Set Amount may not be entered unless a Tier 2 Rate/Set Amount is entered.");
		  	msg.setSeverity(FacesMessage.SEVERITY_ERROR);
		  	FacesContext.getCurrentInstance().addMessage(null, msg);
		  	allowAdd = false;
		}
	    
	  	if((editTaxRate.getCountrySalesTier2MaxAmt().compareTo(editTaxRate.getCountrySalesTier2MinAmt()) <= 0 && editTaxRate.getCountrySalesTier2MinAmt().compareTo(ArithmeticUtils.ZERO) > 0) ||
	  			(editTaxRate.getCountryUseTier2MaxAmt().compareTo(editTaxRate.getCountryUseTier2MinAmt()) <= 0 && editTaxRate.getCountryUseTier2MinAmt().compareTo(ArithmeticUtils.ZERO) > 0)){
	  		allowAdd = false;
	  		FacesMessage msg = new FacesMessage("Country Tier 2 Through Amount Must Be Greater Than From Amount.");
	  		msg.setSeverity(FacesMessage.SEVERITY_ERROR);
	  		FacesContext.getCurrentInstance().addMessage(null, msg);
	  		allowAdd = false;
	  	}
	  	
	  	if(((editTaxRate.getCountrySalesTier2Rate().compareTo(ArithmeticUtils.ZERO) > 0 || editTaxRate.getCountrySalesTier2Setamt().compareTo(ArithmeticUtils.ZERO) > 0) &&
	  			(editTaxRate.getCountrySalesTier1MaxAmt().compareTo(ArithmeticUtils.ZERO) == 0)) ||
	  		((editTaxRate.getCountryUseTier2Rate().compareTo(ArithmeticUtils.ZERO) > 0 || editTaxRate.getCountryUseTier2Setamt().compareTo(ArithmeticUtils.ZERO) > 0) &&
	  		  	(editTaxRate.getCountryUseTier1MaxAmt().compareTo(ArithmeticUtils.ZERO) == 0))){
		  	FacesMessage msg = new FacesMessage("Country Tier 2 Rate/Set Amount may not be entered unless a Tier 1 Through Amount is entered.");
		  	msg.setSeverity(FacesMessage.SEVERITY_ERROR);
		  	FacesContext.getCurrentInstance().addMessage(null, msg);
		  	allowAdd = false;
		}
	  	
	  	if(((editTaxRate.getCountrySalesTier3Rate().compareTo(ArithmeticUtils.ZERO) > 0 || editTaxRate.getCountrySalesTier3Setamt().compareTo(ArithmeticUtils.ZERO) > 0) &&
	  			(editTaxRate.getCountrySalesTier2MaxAmt().compareTo(ArithmeticUtils.ZERO) == 0)) ||
	  		((editTaxRate.getCountryUseTier3Rate().compareTo(ArithmeticUtils.ZERO) > 0 || editTaxRate.getCountryUseTier3Setamt().compareTo(ArithmeticUtils.ZERO) > 0) &&
	  		  	(editTaxRate.getCountryUseTier2MaxAmt().compareTo(ArithmeticUtils.ZERO) == 0))){
		  	FacesMessage msg = new FacesMessage("Country Tier 3 Rate/Set Amount may not be entered unless a Tier 2 Through Amount is entered.");
		  	msg.setSeverity(FacesMessage.SEVERITY_ERROR);
		  	FacesContext.getCurrentInstance().addMessage(null, msg);
		  	allowAdd = false;
		}

	  	if((editTaxRate.getCountrySalesTier1Rate().compareTo(ArithmeticUtils.ZERO)>0 && editTaxRate.getCountrySalesTier1Setamt().compareTo(ArithmeticUtils.ZERO)>0) || 
	  			(editTaxRate.getCountrySalesTier2Rate().compareTo(ArithmeticUtils.ZERO)>0 && editTaxRate.getCountrySalesTier2Setamt().compareTo(ArithmeticUtils.ZERO)>0) ||
	  			(editTaxRate.getCountrySalesTier3Rate().compareTo(ArithmeticUtils.ZERO)>0 && editTaxRate.getCountrySalesTier3Setamt().compareTo(ArithmeticUtils.ZERO)>0) ||
	  			(editTaxRate.getCountryUseTier1Rate().compareTo(ArithmeticUtils.ZERO)>0 && editTaxRate.getCountryUseTier1Setamt().compareTo(ArithmeticUtils.ZERO)>0) || 
	  			(editTaxRate.getCountryUseTier2Rate().compareTo(ArithmeticUtils.ZERO)>0 && editTaxRate.getCountryUseTier2Setamt().compareTo(ArithmeticUtils.ZERO)>0) ||
	  			(editTaxRate.getCountryUseTier3Rate().compareTo(ArithmeticUtils.ZERO)>0 && editTaxRate.getCountryUseTier3Setamt().compareTo(ArithmeticUtils.ZERO)>0)){
	  		FacesMessage msg = new FacesMessage("Country Tier Rate and Set Amount are mutually exclusive.");
	  		msg.setSeverity(FacesMessage.SEVERITY_ERROR);
	  		FacesContext.getCurrentInstance().addMessage(null, msg);
	  		allowAdd = false;
	  	}
	  	
	  	if(((	editTaxRate.getCountrySalesTier2Rate().compareTo(ArithmeticUtils.ZERO)>0 || editTaxRate.getCountrySalesTier2Setamt().compareTo(ArithmeticUtils.ZERO)>0 || 
	  			editTaxRate.getCountrySalesTier3Rate().compareTo(ArithmeticUtils.ZERO)>0 || editTaxRate.getCountrySalesTier3Setamt().compareTo(ArithmeticUtils.ZERO)>0    ) && 
	  			editTaxRate.getCountrySalesMaxtaxAmt().compareTo(ArithmeticUtils.ZERO)>0 )       ||
  			((	editTaxRate.getCountryUseTier2Rate().compareTo(ArithmeticUtils.ZERO)>0 || editTaxRate.getCountryUseTier2Setamt().compareTo(ArithmeticUtils.ZERO)>0 || 
  		  			editTaxRate.getCountryUseTier3Rate().compareTo(ArithmeticUtils.ZERO)>0 || editTaxRate.getCountryUseTier3Setamt().compareTo(ArithmeticUtils.ZERO)>0    ) && 
  		  			editTaxRate.getCountryUseMaxtaxAmt().compareTo(ArithmeticUtils.ZERO)>0 )){
	  		FacesMessage msg = new FacesMessage("Country MaxTax Amount may not be entered when Tier 2 or Tier 3 Rate or Set Amount are entered.");
	  		msg.setSeverity(FacesMessage.SEVERITY_ERROR);
	  		FacesContext.getCurrentInstance().addMessage(null, msg);
	  		allowAdd = false;
	  	}
	  	
	  	//end of country
	  	
	  	//start of state
	  	if(((editTaxRate.getStateSalesTier2Rate().compareTo(ArithmeticUtils.ZERO) > 0 || editTaxRate.getStateSalesTier2Setamt().compareTo(ArithmeticUtils.ZERO) > 0) &&
	  		(editTaxRate.getStateSalesTier1Rate().compareTo(ArithmeticUtils.ZERO) == 0 && editTaxRate.getStateSalesTier1Setamt().compareTo(ArithmeticUtils.ZERO) == 0))  ||
	  	   ((editTaxRate.getStateUseTier2Rate().compareTo(ArithmeticUtils.ZERO) > 0 || editTaxRate.getStateUseTier2Setamt().compareTo(ArithmeticUtils.ZERO) > 0) &&
	  		(editTaxRate.getStateUseTier1Rate().compareTo(ArithmeticUtils.ZERO) == 0 && editTaxRate.getStateUseTier1Setamt().compareTo(ArithmeticUtils.ZERO) == 0))){
		  	FacesMessage msg = new FacesMessage("State Tier 2 Rate/Set Amount may not be entered unless a Tier 1 Rate/Set Amount is entered.");
		  	msg.setSeverity(FacesMessage.SEVERITY_ERROR);
		  	FacesContext.getCurrentInstance().addMessage(null, msg);
		  	allowAdd = false;
		}
	
	  	if(((editTaxRate.getStateSalesTier3Rate().compareTo(ArithmeticUtils.ZERO) > 0 || editTaxRate.getStateSalesTier3Setamt().compareTo(ArithmeticUtils.ZERO) > 0) &&
	  		(editTaxRate.getStateSalesTier2Rate().compareTo(ArithmeticUtils.ZERO) == 0 && editTaxRate.getStateSalesTier2Setamt().compareTo(ArithmeticUtils.ZERO) == 0))  ||
	  	   ((editTaxRate.getStateUseTier3Rate().compareTo(ArithmeticUtils.ZERO) > 0 || editTaxRate.getStateUseTier3Setamt().compareTo(ArithmeticUtils.ZERO) > 0) &&
	  		(editTaxRate.getStateUseTier2Rate().compareTo(ArithmeticUtils.ZERO) == 0 && editTaxRate.getStateUseTier2Setamt().compareTo(ArithmeticUtils.ZERO) == 0))){
		  	FacesMessage msg = new FacesMessage("State Tier 3 Rate/Set Amount may not be entered unless a Tier 2 Rate/Set Amount is entered.");
		  	msg.setSeverity(FacesMessage.SEVERITY_ERROR);
		  	FacesContext.getCurrentInstance().addMessage(null, msg);
		  	allowAdd = false;
		}
	    
	  	if((editTaxRate.getStateSalesTier2MaxAmt().compareTo(editTaxRate.getStateSalesTier2MinAmt()) <= 0 && editTaxRate.getStateSalesTier2MinAmt().compareTo(ArithmeticUtils.ZERO) > 0) ||
	  			(editTaxRate.getStateUseTier2MaxAmt().compareTo(editTaxRate.getStateUseTier2MinAmt()) <= 0 && editTaxRate.getStateUseTier2MinAmt().compareTo(ArithmeticUtils.ZERO) > 0)){
	  		allowAdd = false;
	  		FacesMessage msg = new FacesMessage("State Tier 2 Through Amount Must Be Greater Than From Amount.");
	  		msg.setSeverity(FacesMessage.SEVERITY_ERROR);
	  		FacesContext.getCurrentInstance().addMessage(null, msg);
	  		allowAdd = false;
	  	}
	  	
	  	if(((editTaxRate.getStateSalesTier2Rate().compareTo(ArithmeticUtils.ZERO) > 0 || editTaxRate.getStateSalesTier2Setamt().compareTo(ArithmeticUtils.ZERO) > 0) &&
	  			(editTaxRate.getStateSalesTier1MaxAmt().compareTo(ArithmeticUtils.ZERO) == 0)) ||
	  		((editTaxRate.getStateUseTier2Rate().compareTo(ArithmeticUtils.ZERO) > 0 || editTaxRate.getStateUseTier2Setamt().compareTo(ArithmeticUtils.ZERO) > 0) &&
	  		  	(editTaxRate.getStateUseTier1MaxAmt().compareTo(ArithmeticUtils.ZERO) == 0))){
		  	FacesMessage msg = new FacesMessage("State Tier 2 Rate/Set Amount may not be entered unless a Tier 1 Through Amount is entered.");
		  	msg.setSeverity(FacesMessage.SEVERITY_ERROR);
		  	FacesContext.getCurrentInstance().addMessage(null, msg);
		  	allowAdd = false;
		}
	  	
	  	if(((editTaxRate.getStateSalesTier3Rate().compareTo(ArithmeticUtils.ZERO) > 0 || editTaxRate.getStateSalesTier3Setamt().compareTo(ArithmeticUtils.ZERO) > 0) &&
	  			(editTaxRate.getStateSalesTier2MaxAmt().compareTo(ArithmeticUtils.ZERO) == 0)) ||
	  		((editTaxRate.getStateUseTier3Rate().compareTo(ArithmeticUtils.ZERO) > 0 || editTaxRate.getStateUseTier3Setamt().compareTo(ArithmeticUtils.ZERO) > 0) &&
	  		  	(editTaxRate.getStateUseTier2MaxAmt().compareTo(ArithmeticUtils.ZERO) == 0))){
		  	FacesMessage msg = new FacesMessage("State Tier 3 Rate/Set Amount may not be entered unless a Tier 2 Through Amount is entered.");
		  	msg.setSeverity(FacesMessage.SEVERITY_ERROR);
		  	FacesContext.getCurrentInstance().addMessage(null, msg);
		  	allowAdd = false;
		}

	  	if((editTaxRate.getStateSalesTier1Rate().compareTo(ArithmeticUtils.ZERO)>0 && editTaxRate.getStateSalesTier1Setamt().compareTo(ArithmeticUtils.ZERO)>0) || 
	  			(editTaxRate.getStateSalesTier2Rate().compareTo(ArithmeticUtils.ZERO)>0 && editTaxRate.getStateSalesTier2Setamt().compareTo(ArithmeticUtils.ZERO)>0) ||
	  			(editTaxRate.getStateSalesTier3Rate().compareTo(ArithmeticUtils.ZERO)>0 && editTaxRate.getStateSalesTier3Setamt().compareTo(ArithmeticUtils.ZERO)>0) ||
	  			(editTaxRate.getStateUseTier1Rate().compareTo(ArithmeticUtils.ZERO)>0 && editTaxRate.getStateUseTier1Setamt().compareTo(ArithmeticUtils.ZERO)>0) || 
	  			(editTaxRate.getStateUseTier2Rate().compareTo(ArithmeticUtils.ZERO)>0 && editTaxRate.getStateUseTier2Setamt().compareTo(ArithmeticUtils.ZERO)>0) ||
	  			(editTaxRate.getStateUseTier3Rate().compareTo(ArithmeticUtils.ZERO)>0 && editTaxRate.getStateUseTier3Setamt().compareTo(ArithmeticUtils.ZERO)>0)){
	  		FacesMessage msg = new FacesMessage("State Tier Rate and Set Amount are mutually exclusive.");
	  		msg.setSeverity(FacesMessage.SEVERITY_ERROR);
	  		FacesContext.getCurrentInstance().addMessage(null, msg);
	  		allowAdd = false;
	  	}
	  	
	  	if(((	editTaxRate.getStateSalesTier2Rate().compareTo(ArithmeticUtils.ZERO)>0 || editTaxRate.getStateSalesTier2Setamt().compareTo(ArithmeticUtils.ZERO)>0 || 
	  			editTaxRate.getStateSalesTier3Rate().compareTo(ArithmeticUtils.ZERO)>0 || editTaxRate.getStateSalesTier3Setamt().compareTo(ArithmeticUtils.ZERO)>0    ) && 
	  			editTaxRate.getStateSalesMaxtaxAmt().compareTo(ArithmeticUtils.ZERO)>0 )       ||
  			((	editTaxRate.getStateUseTier2Rate().compareTo(ArithmeticUtils.ZERO)>0 || editTaxRate.getStateUseTier2Setamt().compareTo(ArithmeticUtils.ZERO)>0 || 
  		  			editTaxRate.getStateUseTier3Rate().compareTo(ArithmeticUtils.ZERO)>0 || editTaxRate.getStateUseTier3Setamt().compareTo(ArithmeticUtils.ZERO)>0    ) && 
  		  			editTaxRate.getStateUseMaxtaxAmt().compareTo(ArithmeticUtils.ZERO)>0 )){
	  		FacesMessage msg = new FacesMessage("State MaxTax Amount may not be entered when Tier 2 or Tier 3 Rate or Set Amount are entered.");
	  		msg.setSeverity(FacesMessage.SEVERITY_ERROR);
	  		FacesContext.getCurrentInstance().addMessage(null, msg);
	  		allowAdd = false;
	  	}
	  	
	  	//end of state
	  	
	  	//start of county
	  	if(((editTaxRate.getCountySalesTier2Rate().compareTo(ArithmeticUtils.ZERO) > 0 || editTaxRate.getCountySalesTier2Setamt().compareTo(ArithmeticUtils.ZERO) > 0) &&
	  		(editTaxRate.getCountySalesTier1Rate().compareTo(ArithmeticUtils.ZERO) == 0 && editTaxRate.getCountySalesTier1Setamt().compareTo(ArithmeticUtils.ZERO) == 0))  ||
	  	   ((editTaxRate.getCountyUseTier2Rate().compareTo(ArithmeticUtils.ZERO) > 0 || editTaxRate.getCountyUseTier2Setamt().compareTo(ArithmeticUtils.ZERO) > 0) &&
	  		(editTaxRate.getCountyUseTier1Rate().compareTo(ArithmeticUtils.ZERO) == 0 && editTaxRate.getCountyUseTier1Setamt().compareTo(ArithmeticUtils.ZERO) == 0))){
		  	FacesMessage msg = new FacesMessage("County Tier 2 Rate/Set Amount may not be entered unless a Tier 1 Rate/Set Amount is entered.");
		  	msg.setSeverity(FacesMessage.SEVERITY_ERROR);
		  	FacesContext.getCurrentInstance().addMessage(null, msg);
		  	allowAdd = false;
		}
	
	  	if(((editTaxRate.getCountySalesTier3Rate().compareTo(ArithmeticUtils.ZERO) > 0 || editTaxRate.getCountySalesTier3Setamt().compareTo(ArithmeticUtils.ZERO) > 0) &&
	  		(editTaxRate.getCountySalesTier2Rate().compareTo(ArithmeticUtils.ZERO) == 0 && editTaxRate.getCountySalesTier2Setamt().compareTo(ArithmeticUtils.ZERO) == 0))  ||
	  	   ((editTaxRate.getCountyUseTier3Rate().compareTo(ArithmeticUtils.ZERO) > 0 || editTaxRate.getCountyUseTier3Setamt().compareTo(ArithmeticUtils.ZERO) > 0) &&
	  		(editTaxRate.getCountyUseTier2Rate().compareTo(ArithmeticUtils.ZERO) == 0 && editTaxRate.getCountyUseTier2Setamt().compareTo(ArithmeticUtils.ZERO) == 0))){
		  	FacesMessage msg = new FacesMessage("County Tier 3 Rate/Set Amount may not be entered unless a Tier 2 Rate/Set Amount is entered.");
		  	msg.setSeverity(FacesMessage.SEVERITY_ERROR);
		  	FacesContext.getCurrentInstance().addMessage(null, msg);
		  	allowAdd = false;
		}
	    
	  	if((editTaxRate.getCountySalesTier2MaxAmt().compareTo(editTaxRate.getCountySalesTier2MinAmt()) <= 0 && editTaxRate.getCountySalesTier2MinAmt().compareTo(ArithmeticUtils.ZERO) > 0) ||
	  			(editTaxRate.getCountyUseTier2MaxAmt().compareTo(editTaxRate.getCountyUseTier2MinAmt()) <= 0 && editTaxRate.getCountyUseTier2MinAmt().compareTo(ArithmeticUtils.ZERO) > 0)){
	  		allowAdd = false;
	  		FacesMessage msg = new FacesMessage("County Tier 2 Through Amount Must Be Greater Than From Amount.");
	  		msg.setSeverity(FacesMessage.SEVERITY_ERROR);
	  		FacesContext.getCurrentInstance().addMessage(null, msg);
	  		allowAdd = false;
	  	}
	  	
	  	if(((editTaxRate.getCountySalesTier2Rate().compareTo(ArithmeticUtils.ZERO) > 0 || editTaxRate.getCountySalesTier2Setamt().compareTo(ArithmeticUtils.ZERO) > 0) &&
	  			(editTaxRate.getCountySalesTier1MaxAmt().compareTo(ArithmeticUtils.ZERO) == 0)) ||
	  		((editTaxRate.getCountyUseTier2Rate().compareTo(ArithmeticUtils.ZERO) > 0 || editTaxRate.getCountyUseTier2Setamt().compareTo(ArithmeticUtils.ZERO) > 0) &&
	  		  	(editTaxRate.getCountyUseTier1MaxAmt().compareTo(ArithmeticUtils.ZERO) == 0))){
		  	FacesMessage msg = new FacesMessage("County Tier 2 Rate/Set Amount may not be entered unless a Tier 1 Through Amount is entered.");
		  	msg.setSeverity(FacesMessage.SEVERITY_ERROR);
		  	FacesContext.getCurrentInstance().addMessage(null, msg);
		  	allowAdd = false;
		}
	  	
	  	if(((editTaxRate.getCountySalesTier3Rate().compareTo(ArithmeticUtils.ZERO) > 0 || editTaxRate.getCountySalesTier3Setamt().compareTo(ArithmeticUtils.ZERO) > 0) &&
	  			(editTaxRate.getCountySalesTier2MaxAmt().compareTo(ArithmeticUtils.ZERO) == 0)) ||
	  		((editTaxRate.getCountyUseTier3Rate().compareTo(ArithmeticUtils.ZERO) > 0 || editTaxRate.getCountyUseTier3Setamt().compareTo(ArithmeticUtils.ZERO) > 0) &&
	  		  	(editTaxRate.getCountyUseTier2MaxAmt().compareTo(ArithmeticUtils.ZERO) == 0))){
		  	FacesMessage msg = new FacesMessage("County Tier 3 Rate/Set Amount may not be entered unless a Tier 2 Through Amount is entered.");
		  	msg.setSeverity(FacesMessage.SEVERITY_ERROR);
		  	FacesContext.getCurrentInstance().addMessage(null, msg);
		  	allowAdd = false;
		}

	  	if((editTaxRate.getCountySalesTier1Rate().compareTo(ArithmeticUtils.ZERO)>0 && editTaxRate.getCountySalesTier1Setamt().compareTo(ArithmeticUtils.ZERO)>0) || 
	  			(editTaxRate.getCountySalesTier2Rate().compareTo(ArithmeticUtils.ZERO)>0 && editTaxRate.getCountySalesTier2Setamt().compareTo(ArithmeticUtils.ZERO)>0) ||
	  			(editTaxRate.getCountySalesTier3Rate().compareTo(ArithmeticUtils.ZERO)>0 && editTaxRate.getCountySalesTier3Setamt().compareTo(ArithmeticUtils.ZERO)>0) ||
	  			(editTaxRate.getCountyUseTier1Rate().compareTo(ArithmeticUtils.ZERO)>0 && editTaxRate.getCountyUseTier1Setamt().compareTo(ArithmeticUtils.ZERO)>0) || 
	  			(editTaxRate.getCountyUseTier2Rate().compareTo(ArithmeticUtils.ZERO)>0 && editTaxRate.getCountyUseTier2Setamt().compareTo(ArithmeticUtils.ZERO)>0) ||
	  			(editTaxRate.getCountyUseTier3Rate().compareTo(ArithmeticUtils.ZERO)>0 && editTaxRate.getCountyUseTier3Setamt().compareTo(ArithmeticUtils.ZERO)>0)){
	  		FacesMessage msg = new FacesMessage("County Tier Rate and Set Amount are mutually exclusive.");
	  		msg.setSeverity(FacesMessage.SEVERITY_ERROR);
	  		FacesContext.getCurrentInstance().addMessage(null, msg);
	  		allowAdd = false;
	  	}
	  	
	  	if(((	editTaxRate.getCountySalesTier2Rate().compareTo(ArithmeticUtils.ZERO)>0 || editTaxRate.getCountySalesTier2Setamt().compareTo(ArithmeticUtils.ZERO)>0 || 
	  			editTaxRate.getCountySalesTier3Rate().compareTo(ArithmeticUtils.ZERO)>0 || editTaxRate.getCountySalesTier3Setamt().compareTo(ArithmeticUtils.ZERO)>0    ) && 
	  			editTaxRate.getCountySalesMaxtaxAmt().compareTo(ArithmeticUtils.ZERO)>0 )       ||
  			((	editTaxRate.getCountyUseTier2Rate().compareTo(ArithmeticUtils.ZERO)>0 || editTaxRate.getCountyUseTier2Setamt().compareTo(ArithmeticUtils.ZERO)>0 || 
  		  			editTaxRate.getCountyUseTier3Rate().compareTo(ArithmeticUtils.ZERO)>0 || editTaxRate.getCountyUseTier3Setamt().compareTo(ArithmeticUtils.ZERO)>0    ) && 
  		  			editTaxRate.getCountyUseMaxtaxAmt().compareTo(ArithmeticUtils.ZERO)>0 )){
	  		FacesMessage msg = new FacesMessage("County MaxTax Amount may not be entered when Tier 2 or Tier 3 Rate or Set Amount are entered.");
	  		msg.setSeverity(FacesMessage.SEVERITY_ERROR);
	  		FacesContext.getCurrentInstance().addMessage(null, msg);
	  		allowAdd = false;
	  	}

	  	//end of county
	  	
	  	//start of city
	  	if(((editTaxRate.getCitySalesTier2Rate().compareTo(ArithmeticUtils.ZERO) > 0 || editTaxRate.getCitySalesTier2Setamt().compareTo(ArithmeticUtils.ZERO) > 0) &&
	  		(editTaxRate.getCitySalesTier1Rate().compareTo(ArithmeticUtils.ZERO) == 0 && editTaxRate.getCitySalesTier1Setamt().compareTo(ArithmeticUtils.ZERO) == 0))  ||
	  	   ((editTaxRate.getCityUseTier2Rate().compareTo(ArithmeticUtils.ZERO) > 0 || editTaxRate.getCityUseTier2Setamt().compareTo(ArithmeticUtils.ZERO) > 0) &&
	  		(editTaxRate.getCityUseTier1Rate().compareTo(ArithmeticUtils.ZERO) == 0 && editTaxRate.getCityUseTier1Setamt().compareTo(ArithmeticUtils.ZERO) == 0))){
		  	FacesMessage msg = new FacesMessage("City Tier 2 Rate/Set Amount may not be entered unless a Tier 1 Rate/Set Amount is entered.");
		  	msg.setSeverity(FacesMessage.SEVERITY_ERROR);
		  	FacesContext.getCurrentInstance().addMessage(null, msg);
		  	allowAdd = false;
		}
	
	  	if(((editTaxRate.getCitySalesTier3Rate().compareTo(ArithmeticUtils.ZERO) > 0 || editTaxRate.getCitySalesTier3Setamt().compareTo(ArithmeticUtils.ZERO) > 0) &&
	  		(editTaxRate.getCitySalesTier2Rate().compareTo(ArithmeticUtils.ZERO) == 0 && editTaxRate.getCitySalesTier2Setamt().compareTo(ArithmeticUtils.ZERO) == 0))  ||
	  	   ((editTaxRate.getCityUseTier3Rate().compareTo(ArithmeticUtils.ZERO) > 0 || editTaxRate.getCityUseTier3Setamt().compareTo(ArithmeticUtils.ZERO) > 0) &&
	  		(editTaxRate.getCityUseTier2Rate().compareTo(ArithmeticUtils.ZERO) == 0 && editTaxRate.getCityUseTier2Setamt().compareTo(ArithmeticUtils.ZERO) == 0))){
		  	FacesMessage msg = new FacesMessage("City Tier 3 Rate/Set Amount may not be entered unless a Tier 2 Rate/Set Amount is entered.");
		  	msg.setSeverity(FacesMessage.SEVERITY_ERROR);
		  	FacesContext.getCurrentInstance().addMessage(null, msg);
		  	allowAdd = false;
		}
	    
	  	if((editTaxRate.getCitySalesTier2MaxAmt().compareTo(editTaxRate.getCitySalesTier2MinAmt()) <= 0 && editTaxRate.getCitySalesTier2MinAmt().compareTo(ArithmeticUtils.ZERO) > 0) ||
	  			(editTaxRate.getCityUseTier2MaxAmt().compareTo(editTaxRate.getCityUseTier2MinAmt()) <= 0 && editTaxRate.getCityUseTier2MinAmt().compareTo(ArithmeticUtils.ZERO) > 0)){
	  		allowAdd = false;
	  		FacesMessage msg = new FacesMessage("City Tier 2 Through Amount Must Be Greater Than From Amount.");
	  		msg.setSeverity(FacesMessage.SEVERITY_ERROR);
	  		FacesContext.getCurrentInstance().addMessage(null, msg);
	  		allowAdd = false;
	  	}
	  	
	  	if(((editTaxRate.getCitySalesTier2Rate().compareTo(ArithmeticUtils.ZERO) > 0 || editTaxRate.getCitySalesTier2Setamt().compareTo(ArithmeticUtils.ZERO) > 0) &&
	  			(editTaxRate.getCitySalesTier1MaxAmt().compareTo(ArithmeticUtils.ZERO) == 0)) ||
	  		((editTaxRate.getCityUseTier2Rate().compareTo(ArithmeticUtils.ZERO) > 0 || editTaxRate.getCityUseTier2Setamt().compareTo(ArithmeticUtils.ZERO) > 0) &&
	  		  	(editTaxRate.getCityUseTier1MaxAmt().compareTo(ArithmeticUtils.ZERO) == 0))){
		  	FacesMessage msg = new FacesMessage("City Tier 2 Rate/Set Amount may not be entered unless a Tier 1 Through Amount is entered.");
		  	msg.setSeverity(FacesMessage.SEVERITY_ERROR);
		  	FacesContext.getCurrentInstance().addMessage(null, msg);
		  	allowAdd = false;
		}
	  	
	  	if(((editTaxRate.getCitySalesTier3Rate().compareTo(ArithmeticUtils.ZERO) > 0 || editTaxRate.getCitySalesTier3Setamt().compareTo(ArithmeticUtils.ZERO) > 0) &&
	  			(editTaxRate.getCitySalesTier2MaxAmt().compareTo(ArithmeticUtils.ZERO) == 0)) ||
	  		((editTaxRate.getCityUseTier3Rate().compareTo(ArithmeticUtils.ZERO) > 0 || editTaxRate.getCityUseTier3Setamt().compareTo(ArithmeticUtils.ZERO) > 0) &&
	  		  	(editTaxRate.getCityUseTier2MaxAmt().compareTo(ArithmeticUtils.ZERO) == 0))){
		  	FacesMessage msg = new FacesMessage("City Tier 3 Rate/Set Amount may not be entered unless a Tier 2 Through Amount is entered.");
		  	msg.setSeverity(FacesMessage.SEVERITY_ERROR);
		  	FacesContext.getCurrentInstance().addMessage(null, msg);
		  	allowAdd = false;
		}

	  	if((editTaxRate.getCitySalesTier1Rate().compareTo(ArithmeticUtils.ZERO)>0 && editTaxRate.getCitySalesTier1Setamt().compareTo(ArithmeticUtils.ZERO)>0) || 
	  			(editTaxRate.getCitySalesTier2Rate().compareTo(ArithmeticUtils.ZERO)>0 && editTaxRate.getCitySalesTier2Setamt().compareTo(ArithmeticUtils.ZERO)>0) ||
	  			(editTaxRate.getCitySalesTier3Rate().compareTo(ArithmeticUtils.ZERO)>0 && editTaxRate.getCitySalesTier3Setamt().compareTo(ArithmeticUtils.ZERO)>0) ||
	  			(editTaxRate.getCityUseTier1Rate().compareTo(ArithmeticUtils.ZERO)>0 && editTaxRate.getCityUseTier1Setamt().compareTo(ArithmeticUtils.ZERO)>0) || 
	  			(editTaxRate.getCityUseTier2Rate().compareTo(ArithmeticUtils.ZERO)>0 && editTaxRate.getCityUseTier2Setamt().compareTo(ArithmeticUtils.ZERO)>0) ||
	  			(editTaxRate.getCityUseTier3Rate().compareTo(ArithmeticUtils.ZERO)>0 && editTaxRate.getCityUseTier3Setamt().compareTo(ArithmeticUtils.ZERO)>0)){
	  		FacesMessage msg = new FacesMessage("City Tier Rate and Set Amount are mutually exclusive.");
	  		msg.setSeverity(FacesMessage.SEVERITY_ERROR);
	  		FacesContext.getCurrentInstance().addMessage(null, msg);
	  		allowAdd = false;
	  	}
	  	
	  	if(((	editTaxRate.getCitySalesTier2Rate().compareTo(ArithmeticUtils.ZERO)>0 || editTaxRate.getCitySalesTier2Setamt().compareTo(ArithmeticUtils.ZERO)>0 || 
	  			editTaxRate.getCitySalesTier3Rate().compareTo(ArithmeticUtils.ZERO)>0 || editTaxRate.getCitySalesTier3Setamt().compareTo(ArithmeticUtils.ZERO)>0    ) && 
	  			editTaxRate.getCitySalesMaxtaxAmt().compareTo(ArithmeticUtils.ZERO)>0 )       ||
  			((	editTaxRate.getCityUseTier2Rate().compareTo(ArithmeticUtils.ZERO)>0 || editTaxRate.getCityUseTier2Setamt().compareTo(ArithmeticUtils.ZERO)>0 || 
  		  			editTaxRate.getCityUseTier3Rate().compareTo(ArithmeticUtils.ZERO)>0 || editTaxRate.getCityUseTier3Setamt().compareTo(ArithmeticUtils.ZERO)>0    ) && 
  		  			editTaxRate.getCityUseMaxtaxAmt().compareTo(ArithmeticUtils.ZERO)>0 )){
	  		FacesMessage msg = new FacesMessage("City MaxTax Amount may not be entered when Tier 2 or Tier 3 Rate or Set Amount are entered.");
	  		msg.setSeverity(FacesMessage.SEVERITY_ERROR);
	  		FacesContext.getCurrentInstance().addMessage(null, msg);
	  		allowAdd = false;
	  	}
	  	
	  	//end of city
  
	  	//start of stj
	  	if((editTaxRate.getStj1SalesRate().compareTo(ArithmeticUtils.ZERO)>0 && editTaxRate.getStj1SalesSetamt().compareTo(ArithmeticUtils.ZERO)>0) || 
	  			(editTaxRate.getStj2SalesRate().compareTo(ArithmeticUtils.ZERO)>0 && editTaxRate.getStj2SalesSetamt().compareTo(ArithmeticUtils.ZERO)>0) ||
	  			(editTaxRate.getStj3SalesRate().compareTo(ArithmeticUtils.ZERO)>0 && editTaxRate.getStj3SalesSetamt().compareTo(ArithmeticUtils.ZERO)>0) ||
	  			(editTaxRate.getStj4SalesRate().compareTo(ArithmeticUtils.ZERO)>0 && editTaxRate.getStj4SalesSetamt().compareTo(ArithmeticUtils.ZERO)>0) || 
	  			(editTaxRate.getStj5SalesRate().compareTo(ArithmeticUtils.ZERO)>0 && editTaxRate.getStj5SalesSetamt().compareTo(ArithmeticUtils.ZERO)>0) ||
	  			(editTaxRate.getStj1UseRate().compareTo(ArithmeticUtils.ZERO)>0 && editTaxRate.getStj1UseSetamt().compareTo(ArithmeticUtils.ZERO)>0) || 
	  			(editTaxRate.getStj2UseRate().compareTo(ArithmeticUtils.ZERO)>0 && editTaxRate.getStj2UseSetamt().compareTo(ArithmeticUtils.ZERO)>0) ||
	  			(editTaxRate.getStj3UseRate().compareTo(ArithmeticUtils.ZERO)>0 && editTaxRate.getStj3UseSetamt().compareTo(ArithmeticUtils.ZERO)>0) ||
	  			(editTaxRate.getStj4UseRate().compareTo(ArithmeticUtils.ZERO)>0 && editTaxRate.getStj4UseSetamt().compareTo(ArithmeticUtils.ZERO)>0) || 
	  			(editTaxRate.getStj5UseRate().compareTo(ArithmeticUtils.ZERO)>0 && editTaxRate.getStj5UseSetamt().compareTo(ArithmeticUtils.ZERO)>0)){
	  		FacesMessage msg = new FacesMessage("STJ Tier Rate and Set Amount are mutually exclusive.");
	  		msg.setSeverity(FacesMessage.SEVERITY_ERROR);
	  		FacesContext.getCurrentInstance().addMessage(null, msg);
	  		allowAdd = false;
	  	}  	
	  	//end of stj
	  	
	  	if(!processValidateTaxRates()){
    		allowAdd = false;
	    }
  		
	  	if(!allowAdd){
	  		return false;
	  	}
  
  		try {
  			editTaxRate.replaceNullValue();
  		}
  		catch (Exception e) {
  		}

  		resetCombinedRates(editTaxRate);
  		taxRateService.save(editTaxRate);

	  	
	  	return allowAdd;
	}
    
    private boolean processAddTaxRate() {
    	// Process the Addition of the Tax Rate
    	boolean status = true;
    	boolean allowAdd = false;
    	Long jurisdictionId = editJurisdiction.getJurisdictionId();
    	logger.info("Jurisdiction ID in processAddTaxRate() " + jurisdictionId);
    	// Need get associated tax jurisdiction id and assign to Tax Rate
    	if (jurisdictionId == null) {
    		// Something went wrong with the insert -- ABORT -- ABORT ---
    		addMessage("No Associated Tax Jurisdiction Available.");
    		return false;
    	} 
    	
    	populateTaxRateValues();
    	
    	JurisdictionTaxrate jurisdictionTaxrate = new JurisdictionTaxrate();
	    jurisdictionTaxrate.setJurisdiction(new Jurisdiction(jurisdictionId));
	    List<JurisdictionTaxrate> validateTaxRateList = taxRateService.findByExample(jurisdictionTaxrate, 0);
	    if (!validateTaxRateList.isEmpty()) {
	    	logger.info("Jurisdiction ID inside IF) " + jurisdictionId);
	    	for (JurisdictionTaxrate jTaxrate : validateTaxRateList) {
	    		if (editTaxRate.getRatetypeCode().equalsIgnoreCase(jTaxrate.getRatetypeCode()) && 
        			editTaxRate.getEffectiveDate().equals(jTaxrate.getEffectiveDate())) {
	    			allowAdd = false;
	    			FacesMessage msg = new FacesMessage("A duplicate Effective Date exists for this Jurisdiction and Rate Type.");
	    			msg.setSeverity(FacesMessage.SEVERITY_ERROR);
	    			FacesContext.getCurrentInstance().addMessage(null, msg);
	    			return false;
	    		}
	    		else {
	    			allowAdd = true;
	    		}
	    	}
	    } 
	    else {
	    	allowAdd = true;
	    }
    
    	//Insert new TaxRate
    	editTaxRate.setJurisdiction(new Jurisdiction(jurisdictionId));
    	editTaxRate.setCustomFlag("1"); // Any user added items are custom.
  		editTaxRate.setModifiedFlag("0"); // Adding item, so not modifying
    	if(!processSaveTaxRate()){
    		return false;
    	}

  	  	logger.error("TaxJurisdictionBackingBean::processAddTaxRate::status=" + status);
  	  	return status;
    }
  
  //0003097: Combined Totals are not written to file.
  private void resetCombinedRates(JurisdictionTaxrate editTaxRate){
	  try {
		  editTaxRate.setCombinedUseRate(TaxJurisdictionBackingBean.toBigDecimal(combinedUseRate.getValue().toString()));
	  } catch (Exception e) {
		  editTaxRate.setCombinedUseRate(ArithmeticUtils.ZERO);
	  }
	  
	  try {
		  editTaxRate.setCombinedSalesRate(TaxJurisdictionBackingBean.toBigDecimal(combinedSalesRate.getValue().toString()));
	  } catch (Exception e) {
		  editTaxRate.setCombinedSalesRate(ArithmeticUtils.ZERO);
	  }
  }

  /**
   * This function performs the Update of a Tax Jurisdiction AND/OR Tax Rate to
   * the database.
   * 
   * Note: If the Tax Rate has a problem updating, it will not roll back the
   * updates to the Tax Jurisdiction.
   * 
   * @return - Return boolean of whether the Update Jurisdiction / Tax Rate was
   *         completed or not.
   */
  private Boolean processUpdateJurisdiction() {
    logger
        .debug("  TaxJurisdictionBackingBean::processUpdateJurisdiction::Starting Update Process.");
    // Process the Update of the Tax Jurisdiction and Tax Rate
    // TODO option object to be declared in class
    Boolean jurisdictionStatus = true;
    Boolean taxRateStatus = true;
    boolean allowUpdate = false;
    Long jurisdictionID = 0l;
    if (currentAction == TaxJurisdictionAction.updateJurisdiction) {
      Jurisdiction searchCityCountyStateZip = new Jurisdiction();
      searchCityCountyStateZip.setCity(editJurisdiction.getCity());
      searchCityCountyStateZip.setCounty(editJurisdiction.getCounty());
      searchCityCountyStateZip.setGeocode(editJurisdiction.getGeocode());
      searchCityCountyStateZip.setZip(editJurisdiction.getZip());
      searchCityCountyStateZip.setState(editJurisdiction.getState());
      List<Jurisdiction> searchCityCountyStateZipList = jurisdictionService.findByExample(
          searchCityCountyStateZip, 0);
      if (!searchCityCountyStateZipList.isEmpty()) {
        jurisdictionID = searchCityCountyStateZipList.get(0).getJurisdictionId();
      }
      if (jurisdictionID.equals(editJurisdiction.getJurisdictionId()) || jurisdictionID == null || jurisdictionID == 0L) {
        // allowUpdate=false;
        logger
            .debug("  TaxJurisdictionBackingBean::processUpdateJurisdiction::Tax Jurisdiction CHANGED need to update.");
        editJurisdiction.setModifiedFlag("1");
        jurisdictionService.update(editJurisdiction);
        // // Code to run if update fails
        // if (jurisdictionStatus == true) {
        // logger.debug("
        // TaxJurisdictionBackingBean::processUpdateJurisdiction::Tax
        // Jurisdiction Updated.");
        // }
      } else {
        FacesMessage msg = new FacesMessage("Duplicate Jurisdiction Exists.");
        msg.setSeverity(FacesMessage.SEVERITY_ERROR);
        FacesContext.getCurrentInstance().addMessage(null, msg);
        return false;
      }
    }
    if (currentAction == TaxJurisdictionAction.updateJurisdictionTaxrate) {
    	if (jurisdictionStatus == false) {
    		// Something went wrong with the update -- ABORT -- ABORT ---
    		addMessage("Error Updating Tax Jurisdiction (JurisdictionId=" + editJurisdiction.getJurisdictionId() + ")");
    		logger.error("TaxJurisdictionBackingBean::processUpdateJurisdiction::Error Update Tax Jurisdiction (JurisdictionId=" + editJurisdiction.getJurisdictionId() + ")");
    	}
    	else { 	  
	    	populateTaxRateValues();
	    	
	    	//Update a TaxRate
	    	editTaxRate.setModifiedFlag("1"); // Modifing item
	    	if(!processSaveTaxRate()){
	    		return false;
	    	}
    	}
    }

    return true;

  }

  /**
   * This function performs the Delete of a Tax Jurisdiction to the database. It
   * will also remove all Tax Rates associated with that Tax Jurisdiction.
   * 
   * @return - Return boolean of whether the Delete Jurisdiction was completed
   *         or not.
   * 
   */
  private Boolean processDeleteJurisdiction() {
    Boolean status = false;
    // Process the Deletion of the Tax Jurisdiction and Tax Rate
    //
    // Logic:
    // Get the JurisdictionTaxRateIds of all the Tax Rates associated with the
    // JurisdictionId
    // Delete the Tax Rates
    // Check if Tax Rates still exist
    // If so, throw error, else continue
    // Delete the Tax Jurisdiction
    // Check if the Tax Jurisdiction still exists
    // If so, throw error, else continue
    //

    logger.debug("TaxJurisdictionBackingBean::processDeleteJurisdiction::Starting the deletion.");

    // Get the JurisdictionTaxRateIds of all the Tax Rates associated with the
    // JurisdictionId
    JurisdictionTaxrate sampleTaxRate = new JurisdictionTaxrate();
    sampleTaxRate.setJurisdiction(new Jurisdiction(editJurisdiction.getJurisdictionId()));
    List<JurisdictionTaxrate> taxRates = taxRateService.findByExample(sampleTaxRate, 0);

    // Delete the Tax Rates
    // taxRateService.remove(sampleTaxRate);

 // Delete the Tax Jurisdiction If it isn't used anywhere.
    //Check if the Jurisdiction is still in use somewhere.
    List<String> jurisdictionUsageLocations = checkJurisdictionUsage();
    if(jurisdictionUsageLocations.size() == 0) {
    // Check if Tax Rates still exist
	    for (JurisdictionTaxrate taxRate : taxRates) {
	      if (taxRateService.findById(taxRate.getJurisdictionTaxrateId()) != null) {
	        taxRateService.remove(taxRate);
	        // addMessage("Was not able to delete Tax Rate (" +
	        // taxRate.getJurisdictionTaxrateId() + ")");
	        logger
	            .error("  TaxJurisdictionBackingBean::processDeleteJurisdiction::Able to delete Tax Rate (" + taxRate
	                .getJurisdictionTaxrateId() + ")");
	      }
	    }
	
	    // Delete the Tax Jurisdiction
	    jurisdictionService.remove(editJurisdiction);
	    status = true;
	      
	    // Check if the Tax Jurisdiction still exists
	    if (jurisdictionService.findById(editJurisdiction.getJurisdictionId()) != null) {
	        addMessage("Was not able to delete Tax Jurisdiction (" + editJurisdiction
	            .getJurisdictionId() + ")");
	        logger
	            .error("  TaxJurisdictionBackingBean::processDeleteJurisdiction::Was not able to delete Tax Jurisdiction (" + editJurisdiction
	                .getJurisdictionId() + ")");
	      status = false;
	    }
    } else {
	   	 FacesMessage msg = new FacesMessage("This jurisdiction is used and cannot be deleted.");
	   	 msg.setSeverity(FacesMessage.SEVERITY_ERROR);
	   	 FacesContext currContext = FacesContext.getCurrentInstance();
	   	 currContext.addMessage(null, msg);
    	addMessage("This jurisdiction is used in the following tables: ");
    	for(String location : jurisdictionUsageLocations) {
    		addMessage(location);
    	}
    	
    	status = false;
    }

    logger.debug("TaxJurisdictionBackingBean::processDeleteJurisdiction::Deletion Completed.");

    return status;
  }
  
  private List<String> checkJurisdictionUsage() {
	  return jurisdictionService.checkJurisdictionNotUsed(getSelectedJurisdiction().getId());
  }

  /**
   * This function performs the Delete of a Tax Rate to the database.
   * 
   * @return - Return boolean of whether the Delete Tax Rate was completed or
   *         not.
   * 
   */
  private Boolean processDeleteTaxRate() {
    // Process the Deletion of a Tax Rate
    logger.debug("TaxJurisdictionBackingBean::processDeleteTaxRate::Starting the deletion.");

    Boolean status = true;
    // Delete the Tax Rate
    JurisdictionTaxrate sampleTaxRate = new JurisdictionTaxrate();
    sampleTaxRate.setJurisdictionTaxrateId(editTaxRate.getJurisdictionTaxrateId());
    taxRateService.remove(sampleTaxRate);

    // Check if Tax Rates still exist
    if (taxRateService.findById(editTaxRate.getJurisdictionTaxrateId()) != null) {
      addMessage("Was not able to delete Tax Rate (" + editTaxRate.getJurisdictionTaxrateId() + ")");
      logger
          .error("  TaxJurisdictionBackingBean::processDeleteTaxRate::Error: Was not able to delete Tax Rate (" + editTaxRate
              .getJurisdictionTaxrateId() + ")");
      status = false;
    }

    logger.debug("TaxJurisdictionBackingBean::processDeleteTaxRate::Deletion Completed.");
    return status;
  }

  /*
   * 
   * The ACTION EVENTS and ACTION LISTENERS
   * 
   */

  /**
   * This function is called when the user selects a row on the
   * jurisdictionDataTable. This will set the global "selected" variables so the
   * functions know which object to apply the action against. This function also
   * also load the Tax Rate information into the taxrateDataTable to matches the
   * Tax Jurisdiction.
   * 
   * This function is called by an "actionListener".
   * 
   * @param e ActionEvent of the caller
   * @throws AbortProcessingException
   * 
   */
  public void jurisdictionRowChanged(ActionEvent e) throws AbortProcessingException {
		UIComponent uiComponent = e.getComponent().getParent();
		if (!(uiComponent instanceof UIDataAdaptor)) {
			logger.info("Invalid class to event listener: " + uiComponent.getClass().getName());
			
			return;
		}

		UIDataAdaptor table = (UIDataAdaptor) uiComponent;
		
		// Reset selection, if necessary
		if (table.getRowData() != null) {
			if(!taxRateList.isEmpty()){
				taxRateList.clear();
			}
			getRateTypeDesc();
			this.selectedJurisdiction = (Jurisdiction) table.getRowData();
			this.selectedJurisdictionIndex = table.getRowIndex();

			Long jusrisdictionId = selectedJurisdiction.getJurisdictionId();
			String selectedState = selectedJurisdiction.getState();
			getSelectionsForBinaryWeight(selectedState);
			JurisdictionTaxrate sampleTaxrate = new JurisdictionTaxrate();
			
			sampleTaxrate.setJurisdiction(new Jurisdiction(jusrisdictionId));
			
			if(srchTaxrate.getJurisdictionTaxrateId()!=null){
				sampleTaxrate.setJurisdictionTaxrateId(srchTaxrate.getJurisdictionTaxrateId());
			}
			if(srchTaxrate.getEffectiveDate()!=null){
				sampleTaxrate.setEffectiveDate(srchTaxrate.getEffectiveDate());
			}
			if(srchTaxrate.getExpirationDate()!=null){
				sampleTaxrate.setExpirationDate(srchTaxrate.getExpirationDate());
			}
			
			if(srchTaxrate.getCustomFlag()!=null){
				sampleTaxrate.setCustomFlag(srchTaxrate.getCustomFlag());
			}
			
			if(srchTaxrate.getModifiedFlag()!=null){
				sampleTaxrate.setModifiedFlag(srchTaxrate.getModifiedFlag());
			}
					
			//try {
			//	sampleTaxrate.setCustomFlag(FlagValue.valueOf(customFlagFilter).getFlag());
		   // } catch (Exception err) {
		    //	sampleTaxrate.setCustomFlag(null);
		    //}
			
		   // try {
		   // 	sampleTaxrate.setModifiedFlag(FlagValue.valueOf(modifiedFlagFilter).getFlag());
		   // } catch (Exception err) {
		    //	sampleTaxrate.setModifiedFlag(null);
		    //}
		    
		    if(srchTaxrate.getRatetypeCode()!=null){
		    	sampleTaxrate.setRatetypeCode(srchTaxrate.getRatetypeCode());
			}
			
			taxRateList = this.jurisdictionService.findByIdAndDate(sampleTaxrate);
		//	jurisdictionTaxRateDataModel.setFilterCriteria(sampleTaxrate);
			//taxRateList = selectedJurisdiction.getJurisdictionTaxrates();
			
			logger.debug("Number of tax rates for Jurisdiction ID " + jusrisdictionId + ": " + taxRateList.size());

			selectedTaxRate = null;
			selectedTaxRateIndex = -1;
		}
	}

  /**
   * This function is called when the user selects a row on the
   * taxrateDataTable. This will set the global "selected" variables so that
   * functions know which object to apply the action against.
   * 
   * This function is called by an "actionListener".
   * 
   * @param e ActionEvent of the caller
   * @throws AbortProcessingException
   * 
   */
  public void taxrateRowChanged(ActionEvent e) throws AbortProcessingException {
    UIComponent uiComponent = e.getComponent().getParent();
    if (!(uiComponent instanceof UIDataAdaptor)) {
      logger.info("Invalid class to event listener: " + uiComponent.getClass().getName());

      return;
    }

    UIDataAdaptor table = (UIDataAdaptor) uiComponent;

    // Reset selection, if necessary
    if (table.getRowData() != null) {
    	JurisdictionTaxrate jurisdictionTaxrate = new JurisdictionTaxrate();
      jurisdictionTaxrate = (JurisdictionTaxrate) table.getRowData();
            this.selectedTaxRateIndex = table.getRowIndex();
           this.selectedTaxRate = taxRateService.findById(jurisdictionTaxrate.getJurisdictionTaxrateId());
      if (this.selectedJurisdiction != null) {
        logger
            .debug("TaxJurisdictionBackingBean::taxrateRowChanged::selectedJurisdiction=" + this.selectedJurisdiction
                .getJurisdictionId());
      } else {
        logger
            .debug("TaxJurisdictionBackingBean::taxrateRowChanged::selectedJurisdiction IS EMPTY -- ERROR.");
      }
    }
  }

  /**
   * This function is called to perform the Tax Jurisdiction / Tax Rate Search.
   * It will retrieve all the data and load it into the Data Tables.
   * 
   * TODO: Need to add support for User Input Validation.
   * 
   */
  public String searchJurisdiction() {
    logger.debug("TaxJurisdictionBackingBean::searchJurisdiction");
    taxRateList.clear();
    selectedJurisdiction = null;
    selectedJurisdictionIndex = -1;
    selectedTaxRate = null;
    selectedTaxRateIndex = -1;
    editJurisdiction = null;
    editTaxRate = null;

    // Create the new Jurisdiction Object to search for
    srchJurisdiction = new Jurisdiction();
    srchJurisdiction.setJurisdictionId(this.searchjurisdictionId);
    srchJurisdiction.setGeocode(StringUtils.isNotBlank(this.searchgeocode) ? this.searchgeocode.trim() : null);

    if (searchstate!=null && searchstate.length()!=0) {
    	srchJurisdiction.setState(this.searchstate);
    }
    
    if (searchCountry!=null && searchCountry.length()!=0) {
        srchJurisdiction.setCountry(this.searchCountry);
    }
    
    srchJurisdiction.setCounty(StringUtils.isNotBlank(this.searchcounty) ? this.searchcounty.trim() : null);
    srchJurisdiction.setCity(StringUtils.isNotBlank(this.searchcity) ? this.searchcity.trim() : null);
    srchJurisdiction.setZip(StringUtils.isNotBlank(this.searchzip) ? this.searchzip.trim() : null);
    srchJurisdiction.setZipplus4(StringUtils.isNotBlank(this.searchzipplus4) ? this.searchzipplus4.trim() : null);
    srchJurisdiction.setStj1Name(StringUtils.isNotBlank(this.searchstj) ? this.searchstj.trim() : null);
    
    try {
    	srchJurisdiction.setCustomFlag(FlagValue.valueOf(customFlagFilter).getFlag());
    } catch (Exception e) {
    	srchJurisdiction.setCustomFlag(null);
    }
    srchJurisdiction.setClientGeocode(StringUtils.isNotBlank(this.searchclientGeocode) ? this.searchclientGeocode.trim() : null);
    srchJurisdiction.setCompGeocode(StringUtils.isNotBlank(this.searchcompGeocode) ? this.searchcompGeocode.trim() : null);
    
    if(getFindMode()){
    	srchJurisdiction.setDescription("INVALID");
    }

    // Create the new TaxRate Object to search for
    srchTaxrate = new JurisdictionTaxrate();
    srchTaxrate.setJurisdictionTaxrateId(this.searchjurisdictionTaxrateId);
    srchTaxrate.setEffectiveDate(this.searcheffectiveDate);
    srchTaxrate.setExpirationDate(this.searchexpirationDate);
    srchTaxrate.setRatetypeCode(searchratetype);
    
    try {
    	srchTaxrate.setCustomFlag(FlagValue.valueOf(customFlagFilter).getFlag());
    }
    catch (Exception e) {
    	srchTaxrate.setCustomFlag(null);
    }
    try {
    	srchTaxrate.setModifiedFlag(FlagValue.valueOf(modifiedFlagFilter).getFlag());
    }
    catch (Exception e) {
    	srchTaxrate.setModifiedFlag(null);
    }
    
    //0004533
    if(getFindMode()){
    	srchJurisdiction.setAutoWildSearch(true);
    }
    else{
    	srchJurisdiction.setAutoWildSearch(false);
    	
    	//Save filters for maintenance
    	jurisdictionFilter = new Jurisdiction();
    	BeanUtils.copyProperties(srchJurisdiction, jurisdictionFilter);
    	
    	taxrateFilter = new JurisdictionTaxrate();
    	BeanUtils.copyProperties(srchTaxrate, taxrateFilter);
    }

    //
    // Perform the search
    //
     jurisdictionDataModel.setFilterCriteria(srchJurisdiction, srchTaxrate);
     
     return "tax_jurisdiction";
  }
  
  private void loadMaintenanceFilter(){
	  taxRateList.clear();
	  selectedJurisdiction = null;
	  selectedJurisdictionIndex = -1;
	  selectedTaxRate = null;
	  selectedTaxRateIndex = -1;
	  editJurisdiction = null;
	  editTaxRate = null;
	  
	  if(jurisdictionFilter==null){
		  return;  
	  }
	  
	  srchJurisdiction = new Jurisdiction();
	  srchTaxrate = new JurisdictionTaxrate();
	  
	  if(getFindMode()){
	  }
	  else{
		  BeanUtils.copyProperties(jurisdictionFilter, srchJurisdiction);
		  BeanUtils.copyProperties(taxrateFilter, srchTaxrate);
	  }

	  // Create the new Jurisdiction Object to search for
	  this.searchjurisdictionId = srchJurisdiction.getJurisdictionId();
	  this.searchgeocode = srchJurisdiction.getGeocode();
	  this.searchstate = srchJurisdiction.getState();
	  this.searchCountry = srchJurisdiction.getCountry();
	  this.searchcounty = srchJurisdiction.getCounty();
	  this.searchcity = srchJurisdiction.getCity();
	  this.searchzip = srchJurisdiction.getZip();
	  this.searchzipplus4 = srchJurisdiction.getZipplus4();
	  this.searchstj = srchJurisdiction.getStj1Name();

	  customFlagFilter = srchJurisdiction.getCustomFlag();
	  this.searchclientGeocode = srchJurisdiction.getClientGeocode();
	  this.searchcompGeocode = srchJurisdiction.getCompGeocode();

	  // Create the new TaxRate Object to search for
	  this.searchjurisdictionTaxrateId = srchTaxrate.getJurisdictionTaxrateId();
	  this.searcheffectiveDate = srchTaxrate.getEffectiveDate();
	  this.searchexpirationDate = srchTaxrate.getExpirationDate();
	  searchratetype = srchTaxrate.getRatetypeCode();
	  customFlagFilter = srchTaxrate.getCustomFlag();
	  modifiedFlagFilter = srchTaxrate.getModifiedFlag();

	  //
	  // Perform the search
	  //
	  jurisdictionDataModel.setFilterCriteria(srchJurisdiction, srchTaxrate);
  }

  public String resetSearch(){
  	logger.debug("clearing search");
  	if(!taxRateList.isEmpty()){
  		taxRateList.clear();
  	}
  	
  	searchjurisdictionId = null;
    searchjurisdictionTaxrateId = null;
    searchgeocode = "";
    searchCountry="";
    searchstate = "";
    searchcounty = "";
    searchcity = "";
  	searchzip = "";
  	searchzipplus4 = "";
  	searchstj = "";
  	searchratetype = "";
    searchclientGeocode = "";
    searchcompGeocode = "";
    
    searcheffectiveDate = null;
    searchexpirationDate = null;
    customFlagFilter = "";
    modifiedFlagFilter = "";

    return null;
  }
  
  public List<JurisdictionTaxrate> viewTransactionTaxrate(JurisdictionTaxrate sampleTaxrate){ 
	  getRateTypeDesc();
	  return jurisdictionService.findByIdAndMeasureType(sampleTaxrate);
  }

  public String addFromTransaction(Jurisdiction jurisdiction,String measureTypeCode) {
    selectedJurisdiction = new Jurisdiction(jurisdiction);
    //TODO: AC
    this.rateTypeCodeMenu.setValue(measureTypeCode);
    
    return processJurisdictionSelection(TaxJurisdictionAction.addJurisdictionTaxrateFromTransaction);
  }
  
	public String viewFromTransaction(Jurisdiction jurisdiction, JurisdictionTaxrate jurisdictionTaxrate) {
		selectedJurisdiction = jurisdiction;
		selectedTaxRate = jurisdictionTaxrate;
		if(selectedTaxRate != null) {
			this.rateTypeCodeMenu.setValue(this.selectedTaxRate.getRatetypeCode());
		}

		return processJurisdictionSelection(TaxJurisdictionAction.viewJurisdictionTaxrateFromTransaction);
	}

  //method modified for 3938
public void copyAddFromTransaction(Long jurisdictionId , Long jurisdictionTaxRateId) {
	Date effectiveDate = taxRateService.findByDate(jurisdictionId);
	JurisdictionTaxrate jTaxrate = new JurisdictionTaxrate();
	jTaxrate = taxRateService.findById(jurisdictionTaxRateId);
	jTaxrate.setJurisdiction(new Jurisdiction(jurisdictionId));
	jTaxrate.setEffectiveDate(effectiveDate);
	jTaxrate.setJurisdictionTaxrateId(null);
	try{
		jTaxrate.replaceNullValue();
		taxRateService.save(jTaxrate);
		purchaseTransactionService.processSuspendedTransactions(jTaxrate.getId(), PurchaseTransactionServiceConstants.SuspendReason.SUSPEND_RATE, false);
	}catch(Exception e){
		logger.warn("Failed to process command: ");
		e.printStackTrace();
	}
}
  

  /*
   * The BUTTONS
   * 
   */

  /**
   * 
   */
  public String processTaxJurisdictionCommand() {
	resetRates();

    String command = (String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("command");
    try {
      TaxJurisdictionAction taxJurisdictionAction = TaxJurisdictionAction.valueOf(command);
      logger.debug("taxJurisdictionAction: " + taxJurisdictionAction);
      return processJurisdictionSelection(taxJurisdictionAction);
    } catch (Exception e) {
      logger.warn("Failed to process command: " + command);
      e.printStackTrace();
      return null;
    }
  }

  /**
   * This function is called when the View Button is pressed on the Main Page.
   * 
   * @return String telling Spring to go the appropriate next page.
   */
  public String viewTaxJurisdictionButton() {
    logger.debug("TaxJurisdictionBackingBean::viewTaxJurisdictionButton");
    // Check to make sure there is something to display, else just go back
    // to search page.
    if ((this.selectedJurisdiction == null) && (this.selectedTaxRate == null)) {
      logger.debug("TaxJurisdictionBackingBean::viewTaxJurisdictionButton::Go back to Search Page");
      // Go back to search page
      return null;
    } else {
      logger
          .debug("TaxJurisdictionBackingBean::viewTaxJurisdictionButton::proceed to process action");
      return processJurisdictionSelection(TaxJurisdictionAction.viewJurisdiction);
    }
  }

  /**
   * This function is called when the Cancel Button is pressed on the Detail
   * Page.
   * 
   * @return String telling Spring to go the appropriate next page.
   */

  public String cancelActionButton() {
    logger.info("TaxJurisdictionBackingBean::cancelActionButton::returning tax_jurisdiction");
    resetControls();
    clearMessageQueue(); // Clears user message queue
    
    if (currentAction.equals(TaxJurisdictionAction.addJurisdictionTaxrateFromTransaction)) {
      return "trans_main";
    }
    else if(currentAction.equals(TaxJurisdictionAction.viewJurisdictionFromTransaction) || currentAction.equals(TaxJurisdictionAction.viewJurisdictionTaxrateFromTransaction)) {
    	return "trans_view";
    }
    else if(currentAction.equals(TaxJurisdictionAction.viewJurisdictionTaxrateFromTransactionLog) || currentAction.equals(TaxJurisdictionAction.viewJurisdictionFromTransactionLog)) {
    	return "trans_log_entry_view";
    }
    return "tax_jurisdiction";
  }

  /**
   * This function actually call the process to perform the actual work once the
   * OK button is selected.
   * 
   * @return String telling Spring to go back to the main Tax Jurisdiction / Tax
   *         Rate page.
   * 
   */
  
  	private String tabSelected = "statetab";
  	
  	public void setTabSelected(String tabSelected) {
		this.tabSelected = tabSelected;
	}
	
	public String getTabSelected() {
		return tabSelected;
	}
  
    public String processTabCommand() {
	    String command = (String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("command");
	    tabSelected = command;

	    return null;
	}
    
    public void processTabCommand(ActionEvent e) throws AbortProcessingException {
		String command = (String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("command");
		tabSelected = command;
    }
  
  public String okAndAddTaxRate() {
	  return okActionButton(true);
  }
  
  public String okActionButton() {
	  return okActionButton(false);
  }
  
  public String okActionButton(boolean addTaxRateNext) {
    logger.debug("TaxJurisdictionBackingBean::okActionButton::action=" + currentAction);

    boolean status = true;

    clearMessageQueue(); // Clears user message queue
    
    boolean allowAdd = true;
    
    if(currentAction == TaxJurisdictionAction.addJurisdiction 
    		|| currentAction == TaxJurisdictionAction.copyAddJurisdiction 
    		|| currentAction == TaxJurisdictionAction.updateJurisdiction) {
	    if(editJurisdiction.getEffectiveDate() == null) {
	    	FacesMessage msg = new FacesMessage("Effective Date is required.");
	        msg.setSeverity(FacesMessage.SEVERITY_ERROR);
	        FacesContext.getCurrentInstance().addMessage(null, msg);
	        allowAdd = false;
	    }
	    
	    if(editJurisdiction.getExpirationDate() == null) {
	    	FacesMessage msg = new FacesMessage("Expiration Date is required.");
	        msg.setSeverity(FacesMessage.SEVERITY_ERROR);
	        FacesContext.getCurrentInstance().addMessage(null, msg);
	        allowAdd = false;
	    }
	    
	    if(editJurisdiction.getExpirationDate() != null && editJurisdiction.getEffectiveDate() != null && editJurisdiction.getExpirationDate().getTime() <= editJurisdiction.getEffectiveDate().getTime()) {
	    	FacesMessage msg = new FacesMessage("Expiration Date must be greater than Effective Date.");
	        msg.setSeverity(FacesMessage.SEVERITY_ERROR);
	        FacesContext.getCurrentInstance().addMessage(null, msg);
	        allowAdd = false;
	    }
    }
    
    if(editJurisdiction.getGeocode()==null || editJurisdiction.getGeocode().trim().length()==0){
    	FacesMessage msg = new FacesMessage("Geocode is required.");
        msg.setSeverity(FacesMessage.SEVERITY_ERROR);
        FacesContext.getCurrentInstance().addMessage(null, msg);
        allowAdd = false;
    }
    
    if(editJurisdiction.getCity()==null || editJurisdiction.getCity().trim().length()==0){
    	FacesMessage msg = new FacesMessage("City is required.");
        msg.setSeverity(FacesMessage.SEVERITY_ERROR);
        FacesContext.getCurrentInstance().addMessage(null, msg);
        allowAdd = false;
    }
    
    if(searchstateDetail==null || searchstateDetail.trim().length()==0){
    	FacesMessage msg = new FacesMessage("State is required.");
        msg.setSeverity(FacesMessage.SEVERITY_ERROR);
        FacesContext.getCurrentInstance().addMessage(null, msg);
        allowAdd = false;
    }
    
    if(editJurisdiction.getZip()==null || editJurisdiction.getZip().trim().length()==0){
    	FacesMessage msg = new FacesMessage("Zip is required.");
        msg.setSeverity(FacesMessage.SEVERITY_ERROR);
        FacesContext.getCurrentInstance().addMessage(null, msg);
        allowAdd = false;
    }
    
    if(editJurisdiction.getCounty()==null || editJurisdiction.getCounty().trim().length()==0){
    	FacesMessage msg = new FacesMessage("County is required.");
        msg.setSeverity(FacesMessage.SEVERITY_ERROR);
        FacesContext.getCurrentInstance().addMessage(null, msg);
        allowAdd = false;
    }
    
    if(searchCountryDetail==null || searchCountryDetail.trim().length()==0){
    	FacesMessage msg = new FacesMessage("Country is required.");
        msg.setSeverity(FacesMessage.SEVERITY_ERROR);
        FacesContext.getCurrentInstance().addMessage(null, msg);
        allowAdd = false;
    }
    
    if(!allowAdd){
    	return null;
    }

    if (currentAction == TaxJurisdictionAction.viewJurisdiction) {
      // Do nothing, just go back.
    } else if ((currentAction == TaxJurisdictionAction.addJurisdiction) || (currentAction == TaxJurisdictionAction.copyAddJurisdiction)) {
      // Call function to add Tax Jurisdiction.
      editJurisdiction.setState(searchstateDetail);
      editJurisdiction.setCountry(searchCountryDetail);
      status = processAddJurisdiction();
    } else if ((currentAction == TaxJurisdictionAction.updateJurisdiction) || (currentAction == TaxJurisdictionAction.updateJurisdictionTaxrate)) {
      // Call function to update Tax Jurisdiction.
      editJurisdiction.setState(searchstateDetail);
      editJurisdiction.setCountry(searchCountryDetail);
      status = processUpdateJurisdiction();
    } else if (currentAction == TaxJurisdictionAction.deleteJurisdiction) {
      // Call function to delete Tax Jurisdiction
      status = processDeleteJurisdiction();
    } else if ((currentAction == TaxJurisdictionAction.addJurisdictionTaxrate) || (currentAction == TaxJurisdictionAction.copyAddJurisdictionTaxrate) ||
    		(currentAction == TaxJurisdictionAction.copyUpdateJurisdictionTaxrate)) {
      // Call function to add Tax Rate
      status = processAddTaxRate();
    } else if (currentAction == TaxJurisdictionAction.addJurisdictionTaxrateFromTransaction) {
      // Call function to add Tax Rate and update transaction
    	
      //5215, use the date that was entered 
      //Date glDate = transactionDetailBean.getCurrentTransaction().getGlDate();
      //editTaxRate.setEffectiveDate((glDate.getTime() < effDate.getTime() ? glDate : effDate));
      Date effDate = editTaxRate.getEffectiveDate();  
    	
      status = processAddTaxRate();
	  transactionDetailBean.listTransactions();
    } else if (currentAction == TaxJurisdictionAction.copyAddJurisdictionTaxrateFromTransaction) {
        // Call function to add Tax Rate and update transaction
    	
        //Date effDate = editTaxRate.getEffectiveDate();  
    	
    	//5215, use the earliest G/L Date from all matching Transactions 
    	//Date glDate = transactionDetailBean.getCurrentTransaction().getGlDate();
    	//editTaxRate.setEffectiveDate((glDate.getTime() < effDate.getTime() ? glDate : effDate));
    	Date glDate = taxRateService.findByDate(transactionDetailBean.getCurrentTransaction().getShiptoJurisdictionId());
        editTaxRate.setEffectiveDate(glDate);
      	
        status = processAddTaxRate();
  	    transactionDetailBean.listTransactions();
    }else if (currentAction == TaxJurisdictionAction.deleteJurisdictionTaxrate) {
      // Call function to delete Tax Rate
      status = processDeleteTaxRate();
    }
    
    //4824
    //if (setWarnings()) {
    //	warningsShown = true;
    //	return null;
    //}
    	
    logger.debug("TaxJurisdictionBackingBean::okActionButton::status = " + status);
    if (status == true) {
      if (currentAction == TaxJurisdictionAction.addJurisdictionTaxrateFromTransaction || (currentAction == TaxJurisdictionAction.copyAddJurisdictionTaxrateFromTransaction)) {
        // Return to transaction screen
        return "trans_main";
      }
      else if(addTaxRateNext) {
    	selectedJurisdiction = editJurisdiction;
    	return processJurisdictionSelection(TaxJurisdictionAction.addJurisdictionTaxrate);
      }

      // redo the search so results of actions are shown
      searchJurisdiction();
      return "tax_jurisdiction";
    } else {
      // Prepare the Modal Panel with any message(s) to the user.
      generateMessageModalPanel();
      return null;
    }

  }
  
  public void resetRates(){
	  //PP-24
	  countrySalesTier1RateInputText.resetValue();
	  countrySalesTier1RateInputText.setValue(0.0); 
	  countryUseTier1RateInputText.resetValue();
	  countryUseTier1RateInputText.setValue(0.0); 

	  stj1UseRateInputText.resetValue();
	  stj2UseRateInputText.resetValue();
	  stj3UseRateInputText.resetValue();
	  stj4UseRateInputText.resetValue();
	  stj5UseRateInputText.resetValue();
	  
	  stj1SalesRateInputText.resetValue();
	  stj2SalesRateInputText.resetValue();
	  stj3SalesRateInputText.resetValue();
	  stj4SalesRateInputText.resetValue();
	  stj5SalesRateInputText.resetValue();	  
	  
	  rateUsed = false;
  }
  
  public boolean getRateUsed(){
	  return rateUsed;
  }
  
  public void recalCombinedRates(ValueChangeEvent vce) {
	  //PP-24
	  BigDecimal countrySalesTier1RateDbl = TaxJurisdictionBackingBean.toBigDecimal(countrySalesTier1RateInputText.getValue());
	  BigDecimal countryUseTier1RateDbl = TaxJurisdictionBackingBean.toBigDecimal(countryUseTier1RateInputText.getValue());
	  
	  BigDecimal stateSalesTier1RateDbl = TaxJurisdictionBackingBean.toBigDecimal(stateSalesTier1RateInputText.getValue());
	  BigDecimal stateUseTier1RateDbl = TaxJurisdictionBackingBean.toBigDecimal(stateUseTier1RateInputText.getValue());
	  
	  BigDecimal countySalesTier1RateDbl = TaxJurisdictionBackingBean.toBigDecimal(countySalesTier1RateInputText.getValue());
	  BigDecimal countyUseTier1RateDbl = TaxJurisdictionBackingBean.toBigDecimal(countyUseTier1RateInputText.getValue());
	  
	  BigDecimal citySalesTier1RateDbl = TaxJurisdictionBackingBean.toBigDecimal(citySalesTier1RateInputText.getValue());
	  BigDecimal cityUseTier1RateDbl = TaxJurisdictionBackingBean.toBigDecimal(cityUseTier1RateInputText.getValue());

	  BigDecimal stj1UseRateDbl = TaxJurisdictionBackingBean.toBigDecimal(stj1UseRateInputText.getValue());
	  BigDecimal stj2UseRateDbl = TaxJurisdictionBackingBean.toBigDecimal(stj2UseRateInputText.getValue());
	  BigDecimal stj3UseRateDbl = TaxJurisdictionBackingBean.toBigDecimal(stj3UseRateInputText.getValue());
	  BigDecimal stj4UseRateDbl = TaxJurisdictionBackingBean.toBigDecimal(stj4UseRateInputText.getValue());
	  BigDecimal stj5UseRateDbl = TaxJurisdictionBackingBean.toBigDecimal(stj5UseRateInputText.getValue());
	  
	  BigDecimal stj1SalesRateDbl = TaxJurisdictionBackingBean.toBigDecimal(stj1SalesRateInputText.getValue());
	  BigDecimal stj2SalesRateDbl = TaxJurisdictionBackingBean.toBigDecimal(stj2SalesRateInputText.getValue());
	  BigDecimal stj3SalesRateDbl = TaxJurisdictionBackingBean.toBigDecimal(stj3SalesRateInputText.getValue());
	  BigDecimal stj4SalesRateDbl = TaxJurisdictionBackingBean.toBigDecimal(stj4SalesRateInputText.getValue());
	  BigDecimal stj5SalesRateDbl = TaxJurisdictionBackingBean.toBigDecimal(stj5SalesRateInputText.getValue());
	    
	  BigDecimal d1 = ArithmeticUtils.ZERO;
	  d1 = ArithmeticUtils.add(d1, countryUseTier1RateDbl == null ? ArithmeticUtils.ZERO : countryUseTier1RateDbl);
	  d1 = ArithmeticUtils.add(d1, stateUseTier1RateDbl == null ? ArithmeticUtils.ZERO : stateUseTier1RateDbl);
	  d1 = ArithmeticUtils.add(d1, countyUseTier1RateDbl == null ? ArithmeticUtils.ZERO : countyUseTier1RateDbl);
	  d1 = ArithmeticUtils.add(d1, cityUseTier1RateDbl == null ? ArithmeticUtils.ZERO : cityUseTier1RateDbl);
	  
	  d1 = ArithmeticUtils.add(d1, stj1UseRateDbl == null ? ArithmeticUtils.ZERO : stj1UseRateDbl);
	  d1 = ArithmeticUtils.add(d1, stj2UseRateDbl == null ? ArithmeticUtils.ZERO : stj2UseRateDbl);
	  d1 = ArithmeticUtils.add(d1, stj3UseRateDbl == null ? ArithmeticUtils.ZERO : stj3UseRateDbl);
	  d1 = ArithmeticUtils.add(d1, stj4UseRateDbl == null ? ArithmeticUtils.ZERO : stj4UseRateDbl);
	  d1 = ArithmeticUtils.add(d1, stj5UseRateDbl == null ? ArithmeticUtils.ZERO : stj5UseRateDbl);
	  
	  combinedUseRate.setValue(""+d1.doubleValue());

	  String strWarning = (d1.doubleValue()  >= getWarningLevel())? "Warning: greater than preferred (" + getWarningLevel() + ")" : "";
	  useWarningOutputText.setValue(strWarning);

	  d1 = ArithmeticUtils.ZERO;
	  d1 = ArithmeticUtils.add(d1, countrySalesTier1RateDbl == null ? ArithmeticUtils.ZERO : countrySalesTier1RateDbl);
	  d1 = ArithmeticUtils.add(d1, stateSalesTier1RateDbl == null ? ArithmeticUtils.ZERO : stateSalesTier1RateDbl);
	  d1 = ArithmeticUtils.add(d1, countySalesTier1RateDbl == null ? ArithmeticUtils.ZERO : countySalesTier1RateDbl);
	  d1 = ArithmeticUtils.add(d1, citySalesTier1RateDbl == null ? ArithmeticUtils.ZERO : citySalesTier1RateDbl);

	  d1 = ArithmeticUtils.add(d1, stj1SalesRateDbl == null ? ArithmeticUtils.ZERO : stj1SalesRateDbl);
	  d1 = ArithmeticUtils.add(d1, stj2SalesRateDbl == null ? ArithmeticUtils.ZERO : stj2SalesRateDbl);
	  d1 = ArithmeticUtils.add(d1, stj3SalesRateDbl == null ? ArithmeticUtils.ZERO : stj3SalesRateDbl);
	  d1 = ArithmeticUtils.add(d1, stj4SalesRateDbl == null ? ArithmeticUtils.ZERO : stj4SalesRateDbl);
	  d1 = ArithmeticUtils.add(d1, stj5SalesRateDbl == null ? ArithmeticUtils.ZERO : stj5SalesRateDbl);
	  
	  combinedSalesRate.setValue(""+d1.doubleValue());

	  strWarning = (d1.doubleValue() >= getWarningLevel())? "Warning: greater than preferred (" + getWarningLevel() + ")" : "";
	  salesWarningOutputText.setValue(strWarning);
  }
  
  private Double getRoundup(Double aDouble){
	  Long aLong = (long)(aDouble * 100000000.0 + 0.5);
	  Double newaDouble = ((double)aLong)/100000000.0;
	  return newaDouble;
  }

  public boolean setWarnings() {
	  	BigDecimal d1 = ArithmeticUtils.ZERO;
	    combinedUseRate.setValue(d1);
	    setUseWarning(d1.doubleValue() >= getWarningLevel());
	    
	    d1 = ArithmeticUtils.ZERO;
	    combinedSalesRate.setValue(d1);
	    setSalesWarning(d1.doubleValue() >= getWarningLevel());

	    boolean showWarning = !warningsShown && (getUseWarning() || getSalesWarning());
	    return showWarning;
  }
  
  /*
   * 
   * The VALUE CHANGE LISTENERS
   * 
   */

  /**
   * This function is called when the State Menu has changed on the interface.
   * It resets the Measure Type Code Menu to it's appropriate value.
   * 
   * @param vce - ValueChangeEvent from JSF page.
   */
  public void vclStateMenu(ValueChangeEvent vce) {
    logger.debug("TaxJurisdictionBackingBean::vclStateMenu::Entering.");
    // Get selected city.
    String selectedState = (String) vce.getNewValue();

    getSelectionsForBinaryWeight(selectedState);
  }

  	//start of country
  	public void countrySalesTier1RateInputTextChangeListener(ValueChangeEvent vce) {	  
  		recalCombinedRates(null);
  	}

  	public void countrySalesTier1MaxAmtChangeListener(ValueChangeEvent vce) {
  		try {
  			String newValue = vce.getNewValue().toString(); 	  
  			if(countrySameAsSalesRatesCheck){
  				countryUseTier1MaxAmtInputText.setValue(newValue);
  			}
    	  
  			if (newValue.length() > 0) {
  				Long amount = null;
  				try {
  					amount = Long.parseLong(newValue);
  				}
  				catch (Exception e) {}

  				if (!new Long(JurisdictionTaxrate.MAX_AMOUNT).equals(amount)) {
  					countrySalesTier2MinAmtInputText.setValue(amount);
  					countrySalesTier2MinAmtInputText.setDisabled(true);
  					AddToMap(MODE_EDIT, "STATETIER2MINAMOUNT", PARAM_DISABLED, VALUE_TRUE);
  				}
  			}
  			else {
  				countrySalesTier2MinAmtInputText.setDisabled(false);
  				countrySalesTier2MinAmtInputText.setReadonly(false);
  				countrySalesTier2MinAmtInputText.setValue(""+editTaxRate.getCountrySalesTier2MinAmt());

  				AddToMap(MODE_EDIT, "STATETIER2MINAMOUNT", PARAM_DISABLED, VALUE_FALSE);
  			}
  		}
  		catch (NumberFormatException nfe) {
  		}
  	}
  
    public void countrySalesTier1SetToMaximumFlagChangeListener(ActionEvent e) throws AbortProcessingException {
    		HtmlSelectBooleanCheckbox chk = (HtmlSelectBooleanCheckbox) ((HtmlAjaxSupport) e.getComponent()).getParent();		
    		Boolean flag = (Boolean)chk.getValue();	
    		System.out.println("countrySalesTier1SetToMaximumFlagChangeListener : " + flag);
	    if (flag == true) {	
	        countrySalesTier1MaxAmtInputText.setValue(JurisdictionTaxrate.MAX_STRING);
		    countrySalesTier1MaxAmtInputText.setDisabled(true);
		    countrySalesTier1MaxAmtInputText.setReadonly(true);
		    countrySalesTier2MinAmtInputText.setValue("0");
		    countrySalesTier2MinAmtInputText.setDisabled(false);
		    countrySalesTier2MinAmtInputText.setReadonly(false);
	    } 
	    else {
	        countrySalesTier1MaxAmtInputText.setValue("0");
	        countrySalesTier1MaxAmtInputText.setDisabled(false);
	        countrySalesTier1MaxAmtInputText.setReadonly(false);
	    	
	        countrySalesTier2MinAmtInputText.setDisabled(true);
	        countrySalesTier2MinAmtInputText.setReadonly(true);
	        countrySalesTier2MinAmtInputText.setValue("0");
	    }
	    
	    countrySalesTier1SetToMaximumCheck = flag;
	    countrySalesTier1SetToMaximumCheckBind.setSelected(flag);	  
    }

	public void countrySalesTier2SetToMaximumFlagChangeListener(ActionEvent e) throws AbortProcessingException {
		HtmlSelectBooleanCheckbox chk = (HtmlSelectBooleanCheckbox) ((HtmlAjaxSupport) e.getComponent()).getParent();		
		Boolean flag = (Boolean)chk.getValue();	
		System.out.println("countrySalesTier2SetToMaximumFlagChangeListener : " + flag);

    	if (flag == true) {
    		countrySalesTier2MaxAmtInputText.setValue(JurisdictionTaxrate.MAX_STRING);
    		countrySalesTier2MaxAmtInputText.setDisabled(flag);	
    		
    		countrySalesTier3RateInputText.setValue("0");
  	      	countrySalesTier3RateInputText.setDisabled(flag);
  	      	countrySalesTier3RateInputText.setReadonly(flag);
  	      	countrySalesTier3SetamtInputText.setValue("0");
  	      	countrySalesTier3SetamtInputText.setDisabled(flag);
  	      	countrySalesTier3SetamtInputText.setReadonly(flag);
  	      	
  	      	countrySalesTier3TaxEntireAmountCheck = false;
  	      	countrySalesTier3TaxEntireAmountCheckBind.setSelected(false);
  	      	countrySalesTier3TaxEntireAmountCheckBind.setDisabled(flag);
  	      	countrySalesTier3TaxEntireAmountCheckBind.setReadonly(flag);
      
    		recalCombinedRates(null);
    	} 
    	else {
    		countrySalesTier2MaxAmtInputText.setValue("0");
    		countrySalesTier2MaxAmtInputText.setDisabled(flag);
    		
    		countrySalesTier3RateInputText.setDisabled(flag);
    	    countrySalesTier3RateInputText.setReadonly(flag);
    	    countrySalesTier3SetamtInputText.setDisabled(flag);
    	    countrySalesTier3SetamtInputText.setReadonly(flag);	
    	    countrySalesTier3TaxEntireAmountCheckBind.setDisabled(flag);
    	    countrySalesTier3TaxEntireAmountCheckBind.setReadonly(flag);

    		recalCombinedRates(null);
    	}
    }

	public void countrySalesTier2TaxEntireAmountCheckChangeListener(ActionEvent e) throws AbortProcessingException {
		HtmlSelectBooleanCheckbox chk = (HtmlSelectBooleanCheckbox) ((HtmlAjaxSupport) e.getComponent()).getParent();		
		Boolean flag = (Boolean)chk.getValue();	
		System.out.println("countrySalesTier2TaxEntireAmountCheckChangeListener : " + flag);
      
  	  	countrySalesTier2TaxEntireAmountCheck = flag;
  	  	countrySalesTier2TaxEntireAmountCheckBind.setSelected(flag);
  	  
  	  	if(countrySameAsSalesRatesCheck){
  	  		countryUseTier2TaxEntireAmountCheck = flag;
  	  		countryUseTier2TaxEntireAmountCheckBind.setSelected(flag);
		}
    }
  
	public void countrySalesTier3TaxEntireAmountCheckChangeListener(ActionEvent e) throws AbortProcessingException {
		HtmlSelectBooleanCheckbox chk = (HtmlSelectBooleanCheckbox) ((HtmlAjaxSupport) e.getComponent()).getParent();		
		Boolean flag = (Boolean)chk.getValue();	

  	  	countrySalesTier3TaxEntireAmountCheck = flag;
  	  	countrySalesTier3TaxEntireAmountCheckBind.setSelected(flag);
  	  	
  	  	if(countrySameAsSalesRatesCheck){
  	  		countryUseTier3TaxEntireAmountCheck = flag;
  	  		countryUseTier3TaxEntireAmountCheckBind.setSelected(flag);
  	  	}
    }
    
	public void countryUseTier2TaxEntireAmountCheckChangeListener(ActionEvent e) throws AbortProcessingException {
		HtmlSelectBooleanCheckbox chk = (HtmlSelectBooleanCheckbox) ((HtmlAjaxSupport) e.getComponent()).getParent();		
		Boolean flag = (Boolean)chk.getValue();	

  	  	countryUseTier2TaxEntireAmountCheck = flag;
  	  	countryUseTier2TaxEntireAmountCheckBind.setSelected(flag);
    }
    
	public void countryUseTier3TaxEntireAmountCheckChangeListener(ActionEvent e) throws AbortProcessingException {
		HtmlSelectBooleanCheckbox chk = (HtmlSelectBooleanCheckbox) ((HtmlAjaxSupport) e.getComponent()).getParent();		
		Boolean flag = (Boolean)chk.getValue();	

  	  	countryUseTier3TaxEntireAmountCheck = flag;
  	  	countryUseTier3TaxEntireAmountCheckBind.setSelected(flag);
    }

    public void countryUseTier1MaxAmtChangeListener(ValueChangeEvent vce) {
    	try {
    		String newValue = vce.getNewValue().toString();

    		if (newValue.length() > 0) {
    			Long amount = null;
    			try {
    				amount = Long.parseLong(newValue);
    			}catch (Exception e) {}

    			if (!new Long(JurisdictionTaxrate.MAX_AMOUNT).equals(amount)) {
    				countryUseTier2MinAmtInputText.setValue(amount);
    				countryUseTier2MinAmtInputText.setDisabled(true);
    				AddToMap(MODE_EDIT, "STATETIER2MINAMOUNT", PARAM_DISABLED, VALUE_TRUE);
    			}
    		}
    		else {
    			countryUseTier2MinAmtInputText.setDisabled(false);
    			countryUseTier2MinAmtInputText.setReadonly(false);
    			countryUseTier2MinAmtInputText.setValue(""+editTaxRate.getCountryUseTier2MinAmt());

    			AddToMap(MODE_EDIT, "STATETIER2MINAMOUNT", PARAM_DISABLED, VALUE_FALSE);
    		}
    	}
    	catch (NumberFormatException nfe) {
    	}
    } 
  
	public void countryUseTier1SetToMaximumFlagChangeListener(ActionEvent e) throws AbortProcessingException {
		HtmlSelectBooleanCheckbox chk = (HtmlSelectBooleanCheckbox) ((HtmlAjaxSupport) e.getComponent()).getParent();		
		Boolean flag = (Boolean)chk.getValue();	
		System.out.println("countryUseTier1SetToMaximumFlagChangeListener : " + flag);

    	if (flag == true) {
    		countryUseTier1MaxAmtInputText.setValue(JurisdictionTaxrate.MAX_STRING);
    		countryUseTier1MaxAmtInputText.setDisabled(true);
    		countryUseTier1MaxAmtInputText.setReadonly(true);
    		countryUseTier2MinAmtInputText.setValue("0");
    		countryUseTier2MinAmtInputText.setDisabled(false);
    		countryUseTier2MinAmtInputText.setReadonly(false);
    	} 
    	else {
    		countryUseTier1MaxAmtInputText.setValue("0");
    		countryUseTier1MaxAmtInputText.setDisabled(false);
    		countryUseTier1MaxAmtInputText.setReadonly(false);
    	
    		countryUseTier2MinAmtInputText.setDisabled(true);
    		countryUseTier2MinAmtInputText.setReadonly(true);
    		countryUseTier2MinAmtInputText.setValue("0");
    	}
    
    	countryUseTier1SetToMaximumCheck = flag;
    	countryUseTier1SetToMaximumCheckBind.setSelected(flag);
    }
  
    public void countryUseTier1RateInputTextChangeListener(ValueChangeEvent vce) {	  
	    recalCombinedRates(null);
    }

	public void countrySameAsSalesRatesChangeListener(ActionEvent e) throws AbortProcessingException {
		HtmlSelectBooleanCheckbox chk = (HtmlSelectBooleanCheckbox) ((HtmlAjaxSupport) e.getComponent()).getParent();		
		Boolean flag = (Boolean)chk.getValue();	
    	
    	countrySameAsSalesRatesCheck = flag;
    	countrySameAsSalesRatesCheckBind.setSelected(flag);
	  	
    	if (flag == true) {
    		//Country Tier 1	
    		countryUseTier1RateInputText.setDisabled(true);
		  	countryUseTier1RateInputText.setReadonly(true);
		  
		  	countryUseTier1SetamtInputText.setDisabled(true);
		  	countryUseTier1SetamtInputText.setReadonly(true);
		  
		  	countryUseTier1MaxAmtInputText.setDisabled(true);
		  	countryUseTier1MaxAmtInputText.setReadonly(true);
		  	
		  	//Country Tier 2
		  	countryUseTier2MinAmtInputText.setDisabled(true);
		  	countryUseTier2MinAmtInputText.setReadonly(true);

		  	countryUseTier2RateInputText.setDisabled(true);
		  	countryUseTier2RateInputText.setReadonly(true);

		  	countryUseTier2SetamtInputText.setDisabled(true);
		  	countryUseTier2SetamtInputText.setReadonly(true);

		  	countryUseTier2MaxAmtInputText.setDisabled(true);
		  	countryUseTier2MaxAmtInputText.setReadonly(true);
 
		  	//Country Tier 3
		  	countryUseTier3RateInputText.setDisabled(true);
		  	countryUseTier3RateInputText.setReadonly(true);

		  	countryUseTier3SetamtInputText.setDisabled(true);
		  	countryUseTier3SetamtInputText.setReadonly(true);

		  	countryUseTier1SetToMaximumCheckBind.setDisabled(true);
		  	
		  	countryUseTier2SetToMaximumCheckBind.setDisabled(true);
		  	
		  	countryUseTier2TaxEntireAmountCheckBind.setDisabled(true);
		  	countryUseTier2TaxEntireAmountCheckBind.setReadonly(true);

		  	countryUseTier3TaxEntireAmountCheckBind.setDisabled(true);
		  	countryUseTier3TaxEntireAmountCheckBind.setReadonly(true);
		  	
		  	countryUseMaxtaxAmtInputText.setDisabled(true);
		  	countryUseMaxtaxAmtInputText.setReadonly(true);
    	}
    	else {
    		//Country Tier 1	
    		countryUseTier1RateInputText.setDisabled(false);
    		countryUseTier1RateInputText.setReadonly(false);
		  
    		countryUseTier1SetamtInputText.setDisabled(false);
    		countryUseTier1SetamtInputText.setReadonly(false);
		  
    		countryUseTier1MaxAmtInputText.setDisabled(false);
    		countryUseTier1MaxAmtInputText.setReadonly(false);
    		
    		countryUseTier1SetToMaximumCheckBind.setDisabled(false);

    		//Country Tier 2
    		countryUseTier2MinAmtInputText.setDisabled(false);
    		countryUseTier2MinAmtInputText.setReadonly(false);
    		countryUseTier2RateInputText.setDisabled(false);
    		countryUseTier2RateInputText.setReadonly(false);
    		countryUseTier2SetamtInputText.setDisabled(false);
    		countryUseTier2SetamtInputText.setReadonly(false);
    		countryUseTier2MaxAmtInputText.setDisabled(false);
    		countryUseTier2MaxAmtInputText.setReadonly(false);
    		
    		countryUseTier2SetToMaximumCheckBind.setDisabled(false);
    		
    		countryUseTier2TaxEntireAmountCheckBind.setDisabled(false);
		  	countryUseTier2TaxEntireAmountCheckBind.setReadonly(false);
 
    		//Country Tier 3
    		countryUseTier3RateInputText.setDisabled(false);
    		countryUseTier3RateInputText.setReadonly(false);
    		countryUseTier3SetamtInputText.setDisabled(false);
    		countryUseTier3SetamtInputText.setReadonly(false);
	      
    		//Use Tier 1 Set to Maximum
    		if (countryUseTier1SetToMaximumCheck == true) {
    			countryUseTier1MaxAmtInputText.setDisabled(true);
    			countryUseTier1MaxAmtInputText.setReadonly(true);
    			countryUseTier2MinAmtInputText.setDisabled(false);
    			countryUseTier2MinAmtInputText.setReadonly(false);
    		}
    		else {
    			countryUseTier1MaxAmtInputText.setDisabled(false);
    			countryUseTier1MaxAmtInputText.setReadonly(false);
	        	
    			countryUseTier2MinAmtInputText.setDisabled(true);
    			countryUseTier2MinAmtInputText.setReadonly(true);
    		}
	      
    		//Use Tier 2 Set to Maximum
    		if (countryUseTier2SetToMaximumCheck == true) {
    			countryUseTier2MaxAmtInputText.setDisabled(true);
    			countryUseTier2MaxAmtInputText.setReadonly(true);
	          
    			//Country Tier 3
    			countryUseTier3RateInputText.setDisabled(true);
    			countryUseTier3RateInputText.setReadonly(true);
    			countryUseTier3SetamtInputText.setDisabled(true);
    			countryUseTier3SetamtInputText.setReadonly(true);

    			countryUseTier3TaxEntireAmountCheckBind.setDisabled(true);
    			countryUseTier3TaxEntireAmountCheckBind.setReadonly(true);
    		} 
    		else {
    			countryUseTier2MaxAmtInputText.setDisabled(false);
    			countryUseTier2MaxAmtInputText.setReadonly(false);
	          
    			//Country Tier 3
    			countryUseTier3RateInputText.setDisabled(false);
    			countryUseTier3RateInputText.setReadonly(false);
    			countryUseTier3SetamtInputText.setDisabled(false);
    			countryUseTier3SetamtInputText.setReadonly(false);
		      
    			countryUseTier3TaxEntireAmountCheckBind.setDisabled(false);
    			countryUseTier3TaxEntireAmountCheckBind.setReadonly(false);
    		}
    		
    		countryUseMaxtaxAmtInputText.setDisabled(false);
    		countryUseMaxtaxAmtInputText.setReadonly(false);
    	}
	  
    	countrySameAsSalesRatesCheck = flag;
	  
    	recalCombinedRates(null);
    }

	public void countryUseTier2SetToMaximumFlagChangeListener(ActionEvent e) throws AbortProcessingException {
		HtmlSelectBooleanCheckbox chk = (HtmlSelectBooleanCheckbox) ((HtmlAjaxSupport) e.getComponent()).getParent();		
		Boolean flag = (Boolean)chk.getValue();	

    	if (flag == true) {
    		countryUseTier2MaxAmtInputText.setValue(JurisdictionTaxrate.MAX_STRING);
    		countryUseTier2MaxAmtInputText.setDisabled(flag);
      
    		countryUseTier3RateInputText.setValue("0");
    		countryUseTier3RateInputText.setDisabled(flag);
    		countryUseTier3RateInputText.setReadonly(flag);
    		countryUseTier3SetamtInputText.setValue("0");
    		countryUseTier3SetamtInputText.setDisabled(flag);
    		countryUseTier3SetamtInputText.setReadonly(flag);
      	
    		countryUseTier3TaxEntireAmountCheck = false;
    		countryUseTier3TaxEntireAmountCheckBind.setSelected(false);
    		countryUseTier3TaxEntireAmountCheckBind.setDisabled(flag);
    		countryUseTier3TaxEntireAmountCheckBind.setReadonly(flag);

    		recalCombinedRates(null);
    	} 
    	else {
    		countryUseTier2MaxAmtInputText.setValue("0");
    		countryUseTier2MaxAmtInputText.setDisabled(flag);
      
    		countryUseTier3RateInputText.setDisabled(flag);
    		countryUseTier3RateInputText.setReadonly(flag);
    		countryUseTier3SetamtInputText.setDisabled(flag);
    		countryUseTier3SetamtInputText.setReadonly(flag);	
    		countryUseTier3TaxEntireAmountCheckBind.setDisabled(flag);
    		countryUseTier3TaxEntireAmountCheckBind.setReadonly(flag);
      
    		recalCombinedRates(null);
    	}
    }
    //end of country
    
    //start of state
	public void stateSalesTier1RateInputTextChangeListener(ValueChangeEvent vce) {	  
  		recalCombinedRates(null);
  	}

  	public void stateSalesTier1MaxAmtChangeListener(ValueChangeEvent vce) {
  		try {
  			String newValue = vce.getNewValue().toString(); 	  
  			if(stateSameAsSalesRatesCheck){
  				stateUseTier1MaxAmtInputText.setValue(newValue);
  			}
    	  
  			if (newValue.length() > 0) {
  				Long amount = null;
  				try {
  					amount = Long.parseLong(newValue);
  				}
  				catch (Exception e) {}

  				if (!new Long(JurisdictionTaxrate.MAX_AMOUNT).equals(amount)) {
  					stateSalesTier2MinAmtInputText.setValue(amount);
  					stateSalesTier2MinAmtInputText.setDisabled(true);
  					AddToMap(MODE_EDIT, "STATETIER2MINAMOUNT", PARAM_DISABLED, VALUE_TRUE);
  				}
  			}
  			else {
  				stateSalesTier2MinAmtInputText.setDisabled(false);
  				stateSalesTier2MinAmtInputText.setReadonly(false);
  				stateSalesTier2MinAmtInputText.setValue(""+editTaxRate.getStateSalesTier2MinAmt());

  				AddToMap(MODE_EDIT, "STATETIER2MINAMOUNT", PARAM_DISABLED, VALUE_FALSE);
  			}
  		}
  		catch (NumberFormatException nfe) {
  		}
  	}
  
    public void stateSalesTier1SetToMaximumFlagChangeListener(ActionEvent e) throws AbortProcessingException {
    		HtmlSelectBooleanCheckbox chk = (HtmlSelectBooleanCheckbox) ((HtmlAjaxSupport) e.getComponent()).getParent();		
    		Boolean flag = (Boolean)chk.getValue();	
    		System.out.println("stateSalesTier1SetToMaximumFlagChangeListener : " + flag);
	    if (flag == true) {	
	        stateSalesTier1MaxAmtInputText.setValue(JurisdictionTaxrate.MAX_STRING);
		    stateSalesTier1MaxAmtInputText.setDisabled(true);
		    stateSalesTier1MaxAmtInputText.setReadonly(true);
		    stateSalesTier2MinAmtInputText.setValue("0");
		    stateSalesTier2MinAmtInputText.setDisabled(false);
		    stateSalesTier2MinAmtInputText.setReadonly(false);
	    } 
	    else {
	        stateSalesTier1MaxAmtInputText.setValue("0");
	        stateSalesTier1MaxAmtInputText.setDisabled(false);
	        stateSalesTier1MaxAmtInputText.setReadonly(false);
	    	
	        stateSalesTier2MinAmtInputText.setDisabled(true);
	        stateSalesTier2MinAmtInputText.setReadonly(true);
	        stateSalesTier2MinAmtInputText.setValue("0");
	    }
	    
	    stateSalesTier1SetToMaximumCheck = flag;
	    stateSalesTier1SetToMaximumCheckBind.setSelected(flag);	  
    }

	public void stateSalesTier2SetToMaximumFlagChangeListener(ActionEvent e) throws AbortProcessingException {
		HtmlSelectBooleanCheckbox chk = (HtmlSelectBooleanCheckbox) ((HtmlAjaxSupport) e.getComponent()).getParent();		
		Boolean flag = (Boolean)chk.getValue();	
		System.out.println("stateSalesTier2SetToMaximumFlagChangeListener : " + flag);

    	if (flag == true) {
    		stateSalesTier2MaxAmtInputText.setValue(JurisdictionTaxrate.MAX_STRING);
    		stateSalesTier2MaxAmtInputText.setDisabled(flag);	
    		
    		stateSalesTier3RateInputText.setValue("0");
  	      	stateSalesTier3RateInputText.setDisabled(flag);
  	      	stateSalesTier3RateInputText.setReadonly(flag);
  	      	stateSalesTier3SetamtInputText.setValue("0");
  	      	stateSalesTier3SetamtInputText.setDisabled(flag);
  	      	stateSalesTier3SetamtInputText.setReadonly(flag);
  	      	
  	      	stateSalesTier3TaxEntireAmountCheck = false;
  	      	stateSalesTier3TaxEntireAmountCheckBind.setSelected(false);
  	      	stateSalesTier3TaxEntireAmountCheckBind.setDisabled(flag);
  	      	stateSalesTier3TaxEntireAmountCheckBind.setReadonly(flag);
      
    		recalCombinedRates(null);
    	} 
    	else {
    		stateSalesTier2MaxAmtInputText.setValue("0");
    		stateSalesTier2MaxAmtInputText.setDisabled(flag);
    		
    		stateSalesTier3RateInputText.setDisabled(flag);
    	    stateSalesTier3RateInputText.setReadonly(flag);
    	    stateSalesTier3SetamtInputText.setDisabled(flag);
    	    stateSalesTier3SetamtInputText.setReadonly(flag);	
    	    stateSalesTier3TaxEntireAmountCheckBind.setDisabled(flag);
    	    stateSalesTier3TaxEntireAmountCheckBind.setReadonly(flag);

    		recalCombinedRates(null);
    	}
    }

	public void stateSalesTier2TaxEntireAmountCheckChangeListener(ActionEvent e) throws AbortProcessingException {
		HtmlSelectBooleanCheckbox chk = (HtmlSelectBooleanCheckbox) ((HtmlAjaxSupport) e.getComponent()).getParent();		
		Boolean flag = (Boolean)chk.getValue();	
		System.out.println("stateSalesTier2TaxEntireAmountCheckChangeListener : " + flag);
      
  	  	stateSalesTier2TaxEntireAmountCheck = flag;
  	  	stateSalesTier2TaxEntireAmountCheckBind.setSelected(flag);
  	  
  	  	if(stateSameAsSalesRatesCheck){
  	  		stateUseTier2TaxEntireAmountCheck = flag;
  	  		stateUseTier2TaxEntireAmountCheckBind.setSelected(flag);
		}
    }
  
	public void stateSalesTier3TaxEntireAmountCheckChangeListener(ActionEvent e) throws AbortProcessingException {
		HtmlSelectBooleanCheckbox chk = (HtmlSelectBooleanCheckbox) ((HtmlAjaxSupport) e.getComponent()).getParent();		
		Boolean flag = (Boolean)chk.getValue();	

  	  	stateSalesTier3TaxEntireAmountCheck = flag;
  	  	stateSalesTier3TaxEntireAmountCheckBind.setSelected(flag);
  	  	
  	  	if(stateSameAsSalesRatesCheck){
  	  		stateUseTier3TaxEntireAmountCheck = flag;
  	  		stateUseTier3TaxEntireAmountCheckBind.setSelected(flag);
  	  	}
    }
    
	public void stateUseTier2TaxEntireAmountCheckChangeListener(ActionEvent e) throws AbortProcessingException {
		HtmlSelectBooleanCheckbox chk = (HtmlSelectBooleanCheckbox) ((HtmlAjaxSupport) e.getComponent()).getParent();		
		Boolean flag = (Boolean)chk.getValue();	

  	  	stateUseTier2TaxEntireAmountCheck = flag;
  	  	stateUseTier2TaxEntireAmountCheckBind.setSelected(flag);
    }
    
	public void stateUseTier3TaxEntireAmountCheckChangeListener(ActionEvent e) throws AbortProcessingException {
		HtmlSelectBooleanCheckbox chk = (HtmlSelectBooleanCheckbox) ((HtmlAjaxSupport) e.getComponent()).getParent();		
		Boolean flag = (Boolean)chk.getValue();	

  	  	stateUseTier3TaxEntireAmountCheck = flag;
  	  	stateUseTier3TaxEntireAmountCheckBind.setSelected(flag);
    }

    public void stateUseTier1MaxAmtChangeListener(ValueChangeEvent vce) {
    	try {
    		String newValue = vce.getNewValue().toString();

    		if (newValue.length() > 0) {
    			Long amount = null;
    			try {
    				amount = Long.parseLong(newValue);
    			}catch (Exception e) {}

    			if (!new Long(JurisdictionTaxrate.MAX_AMOUNT).equals(amount)) {
    				stateUseTier2MinAmtInputText.setValue(amount);
    				stateUseTier2MinAmtInputText.setDisabled(true);
    				AddToMap(MODE_EDIT, "STATETIER2MINAMOUNT", PARAM_DISABLED, VALUE_TRUE);
    			}
    		}
    		else {
    			stateUseTier2MinAmtInputText.setDisabled(false);
    			stateUseTier2MinAmtInputText.setReadonly(false);
    			stateUseTier2MinAmtInputText.setValue(""+editTaxRate.getStateUseTier2MinAmt());

    			AddToMap(MODE_EDIT, "STATETIER2MINAMOUNT", PARAM_DISABLED, VALUE_FALSE);
    		}
    	}
    	catch (NumberFormatException nfe) {
    	}
    } 
  
	public void stateUseTier1SetToMaximumFlagChangeListener(ActionEvent e) throws AbortProcessingException {
		HtmlSelectBooleanCheckbox chk = (HtmlSelectBooleanCheckbox) ((HtmlAjaxSupport) e.getComponent()).getParent();		
		Boolean flag = (Boolean)chk.getValue();	
		System.out.println("stateUseTier1SetToMaximumFlagChangeListener : " + flag);

    	if (flag == true) {
    		stateUseTier1MaxAmtInputText.setValue(JurisdictionTaxrate.MAX_STRING);
    		stateUseTier1MaxAmtInputText.setDisabled(true);
    		stateUseTier1MaxAmtInputText.setReadonly(true);
    		stateUseTier2MinAmtInputText.setValue("0");
    		stateUseTier2MinAmtInputText.setDisabled(false);
    		stateUseTier2MinAmtInputText.setReadonly(false);
    	} 
    	else {
    		stateUseTier1MaxAmtInputText.setValue("0");
    		stateUseTier1MaxAmtInputText.setDisabled(false);
    		stateUseTier1MaxAmtInputText.setReadonly(false);
    	
    		stateUseTier2MinAmtInputText.setDisabled(true);
    		stateUseTier2MinAmtInputText.setReadonly(true);
    		stateUseTier2MinAmtInputText.setValue("0");
    	}
    
    	stateUseTier1SetToMaximumCheck = flag;
    	stateUseTier1SetToMaximumCheckBind.setSelected(flag);
    }
  
    public void stateUseTier1RateInputTextChangeListener(ValueChangeEvent vce) {	  
	    recalCombinedRates(null);
    }

	public void stateSameAsSalesRatesChangeListener(ActionEvent e) throws AbortProcessingException {
		HtmlSelectBooleanCheckbox chk = (HtmlSelectBooleanCheckbox) ((HtmlAjaxSupport) e.getComponent()).getParent();		
		Boolean flag = (Boolean)chk.getValue();	
    	
    	stateSameAsSalesRatesCheck = flag;
    	stateSameAsSalesRatesCheckBind.setSelected(flag);
	  	
    	if (flag == true) {
    		//State Tier 1	
    		stateUseTier1RateInputText.setDisabled(true);
		  	stateUseTier1RateInputText.setReadonly(true);
		  
		  	stateUseTier1SetamtInputText.setDisabled(true);
		  	stateUseTier1SetamtInputText.setReadonly(true);
		  
		  	stateUseTier1MaxAmtInputText.setDisabled(true);
		  	stateUseTier1MaxAmtInputText.setReadonly(true);
		  	
		  	//State Tier 2
		  	stateUseTier2MinAmtInputText.setDisabled(true);
		  	stateUseTier2MinAmtInputText.setReadonly(true);

		  	stateUseTier2RateInputText.setDisabled(true);
		  	stateUseTier2RateInputText.setReadonly(true);

		  	stateUseTier2SetamtInputText.setDisabled(true);
		  	stateUseTier2SetamtInputText.setReadonly(true);

		  	stateUseTier2MaxAmtInputText.setDisabled(true);
		  	stateUseTier2MaxAmtInputText.setReadonly(true);
 
		  	//State Tier 3
		  	stateUseTier3RateInputText.setDisabled(true);
		  	stateUseTier3RateInputText.setReadonly(true);

		  	stateUseTier3SetamtInputText.setDisabled(true);
		  	stateUseTier3SetamtInputText.setReadonly(true);

		  	stateUseTier1SetToMaximumCheckBind.setDisabled(true);
		  	
		  	stateUseTier2SetToMaximumCheckBind.setDisabled(true);
		  	
		  	stateUseTier2TaxEntireAmountCheckBind.setDisabled(true);
		  	stateUseTier2TaxEntireAmountCheckBind.setReadonly(true);

		  	stateUseTier3TaxEntireAmountCheckBind.setDisabled(true);
		  	stateUseTier3TaxEntireAmountCheckBind.setReadonly(true);
		  	
		  	stateUseMaxtaxAmtInputText.setDisabled(true);
		  	stateUseMaxtaxAmtInputText.setReadonly(true);
    	}
    	else {
    		//State Tier 1	
    		stateUseTier1RateInputText.setDisabled(false);
    		stateUseTier1RateInputText.setReadonly(false);
		  
    		stateUseTier1SetamtInputText.setDisabled(false);
    		stateUseTier1SetamtInputText.setReadonly(false);
		  
    		stateUseTier1MaxAmtInputText.setDisabled(false);
    		stateUseTier1MaxAmtInputText.setReadonly(false);
    		
    		stateUseTier1SetToMaximumCheckBind.setDisabled(false);

    		//State Tier 2
    		stateUseTier2MinAmtInputText.setDisabled(false);
    		stateUseTier2MinAmtInputText.setReadonly(false);
    		stateUseTier2RateInputText.setDisabled(false);
    		stateUseTier2RateInputText.setReadonly(false);
    		stateUseTier2SetamtInputText.setDisabled(false);
    		stateUseTier2SetamtInputText.setReadonly(false);
    		stateUseTier2MaxAmtInputText.setDisabled(false);
    		stateUseTier2MaxAmtInputText.setReadonly(false);
    		
    		stateUseTier2SetToMaximumCheckBind.setDisabled(false);
    		
    		stateUseTier2TaxEntireAmountCheckBind.setDisabled(false);
		  	stateUseTier2TaxEntireAmountCheckBind.setReadonly(false);
 
    		//State Tier 3
    		stateUseTier3RateInputText.setDisabled(false);
    		stateUseTier3RateInputText.setReadonly(false);
    		stateUseTier3SetamtInputText.setDisabled(false);
    		stateUseTier3SetamtInputText.setReadonly(false);
    		
    		stateUseMaxtaxAmtInputText.setDisabled(false);
    		stateUseMaxtaxAmtInputText.setReadonly(false);
	      
    		//Use Tier 1 Set to Maximum
    		if (stateUseTier1SetToMaximumCheck == true) {
    			stateUseTier1MaxAmtInputText.setDisabled(true);
    			stateUseTier1MaxAmtInputText.setReadonly(true);
    			stateUseTier2MinAmtInputText.setDisabled(false);
    			stateUseTier2MinAmtInputText.setReadonly(false);
    		}
    		else {
    			stateUseTier1MaxAmtInputText.setDisabled(false);
    			stateUseTier1MaxAmtInputText.setReadonly(false);
	        	
    			stateUseTier2MinAmtInputText.setDisabled(true);
    			stateUseTier2MinAmtInputText.setReadonly(true);
    		}
	      
    		//Use Tier 2 Set to Maximum
    		if (stateUseTier2SetToMaximumCheck == true) {
    			stateUseTier2MaxAmtInputText.setDisabled(true);
    			stateUseTier2MaxAmtInputText.setReadonly(true);
	          
    			//State Tier 3
    			stateUseTier3RateInputText.setDisabled(true);
    			stateUseTier3RateInputText.setReadonly(true);
    			stateUseTier3SetamtInputText.setDisabled(true);
    			stateUseTier3SetamtInputText.setReadonly(true);

    			stateUseTier3TaxEntireAmountCheckBind.setDisabled(true);
    			stateUseTier3TaxEntireAmountCheckBind.setReadonly(true);
    		} 
    		else {
    			stateUseTier2MaxAmtInputText.setDisabled(false);
    			stateUseTier2MaxAmtInputText.setReadonly(false);
	          
    			//State Tier 3
    			stateUseTier3RateInputText.setDisabled(false);
    			stateUseTier3RateInputText.setReadonly(false);
    			stateUseTier3SetamtInputText.setDisabled(false);
    			stateUseTier3SetamtInputText.setReadonly(false);
		      
    			stateUseTier3TaxEntireAmountCheckBind.setDisabled(false);
    			stateUseTier3TaxEntireAmountCheckBind.setReadonly(false);
    		}
    	}
	  
    	stateSameAsSalesRatesCheck = flag;
	  
    	recalCombinedRates(null);
    }

	public void stateUseTier2SetToMaximumFlagChangeListener(ActionEvent e) throws AbortProcessingException {
		HtmlSelectBooleanCheckbox chk = (HtmlSelectBooleanCheckbox) ((HtmlAjaxSupport) e.getComponent()).getParent();		
		Boolean flag = (Boolean)chk.getValue();	

    	if (flag == true) {
    		stateUseTier2MaxAmtInputText.setValue(JurisdictionTaxrate.MAX_STRING);
    		stateUseTier2MaxAmtInputText.setDisabled(flag);
      
    		stateUseTier3RateInputText.setValue("0");
    		stateUseTier3RateInputText.setDisabled(flag);
    		stateUseTier3RateInputText.setReadonly(flag);
    		stateUseTier3SetamtInputText.setValue("0");
    		stateUseTier3SetamtInputText.setDisabled(flag);
    		stateUseTier3SetamtInputText.setReadonly(flag);
      	
    		stateUseTier3TaxEntireAmountCheck = false;
    		stateUseTier3TaxEntireAmountCheckBind.setSelected(false);
    		stateUseTier3TaxEntireAmountCheckBind.setDisabled(flag);
    		stateUseTier3TaxEntireAmountCheckBind.setReadonly(flag);

    		recalCombinedRates(null);
    	} 
    	else {
    		stateUseTier2MaxAmtInputText.setValue("0");
    		stateUseTier2MaxAmtInputText.setDisabled(flag);
      
    		stateUseTier3RateInputText.setDisabled(flag);
    		stateUseTier3RateInputText.setReadonly(flag);
    		stateUseTier3SetamtInputText.setDisabled(flag);
    		stateUseTier3SetamtInputText.setReadonly(flag);	
    		stateUseTier3TaxEntireAmountCheckBind.setDisabled(flag);
    		stateUseTier3TaxEntireAmountCheckBind.setReadonly(flag);
      
    		recalCombinedRates(null);
    	}
    }
    //end of state
    
    //start of county
	public void countySalesTier1RateInputTextChangeListener(ValueChangeEvent vce) {	  
  		recalCombinedRates(null);
  	}

  	public void countySalesTier1MaxAmtChangeListener(ValueChangeEvent vce) {
  		try {
  			String newValue = vce.getNewValue().toString(); 	  
  			if(countySameAsSalesRatesCheck){
  				countyUseTier1MaxAmtInputText.setValue(newValue);
  			}
    	  
  			if (newValue.length() > 0) {
  				Long amount = null;
  				try {
  					amount = Long.parseLong(newValue);
  				}
  				catch (Exception e) {}

  				if (!new Long(JurisdictionTaxrate.MAX_AMOUNT).equals(amount)) {
  					countySalesTier2MinAmtInputText.setValue(amount);
  					countySalesTier2MinAmtInputText.setDisabled(true);
  					AddToMap(MODE_EDIT, "STATETIER2MINAMOUNT", PARAM_DISABLED, VALUE_TRUE);
  				}
  			}
  			else {
  				countySalesTier2MinAmtInputText.setDisabled(false);
  				countySalesTier2MinAmtInputText.setReadonly(false);
  				countySalesTier2MinAmtInputText.setValue(""+editTaxRate.getCountySalesTier2MinAmt());

  				AddToMap(MODE_EDIT, "STATETIER2MINAMOUNT", PARAM_DISABLED, VALUE_FALSE);
  			}
  		}
  		catch (NumberFormatException nfe) {
  		}
  	}
  
    public void countySalesTier1SetToMaximumFlagChangeListener(ActionEvent e) throws AbortProcessingException {
    		HtmlSelectBooleanCheckbox chk = (HtmlSelectBooleanCheckbox) ((HtmlAjaxSupport) e.getComponent()).getParent();		
    		Boolean flag = (Boolean)chk.getValue();	
    		System.out.println("countySalesTier1SetToMaximumFlagChangeListener : " + flag);
	    if (flag == true) {	
	        countySalesTier1MaxAmtInputText.setValue(JurisdictionTaxrate.MAX_STRING);
		    countySalesTier1MaxAmtInputText.setDisabled(true);
		    countySalesTier1MaxAmtInputText.setReadonly(true);
		    countySalesTier2MinAmtInputText.setValue("0");
		    countySalesTier2MinAmtInputText.setDisabled(false);
		    countySalesTier2MinAmtInputText.setReadonly(false);
	    } 
	    else {
	        countySalesTier1MaxAmtInputText.setValue("0");
	        countySalesTier1MaxAmtInputText.setDisabled(false);
	        countySalesTier1MaxAmtInputText.setReadonly(false);
	    	
	        countySalesTier2MinAmtInputText.setDisabled(true);
	        countySalesTier2MinAmtInputText.setReadonly(true);
	        countySalesTier2MinAmtInputText.setValue("0");
	    }
	    
	    countySalesTier1SetToMaximumCheck = flag;
	    countySalesTier1SetToMaximumCheckBind.setSelected(flag);	  
    }

	public void countySalesTier2SetToMaximumFlagChangeListener(ActionEvent e) throws AbortProcessingException {
		HtmlSelectBooleanCheckbox chk = (HtmlSelectBooleanCheckbox) ((HtmlAjaxSupport) e.getComponent()).getParent();		
		Boolean flag = (Boolean)chk.getValue();	
		System.out.println("countySalesTier2SetToMaximumFlagChangeListener : " + flag);

    	if (flag == true) {
    		countySalesTier2MaxAmtInputText.setValue(JurisdictionTaxrate.MAX_STRING);
    		countySalesTier2MaxAmtInputText.setDisabled(flag);	
    		
    		countySalesTier3RateInputText.setValue("0");
  	      	countySalesTier3RateInputText.setDisabled(flag);
  	      	countySalesTier3RateInputText.setReadonly(flag);
  	      	countySalesTier3SetamtInputText.setValue("0");
  	      	countySalesTier3SetamtInputText.setDisabled(flag);
  	      	countySalesTier3SetamtInputText.setReadonly(flag);
  	      	
  	      	countySalesTier3TaxEntireAmountCheck = false;
  	      	countySalesTier3TaxEntireAmountCheckBind.setSelected(false);
  	      	countySalesTier3TaxEntireAmountCheckBind.setDisabled(flag);
  	      	countySalesTier3TaxEntireAmountCheckBind.setReadonly(flag);
      
    		recalCombinedRates(null);
    	} 
    	else {
    		countySalesTier2MaxAmtInputText.setValue("0");
    		countySalesTier2MaxAmtInputText.setDisabled(flag);
    		
    		countySalesTier3RateInputText.setDisabled(flag);
    	    countySalesTier3RateInputText.setReadonly(flag);
    	    countySalesTier3SetamtInputText.setDisabled(flag);
    	    countySalesTier3SetamtInputText.setReadonly(flag);	
    	    countySalesTier3TaxEntireAmountCheckBind.setDisabled(flag);
    	    countySalesTier3TaxEntireAmountCheckBind.setReadonly(flag);

    		recalCombinedRates(null);
    	}
    }

	public void countySalesTier2TaxEntireAmountCheckChangeListener(ActionEvent e) throws AbortProcessingException {
		HtmlSelectBooleanCheckbox chk = (HtmlSelectBooleanCheckbox) ((HtmlAjaxSupport) e.getComponent()).getParent();		
		Boolean flag = (Boolean)chk.getValue();	
		System.out.println("countySalesTier2TaxEntireAmountCheckChangeListener : " + flag);
      
  	  	countySalesTier2TaxEntireAmountCheck = flag;
  	  	countySalesTier2TaxEntireAmountCheckBind.setSelected(flag);
  	  
  	  	if(countySameAsSalesRatesCheck){
  	  		countyUseTier2TaxEntireAmountCheck = flag;
  	  		countyUseTier2TaxEntireAmountCheckBind.setSelected(flag);
		}
    }
  
	public void countySalesTier3TaxEntireAmountCheckChangeListener(ActionEvent e) throws AbortProcessingException {
		HtmlSelectBooleanCheckbox chk = (HtmlSelectBooleanCheckbox) ((HtmlAjaxSupport) e.getComponent()).getParent();		
		Boolean flag = (Boolean)chk.getValue();	

  	  	countySalesTier3TaxEntireAmountCheck = flag;
  	  	countySalesTier3TaxEntireAmountCheckBind.setSelected(flag);
  	  	
  	  	if(countySameAsSalesRatesCheck){
  	  		countyUseTier3TaxEntireAmountCheck = flag;
  	  		countyUseTier3TaxEntireAmountCheckBind.setSelected(flag);
  	  	}
    }
    
	public void countyUseTier2TaxEntireAmountCheckChangeListener(ActionEvent e) throws AbortProcessingException {
		HtmlSelectBooleanCheckbox chk = (HtmlSelectBooleanCheckbox) ((HtmlAjaxSupport) e.getComponent()).getParent();		
		Boolean flag = (Boolean)chk.getValue();	

  	  	countyUseTier2TaxEntireAmountCheck = flag;
  	  	countyUseTier2TaxEntireAmountCheckBind.setSelected(flag);
    }
    
	public void countyUseTier3TaxEntireAmountCheckChangeListener(ActionEvent e) throws AbortProcessingException {
		HtmlSelectBooleanCheckbox chk = (HtmlSelectBooleanCheckbox) ((HtmlAjaxSupport) e.getComponent()).getParent();		
		Boolean flag = (Boolean)chk.getValue();	

  	  	countyUseTier3TaxEntireAmountCheck = flag;
  	  	countyUseTier3TaxEntireAmountCheckBind.setSelected(flag);
    }

    public void countyUseTier1MaxAmtChangeListener(ValueChangeEvent vce) {
    	try {
    		String newValue = vce.getNewValue().toString();

    		if (newValue.length() > 0) {
    			Long amount = null;
    			try {
    				amount = Long.parseLong(newValue);
    			}catch (Exception e) {}

    			if (!new Long(JurisdictionTaxrate.MAX_AMOUNT).equals(amount)) {
    				countyUseTier2MinAmtInputText.setValue(amount);
    				countyUseTier2MinAmtInputText.setDisabled(true);
    				AddToMap(MODE_EDIT, "STATETIER2MINAMOUNT", PARAM_DISABLED, VALUE_TRUE);
    			}
    		}
    		else {
    			countyUseTier2MinAmtInputText.setDisabled(false);
    			countyUseTier2MinAmtInputText.setReadonly(false);
    			countyUseTier2MinAmtInputText.setValue(""+editTaxRate.getCountyUseTier2MinAmt());

    			AddToMap(MODE_EDIT, "STATETIER2MINAMOUNT", PARAM_DISABLED, VALUE_FALSE);
    		}
    	}
    	catch (NumberFormatException nfe) {
    	}
    } 
  
	public void countyUseTier1SetToMaximumFlagChangeListener(ActionEvent e) throws AbortProcessingException {
		HtmlSelectBooleanCheckbox chk = (HtmlSelectBooleanCheckbox) ((HtmlAjaxSupport) e.getComponent()).getParent();		
		Boolean flag = (Boolean)chk.getValue();	
		System.out.println("countyUseTier1SetToMaximumFlagChangeListener : " + flag);

    	if (flag == true) {
    		countyUseTier1MaxAmtInputText.setValue(JurisdictionTaxrate.MAX_STRING);
    		countyUseTier1MaxAmtInputText.setDisabled(true);
    		countyUseTier1MaxAmtInputText.setReadonly(true);
    		countyUseTier2MinAmtInputText.setValue("0");
    		countyUseTier2MinAmtInputText.setDisabled(false);
    		countyUseTier2MinAmtInputText.setReadonly(false);
    	} 
    	else {
    		countyUseTier1MaxAmtInputText.setValue("0");
    		countyUseTier1MaxAmtInputText.setDisabled(false);
    		countyUseTier1MaxAmtInputText.setReadonly(false);
    	
    		countyUseTier2MinAmtInputText.setDisabled(true);
    		countyUseTier2MinAmtInputText.setReadonly(true);
    		countyUseTier2MinAmtInputText.setValue("0");
    	}
    
    	countyUseTier1SetToMaximumCheck = flag;
    	countyUseTier1SetToMaximumCheckBind.setSelected(flag);
    }
  
    public void countyUseTier1RateInputTextChangeListener(ValueChangeEvent vce) {	  
	    recalCombinedRates(null);
    }

	public void countySameAsSalesRatesChangeListener(ActionEvent e) throws AbortProcessingException {
		HtmlSelectBooleanCheckbox chk = (HtmlSelectBooleanCheckbox) ((HtmlAjaxSupport) e.getComponent()).getParent();		
		Boolean flag = (Boolean)chk.getValue();	
    	
    	countySameAsSalesRatesCheck = flag;
    	countySameAsSalesRatesCheckBind.setSelected(flag);
	  	
    	if (flag == true) {
    		//County Tier 1	
    		countyUseTier1RateInputText.setDisabled(true);
		  	countyUseTier1RateInputText.setReadonly(true);
		  
		  	countyUseTier1SetamtInputText.setDisabled(true);
		  	countyUseTier1SetamtInputText.setReadonly(true);
		  
		  	countyUseTier1MaxAmtInputText.setDisabled(true);
		  	countyUseTier1MaxAmtInputText.setReadonly(true);
		  	
		  	//County Tier 2
		  	countyUseTier2MinAmtInputText.setDisabled(true);
		  	countyUseTier2MinAmtInputText.setReadonly(true);

		  	countyUseTier2RateInputText.setDisabled(true);
		  	countyUseTier2RateInputText.setReadonly(true);

		  	countyUseTier2SetamtInputText.setDisabled(true);
		  	countyUseTier2SetamtInputText.setReadonly(true);

		  	countyUseTier2MaxAmtInputText.setDisabled(true);
		  	countyUseTier2MaxAmtInputText.setReadonly(true);
 
		  	//County Tier 3
		  	countyUseTier3RateInputText.setDisabled(true);
		  	countyUseTier3RateInputText.setReadonly(true);

		  	countyUseTier3SetamtInputText.setDisabled(true);
		  	countyUseTier3SetamtInputText.setReadonly(true);

		  	countyUseTier1SetToMaximumCheckBind.setDisabled(true);
		  	
		  	countyUseTier2SetToMaximumCheckBind.setDisabled(true);
		  	
		  	countyUseTier2TaxEntireAmountCheckBind.setDisabled(true);
		  	countyUseTier2TaxEntireAmountCheckBind.setReadonly(true);

		  	countyUseTier3TaxEntireAmountCheckBind.setDisabled(true);
		  	countyUseTier3TaxEntireAmountCheckBind.setReadonly(true);
		  	
		  	countyUseMaxtaxAmtInputText.setDisabled(true);
		  	countyUseMaxtaxAmtInputText.setReadonly(true);
    	}
    	else {
    		//County Tier 1	
    		countyUseTier1RateInputText.setDisabled(false);
    		countyUseTier1RateInputText.setReadonly(false);
		  
    		countyUseTier1SetamtInputText.setDisabled(false);
    		countyUseTier1SetamtInputText.setReadonly(false);
		  
    		countyUseTier1MaxAmtInputText.setDisabled(false);
    		countyUseTier1MaxAmtInputText.setReadonly(false);
    		
    		countyUseTier1SetToMaximumCheckBind.setDisabled(false);

    		//County Tier 2
    		countyUseTier2MinAmtInputText.setDisabled(false);
    		countyUseTier2MinAmtInputText.setReadonly(false);
    		countyUseTier2RateInputText.setDisabled(false);
    		countyUseTier2RateInputText.setReadonly(false);
    		countyUseTier2SetamtInputText.setDisabled(false);
    		countyUseTier2SetamtInputText.setReadonly(false);
    		countyUseTier2MaxAmtInputText.setDisabled(false);
    		countyUseTier2MaxAmtInputText.setReadonly(false);
    		
    		countyUseTier2SetToMaximumCheckBind.setDisabled(false);
    		
    		countyUseTier2TaxEntireAmountCheckBind.setDisabled(false);
		  	countyUseTier2TaxEntireAmountCheckBind.setReadonly(false);
 
    		//County Tier 3
    		countyUseTier3RateInputText.setDisabled(false);
    		countyUseTier3RateInputText.setReadonly(false);
    		countyUseTier3SetamtInputText.setDisabled(false);
    		countyUseTier3SetamtInputText.setReadonly(false);
    		
    		countyUseMaxtaxAmtInputText.setDisabled(false);
    		countyUseMaxtaxAmtInputText.setReadonly(false);
	      
    		//Use Tier 1 Set to Maximum
    		if (countyUseTier1SetToMaximumCheck == true) {
    			countyUseTier1MaxAmtInputText.setDisabled(true);
    			countyUseTier1MaxAmtInputText.setReadonly(true);
    			countyUseTier2MinAmtInputText.setDisabled(false);
    			countyUseTier2MinAmtInputText.setReadonly(false);
    		}
    		else {
    			countyUseTier1MaxAmtInputText.setDisabled(false);
    			countyUseTier1MaxAmtInputText.setReadonly(false);
	        	
    			countyUseTier2MinAmtInputText.setDisabled(true);
    			countyUseTier2MinAmtInputText.setReadonly(true);
    		}
	      
    		//Use Tier 2 Set to Maximum
    		if (countyUseTier2SetToMaximumCheck == true) {
    			countyUseTier2MaxAmtInputText.setDisabled(true);
    			countyUseTier2MaxAmtInputText.setReadonly(true);
	          
    			//County Tier 3
    			countyUseTier3RateInputText.setDisabled(true);
    			countyUseTier3RateInputText.setReadonly(true);
    			countyUseTier3SetamtInputText.setDisabled(true);
    			countyUseTier3SetamtInputText.setReadonly(true);

    			countyUseTier3TaxEntireAmountCheckBind.setDisabled(true);
    			countyUseTier3TaxEntireAmountCheckBind.setReadonly(true);
    		} 
    		else {
    			countyUseTier2MaxAmtInputText.setDisabled(false);
    			countyUseTier2MaxAmtInputText.setReadonly(false);
	          
    			//County Tier 3
    			countyUseTier3RateInputText.setDisabled(false);
    			countyUseTier3RateInputText.setReadonly(false);
    			countyUseTier3SetamtInputText.setDisabled(false);
    			countyUseTier3SetamtInputText.setReadonly(false);
		      
    			countyUseTier3TaxEntireAmountCheckBind.setDisabled(false);
    			countyUseTier3TaxEntireAmountCheckBind.setReadonly(false);
    		}
    	}
	  
    	countySameAsSalesRatesCheck = flag;
	  
    	recalCombinedRates(null);
    }

	public void countyUseTier2SetToMaximumFlagChangeListener(ActionEvent e) throws AbortProcessingException {
		HtmlSelectBooleanCheckbox chk = (HtmlSelectBooleanCheckbox) ((HtmlAjaxSupport) e.getComponent()).getParent();		
		Boolean flag = (Boolean)chk.getValue();	

    	if (flag == true) {
    		countyUseTier2MaxAmtInputText.setValue(JurisdictionTaxrate.MAX_STRING);
    		countyUseTier2MaxAmtInputText.setDisabled(flag);
      
    		countyUseTier3RateInputText.setValue("0");
    		countyUseTier3RateInputText.setDisabled(flag);
    		countyUseTier3RateInputText.setReadonly(flag);
    		countyUseTier3SetamtInputText.setValue("0");
    		countyUseTier3SetamtInputText.setDisabled(flag);
    		countyUseTier3SetamtInputText.setReadonly(flag);
      	
    		countyUseTier3TaxEntireAmountCheck = false;
    		countyUseTier3TaxEntireAmountCheckBind.setSelected(false);
    		countyUseTier3TaxEntireAmountCheckBind.setDisabled(flag);
    		countyUseTier3TaxEntireAmountCheckBind.setReadonly(flag);

    		recalCombinedRates(null);
    	} 
    	else {
    		countyUseTier2MaxAmtInputText.setValue("0");
    		countyUseTier2MaxAmtInputText.setDisabled(flag);
      
    		countyUseTier3RateInputText.setDisabled(flag);
    		countyUseTier3RateInputText.setReadonly(flag);
    		countyUseTier3SetamtInputText.setDisabled(flag);
    		countyUseTier3SetamtInputText.setReadonly(flag);	
    		countyUseTier3TaxEntireAmountCheckBind.setDisabled(flag);
    		countyUseTier3TaxEntireAmountCheckBind.setReadonly(flag);
      
    		recalCombinedRates(null);
    	}
    }
    //end of county
    
	//start of city
	public void citySalesTier1RateInputTextChangeListener(ValueChangeEvent vce) {	  
  		recalCombinedRates(null);
  	}

  	public void citySalesTier1MaxAmtChangeListener(ValueChangeEvent vce) {
  		try {
  			String newValue = vce.getNewValue().toString(); 	  
  			if(citySameAsSalesRatesCheck){
  				cityUseTier1MaxAmtInputText.setValue(newValue);
  			}
    	  
  			if (newValue.length() > 0) {
  				Long amount = null;
  				try {
  					amount = Long.parseLong(newValue);
  				}
  				catch (Exception e) {}

  				if (!new Long(JurisdictionTaxrate.MAX_AMOUNT).equals(amount)) {
  					citySalesTier2MinAmtInputText.setValue(amount);
  					citySalesTier2MinAmtInputText.setDisabled(true);
  					AddToMap(MODE_EDIT, "STATETIER2MINAMOUNT", PARAM_DISABLED, VALUE_TRUE);
  				}
  			}
  			else {
  				citySalesTier2MinAmtInputText.setDisabled(false);
  				citySalesTier2MinAmtInputText.setReadonly(false);
  				citySalesTier2MinAmtInputText.setValue(""+editTaxRate.getCitySalesTier2MinAmt());

  				AddToMap(MODE_EDIT, "STATETIER2MINAMOUNT", PARAM_DISABLED, VALUE_FALSE);
  			}
  		}
  		catch (NumberFormatException nfe) {
  		}
  	}
  
    public void citySalesTier1SetToMaximumFlagChangeListener(ActionEvent e) throws AbortProcessingException {
    		HtmlSelectBooleanCheckbox chk = (HtmlSelectBooleanCheckbox) ((HtmlAjaxSupport) e.getComponent()).getParent();		
    		Boolean flag = (Boolean)chk.getValue();	
    		System.out.println("citySalesTier1SetToMaximumFlagChangeListener : " + flag);
	    if (flag == true) {	
	        citySalesTier1MaxAmtInputText.setValue(JurisdictionTaxrate.MAX_STRING);
		    citySalesTier1MaxAmtInputText.setDisabled(true);
		    citySalesTier1MaxAmtInputText.setReadonly(true);
		    citySalesTier2MinAmtInputText.setValue("0");
		    citySalesTier2MinAmtInputText.setDisabled(false);
		    citySalesTier2MinAmtInputText.setReadonly(false);
	    } 
	    else {
	        citySalesTier1MaxAmtInputText.setValue("0");
	        citySalesTier1MaxAmtInputText.setDisabled(false);
	        citySalesTier1MaxAmtInputText.setReadonly(false);
	    	
	        citySalesTier2MinAmtInputText.setDisabled(true);
	        citySalesTier2MinAmtInputText.setReadonly(true);
	        citySalesTier2MinAmtInputText.setValue("0");
	    }
	    
	    citySalesTier1SetToMaximumCheck = flag;
	    citySalesTier1SetToMaximumCheckBind.setSelected(flag);	  
    }

	public void citySalesTier2SetToMaximumFlagChangeListener(ActionEvent e) throws AbortProcessingException {
		HtmlSelectBooleanCheckbox chk = (HtmlSelectBooleanCheckbox) ((HtmlAjaxSupport) e.getComponent()).getParent();		
		Boolean flag = (Boolean)chk.getValue();	
		System.out.println("citySalesTier2SetToMaximumFlagChangeListener : " + flag);

    	if (flag == true) {
    		citySalesTier2MaxAmtInputText.setValue(JurisdictionTaxrate.MAX_STRING);
    		citySalesTier2MaxAmtInputText.setDisabled(flag);	
    		
    		citySalesTier3RateInputText.setValue("0");
  	      	citySalesTier3RateInputText.setDisabled(flag);
  	      	citySalesTier3RateInputText.setReadonly(flag);
  	      	citySalesTier3SetamtInputText.setValue("0");
  	      	citySalesTier3SetamtInputText.setDisabled(flag);
  	      	citySalesTier3SetamtInputText.setReadonly(flag);
  	      	
  	      	citySalesTier3TaxEntireAmountCheck = false;
  	      	citySalesTier3TaxEntireAmountCheckBind.setSelected(false);
  	      	citySalesTier3TaxEntireAmountCheckBind.setDisabled(flag);
  	      	citySalesTier3TaxEntireAmountCheckBind.setReadonly(flag);
      
    		recalCombinedRates(null);
    	} 
    	else {
    		citySalesTier2MaxAmtInputText.setValue("0");
    		citySalesTier2MaxAmtInputText.setDisabled(flag);
    		
    		citySalesTier3RateInputText.setDisabled(flag);
    	    citySalesTier3RateInputText.setReadonly(flag);
    	    citySalesTier3SetamtInputText.setDisabled(flag);
    	    citySalesTier3SetamtInputText.setReadonly(flag);	
    	    citySalesTier3TaxEntireAmountCheckBind.setDisabled(flag);
    	    citySalesTier3TaxEntireAmountCheckBind.setReadonly(flag);

    		recalCombinedRates(null);
    	}
    }

	public void citySalesTier2TaxEntireAmountCheckChangeListener(ActionEvent e) throws AbortProcessingException {
		HtmlSelectBooleanCheckbox chk = (HtmlSelectBooleanCheckbox) ((HtmlAjaxSupport) e.getComponent()).getParent();		
		Boolean flag = (Boolean)chk.getValue();	
		System.out.println("citySalesTier2TaxEntireAmountCheckChangeListener : " + flag);
      
  	  	citySalesTier2TaxEntireAmountCheck = flag;
  	  	citySalesTier2TaxEntireAmountCheckBind.setSelected(flag);
  	  
  	  	if(citySameAsSalesRatesCheck){
  	  		cityUseTier2TaxEntireAmountCheck = flag;
  	  		cityUseTier2TaxEntireAmountCheckBind.setSelected(flag);
		}
    }
  
	public void citySalesTier3TaxEntireAmountCheckChangeListener(ActionEvent e) throws AbortProcessingException {
		HtmlSelectBooleanCheckbox chk = (HtmlSelectBooleanCheckbox) ((HtmlAjaxSupport) e.getComponent()).getParent();		
		Boolean flag = (Boolean)chk.getValue();	

  	  	citySalesTier3TaxEntireAmountCheck = flag;
  	  	citySalesTier3TaxEntireAmountCheckBind.setSelected(flag);
  	  	
  	  	if(citySameAsSalesRatesCheck){
  	  		cityUseTier3TaxEntireAmountCheck = flag;
  	  		cityUseTier3TaxEntireAmountCheckBind.setSelected(flag);
  	  	}
    }
    
	public void cityUseTier2TaxEntireAmountCheckChangeListener(ActionEvent e) throws AbortProcessingException {
		HtmlSelectBooleanCheckbox chk = (HtmlSelectBooleanCheckbox) ((HtmlAjaxSupport) e.getComponent()).getParent();		
		Boolean flag = (Boolean)chk.getValue();	

  	  	cityUseTier2TaxEntireAmountCheck = flag;
  	  	cityUseTier2TaxEntireAmountCheckBind.setSelected(flag);
    }
    
	public void cityUseTier3TaxEntireAmountCheckChangeListener(ActionEvent e) throws AbortProcessingException {
		HtmlSelectBooleanCheckbox chk = (HtmlSelectBooleanCheckbox) ((HtmlAjaxSupport) e.getComponent()).getParent();		
		Boolean flag = (Boolean)chk.getValue();	

  	  	cityUseTier3TaxEntireAmountCheck = flag;
  	  	cityUseTier3TaxEntireAmountCheckBind.setSelected(flag);
    }

    public void cityUseTier1MaxAmtChangeListener(ValueChangeEvent vce) {
    	try {
    		String newValue = vce.getNewValue().toString();

    		if (newValue.length() > 0) {
    			Long amount = null;
    			try {
    				amount = Long.parseLong(newValue);
    			}catch (Exception e) {}

    			if (!new Long(JurisdictionTaxrate.MAX_AMOUNT).equals(amount)) {
    				cityUseTier2MinAmtInputText.setValue(amount);
    				cityUseTier2MinAmtInputText.setDisabled(true);
    				AddToMap(MODE_EDIT, "STATETIER2MINAMOUNT", PARAM_DISABLED, VALUE_TRUE);
    			}
    		}
    		else {
    			cityUseTier2MinAmtInputText.setDisabled(false);
    			cityUseTier2MinAmtInputText.setReadonly(false);
    			cityUseTier2MinAmtInputText.setValue(""+editTaxRate.getCityUseTier2MinAmt());

    			AddToMap(MODE_EDIT, "STATETIER2MINAMOUNT", PARAM_DISABLED, VALUE_FALSE);
    		}
    	}
    	catch (NumberFormatException nfe) {
    	}
    } 
  
	public void cityUseTier1SetToMaximumFlagChangeListener(ActionEvent e) throws AbortProcessingException {
		HtmlSelectBooleanCheckbox chk = (HtmlSelectBooleanCheckbox) ((HtmlAjaxSupport) e.getComponent()).getParent();		
		Boolean flag = (Boolean)chk.getValue();	
		System.out.println("cityUseTier1SetToMaximumFlagChangeListener : " + flag);

    	if (flag == true) {
    		cityUseTier1MaxAmtInputText.setValue(JurisdictionTaxrate.MAX_STRING);
    		cityUseTier1MaxAmtInputText.setDisabled(true);
    		cityUseTier1MaxAmtInputText.setReadonly(true);
    		cityUseTier2MinAmtInputText.setValue("0");
    		cityUseTier2MinAmtInputText.setDisabled(false);
    		cityUseTier2MinAmtInputText.setReadonly(false);
    	} 
    	else {
    		cityUseTier1MaxAmtInputText.setValue("0");
    		cityUseTier1MaxAmtInputText.setDisabled(false);
    		cityUseTier1MaxAmtInputText.setReadonly(false);
    	
    		cityUseTier2MinAmtInputText.setDisabled(true);
    		cityUseTier2MinAmtInputText.setReadonly(true);
    		cityUseTier2MinAmtInputText.setValue("0");
    	}
    
    	cityUseTier1SetToMaximumCheck = flag;
    	cityUseTier1SetToMaximumCheckBind.setSelected(flag);
    }
  
    public void cityUseTier1RateInputTextChangeListener(ValueChangeEvent vce) {	  
	    recalCombinedRates(null);
    }

	public void citySameAsSalesRatesChangeListener(ActionEvent e) throws AbortProcessingException {
		HtmlSelectBooleanCheckbox chk = (HtmlSelectBooleanCheckbox) ((HtmlAjaxSupport) e.getComponent()).getParent();		
		Boolean flag = (Boolean)chk.getValue();	
    	
    	citySameAsSalesRatesCheck = flag;
    	citySameAsSalesRatesCheckBind.setSelected(flag);
	  	
    	if (flag == true) {
    		//City Tier 1	
    		cityUseTier1RateInputText.setDisabled(true);
		  	cityUseTier1RateInputText.setReadonly(true);
		  
		  	cityUseTier1SetamtInputText.setDisabled(true);
		  	cityUseTier1SetamtInputText.setReadonly(true);
		  
		  	cityUseTier1MaxAmtInputText.setDisabled(true);
		  	cityUseTier1MaxAmtInputText.setReadonly(true);
		  	
		  	//City Tier 2
		  	cityUseTier2MinAmtInputText.setDisabled(true);
		  	cityUseTier2MinAmtInputText.setReadonly(true);

		  	cityUseTier2RateInputText.setDisabled(true);
		  	cityUseTier2RateInputText.setReadonly(true);

		  	cityUseTier2SetamtInputText.setDisabled(true);
		  	cityUseTier2SetamtInputText.setReadonly(true);

		  	cityUseTier2MaxAmtInputText.setDisabled(true);
		  	cityUseTier2MaxAmtInputText.setReadonly(true);
 
		  	//City Tier 3
		  	cityUseTier3RateInputText.setDisabled(true);
		  	cityUseTier3RateInputText.setReadonly(true);

		  	cityUseTier3SetamtInputText.setDisabled(true);
		  	cityUseTier3SetamtInputText.setReadonly(true);

		  	cityUseTier1SetToMaximumCheckBind.setDisabled(true);
		  	
		  	cityUseTier2SetToMaximumCheckBind.setDisabled(true);
		  	
		  	cityUseTier2TaxEntireAmountCheckBind.setDisabled(true);
		  	cityUseTier2TaxEntireAmountCheckBind.setReadonly(true);

		  	cityUseTier3TaxEntireAmountCheckBind.setDisabled(true);
		  	cityUseTier3TaxEntireAmountCheckBind.setReadonly(true);
		  	
		  	cityUseMaxtaxAmtInputText.setDisabled(true);
		  	cityUseMaxtaxAmtInputText.setReadonly(true);
    	}
    	else {
    		//City Tier 1	
    		cityUseTier1RateInputText.setDisabled(false);
    		cityUseTier1RateInputText.setReadonly(false);
		  
    		cityUseTier1SetamtInputText.setDisabled(false);
    		cityUseTier1SetamtInputText.setReadonly(false);
		  
    		cityUseTier1MaxAmtInputText.setDisabled(false);
    		cityUseTier1MaxAmtInputText.setReadonly(false);
    		
    		cityUseTier1SetToMaximumCheckBind.setDisabled(false);

    		//City Tier 2
    		cityUseTier2MinAmtInputText.setDisabled(false);
    		cityUseTier2MinAmtInputText.setReadonly(false);
    		cityUseTier2RateInputText.setDisabled(false);
    		cityUseTier2RateInputText.setReadonly(false);
    		cityUseTier2SetamtInputText.setDisabled(false);
    		cityUseTier2SetamtInputText.setReadonly(false);
    		cityUseTier2MaxAmtInputText.setDisabled(false);
    		cityUseTier2MaxAmtInputText.setReadonly(false);
    		
    		cityUseTier2SetToMaximumCheckBind.setDisabled(false);
    		
    		cityUseTier2TaxEntireAmountCheckBind.setDisabled(false);
		  	cityUseTier2TaxEntireAmountCheckBind.setReadonly(false);
 
    		//City Tier 3
    		cityUseTier3RateInputText.setDisabled(false);
    		cityUseTier3RateInputText.setReadonly(false);
    		cityUseTier3SetamtInputText.setDisabled(false);
    		cityUseTier3SetamtInputText.setReadonly(false);
    		
    		cityUseMaxtaxAmtInputText.setDisabled(false);
    		cityUseMaxtaxAmtInputText.setReadonly(false);
	      
    		//Use Tier 1 Set to Maximum
    		if (cityUseTier1SetToMaximumCheck == true) {
    			cityUseTier1MaxAmtInputText.setDisabled(true);
    			cityUseTier1MaxAmtInputText.setReadonly(true);
    			cityUseTier2MinAmtInputText.setDisabled(false);
    			cityUseTier2MinAmtInputText.setReadonly(false);
    		}
    		else {
    			cityUseTier1MaxAmtInputText.setDisabled(false);
    			cityUseTier1MaxAmtInputText.setReadonly(false);
	        	
    			cityUseTier2MinAmtInputText.setDisabled(true);
    			cityUseTier2MinAmtInputText.setReadonly(true);
    		}
	      
    		//Use Tier 2 Set to Maximum
    		if (cityUseTier2SetToMaximumCheck == true) {
    			cityUseTier2MaxAmtInputText.setDisabled(true);
    			cityUseTier2MaxAmtInputText.setReadonly(true);
	          
    			//City Tier 3
    			cityUseTier3RateInputText.setDisabled(true);
    			cityUseTier3RateInputText.setReadonly(true);
    			cityUseTier3SetamtInputText.setDisabled(true);
    			cityUseTier3SetamtInputText.setReadonly(true);

    			cityUseTier3TaxEntireAmountCheckBind.setDisabled(true);
    			cityUseTier3TaxEntireAmountCheckBind.setReadonly(true);
    		} 
    		else {
    			cityUseTier2MaxAmtInputText.setDisabled(false);
    			cityUseTier2MaxAmtInputText.setReadonly(false);
	          
    			//City Tier 3
    			cityUseTier3RateInputText.setDisabled(false);
    			cityUseTier3RateInputText.setReadonly(false);
    			cityUseTier3SetamtInputText.setDisabled(false);
    			cityUseTier3SetamtInputText.setReadonly(false);
		      
    			cityUseTier3TaxEntireAmountCheckBind.setDisabled(false);
    			cityUseTier3TaxEntireAmountCheckBind.setReadonly(false);
    		}
    	}
	  
    	citySameAsSalesRatesCheck = flag;
	  
    	recalCombinedRates(null);
    }

	public void cityUseTier2SetToMaximumFlagChangeListener(ActionEvent e) throws AbortProcessingException {
		HtmlSelectBooleanCheckbox chk = (HtmlSelectBooleanCheckbox) ((HtmlAjaxSupport) e.getComponent()).getParent();		
		Boolean flag = (Boolean)chk.getValue();	

    	if (flag == true) {
    		cityUseTier2MaxAmtInputText.setValue(JurisdictionTaxrate.MAX_STRING);
    		cityUseTier2MaxAmtInputText.setDisabled(flag);
      
    		cityUseTier3RateInputText.setValue("0");
    		cityUseTier3RateInputText.setDisabled(flag);
    		cityUseTier3RateInputText.setReadonly(flag);
    		cityUseTier3SetamtInputText.setValue("0");
    		cityUseTier3SetamtInputText.setDisabled(flag);
    		cityUseTier3SetamtInputText.setReadonly(flag);
      	
    		cityUseTier3TaxEntireAmountCheck = false;
    		cityUseTier3TaxEntireAmountCheckBind.setSelected(false);
    		cityUseTier3TaxEntireAmountCheckBind.setDisabled(flag);
    		cityUseTier3TaxEntireAmountCheckBind.setReadonly(flag);

    		recalCombinedRates(null);
    	} 
    	else {
    		cityUseTier2MaxAmtInputText.setValue("0");
    		cityUseTier2MaxAmtInputText.setDisabled(flag);
      
    		cityUseTier3RateInputText.setDisabled(flag);
    		cityUseTier3RateInputText.setReadonly(flag);
    		cityUseTier3SetamtInputText.setDisabled(flag);
    		cityUseTier3SetamtInputText.setReadonly(flag);	
    		cityUseTier3TaxEntireAmountCheckBind.setDisabled(flag);
    		cityUseTier3TaxEntireAmountCheckBind.setReadonly(flag);
      
    		recalCombinedRates(null);
    	}
    }

    public void stjSameAsSalesRatesChangeListener(ValueChangeEvent vce) {
    	Boolean flag = ((Boolean) vce.getNewValue()).booleanValue();
    	if (flag == true) {	
    		stj1UseRateInputText.setValue(stj1SalesRateInputText.getValue());
    		stj1UseRateInputText.setDisabled(true);
    		stj1UseRateInputText.setReadonly(true);
		  
    		stj1UseSetamtInputText.setValue(stj1SalesSetamtInputText.getValue());
    		stj1UseSetamtInputText.setDisabled(true);
    		stj1UseSetamtInputText.setReadonly(true);
		  
    	}
    	else {
    		stj1UseRateInputText.setDisabled(false);
    		stj1UseRateInputText.setReadonly(false);
		  
    		stj1UseSetamtInputText.setDisabled(false);
    		stj1UseSetamtInputText.setReadonly(false);
    	}
	  
    	stjSameAsSalesRatesCheck = flag;
    }
    
    public void stjRatesInputTextChangeListener(ValueChangeEvent vce) {
  		recalCombinedRates(null);
  	}
    //end of stj

  	public void vclValidateTier3(ValueChangeEvent event){
	  logger.info("Enter vclValidateTier3 "+ stateUseTier3RateInputText.getValue().toString());
	  logger.info("Enter vclValidateTier3 "+ stateSalesTier3RateInputText.getValue().toString());
	  if(!"".equalsIgnoreCase(stateUseTier3RateInputText.getValue().toString())
			  || !"0.0".equalsIgnoreCase(stateUseTier3RateInputText.getValue().toString())
			  || !"".equalsIgnoreCase(stateUseTier2RateInput.getValue().toString())
			  || !"0.0".equalsIgnoreCase(stateUseTier2RateInput.getValue().toString())
			  
			  || !"".equalsIgnoreCase(stateSalesTier3RateInputText.getValue().toString())
			  || !"0.0".equalsIgnoreCase(stateSalesTier3RateInputText.getValue().toString())
			  || !"".equalsIgnoreCase(stateSalesTier2RateInput.getValue().toString())
			  || !"0.0".equalsIgnoreCase(stateSalesTier2RateInput.getValue().toString())){
		  
		  stateMaxtaxAmount.setDisabled(true);
		  
	  }
	  if(("".equalsIgnoreCase(stateUseTier3RateInputText.getValue().toString())
			  || "0.0".equalsIgnoreCase(stateUseTier3RateInputText.getValue().toString())
			  || "0".equalsIgnoreCase(stateUseTier3RateInputText.getValue().toString()))   
			  && ("".equalsIgnoreCase(stateUseTier2RateInput.getValue().toString())
			  || "0.0".equalsIgnoreCase(stateUseTier2RateInput.getValue().toString())
			  || "0".equalsIgnoreCase(stateUseTier2RateInput.getValue().toString()))
			  
		  && ("".equalsIgnoreCase(stateSalesTier3RateInputText.getValue().toString())
				  || "0.0".equalsIgnoreCase(stateSalesTier3RateInputText.getValue().toString())
				  || "0".equalsIgnoreCase(stateSalesTier3RateInputText.getValue().toString()))   
				  && ("".equalsIgnoreCase(stateSalesTier2RateInput.getValue().toString())
				  || "0.0".equalsIgnoreCase(stateSalesTier2RateInput.getValue().toString())
				  || "0".equalsIgnoreCase(stateSalesTier2RateInput.getValue().toString()))){
		  
		  stateMaxtaxAmount.setDisabled(false);
			 stateMaxtaxAmount.setValue(0.0);
		  
	  }
  }
  
  public void vclValidateTier2(ValueChangeEvent event){
	  logger.info("Enter vclValidateTier2 "+ stateUseTier2RateInput.getValue().toString());
	  logger.info("Enter vclValidateTier2 "+ stateSalesTier2RateInput.getValue().toString());
	  if(!"".equalsIgnoreCase(stateUseTier2RateInput.getValue().toString())
			  || !"0.0".equalsIgnoreCase(stateUseTier2RateInput.getValue().toString())
			  || !"".equalsIgnoreCase(stateUseTier3RateInputText.getValue().toString())
			  || !"0.0".equalsIgnoreCase(stateUseTier3RateInputText.getValue().toString())
			  
	  		  || !"".equalsIgnoreCase(stateSalesTier2RateInput.getValue().toString())
			  || !"0.0".equalsIgnoreCase(stateSalesTier2RateInput.getValue().toString())
			  || !"".equalsIgnoreCase(stateSalesTier3RateInputText.getValue().toString())
			  || !"0.0".equalsIgnoreCase(stateSalesTier3RateInputText.getValue().toString())){
		  
		  stateMaxtaxAmount.setDisabled(true);
		  
	  }
	  if(("".equalsIgnoreCase(stateUseTier2RateInput.getValue().toString())
			  || "0.0".equalsIgnoreCase(stateUseTier2RateInput.getValue().toString())
			  || "0".equalsIgnoreCase(stateUseTier2RateInput.getValue().toString())) && (
			   "".equalsIgnoreCase(stateUseTier3RateInputText.getValue().toString())
			  || "0.0".equalsIgnoreCase(stateUseTier3RateInputText.getValue().toString())
			  || "0".equalsIgnoreCase(stateUseTier3RateInputText.getValue().toString()))
			  
	  		&& ("".equalsIgnoreCase(stateSalesTier2RateInput.getValue().toString())
	  			  || "0.0".equalsIgnoreCase(stateSalesTier2RateInput.getValue().toString())
				  || "0".equalsIgnoreCase(stateSalesTier2RateInput.getValue().toString())) && (
				   "".equalsIgnoreCase(stateSalesTier3RateInputText.getValue().toString())
				  || "0.0".equalsIgnoreCase(stateSalesTier3RateInputText.getValue().toString())
				  || "0".equalsIgnoreCase(stateSalesTier3RateInputText.getValue().toString()))){
		  
		  stateMaxtaxAmount.setDisabled(false);
			 stateMaxtaxAmount.setValue(0.0);
		  
	  }
  }

  /*
   * **************************************** The GETTER METHODS
   * ****************************************
   */

  /**
   * @return the actionMap
   */
  public Map<String, Map<String, Map<String, String>>> getActionMap() {
    return actionMap;
  }

  public CacheManager getCacheManager() {
    return cacheManager;
  }

  /**
   * @return the currentMode
   */
  public String getCurrentMode() {
    return currentMode;
  }

  /**
   * @return the editJurisdiction
   */
  public Jurisdiction getEditJurisdiction() {
    return editJurisdiction;
  }

  /**
   * @return the editTaxRate
   */
  public JurisdictionTaxrate getEditTaxRate() {
    return editTaxRate;
  }

  /**
   * @return the fieldSizeMap
   */
  public Map<String, Integer> getFieldSizeMap() {
    return fieldSizeMap;
  }

  /**
   * @return the formMessagesDataTable
   */
  public HtmlDataTable getFormMessagesDataTable() {
    return formMessagesDataTable;
  }

  /**
   * @return the inOutMenu
   */
  public HtmlSelectOneMenu getInOutMenu() {
    return inOutMenu;
  }

  public Locale getLocale() {
    return locale;
  }
  
  public List<SelectItem> getCountyLocalItems() {
	if(countyLocalItems == null) {
		countyLocalItems = cacheManager.createCountyLocalItems();
	}
    return countyLocalItems;
  }

  /**
   * @return the rateTypeCodeItems
   */
  public List<SelectItem> getRateTypeCodeItems() {
	if(rateTypeCodeItems == null) {
		rateTypeCodeItems = cacheManager.createRateTypeItems();
	}
    return rateTypeCodeItems;
  }

  /**
   * @return the rateTypeCodeMenu
   */
  public HtmlSelectOneMenu getRateTypeCodeMenu() {
    return rateTypeCodeMenu;
  }

  /**
   * @return the messagesModalPanel
   */
  public HtmlModalPanel getMessagesModalPanel() {
    return messagesModalPanel;
  }

  /**
   * @return the PARAM_DISABLED
   */
  public String getPARAM_DISABLED() {
    return PARAM_DISABLED;
  }

  public int getRowCount() {
    return rowCount;
  }

  /**
   * @return the searchcity
   */
  public String getSearchcity() {
    return searchcity;
  }

  /**
   * @return the searchclientGeocode
   */
  public String getSearchclientGeocode() {
    return searchclientGeocode;
  }

  /**
   * @return the searchcompGeocode
   */
  public String getSearchcompGeocode() {
    return searchcompGeocode;
  }

  /**
   * @return the searchcounty
   */
  public String getSearchcounty() {
    return searchcounty;
  }

  /**
   * @return the searcheffectiveDate
   */
  public Date getSearcheffectiveDate() {
    return searcheffectiveDate;
  }

  /**
   * @return the searchexpirationDate
   */
  public Date getSearchexpirationDate() {
    return searchexpirationDate;
  }

  /**
   * @return the searchgeocode
   */
  public String getSearchgeocode() {
    return searchgeocode;
  }
  
  public String getSearchstj() {
	return searchstj;
  }
  
  public void setSearchstj(String searchstj) {
	this.searchstj = searchstj;
  }
  
  public String getSearchratetype() {
	return searchratetype;
  }
  
  public void setSearchratetype(String searchratetype) {
	this.searchratetype = searchratetype;
  }

  // Search GETTERS and SETTERS
  /**
   * @return the searchjurisdictionId
   */
  public Long getSearchjurisdictionId() {
    return searchjurisdictionId;
  }

  /**
   * @return the searchjurisdictionTaxrateId
   */
  public Long getSearchjurisdictionTaxrateId() {
    return searchjurisdictionTaxrateId;
  }

  /**
   * @return the searchstate
   */
  public String getSearchstate() {
    return searchstate;
  }
  
  public String getSearchstateDetail() {
	    return searchstateDetail;
	  }

  /**
   * @return the searchzip
   */
  public String getSearchzip() {
    return searchzip;
  }
  
  public String getSearchzipplus4() {
	 return searchzipplus4;
  }

  public Jurisdiction getSelectedJurisdiction() {
    return selectedJurisdiction;
  }

  public JurisdictionTaxrate getSelectedTaxRate() {
    return selectedTaxRate;
  }

  /**
   * @return the showCancelButton
   */
  public Boolean getShowCancelButton() {
    return showCancelButton;
  }

  public HtmlSelectOneMenu getStateMenu() {
    return stateMenu;
  }
  
  public void searchCountryChanged(ValueChangeEvent e){
	  String newCountry = (String)e.getNewValue();
	  if(newCountry==null) searchCountry = "";
	  else searchCountry = newCountry;
	
	  if(stateMenuItems!=null) stateMenuItems.clear();

	  stateMenuItems = cacheManager.createStateItems(searchCountry);
	  searchstate = "";
  }
  
  public void searchCountryDetailChanged(ValueChangeEvent e){
	  String newCountry = (String)e.getNewValue();
	  if(newCountry==null)searchCountryDetail = "";
	  else searchCountryDetail = newCountry;
	  
	  if(stateMenuItemsDetail!=null) stateMenuItemsDetail.clear();
	  
	  stateMenuItemsDetail = cacheManager.createStateItems(searchCountryDetail);
	  searchstateDetail = "";
  }
  
  public List<SelectItem> getStateMenuItemsDetail() { 
	  stateMenuItemsDetail = cacheManager.createStateItems(searchCountryDetail);
	  return stateMenuItemsDetail;
  }
  
  private List<SelectItem> countryMenuItems;
  public List<SelectItem> getCountryMenuItems() {
	  if(countryMenuItems==null){
		  //MF - do not set default country
		  //searchCountry= filePreferenceBean.getUserPreferenceDTO().getUserCountry();
		  //searchCountry = "";
	  }
	  countryMenuItems = cacheManager.createCountryItems();   
	  return countryMenuItems;
  }

  public List<SelectItem> getStateMenuItems() { 
	  if (searchCountry != null) {
		  stateMenuItems = cacheManager.createStateItems(searchCountry);
		  //searchstate = "";
	   }	    
	  return stateMenuItems;
  }
  
  private List<SelectItem> countryMenuItemsDetail;
  public List<SelectItem> getCountryMenuItemsDetail() {	
	  //if(countryMenuItemsDetail==null){
	  //	  searchCountryDetail = "";
	  //}
	  countryMenuItemsDetail = cacheManager.createCountryItems();   
	  return countryMenuItemsDetail;
  }

  	public HtmlInputText getCombinedUseRate() {
  		return combinedUseRate;
  	}

  	public void setCombinedUseRate(HtmlInputText combinedUseRate) {
  		this.combinedUseRate = combinedUseRate;
  	}

  	public HtmlInputText getCombinedSalesRate() {
  		return combinedSalesRate;
  	}

  	public void setCombinedSalesRate(HtmlInputText combinedSalesRate) {
  		this.combinedSalesRate = combinedSalesRate;
  	}

  private Collection<TaxCodeStateDTO> getTaxCodeStateDTOList() {
    return cacheManager.getTaxCodeStateMap().values();
  }

  /**
   * @return the taxCodeStateService
   */
  public TaxCodeStateService getTaxCodeStateService() {
    return taxCodeStateService;
  }

  public JurisdictionService getJurisdictionService() {
    return jurisdictionService;
  }

  public List<JurisdictionTaxrate> gettaxRateList() {
    return taxRateList;
  }

  public JurisdictionTaxrateService getJurisdictionTaxrateService() {
    return taxRateService;
  }

  /**
   * @return the trShowSection
   */
  public String getTrShowSection() {
    return trShowSection;
  }

  /**
   * @return the searchcustomFlag
   */
  // public boolean isSearchcustomFlag() {
  // return searchcustomFlag;
  // }
  /**
   * @return the searchmodifiedFlag
   */
  public boolean isSearchmodifiedFlag() {
    return searchmodifiedFlag;
  }

  /*
   * **************************************** The SETTER METHODS
   * ****************************************
   */

  /**
   * @param actionMap the actionMap to set
   */
  public void setActionMap(Map<String, Map<String, Map<String, String>>> actionMap) {
    this.actionMap = actionMap;
  }

  public void setCacheManager(CacheManager cacheManager) {
    this.cacheManager = cacheManager;
  }

  /**
   * @param currentMode the currentMode to set
   */
  public void setCurrentMode(String currentMode) {
    this.currentMode = currentMode;
  }

  /**
   * @param editJurisdiction the editJurisdiction to set
   */
  public void setEditJurisdiction(Jurisdiction editJurisdiction) {
    this.editJurisdiction = editJurisdiction;
  }

  /**
   * @param editTaxRate the editTaxRate to set
   */
  public void setEditTaxRate(JurisdictionTaxrate editTaxRate) {
    this.editTaxRate = editTaxRate;
  }

  /**
   * @param fieldSizeMap the fieldSizeMap to set
   */
  public void setFieldSizeMap(Map<String, Integer> fieldSizeMap) {
    this.fieldSizeMap = fieldSizeMap;
  }

  /**
   * @param formMessagesDataTable the formMessagesDataTable to set
   */
  public void setFormMessagesDataTable(HtmlDataTable formMessagesDataTable) {
    this.formMessagesDataTable = formMessagesDataTable;
  }

  /**
   * @param inOutMenu the inOutMenu to set
   */
  public void setInOutMenu(HtmlSelectOneMenu inOutMenu) {
    this.inOutMenu = inOutMenu;
  }

  public void setLocale(Locale locale) {
    this.locale = locale;
  }

  /**
   * @param rateTypeCodeItems the rateTypeCodeItems to set
   */
  public void setRateTypeCodeItems(List<SelectItem> rateTypeCodeItems) {
    this.rateTypeCodeItems = rateTypeCodeItems;
  }

  /**
   * @param rateTypeCodeMenu the rateTypeCodeMenu to set
   */
  public void setRateTypeCodeMenu(HtmlSelectOneMenu rateTypeCodeMenu) {
    this.rateTypeCodeMenu = rateTypeCodeMenu;
  }

  /**
   * @param messagesModalPanel the messagesModalPanel to set
   */
  public void setMessagesModalPanel(HtmlModalPanel messagesModalPanel) {
    this.messagesModalPanel = messagesModalPanel;
  }

  public void setRowCount(int rowCount) {
    this.rowCount = rowCount;
  }

  /**
   * @param searchcity the searchcity to set
   */
  public void setSearchcity(String searchcity) {
    this.searchcity = searchcity;
  }

  /**
   * @param searchclientGeocode the searchclientGeocode to set
   */
  public void setSearchclientGeocode(String searchclientGeocode) {
    this.searchclientGeocode = searchclientGeocode;
  }

  /**
   * @param searchcompGeocode the searchcompGeocode to set
   */
  public void setSearchcompGeocode(String searchcompGeocode) {
    this.searchcompGeocode = searchcompGeocode;
  }

  /**
   * @param searchcounty the searchcounty to set
   */
  public void setSearchcounty(String searchcounty) {
    this.searchcounty = searchcounty;
  }

  /**
   * @param searcheffectiveDate the searcheffectiveDate to set
   */
  public void setSearcheffectiveDate(Date searcheffectiveDate) {
    this.searcheffectiveDate = searcheffectiveDate;
  }

  /**
   * @param searchexpirationDate the searchexpirationDate to set
   */
  public void setSearchexpirationDate(Date searchexpirationDate) {
    this.searchexpirationDate = searchexpirationDate;
  }

  /**
   * @param searchgeocode the searchgeocode to set
   */
  public void setSearchgeocode(String searchgeocode) {
    this.searchgeocode = searchgeocode;
  }

  /**
   * @param searchjurisdictionId the searchjurisdictionId to set
   */
  public void setSearchjurisdictionId(Long searchjurisdictionId) {
    // Empty values get converted to 0, change back to null
    if ((searchjurisdictionId != null) && (searchjurisdictionId <= 0L)) {
      searchjurisdictionId = null;
    }
    this.searchjurisdictionId = searchjurisdictionId;
  }

  /**
   * @param searchjurisdictionTaxrateId the searchjurisdictionTaxrateId to set
   */
  public void setSearchjurisdictionTaxrateId(Long searchjurisdictionTaxrateId) {
    // Empty values get converted to 0, change back to null
    if ((searchjurisdictionTaxrateId != null) && (searchjurisdictionTaxrateId <= 0L)) {
      searchjurisdictionTaxrateId = null;
    }
    this.searchjurisdictionTaxrateId = searchjurisdictionTaxrateId;
  }

  /**
   * @param searchmodifiedFlag the searchmodifiedFlag to set
   */
  public void setSearchmodifiedFlag(boolean searchmodifiedFlag) {
    this.searchmodifiedFlag = searchmodifiedFlag;
  }

  /**
   * @param searchstate the searchstate to set
   */
  public void setSearchstate(String searchstate) {
    this.searchstate = searchstate;
  }
  
  public void setSearchstateDetail(String searchstateDetail) {
	 this.searchstateDetail = searchstateDetail;
  }

  /**
   * @param searchzip the searchzip to set
   */
  public void setSearchzip(String searchzip) {
    this.searchzip = searchzip;
  }
  
  public void setSearchzipplus4(String searchzipplus4) {
	this.searchzipplus4 = searchzipplus4;
  }

  public void setSelectedJurisdiction(Jurisdiction selectedJurisdiction) {
    this.selectedJurisdiction = selectedJurisdiction;
  }

  public void setSelectedTaxRate(JurisdictionTaxrate selectedTaxRate) {
    this.selectedTaxRate = selectedTaxRate;
  }

  /**
   * @param showCancelButton the showCancelButton to set
   */
  public void setShowCancelButton(Boolean showCancelButton) {
    this.showCancelButton = showCancelButton;
  }

  public void setStateMenu(HtmlSelectOneMenu stateMenu) {
    this.stateMenu = stateMenu;
  }

  /**
   * @param stateMenuItems the stateMenuItems to set
   */
  public void setStateMenuItems(List<SelectItem> stateMenuItems) {
    this.stateMenuItems = stateMenuItems;
  }
  
  public HtmlOutputText getUseWarningOutputText() {
	  return useWarningOutputText;
  }
  
  public HtmlOutputText getSalesWarningOutputText() {
	  return salesWarningOutputText;
  }

  public void setUseWarningOutputText(HtmlOutputText useWarningOutputText) {
	  this.useWarningOutputText = useWarningOutputText;
  }
  
  public void setSalesWarningOutputText(HtmlOutputText salesWarningOutputText) {
	  this.salesWarningOutputText = salesWarningOutputText;
  }
  
  	//start of country 
  	public void setCountrySalesTier1MaxAmtInputText(HtmlInputText countrySalesTier1MaxAmtInputText) {
  		this.countrySalesTier1MaxAmtInputText = countrySalesTier1MaxAmtInputText;
	}

	public void setCountrySalesTier2MaxAmtInputText(HtmlInputText countrySalesTier2MaxAmtInputText) {
	    this.countrySalesTier2MaxAmtInputText = countrySalesTier2MaxAmtInputText;
	}

	public void setCountrySalesTier2MinAmtInputText(HtmlInputText countrySalesTier2MinAmtInputText) {
	    this.countrySalesTier2MinAmtInputText = countrySalesTier2MinAmtInputText;
	}
	  
	public HtmlInputText getCountrySalesTier1RateInputText() {
		return countrySalesTier1RateInputText;
	}
	
	public HtmlInputText getCountryUseTier1RateInputText() {
		return countryUseTier1RateInputText;
	}
	  
	public void setCountrySalesTier1RateInputText(HtmlInputText countrySalesTier1RateInputText) {
		this.countrySalesTier1RateInputText = countrySalesTier1RateInputText;
	}
  
	public void setCountryUseTier1RateInputText(HtmlInputText countryUseTier1RateInputText) {
		this.countryUseTier1RateInputText = countryUseTier1RateInputText;
	}
  
	public HtmlInputText getCountrySalesTier1SetamtInputText() {
		return countrySalesTier1SetamtInputText;
	}

	public void setCountrySalesTier1SetamtInputText(HtmlInputText countrySalesTier1SetamtInputText) {
		this.countrySalesTier1SetamtInputText = countrySalesTier1SetamtInputText;
	}
  
	public HtmlInputText getCountrySalesTier2RateInputText() {
		return countrySalesTier2RateInputText;
	}

	public void setCountrySalesTier2RateInputText(HtmlInputText countrySalesTier2RateInputText) {
		this.countrySalesTier2RateInputText = countrySalesTier2RateInputText;
	}
  
	public HtmlInputText getCountrySalesTier2SetamtInputText() {
		return countrySalesTier2SetamtInputText;
	}

	public void setCountrySalesTier2SetamtInputText(HtmlInputText countrySalesTier2SetamtInputText) {
		this.countrySalesTier2SetamtInputText = countrySalesTier2SetamtInputText;
	}
  
	public HtmlInputText getCountrySalesTier3RateInputText() {
		return countrySalesTier3RateInputText;
	}

	public void setCountrySalesTier3RateInputText(HtmlInputText countrySalesTier3RateInputText) {
		this.countrySalesTier3RateInputText = countrySalesTier3RateInputText;
	}
  
	public HtmlInputText getCountrySalesTier3SetamtInputText() {
		return countrySalesTier3SetamtInputText;
	}

	public void setCountrySalesTier3SetamtInputText(HtmlInputText countrySalesTier3SetamtInputText) {
		this.countrySalesTier3SetamtInputText = countrySalesTier3SetamtInputText;
	}
  
	public HtmlInputText getCountryUseTier1SetamtInputText() {
		return countryUseTier1SetamtInputText;
	}

	public void setCountryUseTier1SetamtInputText(HtmlInputText countryUseTier1SetamtInputText) {
		this.countryUseTier1SetamtInputText = countryUseTier1SetamtInputText;
	}
  
	public HtmlInputText getCountryUseTier2RateInputText() {
		return countryUseTier2RateInputText;
	}

	public void setCountryUseTier2RateInputText(HtmlInputText countryUseTier2RateInputText) {
		this.countryUseTier2RateInputText = countryUseTier2RateInputText;
	}
  
	public HtmlInputText getCountryUseTier2SetamtInputText() {
		return countryUseTier2SetamtInputText;
	}

	public void setCountryUseTier2SetamtInputText(HtmlInputText countryUseTier2SetamtInputText) {
		this.countryUseTier2SetamtInputText = countryUseTier2SetamtInputText;
	}
  
	public HtmlInputText getCountryUseTier3RateInputText() {
		return countryUseTier3RateInputText;
	}

	public void setCountryUseTier3RateInputText(HtmlInputText countryUseTier3RateInputText) {
		this.countryUseTier3RateInputText = countryUseTier3RateInputText;
	}
  
	public HtmlInputText getCountryUseTier3SetamtInputText() {
		return countryUseTier3SetamtInputText;
	}

	public void setCountryUseTier3SetamtInputText(HtmlInputText countryUseTier3SetamtInputText) {
		this.countryUseTier3SetamtInputText = countryUseTier3SetamtInputText;
	}
	
	public HtmlSelectBooleanCheckbox getCountrySalesTier1SetToMaximumCheckBind() {
		return countrySalesTier1SetToMaximumCheckBind;
	}

	public void setCountrySalesTier1SetToMaximumCheckBind(HtmlSelectBooleanCheckbox countrySalesTier1SetToMaximumCheckBind) {
		this.countrySalesTier1SetToMaximumCheckBind = countrySalesTier1SetToMaximumCheckBind;
	}
	
	public HtmlSelectBooleanCheckbox getCountrySalesTier2SetToMaximumCheckBind() {
		return countrySalesTier2SetToMaximumCheckBind;
	}

	public void setCountrySalesTier2SetToMaximumCheckBind(HtmlSelectBooleanCheckbox countrySalesTier2SetToMaximumCheckBind) {
		this.countrySalesTier2SetToMaximumCheckBind = countrySalesTier2SetToMaximumCheckBind;
	}
	
	public HtmlSelectBooleanCheckbox getCountryUseTier2SetToMaximumCheckBind() {
		return countryUseTier2SetToMaximumCheckBind;
	}

	public void setCountryUseTier2SetToMaximumCheckBind(HtmlSelectBooleanCheckbox countryUseTier2SetToMaximumCheckBind) {
		this.countryUseTier2SetToMaximumCheckBind = countryUseTier2SetToMaximumCheckBind;
	}
	
	public HtmlSelectBooleanCheckbox getCountryUseTier1SetToMaximumCheckBind() {
		return countryUseTier1SetToMaximumCheckBind;
	}

	public void setCountryUseTier1SetToMaximumCheckBind(HtmlSelectBooleanCheckbox countryUseTier1SetToMaximumCheckBind) {
		this.countryUseTier1SetToMaximumCheckBind = countryUseTier1SetToMaximumCheckBind;
	}
	
	public HtmlSelectBooleanCheckbox getCountrySalesTier2TaxEntireAmountCheckBind() {
		return countrySalesTier2TaxEntireAmountCheckBind;
	}

	public void setCountrySalesTier2TaxEntireAmountCheckBind(HtmlSelectBooleanCheckbox countrySalesTier2TaxEntireAmountCheckBind) {
		this.countrySalesTier2TaxEntireAmountCheckBind = countrySalesTier2TaxEntireAmountCheckBind;
	}
	
	public HtmlSelectBooleanCheckbox getCountrySameAsSalesRatesCheckBind() {
		return countrySameAsSalesRatesCheckBind;
	}

	public void setCountrySameAsSalesRatesCheckBind(HtmlSelectBooleanCheckbox countrySameAsSalesRatesCheckBind) {
		this.countrySameAsSalesRatesCheckBind = countrySameAsSalesRatesCheckBind;
	}
	
	public HtmlSelectBooleanCheckbox getCountrySalesTier3TaxEntireAmountCheckBind() {
		return countrySalesTier3TaxEntireAmountCheckBind;
	}

	public void setCountrySalesTier3TaxEntireAmountCheckBind(HtmlSelectBooleanCheckbox countrySalesTier3TaxEntireAmountCheckBind) {
		this.countrySalesTier3TaxEntireAmountCheckBind = countrySalesTier3TaxEntireAmountCheckBind;
	}
	
	public HtmlSelectBooleanCheckbox getCountryUseTier2TaxEntireAmountCheckBind() {
		return countryUseTier2TaxEntireAmountCheckBind;
	}

	public void setCountryUseTier2TaxEntireAmountCheckBind(HtmlSelectBooleanCheckbox countryUseTier2TaxEntireAmountCheckBind) {
		this.countryUseTier2TaxEntireAmountCheckBind = countryUseTier2TaxEntireAmountCheckBind;
	}
	
	public HtmlSelectBooleanCheckbox getCountryUseTier3TaxEntireAmountCheckBind() {
		return countryUseTier3TaxEntireAmountCheckBind;
	}

	public void setCountryUseTier3TaxEntireAmountCheckBind(HtmlSelectBooleanCheckbox countryUseTier3TaxEntireAmountCheckBind) {
		this.countryUseTier3TaxEntireAmountCheckBind = countryUseTier3TaxEntireAmountCheckBind;
	}
	
	public HtmlInputText getCountrySalesTier2MinAmtInputText() {
		return countrySalesTier2MinAmtInputText;
	}
	
  	public HtmlInputText getCountrySalesTier1MaxAmtInputText() {
  		return countrySalesTier1MaxAmtInputText;
  	}

  	public HtmlInputText getCountrySalesTier2MaxAmtInputText() {
  		return countrySalesTier2MaxAmtInputText;
  	}
  
  	public HtmlInputText getCountryUseTier1MaxAmtInputText() {
  		return countryUseTier1MaxAmtInputText;
  	}

  	public void setCountryUseTier1MaxAmtInputText(HtmlInputText countryUseTier1MaxAmtInputText) {
  		this.countryUseTier1MaxAmtInputText = countryUseTier1MaxAmtInputText;
  	}
	  
  	public HtmlInputText getCountryUseTier2MaxAmtInputText() {
  		return countryUseTier2MaxAmtInputText ;
  	}

  	public void setCountryUseTier2MaxAmtInputText(HtmlInputText countryUseTier2MaxAmtInputText) {
  		this.countryUseTier2MaxAmtInputText = countryUseTier2MaxAmtInputText ;
  	}
	  
  	public HtmlInputText getCountryUseTier2MinAmtInputText () {
  		return countryUseTier2MinAmtInputText;
  	}
	
  	public void setCountryUseTier2MinAmtInputText(HtmlInputText countryUseTier2MinAmtInputText) {
  		this.countryUseTier2MinAmtInputText = countryUseTier2MinAmtInputText;
  	}
  	
  	public boolean getCountrySalesTier2SetToMaximumCheck() {
  		return countrySalesTier2SetToMaximumCheck;
  	}

  	public void setCountrySalesTier2SetToMaximumCheck(boolean countrySalesTier2SetToMaximumCheck) {
  		this.countrySalesTier2SetToMaximumCheck = countrySalesTier2SetToMaximumCheck;
  	}

  	public boolean getCountrySalesTier1SetToMaximumCheck() {
  		return countrySalesTier1SetToMaximumCheck;
  	}

	public void setCountrySalesTier1SetToMaximumCheck(boolean countrySalesTier1SetToMaximumCheck) {
	    this.countrySalesTier1SetToMaximumCheck = countrySalesTier1SetToMaximumCheck;
	}
  
	public boolean getCountrySameAsSalesRatesCheck() {
		return countrySameAsSalesRatesCheck;
	}

	public void setCountrySameAsSalesRatesCheck(boolean countrySameAsSalesRatesCheck) {
		this.countrySameAsSalesRatesCheck = countrySameAsSalesRatesCheck;
	}

	public boolean getCountryUseTier2SetToMaximumCheck() {
		return countryUseTier2SetToMaximumCheck;
	}

	public void setCountryUseTier2SetToMaximumCheck(boolean countryUseTier2SetToMaximumCheck) {
		this.countryUseTier2SetToMaximumCheck = countryUseTier2SetToMaximumCheck;
	}

	public boolean getCountryUseTier1SetToMaximumCheck() {
		return countryUseTier1SetToMaximumCheck;
	}

	public void setCountryUseTier1SetToMaximumCheck(boolean countryUseTier1SetToMaximumCheck) {
		this.countryUseTier1SetToMaximumCheck = countryUseTier1SetToMaximumCheck;
	}
  
	public boolean getCountrySalesTier2TaxEntireAmountCheck() {
		return countrySalesTier2TaxEntireAmountCheck;
	}

	public void setCountrySalesTier2TaxEntireAmountCheck(boolean countrySalesTier2TaxEntireAmountCheck) {
		this.countrySalesTier2TaxEntireAmountCheck = countrySalesTier2TaxEntireAmountCheck;
	}
  
	public boolean getCountrySalesTier3TaxEntireAmountCheck() {
		return countrySalesTier3TaxEntireAmountCheck;
	}

	public void setCountrySalesTier3TaxEntireAmountCheck(boolean countrySalesTier3TaxEntireAmountCheck) {
		this.countrySalesTier3TaxEntireAmountCheck = countrySalesTier3TaxEntireAmountCheck;
	}
  
	public boolean getCountryUseTier2TaxEntireAmountCheck() {
		return countryUseTier2TaxEntireAmountCheck;
	}

	public void setCountryUseTier2TaxEntireAmountCheck(boolean countryUseTier2TaxEntireAmountCheck) {
		this.countryUseTier2TaxEntireAmountCheck = countryUseTier2TaxEntireAmountCheck;
	}
  
	public boolean getCountryUseTier3TaxEntireAmountCheck() {
		return countryUseTier3TaxEntireAmountCheck;
	}

	public void setCountryUseTier3TaxEntireAmountCheck(boolean countryUseTier3TaxEntireAmountCheck) {
		this.countryUseTier3TaxEntireAmountCheck = countryUseTier3TaxEntireAmountCheck;
	}
	
	public HtmlInputText getCountrySalesMaxtaxAmtInputText() {
		return countrySalesMaxtaxAmtInputText;
	}

	public void setCountrySalesMaxtaxAmtInputText(HtmlInputText countrySalesMaxtaxAmtInputText) {
		this.countrySalesMaxtaxAmtInputText = countrySalesMaxtaxAmtInputText;
	}
	
	public HtmlInputText getCountryUseMaxtaxAmtInputText() {
		return countryUseMaxtaxAmtInputText;
	}

	public void setCountryUseMaxtaxAmtInputText(HtmlInputText countryUseMaxtaxAmtInputText) {
		this.countryUseMaxtaxAmtInputText = countryUseMaxtaxAmtInputText;
	}
	//end of country

	//start of state 
  	public void setStateSalesTier1MaxAmtInputText(HtmlInputText stateSalesTier1MaxAmtInputText) {
  		this.stateSalesTier1MaxAmtInputText = stateSalesTier1MaxAmtInputText;
	}

	public void setStateSalesTier2MaxAmtInputText(HtmlInputText stateSalesTier2MaxAmtInputText) {
	    this.stateSalesTier2MaxAmtInputText = stateSalesTier2MaxAmtInputText;
	}

	public void setStateSalesTier2MinAmtInputText(HtmlInputText stateSalesTier2MinAmtInputText) {
	    this.stateSalesTier2MinAmtInputText = stateSalesTier2MinAmtInputText;
	}
	  
	public HtmlInputText getStateSalesTier1RateInputText() {
		return stateSalesTier1RateInputText;
	}
	
	public HtmlInputText getStateUseTier1RateInputText() {
		return stateUseTier1RateInputText;
	}
	  
	public void setStateSalesTier1RateInputText(HtmlInputText stateSalesTier1RateInputText) {
		this.stateSalesTier1RateInputText = stateSalesTier1RateInputText;
	}
  
	public void setStateUseTier1RateInputText(HtmlInputText stateUseTier1RateInputText) {
		this.stateUseTier1RateInputText = stateUseTier1RateInputText;
	}
  
	public HtmlInputText getStateSalesTier1SetamtInputText() {
		return stateSalesTier1SetamtInputText;
	}

	public void setStateSalesTier1SetamtInputText(HtmlInputText stateSalesTier1SetamtInputText) {
		this.stateSalesTier1SetamtInputText = stateSalesTier1SetamtInputText;
	}
  
	public HtmlInputText getStateSalesTier2RateInputText() {
		return stateSalesTier2RateInputText;
	}

	public void setStateSalesTier2RateInputText(HtmlInputText stateSalesTier2RateInputText) {
		this.stateSalesTier2RateInputText = stateSalesTier2RateInputText;
	}
  
	public HtmlInputText getStateSalesTier2SetamtInputText() {
		return stateSalesTier2SetamtInputText;
	}

	public void setStateSalesTier2SetamtInputText(HtmlInputText stateSalesTier2SetamtInputText) {
		this.stateSalesTier2SetamtInputText = stateSalesTier2SetamtInputText;
	}
  
	public HtmlInputText getStateSalesTier3RateInputText() {
		return stateSalesTier3RateInputText;
	}

	public void setStateSalesTier3RateInputText(HtmlInputText stateSalesTier3RateInputText) {
		this.stateSalesTier3RateInputText = stateSalesTier3RateInputText;
	}
  
	public HtmlInputText getStateSalesTier3SetamtInputText() {
		return stateSalesTier3SetamtInputText;
	}

	public void setStateSalesTier3SetamtInputText(HtmlInputText stateSalesTier3SetamtInputText) {
		this.stateSalesTier3SetamtInputText = stateSalesTier3SetamtInputText;
	}
  
	public HtmlInputText getStateUseTier1SetamtInputText() {
		return stateUseTier1SetamtInputText;
	}

	public void setStateUseTier1SetamtInputText(HtmlInputText stateUseTier1SetamtInputText) {
		this.stateUseTier1SetamtInputText = stateUseTier1SetamtInputText;
	}
  
	public HtmlInputText getStateUseTier2RateInputText() {
		return stateUseTier2RateInputText;
	}

	public void setStateUseTier2RateInputText(HtmlInputText stateUseTier2RateInputText) {
		this.stateUseTier2RateInputText = stateUseTier2RateInputText;
	}
  
	public HtmlInputText getStateUseTier2SetamtInputText() {
		return stateUseTier2SetamtInputText;
	}

	public void setStateUseTier2SetamtInputText(HtmlInputText stateUseTier2SetamtInputText) {
		this.stateUseTier2SetamtInputText = stateUseTier2SetamtInputText;
	}
  
	public HtmlInputText getStateUseTier3RateInputText() {
		return stateUseTier3RateInputText;
	}

	public void setStateUseTier3RateInputText(HtmlInputText stateUseTier3RateInputText) {
		this.stateUseTier3RateInputText = stateUseTier3RateInputText;
	}
  
	public HtmlInputText getStateUseTier3SetamtInputText() {
		return stateUseTier3SetamtInputText;
	}

	public void setStateUseTier3SetamtInputText(HtmlInputText stateUseTier3SetamtInputText) {
		this.stateUseTier3SetamtInputText = stateUseTier3SetamtInputText;
	}
	
	public HtmlSelectBooleanCheckbox getStateSalesTier1SetToMaximumCheckBind() {
		return stateSalesTier1SetToMaximumCheckBind;
	}

	public void setStateSalesTier1SetToMaximumCheckBind(HtmlSelectBooleanCheckbox stateSalesTier1SetToMaximumCheckBind) {
		this.stateSalesTier1SetToMaximumCheckBind = stateSalesTier1SetToMaximumCheckBind;
	}
	
	public HtmlSelectBooleanCheckbox getStateSalesTier2SetToMaximumCheckBind() {
		return stateSalesTier2SetToMaximumCheckBind;
	}

	public void setStateSalesTier2SetToMaximumCheckBind(HtmlSelectBooleanCheckbox stateSalesTier2SetToMaximumCheckBind) {
		this.stateSalesTier2SetToMaximumCheckBind = stateSalesTier2SetToMaximumCheckBind;
	}
	
	public HtmlSelectBooleanCheckbox getStateUseTier2SetToMaximumCheckBind() {
		return stateUseTier2SetToMaximumCheckBind;
	}

	public void setStateUseTier2SetToMaximumCheckBind(HtmlSelectBooleanCheckbox stateUseTier2SetToMaximumCheckBind) {
		this.stateUseTier2SetToMaximumCheckBind = stateUseTier2SetToMaximumCheckBind;
	}
	
	public HtmlSelectBooleanCheckbox getStateUseTier1SetToMaximumCheckBind() {
		return stateUseTier1SetToMaximumCheckBind;
	}

	public void setStateUseTier1SetToMaximumCheckBind(HtmlSelectBooleanCheckbox stateUseTier1SetToMaximumCheckBind) {
		this.stateUseTier1SetToMaximumCheckBind = stateUseTier1SetToMaximumCheckBind;
	}
	
	public HtmlSelectBooleanCheckbox getStateSalesTier2TaxEntireAmountCheckBind() {
		return stateSalesTier2TaxEntireAmountCheckBind;
	}

	public void setStateSalesTier2TaxEntireAmountCheckBind(HtmlSelectBooleanCheckbox stateSalesTier2TaxEntireAmountCheckBind) {
		this.stateSalesTier2TaxEntireAmountCheckBind = stateSalesTier2TaxEntireAmountCheckBind;
	}
	
	public HtmlSelectBooleanCheckbox getStateSameAsSalesRatesCheckBind() {
		return stateSameAsSalesRatesCheckBind;
	}

	public void setStateSameAsSalesRatesCheckBind(HtmlSelectBooleanCheckbox stateSameAsSalesRatesCheckBind) {
		this.stateSameAsSalesRatesCheckBind = stateSameAsSalesRatesCheckBind;
	}
	
	public HtmlSelectBooleanCheckbox getStateSalesTier3TaxEntireAmountCheckBind() {
		return stateSalesTier3TaxEntireAmountCheckBind;
	}

	public void setStateSalesTier3TaxEntireAmountCheckBind(HtmlSelectBooleanCheckbox stateSalesTier3TaxEntireAmountCheckBind) {
		this.stateSalesTier3TaxEntireAmountCheckBind = stateSalesTier3TaxEntireAmountCheckBind;
	}
	
	public HtmlSelectBooleanCheckbox getStateUseTier2TaxEntireAmountCheckBind() {
		return stateUseTier2TaxEntireAmountCheckBind;
	}

	public void setStateUseTier2TaxEntireAmountCheckBind(HtmlSelectBooleanCheckbox stateUseTier2TaxEntireAmountCheckBind) {
		this.stateUseTier2TaxEntireAmountCheckBind = stateUseTier2TaxEntireAmountCheckBind;
	}
	
	public HtmlSelectBooleanCheckbox getStateUseTier3TaxEntireAmountCheckBind() {
		return stateUseTier3TaxEntireAmountCheckBind;
	}

	public void setStateUseTier3TaxEntireAmountCheckBind(HtmlSelectBooleanCheckbox stateUseTier3TaxEntireAmountCheckBind) {
		this.stateUseTier3TaxEntireAmountCheckBind = stateUseTier3TaxEntireAmountCheckBind;
	}
	
	public HtmlInputText getStateSalesTier2MinAmtInputText() {
		return stateSalesTier2MinAmtInputText;
	}
	
  	public HtmlInputText getStateSalesTier1MaxAmtInputText() {
  		return stateSalesTier1MaxAmtInputText;
  	}

  	public HtmlInputText getStateSalesTier2MaxAmtInputText() {
  		return stateSalesTier2MaxAmtInputText;
  	}
  
  	public HtmlInputText getStateUseTier1MaxAmtInputText() {
  		return stateUseTier1MaxAmtInputText;
  	}

  	public void setStateUseTier1MaxAmtInputText(HtmlInputText stateUseTier1MaxAmtInputText) {
  		this.stateUseTier1MaxAmtInputText = stateUseTier1MaxAmtInputText;
  	}
	  
  	public HtmlInputText getStateUseTier2MaxAmtInputText() {
  		return stateUseTier2MaxAmtInputText ;
  	}

  	public void setStateUseTier2MaxAmtInputText(HtmlInputText stateUseTier2MaxAmtInputText) {
  		this.stateUseTier2MaxAmtInputText = stateUseTier2MaxAmtInputText ;
  	}
	  
  	public HtmlInputText getStateUseTier2MinAmtInputText () {
  		return stateUseTier2MinAmtInputText;
  	}
	
  	public void setStateUseTier2MinAmtInputText(HtmlInputText stateUseTier2MinAmtInputText) {
  		this.stateUseTier2MinAmtInputText = stateUseTier2MinAmtInputText;
  	}
  	
  	public boolean getStateSalesTier2SetToMaximumCheck() {
  		return stateSalesTier2SetToMaximumCheck;
  	}

  	public void setStateSalesTier2SetToMaximumCheck(boolean stateSalesTier2SetToMaximumCheck) {
  		this.stateSalesTier2SetToMaximumCheck = stateSalesTier2SetToMaximumCheck;
  	}

  	public boolean getStateSalesTier1SetToMaximumCheck() {
  		return stateSalesTier1SetToMaximumCheck;
  	}

	public void setStateSalesTier1SetToMaximumCheck(boolean stateSalesTier1SetToMaximumCheck) {
	    this.stateSalesTier1SetToMaximumCheck = stateSalesTier1SetToMaximumCheck;
	}
  
	public boolean getStateSameAsSalesRatesCheck() {
		return stateSameAsSalesRatesCheck;
	}

	public void setStateSameAsSalesRatesCheck(boolean stateSameAsSalesRatesCheck) {
		this.stateSameAsSalesRatesCheck = stateSameAsSalesRatesCheck;
	}

	public boolean getStateUseTier2SetToMaximumCheck() {
		return stateUseTier2SetToMaximumCheck;
	}

	public void setStateUseTier2SetToMaximumCheck(boolean stateUseTier2SetToMaximumCheck) {
		this.stateUseTier2SetToMaximumCheck = stateUseTier2SetToMaximumCheck;
	}

	public boolean getStateUseTier1SetToMaximumCheck() {
		return stateUseTier1SetToMaximumCheck;
	}

	public void setStateUseTier1SetToMaximumCheck(boolean stateUseTier1SetToMaximumCheck) {
		this.stateUseTier1SetToMaximumCheck = stateUseTier1SetToMaximumCheck;
	}
  
	public boolean getStateSalesTier2TaxEntireAmountCheck() {
		return stateSalesTier2TaxEntireAmountCheck;
	}

	public void setStateSalesTier2TaxEntireAmountCheck(boolean stateSalesTier2TaxEntireAmountCheck) {
		this.stateSalesTier2TaxEntireAmountCheck = stateSalesTier2TaxEntireAmountCheck;
	}
  
	public boolean getStateSalesTier3TaxEntireAmountCheck() {
		return stateSalesTier3TaxEntireAmountCheck;
	}

	public void setStateSalesTier3TaxEntireAmountCheck(boolean stateSalesTier3TaxEntireAmountCheck) {
		this.stateSalesTier3TaxEntireAmountCheck = stateSalesTier3TaxEntireAmountCheck;
	}
  
	public boolean getStateUseTier2TaxEntireAmountCheck() {
		return stateUseTier2TaxEntireAmountCheck;
	}

	public void setStateUseTier2TaxEntireAmountCheck(boolean stateUseTier2TaxEntireAmountCheck) {
		this.stateUseTier2TaxEntireAmountCheck = stateUseTier2TaxEntireAmountCheck;
	}
  
	public boolean getStateUseTier3TaxEntireAmountCheck() {
		return stateUseTier3TaxEntireAmountCheck;
	}

	public void setStateUseTier3TaxEntireAmountCheck(boolean stateUseTier3TaxEntireAmountCheck) {
		this.stateUseTier3TaxEntireAmountCheck = stateUseTier3TaxEntireAmountCheck;
	}

	public HtmlInputText getStateSalesMaxtaxAmtInputText() {
		return stateSalesMaxtaxAmtInputText;
	}

	public void setStateSalesMaxtaxAmtInputText(HtmlInputText stateSalesMaxtaxAmtInputText) {
		this.stateSalesMaxtaxAmtInputText = stateSalesMaxtaxAmtInputText;
	}
	
	public HtmlInputText getStateUseMaxtaxAmtInputText() {
		return stateUseMaxtaxAmtInputText;
	}

	public void setStateUseMaxtaxAmtInputText(HtmlInputText stateUseMaxtaxAmtInputText) {
		this.stateUseMaxtaxAmtInputText = stateUseMaxtaxAmtInputText;
	}
	//end of state
	
	//start of county 
  	public void setCountySalesTier1MaxAmtInputText(HtmlInputText countySalesTier1MaxAmtInputText) {
  		this.countySalesTier1MaxAmtInputText = countySalesTier1MaxAmtInputText;
	}

	public void setCountySalesTier2MaxAmtInputText(HtmlInputText countySalesTier2MaxAmtInputText) {
	    this.countySalesTier2MaxAmtInputText = countySalesTier2MaxAmtInputText;
	}

	public void setCountySalesTier2MinAmtInputText(HtmlInputText countySalesTier2MinAmtInputText) {
	    this.countySalesTier2MinAmtInputText = countySalesTier2MinAmtInputText;
	}
	  
	public HtmlInputText getCountySalesTier1RateInputText() {
		return countySalesTier1RateInputText;
	}
	
	public HtmlInputText getCountyUseTier1RateInputText() {
		return countyUseTier1RateInputText;
	}
	  
	public void setCountySalesTier1RateInputText(HtmlInputText countySalesTier1RateInputText) {
		this.countySalesTier1RateInputText = countySalesTier1RateInputText;
	}
  
	public void setCountyUseTier1RateInputText(HtmlInputText countyUseTier1RateInputText) {
		this.countyUseTier1RateInputText = countyUseTier1RateInputText;
	}
  
	public HtmlInputText getCountySalesTier1SetamtInputText() {
		return countySalesTier1SetamtInputText;
	}

	public void setCountySalesTier1SetamtInputText(HtmlInputText countySalesTier1SetamtInputText) {
		this.countySalesTier1SetamtInputText = countySalesTier1SetamtInputText;
	}
  
	public HtmlInputText getCountySalesTier2RateInputText() {
		return countySalesTier2RateInputText;
	}

	public void setCountySalesTier2RateInputText(HtmlInputText countySalesTier2RateInputText) {
		this.countySalesTier2RateInputText = countySalesTier2RateInputText;
	}
  
	public HtmlInputText getCountySalesTier2SetamtInputText() {
		return countySalesTier2SetamtInputText;
	}

	public void setCountySalesTier2SetamtInputText(HtmlInputText countySalesTier2SetamtInputText) {
		this.countySalesTier2SetamtInputText = countySalesTier2SetamtInputText;
	}
  
	public HtmlInputText getCountySalesTier3RateInputText() {
		return countySalesTier3RateInputText;
	}

	public void setCountySalesTier3RateInputText(HtmlInputText countySalesTier3RateInputText) {
		this.countySalesTier3RateInputText = countySalesTier3RateInputText;
	}
  
	public HtmlInputText getCountySalesTier3SetamtInputText() {
		return countySalesTier3SetamtInputText;
	}

	public void setCountySalesTier3SetamtInputText(HtmlInputText countySalesTier3SetamtInputText) {
		this.countySalesTier3SetamtInputText = countySalesTier3SetamtInputText;
	}
  
	public HtmlInputText getCountyUseTier1SetamtInputText() {
		return countyUseTier1SetamtInputText;
	}

	public void setCountyUseTier1SetamtInputText(HtmlInputText countyUseTier1SetamtInputText) {
		this.countyUseTier1SetamtInputText = countyUseTier1SetamtInputText;
	}
  
	public HtmlInputText getCountyUseTier2RateInputText() {
		return countyUseTier2RateInputText;
	}

	public void setCountyUseTier2RateInputText(HtmlInputText countyUseTier2RateInputText) {
		this.countyUseTier2RateInputText = countyUseTier2RateInputText;
	}
  
	public HtmlInputText getCountyUseTier2SetamtInputText() {
		return countyUseTier2SetamtInputText;
	}

	public void setCountyUseTier2SetamtInputText(HtmlInputText countyUseTier2SetamtInputText) {
		this.countyUseTier2SetamtInputText = countyUseTier2SetamtInputText;
	}
  
	public HtmlInputText getCountyUseTier3RateInputText() {
		return countyUseTier3RateInputText;
	}

	public void setCountyUseTier3RateInputText(HtmlInputText countyUseTier3RateInputText) {
		this.countyUseTier3RateInputText = countyUseTier3RateInputText;
	}
  
	public HtmlInputText getCountyUseTier3SetamtInputText() {
		return countyUseTier3SetamtInputText;
	}

	public void setCountyUseTier3SetamtInputText(HtmlInputText countyUseTier3SetamtInputText) {
		this.countyUseTier3SetamtInputText = countyUseTier3SetamtInputText;
	}
	
	public HtmlSelectBooleanCheckbox getCountySalesTier1SetToMaximumCheckBind() {
		return countySalesTier1SetToMaximumCheckBind;
	}

	public void setCountySalesTier1SetToMaximumCheckBind(HtmlSelectBooleanCheckbox countySalesTier1SetToMaximumCheckBind) {
		this.countySalesTier1SetToMaximumCheckBind = countySalesTier1SetToMaximumCheckBind;
	}
	
	public HtmlSelectBooleanCheckbox getCountySalesTier2SetToMaximumCheckBind() {
		return countySalesTier2SetToMaximumCheckBind;
	}

	public void setCountySalesTier2SetToMaximumCheckBind(HtmlSelectBooleanCheckbox countySalesTier2SetToMaximumCheckBind) {
		this.countySalesTier2SetToMaximumCheckBind = countySalesTier2SetToMaximumCheckBind;
	}
	
	public HtmlSelectBooleanCheckbox getCountyUseTier2SetToMaximumCheckBind() {
		return countyUseTier2SetToMaximumCheckBind;
	}

	public void setCountyUseTier2SetToMaximumCheckBind(HtmlSelectBooleanCheckbox countyUseTier2SetToMaximumCheckBind) {
		this.countyUseTier2SetToMaximumCheckBind = countyUseTier2SetToMaximumCheckBind;
	}
	
	public HtmlSelectBooleanCheckbox getCountyUseTier1SetToMaximumCheckBind() {
		return countyUseTier1SetToMaximumCheckBind;
	}

	public void setCountyUseTier1SetToMaximumCheckBind(HtmlSelectBooleanCheckbox countyUseTier1SetToMaximumCheckBind) {
		this.countyUseTier1SetToMaximumCheckBind = countyUseTier1SetToMaximumCheckBind;
	}
	
	public HtmlSelectBooleanCheckbox getCountySalesTier2TaxEntireAmountCheckBind() {
		return countySalesTier2TaxEntireAmountCheckBind;
	}

	public void setCountySalesTier2TaxEntireAmountCheckBind(HtmlSelectBooleanCheckbox countySalesTier2TaxEntireAmountCheckBind) {
		this.countySalesTier2TaxEntireAmountCheckBind = countySalesTier2TaxEntireAmountCheckBind;
	}
	
	public HtmlSelectBooleanCheckbox getCountySameAsSalesRatesCheckBind() {
		return countySameAsSalesRatesCheckBind;
	}

	public void setCountySameAsSalesRatesCheckBind(HtmlSelectBooleanCheckbox countySameAsSalesRatesCheckBind) {
		this.countySameAsSalesRatesCheckBind = countySameAsSalesRatesCheckBind;
	}
	
	public HtmlSelectBooleanCheckbox getCountySalesTier3TaxEntireAmountCheckBind() {
		return countySalesTier3TaxEntireAmountCheckBind;
	}

	public void setCountySalesTier3TaxEntireAmountCheckBind(HtmlSelectBooleanCheckbox countySalesTier3TaxEntireAmountCheckBind) {
		this.countySalesTier3TaxEntireAmountCheckBind = countySalesTier3TaxEntireAmountCheckBind;
	}
	
	public HtmlSelectBooleanCheckbox getCountyUseTier2TaxEntireAmountCheckBind() {
		return countyUseTier2TaxEntireAmountCheckBind;
	}

	public void setCountyUseTier2TaxEntireAmountCheckBind(HtmlSelectBooleanCheckbox countyUseTier2TaxEntireAmountCheckBind) {
		this.countyUseTier2TaxEntireAmountCheckBind = countyUseTier2TaxEntireAmountCheckBind;
	}
	
	public HtmlSelectBooleanCheckbox getCountyUseTier3TaxEntireAmountCheckBind() {
		return countyUseTier3TaxEntireAmountCheckBind;
	}

	public void setCountyUseTier3TaxEntireAmountCheckBind(HtmlSelectBooleanCheckbox countyUseTier3TaxEntireAmountCheckBind) {
		this.countyUseTier3TaxEntireAmountCheckBind = countyUseTier3TaxEntireAmountCheckBind;
	}
	
	public HtmlInputText getCountySalesTier2MinAmtInputText() {
		return countySalesTier2MinAmtInputText;
	}
	
  	public HtmlInputText getCountySalesTier1MaxAmtInputText() {
  		return countySalesTier1MaxAmtInputText;
  	}

  	public HtmlInputText getCountySalesTier2MaxAmtInputText() {
  		return countySalesTier2MaxAmtInputText;
  	}
  
  	public HtmlInputText getCountyUseTier1MaxAmtInputText() {
  		return countyUseTier1MaxAmtInputText;
  	}

  	public void setCountyUseTier1MaxAmtInputText(HtmlInputText countyUseTier1MaxAmtInputText) {
  		this.countyUseTier1MaxAmtInputText = countyUseTier1MaxAmtInputText;
  	}
	  
  	public HtmlInputText getCountyUseTier2MaxAmtInputText() {
  		return countyUseTier2MaxAmtInputText ;
  	}

  	public void setCountyUseTier2MaxAmtInputText(HtmlInputText countyUseTier2MaxAmtInputText) {
  		this.countyUseTier2MaxAmtInputText = countyUseTier2MaxAmtInputText ;
  	}
	  
  	public HtmlInputText getCountyUseTier2MinAmtInputText () {
  		return countyUseTier2MinAmtInputText;
  	}
	
  	public void setCountyUseTier2MinAmtInputText(HtmlInputText countyUseTier2MinAmtInputText) {
  		this.countyUseTier2MinAmtInputText = countyUseTier2MinAmtInputText;
  	}
  	
  	public boolean getCountySalesTier2SetToMaximumCheck() {
  		return countySalesTier2SetToMaximumCheck;
  	}

  	public void setCountySalesTier2SetToMaximumCheck(boolean countySalesTier2SetToMaximumCheck) {
  		this.countySalesTier2SetToMaximumCheck = countySalesTier2SetToMaximumCheck;
  	}

  	public boolean getCountySalesTier1SetToMaximumCheck() {
  		return countySalesTier1SetToMaximumCheck;
  	}

	public void setCountySalesTier1SetToMaximumCheck(boolean countySalesTier1SetToMaximumCheck) {
	    this.countySalesTier1SetToMaximumCheck = countySalesTier1SetToMaximumCheck;
	}
  
	public boolean getCountySameAsSalesRatesCheck() {
		return countySameAsSalesRatesCheck;
	}

	public void setCountySameAsSalesRatesCheck(boolean countySameAsSalesRatesCheck) {
		this.countySameAsSalesRatesCheck = countySameAsSalesRatesCheck;
	}

	public boolean getCountyUseTier2SetToMaximumCheck() {
		return countyUseTier2SetToMaximumCheck;
	}

	public void setCountyUseTier2SetToMaximumCheck(boolean countyUseTier2SetToMaximumCheck) {
		this.countyUseTier2SetToMaximumCheck = countyUseTier2SetToMaximumCheck;
	}

	public boolean getCountyUseTier1SetToMaximumCheck() {
		return countyUseTier1SetToMaximumCheck;
	}

	public void setCountyUseTier1SetToMaximumCheck(boolean countyUseTier1SetToMaximumCheck) {
		this.countyUseTier1SetToMaximumCheck = countyUseTier1SetToMaximumCheck;
	}
  
	public boolean getCountySalesTier2TaxEntireAmountCheck() {
		return countySalesTier2TaxEntireAmountCheck;
	}

	public void setCountySalesTier2TaxEntireAmountCheck(boolean countySalesTier2TaxEntireAmountCheck) {
		this.countySalesTier2TaxEntireAmountCheck = countySalesTier2TaxEntireAmountCheck;
	}
  
	public boolean getCountySalesTier3TaxEntireAmountCheck() {
		return countySalesTier3TaxEntireAmountCheck;
	}

	public void setCountySalesTier3TaxEntireAmountCheck(boolean countySalesTier3TaxEntireAmountCheck) {
		this.countySalesTier3TaxEntireAmountCheck = countySalesTier3TaxEntireAmountCheck;
	}
  
	public boolean getCountyUseTier2TaxEntireAmountCheck() {
		return countyUseTier2TaxEntireAmountCheck;
	}

	public void setCountyUseTier2TaxEntireAmountCheck(boolean countyUseTier2TaxEntireAmountCheck) {
		this.countyUseTier2TaxEntireAmountCheck = countyUseTier2TaxEntireAmountCheck;
	}
  
	public boolean getCountyUseTier3TaxEntireAmountCheck() {
		return countyUseTier3TaxEntireAmountCheck;
	}

	public void setCountyUseTier3TaxEntireAmountCheck(boolean countyUseTier3TaxEntireAmountCheck) {
		this.countyUseTier3TaxEntireAmountCheck = countyUseTier3TaxEntireAmountCheck;
	}
	
	public HtmlInputText getCountySalesMaxtaxAmtInputText() {
		return countySalesMaxtaxAmtInputText;
	}

	public void setCountySalesMaxtaxAmtInputText(HtmlInputText countySalesMaxtaxAmtInputText) {
		this.countySalesMaxtaxAmtInputText = countySalesMaxtaxAmtInputText;
	}
	
	public HtmlInputText getCountyUseMaxtaxAmtInputText() {
		return countyUseMaxtaxAmtInputText;
	}

	public void setCountyUseMaxtaxAmtInputText(HtmlInputText countyUseMaxtaxAmtInputText) {
		this.countyUseMaxtaxAmtInputText = countyUseMaxtaxAmtInputText;
	}
	//end of county
	
	//start of city 
  	public void setCitySalesTier1MaxAmtInputText(HtmlInputText citySalesTier1MaxAmtInputText) {
  		this.citySalesTier1MaxAmtInputText = citySalesTier1MaxAmtInputText;
	}

	public void setCitySalesTier2MaxAmtInputText(HtmlInputText citySalesTier2MaxAmtInputText) {
	    this.citySalesTier2MaxAmtInputText = citySalesTier2MaxAmtInputText;
	}

	public void setCitySalesTier2MinAmtInputText(HtmlInputText citySalesTier2MinAmtInputText) {
	    this.citySalesTier2MinAmtInputText = citySalesTier2MinAmtInputText;
	}
	  
	public HtmlInputText getCitySalesTier1RateInputText() {
		return citySalesTier1RateInputText;
	}
	
	public HtmlInputText getCityUseTier1RateInputText() {
		return cityUseTier1RateInputText;
	}
	  
	public void setCitySalesTier1RateInputText(HtmlInputText citySalesTier1RateInputText) {
		this.citySalesTier1RateInputText = citySalesTier1RateInputText;
	}
  
	public void setCityUseTier1RateInputText(HtmlInputText cityUseTier1RateInputText) {
		this.cityUseTier1RateInputText = cityUseTier1RateInputText;
	}
  
	public HtmlInputText getCitySalesTier1SetamtInputText() {
		return citySalesTier1SetamtInputText;
	}

	public void setCitySalesTier1SetamtInputText(HtmlInputText citySalesTier1SetamtInputText) {
		this.citySalesTier1SetamtInputText = citySalesTier1SetamtInputText;
	}
  
	public HtmlInputText getCitySalesTier2RateInputText() {
		return citySalesTier2RateInputText;
	}

	public void setCitySalesTier2RateInputText(HtmlInputText citySalesTier2RateInputText) {
		this.citySalesTier2RateInputText = citySalesTier2RateInputText;
	}
  
	public HtmlInputText getCitySalesTier2SetamtInputText() {
		return citySalesTier2SetamtInputText;
	}

	public void setCitySalesTier2SetamtInputText(HtmlInputText citySalesTier2SetamtInputText) {
		this.citySalesTier2SetamtInputText = citySalesTier2SetamtInputText;
	}
  
	public HtmlInputText getCitySalesTier3RateInputText() {
		return citySalesTier3RateInputText;
	}

	public void setCitySalesTier3RateInputText(HtmlInputText citySalesTier3RateInputText) {
		this.citySalesTier3RateInputText = citySalesTier3RateInputText;
	}
  
	public HtmlInputText getCitySalesTier3SetamtInputText() {
		return citySalesTier3SetamtInputText;
	}

	public void setCitySalesTier3SetamtInputText(HtmlInputText citySalesTier3SetamtInputText) {
		this.citySalesTier3SetamtInputText = citySalesTier3SetamtInputText;
	}
  
	public HtmlInputText getCityUseTier1SetamtInputText() {
		return cityUseTier1SetamtInputText;
	}

	public void setCityUseTier1SetamtInputText(HtmlInputText cityUseTier1SetamtInputText) {
		this.cityUseTier1SetamtInputText = cityUseTier1SetamtInputText;
	}
  
	public HtmlInputText getCityUseTier2RateInputText() {
		return cityUseTier2RateInputText;
	}

	public void setCityUseTier2RateInputText(HtmlInputText cityUseTier2RateInputText) {
		this.cityUseTier2RateInputText = cityUseTier2RateInputText;
	}
  
	public HtmlInputText getCityUseTier2SetamtInputText() {
		return cityUseTier2SetamtInputText;
	}

	public void setCityUseTier2SetamtInputText(HtmlInputText cityUseTier2SetamtInputText) {
		this.cityUseTier2SetamtInputText = cityUseTier2SetamtInputText;
	}
  
	public HtmlInputText getCityUseTier3RateInputText() {
		return cityUseTier3RateInputText;
	}

	public void setCityUseTier3RateInputText(HtmlInputText cityUseTier3RateInputText) {
		this.cityUseTier3RateInputText = cityUseTier3RateInputText;
	}
  
	public HtmlInputText getCityUseTier3SetamtInputText() {
		return cityUseTier3SetamtInputText;
	}

	public void setCityUseTier3SetamtInputText(HtmlInputText cityUseTier3SetamtInputText) {
		this.cityUseTier3SetamtInputText = cityUseTier3SetamtInputText;
	}
	
	public HtmlSelectBooleanCheckbox getCitySalesTier1SetToMaximumCheckBind() {
		return citySalesTier1SetToMaximumCheckBind;
	}

	public void setCitySalesTier1SetToMaximumCheckBind(HtmlSelectBooleanCheckbox citySalesTier1SetToMaximumCheckBind) {
		this.citySalesTier1SetToMaximumCheckBind = citySalesTier1SetToMaximumCheckBind;
	}
	
	public HtmlSelectBooleanCheckbox getCitySalesTier2SetToMaximumCheckBind() {
		return citySalesTier2SetToMaximumCheckBind;
	}

	public void setCitySalesTier2SetToMaximumCheckBind(HtmlSelectBooleanCheckbox citySalesTier2SetToMaximumCheckBind) {
		this.citySalesTier2SetToMaximumCheckBind = citySalesTier2SetToMaximumCheckBind;
	}
	
	public HtmlSelectBooleanCheckbox getCityUseTier2SetToMaximumCheckBind() {
		return cityUseTier2SetToMaximumCheckBind;
	}

	public void setCityUseTier2SetToMaximumCheckBind(HtmlSelectBooleanCheckbox cityUseTier2SetToMaximumCheckBind) {
		this.cityUseTier2SetToMaximumCheckBind = cityUseTier2SetToMaximumCheckBind;
	}
	
	public HtmlSelectBooleanCheckbox getCityUseTier1SetToMaximumCheckBind() {
		return cityUseTier1SetToMaximumCheckBind;
	}

	public void setCityUseTier1SetToMaximumCheckBind(HtmlSelectBooleanCheckbox cityUseTier1SetToMaximumCheckBind) {
		this.cityUseTier1SetToMaximumCheckBind = cityUseTier1SetToMaximumCheckBind;
	}
	
	public HtmlSelectBooleanCheckbox getCitySalesTier2TaxEntireAmountCheckBind() {
		return citySalesTier2TaxEntireAmountCheckBind;
	}

	public void setCitySalesTier2TaxEntireAmountCheckBind(HtmlSelectBooleanCheckbox citySalesTier2TaxEntireAmountCheckBind) {
		this.citySalesTier2TaxEntireAmountCheckBind = citySalesTier2TaxEntireAmountCheckBind;
	}
	
	public HtmlSelectBooleanCheckbox getCitySameAsSalesRatesCheckBind() {
		return citySameAsSalesRatesCheckBind;
	}

	public void setCitySameAsSalesRatesCheckBind(HtmlSelectBooleanCheckbox citySameAsSalesRatesCheckBind) {
		this.citySameAsSalesRatesCheckBind = citySameAsSalesRatesCheckBind;
	}
	
	public HtmlSelectBooleanCheckbox getCitySalesTier3TaxEntireAmountCheckBind() {
		return citySalesTier3TaxEntireAmountCheckBind;
	}

	public void setCitySalesTier3TaxEntireAmountCheckBind(HtmlSelectBooleanCheckbox citySalesTier3TaxEntireAmountCheckBind) {
		this.citySalesTier3TaxEntireAmountCheckBind = citySalesTier3TaxEntireAmountCheckBind;
	}
	
	public HtmlSelectBooleanCheckbox getCityUseTier2TaxEntireAmountCheckBind() {
		return cityUseTier2TaxEntireAmountCheckBind;
	}

	public void setCityUseTier2TaxEntireAmountCheckBind(HtmlSelectBooleanCheckbox cityUseTier2TaxEntireAmountCheckBind) {
		this.cityUseTier2TaxEntireAmountCheckBind = cityUseTier2TaxEntireAmountCheckBind;
	}
	
	public HtmlSelectBooleanCheckbox getCityUseTier3TaxEntireAmountCheckBind() {
		return cityUseTier3TaxEntireAmountCheckBind;
	}

	public void setCityUseTier3TaxEntireAmountCheckBind(HtmlSelectBooleanCheckbox cityUseTier3TaxEntireAmountCheckBind) {
		this.cityUseTier3TaxEntireAmountCheckBind = cityUseTier3TaxEntireAmountCheckBind;
	}
	
	public HtmlInputText getCitySalesTier2MinAmtInputText() {
		return citySalesTier2MinAmtInputText;
	}
	
  	public HtmlInputText getCitySalesTier1MaxAmtInputText() {
  		return citySalesTier1MaxAmtInputText;
  	}

  	public HtmlInputText getCitySalesTier2MaxAmtInputText() {
  		return citySalesTier2MaxAmtInputText;
  	}
  
  	public HtmlInputText getCityUseTier1MaxAmtInputText() {
  		return cityUseTier1MaxAmtInputText;
  	}

  	public void setCityUseTier1MaxAmtInputText(HtmlInputText cityUseTier1MaxAmtInputText) {
  		this.cityUseTier1MaxAmtInputText = cityUseTier1MaxAmtInputText;
  	}
	  
  	public HtmlInputText getCityUseTier2MaxAmtInputText() {
  		return cityUseTier2MaxAmtInputText ;
  	}

  	public void setCityUseTier2MaxAmtInputText(HtmlInputText cityUseTier2MaxAmtInputText) {
  		this.cityUseTier2MaxAmtInputText = cityUseTier2MaxAmtInputText ;
  	}
	  
  	public HtmlInputText getCityUseTier2MinAmtInputText () {
  		return cityUseTier2MinAmtInputText;
  	}
	
  	public void setCityUseTier2MinAmtInputText(HtmlInputText cityUseTier2MinAmtInputText) {
  		this.cityUseTier2MinAmtInputText = cityUseTier2MinAmtInputText;
  	}
  	
  	public boolean getCitySalesTier2SetToMaximumCheck() {
  		return citySalesTier2SetToMaximumCheck;
  	}

  	public void setCitySalesTier2SetToMaximumCheck(boolean citySalesTier2SetToMaximumCheck) {
  		this.citySalesTier2SetToMaximumCheck = citySalesTier2SetToMaximumCheck;
  	}

  	public boolean getCitySalesTier1SetToMaximumCheck() {
  		return citySalesTier1SetToMaximumCheck;
  	}

	public void setCitySalesTier1SetToMaximumCheck(boolean citySalesTier1SetToMaximumCheck) {
	    this.citySalesTier1SetToMaximumCheck = citySalesTier1SetToMaximumCheck;
	}
  
	public boolean getCitySameAsSalesRatesCheck() {
		return citySameAsSalesRatesCheck;
	}

	public void setCitySameAsSalesRatesCheck(boolean citySameAsSalesRatesCheck) {
		this.citySameAsSalesRatesCheck = citySameAsSalesRatesCheck;
	}

	public boolean getCityUseTier2SetToMaximumCheck() {
		return cityUseTier2SetToMaximumCheck;
	}

	public void setCityUseTier2SetToMaximumCheck(boolean cityUseTier2SetToMaximumCheck) {
		this.cityUseTier2SetToMaximumCheck = cityUseTier2SetToMaximumCheck;
	}

	public boolean getCityUseTier1SetToMaximumCheck() {
		return cityUseTier1SetToMaximumCheck;
	}

	public void setCityUseTier1SetToMaximumCheck(boolean cityUseTier1SetToMaximumCheck) {
		this.cityUseTier1SetToMaximumCheck = cityUseTier1SetToMaximumCheck;
	}
  
	public boolean getCitySalesTier2TaxEntireAmountCheck() {
		return citySalesTier2TaxEntireAmountCheck;
	}

	public void setCitySalesTier2TaxEntireAmountCheck(boolean citySalesTier2TaxEntireAmountCheck) {
		this.citySalesTier2TaxEntireAmountCheck = citySalesTier2TaxEntireAmountCheck;
	}
  
	public boolean getCitySalesTier3TaxEntireAmountCheck() {
		return citySalesTier3TaxEntireAmountCheck;
	}

	public void setCitySalesTier3TaxEntireAmountCheck(boolean citySalesTier3TaxEntireAmountCheck) {
		this.citySalesTier3TaxEntireAmountCheck = citySalesTier3TaxEntireAmountCheck;
	}
  
	public boolean getCityUseTier2TaxEntireAmountCheck() {
		return cityUseTier2TaxEntireAmountCheck;
	}

	public void setCityUseTier2TaxEntireAmountCheck(boolean cityUseTier2TaxEntireAmountCheck) {
		this.cityUseTier2TaxEntireAmountCheck = cityUseTier2TaxEntireAmountCheck;
	}
  
	public boolean getCityUseTier3TaxEntireAmountCheck() {
		return cityUseTier3TaxEntireAmountCheck;
	}

	public void setCityUseTier3TaxEntireAmountCheck(boolean cityUseTier3TaxEntireAmountCheck) {
		this.cityUseTier3TaxEntireAmountCheck = cityUseTier3TaxEntireAmountCheck;
	}
	
	public HtmlInputText getCitySalesMaxtaxAmtInputText() {
		return citySalesMaxtaxAmtInputText;
	}

	public void setCitySalesMaxtaxAmtInputText(HtmlInputText citySalesMaxtaxAmtInputText) {
		this.citySalesMaxtaxAmtInputText = citySalesMaxtaxAmtInputText;
	}
	
	public HtmlInputText getCityUseMaxtaxAmtInputText() {
		return cityUseMaxtaxAmtInputText;
	}

	public void setCityUseMaxtaxAmtInputText(HtmlInputText cityUseMaxtaxAmtInputText) {
		this.cityUseMaxtaxAmtInputText = cityUseMaxtaxAmtInputText;
	}
	//end of city
	
	//start of stj
	public void stjSameAsSalesRatesChangeListener(ActionEvent e) throws AbortProcessingException {
		HtmlSelectBooleanCheckbox chk = (HtmlSelectBooleanCheckbox) ((HtmlAjaxSupport) e.getComponent()).getParent();		
		Boolean flag = (Boolean)chk.getValue();	
    	
		stjSameAsSalesRatesCheck = flag;
		stjSameAsSalesRatesCheckBind.setSelected(flag);
	  	
    	if (flag == true) {
    		//stj Tier 1	
    		stj1UseRateInputText.setDisabled(true);  
    		stj1UseRateInputText.setReadonly(true);
		  
    		stj1UseSetamtInputText.setDisabled(true); 
    		stj1UseSetamtInputText.setReadonly(true);
		  
    		//stj Tier 2	
    		stj2UseRateInputText.setDisabled(true);  
    		stj2UseRateInputText.setReadonly(true);
		  
    		stj2UseSetamtInputText.setDisabled(true); 
    		stj2UseSetamtInputText.setReadonly(true);
    		
    		//stj Tier 3	
    		stj3UseRateInputText.setDisabled(true);  
    		stj3UseRateInputText.setReadonly(true);
		  
    		stj3UseSetamtInputText.setDisabled(true); 
    		stj3UseSetamtInputText.setReadonly(true);
    		
    		//stj Tier 4	
    		stj4UseRateInputText.setDisabled(true);  
    		stj4UseRateInputText.setReadonly(true);
		  
    		stj4UseSetamtInputText.setDisabled(true); 
    		stj4UseSetamtInputText.setReadonly(true);
    		
    		//stj Tier 5	
    		stj5UseRateInputText.setDisabled(true);  
    		stj5UseRateInputText.setReadonly(true);
		  
    		stj5UseSetamtInputText.setDisabled(true); 
    		stj5UseSetamtInputText.setReadonly(true);
    	}
    	else {
    		//stj Tier 1	
    		stj1UseRateInputText.setDisabled(false);  
    		stj1UseRateInputText.setReadonly(false);
		  
    		stj1UseSetamtInputText.setDisabled(false); 
    		stj1UseSetamtInputText.setReadonly(false);
		  
    		//stj Tier 2	
    		stj2UseRateInputText.setDisabled(false);  
    		stj2UseRateInputText.setReadonly(false);
		  
    		stj2UseSetamtInputText.setDisabled(false); 
    		stj2UseSetamtInputText.setReadonly(false);
    		
    		//stj Tier 3	
    		stj3UseRateInputText.setDisabled(false);  
    		stj3UseRateInputText.setReadonly(false);
		  
    		stj3UseSetamtInputText.setDisabled(false); 
    		stj3UseSetamtInputText.setReadonly(false);
    		
    		//stj Tier 4	
    		stj4UseRateInputText.setDisabled(false);  
    		stj4UseRateInputText.setReadonly(false);
		  
    		stj4UseSetamtInputText.setDisabled(false); 
    		stj4UseSetamtInputText.setReadonly(false);
    		
    		//stj Tier 5	
    		stj5UseRateInputText.setDisabled(false);  
    		stj5UseRateInputText.setReadonly(false);
		  
    		stj5UseSetamtInputText.setDisabled(false); 
    		stj5UseSetamtInputText.setReadonly(false);
		  
    	}	

    	recalCombinedRates(null);
    }
	
	public HtmlSelectBooleanCheckbox getStjSameAsSalesRatesCheckBind() {
		return stjSameAsSalesRatesCheckBind;
	}

	public void setstjSameAsSalesRatesCheckBind(HtmlSelectBooleanCheckbox stjSameAsSalesRatesCheckBind) {
		this.stjSameAsSalesRatesCheckBind = stjSameAsSalesRatesCheckBind;
	}

	public boolean getStjSameAsSalesRatesCheck() {
		return stjSameAsSalesRatesCheck;
	}

	public void setStjSameAsSalesRatesCheck(boolean stjSameAsSalesRatesCheck) {
		this.stjSameAsSalesRatesCheck = stjSameAsSalesRatesCheck;
	}
  
	public HtmlInputText getStj1SalesSetamtInputText() {
		return stj1SalesSetamtInputText;
	}

	public void setStj1SalesSetamtInputText(HtmlInputText stj1SalesSetamtInputText) {
		this.stj1SalesSetamtInputText = stj1SalesSetamtInputText;
	}
  
	public HtmlInputText getStj1UseSetamtInputText() {
		return stj1UseSetamtInputText;
	}

	public void setStj1UseSetamtInputText(HtmlInputText stj1UseSetamtInputText) {
		this.stj1UseSetamtInputText = stj1UseSetamtInputText;
	}
  
	public HtmlInputText getStj2SalesSetamtInputText() {
		return stj2SalesSetamtInputText;
	}

	public void setStj2SalesSetamtInputText(HtmlInputText stj2SalesSetamtInputText) {
		this.stj2SalesSetamtInputText = stj2SalesSetamtInputText;
	}
  
	public HtmlInputText getStj2UseSetamtInputText() {
		return stj2UseSetamtInputText;
	}

	public void setStj2UseSetamtInputText(HtmlInputText stj2UseSetamtInputText) {
		this.stj2UseSetamtInputText = stj2UseSetamtInputText;
	}
  
	public HtmlInputText getStj3SalesSetamtInputText() {
		return stj3SalesSetamtInputText;
	}

	public void setStj3SalesSetamtInputText(HtmlInputText stj3SalesSetamtInputText) {
		this.stj3SalesSetamtInputText = stj3SalesSetamtInputText;
	}
  
	public HtmlInputText getStj3UseSetamtInputText() {
		return stj3UseSetamtInputText;
	}

	public void setStj3UseSetamtInputText(HtmlInputText stj3UseSetamtInputText) {
		this.stj3UseSetamtInputText = stj3UseSetamtInputText;
	}
  
	public HtmlInputText getStj4SalesSetamtInputText() {
		return stj4SalesSetamtInputText;
	}

	public void setStj4SalesSetamtInputText(HtmlInputText stj4SalesSetamtInputText) {
		this.stj4SalesSetamtInputText = stj4SalesSetamtInputText;
	}
	
	public HtmlInputText getStj4UseSetamtInputText() {
		return stj4UseSetamtInputText;
	}
	
	public void setStj4UseSetamtInputText(HtmlInputText stj4UseSetamtInputText) {
		this.stj4UseSetamtInputText = stj4UseSetamtInputText;
	}
	
	public HtmlInputText getStj5SalesSetamtInputText() {
		return stj5SalesSetamtInputText;
	}

	public void setStj5SalesSetamtInputText(HtmlInputText stj5SalesSetamtInputText) {
		this.stj5SalesSetamtInputText = stj5SalesSetamtInputText;
	}
	
	public HtmlInputText getStj5UseSetamtInputText() {
		return stj5UseSetamtInputText;
	}
	
	public void setStj5UseSetamtInputText(HtmlInputText stj5UseSetamtInputText) {
		this.stj5UseSetamtInputText = stj5UseSetamtInputText;
	}
	
	public boolean getIsSTJDisabled() {
	    return (editJurisdiction.getStj1Name()==null || editJurisdiction.getStj1Name().length()==0);
	}	
	//end of stj
	
	public boolean getDisplaySameasMessage() {
		return (tabSelected.equalsIgnoreCase("countrytab") && countrySameAsSalesRatesCheck) || (tabSelected.equalsIgnoreCase("statetab") && stateSameAsSalesRatesCheck) ||
				(tabSelected.equalsIgnoreCase("countytab") && countySameAsSalesRatesCheck)  || (tabSelected.equalsIgnoreCase("citytab") && citySameAsSalesRatesCheck)  ||
				(tabSelected.equalsIgnoreCase("stjstab") && stjSameAsSalesRatesCheck) ;
	}

  /**
   * @param taxCodeStateService the taxCodeStateService to set
   */
  public void setTaxCodeStateService(TaxCodeStateService taxCodeStateService) {
    this.taxCodeStateService = taxCodeStateService;
  }

  public void setJurisdictionService(JurisdictionService jurisdictionService) {
    this.jurisdictionService = jurisdictionService;
    
    //4824
    //buildStateSelectionMenu();
  }

  public void settaxRateList(List<JurisdictionTaxrate> taxRateList) {
    this.taxRateList = taxRateList;
  }

  public void setJurisdictionTaxrateService(JurisdictionTaxrateService taxRateService) {
    this.taxRateService = taxRateService;
  }

  /**
   * @param trShowSection the trShowSection to set
   */
  public void setTrShowSection(String trShowSection) {
    this.trShowSection = trShowSection;
  }

  /*
   * **************************************** Initialization Methods
   * ****************************************
   */

  private void generateNewActionMap() {
    HashMap<String, FieldOptions> fieldToOptionsMap;

    // viewJurisdiction map
    fieldToOptionsMap = new HashMap<String, FieldOptions>();
    newActionMap.put(TaxJurisdictionAction.viewJurisdictionTaxrate, fieldToOptionsMap);

    fieldToOptionsMap.put("JURISDICTIONID", new FieldOptions(true, false));
    fieldToOptionsMap.put("GEOCODE", new FieldOptions(true, false));
    fieldToOptionsMap.put("COUNTRY", new FieldOptions(true, true));
    fieldToOptionsMap.put("STATE", new FieldOptions(true, true));
    fieldToOptionsMap.put("COUNTY", new FieldOptions(true, false));
    fieldToOptionsMap.put("CITY", new FieldOptions(true, false));
    fieldToOptionsMap.put("ZIP", new FieldOptions(true, false));
    fieldToOptionsMap.put("ZIPPLUS4", new FieldOptions(true, false));
    fieldToOptionsMap.put("INOUT", new FieldOptions(true, true));
    fieldToOptionsMap.put("CUSTOMFLAG", new FieldOptions(true, true));
    fieldToOptionsMap.put("DESCRIPTION", new FieldOptions(true, false));
    fieldToOptionsMap.put("CLIENTGEOCODE", new FieldOptions(true, false));
    fieldToOptionsMap.put("COMPGEOCODE", new FieldOptions(true, false));
    fieldToOptionsMap.put("UPDATEUSERID", new FieldOptions(true, true));
    fieldToOptionsMap.put("UPDATETIMESTAMP", new FieldOptions(true, true));
    fieldToOptionsMap.put("REPORTINGCITY", new FieldOptions(true, true));
    fieldToOptionsMap.put("REPORTINGCITYFIPS", new FieldOptions(true, true));
    fieldToOptionsMap.put("STJ1NAME", new FieldOptions(true, true));
    fieldToOptionsMap.put("STJ2NAME", new FieldOptions(true, true));
    fieldToOptionsMap.put("STJ3NAME", new FieldOptions(true, true));
    fieldToOptionsMap.put("STJ4NAME", new FieldOptions(true, true));
    fieldToOptionsMap.put("STJ5NAME", new FieldOptions(true, true));
    fieldToOptionsMap.put("MODIFIED_FLAG", new FieldOptions(true, true));
    fieldToOptionsMap.put("EFFECTIVEDATE", new FieldOptions(true, true));
    fieldToOptionsMap.put("EXPIRATIONDATE", new FieldOptions(true, true));
    fieldToOptionsMap.put("COUNTRYNEXUSINDCODE", new FieldOptions(true, true));
    fieldToOptionsMap.put("STATENEXUSINDCODE", new FieldOptions(true, true));
    fieldToOptionsMap.put("COUNTYNEXUSINDCODE", new FieldOptions(true, true));
    fieldToOptionsMap.put("CITYNEXUSINDCODE", new FieldOptions(true, true));
    fieldToOptionsMap.put("STJ1NEXUSINDCODE", new FieldOptions(true, true));
    fieldToOptionsMap.put("STJ2NEXUSINDCODE", new FieldOptions(true, true));
    fieldToOptionsMap.put("STJ3NEXUSINDCODE", new FieldOptions(true, true));
    fieldToOptionsMap.put("STJ4NEXUSINDCODE", new FieldOptions(true, true));
    fieldToOptionsMap.put("STJ5NEXUSINDCODE", new FieldOptions(true, true));
    fieldToOptionsMap.put("STJ1LOCALCODE", new FieldOptions(true, true));
    fieldToOptionsMap.put("STJ2LOCALCODE", new FieldOptions(true, true));
    fieldToOptionsMap.put("STJ3LOCALCODE", new FieldOptions(true, true));
    fieldToOptionsMap.put("STJ4LOCALCODE", new FieldOptions(true, true));
    fieldToOptionsMap.put("STJ5LOCALCODE", new FieldOptions(true, true));

    fieldToOptionsMap.put("JURISDICTIONTAXRATEID", new FieldOptions(true, false));
    fieldToOptionsMap.put("RATETYPECODE", new FieldOptions(true, true));
    fieldToOptionsMap.put("EFFECTIVEDATE", new FieldOptions(true, true));
    fieldToOptionsMap.put("EXPIRATIONDATE", new FieldOptions(true, true));
    fieldToOptionsMap.put("MODIFIEDFLAG", new FieldOptions(true, true));
    fieldToOptionsMap.put("STATESALESRATE", new FieldOptions(true, false));
    
    fieldToOptionsMap.put("STATEUSERATE", new FieldOptions(true, false));
    fieldToOptionsMap.put("STATEUSETIER2RATE", new FieldOptions(true, false));
    fieldToOptionsMap.put("STATEUSETIER3RATE", new FieldOptions(true, false));
    fieldToOptionsMap.put("STATESALESTIER2RATE", new FieldOptions(true, false));
    fieldToOptionsMap.put("STATESALESTIER3RATE", new FieldOptions(true, false));
    fieldToOptionsMap.put("STATESPLITAMOUNT", new FieldOptions(true, false));
    fieldToOptionsMap.put("STATETIER2MINAMOUNT", new FieldOptions(true, false));
    fieldToOptionsMap.put("STATETIER2MAXAMOUNT", new FieldOptions(true, false));
    fieldToOptionsMap.put("STATEMAXTAXAMOUNT", new FieldOptions(true, false));
    fieldToOptionsMap.put("COUNTYSALESRATE", new FieldOptions(true, false));
    fieldToOptionsMap.put("COUNTYUSERATE", new FieldOptions(true, false));
    fieldToOptionsMap.put("COUNTYSPLITAMOUNT", new FieldOptions(true, false));
    fieldToOptionsMap.put("COUNTYMAXTAXAMOUNT", new FieldOptions(true, false));
    fieldToOptionsMap.put("COUNTYSINGLEFLAG", new FieldOptions(true, true));
    fieldToOptionsMap.put("COUNTYDEFAULTFLAG", new FieldOptions(true, true));
    fieldToOptionsMap.put("CITYSALESRATE", new FieldOptions(true, false));
    fieldToOptionsMap.put("CITYUSERATE", new FieldOptions(true, false));
    fieldToOptionsMap.put("CITYSPLITAMOUNT", new FieldOptions(true, false));
    fieldToOptionsMap.put("CITYSPLITSALESRATE", new FieldOptions(true, false));
    fieldToOptionsMap.put("CITYSPLITUSERATE", new FieldOptions(true, false));
    fieldToOptionsMap.put("CITYSINGLEFLAG", new FieldOptions(true, true));
    fieldToOptionsMap.put("CITYDEFAULTFLAG", new FieldOptions(true, true));

    fieldToOptionsMap.put("STATETIER1MINAMOUNT", new FieldOptions(true, true));
    fieldToOptionsMap.put("STATETIER1MAXFLAG", new FieldOptions(true, true));
    
    fieldToOptionsMap.put("STATETIER2MAXFLAG", new FieldOptions(true, true));
    fieldToOptionsMap.put("STATETIER3MAXAMOUNT", new FieldOptions(true, true));
    
    fieldToOptionsMap.put("STJ1USERATE", new FieldOptions(true, true));
    fieldToOptionsMap.put("STJ2USERATE", new FieldOptions(true, true));
    fieldToOptionsMap.put("STJ3USERATE", new FieldOptions(true, true));
    fieldToOptionsMap.put("STJ4USERATE", new FieldOptions(true, true));
    fieldToOptionsMap.put("STJ5USERATE", new FieldOptions(true, true));
    
    fieldToOptionsMap.put("STJ1SALESRATE", new FieldOptions(true, true));
    fieldToOptionsMap.put("STJ2SALESRATE", new FieldOptions(true, true));
    fieldToOptionsMap.put("STJ3SALESRATE", new FieldOptions(true, true));
    fieldToOptionsMap.put("STJ4SALESRATE", new FieldOptions(true, true));
    fieldToOptionsMap.put("STJ5SALESRATE", new FieldOptions(true, true));
    
    fieldToOptionsMap.put("STJ1USESETAMT", new FieldOptions(true, true));
    fieldToOptionsMap.put("STJ2USESETAMT", new FieldOptions(true, true));
    fieldToOptionsMap.put("STJ3USESETAMT", new FieldOptions(true, true));
    fieldToOptionsMap.put("STJ4USESETAMT", new FieldOptions(true, true));
    fieldToOptionsMap.put("STJ5USESETAMT", new FieldOptions(true, true));

    fieldToOptionsMap.put("STJ1SALESSETAMT", new FieldOptions(true, true));
    fieldToOptionsMap.put("STJ2SALESSETAMT", new FieldOptions(true, true));
    fieldToOptionsMap.put("STJ3SALESSETAMT", new FieldOptions(true, true));
    fieldToOptionsMap.put("STJ4SALESSETAMT", new FieldOptions(true, true));
    fieldToOptionsMap.put("STJ5SALESSETAMT", new FieldOptions(true, true));
    

    // deleteJurisdiction map
    newActionMap.put(TaxJurisdictionAction.deleteJurisdiction,
        (HashMap<String, FieldOptions>) fieldToOptionsMap.clone());
    
    // viewJurisdiction map
    newActionMap.put(TaxJurisdictionAction.viewJurisdiction,
        (HashMap<String, FieldOptions>) fieldToOptionsMap.clone());
    
    // viewJurisdictionFromTransaction map
    newActionMap.put(TaxJurisdictionAction.viewJurisdictionFromTransaction,
        (HashMap<String, FieldOptions>) fieldToOptionsMap.clone());
    
    // viewJurisdictionFromTransactionLog map
    newActionMap.put(TaxJurisdictionAction.viewJurisdictionFromTransactionLog,
        (HashMap<String, FieldOptions>) fieldToOptionsMap.clone());

    // deleteJurisdictionTaxrate map
    newActionMap.put(TaxJurisdictionAction.deleteJurisdictionTaxrate,
        (HashMap<String, FieldOptions>) fieldToOptionsMap.clone());
    
    // deleteJurisdiction map
    newActionMap.put(TaxJurisdictionAction.viewJurisdictionTaxrateFromTransaction,
        (HashMap<String, FieldOptions>) fieldToOptionsMap.clone());

    // addJurisdiction map
    fieldToOptionsMap = new HashMap<String, FieldOptions>();
    newActionMap.put(TaxJurisdictionAction.addJurisdiction, fieldToOptionsMap);

    fieldToOptionsMap.put("JURISDICTIONID", new FieldOptions(true, true));
    fieldToOptionsMap.put("GEOCODE", new FieldOptions(false, false));
    fieldToOptionsMap.put("STATE", new FieldOptions(false, false));
    fieldToOptionsMap.put("COUNTY", new FieldOptions(false, false));
    fieldToOptionsMap.put("CITY", new FieldOptions(false, false));
    fieldToOptionsMap.put("ZIP", new FieldOptions(false, false));
    fieldToOptionsMap.put("ZIPPLUS4", new FieldOptions(false, false));
    fieldToOptionsMap.put("INOUT", new FieldOptions(false, false));
    fieldToOptionsMap.put("CUSTOMFLAG", new FieldOptions(false, false));
    fieldToOptionsMap.put("DESCRIPTION", new FieldOptions(false, false));
    fieldToOptionsMap.put("CLIENTGEOCODE", new FieldOptions(false, false));
    fieldToOptionsMap.put("COMPGEOCODE", new FieldOptions(false, false));
    fieldToOptionsMap.put("UPDATEUSERID", new FieldOptions(false, true));
    fieldToOptionsMap.put("UPDATETIMESTAMP", new FieldOptions(false, true));
    fieldToOptionsMap.put("REPORTINGCITY", new FieldOptions(false, false));
    fieldToOptionsMap.put("REPORTINGCITYFIPS", new FieldOptions(false, false));
    fieldToOptionsMap.put("STJ1NAME", new FieldOptions(false, false));
    fieldToOptionsMap.put("STJ2NAME", new FieldOptions(false, false));
    fieldToOptionsMap.put("STJ3NAME", new FieldOptions(false, false));
    fieldToOptionsMap.put("STJ4NAME", new FieldOptions(false, false));
    fieldToOptionsMap.put("STJ5NAME", new FieldOptions(false, false));
    fieldToOptionsMap.put("MODIFIED_FLAG", new FieldOptions(false, false));
    fieldToOptionsMap.put("EFFECTIVEDATE", new FieldOptions(false, false));
    fieldToOptionsMap.put("EXPIRATIONDATE", new FieldOptions(false, false));
    fieldToOptionsMap.put("COUNTRYNEXUSINDCODE", new FieldOptions(false, false));
    fieldToOptionsMap.put("STATENEXUSINDCODE", new FieldOptions(false, false));
    fieldToOptionsMap.put("COUNTYNEXUSINDCODE", new FieldOptions(false, false));
    fieldToOptionsMap.put("CITYNEXUSINDCODE", new FieldOptions(false, false));
    fieldToOptionsMap.put("STJ1NEXUSINDCODE", new FieldOptions(false, false));
    fieldToOptionsMap.put("STJ2NEXUSINDCODE", new FieldOptions(false, false));
    fieldToOptionsMap.put("STJ3NEXUSINDCODE", new FieldOptions(false, false));
    fieldToOptionsMap.put("STJ4NEXUSINDCODE", new FieldOptions(false, false));
    fieldToOptionsMap.put("STJ5NEXUSINDCODE", new FieldOptions(false, false));  
    fieldToOptionsMap.put("STJ1LOCALCODE", new FieldOptions(false, false));
    fieldToOptionsMap.put("STJ2LOCALCODE", new FieldOptions(false, false));
    fieldToOptionsMap.put("STJ3LOCALCODE", new FieldOptions(false, false));
    fieldToOptionsMap.put("STJ4LOCALCODE", new FieldOptions(false, false));
    fieldToOptionsMap.put("STJ5LOCALCODE", new FieldOptions(false, false));

    fieldToOptionsMap.put("JURISDICTIONTAXRATEID", new FieldOptions(true, true));
    fieldToOptionsMap.put("RATETYPECODE", new FieldOptions(false, false));
    fieldToOptionsMap.put("EFFECTIVEDATE", new FieldOptions(false, false));
    fieldToOptionsMap.put("EXPIRATIONDATE", new FieldOptions(false, false));
    fieldToOptionsMap.put("MODIFIEDFLAG", new FieldOptions(false, false));
    fieldToOptionsMap.put("STATESALESRATE", new FieldOptions(false, false));

    fieldToOptionsMap.put("STATEUSERATE", new FieldOptions(false, false));
    fieldToOptionsMap.put("STATEUSETIER2RATE", new FieldOptions(false, false));
    fieldToOptionsMap.put("STATEUSETIER3RATE", new FieldOptions(false, false));
    fieldToOptionsMap.put("STATESALESTIER2RATE", new FieldOptions(false, false));
    fieldToOptionsMap.put("STATESALESTIER3RATE", new FieldOptions(false, false));
    fieldToOptionsMap.put("STATESPLITAMOUNT", new FieldOptions(false, false));
    fieldToOptionsMap.put("STATETIER2MINAMOUNT", new FieldOptions(false, false));
    fieldToOptionsMap.put("STATETIER2MAXAMOUNT", new FieldOptions(false, false));
    fieldToOptionsMap.put("STATEMAXTAXAMOUNT", new FieldOptions(false, false));
    fieldToOptionsMap.put("COUNTYSALESRATE", new FieldOptions(false, false));
    fieldToOptionsMap.put("COUNTYUSERATE", new FieldOptions(false, false));
    fieldToOptionsMap.put("COUNTYSPLITAMOUNT", new FieldOptions(false, false));
    fieldToOptionsMap.put("COUNTYMAXTAXAMOUNT", new FieldOptions(false, false));
    fieldToOptionsMap.put("COUNTYSINGLEFLAG", new FieldOptions(false, false));
    fieldToOptionsMap.put("COUNTYDEFAULTFLAG", new FieldOptions(false, false));
    fieldToOptionsMap.put("CITYSALESRATE", new FieldOptions(false, false));
    fieldToOptionsMap.put("CITYUSERATE", new FieldOptions(false, false));
    fieldToOptionsMap.put("CITYSPLITAMOUNT", new FieldOptions(false, false));
    fieldToOptionsMap.put("CITYSPLITSALESRATE", new FieldOptions(false, false));
    fieldToOptionsMap.put("CITYSPLITUSERATE", new FieldOptions(false, false));
    fieldToOptionsMap.put("CITYSINGLEFLAG", new FieldOptions(false, false));
    fieldToOptionsMap.put("CITYDEFAULTFLAG", new FieldOptions(false, false));

    fieldToOptionsMap.put("STATETIER1MINAMOUNT", new FieldOptions(true, true));
    fieldToOptionsMap.put("STATETIER1MAXFLAG", new FieldOptions(false, false));
    
    fieldToOptionsMap.put("STATETIER2MAXFLAG", new FieldOptions(false, false));
    fieldToOptionsMap.put("STATETIER3MAXAMOUNT", new FieldOptions(true, true));
    
    fieldToOptionsMap.put("STJ1USERATE", new FieldOptions(false, false));
    fieldToOptionsMap.put("STJ2USERATE", new FieldOptions(false, false));
    fieldToOptionsMap.put("STJ3USERATE", new FieldOptions(false, false));
    fieldToOptionsMap.put("STJ4USERATE", new FieldOptions(false, false));
    fieldToOptionsMap.put("STJ5USERATE", new FieldOptions(false, false));
    
    fieldToOptionsMap.put("STJ1SALESRATE", new FieldOptions(false, false));
    fieldToOptionsMap.put("STJ2SALESRATE", new FieldOptions(false, false));
    fieldToOptionsMap.put("STJ3SALESRATE", new FieldOptions(false, false));
    fieldToOptionsMap.put("STJ4SALESRATE", new FieldOptions(false, false));
    fieldToOptionsMap.put("STJ5SALESRATE", new FieldOptions(false, false));
    
    fieldToOptionsMap.put("STJ1USESETAMT", new FieldOptions(false, false));
    fieldToOptionsMap.put("STJ2USESETAMT", new FieldOptions(false, false));
    fieldToOptionsMap.put("STJ3USESETAMT", new FieldOptions(false, false));
    fieldToOptionsMap.put("STJ4USESETAMT", new FieldOptions(false, false));
    fieldToOptionsMap.put("STJ5USESETAMT", new FieldOptions(false, false));

    fieldToOptionsMap.put("STJ1SALESSETAMT", new FieldOptions(false, false));
    fieldToOptionsMap.put("STJ2SALESSETAMT", new FieldOptions(false, false));
    fieldToOptionsMap.put("STJ3SALESSETAMT", new FieldOptions(false, false));
    fieldToOptionsMap.put("STJ4SALESSETAMT", new FieldOptions(false, false));
    fieldToOptionsMap.put("STJ5SALESSETAMT", new FieldOptions(false, false));

    // copyAddJurisdiction map
    newActionMap.put(TaxJurisdictionAction.copyAddJurisdiction,
        (HashMap<String, FieldOptions>) fieldToOptionsMap.clone());

    // updateJurisdiction map
    newActionMap.put(TaxJurisdictionAction.updateJurisdiction,
        (HashMap<String, FieldOptions>) fieldToOptionsMap.clone());

    // addJurisdictionTaxrate map
    fieldToOptionsMap = new HashMap<String, FieldOptions>();
    newActionMap.put(TaxJurisdictionAction.addJurisdictionTaxrate, fieldToOptionsMap);

    fieldToOptionsMap.put("JURISDICTIONID", new FieldOptions(true, true));
    fieldToOptionsMap.put("GEOCODE", new FieldOptions(true, true));
    fieldToOptionsMap.put("STATE", new FieldOptions(true, true));
    fieldToOptionsMap.put("COUNTY", new FieldOptions(true, true));
    fieldToOptionsMap.put("CITY", new FieldOptions(true, true));
    fieldToOptionsMap.put("ZIP", new FieldOptions(true, true));
    fieldToOptionsMap.put("ZIPPLUS4", new FieldOptions(true, true));
    fieldToOptionsMap.put("INOUT", new FieldOptions(true, true));
    fieldToOptionsMap.put("CUSTOMFLAG", new FieldOptions(true, true));
    fieldToOptionsMap.put("DESCRIPTION", new FieldOptions(true, true));
    fieldToOptionsMap.put("CLIENTGEOCODE", new FieldOptions(true, true));
    fieldToOptionsMap.put("COMPGEOCODE", new FieldOptions(true, true));
    fieldToOptionsMap.put("UPDATEUSERID", new FieldOptions(true, true));
    fieldToOptionsMap.put("UPDATETIMESTAMP", new FieldOptions(true, true));
    fieldToOptionsMap.put("REPORTINGCITY", new FieldOptions(true, true));
    fieldToOptionsMap.put("REPORTINGCITYFIPS", new FieldOptions(true, true));
    fieldToOptionsMap.put("STJ1NAME", new FieldOptions(true, true));
    fieldToOptionsMap.put("STJ2NAME", new FieldOptions(true, true));
    fieldToOptionsMap.put("STJ3NAME", new FieldOptions(true, true));
    fieldToOptionsMap.put("STJ4NAME", new FieldOptions(true, true));
    fieldToOptionsMap.put("STJ5NAME", new FieldOptions(true, true));
    fieldToOptionsMap.put("MODIFIED_FLAG", new FieldOptions(true, true));
    fieldToOptionsMap.put("EFFECTIVEDATE", new FieldOptions(true, true));
    fieldToOptionsMap.put("EXPIRATIONDATE", new FieldOptions(true, true));
    fieldToOptionsMap.put("COUNTRYNEXUSINDCODE", new FieldOptions(true, true));
    fieldToOptionsMap.put("STATENEXUSINDCODE", new FieldOptions(true, true));
    fieldToOptionsMap.put("COUNTYNEXUSINDCODE", new FieldOptions(true, true));
    fieldToOptionsMap.put("CITYNEXUSINDCODE", new FieldOptions(true, true));
    fieldToOptionsMap.put("STJ1NEXUSINDCODE", new FieldOptions(true, true));
    fieldToOptionsMap.put("STJ2NEXUSINDCODE", new FieldOptions(true, true));
    fieldToOptionsMap.put("STJ3NEXUSINDCODE", new FieldOptions(true, true));
    fieldToOptionsMap.put("STJ4NEXUSINDCODE", new FieldOptions(true, true));
    fieldToOptionsMap.put("STJ5NEXUSINDCODE", new FieldOptions(true, true));   
    fieldToOptionsMap.put("STJ1LOCALCODE", new FieldOptions(true, true));
    fieldToOptionsMap.put("STJ2LOCALCODE", new FieldOptions(true, true));
    fieldToOptionsMap.put("STJ3LOCALCODE", new FieldOptions(true, true));
    fieldToOptionsMap.put("STJ4LOCALCODE", new FieldOptions(true, true));
    fieldToOptionsMap.put("STJ5LOCALCODE", new FieldOptions(true, true));

    fieldToOptionsMap.put("JURISDICTIONTAXRATEID", new FieldOptions(true, true));
    fieldToOptionsMap.put("RATETYPECODE", new FieldOptions(false, false));
    fieldToOptionsMap.put("EFFECTIVEDATE", new FieldOptions(false, false));
    fieldToOptionsMap.put("EXPIRATIONDATE", new FieldOptions(false, false));
    fieldToOptionsMap.put("MODIFIEDFLAG", new FieldOptions(false, false));
    fieldToOptionsMap.put("STATESALESRATE", new FieldOptions(false, false));

    //PP-24
    //COUNTRY
    fieldToOptionsMap.put("COUNTRYSALESTIER1RATE", new FieldOptions(false, false));
    fieldToOptionsMap.put("COUNTRYSALESTIER1SETAMT", new FieldOptions(false, false));
    fieldToOptionsMap.put("COUNTRYSALESTIER1MAXAMT", new FieldOptions(false, false));
    fieldToOptionsMap.put("COUNTRYSALESTIER2MINAMT", new FieldOptions(false, false));
    fieldToOptionsMap.put("COUNTRYSALESTIER2RATE", new FieldOptions(false, false));
    fieldToOptionsMap.put("COUNTRYSALESTIER2SETAMT", new FieldOptions(false, false));
    fieldToOptionsMap.put("COUNTRYSALESTIER2MAXAMT", new FieldOptions(false, false));
    fieldToOptionsMap.put("COUNTRYSALESTIER2ENTAMFLAG", new FieldOptions(false, false));
    fieldToOptionsMap.put("COUNTRYSALESTIER3RATE", new FieldOptions(false, false));
    fieldToOptionsMap.put("COUNTRYSALESTIER3SETAMT", new FieldOptions(false, false));
    fieldToOptionsMap.put("COUNTRYSALESTIER3ENTAMFLAG", new FieldOptions(false, false));
    fieldToOptionsMap.put("COUNTRYSALESTIER1SETTOMAXIMUMCHECK", new FieldOptions(false, false));
    fieldToOptionsMap.put("COUNTRYSALESTIER2SETTOMAXIMUMCHECK", new FieldOptions(false, false));
    
    fieldToOptionsMap.put("COUNTRYUSETIER1RATE", new FieldOptions(false, false));
    fieldToOptionsMap.put("COUNTRYUSETIER1SETAMT", new FieldOptions(false, false));
    fieldToOptionsMap.put("COUNTRYUSETIER1MAXAMT", new FieldOptions(false, false));
    fieldToOptionsMap.put("COUNTRYUSETIER2MINAMT", new FieldOptions(false, false));
    fieldToOptionsMap.put("COUNTRYUSETIER2RATE", new FieldOptions(false, false));
    fieldToOptionsMap.put("COUNTRYUSETIER2SETAMT", new FieldOptions(false, false));
    fieldToOptionsMap.put("COUNTRYUSETIER2MAXAMT", new FieldOptions(false, false));
    fieldToOptionsMap.put("COUNTRYUSETIER2ENTAMFLAG", new FieldOptions(false, false));
    fieldToOptionsMap.put("COUNTRYUSETIER3RATE", new FieldOptions(false, false));
    fieldToOptionsMap.put("COUNTRYUSETIER3SETAMT", new FieldOptions(false, false));
    fieldToOptionsMap.put("COUNTRYUSETIER3ENTAMFLAG", new FieldOptions(false, false));
    fieldToOptionsMap.put("COUNTRYUSETIER1SETTOMAXIMUMCHECK", new FieldOptions(false, false));
    fieldToOptionsMap.put("COUNTRYUSETIER2SETTOMAXIMUMCHECK", new FieldOptions(false, false));
    
    //STATE
    fieldToOptionsMap.put("STATESALESTIER1RATE", new FieldOptions(false, false));
    fieldToOptionsMap.put("STATESALESTIER1SETAMT", new FieldOptions(false, false));
    fieldToOptionsMap.put("STATESALESTIER1MAXAMT", new FieldOptions(false, false));
    fieldToOptionsMap.put("STATESALESTIER2MINAMT", new FieldOptions(false, false));
    fieldToOptionsMap.put("STATESALESTIER2RATE", new FieldOptions(false, false));
    fieldToOptionsMap.put("STATESALESTIER2SETAMT", new FieldOptions(false, false));
    fieldToOptionsMap.put("STATESALESTIER2MAXAMT", new FieldOptions(false, false));
    fieldToOptionsMap.put("STATESALESTIER2ENTAMFLAG", new FieldOptions(false, false));
    fieldToOptionsMap.put("STATESALESTIER3RATE", new FieldOptions(false, false));
    fieldToOptionsMap.put("STATESALESTIER3SETAMT", new FieldOptions(false, false));
    fieldToOptionsMap.put("STATESALESTIER3ENTAMFLAG", new FieldOptions(false, false));
    fieldToOptionsMap.put("STATESALESTIER1SETTOMAXIMUMCHECK", new FieldOptions(false, false));
    fieldToOptionsMap.put("STATESALESTIER2SETTOMAXIMUMCHECK", new FieldOptions(false, false));
    
    fieldToOptionsMap.put("STATEUSETIER1RATE", new FieldOptions(false, false));
    fieldToOptionsMap.put("STATEUSETIER1SETAMT", new FieldOptions(false, false));
    fieldToOptionsMap.put("STATEUSETIER1MAXAMT", new FieldOptions(false, false));
    fieldToOptionsMap.put("STATEUSETIER2MINAMT", new FieldOptions(false, false));
    fieldToOptionsMap.put("STATEUSETIER2RATE", new FieldOptions(false, false));
    fieldToOptionsMap.put("STATEUSETIER2SETAMT", new FieldOptions(false, false));
    fieldToOptionsMap.put("STATEUSETIER2MAXAMT", new FieldOptions(false, false));
    fieldToOptionsMap.put("STATEUSETIER2ENTAMFLAG", new FieldOptions(false, false));
    fieldToOptionsMap.put("STATEUSETIER3RATE", new FieldOptions(false, false));
    fieldToOptionsMap.put("STATEUSETIER3SETAMT", new FieldOptions(false, false));
    fieldToOptionsMap.put("STATEUSETIER3ENTAMFLAG", new FieldOptions(false, false));
    fieldToOptionsMap.put("STATEUSETIER1SETTOMAXIMUMCHECK", new FieldOptions(false, false));
    fieldToOptionsMap.put("STATEUSETIER2SETTOMAXIMUMCHECK", new FieldOptions(false, false));
    
    //COUNTY
    fieldToOptionsMap.put("COUNTYSALESTIER1RATE", new FieldOptions(false, false));
    fieldToOptionsMap.put("COUNTYSALESTIER1SETAMT", new FieldOptions(false, false));
    fieldToOptionsMap.put("COUNTYSALESTIER1MAXAMT", new FieldOptions(false, false));
    fieldToOptionsMap.put("COUNTYSALESTIER2MINAMT", new FieldOptions(false, false));
    fieldToOptionsMap.put("COUNTYSALESTIER2RATE", new FieldOptions(false, false));
    fieldToOptionsMap.put("COUNTYSALESTIER2SETAMT", new FieldOptions(false, false));
    fieldToOptionsMap.put("COUNTYSALESTIER2MAXAMT", new FieldOptions(false, false));
    fieldToOptionsMap.put("COUNTYSALESTIER2ENTAMFLAG", new FieldOptions(false, false));
    fieldToOptionsMap.put("COUNTYSALESTIER3RATE", new FieldOptions(false, false));
    fieldToOptionsMap.put("COUNTYSALESTIER3SETAMT", new FieldOptions(false, false));
    fieldToOptionsMap.put("COUNTYSALESTIER3ENTAMFLAG", new FieldOptions(false, false));
    fieldToOptionsMap.put("COUNTYSALESTIER1SETTOMAXIMUMCHECK", new FieldOptions(false, false));
    fieldToOptionsMap.put("COUNTYSALESTIER2SETTOMAXIMUMCHECK", new FieldOptions(false, false));
    
    fieldToOptionsMap.put("COUNTYUSETIER1RATE", new FieldOptions(false, false));
    fieldToOptionsMap.put("COUNTYUSETIER1SETAMT", new FieldOptions(false, false));
    fieldToOptionsMap.put("COUNTYUSETIER1MAXAMT", new FieldOptions(false, false));
    fieldToOptionsMap.put("COUNTYUSETIER2MINAMT", new FieldOptions(false, false));
    fieldToOptionsMap.put("COUNTYUSETIER2RATE", new FieldOptions(false, false));
    fieldToOptionsMap.put("COUNTYUSETIER2SETAMT", new FieldOptions(false, false));
    fieldToOptionsMap.put("COUNTYUSETIER2MAXAMT", new FieldOptions(false, false));
    fieldToOptionsMap.put("COUNTYUSETIER2ENTAMFLAG", new FieldOptions(false, false));
    fieldToOptionsMap.put("COUNTYUSETIER3RATE", new FieldOptions(false, false));
    fieldToOptionsMap.put("COUNTYUSETIER3SETAMT", new FieldOptions(false, false));
    fieldToOptionsMap.put("COUNTYUSETIER3ENTAMFLAG", new FieldOptions(false, false));
    fieldToOptionsMap.put("COUNTYUSETIER1SETTOMAXIMUMCHECK", new FieldOptions(false, false));
    fieldToOptionsMap.put("COUNTYUSETIER2SETTOMAXIMUMCHECK", new FieldOptions(false, false));
    
    //CITY
    fieldToOptionsMap.put("CITYSALESTIER1RATE", new FieldOptions(false, false));
    fieldToOptionsMap.put("CITYSALESTIER1SETAMT", new FieldOptions(false, false));
    fieldToOptionsMap.put("CITYSALESTIER1MAXAMT", new FieldOptions(false, false));
    fieldToOptionsMap.put("CITYSALESTIER2MINAMT", new FieldOptions(false, false));
    fieldToOptionsMap.put("CITYSALESTIER2RATE", new FieldOptions(false, false));
    fieldToOptionsMap.put("CITYSALESTIER2SETAMT", new FieldOptions(false, false));
    fieldToOptionsMap.put("CITYSALESTIER2MAXAMT", new FieldOptions(false, false));
    fieldToOptionsMap.put("CITYSALESTIER2ENTAMFLAG", new FieldOptions(false, false));
    fieldToOptionsMap.put("CITYSALESTIER3RATE", new FieldOptions(false, false));
    fieldToOptionsMap.put("CITYSALESTIER3SETAMT", new FieldOptions(false, false));
    fieldToOptionsMap.put("CITYSALESTIER3ENTAMFLAG", new FieldOptions(false, false));
    fieldToOptionsMap.put("CITYSALESTIER1SETTOMAXIMUMCHECK", new FieldOptions(false, false));
    fieldToOptionsMap.put("CITYSALESTIER2SETTOMAXIMUMCHECK", new FieldOptions(false, false));
    
    fieldToOptionsMap.put("CITYUSETIER1RATE", new FieldOptions(false, false));
    fieldToOptionsMap.put("CITYUSETIER1SETAMT", new FieldOptions(false, false));
    fieldToOptionsMap.put("CITYUSETIER1MAXAMT", new FieldOptions(false, false));
    fieldToOptionsMap.put("CITYUSETIER2MINAMT", new FieldOptions(false, false));
    fieldToOptionsMap.put("CITYUSETIER2RATE", new FieldOptions(false, false));
    fieldToOptionsMap.put("CITYUSETIER2SETAMT", new FieldOptions(false, false));
    fieldToOptionsMap.put("CITYUSETIER2MAXAMT", new FieldOptions(false, false));
    fieldToOptionsMap.put("CITYUSETIER2ENTAMFLAG", new FieldOptions(false, false));
    fieldToOptionsMap.put("CITYUSETIER3RATE", new FieldOptions(false, false));
    fieldToOptionsMap.put("CITYUSETIER3SETAMT", new FieldOptions(false, false));
    fieldToOptionsMap.put("CITYUSETIER3ENTAMFLAG", new FieldOptions(false, false));
    fieldToOptionsMap.put("CITYUSETIER1SETTOMAXIMUMCHECK", new FieldOptions(false, false));
    fieldToOptionsMap.put("CITYUSETIER2SETTOMAXIMUMCHECK", new FieldOptions(false, false));
    
    fieldToOptionsMap.put("STJ1USERATE", new FieldOptions(false, false));
    fieldToOptionsMap.put("STJ2USERATE", new FieldOptions(false, false));
    fieldToOptionsMap.put("STJ3USERATE", new FieldOptions(false, false));
    fieldToOptionsMap.put("STJ4USERATE", new FieldOptions(false, false));
    fieldToOptionsMap.put("STJ5USERATE", new FieldOptions(false, false));
    
    fieldToOptionsMap.put("STJ1SALESRATE", new FieldOptions(false, false));
    fieldToOptionsMap.put("STJ2SALESRATE", new FieldOptions(false, false));
    fieldToOptionsMap.put("STJ3SALESRATE", new FieldOptions(false, false));
    fieldToOptionsMap.put("STJ4SALESRATE", new FieldOptions(false, false));
    fieldToOptionsMap.put("STJ5SALESRATE", new FieldOptions(false, false));
    
    fieldToOptionsMap.put("STJ1USESETAMT", new FieldOptions(false, false));
    fieldToOptionsMap.put("STJ2USESETAMT", new FieldOptions(false, false));
    fieldToOptionsMap.put("STJ3USESETAMT", new FieldOptions(false, false));
    fieldToOptionsMap.put("STJ4USESETAMT", new FieldOptions(false, false));
    fieldToOptionsMap.put("STJ5USESETAMT", new FieldOptions(false, false));

    fieldToOptionsMap.put("STJ1SALESSETAMT", new FieldOptions(false, false));
    fieldToOptionsMap.put("STJ2SALESSETAMT", new FieldOptions(false, false));
    fieldToOptionsMap.put("STJ3SALESSETAMT", new FieldOptions(false, false));
    fieldToOptionsMap.put("STJ4SALESSETAMT", new FieldOptions(false, false));
    fieldToOptionsMap.put("STJ5SALESSETAMT", new FieldOptions(false, false));
    
    fieldToOptionsMap.put("COUNTRYSALESMAXTAXAMT", new FieldOptions(false, false));
    fieldToOptionsMap.put("STATESALESMAXTAXAMT", new FieldOptions(false, false));
    fieldToOptionsMap.put("COUNTYSALESMAXTAXAMT", new FieldOptions(false, false));
    fieldToOptionsMap.put("CITYSALESMAXTAXAMT", new FieldOptions(false, false));  
    fieldToOptionsMap.put("COUNTRYUSEMAXTAXAMT", new FieldOptions(false, false));
    fieldToOptionsMap.put("STATEUSEMAXTAXAMT", new FieldOptions(false, false));
    fieldToOptionsMap.put("COUNTYUSEMAXTAXAMT", new FieldOptions(false, false));
    fieldToOptionsMap.put("CITYUSEMAXTAXAMT", new FieldOptions(false, false));

    // copyAddJurisdictionTaxrate map
    newActionMap.put(TaxJurisdictionAction.copyAddJurisdictionTaxrate,
        (HashMap<String, FieldOptions>) fieldToOptionsMap.clone());

    // updateJurisdictionTaxrate map
    newActionMap.put(TaxJurisdictionAction.updateJurisdictionTaxrate,
        (HashMap<String, FieldOptions>) fieldToOptionsMap.clone());

    // addJurisdictionTaxrateFromTransaction map
    newActionMap.put(TaxJurisdictionAction.addJurisdictionTaxrateFromTransaction,
        (HashMap<String, FieldOptions>) fieldToOptionsMap.clone());

  }

  /**
   * This functions set a Three-Dimensional Map that is used to control
   * accessibility to fields on the User Interface.
   * 
   */
  private void generateFieldAndActionMaps() {

    //
    // Define Maps for [MODE][FIELDS][PARAMETERS] = VALUE
    //

    //
    // Settings for Jurisdiction (Delete, View) Tax Rate (Delete)
    //

    // Generate Maps for TaxJurisdiction
    AddToMap(MODE_VIEW, "JURISDICTIONID", PARAM_DISABLED, VALUE_TRUE);
    AddToMap(MODE_VIEW, "GEOCODE", PARAM_DISABLED, VALUE_TRUE);
    AddToMap(MODE_VIEW, "STATE", PARAM_DISABLED, VALUE_TRUE);
    AddToMap(MODE_VIEW, "COUNTY", PARAM_DISABLED, VALUE_TRUE);
    AddToMap(MODE_VIEW, "CITY", PARAM_DISABLED, VALUE_TRUE);
    AddToMap(MODE_VIEW, "ZIP", PARAM_DISABLED, VALUE_TRUE);
    AddToMap(MODE_VIEW, "ZIPPLUS4", PARAM_DISABLED, VALUE_TRUE);
    AddToMap(MODE_VIEW, "INOUT", PARAM_DISABLED, VALUE_TRUE);
    AddToMap(MODE_VIEW, "CUSTOMFLAG", PARAM_DISABLED, VALUE_TRUE);
    AddToMap(MODE_VIEW, "DESCRIPTION", PARAM_DISABLED, VALUE_TRUE);
    AddToMap(MODE_VIEW, "CLIENTGEOCODE", PARAM_DISABLED, VALUE_TRUE);
    AddToMap(MODE_VIEW, "COMPGEOCODE", PARAM_DISABLED, VALUE_TRUE);
    AddToMap(MODE_VIEW, "UPDATEUSERID", PARAM_DISABLED, VALUE_TRUE);
    AddToMap(MODE_VIEW, "UPDATETIMESTAMP", PARAM_DISABLED, VALUE_TRUE);

    // Generate Maps for TaxRate
    AddToMap(MODE_VIEW, "JURISDICTIONTAXRATEID", PARAM_DISABLED, VALUE_TRUE);
    AddToMap(MODE_VIEW, "RATETYPECODE", PARAM_DISABLED, VALUE_FALSE);
    AddToMap(MODE_VIEW, "EFFECTIVEDATE", PARAM_DISABLED, VALUE_FALSE);
    AddToMap(MODE_VIEW, "EXPIRATIONDATE", PARAM_DISABLED, VALUE_TRUE);
    AddToMap(MODE_VIEW, "MODIFIEDFLAG", PARAM_DISABLED, VALUE_TRUE);
    AddToMap(MODE_VIEW, "STATESALESRATE", PARAM_DISABLED, VALUE_TRUE);
    
    //PP-24
    AddToMap(MODE_VIEW, "STATEUSERATE", PARAM_DISABLED, VALUE_TRUE);
    AddToMap(MODE_VIEW, "STATEUSETIER2RATE", PARAM_DISABLED, VALUE_TRUE);
    AddToMap(MODE_VIEW, "STATEUSETIER3RATE", PARAM_DISABLED, VALUE_TRUE); 
    AddToMap(MODE_VIEW, "STATESALESTIER2RATE", PARAM_DISABLED, VALUE_TRUE);
    AddToMap(MODE_VIEW, "STATESALESTIER3RATE", PARAM_DISABLED, VALUE_TRUE); 
    AddToMap(MODE_VIEW, "STATESPLITAMOUNT", PARAM_DISABLED, VALUE_TRUE);
    AddToMap(MODE_VIEW, "STATETIER2MINAMOUNT", PARAM_DISABLED, VALUE_TRUE);
    AddToMap(MODE_VIEW, "STATETIER2MAXAMOUNT", PARAM_DISABLED, VALUE_TRUE);
    AddToMap(MODE_VIEW, "STATEMAXTAXAMOUNT", PARAM_DISABLED, VALUE_TRUE);
    AddToMap(MODE_VIEW, "COUNTYSALESRATE", PARAM_DISABLED, VALUE_TRUE);
    AddToMap(MODE_VIEW, "COUNTYUSERATE", PARAM_DISABLED, VALUE_TRUE);
    AddToMap(MODE_VIEW, "COUNTYSPLITAMOUNT", PARAM_DISABLED, VALUE_TRUE);
    AddToMap(MODE_VIEW, "COUNTYMAXTAXAMOUNT", PARAM_DISABLED, VALUE_TRUE);
    AddToMap(MODE_VIEW, "COUNTYSINGLEFLAG", PARAM_DISABLED, VALUE_TRUE);
    AddToMap(MODE_VIEW, "COUNTYDEFAULTFLAG", PARAM_DISABLED, VALUE_TRUE);
    AddToMap(MODE_VIEW, "CITYSALESRATE", PARAM_DISABLED, VALUE_TRUE);
    AddToMap(MODE_VIEW, "CITYUSERATE", PARAM_DISABLED, VALUE_TRUE);
    AddToMap(MODE_VIEW, "CITYSPLITAMOUNT", PARAM_DISABLED, VALUE_TRUE);
    AddToMap(MODE_VIEW, "CITYSPLITSALESRATE", PARAM_DISABLED, VALUE_TRUE);
    AddToMap(MODE_VIEW, "CITYSPLITUSERATE", PARAM_DISABLED, VALUE_TRUE);
    AddToMap(MODE_VIEW, "CITYSINGLEFLAG", PARAM_DISABLED, VALUE_TRUE);
    AddToMap(MODE_VIEW, "CITYDEFAULTFLAG", PARAM_DISABLED, VALUE_TRUE);

    // Generate Maps for UI Fields that don't have Database equivalents
    AddToMap(MODE_VIEW, "STATETIER1MINAMOUNT", PARAM_DISABLED, VALUE_TRUE);
    AddToMap(MODE_VIEW, "STATETIER1MAXFLAG", PARAM_DISABLED, VALUE_TRUE);
    
    //PP-24
    AddToMap(MODE_VIEW, "COUNTRYSALESTIER1SETTOMAXIMUMCHECK", PARAM_DISABLED, VALUE_TRUE);
    AddToMap(MODE_VIEW, "COUNTRYSALESTIER2SETTOMAXIMUMCHECK", PARAM_DISABLED, VALUE_TRUE);
    AddToMap(MODE_VIEW, "COUNTRYUSETIER1SETTOMAXIMUMCHECK", PARAM_DISABLED, VALUE_TRUE);
    AddToMap(MODE_VIEW, "COUNTRYUSETIER2SETTOMAXIMUMCHECK", PARAM_DISABLED, VALUE_TRUE);
    
    AddToMap(MODE_VIEW, "STATETIER2MAXFLAG", PARAM_DISABLED, VALUE_TRUE);
    AddToMap(MODE_VIEW, "STATETIER3MAXAMOUNT", PARAM_DISABLED, VALUE_TRUE);

    //
    // Settings for Jurisdiction (Add, Copy/Add, Update)
    //

    // Generate Maps for TaxJurisdiction
    AddToMap(MODE_EDIT, "JURISDICTIONID", PARAM_DISABLED, VALUE_TRUE);
    AddToMap(MODE_EDIT, "GEOCODE", PARAM_DISABLED, VALUE_FALSE);
    AddToMap(MODE_EDIT, "STATE", PARAM_DISABLED, VALUE_FALSE);
    AddToMap(MODE_EDIT, "COUNTY", PARAM_DISABLED, VALUE_FALSE);
    AddToMap(MODE_EDIT, "CITY", PARAM_DISABLED, VALUE_FALSE);
    AddToMap(MODE_EDIT, "ZIP", PARAM_DISABLED, VALUE_FALSE);
    AddToMap(MODE_EDIT, "ZIPPLUS4", PARAM_DISABLED, VALUE_FALSE);
    AddToMap(MODE_EDIT, "INOUT", PARAM_DISABLED, VALUE_FALSE);
    AddToMap(MODE_EDIT, "CUSTOMFLAG", PARAM_DISABLED, VALUE_FALSE);
    AddToMap(MODE_EDIT, "DESCRIPTION", PARAM_DISABLED, VALUE_FALSE);
    AddToMap(MODE_EDIT, "CLIENTGEOCODE", PARAM_DISABLED, VALUE_FALSE);
    AddToMap(MODE_EDIT, "COMPGEOCODE", PARAM_DISABLED, VALUE_FALSE);
    AddToMap(MODE_EDIT, "UPDATEUSERID", PARAM_DISABLED, VALUE_TRUE);
    AddToMap(MODE_EDIT, "UPDATETIMESTAMP", PARAM_DISABLED, VALUE_TRUE);

    // Generate Maps for TaxRate
    AddToMap(MODE_EDIT, "JURISDICTIONTAXRATEID", PARAM_DISABLED, VALUE_TRUE);
    AddToMap(MODE_EDIT, "RATETYPECODE", PARAM_DISABLED, VALUE_FALSE);
    AddToMap(MODE_EDIT, "EFFECTIVEDATE", PARAM_DISABLED, VALUE_FALSE);
    AddToMap(MODE_EDIT, "EXPIRATIONDATE", PARAM_DISABLED, VALUE_FALSE);
    AddToMap(MODE_EDIT, "MODIFIEDFLAG", PARAM_DISABLED, VALUE_FALSE);
    AddToMap(MODE_EDIT, "STATESALESRATE", PARAM_DISABLED, VALUE_FALSE);
    
    //PP-24
    AddToMap(MODE_EDIT, "STATEUSERATE", PARAM_DISABLED, VALUE_FALSE);
    AddToMap(MODE_EDIT, "STATEUSETIER2RATE", PARAM_DISABLED, VALUE_FALSE);
    AddToMap(MODE_EDIT, "STATEUSETIER3RATE", PARAM_DISABLED, VALUE_FALSE);
    AddToMap(MODE_EDIT, "STATESALESTIER2RATE", PARAM_DISABLED, VALUE_FALSE);
    AddToMap(MODE_EDIT, "STATESALESTIER3RATE", PARAM_DISABLED, VALUE_FALSE);
    AddToMap(MODE_EDIT, "STATESPLITAMOUNT", PARAM_DISABLED, VALUE_FALSE);
    AddToMap(MODE_EDIT, "STATETIER2MINAMOUNT", PARAM_DISABLED, VALUE_FALSE);
    AddToMap(MODE_EDIT, "STATETIER2MAXAMOUNT", PARAM_DISABLED, VALUE_FALSE);
    AddToMap(MODE_EDIT, "STATEMAXTAXAMOUNT", PARAM_DISABLED, VALUE_FALSE);
    AddToMap(MODE_EDIT, "COUNTYSALESRATE", PARAM_DISABLED, VALUE_FALSE);
    AddToMap(MODE_EDIT, "COUNTYUSERATE", PARAM_DISABLED, VALUE_FALSE);
    AddToMap(MODE_EDIT, "COUNTYSPLITAMOUNT", PARAM_DISABLED, VALUE_FALSE);
    AddToMap(MODE_EDIT, "COUNTYMAXTAXAMOUNT", PARAM_DISABLED, VALUE_FALSE);
    AddToMap(MODE_EDIT, "COUNTYSINGLEFLAG", PARAM_DISABLED, VALUE_FALSE);
    AddToMap(MODE_EDIT, "COUNTYDEFAULTFLAG", PARAM_DISABLED, VALUE_FALSE);
    AddToMap(MODE_EDIT, "CITYSALESRATE", PARAM_DISABLED, VALUE_FALSE);
    AddToMap(MODE_EDIT, "CITYUSERATE", PARAM_DISABLED, VALUE_FALSE);
    AddToMap(MODE_EDIT, "CITYSPLITAMOUNT", PARAM_DISABLED, VALUE_FALSE);
    AddToMap(MODE_EDIT, "CITYSPLITSALESRATE", PARAM_DISABLED, VALUE_FALSE);
    AddToMap(MODE_EDIT, "CITYSPLITUSERATE", PARAM_DISABLED, VALUE_FALSE);
    AddToMap(MODE_EDIT, "CITYSINGLEFLAG", PARAM_DISABLED, VALUE_FALSE);
    AddToMap(MODE_EDIT, "CITYDEFAULTFLAG", PARAM_DISABLED, VALUE_FALSE);

    // Generate Maps for UI Fields that don't have Database equivalents
    AddToMap(MODE_EDIT, "STATETIER1MINAMOUNT", PARAM_DISABLED, VALUE_TRUE);
    AddToMap(MODE_EDIT, "STATETIER1MAXFLAG", PARAM_DISABLED, VALUE_FALSE);
    
    //PP-24
    AddToMap(MODE_VIEW, "COUNTRYSALESTIER1SETTOMAXIMUMCHECK", PARAM_DISABLED, VALUE_TRUE);
    AddToMap(MODE_VIEW, "COUNTRYSALESTIER2SETTOMAXIMUMCHECK", PARAM_DISABLED, VALUE_TRUE);
    AddToMap(MODE_VIEW, "COUNTRYUSETIER1SETTOMAXIMUMCHECK", PARAM_DISABLED, VALUE_TRUE);
    AddToMap(MODE_VIEW, "COUNTRYUSETIER2SETTOMAXIMUMCHECK", PARAM_DISABLED, VALUE_TRUE);
    
    AddToMap(MODE_EDIT, "STATETIER2MAXFLAG", PARAM_DISABLED, VALUE_FALSE);
    AddToMap(MODE_EDIT, "STATETIER3MAXAMOUNT", PARAM_DISABLED, VALUE_TRUE);

    //
    // Settings for Tax Rate (Add, Copy/Add, Update)
    //

    // Generate Maps for TaxJurisdiction
    AddToMap(MODE_EDITTR, "JURISDICTIONID", PARAM_DISABLED, VALUE_TRUE);
    AddToMap(MODE_EDITTR, "GEOCODE", PARAM_DISABLED, VALUE_TRUE);
    AddToMap(MODE_EDITTR, "STATE", PARAM_DISABLED, VALUE_TRUE);
    AddToMap(MODE_EDITTR, "COUNTY", PARAM_DISABLED, VALUE_TRUE);
    AddToMap(MODE_EDITTR, "CITY", PARAM_DISABLED, VALUE_TRUE);
    AddToMap(MODE_EDITTR, "ZIP", PARAM_DISABLED, VALUE_TRUE);
    AddToMap(MODE_EDITTR, "ZIPPLUS4", PARAM_DISABLED, VALUE_TRUE);
    AddToMap(MODE_EDITTR, "INOUT", PARAM_DISABLED, VALUE_TRUE);
    AddToMap(MODE_EDITTR, "CUSTOMFLAG", PARAM_DISABLED, VALUE_TRUE);
    AddToMap(MODE_EDITTR, "DESCRIPTION", PARAM_DISABLED, VALUE_TRUE);
    AddToMap(MODE_EDITTR, "CLIENTGEOCODE", PARAM_DISABLED, VALUE_TRUE);
    AddToMap(MODE_EDITTR, "COMPGEOCODE", PARAM_DISABLED, VALUE_TRUE);
    AddToMap(MODE_EDITTR, "UPDATEUSERID", PARAM_DISABLED, VALUE_TRUE);
    AddToMap(MODE_EDITTR, "UPDATETIMESTAMP", PARAM_DISABLED, VALUE_TRUE);

    // Generate Maps for TaxRate
    AddToMap(MODE_EDITTR, "JURISDICTIONTAXRATEID", PARAM_DISABLED, VALUE_TRUE);
    AddToMap(MODE_EDITTR, "RATETYPECODE", PARAM_DISABLED, VALUE_FALSE);
    AddToMap(MODE_EDITTR, "EFFECTIVEDATE", PARAM_DISABLED, VALUE_FALSE);
    AddToMap(MODE_EDITTR, "EXPIRATIONDATE", PARAM_DISABLED, VALUE_FALSE);
    AddToMap(MODE_EDITTR, "MODIFIEDFLAG", PARAM_DISABLED, VALUE_FALSE);
    AddToMap(MODE_EDITTR, "STATESALESRATE", PARAM_DISABLED, VALUE_FALSE);
    
    //PP-24
    AddToMap(MODE_EDITTR, "xxxxxx", PARAM_DISABLED, VALUE_FALSE);
    
    
    AddToMap(MODE_EDITTR, "STATEUSERATE", PARAM_DISABLED, VALUE_FALSE);
    AddToMap(MODE_EDITTR, "STATEUSETIER2RATE", PARAM_DISABLED, VALUE_FALSE);
    AddToMap(MODE_EDITTR, "STATEUSETIER3RATE", PARAM_DISABLED, VALUE_FALSE); 
    AddToMap(MODE_EDITTR, "STATESALESTIER2RATE", PARAM_DISABLED, VALUE_FALSE);
    AddToMap(MODE_EDITTR, "STATESALESTIER3RATE", PARAM_DISABLED, VALUE_FALSE);
    AddToMap(MODE_EDITTR, "STATESPLITAMOUNT", PARAM_DISABLED, VALUE_FALSE);
    AddToMap(MODE_EDITTR, "STATETIER2MINAMOUNT", PARAM_DISABLED, VALUE_FALSE);
    AddToMap(MODE_EDITTR, "STATETIER2MAXAMOUNT", PARAM_DISABLED, VALUE_FALSE);
    AddToMap(MODE_EDITTR, "STATEMAXTAXAMOUNT", PARAM_DISABLED, VALUE_FALSE);
    AddToMap(MODE_EDITTR, "COUNTYSALESRATE", PARAM_DISABLED, VALUE_FALSE);
    AddToMap(MODE_EDITTR, "COUNTYUSERATE", PARAM_DISABLED, VALUE_FALSE);
    AddToMap(MODE_EDITTR, "COUNTYSPLITAMOUNT", PARAM_DISABLED, VALUE_FALSE);
    AddToMap(MODE_EDITTR, "COUNTYMAXTAXAMOUNT", PARAM_DISABLED, VALUE_FALSE);
    AddToMap(MODE_EDITTR, "COUNTYSINGLEFLAG", PARAM_DISABLED, VALUE_FALSE);
    AddToMap(MODE_EDITTR, "COUNTYDEFAULTFLAG", PARAM_DISABLED, VALUE_FALSE);
    AddToMap(MODE_EDITTR, "CITYSALESRATE", PARAM_DISABLED, VALUE_FALSE);
    AddToMap(MODE_EDITTR, "CITYUSERATE", PARAM_DISABLED, VALUE_FALSE);
    AddToMap(MODE_EDITTR, "CITYSPLITAMOUNT", PARAM_DISABLED, VALUE_FALSE);
    AddToMap(MODE_EDITTR, "CITYSPLITSALESRATE", PARAM_DISABLED, VALUE_FALSE);
    AddToMap(MODE_EDITTR, "CITYSPLITUSERATE", PARAM_DISABLED, VALUE_FALSE);
    AddToMap(MODE_EDITTR, "CITYSINGLEFLAG", PARAM_DISABLED, VALUE_FALSE);
    AddToMap(MODE_EDITTR, "CITYDEFAULTFLAG", PARAM_DISABLED, VALUE_FALSE);

    // Generate Maps for UI Fields that don't have Database equivalents
    AddToMap(MODE_EDITTR, "STATETIER1MINAMOUNT", PARAM_DISABLED, VALUE_TRUE);
    AddToMap(MODE_EDITTR, "STATETIER1MAXFLAG", PARAM_DISABLED, VALUE_FALSE);
    
    //PP-24
    AddToMap(MODE_VIEW, "COUNTRYSALESTIER1SETTOMAXIMUMCHECK", PARAM_DISABLED, VALUE_TRUE);
    AddToMap(MODE_VIEW, "COUNTRYSALESTIER2SETTOMAXIMUMCHECK", PARAM_DISABLED, VALUE_TRUE);
    AddToMap(MODE_VIEW, "COUNTRYUSETIER1SETTOMAXIMUMCHECK", PARAM_DISABLED, VALUE_TRUE);
    AddToMap(MODE_VIEW, "COUNTRYUSETIER2SETTOMAXIMUMCHECK", PARAM_DISABLED, VALUE_TRUE);
    
    AddToMap(MODE_EDITTR, "STATETIER2MAXFLAG", PARAM_DISABLED, VALUE_FALSE);
    AddToMap(MODE_EDITTR, "STATETIER3MAXAMOUNT", PARAM_DISABLED, VALUE_TRUE);

  }

  /**
   * This functions sets the field maxlength sizes of the user interface
   * elements.
   */
  private void generateFieldSizeMap() {
    // TaxJurisdiction fields
    fieldSizeMap.put("JURISDICTIONID", Jurisdiction.JURISDICTIONID_SIZE);
    fieldSizeMap.put("GEOCODE", Jurisdiction.GEOCODE_SIZE);
    fieldSizeMap.put("STATE", Jurisdiction.STATE_SIZE);
    fieldSizeMap.put("COUNTY", Jurisdiction.COUNTY_SIZE);
    fieldSizeMap.put("CITY", Jurisdiction.CITY_SIZE);
    fieldSizeMap.put("ZIP", Jurisdiction.ZIP_SIZE);
    fieldSizeMap.put("ZIPPLUS4", Jurisdiction.ZIPPLUS4_SIZE);
    fieldSizeMap.put("INOUT", Jurisdiction.INOUT_SIZE);
    fieldSizeMap.put("CUSTOMFLAG", Jurisdiction.CUSTOMFLAG_SIZE);
    fieldSizeMap.put("DESCRIPTION", Jurisdiction.DESCRIPTION_SIZE);
    fieldSizeMap.put("CLIENTGEOCODE", Jurisdiction.CLIENTGEOCODE_SIZE);
    fieldSizeMap.put("COMPGEOCODE", Jurisdiction.COMPGEOCODE_SIZE);
    fieldSizeMap.put("UPDATEUSERID", Jurisdiction.UPDATEUSERID_SIZE);
    fieldSizeMap.put("UPDATETIMESTAMP", Jurisdiction.UPDATETIMESTAMP_SIZE);
    fieldSizeMap.put("REPORTINGCITY", Jurisdiction.REPORTINGCITY_SIZE);
    fieldSizeMap.put("REPORTINGCITYFIPS", Jurisdiction.REPORTINGCITYFIPS_SIZE);
    fieldSizeMap.put("STJ1NAME", Jurisdiction.STJ1NAME_SIZE);
    fieldSizeMap.put("STJ2NAME", Jurisdiction.STJ2NAME_SIZE);
    fieldSizeMap.put("STJ3NAME", Jurisdiction.STJ3NAME_SIZE);
    fieldSizeMap.put("STJ4NAME", Jurisdiction.STJ4NAME_SIZE);
    fieldSizeMap.put("STJ5NAME", Jurisdiction.STJ5NAME_SIZE);
    fieldSizeMap.put("MODIFIED_FLAG", Jurisdiction.MODIFIED_FLAG_SIZE);
    fieldSizeMap.put("EFFECTIVEDATE", Jurisdiction.EFFECTIVEDATE_SIZE);
    fieldSizeMap.put("EXPIRATIONDATE", Jurisdiction.EXPIRATIONDATE_SIZE);
    fieldSizeMap.put("COUNTRYNEXUSINDCODE", Jurisdiction.COUNTRYNEXUSINDCODE_SIZE);
    fieldSizeMap.put("STATENEXUSINDCODE", Jurisdiction.STATENEXUSINDCODE_SIZE);
    fieldSizeMap.put("COUNTYNEXUSINDCODE", Jurisdiction.COUNTYNEXUSINDCODE_SIZE);
    fieldSizeMap.put("CITYNEXUSINDCODE", Jurisdiction.CITYNEXUSINDCODE_SIZE);
    fieldSizeMap.put("STJ1NEXUSINDCODE", Jurisdiction.STJ1NEXUSINDCODE_SIZE);
    fieldSizeMap.put("STJ2NEXUSINDCODE", Jurisdiction.STJ2NEXUSINDCODE_SIZE);
    fieldSizeMap.put("STJ3NEXUSINDCODE", Jurisdiction.STJ3NEXUSINDCODE_SIZE);
    fieldSizeMap.put("STJ4NEXUSINDCODE", Jurisdiction.STJ4NEXUSINDCODE_SIZE);
    fieldSizeMap.put("STJ5NEXUSINDCODE", Jurisdiction.STJ5NEXUSINDCODE_SIZE);
    fieldSizeMap.put("STJ1LOCALCODE", Jurisdiction.STJ1LOCALCODE_SIZE);
    fieldSizeMap.put("STJ2LOCALCODE", Jurisdiction.STJ2LOCALCODE_SIZE);
    fieldSizeMap.put("STJ3LOCALCODE", Jurisdiction.STJ3LOCALCODE_SIZE);
    fieldSizeMap.put("STJ4LOCALCODE", Jurisdiction.STJ4LOCALCODE_SIZE);
    fieldSizeMap.put("STJ5LOCALCODE", Jurisdiction.STJ5LOCALCODE_SIZE);

    // TaxRate Fields
    fieldSizeMap.put("JURISDICTIONTAXRATEID", JurisdictionTaxrate.JURISDICTIONTAXRATEID_SIZE);
    fieldSizeMap.put("RATETYPECODE", JurisdictionTaxrate.RATETYPECODE_SIZE);
    fieldSizeMap.put("EFFECTIVEDATE", JurisdictionTaxrate.EFFECTIVEDATE_SIZE);
    fieldSizeMap.put("EXPIRATIONDATE", JurisdictionTaxrate.EXPIRATIONDATE_SIZE);
    fieldSizeMap.put("MODIFIEDFLAG", JurisdictionTaxrate.MODIFIEDFLAG_SIZE);
    fieldSizeMap.put("STATESALESRATE", JurisdictionTaxrate.STATESALESRATE_SIZE);
    
    //PP-24
    //COUNTRY
    fieldSizeMap.put("COUNTRYSALESTIER1RATE", JurisdictionTaxrate.COUNTRYSALESTIER1RATE);
    fieldSizeMap.put("COUNTRYSALESTIER1SETAMT", JurisdictionTaxrate.COUNTRYSALESTIER1SETAMT);
    fieldSizeMap.put("COUNTRYSALESTIER1MAXAMT", JurisdictionTaxrate.COUNTRYSALESTIER1MAXAMT);
    fieldSizeMap.put("COUNTRYSALESTIER2MINAMT", JurisdictionTaxrate.COUNTRYSALESTIER2MINAMT);
    fieldSizeMap.put("COUNTRYSALESTIER2RATE", JurisdictionTaxrate.COUNTRYSALESTIER2RATE);
    fieldSizeMap.put("COUNTRYSALESTIER2SETAMT", JurisdictionTaxrate.COUNTRYSALESTIER2SETAMT);
    fieldSizeMap.put("COUNTRYSALESTIER2MAXAMT", JurisdictionTaxrate.COUNTRYSALESTIER2MAXAMT);
    fieldSizeMap.put("COUNTRYSALESTIER2ENTAMFLAG", JurisdictionTaxrate.COUNTRYSALESTIER2ENTAMFLAG);
    fieldSizeMap.put("COUNTRYSALESTIER3RATE", JurisdictionTaxrate.COUNTRYSALESTIER3RATE);
    fieldSizeMap.put("COUNTRYSALESTIER3SETAMT", JurisdictionTaxrate.COUNTRYSALESTIER3SETAMT);
    fieldSizeMap.put("COUNTRYSALESTIER3ENTAMFLAG", JurisdictionTaxrate.COUNTRYSALESTIER3ENTAMFLAG);
    
    fieldSizeMap.put("COUNTRYUSETIER1RATE", JurisdictionTaxrate.COUNTRYUSETIER1RATE);
    fieldSizeMap.put("COUNTRYUSETIER1SETAMT", JurisdictionTaxrate.COUNTRYUSETIER1SETAMT);
    fieldSizeMap.put("COUNTRYUSETIER1MAXAMT", JurisdictionTaxrate.COUNTRYUSETIER1MAXAMT);
    fieldSizeMap.put("COUNTRYUSETIER2MINAMT", JurisdictionTaxrate.COUNTRYUSETIER2MINAMT);
    fieldSizeMap.put("COUNTRYUSETIER2RATE", JurisdictionTaxrate.COUNTRYUSETIER2RATE);
    fieldSizeMap.put("COUNTRYUSETIER2SETAMT", JurisdictionTaxrate.COUNTRYUSETIER2SETAMT);
    fieldSizeMap.put("COUNTRYUSETIER2MAXAMT", JurisdictionTaxrate.COUNTRYUSETIER2MAXAMT);
    fieldSizeMap.put("COUNTRYUSETIER2ENTAMFLAG", JurisdictionTaxrate.COUNTRYUSETIER2ENTAMFLAG);
    fieldSizeMap.put("COUNTRYUSETIER3RATE", JurisdictionTaxrate.COUNTRYUSETIER3RATE);
    fieldSizeMap.put("COUNTRYUSETIER3SETAMT", JurisdictionTaxrate.COUNTRYUSETIER3SETAMT);
    fieldSizeMap.put("COUNTRYUSETIER3ENTAMFLAG", JurisdictionTaxrate.COUNTRYUSETIER3ENTAMFLAG);
    
    //STATE
    fieldSizeMap.put("STATESALESTIER1RATE", JurisdictionTaxrate.STATESALESTIER1RATE);
    fieldSizeMap.put("STATESALESTIER1SETAMT", JurisdictionTaxrate.STATESALESTIER1SETAMT);
    fieldSizeMap.put("STATESALESTIER1MAXAMT", JurisdictionTaxrate.STATESALESTIER1MAXAMT);
    fieldSizeMap.put("STATESALESTIER2MINAMT", JurisdictionTaxrate.STATESALESTIER2MINAMT);
    fieldSizeMap.put("STATESALESTIER2RATE", JurisdictionTaxrate.STATESALESTIER2RATE);
    fieldSizeMap.put("STATESALESTIER2SETAMT", JurisdictionTaxrate.STATESALESTIER2SETAMT);
    fieldSizeMap.put("STATESALESTIER2MAXAMT", JurisdictionTaxrate.STATESALESTIER2MAXAMT);
    fieldSizeMap.put("STATESALESTIER2ENTAMFLAG", JurisdictionTaxrate.STATESALESTIER2ENTAMFLAG);
    fieldSizeMap.put("STATESALESTIER3RATE", JurisdictionTaxrate.STATESALESTIER3RATE);
    fieldSizeMap.put("STATESALESTIER3SETAMT", JurisdictionTaxrate.STATESALESTIER3SETAMT);
    fieldSizeMap.put("STATESALESTIER3ENTAMFLAG", JurisdictionTaxrate.STATESALESTIER3ENTAMFLAG);
    
    fieldSizeMap.put("STATEUSETIER1RATE", JurisdictionTaxrate.STATEUSETIER1RATE);
    fieldSizeMap.put("STATEUSETIER1SETAMT", JurisdictionTaxrate.STATEUSETIER1SETAMT);
    fieldSizeMap.put("STATEUSETIER1MAXAMT", JurisdictionTaxrate.STATEUSETIER1MAXAMT);
    fieldSizeMap.put("STATEUSETIER2MINAMT", JurisdictionTaxrate.STATEUSETIER2MINAMT);
    fieldSizeMap.put("STATEUSETIER2RATE", JurisdictionTaxrate.STATEUSETIER2RATE);
    fieldSizeMap.put("STATEUSETIER2SETAMT", JurisdictionTaxrate.STATEUSETIER2SETAMT);
    fieldSizeMap.put("STATEUSETIER2MAXAMT", JurisdictionTaxrate.STATEUSETIER2MAXAMT);
    fieldSizeMap.put("STATEUSETIER2ENTAMFLAG", JurisdictionTaxrate.STATEUSETIER2ENTAMFLAG);
    fieldSizeMap.put("STATEUSETIER3RATE", JurisdictionTaxrate.STATEUSETIER3RATE);
    fieldSizeMap.put("STATEUSETIER3SETAMT", JurisdictionTaxrate.STATEUSETIER3SETAMT);
    fieldSizeMap.put("STATEUSETIER3ENTAMFLAG", JurisdictionTaxrate.STATEUSETIER3ENTAMFLAG);
    
    //COUNTY
    fieldSizeMap.put("COUNTYSALESTIER1RATE", JurisdictionTaxrate.COUNTYSALESTIER1RATE);
    fieldSizeMap.put("COUNTYSALESTIER1SETAMT", JurisdictionTaxrate.COUNTYSALESTIER1SETAMT);
    fieldSizeMap.put("COUNTYSALESTIER1MAXAMT", JurisdictionTaxrate.COUNTYSALESTIER1MAXAMT);
    fieldSizeMap.put("COUNTYSALESTIER2MINAMT", JurisdictionTaxrate.COUNTYSALESTIER2MINAMT);
    fieldSizeMap.put("COUNTYSALESTIER2RATE", JurisdictionTaxrate.COUNTYSALESTIER2RATE);
    fieldSizeMap.put("COUNTYSALESTIER2SETAMT", JurisdictionTaxrate.COUNTYSALESTIER2SETAMT);
    fieldSizeMap.put("COUNTYSALESTIER2MAXAMT", JurisdictionTaxrate.COUNTYSALESTIER2MAXAMT);
    fieldSizeMap.put("COUNTYSALESTIER2ENTAMFLAG", JurisdictionTaxrate.COUNTYSALESTIER2ENTAMFLAG);
    fieldSizeMap.put("COUNTYSALESTIER3RATE", JurisdictionTaxrate.COUNTYSALESTIER3RATE);
    fieldSizeMap.put("COUNTYSALESTIER3SETAMT", JurisdictionTaxrate.COUNTYSALESTIER3SETAMT);
    fieldSizeMap.put("COUNTYSALESTIER3ENTAMFLAG", JurisdictionTaxrate.COUNTYSALESTIER3ENTAMFLAG);
    
    fieldSizeMap.put("COUNTYUSETIER1RATE", JurisdictionTaxrate.COUNTYUSETIER1RATE);
    fieldSizeMap.put("COUNTYUSETIER1SETAMT", JurisdictionTaxrate.COUNTYUSETIER1SETAMT);
    fieldSizeMap.put("COUNTYUSETIER1MAXAMT", JurisdictionTaxrate.COUNTYUSETIER1MAXAMT);
    fieldSizeMap.put("COUNTYUSETIER2MINAMT", JurisdictionTaxrate.COUNTYUSETIER2MINAMT);
    fieldSizeMap.put("COUNTYUSETIER2RATE", JurisdictionTaxrate.COUNTYUSETIER2RATE);
    fieldSizeMap.put("COUNTYUSETIER2SETAMT", JurisdictionTaxrate.COUNTYUSETIER2SETAMT);
    fieldSizeMap.put("COUNTYUSETIER2MAXAMT", JurisdictionTaxrate.COUNTYUSETIER2MAXAMT);
    fieldSizeMap.put("COUNTYUSETIER2ENTAMFLAG", JurisdictionTaxrate.COUNTYUSETIER2ENTAMFLAG);
    fieldSizeMap.put("COUNTYUSETIER3RATE", JurisdictionTaxrate.COUNTYUSETIER3RATE);
    fieldSizeMap.put("COUNTYUSETIER3SETAMT", JurisdictionTaxrate.COUNTYUSETIER3SETAMT);
    fieldSizeMap.put("COUNTYUSETIER3ENTAMFLAG", JurisdictionTaxrate.COUNTYUSETIER3ENTAMFLAG);
    
    //CITY
    fieldSizeMap.put("CITYSALESTIER1RATE", JurisdictionTaxrate.CITYSALESTIER1RATE);
    fieldSizeMap.put("CITYSALESTIER1SETAMT", JurisdictionTaxrate.CITYSALESTIER1SETAMT);
    fieldSizeMap.put("CITYSALESTIER1MAXAMT", JurisdictionTaxrate.CITYSALESTIER1MAXAMT);
    fieldSizeMap.put("CITYSALESTIER2MINAMT", JurisdictionTaxrate.CITYSALESTIER2MINAMT);
    fieldSizeMap.put("CITYSALESTIER2RATE", JurisdictionTaxrate.CITYSALESTIER2RATE);
    fieldSizeMap.put("CITYSALESTIER2SETAMT", JurisdictionTaxrate.CITYSALESTIER2SETAMT);
    fieldSizeMap.put("CITYSALESTIER2MAXAMT", JurisdictionTaxrate.CITYSALESTIER2MAXAMT);
    fieldSizeMap.put("CITYSALESTIER2ENTAMFLAG", JurisdictionTaxrate.CITYSALESTIER2ENTAMFLAG);
    fieldSizeMap.put("CITYSALESTIER3RATE", JurisdictionTaxrate.CITYSALESTIER3RATE);
    fieldSizeMap.put("CITYSALESTIER3SETAMT", JurisdictionTaxrate.CITYSALESTIER3SETAMT);
    fieldSizeMap.put("CITYSALESTIER3ENTAMFLAG", JurisdictionTaxrate.CITYSALESTIER3ENTAMFLAG);
    
    fieldSizeMap.put("CITYUSETIER1RATE", JurisdictionTaxrate.CITYUSETIER1RATE);
    fieldSizeMap.put("CITYUSETIER1SETAMT", JurisdictionTaxrate.CITYUSETIER1SETAMT);
    fieldSizeMap.put("CITYUSETIER1MAXAMT", JurisdictionTaxrate.CITYUSETIER1MAXAMT);
    fieldSizeMap.put("CITYUSETIER2MINAMT", JurisdictionTaxrate.CITYUSETIER2MINAMT);
    fieldSizeMap.put("CITYUSETIER2RATE", JurisdictionTaxrate.CITYUSETIER2RATE);
    fieldSizeMap.put("CITYUSETIER2SETAMT", JurisdictionTaxrate.CITYUSETIER2SETAMT);
    fieldSizeMap.put("CITYUSETIER2MAXAMT", JurisdictionTaxrate.CITYUSETIER2MAXAMT);
    fieldSizeMap.put("CITYUSETIER2ENTAMFLAG", JurisdictionTaxrate.CITYUSETIER2ENTAMFLAG);
    fieldSizeMap.put("CITYUSETIER3RATE", JurisdictionTaxrate.CITYUSETIER3RATE);
    fieldSizeMap.put("CITYUSETIER3SETAMT", JurisdictionTaxrate.CITYUSETIER3SETAMT);
    fieldSizeMap.put("CITYUSETIER3ENTAMFLAG", JurisdictionTaxrate.CITYUSETIER3ENTAMFLAG);
    
    fieldSizeMap.put("COUNTRYSALESMAXTAXAMT", JurisdictionTaxrate.COUNTRYSALESMAXTAXAMT);
    fieldSizeMap.put("STATESALESMAXTAXAMT", JurisdictionTaxrate.STATESALESMAXTAXAMT);
    fieldSizeMap.put("COUNTYSALESMAXTAXAMT", JurisdictionTaxrate.COUNTYSALESMAXTAXAMT);
    fieldSizeMap.put("CITYSALESMAXTAXAMT", JurisdictionTaxrate.CITYSALESMAXTAXAMT);
    fieldSizeMap.put("COUNTRYUSEMAXTAXAMT", JurisdictionTaxrate.COUNTRYUSEMAXTAXAMT);
    fieldSizeMap.put("STATEUSEMAXTAXAMT", JurisdictionTaxrate.STATEUSEMAXTAXAMT);
    fieldSizeMap.put("COUNTYUSEMAXTAXAMT", JurisdictionTaxrate.COUNTYUSEMAXTAXAMT);
    fieldSizeMap.put("CITYUSEMAXTAXAMT", JurisdictionTaxrate.CITYUSEMAXTAXAMT);
    
    fieldSizeMap.put("STATEUSERATE", JurisdictionTaxrate.STATEUSERATE_SIZE);
    fieldSizeMap.put("STATEUSETIER2RATE", JurisdictionTaxrate.STATEUSETIER2RATE_SIZE);
    fieldSizeMap.put("STATEUSETIER3RATE", JurisdictionTaxrate.STATEUSETIER3RATE_SIZE);
    fieldSizeMap.put("STATESALESTIER2RATE", JurisdictionTaxrate.STATESALESTIER2RATE_SIZE);
    fieldSizeMap.put("STATESALESTIER3RATE", JurisdictionTaxrate.STATESALESTIER3RATE_SIZE);
    fieldSizeMap.put("STATESPLITAMOUNT", JurisdictionTaxrate.STATESPLITAMOUNT_SIZE);
    fieldSizeMap.put("STATETIER2MINAMOUNT", JurisdictionTaxrate.STATETIER2MINAMOUNT_SIZE);
    fieldSizeMap.put("STATETIER2MAXAMOUNT", JurisdictionTaxrate.STATETIER2MAXAMOUNT_SIZE);
    fieldSizeMap.put("STATEMAXTAXAMOUNT", JurisdictionTaxrate.STATEMAXTAXAMOUNT_SIZE);
    fieldSizeMap.put("COUNTYSALESRATE", JurisdictionTaxrate.COUNTYSALESRATE_SIZE);
    fieldSizeMap.put("COUNTYUSERATE", JurisdictionTaxrate.COUNTYUSERATE_SIZE);
    fieldSizeMap.put("COUNTYSPLITAMOUNT", JurisdictionTaxrate.COUNTYSPLITAMOUNT_SIZE);
    fieldSizeMap.put("COUNTYMAXTAXAMOUNT", JurisdictionTaxrate.COUNTYMAXTAXAMOUNT_SIZE);
    fieldSizeMap.put("COUNTYSINGLEFLAG", JurisdictionTaxrate.COUNTYSINGLEFLAG_SIZE);
    fieldSizeMap.put("COUNTYDEFAULTFLAG", JurisdictionTaxrate.COUNTYDEFAULTFLAG_SIZE);
    fieldSizeMap.put("CITYSALESRATE", JurisdictionTaxrate.CITYSALESRATE_SIZE);
    fieldSizeMap.put("CITYUSERATE", JurisdictionTaxrate.CITYUSERATE_SIZE);
    fieldSizeMap.put("CITYSPLITAMOUNT", JurisdictionTaxrate.CITYSPLITAMOUNT_SIZE);
    fieldSizeMap.put("CITYSPLITUSERATE", JurisdictionTaxrate.CITYSPLITUSERATE_SIZE);
    fieldSizeMap.put("CITYSINGLEFLAG", JurisdictionTaxrate.CITYSINGLEFLAG_SIZE);
    fieldSizeMap.put("CITYDEFAULTFLAG", JurisdictionTaxrate.CITYDEFAULTFLAG_SIZE);

    fieldSizeMap.put("STJ1USERATE", JurisdictionTaxrate.STJ1USERATE_SIZE);
    fieldSizeMap.put("STJ2USERATE", JurisdictionTaxrate.STJ2USERATE_SIZE);
    fieldSizeMap.put("STJ3USERATE", JurisdictionTaxrate.STJ3USERATE_SIZE);
    fieldSizeMap.put("STJ4USERATE", JurisdictionTaxrate.STJ4USERATE_SIZE);
    fieldSizeMap.put("STJ5USERATE", JurisdictionTaxrate.STJ5USERATE_SIZE);
    
    fieldSizeMap.put("STJ1SALESRATE", JurisdictionTaxrate.STJ1SALESRATE_SIZE);
    fieldSizeMap.put("STJ2SALESRATE", JurisdictionTaxrate.STJ2SALESRATE_SIZE);
    fieldSizeMap.put("STJ3SALESRATE", JurisdictionTaxrate.STJ3SALESRATE_SIZE);
    fieldSizeMap.put("STJ4SALESRATE", JurisdictionTaxrate.STJ4SALESRATE_SIZE);
    fieldSizeMap.put("STJ5SALESRATE", JurisdictionTaxrate.STJ5SALESRATE_SIZE);

    fieldSizeMap.put("STJ1USESETAMT", JurisdictionTaxrate.STJ1USERATE_SIZE);
    fieldSizeMap.put("STJ2USESETAMT", JurisdictionTaxrate.STJ2USERATE_SIZE);
    fieldSizeMap.put("STJ3USESETAMT", JurisdictionTaxrate.STJ3USERATE_SIZE);
    fieldSizeMap.put("STJ4USESETAMT", JurisdictionTaxrate.STJ4USERATE_SIZE);
    fieldSizeMap.put("STJ5USESETAMT", JurisdictionTaxrate.STJ5USERATE_SIZE);
    
    fieldSizeMap.put("STJ1SALESSETAMT", JurisdictionTaxrate.STJ1SALESRATE_SIZE);
    fieldSizeMap.put("STJ2SALESSETAMT", JurisdictionTaxrate.STJ2SALESRATE_SIZE);
    fieldSizeMap.put("STJ3SALESSETAMT", JurisdictionTaxrate.STJ3SALESRATE_SIZE);
    fieldSizeMap.put("STJ4SALESSETAMT", JurisdictionTaxrate.STJ4SALESRATE_SIZE);
    fieldSizeMap.put("STJ5SALESSETAMT", JurisdictionTaxrate.STJ5SALESRATE_SIZE);

    // UI Fields that don't have Database equivalents
    fieldSizeMap.put("STATETIER1MINAMOUNT", 31);
    fieldSizeMap.put("STATETIER1MAXFLAG", 1);
    
    //PP-24
    fieldSizeMap.put("STATETIER2MAXFLAG", 1);
    fieldSizeMap.put("STATETIER3MAXAMOUNT", 31);    
  }

  /*
   * 
   * The HELPER and UI Widget Generation Methods
   * 
   */

  /**
   * This function adds the passed into parameters to a Three-Dimensional Map.
   * 
   * @param action - ACTION_VIEW | ACTION_EDIT | ACTION_EDITTR
   * 
   * @param field - Which UI element to set the value for
   * 
   * @param parameter - Which UI attribute did you want to set.
   * 
   * @param value - The value that will be passed to the JSF page.
   */
  private void AddToMap(String action, String field, String parameter, String value) {
    // check to see if the Action exists in Map
    if (actionMap.containsKey(action) == false) {
      // Create new Key ACTION and value fieldMap
      Map<String, Map<String, String>> fieldMap = new HashMap<String, Map<String, String>>();
      actionMap.put(action, fieldMap);
    }

    // Check if the Field Exists in Map
    if (actionMap.get(action).containsKey(field) == false) {
      // Create new Key FIELD and value parameterMap
      Map<String, String> parameterMap = new HashMap<String, String>();
      actionMap.get(action).put(field, parameterMap);
    }

    // Create/append key PARAMETER and value Value
    actionMap.get(action).get(field).put(parameter, value);
  }

  /**
   * Builds the InOut Selection pull-down.
   * 
   * @param uniqueIdNumber - a unique number to assign to the Selections Id.
   */
  private void buildInOutSelection(String uniqueIdNumber) {
    inOutMenu = new HtmlSelectOneMenu();
    inOutMenu.setId("inOutMenu1");
    UISelectItems inOutItems = new UISelectItems();
    inOutItems.setId("inOutItems" + uniqueIdNumber);
    List<SelectItem> inOutList = new ArrayList<SelectItem>();
    inOutList.add(new SelectItem("I", "In"));
    inOutList.add(new SelectItem("O", "Out"));
    inOutList.add(new SelectItem("B", "Both"));
    inOutItems.setValue(inOutList);
    inOutMenu.getChildren().add(inOutItems);
  }

  /*
   * 
   * These functions handle the Messages for the ModalPanel that displays errors
   * to the user.
   * 
   */

  /**
   * This method adds a String to the list of Message Strings.
   * 
   * @param msg - String of message
   */
  public void addMessage(String msg) {
    if ((msg != null) && (msg.length() > 0)) {
      this.messages.add(msg);
    }
  }

  /**
   * This method adds a List of Strings to the existing Message List.
   * 
   * @param msgs -- List of Strings that contain messages.
   */
  public void addMessages(List<String> msgs) {
    if ((msgs != null) && (msgs.size() > 0)) {
      for (String msg : msgs) {
        if ((msg != null) && (msg.length() > 0)) {
          addMessage(msg);
        }
      }
    }
  }

  /**
   * This method clears the List of Messages.
   */
  public void clearMessageQueue() {
    if (this.messages != null) {
      this.messages.clear();
    }
    messagesModalPanel.setShowWhenRendered(false);
  }

  /**
   * This method is called right before the page is refreshed to load the list
   * of messages to the user into a Data Table that will display the messages in
   * a ModalPanel.
   */
  public void generateMessageModalPanel() {
    logger
        .debug("TaxJurisdictionBackingBean::generateMessageModalPanel::messages size=" + this.messages
            .size());
    if (this.messages.size() > 0) {
      messagesModalPanel.setShowWhenRendered(true);
      // Should not need to do this, but seems to be a bug with Autosized
      messagesModalPanel.setMinHeight(200);
      messagesModalPanel.setMinWidth(200);

      formMessagesDataTable.setValue(this.messages);
    } else {
      messagesModalPanel.setShowWhenRendered(false);
    }
  }

  public List<SelectItem> getFlagMenuItems() {
	  return FlagValue.getFlagMenuItems();
  }
  
  public List<SelectItem> getCustomFlagMenuItems() {
	  if(customFlagMenuItems==null){
		  List<SelectItem> selectItems = new ArrayList<SelectItem>();
		  selectItems.add(new SelectItem("","All"));
		  selectItems.add(new SelectItem("True","Custom Only"));
		  selectItems.add(new SelectItem("False","Non-Custom Only"));

		  customFlagMenuItems = selectItems;
	  }

	  return customFlagMenuItems;
  }
  
  public List<SelectItem> getModifiedFlagMenuItems() {
	  if(modifiedFlagMenuItems==null){
		  List<SelectItem> selectItems = new ArrayList<SelectItem>();
		  selectItems.add(new SelectItem("","All"));
		  selectItems.add(new SelectItem("True","Modified Only"));
		  selectItems.add(new SelectItem("False","Non-Modified Only"));

		  modifiedFlagMenuItems = selectItems;
	  }

	  return modifiedFlagMenuItems;
  }

  public String getCustomFlagFilter() {
    return customFlagFilter;
  }

  public void setCustomFlagFilter(String customFlagFilter) {
    this.customFlagFilter = customFlagFilter;
  }

  public String getModifiedFlagFilter() {
    return modifiedFlagFilter;
  }

  public void setModifiedFlagFilter(String modifiedFlagFilter) {
    this.modifiedFlagFilter = modifiedFlagFilter;
  }

  public JurisdictionDataModel getJurisdictionDataModel() {
    return jurisdictionDataModel;
  }

  public void setJurisdictionDataModel(JurisdictionDataModel jurisdictionDataModel) {
    this.jurisdictionDataModel = jurisdictionDataModel;
  }

  public Boolean getValidJurisdictionSelection() {
    Long id = (selectedJurisdiction == null) ? null : selectedJurisdiction.getJurisdictionId();
    return ((id != null) && (id > 0L));
  }

  public Boolean getCanDeleteJurisdictionSelection() {
    return ((getValidJurisdictionSelection()) && (Boolean.TRUE.equals(selectedJurisdiction.getCustomFlagBoolean())));
  }

  public Boolean getValidJurisdictionTaxrateSelection() {
    Long id = (selectedTaxRate == null) ? null : selectedTaxRate.getJurisdictionTaxrateId();
    return ((id != null) && (id > 0L));
  }

  public Boolean getCanDeleteJurisdictionTaxrateSelection() {
    return ((getValidJurisdictionTaxrateSelection()) && (Boolean.TRUE.equals(selectedTaxRate.getCustomFlagBoolean())));
  }

  public TaxJurisdictionAction getCurrentAction() {
    return currentAction;
  }

  public Map<String, FieldOptions> getCurrentActionMap() {
    return newActionMap.get(currentAction);
  }

  public boolean getIsShortDetailAction() {
    return currentAction.isShortDetailAction();
  }

  public int getSelectedJurisdictionIndex() {
    return selectedJurisdictionIndex;
  }

  public void setSelectedJurisdictionIndex(int selectedJurisdictionIndex) {
    this.selectedJurisdictionIndex = selectedJurisdictionIndex;
  }

  public int getSelectedTaxRateIndex() {
    return selectedTaxRateIndex;
  }

  public void setSelectedTaxRateIndex(int selectedTaxRateIndex) {
    this.selectedTaxRateIndex = selectedTaxRateIndex;
  }

  public Map<String, String> getRateTypeCodeToRateTypeMap() {
    return rateTypeCodeToRateTypeMap;
  }

  public OptionService getOptionService() {
    return optionService;
  }

  public void setOptionService(OptionService optionService) {
    this.optionService = optionService;
  }

  public String getPageTaxRateDescription() {
    String format = "Displaying rows %d through %d (%d matching rows)";
    if (taxRateList != null && taxRateList.size() > 0) {
      return String.format(format, 1, taxRateList.size(), taxRateList.size());
    } else {
      return String.format(format, 0, 0, 0);
    }
  }
	
	public void sortAction(ActionEvent e) {
		jurisdictionDataModel.toggleSortOrder(e.getComponent().getParent().getId());
	}

	public List<Jurisdiction> getJurisdictionList() {
		return jurisdictionList;
	}

	public void setJurisdictionList(List<Jurisdiction> jurisdictionList) {
		this.jurisdictionList = jurisdictionList;
	}

	public HtmlInputText getStateMaxtaxAmount() {
		return stateMaxtaxAmount;
	}

	public void setStateMaxtaxAmount(HtmlInputText stateMaxtaxAmount) {
		this.stateMaxtaxAmount = stateMaxtaxAmount;
	}

	public HtmlInputText getStateUseTier2RateInput() {
		return stateUseTier2RateInput;
	}
	
	public HtmlInputText getStateSalesTier2RateInput() {
		return stateSalesTier2RateInput;
	}

	public void setStateUseTier2RateInput(HtmlInputText stateUseTier2RateInput) {
		this.stateUseTier2RateInput = stateUseTier2RateInput;
	}
	
	public void setStateSalesTier2RateInput(HtmlInputText stateSalesTier2RateInput) {
		this.stateSalesTier2RateInput = stateSalesTier2RateInput;
	}

	public HtmlCalendar getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(HtmlCalendar effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public HtmlCalendar getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(HtmlCalendar expirationDate) {
		this.expirationDate = expirationDate;
	}
	
	// Fix for Bug 2234
	 /**
	   * This function will generate the MeasureTypeCode Selection pulldown.
	   * 
	   * @param selectedState - State which corresponds with the Measure Type Code.
	   *            If Null or Empty is passed in, display all Measure Type Codes.
	   * 
	   */
	public void getSelectionsForBinaryWeight(String selectedState) {
		//The Measure Type Weight field (measure_type_weight) has been removed from the TaxCode State table (tb_taxcode_state), so all Rate Types are available for all States.
	}
	   
	// Fix for Bug 2234	
	private List<Long> getBinaryCodeForBinaryWeight(Long weight) {

	    List<Long> list = new ArrayList<Long>();
		String bin = Long.toBinaryString(weight);	
		char[] charr = bin.toCharArray();
		for (int i = 0; i < charr.length ; i++) {
	        if ("1".equalsIgnoreCase(""+charr[i])) {
	            int pos = 1;
	            for (int j = 0; j < ((charr.length-1) - i); j++ ) {
	                pos = 2 * pos;	
	            }
	            list.add(new Long(pos));
	        }
		}
		return list;
	}
	@SuppressWarnings("serial")
	public void getRateTypeDesc(){
		rateTypeCodeToRateTypeMap = new HashMap<String, String>() {
	    @SuppressWarnings("unused")
		String get(String key) {
	        String value = super.get(key);
	        return ((value == null) ? "Invalid" : value);
	      }
	    };
		listCodesList = listCodesService.getListCodesByCodeTypeCode(codeTypeCode);
		for (ListCodes listCodes:listCodesList){
			rateTypeCodeToRateTypeMap.put(listCodes.getCodeCode(), listCodes.getDescription());
		}
	}

	public ListCodesService getListCodesService() {
		return listCodesService;
	}

	public void setListCodesService(ListCodesService listCodesService) {
		this.listCodesService = listCodesService;
	}

	public List<ListCodes> getListCodesList() {
		return listCodesList;
	}

	public void setListCodesList(List<ListCodes> listCodesList) {
		this.listCodesList = listCodesList;
	}  
	
	public Boolean getUseWarning() {
		return useWarning;
	}

	public void setUseWarning(Boolean useWarning) {
		this.useWarning = useWarning;
	}

	public Boolean getSalesWarning() {
		return salesWarning;
	}

	public void setSalesWarning(Boolean salesWarning) {
		this.salesWarning = salesWarning;
	}
	
	public boolean processValidateRateType(){
    	if(StringUtils.isBlank(editTaxRate.getRatetypeCode())) {
    		FacesMessage msg = new FacesMessage("Rate Type is required.");
    		msg.setSeverity(FacesMessage.SEVERITY_ERROR);
    		FacesContext.getCurrentInstance().addMessage(null, msg);
    		return false;
    	}
    	else{
    		return true;
    	}
	}
	
	public boolean processValidateTaxRates(){
		if (editTaxRate.isRateAndAmountZero()) {
			FacesMessage msg = new FacesMessage("All Rates/Set Amounts may not be zero.");
			msg.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, msg);
			return false;
		} 
		else{
			return true;
		}
	}

	public boolean processValidateDates(){
		Date effectTS = (Date)editTaxRate.getEffectiveDate();
		Date exprTS = (Date)editTaxRate.getExpirationDate();
		if (effectTS==null) {  
			FacesMessage msg = new FacesMessage("Effective Date may not be blank.");
			msg.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, msg);
			return false; 
		}
		else if (exprTS==null) {
			FacesMessage msg = new FacesMessage("Expiration Date may not be blank.");
			msg.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, msg);
			return false; 
		}
		else if (effectTS!=null && exprTS!=null && exprTS.before(effectTS)) {
			FacesMessage msg = new FacesMessage("Expiration Date may not be earlier than Effective Date.");
			msg.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, msg);
			return false; 
		} 
		else{
			return true;
		}
	}

	public TransactionDetailBean getTransactionDetailBean() {
		return transactionDetailBean;
	}

	public void setTransactionDetailBean(TransactionDetailBean transactionDetailBean) {
		this.transactionDetailBean = transactionDetailBean;
	}

	public List<SelectItem> getNexusIndItems() {
		if(nexusIndItems == null) {
			nexusIndItems = cacheManager.createNexusIndItems();
		}
		return nexusIndItems;
	}

	public SearchJurisdictionHandler getAddressHandler() {
		if(addressHandler == null) {
			addressHandler = new SearchJurisdictionHandler();
		}
		return addressHandler;
	}
	
	public void resetAddressHandler() {
		if(addressHandler == null) {
			addressHandler = new SearchJurisdictionHandler();
		}
		else {
			addressHandler.reset();
		}
		addressHandler.setGeocode(this.editJurisdiction.getGeocode());
		addressHandler.setCountry(this.editJurisdiction.getCountry());
		addressHandler.setState(this.editJurisdiction.getState());
		addressHandler.setCounty(this.editJurisdiction.getCounty());
		addressHandler.setCity(this.editJurisdiction.getCity());
		addressHandler.setZip(this.editJurisdiction.getZip());
		addressHandler.setZipPlus4(this.editJurisdiction.getZipplus4());
	}

	public HtmlInputText getStj1UseRateInputText() {
		return stj1UseRateInputText;
	}

	public void setStj1UseRateInputText(HtmlInputText stj1UseRateInputText) {
		this.stj1UseRateInputText = stj1UseRateInputText;
	}

	public HtmlInputText getStj2UseRateInputText() {
		return stj2UseRateInputText;
	}

	public void setStj2UseRateInputText(HtmlInputText stj2UseRateInputText) {
		this.stj2UseRateInputText = stj2UseRateInputText;
	}

	public HtmlInputText getStj3UseRateInputText() {
		return stj3UseRateInputText;
	}

	public void setStj3UseRateInputText(HtmlInputText stj3UseRateInputText) {
		this.stj3UseRateInputText = stj3UseRateInputText;
	}

	public HtmlInputText getStj4UseRateInputText() {
		return stj4UseRateInputText;
	}

	public void setStj4UseRateInputText(HtmlInputText stj4UseRateInputText) {
		this.stj4UseRateInputText = stj4UseRateInputText;
	}

	public HtmlInputText getStj5UseRateInputText() {
		return stj5UseRateInputText;
	}

	public void setStj5UseRateInputText(HtmlInputText stj5UseRateInputText) {
		this.stj5UseRateInputText = stj5UseRateInputText;
	}

	public HtmlInputText getStj1SalesRateInputText() {
		return stj1SalesRateInputText;
	}

	public void setStj1SalesRateInputText(HtmlInputText stj1SalesRateInputText) {
		this.stj1SalesRateInputText = stj1SalesRateInputText;
	}

	public HtmlInputText getStj2SalesRateInputText() {
		return stj2SalesRateInputText;
	}

	public void setStj2SalesRateInputText(HtmlInputText stj2SalesRateInputText) {
		this.stj2SalesRateInputText = stj2SalesRateInputText;
	}

	public HtmlInputText getStj3SalesRateInputText() {
		return stj3SalesRateInputText;
	}

	public void setStj3SalesRateInputText(HtmlInputText stj3SalesRateInputText) {
		this.stj3SalesRateInputText = stj3SalesRateInputText;
	}

	public HtmlInputText getStj4SalesRateInputText() {
		return stj4SalesRateInputText;
	}

	public void setStj4SalesRateInputText(HtmlInputText stj4SalesRateInputText) {
		this.stj4SalesRateInputText = stj4SalesRateInputText;
	}

	public HtmlInputText getStj5SalesRateInputText() {
		return stj5SalesRateInputText;
	}

	public void setStj5SalesRateInputText(HtmlInputText stj5SalesRateInputText) {
		this.stj5SalesRateInputText = stj5SalesRateInputText;
	}
	
	//Text to hide Okay button when delete is not available.
	public boolean getDisplayOkButton() {
		return (getShowCancelButton() && getShowOkButton());
	}
	
	public boolean getIsDeleteAction( ) {
		return (currentAction == TaxJurisdictionAction.deleteJurisdiction);
	}
	
	public boolean getDisplayOkAndAddTaxRate() {
		
		if(currentAction == TaxJurisdictionAction.updateJurisdiction || currentAction == TaxJurisdictionAction.deleteJurisdiction
				          || currentAction == TaxJurisdictionAction.viewJurisdiction){
			return true;
		}
		
		if(currentAction == TaxJurisdictionAction.copyAddJurisdiction) {
			if(selectedJurisdiction.getCopyRatesFlag()){
				return true;
			}
			else {
				return false;
			}
		}
		
		return !(currentAction == TaxJurisdictionAction.addJurisdiction);
	}
	
	public String viewJurisdiction(Long jurisdictionId,String fromview) {
		if("viewJurisdictionFromTransactionLog".equals(fromview)){
			this.currentAction = TaxJurisdictionAction.viewJurisdictionFromTransactionLog;
		}
		else {
			this.currentAction = TaxJurisdictionAction.viewJurisdictionFromTransaction;
		}
		
		this.selectedJurisdiction = jurisdictionService.findById(jurisdictionId);
		if(this.selectedJurisdiction != null) {
			return processTaxJurisdictionCommand();
		}else {
			return null;
		}
		
	}
	
	//Hidden field used when deleting a jurisdiction to determine whether to display error message
	  private String jurisdictionInUseMessage;
	  
	  public String getJurisdictionInUseMessage() {
	      if(currentAction.equals(TaxJurisdictionAction.deleteJurisdiction)  && jurisdictionInUseMessage == null && checkJurisdictionUsage().size() > 0) {
	    	 setShowOkButton(false);
	    	 
	    	 FacesMessage msg = new FacesMessage("This jurisdiction is used and cannot be deleted.");
	    	 msg.setSeverity(FacesMessage.SEVERITY_ERROR);
	    	 FacesContext currContext = FacesContext.getCurrentInstance();
	    	 currContext.addMessage(null, msg);
	    	 setJurisdictionInUseMessage("in use");
	      }
	      return null;
	  }
	  
	  public void setJurisdictionInUseMessage(String message) {
		  jurisdictionInUseMessage = message;
	  }
	
	public String viewJurisdictionTaxRate(Long jurisdictionTaxRateId,String fromview) {
		if("viewJurisdictionTaxrateFromTransactionLog".equals(fromview)){
			this.currentAction = TaxJurisdictionAction.viewJurisdictionTaxrateFromTransactionLog;
		}else {
			this.currentAction = TaxJurisdictionAction.viewJurisdictionTaxrateFromTransaction;
		}
		 this.selectedTaxRate =  taxRateService.findByIdInitialized(jurisdictionTaxRateId);
		 if(this.selectedTaxRate != null){
			 this.selectedJurisdiction = jurisdictionService.findById(this.selectedTaxRate.getJurisdiction().getId());
				return processTaxJurisdictionCommand();
		 }
		 return null;
    }
	
	public User getCurrentUser() {
		return loginBean.getUser();
   }
}
