package com.ncsts.view.bean;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.faces.event.AbortProcessingException;
import javax.faces.event.ActionEvent;

import org.ajax4jsf.component.html.HtmlAjaxSupport;
import org.apache.log4j.Logger;
import org.richfaces.component.html.HtmlDataTable;

import com.ncsts.dto.FileDTO;

public class DirectoryBrowserBackingBean {
	private Logger logger = Logger.getLogger(DirectoryBrowserBackingBean.class);
	private List<FileDTO> directories = null;
	private String selectedDirectory;
	private boolean displayBackButton = false;
	
	private boolean fromPreference = false;
	private boolean fromImportSpec = false;
	private boolean fromCrystalReport = false;
	private boolean fromRefDoc = false;
	private boolean fromGlExtract = false;
	
	private String forRender = "";
	
	
	private FilePreferenceBackingBean filePreferenceBackingBean ;
	private ImportDefinitionAndSpecsBean importDefAndSpecsBean;
	private SupportingCodeBackingBean supportingCodeBackingBean;
	
	public boolean isFromPreference() {
		return fromPreference;
	}

	public void setFromPreference(boolean fromPreference) {
		this.fromPreference = fromPreference;
	}

	public boolean isFromImportSpec() {
		return fromImportSpec;
	}

	public void setFromImportSpec(boolean fromImportSpec) {
		this.fromImportSpec = fromImportSpec;
	}


	public boolean isDisplayBackButton() {
		return displayBackButton;
	}

	public void setDisplayBackButton(boolean displayBackButton) {
		this.displayBackButton = displayBackButton;
	}

	public List<FileDTO> getDirectories() {
		if (directories == null) {
	        File drives[] = File.listRoots();   
	        if (drives != null) {
            	directories = new ArrayList<FileDTO>();			        	
		        // Loop through the drive list and display the drives.   
		        for (int index = 0; index < drives.length; index++) {   
		            System.out.println("level 1 " + drives[index]); 
		            FileDTO dto = new FileDTO();
		            dto.setFilepath(drives[index].toString());
		            dto.setFilename(drives[index].getName());
		            dto.setParentdir(drives[index].getParent());
		            if (drives[index].isDirectory()) {
		            	dto.setFiletype("directory");
		            } else {
		            	dto.setFiletype("file");
		            }				            
            	    logger.info("L1 -> dto.getFilepath() = " + dto.getFilepath());
            	    logger.info("L1 -> dto.getFilename() = " + dto.getFilename());				            
		            directories.add(dto);
		            File dir = drives[index];
		            File contents [] = dir.listFiles();
		            if (contents != null) {
		                for (int i = 0; i < contents.length; i++) {
				            dto = new FileDTO();
				            dto.setFilepath(contents[i].toString());
				            dto.setFilename(contents[i].getName());	
				            dto.setParentdir(contents[i].getParent());
				            if (contents[i].isDirectory()) {
				            	dto.setFiletype("directory");
				            } else {
				            	dto.setFiletype("file");
				            }
		            	    logger.info("L2 -> dto.getFilepath() = " + dto.getFilepath());
		            	    logger.info("L2 -> dto.getFilename() = " + dto.getFilename());				            	    
				            directories.add(dto);			            	    
		                }
		            }
		        } 		        	
	        }					
		}
		return directories;
	}

	public void setDirectories(List<FileDTO> directories) {
		this.directories = directories;
	}     
	
	public void selectedDirectoryRowChanged (ActionEvent e) throws AbortProcessingException { 
		logger.info("enter selectedDirectoryRowChanged");
		FileDTO dto = new FileDTO();
		HtmlDataTable table = (HtmlDataTable) ((HtmlAjaxSupport) e.getComponent()).getParent();
		dto = (FileDTO)table.getRowData(); 
		logger.info("FILE PATH "+dto.getFilepath());
		File dir = null;
		if(table.getRowData()!=null){
			this.setDisplayBackButton(true);
			this.setSelectedDirectory(dto.getFilepath());
			logger.info("selectedDirectory = " + getSelectedDirectory());
			 dir = new File (getSelectedDirectory());
            File contents [] = dir.listFiles();
            if (contents != null) {
            	directories = new ArrayList<FileDTO>();
                for (int i = 0; i < contents.length; i++) {
            	    dto = new FileDTO();
            	    dto.setFilepath(contents[i].toString());
            	    dto.setFilename(contents[i].getName());
		            dto.setParentdir(contents[i].getParent());
		            if (contents[i].isDirectory()) {
		            	dto.setFiletype("directory");
		            } else {
		            	dto.setFiletype("file");
		            }				            
		            directories.add(dto);			            	    
                }
            }					
		}
		setValues(dir);
		logger.info("exit selectedDirectoryRowChanged");				
	}
	
	
	private void setValues(File dir){
		if(fromPreference){
			if(dir.isFile()){
				forRender = "refDocFilePath, back";
			}else {
				forRender = "dir, refDocFilePath, back";
			}
			filePreferenceBackingBean.setDirectoryPath(getSelectedDirectory());
		}
		
		if(fromImportSpec){
			if(dir.isFile()){
				forRender = "defaultDirectoryForSpec, back";
			}else {
				forRender = "dir, defaultDirectoryForSpec, back";
			}
			
			importDefAndSpecsBean.setDirectoryPath(getSelectedDirectory());
		}
		
		if(fromCrystalReport){
			if(dir.isFile()){
				forRender = "back, defaultDirCrystal";
			}else {
				forRender = "dir,  back, defaultDirCrystal";
			}
			
			filePreferenceBackingBean.setCrystalReportPath(getSelectedDirectory());
		}
		
		if(fromRefDoc){
			if(dir.isFile()){
				forRender = "back, docName, url";
				String pathOnly = dir.getAbsolutePath().substring(0, dir.getAbsolutePath().lastIndexOf("\\"));
				supportingCodeBackingBean.setDocNameAndPath(dir.getName(), pathOnly);
				logger.info("YES, ITS A FILE");
			}else {
				forRender = "dir, back, docName, url";
				logger.info("ITS NOT A FILE");
			}
			
		}
		
		if(fromGlExtract){
			if(dir.isFile()){
				forRender = "back, glExtractFilePath";
			}else {
				forRender = "dir, back, glExtractFilePath";
			
			}
			filePreferenceBackingBean.setGlExtractFilePath(getSelectedDirectory());
			
		}
		
	}
	
	public void searchActionListener(ActionEvent e){
		logger.info("enter searchActionListener");
		setFromImportSpec(true);
		logger.info("exit searchActionListener");				
	}
	
	public void searchActionPreferences(ActionEvent e){
		setFromPreference(true);
	}
	
	public void searchActionCrystalReport(ActionEvent event){
		setFromCrystalReport(true);
	}
	
	public void searchActionReferenceDocument(ActionEvent event){
		setFromRefDoc(true);
	}
	
	public void searchActionGlExtract(ActionEvent event){
		setFromGlExtract(true);
	}
	
	
	
	public void backSearchDirectoryActionListener(ActionEvent e){
		logger.info("enter backSearchDirectoryActionListener"); 
        try {
			File file = new File (getSelectedDirectory());
			logger.info("selected directory = " + this.getSelectedDirectory()); 
			if(!"".equals(file.getParent()) && file.getParent()!=null){
			String parent = file.getParent();
			this.setSelectedDirectory(file.getParent());
			logger.info("parent = " + parent);
			File parentdir = new File (parent);
			File contents [] = parentdir.listFiles();
			if (contents != null) {
				directories = new ArrayList<FileDTO>();	            	
			    for (int i = 0; i < contents.length; i++) {
			        FileDTO dto = new FileDTO();
			        dto.setFilepath(contents[i].toString());
			        dto.setFilename(contents[i].getName());	
			        dto.setParentdir(contents[i].getParent());
			        if (contents[i].isDirectory()) {
			        	dto.setFiletype("directory");
			        } else {
			        	dto.setFiletype("file");
			        }			            
			        directories.add(dto);			            	    
			    }
			}
			}
			else{
				File drives[] = File.listRoots();
		        if (drives != null) {
	            	directories = new ArrayList<FileDTO>();			        	
			            for (int index = 0; index < drives.length; index++) {   
			            System.out.println("level 1 " + drives[index]); 
			            FileDTO dto = new FileDTO();
			            dto.setFilepath(drives[index].toString());
			            dto.setFilename(drives[index].getName());
			            dto.setParentdir(drives[index].getParent());
			            if (drives[index].isDirectory()) {
			            	dto.setFiletype("directory");
			            } else {
			            	dto.setFiletype("file");
			            }				            
			            directories.add(dto);
			            File dir = drives[index];
			            File contents [] = dir.listFiles();
			            if (contents != null) {
			                for (int i = 0; i < contents.length; i++) {
					            dto = new FileDTO();
					            dto.setFilepath(contents[i].toString());
					            dto.setFilename(contents[i].getName());	
					            dto.setParentdir(contents[i].getParent());
					            if (contents[i].isDirectory()) {
					            	dto.setFiletype("directory");
					            } else {
					            	dto.setFiletype("file");
					            }
					            directories.add(dto);			            	    
			                }
			            }
			        } 		        	
		        }	
				
			}
		} catch (RuntimeException e1) {
			e1.printStackTrace();
			logger.info("Exception "+e1.getMessage());
			
		}                
		logger.info("exit backSearchDirectoryActionListener");			
	}
	
	public void clearSearchDirectoryActionListener(ActionEvent e){
		logger.info("enter clearSearchDirectoryActionListener"); 
        this.setDisplayBackButton(false);
        setFromCrystalReport(false);
		setFromPreference(false);
		setFromImportSpec(false);
		setFromRefDoc(false);
		setFromGlExtract(false);
        getDirectories();
		logger.info("exit clearSearchDirectoryActionListener");				
	}			

	public String getSelectedDirectory() {
		return selectedDirectory;
	}

	public void setSelectedDirectory(String selectedDirectory) {
		this.selectedDirectory = selectedDirectory;
	}

	public FilePreferenceBackingBean getFilePreferenceBackingBean() {
		return filePreferenceBackingBean;
	}

	public void setFilePreferenceBackingBean(
			FilePreferenceBackingBean filePreferenceBackingBean) {
		this.filePreferenceBackingBean = filePreferenceBackingBean;
	}

	public ImportDefinitionAndSpecsBean getImportDefAndSpecsBean() {
		return importDefAndSpecsBean;
	}

	public void setImportDefAndSpecsBean(
			ImportDefinitionAndSpecsBean importDefAndSpecsBean) {
		this.importDefAndSpecsBean = importDefAndSpecsBean;
	}

	public boolean isFromCrystalReport() {
		return fromCrystalReport;
	}

	public void setFromCrystalReport(boolean fromCrystalReport) {
		this.fromCrystalReport = fromCrystalReport;
	}

	public SupportingCodeBackingBean getSupportingCodeBackingBean() {
		return supportingCodeBackingBean;
	}

	public void setSupportingCodeBackingBean(
			SupportingCodeBackingBean supportingCodeBackingBean) {
		this.supportingCodeBackingBean = supportingCodeBackingBean;
	}

	public boolean isFromRefDoc() {
		return fromRefDoc;
	}

	public void setFromRefDoc(boolean fromRefDoc) {
		this.fromRefDoc = fromRefDoc;
	}

	public String getForRender() {
		return forRender;
	}

	public void setForRender(String forRender) {
		this.forRender = forRender;
	}

	public boolean isFromGlExtract() {
		return fromGlExtract;
	}

	public void setFromGlExtract(boolean fromGlExtract) {
		this.fromGlExtract = fromGlExtract;
	}


}
