package com.ncsts.view.bean;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlPanelGrid;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.persistence.Column;
import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;

import org.ajax4jsf.component.UIDataAdaptor;
import org.ajax4jsf.component.html.HtmlAjaxSupport;
import org.apache.log4j.Logger;
import org.richfaces.component.html.HtmlCalendar;
import org.richfaces.component.html.HtmlDataTable;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.ncsts.common.ObjectCloner;
import com.ncsts.common.Util;
import com.ncsts.dao.BCPTransactionDetailDAO;
import com.ncsts.dao.GenericDAO;
import com.ncsts.dao.TransactionDetailDAO;
import com.ncsts.domain.BCPPurchaseTransaction;
import com.ncsts.domain.DataDefinitionColumn;
import com.ncsts.domain.Idable;
import com.ncsts.domain.JurisdictionAllocationMatrixDetail;
import com.ncsts.domain.ListCodes;
import com.ncsts.domain.TaxAllocationMatrix;
import com.ncsts.domain.TaxAllocationMatrixDetail;
import com.ncsts.domain.TaxCode;
import com.ncsts.domain.TaxCodePK;
import com.ncsts.exception.PurchaseTransactionProcessingException;
import com.ncsts.domain.PurchaseTransaction;
import com.ncsts.jsf.model.TransactionInMemoryDataModel;
import com.ncsts.services.PurchaseTransactionService;
import com.ncsts.services.TaxCodeDetailService;
import com.ncsts.view.util.ArithmeticUtils;
import com.ncsts.view.util.JsfHelper;
import com.ncsts.view.util.StateObservable;
import com.ncsts.ws.message.MicroApiTransactionDocument;
import com.ncsts.ws.message.PurchaseTransactionDocument;

public class TransactionSplittingToolBean<T extends Idable<Long>> implements Observer, HttpSessionBindingListener {
	private static final Logger logger = Logger.getLogger(TransactionSplittingToolBean.class);
	
	@Autowired
	private TaxCodeDetailService taxCodeDetailService;
	
	@Autowired
	private PurchaseTransactionService purchaseTransactionService;
	
	private HtmlCalendar effectiveDateCalendar;
	private HtmlCalendar expirationDateCalendar;

	
	private StateObservable ov = null;
	private MatrixCommonBean matrixCommonBean;
	
	private TaxAllocationMatrix filterSelectionTaxMatrix = null;
	private TaxAllocationMatrix editFilterSelectionTaxMatrix = null;
	private HtmlPanelGrid filterSelectionPanel;
	
	private T transactionDetail;
	
	private int selectedRowIndex = -1;
	private T selectedTransaction;
	private T selectedSplitTransaction;
	private int selectedSplitRowIndex = -1;
	
	private TransactionInMemoryDataModel<T, Long> transactionDataModel;
	private TransactionInMemoryDataModel<T, Long> splitTransactionDataModel;
	private TransactionDetailBean transactionDetailBean = null;

	private HtmlDataTable trDetailTable;
	private HtmlDataTable splitTransactionDetailTable;
	
	private HtmlPanelGrid originalFilterSelectionPanel;
	private HtmlPanelGrid editFilterSelectionPanel;
	
	private SearchDriverHandler driverHandler = new SearchDriverHandler();
	
	private List<TaxAllocationMatrixDetail> taxAllocationSplits;
	private List<T> splitTransactions;
	private Map<Long, List<JurisdictionAllocationMatrixDetail>> jurisSplitTransactions;
	private List<Long> splitTransactionsDelete;
	
	private BigDecimal originalDistributionAmount = ArithmeticUtils.ZERO;
	
	private BigDecimal distributionAmount = ArithmeticUtils.ZERO;
	private BigDecimal totalDistributionAmount = ArithmeticUtils.ZERO;
	private BigDecimal remainingDistributionAmount = ArithmeticUtils.ZERO;
	
	private BigDecimal freightAmount = ArithmeticUtils.ZERO;
	private BigDecimal totalFreightAmount = ArithmeticUtils.ZERO;
	private BigDecimal remainingFreightAmount = ArithmeticUtils.ZERO;
	
	private BigDecimal discountAmount = ArithmeticUtils.ZERO;
	private BigDecimal totalDiscountAmount = ArithmeticUtils.ZERO;
	private BigDecimal remainingDiscountAmount = ArithmeticUtils.ZERO;
	
	private String comments = null;
	
	private EditAction currentAction = EditAction.VIEW;
	
	private GenericDAO<T,Long> commonDAO;
	
	private Object transactionViewBean;
	
	private TaxAllocationMatrixViewBean taxAllocationMatrixViewBean;
	private TaxAllocationMatrix taxAllocationMatrix = null;
	
	private Long displayWarning = 0L;
	private Long displayGlobalJurisWarning = 0L;
	private Long displayIndividualJurisWarning = 0L;
	
	private SplitSaveOption saveOption = SplitSaveOption.NONE;
	private boolean saveToTaxAllocMatrix = false;
	private boolean splitUpdate = false;
	
	private List<SelectItem> taxCodeItems = null; 
	private Long instanceCreatedId;
	private Long jurisSplitInstanceId;
	Map<Long, Long> instanceIdDetailIdMap = null;
	Map<Long, Boolean> jurisSplitInfoMap = null;
	private boolean globalSplit = false;
	
	@Autowired
	private DriverHandlerBean driverHandlerBean;
	
	public enum SplitSaveOption {
		NONE (0),
		TAXABILITY_ONLY (1),
		BOTH (2),
		NOT_APPLICABLE (3);
		
		private final int value;
		SplitSaveOption(int value) {
			this.value = value;
		}
		
		public int getValue() {
			return value;
		}
	}
	
	public boolean getIsFromTransaction() {	
		return  (transactionDetail!=null && transactionDetail instanceof PurchaseTransaction);
	}
	
	public HtmlCalendar getEffectiveDateCalendar() {
		if (effectiveDateCalendar == null) {
			effectiveDateCalendar = new HtmlCalendar();
		}
		return effectiveDateCalendar;
	}

	public void setEffectiveDateCalendar(HtmlCalendar effectiveDateCalendar) {
		this.effectiveDateCalendar = effectiveDateCalendar;
	}
	
	public HtmlCalendar getExpirationDateCalendar() {
		if (expirationDateCalendar == null) {
			expirationDateCalendar = new HtmlCalendar();
		}
		return expirationDateCalendar;
	}

	public void setExpirationDateCalendar(HtmlCalendar expirationDateCalendar) {
		this.expirationDateCalendar = expirationDateCalendar;
	}
	
	private Date effectiveDate;
	private Date expirationDate;
	private String futureSplit;
	private String matrixComments;
	private String activeFlag;
	
	public String getFutureSplit() {
		return futureSplit;
	}

	public void setFutureSplit(String futureSplit) {
		this.futureSplit = futureSplit;
	}

	public String getmatrixComments() {
		return matrixComments;
	}

	public void setMatrixComments(String matrixComments) {
		this.matrixComments = matrixComments;
	}
	
	public String getActiveFlag() {
		return this.activeFlag;
	}

	public void setActiveFlag(String activeFlag) {
		this.activeFlag = activeFlag;
	}
	
	public Boolean getActiveBooleanFlag() {
    	return "1".equals(this.activeFlag);
	}

	public void setActiveBooleanFlag(Boolean activeFlag) {
		this.activeFlag = activeFlag == Boolean.TRUE ? "1" : "0";
	}
	
	public String getFutureSplitFlag() {
		return this.futureSplit;
	}

	public void setFutureSplitFlag(String futureSplit) {
		this.futureSplit = futureSplit;
	}
	
	public Boolean getFutureSplitBooleanFlag() {
    	return "1".equals(this.futureSplit);
	}

	public void setFutureSplitBooleanFlag(Boolean futureSplit) {
		this.futureSplit = futureSplit == Boolean.TRUE ? "1" : "0";
	}
	
	public Date getEffectiveDate(){
		return effectiveDate;
	}
	public void setEffectiveDate(Date effectiveDate){
		this.effectiveDate = effectiveDate;
	}
	
	public Date getExpirationDate(){
		return expirationDate;
	}
	public void setExpirationDate(Date expirationDate){
		this.expirationDate = expirationDate;
	}
	
	public void initializeDefaults() {
		try {
			Date expDate = (new SimpleDateFormat("MM/dd/yyyy")).parse("12/31/9999");
			setExpirationDate(expDate);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		try {
			Calendar cal = Calendar.getInstance();
			cal.set(Calendar.HOUR_OF_DAY, 0);
			cal.set(Calendar.MINUTE, 0);
			cal.set(Calendar.SECOND, 0);
			cal.set(Calendar.MILLISECOND, 0);
			Date now = cal.getTime();
			setEffectiveDate(now);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		setActiveBooleanFlag(true);
		setFutureSplitBooleanFlag(false);
		setMatrixComments("");
	}
	
	public HtmlPanelGrid getFilterSelectionPanel() {
		try{
			setMatrixProperties();
		}
		catch(Exception e){
			e.printStackTrace();
			filterSelectionPanel =  MatrixCommonBean.getErrorHtmlPanelGrid(TaxAllocationMatrix.DRIVER_CODE+"_"+"filterPanel", e);
		}
		
		if(filterSelectionPanel == null) {
			filterSelectionPanel = MatrixCommonBean.createDriverPanel(
				TaxAllocationMatrix.DRIVER_CODE, matrixCommonBean.getCacheManager(), 
				matrixCommonBean.getUserEntityService().getEntityItemDTO(),
				"filterPanel", 
				"currentSplittingToolBean[0]", "filterSelectionTaxMatrix", "matrixCommonBean", 
				true, false, false, true, false);
			
			matrixCommonBean.prepareTaxDriverPanel(filterSelectionPanel);
			if(!this.splitUpdate) {
				matrixCommonBean.initDriverPanelCheckboxes(filterSelectionPanel, filterSelectionTaxMatrix);
			}
		}
		
		return filterSelectionPanel;
	}
	
	public void setFilterSelectionPanel(HtmlPanelGrid filterSelectionPanel) {
		this.filterSelectionPanel = filterSelectionPanel;
	}
	
	public void setMatrixProperties() {
		if(transactionDetail instanceof BCPPurchaseTransaction) {
			matrixCommonBean.setMatrixProperties((BCPPurchaseTransaction) transactionDetail, filterSelectionTaxMatrix);
			matrixCommonBean.setMatrixProperties((BCPPurchaseTransaction) transactionDetail, editFilterSelectionTaxMatrix);
		}
		else if(transactionDetail instanceof PurchaseTransaction) {
			matrixCommonBean.setMatrixProperties((PurchaseTransaction) transactionDetail, filterSelectionTaxMatrix);
			matrixCommonBean.setMatrixProperties((PurchaseTransaction) transactionDetail, editFilterSelectionTaxMatrix);
		}
		
		if(this.taxAllocationMatrix == null) {
			this.taxAllocationMatrix = new TaxAllocationMatrix();
			
			Calendar cal = Calendar.getInstance();
			cal.clear();
			cal.set(9999, Calendar.DECEMBER, 31);
			this.taxAllocationMatrix.setExpirationDate(cal.getTime());
		}
	}
	
	public Object getCurrentTransaction() {
		return transactionDetail;
	}
	
	public Object getTransactionDataModel() {
		return transactionDataModel;
	}
	
	@Override
	public void valueBound(HttpSessionBindingEvent arg0) {
		StateObservable ov = StateObservable.getInstance();
		this.ov = ov;
		this.ov.addObserver(this);
	}

	@Override
	public void valueUnbound(HttpSessionBindingEvent arg0) {
		if(ov!=null){
			ov.deleteObserver(this);
		}
	}

	@Override
	public void update(Observable o, Object arg) {
		if (o == ov) {
			
		}
	}

	public MatrixCommonBean getMatrixCommonBean() {
		return matrixCommonBean;
	}

	public void setMatrixCommonBean(MatrixCommonBean matrixCommonBean) {
		this.matrixCommonBean = matrixCommonBean;
		
		driverHandler.setDriverReferenceService(matrixCommonBean.getDriverReferenceService());
		driverHandler.setCacheManager(matrixCommonBean.getCacheManager());
	}

	public TaxAllocationMatrix getFilterSelectionTaxMatrix() {
		return filterSelectionTaxMatrix;
	}

	public void setFilterSelectionTaxMatrix(TaxAllocationMatrix filterSelectionTaxMatrix) {
		this.filterSelectionTaxMatrix = filterSelectionTaxMatrix;
	}
	
	public HtmlDataTable getTrDetailTable() {
		trDetailTable = new HtmlDataTable();
		trDetailTable.setId("aMDetailList");
		trDetailTable.setVar("trdetail");
		
		if(transactionDataModel == null) {
			transactionDataModel = new TransactionInMemoryDataModel<T, Long>(transactionDetail.getClass());
		}
		
		if(transactionDetail instanceof BCPPurchaseTransaction) {
			matrixCommonBean.createBCPPurchaseTransactionTable(trDetailTable,
				"currentSplittingToolBean[0]", "transactionSplittingToolBean.transactionDataModel", 
				matrixCommonBean.getBCPTransactionPropertyMap(), false, false, false, false, true);
		}
		else if(transactionDetail instanceof PurchaseTransaction) {
			matrixCommonBean.createTransactionDetailTable(trDetailTable,
					"currentSplittingToolBean[0]", "transactionSplittingToolBean.transactionDataModel", 
					matrixCommonBean.getTransactionPropertyMap(), false, false, false, false, true);
		}
		
		return trDetailTable;
	}
	
	public HtmlDataTable getSplitTransactionDetailTable() {
		splitTransactionDetailTable = new HtmlDataTable();
		splitTransactionDetailTable.setId("aMDetailListSplit");
		splitTransactionDetailTable.setVar("trdetailSplit");
		
		splitTransactionDataModel  = new TransactionInMemoryDataModel<T, Long>(transactionDetail.getClass());
		splitTransactionDataModel.setData(splitTransactions);
		
		if(transactionDetail instanceof BCPPurchaseTransaction) {
			Map<String, DataDefinitionColumn> columns = matrixCommonBean.getBCPTransactionPropertyMap();
			matrixCommonBean.createBCPPurchaseTransactionTable(splitTransactionDetailTable,
				"currentSplittingToolBean[0]", "transactionSplittingToolBean.splitTransactionDataModel", 
				columns, false, false, false, false, true);
		}
		else if(transactionDetail instanceof PurchaseTransaction) {
			Map<String, DataDefinitionColumn> columns = matrixCommonBean.getTransactionPropertyMap();
			matrixCommonBean.createTransactionDetailTable(splitTransactionDetailTable,
					"currentSplittingToolBean[0]", "transactionSplittingToolBean.splitTransactionDataModel", 
					columns, false, false, false, false, true);
		}
		
		return splitTransactionDetailTable;
	}
	
	public void setSplitTransactionDetailTable(HtmlDataTable splitTransactionDetailTable) {
		this.splitTransactionDetailTable = splitTransactionDetailTable;
	}

	public void setTrDetailTable(HtmlDataTable trDetailTable) {
		this.trDetailTable = trDetailTable;
	}
	
	public Long getCurrentTransactionId() {
		Long id = 0L;
		if(transactionDetail instanceof BCPPurchaseTransaction) {
			id = ((BCPPurchaseTransaction)transactionDetail).getBcpTransactionId();
		}
		else if(transactionDetail instanceof PurchaseTransaction) {
			id = ((PurchaseTransaction)transactionDetail).getPurchtransId();
		}
		
		return id;
	}
	
	public String addAction() {
		currentAction = EditAction.ADD;
		
		setMatrixProperties();
		
		this.distributionAmount = this.remainingDistributionAmount;
		this.comments = null;
		
		return "split_transactions_manage";
	}
	
	public void sortAction(ActionEvent e) {
		if(transactionDataModel != null) {
			String id = e.getComponent().getId();
			if(id.startsWith("s_trans_")){
				//PP-357
				transactionDataModel.toggleSortOrder(e.getComponent().getId().replace("s_trans_", ""));
			}
			else{		
				transactionDataModel.toggleSortOrder(e.getComponent().getId().replace("s_", ""));
			}
		}
	}

	public T getTransactionDetail() {
		return transactionDetail;
	}
	
	public void setTransactionDetailBean(TransactionDetailBean transactionDetailBean){
		this.transactionDetailBean = transactionDetailBean;
	}

	@SuppressWarnings("unused")
	public void setTransactionDetail(T transactionDetail) {
		reset();
		
		this.jurisSplitTransactions = new HashMap<Long, List<JurisdictionAllocationMatrixDetail>>();
		if(transactionDetail instanceof BCPPurchaseTransaction) {
			BCPPurchaseTransaction BCPPurchaseTransaction = (BCPPurchaseTransaction) transactionDetail;
			
			if(BCPPurchaseTransaction.getMultiTransCode() != null && BCPPurchaseTransaction.getMultiTransCode().length() > 0 && !"FS".equalsIgnoreCase(BCPPurchaseTransaction.getMultiTransCode())) {
				//split update
				this.splitUpdate = true;
				BCPPurchaseTransaction exampleInstance = new BCPPurchaseTransaction();
				exampleInstance.setSplitSubtransId(BCPPurchaseTransaction.getSplitSubtransId());
				BCPTransactionDetailDAO dao = (BCPTransactionDetailDAO) commonDAO;
				List<BCPPurchaseTransaction> list = dao.findByExample(exampleInstance, Integer.MAX_VALUE, null);
				this.taxAllocationSplits = new ArrayList<TaxAllocationMatrixDetail>();
				if(list != null) {
					this.splitTransactions = new ArrayList<T>();
					this.splitTransactionsDelete = new ArrayList<Long>();
					this.taxAllocationMatrix = new TaxAllocationMatrix();
					this.taxAllocationMatrix.setJurisdictionAllocationMatrixDetails(new ArrayList<JurisdictionAllocationMatrixDetail>());

					Map<Long, Long> bcpIdToAllocSubTransId = new LinkedHashMap<Long, Long>();
					Map<Long, List<JurisdictionAllocationMatrixDetail>> allocSubTransIdToJurisSplit = new HashMap<Long, List<JurisdictionAllocationMatrixDetail>>();
					BigDecimal ta = ArithmeticUtils.ZERO;
					for(BCPPurchaseTransaction t : list) {
						T td = (T) t;
						if("OS".equalsIgnoreCase(t.getMultiTransCode())) {
							this.transactionDetail = td;
							this.originalDistributionAmount = ArithmeticUtils.toBigDecimal(t.getGlLineItmDistAmt());
						}
						else if("OA".equalsIgnoreCase(t.getMultiTransCode())) {
							this.splitTransactions.add(td);
							ta = ArithmeticUtils.add(ta, t.getGlLineItmDistAmt());
							bcpIdToAllocSubTransId.put(t.getId(), t.getAllocationSubtransId());
						}
						else if("A".equalsIgnoreCase(t.getMultiTransCode())) {
							this.splitTransactionsDelete.add(t.getBcpTransactionId());
							JurisdictionAllocationMatrixDetail jd = new JurisdictionAllocationMatrixDetail();
							//jd.setAllocationPercent(ArithmeticUtils.divide(t.getGlLineItmDistAmt(), t.getOrigGlLineItmDistAmt()));
							jd.setJurisdiction(matrixCommonBean.getJurisdictionService().findById(t.getShiptoJurisdictionId()));
							if(!allocSubTransIdToJurisSplit.containsKey(t.getAllocationSubtransId())) {
								allocSubTransIdToJurisSplit.put(t.getAllocationSubtransId(), new ArrayList<JurisdictionAllocationMatrixDetail>());
							}
							allocSubTransIdToJurisSplit.get(t.getAllocationSubtransId()).add(jd);
						}
						else {
							this.splitTransactions.add(td);
							ta = ArithmeticUtils.add(ta, t.getGlLineItmDistAmt());
							bcpIdToAllocSubTransId.put(t.getId(), null);
						}
					}
					
					for(Map.Entry<Long, Long> e : bcpIdToAllocSubTransId.entrySet()) {
						if(e.getValue() == null) {
							this.taxAllocationSplits.add(new TaxAllocationMatrixDetail());
						}
						else {
							List<JurisdictionAllocationMatrixDetail> jsList = allocSubTransIdToJurisSplit.get(e.getValue());
							TaxAllocationMatrixDetail d = new TaxAllocationMatrixDetail();
							this.taxAllocationSplits.add(d);
							if(jsList != null) {
								for(JurisdictionAllocationMatrixDetail jd : jsList) {
									jd.setTaxAllocationMatrixDetailInstanceId(d.getInstanceCreatedId());
									taxAllocationMatrix.getJurisdictionAllocationMatrixDetails().add(jd);
								}
								this.jurisSplitTransactions.put(e.getKey(), jsList);
							}
						}
					}
					
					remainingDistributionAmount = ArithmeticUtils.subtract(originalDistributionAmount, ta);
					totalDistributionAmount = ta;
				}
				else {
					taxAllocationMatrix = null;
				}
				this.saveOption = SplitSaveOption.NOT_APPLICABLE;
			}
			else {
				//new split
				this.transactionDetail = transactionDetail;
				BCPPurchaseTransaction processedTransactionDetail = (BCPPurchaseTransaction) this.transactionDetail; 
				this.originalDistributionAmount = ArithmeticUtils.toBigDecimal(processedTransactionDetail.getGlLineItmDistAmt());
				remainingDistributionAmount = originalDistributionAmount;
				
				this.splitTransactions = new ArrayList<T>();
				this.taxAllocationMatrix = new TaxAllocationMatrix();
				this.taxAllocationMatrix.setJurisdictionAllocationMatrixDetails(new ArrayList<JurisdictionAllocationMatrixDetail>());

				currentAction = EditAction.ADD;
				addMatrixDetail();
			}
		}
		else if(transactionDetail instanceof PurchaseTransaction) {
			this.transactionDetail = transactionDetail;
			PurchaseTransaction processedTransactionDetail = (PurchaseTransaction) this.transactionDetail; 
			this.originalDistributionAmount = ArithmeticUtils.toBigDecimal(processedTransactionDetail.getGlLineItmDistAmt());
			remainingDistributionAmount = originalDistributionAmount;
			
			this.splitTransactions = new ArrayList<T>();
			this.taxAllocationMatrix = new TaxAllocationMatrix();
			this.taxAllocationMatrix.setJurisdictionAllocationMatrixDetails(new ArrayList<JurisdictionAllocationMatrixDetail>());

			currentAction = EditAction.ADD;
			addMatrixDetail();
		}
		
		taxAllocationMatrixViewBean.setGlobalSplit(false);
	}
	
	public String cancelSaveAction() {
		return "split_transactions";
	}
	
	public String cancelAction() {
		reset();
		
		String view = null;
		if(this.transactionDetail instanceof BCPPurchaseTransaction) {
			view = "importmap_transactions";
		}
		else if(this.transactionDetail instanceof PurchaseTransaction) {
			view = "transactions_process";
		}
		
		return view;
	}
	
	public String manageCancelAction() {
		return "split_transactions";
	}
	
	protected T copyTransaction(T source) {
		try {
			return (T)ObjectCloner.deepCopy(source);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	public String okAction() {
		if(distributionAmount.compareTo(ArithmeticUtils.ZERO) == 0) {
			FacesMessage message = new FacesMessage("Distribution amount cannot be 0");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
			return null;
		}
		
		if(currentAction == EditAction.UPDATE) {
			T trans = splitTransactions.get(selectedSplitRowIndex);
			if(transactionDetail instanceof BCPPurchaseTransaction) {
				BCPPurchaseTransaction BCPPurchaseTransaction = (BCPPurchaseTransaction) trans;
				remainingDistributionAmount = ArithmeticUtils.add(remainingDistributionAmount, BCPPurchaseTransaction.getGlLineItmDistAmt());
				remainingFreightAmount = ArithmeticUtils.add(remainingFreightAmount, BCPPurchaseTransaction.getInvoiceFreightAmt());
				remainingDiscountAmount = ArithmeticUtils.add(remainingDiscountAmount, BCPPurchaseTransaction.getInvoiceDiscountAmt());
				totalDistributionAmount = ArithmeticUtils.subtract(totalDistributionAmount, BCPPurchaseTransaction.getGlLineItmDistAmt());
				totalFreightAmount = ArithmeticUtils.subtract(totalFreightAmount, BCPPurchaseTransaction.getInvoiceFreightAmt());
				totalDiscountAmount = ArithmeticUtils.subtract(totalDiscountAmount, BCPPurchaseTransaction.getInvoiceDiscountAmt());
			}
			else if(transactionDetail instanceof PurchaseTransaction) {
				PurchaseTransaction processedTransactionDetail = (PurchaseTransaction) trans;
				remainingDistributionAmount = ArithmeticUtils.add(remainingDistributionAmount, processedTransactionDetail.getGlLineItmDistAmt());
				remainingFreightAmount = ArithmeticUtils.add(remainingFreightAmount, processedTransactionDetail.getInvoiceFreightAmt());
				remainingDiscountAmount = ArithmeticUtils.add(remainingDiscountAmount, processedTransactionDetail.getInvoiceDiscountAmt());
				totalDistributionAmount = ArithmeticUtils.subtract(totalDistributionAmount, processedTransactionDetail.getGlLineItmDistAmt());
				totalFreightAmount = ArithmeticUtils.subtract(totalFreightAmount, processedTransactionDetail.getInvoiceFreightAmt());
				totalDiscountAmount = ArithmeticUtils.subtract(totalDiscountAmount, processedTransactionDetail.getInvoiceDiscountAmt());
			}
		}
		else if(currentAction == EditAction.DELETE) {
			T trans = splitTransactions.get(selectedSplitRowIndex);
			if(transactionDetail instanceof BCPPurchaseTransaction) {
				BCPPurchaseTransaction BCPPurchaseTransaction = (BCPPurchaseTransaction) trans;
				remainingDistributionAmount = ArithmeticUtils.add(remainingDistributionAmount, BCPPurchaseTransaction.getGlLineItmDistAmt());
				remainingFreightAmount = ArithmeticUtils.add(remainingFreightAmount, BCPPurchaseTransaction.getInvoiceFreightAmt());
				remainingDiscountAmount = ArithmeticUtils.add(remainingDiscountAmount, BCPPurchaseTransaction.getInvoiceDiscountAmt());
				totalDistributionAmount = ArithmeticUtils.subtract(totalDistributionAmount, BCPPurchaseTransaction.getGlLineItmDistAmt());
				totalFreightAmount = ArithmeticUtils.subtract(totalFreightAmount, BCPPurchaseTransaction.getInvoiceFreightAmt());
				totalDiscountAmount = ArithmeticUtils.subtract(totalDiscountAmount, BCPPurchaseTransaction.getInvoiceDiscountAmt());
				if(BCPPurchaseTransaction.getBcpTransactionId() != null) {
					this.splitTransactionsDelete.add(BCPPurchaseTransaction.getBcpTransactionId());
				}
			}
			else if(transactionDetail instanceof PurchaseTransaction) {
				PurchaseTransaction processedTransactionDetail = (PurchaseTransaction) trans;
				remainingDistributionAmount = ArithmeticUtils.add(remainingDistributionAmount, processedTransactionDetail.getGlLineItmDistAmt());
				remainingFreightAmount = ArithmeticUtils.add(remainingFreightAmount, processedTransactionDetail.getInvoiceFreightAmt());
				remainingDiscountAmount = ArithmeticUtils.add(remainingDiscountAmount, processedTransactionDetail.getInvoiceDiscountAmt());
				totalDistributionAmount = ArithmeticUtils.subtract(totalDistributionAmount, processedTransactionDetail.getGlLineItmDistAmt());
				totalFreightAmount = ArithmeticUtils.subtract(totalFreightAmount, processedTransactionDetail.getInvoiceFreightAmt());
				totalDiscountAmount = ArithmeticUtils.subtract(totalDiscountAmount, processedTransactionDetail.getInvoiceDiscountAmt());
			}
			
			this.taxAllocationSplits.remove(selectedSplitRowIndex);
			this.splitTransactions.remove(selectedSplitRowIndex);
			
			editFilterSelectionPanel = MatrixCommonBean.createDriverPanel(
					TaxAllocationMatrix.DRIVER_CODE, matrixCommonBean.getCacheManager(), 
					matrixCommonBean.getUserEntityService().getEntityItemDTO(),
					"editfilterPanel", 
					"currentSplittingToolBean[0]", "editFilterSelectionTaxMatrix", "matrixCommonBean", 
					false, true, false, true, false, true);
			
			return "split_transactions";
		}
		
		TaxAllocationMatrixDetail taxAllocationMatrixDetail = new TaxAllocationMatrixDetail();
		List<HtmlInputText> inputControls = this.matrixCommonBean.findInputControls(editFilterSelectionPanel, new ArrayList<HtmlInputText>());
		this.matrixCommonBean.setMatrixProperties(inputControls, taxAllocationMatrixDetail, taxAllocationMatrix == null ? (new TaxAllocationMatrix()).getDriverCount() : taxAllocationMatrix.getDriverCount());
		
		BigDecimal allocationPercent = ArithmeticUtils.divide(distributionAmount, originalDistributionAmount);
		taxAllocationMatrixDetail.setAllocationPercent(allocationPercent);
		taxAllocationMatrixDetail.setComments(comments);
		
		if(currentAction == EditAction.ADD) {
			if(this.taxAllocationSplits == null) {
				this.taxAllocationSplits = new ArrayList<TaxAllocationMatrixDetail>();
			}
			this.taxAllocationSplits.add(taxAllocationMatrixDetail);
			
			if(this.taxAllocationMatrix.getJurisdictionAllocationMatrixDetails() != null) {
				for(JurisdictionAllocationMatrixDetail jd : taxAllocationMatrix.getJurisdictionAllocationMatrixDetails()) {
					if(jd.getTaxAllocationMatrixDetailInstanceId() == null || jd.getTaxAllocationMatrixDetailInstanceId() < 0L) {
						jd.setTaxAllocationMatrixDetailInstanceId(taxAllocationMatrixDetail.getInstanceCreatedId());
					}
				}
			}
			
		}
		else if(currentAction == EditAction.UPDATE) {
			TaxAllocationMatrixDetail d = this.taxAllocationSplits.get(selectedSplitRowIndex);
			taxAllocationMatrixDetail.setInstanceCreatedId(d.getInstanceCreatedId());
			this.taxAllocationSplits.set(selectedSplitRowIndex, taxAllocationMatrixDetail);
		}
		
		List<JurisdictionAllocationMatrixDetail> jurisAllocs = new ArrayList<JurisdictionAllocationMatrixDetail>();
		if(this.taxAllocationMatrix != null && this.taxAllocationMatrix.getJurisdictionAllocationMatrixDetails() != null) {
			for(JurisdictionAllocationMatrixDetail d : this.taxAllocationMatrix.getJurisdictionAllocationMatrixDetails()) {
				if(d.getTaxAllocationMatrixDetailInstanceId() != null && d.getTaxAllocationMatrixDetailInstanceId().equals(taxAllocationMatrixDetail.getInstanceCreatedId())) {
					jurisAllocs.add(d);
				}
			}
		}
		
		T copy = null;
		if(transactionDetail instanceof BCPPurchaseTransaction) {
			BCPPurchaseTransaction BCPPurchaseTransaction = null;
			if(currentAction == EditAction.UPDATE) {
				BCPPurchaseTransaction = (BCPPurchaseTransaction) splitTransactions.get(selectedSplitRowIndex);
				matrixCommonBean.setBCPTransactionProperties(editFilterSelectionTaxMatrix, BCPPurchaseTransaction);
			}
			else {
				BCPPurchaseTransaction = new BCPPurchaseTransaction();
				BeanUtils.copyProperties(transactionDetail, BCPPurchaseTransaction, new String[]{"id", "bcpTransactionId", "instanceCreatedId", "transactionInd"});
				matrixCommonBean.setBCPTransactionProperties(editFilterSelectionTaxMatrix, BCPPurchaseTransaction);
				BCPPurchaseTransaction.setBcpTransactionId(null);
			}
			BCPPurchaseTransaction.setGlLineItmDistAmt(distributionAmount);
			//BCPPurchaseTransaction.setOrigGlLineItmDistAmt(originalDistributionAmount);
			BCPPurchaseTransaction.setComments(comments);
			if(jurisAllocs.size() > 0) {
				this.jurisSplitTransactions.put(BCPPurchaseTransaction.getId(), jurisAllocs);
				BCPPurchaseTransaction.setMultiTransCode("OA");
			}
			else {
				this.jurisSplitTransactions.remove(BCPPurchaseTransaction.getId());
				BCPPurchaseTransaction.setMultiTransCode("S");
			}
			copy = (T) BCPPurchaseTransaction;
		}
		else if(transactionDetail instanceof PurchaseTransaction) {
			PurchaseTransaction processedTransactionDetail = new PurchaseTransaction();
			BeanUtils.copyProperties(transactionDetail, processedTransactionDetail, new String[]{"id", "transactionDetailId", "instanceCreatedId", "transactionInd"});
			matrixCommonBean.setTransactionProperties(editFilterSelectionTaxMatrix, processedTransactionDetail);
			processedTransactionDetail.setPurchtransId(null);
			processedTransactionDetail.setGlLineItmDistAmt(distributionAmount);
			processedTransactionDetail.setComments(comments);
			if(jurisAllocs.size() > 0) {
				this.jurisSplitTransactions.put(processedTransactionDetail.getInstanceCreatedId(), jurisAllocs);
				processedTransactionDetail.setMultiTransCode("OA");
			}
			else {
				this.jurisSplitTransactions.remove(processedTransactionDetail.getInstanceCreatedId());
				processedTransactionDetail.setMultiTransCode("S");	
			}
			copy = (T) processedTransactionDetail;
		}
		
		if(currentAction == EditAction.ADD) {
			if(this.splitTransactions == null) {
				this.splitTransactions = new ArrayList<T>();
			}
			this.splitTransactions.add(copy);
		}
		else if(currentAction == EditAction.UPDATE) {
			this.splitTransactions.set(selectedSplitRowIndex, copy);
			selectedSplitTransaction = copy;
		}
		
		totalDistributionAmount = ArithmeticUtils.add(totalDistributionAmount, distributionAmount);
		remainingDistributionAmount = ArithmeticUtils.subtract(remainingDistributionAmount, distributionAmount);
		distributionAmount = remainingDistributionAmount;
		
		totalFreightAmount = ArithmeticUtils.add(totalFreightAmount, freightAmount);
		remainingFreightAmount = ArithmeticUtils.subtract(remainingFreightAmount, freightAmount);
		freightAmount = remainingFreightAmount;
		
		totalDiscountAmount = ArithmeticUtils.add(totalDiscountAmount, discountAmount);
		remainingDiscountAmount = ArithmeticUtils.subtract(remainingDiscountAmount, discountAmount);
		discountAmount = remainingDiscountAmount;
		
		comments = null;
		
		return "split_transactions";
	}
	
	public void selectedTransactionChanged (ActionEvent e) throws AbortProcessingException {
		UIComponent uiComponent = e.getComponent().getParent();
		UIDataAdaptor table = (UIDataAdaptor) uiComponent;
		selectedTransaction = (T) table.getRowData();
		selectedRowIndex = table.getRowIndex();
	}
	
	public void selectedSplitTransactionChanged (ActionEvent e) throws AbortProcessingException {
		UIComponent uiComponent = e.getComponent().getParent();
		UIDataAdaptor table = (UIDataAdaptor) uiComponent;
		selectedSplitRowIndex = table.getRowIndex();
		selectedSplitTransaction = (T) table.getRowData();
	}
	
	private void reset() {
		currentAction = EditAction.VIEW;
		
		selectedTransaction = null;
		selectedRowIndex = -1;
		filterSelectionTaxMatrix = new TaxAllocationMatrix();
		editFilterSelectionTaxMatrix = new TaxAllocationMatrix();
		filterSelectionPanel = null;
		trDetailTable = null;
		if(transactionDataModel != null) {
			transactionDataModel.setData(null);
		}
		
		if(splitTransactionDataModel != null) {
			splitTransactionDataModel.setData(null);
		}
		
		splitTransactions = null;
		splitTransactionsDelete = null;
		taxAllocationSplits = null;
		
		selectedSplitTransaction = null;
		selectedSplitRowIndex = -1;
		
		originalDistributionAmount = ArithmeticUtils.ZERO;
		
		totalDistributionAmount = ArithmeticUtils.ZERO;
		remainingDistributionAmount = ArithmeticUtils.ZERO;
		
		totalFreightAmount = ArithmeticUtils.ZERO;
		remainingFreightAmount = ArithmeticUtils.ZERO;
		
		totalDiscountAmount = ArithmeticUtils.ZERO;
		remainingDiscountAmount = ArithmeticUtils.ZERO;
		
		saveOption = SplitSaveOption.NONE;
		this.splitUpdate = false;
	}
	
	public boolean getDisplayEdit() {
		return selectedSplitTransaction != null;
	}
	
	public boolean getDisplaySave() {
		return getRemainingGlLineItmDistAmt().compareTo(ArithmeticUtils.ZERO) == 0;
	}
	
	public int getSelectedRowIndex() {
		return selectedRowIndex;
	}
	
	public String updateAction() {
		currentAction = EditAction.UPDATE;
		
		if(transactionDetail instanceof BCPPurchaseTransaction) {
			BCPPurchaseTransaction BCPPurchaseTransaction = (BCPPurchaseTransaction) selectedSplitTransaction;
			matrixCommonBean.setMatrixProperties(BCPPurchaseTransaction, editFilterSelectionTaxMatrix);
			distributionAmount = ArithmeticUtils.toBigDecimal(BCPPurchaseTransaction.getGlLineItmDistAmt());
			freightAmount = ArithmeticUtils.toBigDecimal(BCPPurchaseTransaction.getInvoiceFreightAmt());
			discountAmount = ArithmeticUtils.toBigDecimal(BCPPurchaseTransaction.getInvoiceDiscountAmt());
			comments = BCPPurchaseTransaction.getComments();
		}
		else if(transactionDetail instanceof PurchaseTransaction) {
			PurchaseTransaction processedTransactionDetail = (PurchaseTransaction) selectedSplitTransaction;
			matrixCommonBean.setMatrixProperties(processedTransactionDetail, editFilterSelectionTaxMatrix);
			distributionAmount = ArithmeticUtils.toBigDecimal(processedTransactionDetail.getGlLineItmDistAmt());
			freightAmount = ArithmeticUtils.toBigDecimal(processedTransactionDetail.getInvoiceFreightAmt());
			discountAmount = ArithmeticUtils.toBigDecimal(processedTransactionDetail.getInvoiceDiscountAmt());
			comments = processedTransactionDetail.getComments();
		}
		
		editFilterSelectionPanel = MatrixCommonBean.createDriverPanel(
				TaxAllocationMatrix.DRIVER_CODE, matrixCommonBean.getCacheManager(), 
				matrixCommonBean.getUserEntityService().getEntityItemDTO(),
				"editfilterPanel", 
				"currentSplittingToolBean[0]", "editFilterSelectionTaxMatrix", "matrixCommonBean", 
				false, true, false, true, false, true);
		
		matrixCommonBean.prepareTaxDriverPanel(editFilterSelectionPanel);
		
		return "split_transactions_manage";
	}
	
	public Boolean getInDeleteAction() {
		return currentAction == EditAction.DELETE;
	}
	
	public String deleteAction() {
		currentAction = EditAction.DELETE;
		
		if(transactionDetail instanceof BCPPurchaseTransaction) {
			BCPPurchaseTransaction BCPPurchaseTransaction = (BCPPurchaseTransaction) selectedSplitTransaction;
			matrixCommonBean.setMatrixProperties(BCPPurchaseTransaction, editFilterSelectionTaxMatrix);
			distributionAmount = ArithmeticUtils.toBigDecimal(BCPPurchaseTransaction.getGlLineItmDistAmt());
			freightAmount = ArithmeticUtils.toBigDecimal(BCPPurchaseTransaction.getInvoiceFreightAmt());
			discountAmount = ArithmeticUtils.toBigDecimal(BCPPurchaseTransaction.getInvoiceDiscountAmt());
			comments = BCPPurchaseTransaction.getComments();
		}
		else if(transactionDetail instanceof PurchaseTransaction) {
			PurchaseTransaction processedTransactionDetail = (PurchaseTransaction) selectedSplitTransaction;
			matrixCommonBean.setMatrixProperties(processedTransactionDetail, editFilterSelectionTaxMatrix);
			distributionAmount = ArithmeticUtils.toBigDecimal(processedTransactionDetail.getGlLineItmDistAmt());
			freightAmount = ArithmeticUtils.toBigDecimal(processedTransactionDetail.getInvoiceFreightAmt());
			discountAmount = ArithmeticUtils.toBigDecimal(processedTransactionDetail.getInvoiceDiscountAmt());
			comments = processedTransactionDetail.getComments();
		}
		
		editFilterSelectionPanel = MatrixCommonBean.createDriverPanel(
				TaxAllocationMatrix.DRIVER_CODE, matrixCommonBean.getCacheManager(), 
				matrixCommonBean.getUserEntityService().getEntityItemDTO(),
				"editfilterPanel", 
				"currentSplittingToolBean[0]", "editFilterSelectionTaxMatrix", "matrixCommonBean", 
				true, true, false, true, false, true);
		
		return "split_transactions_manage";
	}
	
	private boolean hasIndividualSplit() {
		if(this.taxAllocationMatrix != null && this.taxAllocationMatrix.getJurisdictionAllocationMatrixDetails() != null) {
			for(JurisdictionAllocationMatrixDetail d : this.taxAllocationMatrix.getJurisdictionAllocationMatrixDetails()) {
				if((d.getTaxAllocationMatrixDetailId() != null && d.getTaxAllocationMatrixDetailId() != 0L) ||
				   (d.getTaxAllocationMatrixDetailInstanceId() != null && d.getTaxAllocationMatrixDetailInstanceId() != 0L)) {
					return true;
				}
			}
		}
		
		return false;
	}
	
	public boolean getDisplayGlobalJurisSplitEnabled() {
		boolean enabled = false;
		if(this.taxAllocationMatrix != null && this.taxAllocationMatrix.getJurisdictionAllocationMatrixDetails() != null) {
			for(JurisdictionAllocationMatrixDetail d : this.taxAllocationMatrix.getJurisdictionAllocationMatrixDetails()) {
				if((d.getTaxAllocationMatrixDetailId() != null && d.getTaxAllocationMatrixDetailId() == 0L) ||
				   (d.getTaxAllocationMatrixDetailInstanceId() != null && d.getTaxAllocationMatrixDetailInstanceId() == 0L)) {
					enabled = true;
					break;
				}
			}
		}
		
		return enabled;
	}
	
	public String globalJurisSplitAction() {
		if(displayGlobalJurisWarning == 0L && hasIndividualSplit()) {
			displayGlobalJurisWarning = 1L;
			return null;
		}
		else {
			displayGlobalJurisWarning = 0L;
		}
		
		taxAllocationMatrixViewBean.setJurisOkAction("split_transactions");
		
		List<JurisdictionAllocationMatrixDetail> jamd = taxAllocationMatrix.getJurisdictionAllocationMatrixDetails();
		matrixCommonBean.setMatrixProperties(filterSelectionTaxMatrix, taxAllocationMatrix);
		taxAllocationMatrix.setTaxAllocationMatrixDetails(this.taxAllocationSplits);
		taxAllocationMatrix.setJurisdictionAllocationMatrixDetails(jamd);
		
		return taxAllocationMatrixViewBean.prepareJurisSplit(taxAllocationMatrix, 0L, EditAction.ADD_FROM_TRANSACTION);
	}
	
	public String jurisSplitAction() {
		if(displayIndividualJurisWarning == 0L && getDisplayGlobalJurisSplitEnabled()) {
			displayIndividualJurisWarning = 1L;
			return null;
		}
		else {
			displayIndividualJurisWarning = 0L;
		}
		
		Long jurisSplitInstanceId = -1L;
		if(currentAction.equals(EditAction.UPDATE) || currentAction.equals(EditAction.DELETE)) {
			TaxAllocationMatrixDetail detail = this.taxAllocationSplits.get(selectedSplitRowIndex);
			jurisSplitInstanceId = detail.getInstanceCreatedId();
		}
		
		taxAllocationMatrixViewBean.setJurisOkAction("split_transactions_manage");
		EditAction action = currentAction.equals(EditAction.DELETE) ? EditAction.DELETE : EditAction.ADD_FROM_TRANSACTION;
		
		List<JurisdictionAllocationMatrixDetail> jamd = taxAllocationMatrix.getJurisdictionAllocationMatrixDetails();
		matrixCommonBean.setMatrixProperties(editFilterSelectionTaxMatrix, taxAllocationMatrix);
		taxAllocationMatrix.setTaxAllocationMatrixDetails(this.taxAllocationSplits);
		taxAllocationMatrix.setJurisdictionAllocationMatrixDetails(jamd);
		
		return taxAllocationMatrixViewBean.prepareJurisSplit(taxAllocationMatrix, jurisSplitInstanceId, action);
	}
	
	public String individualJurisSplitAction_jeesmon() {
		if(displayIndividualJurisWarning == 0L && getDisplayGlobalJurisSplitEnabled()) {
			displayIndividualJurisWarning = 1L;
			return "split_transactions";
		}
		else {
			displayIndividualJurisWarning = 0L;
		}
		
		for(TaxAllocationMatrixDetail t : this.taxAllocationSplits) {
			
			if(t.getInstanceCreatedId() != null && t.getInstanceCreatedId().equals(jurisSplitInstanceId)) {
				if(t.getTaxcodeCode()==null || t.getTaxcodeCode().length()==0){
					return null;//taxCode not selected yet.
				}
			}
		}
		
		taxAllocationMatrixViewBean.setJurisOkAction("split_transactions");
		
		EditAction action = currentAction.equals(EditAction.DELETE) ? EditAction.DELETE : EditAction.ADD_FROM_TRANSACTION;
		
		List<JurisdictionAllocationMatrixDetail> jamd = taxAllocationMatrix.getJurisdictionAllocationMatrixDetails();
		matrixCommonBean.setMatrixProperties(editFilterSelectionTaxMatrix, taxAllocationMatrix);
		taxAllocationMatrix.setTaxAllocationMatrixDetails(this.taxAllocationSplits);
		taxAllocationMatrix.setJurisdictionAllocationMatrixDetails(jamd);
		
		return taxAllocationMatrixViewBean.prepareJurisSplit(taxAllocationMatrix, jurisSplitInstanceId, action);
	}
	
	public String viewSplitAction() {
		if(transactionDetail instanceof BCPPurchaseTransaction) {
			BCPTransactionViewBean viewBean = (BCPTransactionViewBean) transactionViewBean;
			viewBean.setSelectedTransaction((BCPPurchaseTransaction) selectedSplitTransaction);
			viewBean.setOkAction("split_transactions");
			return "bcp_trans_view";
		}
		else if(transactionDetail instanceof PurchaseTransaction) {
			TransactionViewBean viewBean = (TransactionViewBean) transactionViewBean;
			viewBean.setSelectedTransaction((PurchaseTransaction) selectedSplitTransaction);
			viewBean.setOkAction("split_transactions");
			return "trans_view";
		}
		
		return null;
	}
	
	public String viewCurrentAction() {
		if(transactionDetail instanceof BCPPurchaseTransaction) {
			BCPTransactionViewBean viewBean = (BCPTransactionViewBean) transactionViewBean;
			viewBean.setSelectedTransaction((BCPPurchaseTransaction) transactionDetail);
			viewBean.setOkAction("split_transactions");
			return "bcp_trans_view";
		}
		else if(transactionDetail instanceof PurchaseTransaction) {
			TransactionViewBean viewBean = (TransactionViewBean) transactionViewBean;
			viewBean.setSelectedTransaction((PurchaseTransaction) transactionDetail);
			viewBean.setOkAction("split_transactions");
			return "trans_view";
		}
		
		return null;
	}
	
	private boolean calculateTaxAllocationPercent(List<TaxAllocationMatrixDetail> taxAllocationMatrixDetails){		
		if(taxAllocationMatrixDetails!=null && taxAllocationMatrixDetails.size() > 0){
			BigDecimal totalGlLineItmDistAmt = getTotalGlLineItmDistAmt(taxAllocationMatrixDetails);
			if(totalGlLineItmDistAmt==null || totalGlLineItmDistAmt.compareTo(ArithmeticUtils.ZERO) == 0){
					FacesMessage message = new FacesMessage("Total Distribution amount cannot be 0.");
					message.setSeverity(FacesMessage.SEVERITY_ERROR);
					FacesContext.getCurrentInstance().addMessage(null, message);
					return false;
			}
			
			BigDecimal glLineItmDistAmt = ArithmeticUtils.ZERO;
			for (TaxAllocationMatrixDetail detail : taxAllocationSplits) {
				glLineItmDistAmt = detail.getGlLineItmDistAmt();
				if (glLineItmDistAmt== null || glLineItmDistAmt.compareTo(ArithmeticUtils.ZERO) == 0) {
					FacesMessage message = new FacesMessage("Distribution amount cannot be 0.");
					message.setSeverity(FacesMessage.SEVERITY_ERROR);
					FacesContext.getCurrentInstance().addMessage(null, message);
					return false;
				}	
				
				detail.setAllocationPercent(ArithmeticUtils.divide(glLineItmDistAmt, totalGlLineItmDistAmt));
			}
		}
		
		return true;
	}
	
	public String saveAsMatrixAction() {	
		if(!calculateTaxAllocationPercent(this.taxAllocationSplits)){return null;}
		
		initializeDefaults();

		return "confirm_taxallocation";
	}
	
	public String saveOneTimeAction() {
		this.saveOption = SplitSaveOption.NONE; //Transaction only
		this.saveToTaxAllocMatrix = false;
		return saveAction();
	}
	
	public String saveTaxabilityAction() {
		this.saveOption = SplitSaveOption.TAXABILITY_ONLY; //Transaction & Taxability
		this.saveToTaxAllocMatrix = true;
		
		return saveAction();
	}
	
	public String saveTaxabilityJurisdictionAction() {
		this.saveOption = SplitSaveOption.BOTH; //Transaction, Taxability & Jurisdiction
		this.saveToTaxAllocMatrix = true;
		
		if(!calculateTaxAllocationPercent(this.taxAllocationSplits)){return null;}
		
		return saveAction();
	}
	
	@SuppressWarnings("unchecked")
	public String saveAction() {
		/*
		if(!this.saveOption.equals(SplitSaveOption.NOT_APPLICABLE) && (this.taxAllocationSplits.size() > 0 || this.taxAllocationMatrix.getJurisdictionAllocationMatrixDetails().size() > 0)) {
			if(displayWarning == 0L) {
				this.saveOption = SplitSaveOption.TAXABILITY_ONLY;
				this.saveToTaxAllocMatrix = false;
				
				displayWarning = 1L;
				return null;
			}
			else {
				displayWarning = 0L;
			}
		}
		*/
		logger.debug("this.saveToTaxAllocMatrix: " + this.saveToTaxAllocMatrix);
		logger.debug("this.saveOption: " + this.saveOption);
		
		if(!this.saveOption.equals(SplitSaveOption.NOT_APPLICABLE) && !this.saveOption.equals(SplitSaveOption.NONE) && this.saveToTaxAllocMatrix) {
			if(this.saveOption.equals(SplitSaveOption.TAXABILITY_ONLY)) {
				this.taxAllocationMatrix.getJurisdictionAllocationMatrixDetails().clear();
			}
			
			taxAllocationMatrix.initNullDriverFields();
			
			//BeanUtils.copyProperties set jurisdictionAllocationMatrixDetails to null. so save it before copy
			List<JurisdictionAllocationMatrixDetail> jurisDetails = taxAllocationMatrix.getJurisdictionAllocationMatrixDetails();
			BeanUtils.copyProperties(filterSelectionTaxMatrix, taxAllocationMatrix);
			taxAllocationMatrix.setJurisdictionAllocationMatrixDetails(jurisDetails);
			
			for (String property : matrixCommonBean.findUncheckedDrivers(filterSelectionPanel, "DRIVERPROP")) {
				Util.setProperty(taxAllocationMatrix, property, "*ALL", String.class);
			}
			
			this.taxAllocationMatrix.setTaxAllocationMatrixDetails(this.taxAllocationSplits);
					
			taxAllocationMatrix.setEffectiveDate(getEffectiveDate());
			taxAllocationMatrix.setExpirationDate(getExpirationDate());
			taxAllocationMatrix.setFutureSplit(this.getFutureSplitFlag());
			taxAllocationMatrix.setActiveFlag(this.getActiveFlag());
			taxAllocationMatrix.setComments(this.getmatrixComments());
			
			this.taxAllocationMatrixViewBean.setFutureSplit(this.getFutureSplitBooleanFlag());//TaxAlloc Detail will be reset
			this.taxAllocationMatrixViewBean.setSelectedMatrix(taxAllocationMatrix);
			this.taxAllocationMatrixViewBean.setViewPanel(filterSelectionPanel);
			String result = this.taxAllocationMatrixViewBean.saveFromSplittingTool();
			if(result == null) {
				return null;
			}
		}
		
		String view = null;
		if(transactionDetail instanceof BCPPurchaseTransaction) {
			if(taxAllocationSplits!=null && taxAllocationSplits.size()>0){
				this.splitTransactions = new ArrayList<T>(); 
				this.jurisSplitTransactions = new HashMap<Long, List<JurisdictionAllocationMatrixDetail>>();
				
				for(TaxAllocationMatrixDetail allocationMatrixDetail : taxAllocationSplits) {
					
					BCPPurchaseTransaction jtd = new BCPPurchaseTransaction();
					BeanUtils.copyProperties(transactionDetail, jtd, new String[]{"id", "transactionDetailId", "instanceCreatedId", "transactionInd"});
					jtd.setSplitSubtransId(null); //Set in DAO
					jtd.setBcpPurchtransId(null);
					jtd.setTaxcodeCode(allocationMatrixDetail.getTaxcodeCode());	
					jtd.setMultiTransCode("S"); //The (unALLOCATED) SPLIT transactions = S, null  (these are regular, normal SPLITS)
					jtd.setManualTaxcodeInd(null); //Assume no Juris on split tran					
					//jtd.setOrigGlLineItmDistAmt(((BCPPurchaseTransaction) transactionDetail).getGlLineItmDistAmt());					
					jtd.setGlLineItmDistAmt(allocationMatrixDetail.getGlLineItmDistAmt());
					jtd.setInvoiceFreightAmt(allocationMatrixDetail.getInvoiceFreightAmt());
					jtd.setInvoiceDiscountAmt(allocationMatrixDetail.getInvoiceDiscountAmt());					
					splitTransactions.add((T)jtd);	
					
					if(this.taxAllocationMatrix != null && this.taxAllocationMatrix.getJurisdictionAllocationMatrixDetails() != null) {
						//Get Jurisdiction Map
						List<JurisdictionAllocationMatrixDetail> jurisAllocs = new ArrayList<JurisdictionAllocationMatrixDetail>();
						for(JurisdictionAllocationMatrixDetail d : this.taxAllocationMatrix.getJurisdictionAllocationMatrixDetails()) {
							if(d.getTaxAllocationMatrixDetailInstanceId() != null && d.getTaxAllocationMatrixDetailInstanceId().equals(allocationMatrixDetail.getInstanceCreatedId())) {
								jurisAllocs.add(d);
							}
						}						
						if(jurisAllocs.size() > 0) {
							this.jurisSplitTransactions.put(jtd.getId(), jurisAllocs);
						}
					}					
				}
			}
			
			BCPTransactionDetailDAO dao = (BCPTransactionDetailDAO) commonDAO;
			BCPPurchaseTransaction t = (BCPPurchaseTransaction) transactionDetail;
			t.setMultiTransCode("OS");
			
			//create juris allocations
			List<BCPPurchaseTransaction> jurisSplits = new ArrayList<BCPPurchaseTransaction>();
			if(this.taxAllocationMatrix != null && this.taxAllocationMatrix.getJurisdictionAllocationMatrixDetails() != null) {
				boolean globalAlloc = false;
				for(JurisdictionAllocationMatrixDetail jd : this.taxAllocationMatrix.getJurisdictionAllocationMatrixDetails()) {
					if(jd.getTaxAllocationMatrixDetailInstanceId() != null && jd.getTaxAllocationMatrixDetailInstanceId() == 0L) {
						globalAlloc = true;
						break;
					}
				}
				
				if(globalAlloc) {
					for(T td : splitTransactions) {
						BCPPurchaseTransaction btd = (BCPPurchaseTransaction) td;
						if(btd != null) {
							Long allocationSubtransId = dao.getNextAllocSubTransId();
							btd.setMultiTransCode("OA");
							btd.setAllocationSubtransId(allocationSubtransId);
							for(JurisdictionAllocationMatrixDetail jd : this.taxAllocationMatrix.getJurisdictionAllocationMatrixDetails()) {
								BCPPurchaseTransaction jtd = new BCPPurchaseTransaction();
								BeanUtils.copyProperties(btd, jtd, new String[]{"id", "bcpTransactionId", "instanceCreatedId", "transactionInd"});
								jtd.setGlLineItmDistAmt(ArithmeticUtils.multiply(btd.getGlLineItmDistAmt(), jd.getAllocationPercent()));
								//jtd.setOrigGlLineItmDistAmt(btd.getGlLineItmDistAmt());
								jtd.setMultiTransCode("A");
								jtd.setShiptoJurisdictionId(jd.getJurisdiction().getJurisdictionId());
								jtd.setAllocationSubtransId(allocationSubtransId);
								jtd.setAllocationMatrixId(-1L);
								jurisSplits.add(jtd);
							}
						}
					}
				}
				else {
					for(T td : splitTransactions) {
						BCPPurchaseTransaction btd = (BCPPurchaseTransaction) td;
						if(btd != null) {
							if(this.jurisSplitTransactions.containsKey(btd.getId())) {
								Long allocationSubtransId = dao.getNextAllocSubTransId();
								btd.setMultiTransCode("OA");
								btd.setAllocationSubtransId(allocationSubtransId);
								for(JurisdictionAllocationMatrixDetail jd : this.jurisSplitTransactions.get(btd.getId())) {
									BCPPurchaseTransaction jtd = new BCPPurchaseTransaction();
									BeanUtils.copyProperties(btd, jtd, new String[]{"id", "bcpTransactionId", "instanceCreatedId", "transactionInd"});
									jtd.setGlLineItmDistAmt(ArithmeticUtils.multiply(btd.getGlLineItmDistAmt(), jd.getAllocationPercent()));
									//jtd.setOrigGlLineItmDistAmt(btd.getGlLineItmDistAmt());
									jtd.setMultiTransCode("A");
									jtd.setShiptoJurisdictionId(jd.getJurisdiction().getJurisdictionId());
									jtd.setAllocationSubtransId(allocationSubtransId);
									jtd.setAllocationMatrixId(-1L);
									jurisSplits.add(jtd);
								}
							}
						}
					}
				}
			}
						
			dao.saveSplits(t, (List<BCPPurchaseTransaction>) splitTransactions, this.splitTransactionsDelete, jurisSplits);
			view = "importmap_transactions";
		}
		else if(transactionDetail instanceof PurchaseTransaction) {
			if(taxAllocationSplits!=null && taxAllocationSplits.size()>0){
				this.splitTransactions = new ArrayList<T>(); 
				this.jurisSplitTransactions = new HashMap<Long, List<JurisdictionAllocationMatrixDetail>>();
				
				for(TaxAllocationMatrixDetail allocationMatrixDetail : taxAllocationSplits) {
					
					PurchaseTransaction jtd = new PurchaseTransaction();
					BeanUtils.copyProperties(transactionDetail, jtd, new String[]{"id", "transactionDetailId", "instanceCreatedId", "transactionInd"});
					jtd.setSplitSubtransId(null); //Set in DAO
					jtd.setPurchtransId(null);
					jtd.setTaxcodeCode(allocationMatrixDetail.getTaxcodeCode());	
					jtd.setMultiTransCode("S"); //The (unALLOCATED) SPLIT transactions = S, null  (these are regular, normal SPLITS)
					jtd.setManualTaxcodeInd(null); //Assume no Juris on split tran					
					jtd.setGlLineItmDistAmt(allocationMatrixDetail.getGlLineItmDistAmt());
					jtd.setInvoiceFreightAmt(allocationMatrixDetail.getInvoiceFreightAmt());
					jtd.setInvoiceDiscountAmt(allocationMatrixDetail.getInvoiceDiscountAmt());					
					splitTransactions.add((T)jtd);	
					
					if(this.taxAllocationMatrix != null && this.taxAllocationMatrix.getJurisdictionAllocationMatrixDetails() != null) {
						//Get Jurisdiction Map
						List<JurisdictionAllocationMatrixDetail> jurisAllocs = new ArrayList<JurisdictionAllocationMatrixDetail>();
						for(JurisdictionAllocationMatrixDetail d : this.taxAllocationMatrix.getJurisdictionAllocationMatrixDetails()) {
							if(d.getTaxAllocationMatrixDetailInstanceId() != null && d.getTaxAllocationMatrixDetailInstanceId().equals(allocationMatrixDetail.getInstanceCreatedId())) {
								jurisAllocs.add(d);
							}
						}						
						if(jurisAllocs.size() > 0) {
							this.jurisSplitTransactions.put(jtd.getInstanceCreatedId(), jurisAllocs);
						}
					}					
				}
			}
			
			TransactionDetailDAO dao = (TransactionDetailDAO) commonDAO;
			PurchaseTransaction t = (PurchaseTransaction) transactionDetail;
			t.setMultiTransCode("OS");
			t.setTransactionInd("O");
			t.setSuspendInd(null);
			
			//create juris allocations
			List<PurchaseTransaction> jurisSplits = new ArrayList<PurchaseTransaction>();
			if(this.taxAllocationMatrix != null && this.taxAllocationMatrix.getJurisdictionAllocationMatrixDetails() != null) {
				boolean globalAlloc = false;
				for(JurisdictionAllocationMatrixDetail jd : this.taxAllocationMatrix.getJurisdictionAllocationMatrixDetails()) {
					if(jd.getTaxAllocationMatrixDetailInstanceId() != null && jd.getTaxAllocationMatrixDetailInstanceId() == 0L) {
						globalAlloc = true;
						break;
					}
				}
				
				if(globalAlloc) {
					for(T td : splitTransactions) {
						PurchaseTransaction btd = (PurchaseTransaction) td;
						if(btd != null) {
							Long allocationSubtransId = dao.getNextAllocSubTransId();
							btd.setAllocationSubtransId(allocationSubtransId);
							for(JurisdictionAllocationMatrixDetail jd : this.taxAllocationMatrix.getJurisdictionAllocationMatrixDetails()) {
								PurchaseTransaction jtd = new PurchaseTransaction();
								BeanUtils.copyProperties(btd, jtd, new String[]{"id", "transactionDetailId", "instanceCreatedId", "transactionInd"});
								jtd.setGlLineItmDistAmt(ArithmeticUtils.toBigDecimal(btd.getGlLineItmDistAmt()).multiply(jd.getAllocationPercent()));
								jtd.setMultiTransCode("A");//these are the Jurisdiction ALLOCATIONS
								jtd.setTransactionInd(null);

								btd.setMultiTransCode("OA");//SPLIT transactions with global or local ALLOCATIONs 
								btd.setTransactionInd("O");//
								
								jtd.setShiptoJurisdictionId(jd.getJurisdiction().getJurisdictionId());
								jtd.setAllocationSubtransId(allocationSubtransId);
								jtd.setAllocationMatrixId(-1L);
								jurisSplits.add(jtd);
							}
						}
					}
				}
				else {
					for(T td : splitTransactions) {
						PurchaseTransaction btd = (PurchaseTransaction) td;
						if(btd != null) {
							if(this.jurisSplitTransactions.containsKey(btd.getInstanceCreatedId())) {
								Long allocationSubtransId = dao.getNextAllocSubTransId();
								btd.setAllocationSubtransId(allocationSubtransId);
								for(JurisdictionAllocationMatrixDetail jd : this.jurisSplitTransactions.get(btd.getInstanceCreatedId())) {
									PurchaseTransaction jtd = new PurchaseTransaction();
									BeanUtils.copyProperties(btd, jtd, new String[]{"id", "transactionDetailId", "instanceCreatedId", "transactionInd"});
									jtd.setGlLineItmDistAmt(ArithmeticUtils.toBigDecimal(btd.getGlLineItmDistAmt()).multiply(jd.getAllocationPercent()));
									jtd.setMultiTransCode("A");	//these are the Jurisdiction ALLOCATIONS								
									jtd.setTransactionInd(null);
									
									btd.setMultiTransCode("OA");//SPLIT transactions with global or local ALLOCATIONs 
									btd.setTransactionInd("O");//
									
									jtd.setShiptoJurisdictionId(jd.getJurisdiction().getJurisdictionId());
									jtd.setAllocationSubtransId(allocationSubtransId);
									jtd.setAllocationMatrixId(-1L);
									jurisSplits.add(jtd);
								}
							}
						}
					}
				}
			}
			
			
			dao.saveSplits(t, (List<PurchaseTransaction>) splitTransactions, this.splitTransactionsDelete, jurisSplits);

			PurchaseTransactionDocument ptDoc = new PurchaseTransactionDocument();
			ptDoc.setPurchaseTransaction((List<PurchaseTransaction>) splitTransactions);
			MicroApiTransactionDocument doc = new MicroApiTransactionDocument(ptDoc);
			try {
				purchaseTransactionService.getPurchaseTransactionEvaluateForAccrual(doc, true);
			} catch (PurchaseTransactionProcessingException e) {
				logger.error("Unable to process split transactions" + e.getStackTrace());
				
			}
			
			
			String errorCode = dao.callTransactionProcessProc();
			if(errorCode != null && errorCode.length() > 0) {
				ListCodes code = matrixCommonBean.getCacheManager().getErrorListCodes().get(errorCode);
				if(code != null) {
					FacesMessage message = new FacesMessage(code.getMessage());
					message.setSeverity(FacesMessage.SEVERITY_ERROR);
					FacesContext.getCurrentInstance().addMessage(null, message);
					return null;
				}
			}
			
			if(transactionDetailBean!=null){
				transactionDetailBean.listTransactions();
			}
			
			view = "transactions_process";			
		}
		
		return view;
	}

	public HtmlPanelGrid getOriginalFilterSelectionPanel() {
		try{
			setMatrixProperties();
		}
		catch(Exception e){
			e.printStackTrace();
			originalFilterSelectionPanel =  MatrixCommonBean.getErrorHtmlPanelGrid(TaxAllocationMatrix.DRIVER_CODE+"_"+"filterPanel", e);
		}
			
		if(originalFilterSelectionPanel == null) {
			originalFilterSelectionPanel = MatrixCommonBean.createDriverPanel(
				TaxAllocationMatrix.DRIVER_CODE, matrixCommonBean.getCacheManager(), 
				matrixCommonBean.getUserEntityService().getEntityItemDTO(),
				"filterPanel", 
				"currentSplittingToolBean[0]", "filterSelectionTaxMatrix", "matrixCommonBean", 
				true, true, false, true, false);
		}
		
		matrixCommonBean.prepareTaxDriverPanel(originalFilterSelectionPanel);
		
		return originalFilterSelectionPanel;
	}

	public void setOriginalFilterSelectionPanel(
			HtmlPanelGrid originalFilterSelectionPanel) {
		this.originalFilterSelectionPanel = originalFilterSelectionPanel;
	}

	public HtmlPanelGrid getEditFilterSelectionPanel() {
		if(editFilterSelectionPanel == null) {
			editFilterSelectionPanel = MatrixCommonBean.createDriverPanel(
				TaxAllocationMatrix.DRIVER_CODE, matrixCommonBean.getCacheManager(), 
				matrixCommonBean.getUserEntityService().getEntityItemDTO(),
				"editfilterPanel", 
				"currentSplittingToolBean[0]", "editFilterSelectionTaxMatrix", "matrixCommonBean", 
				false, true, false, true, false, true);
		}
		
		matrixCommonBean.prepareTaxDriverPanel(editFilterSelectionPanel);
		
		return editFilterSelectionPanel;
	}

	public void setEditFilterSelectionPanel(HtmlPanelGrid editFilterSelectionPanel) {
		this.editFilterSelectionPanel = editFilterSelectionPanel;
	}

	public SearchDriverHandler getDriverHandler() {
		return driverHandler;
	}

	public void setDriverHandler(SearchDriverHandler driverHandler) {
		this.driverHandler = driverHandler;
	}
	
	public void searchDriver(ActionEvent e) {
		driverHandlerBean.initSearch(
				editFilterSelectionTaxMatrix, 
				(HtmlInputText) e.getComponent().getParent().getChildren().get(0), e.getComponent());
		driverHandlerBean.setCallBackScreen(null);
	}
	
	public boolean getDisplayAdd() {
		return true;
	}
	
	public boolean getDisplayAddJurisSplitEnabled() {
		if(currentAction.equals(EditAction.ADD)) {
			if(taxAllocationMatrix != null && taxAllocationMatrix.getJurisdictionAllocationMatrixDetails() != null) {
				for(JurisdictionAllocationMatrixDetail d : taxAllocationMatrix.getJurisdictionAllocationMatrixDetails()) {
					if(d.getTaxAllocationMatrixDetailInstanceId() == -1L) {
						return true;
					}
				}
			}
		}
		else if(currentAction.equals(EditAction.UPDATE) || currentAction.equals(EditAction.DELETE)) {
			TaxAllocationMatrixDetail detail = this.taxAllocationSplits.get(selectedSplitRowIndex);
			Long jurisSplitInstanceId = detail.getInstanceCreatedId();
			
			if(taxAllocationMatrix != null && taxAllocationMatrix.getJurisdictionAllocationMatrixDetails() != null) {
				for(JurisdictionAllocationMatrixDetail d : taxAllocationMatrix.getJurisdictionAllocationMatrixDetails()) {
					if(d.getTaxAllocationMatrixDetailInstanceId() != null && d.getTaxAllocationMatrixDetailInstanceId().equals(jurisSplitInstanceId)) {
						return true;
					}
				}
			}
		}
		
		return false;
	}
	
	public boolean getDisplayAddJurisSplit() {
		return !getDisplayAddJurisSplitEnabled();
	}

	public BigDecimal getDistributionAmount() {
		return distributionAmount;
	}

	public void setDistributionAmount(BigDecimal distributionAmount) {
		this.distributionAmount = ArithmeticUtils.toBigDecimal(distributionAmount.doubleValue());
	}

	public BigDecimal getFreightAmount() {
		return freightAmount;
	}

	public void setFreightAmount(BigDecimal freightAmount) {
		this.freightAmount = ArithmeticUtils.toBigDecimal(freightAmount.doubleValue());
	}

	public BigDecimal getDiscountAmount() {
		return discountAmount;
	}

	public void setDiscountAmount(BigDecimal discountAmount) {
		this.discountAmount = ArithmeticUtils.toBigDecimal(discountAmount.doubleValue());
	}

	public BigDecimal getTotalDistributionAmount() {
		return totalDistributionAmount;
	}

	public BigDecimal getRemainingDistributionAmount() {
		return remainingDistributionAmount;
	}

	public TransactionInMemoryDataModel<T, Long> getSplitTransactionDataModel() {
		return splitTransactionDataModel;
	}

	public void setSplitTransactionDataModel(
			TransactionInMemoryDataModel<T, Long> splitTransactionDataModel) {
		this.splitTransactionDataModel = splitTransactionDataModel;
	}

	public List<T> getSplitTransactions() {
		return splitTransactions;
	}

	public void setSplitTransactions(List<T> splitTransactions) {
		this.splitTransactions = splitTransactions;
	}

	public TaxAllocationMatrix getEditFilterSelectionTaxMatrix() {
		return editFilterSelectionTaxMatrix;
	}

	public void setEditFilterSelectionTaxMatrix(
			TaxAllocationMatrix editFilterSelectionTaxMatrix) {
		this.editFilterSelectionTaxMatrix = editFilterSelectionTaxMatrix;
	}

	public T getSelectedSplitTransaction() {
		return selectedSplitTransaction;
	}

	public void setSelectedSplitTransaction(T selectedSplitTransaction) {
		this.selectedSplitTransaction = selectedSplitTransaction;
	}

	public int getSelectedSplitRowIndex() {
		return selectedSplitRowIndex;
	}

	public void setSelectedSplitRowIndex(int selectedSplitRowIndex) {
		this.selectedSplitRowIndex = selectedSplitRowIndex;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public GenericDAO<T, Long> getCommonDAO() {
		return commonDAO;
	}

	public void setCommonDAO(GenericDAO<T, Long> commonDAO) {
		this.commonDAO = commonDAO;
	}

	public Object getTransactionViewBean() {
		return transactionViewBean;
	}

	public void setTransactionViewBean(Object transactionViewBean) {
		this.transactionViewBean = transactionViewBean;
	}

	public TaxAllocationMatrixViewBean getTaxAllocationMatrixViewBean() {
		return taxAllocationMatrixViewBean;
	}

	public void setTaxAllocationMatrixViewBean(
			TaxAllocationMatrixViewBean taxAllocationMatrixViewBean) {
		this.taxAllocationMatrixViewBean = taxAllocationMatrixViewBean;
	}

	public Long getDisplayWarning() {
		return displayWarning;
	}

	public void setDisplayWarning(Long displayWarning) {
		this.displayWarning = displayWarning;
	}
	
	public void cancelWarning(ActionEvent e) {
		this.displayWarning = 0L;
	}

	public int getSaveOption() {
		return saveOption.value;
	}

	public void setSaveOption(int saveOption) {
		for(SplitSaveOption o : SplitSaveOption.values()) {
			if(o.value == saveOption) {
				this.saveOption = o;
				break;
			}
		}
	}
	
	public Long getDisplayGlobalJurisWarning() {
		return displayGlobalJurisWarning;
	}

	public void setDisplayGlobalJurisWarning(Long displayGlobalJurisWarning) {
		this.displayGlobalJurisWarning = displayGlobalJurisWarning;
	}

	public Long getDisplayIndividualJurisWarning() {
		return displayIndividualJurisWarning;
	}

	public void setDisplayIndividualJurisWarning(Long displayIndividualJurisWarning) {
		this.displayIndividualJurisWarning = displayIndividualJurisWarning;
	}
	
	public void globalJurisWarningCancel(ActionEvent e) {
		this.displayGlobalJurisWarning = 0L;
	}
	
	public void individualJurisWarningCancel(ActionEvent e) {
		this.displayIndividualJurisWarning = 0L;
	}

	public boolean isSaveToTaxAllocMatrix() {
		return saveToTaxAllocMatrix;
	}

	public void setSaveToTaxAllocMatrix(boolean saveToTaxAllocMatrix) {
		this.saveToTaxAllocMatrix = saveToTaxAllocMatrix;
	}
	
	public void saveToTaxAllocMatrixValueChanged(ValueChangeEvent e) {
		Boolean newVal = (Boolean) e.getNewValue();
		if(newVal != null) {
			this.saveToTaxAllocMatrix = newVal.booleanValue();
		}
	}
	
	public String getActionText() {
		return currentAction.getActionText();
	}
	
	public Long getJurisSplitInstanceId() {
		return jurisSplitInstanceId;
	}

	public void setJurisSplitInstanceId(Long jurisSplitInstanceId) {
		this.jurisSplitInstanceId = jurisSplitInstanceId;
	}
	
	public TaxAllocationMatrix getTaxAllocationMatrix(){
		if(this.taxAllocationMatrix == null) {
			this.taxAllocationMatrix = new TaxAllocationMatrix();
		}
		
		return this.taxAllocationMatrix;
	}
	
	public void taxcodeCodeSelected(ActionEvent event) {
	    String command = (String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("command");
	    
	    int commandInt = 0;
	    try{
	    	commandInt = Integer.parseInt(command);
	    }
	    catch(Exception e){
	    	return;
	    }
	    
	    TaxAllocationMatrixDetail selectedTaxObject = (TaxAllocationMatrixDetail) this.taxAllocationSplits.get(commandInt);    
	    String strTaxCode = (String) ((HtmlSelectOneMenu) ((HtmlAjaxSupport) event.getComponent()).getParent()).getSubmittedValue();
	    if(strTaxCode!=null && strTaxCode.length()>0){
		    TaxCode code = taxCodeDetailService.findTaxCode(new TaxCodePK(strTaxCode));
		    if(code!=null){
		    	selectedTaxObject.setDescription(code.getDescription());		    	
		    	selectedTaxObject.setTaxcodeCode(strTaxCode);
		    }
		    else{
		    	selectedTaxObject.setDescription("");
		    	selectedTaxObject.setTaxcodeCode("");
		    }
	    }
	    else{
	    	selectedTaxObject.setDescription("");
	    	selectedTaxObject.setTaxcodeCode("");
	    }
	
	    return;
	}
	
	public BigDecimal getRemainingAllocationPercent() {
		if(this.getTaxAllocationMatrix() != null) {
			return ArithmeticUtils.subtract(new BigDecimal("1"), this.getTaxAllocationMatrix().getTotalAllocationPercent());
		}
		else {
			return ArithmeticUtils.ZERO;
		}
	}
	
	public BigDecimal getTotalGlLineItmDistAmt(List<TaxAllocationMatrixDetail> taxAllocationMatrixDetails) {
		BigDecimal sum = ArithmeticUtils.ZERO;
		if (taxAllocationMatrixDetails != null) {
			for (TaxAllocationMatrixDetail detail : taxAllocationMatrixDetails) {
				if (detail.getGlLineItmDistAmt() != null) {
				    sum = ArithmeticUtils.add(sum, detail.getGlLineItmDistAmt());	
				}					
			}
		}
		
		return sum;
	}
	
	public BigDecimal getTotalInvoiceFreightAmt(List<TaxAllocationMatrixDetail> taxAllocationMatrixDetails) {
		BigDecimal sum = ArithmeticUtils.ZERO;
		if (taxAllocationMatrixDetails != null) {
			for (TaxAllocationMatrixDetail detail : taxAllocationMatrixDetails) {
				if (detail.getInvoiceFreightAmt() != null) {
				    sum = ArithmeticUtils.add(sum, detail.getInvoiceFreightAmt());	
				}					
			}
		}
		
		return sum;
	}
	
	public BigDecimal getTotalInvoiceDiscountAmt(List<TaxAllocationMatrixDetail> taxAllocationMatrixDetails) {
		BigDecimal sum = ArithmeticUtils.ZERO;
		if (taxAllocationMatrixDetails != null) {
			for (TaxAllocationMatrixDetail detail : taxAllocationMatrixDetails) {
				if (detail.getInvoiceDiscountAmt() != null) {
				    sum = ArithmeticUtils.add(sum, detail.getInvoiceDiscountAmt());	
				}					
			}
		}
		
		return sum;
	}
	
	public BigDecimal getRemainingGlLineItmDistAmt() {
		if(this.getTaxAllocationMatrix() != null) {
			if(transactionDetail instanceof BCPPurchaseTransaction) {
				BCPPurchaseTransaction t = (BCPPurchaseTransaction) transactionDetail;
				if(t.getGlLineItmDistAmt()!=null){
					return ArithmeticUtils.subtract(t.getGlLineItmDistAmt(), getTotalGlLineItmDistAmt(this.taxAllocationSplits));
				}
				else{
					return ArithmeticUtils.subtract(ArithmeticUtils.ZERO, getTotalGlLineItmDistAmt(this.taxAllocationSplits));
				}	
			}
			else if(transactionDetail instanceof PurchaseTransaction) {
				PurchaseTransaction t = (PurchaseTransaction) transactionDetail;
				if(t.getGlLineItmDistAmt()!=null){
					return ArithmeticUtils.subtract(t.getGlLineItmDistAmt(), getTotalGlLineItmDistAmt(this.taxAllocationSplits));
				}
				else{
					return ArithmeticUtils.subtract(ArithmeticUtils.ZERO, getTotalGlLineItmDistAmt(this.taxAllocationSplits));
				}			
			}
			else{
				return ArithmeticUtils.ZERO;
			}
		}
		else {
			return ArithmeticUtils.ZERO;
		}
	}
	
	public BigDecimal getRemainingInvoiceFreightAmt() {
		if(this.getTaxAllocationMatrix() != null) {
			if(transactionDetail instanceof BCPPurchaseTransaction) {
				BCPPurchaseTransaction t = (BCPPurchaseTransaction) transactionDetail;
				if(t.getInvoiceFreightAmt()!=null){
					return ArithmeticUtils.subtract(t.getInvoiceFreightAmt(), getTotalInvoiceFreightAmt(this.taxAllocationSplits));
				}
				else{
					return ArithmeticUtils.subtract(ArithmeticUtils.ZERO, getTotalInvoiceFreightAmt(this.taxAllocationSplits));
				}	
			}
			else if(transactionDetail instanceof PurchaseTransaction) {
				PurchaseTransaction t = (PurchaseTransaction) transactionDetail;
				if(t.getInvoiceFreightAmt()!=null){
					return ArithmeticUtils.subtract(t.getInvoiceFreightAmt(), getTotalInvoiceFreightAmt(this.taxAllocationSplits));
				}
				else{
					return ArithmeticUtils.subtract(ArithmeticUtils.ZERO, getTotalInvoiceFreightAmt(this.taxAllocationSplits));
				}			
			}
			else{
				return ArithmeticUtils.ZERO;
			}
		}
		else {
			return ArithmeticUtils.ZERO;
		}
	}
	
	public BigDecimal getRemainingInvoiceDiscountAmt() {
		if(this.getTaxAllocationMatrix() != null) {
			if(transactionDetail instanceof BCPPurchaseTransaction) {
				BCPPurchaseTransaction t = (BCPPurchaseTransaction) transactionDetail;
				if(t.getInvoiceDiscountAmt()!=null){
					return ArithmeticUtils.subtract(t.getInvoiceDiscountAmt(), getTotalInvoiceDiscountAmt(this.taxAllocationSplits));
				}
				else{
					return ArithmeticUtils.subtract(ArithmeticUtils.ZERO, getTotalInvoiceDiscountAmt(this.taxAllocationSplits));
				}	
			}
			else if(transactionDetail instanceof PurchaseTransaction) {
				PurchaseTransaction t = (PurchaseTransaction) transactionDetail;
				if(t.getInvoiceDiscountAmt()!=null){
					return ArithmeticUtils.subtract(t.getInvoiceDiscountAmt(), getTotalInvoiceDiscountAmt(this.taxAllocationSplits));
				}
				else{
					return ArithmeticUtils.subtract(ArithmeticUtils.ZERO, getTotalInvoiceDiscountAmt(this.taxAllocationSplits));
				}			
			}
			else{
				return ArithmeticUtils.ZERO;
			}
		}
		else {
			return ArithmeticUtils.ZERO;
		}
	}

	public Boolean getDisplayDeleteAction() {
		return (currentAction == EditAction.DELETE);
	}
	public Boolean getDisplayViewAction() {
		return (currentAction == EditAction.VIEW);
	}
	
	public Boolean getDisplayAddAction() {
		return (currentAction == EditAction.ADD);
	}
	
	public Boolean getDisplayUpdateAction() {
		return (currentAction == EditAction.UPDATE);
	}
	
	public String addMatrixDetail() {

		TaxAllocationMatrixDetail taxAllocationMatrixDetail = new TaxAllocationMatrixDetail();
		
		if(this.taxAllocationSplits==null){
			this.taxAllocationSplits = new ArrayList<TaxAllocationMatrixDetail>();
		}

		this.taxAllocationSplits.add(taxAllocationMatrixDetail);

		return null;
	}
	
	public void processAddWhenCommand(ActionEvent event) {
	    String command = (String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("command");
	    
	    addMatrixDetail();
	    return;
	}

	public List<TaxAllocationMatrixDetail> getSelectedMatrixTaxAllocDetails() {
		List<TaxAllocationMatrixDetail> result = this.taxAllocationSplits;
		
		if(result != null) {
			logger.debug("Detail items: " + result.size());
		}
		
		instanceIdDetailIdMap = new HashMap<Long, Long>();
		jurisSplitInfoMap = new HashMap<Long, Boolean>();
		
		Map<Long, Long> detailIdInstanceIdMap = new HashMap<Long, Long>();
		if(result != null) {
			for(TaxAllocationMatrixDetail d : result) {
				if(d.getTaxAllocationMatrixDetailId() != null) {
					detailIdInstanceIdMap.put(d.getTaxAllocationMatrixDetailId(), d.getInstanceCreatedId());
				}
				instanceIdDetailIdMap.put(d.getInstanceCreatedId(), d.getTaxAllocationMatrixDetailId());
				jurisSplitInfoMap.put(d.getInstanceCreatedId(), Boolean.FALSE);
			}
		}
		
		List<JurisdictionAllocationMatrixDetail> jurisDetails = this.getTaxAllocationMatrix().getJurisdictionAllocationMatrixDetails();
		if(jurisDetails != null) {
			for(JurisdictionAllocationMatrixDetail d : jurisDetails) {
				if(d.getTaxAllocationMatrixDetailId() != null && d.getTaxAllocationMatrixDetailId() == 0L) {
					globalSplit = true;
				}
				
				if(d.getTaxAllocationMatrixDetailId() != null) {
					d.setTaxAllocationMatrixDetailInstanceId(detailIdInstanceIdMap.get(d.getTaxAllocationMatrixDetailId()));
				}
				
				if(d.getTaxAllocationMatrixDetailInstanceId() != null) {
					jurisSplitInfoMap.put(d.getTaxAllocationMatrixDetailInstanceId(), Boolean.TRUE);
				}
			}
		}
		
		return result;
	}
	
	public List<SelectItem> getTaxCodeItems() {
		if(taxCodeItems==null){
			taxCodeItems = new ArrayList<SelectItem>();
			taxCodeItems.add(new SelectItem("","Select a TaxCode"));
			List<TaxCode> codes = taxCodeDetailService.getAllTaxCode();
			

			if (codes != null) {
				for (TaxCode code : codes) {
					String value = code.getTaxCodePK().getTaxcodeCode();
					taxCodeItems.add(new SelectItem(value, value));
				}
			}
		}
		
		return taxCodeItems;
	}
	public Map<Long, Boolean> getJurisSplitInfoMap() {
		return jurisSplitInfoMap;
	}

	public void setJurisSplitInfoMap(Map<Long, Boolean> jurisSplitInfoMap) {
		this.jurisSplitInfoMap = jurisSplitInfoMap;
	}

	public void processRemoveWhenCommand(ActionEvent event) {
	    String command = (String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("command");
	    
	    int commandInt = 0;
	    try{
	    	commandInt = Integer.parseInt(command);
	    }
	    catch(Exception e){
	    	return;
	    }
	    
	    this.taxAllocationSplits.remove(commandInt);

	    return;
	}

}
