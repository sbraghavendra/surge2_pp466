package com.ncsts.view.bean;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.ncsts.common.DatabaseUtil;
import com.ncsts.dao.BatchMaintenanceDAO;
import com.ncsts.domain.Auditable;
import com.ncsts.domain.BatchMaintenance;
import com.ncsts.domain.Option;
import com.ncsts.domain.OptionCodePK;
import com.ncsts.domain.PurchaseTransaction;
import com.ncsts.dto.DbPropertiesDTO;
import com.ncsts.management.DbPropertiesBean;
import com.ncsts.services.OptionService;

public class DownloadAddBean {
	private Logger logger = Logger.getLogger(DownloadAddBean.class);
	
	@Autowired
	private OptionService optionService;
	
	private BatchMaintenance downloadBatchMaintenance;
	private BatchMaintenanceDAO batchMaintenanceDAO;
	private DbPropertiesDTO dbPropertiesDTO = null;
	private DbPropertiesBean dbPropertiesBean;
	private BatchMaintenanceBackingBean batchMaintenanceBean;
	private Thread background;
	
	private String extendWhere = "1=1"; 
	private String strOrderBy = "PURCHTRANS_ID asc";
	private String returnScreen = "";
	private boolean stopDownload = true;
	private String message = "";
	private String delimiter = ",";
	private String textQualifier = "";
	private List<String> columnList = new ArrayList<String>();
	
	private boolean isProcessStatusFound = false;
	
	List<String> processStatusList = new ArrayList<String>();
	
	private int processStatusPosition;
	
	private int headersSize ;
	
	private static String  PROCESS_STATUS_COLUMN =  "processStatus";
	
	public int getHeadersSize() {
		return headersSize;
	}
	
	public void setHeadersSize(int headersSize) {
		this.headersSize = headersSize;
	}

	public List<String> getProcessStatusList() {
		return processStatusList;
	}

	public void setProcessStatusList(List<String> processStatusList) {
		this.processStatusList = processStatusList;
	}

	public int getProcessStatusPosition() {
		return processStatusPosition;
	}

	public void setProcessStatusPosition(int processStatusPosition) {
		this.processStatusPosition = processStatusPosition;
	}

	public boolean isProcessStatusFound() {
		return isProcessStatusFound;
	}

	public void setProcessStatusFound(boolean isProcessStatusFound) {
		this.isProcessStatusFound = isProcessStatusFound;
	}

	public boolean getStopDownload() {
		return stopDownload;
	}

	public void setStopDownload(boolean stopDownload) {
		this.stopDownload = stopDownload;
	}
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	public void setColumnList(List<String> columnList){
		this.columnList = columnList;
	}
	
	public void setExtendWhere(String extendWhere) {
		this.extendWhere = extendWhere;
	}
	
	public void setStrOrderBy(String strOrderBy) {
		this.strOrderBy = strOrderBy;
	}
	
	public void setReturnScreen(String returnScreen) {
		this.returnScreen = returnScreen;
	}
	
	public BatchMaintenance getDownloadBatchMaintenance() {
		return downloadBatchMaintenance;
	}
	
	public BatchMaintenanceBackingBean  getBatchMaintenanceBean() {
		return batchMaintenanceBean;
	}

	public void setBatchMaintenanceBean(BatchMaintenanceBackingBean  batchMaintenanceBean) {
		this.batchMaintenanceBean = batchMaintenanceBean;
	}

	public void setDownloadBatchMaintenance(BatchMaintenance downloadBatchMaintenance) {
		this.downloadBatchMaintenance = downloadBatchMaintenance;
	}
	
	public BatchMaintenanceDAO getBatchMaintenanceDAO() {
		return this.batchMaintenanceDAO;
	}

	public void setBatchMaintenanceDAO(BatchMaintenanceDAO batchMaintenanceDAO) {
		this.batchMaintenanceDAO = batchMaintenanceDAO;
	}
	
	public void setDbPropertiesBean(DbPropertiesBean dbPropertiesBean) {
		this.dbPropertiesBean = dbPropertiesBean;
	}
	
	public DbPropertiesBean getDbPropertiesBean() {
		return this.dbPropertiesBean;
	}
	
	public String addDownloadAction() {
		//Check if Downloading File is in process.
		List<BatchMaintenance> batchMaintenanceList = batchMaintenanceDAO.findByStatusUpdateUser("DL", "DX", Auditable.currentUserCode());		
		if(batchMaintenanceList!=null && batchMaintenanceList.size()>0){
			this.setStopDownload(true);
			this.setMessage("Cannot start a new download while previous download is running.");	
			return "transaction_download";
		}
		
		//Check if there is any file existing. And set status to D
		batchMaintenanceList = batchMaintenanceDAO.findByStatusUpdateUser("DL", "P", Auditable.currentUserCode());		
		if(batchMaintenanceList!=null && batchMaintenanceList.size()>0){		
			for(int i=0; i<batchMaintenanceList.size(); i++ ){
				BatchMaintenance batchMaintenance = (BatchMaintenance) batchMaintenanceList.get(i);
				
				batchMaintenance.setBatchStatusCode("D");
				batchMaintenanceDAO.update(batchMaintenance);
				
				//delete file
				DownloadAddBean.deleteDownloadFile(batchMaintenance.getVc01());
			}
		}

		downloadBatchMaintenance.setBatchId(null); // new record
		downloadBatchMaintenance.setTotalRows(0l);
		downloadBatchMaintenance.setActualStartTimestamp(new Date());
		
		batchMaintenanceDAO.save(downloadBatchMaintenance);

		//extendWhere = "gl_cc_nbr_dept_id=301374 and GL_COMPANY_NBR='PL501' and PROCESS_BATCH_NO=17";
		//strOrderBy = "transaction_detail_id asc";
		//returnScreen = "transactions_process";
		processUpdateInBackground(downloadBatchMaintenance);

		//return "transactions_process";
		return "data_utility_batch";//Return to batch screen
	}
	
	public String cancelDownloadAction(){
		return returnScreen;
	}
	
	public static String DOWNLOAD_FOLDER = "downloads";
	
	public static String downloadFolder(){
		String path = System.getProperty("download.home", "").trim();
		if(path == null || path.length() == 0) {
			path = System.getProperty("catalina.home", System.getProperty("java.io.tmpdir"));	
		}

		return path;
	}
	
	public synchronized static String createDownloadFileName() {
		try {Thread.sleep(1000);} catch (Exception ex){} //Sleep 1 second to prevent duplicate.
		
		String fileName = "TP_DOWNLOAD_" + new SimpleDateFormat("yyyyMMddhhmmssSSS").format(new Date());
		return fileName;
	}
	
	public static boolean deleteDownloadFile(String filename){	
		boolean succeeded = false;
    	try{
    		String absolutePath = downloadFolder() + File.separator + DOWNLOAD_FOLDER + File.separator + filename;
    		File file = new File(absolutePath);
    		if(file.delete()){
    			succeeded = true;
    		}else{
    			succeeded = false;
    		}
    	}catch(Exception e){
    		e.printStackTrace();
    		succeeded = false;
    	}	
    	return succeeded;
    }
	
	public String getFlatDelCharDecoded(String flatDelChar) {
		if ("~t".equalsIgnoreCase(flatDelChar)) return "\t";
		if ("~n".equalsIgnoreCase(flatDelChar)) return "\n";
		if ("~r".equalsIgnoreCase(flatDelChar)) return "\r";
		if ("~b".equalsIgnoreCase(flatDelChar)) return "\b";
		if ("~f".equalsIgnoreCase(flatDelChar)) return "\f";
		if ("~v".equalsIgnoreCase(flatDelChar)) return "\u000B"; // vertical tab
		return flatDelChar;
    }
	
	private void processUpdateInBackground(final BatchMaintenance batch) {
		String selectedDb = ChangeContextHolder.getDataBaseType();	
		dbPropertiesDTO = dbPropertiesBean.getDbPropertiesDTO(selectedDb);
		
		textQualifier = batchMaintenanceBean.getFilePreferenceBean().getBatchPreferenceDTO().getTextQualifier();
		if(textQualifier == null){
			textQualifier = "";
		}
		
		//4916
		Option delimiterOption = optionService.findByPK(new OptionCodePK("EXPORTDELIMITER", "SYSTEM", "SYSTEM"));	
		String delimiterDb = "";	
		if(delimiterOption!=null){
			delimiterDb = delimiterOption.getValue();
		}
		
		if(delimiterDb==null || delimiterDb.length()==0){
			delimiterDb = "~t";//PP-142
		}
		
		delimiter = getFlatDelCharDecoded(delimiterDb);

		background = new Thread(new Runnable() {
			public void run() {

				System.out.println("############ processUpdateInBackground for Download using URL: " + dbPropertiesDTO.getUrl());

				try {Thread.sleep(2000);} catch (Exception ex){}
				
				try {
					Class.forName(dbPropertiesDTO.getDriverClassName());
			    } catch (ClassNotFoundException e) {
			    	System.out.println("xxxxxxxxxxxxxx Can't load class: " + dbPropertiesDTO.getDriverClassName());
			    	return;
			    }
			    Connection conn = null;

				try {	

					//Standalone connection
					conn = DriverManager.getConnection(dbPropertiesDTO.getUrl(), dbPropertiesDTO.getUserName(), dbPropertiesDTO.getPassword());
					conn.setAutoCommit(false);
					
					File file = null;
				
					String home = downloadFolder();  //System.getProperty("catalina.home", System.getProperty("java.io.tmpdir"));
					File parent = new File(home, DOWNLOAD_FOLDER);
					if(!parent.isDirectory()) {
						parent.mkdirs();
					}
			
					String actualFileName = downloadBatchMaintenance.getVc01();
					String absoluteFilePath = downloadFolder() + File.separator + DOWNLOAD_FOLDER + File.separator + actualFileName;
					
					BufferedWriter writer = null;
					try {
						writer = new BufferedWriter(new FileWriter(absoluteFilePath));
					} catch (Exception e) {
						e.printStackTrace();
					}

					//Write to file
					int rowsPerRequest = 1000;
					int totalCount = 0;
					int numberRequest = 0;
					long totalCountActual = 0;

					//Get total count
					String queryCount = "select count(*)  as count FROM TB_PURCHTRANS LEFT JOIN TB_PURCHTRANS_JURDTL ON TB_PURCHTRANS.PURCHTRANS_ID = TB_PURCHTRANS_JURDTL.PURCHTRANS_JURDTL_ID where :extendWhere ";
					queryCount = queryCount.replaceAll(":extendWhere", extendWhere);
					Statement statCount = conn.createStatement();
					ResultSet rsCount = statCount.executeQuery(queryCount);	
					
					if(rsCount.next()){
						totalCount = rsCount.getInt(1);
					}	
					rsCount.close();	
					statCount.close();
					
					
					numberRequest = (totalCount/rowsPerRequest) + 1;
					
					System.out.println("   ... rowsPerRequest: " + rowsPerRequest);
					System.out.println("   ... totalCount: " + totalCount);
					System.out.println("   ... numberRequest: " + numberRequest);
				
					//Retrieve data
					Statement stat = null;
					stat = conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
					ResultSet rs = null;
					if(isProcessStatusFound()) {
						Set<String> columnSet = new LinkedHashSet<String>(columnList);
						columnSet.addAll(getProcessStatusColumnsList());
						columnList = new ArrayList<String>(columnSet);
					}
					String columnNames = getTransactionColumnNames();
					
					System.out.println("   ... before loop");
					for(int i=0; i<numberRequest; i++){
						String sqlQuery = "";
						if(strOrderBy!=null && strOrderBy.length()>0){	
							sqlQuery = "select * from (Select ROW_NUMBER() OVER (ORDER BY :orderby ) AS RowNumber, " + columnNames + 
								" FROM TB_PURCHTRANS " +
								" LEFT JOIN TB_PURCHTRANS_USER ON TB_PURCHTRANS.PURCHTRANS_ID = TB_PURCHTRANS_USER.PURCHTRANS_USER_ID " +
								" LEFT JOIN TB_PURCHTRANS_JURDTL ON TB_PURCHTRANS.PURCHTRANS_ID = TB_PURCHTRANS_JURDTL.PURCHTRANS_JURDTL_ID " +
								" LEFT JOIN TB_PURCHTRANS_LOCN ON TB_PURCHTRANS.PURCHTRANS_ID = TB_PURCHTRANS_LOCN.PURCHTRANS_LOCN_ID " + 
								" WHERE :extendWhere) WHERE RowNumber BETWEEN :minrow AND :maxrow ";
							
							sqlQuery = sqlQuery.replaceAll(":orderby", strOrderBy);
							sqlQuery = sqlQuery.replaceAll(":extendWhere", extendWhere);
							sqlQuery = sqlQuery.replaceAll(":minrow", "" + (i*rowsPerRequest + 1));
							sqlQuery = sqlQuery.replaceAll(":maxrow", "" + ((i+1)*rowsPerRequest));
						}
						else{	
							sqlQuery = "select * from (Select rownum AS RowNumber, " + columnNames + 
								" FROM TB_PURCHTRANS " +
								" LEFT JOIN TB_PURCHTRANS_USER ON TB_PURCHTRANS.PURCHTRANS_ID = TB_PURCHTRANS_USER.PURCHTRANS_USER_ID " +
								" LEFT JOIN TB_PURCHTRANS_JURDTL ON TB_PURCHTRANS.PURCHTRANS_ID = TB_PURCHTRANS_JURDTL.PURCHTRANS_JURDTL_ID " +
								" LEFT JOIN TB_PURCHTRANS_LOCN ON TB_PURCHTRANS.PURCHTRANS_ID = TB_PURCHTRANS_LOCN.PURCHTRANS_LOCN_ID " + 
								" WHERE :extendWhere) WHERE RowNumber BETWEEN :minrow AND :maxrow ";
								
							sqlQuery = sqlQuery.replaceAll(":extendWhere", extendWhere);
							sqlQuery = sqlQuery.replaceAll(":minrow", "" + (i*rowsPerRequest + 1));
							sqlQuery = sqlQuery.replaceAll(":maxrow", "" + ((i+1)*rowsPerRequest));
						}
						
				    	rs = stat.executeQuery(sqlQuery);	
				    	if(processStatusList.size() >0 ) {
				    		processStatusList.clear();
				    	}
				    	if(isProcessStatusFound()) {
				    		while(rs.next()) {
								String processStatus ;
								PurchaseTransaction purchaseTransaction = new PurchaseTransaction();
								purchaseTransaction.setTransactionInd(rs.getString("TRANSACTION_IND"));
								purchaseTransaction.setGlExtractFlag(rs.getString(("GL_EXTRACT_FLAG")));
								purchaseTransaction.setStateTaxcodeTypeCode(rs.getString("STATE_TAXCODE_TYPE_CODE"));
								purchaseTransaction.setCountyTaxcodeTypeCode(rs.getString("COUNTY_TAXCODE_TYPE_CODE"));
								purchaseTransaction.setCityTaxcodeTypeCode(rs.getString("CITY_TAXCODE_TYPE_CODE"));
								purchaseTransaction.setSuspendInd(rs.getString("SUSPEND_IND"));
								purchaseTransaction.setMultiTransCode(rs.getString("MULTI_TRANS_CODE")); 
								processStatus = purchaseTransaction.getProcessStatus();
								processStatusList.add(processStatus);
					    	}
				    	}
				    	
				    	try {
					    	if(i==0){ 
					    		totalCountActual = totalCountActual + writeRersultSetToFile(rs, writer, true, delimiter, textQualifier,getProcessStatusPosition());
					    	}
					    	else{
					    		totalCountActual = totalCountActual + writeRersultSetToFile(rs, writer, false, delimiter, textQualifier,getProcessStatusPosition());
					    	}
				    	} catch (Exception e) {
							e.printStackTrace();
						}
				    	
				    	System.out.println("   ... End Writing: record " + totalCountActual);
				    	
				    	if((i%5) == 0){
				    		System.out.println("   ... Begin Set Batch TotalRows: " + totalCountActual);
				    		
				    		//Update every 5 * rowsPerRequest records
					    	Date end = new Date();
				  	  	  	batch.setTotalRows(totalCountActual);//22
				  	  	  	batch.setStatusUpdateTimestamp(end);
				  	  	  	batch.setUpdateTimestamp(end); 	  	  	
				  	  	  	updateBatchUsingJDBC(batch, conn);
				  	  	  	
				  	  	  	System.out.println("   ... End Set Batch TotalRows: " + totalCountActual);
				    	}
				    	 	
				    	rs.close();	
					}
					stat.close();
					if(writer!=null){
						writer.close();		 
					}
			
					// force completion
					System.out.println("   ... Begin Final Set Batch TotalRows: " + totalCountActual);
					
					
					Date end = new Date();
		  	  	  	batch.setTotalRows(totalCountActual);
		  	  	  	batch.setBatchStatusCode("P");
		  	  	  	batch.setVc01(actualFileName);
		  	  	  	
		  	  	  	batch.setStatusUpdateTimestamp(end);
		  	  		batch.setStatusUpdateUserId(batch.getUpdateUserId());
		  	  		batch.setUpdateUserId(batch.getUpdateUserId());
		  	  	  	batch.setUpdateTimestamp(end);
		  	  	  	batch.setActualEndTimestamp(end);
		  	  	  	  	  	  	
		  	  	  	updateBatchUsingJDBC(batch, conn);
		  	  	  	System.out.println("   ... Set Batch Status Code to P");
		  	  	  	System.out.println("   ... End Final Set Batch TotalRows: " + totalCountActual);
				
				} catch (Throwable e) {
					e.printStackTrace();
					if(e.getCause() != null) {e.getCause().printStackTrace();}
				
					batch.setBatchStatusCode("ABP");
					try{
						updateBatchUsingJDBC(batch, conn);
						System.out.println("   ... Set Batch Status Code to ABP");
					}
					catch(Throwable ee){e.printStackTrace();}
	
					logger.debug(e, e);
				} finally {
					if (conn != null)
						try {
							conn.close();
						} catch (SQLException ignore) {
			        }
			    }
			}
	  	});
	  	background.start();
		logger.debug("Background started");
		
		batchMaintenanceBean.retrieveBatchMaintenance();
	  }
		
		public long writeRersultSetToFile(ResultSet rs, BufferedWriter writer, boolean header, String delimiterChar, String textQualifier,
				                          int procStatusposition) {
			long rows = 0;
			try {
				// make sure resultset is not null
				if (rs != null) {

					Map<String, String> textTypeMap  = new LinkedHashMap<String,String>();
					List<String> columns = new ArrayList<String>();
					ResultSetMetaData rsmd = rs.getMetaData();
					int columnCount = rsmd.getColumnCount();
					boolean found = false;
					if(isProcessStatusFound()) {
						columnCount = columnCount  + 1;
					}
					int j,headerCount = 0;
					for (int i = 0; i < columnCount; i++) {
						if (i > 0) {
							if(header){
								writer.write(delimiterChar);
							}
						}
						if(isProcessStatusFound()) { 
							j = i + 1;
							if(j == procStatusposition && !found ) {
								if(header){
									writer.write("processStatus");
								}
								else {
									textTypeMap.put("processStatus", "processStatus");
								}
								found = true;
								columns.add("processStatus");
								i = i - 1; 
							}
							else {
								String columnName = rsmd.getColumnName(i + 1);					
								if(header){
									writer.write(columnName);
								}
								columns.add(columnName);
								//NUMBER, VARCHAR2, DATE, CHAR
								if(rsmd.getColumnTypeName(i + 1).equalsIgnoreCase("VARCHAR2") || 
										rsmd.getColumnTypeName(i + 1).equalsIgnoreCase("CHAR")){
									textTypeMap.put(columnName, columnName);
								}
							}
							headerCount = headerCount + 1;
							if(getHeadersSize() == headerCount) {
								break;
							}
						}
						else {
							String columnName = rsmd.getColumnName(i + 1);					
							if(header){
								writer.write(columnName);
							}
							columns.add(columnName);
							//NUMBER, VARCHAR2, DATE, CHAR
							if(rsmd.getColumnTypeName(i + 1).equalsIgnoreCase("VARCHAR2") || 
									rsmd.getColumnTypeName(i + 1).equalsIgnoreCase("CHAR")){
								textTypeMap.put(columnName, columnName);
							}
						}
					}
					if(header){
						//writer.write("\n");
						writer.newLine();
						writer.flush();
					}

					int count = 0;
					int processStatusListIndex = 0;
					rs.beforeFirst();
					while (rs.next()) {
						count = 0;
						Object columnValue;
						for (String c : columns) {
							if (count > 0) {
								writer.write(delimiterChar);
							}
							try {
								if(PROCESS_STATUS_COLUMN.equals(c)) {
									columnValue = (String)processStatusList.get(processStatusListIndex);
								}
								else {
									columnValue = rs.getObject(c);
								}
								if (columnValue != null) {
									if(textTypeMap.get(c)!=null){
										writer.write(textQualifier);
									}
									
									String commentsStr = (String) columnValue.toString();
									commentsStr = commentsStr.replaceAll("\r", " ");
									commentsStr = commentsStr.replaceAll("\n", " ");
									commentsStr = commentsStr.replaceAll("\t", " ");
									commentsStr = commentsStr.replaceAll("\b", " ");
									commentsStr = commentsStr.replaceAll("\f", " ");
									writer.write(commentsStr + "");
									
									if(textTypeMap.get(c)!=null){
										writer.write(textQualifier);
									}
								}
							} catch (Exception e) {
							}
							count++;
						}
						//writer.write("\n");
						
						writer.newLine();
						writer.flush();
						
						rows++;
						processStatusListIndex++;
					}

					
				} else {
				}
			} catch (SQLException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}

			return rows;
		}
		
		public String getTransactionColumnNames() throws Exception {
			StringBuffer colBuf = new StringBuffer("");
			
			boolean isInitial = true;
			for(String column : columnList){
				if(isInitial){
					isInitial = false;
				}
				else{
					colBuf.append(", ");
					
				}			
				colBuf.append(column);
			}
			/*
			String sql = 
			"TRANSACTION_DETAIL_ID, " +
			"SOURCE_TRANSACTION_ID, " +
			"PROCESS_BATCH_NO, " +
			"GL_EXTRACT_BATCH_NO, " +
			"ARCHIVE_BATCH_NO, " +
			"ALLOCATION_MATRIX_ID, " +
			"ALLOCATION_SUBTRANS_ID, " +
			"ENTERED_DATE, " +
			"TRANSACTION_STATUS, " +
			"GL_DATE, " +
			"GL_COMPANY_NBR, " +
			"GL_COMPANY_NAME, " +
			"GL_DIVISION_NBR, " +
			"GL_DIVISION_NAME, " +
			"GL_CC_NBR_DEPT_ID, " +
			"GL_CC_NBR_DEPT_NAME, " +
			"GL_LOCAL_ACCT_NBR, " +
			"GL_LOCAL_ACCT_NAME, " +
			"GL_LOCAL_SUB_ACCT_NBR, " +
			"GL_LOCAL_SUB_ACCT_NAME, " +
			"GL_FULL_ACCT_NBR, " +
			"GL_FULL_ACCT_NAME, " +
			"GL_LINE_ITM_DIST_AMT, " +
			"ORIG_GL_LINE_ITM_DIST_AMT, " +
			"VENDOR_NBR, " +
			"VENDOR_NAME, " +
			"VENDOR_ADDRESS_LINE_1, " +
			"VENDOR_ADDRESS_LINE_2, " +
			"VENDOR_ADDRESS_LINE_3, " +
			"VENDOR_ADDRESS_LINE_4, " +
			"VENDOR_ADDRESS_CITY, " +
			"VENDOR_ADDRESS_COUNTY, " +
			"VENDOR_ADDRESS_STATE, " +
			"VENDOR_ADDRESS_ZIP, " +
			"VENDOR_ADDRESS_COUNTRY, " +
			"VENDOR_TYPE, " +
			"VENDOR_TYPE_NAME, " +
			"INVOICE_NBR, " +
			"INVOICE_DESC, " +
			"INVOICE_DATE, " +
			"INVOICE_FREIGHT_AMT, " +
			"INVOICE_DISCOUNT_AMT, " +
			"INVOICE_TAX_AMT, " +
			"INVOICE_TOTAL_AMT, " +
			"INVOICE_TAX_FLG, " +
			"INVOICE_LINE_NBR, " +
			"INVOICE_LINE_NAME, " +
			"INVOICE_LINE_TYPE, " +
			"INVOICE_LINE_TYPE_NAME, " +
			"INVOICE_LINE_AMT, " +
			"INVOICE_LINE_TAX, " +
			"AFE_PROJECT_NBR, " +
			"AFE_PROJECT_NAME, " +
			"AFE_CATEGORY_NBR, " +
			"AFE_CATEGORY_NAME, " +
			"AFE_SUB_CAT_NBR, " +
			"AFE_SUB_CAT_NAME, " +
			"AFE_USE, " +
			"AFE_CONTRACT_TYPE, " +
			"AFE_CONTRACT_STRUCTURE, " +
			"AFE_PROPERTY_CAT, " +
			"INVENTORY_NBR, " +
			"INVENTORY_NAME, " +
			"INVENTORY_CLASS, " +
			"INVENTORY_CLASS_NAME, " +
			"PO_NBR, " +
			"PO_NAME, " +
			"PO_DATE, " +
			"PO_LINE_NBR, " +
			"PO_LINE_NAME, " +
			"PO_LINE_TYPE, " +
			"PO_LINE_TYPE_NAME, " +
			"SHIP_TO_LOCATION, " +
			"SHIP_TO_LOCATION_NAME, " +
			"SHIP_TO_ADDRESS_LINE_1, " +
			"SHIP_TO_ADDRESS_LINE_2, " +
			"SHIP_TO_ADDRESS_LINE_3, " +
			"SHIP_TO_ADDRESS_LINE_4, " +
			"SHIP_TO_ADDRESS_CITY, " +
			"SHIP_TO_ADDRESS_COUNTY, " +
			"SHIP_TO_ADDRESS_STATE, " +
			"SHIP_TO_ADDRESS_ZIP, " +
			"SHIP_TO_ADDRESS_COUNTRY, " +
			"WO_NBR, " +
			"WO_NAME, " +
			"WO_DATE, " +
			"WO_TYPE, " +
			"WO_TYPE_DESC, " +
			"WO_CLASS, " +
			"WO_CLASS_DESC, " +
			"WO_ENTITY, " +
			"WO_ENTITY_DESC, " +
			"WO_LINE_NBR, " +
			"WO_LINE_NAME, " +
			"WO_LINE_TYPE, " +
			"WO_LINE_TYPE_DESC, " +
			"WO_SHUT_DOWN_CD, " +
			"WO_SHUT_DOWN_CD_DESC, " +
			"VOUCHER_ID, " +
			"VOUCHER_NAME, " +
			"VOUCHER_DATE, " +
			"VOUCHER_LINE_NBR, " +
			"VOUCHER_LINE_DESC, " +
			"CHECK_NBR, " +
			"CHECK_NO, " +
			"CHECK_DATE, " +
			"CHECK_AMT, " +
			"CHECK_DESC, " +
			"USER_TEXT_01, " +
			"USER_TEXT_02, " +
			"USER_TEXT_03, " +
			"USER_TEXT_04, " +
			"USER_TEXT_05, " +
			"USER_TEXT_06, " +
			"USER_TEXT_07, " +
			"USER_TEXT_08, " +
			"USER_TEXT_09, " +
			"USER_TEXT_10, " +
			"USER_TEXT_11, " +
			"USER_TEXT_12, " +
			"USER_TEXT_13, " +
			"USER_TEXT_14, " +
			"USER_TEXT_15, " +
			"USER_TEXT_16, " +
			"USER_TEXT_17, " +
			"USER_TEXT_18, " +
			"USER_TEXT_19, " +
			"USER_TEXT_20, " +
			"USER_TEXT_21, " +
			"USER_TEXT_22, " +
			"USER_TEXT_23, " +
			"USER_TEXT_24, " +
			"USER_TEXT_25, " +
			"USER_TEXT_26, " +
			"USER_TEXT_27, " +
			"USER_TEXT_28, " +
			"USER_TEXT_29, " +
			"USER_TEXT_30, " +
			"USER_NUMBER_01, " +
			"USER_NUMBER_02, " +
			"USER_NUMBER_03, " +
			"USER_NUMBER_04, " +
			"USER_NUMBER_05, " +
			"USER_NUMBER_06, " +
			"USER_NUMBER_07, " +
			"USER_NUMBER_08, " +
			"USER_NUMBER_09, " +
			"USER_NUMBER_10, " +
			"USER_DATE_01, " +
			"USER_DATE_02, " +
			"USER_DATE_03, " +
			"USER_DATE_04, " +
			"USER_DATE_05, " +
			"USER_DATE_06, " +
			"USER_DATE_07, " +
			"USER_DATE_08, " +
			"USER_DATE_09, " +
			"USER_DATE_10, " +
			"COMMENTS, " +
			"TB_CALC_TAX_AMT, " +
			"STATE_USE_AMOUNT, " +
			"STATE_USE_TIER2_AMOUNT, " +
			"STATE_USE_TIER3_AMOUNT, " +
			"COUNTY_USE_AMOUNT, " +
			"COUNTY_LOCAL_USE_AMOUNT, " +
			"CITY_USE_AMOUNT, " +
			"CITY_LOCAL_USE_AMOUNT, " +
			"TRANSACTION_STATE_CODE, " +
			"AUTO_TRANSACTION_STATE_CODE, " +
			"TRANSACTION_IND, " +
			"SUSPEND_IND, " +
			"TAXCODE_DETAIL_ID, " +
			"TAXCODE_STATE_CODE, " +
			"TAXCODE_TYPE_CODE, " +
			"TAXCODE_CODE, " +
//000027	"CCH_TAXCAT_CODE, " +
//			"CCH_GROUP_CODE, " +
//			"CCH_ITEM_CODE, " +
			"MANUAL_TAXCODE_IND, " +
			"TAX_MATRIX_ID, " +
			"LOCATION_MATRIX_ID, " +
			"JURISDICTION_ID, " +
			"JURISDICTION_TAXRATE_ID, " +
			"MANUAL_JURISDICTION_IND, " +
			"MEASURE_TYPE_CODE, " +
			"STATE_USE_RATE, " +
			"STATE_USE_TIER2_RATE, " +
			"STATE_USE_TIER3_RATE, " +
			"STATE_SPLIT_AMOUNT, " +
			"STATE_TIER2_MIN_AMOUNT, " +
			"STATE_TIER2_MAX_AMOUNT, " +
			"STATE_MAXTAX_AMOUNT, " +
			"COUNTY_USE_RATE, " +
			"COUNTY_LOCAL_USE_RATE, " +
			"COUNTY_SPLIT_AMOUNT, " +
			"COUNTY_MAXTAX_AMOUNT, " +
			"COUNTY_SINGLE_FLAG, " +
			"COUNTY_DEFAULT_FLAG, " +
			"CITY_USE_RATE, " +
			"CITY_LOCAL_USE_RATE, " +
			"CITY_SPLIT_AMOUNT, " +
			"CITY_SPLIT_USE_RATE, " +
			"CITY_SINGLE_FLAG, " +
			"CITY_DEFAULT_FLAG, " +
			"COMBINED_USE_RATE, " +
			"LOAD_TIMESTAMP, " +
			"GL_EXTRACT_UPDATER, " +
			"GL_EXTRACT_TIMESTAMP, " +
			"GL_EXTRACT_FLAG, " +
			"GL_LOG_FLAG, " +
			"GL_EXTRACT_AMT, " +
			"AUDIT_FLAG, " +
			"AUDIT_USER_ID, " +
			"AUDIT_TIMESTAMP, " +
			"MODIFY_USER_ID, " +
			"MODIFY_TIMESTAMP, " +
			"UPDATE_USER_ID, " +
			"UPDATE_TIMESTAMP";
			*/
			return colBuf.toString();
		}
		
		protected void updateBatchUsingJDBC(BatchMaintenance batchMaintenance, Connection conn) throws Exception {	
			String sql = batchMaintenance.getRawUpdateStatement();
			PreparedStatement prep = conn.prepareStatement(sql);
			batchMaintenance.populateUpdatePreparedStatement(conn, prep);
			prep.addBatch();
		
			prep.executeBatch();
			conn.commit();
			prep.clearBatch();
			
			return;
		}
		
		protected BatchMaintenance addBatchUsingJDBC(BatchMaintenance batchMaintenance, Connection conn) {	
		  	//Get next batch Id 
			long nextId = -1;
			Statement stat = null;
			try{
				stat = conn.createStatement();
				
				String sql = DatabaseUtil.getSequenceQuery(DatabaseUtil.getDatabaseProductName(stat), "sq_tb_batch_id", "NEXTID");
				
		    	ResultSet rs = stat.executeQuery(sql);
		    	if (rs.next()) {
		    		nextId = rs.getLong("NEXTID");	
		    	}
		    	rs.close();	
			}
			catch (Exception e){
				logger.error("############ addBatchUsingJDBC() failed: " + e.toString()); 
				return null;
			}
			finally{
				if(stat != null){
					try{stat.close(); }catch (Exception ex){}
				}
			}
			
			//Set new batch id
			batchMaintenance.setBatchId(nextId);
			PreparedStatement prep = null;
			try{
			  String sql = batchMaintenance.getRawAddStatement();	
			  prep = conn.prepareStatement(sql);
			  batchMaintenance.populateAddPreparedStatement(conn, prep);
			  prep.addBatch();
			  prep.executeBatch();
			  conn.commit();
			}
			catch (Exception e){	
				return null;
			}
			finally{
			  if(prep!=null){try{prep.close();}catch(Exception ex){}}
			}
			
			return batchMaintenance;
		  }
		
		public static void main(String args[]){
			String fileName="C://" + "TP_DOWNLOAD_" + new SimpleDateFormat("yyyyMMdd_hhmmss_SSS").format(new Date()) + ".csv" ;
			
			System.out.println("fileName ; " + fileName);
			BufferedWriter out = null;
			try {
			    out = new BufferedWriter(new FileWriter(fileName));
			    
			    out.write("qqqqqqqqqqqqqqqww        ");
			    out.newLine();
			    out.flush();

			   // out.write("aString");
			} catch (Exception e) {
				e.printStackTrace();
			}

			
		//	FileReaderUtil file=new FileReaderUtil();
		//	file.readFile(fileName,true);
				
		}
		
		public List<String> getProcessStatusColumnsList() {
			String[] procStatusColumnsArray = { "TRANSACTION_IND", "GL_EXTRACT_FLAG", "STATE_TAXCODE_TYPE_CODE", 
					"COUNTY_TAXCODE_TYPE_CODE", "CITY_TAXCODE_TYPE_CODE", "SUSPEND_IND", "MULTI_TRANS_CODE"};
			
			List<String> procStatusColumnsList = Arrays.asList(procStatusColumnsArray);
			return procStatusColumnsList;
			
		}

}
