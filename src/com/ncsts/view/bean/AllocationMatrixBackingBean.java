package com.ncsts.view.bean;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Observer;
import java.util.Observable;

import org.ajax4jsf.component.html.HtmlAjaxSupport;
import org.apache.log4j.*;
import org.richfaces.component.html.HtmlDataTable;
import org.springframework.beans.BeanUtils;

import javax.faces.application.FacesMessage;
import javax.faces.component.html.HtmlInputText;
import javax.faces.context.FacesContext;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.ActionEvent;
import javax.servlet.http.HttpSessionBindingListener;
import javax.servlet.http.HttpSessionBindingEvent;

import com.ncsts.domain.AllocationMatrix;
import com.ncsts.domain.AllocationMatrixDetail;
import com.ncsts.domain.Jurisdiction;
import com.ncsts.domain.PurchaseTransaction;
import com.ncsts.jsf.model.AllocationMatrixDataModel;
import com.ncsts.services.AllocationMatrixService;
import com.ncsts.services.JurisdictionService;
import com.ncsts.view.util.FacesUtils;
import com.ncsts.common.*;
import com.ncsts.view.util.StateObservable;

/**
 * @author Paul Govindan
 *
 */

public class AllocationMatrixBackingBean extends BaseMatrixBackingBean<AllocationMatrix, AllocationMatrixService>  
	implements Observer, HttpSessionBindingListener{
	
    static private Logger logger = LoggerFactory.getInstance().getLogger(AllocationMatrixBackingBean.class);
    
    private StateObservable ov = null;
	
    private JurisdictionService jurisdictionService;
    
	private SearchJurisdictionHandler filterHandler = new SearchJurisdictionHandler();
	
	private SearchJurisdictionHandler jurisdictionHandler = new SearchJurisdictionHandler();
	
	private TaxJurisdictionBackingBean taxJurisdictionBean;
	
	private HtmlInputText allocationPercent;
	private Long jurisdictionId;
	private Long instanceCreatedId; //bug 5090
	
	private int selectedMasterRowIndex= -1;
	private AllocationMatrix selectedMasterMatrixSource = null;
	private List<AllocationMatrix> selectedMatrixDetails = null;
	
	public TaxJurisdictionBackingBean getTaxJurisdictionBean() {
		return taxJurisdictionBean;
	}

	public void setTaxJurisdictionBean(TaxJurisdictionBackingBean taxJurisdictionBean) {
		this.taxJurisdictionBean = taxJurisdictionBean;
	}
	
	public Long getInstanceCreatedId() {
		return instanceCreatedId;
	}
	
	public void init() {
		updateMatrixList();
	}

	public void setInstanceCreatedId(Long instanceCreatedId) {
		this.instanceCreatedId = instanceCreatedId;
	}
	
	public int getselectedMasterRowIndex() {
		return selectedMasterRowIndex;
	}

	public void setselectedMasterRowIndex(int selectedMasterRowIndex) {
		this.selectedMasterRowIndex= selectedMasterRowIndex;
	}
	
	@Override
	public void resetSelection() {
		selectedMasterRowIndex= -1;
		selectedMasterMatrixSource = null;
		selectedMatrixDetails = null;
		super.resetSelection();
	}

	public List<AllocationMatrix> getSelectedMatrixDetails() {	
		if(selectedMatrixDetails==null){
			selectedMatrixSource = null;
			setSelectedRowIndex(-1);
			selectedMatrixDetails = new ArrayList<AllocationMatrix>();
		}
		return selectedMatrixDetails;
	}
	
	@SuppressWarnings("unchecked")
	public synchronized void selectedMasterRowChanged(ActionEvent e) throws AbortProcessingException {
	    HtmlDataTable table = (HtmlDataTable) ((HtmlAjaxSupport) e.getComponent()).getParent(); 
	    selectedMasterRowIndex = table.getRowIndex();
	    selectedMasterMatrixSource = (AllocationMatrix) table.getRowData();
	    
	    super.resetSelection();
	    
	    //Reset middle table
	    selectedMatrixDetails = null;

	    AllocationMatrix matrix = selectedMasterMatrixSource;
	    if(selectedMasterMatrixSource != null){
			selectedMatrixDetails = getMatrixService().getDetailRecords(matrix,getMatrixDataModel().getDetailOrderBy());
			
			//Set first row as selected if exists.
			if(selectedMatrixDetails != null && selectedMatrixDetails.size()>0){	
				AllocationMatrix matrixMiddle = ((AllocationMatrix) selectedMatrixDetails.get(0));
				if (matrixMiddle != null) {
					selectedMatrixSource = getMatrixService().findByIdWithDetails(matrixMiddle.getAllocationMatrixId());
					setSelectedRowIndex(0);
				}
			}
			else{
				selectedMatrixDetails = new ArrayList<AllocationMatrix>();
			}
		}
		else {
			selectedMatrixDetails = new ArrayList<AllocationMatrix>();
		}
	}
	
	@Override
	public String getBeanName() {
		return "allocationMatrixBean";
	}

	@Override
	public boolean getIncludeDescFields() {
		return false;
	}
	
	@Override
	public String getMainPageAction() {
		return "allocation_matrix_main";
	}
	
	@Override
	public String getDetailFormName() {
		return "matrixDetailForm";
	}

	public List<AllocationMatrixDetail> getSelectedMatrixAllocDetails() {
		AllocationMatrix matrix = (selectedMatrix != null)? selectedMatrix:selectedMatrixSource;
		//AllocationMatrix matrix = selectedMatrixSource;
		List<AllocationMatrixDetail> result = (matrix == null)?
				(new ArrayList<AllocationMatrixDetail>()):matrix.getAllocationMatrixDetails();
		logger.debug("Detail items: " + result.size());
		return result;
	}
	
	public JurisdictionService getJurisdictionService() {
		return jurisdictionService;
	}

	public void setJurisdictionService(JurisdictionService jurisdictionService) {
		this.jurisdictionService = jurisdictionService;
	}		

	@Override
	public void setMatrixCommonBean(MatrixCommonBean matrixCommonBean) {
		super.setMatrixCommonBean(matrixCommonBean);
		filterHandler.setJurisdictionService(matrixCommonBean.getJurisdictionService());
		filterHandler.setCacheManager(matrixCommonBean.getCacheManager());
		filterHandler.setTaxJurisdictionBean(taxJurisdictionBean);
		filterHandler.setCallbackScreen("allocation_matrix_main");
		
		jurisdictionHandler.setJurisdictionService(matrixCommonBean.getJurisdictionService());
		//MF - do not set default country
		//filterHandler.reset();
		//filterHandler.setCountry(getMatrixCommonBean().getUserPreferenceDTO().getUserCountry());
		//filterHandler.setState("");

		jurisdictionHandler.setJurisdictionService(matrixCommonBean.getJurisdictionService());
		jurisdictionHandler.setCacheManager(matrixCommonBean.getCacheManager());
		jurisdictionHandler.setTaxJurisdictionBean(taxJurisdictionBean);
		jurisdictionHandler.setCallbackScreen("allocation_matrix_detail");
		
	}

	public void update(Observable obs, Object obj) {
		if (obs == ov) {
			filterHandler.setChanged();
			jurisdictionHandler.setChanged();
		}
	}
	
	public void valueBound(HttpSessionBindingEvent event){
		StateObservable ov = StateObservable.getInstance();
		this.ov = ov;
		this.ov.addObserver(this);
	}

	public void valueUnbound(HttpSessionBindingEvent event){	
		if(ov!=null){
			ov.deleteObserver(this);
		}
	}
	
	public SearchJurisdictionHandler getFilterHandler() {
		return filterHandler;
	}

	public SearchJurisdictionHandler getJurisdictionHandler() {
		return jurisdictionHandler;
	}

	@Override
	public String resetFilter() {
		filterHandler.reset();
		//MF - do not set default country
		//filterHandler.setCountry(getMatrixCommonBean().getUserPreferenceDTO().getUserCountry());
		//filterHandler.setState("");
		
		super.resetFilter();
		return "allocation_matrix_main";
	}

	public String updateMatrixList () {
		
		AllocationMatrix matrix = newMatrix();
		BeanUtils.copyProperties(getFilterSelection(), matrix);
		
		// Now, map entity level value to the driver properties
		matrixCommonBean.setDriverEntityProperties(matrix);
		
		// Fix the driver values
		matrix.adjustDriverFields();
		
		// NULLs get converted to 0, change back to null
		Long id = matrix.getId();
		if ((id != null) && (id <= 0L)) {
			matrix.setId(null);
		}
		
		updateMatrixFilter(matrix);

		((AllocationMatrixDataModel) getMatrixDataModel()).setFilterCriteria(matrix, 
				filterHandler.getGeocode(), filterHandler.getCity(), filterHandler.getCounty(), filterHandler.getState(), filterHandler.getCountry(), filterHandler.getZip(),
				matrix.getAllocationMatrixId());
	  	resetSelection();
	  	return "allocation_matrix_main";
    }
	
	public HtmlInputText getAllocationPercent() {
		return allocationPercent;
	}

	public void setAllocationPercent(HtmlInputText allocationPercent) {
		this.allocationPercent = allocationPercent;
	}

	public Long getJurisdictionId() {
		return jurisdictionId;
	}

	public void setJurisdictionId(Long jurisdictionId) {
		this.jurisdictionId = jurisdictionId;
	}

	@Override
	@SuppressWarnings("unchecked")
	public synchronized void selectedRowChanged(ActionEvent e) throws AbortProcessingException {
		// NOTE:  We preset selection to null to deal 
		// with multiple requests coming in while still
		// fetching matrix details
		selectedMatrixSource = null;
	    HtmlDataTable table = (HtmlDataTable) ((HtmlAjaxSupport) e.getComponent()).getParent(); 
		setSelectedRowIndex(table.getRowIndex());
		AllocationMatrix matrix = ((AllocationMatrix) table.getRowData());
		if (matrix != null) {
			selectedMatrixSource = getMatrixService().findByIdWithDetails(matrix.getAllocationMatrixId());
		}
		logger.debug("Selected row: " + getSelectedRowIndex() + 
				"  Matrix ID: " + ((selectedMatrixSource == null)? "NULL":selectedMatrixSource.getId()));		
	}

	@Override
	protected void prepareDetailDisplay(EditAction action, PurchaseTransaction detail) {
		if(filterHandler.getCountry()==null || filterHandler.getCountry().equals("")){
			//Set default country
			jurisdictionHandler.reset();
			jurisdictionHandler.setCountry(getMatrixCommonBean().getUserPreferenceDTO().getUserCountry());
			jurisdictionHandler.setState("");
		}
		else{
			//Set default country
			jurisdictionHandler.reset();
			jurisdictionHandler.setCountry(filterHandler.getCountry());
			jurisdictionHandler.setState("");
		}

		allocationPercent = null;
		super.prepareDetailDisplay(action, detail);
	}

	@Override
	protected AllocationMatrix copyMatrix(AllocationMatrix source) {
		AllocationMatrix result;
		List<AllocationMatrixDetail> details;
		
		// Details are handled with care
		details = source.getAllocationMatrixDetails(); 
		source.setAllocationMatrixDetails(null);

		result = super.copyMatrix(source);
		result.setAllocationMatrixDetails(new ArrayList<AllocationMatrixDetail>());
		
		if (details != null) {
			for (AllocationMatrixDetail d : details) {
				AllocationMatrixDetail copy = new AllocationMatrixDetail();
				copy.setAllocationPercent(d.getAllocationPercent());
				copy.setJurisdiction(d.getJurisdiction());
				result.getAllocationMatrixDetails().add(copy);
			}

			// Restore original value
			source.setAllocationMatrixDetails(details);
		}
		
		return result;
	}
	
	@Override
	public String saveAction() {
		boolean valid = validateDrivers(FacesContext.getCurrentInstance(), null);
		
		//Midtier project code modified - january 2009
		//Replace of trigger
		selectedMatrix.setBinaryWeight(super.CalcBinaryWeight());
		selectedMatrix.setSignificantDigits(super.CalcSignificantDigits());
		
		logger.debug("\n \t binaryWeight by tb_allocation_matrix_b_iu : " + selectedMatrix.getBinaryWeight());
		logger.debug("\n \t significantDigits by tb_allocation_matrix_b_iu : " + selectedMatrix.getSignificantDigits());
		//End of trigger
		
		if(selectedMatrix.getAllocationMatrixDetails()!=null){
			for(AllocationMatrixDetail allocationMatrixDetail : selectedMatrix.getAllocationMatrixDetails()){
				if(allocationMatrixDetail.getJurisdiction()==null){
					FacesMessage message = new FacesMessage("Jurisdiction not found.");
					message.setSeverity(FacesMessage.SEVERITY_ERROR);
					FacesContext.getCurrentInstance().addMessage(null, message);
					return null;
				}
			}
		}
		
		if ((selectedMatrix != null) && (Math.abs(selectedMatrix.getTotalAllocationPercent() - 1.0) > 0.00000001)) {
			FacesMessage message = new FacesMessage("Total allocation % must equal 100%.");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
			valid = false;
	    }
		
		if (!valid) return null; 

		return super.saveAction();
	}

	// METHOD MODIFIED - 02-12-2009 - Bug 0003956 
	// 4073 
	public String addMatrixDetail() { 
		logger.info("enter addMatrixDetail");
		// Re-verify jurisdiction, 
		Jurisdiction jurisdiction = jurisdictionHandler.getJurisdictionFromDB();
		if (jurisdiction == null) {
			FacesMessage message = new FacesMessage("Jurisdiction not found or not unique.");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
			return null;
		}

        logger.info("jurisdiction.id = " + jurisdiction.getJurisdictionId());
	    logger.info("jurisdiction.state = " + jurisdiction.getState());
	    logger.info("jurisdiction.geocode = " + jurisdiction.getGeocode());
	    logger.info("alloc id = " + this.selectedMatrix.getAllocationMatrixId());

	    // Make sure not already assigned
	    for (AllocationMatrixDetail amd : selectedMatrix.getAllocationMatrixDetails()) {
	    	if(amd.getJurisdiction()==null){
				FacesMessage message = new FacesMessage("Jurisdiction not found.");
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage(null, message);
				return null;
			}
	    	
	    	if (amd.getJurisdiction().getJurisdictionId().equals(jurisdiction.getJurisdictionId())) {
				FacesMessage message = new FacesMessage("Jurisdiction allocation already exists.");
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage(null, message);
				return null;
	    	}
	    }
	    
	    Number num = (Number) allocationPercent.getValue();
	    Double pct = (num == null)? null:num.doubleValue();
	  	    	   	    
	    if ((pct == null) || (pct <= 0) || (pct > 1)) {
			FacesMessage message = new FacesMessage("Allocation percent must be between 0 and 1.");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
			return null;
	    }

	    AllocationMatrixDetail amd = new AllocationMatrixDetail();
	    amd.setJurisdiction(jurisdiction);
	    amd.setAllocationPercent(pct);
	    
    	selectedMatrix.getAllocationMatrixDetails().add(amd);
    	
    	//Set default country
		jurisdictionHandler.reset();
		jurisdictionHandler.setCountry(getMatrixCommonBean().getUserPreferenceDTO().getUserCountry());
		jurisdictionHandler.setState("");
    	
    	allocationPercent.setValue(null);
	    return null;
	} 	
	
	public void deleteMatrixDetail(ActionEvent e) {
		logger.info("enter deleteMatrixDetail");
		
		Iterator<AllocationMatrixDetail> iter = selectedMatrix.getAllocationMatrixDetails().iterator(); 
		while (iter.hasNext()) {
			AllocationMatrixDetail detail = iter.next();
			
			if (detail.getInstanceCreatedId().equals(instanceCreatedId)) {
				iter.remove();
				FacesUtils.refreshForm("matrixDetailForm");
				break;
			}
		}
	}
	
	
	public String ViewJurAllocn(Long allocationMatrixId,String fromview) {
		if(fromview.equals("viewFromTransaction")){
			currentAction = EditAction.VIEW_FROM_TRANSACTION;
		}
		else {
			currentAction = EditAction.VIEW_FROM_TRANSACTION_LOG_ENTRY;
		}
		
		selectedMatrixSource = getMatrixService().findByIdWithDetails(allocationMatrixId);
		if(selectedMatrixSource == null){
			return null;
		}
		selectedMatrix = selectedMatrixSource;
		prepareDetailDisplay(currentAction, null);
	    return "matrix_allocation_view";
	}
	
	@Override
	public void searchDriver(ActionEvent e) {
		String id = (String) e.getComponent().getAttributes().get("ID");
		super.searchDriver(e);

		if(id.equalsIgnoreCase("filterPanel")) {
			getDriverHandlerBean().setCallBackScreen("allocation_matrix_main");
		}
		else{
			getDriverHandlerBean().setCallBackScreen("allocation_matrix_detail");

		}		
	}
	
	public String viewAction() {
		return mainPageAction();
	}
}

