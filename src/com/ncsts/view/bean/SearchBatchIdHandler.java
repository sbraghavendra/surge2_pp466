package com.ncsts.view.bean;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.faces.component.UIInput;
import javax.faces.component.UISelectItems;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.ajax4jsf.component.html.HtmlAjaxSupport;
import org.apache.log4j.Logger;
import org.richfaces.component.html.HtmlDataTable;

import com.ncsts.common.CacheManager;
import com.ncsts.common.LoggerFactory;
import com.ncsts.domain.BatchMaintenance;
import com.ncsts.domain.ListCodes;
import com.ncsts.common.Util;
import com.ncsts.services.BatchMaintenanceService;
import com.ncsts.jsf.model.BaseExtendedDataModel;

public class SearchBatchIdHandler {

   	static private Logger logger = LoggerFactory.getInstance().getLogger(SearchBatchIdHandler.class);
   	
   	private HtmlSelectOneMenu batchTypeInput = new HtmlSelectOneMenu();
   	private HtmlSelectOneMenu batchStatusInput = new HtmlSelectOneMenu();
   	
	private Long selectedItemKey;
	private List<BatchMaintenance> searchList;
	
	private BatchMaintenanceService batchMaintenanceService;
	private CacheManager cacheManager;
	
	private String defaultSearch = "";
	Map<String,String> sortOrder;

	public HtmlSelectOneMenu getBatchTypeInput() {
		if(getIsDefaultSearch()){
			setBatchType(defaultSearch);
		}
		return batchTypeInput;
	}

	public void setBatchTypeInput(HtmlSelectOneMenu batchTypeInput) {
		this.batchTypeInput = batchTypeInput;
	}

	public HtmlSelectOneMenu getBatchStatusInput() {
		return batchStatusInput;
	}

	public void setBatchStatusInput(HtmlSelectOneMenu batchStatusInput) {
		this.batchStatusInput = batchStatusInput;
	}

	public CacheManager getCacheManager() {
		return cacheManager;
	}

	public void setCacheManager(CacheManager cacheManager) {
		this.cacheManager = cacheManager;

		List<SelectItem> batchTypeList = new ArrayList<SelectItem>();		
		batchTypeList.add(new SelectItem("","Select Batch Type"));		
		for (ListCodes listcodes : cacheManager.getListCodesByType("BATCHTYPE")) {		
			batchTypeList.add(new SelectItem(listcodes.getCodeCode(),listcodes.getDescription()));
		}
		
		UISelectItems items = new UISelectItems();
		items.setValue(batchTypeList);
		batchTypeInput.getChildren().clear();
		batchTypeInput.getChildren().add(items);
		
		List<SelectItem> batchStatuslist = new ArrayList<SelectItem>();
		batchStatuslist.add(new SelectItem("","Select Batch Status"));
		for (ListCodes listcodes : cacheManager.getListCodesByType("BATCHSTAT")) {
			batchStatuslist.add(new SelectItem(listcodes.getCodeCode(),listcodes.getDescription()));
		}

		items = new UISelectItems();
		items.setValue(batchStatuslist);
		batchStatusInput.getChildren().clear();
		batchStatusInput.getChildren().add(items);
	}

	public BatchMaintenanceService getBatchMaintenanceService() {
		return batchMaintenanceService;
	}

	public void setBatchMaintenanceService(BatchMaintenanceService batchMaintenanceService) {
		this.batchMaintenanceService = batchMaintenanceService;
	}

	public String getBatchType() {
		return getInputValue(batchTypeInput);
	}

	public void setBatchType(String batchType) {
		setInputValue(batchTypeInput, batchType);
	}

	public String getBatchStatus() {
		return getInputValue(batchStatusInput);
	}

	public void setBatchStatus(String batchStatus) {
		setInputValue(batchStatusInput, batchStatus);
	}

	public void reset() {
		if(!getIsDefaultSearch()){
			setBatchType(null);
		}
		setBatchStatus(null);
		searchList = null;
		selectedItemKey = null;
		sortOrder = null;
	}
	
	public Long getSelectedItemKey() {
		return selectedItemKey;
	}

	public void setSelectedItemKey(Long selectedItemKey) {
		this.selectedItemKey = selectedItemKey;
	}

	public List<BatchMaintenance> getSearchList() {
		return searchList;
	}

	protected void initializeSearch() {
		searchList = performSearch();
		selectedItemKey = null;
	}
	
	public String getDefaultSearch(){		
		return defaultSearch;		
	}
	
	public void setDefaultSearch(String defaultSearch){
		this.defaultSearch = defaultSearch;
		
		setBatchType(defaultSearch);
	}
	
	public boolean getIsDefaultSearch(){
		if(defaultSearch==null || defaultSearch.length()==0){
			return false;
		}
		else{
			return true;
		}
	}
	
	public boolean getIsIP(){
		if(defaultSearch!=null && defaultSearch.equalsIgnoreCase("IP")){
			return true;
		}
		else{
			return false;
		}
	}
	
	public void clearAction(ActionEvent e) {
		reset();
	}
	
	public void searchAction(ActionEvent e) {
		initializeSearch();
	}
	
	public boolean getValidSearchResult() {
		return (selectedItemKey != null);
	}

	@SuppressWarnings("unchecked")
	public void selectItem(ActionEvent e) { 
	    HtmlDataTable table = (HtmlDataTable) ((HtmlAjaxSupport) e.getComponent()).getParent(); 
	    selectedItemKey = ((BatchMaintenance) table.getRowData()).getId();
		logger.debug("Selected ID: " + ((selectedItemKey == null)? "NULL":selectedItemKey));		
	}

	public BatchMaintenance getSelectedItem() {
		for (BatchMaintenance ref : searchList) {
			if (ref.getId().equals(selectedItemKey)) {
				return ref;
			}
		}
		return null;
	}
	
	private List<BatchMaintenance> performSearch() {
		BatchMaintenance example = new BatchMaintenance();
		example.setBatchTypeCode(getBatchType());
		example.setBatchStatusCode(getBatchStatus());
		
		Map<String,String> localSortOrder = getSortOrder();
		String sortField = "";
		boolean ascending = false;
		for (String field : localSortOrder.keySet()) {
			if (BaseExtendedDataModel.SORT_ASCENDING.equals(localSortOrder.get(field))) {
				sortField = field;
				ascending = true;
			} 
			else if (BaseExtendedDataModel.SORT_DESCENDING.equals(localSortOrder.get(field))) {
				sortField = field;
				ascending = false;
			}
		}
		
		if(sortField!=null && sortField.length()!=0){
			return batchMaintenanceService.findByExampleSorted(example, 0, sortField, ascending);
		}
		else{
			return batchMaintenanceService.findByExample(example, 0);
		}
	}
	
	private void setInputValue(UIInput input, String value) {
		input.setLocalValueSet(false);
		input.setSubmittedValue(null);
		input.setValue(value);
	}

	private String getInputValue(UIInput input) {
		String value = (String) input.getSubmittedValue();
		return (value != null)? value:(String) input.getValue();
	} 
	
	public void sortAction(ActionEvent e) {
		toggleSortOrder(e.getComponent().getParent().getId());
	}
	
	public Map<String,String> getSortOrder() {
    	if (sortOrder == null) {
    		sortOrder = new HashMap<String,String>();
        	for (Method m : BatchMaintenance.class.getMethods()) {
        		if (m.getName().startsWith("get")) {
        			String field = Util.lowerCaseFirst(m.getName().substring(3));
        			sortOrder.put(field, BaseExtendedDataModel.SORT_NONE);
        		}
        	}
    	}
    	return sortOrder;
    }
	
	public void toggleSortOrder(String field) {
    	Map<String,String> sortOrder = getSortOrder();
    	if (sortOrder.containsKey(field) && BaseExtendedDataModel.SORT_ASCENDING.equals(sortOrder.get(field))) {
			sortOrder.put(field, BaseExtendedDataModel.SORT_DESCENDING);
		} else {
			sortOrder.put(field, BaseExtendedDataModel.SORT_ASCENDING);
		}
    	
		for (String key : sortOrder.keySet()) {
			if (!key.equals(field)) {
				sortOrder.put(key, BaseExtendedDataModel.SORT_NONE);
			}
		}
		
		initializeSearch();
    }
}

