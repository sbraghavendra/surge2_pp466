package com.ncsts.view.bean;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.ActionEvent;
import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;

import org.ajax4jsf.component.UIDataAdaptor;
import org.apache.log4j.Logger;
import org.apache.myfaces.custom.fileupload.HtmlInputFileUpload;
import org.richfaces.component.html.HtmlCalendar;
import org.springframework.beans.factory.annotation.Autowired;

import com.ncsts.common.CacheManager;
import com.ncsts.common.DatabaseUtil;
import com.ncsts.domain.Auditable;
import com.ncsts.domain.BCPSitusRule;
import com.ncsts.domain.BCPSitusRuleText;
import com.ncsts.domain.BatchErrorLog;
import com.ncsts.domain.BatchMaintenance;
import com.ncsts.domain.BatchMetadata;
import com.ncsts.domain.SitusRulesBatch;
import com.ncsts.domain.User;
import com.ncsts.dto.BatchMetadataList;
import com.ncsts.dto.DbPropertiesDTO;
import com.ncsts.jsf.model.BCPSitusRuleDataModel;
import com.ncsts.jsf.model.BCPSitusRuleTextDataModel;
import com.ncsts.jsf.model.BatchMaintenanceDataModel;
import com.ncsts.management.DbPropertiesBean;
import com.ncsts.services.BatchMaintenanceService;
import com.ncsts.view.util.BatchStatistics;
import com.ncsts.view.util.FacesUtils;
import com.ncsts.view.util.SitusRulesBatchValidator;
import com.ncsts.view.util.SitusRulesUploadParser;
import com.ncsts.view.util.StateObservable;

public class SitusRulesBackingBean implements Observer,
		HttpSessionBindingListener {
	private Logger logger = Logger.getLogger(SitusRulesBackingBean.class);
	private StateObservable ov = null;

	private enum AddState {
		NEED_FILE, HAVE_FILE, PROCESSING, COMPLETE
	}

	private BatchMaintenanceBackingBean batchMaintenanceBean;
	private FilePreferenceBackingBean filePreferenceBean;
	private BatchMaintenanceService batchMaintenanceService;
	private BatchStatistics batchStatistics;
	private BatchErrorsBackingBean batchErrorsBean;
	private BatchMaintenance exampleBatchMaintenance;

	private BatchMaintenance selectedBatch = null;
	private int selectedBatchIndex = -1;
	private List<SitusRulesBatch> selectedBatches;

	private BatchMaintenanceDataModel batchMaintenanceDataModel;
	private BatchMetadata batchMetadata;

	private BCPSitusRuleDataModel bcpSitusRuleDataModel;
	private BCPSitusRuleTextDataModel bcpSitusRuleTextDataModel;

	private BatchMetadataList batchMetadataList;

	private Thread background;
	private String backgroundError;
	private AddState state;
	
	private HtmlCalendar batchStartTime = new HtmlCalendar();

	private SitusRulesUploadParser parser = new SitusRulesUploadParser(
			FacesUtils.getServletContext().getRealPath("")
					+ "/situsrulesfile/");

	private HtmlInputFileUpload uploadFile;

	private DbPropertiesBean dbPropertiesBean;
	private DbPropertiesDTO dbPropertiesDTO = null;
	
	@Autowired
	private loginBean loginBean;
	
	@Autowired
	private CacheManager cacheManager;
	
	public void setCacheManager(CacheManager cacheManager) {
		this.cacheManager = cacheManager;
	}


    public BatchMaintenanceBackingBean getBatchMaintenanceBean() {
		return batchMaintenanceBean;
	}

	public void setBatchMaintenanceBean(
			BatchMaintenanceBackingBean batchMaintenanceBean) {
		this.batchMaintenanceBean = batchMaintenanceBean;
	}


	@Override
	public void update(Observable obs, Object obj) {
	}

	@Override
	public void valueBound(HttpSessionBindingEvent event) {
		StateObservable ov = StateObservable.getInstance();
		this.ov = ov;
		this.ov.addObserver(this);
	}

	@Override
	public void valueUnbound(HttpSessionBindingEvent event) {
		if (ov != null) {
			ov.deleteObserver(this);
		}
	}

	public BatchMaintenance getSelectedBatch() {
		return selectedBatch;
	}

	public void setSelectedBatch(BatchMaintenance selectedBatch) {
		this.selectedBatch = selectedBatch;
	}

	public int getSelectedBatchIndex() {
		return selectedBatchIndex;
	}

	public void setSelectedBatchIndex(int selectedBatchIndex) {
		this.selectedBatchIndex = selectedBatchIndex;
	}

	public BatchMaintenanceDataModel getBatchMaintenanceDataModel() {
		if (exampleBatchMaintenance == null) {
			init();
		}
		return batchMaintenanceDataModel;
	}

	public void setBatchMaintenanceDataModel(
			BatchMaintenanceDataModel batchMaintenanceDataModel) {
		this.batchMaintenanceDataModel = batchMaintenanceDataModel;
	}

	public BatchMetadata getBatchMetadata() {
		if (this.batchMetadata == null) {
			this.batchMetadata = batchMaintenanceService
					.findBatchMetadataById(SitusRulesBatch.BATCH_CODE);
		}
		return batchMetadata;
	}

	public void setBatchMetadata(BatchMetadata batchMetadata) {
		this.batchMetadata = batchMetadata;
	}

	public BatchMaintenanceService getBatchMaintenanceService() {
		return batchMaintenanceService;
	}

	public void setBatchMaintenanceService(
			BatchMaintenanceService batchMaintenanceService) {
		this.batchMaintenanceService = batchMaintenanceService;
	}

	public String processAddUpdateAction() {
		background = null;
		state = AddState.NEED_FILE;
		parser.clear();
		this.uploadFile = new HtmlInputFileUpload();
		return "situs_rules_add";
	}

	public HtmlInputFileUpload getUploadFile() {
		return uploadFile;
	}

	public void setUploadFile(HtmlInputFileUpload uploadFile) {
		this.uploadFile = uploadFile;
	}

	public String selectAllAction() {
		batchMaintenanceDataModel.setSelectAllSitusRulesUpdates();
		return null;
	}

	public Boolean getDisplayProcess() {
		if ((selectedBatchIndex != -1) && (selectedBatch != null)
				&& (SitusRulesBatch.canBeProcessed(selectedBatch)))
			return true;
		return getHasSelection();
	}

	private boolean getHasSelection() {
		Iterator<BatchMaintenance> iter = batchMaintenanceDataModel.iterator();
		while (iter.hasNext()) {
			if (iter.next().getSelected()) {
				return true;
			}
		}
		return false;
	}

	public String processAction() {
		SitusRulesBatchValidator validator = new SitusRulesBatchValidator(
				batchMaintenanceDataModel,
				filePreferenceBean.getImportMapProcessPreferenceDTO());
		Iterator<BatchMaintenance> iter = batchMaintenanceDataModel.iterator();
		selectedBatches = new ArrayList<SitusRulesBatch>();
		while (iter.hasNext()) {
			BatchMaintenance bm = iter.next();
			if (bm.getSelected())
				selectedBatches.add(new SitusRulesBatch(bm));
		}
		if ((selectedBatches.size() < 1) && (selectedBatch != null))
			selectedBatches.add(new SitusRulesBatch(selectedBatch));
		// Iterate through batches in reverse order, adjusting last processed
		// along the way
		Collections.sort(selectedBatches, new BatchComparator());
		for (int i = selectedBatches.size(); i > 0; i--) {
			validator.validate(selectedBatches.get(i - 1));
		}

		return "situs_rules_submit_batches";
	}

	public class BatchComparator implements Comparator<SitusRulesBatch> {
		@Override
		public int compare(SitusRulesBatch o1, SitusRulesBatch o2) {
			if (o1.getReleaseDate().getTime() > o2.getReleaseDate().getTime())
				return -1;
			else if (o1.getReleaseDate().getTime() < o2.getReleaseDate()
					.getTime())
				return 1;
			else {
				if (o1.getReleaseVersion() > o2.getReleaseVersion())
					return -1;
				else if (o1.getReleaseVersion() < o2.getReleaseVersion())
					return 1;
			}
			return 0;
		}

	}

	public FilePreferenceBackingBean getFilePreferenceBean() {
		return filePreferenceBean;
	}

	public void setFilePreferenceBean(
			FilePreferenceBackingBean filePreferenceBean) {
		this.filePreferenceBean = filePreferenceBean;
	}

	public String statisticsAction() {
		batchStatistics = new BatchStatistics(selectedBatch,
				batchMaintenanceDataModel.getBatchMaintenanceDAO(),
				filePreferenceBean.getBatchPreferenceDTO());
		batchStatistics.calcStatistics();
		return "situs_rules_batch_statistics";
	}

	public BatchStatistics getBatchStatistics() {
		return batchStatistics;
	}

	public void setBatchStatistics(BatchStatistics batchStatistics) {
		this.batchStatistics = batchStatistics;
	}

	public String errorsAction() {
		batchErrorsBean.setBatchMaintenance(selectedBatch);
		batchErrorsBean.setReturnView("situs_rules");
		
		//0001701: Import errors make the system crash
		batchErrorsBean.getBatchErrorDataModel().setBatchMaintenance(selectedBatch);
		
		return "batch_maintenance_errors";
	}

	public BatchErrorsBackingBean getBatchErrorsBean() {
		return batchErrorsBean;
	}

	public void setBatchErrorsBean(BatchErrorsBackingBean batchErrorsBean) {
		this.batchErrorsBean = batchErrorsBean;
	}

	public Boolean getDisplayError() {
		return (selectedBatchIndex != -1)
				&& (selectedBatch.getErrorSevCode() != null)
				&& (selectedBatch.getErrorSevCode().trim().length() > 0);
	}

	public String viewAction() {
		bcpSitusRuleDataModel.setBatchMaintenance(selectedBatch);
		if (getUploadRecordCount()) {
			return viewFormattedAction();
		} else {
			return viewUnformattedAction();
		}
	}

	public Boolean getUploadRecordCount() {
		return (bcpSitusRuleDataModel.count() > 0);
	}

	public String viewFormattedAction() {
		bcpSitusRuleDataModel.setBatchMaintenance(selectedBatch);
		return "situs_rules_view_formatted";
	}

	public String viewUnformattedAction() {
		bcpSitusRuleTextDataModel.setBatchMaintenance(selectedBatch);
		return "situs_rules_view_unformatted";
	}
	public BCPSitusRuleDataModel getBcpSitusRuleDataModel() {
		return bcpSitusRuleDataModel;
	}

	public void setBcpSitusRuleDataModel(
			BCPSitusRuleDataModel bcpSitusRuleDataModel) {
		this.bcpSitusRuleDataModel = bcpSitusRuleDataModel;
	}
	
	public BCPSitusRuleTextDataModel getBcpSitusRuleTextDataModel() {
		return bcpSitusRuleTextDataModel;
	}

	public void setBcpSitusRuleTextDataModel(
			BCPSitusRuleTextDataModel bcpSitusRuleTextDataModel) {
		this.bcpSitusRuleTextDataModel = bcpSitusRuleTextDataModel;
	}

	public String refreshAction() {
		init();
		batchMaintenanceDataModel.refreshData(false);
		selectedBatchIndex = -1;
		return "situsRulesUploadSuccess";
	}

	private void init() {
		batchMaintenanceDataModel.setDefaultSortOrder("batchId",
				BatchMaintenanceDataModel.SORT_DESCENDING);
		exampleBatchMaintenance = new BatchMaintenance();
        batchMaintenanceBean.resetSearchAction();
		exampleBatchMaintenance.setEntryTimestamp(null);
		exampleBatchMaintenance.setBatchStatusCode(null);
		exampleBatchMaintenance.setBatchTypeCode(SitusRulesBatch.BATCH_CODE);
        batchMaintenanceBean.setBatchTypeCode(SitusRulesBatch.BATCH_CODE) ;  
		batchMaintenanceBean.setCalledFromConfigImportExpMenu(false);
	    batchMaintenanceBean.prepareBatchTypeSpecificsFields();
		batchMaintenanceDataModel.setPageSize(50);
		batchMaintenanceDataModel.setCriteria(exampleBatchMaintenance);
	}

	public String getLastSitusRuleRelUpdate() {
		return filePreferenceBean.getImportMapProcessPreferenceDTO()
				.getLastSitusRulesDateUpdate();
	}

	public boolean getDisplayFileSelect() {
		return state == AddState.NEED_FILE;
	}

	public boolean getDisplayFileHeader() {
		switch (state) {
		case HAVE_FILE:
			return true;
		case PROCESSING:
			return true;
		case COMPLETE:
			return true;
		default:
			return false;
		}
	}

	public boolean getDisplayChangeFile() {
		return state == AddState.HAVE_FILE;
	}

	public String getUploadFileName() {
		return parser.getUploadFileName();
	}

	public Boolean getDisplayUploadAdd() {
		logger.debug("displayUploadAdd: " + state);
		return state == AddState.HAVE_FILE;
	}

	public String getFileType() {
		return parser.getFileType();
	}

	public Date getReleaseDate() {
		return parser.getReleaseDate();
	}

	public String getReleaseVersion() {
		return this.parser.getReleaseVersion();
	}

	public String getProgress() {
		if (backgroundError != null) {
			addError(backgroundError);
			return "100.0";
		} else if (state == AddState.COMPLETE) {
			logger.debug("getProgress: complete");
			return "100.0";
		}
		NumberFormat nf = NumberFormat.getNumberInstance();
		nf.setMaximumFractionDigits(1);
		try {
			String s = "0.0";
			Double scale = parser.getProgress() * 99.0;
			if (scale >= 0.01) {
				s = nf.format(scale);
			}
			logger.debug("Progress at: " + s);
			return s;
		} catch (Exception e) {
			logger.error(e.getMessage());
			return "0.0";
		}
	}

	private void addError(String s) {
		addMessage(s, FacesMessage.SEVERITY_ERROR);
	}

	private void addMessage(String s, FacesMessage.Severity severity) {
		FacesContext.getCurrentInstance().addMessage(null,
				new FacesMessage(severity, s, null));
	}

	public Boolean getDisplayProgress() {
		if (background != null)
			logger.debug("Progress: " + state.toString() + ", "
					+ background.getState().toString());
		else
			logger.debug("Progress: " + state.toString());
		switch (state) {
		case PROCESSING:
			return true;
		case COMPLETE:
			return true;
		default:
			return false;
		}
	}

	public boolean getDisplayComplete() {
		return state == AddState.COMPLETE;
	}

	public String backgroundCompleteAction() {
		background = null;

		batchMaintenanceDataModel.refreshData(false);
		selectedBatchIndex = -1;
		selectedBatch = null;
		return "situs_rules_cancel_action";
	}

	public String cancelAction() {
		background = null;
		parser.clear();
		return "situs_rules_cancel_action";
	}

	public String getLastSitusRulesRelUpdate() {
		return filePreferenceBean.getImportMapProcessPreferenceDTO()
				.getLastSitusRulesDateUpdate();
	}
	
	public String getLastTaxRateRelUpdate() {
		return filePreferenceBean.getImportMapProcessPreferenceDTO()
				.getLastTaxRateRelUpdate();
	}

	public void fileUploadAction() throws IOException {
		if (this.uploadFile.getUploadedFile() != null) {
			boolean foundError = false; // 4323 - see below
			parser.upload(this.uploadFile.getUploadedFile().getName(),
					this.uploadFile.getUploadedFile().getInputStream());
			for (String m : parser.getErrors()) {
				addError(m);
				foundError = true;
			}
			if (foundError == false) {
				state = AddState.HAVE_FILE; // //4323 -- prior to this we were
											// always changing state and
											// advancing screen even if there
											// was no file etc...
			}
		}
	}

	public String addAction() {
		selectedBatch = new BatchMaintenance();
		// since this runs in a background thread, it has no access to a
		// FacesContext, so we need to get it now in foreground
		selectedBatch.setUpdateUserId(Auditable.currentUserCode());
		selectedBatch.setUpdateTimestamp(new Date());
		selectedBatch.setBatchTypeCode(SitusRulesBatch.BATCH_CODE);
		selectedBatch.setBatchStatusCode(SitusRulesBatch.IMPORTING);
		selectedBatch.setTotalBytes(parser.getTotalBytes());
		selectedBatch.setVc01(parser.getUploadFileName());
		selectedBatch.setVc02(parser.getFileType());
		selectedBatch.setNu01(new Double(parser.getReleaseVersion()));
		selectedBatch.setTs01(parser.getReleaseDate());
		selectedBatch.setEntryTimestamp(new Date());
		selectedBatch.setBatchId(null); // new record
		batchMaintenanceDataModel.getBatchMaintenanceDAO().save(selectedBatch);

		processUpdateInBackground(selectedBatch);
		selectedBatch = null;
		return null;
	}

	private void processUpdateInBackground(final BatchMaintenance batch) {
		String selectedDb = ChangeContextHolder.getDataBaseType();
		dbPropertiesDTO = dbPropertiesBean.getDbPropertiesDTO(selectedDb);

		state = AddState.PROCESSING;
		backgroundError = null;
		background = new Thread(new Runnable() {
			public void run() {
				System.out
						.println("############ processUpdateInBackground for Situs update using URL: "
								+ dbPropertiesDTO.getUrl());

				try {
					Thread.sleep(2000);
				} catch (Exception ex) {
				}

				try {
					Class.forName(dbPropertiesDTO.getDriverClassName());
				} catch (ClassNotFoundException e) {
					System.out.println("############ Can't load class: "
							+ dbPropertiesDTO.getDriverClassName());
					return;
				}
				Connection conn = null;

				try {
					// Standalone connection
					conn = DriverManager.getConnection(
							dbPropertiesDTO.getUrl(),
							dbPropertiesDTO.getUserName(),
							dbPropertiesDTO.getPassword());
					conn.setAutoCommit(false);

					// Import Situs update
					importSitusRuleUpdatesUsingJDBC(batch, parser, conn);

					// 4947
					if (parser.getBatchErrorLogs().size() > 0) {
						batch.setHeldFlag("1");
						batch.setErrorSevCode(BatchMaintenance.ErrorState.Error);
					}
					batch.setBatchStatusCode(SitusRulesBatch.IMPORTED);
	//AC				batch.setNu02(parser.getSpecialRateTotal().doubleValue());
					batch.setTotalRows(new Long(parser.getRows()));
					batch.setStatusUpdateTimestamp(new Date());
					batch.setStatusUpdateUserId(batch.getUpdateUserId());

					updateBatchUsingJDBC(batch, conn);
				} catch (Throwable e) {
					e.printStackTrace();
					logger.debug(e);
					backgroundError = ("Unable to save batch: " + e
							.getMessage());
				} finally {
					if (conn != null)
						try {
							conn.close();
						} catch (SQLException ignore) {

						}
				}

				// force completion
				state = AddState.COMPLETE;
				logger.debug("Background complete");
			}
		});
		background.start();
		logger.debug("Background started");
	}

	protected BatchMaintenance importSitusRuleUpdatesUsingJDBC(
			BatchMaintenance batchMaintenance, SitusRulesUploadParser parser,
			Connection conn) throws Exception {

		Iterator<BCPSitusRule> truIter = parser.iterator();

		String sql = null;
		String sqlText = null;
		PreparedStatement prep = null;
		PreparedStatement prepText = null;
		Statement stat = null;

		int i = 1;
		while (truIter.hasNext()) {
			BCPSitusRule tr = truIter.next();

			// 4947
			if (tr != null && tr.getLine() != null) {
				BCPSitusRuleText txt = new BCPSitusRuleText(
						batchMaintenance.getBatchId(), tr.getLine(),
						tr.getText());

				if (prep == null) {
					sql = tr.getRawInsertStatement();
					prep = conn.prepareStatement(sql);
					sqlText = txt.getRawInsertStatement();
					prepText = conn.prepareStatement(sqlText);
					stat = conn.createStatement();
				}

				tr.setBcpSitusRuleId(getNextBcpSitusRuleId(stat));
				tr.setBatchId(batchMaintenance.getbatchId());

				// populate data to Prepared Statement
				tr.populatePreparedStatement(conn, prep);
				prep.addBatch();

				txt.populatePreparedStatement(conn, prepText);
				prepText.addBatch();

				if ((i++ % 50) == 0) {
					logger.debug("Begin Flush: " + i);
					prep.executeBatch();
					prepText.executeBatch();
					conn.commit();
					prep.clearBatch();
					prepText.clearBatch();
					logger.debug("End Flush");
					parser.setLinesWritten(i);
				}
				tr = null;
				txt = null;
			}
		}

		if (prep != null) {
			prep.executeBatch();
			prepText.executeBatch();
			conn.commit();
			prep.clearBatch();
			prepText.clearBatch();

			try {
				prep.close();
			} catch (Exception ex) {
			}
			try {
				prepText.close();
			} catch (Exception ex) {
			}
		}

		// perform the checksum and generate any more ErrorLogs
		sql = null;
		prep = null;
		sqlText = null;
		prepText = null;

		parser.setCacheManager(cacheManager);
		parser.checksum();
		Statement statErrorNext = null;
		int ie = 1;
		for (BatchErrorLog error : parser.getBatchErrorLogs()) {
			if (prep == null) {
				sql = error.getRawInsertStatement();
				prep = conn.prepareStatement(sql);
				statErrorNext = conn.createStatement();
			}

			error.setBatchErrorLogId(getNextBatchErrorLogId(statErrorNext));
			error.setProcessType(SitusRulesBatch.IMPORT);
			error.setProcessTimestamp(batchMaintenance.getEntryTimestamp());
			error.setBatchId(batchMaintenance.getBatchId());
			error.populatePreparedStatement(conn, prep);
			prep.addBatch();

			if ((ie++ % 50) == 0) {
				prep.executeBatch();
				conn.commit();
				prep.clearBatch();
			}
		}

		if (prep != null) {
			prep.executeBatch();
			conn.commit();
			prep.clearBatch();

			try {
				prep.close();
			} catch (Exception ex) {
			}
		}

		if (stat != null) {
			try {
				stat.close();
			} catch (Exception ex) {
			}
		}

		if (statErrorNext != null) {
			try {
				statErrorNext.close();
			} catch (Exception ex) {
			}
		}

		parser.setLinesWritten(i);

		return batchMaintenance;
	}

	private long getNextBcpSitusRuleId(Statement stat) {
		long nextId = -1;
		try {
			String sql = DatabaseUtil.getSequenceQuery(
					DatabaseUtil.getDatabaseProductName(stat),
					"sq_tb_bcp_situs_rules_id", "NEXTID");

			ResultSet rs = stat.executeQuery(sql);
			if (rs.next()) {
				nextId = rs.getLong("NEXTID");
			}
			rs.close();
		} catch (Exception e) {
			logger.error("############ getNextBcpSitusRuleId() failed: "
					+ e.toString());
		}

		return nextId;
	}

	private long getNextBatchErrorLogId(Statement statErrorNext) {
		long nextId = -1;
		try {
			String sql = DatabaseUtil.getSequenceQuery(
					DatabaseUtil.getDatabaseProductName(statErrorNext),
					"sq_tb_batch_error_log_id", "NEXTID");

			// Statement stat = conn.createStatement();
			ResultSet rs = statErrorNext.executeQuery(sql);
			if (rs.next()) {
				nextId = rs.getLong("NEXTID");
			}
			rs.close();
		} catch (Exception e) {
			logger.error("############ getNextBatchErrorLogId() failed: "
					+ e.toString());
		} finally {
		}

		return nextId;
	}

	protected void updateBatchUsingJDBC(BatchMaintenance batchMaintenance,
			Connection conn) throws Exception {
		String sql = batchMaintenance.getRawUpdateStatement();
		PreparedStatement prep = conn.prepareStatement(sql);
		batchMaintenance.populateUpdatePreparedStatement(conn, prep);
		prep.addBatch();

		prep.executeBatch();
		conn.commit();
		prep.clearBatch();

		return;
	}

	public DbPropertiesBean getDbPropertiesBean() {
		return dbPropertiesBean;
	}

	public void setDbPropertiesBean(DbPropertiesBean dbPropertiesBean) {
		this.dbPropertiesBean = dbPropertiesBean;
	}

	public void selectedRowChanged(ActionEvent e)
			throws AbortProcessingException {
		UIComponent uiComponent = e.getComponent().getParent();
		if (!(uiComponent instanceof UIDataAdaptor)) {
			logger.info("Invalid class to event listener: "
					+ uiComponent.getClass().getName());
		}
		UIDataAdaptor table = (UIDataAdaptor) uiComponent;
		if (table != null) {
			this.selectedBatch = new SitusRulesBatch(
					(BatchMaintenance) table.getRowData());
			this.selectedBatchIndex = table.getRowIndex();

			BatchMetadata batchMetadata = batchMaintenanceService.findBatchMetadataById(selectedBatch.getBatchTypeCode());
			batchMetadataList = new BatchMetadataList(batchMetadata, selectedBatch);
		}
	}
	
	public String submitProcessAction() {
	  	for (SitusRulesBatch batch : selectedBatches) {
	  		if (batch.getError() == null && selectedBatches.size()==1) {
	  			BatchMaintenance bm = batchMaintenanceDataModel.getById(batch.getBatchId());
	  			bm.setBatchStatusCode(SitusRulesBatch.PROCESS);
	  			bm.setSchedStartTimestamp((Date)batchStartTime.getValue());
	  			batchMaintenanceService.update(bm);
	  			batchMaintenanceDataModel.refreshData(false);
	  		}
	  		  		
	  		if( selectedBatches.size()>1 ){
	  			if(batch.getError()!=null){
	  				selectedBatches.remove(batch.getBatchId());
	  				logger.info("!!!!!Batch id Ommited !!!!!!! "+batch.getBatchId());
	  				continue;
	  			}
	  			BatchMaintenance bm = batchMaintenanceDataModel.getById(batch.getBatchId());
	  			logger.info("Batch id Updated "+batch.getBatchId());
	  			bm.setBatchStatusCode(SitusRulesBatch.PROCESS);
	  			bm.setSchedStartTimestamp((Date)batchStartTime.getValue());
	  			batchMaintenanceService.update(bm);
	  		}
	  	}
	  	batchMaintenanceDataModel.refreshData(false);
	  	selectedBatchIndex = -1;
		return "situs_rules";
	  }

	public HtmlCalendar getBatchStartTime() {
		batchStartTime.resetValue();
	    batchStartTime.setValue(filePreferenceBean.getBatchPreferenceDTO().getDefaultBatchStartTime());
		return batchStartTime;
	}

	public void setBatchStartTime(HtmlCalendar batchStartTime) {
		this.batchStartTime = batchStartTime;
	}

	public List<SitusRulesBatch> getSelectedBatches() {
		return selectedBatches;
	}

	public BatchMetadataList getBatchMetadataList() {
		return batchMetadataList;
	}

	public void setBatchMetadataList(BatchMetadataList batchMetadataList) {
		this.batchMetadataList = batchMetadataList;
	}

	public String updateUserFieldsAction() {
		return "situs_rules_user_fields_update";
	}

	public String updateUserFields() {
		batchMaintenanceService.update(batchMetadataList.update());
		return "situs_rules";
	}

	public String cancelUpdateUserFields() {
		return "situs_rules";
	}

	public boolean isNavigatedFlag() {
		return true;
	}

	public void setNavigatedFlag(boolean navigatedFlag) {}
	
	public User getCurrentUser() {
		return loginBean.getUser();
   }

  	public void retrieveBatchMaintenance() { 
  		BatchMaintenance batchMaintenance = new BatchMaintenance();
		batchMaintenance.setBatchTypeCode(exampleBatchMaintenance.getBatchTypeCode());
  		this.batchMaintenanceDataModel =  batchMaintenanceBean.prepareBatchMaintenance(batchMaintenance);
  	}
  	
  	public String resetBatchFields(){
  		batchMaintenanceBean.setResetBatchAccordion(false);
  		batchMaintenanceBean.resetOtherBatchFields();
  		return null;
  	}
  	
  	public String navigateAction() {
  		init();
  		return "situs_rules";
  	}

}
