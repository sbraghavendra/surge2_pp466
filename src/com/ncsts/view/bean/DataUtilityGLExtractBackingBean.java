package com.ncsts.view.bean;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.html.HtmlSelectBooleanCheckbox;
import javax.faces.context.FacesContext;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.ajax4jsf.component.UIDataAdaptor;
import org.ajax4jsf.component.html.HtmlAjaxSupport;
import org.apache.log4j.Logger;
import org.richfaces.component.html.HtmlCalendar;
import org.richfaces.component.html.HtmlDataTable;

import com.ncsts.common.LogMemory;
import com.ncsts.domain.BatchMaintenance;
import com.ncsts.domain.BatchMetadata;
import com.ncsts.domain.FisYear;
import com.ncsts.domain.Option;
import com.ncsts.domain.OptionCodePK;
import com.ncsts.domain.PurchaseTransaction;
import com.ncsts.domain.PurchaseTransactionLog;
import com.ncsts.domain.User;
import com.ncsts.dto.BatchMetadataList;
import com.ncsts.jsf.model.BatchMaintenanceDataModel;
import com.ncsts.jsf.model.GLExtractDataModel;
import com.ncsts.jsf.model.GlExtractAetnaDataModel;
import com.ncsts.services.BatchMaintenanceService;
import com.ncsts.services.CallGLExtractService;
import com.ncsts.services.FisYearService;
import com.ncsts.services.OptionService;
import com.ncsts.services.PreferenceService;
import com.ncsts.services.TransactionDetailService;


public class DataUtilityGLExtractBackingBean {
    @SuppressWarnings("unused")
    private Logger logger = Logger.getLogger(DataUtilityGLExtractBackingBean.class);

    private BatchMaintenanceService batchMaintenanceService;
    private BatchMaintenance batchMaintenance;

    private BatchErrorsBackingBean batchErrorsBean;

    private GLExtractDataModel glExtractDataModel;
    

    private CallGLExtractService callGLExtractService;

    private PreferenceService preferenceService;
    private TransactionDetailService transactionDetailService;
    private OptionService optionService;

    private FisYearService fisYearService;
    private MatrixCommonBean matrixCommonBean;
    private BatchMaintenanceBackingBean batchMaintenanceBean;

    private GlExtractAetnaDataModel glExtractAetnaDataModel;
    private List<SelectItem> filterYearPeriodMenuItems;
    private String filterYear = " ";
    private HtmlCalendar myCalendarDate = new HtmlCalendar();
    
    private Date myCalendar;   
    private HtmlCalendar glmarkCalendarDate = new HtmlCalendar();
    private EditAction currentAction = EditAction.VIEW;
    private String fileName;
    private boolean displayAllFields = false;
    private BatchMaintenance selectedBatch = null;

    private int selectedBatchIndex = -1;
    private BigDecimal transactionCount = null;
    private BigDecimal recordCount = null;
    private BigDecimal taxAmount = null;

    private BatchMaintenanceDataModel batchMaintenanceDataModel;
    private int selectedRowTrIndex = -1;
    private List<PurchaseTransaction> currentTransactionList;
    private BatchMetadata batchMetadata;
    private HtmlDataTable trDetailTable;
    private PurchaseTransactionLog selectedTransaction;
    private boolean searched = false;

    private boolean extractedAction = false;
    private String extractedActionMessage = "";

    private BatchMetadataList batchMetadataList;
    
    public static final String GE_BATCH_TYPE_CODE = "GE";
    
  

    public Date getmyCalendar() {
		return myCalendar;
	}

	public void setmyCalendar(Date myCalendar) {
		this.myCalendar = myCalendar;
	}

	public void selectedRowChanged(ActionEvent e) throws AbortProcessingException {
        UIComponent uiComponent = e.getComponent().getParent();
        if (!(uiComponent instanceof UIDataAdaptor)) {
            logger.info("Invalid class to event listener: " + uiComponent.getClass().getName());
            return;
        }
        UIDataAdaptor table = (UIDataAdaptor) uiComponent;
        if (table != null) {
            this.selectedBatch = (BatchMaintenance) table.getRowData();
            this.selectedBatchIndex = table.getRowIndex();

            BatchMetadata batchMetadata = batchMaintenanceService.findBatchMetadataById(selectedBatch.getBatchTypeCode());
            batchMetadataList = new BatchMetadataList(batchMetadata, selectedBatch);
        }
    }

    public List<SelectItem> getFilterYearPeriodMenuItems() {
        if (filterYearPeriodMenuItems == null) {
            filterYearPeriodMenuItems = new ArrayList<SelectItem>();
            filterYearPeriodMenuItems.add(new SelectItem("", "Select a Fiscal Period"));
            List<FisYear> actualRowsList = fisYearService.getAllPeriod();
            for (FisYear fiscalPeriod : actualRowsList) {
                filterYearPeriodMenuItems.add(new SelectItem(fiscalPeriod.getYearPeriod(), fiscalPeriod.getDescription()));
            }
        }

        return filterYearPeriodMenuItems;
    }

    public void sortAction(ActionEvent e) {
        batchMaintenanceDataModel.toggleSortOrder(e.getComponent().getParent().getId());
    }
    
    public void sortGlEtractAction(ActionEvent e) {
    	glExtractDataModel.toggleSortOrder(e.getComponent().getParent().getId());
    }
    
    public void setSelectedRowTrIndex(int selectedRowTrIndex) {
		this.selectedRowTrIndex = selectedRowTrIndex;
	}
    
    public BatchMetadata getBatchMetadataAsIs() {
        return batchMetadata;
    }

    public void selectedTransactionChanged(ActionEvent e) throws AbortProcessingException {

        HtmlDataTable table = (HtmlDataTable) (((HtmlAjaxSupport) e.getComponent()).getParent());
        selectedTransaction = (PurchaseTransactionLog) table.getRowData();
        selectedRowTrIndex = table.getRowIndex();


    }

    public String errorsAction() {
        batchErrorsBean.setBatchMaintenance(selectedBatch);
        batchErrorsBean.setReturnView("glExtract_main");

        //0001701: Import errors make the system crash
        batchErrorsBean.getBatchErrorDataModel().setBatchMaintenance(selectedBatch);

        return "batch_maintenance_errors";
    }

    public String errorsMarkTransAction() {
        batchErrorsBean.setBatchMaintenance(selectedBatch);
        batchErrorsBean.setReturnView("glmarktrans_main");

        //0001701: Import errors make the system crash
        batchErrorsBean.getBatchErrorDataModel().setBatchMaintenance(selectedBatch);

        return "batch_maintenance_errors";
    }

    public String getActionText() {
        if (currentAction.equals(EditAction.ADD)) {
            return "Add";
        } else if (currentAction.equals(EditAction.UPDATE)) {
            return "Update";
        } else if (currentAction.equals(EditAction.DELETE)) {
            return "Delete";
        } else if (currentAction.equals(EditAction.VIEW)) {
            return "View";

        } else if (currentAction.equals(EditAction.OPEN)) {
            return "Open";

        } else if (currentAction.equals(EditAction.CLOSE)) {
            return "Close";
        }

        return "";
    }

    public String addAction() {
        glExtractAetnaDataModel.setEnabled(false);
        extractedActionMessage = "";
        extractedAction = false;
        fileName = createFilename();
        return "glextract_add";
    }

    public String addMarkTransAction() {
        glExtractAetnaDataModel.setEnabled(false);
        extractedActionMessage = "";
        extractedAction = false;
        fileName = createFilename();

        //Reset data
        resetFilterGLExtract();

        return "glmarktrans_add";
    }

    public boolean getExtractedAction() {
        return extractedAction;
    }

    public String getExtractedActionMessage() {
        return extractedActionMessage;
    }

    public void setExtractedActionMessage(String extractedActionMessage) {
        this.extractedActionMessage = extractedActionMessage;
    }

    public void setExtractedAction(boolean extractedAction) {
        this.extractedAction = extractedAction;
    }

    /* --- Action handlers for GL Extract -- */
    public String extractAction() {
        Date d = (Date) myCalendarDate.getValue();
        DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
        String glDate = df.format(d);

        if (fileName == null || fileName.trim().length() == 0) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO, "Extract Filename cannot be empty.", null));
            return null;
        }

        String returnCode = callGLExtractService.callGlExtractProc(glDate, fileName, 1, 0);
        glExtractAetnaDataModel.setEnabled(true);
        if (returnCode == null || returnCode.length() == 0) {
            extractedActionMessage = "The extract procedure completed successfully.";
            extractedAction = true;
        } else {
            extractedActionMessage = "The extract procedure failed: " + returnCode;
            extractedAction = false;
        }

        return "glextract_add"; 
    }

    public String saveAction() {
        Date d = (Date) myCalendarDate.getValue();
        DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
        String glDate = df.format(d);
        callGLExtractService.callGlExtractProc(glDate, fileName, 1, 0);
        FacesContext.getCurrentInstance().addMessage(null,
                new FacesMessage(FacesMessage.SEVERITY_INFO, fileName + " successfully extracted", null));
        return "glExtract_main";
    }

    public String closeAction() {
        extractedActionMessage = "";
        extractedAction = false;
        fileName = "";
        return "glExtract_main";
    }

    public String closeMarkTransAction() {
        extractedActionMessage = "";
        extractedAction = false;
        fileName = "";
        return "glmarktrans_main";
    }

    /**
     * @return default file name This name has to be created from the preferences,
     * can be replaced with the preferences gl extract file name.
     */
    private String createFilename() {
        Option option = optionService.findByPK(new OptionCodePK("GLEXTRACTPREFIX", "SYSTEM", "SYSTEM"));
        String preFix = (option == null) ? "" : option.getValue();

        option = optionService.findByPK(new OptionCodePK("GLEXTRACTEXT", "SYSTEM", "SYSTEM"));
        String extensionTxt = (option == null) ? "" : option.getValue();

        if (preFix == null || preFix.length() == 0) {
            preFix = "GL_";
        }

        if (extensionTxt == null || extensionTxt.length() == 0) {
            extensionTxt = ".txt";
        }

        Date d = new Date();
        DateFormat dateFormat = new SimpleDateFormat("MMddyyyy");
        String fileString = preFix + dateFormat.format(d) + "." + extensionTxt;

        //String fileString = "SD_" + dateFormat.format(d) + "-BU#.txt"; // why this
        // rule is
        // hard
        // coded..this
        // rule
        // should be
        // extracted
        // from
        // preferences.
        return fileString;
    }

    public String refreshAction() {
        init();
        batchMaintenanceDataModel.refreshData(false);
        selectedBatchIndex = -1;
        return null;
    }

    private void init() {
        batchMaintenanceDataModel.setDefaultSortOrder("batchId", BatchMaintenanceDataModel.SORT_DESCENDING);
        batchMaintenance = new BatchMaintenance();
        batchMaintenanceBean.resetSearchAction(); 
        batchMaintenance.setEntryTimestamp(null);
        batchMaintenance.setBatchStatusCode(null);
        batchMaintenance.setBatchTypeCode(GE_BATCH_TYPE_CODE);
        batchMaintenanceBean.setBatchTypeCode(GE_BATCH_TYPE_CODE) ; 
    	batchMaintenanceBean.setCalledFromConfigImportExpMenu(false);
        batchMaintenanceBean.prepareBatchTypeSpecificsFields();
        batchMaintenanceDataModel.setPageSize(50);
        batchMaintenanceDataModel.setCriteria(batchMaintenance);
        selectedRowTrIndex = -1;
        selectedTransaction = null;
        transactionCount = null;
        recordCount = null;
        taxAmount = null;
        resetDetailTable();
        
    }


    public BatchMetadata getBatchMetadata() {
        if (batchMetadata == null) {
            batchMetadata = batchMaintenanceService.findBatchMetadataById("GE");
        }
        return batchMetadata;
    }

    /**
     * @return the glBatchMaintenanceDataModel
     */
    public BatchMaintenanceDataModel getBatchMaintenanceDataModel() {
        return this.batchMaintenanceDataModel;
    }

    public Boolean getDisplayError() {
        return (selectedBatchIndex != -1) && (selectedBatch.getErrorSevCode() != null) &&
                (selectedBatch.getErrorSevCode().trim().length() > 0);
    }

    public String getFileName() {
        return fileName;
    }

    /**
     * @return the batchMaintenanceService
     */
    public BatchMaintenanceService getBatchMaintenanceService() {
        return this.batchMaintenanceService;
    }

    /**
     * @param batchMaintenanceService the batchMaintenanceService to set
     */
    public void setBatchMaintenanceService(BatchMaintenanceService batchMaintenanceService) {
        this.batchMaintenanceService = batchMaintenanceService;
    }

    /**
     * @return the batchMaintenance
     */
    public BatchMaintenance getBatchMaintenance() {
        return this.batchMaintenance;
    }

    /**
     * @param batchMaintenance the batchMaintenance to set
     */
    public void setBatchMaintenance(BatchMaintenance batchMaintenance) {
        this.batchMaintenance = batchMaintenance;
    }

    /**
     * @return the batchErrorsBean
     */
    public BatchErrorsBackingBean getBatchErrorsBean() {
        return this.batchErrorsBean;
    }

    /**
     * @param batchErrorsBean the batchErrorsBean to set
     */
    public void setBatchErrorsBean(BatchErrorsBackingBean batchErrorsBean) {
        this.batchErrorsBean = batchErrorsBean;
    }

    /**
     * @return the callGLExtractService
     */
    public CallGLExtractService getCallGLExtractService() {
        return this.callGLExtractService;
    }

    /**
     * @param callGLExtractService the callGLExtractService to set
     */
    public void setCallGLExtractService(CallGLExtractService callGLExtractService) {
        this.callGLExtractService = callGLExtractService;
    }

    /**
     * @return the batchMaintenanceBean
     */
    public BatchMaintenanceBackingBean getBatchMaintenanceBean() {
        return this.batchMaintenanceBean;
    }

    /**
     * @param batchMaintenanceBean the batchMaintenanceBean to set
     */
    public void setBatchMaintenanceBean(BatchMaintenanceBackingBean batchMaintenanceBean) {
        this.batchMaintenanceBean = batchMaintenanceBean;
    }

    /**
     * @return the glExtractAetnaDataModel
     */
    public GlExtractAetnaDataModel getGlExtractAetnaDataModel() {
        return this.glExtractAetnaDataModel;
    }

    /**
     * @param glExtractAetnaDataModel the glExtractAetnaDataModel to set
     */
    public void setGlExtractAetnaDataModel(GlExtractAetnaDataModel glExtractAetnaDataModel) {
        this.glExtractAetnaDataModel = glExtractAetnaDataModel;
    }
    
    public HtmlCalendar getGlmarkCalendarDate() {
		return glmarkCalendarDate;
	}

	public void setGlmarkCalendarDate(HtmlCalendar glmarkCalendarDate) {
		this.glmarkCalendarDate = glmarkCalendarDate;
	}

	/**
     * @return the myCalendarDate
     */
    public HtmlCalendar getMyCalendarDate() {
        DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
        HtmlCalendar myCalendarDate = new HtmlCalendar();
        // myCalendarDate.setCurrentDate(dateFormat.format(new Date()));
        myCalendarDate.setValue(dateFormat.format(new com.ncsts.view.util.DateConverter().getUserLocalDate()));
        return myCalendarDate;
    }

    /**
     * @param myCalendarDate the myCalendarDate to set
     */
    public void setMyCalendarDate(HtmlCalendar calendarDate) {
        this.myCalendarDate = calendarDate;
        this.glmarkCalendarDate = calendarDate;
    }

    /**
     * @return the selectedBatch
     */
    public BatchMaintenance getSelectedBatch() {
        return this.selectedBatch;
    }

    /**
     * @param selectedBatch the selectedBatch to set
     */
    public void setSelectedBatch(BatchMaintenance selectedBatch) {
        this.selectedBatch = selectedBatch;
    }

    /**
     * @return the selectedBatchIndex
     */
    public int getSelectedBatchIndex() {
        return this.selectedBatchIndex;
    }

    /**
     * @param selectedBatchIndex the selectedBatchIndex to set
     */
    public void setSelectedBatchIndex(int selectedBatchIndex) {
        this.selectedBatchIndex = selectedBatchIndex;
    }

    /**
     * @param fileName the fileName to set
     */
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    /**
     * @param batchMaintenanceDataModel the batchMaintenanceDataModel to set
     */
    public void setBatchMaintenanceDataModel(BatchMaintenanceDataModel batchMaintenanceDataModel) {
        this.batchMaintenanceDataModel = batchMaintenanceDataModel;
    }

    /**
     * @param batchMetadata the batchMetadata to set
     */
    public void setBatchMetadata(BatchMetadata batchMetadata) {
        this.batchMetadata = batchMetadata;
    }

    /**
     * @return the preferenceService
     */
    public PreferenceService getPreferenceService() {
        return this.preferenceService;
    }

    /**
     * @param preferenceService the preferenceService to set
     */
    public void setPreferenceService(PreferenceService preferenceService) {
        this.preferenceService = preferenceService;
    }

    public FisYearService getFisYearService() {
        return fisYearService;
    }

    public void setFisYearService(FisYearService fisYearService) {
        this.fisYearService = fisYearService;
    }

    public OptionService getOptionService() {
        return optionService;
    }

    public void setOptionService(OptionService optionService) {
        this.optionService = optionService;
    }

    public Boolean getDisplayMarkAction() {
        return (glExtractDataModel.count() > 0L);
    }

    public boolean getSearched() {
        return searched;
    }

    public void setSearched(boolean searched) {
        this.searched = searched;
    }

    public void resetFilterGLExtract() {
        //Reset data
    	setmyCalendar(new com.ncsts.view.util.DateConverter().getUserLocalDate());
    	setFilterYear("");
        transactionCount = null;
        recordCount = null;
        taxAmount = null;
        if (glExtractDataModel != null) {
            glExtractDataModel.resetFilterCriteria();
        }
        searched = false;
    }

    public void retrieveFilterGLExtract() {
        selectedRowTrIndex = -1;
        selectedTransaction = null;

        String glDate = null;
        DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
        Date d = (Date) myCalendarDate.getValue();
        glDate = df.format(d);
        if (glDate != null) {
            transactionCount = callGLExtractService.getTransactionCount(glDate);
            recordCount = callGLExtractService.getRecordCount(glDate);
            taxAmount = callGLExtractService.getTaxAmountCount(glDate);
            glExtractDataModel.setFilterCriteria(new PurchaseTransactionLog(), glDate, new ArrayList<String>());
        }

        searched = true;
    }

    public String markAction() {
        String glDate = null;
        DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
        Date d = (Date) myCalendarDate.getValue();
        glDate = df.format(d);
        BatchMaintenance batchMaintenance = new BatchMaintenance();
        batchMaintenance.setYearPeriod(getFilterYear());
        this.batchMaintenanceService.createNewBatchGL(batchMaintenance, glDate);

        refreshAction();
        return "glmarktrans_main";
    }

    public String getFilterYear() {
        return filterYear;
    }

    public void setFilterYear(String filterYear) {
        this.filterYear = filterYear;
    }

    public HtmlDataTable getTrDetailTable() {
        if (trDetailTable == null) {
            trDetailTable = new HtmlDataTable();

            trDetailTable.setId("aMDetailList");
            trDetailTable.setVar("trdetail");
            trDetailTable.setRowClasses("odd-row,even-row");
            //matrixCommonBean.createTransactionStatisticsTable(trDetailTable,"dataUtilityGLExtractBackingBean", "dataUtilityGLExtractBackingBean.glExtractDataModel",
            //matrixCommonBean.getTransactionPropertyMap(),true);

            matrixCommonBean.createTransactionDetailLogTable(trDetailTable, "dataUtilityGLExtractBackingBean", "dataUtilityGLExtractBackingBean.glExtractDataModel",
                    matrixCommonBean.getTransactionLogPropertyMap(), false, displayAllFields, false, false, false);


            String rowStr = (displayAllFields) ? matrixCommonBean.getMatrixDTO().getSysFullThreshold() :
                    matrixCommonBean.getMatrixDTO().getSysLimitedThreshold();
            int rowpage = 0;
            if (rowStr == null || rowStr.length() == 0) {
                rowpage = (displayAllFields) ? 25 : 100;
            } else {
                rowpage = Integer.parseInt(rowStr);
            }

            String muitipleStr = (displayAllFields) ? matrixCommonBean.getMatrixDTO().getSysFullThresholdPages() :
                    matrixCommonBean.getMatrixDTO().getSysLimitedThresholdPages();
            int muitiplepage = 0;
            if (muitipleStr == null || muitipleStr.length() == 0) {
                muitiplepage = (displayAllFields) ? 4 : 4;
            } else {
                muitiplepage = Integer.parseInt(muitipleStr);
            }

            glExtractDataModel.setPageSize(rowpage);
            glExtractDataModel.setDbFetchMultiple(muitiplepage);
        }
        return trDetailTable;

    }

    public void resetDetailTable() {
        String rowStr = (displayAllFields) ? matrixCommonBean.getMatrixDTO().getSysFullThreshold() :
                matrixCommonBean.getMatrixDTO().getSysLimitedThreshold();
        int rowpage = 0;
        if (rowStr == null || rowStr.length() == 0) {
            rowpage = (displayAllFields) ? 25 : 100;
        } else {
            rowpage = Integer.parseInt(rowStr);
        }

        String muitipleStr = (displayAllFields) ? matrixCommonBean.getMatrixDTO().getSysFullThresholdPages() :
                matrixCommonBean.getMatrixDTO().getSysLimitedThresholdPages();
        int muitiplepage = 0;
        if (muitipleStr == null || muitipleStr.length() == 0) {
            muitiplepage = (displayAllFields) ? 4 : 4;
        } else {
            muitiplepage = Integer.parseInt(muitipleStr);
        }

        if (rowpage == glExtractDataModel.getPageSize() && muitiplepage == glExtractDataModel.getDbFetchMultiple()) {
            return;
        }

        // Rebuild the table
        logger.debug("Rebuilding table - resetDetailTable: " + displayAllFields);

        glExtractDataModel.setPageSize(rowpage);
        glExtractDataModel.setDbFetchMultiple(muitiplepage);

        resetForHeaderRearrangeTemp();

        LogMemory.executeGC();
    }


    public void displayChange(ActionEvent e) {
        HtmlSelectBooleanCheckbox check = (HtmlSelectBooleanCheckbox) ((HtmlAjaxSupport) e.getComponent()).getParent();
        displayAllFields = (check.getSubmittedValue() == null) ?
                ((check.getValue() == null) ? false : (Boolean) check.getValue()) : Boolean.parseBoolean((String) check.getSubmittedValue());
        // Rebuild the table
        logger.debug("Rebuilding table - all fields: " + displayAllFields);
        matrixCommonBean.createTransactionDetailTable(trDetailTable,
                "dataUtilityGLExtractBackingBean", "dataUtilityGLExtractBackingBean.glExtractDataModel",
                matrixCommonBean.getTransactionPropertyMap(), true, displayAllFields, false, false, true);

        String rowStr = (displayAllFields) ? matrixCommonBean.getMatrixDTO().getSysFullThreshold() :
                matrixCommonBean.getMatrixDTO().getSysLimitedThreshold();
        int rowpage = 0;
        if (rowStr == null || rowStr.length() == 0) {
            rowpage = (displayAllFields) ? 25 : 100;
        } else {
            rowpage = Integer.parseInt(rowStr);
        }

        String muitipleStr = (displayAllFields) ? matrixCommonBean.getMatrixDTO().getSysFullThresholdPages() :
                matrixCommonBean.getMatrixDTO().getSysLimitedThresholdPages();
        int muitiplepage = 0;
        if (muitipleStr == null || muitipleStr.length() == 0) {
            muitiplepage = (displayAllFields) ? 4 : 4;
        } else {
            muitiplepage = Integer.parseInt(muitipleStr);
        }

        glExtractDataModel.setPageSize(rowpage);
        glExtractDataModel.setDbFetchMultiple(muitiplepage);

        resetForHeaderRearrangeTemp();

        LogMemory.executeGC();
    }

    public void resetForHeaderRearrangeTemp() {
        matrixCommonBean.setForHeaderRearrangeTemp(null);
    }

    public MatrixCommonBean getMatrixCommonBean() {
        return matrixCommonBean;
    }

    public BigDecimal getTransactionCount() {
        return transactionCount;
    }

    public void setTransactionCount(BigDecimal transactionCount) {
        this.transactionCount = transactionCount;
    }

    public BigDecimal getRecordCount() {
        return recordCount;
    }

    public void setRecordCount(BigDecimal recordCount) {
        this.recordCount = recordCount;
    }

    public BigDecimal getTaxAmount() {
        return taxAmount;
    }

    public void setTaxAmount(BigDecimal taxAmount) {
        this.taxAmount = taxAmount;
    }

    public void setMatrixCommonBean(MatrixCommonBean matrixCommonBean) {
        this.matrixCommonBean = matrixCommonBean;
    }

    public boolean isDisplayAllFields() {
        return displayAllFields;
    }

    public void setDisplayAllFields(boolean displayAllFields) {
        this.displayAllFields = displayAllFields;
    }

    public void setTrDetailTable(HtmlDataTable trDetailTable) {
        this.trDetailTable = trDetailTable;
    }

    public TransactionDetailService getTransactionDetailService() {
        return transactionDetailService;
    }

    public void setTransactionDetailService(
            TransactionDetailService transactionDetailService) {
        this.transactionDetailService = transactionDetailService;
    }

    public List<PurchaseTransaction> getCurrentTransactionList() {
        return currentTransactionList;
    }

    public void setCurrentTransactionList(
            List<PurchaseTransaction> currentTransactionList) {
        this.currentTransactionList = currentTransactionList;
    }

    public String cancelAction() {
        return "glExtract_main";
    }

    public GLExtractDataModel getGlExtractDataModel() {
		return glExtractDataModel;
	}

	public void setGlExtractDataModel(GLExtractDataModel glExtractDataModel) {
		this.glExtractDataModel = glExtractDataModel;
	}

	public String cancelMarkTransAction() {
        return "glmarktrans_main";
    }

    public Boolean getDisplayMarkButtons() {
        return (selectedRowTrIndex != -1);
    }

    public PurchaseTransactionLog getSelectedTransaction() {
        return selectedTransaction;
    }

    public void setSelectedTransaction(PurchaseTransactionLog selectedTransaction) {
        this.selectedTransaction = selectedTransaction;
    }

    public String updateUserFieldsAction() {
        return "data_utility_gl_extract_update_user_fields";
    }

    public String updateUserFields() {
        batchMaintenanceService.update(batchMetadataList.update());
        return "data_utility_gl_extract";
    }

    public String cancelUpdateUserFields() {
        return "data_utility_gl_extract";
    }

    public BatchMetadataList getBatchMetadataList() {
        return batchMetadataList;
    }

    public void setBatchMetadataList(BatchMetadataList batchMetadataList) {
        this.batchMetadataList = batchMetadataList;
    }

    public boolean isNavigatedFlag() {
        return true;
    }

    public void setNavigatedFlag(boolean navigatedFlag) {}
    
   	public User getCurrentUser() {
		return matrixCommonBean.getLoginBean().getUser();
   }

	public void retrieveBatchMaintenance() {
		selectedBatchIndex = -1;
		BatchMaintenance batchMaintenance = new BatchMaintenance();
		batchMaintenance.setBatchTypeCode(GE_BATCH_TYPE_CODE);
		this.batchMaintenanceDataModel =  batchMaintenanceBean.prepareBatchMaintenance(batchMaintenance);
	}
	
	public String resetBatchFields(){
		batchMaintenanceBean.setResetBatchAccordion(false);
		batchMaintenanceBean.resetOtherBatchFields();
		return null;
	}
	
	public String navigateAction() {
		init();
		return "data_utility_gl_extract";
	}
}