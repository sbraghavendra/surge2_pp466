package com.ncsts.view.bean;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;
import java.util.Set;

import javax.faces.component.UIComponent;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlPanelGrid;
import javax.faces.component.html.HtmlPanelGroup;
import javax.faces.component.html.HtmlSelectBooleanCheckbox;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.MethodExpressionActionListener;
import javax.faces.model.SelectItem;
import javax.persistence.Column;
import javax.servlet.ServletException;
import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;

import org.ajax4jsf.component.html.HtmlAjaxCommandButton;
import org.ajax4jsf.component.html.HtmlAjaxCommandLink;
import org.ajax4jsf.component.html.HtmlAjaxSupport;
import org.apache.log4j.Logger;
import org.apache.myfaces.component.html.ext.HtmlOutputText;
import org.richfaces.component.html.HtmlColumn;
import org.richfaces.component.html.HtmlDataTable;
import org.richfaces.component.html.HtmlSpacer;
import org.springframework.beans.factory.annotation.Autowired;

import com.ncsts.common.CacheManager;
import com.ncsts.common.LoggerFactory;
import com.ncsts.common.Util;
import com.ncsts.domain.AllocationMatrix;
import com.ncsts.domain.BCPBillTransaction;
import com.ncsts.domain.BCPPurchaseTransaction;
import com.ncsts.domain.BillTransaction;
import com.ncsts.domain.CCHCode;
import com.ncsts.domain.CCHTaxMatrix;
import com.ncsts.domain.DataDefinitionColumn;
import com.ncsts.domain.DriverNames;
import com.ncsts.domain.EntityItem;
import com.ncsts.domain.GSBMatrix;
import com.ncsts.domain.GlExtractMap;
import com.ncsts.domain.Jurisdiction;
import com.ncsts.domain.ListCodes;
import com.ncsts.domain.LocationMatrix;
import com.ncsts.domain.Matrix;
import com.ncsts.domain.PurchaseTransaction;
import com.ncsts.domain.PurchaseTransactionLog;
import com.ncsts.domain.TaxAllocationMatrix;
import com.ncsts.domain.TaxAllocationMatrixDetail;
import com.ncsts.domain.TaxMatrix;
import com.ncsts.domain.TransactionHeader;
import com.ncsts.dto.EntityItemDTO;
import com.ncsts.dto.MatrixDTO;
import com.ncsts.dto.TaxCodeStateDTO;
import com.ncsts.dto.UserPreferenceDTO;
import com.ncsts.services.DriverReferenceService;
import com.ncsts.services.DwColumnService;
import com.ncsts.services.EntityItemService;
import com.ncsts.services.JurisdictionService;
import com.ncsts.services.TaxCodeDetailService;
import com.ncsts.services.TransactionDetailService;
import com.ncsts.services.UserEntityService;
import com.ncsts.view.util.DateConverter;
import com.ncsts.view.util.DateTimeConverter;
import com.ncsts.view.util.JsfHelper;
import com.ncsts.view.util.LinkConverter;
import com.ncsts.view.util.MaxAmountConverter;
import com.ncsts.view.util.NumberConverter;
import com.ncsts.view.util.PercentageConverter;
import com.ncsts.view.util.StateObservable;


public class MatrixCommonBean implements Observer, HttpSessionBindingListener{

	static private Logger logger = LoggerFactory.getInstance().getLogger(MatrixCommonBean.class);
	
	@Autowired
	private EntityItemService entityItemService;
	
	@Autowired
	private DwColumnService dwColumnService;
	
	private StateObservable ov = null;
	private CacheManager cacheManager;
	private TaxCodeDetailService taxCodeDetailService;
	private DriverReferenceService driverReferenceService;
	private FilePreferenceBackingBean filePreferenceBean;
	private JurisdictionService jurisdictionService;
	
	private List<SelectItem> activeStateItems;
	private List<SelectItem> allStateItems;
	private List<SelectItem> zipItems;
	private List<SelectItem> relationItems;
	private List<SelectItem> taxCodeStateItems;
	private List<SelectItem> taxCodeTypeItems;
	private List<SelectItem> cchTaxCatItems;
	private List<SelectItem> cchTaxGrpItems;
	private List<SelectItem> multiTransCodeItems;
	private List<SelectItem> allEntityItems;
	
	private Map<String,String> taxMatrixColumnMap;
	private Map<String,String> gsbMatrixColumnMap;
	private Map<String,String> locationMatrixColumnMap;
	private Map<String,String> allocationMatrixColumnMap;
	private Map<String,String> glExtractMap;
	private Map<String,String> salesColumnNameMap;
	private Map<String,String> transactionColumnNameMap;
	
	private HtmlPanelGrid minimumTaxDriversPanel;
	private HtmlPanelGrid minimumTaxDriversPanelgsb;
	private Map<String,Boolean> minimumTaxDriversMap;
	private Map<String,Boolean> minimumTaxDriversMapGSB;
	// Default Line options.
	private List<SelectItem> defaultLineItems;
	public static final String DEFAULT_LINE_ALL_VALUE = "2";
	private static final String DEFAULT_LINE_ALL_LABEL = "ALL matrix";
	public static final String DEFAULT_LINE_GLOBAL_VALUE = "1";
	private static final String DEFAULT_LINE_GLOBAL_LABEL = "DEFAULT matrix";
	public static final String DEFAULT_LINE_NON_GLOBAL_VALUE = "0";
	private static final String DEFAULT_LINE_NON_GLOBAL_LABEL = "NON-DEFAULT matrix";
	public static final String DEFAULT_LINE_DEFAULT_VALUE = DEFAULT_LINE_ALL_VALUE;
	
	// Global Company Number options.
	private List<SelectItem> globalCompanyNumberItems;
	public static final String GLOBAL_COMPANY_NUMBER_ALL_VALUE = "2";
	private static final String GLOBAL_COMPANY_NUMBER_ALL_LABEL = "ALL matrix";
	public static final String GLOBAL_COMPANY_NUMBER_GLOBAL_VALUE = "1";
	private static final String GLOBAL_COMPANY_NUMBER_GLOBAL_LABEL = "GLOBAL matrix";
	public static final String GLOBAL_COMPANY_NUMBER_NON_GLOBAL_VALUE = "0";
	private static final String GLOBAL_COMPANY_NUMBER_NON_GLOBAL_LABEL = "NON-GLOBAL matrix";
	public static final String GLOBAL_COMPANY_NUMBER_DEFAULT_VALUE = GLOBAL_COMPANY_NUMBER_ALL_VALUE;
	
	//Active options.
	private List<SelectItem> activeItems;
	public static final String ACTIVE_ALL_VALUE = "2";
	private static final String ACTIVE_ALL_LABEL = "ALL matrix";
	public static final String ACTIVE_GLOBAL_VALUE = "1";
	private static final String ACTIVE_GLOBAL_LABEL = "ACTIVE matrix";
	public static final String ACTIVE_NON_GLOBAL_VALUE = "0";
	private static final String ACTIVE_NON_GLOBAL_LABEL = "NON-ACTIVE matrix";
	public static final String ACTIVE_DEFAULT_VALUE = ACTIVE_ALL_VALUE;
	
	
	
	public static final String WINDOW_NAME_TRANSACTION_PROCESS = "TRANSACTION_PROCESS";
	
	public static final String WILDCARD_LABEL = "W";
	
	public static final String NULLS_LABEL = "N";

	private Locale locale = Locale.US;

	private UserEntityService userEntityService;
	private loginBean loginBean;
	
	// Bug 3987
	private static List<TransactionHeader> forHeaderRearrange;
	private static List<TransactionHeader> forHeaderRearrangeTemp;
	private List<String> neverSaveAllList = new ArrayList<String>();
	private List<String> neverSaveFixedList = new ArrayList<String>();
	
	private Set<String> defaultViewColumns;
	
	private TransactionDetailService transactionDetailService;
	
	private boolean inColumnRefresh = false;
	
	private static LinkConverter linkConverter = new LinkConverter();
	public Map<String,DriverNames> taxMatrixDriverMap;
	public Map<String,DriverNames> locationMatrixDriverMap;
	
/** End **/
	
	
	public MatrixCommonBean() {
		// Initialize Default Line menu
		defaultLineItems = new ArrayList<SelectItem>();
		defaultLineItems.add(new SelectItem(DEFAULT_LINE_ALL_VALUE, DEFAULT_LINE_ALL_LABEL));
		defaultLineItems.add(new SelectItem(DEFAULT_LINE_GLOBAL_VALUE, DEFAULT_LINE_GLOBAL_LABEL));
		defaultLineItems.add(new SelectItem(DEFAULT_LINE_NON_GLOBAL_VALUE, DEFAULT_LINE_NON_GLOBAL_LABEL));

		// Initialize Global Company Number menu
		globalCompanyNumberItems = new ArrayList<SelectItem>();
		globalCompanyNumberItems.add(new SelectItem(GLOBAL_COMPANY_NUMBER_ALL_VALUE, GLOBAL_COMPANY_NUMBER_ALL_LABEL));
		globalCompanyNumberItems.add(new SelectItem(GLOBAL_COMPANY_NUMBER_GLOBAL_VALUE, GLOBAL_COMPANY_NUMBER_GLOBAL_LABEL));
		globalCompanyNumberItems.add(new SelectItem(GLOBAL_COMPANY_NUMBER_NON_GLOBAL_VALUE, GLOBAL_COMPANY_NUMBER_NON_GLOBAL_LABEL));
		
		//Initialize Active Menu
		activeItems=new ArrayList<SelectItem>();
		activeItems.add(new SelectItem(ACTIVE_ALL_VALUE,ACTIVE_ALL_LABEL));
		activeItems.add(new SelectItem(ACTIVE_GLOBAL_VALUE,ACTIVE_GLOBAL_LABEL));
		activeItems.add(new SelectItem(ACTIVE_NON_GLOBAL_VALUE,ACTIVE_NON_GLOBAL_LABEL));
		
		
	}
	
	public EntityItemService entityItemService(){
		return entityItemService;
	}
	
	public void update(Observable obs, Object obj) {
		if (obs == ov) {
			activeStateItems = null;
		}
	}
	
	public void valueBound(HttpSessionBindingEvent event){
		StateObservable ov = StateObservable.getInstance();
		this.ov = ov;
		this.ov.addObserver(this);
	}

	public void valueUnbound(HttpSessionBindingEvent event){		
		if(ov!=null){
			ov.deleteObserver(this);
		}
	}
	
	public Map<String, String> getTaxMatrixColumnMap() {
		if (taxMatrixColumnMap == null) {
			taxMatrixColumnMap = getMatrixColumnMap(TaxMatrix.DRIVER_CODE, TaxMatrix.DRIVER_COUNT);
		}
		return taxMatrixColumnMap;
	}
	public Map<String, String> getGsbMatrixColumnMap() {
		if (gsbMatrixColumnMap == null) {
			gsbMatrixColumnMap = getMatrixColumnMap(GSBMatrix.DRIVER_CODE, GSBMatrix.DRIVER_COUNT);
		}
		return gsbMatrixColumnMap;
	}
	
	public Map<String, String> getSalesColumnNameMap() {
		if (salesColumnNameMap == null) {
			salesColumnNameMap = new HashMap<String,String>();
			
			Map<String,DataDefinitionColumn> propertyMap = cacheManager.getDataDefinitionPropertyMap("TB_SALETRANS");
			DataDefinitionColumn definitionColumn = null;
	    	for (Map.Entry<String, DataDefinitionColumn> e : propertyMap.entrySet()) {
	    		definitionColumn = propertyMap.get(e.getKey());	
	    		salesColumnNameMap.put(definitionColumn.getColumnName(),definitionColumn.getDescription());
	    	}
		}
		return salesColumnNameMap;
	}
	
	public Map<String, String> getTransactionColumnNameMap() {
		if (transactionColumnNameMap == null) {
			transactionColumnNameMap = new HashMap<String,String>();
			
			Map<String,DataDefinitionColumn> propertyMap = cacheManager.getDataDefinitionPropertyMap("TB_PURCHTRANS");
			DataDefinitionColumn definitionColumn = null;
	    	for (Map.Entry<String, DataDefinitionColumn> e : propertyMap.entrySet()) {
	    		definitionColumn = propertyMap.get(e.getKey());	
	    		transactionColumnNameMap.put(definitionColumn.getColumnName(),definitionColumn.getDescription());
	    	}
		}
		return transactionColumnNameMap;
	}

	public Map<String, String> getAllocationMatrixColumnMap() {
		if (allocationMatrixColumnMap == null) {
			allocationMatrixColumnMap = getMatrixColumnMap(AllocationMatrix.DRIVER_CODE, AllocationMatrix.DRIVER_COUNT);
		}
		return allocationMatrixColumnMap;
	}
	
	public Map<String, String> getLocationMatrixColumnMap() {
		if (locationMatrixColumnMap == null) {
			locationMatrixColumnMap = getMatrixColumnMap(LocationMatrix.DRIVER_CODE, LocationMatrix.DRIVER_COUNT);
		}
		return locationMatrixColumnMap;
	}
	
	public Map<String, String> getGlExtractMap(){
		if(glExtractMap == null){
			glExtractMap = getMatrixColumnMap(GlExtractMap.DRIVER_CODE, GlExtractMap.DRIVER_COUNT);
		}
		return glExtractMap;
	}
	
	private Map<String, String> getMatrixColumnMap(String code, long driverCount) {
		Map<String, String> map = new HashMap<String,String>();
		Map<String,DriverNames> matrixDriverNamesMap = cacheManager.getMatrixColDriverNamesMap(code);
		//boolean isSaleTrans = "GSB".equals(code);
		
		Map<String,DataDefinitionColumn> transactionColumnMap =null;// cacheManager.getDataDefinitionColumnMap("TB_TRANSACTION_DETAIL");
		if(code.equals("GSB"))
			transactionColumnMap=	cacheManager.getDataDefinitionColumnMap("TB_BILLTRANS");
		else{
			transactionColumnMap=	cacheManager.getDataDefinitionColumnMap("TB_PURCHTRANS");
		}

		for (int i = 1; i <= driverCount; i++) {
			String driver = String.format("DRIVER_%02d",i);
			if (matrixDriverNamesMap.containsKey(driver) && transactionColumnMap != null) {
				String transDtlColName = matrixDriverNamesMap.get(driver).getTransDtlColName();
				if(transactionColumnMap.get(transDtlColName) != null)
					map.put(driver, transactionColumnMap.get(transDtlColName).getAbbrDesc());
				else
					map.put(driver, "");
			} else {
				map.put(driver, "");
			}
		}
		
		return map;
	}

	public Map<String,DataDefinitionColumn> getBCPSaleTransactionPropertyMap() {
		return cacheManager.getDataDefinitionPropertyMap("TB_BCP_BILLTRANS");
	}
	
	public Map<String,DataDefinitionColumn> getBCPTransactionPropertyMap() {
		return cacheManager.getDataDefinitionPropertyMap("TB_BCP_TRANSACTIONS");
	}
	
	public Map<String,DataDefinitionColumn> getTransactionPropertyMap() {
		return cacheManager.getDataDefinitionPropertyMap("TB_PURCHTRANS");
	}
	
	public Map<String,DataDefinitionColumn> getTransactionLogPropertyMap() {
		return cacheManager.getDataDefinitionPropertyMap("TB_PURCHTRANS_LOG");
	}
	
	public Map<String,DataDefinitionColumn> getSaleTransactionPropertyMap() {
		return cacheManager.getDataDefinitionPropertyMap("TB_SALETRANS");
	}
	
	public Map<String,DataDefinitionColumn> getBillTransactionPropertyMap() {
		return cacheManager.getDataDefinitionPropertyMap("TB_BILLTRANS");
	}
	
	public Map<String,DataDefinitionColumn> getImportConnectionPropertyMap() {
		return cacheManager.getDataDefinitionPropertyMap("TB_IMPORT_CONNECTION");
	}
	
	public Map<String,DataDefinitionColumn> getImportSpecPropertyMap() {
		return cacheManager.getDataDefinitionPropertyMap("TB_IMPORT_SPEC");
	}
	
	public Map<String,DataDefinitionColumn> getImportDefinitionPropertyMap() {
		return cacheManager.getDataDefinitionPropertyMap("TB_IMPORT_DEFINITION");
	}
		
	public Map<String,DriverNames> getTransactionPropertyAllDriverNamesMap() {
		return cacheManager.getTransactionPropertyDriverNamesMap(null);
	}
	public boolean setSaleTransactionProperties(Matrix matrix, BillTransaction trDetail) {
		boolean valueSet = false;
		Map<String,DriverNames> transactionDriverNamesMap = cacheManager.getTransactionPropertyDriverNamesMap(matrix.getDriverCode());
		
		//0001934: Company Config - Select Entity
		EntityItemDTO entityItemDTO = getEntityItemDTO();
		Map<String, String> parentEntityLevelMap = cacheManager.getParentEntityLevelMap(entityItemDTO.getEntityId());
		for (String property : transactionDriverNamesMap.keySet()) {
			DriverNames driverNames = transactionDriverNamesMap.get(property);
			String driver = driverNames.getMatrixColName();
			String value = Util.getPropertyAsString(matrix, Util.columnToProperty(driver)); 
			//0001934: Company Config - Select Entity
			//if (driverNames.isBusinessUnit() && driverNames.isActive() && !entityItemDTO.isEntityCodeAll()) {
	    	//value = entityItemDTO.getEntityCode();
			//}		
			if (!entityItemDTO.isEntityCodeAll() && parentEntityLevelMap.containsKey(driverNames.getTransDtlColName())) {
				value = (String)parentEntityLevelMap.get(driverNames.getTransDtlColName());
			}	
			
			if ((value != null) && !value.equals("") && !value.equals("*ALL")) {
				logger.debug("Setting property '"+property+"' to value '"+value+"' from " + driver);
				try {
					Util.setProperty(trDetail, property, value.toUpperCase(), String.class);
					valueSet = true;
				} catch (RuntimeException e) {
					logger.error("Could not set '"+property+"' to value '"+value+"' from " + driver);
				}
			}
		}
	
		return valueSet;
	}
	public boolean setBCPTransactionProperties(Matrix matrix, BCPPurchaseTransaction trDetail) {
		boolean valueSet = false;
		Map<String,DriverNames> transactionDriverNamesMap = 
			cacheManager.getTransactionPropertyDriverNamesMap(matrix.getDriverCode());
		
		//0001934: Company Config - Select Entity
		EntityItemDTO entityItemDTO = getEntityItemDTO();
		Map<String, String> parentEntityLevelMap = cacheManager.getParentEntityLevelMap(entityItemDTO.getEntityId());
		for (String property : transactionDriverNamesMap.keySet()) {
			DriverNames driverNames = transactionDriverNamesMap.get(property);
			String driver = driverNames.getMatrixColName();
			String value = Util.getPropertyAsString(matrix, Util.columnToProperty(driver)); 
			//0001934: Company Config - Select Entity
			//if (driverNames.isBusinessUnit() && driverNames.isActive() && !entityItemDTO.isEntityCodeAll()) {
	    	//value = entityItemDTO.getEntityCode();
			//}		
			if (!entityItemDTO.isEntityCodeAll() && parentEntityLevelMap.containsKey(driverNames.getTransDtlColName())) {
				value = (String)parentEntityLevelMap.get(driverNames.getTransDtlColName());
			}	
			
			if ((value != null) && !value.equals("") && !value.equals("*ALL")) {
				logger.debug("Setting property '"+property+"' to value '"+value+"' from " + driver);
				try {
					Util.setProperty(trDetail, property, value.toUpperCase(), String.class);
					valueSet = true;
				} catch (RuntimeException e) {
					logger.error("Could not set '"+property+"' to value '"+value+"' from " + driver);
				}
			}
		}
	
		return valueSet;
	}
	
	public boolean setTransactionProperties(Matrix matrix, PurchaseTransaction trDetail) {
		boolean valueSet = false;
		Map<String,DriverNames> transactionDriverNamesMap = 
			cacheManager.getTransactionPropertyDriverNamesMap(matrix.getDriverCode());

		//0001934: Company Config - Select Entity
		EntityItemDTO entityItemDTO = getEntityItemDTO();
		Map<String, String> parentEntityLevelMap = cacheManager.getParentEntityLevelMap(entityItemDTO.getEntityId());
		for (String property : transactionDriverNamesMap.keySet()) {
			DriverNames driverNames = transactionDriverNamesMap.get(property);
			String driver = driverNames.getMatrixColName();
			String value = Util.getPropertyAsString(matrix, Util.columnToProperty(driver)); 
			//0001934: Company Config - Select Entity
			//if (driverNames.isBusinessUnit() && driverNames.isActive() && !entityItemDTO.isEntityCodeAll()) {
	    	//value = entityItemDTO.getEntityCode();
			//}
			if (!entityItemDTO.isEntityCodeAll() && parentEntityLevelMap.containsKey(driverNames.getTransDtlColName())) {
				value = (String)parentEntityLevelMap.get(driverNames.getTransDtlColName());
			}	
			
			if ((value != null) && !value.equals("") && !value.equals("*ALL")) {
				logger.debug("Setting property '"+property+"' to value '"+value+"' from " + driver);
				try {
					Util.setProperty(trDetail, property, value.toUpperCase(), String.class);
					valueSet = true;
				} catch (RuntimeException e) {
					logger.error("Could not set '"+property+"' to value '"+value+"' from " + driver);
				}
			}
		}
	
		return valueSet;
	}
	
	public boolean setMatrixProperties(PurchaseTransaction trDetail, Matrix matrix) {
		boolean valueSet = false;
		
		Map<String,DriverNames> transactionDriverNamesMap = 
				cacheManager.getTransactionPropertyDriverNamesMap(matrix.getDriverCode());
			Map<String,DataDefinitionColumn> transactionColumnMap = null;
				
			if(matrix.getDriverCode().equals("GSB"))
				transactionColumnMap=	cacheManager.getDataDefinitionColumnMap("TB_BILLTRANS");
			else{
				transactionColumnMap=	cacheManager.getDataDefinitionColumnMap("TB_PURCHTRANS");
			}
 	
		//0001934: Company Config - Select Entity
		EntityItemDTO entityItemDTO = getEntityItemDTO();
		Map<String, String> parentEntityLevelMap = cacheManager.getParentEntityLevelMap(entityItemDTO.getEntityId());
		for (String property : transactionDriverNamesMap.keySet()) {
			DriverNames driverNames = transactionDriverNamesMap.get(property);
			String driver = driverNames.getMatrixColName();
			
			String value = Util.getPropertyAsString(trDetail, property);
			//0001934: Company Config - Select Entity
			//if (driverNames.isBusinessUnit() && driverNames.isActive() && !entityItemDTO.isEntityCodeAll()) {
	    	//value = entityItemDTO.getEntityCode();
			//}
			if (!entityItemDTO.isEntityCodeAll() && parentEntityLevelMap.containsKey(driverNames.getTransDtlColName())) {
				value = (String)parentEntityLevelMap.get(driverNames.getTransDtlColName());
			}
			
			//Fix for 3371:1. Display all proper values on the screen - the driver value and description. 
			/*if (!getFlagValue(driverNames.getActiveFlag())) {
				logger.debug("Setting inactive '"+driver+"' value to '"+value+"' ("+ property + ")");
				try {
					Util.setProperty(matrix, Util.columnToProperty(driver), "*ALL", String.class);
					Util.setProperty(matrix, Util.columnToProperty(driver)+"Desc", value, String.class);
				} catch (RuntimeException e) {
					logger.error("Could not set '"+driver+"' value to '"+value+"' from " + property);
				}
			} else */if ((value != null) && !value.trim().equals("")) {
				logger.debug("Setting '"+driver+"' value to '"+value+"' from " + property);
				try {
					Util.setProperty(matrix, Util.columnToProperty(driver), value.trim().toUpperCase(), String.class);
					
					if (matrix.getHasDescriptions()) {
						String descColumn = transactionColumnMap.get(driverNames.getTransDtlColName()).getDescColumnName();
						if ((descColumn != null) && !descColumn.equals("")) {
							String desc = Util.getPropertyAsString(trDetail, Util.columnToProperty(descColumn));
							logger.debug("Setting '"+driver+"' desc to '"+desc+"' from " + descColumn);
							try {
								Util.setProperty(matrix, Util.columnToProperty(driver)+"Desc", desc, String.class);
							} catch (RuntimeException e) {
								logger.error("Could not set '"+driver+"' desc to '"+desc+"' from " + descColumn);
							}
						}
					}
				} catch (RuntimeException e) {
					logger.error("Could not set '"+driver+"' value to '"+value+"' from " + property);
				}
			} else { // if (getFlagValue(driverNames.getActiveFlag())){//Fix for 3371
				//value = getFlagValue(driverNames.getNullDriverFlag())? "*NULL":"*ALL";
				value = "*ALL";
				logger.debug("Setting '"+driver+"' value to '"+value+"' from " + property);
				try {
					Util.setProperty(matrix, Util.columnToProperty(driver), value, String.class);
				} catch (RuntimeException e) {
					logger.error("Could not set '"+driver+"' value to '"+value+"' from " + property);
				}
			}
		}
	
		return valueSet;
	}
	
	public boolean setMatrixProperties(BCPPurchaseTransaction trDetail, Matrix matrix) {
		boolean valueSet = false;
		Map<String,DriverNames> transactionDriverNamesMap = 
			cacheManager.getTransactionPropertyDriverNamesMap(matrix.getDriverCode());
		Map<String,DataDefinitionColumn> transactionColumnMap = 
			cacheManager.getDataDefinitionColumnMap("TB_BCP_TRANSACTIONS");
		
		//0001934: Company Config - Select Entity
		EntityItemDTO entityItemDTO = getEntityItemDTO();
		Map<String, String> parentEntityLevelMap = cacheManager.getParentEntityLevelMap(entityItemDTO.getEntityId());
		for (String property : transactionDriverNamesMap.keySet()) {
			DriverNames driverNames = transactionDriverNamesMap.get(property);
			String driver = driverNames.getMatrixColName();
			
			String value = Util.getPropertyAsString(trDetail, property);
			//0001934: Company Config - Select Entity
			//if (driverNames.isBusinessUnit() && driverNames.isActive() && !entityItemDTO.isEntityCodeAll()) {
	    	//value = entityItemDTO.getEntityCode();
			//}
			if (!entityItemDTO.isEntityCodeAll() && parentEntityLevelMap.containsKey(driverNames.getTransDtlColName())) {
				value = (String)parentEntityLevelMap.get(driverNames.getTransDtlColName());
			}
			
			//Fix for 3371:1. Display all proper values on the screen - the driver value and description. 
			/*if (!getFlagValue(driverNames.getActiveFlag())) {
				logger.debug("Setting inactive '"+driver+"' value to '"+value+"' ("+ property + ")");
				try {
					Util.setProperty(matrix, Util.columnToProperty(driver), "*ALL", String.class);
					Util.setProperty(matrix, Util.columnToProperty(driver)+"Desc", value, String.class);
				} catch (RuntimeException e) {
					logger.error("Could not set '"+driver+"' value to '"+value+"' from " + property);
				}
			} else */if ((value != null) && !value.trim().equals("")) {
				logger.debug("Setting '"+driver+"' value to '"+value+"' from " + property);
				try {
					Util.setProperty(matrix, Util.columnToProperty(driver), value.trim().toUpperCase(), String.class);
					
					if (matrix.getHasDescriptions()) {
						String descColumn = transactionColumnMap.get(driverNames.getTransDtlColName()).getDescColumnName();
						if ((descColumn != null) && !descColumn.equals("")) {
							String desc = Util.getPropertyAsString(trDetail, Util.columnToProperty(descColumn));
							logger.debug("Setting '"+driver+"' desc to '"+desc+"' from " + descColumn);
							try {
								Util.setProperty(matrix, Util.columnToProperty(driver)+"Desc", desc, String.class);
							} catch (RuntimeException e) {
								logger.error("Could not set '"+driver+"' desc to '"+desc+"' from " + descColumn);
							}
						}
					}
				} catch (RuntimeException e) {
					logger.error("Could not set '"+driver+"' value to '"+value+"' from " + property);
				}
			} else { // if (getFlagValue(driverNames.getActiveFlag())){//Fix for 3371
				//value = getFlagValue(driverNames.getNullDriverFlag())? "*NULL":"*ALL";
				value = "*ALL";
				logger.debug("Setting '"+driver+"' value to '"+value+"' from " + property);
				try {
					Util.setProperty(matrix, Util.columnToProperty(driver), value, String.class);
				} catch (RuntimeException e) {
					logger.error("Could not set '"+driver+"' value to '"+value+"' from " + property);
				}
			}
		}
	
		return valueSet;
	}
	
	public boolean setMatrixProperties(Matrix source, TaxAllocationMatrix destination) {
		Map<String, DriverNames> driverNamesMap = getCacheManager().getMatrixColDriverNamesMap(source.getDriverCode());
		for(String property : driverNamesMap.keySet()) {
			DriverNames driverNames = driverNamesMap.get(property);
			String driver = driverNames.getMatrixColName();
			String value = Util.getPropertyAsString(source, Util.columnToProperty(driver));
			if(getFlagValue(driverNames.getActiveFlag())) {
				Util.setProperty(destination, Util.columnToProperty(driver), value, String.class);
			}
		}
		
		return true;
	}
	
	public boolean setMatrixProperties(TaxAllocationMatrixDetail source, Matrix destination) {
		Map<String, DriverNames> driverNamesMap = getCacheManager().getMatrixColDriverNamesMap(destination.getDriverCode());
		for(String property : driverNamesMap.keySet()) {
			DriverNames driverNames = driverNamesMap.get(property);
			String driver = driverNames.getMatrixColName();
			String value = Util.getPropertyAsString(source, Util.columnToProperty(driver));
			if(getFlagValue(driverNames.getActiveFlag())) {
				Util.setProperty(destination, Util.columnToProperty(driver), value, String.class);
			}
		}
		
		return true;
	}
	
	public boolean setMatrixProperties(List<HtmlInputText> source, TaxAllocationMatrixDetail destination, Long driverCount) {
		for (HtmlInputText input : source) {
			String driver = (String)input.getAttributes().get("DRIVER");
			String value = input.getSubmittedValue() == null ? (String) input.getValue() : (String) input.getSubmittedValue();
			String driverProperty = Util.columnToProperty(driver);
			Util.setProperty(destination, driverProperty, value, String.class);
		}
		
		for (Long i = 1L; i <= driverCount; i++) {
			String value = (String) Util.getProperty(destination, String.format("driver%02d", i));
			if ((value == null) || value.trim().equals("")) {
				Util.setProperty(destination, String.format("driver%02d", i), "*ALL", String.class);
			}
		}
		
		return true;
	}
	
	public boolean setDriverEntityProperties(Matrix matrix) {
		boolean valueSet = false;
		Map<String,DriverNames> transactionDriverNamesMap = 
			cacheManager.getTransactionPropertyDriverNamesMap(matrix.getDriverCode());

		//0001934: Company Config - Select Entity
		EntityItemDTO entityItemDTO = getEntityItemDTO();
		Map<String, String> parentEntityLevelMap = cacheManager.getParentEntityLevelMap(entityItemDTO.getEntityId());
		for (String property : transactionDriverNamesMap.keySet()) {
			DriverNames driverNames = transactionDriverNamesMap.get(property);
			String driver = driverNames.getMatrixColName();
			String value = Util.getPropertyAsString(matrix, Util.columnToProperty(driver)); 

			if (!entityItemDTO.isEntityCodeAll() && parentEntityLevelMap.containsKey(driverNames.getTransDtlColName())) {
				value = (String)parentEntityLevelMap.get(driverNames.getTransDtlColName());
			}	
			
			if ((value != null) && !value.equals("") && !value.equals("*ALL")) {
				logger.debug("Setting property '"+property+"' to value '"+value+"' from " + driver);
				try {
					Util.setProperty(matrix, Util.columnToProperty(driver), value.toUpperCase(), String.class);
					valueSet = true;
				} catch (RuntimeException e) {
					logger.error("Could not set '"+property+"' to value '"+value+"' from " + driver);
				}
			}
		}
	
		return valueSet;
	}

	public List<SelectItem> getZipcodeItems() {
		if (zipItems == null) {
			zipItems = new ArrayList<SelectItem>();
			zipItems.add(new SelectItem(new Long(0), "Select Zip"));
			// TODO problem here since we're not returning all posibilities!!
			List<Jurisdiction> list = jurisdictionService.findByExample(new Jurisdiction(), 200);
			logger.debug("getZipItems(): size of zipcode list = " + list.size());
			for (Jurisdiction jurisdiction : list) {
				zipItems.add(new SelectItem(jurisdiction.getJurisdictionId(), jurisdiction.getZip()));
			}
		}
		
		return zipItems;
	}
	
	public List<SelectItem> getActiveStateItems() {
		if (activeStateItems == null) {
			activeStateItems = new ArrayList<SelectItem>();
			for (TaxCodeStateDTO tcs : cacheManager.getTaxCodeStateMap().values()) {
				// Only add states that are active
				String active = tcs.getActiveFlag();
				if ((active != null) && active.equals("1")) {
					activeStateItems.add(new SelectItem(tcs.getTaxCodeState(), tcs.getName()));
				}
			}
			activeStateItems.add(0,new SelectItem("", "Select State"));
		}
		
		return activeStateItems;
	}
	
	public List<SelectItem> getAllStateItems() {
		if (allStateItems == null) {
			allStateItems = new ArrayList<SelectItem>();
			for (TaxCodeStateDTO tcs : cacheManager.getTaxCodeStateMap().values()) {
				allStateItems.add(new SelectItem(tcs.getTaxCodeState(), tcs.getName()));
			}
			allStateItems.add(0,new SelectItem("", "Select State"));
		}
		
		return allStateItems;
	}
	
	public Map<String, Boolean> getMinimumTaxDriversMap() {
		if (minimumTaxDriversMap == null) {
			minimumTaxDriversMap = new HashMap<String,Boolean>();
			
			
			String code = TaxMatrix.DRIVER_CODE;
	    	
	    	
			Map<String,DriverNames> transactionDriverNamesMap = cacheManager.getTransactionPropertyDriverNamesMap(code);

			for (String transProperty : transactionDriverNamesMap.keySet()) {
				DriverNames driverNames = transactionDriverNamesMap.get(transProperty);
				String driver = driverNames.getMatrixColName();
				String driverProperty = Util.columnToProperty(driver);
				
				minimumTaxDriversMap.put(driverProperty, new Boolean(getFlagValue(driverNames.getMandatoryFlag())));
			}
		}
		return minimumTaxDriversMap;
	}
	public Map<String, Boolean> getMinimumTaxDriversMapGSB() {
		if (minimumTaxDriversMapGSB == null) {
			minimumTaxDriversMapGSB = new HashMap<String,Boolean>();
			
			
			String code = GSBMatrix.DRIVER_CODE;
	    	
	    	
			Map<String,DriverNames> transactionDriverNamesMap = cacheManager.getTransactionPropertyDriverNamesMap(code);

			for (String transProperty : transactionDriverNamesMap.keySet()) {
				DriverNames driverNames = transactionDriverNamesMap.get(transProperty);
				String driver = driverNames.getMatrixColName();
				String driverProperty = Util.columnToProperty(driver);
				
				minimumTaxDriversMapGSB.put(driverProperty, new Boolean(getFlagValue(driverNames.getMandatoryFlag())));
			}
		}
		return minimumTaxDriversMapGSB;
	}
	public HtmlPanelGrid getMinimumTaxDriversPanelgsb() {
		if (minimumTaxDriversPanelgsb == null) {
			HtmlPanelGrid parentPanel = new HtmlPanelGrid();
			parentPanel.setColumns(3);
			parentPanel.setId("minDrivers");
			parentPanel.setCellpadding("0");
			parentPanel.setCellspacing("0");
			parentPanel.setColumnClasses("column-input,column-input,column-input");

			String code = GSBMatrix.DRIVER_CODE;
			String id = code + "_" + parentPanel.getId();
			Map<String,DriverNames> transactionDriverNamesMap = cacheManager.getTransactionPropertyDriverNamesMap(code);
			Map<String,DataDefinitionColumn> transactionPropertyMap = cacheManager.getDataDefinitionPropertyMap("TB_SALETRANS");

			int counter = 0;
			for (String transProperty : transactionDriverNamesMap.keySet()) {
				DriverNames driverNames = transactionDriverNamesMap.get(transProperty);
				String driver = driverNames.getMatrixColName();
				String driverProperty = Util.columnToProperty(driver);

				// Add item
				HtmlPanelGroup grid = new HtmlPanelGroup();
				grid.setId(id+"_label_grp"+counter);
				
				HtmlSelectBooleanCheckbox check = new HtmlSelectBooleanCheckbox();
				check.setId(id+"_check"+counter);
				check.getAttributes().put("DRIVERPROP", driverProperty);
				check.getAttributes().put("TRANSPROP", transProperty);
				check.setStyle("float:left;");
				check.setStyleClass("check");
				check.setDisabled(!getFlagValue(driverNames.getActiveFlag()) || getFlagValue(driverNames.getMandatoryFlag()));
				check.setValueExpression("value", JsfHelper.createValueExpression("#{matrixCommonBean.minimumTaxDriversMapGSB['" + driverProperty + "']}", Boolean.class));
				grid.getChildren().add(check);
				
				HtmlOutputText label = new HtmlOutputText();
				if (transactionPropertyMap.get(transProperty) != null) {
					label.setValue(transactionPropertyMap.get(transProperty).getAbbrDesc());					
				}
				label.setId(id+"_label"+counter);
				if (!getFlagValue(driverNames.getActiveFlag())) {
					label.setStyle("color:gray;float:left;");
				} else if (getFlagValue(driverNames.getMandatoryFlag())) {
					label.setStyle("color:red;float:left;");
				} else {
					label.setStyle("float:left;");
				}
				grid.getChildren().add(label);
				parentPanel.getChildren().add(grid);

				counter++;
			}
			
			// Finish the row
			while ((counter % 3) != 0) {
				HtmlSpacer spacer = new HtmlSpacer();
				spacer.setId(id+"_lblspacer"+counter);
				parentPanel.getChildren().add(spacer);
				
				counter++;
			}
			
			minimumTaxDriversPanelgsb = parentPanel;
		}
		return minimumTaxDriversPanelgsb;
	}
	public HtmlPanelGrid getMinimumTaxDriversPanel() {
		if (minimumTaxDriversPanel == null) {
			HtmlPanelGrid parentPanel = new HtmlPanelGrid();
			parentPanel.setColumns(3);
			parentPanel.setId("minDrivers");
			parentPanel.setCellpadding("0");
			parentPanel.setCellspacing("0");
			parentPanel.setColumnClasses("column-input,column-input,column-input");

			String code = TaxMatrix.DRIVER_CODE;
			String id = code + "_" + parentPanel.getId();
			Map<String,DriverNames> transactionDriverNamesMap = cacheManager.getTransactionPropertyDriverNamesMap(code);
			Map<String,DataDefinitionColumn> transactionPropertyMap =null;// cacheManager.getDataDefinitionPropertyMap("TB_TRANSACTION_DETAIL");
			if(code.equals("GSB"))
				transactionPropertyMap=	cacheManager.getDataDefinitionPropertyMap("TB_BILLTRANS");
			else{
				transactionPropertyMap=	cacheManager.getDataDefinitionPropertyMap("TB_PURCHTRANS");
			}

			int counter = 0;
			for (String transProperty : transactionDriverNamesMap.keySet()) {
				DriverNames driverNames = transactionDriverNamesMap.get(transProperty);
				String driver = driverNames.getMatrixColName();
				String driverProperty = Util.columnToProperty(driver);

				// Add item
				HtmlPanelGroup grid = new HtmlPanelGroup();
				grid.setId(id+"_label_grp"+counter);
				
				HtmlSelectBooleanCheckbox check = new HtmlSelectBooleanCheckbox();
				check.setId(id+"_check"+counter);
				check.getAttributes().put("DRIVERPROP", driverProperty);
				check.getAttributes().put("TRANSPROP", transProperty);
				check.setStyle("float:left;");
				check.setStyleClass("check");
				check.setDisabled(!getFlagValue(driverNames.getActiveFlag()) || getFlagValue(driverNames.getMandatoryFlag()));
				check.setValueExpression("value", JsfHelper.createValueExpression("#{matrixCommonBean.minimumTaxDriversMap['" + driverProperty + "']}", Boolean.class));
				grid.getChildren().add(check);
				
				HtmlOutputText label = new HtmlOutputText();
				if (transactionPropertyMap.get(transProperty) != null) {
					label.setValue(transactionPropertyMap.get(transProperty).getAbbrDesc());					
				}
				label.setId(id+"_label"+counter);
				if (!getFlagValue(driverNames.getActiveFlag())) {
					label.setStyle("color:gray;float:left;");
				} else if (getFlagValue(driverNames.getMandatoryFlag())) {
					label.setStyle("color:red;float:left;");
				} else {
					label.setStyle("float:left;");
				}
				grid.getChildren().add(label);
				parentPanel.getChildren().add(grid);

				counter++;
			}
			
			// Finish the row
			while ((counter % 3) != 0) {
				HtmlSpacer spacer = new HtmlSpacer();
				spacer.setId(id+"_lblspacer"+counter);
				parentPanel.getChildren().add(spacer);
				
				counter++;
			}
			
			minimumTaxDriversPanel = parentPanel;
		}
		return minimumTaxDriversPanel;
	}

	public void setMinimumTaxDriversPanel(HtmlPanelGrid minimumTaxDriversPanel) {
		this.minimumTaxDriversPanel = minimumTaxDriversPanel;
	}

	// TODO move to JsfHelper
	protected void setReadonly(UIComponent parent, boolean readonly) {
		if ((parent != null) && (parent.getChildCount() > 0)) {
			for (UIComponent child : parent.getChildren()) {
				if (child instanceof HtmlInputText) {
					HtmlInputText input = (HtmlInputText) child;
					input.setReadonly(readonly);
				}
				
				// recurse
				setReadonly(child, readonly);
			}
		}
	}
	
	public Collection<HtmlSelectBooleanCheckbox> findWildcardDrivers(UIComponent parent) {
		Set<HtmlSelectBooleanCheckbox> result = new HashSet<HtmlSelectBooleanCheckbox>();
		findWildcardDrivers(parent, result);
		return result;
	}
	
	private void findWildcardDrivers(UIComponent parent, Set<HtmlSelectBooleanCheckbox> drivers) {
		if ((parent != null) && (parent.getChildCount() > 0)) {
			for (UIComponent child : parent.getChildren()) {
				if (child instanceof HtmlSelectBooleanCheckbox) {
					HtmlSelectBooleanCheckbox input = (HtmlSelectBooleanCheckbox) child;
					Boolean wildcard = (Boolean) input.getAttributes().get("WILDCARD");
					if ((wildcard != null) && wildcard) {
						Boolean value = (Boolean) input.getValue();
						if ((value != null) && value) {
							drivers.add(input);
						}
					}
				}
				
				// recurse
				findWildcardDrivers(child, drivers);
			}
		}
	}
	
	public Collection<String> findCheckedDrivers(UIComponent parent, String attribute) {
		Set<String> result = new HashSet<String>();
		findCheckedDrivers(parent, result, attribute);
		return result;
	}
	
	private void findCheckedDrivers(UIComponent parent, Set<String> drivers, String attribute) {
		if ((parent != null) && (parent.getChildCount() > 0)) {
			for (UIComponent child : parent.getChildren()) {
				if (child instanceof HtmlSelectBooleanCheckbox) {
					HtmlSelectBooleanCheckbox input = (HtmlSelectBooleanCheckbox) child;
					Boolean value = input.getValue() == null ? (Boolean) input.getSubmittedValue() : (Boolean) input.getValue();
					if (value != null && value) {
						drivers.add((String) input.getAttributes().get(attribute));
					}
				}
				
				// recurse
				findCheckedDrivers(child, drivers, attribute);
			}
		}
	}
	
	public void checkDrivers(UIComponent parent, Collection<String> drivers, String attribute) {
		if ((parent != null) && (parent.getChildCount() > 0)) {
			for (UIComponent child : parent.getChildren()) {
				if (child instanceof HtmlSelectBooleanCheckbox) {
					HtmlSelectBooleanCheckbox input = (HtmlSelectBooleanCheckbox) child;
					String driver = (String) input.getAttributes().get(attribute);
					
					if(drivers.contains(driver)) {
						input.setValue(Boolean.TRUE);
					}
				}
				
				// recurse
				checkDrivers(child, drivers, attribute);
			}
		}
	}
	
	public Collection<String> findUncheckedDrivers(UIComponent parent, String attribute) {
		Set<String> result = new HashSet<String>();
		findUncheckedDrivers(parent, result, attribute);
		return result;
	}
	
	public List<HtmlInputText> findInputControls(UIComponent parent, List<HtmlInputText> list) {
		if ((parent != null) && (parent.getChildCount() > 0)) {
			for (UIComponent child : parent.getChildren()) {
				if (child instanceof HtmlInputText) {
					HtmlInputText input = (HtmlInputText) child;
					String driver = (String)input.getAttributes().get("DRIVER");
					if ((driver != null) && !driver.equals("")) {
						list.add(input);
					}
				}
				findInputControls(child,list);
			}
		}
		return list;
	}
	
	private void findUncheckedDrivers(UIComponent parent, Set<String> drivers, String attribute) {
		if ((parent != null) && (parent.getChildCount() > 0)) {
			for (UIComponent child : parent.getChildren()) {
				if (child instanceof HtmlSelectBooleanCheckbox) {
					HtmlSelectBooleanCheckbox input = (HtmlSelectBooleanCheckbox) child;
					Boolean value = (Boolean) input.getValue();
					if ((value == null) || (value == false)) {
						drivers.add((String) input.getAttributes().get(attribute));
					}
				}
				
				// recurse
				findUncheckedDrivers(child, drivers, attribute);
			}
		}
	}
	
	public void initAllDriverPanelCheckboxes(UIComponent parent, Matrix matrix) {
		if ((parent != null) && (parent.getChildCount() > 0)) {
			for (UIComponent child : parent.getChildren()) {
				if (child instanceof HtmlSelectBooleanCheckbox) {
					HtmlSelectBooleanCheckbox input = (HtmlSelectBooleanCheckbox) child;
					input.setStyle((matrix != null)? "display:block;float:left;":"display:none;float:left;");
					input.setDisabled(false);
					input.setValue(true);
					input.setOnclick(null);
					
					if (matrix != null) {
						String property = (String) input.getAttributes().get("DRIVERPROP");
						if (property != null) {
							String value = Util.getPropertyAsString(matrix, property);
							if ((value != null) && !value.equals("") && !value.equals("*ALL")) {
								Boolean mandatory;
								if (matrix.getDriverCode().equals(TaxMatrix.DRIVER_CODE)) {
									mandatory = getMinimumTaxDriversMap().get(property);
								} else if(matrix.getDriverCode().equals(GSBMatrix.DRIVER_CODE)){
									mandatory = getMinimumTaxDriversMapGSB().get(property);
							     }else {
									mandatory = (Boolean) input.getAttributes().get("MANDATORY");
								}
								
								if(mandatory) {
									input.setDisabled(mandatory);
									input.setValue(mandatory);
								}
							}
						}
					}
				}
				
				initAllDriverPanelCheckboxes(child, matrix);
			}
		}
	}
	
	public void initDriverPanelCheckboxes(UIComponent parent, Matrix matrix) {
		if ((parent != null) && (parent.getChildCount() > 0)) {
			for (UIComponent child : parent.getChildren()) {
				if (child instanceof HtmlSelectBooleanCheckbox) {
					HtmlSelectBooleanCheckbox input = (HtmlSelectBooleanCheckbox) child;
		
					input.getAttributes().remove("ORIGVALUE");
					input.setStyle((matrix != null)? "display:block;float:left;":"display:none;float:left;");
					input.setValue(null);
					input.setSubmittedValue(null);
					input.setDisabled(true);

					if (matrix != null) {
						String property = (String) input.getAttributes().get("DRIVERPROP");
						if (property != null) {
							String value = Util.getPropertyAsString(matrix, property);
							if ((value != null) && !value.equals("") && !value.equals("*ALL")) {
								Boolean mandatory;
								if (matrix.getDriverCode().equals(TaxMatrix.DRIVER_CODE)) {
									mandatory = getMinimumTaxDriversMap().get(property);
								} else if(matrix.getDriverCode().equals(GSBMatrix.DRIVER_CODE)){
									mandatory = getMinimumTaxDriversMapGSB().get(property);
								}else {
									mandatory = (Boolean) input.getAttributes().get("MANDATORY");
								}
								input.setDisabled(mandatory);
								input.setValue(mandatory);
								
								// wildcard support
								Boolean wildcard = (Boolean) input.getAttributes().get("WILDCARD");
								if ((wildcard != null) && wildcard) {
									input.getAttributes().put("ORIGVALUE", value);
								}
							}
							else if ((value != null) && !value.equals("") && value.equals("*ALL")){
								// Active & null enabled
								Boolean nullEnabled = (Boolean) input.getAttributes().get("NULLS");
								if ((nullEnabled != null) && nullEnabled) {		
									Util.setProperty(matrix, property, "*NULL", String.class);
										
									Boolean mandatory;
									if (matrix.getDriverCode().equals(TaxMatrix.DRIVER_CODE)) {
										mandatory = getMinimumTaxDriversMap().get(property);
									} else if(matrix.getDriverCode().equals(GSBMatrix.DRIVER_CODE)){
										mandatory = getMinimumTaxDriversMapGSB().get(property);
									}
									else {
										mandatory = (Boolean) input.getAttributes().get("MANDATORY");
									}
									
									if(mandatory){
										input.setDisabled(true);
										input.setSelected(true);
									}
									else{
										input.setDisabled(false);
									}
								}
							}
						}
					}
				} else if (child instanceof HtmlInputText) {
					HtmlInputText input = (HtmlInputText) child;
					if (input.getId().contains("output")) {
						input.setDisabled(true);
					}
				} else if (child instanceof HtmlOutputText) {
					HtmlOutputText label = (HtmlOutputText) child;
					if (label.getId().contains("label")) {
						if (matrix == null) {
							label.setValue(((String) label.getValue()).replace("%:", ":"));
						} else {
							Boolean wildcard = (Boolean) label.getAttributes().get("WILDCARD");
							if ((wildcard != null) && wildcard) {
								label.setValue(((String) label.getValue()).replace(":", "%:"));
							}
						}
					}
				}
				
				// recurse
				initDriverPanelCheckboxes(child, matrix);
			}
		}
	}
	
	public void prepareTaxDriverPanel(UIComponent parent) {
		if ((parent != null) && (parent.getChildCount() > 0)) {
			for (UIComponent child : parent.getChildren()) {
				if (child instanceof HtmlOutputText) {
					String property = (String) child.getAttributes().get("DRIVERPROP");
					if ((property != null) && !property.equals("")) {
						HtmlOutputText label = (HtmlOutputText) child;
						
						label.setStyle(getMinimumTaxDriversMap().get(property)? 
								"color:red;float:left;":"float:left;");
					}
				}
				
				// recurse
				prepareTaxDriverPanel(child);
			}
		}
	}
	public void prepareTaxDriverPanelGSB(UIComponent parent) {
		if ((parent != null) && (parent.getChildCount() > 0)) {
			for (UIComponent child : parent.getChildren()) {
				if (child instanceof HtmlOutputText) {
					String property = (String) child.getAttributes().get("DRIVERPROP");
					if ((property != null) && !property.equals("")) {
						HtmlOutputText label = (HtmlOutputText) child;
						
						label.setStyle(getMinimumTaxDriversMapGSB().get(property)? 
								"color:red;float:left;":"float:left;");
					}
				}
				
				// recurse
				prepareTaxDriverPanelGSB(child);
			}
		}
	}
	public static HtmlPanelGrid getErrorHtmlPanelGrid(String id, Exception ex){
		HtmlPanelGrid parentPanel = new HtmlPanelGrid();
		parentPanel.setColumns(1);
		parentPanel.setId(id);
		parentPanel.setCellpadding("0");
		parentPanel.setCellspacing("0");
		parentPanel.setColumnClasses("column-input,column-input,column-input");

		HtmlOutputText label = new HtmlOutputText();
		label.setValue("");					
		label.setId(id+"_messagelabel1");
		label.setStyle("color:red;float:left;width:800px");
		parentPanel.getChildren().add(label);
		
		label = new HtmlOutputText();
		label.setValue("An internal issue has occurred in the application. Please email the information below to the support team at "+ ErrorBean.SUPPORT_EMAIL + ".");	
		label.setId(id+"_titlelabel");
		label.setStyle("font-size:12px;color:red;float:left;width:800px");
		parentPanel.getChildren().add(label);
		
		label = new HtmlOutputText();
		label.setValue("");					
		label.setId(id+"_messagelabel2");
		label.setStyle("color:red;float:left;width:800px");
		parentPanel.getChildren().add(label);
		
		StringWriter writer = new StringWriter();
        PrintWriter pw = new PrintWriter(writer);
        pw.println(" ");     
        fillStackTrace(ex, pw);
        
		label = new HtmlOutputText();
		label.setValue(writer.toString());					
		label.setId(id+"_messagelabel3");
		label.setStyle("color:red;float:left;width:800px");
		parentPanel.getChildren().add(label);
			
		return parentPanel;	
	}
	
	public static void fillStackTrace(Throwable ex, PrintWriter pw) {
        if (null == ex) {
            return;
        }

        ex.printStackTrace(pw);

        if (ex instanceof ServletException) {
            Throwable cause = ((ServletException) ex).getRootCause();

            if (null != cause) {
                pw.println("Root Cause:");
                fillStackTrace(cause, pw);
            }
        } else {
            Throwable cause = ex.getCause();

            if (null != cause) {
                pw.println("Cause:");
                fillStackTrace(cause, pw);
            }
        }
    }
	
	public static HtmlPanelGrid createDriverPanel(String code, CacheManager cacheManager,
			EntityItemDTO entityItemDTO,
			String id, String beanName, String objectProperty, String commonBeanProperty, 
			Boolean readOnly, Boolean requiredFields, Boolean includeDescFields,
			Boolean setupForMatrixDisplay,
			Boolean enableInactiveFields) {
		
		try{
			return createDriverPanel(code, cacheManager, entityItemDTO, id, beanName, 
				objectProperty, commonBeanProperty, readOnly, requiredFields, 
				includeDescFields, setupForMatrixDisplay, enableInactiveFields, false);
		}
		catch(Exception e){
			e.printStackTrace();
			return getErrorHtmlPanelGrid(code+"_"+id, e);
		}
	}
	
	public static HtmlPanelGrid createDriverPanel(String code, CacheManager cacheManager,
			EntityItemDTO entityItemDTO,
			String id, String beanName, String objectProperty, String commonBeanProperty, 
			Boolean readOnly, Boolean requiredFields, Boolean includeDescFields,
			Boolean setupForMatrixDisplay,
			Boolean enableInactiveFields,
			Boolean enableSplitDriversOnly) {
		HtmlPanelGrid parentPanel = new HtmlPanelGrid();
		parentPanel.setColumns(8);
		parentPanel.setId(id);
		parentPanel.setCellpadding("0");
		parentPanel.setCellspacing("0");

		if (!includeDescFields) {
			// ROWS are
			//	spacer - lbl - cntrl - lbl - cntrl - lbl - cntrl - spacer
			String input = (readOnly)? "column-input":"column-input-search";
			parentPanel.setColumnClasses("column-spacer,column-label,"+input+",column-label,"+input+",column-label,"+input+",column-spacer");
			
		} else {
			// ROWS are
			//	spacer - lbl - cntrl - desc - lbl - cntrl - desc - spacer
			String input = (readOnly)? "column-input-small":"column-input-small-search";
			parentPanel.setColumnClasses("column-spacer,column-label-gsm,"+input+",column-description-wild-null,column-label-gsm,"+input+",column-description-wild-null,column-spacer");
		}
		
		createDriverPanel(code, cacheManager,
				entityItemDTO,
				parentPanel, 
				beanName, objectProperty, commonBeanProperty, 
				readOnly, requiredFields, includeDescFields, setupForMatrixDisplay, enableInactiveFields, enableSplitDriversOnly);
		return parentPanel;
	}
	
	public void wildcardCheckChanged(ActionEvent e) {
		HtmlSelectBooleanCheckbox check = (HtmlSelectBooleanCheckbox) e.getComponent().getParent();
		String id = check.getId().replace("check", "output");
		HtmlInputText input = (HtmlInputText) check.getParent().getParent().findComponent(id);
		
		//special case for BU flag.
		if(input == null) {
			if(id != null){
				if(id.endsWith("_buflag0")) {
					String idCopy = id;
					
					//checkbox id ends with _buflag0 and input id ends with _buflag1
					id = idCopy.replaceAll("_buflag0$", "_buflag1");
					input = (HtmlInputText) check.getParent().getParent().findComponent(id);
					
					if(input == null) {
						//checkbox id ends with _buflag0 and input id ends without _buflag1 
						id = idCopy.replaceAll("_buflag0$", "");
						input = (HtmlInputText) check.getParent().getParent().findComponent(id);
					}
				}
				else {
					//checkbox id ends without _buflag0 and input ends with _buflag1
					id = id + "_buflag1";
					input = (HtmlInputText) check.getParent().getParent().findComponent(id);
				}
			}
		}
		
		Boolean value = new Boolean((String) check.getSubmittedValue());
		if(input!=null){
			if ((value != null) && value && (!"*NULL".equalsIgnoreCase((String)input.getValue()))) {
				input.setDisabled(false);
			} else {
				input.setDisabled(true);
				input.setValue(check.getAttributes().get("ORIGVALUE"));
				input.setSubmittedValue(null);
				input.setLocalValueSet(false);
			}
		}
	}
	
	public static void createDriverPanel(String code, CacheManager cacheManager,
			EntityItemDTO entityItemDTO,
			UIComponent parentPanel, 
			String beanName, String objectProperty, String commonBeanProperty,
			Boolean readOnly, Boolean requiredFields, Boolean includeDescFields,
			Boolean setupForMatrixDisplay,
			Boolean enableInactiveFields) {
		createDriverPanel(code, cacheManager, entityItemDTO, parentPanel, beanName, objectProperty, 
				commonBeanProperty, readOnly, requiredFields, includeDescFields, setupForMatrixDisplay, 
				enableInactiveFields, false);
	}
		
	@SuppressWarnings("deprecation")
	public static void createDriverPanel(String code, CacheManager cacheManager,
			EntityItemDTO entityItemDTO,
			UIComponent parentPanel, 
			String beanName, String objectProperty, String commonBeanProperty,
			Boolean readOnly, Boolean requiredFields, Boolean includeDescFields,
			Boolean setupForMatrixDisplay,
			Boolean enableInactiveFields, Boolean enableSplitDriversOnly) {
		String id = code + "_" + parentPanel.getId();
	
  	Map<String,DriverNames> transactionDriverNamesMap = cacheManager.getTransactionPropertyDriverNamesMap(code);
  	
  	Map<String,DataDefinitionColumn> transactionPropertyMap = null;//cacheManager.getDataDefinitionPropertyMap("TB_TRANSACTION_DETAIL");
	
  	if(code.equals("GSB"))
		transactionPropertyMap=	cacheManager.getDataDefinitionPropertyMap("TB_BILLTRANS");
	else{
		transactionPropertyMap=	cacheManager.getDataDefinitionPropertyMap("TB_PURCHTRANS");
	}

		
		Map<String, String> parentEntityLevelMap = cacheManager.getParentEntityLevelMap(entityItemDTO.getEntityId());
        int rowControlCount = (includeDescFields)? 2:3;
		
		int counter = 0;
		for (String transProperty : transactionDriverNamesMap.keySet()) {
			DriverNames driverNames = transactionDriverNamesMap.get(transProperty);
			String driver = driverNames.getMatrixColName();
			String driverProperty = Util.columnToProperty(driver);
			String valueProperty = (setupForMatrixDisplay)?
					driverProperty:transProperty;
			
			/*System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! get properties in createDriverPanel");
			for(String key : transactionPropertyMap.keySet()){
				System.out.println("!!!!!!!!!!! key is "+key +" maps to "+transactionPropertyMap.get(key));
			}*/
			
			String descProperty = (setupForMatrixDisplay)?
					driverProperty + "Desc":Util.columnToProperty(transactionPropertyMap.get(transProperty).getDescColumnName());
			Boolean buFlag = (requiredFields && getFlagValue(driverNames.getBusinessUnitFlag()) && getFlagValue(driverNames.getActiveFlag()));
			Boolean buFlagCheck = (!requiredFields && getFlagValue(driverNames.getBusinessUnitFlag()) && getFlagValue(driverNames.getActiveFlag()));
			Boolean wildcard = (getFlagValue(driverNames.getWildcardFlag()) && getFlagValue(driverNames.getActiveFlag()));
			Boolean nulls = (getFlagValue(driverNames.getNullDriverFlag()) && getFlagValue(driverNames.getActiveFlag()));
			
			if ((counter % rowControlCount) == 0) {
				HtmlSpacer spacer = new HtmlSpacer();
				spacer.setId(id+"_lspacer"+counter);
				parentPanel.getChildren().add(spacer);
			}
			
			// Add item
			HtmlPanelGroup grid = new HtmlPanelGroup();
			grid.setId(id+"_label_grp"+counter);
			// grid.setLayout("inline");
			
			// Always add a checkbox, but with rendered = false
			// users of panel will traverse/panel and change this
			// when checkboxes should be displayed
			if (getFlagValue(driverNames.getActiveFlag())) {
				HtmlSelectBooleanCheckbox check = new HtmlSelectBooleanCheckbox();
				check.setId(id+"_check"+counter+((buFlagCheck)? "_buflag0":""));

				if (transactionPropertyMap.get(transProperty) != null) {
					check.setLabel(transactionPropertyMap.get(transProperty).getAbbrDesc());					
				}
				check.getAttributes().put("DRIVERPROP", driverProperty);
				check.getAttributes().put("TRANSPROP", transProperty);
				check.getAttributes().put("MANDATORY", new Boolean(getFlagValue(driverNames.getMandatoryFlag())));
				check.getAttributes().put("WILDCARD", wildcard);
				check.getAttributes().put("NULLS", nulls);
				check.setStyle("display:none;float:left;");
				check.setStyleClass("check");
				
				if (wildcard) {
					HtmlAjaxSupport support = new HtmlAjaxSupport();
					support.setId(id+"_support"+counter);
					support.setEvent("onclick");
					support.addActionListener(new MethodExpressionActionListener(
							JsfHelper.createListenerExpression(
									"#{matrixCommonBean.wildcardCheckChanged}", ActionEvent.class)));
					support.setReRender(id+"_output"+counter);
					//support.setImmediate(true);
					check.getChildren().add(support);
				}
				
				grid.getChildren().add(check);
			}

			HtmlOutputText label = new HtmlOutputText();
			if (transactionPropertyMap.get(transProperty) != null) {
				
				//PP-125 added this below if condition for displaying driver no  in front  of the label in Drivers toggle panel in transaction view screen
				if("locationDriverViewPanelFromTransactioView".equals(parentPanel.getId()) || "taxDriverViewPanelFromTransactioView".equals(parentPanel.getId())){
					label.setValue(driverNames.getDriverId() + ": " + transactionPropertyMap.get(transProperty).getAbbrDesc() + ":");		
				}
				else {
					label.setValue(transactionPropertyMap.get(transProperty).getAbbrDesc() + ":");				
				}
			}
			label.setId(id+"_label"+counter);
			label.getAttributes().put("WILDCARD", wildcard);
			if (!getFlagValue(driverNames.getActiveFlag())) {
				label.setStyle("color:gray;float:left;");
			} else {
				label.setStyle(getFlagValue(driverNames.getMandatoryFlag())? 
						"color:red;float:left;":"float:left;");
				
				// Only set on tax code!
				if (code.equals(TaxMatrix.DRIVER_CODE)) {
					label.getAttributes().put("DRIVERPROP", driverProperty);
				}
				if(code.equals(GSBMatrix.DRIVER_CODE)){
					label.getAttributes().put("DRIVERPROP", driverProperty);
				}
				
			}
			grid.getChildren().add(label);
			parentPanel.getChildren().add(grid);

			//3517: if this driver is a BU driver and active, then use value from entity user is logged in under
			//if (driverNames.isBusinessUnit() && driverNames.isActive() && !(entityItemDTO.isEntityCodeAll() || entityItemDTO.getEntityId() == 0)) {
			//0001934: Company Config - Select Entity
			if (driverNames.isActive() && !(entityItemDTO.isEntityCodeAll() || entityItemDTO.getEntityId() == 0) && parentEntityLevelMap.containsKey(driverNames.getTransDtlColName())) {
				HtmlInputText output = new HtmlInputText();
				output.getAttributes().put("DONOTCLEAR", true);
				output.setId(id+"_output"+counter);
				if (transactionPropertyMap.get(transProperty) != null) {
					output.setLabel(transactionPropertyMap.get(transProperty).getAbbrDesc());					
				}
				output.setValueExpression("value", JsfHelper.createValueExpression((String)parentEntityLevelMap.get(driverNames.getTransDtlColName()), String.class));
		
				output.setDisabled(true);
				parentPanel.getChildren().add(output);
			} else if (readOnly || (!getFlagValue(driverNames.getActiveFlag()) && !enableInactiveFields)) {
				//3212: inactive drivers should be protected.
				HtmlInputText output = new HtmlInputText();
				output.setId(id+"_output"+counter);
				if (transactionPropertyMap.get(transProperty) != null) {
					output.setLabel(transactionPropertyMap.get(transProperty).getAbbrDesc());					
				}
				output.setValueExpression("value", JsfHelper.createValueExpression("#{" + beanName + "." + objectProperty + "." + valueProperty + "}", String.class));
				output.setDisabled(true);
				output.setOnkeypress("upperCaseInputTextKey(event,this)");
				output.setOnblur("upperCaseInputText(this)");
				parentPanel.getChildren().add(output);
			} else {
				HtmlPanelGroup panel = new HtmlPanelGroup();
				panel.setId(id+"_input_pnl"+counter);
				
				HtmlInputText input = new HtmlInputText();
				input.setId(id+"_input"+counter+((buFlag)? "_buflag1":""));
				input.setDisabled(requiredFields && !getFlagValue(driverNames.getActiveFlag()));
				input.getAttributes().put("DRIVER", driver);
				input.getAttributes().put("DRIVER_CODE", code);
				input.setStyle("float:left;");
				if (transactionPropertyMap.get(transProperty) != null) {
					input.setLabel(transactionPropertyMap.get(transProperty).getAbbrDesc());					
				}
				input.setValueExpression("value", JsfHelper.createValueExpression("#{" + beanName + "." + objectProperty + "." + valueProperty + "}", String.class));
				input.setOnkeypress("upperCaseInputTextKey(event,this)");
				input.setOnblur("upperCaseInputText(this)");				
				if(readOnly){
					input.setDisabled(true);
				}
				panel.getChildren().add(input);
				
				if(!readOnly){
					HtmlAjaxCommandButton btn = new HtmlAjaxCommandButton();
					btn.setId(id+"_btn"+counter+((buFlag)? "_buflag2":""));
					btn.setDisabled(requiredFields && !getFlagValue(driverNames.getActiveFlag()));
					btn.getAttributes().put("DRIVER", driver);
					btn.getAttributes().put("DRIVER_CODE", code);
					btn.getAttributes().put("DESC", includeDescFields);
					btn.getAttributes().put("ID", parentPanel.getId());
					btn.getAttributes().put("RERENDER", 
							(id+"_input"+counter+((buFlag)? "_buflag1":"")) + 
							((includeDescFields)? (","+id+"_desc"+counter+((buFlag)? "_buflag3":"")):""));
					btn.setImage("/images/search_small.png");
					btn.setStyle("float:left;width:16px;height:16px;");
					btn.setStyleClass("image");
					//btn.setImmediate(true);
					
					//PP-177, Create New Driver Search screen
					//btn.setOncomplete("javascript:Richfaces.showModalPanel('driverSearch');");
					//btn.setReRender("driverSearchForm");
					btn.addActionListener(new MethodExpressionActionListener(
							JsfHelper.createListenerExpression(
									"#{" + beanName + ".searchDriver}", ActionEvent.class)));					
					btn.setAction(FacesContext.getCurrentInstance().getApplication().createMethodBinding("#{driverHandlerBean.displayDriverSearch}", null));
					
					panel.getChildren().add(btn);
				}
				
				parentPanel.getChildren().add(panel);
			}

			if (includeDescFields) {
				HtmlPanelGroup descwildnullgrid = new HtmlPanelGroup(); 
				descwildnullgrid.setId(id+"_descwildnull"+counter);
				
				HtmlInputText output = new HtmlInputText();
				output.setId(id+"_desc"+counter+((buFlag)? "_buflag3":""));
				output.setDisabled(true);
				if ((descProperty != null) && !descProperty.equals("")) {
					output.setValueExpression("value", JsfHelper.createValueExpression("#{" + beanName + "." + objectProperty + "." + descProperty + "}", String.class));
				}
				descwildnullgrid.getChildren().add(output);
				if(wildcard) {
					HtmlOutputText wildcardlabel = new HtmlOutputText();
					wildcardlabel.setValue(WILDCARD_LABEL);				
					wildcardlabel.setId(id+"_wildcardlabel"+counter);
					wildcardlabel.setStyle("display: inline-block;width:12px;font-size: 12px;text-align: center;background-color:#00b300;");
					descwildnullgrid.getChildren().add(wildcardlabel);
					
				}
				
				if(nulls) {
					HtmlOutputText nullslabel = new HtmlOutputText();
					nullslabel.setId(id+"_nullslabel"+counter);
					nullslabel.setValue(NULLS_LABEL);				
					nullslabel.setStyle("display: inline-block;width:12px;font-size: 12px;text-align: center;background-color:#ff0080;");
					descwildnullgrid.getChildren().add(nullslabel);
				}
				parentPanel.getChildren().add(descwildnullgrid);
			}
			counter++;
			
			if ((counter % rowControlCount) == 0) {
				HtmlSpacer spacer = new HtmlSpacer();
				spacer.setId(id+"_rspacer"+counter);
				parentPanel.getChildren().add(spacer);
			}
		}

		// Finish the row
		if ((counter % rowControlCount) != 0) {
			HtmlSpacer spacer;
			
			while ((counter % rowControlCount) != 0) {
				spacer = new HtmlSpacer();
				spacer.setId(id+"_lblspacer"+counter);
				parentPanel.getChildren().add(spacer);
				
				spacer = new HtmlSpacer();
				spacer.setId(id+"_inpspacer"+counter);
				parentPanel.getChildren().add(spacer);
				
				if (includeDescFields) {
					spacer = new HtmlSpacer();
					spacer.setId(id+"_descpspacer"+counter);
					parentPanel.getChildren().add(spacer);
				}
				
				counter++;
			}
			
			spacer = new HtmlSpacer();
			spacer.setId(id+"_rspacer"+counter);
			parentPanel.getChildren().add(spacer);
		}
	}
	
	static public boolean getFlagValue(String flag) {
		return ((flag != null) && flag.equals("1"));
	}
	
	public List<SelectItem> getTaxCodeStateItems() {
			taxCodeStateItems = new ArrayList<SelectItem>();
			taxCodeStateItems.add(new SelectItem("","State"));
			for (TaxCodeStateDTO tcs : cacheManager.getTaxCodeStateMap().values()) {
				// Only add states that are active
				String active = tcs.getActiveFlag();
				if ((active != null) && active.equals("1")) {
					taxCodeStateItems.add(new SelectItem(tcs.getTaxCodeState(), tcs.getName()));
				}
			}
			  
		return taxCodeStateItems;
	}
	
	public List<SelectItem> getAllEntityItems() {
		if(allEntityItems==null){
			allEntityItems = new ArrayList<SelectItem>();
			allEntityItems.add(new SelectItem("","Select Entity"));
	    	List<EntityItem> eList = entityItemService.findAllEntityItems();//source list   
		    if(eList != null && eList.size() > 0){
		    	for(EntityItem entityItem:eList){
		    		allEntityItems.add(new SelectItem(entityItem.getEntityCode(),entityItem.getEntityCode() + " - " + entityItem.getEntityName()));
		    	}
		    } 	
		}
    
    	return allEntityItems;
	}

	public List<SelectItem> getTaxCodeTypeItems() {
			taxCodeTypeItems = new ArrayList<SelectItem>();
			taxCodeTypeItems.add(new SelectItem("","Type"));
			List<ListCodes> codes = cacheManager.getListCodesByType("TCTYPE"); 
			if (codes != null) {
				for (ListCodes code : codes) {
					taxCodeTypeItems.add(new SelectItem(code.getCodeCode(), code.getDescription()));
				}
			}
		return taxCodeTypeItems;
	}
	
	public List<SelectItem> getCchTaxCatItems() {

			cchTaxCatItems = new ArrayList<SelectItem>();
			cchTaxCatItems.add(new SelectItem("","CCH Tax Category"));
			for (CCHCode code : cacheManager.getCchCodeMap().values()) {
				cchTaxCatItems.add(new SelectItem(code.getCodePK().getCode(), 
						code.getCodePK().getCode() + " - " + code.getDescription()));
			}

		  
		return cchTaxCatItems;
	}

	public List<SelectItem> getCchTaxGrpItems() {
	
			cchTaxGrpItems = new ArrayList<SelectItem>();
			cchTaxGrpItems.add(new SelectItem("","CCH Tax Group"));
			for (CCHTaxMatrix code : cacheManager.getCchGroupMap().values()) {
				cchTaxGrpItems.add(new SelectItem(code.getCchTaxMatrixPK().getGroupCode(), 
						code.getCchTaxMatrixPK().getGroupCode() + " - " + code.getGroupDesc()));
			}
		
		
		return cchTaxGrpItems;
	}

	public List<SelectItem> getRelationItems() {
	
			// Initialize relation menu
			relationItems = new ArrayList<SelectItem>();
			List<ListCodes> codes = cacheManager.getListCodesByType("RELCODE"); 
			if (codes != null) {
				for (ListCodes code : codes) {
					relationItems.add(new SelectItem(code.getCodeCode(), code.getDescription()));
				}
			}
		
		
		return relationItems;
	}

	public List<SelectItem> getDefaultLineItems() {
		return defaultLineItems;
	}
	
	public List<SelectItem> getGlobalCompnyNumberItems() {
		return globalCompanyNumberItems;
	}

	
	public List<SelectItem> getActiveItems() {
		return activeItems;
	}

	public void setActiveItems(List<SelectItem> activeItems) {
		this.activeItems = activeItems;
	}

	
	public Locale getLocale() {
		return locale;
	}

	public void setLocale(Locale locale) {
		this.locale = locale;
	}
	
	public void createTransactionStatisticsTable(
			HtmlDataTable table, String beanName, String dataModel,
			Map<String,DataDefinitionColumn> propertyMap, boolean allColumns) {

		// Remove existing columns
		table.getChildren().clear();
		
		Set<String> fixedColumns = new LinkedHashSet<String>();
		fixedColumns.add("processStatus");
		fixedColumns.add("purchtransId");
		
		// Add in all of the driver columns
		Map<String,DriverNames> transactionDriverNamesMap = 
			cacheManager.getTransactionPropertyDriverNamesMap(TaxMatrix.DRIVER_CODE);
		
		for (String property : transactionDriverNamesMap.keySet()) {
			fixedColumns.add(property);
		}
		//transactionDriverNamesMap=cacheManager.getTransactionPropertyDriverNamesMap(GSBMatrix.DRIVER_CODE);

		//for (String property : transactionDriverNamesMap.keySet()) {
			//fixedColumns.add(property);
		//}
		transactionDriverNamesMap = 
			cacheManager.getTransactionPropertyDriverNamesMap(LocationMatrix.DRIVER_CODE);

		for (String property : transactionDriverNamesMap.keySet()) {
			fixedColumns.add(property);
		}
		
		for (String property : propertyMap.keySet()) {
			DataDefinitionColumn ddc = propertyMap.get(property);
			if (getFlagValue(ddc.getMinimumViewFlag())) {
				fixedColumns.add(property);
			}
		}
		
		Set<String> excludeColumns = new LinkedHashSet<String>();
		excludeColumns.add("id");
		excludeColumns.add("idPropertyName");
		excludeColumns.add("class");
		excludeColumns.add("auditFlag");
		excludeColumns.add("auditUserId");
		excludeColumns.add("auditTimestamp");
		excludeColumns.add("glLogFlag");
		excludeColumns.add("glExtractUpdater");
		excludeColumns.add("glExtractTimestamp");
		//CCH		excludeColumns.add("cchTaxcatCode");
		//CCH		excludeColumns.add("cchGroupCode");
		//CCH		excludeColumns.add("cchItemCode");
		excludeColumns.add("modifyUserId");
		excludeColumns.add("modifyTimestamp");
		
		createDynamicTableStatistics(PurchaseTransaction.class, 
				table, beanName, dataModel, 
				fixedColumns, excludeColumns, null, null, 
				propertyMap, allColumns, false);
	}
	
	public void createViewTransactionSortMap(
			Map<String, String> sortMap, String beanName, String dataModel,
			Map<String,DataDefinitionColumn> propertyMap, 
			boolean multiSelect, String viewName, String userName) {
		
		Set<String> fixedColumns = getViewFixedColumns(viewName, userName, propertyMap);	
		for (String field : fixedColumns) {
			if(isSortable(null, null, field)){
				sortMap.put(field, getLabelForField(propertyMap, field));
			}
		}
	}
	
	public void createTransactionSortMap(
			Map<String, String> sortMap, String beanName, String dataModel,
			Map<String,DataDefinitionColumn> propertyMap, 
			boolean multiSelect, boolean allColumns) {

		Set<String> fixedColumns = new LinkedHashSet<String>();
		fixedColumns.add("processStatus");
		fixedColumns.add("transactionDetailId");
		
	
		// Add in all of the driver columns
		Map<String,DriverNames> transactionDriverNamesMap = 
			cacheManager.getTransactionPropertyDriverNamesMap(TaxMatrix.DRIVER_CODE);
		
		for (String property : transactionDriverNamesMap.keySet()) {
			fixedColumns.add(property);
		}
		
		transactionDriverNamesMap = 
			cacheManager.getTransactionPropertyDriverNamesMap(LocationMatrix.DRIVER_CODE);

		for (String property : transactionDriverNamesMap.keySet()) {
			fixedColumns.add(property);
		}
		
		for (String property : propertyMap.keySet()) {
			DataDefinitionColumn ddc = propertyMap.get(property);
			if (getFlagValue(ddc.getMinimumViewFlag())) {
				fixedColumns.add(property);
			}
		}
		
		Set<String> excludeColumns = new LinkedHashSet<String>();
		excludeColumns.add("id");
		excludeColumns.add("idPropertyName");
		excludeColumns.add("class");
		excludeColumns.add("comments");

		createDynamicSort(PurchaseTransaction.class, 
				sortMap, beanName, dataModel, 
				fixedColumns, excludeColumns, null, null, 
				propertyMap, allColumns, multiSelect);
	}
	
	static public void createDynamicSort(Class<?> clazz, Map<String, String> sortMap, 
			String beanName, String dataModel, 
			Collection<String> fixedColumns, Collection<String> excludeColumns, 
			Collection<String> sortableColumns, Collection<String> nonsortableColumns,
			Map<String,DataDefinitionColumn> propertyMap,
			boolean allColumns, boolean multiSelect) {
		
		int sumCounter = 1;
		for (String field : fixedColumns) {
			if(isSortable(sortableColumns, nonsortableColumns, field)){
				sortMap.put(field, getLabelForField(propertyMap, field));
			}
		}

		// Conditionally add in remaining columns
		if (allColumns) {
			String trFields[] = fieldArray(); 
			
			for (String field : trFields) {			
				if (!containsValue(excludeColumns, field) &&
					!containsValue(fixedColumns, field)) {
					if(isSortable(sortableColumns, nonsortableColumns, field)){
						sortMap.put(field, getLabelForField(propertyMap, field));
					}
				}
			}
		}
	}
	
	public Set<String> getBCPDefaultColumns(){
		Map<String,DataDefinitionColumn> propertyMap =	getBCPTransactionPropertyMap();
		Set<String> fixedColumns = new LinkedHashSet<String>();
		
		fixedColumns.add("bcpTransactionId");
		fixedColumns.add("processBatchNo");
		fixedColumns.add("multiTransCodeDesc");
		
		// Add in all of the driver columns
		Map<String,DriverNames> transactionDriverNamesMap = 
			cacheManager.getTransactionPropertyDriverNamesMap(TaxMatrix.DRIVER_CODE);
		
		for (String property : transactionDriverNamesMap.keySet()) {
			fixedColumns.add(property);
		}
		
		transactionDriverNamesMap = 
			cacheManager.getTransactionPropertyDriverNamesMap(LocationMatrix.DRIVER_CODE);

		for (String property : transactionDriverNamesMap.keySet()) {
			fixedColumns.add(property);
		}
		
		for (String property : propertyMap.keySet()) {
			DataDefinitionColumn ddc = propertyMap.get(property);
			if (getFlagValue(ddc.getMinimumViewFlag())) {
				fixedColumns.add(property);
			}
		}
		
		fixedColumns.add("glLineItmDistAmt");
		fixedColumns.add("invoiceFreightAmt");
		fixedColumns.add("invoiceDiscountAmt");
		
		return fixedColumns;
	}
	
	public Set<String> getDefaultColumns(){
		return getDefaultColumns(null);
	}
	
	public Set<String> getDefaultColumns(String[] includeColumns){
		Map<String,DataDefinitionColumn> propertyMap =	getTransactionPropertyMap();
		Set<String> fixedColumns = new LinkedHashSet<String>();
		if(includeColumns != null) {
			for(String c : includeColumns) {
				fixedColumns.add(c);
			}
		}
		fixedColumns.add("processStatus");
		fixedColumns.add("purchtransId");
		fixedColumns.add("multiTransCodeDesc");
		
		// Add in all of the driver columns
		Map<String,DriverNames> transactionDriverNamesMap = 
			cacheManager.getTransactionPropertyDriverNamesMap(TaxMatrix.DRIVER_CODE);
		
		for (String property : transactionDriverNamesMap.keySet()) {
			fixedColumns.add(property);
		}
		
		transactionDriverNamesMap = 
			cacheManager.getTransactionPropertyDriverNamesMap(LocationMatrix.DRIVER_CODE);

		for (String property : transactionDriverNamesMap.keySet()) {
			fixedColumns.add(property);
		}
	
		for (String property : propertyMap.keySet()) {
			DataDefinitionColumn ddc = propertyMap.get(property);
			if (getFlagValue(ddc.getMinimumViewFlag())) {
				fixedColumns.add(property);
			}
		}
		
		fixedColumns.add("glLineItmDistAmt");
		fixedColumns.add("splitSubtransId");
		fixedColumns.add("taxAllocMatrixId");
		
		return fixedColumns;
	}
	
	public Set<String> getFixedColumns(boolean reOrder, boolean allColumns, boolean saveNow, boolean fromTransaction){
		return getFixedColumns(reOrder, allColumns, saveNow, fromTransaction, null);
	}

	// Bug 3987, method newly added (some codes already exists moved to this method)
	public Set<String> getFixedColumns(boolean reOrder, boolean allColumns, boolean saveNow, boolean fromTransaction, String[] includeColumns){
		List<String> fixedList = new ArrayList<String>();
		fixedList = transactionDetailService.getColumnHeaders(loginBean.getUserDTO().getUserCode(), "N", WINDOW_NAME_TRANSACTION_PROCESS, null);
		syncHeaderColumns(fixedList, getDefaultColumns());
		
		List<String> allList = new ArrayList<String>();
		allList = transactionDetailService.getColumnHeaders(loginBean.getUserDTO().getUserCode(), "Y", WINDOW_NAME_TRANSACTION_PROCESS, null);
		syncHeaderColumns(allList, new HashSet<String>(Arrays.asList(fieldArray())));
		
		Set<String> fixedColumns = new LinkedHashSet<String>();
		Map<String,DataDefinitionColumn> propertyMap =	getTransactionPropertyMap();
		
		if(neverSaveFixedList.size()>0){
			for(String temp:neverSaveFixedList){
				fixedColumns.add(temp);
			}
			
			
		}else{
		if(fixedList.size()>0 && !allColumns && fromTransaction){
			// if the user have saved the column order
			
			for(String header:fixedList){
				fixedColumns.add(header);
			}
		}
		else {
			fixedColumns = getDefaultColumns(includeColumns);
		}
		}

		// when re-arrange button clicked
		if(reOrder){

			if(allColumns && saveNow){
				List<TransactionHeader> insertList = new ArrayList<TransactionHeader>();
				
				int i=0;
				for(TransactionHeader transactionHeader:forHeaderRearrange){
					TransactionHeader header = new TransactionHeader();
					header.setUserCode(loginBean.getUserDTO().getUserCode());
					header.setColumnName(transactionHeader.getColumnName());
					header.setWindowName(WINDOW_NAME_TRANSACTION_PROCESS);
					header.setColumnId(++i);
					header.setAllColumns("Y");
					insertList.add(header);
				}
				
				transactionDetailService.saveColumns(loginBean.getUserDTO().getUserCode(), "Y", WINDOW_NAME_TRANSACTION_PROCESS, null, insertList);
			}else {
				fixedColumns = new LinkedHashSet<String>();
				
				List<TransactionHeader> insertList = new ArrayList<TransactionHeader>();
				
				int i = 0;
				for (TransactionHeader temp : forHeaderRearrange) {
					fixedColumns.add(temp.getColumnName());
					// save the column orders to db only as per user preference
					if(saveNow){
						TransactionHeader header = new TransactionHeader();
						header.setUserCode(loginBean.getUserDTO().getUserCode());
						header.setColumnName(temp.getColumnName());
						header.setWindowName(WINDOW_NAME_TRANSACTION_PROCESS);
						header.setColumnId(++i);
						header.setAllColumns("N");
						insertList.add(header);
					}
				}
				
				if(saveNow) {
					transactionDetailService.saveColumns(loginBean.getUserDTO().getUserCode(), "N", WINDOW_NAME_TRANSACTION_PROCESS, null, insertList);
				}
			}
			if(allColumns && !saveNow){
				neverSaveAllList = new ArrayList<String>();
				for (TransactionHeader trh:forHeaderRearrange){
					neverSaveAllList.add(trh.getColumnName());
				}
				
			}
			if(!allColumns && !saveNow){
				neverSaveFixedList = new ArrayList<String>();
				for (TransactionHeader trh:forHeaderRearrange){
					neverSaveFixedList.add(trh.getColumnName());
				}
			}
		}
		// if the all column check box checked, to add all columns to re order panel
		if(allColumns){
			if(saveNow && allList.size()>0){
				allList.clear();
				allList = transactionDetailService.getColumnHeaders(loginBean.getUserDTO().getUserCode(), "Y", WINDOW_NAME_TRANSACTION_PROCESS, null);
				syncHeaderColumns(allList, new HashSet<String>(Arrays.asList(fieldArray())));
				
	}
		 


			if(allList.size()>0){
				forHeaderRearrange = new ArrayList<TransactionHeader>();
				for(String temp:allList){
					TransactionHeader header = new TransactionHeader();
					header.setColumnName(temp);
					header.setColumnLabel(getLabelForField(propertyMap, temp));
					forHeaderRearrange.add(header);
				}
				
			}
			if(allList.size()==0 && forHeaderRearrange.size()<36 && neverSaveAllList.size()==0) {
			forHeaderRearrange = new ArrayList<TransactionHeader>();
			for(String temp:fieldArray()){
				TransactionHeader header = new TransactionHeader();
				header.setColumnName(temp);
				header.setColumnLabel(getLabelForField(propertyMap, temp));
				forHeaderRearrange.add(header);
		}
			
		}
			if(neverSaveAllList.size()>0){
				forHeaderRearrange = new ArrayList<TransactionHeader>();
				for(String temp:neverSaveAllList){
					TransactionHeader header = new TransactionHeader();
					header.setColumnName(temp);
					header.setColumnLabel(getLabelForField(propertyMap, temp));
					forHeaderRearrange.add(header);
			}
			}
			
			// when the process transaction page loaded for first time
		}else {
			
			
			forHeaderRearrange = new ArrayList<TransactionHeader>();
			for (String temp:fixedColumns) {
				TransactionHeader header = new TransactionHeader();
				header.setColumnName(temp);
				header.setColumnLabel(getLabelForField(propertyMap, temp));
				forHeaderRearrange.add(header);
									
			}
			
			
		}

			
	
	
			
		
		return fixedColumns;
	}
	
	public void createBCPSaleTransactionDetailTable(
			HtmlDataTable table, String beanName, String dataModel,
			Map<String,DataDefinitionColumn> propertyMap, 
			boolean multiSelect, boolean allColumns, boolean reOrder, boolean saveNow, boolean fromTransaction) {
	
		table.getChildren().clear();
		
		Set<String> fixedColumns = getBCPSaleFixedColumns();
		
		if(allColumns) {
			List<String> allfields = new ArrayList<String>();
			try {
				for(Field f : BCPBillTransaction.class.getDeclaredFields()) {
					if(f.getAnnotation(Column.class) != null) {
						allfields.add(f.getName());
					}
				}
			}
			catch(Exception e) {
				logger.error(e);
			}
			
			forHeaderRearrange = new ArrayList<TransactionHeader>();
			for(String temp : allfields){
				TransactionHeader header = new TransactionHeader();
				header.setColumnName(temp);
				header.setColumnLabel(getLabelForField(propertyMap, temp));
				forHeaderRearrange.add(header);
			}
		}
		
		createDynamicTable(BCPBillTransaction.class, 
				table, beanName, dataModel, 
				fixedColumns, new LinkedHashSet<String>() , null, null, 
				propertyMap, allColumns, multiSelect, null);
	}
	
	public void createBCPPurchaseTransactionTable(
			HtmlDataTable table, String beanName, String dataModel,
			Map<String,DataDefinitionColumn> propertyMap, 
			boolean multiSelect, boolean allColumns, boolean reOrder, boolean saveNow, boolean fromTransaction) {
	
		table.getChildren().clear();
		
		Set<String> fixedColumns = getBCPDefaultColumns();
		
		if(allColumns) {
			List<String> allfields = new ArrayList<String>();
			try {
				for(Field f : BCPPurchaseTransaction.class.getDeclaredFields()) {
					if(f.getAnnotation(Column.class) != null) {
						allfields.add(f.getName());
					}
				}
			}
			catch(Exception e) {
				logger.error(e);
			}
			
			forHeaderRearrange = new ArrayList<TransactionHeader>();
			for(String temp : allfields){
				TransactionHeader header = new TransactionHeader();
				header.setColumnName(temp);
				header.setColumnLabel(getLabelForField(propertyMap, temp));
				forHeaderRearrange.add(header);
			}
		}
		
		createDynamicTable(BCPPurchaseTransaction.class, 
				table, beanName, dataModel, 
				fixedColumns, new LinkedHashSet<String>() , null, null, 
				propertyMap, allColumns, multiSelect, null);
	}

	public Set<String> getBCPSaleFixedColumns(){
		Set<String> fixedColumns = new LinkedHashSet<String>();		
		fixedColumns.add("bcpSaletransId");
		fixedColumns.add("processBatchNo");	
		
		Set<String> saleFixedColumns = getSaleFixedColumns();		
		fixedColumns.addAll(saleFixedColumns);
		
		return fixedColumns;
	}
	
	public Set<String> getSaleFixedColumns(){
		Set<String> fixedColumns = new LinkedHashSet<String>();		

		fixedColumns.add("saletransId");
		fixedColumns.add("companyNbr");
		fixedColumns.add("divisionNbr");
		fixedColumns.add("custNbr");
		fixedColumns.add("transactionTypeCode");
		fixedColumns.add("ratetypeCode");
		fixedColumns.add("methodDeliveryCode");
		fixedColumns.add("glDate");
		fixedColumns.add("invoiceNbr");
		fixedColumns.add("invoiceGrossAmt");
		fixedColumns.add("invoiceFreightAmt");
		fixedColumns.add("invoiceDiscAmt");
		fixedColumns.add("invoiceDiscTypeCode");
		fixedColumns.add("invoiceLineNbr");
		fixedColumns.add("invoiceLineDesc");
		fixedColumns.add("invoiceLineGrossAmt");
		fixedColumns.add("invoiceLineItemAmt");
		fixedColumns.add("invoiceLineItemCount");
		fixedColumns.add("invoiceLineFreightAmt");
		fixedColumns.add("invoiceLineDiscAmt");
		fixedColumns.add("invoiceLineDiscTypeCode");
		fixedColumns.add("taxcodeCode");
		fixedColumns.add("exemptAmt");
		fixedColumns.add("exemptCode");
		fixedColumns.add("exemptInd");
		fixedColumns.add("exemptReason");
		fixedColumns.add("taxPaidToVendorFlag");
		fixedColumns.add("certNbr");
		fixedColumns.add("shipfromGeocode");
		fixedColumns.add("shipfromCity");
		fixedColumns.add("shipfromCounty");
		fixedColumns.add("shipfromStateCode");
		fixedColumns.add("shipfromCountryCode");
		fixedColumns.add("shipfromZip");
		fixedColumns.add("shipfromZipplus4");
		fixedColumns.add("shipfromEntityCode");
		fixedColumns.add("shipfromJurisdictionId");
		fixedColumns.add("shiptoGeocode");
		fixedColumns.add("shiptoCity");
		fixedColumns.add("shiptoCounty");
		fixedColumns.add("shiptoStateCode");
		fixedColumns.add("shiptoCountryCode");
		fixedColumns.add("shiptoZip");
		fixedColumns.add("shiptoZipplus4");
		fixedColumns.add("shiptoEntityCode");
		fixedColumns.add("shiptoJurisdictionId");
		fixedColumns.add("countryCode");
		fixedColumns.add("countryExemptAmt");
		fixedColumns.add("countryTaxableAmt");
		fixedColumns.add("countryRate");
		fixedColumns.add("countryTaxAmt");
		fixedColumns.add("countryTaxtypeCode");
		fixedColumns.add("stateCode");
		fixedColumns.add("stateExemptAmt");
		fixedColumns.add("stateTaxableAmt");
		fixedColumns.add("stateRate");
		fixedColumns.add("stateTaxtypeCode");
		fixedColumns.add("stateTier1TaxAmt");
		fixedColumns.add("stateTier2TaxAmt");
		fixedColumns.add("stateTier3TaxAmt");
		fixedColumns.add("countyName");
		fixedColumns.add("countyExemptAmt");
		fixedColumns.add("countyTaxableAmt");
		fixedColumns.add("countyRate");
		fixedColumns.add("countyTaxAmt");
		fixedColumns.add("countyTaxtypeCode");
		fixedColumns.add("countySplitAmount");
		fixedColumns.add("countyMaxtaxAmount");
		fixedColumns.add("cityName");
		fixedColumns.add("cityExemptAmt");
		fixedColumns.add("cityTaxableAmt");
		fixedColumns.add("cityRate");
		fixedColumns.add("cityTaxAmt");
		fixedColumns.add("cityTaxtypeCode");
		fixedColumns.add("citySplitAmount");
		fixedColumns.add("citySplitRate");
		fixedColumns.add("stj1Name");
		fixedColumns.add("stj1ExemptAmt");
		fixedColumns.add("stj1TaxableAmt");
		fixedColumns.add("stj1Rate");
		fixedColumns.add("stj1TaxAmt");
		fixedColumns.add("stj1TaxtypeCode");
		fixedColumns.add("stj2Name");
		fixedColumns.add("stj2ExemptAmt");
		fixedColumns.add("stj2TaxableAmt");
		fixedColumns.add("stj2Rate");
		fixedColumns.add("stj2TaxAmt");
		fixedColumns.add("stj2TaxtypeCode");
		fixedColumns.add("stj3Name");
		fixedColumns.add("stj3ExemptAmt");
		fixedColumns.add("stj3TaxableAmt");
		fixedColumns.add("stj3Rate");
		fixedColumns.add("stj3TaxAmt");
		fixedColumns.add("stj3TaxtypeCode");
		fixedColumns.add("stj4Name");
		fixedColumns.add("stj4ExemptAmt");
		fixedColumns.add("stj4TaxableAmt");
		fixedColumns.add("stj4Rate");
		fixedColumns.add("stj4TaxAmt");
		fixedColumns.add("stj4TaxtypeCode");
		fixedColumns.add("stj5Name");
		fixedColumns.add("stj5ExemptAmt");
		fixedColumns.add("stj5TaxableAmt");
		fixedColumns.add("stj5Rate");
		fixedColumns.add("stj5TaxAmt");
		fixedColumns.add("stj5TaxtypeCode");
		fixedColumns.add("taxableAmt");
		fixedColumns.add("combinedRate");
		fixedColumns.add("taxAmt");
		fixedColumns.add("relatedSubtransId");		
		fixedColumns.add("fatalErrorCode");
		
		return fixedColumns;
	}
	
	public Set<String> getBillFixedColumns(){
		Set<String> fixedColumns = new LinkedHashSet<String>();		

		fixedColumns.add("billtransId");
		fixedColumns.add("glCompanyNbr");
		fixedColumns.add("glDivisionNbr");
		fixedColumns.add("custNbr");
		fixedColumns.add("transactionTypeCode");
		fixedColumns.add("ratetypeCode");
		fixedColumns.add("methodDeliveryCode");
		fixedColumns.add("glDate");
		fixedColumns.add("invoiceNbr");
		fixedColumns.add("invoiceGrossAmt");
		fixedColumns.add("invoiceFreightAmt");
		fixedColumns.add("invoiceDiscountAmt");//fixedColumns.add("invoiceDiscAmt");
		fixedColumns.add("invoiceDiscTypeCode");
		fixedColumns.add("invoiceLineNbr");
		fixedColumns.add("invoiceLineName");
		fixedColumns.add("invoiceLineGrossAmt");
		fixedColumns.add("invoiceLineAmt");
		fixedColumns.add("invoiceLineCount");
		fixedColumns.add("invoiceLineFreightAmt");
		fixedColumns.add("invoiceLineDiscAmt");
		fixedColumns.add("invoiceLineDiscTypeCode");
		fixedColumns.add("invoiceLineProductCode");
		fixedColumns.add("taxcodeCode");
		fixedColumns.add("exemptAmt");
		fixedColumns.add("exemptCode");
		fixedColumns.add("exemptInd");
		fixedColumns.add("exemptReason");
		fixedColumns.add("taxPaidToVendorFlag");
		fixedColumns.add("certNbr");
		fixedColumns.add("shipfromGeocode");
		fixedColumns.add("shipfromCity");
		fixedColumns.add("shipfromCounty");
		fixedColumns.add("shipfromStateCode");
		fixedColumns.add("shipfromCountryCode");
		fixedColumns.add("shipfromZip");
		fixedColumns.add("shipfromZipplus4");
		fixedColumns.add("shipfromEntityCode");
		fixedColumns.add("shipfromJurisdictionId");
		fixedColumns.add("shiptoGeocode");
		fixedColumns.add("shiptoCity");
		fixedColumns.add("shiptoCounty");
		fixedColumns.add("shiptoStateCode");
		fixedColumns.add("shiptoCountryCode");
		fixedColumns.add("shiptoZip");
		fixedColumns.add("shiptoZipplus4");
		fixedColumns.add("shiptoEntityCode");
		fixedColumns.add("shiptoJurisdictionId");
		fixedColumns.add("countryCode");
		fixedColumns.add("countryExemptAmt");
		fixedColumns.add("countryTier1TaxableAmt");//fixedColumns.add("countryTaxableAmt");
		//fixedColumns.add("countryRate");
		fixedColumns.add("countryTier1TaxAmt");//fixedColumns.add("countryTaxAmt");
		fixedColumns.add("countryTaxtypeUsedCode");
		fixedColumns.add("stateCode");
		fixedColumns.add("stateExemptAmt");
		//fixedColumns.add("stateTaxableAmt");
		//fixedColumns.add("stateRate");
		fixedColumns.add("stateTier1TaxableAmt");//fixedColumns.add("stateTaxableAmt");
		fixedColumns.add("stateTaxtypeUsedCode");
		fixedColumns.add("stateTier1TaxAmt");
		fixedColumns.add("stateTier2TaxAmt");
		fixedColumns.add("stateTier3TaxAmt");
		fixedColumns.add("countyName");
		fixedColumns.add("countyExemptAmt");
		fixedColumns.add("countyTier1TaxableAmt");//fixedColumns.add("countyTaxableAmt");
		//fixedColumns.add("countyRate");
		fixedColumns.add("countyTier1TaxAmt");//fixedColumns.add("countyTaxAmt");
		fixedColumns.add("countyTaxtypeUsedCode");
		//fixedColumns.add("countySplitAmount");
		//fixedColumns.add("countyMaxtaxAmount");
		fixedColumns.add("cityName");
		fixedColumns.add("cityExemptAmt");
		fixedColumns.add("cityTier1TaxableAmt"); //fixedColumns.add("cityTaxableAmt");
		//fixedColumns.add("cityRate");
		fixedColumns.add("cityTier1TaxAmt"); //fixedColumns.add("cityTaxAmt");
		fixedColumns.add("cityTaxtypeUsedCode");
		//fixedColumns.add("citySplitAmount");
		//fixedColumns.add("citySplitRate");
		fixedColumns.add("stj1Name");
		fixedColumns.add("stj1ExemptAmt");
		fixedColumns.add("stj1TaxableAmt");
		fixedColumns.add("stj1Rate");
		fixedColumns.add("stj1TaxAmt");
		fixedColumns.add("stj1TaxtypeUsedCode");
		fixedColumns.add("stj2Name");
		fixedColumns.add("stj2ExemptAmt");
		fixedColumns.add("stj2TaxableAmt");
		fixedColumns.add("stj2Rate");
		fixedColumns.add("stj2TaxAmt");
		fixedColumns.add("stj2TaxtypeUsedCode");
		fixedColumns.add("stj3Name");
		fixedColumns.add("stj3ExemptAmt");
		fixedColumns.add("stj3TaxableAmt");
		fixedColumns.add("stj3Rate");
		fixedColumns.add("stj3TaxAmt");
		fixedColumns.add("stj3TaxtypeUsedCode");
		fixedColumns.add("stj4Name");
		fixedColumns.add("stj4ExemptAmt");
		fixedColumns.add("stj4TaxableAmt");
		fixedColumns.add("stj4Rate");
		fixedColumns.add("stj4TaxAmt");
		fixedColumns.add("stj4TaxtypeUsedCode");
		fixedColumns.add("stj5Name");
		fixedColumns.add("stj5ExemptAmt");
		fixedColumns.add("stj5TaxableAmt");
		fixedColumns.add("stj5Rate");
		fixedColumns.add("stj5TaxAmt");
		fixedColumns.add("stj5TaxtypeUsedCode");
		fixedColumns.add("taxableAmt");
		fixedColumns.add("combinedRate");
		fixedColumns.add("tbCalcTaxAmt");
		fixedColumns.add("relatedSubtransId");		
		fixedColumns.add("fatalErrorCode");
		
		return fixedColumns;
	}
	
	public void createSaleTransactionDetailTable(
		HtmlDataTable table, String beanName, String dataModel,
		Map<String,DataDefinitionColumn> propertyMap, 
		boolean multiSelect, boolean allColumns, boolean reOrder, boolean saveNow, boolean fromTransaction) {

		table.getChildren().clear();
		
		Set<String> fixedColumns = getSaleFixedColumns();			

		Set<String> excludeColumns = new LinkedHashSet<String>();
		excludeColumns.add("id");
		excludeColumns.add("idPropertyName");
		excludeColumns.add("class");
		excludeColumns.add("comments");
		excludeColumns.add("countrySitusMatrixId");
		excludeColumns.add("stateSitusMatrixId");
		excludeColumns.add("countySitusMatrixId");
		excludeColumns.add("citySitusMatrixId");
		excludeColumns.add("stj1SitusMatrixId");
		excludeColumns.add("stj2SitusMatrixId");
		excludeColumns.add("stj3SitusMatrixId");
		excludeColumns.add("stj4SitusMatrixId");
		excludeColumns.add("stj5SitusMatrixId");
		excludeColumns.add("transactionInd");
		int sumCounter = 1;
		
		// Add "fixed" columns
		if (multiSelect) {
			// Multi-select
			HtmlColumn col = new HtmlColumn();
			col.setId("selected");
			col.setStyle("text-align:center;");
			col.setValueExpression("rendered",
					JsfHelper.createValueExpression("#{" + beanName + ".multiSelect}", Boolean.class));
			
			HtmlOutputText o = new HtmlOutputText();
			o.setValue("Selected?");
			col.getFacets().put("header", o);
			
			HtmlSelectBooleanCheckbox check = new HtmlSelectBooleanCheckbox();
			check.setId("selectMulti");
			check.setStyleClass("check");
			check.setValueExpression("value", 
					JsfHelper.createValueExpression("#{" + dataModel + ".selectionMap[trdetail.id]}", Boolean.class));

			HtmlAjaxSupport support = new HtmlAjaxSupport(); 
			support.setId("a4j-selectMulti");
			support.setOnsubmit("extendSelection(event);");
			support.setEvent("onclick");
			support.setAjaxSingle(true);
			support.setStatus("rowstatus");
			support.addActionListener(new MethodExpressionActionListener(
					JsfHelper.createListenerExpression(
							"#{" + beanName + ".multiSelectionChanged}", ActionEvent.class)));
			check.getChildren().add(support);

			col.getChildren().add(check);
			table.getChildren().add(col);
		}
		
		if (multiSelect) {
			//Add result
			HtmlColumn resultColumn = new HtmlColumn();
			resultColumn.setId("result");
	
			HtmlOutputText resultLabel = new HtmlOutputText();
			resultLabel.setValue("Status");
			resultColumn.getFacets().put("header", resultLabel);
			
			resultLabel = new HtmlOutputText();
			resultLabel.setValueExpression("value", JsfHelper.createValueExpression("#{" + table.getVar() + ".result}", String.class));
			
			resultColumn.getChildren().add(resultLabel);
			table.getChildren().add(resultColumn);
			
			/// transactionInd
			HtmlColumn transactionIndColumn = new HtmlColumn();
			transactionIndColumn.setId("transactionInd");
	
			HtmlOutputText transactionIndLabel = new HtmlOutputText();
			transactionIndLabel.setValue("Trans. Ind.");
			transactionIndColumn.getFacets().put("header", transactionIndLabel);
			
			transactionIndLabel = new HtmlOutputText();
			transactionIndLabel.setValueExpression("value", JsfHelper.createValueExpression("#{" + table.getVar() + ".transactionInd}", String.class));
			
			transactionIndColumn.getChildren().add(transactionIndLabel);
			table.getChildren().add(transactionIndColumn);

			
			
			
		}

		String maxdecimalplace = filePreferenceBean.getMaxdecimalplace();
		if (allColumns) {
			Class<?> cls = com.seconddecimal.billing.domain.SaleTransaction.class;
			Field [] fields = cls.getDeclaredFields();
			//com.seconddecimal.billing.domain.SaleTransaction as = new com.seconddecimal.billing.domain.SaleTransaction();
			String field = "";
			try{
				int idx = 0;
				for (Field f : fields) {
					Column col = f.getAnnotation(Column.class);
					//Formula formula = f.getAnnotation(Formula.class);
					if (col != null ) {
						idx++;
						field = f.getName();
						if (!containsValue(excludeColumns, field)) {
						sumCounter = addColumn(com.seconddecimal.billing.domain.SaleTransaction.class, sumCounter, table, beanName, dataModel, getLabelForField(propertyMap, field), field, 
								true, false, maxdecimalplace);
						}
					} 
				}
			}
			catch(Exception e){
				e.printStackTrace();
			}
		} else {
			for (String field : fixedColumns) {
				sumCounter = addColumn(com.seconddecimal.billing.domain.SaleTransaction.class, sumCounter, table, beanName, dataModel, getLabelForField(propertyMap, field), field, 
						true, false, maxdecimalplace);
			}
		}
	}
	
	public void createWhatIfTransactionDetailTable(//createTransactionDetailTable
			HtmlDataTable table, String beanName, String dataModel, 
			boolean multiSelect, boolean allColumns, boolean fromTransaction,
			String viewName, String userName) {
		
		table.getChildren().clear();
		
		Map<String,DataDefinitionColumn> propertyMap =	getTransactionPropertyMap();
		
		Set<String> fixedColumns = getViewFixedColumns(viewName, userName, propertyMap);
		
		Set<String> excludeColumns = new LinkedHashSet<String>();

		Set<String> nonSortableColumns = new LinkedHashSet<String>();
		nonSortableColumns.add("taxcodeDescription");
		
		createDynamicTable(PurchaseTransaction.class, 
				table, beanName, dataModel, 
				fixedColumns, excludeColumns, null, nonSortableColumns,
				propertyMap, allColumns, multiSelect, null);
	}
	
	public int createViewTransactionDetailTable(HtmlDataTable table, String beanName, String dataModel, String viewName, boolean multiSelect,String userName, 
			                                    String maxdecimalplace) {

		table.getChildren().clear();
		
		Map<String,DataDefinitionColumn> propertyMap =	getTransactionPropertyMap();

		Set<String> fixedColumns = getViewFixedColumns(viewName, userName, propertyMap);
		
		Set<String> excludeColumns = new LinkedHashSet<String>();



		Set<String> nonSortableColumns = new LinkedHashSet<String>();
		nonSortableColumns.add("taxcodeDescription");

		// Add "fixed" columns
		if (multiSelect) {
			// Multi-select
			HtmlColumn col = new HtmlColumn();
			col.setId("selected");
			col.setStyle("text-align:center;");
			col.setValueExpression("rendered",
					JsfHelper.createValueExpression("#{" + beanName + ".multiSelect}", Boolean.class));
			
			HtmlOutputText o = new HtmlOutputText();
			o.setValue("Selected?");
			col.getFacets().put("header", o);
			
			HtmlSelectBooleanCheckbox check = new HtmlSelectBooleanCheckbox();
			check.setId("selectMulti");
			check.setStyleClass("check");
			check.setValueExpression("value", 
					JsfHelper.createValueExpression("#{" + dataModel + ".selectionMap[trdetail.purchtransId]}", Boolean.class));

			HtmlAjaxSupport support = new HtmlAjaxSupport(); 
			support.setId("a4j-selectMulti");
			support.setOnsubmit("extendSelection(event);");
			support.setEvent("onclick");
			support.setAjaxSingle(true);
			support.setStatus("pageInfo");
			support.addActionListener(new MethodExpressionActionListener(
					JsfHelper.createListenerExpression("#{" + beanName + ".multiSelectionChanged}", ActionEvent.class)));
			support.setReRender(calulatedViewFieldIds(fixedColumns, excludeColumns));
			check.getChildren().add(support);

			col.getChildren().add(check);
			table.getChildren().add(col);
		}
			
		createDynamicTable(PurchaseTransaction.class, 
				table, beanName, dataModel, 
				fixedColumns, excludeColumns, null, nonSortableColumns,
				propertyMap, false, multiSelect, maxdecimalplace);// Treat all are fixed columns
		
		return fixedColumns.size();
	}
	
	
	
	public void createBillTransactionDetailTable(
		HtmlDataTable table, String beanName, String dataModel,
		Map<String,DataDefinitionColumn> propertyMap, 
		boolean multiSelect, boolean allColumns, boolean reOrder, boolean saveNow, boolean fromTransaction) {

		table.getChildren().clear();
		
		Set<String> fixedColumns = getBillFixedColumns();			

		Set<String> excludeColumns = new LinkedHashSet<String>();
		excludeColumns.add("id");
		excludeColumns.add("idPropertyName");
		excludeColumns.add("class");
		excludeColumns.add("comments");
		excludeColumns.add("countrySitusMatrixId");
		excludeColumns.add("stateSitusMatrixId");
		excludeColumns.add("countySitusMatrixId");
		excludeColumns.add("citySitusMatrixId");
		excludeColumns.add("stj1SitusMatrixId");
		excludeColumns.add("stj2SitusMatrixId");
		excludeColumns.add("stj3SitusMatrixId");
		excludeColumns.add("stj4SitusMatrixId");
		excludeColumns.add("stj5SitusMatrixId");
		excludeColumns.add("transactionInd");
		int sumCounter = 1;
		
		// Add "fixed" columns
		if (multiSelect) {
			// Multi-select
			HtmlColumn col = new HtmlColumn();
			col.setId("selected");
			col.setStyle("text-align:center;");
			col.setValueExpression("rendered",
					JsfHelper.createValueExpression("#{" + beanName + ".multiSelect}", Boolean.class));
			
			HtmlOutputText o = new HtmlOutputText();
			o.setValue("Selected?");
			col.getFacets().put("header", o);
			
			HtmlSelectBooleanCheckbox check = new HtmlSelectBooleanCheckbox();
			check.setId("selectMulti");
			check.setStyleClass("check");
			check.setValueExpression("value", 
					JsfHelper.createValueExpression("#{" + dataModel + ".selectionMap[trdetail.id]}", Boolean.class));

			HtmlAjaxSupport support = new HtmlAjaxSupport(); 
			support.setId("a4j-selectMulti");
			support.setOnsubmit("extendSelection(event);");
			support.setEvent("onclick");
			support.setAjaxSingle(true);
			support.setStatus("rowstatus");
			support.addActionListener(new MethodExpressionActionListener(
					JsfHelper.createListenerExpression(
							"#{" + beanName + ".multiSelectionChanged}", ActionEvent.class)));
			check.getChildren().add(support);

			col.getChildren().add(check);
			table.getChildren().add(col);
		}
		
		if (multiSelect) {
			//Add result
			HtmlColumn resultColumn = new HtmlColumn();
			resultColumn.setId("result");
	
			HtmlOutputText resultLabel = new HtmlOutputText();
			resultLabel.setValue("Status");
			resultColumn.getFacets().put("header", resultLabel);
			
			resultLabel = new HtmlOutputText();
			resultLabel.setValueExpression("value", JsfHelper.createValueExpression("#{" + table.getVar() + ".result}", String.class));
			
			resultColumn.getChildren().add(resultLabel);
			table.getChildren().add(resultColumn);
			
			/// transactionInd
			HtmlColumn transactionIndColumn = new HtmlColumn();
			transactionIndColumn.setId("transactionInd");
	
			HtmlOutputText transactionIndLabel = new HtmlOutputText();
			transactionIndLabel.setValue("Trans. Ind.");
			transactionIndColumn.getFacets().put("header", transactionIndLabel);
			
			transactionIndLabel = new HtmlOutputText();
			transactionIndLabel.setValueExpression("value", JsfHelper.createValueExpression("#{" + table.getVar() + ".transactionInd}", String.class));
			
			transactionIndColumn.getChildren().add(transactionIndLabel);
			table.getChildren().add(transactionIndColumn);

			
			
			
		}

		if (allColumns) {
			Class<?> cls = com.ncsts.domain.BillTransaction.class;
			String maxdecimalplace = filePreferenceBean.getMaxdecimalplace();
			Field [] fields = cls.getDeclaredFields();
			//com.seconddecimal.billing.domain.SaleTransaction as = new com.seconddecimal.billing.domain.SaleTransaction();
			String field = "";
			try{
				int idx = 0;
				for (Field f : fields) {
					Column col = f.getAnnotation(Column.class);
					//Formula formula = f.getAnnotation(Formula.class);
					if (col != null ) {
						idx++;
						field = f.getName();
						if (!containsValue(excludeColumns, field)) {
						sumCounter = addColumn(com.ncsts.domain.BillTransaction.class, sumCounter, table, beanName, dataModel, getLabelForField(propertyMap, field), field, 
								true, false, maxdecimalplace);
						}
					} 
				}
			}
			catch(Exception e){
				e.printStackTrace();
			}
		} else {
			for (String field : fixedColumns) {
				sumCounter = addColumn(com.ncsts.domain.BillTransaction.class, sumCounter, table, beanName, dataModel, getLabelForField(propertyMap, field), field, 
						true, false, null);
			}
		}
	}
	
	public int createViewTransactionDetailTable(HtmlDataTable table, String beanName, String dataModel, String viewName, String userName, boolean multiSelect, String maxdecimalplace) {

		table.getChildren().clear();
		
		Map<String,DataDefinitionColumn> propertyMap =	getTransactionPropertyMap();
		Set<String> fixedColumns = getViewFixedColumns(viewName, userName, propertyMap);
		
		Set<String> excludeColumns = new LinkedHashSet<String>();
			
		// Add "fixed" columns
		if (multiSelect) {
			// Multi-select
			HtmlColumn col = new HtmlColumn();
			col.setId("selected");
			col.setStyle("text-align:center;");
			col.setValueExpression("rendered",
					JsfHelper.createValueExpression("#{" + beanName + ".multiSelect}", Boolean.class));
			
			HtmlOutputText o = new HtmlOutputText();
			o.setValue("Selected?");
			col.getFacets().put("header", o);
			
			HtmlSelectBooleanCheckbox check = new HtmlSelectBooleanCheckbox();
			check.setId("selectMulti");
			check.setStyleClass("check");
			check.setValueExpression("value", 
					JsfHelper.createValueExpression("#{" + dataModel + ".selectionMap[trdetail.purchtransId]}", Boolean.class));

			HtmlAjaxSupport support = new HtmlAjaxSupport(); 
			support.setId("a4j-selectMulti");
			support.setOnsubmit("extendSelection(event);");
			support.setEvent("onclick");
			support.setAjaxSingle(true);
			support.setStatus("rowstatus");
			support.addActionListener(new MethodExpressionActionListener(
					JsfHelper.createListenerExpression("#{" + beanName + ".multiSelectionChanged}", ActionEvent.class)));
			support.setReRender(calulatedViewFieldIds(fixedColumns, excludeColumns));
			check.getChildren().add(support);

			col.getChildren().add(check);
			table.getChildren().add(col);
		}
			
		createDynamicTable(PurchaseTransaction.class, 
				table, beanName, dataModel, 
				fixedColumns, excludeColumns, null, null, 
				propertyMap, false, multiSelect, maxdecimalplace);// Treat all are fixed columns
		
		return fixedColumns.size();
	}
	
	
	public Set<String> getDefaultViewColumns() { 
		if(defaultViewColumns==null){
			defaultViewColumns = new LinkedHashSet<String>();
			
			String transactionColumnname = "";
			String name = "";
			defaultViewColumns.add("processStatus");
			//defaultViewColumns.add("transactionDetailId");
			defaultViewColumns.add("purchtransId");
			Map<String,DriverNames> taxDriversMap =  cacheManager.getMatrixColDriverNamesMap(TaxMatrix.DRIVER_CODE);
			for (String matrixColumnName : taxDriversMap.keySet()) {
				DriverNames driverNames = taxDriversMap.get(matrixColumnName);
				transactionColumnname = driverNames.getTransDtlColName();
				
				name = Util.columnToProperty(transactionColumnname);// like glCompanyNbr			
				defaultViewColumns.add(name);
			}
			
			Map<String,DriverNames> locationDriversMap =  cacheManager.getMatrixColDriverNamesMap(LocationMatrix.DRIVER_CODE);
			for (String matrixColumnName : locationDriversMap.keySet()) {
				DriverNames driverNames = locationDriversMap.get(matrixColumnName);
				transactionColumnname = driverNames.getTransDtlColName();			
				name = Util.columnToProperty(transactionColumnname);// like glCompanyNbr			
				defaultViewColumns.add(name);
			}
		}
	
	
		return defaultViewColumns;
	}
	
	public Set<String> getViewFixedColumns(String viewName, String userName, Map<String,DataDefinitionColumn> propertyMap){
		Set<String> fixedColumns = new LinkedHashSet<String>();
		
		String searchViewName = viewName;
		if(viewName==null || viewName.length()==0){
			searchViewName = "*DEFAULT";
		}
		
		List<TransactionHeader> transactionHeaders = dwColumnService.getColumnsByUserandDataWindowName("TB_PURCHTRANS", searchViewName, userName);
		if(transactionHeaders!=null && transactionHeaders.size()>0){
			for (TransactionHeader transactionHeader : transactionHeaders) {	
				fixedColumns.add(transactionHeader.getColumnName());
			}
		}
		else{
			fixedColumns = getDefaultViewColumns();
		}

		return fixedColumns;
	}
	
	public void createTransactionDetailTable(
			HtmlDataTable table, String beanName, String dataModel,
			Map<String,DataDefinitionColumn> propertyMap, 
			boolean multiSelect, boolean allColumns, boolean reOrder, boolean saveNow, boolean fromTransaction) {
		createTransactionDetailTable(table, beanName, dataModel, propertyMap, multiSelect, allColumns, reOrder, saveNow, fromTransaction, null);
	}
	
	public void createTransactionDetailLogTable(
			HtmlDataTable table, String beanName, String dataModel,
			Map<String,DataDefinitionColumn> propertyMap, 
			boolean multiSelect, boolean allColumns, boolean reOrder, boolean saveNow, boolean fromTransaction) {
		createTransactionDetailLogTable(table, beanName, dataModel, propertyMap, multiSelect, allColumns, reOrder, saveNow, fromTransaction, null);
	}
	
	public void createTransactionDetailLogTable(
			HtmlDataTable table, String beanName, String dataModel,
			Map<String,DataDefinitionColumn> propertyMap, 
			boolean multiSelect, boolean allColumns, boolean reOrder, boolean saveNow, boolean fromTransaction, String[] includeTransColumns) {

		// Remove existing columns
		table.getChildren().clear();
		
		Set<String> fixedColumns = new LinkedHashSet<String>();
		Set<String> excludeColumns = new LinkedHashSet<String>();
		
		if(allColumns) {
			List<String> allfields = new ArrayList<String>();
			try {
				for(Field f : PurchaseTransactionLog.class.getDeclaredFields()) {
					if(f.getAnnotation(Column.class) != null) {
						allfields.add(f.getName());
					}
				}
			}
			catch(Exception e) {
				logger.error(e);
			}
			
			forHeaderRearrange = new ArrayList<TransactionHeader>();
			for(String temp : allfields){
				TransactionHeader header = new TransactionHeader();
				header.setColumnName(temp);
				header.setColumnLabel(getLabelForField(propertyMap, temp));
				forHeaderRearrange.add(header);
			}
		}
		
		String maxdecimalplace = filePreferenceBean.getMaxdecimalplace();
		createDynamicTable(PurchaseTransactionLog.class, 
				table, beanName, dataModel, 
				fixedColumns, excludeColumns, null, null, 
				propertyMap, allColumns, multiSelect, maxdecimalplace);
		
	}
	
	// Bug 3987
	public void createTransactionDetailTable(
			HtmlDataTable table, String beanName, String dataModel,
			Map<String,DataDefinitionColumn> propertyMap, 
			boolean multiSelect, boolean allColumns, boolean reOrder, boolean saveNow, boolean fromTransaction, String[] includeTransColumns) {

		// Remove existing columns
		table.getChildren().clear();
		
		Set<String> fixedColumns = new LinkedHashSet<String>();
		
		fixedColumns=getFixedColumns(reOrder, allColumns, saveNow, fromTransaction, includeTransColumns);
		
		
		Set<String> excludeColumns = new LinkedHashSet<String>();
		excludeColumns.add("id");
		excludeColumns.add("idPropertyName");
		excludeColumns.add("class");
		excludeColumns.add("comments");
		excludeColumns.add("countrySitusMatrixId");
		excludeColumns.add("stateSitusMatrixId");
		excludeColumns.add("countySitusMatrixId");
		excludeColumns.add("citySitusMatrixId");
		excludeColumns.add("stj1SitusMatrixId");
		excludeColumns.add("stj2SitusMatrixId");
		excludeColumns.add("stj3SitusMatrixId");
		excludeColumns.add("stj4SitusMatrixId");
		excludeColumns.add("stj5SitusMatrixId");
		
		// Add "fixed" columns
		if (multiSelect) {
			// Multi-select
			HtmlColumn col = new HtmlColumn();
			col.setId("selected");
			col.setStyle("text-align:center;");
			col.setValueExpression("rendered",
					JsfHelper.createValueExpression("#{" + beanName + ".multiSelect}", Boolean.class));
			
			HtmlOutputText o = new HtmlOutputText();
			o.setValue("Selected?");
			col.getFacets().put("header", o);
			
			HtmlSelectBooleanCheckbox check = new HtmlSelectBooleanCheckbox();
			check.setId("selectMulti");
			check.setStyleClass("check");
			check.setValueExpression("value", 
					JsfHelper.createValueExpression("#{" + dataModel + ".selectionMap[trdetail.purchtransId]}", Boolean.class));

			HtmlAjaxSupport support = new HtmlAjaxSupport(); 
			support.setId("a4j-selectMulti");
			support.setOnsubmit("extendSelection(event);");
			support.setEvent("onclick");
			support.setAjaxSingle(true);
			support.setStatus("rowstatus");
			support.addActionListener(new MethodExpressionActionListener(
					JsfHelper.createListenerExpression(
							"#{" + beanName + ".multiSelectionChanged}", ActionEvent.class)));
			support.setReRender(calulatedFieldIds(fixedColumns, excludeColumns, allColumns));
			check.getChildren().add(support);

			col.getChildren().add(check);
			table.getChildren().add(col);
		}
		
		
		String maxdecimalplace = filePreferenceBean.getMaxdecimalplace();
        createDynamicTable(PurchaseTransaction.class, 
				table, beanName, dataModel, 
				fixedColumns, excludeColumns, null, null, 
				propertyMap, allColumns, multiSelect, maxdecimalplace);
		
	}
	
	// Bug 3987
	static private String calulatedFieldIds(Collection<String> fixedColumns, Collection<String> excludeColumns, boolean allColumns) {
		StringBuilder ids = new StringBuilder();
		int sumCounter = 0;
		
		
		// Conditionally add in remaining columns
		if (allColumns) {
			
			for (TransactionHeader header : forHeaderRearrange) {
				String field = header.getColumnName();
				if (!containsValue(excludeColumns, field) &&
					!containsValue(fixedColumns, field)) {
					try {
						Method m = PurchaseTransaction.class.getMethod("get" + Util.upperCaseFirst(field), new Class[] { });
						if (m.getReturnType().equals(Float.class) ||
								  m.getReturnType().equals(Double.class) ||
								  m.getReturnType().equals(BigDecimal.class)) {
							sumCounter++;
						}
					} catch (SecurityException e) {
						e.printStackTrace();
					} catch (NoSuchMethodException e) {
						e.printStackTrace();
					}
				}
			}
		}else {
			for (String field : fixedColumns) {
				try {
					Method m = PurchaseTransaction.class.getMethod("get" + Util.upperCaseFirst(field), new Class[] { });
					if (m.getReturnType().equals(Float.class) ||
							  m.getReturnType().equals(Double.class) ||
							  	m.getReturnType().equals(BigDecimal.class)) {
						sumCounter++;
					}
				} catch (SecurityException e) {
					e.printStackTrace();
				} catch (NoSuchMethodException e) {
					e.printStackTrace();
				}
			}
		}

		for (int i = 1; i <= sumCounter; i++) {
			ids.append(((i > 1)? ",":"") + String.format("sum%d", i));
		}
		// JMW: Ajax4jsf does not like a render list that is empty string... 
		if (sumCounter == 0) return null;
		return ids.toString();
	}
	
	static private String calulatedViewFieldIds(Collection<String> fixedColumns, Collection<String> excludeColumns) {
		StringBuilder ids = new StringBuilder();
		int sumCounter = 0;
		
		// Conditionally add in remaining columns
		for (String field : fixedColumns) {
			try {
				Method m = PurchaseTransaction.class.getMethod("get" + Util.upperCaseFirst(field), new Class[] { });
				if (m.getReturnType().equals(Float.class) ||
						  m.getReturnType().equals(Double.class) ||
						  	m.getReturnType().equals(BigDecimal.class)) {
					sumCounter++;
				}
			} catch (SecurityException e) {
				e.printStackTrace();
			} catch (NoSuchMethodException e) {
				e.printStackTrace();
			}
		}
	
		for (int i = 1; i <= sumCounter; i++) {
			ids.append(((i > 1)? ",":"") + String.format("sum%d", i));
		}
		// JMW: Ajax4jsf does not like a render list that is empty string... 
		if (sumCounter == 0) return null;
		return ids.toString();
	}
	
	static private String[] fieldArray() {
		String trFields[] = {
				  "processStatus",
			      "purchtransId",
			      "sourceTransactionId",
			      "processBatchNo",
			      "multiTransCodeDesc",
			      "glExtractBatchNo",
			      "allocationMatrixId",
			      "allocationSubtransId",
			      "enteredDate",
			      "transactionStatus",
			      "glDate",
			      "glCompanyNbr",
			      "glCompanyName",
			      "glDivisionNbr",
			      "glDivisionName",
			      "glCcNbrDeptId",
			      "glCcNbrDeptName",
			      "glLocalAcctNbr",
			      "glLocalAcctName",
			      "glLocalSubAcctNbr",
			      "glLocalSubAcctName",
			      "glFullAcctNbr",
			      "glFullAcctName",
			      "glLineItmDistAmt",
			      "vendorName",
			      "vendorAddressLine1",
			      "vendorAddressLine2",
			      "vendorAddressCity",
			      "vendorAddressCounty",
			      "vendorAddressState",
			      "vendorAddressZip",
			      "vendorAddressCountry",
			      "vendorType",
			      "vendorTypeName",
			      "invoiceNbr",
			      "invoiceDesc",
			      "invoiceDate",
			      "invoiceFreightAmt",
			      "invoiceDiscountAmt",
			      "invoiceTaxAmt",
			      "invoiceTotalAmt",
			      "invoiceTaxFlg",
			      "invoiceLineNbr",
			      "invoiceLineName",
			      "invoiceLineType",
			      "invoiceLineTypeName",
			      "invoiceLineAmt",
			      "invoiceLineTax",
			      "afeProjectNbr",
			      "afeProjectName",
			      "afeCategoryNbr",
			      "afeCategoryName",
			      "afeSubCatNbr",
			      "afeSubCatName",
			      "afeUse",
			      "afeContractType",
			      "afeContractStructure",
			      "afePropertyCat",
			      "inventoryNbr",
			      "inventoryName",
			      "inventoryClass",
			      "inventoryClassName",
			      "poNbr",
			      "poName",
			      "poDate",
			      "poLineNbr",
			      "poLineName",
			      "poLineType",
			      "poLineTypeName",
			      "woNbr",
			      "woName",
			      "woDate",
			      "woType",
			      "woTypeDesc",
			      "woClass",
			      "woClassDesc",
			      "woEntity",
			      "woEntityDesc",
			      "woLineNbr",
			      "woLineName",
			      "woLineType",
			      "woLineTypeDesc",
			      "woShutDownCd",
			      "woShutDownCdDesc",
			      "voucherId",
			      "voucherName",
			      "voucherDate",
			      "voucherLineNbr",
			      "voucherLineDesc",
			      "checkNbr",
			      "checkNo",
			      "checkDate",
			      "checkAmt",
			      "checkDesc",
			      "userText01",
			      "userText02",
			      "userText03",
			      "userText04",
			      "userText05",
			      "userText06",
			      "userText07",
			      "userText08",
			      "userText09",
			      "userText10",
			      "userText11",
			      "userText12",
			      "userText13",
			      "userText14",
			      "userText15",
			      "userText16",
			      "userText17",
			      "userText18",
			      "userText19",
			      "userText20",
			      "userText21",
			      "userText22",
			      "userText23",
			      "userText24",
			      "userText25",
			      "userText26",
			      "userText27",
			      "userText28",
			      "userText29",
			      "userText30",
			      "userText31",
			      "userText32",
			      "userText33",
			      "userText34",
			      "userText35",
			      "userText36",
			      "userText37",
			      "userText38",
			      "userText39",
			      "userText40",
			      "userText41",
			      "userText42",
			      "userText43",
			      "userText44",
			      "userText45",
			      "userText46",
			      "userText47",
			      "userText48",
			      "userText49",
			      "userText50",
			      "userText51",
			      "userText52",
			      "userText53",
			      "userText54",
			      "userText55",
			      "userText56",
			      "userText57",
			      "userText58",
			      "userText59",
			      "userText60",
			      "userText61",
			      "userText62",
			      "userText63",
			      "userText64",
			      "userText65",
			      "userText66",
			      "userText67",
			      "userText68",
			      "userText69",
			      "userText70",
			      "userText71",
			      "userText72",
			      "userText73",
			      "userText74",
			      "userText75",
			      "userText76",
			      "userText77",
			      "userText78",
			      "userText79",
			      "userText80",
			      "userText81",
			      "userText82",
			      "userText83",
			      "userText84",
			      "userText85",
			      "userText86",
			      "userText87",
			      "userText88",
			      "userText89",
			      "userText90",
			      "userText91",
			      "userText92",
			      "userText93",
			      "userText94",
			      "userText95",
			      "userText96",
			      "userText97",
			      "userText98",
			      "userText99",
			      "userText100",
			      "userNumber01",
			      "userNumber02",
			      "userNumber03",
			      "userNumber04",
			      "userNumber05",
			      "userNumber06",
			      "userNumber07",
			      "userNumber08",
			      "userNumber09",
			      "userNumber10",
			      "userNumber11",
			      "userNumber12",
			      "userNumber13",
			      "userNumber14",
			      "userNumber15",
			      "userNumber16",
			      "userNumber17",
			      "userNumber18",
			      "userNumber19",
			      "userNumber20",
			      "userNumber21",
			      "userNumber22",
			      "userNumber23",
			      "userNumber24",
			      "userNumber25",
			      "userDate01",
			      "userDate02",
			      "userDate03",
			      "userDate04",
			      "userDate05",
			      "userDate06",
			      "userDate07",
			      "userDate08",
			      "userDate09",
			      "userDate10",
			      "userDate11",
			      "userDate12",
			      "userDate13",
			      "userDate14",
			      "userDate15",
			      "userDate16",
			      "userDate17",
			      "userDate18",
			      "userDate19",
			      "userDate20",
			      "userDate21",
			      "userDate22",
			      "userDate23",
			      "userDate24",
			      "userDate25",
			      "comments",
			      "tbCalcTaxAmt",
			      "transactionCountryCode",
			      "autoTransactionCountryCode",

			      "transactionStateCode",
			      "autoTransactionStateCode",
			      "transactionInd",
			      "suspendInd",
			      "taxcodeCode",
			      //CCH		"cchTaxcatCode",
			      //CCH		"cchGroupCode",
			      //CCH  	"cchItemCode",
			      "manualTaxcodeInd",
			      "taxMatrixId",
			      "shiptoJurisdictionId",
			      "measureTypeCode",
			      "loadTimestamp",
			      "glExtractUpdater",
			      "glExtractTimestamp",
			      "glExtractFlag",
			      "glLogFlag",
			      "auditFlag",
			      "rejectFlag",
			      "auditUserId",
			      "auditTimestamp",
			      "modifyUserId",
			      "modifyTimestamp",
			      "hasComments",
			      "splitSubtransId",
			      "taxAllocMatrixId",
			      "updateUserId",
			      "updateTimestamp",
			      "stateTaxcodeDetailId",
			      "countyTaxcodeDetailId",
			      "cityTaxcodeDetailId",
			      "stateTaxcodeTypeCode",
			      "countyTaxcodeTypeCode",
			      "cityTaxcodeTypeCode",	
			      //
			      "stj1Rate",
			      "stj2Rate",
			      "stj3Rate",
			      "stj4Rate",
			      "stj5Rate",	      
			   
			};
		return trFields;
	}
	
	static public void createDynamicTableStatistics(Class<?> clazz, HtmlDataTable table, 
			String beanName, String dataModel, 
			Collection<String> fixedColumns, Collection<String> excludeColumns, 
			Collection<String> sortableColumns, Collection<String> nonsortableColumns,
			Map<String,DataDefinitionColumn> propertyMap,
			boolean allColumns, boolean multiSelect) {
		
		int sumCounter = 1;
		for (String field : fixedColumns) {
			sumCounter = addColumn(clazz, sumCounter, table, beanName, dataModel, getLabelForField(propertyMap, field), field, 
					isSortable(sortableColumns, nonsortableColumns, field), multiSelect, null);
		}

		// Conditionally add in remaining columns
		if (allColumns) {
			String trFields[] = fieldArray(); 
			/*
			for (Method m : clazz.getMethods()) {
				String name = m.getName();
				String field = Util.lowerCaseFirst(name.substring(3));
				// Look for properties only
				if (name.startsWith("get") && 
						(m.getParameterTypes().length == 0) &&
						!containsValue(excludeColumns, field) &&
						!containsValue(fixedColumns, field)) {
			*/
			for (String field : trFields) {
				if (!containsValue(excludeColumns, field) &&
					!containsValue(fixedColumns, field)) {
					sumCounter = addColumn(clazz, sumCounter, table, beanName, dataModel, getLabelForField(propertyMap, field), field, 
							isSortable(sortableColumns, nonsortableColumns, field), multiSelect, null);
				}
			}
		}
	}
	
	static public void createDynamicTable(Class<?> clazz, HtmlDataTable table, 
			String beanName, String dataModel, 
			Collection<String> fixedColumns, Collection<String> excludeColumns, 
			Collection<String> sortableColumns, Collection<String> nonsortableColumns,
			Map<String,DataDefinitionColumn> propertyMap,
			boolean allColumns, boolean multiSelect, String maxdecimalplace) {
		
		int sumCounter = 1;


		// Conditionally add in remaining columns
		if (allColumns) {
			//String trFields[] = fieldArray(); 
			/*
			for (Method m : clazz.getMethods()) {
				String name = m.getName();
				String field = Util.lowerCaseFirst(name.substring(3));
				// Look for properties only
				if (name.startsWith("get") && 
						(m.getParameterTypes().length == 0) &&
						!containsValue(excludeColumns, field) &&
						!containsValue(fixedColumns, field)) {
			*/
			for (TransactionHeader header : forHeaderRearrange) {
				String field = header.getColumnName();
				if (!containsValue(excludeColumns, field) 
						/*&&
					!containsValue(fixedColumns, field)*/) {
					sumCounter = addColumn(clazz, sumCounter, table, beanName, dataModel, getLabelForField(propertyMap, field), field, 
							isSortable(sortableColumns, nonsortableColumns, field), multiSelect, maxdecimalplace);
				}
			}
		}else {
			for (String field : fixedColumns) {
				sumCounter = addColumn(clazz, sumCounter, table, beanName, dataModel, getLabelForField(propertyMap, field), field, 
						isSortable(sortableColumns, nonsortableColumns, field), multiSelect, maxdecimalplace);
			}
		}
	}
	
	static private String getLabelForField(Map<String,DataDefinitionColumn> propertyMap, String field) {
		DataDefinitionColumn ddc = (propertyMap == null)? null:propertyMap.get(field);
		String label = (ddc == null)? null:ddc.getAbbrDesc();
		return (label == null)? Util.propertyToLabel(field):label; 
	}
	
	static private boolean isSortable(Collection<String> sortableColumns, Collection<String> nonSortableColumns, String field) {
		return (((sortableColumns == null) || containsValue(sortableColumns, field)) && 
				((nonSortableColumns == null) || !containsValue(nonSortableColumns, field)));
	}
	
	static private boolean containsValue(Collection<String> list, String value) {
		return list.contains(value);
	}
	
	static private int addColumn(Class<?> clazz, int sumCounter, HtmlDataTable table, String beanName, String dataModel, String label, String field, boolean sortable, boolean multiSelect, String maxdecimalplace) {
		try {
			Method m = clazz.getMethod("get" + Util.upperCaseFirst(field), new Class[] { });
			return addColumn(sumCounter, table, beanName, dataModel, label, field, sortable, m, multiSelect, maxdecimalplace);
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		}
		return sumCounter;
	}
	
	static private int addColumn(int sumCounter, HtmlDataTable table, 
			String beanName, String dataModel, String label, String field, boolean sortable, Method m, boolean multiSelect, String maxdecimalplace) {
		HtmlColumn col = new HtmlColumn();
		col.setId(field);

		if (sortable) {
			// Sortable header
			sumCounter = addSortableHeader(sumCounter, col, table.getId(), beanName, dataModel, label, field, m, multiSelect);
		} else {
			// regular header
			sumCounter = addHeader(sumCounter, col, beanName, label, field, m, multiSelect);
		}

		addValue(col, table.getVar(), field, m, maxdecimalplace);
		table.getChildren().add(col);
		return sumCounter;
	}
	
	static private int addSortableHeader(int sumCounter, HtmlColumn col, String tableId, 
			String beanName, String dataModel, String label, String field, Method m, boolean multiSelect) {
		// Sortable header
		HtmlPanelGroup header = new HtmlPanelGroup();
		HtmlAjaxCommandLink cmd = new HtmlAjaxCommandLink();
		if(dataModel.endsWith("transactionDataModel") || dataModel.endsWith("transactionSaleDataModel")){
			cmd.setId("s_trans_"+field);
		}
		else{
			cmd.setId("s_"+field);
		}
		cmd.setImmediate(true);
		cmd.setValue(label);
		cmd.setReRender(tableId);
		
		cmd.addActionListener(new MethodExpressionActionListener(
				JsfHelper.createListenerExpression("#{" + beanName + ".sortAction}", ActionEvent.class)));
		
		cmd.setValueExpression("styleClass", 
				JsfHelper.createValueExpression("sort-#{" + dataModel + ".sortOrder['"+field+"']}", String.class));

		//disable all operations during sorting only for statistics view page
		if("databaseStatisticsBean.statisticsDataModel".equalsIgnoreCase(dataModel)) {
			//PP-357
			//cmd.setOnclick("disableButtons();");
			//cmd.setOndblclick("disableButtons();");
			cmd.setReRender(tableId+",cancelAction,saveAs,viewTr");
		}
		
		cmd.setOncomplete("initScrollingTables();");
		// cmd.setStyle("float:top;");
		header.getChildren().add(cmd);

		if (multiSelect && (m.getReturnType().equals(Float.class) || m.getReturnType().equals(Double.class) || m.getReturnType().equals(BigDecimal.class))) {
			HtmlOutputText txt = new HtmlOutputText();
			txt.setEscape(false);
			txt.setValue("<br/>");
			header.getChildren().add(txt);
			
			txt = new HtmlOutputText();
			txt.setId(String.format("sum%d", sumCounter++));
			txt.setStyle("color:blue;");

			txt.setValueExpression("value", 
					JsfHelper.createValueExpression("#{" + beanName + ".fieldSum['"+field+"']}", Double.class));
			
			if(!field.endsWith("Rate")){
				NumberConverter converter = new NumberConverter();
				converter.setPattern("#,##0.00");
				txt.setConverter(converter);
			}
			
			header.getChildren().add(txt);
		}
		
		col.getFacets().put("header", header);
		return sumCounter;
	}

	static private int addHeader(int sumCounter, HtmlColumn col, String beanName, String label, String field, Method m, boolean multiSelect) {
		// Regular header
		if (multiSelect && (m.getReturnType().equals(Float.class) || m.getReturnType().equals(Double.class) || m.getReturnType().equals(BigDecimal.class))) {
			HtmlPanelGroup header = new HtmlPanelGroup();
			HtmlOutputText txt = new HtmlOutputText();
			txt.setValue(label);
			header.getChildren().add(txt);

			txt = new HtmlOutputText();
			txt.setEscape(false);
			txt.setValue("<br/>");
			header.getChildren().add(txt);
			
			txt = new HtmlOutputText();
			txt.setId(String.format("sum%d", sumCounter++));
			txt.setValueExpression("value", 
					JsfHelper.createValueExpression("#{" + beanName + ".fieldSum['"+field+"']}", Double.class));
			
			txt.setStyle("color:blue;");
			
			if(!field.endsWith("Rate")){
				NumberConverter converter = new NumberConverter();
				converter.setPattern("#0.00");
				txt.setConverter(converter);
			}

			header.getChildren().add(txt);
			
			col.getFacets().put("header", header);
		} else {
			HtmlOutputText header = new HtmlOutputText();
			header.setValue(label);
			col.getFacets().put("header", header);
		}
		return sumCounter;
	}
	
	static private Map<String,String> formatDataMap = null;
	
	static private Map<String,String> getFormatDataMap() {
		
		if (formatDataMap == null) {
			formatDataMap =  new LinkedHashMap<String,String>();
			formatDataMap.put("transactionTypeCode","#{cacheManager.transTypeMap[:var.:field]}");
			formatDataMap.put("methodDeliveryCode","#{cacheManager.modMap[:var.:field]}");	
			formatDataMap.put("ratetypeCode","#{cacheManager.rateTypeMap[:var.:field]}");
			formatDataMap.put("geoReconCode","#{cacheManager.geoReconMap[:var.:field]}");
			formatDataMap.put("entityCode","#{cacheManager.entityCodeMap[:var.:field]}");
			formatDataMap.put("custEntityCode","#{cacheManager.entityCodeMap[:var.:field]}");
			formatDataMap.put("shiptoEntityCode","#{cacheManager.entityCodeMap[:var.:field]}");
			formatDataMap.put("ordrorgnEntityCode","#{cacheManager.entityCodeMap[:var.:field]}");
			formatDataMap.put("ordracptEntityCode","#{cacheManager.entityCodeMap[:var.:field]}");
			formatDataMap.put("shipfromEntityCode","#{cacheManager.entityCodeMap[:var.:field]}");
			formatDataMap.put("firstuseEntityCode","#{cacheManager.entityCodeMap[:var.:field]}");
			formatDataMap.put("billtoEntityCode","#{cacheManager.entityCodeMap[:var.:field]}");
			formatDataMap.put("invoiceDiscTypeCode","#{cacheManager.discTypeMap[:var.:field]}");
			formatDataMap.put("invoiceLineDiscTypeCode","#{cacheManager.discTypeMap[:var.:field]}");
			formatDataMap.put("exemptReason","#{cacheManager.exReasonMap[:var.:field]}");
			formatDataMap.put("invoiceLineDiscTypeCode","#{cacheManager.discTypeMap[:var.:field]}");
			//formatDataMap.put("processMethod","#{cacheManager.procMethodMap[:var.:field]}");
			formatDataMap.put("freightItemFlag","#{cacheManager.booleanMap[:var.:field]}");
			formatDataMap.put("discountItemFlag","#{cacheManager.booleanMap[:var.:field]}");
			formatDataMap.put("countryCode","#{cacheManager.countryMap[:var.:field]} (#{:var.:field})");
			formatDataMap.put("custCountryCode","#{cacheManager.countryMap[:var.:field]} (#{:var.:field})");
			formatDataMap.put("shiptoCountryCode","#{cacheManager.countryMap[:var.:field]} (#{:var.:field})");
			formatDataMap.put("shipfromCountryCode","#{cacheManager.countryMap[:var.:field]} (#{:var.:field})");
			formatDataMap.put("ordracptCountryCode","#{cacheManager.countryMap[:var.:field]} (#{:var.:field})");
			formatDataMap.put("ordrorgnCountryCode","#{cacheManager.countryMap[:var.:field]} (#{:var.:field})");
			formatDataMap.put("firstuseCountryCode","#{cacheManager.countryMap[:var.:field]} (#{:var.:field})");
			formatDataMap.put("billtoCountryCode","#{cacheManager.countryMap[:var.:field]} (#{:var.:field})");
			formatDataMap.put("countryTaxtypeCode","#{cacheManager.taxtypeMap[:var.:field]}");
			formatDataMap.put("stateTaxtypeCode","#{cacheManager.taxtypeMap[:var.:field]}");
			formatDataMap.put("countyTaxtypeCode","#{cacheManager.taxtypeMap[:var.:field]}");
			formatDataMap.put("cityTaxtypeCode","#{cacheManager.taxtypeMap[:var.:field]}");
			formatDataMap.put("stj1TaxtypeCode","#{cacheManager.taxtypeMap[:var.:field]}");
			formatDataMap.put("stj2TaxtypeCode","#{cacheManager.taxtypeMap[:var.:field]}");
			formatDataMap.put("stj3TaxtypeCode","#{cacheManager.taxtypeMap[:var.:field]}");
			formatDataMap.put("stj4TaxtypeCode","#{cacheManager.taxtypeMap[:var.:field]}");
			formatDataMap.put("stj5TaxtypeCode","#{cacheManager.taxtypeMap[:var.:field]}");
			formatDataMap.put("stj6TaxtypeCode","#{cacheManager.taxtypeMap[:var.:field]}");
			formatDataMap.put("stj7TaxtypeCode","#{cacheManager.taxtypeMap[:var.:field]}");
			formatDataMap.put("stj8TaxtypeCode","#{cacheManager.taxtypeMap[:var.:field]}");
			formatDataMap.put("stj9TaxtypeCode","#{cacheManager.taxtypeMap[:var.:field]}");
			formatDataMap.put("stj10TaxtypeCode","#{cacheManager.taxtypeMap[:var.:field]}");
			formatDataMap.put("stateCode","#{cacheManager.taxCodeStateMap[:var.:field].name} #{(not empty cacheManager.taxCodeStateMap[:var.:field].name) ? '(' : ''}#{:var.:field}#{(not empty cacheManager.taxCodeStateMap[:var.:field].name) ? ')' : ''}");
			formatDataMap.put("custStateCode","#{cacheManager.taxCodeStateMap[:var.:field].name} #{(not empty cacheManager.taxCodeStateMap[:var.:field].name) ? '(' : ''}#{:var.:field}#{(not empty cacheManager.taxCodeStateMap[:var.:field].name) ? ')' : ''}");
			formatDataMap.put("shiptoStateCode","#{cacheManager.taxCodeStateMap[:var.:field].name} #{(not empty cacheManager.taxCodeStateMap[:var.:field].name) ? '(' : ''}#{:var.:field}#{(not empty cacheManager.taxCodeStateMap[:var.:field].name) ? ')' : ''}");
			formatDataMap.put("shipfromStateCode","#{cacheManager.taxCodeStateMap[:var.:field].name} #{(not empty cacheManager.taxCodeStateMap[:var.:field].name) ? '(' : ''}#{:var.:field}#{(not empty cacheManager.taxCodeStateMap[:var.:field].name) ? ')' : ''}");
			formatDataMap.put("ordracptStateCode","#{cacheManager.taxCodeStateMap[:var.:field].name} #{(not empty cacheManager.taxCodeStateMap[:var.:field].name) ? '(' : ''}#{:var.:field}#{(not empty cacheManager.taxCodeStateMap[:var.:field].name) ? ')' : ''}");
			formatDataMap.put("ordrorgnStateCode","#{cacheManager.taxCodeStateMap[:var.:field].name} #{(not empty cacheManager.taxCodeStateMap[:var.:field].name) ? '(' : ''}#{:var.:field}#{(not empty cacheManager.taxCodeStateMap[:var.:field].name) ? ')' : ''}");
			formatDataMap.put("firstuseStateCode","#{cacheManager.taxCodeStateMap[:var.:field].name} #{(not empty cacheManager.taxCodeStateMap[:var.:field].name) ? '(' : ''}#{:var.:field}#{(not empty cacheManager.taxCodeStateMap[:var.:field].name) ? ')' : ''}");
			formatDataMap.put("billtoStateCode","#{cacheManager.taxCodeStateMap[:var.:field].name} #{(not empty cacheManager.taxCodeStateMap[:var.:field].name) ? '(' : ''}#{:var.:field}#{(not empty cacheManager.taxCodeStateMap[:var.:field].name) ? ')' : ''}");
			formatDataMap.put("billtoStateCode","#{cacheManager.taxCodeStateMap[:var.:field].name} #{(not empty cacheManager.taxCodeStateMap[:var.:field].name) ? '(' : ''}#{:var.:field}#{(not empty cacheManager.taxCodeStateMap[:var.:field].name) ? ')' : ''}");
			formatDataMap.put("exemptReason","#{cacheManager.listCodeMap['EXREASON'][:var.:field].description} #{(not empty cacheManager.listCodeMap['EXREASON'][:var.:field].description) ? '(' : ''}#{:var.:field}#{(not empty cacheManager.listCodeMap['EXREASON'][:var.:field].description) ? ')' : ''}");
			formatDataMap.put("exemptCode","#{cacheManager.listCodeMap['EXEMPTCODE'][:var.:field].description} #{(not empty cacheManager.listCodeMap['EXEMPTCODE'][:var.:field].description) ? '(' : ''}#{:var.:field}#{(not empty cacheManager.listCodeMap['EXEMPTCODE'][:var.:field].description) ? ')' : ''}");
			
		}	    
		return formatDataMap;
	}
	
	static private Map<String,String> formatBillDataMap = null;
	
	static private Map<String,String> getFormatBillDataMap() {
		
		if (formatDataMap == null) {
			formatDataMap =  new LinkedHashMap<String,String>();
			formatDataMap.put("transactionTypeCode","#{cacheManager.transTypeMap[:var.:field]}");
			formatDataMap.put("methodDeliveryCode","#{cacheManager.modMap[:var.:field]}");	
			formatDataMap.put("ratetypeCode","#{cacheManager.rateTypeMap[:var.:field]}");
			formatDataMap.put("geoReconCode","#{cacheManager.geoReconMap[:var.:field]}");
			formatDataMap.put("entityCode","#{cacheManager.entityCodeMap[:var.:field]}");
			formatDataMap.put("custEntityCode","#{cacheManager.entityCodeMap[:var.:field]}");
			formatDataMap.put("shiptoEntityCode","#{cacheManager.entityCodeMap[:var.:field]}");
			formatDataMap.put("ordrorgnEntityCode","#{cacheManager.entityCodeMap[:var.:field]}");
			formatDataMap.put("ordracptEntityCode","#{cacheManager.entityCodeMap[:var.:field]}");
			formatDataMap.put("shipfromEntityCode","#{cacheManager.entityCodeMap[:var.:field]}");
			formatDataMap.put("firstuseEntityCode","#{cacheManager.entityCodeMap[:var.:field]}");
			formatDataMap.put("billtoEntityCode","#{cacheManager.entityCodeMap[:var.:field]}");
			formatDataMap.put("invoiceDiscTypeCode","#{cacheManager.discTypeMap[:var.:field]}");
			formatDataMap.put("invoiceLineDiscTypeCode","#{cacheManager.discTypeMap[:var.:field]}");
			formatDataMap.put("exemptReason","#{cacheManager.exReasonMap[:var.:field]}");
			formatDataMap.put("invoiceLineDiscTypeCode","#{cacheManager.discTypeMap[:var.:field]}");
			//formatDataMap.put("processMethod","#{cacheManager.procMethodMap[:var.:field]}");
			formatDataMap.put("freightItemFlag","#{cacheManager.booleanMap[:var.:field]}");
			formatDataMap.put("discountItemFlag","#{cacheManager.booleanMap[:var.:field]}");
			formatDataMap.put("countryCode","#{cacheManager.countryMap[:var.:field]} (#{:var.:field})");
			formatDataMap.put("custCountryCode","#{cacheManager.countryMap[:var.:field]} (#{:var.:field})");
			formatDataMap.put("shiptoCountryCode","#{cacheManager.countryMap[:var.:field]} (#{:var.:field})");
			formatDataMap.put("shipfromCountryCode","#{cacheManager.countryMap[:var.:field]} (#{:var.:field})");
			formatDataMap.put("ordracptCountryCode","#{cacheManager.countryMap[:var.:field]} (#{:var.:field})");
			formatDataMap.put("ordrorgnCountryCode","#{cacheManager.countryMap[:var.:field]} (#{:var.:field})");
			formatDataMap.put("firstuseCountryCode","#{cacheManager.countryMap[:var.:field]} (#{:var.:field})");
			formatDataMap.put("billtoCountryCode","#{cacheManager.countryMap[:var.:field]} (#{:var.:field})");
			formatDataMap.put("countryTaxtypeUsedCode","#{cacheManager.taxtypeMap[:var.:field]}");
			formatDataMap.put("stateTaxtypeUsedCode","#{cacheManager.taxtypeMap[:var.:field]}");
			formatDataMap.put("countyTaxtypeUsedCode","#{cacheManager.taxtypeMap[:var.:field]}");
			formatDataMap.put("cityTaxtypeUsedCode","#{cacheManager.taxtypeMap[:var.:field]}");
			formatDataMap.put("stj1TaxtypeUsedCode","#{cacheManager.taxtypeMap[:var.:field]}");
			formatDataMap.put("stj2TaxtypeUsedCode","#{cacheManager.taxtypeMap[:var.:field]}");
			formatDataMap.put("stj3TaxtypeUsedCode","#{cacheManager.taxtypeMap[:var.:field]}");
			formatDataMap.put("stj4TaxtypeUsedCode","#{cacheManager.taxtypeMap[:var.:field]}");
			formatDataMap.put("stj5TaxtypeUsedCode","#{cacheManager.taxtypeMap[:var.:field]}");
			formatDataMap.put("stj6TaxtypeUsedCode","#{cacheManager.taxtypeMap[:var.:field]}");
			formatDataMap.put("stj7TaxtypeUsedCode","#{cacheManager.taxtypeMap[:var.:field]}");
			formatDataMap.put("stj8TaxtypeUsedCode","#{cacheManager.taxtypeMap[:var.:field]}");
			formatDataMap.put("stj9TaxtypeUsedCode","#{cacheManager.taxtypeMap[:var.:field]}");
			formatDataMap.put("stj10TaxtypeUsedCode","#{cacheManager.taxtypeMap[:var.:field]}");
			formatDataMap.put("stateCode","#{cacheManager.taxCodeStateMap[:var.:field].name} #{(not empty cacheManager.taxCodeStateMap[:var.:field].name) ? '(' : ''}#{:var.:field}#{(not empty cacheManager.taxCodeStateMap[:var.:field].name) ? ')' : ''}");
			formatDataMap.put("custStateCode","#{cacheManager.taxCodeStateMap[:var.:field].name} #{(not empty cacheManager.taxCodeStateMap[:var.:field].name) ? '(' : ''}#{:var.:field}#{(not empty cacheManager.taxCodeStateMap[:var.:field].name) ? ')' : ''}");
			formatDataMap.put("shiptoStateCode","#{cacheManager.taxCodeStateMap[:var.:field].name} #{(not empty cacheManager.taxCodeStateMap[:var.:field].name) ? '(' : ''}#{:var.:field}#{(not empty cacheManager.taxCodeStateMap[:var.:field].name) ? ')' : ''}");
			formatDataMap.put("shipfromStateCode","#{cacheManager.taxCodeStateMap[:var.:field].name} #{(not empty cacheManager.taxCodeStateMap[:var.:field].name) ? '(' : ''}#{:var.:field}#{(not empty cacheManager.taxCodeStateMap[:var.:field].name) ? ')' : ''}");
			formatDataMap.put("ordracptStateCode","#{cacheManager.taxCodeStateMap[:var.:field].name} #{(not empty cacheManager.taxCodeStateMap[:var.:field].name) ? '(' : ''}#{:var.:field}#{(not empty cacheManager.taxCodeStateMap[:var.:field].name) ? ')' : ''}");
			formatDataMap.put("ordrorgnStateCode","#{cacheManager.taxCodeStateMap[:var.:field].name} #{(not empty cacheManager.taxCodeStateMap[:var.:field].name) ? '(' : ''}#{:var.:field}#{(not empty cacheManager.taxCodeStateMap[:var.:field].name) ? ')' : ''}");
			formatDataMap.put("firstuseStateCode","#{cacheManager.taxCodeStateMap[:var.:field].name} #{(not empty cacheManager.taxCodeStateMap[:var.:field].name) ? '(' : ''}#{:var.:field}#{(not empty cacheManager.taxCodeStateMap[:var.:field].name) ? ')' : ''}");
			formatDataMap.put("billtoStateCode","#{cacheManager.taxCodeStateMap[:var.:field].name} #{(not empty cacheManager.taxCodeStateMap[:var.:field].name) ? '(' : ''}#{:var.:field}#{(not empty cacheManager.taxCodeStateMap[:var.:field].name) ? ')' : ''}");
			formatDataMap.put("billtoStateCode","#{cacheManager.taxCodeStateMap[:var.:field].name} #{(not empty cacheManager.taxCodeStateMap[:var.:field].name) ? '(' : ''}#{:var.:field}#{(not empty cacheManager.taxCodeStateMap[:var.:field].name) ? ')' : ''}");
			formatDataMap.put("exemptReason","#{cacheManager.listCodeMap['EXREASON'][:var.:field].description} #{(not empty cacheManager.listCodeMap['EXREASON'][:var.:field].description) ? '(' : ''}#{:var.:field}#{(not empty cacheManager.listCodeMap['EXREASON'][:var.:field].description) ? ')' : ''}");
			formatDataMap.put("exemptCode","#{cacheManager.listCodeMap['EXEMPTCODE'][:var.:field].description} #{(not empty cacheManager.listCodeMap['EXEMPTCODE'][:var.:field].description) ? '(' : ''}#{:var.:field}#{(not empty cacheManager.listCodeMap['EXEMPTCODE'][:var.:field].description) ? ')' : ''}");
			
		}	    
		return formatDataMap;
	}
	
	static private void addValue(HtmlColumn col, String var, String field, Method m, String maxdecimalplace) {
		// Field value
		if (m.getReturnType().equals(Boolean.class)) {
			
			HtmlSelectBooleanCheckbox o = new HtmlSelectBooleanCheckbox();
			o.setValueExpression("value", JsfHelper.createValueExpression("#{" + var + "." + field + "}", m.getReturnType()));
			o.setDisabled(true);
			o.setStyleClass("check");
			col.setStyle("text-align: center;");
			col.getChildren().add(o);
		} 
		else {	
			HtmlOutputText o = new HtmlOutputText();
			String expression  = getFormatDataMap().get(field);
			
			if(expression!=null){
				expression = expression.replaceAll(":var", var).replaceAll(":field", field);
				o.setValueExpression("value", JsfHelper.createValueExpression(expression, m.getReturnType()));
			}
			else{
				
			
				o.setValueExpression("value", JsfHelper.createValueExpression("#{" + var + "." + field + "}", m.getReturnType()));
			}
			
			// Stylize based on type of value
			if (m.getReturnType().equals(Date.class)) {
				if (field.endsWith("Timestamp")) {
					DateTimeConverter converter = new DateTimeConverter();
					converter.setType("both");
					converter.setPattern("MM/dd/yyyy h:mm:ss a");
					o.setConverter(converter);
				} else {
					DateConverter converter = new DateConverter();
					converter.setType("date");
					converter.setPattern("MM/dd/yyyy");
					o.setConverter(converter);
				}	
			}if (m.getReturnType().equals(Float.class) ||
				  m.getReturnType().equals(Double.class) ||
				  m.getReturnType().equals(BigDecimal.class)) {
				  col.setStyle("text-align: right;");
				
				if(field.endsWith("Rate")){
					PercentageConverter converter=new PercentageConverter();
                    if(maxdecimalplace !=null) {
						converter.setMaxFractionDigits(Integer.parseInt(maxdecimalplace));
					}
					o.setConverter(converter);
			
				}else{
					NumberConverter converter = new NumberConverter();
					String newPattern = returnNumberPattern("#,##0.00", maxdecimalplace);
				    converter.setPattern(newPattern);
					if(maxdecimalplace !=null) {
						converter.setMaxFractionDigits(Integer.parseInt(maxdecimalplace));
					}
					converter.setMinFractionDigits(2);
					o.setConverter(converter);
					
				}
			if(field.endsWith("MaxAmount")||field.endsWith("MinAmount")||field.endsWith("stateMaxtaxAmount"))
	        {
			 MaxAmountConverter maxConverter=new MaxAmountConverter();
			 String newPattern = returnNumberPattern("#,##0.00", maxdecimalplace);
			 maxConverter.setPattern(newPattern);
			 if(maxdecimalplace !=null) {
				 maxConverter.setMaxFractionDigits(Integer.parseInt(maxdecimalplace));
			 }
			 maxConverter.setMinFractionDigits(2);
	         o.setConverter(maxConverter);                    
	        }
			} if (m.getReturnType().equals(Long.class)) {
				col.setStyle("text-align: right;");
			}
			
			if(m.getReturnType().equals(String.class)) {
				o.setConverter(linkConverter);
			}
		
			col.getChildren().add(o);
		}
	}
	
	public static String returnNumberPattern(String pattern, String maxdecimalplace) {
		StringBuffer newPattern = new StringBuffer(pattern);
		StringBuffer appstr = new StringBuffer();
	    if(maxdecimalplace != null) {
	    	int poundcount = Integer.parseInt(maxdecimalplace) - 2;
		    for (int j =1;j<=poundcount;j++) 
		    	appstr.append("#");
	    }
	    return newPattern.append(appstr).toString();
	}
	
	public CacheManager getCacheManager() {
		return cacheManager;
	}

	public void setCacheManager(CacheManager cacheManager) {
		this.cacheManager = cacheManager;
	}
	
	public DriverReferenceService getDriverReferenceService() {
		return driverReferenceService;
	}

	public void setDriverReferenceService(DriverReferenceService driverReferenceService) {
		this.driverReferenceService = driverReferenceService;
	}

	public TaxCodeDetailService getTaxCodeDetailService() {
		return taxCodeDetailService;
	}

	public void setTaxCodeDetailService(TaxCodeDetailService taxCodeDetailService) {
		this.taxCodeDetailService = taxCodeDetailService;
	}

	public JurisdictionService getJurisdictionService() {
		return jurisdictionService;
	}

	public void setJurisdictionService(JurisdictionService jurisdictionService) {
		this.jurisdictionService = jurisdictionService;
	}

	public UserEntityService getUserEntityService() {
		return this.userEntityService;
	}

	public void setUserEntityService(UserEntityService userEntityService) {
		this.userEntityService = userEntityService;
	}

	public EntityItemDTO getEntityItemDTO() {
		return userEntityService.getEntityItemDTO();
	}

	public loginBean getLoginBean() {
		return this.loginBean;
	}

	public void setLoginBean(loginBean loginBean) {
		this.loginBean = loginBean;
	}
	
	public boolean getUserGlobalBooleanFlag() {
		return loginBean.getUser().getGlobalBooleanFlag();
	}
	public boolean getUserDefaultMatrixLineFlag() {
		return loginBean.getUser().getDefaultMatrixLineBooleanFlag();
	}

	public boolean getUserDefaultActiveFlag(){
		return loginBean.getUser().getActiveBooleanFlag();
	}
	public FilePreferenceBackingBean getFilePreferenceBean() {
		return this.filePreferenceBean;
	}
	public UserPreferenceDTO getUserPreferenceDTO() {
		return this.filePreferenceBean.getUserPreferenceDTO();
	}
	public MatrixDTO getMatrixDTO() {
		return this.filePreferenceBean.getMatrixDTO();
	}

	public void setFilePreferenceBean(FilePreferenceBackingBean filePreferenceBean) {
		this.filePreferenceBean = filePreferenceBean;
	}

	public List<TransactionHeader> getForHeaderRearrange() {
		return forHeaderRearrange;
	}

	public void setForHeaderRearrange(List<TransactionHeader> forHeaderRearrange) {
		this.forHeaderRearrange = forHeaderRearrange;
	}

	public TransactionDetailService getTransactionDetailService() {
		return transactionDetailService;
	}

	public void setTransactionDetailService(
			TransactionDetailService transactionDetailService) {
		this.transactionDetailService = transactionDetailService;
	}

	public List<SelectItem> getMultiTransCodeItems() {
		if (multiTransCodeItems == null) {
			multiTransCodeItems = new ArrayList<SelectItem>();
			
			List<ListCodes> codes = cacheManager.getListCodesByType("MULTITRANS"); 
			if (codes != null) {
				for (ListCodes code : codes) {
					multiTransCodeItems.add(new SelectItem(code.getCodeCode(), code.getDescription()));
				}
			}
			multiTransCodeItems.add(0, new SelectItem("", "Select Multi-Trans."));
		}
		
		return multiTransCodeItems;
	}

	public void setMultiTransCodeItems(List<SelectItem> multiTransCodeItems) {
		this.multiTransCodeItems = multiTransCodeItems;
	}

	public List<TransactionHeader> getForHeaderRearrangeForModal() {
		if(inColumnRefresh) {
			inColumnRefresh = false;
		}
		else if(forHeaderRearrangeTemp == null) {
			forHeaderRearrangeTemp = forHeaderRearrange;
		}
		return forHeaderRearrangeTemp;
	}
	
	public void setForHeaderRearrangeForModal(
			List<TransactionHeader> forHeaderRearrangeTemp) {
		MatrixCommonBean.forHeaderRearrangeTemp = forHeaderRearrangeTemp;
	}

	public void setForHeaderRearrangeTemp(
			List<TransactionHeader> forHeaderRearrangeTemp) {
		MatrixCommonBean.forHeaderRearrangeTemp = forHeaderRearrangeTemp;
	}

	public static List<TransactionHeader> getForHeaderRearrangeTemp() {
		return forHeaderRearrangeTemp;
	}
	
	public void refreshColumns(boolean allColumns) {
		Map<String,DataDefinitionColumn> propertyMap =	getTransactionPropertyMap();
		if(allColumns) {
			List<TransactionHeader> refreshList = new ArrayList<TransactionHeader>();
			for(String temp:fieldArray()){
				TransactionHeader header = new TransactionHeader();
				header.setColumnName(temp);
				header.setColumnLabel(getLabelForField(propertyMap, temp));
				refreshList.add(header);
			}
			forHeaderRearrangeTemp = refreshList;
		}
		else {
			List<TransactionHeader> refreshList = new ArrayList<TransactionHeader>();
			for(String temp:getDefaultColumns()){
				TransactionHeader header = new TransactionHeader();
				header.setColumnName(temp);
				header.setColumnLabel(getLabelForField(propertyMap, temp));
				refreshList.add(header);
			}
			forHeaderRearrangeTemp = refreshList;
		}
		
		inColumnRefresh = true;
	}
	
	//sometime columns saved in TB_DW_COLUMN may not have all fields. so add remaining fields to the end.
	//otherwise reorder columns will fail to save
	public void syncHeaderColumns(List<String> list, Set<String> fieldSet) {
		if(list != null && list.size() >= 0 && list.size() < fieldSet.size()) {//0002071
			for(String s : list) {
				fieldSet.remove(s);
			}
			
			for(String s : fieldSet) {
				list.add(s);
			}
		}
	}

	public DwColumnService getDwColumnService() {
		return dwColumnService;
	}
	
	public Map<String,DriverNames> getTaxMatrixDriverMap(){
		if (taxMatrixDriverMap == null) {
			taxMatrixDriverMap = cacheManager.getMatrixColDriverNamesMap(TaxMatrix.DRIVER_CODE);
		}
		return taxMatrixDriverMap;
	}
	
	public Map<String,DriverNames> getLocationMatrixDriverMap(){
		if (locationMatrixDriverMap == null) {
			locationMatrixDriverMap = cacheManager.getMatrixColDriverNamesMap(LocationMatrix.DRIVER_CODE);
		}
		return locationMatrixDriverMap;
	}
	
	
	
}
