package com.ncsts.view.bean;

/**
 * @author AChen
 * 
 */

import com.ncsts.common.LoggerFactory;
import com.ncsts.services.DatabaseSetupService;
import com.ncsts.view.bean.loginBean;
import org.apache.log4j.Logger;

public class DatabaseSetupBean {
	
	private Logger logger = LoggerFactory.getInstance().getLogger(DatabaseSetupBean.class);
	private DatabaseSetupService databaseSetupService;

	private boolean enterImportDefinitionSpecDoneFlag = false;
	private boolean enterDriversDoneFlag = false;
	private boolean enterEntityLevelsDoneFlag = false;
	private boolean enterEntitiesDoneFlag = false;
	
	private boolean verityEnterImportDefinitionSpecDoneFlag = false;
	private boolean verityEnterDriversDoneFlag = false;
	private boolean verityEnterEntityLevelsDoneFlag = false;
	private boolean verityEnterEntitiesDoneFlag = false;
 
	public DatabaseSetupService getDatabaseSetupService() {
		return databaseSetupService;
	}
	
	public void setDatabaseSetupService(DatabaseSetupService databaseSetupService) {
		this.databaseSetupService = databaseSetupService;
	}
	
	public boolean getEnterImportDefinitionSpecDoneFlag() {
		if(verityEnterImportDefinitionSpecDoneFlag){
			return enterImportDefinitionSpecDoneFlag;
		}
		
		enterImportDefinitionSpecDoneFlag = databaseSetupService.isEnterImportDefinitionSpecDone(); 
		logger.info("called isEnterImportDefinitionSpecDone()");
		
		verityEnterImportDefinitionSpecDoneFlag = true;
		return enterImportDefinitionSpecDoneFlag;
	}
	
	public boolean getEnterDriversDoneFlag() {
		if(verityEnterDriversDoneFlag){
			return enterDriversDoneFlag;
		}
		
		if(!enterImportDefinitionSpecDoneFlag){
			enterDriversDoneFlag = false;
		}
		else{
			enterDriversDoneFlag = databaseSetupService.isEnterDriversDone();
			logger.info("called isEnterDriversDone()");
		}
		
		verityEnterDriversDoneFlag = true;
		return enterDriversDoneFlag;
	}
	
	public boolean getEnterEntityLevelsDoneFlag() {	
		if(verityEnterEntityLevelsDoneFlag){
			return enterEntityLevelsDoneFlag;
		}
		
		if(!enterDriversDoneFlag){
			enterEntityLevelsDoneFlag = false;
		}
		else{
			enterEntityLevelsDoneFlag = databaseSetupService.isEnterEntityLevelsDone();
			logger.info("called isEnterEntityLevelsDone()");
		}
		
		verityEnterEntityLevelsDoneFlag = true;
		return enterEntityLevelsDoneFlag;
	}
	
	public boolean getEnterEntitiesDoneFlag() {
		if(verityEnterEntitiesDoneFlag){
			return enterEntitiesDoneFlag;
		}
		
		if(!enterEntityLevelsDoneFlag){
			enterEntitiesDoneFlag = false;
		}
		else{
			enterEntitiesDoneFlag = databaseSetupService.isEnterEntitiesDone();
			logger.info("called isEnterEntitiesDone()");
		}
		
		verityEnterEntitiesDoneFlag = true;
		return enterEntitiesDoneFlag;
	}
}