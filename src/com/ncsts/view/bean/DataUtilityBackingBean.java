package com.ncsts.view.bean;
/** 
 * Most of the code here is obsolete, except for the Archive stuff.
 * The GL and TRU logic has been moved to their own beans.
 * JMW 8/28/08
 */
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.ajax4jsf.component.UIDataAdaptor;
import org.apache.log4j.Logger;
import org.apache.myfaces.custom.fileupload.HtmlInputFileUpload;
import org.apache.myfaces.custom.fileupload.UploadedFile;
import org.richfaces.component.html.HtmlCalendar;

import com.ncsts.dao.PreferenceDAO.SystemPreference;
import com.ncsts.domain.BatchMaintenance;
import com.ncsts.domain.BatchMetadata;
import com.ncsts.domain.EntityItem;
import com.ncsts.domain.JurisdictionTaxrate;
import com.ncsts.domain.ListCodes;
import com.ncsts.domain.TaxRateUpdatesBatch;
import com.ncsts.dto.BatchMaintenanceDTO;
import com.ncsts.dto.ListCodesDTO;
import com.ncsts.jsf.model.BatchMaintenanceDataModel;
import com.ncsts.jsf.model.GlExtractAetnaDataModel;
import com.ncsts.services.BatchMaintenanceService;
import com.ncsts.services.CallGLExtractService;
import com.ncsts.services.EntityItemService;
import com.ncsts.services.ListCodesService;
import com.ncsts.services.PreferenceService;
import com.ncsts.view.util.FacesUtils;

public class DataUtilityBackingBean {
  @SuppressWarnings("unused")
  private Logger logger = Logger.getLogger(DataUtilityBackingBean.class);

  private BatchMaintenanceService batchMaintenanceService;
  private BatchMaintenance batchMaintenance;
  
  private BatchErrorsBackingBean batchErrorsBean;

  private CallGLExtractService callGLExtractService;

  private EntityItemService entityItemService; // for Batch archive add page

  private List<SelectItem> buItemList = new ArrayList<SelectItem>();

  private BatchMaintenanceDTO baAddDTO = new BatchMaintenanceDTO(); // for batch

  private BatchMaintenanceBackingBean batchMaintenanceBean;
  
  // archive
  // add page

  private List<BatchMaintenanceDTO> arBatchList;

  private GlExtractAetnaDataModel glExtractAetnaDataModel;

  private String description = "G/L Extract";

  private String batchStatus = "Extracted";

  // private String gldate;
  private String fileName = "";

  private String glSelection = "";

  private HtmlCalendar myCalendarDate = new HtmlCalendar();

  private HtmlInputFileUpload uploadFile;

  private String fileType = ""; // taxrate update

  private String releaseDate = ""; // taxrate update

  private String releaseVersion = ""; // taxrate update
  
  private String uploadFileName = ""; // taxrate update

  private BatchMaintenanceDTO trAddDTO = new BatchMaintenanceDTO(); // for

  private PreferenceService preferenceService;
  
  // taxRate
  // add page

  private BatchMaintenance selectedTRBatch = null;
  private BatchMaintenance selectedGLBatch = null;

  private int selectedTRBatchIndex = -1;
  private int selectedGLBatchIndex = -1;
  private boolean hasSelection;

  private BatchMaintenanceDTO statisticsTRBatch = new BatchMaintenanceDTO();

  // private Date gDate = new Date();
  private List<JurisdictionTaxrate> taxrateList = new ArrayList<JurisdictionTaxrate>();

  private ListCodesService listCodesService;

  private String trBatchStatus = null;

  private String trBatchType = null;

  private List<String> fileData;

  private Boolean displayUploadOK = false;

  private Date systemDate = null;

  private String elapsed = "00:00:00";

  private String estimatedTimeToEnd = "TBD";

  private Date estimatedEndDatetime = new Date();

  private Double rowPerMinute = 0.00;

  private Long percentage = 0L;

//  private List<BatchMaintenanceDTO> glBatchList;
//
//  private List<BatchMaintenanceDTO> trBatchList;

  private BatchMaintenanceDataModel trBatchMaintenanceDataModel;
  private BatchMaintenanceDataModel glBatchMaintenanceDataModel;

  private Map<String, String> batchtypeCodeCodeToDescriptionMap;

  private Map<String, String> batchstatCodeCodeToDescriptionMap;

  private Map<String, String> errorsevCodeCodeToDescriptionMap;

  private BatchMetadata glBatchMetadata;

  private BatchMetadata trBatchMetadata;

  
  public List<BatchMaintenanceDTO> getArBatchList() {
    if (arBatchList != null) {
      arBatchList.clear();
    }
    arBatchList = batchMaintenanceService.getAllBatchesByBatchTypeCode("A");
    return arBatchList;
  }

  public void setArBatchList(List<BatchMaintenanceDTO> arBatchList) {
    this.arBatchList = arBatchList;
  }

  public List<SelectItem> getBuItemList() {
    if (buItemList != null) {
      buItemList.clear();
    }
    List<EntityItem> list = entityItemService.findAllEntityItems();
    for (EntityItem ei : list) {
      buItemList.add(new SelectItem(ei.getEntityCode() + " - " + ei.getEntityName(), ei
          .getEntityCode() + " - " + ei.getEntityName()));
    }
    return buItemList;
  }

//  public String getErrorMessages() {
//    String message;
//    if (errorMessages.size() == 0) {
//      message = "";
//    } else {
//      message = "<FONT COLOR=RED><B><UL>\n";
//      for (int i = 0; i < errorMessages.size(); i++) {
//        message = message + "<LI>" + (String) errorMessages.get(i) + "\n";
//      }
//      message = message + "</UL></B></FONT>\n";
//    }
//    return (message);
//  }
//
  public void fileUploadAction() {
    FileOutputStream fw = null;
    BufferedOutputStream fos = null;
    BufferedReader din = null;
    InputStream stream = null;
    try {
      UploadedFile myFile = this.getUploadFile().getUploadedFile();
      logger.debug("file ::: " + myFile);
      byte[] buffer = null;
      String line = "";
      /* reading header values */
      stream = myFile.getInputStream();
      din = new BufferedReader(new InputStreamReader(stream));
      line = din.readLine();
      // Getting header info
      if (line != null) {
        String ar[] = line.split(",");
        this.fileType = ar[0].substring(6);
        this.releaseDate = ar[1].substring(7);
        this.releaseVersion = ar[2].substring(6);
      }
      /* reading file */
      stream = myFile.getInputStream();
      long fSize = myFile.getSize();
      buffer = new byte[(int) fSize];
      stream.read(buffer, 0, (int) fSize);
      String fileName = FacesUtils.getName(myFile);
      if (fileName == null) fileName = myFile.getName();

      /* Get context root and create file dir(s) if they do not exist */
      String path = FacesUtils.getServletContext().getRealPath("") + "/taxratefile/";
      new File(path).mkdirs();
      this.uploadFileName = fileName;
      File inputFile = new File(path + fileName);
      fw = new FileOutputStream(inputFile);
      fos = new BufferedOutputStream(fw);
      fos.write(buffer);

      this.displayUploadOK = true;
    } catch (Exception e) {
      FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_FATAL, e.getClass().getName(),
          e.getMessage());
      FacesContext.getCurrentInstance().addMessage(null, message);
      e.printStackTrace();
    } finally {
      try {
        if (fos != null) fos.close();
        if (fw != null) fw.close();
        if (stream != null) stream.close();
        if (din != null) din.close();
      } catch (IOException e) {
        e.printStackTrace();
        logger.debug("Exception while closing stream");
      }
    }
    // return "uploadSuccess";
  }

  public String processAddUpdateAction() {
    this.uploadFile = new HtmlInputFileUpload();
  this.fileType = null;
    this.releaseDate = null;
    this.releaseVersion = null;
    this.displayUploadOK = false;
    this.uploadFileName = "";
    return "data_utility_tradd";
  }

  // public boolean fileValidate(UploadedFile myFile, String header)
  // {
  // boolean isValid = true;
  // String fileExt = myFile.getContentType();
  //		
  // if(!fileExt.equals("text/plain")) {
  // errorMessages.add("Invalid content type, expected 'text/plain', but was " +
  // fileExt);
  // isValid = false;
  // } else if(header == null) {
  // errorMessages.add(myFile.getName() + " appears to be empty");
  // isValid = false;
  // } else if(!header.contains("Type")) {
  // errorMessages.add(myFile.getName() + " header line is missing Type
  // column");
  // isValid = false;
  // } else if(!header.contains("Date")) {
  // errorMessages.add(myFile.getName() + " header line is missing Date
  // column");
  // isValid = false;
  // }/*else {
  // logger.debug("header : " + header);
  // String ar[] = header.split(",");
  // String type = ar[0].substring(6);
  //		    
  // if(!type.equalsIgnoreCase("Full") || !type.equalsIgnoreCase("Update")){
  // logger.debug("fileType : " + type + type.length());
  // isValid = false;
  // }
  // }*/
  // return isValid;
  // }

	public void glSortAction(ActionEvent e) {
		glBatchMaintenanceDataModel.toggleSortOrder(e.getComponent().getParent().getId());
	}
	public void trSortAction(ActionEvent e) {
		trBatchMaintenanceDataModel.toggleSortOrder(e.getComponent().getParent().getId());
	}
	
	public String trSelectAllAction() {
		trBatchMaintenanceDataModel.setSelectAllTaxRateUpdates();
  	Iterator<BatchMaintenance> iter = trBatchMaintenanceDataModel.iterator();
  	hasSelection = false;
  	while (iter.hasNext()) {
  		if (iter.next().getSelected()) {
  			hasSelection = true;
  			break;
  		}
  	}
		return null;
	}
	
	public String addTRAction() {
    trAddDTO.setBatchTypeCode("TR");
    trAddDTO.setBatchStatusCode(TaxRateUpdatesBatch.IMPORTING);
    trAddDTO.setVc01(this.uploadFileName);
    trAddDTO.setVc02(this.fileType);
    trAddDTO.setNu01(new Double(releaseVersion));
    trAddDTO.setEntryTimestamp(new Date()); // hard coded
    batchMaintenanceService.save(trAddDTO);
    trAddDTO = new BatchMaintenanceDTO();
    return "uploadSuccess";
  }

  protected UIDataAdaptor selectedRowChanged(ActionEvent e) throws AbortProcessingException {
    UIComponent uiComponent = e.getComponent().getParent();
    if (!(uiComponent instanceof UIDataAdaptor)) {
      logger.info("Invalid class to event listener: " + uiComponent.getClass().getName());
      return null;
    }
    return (UIDataAdaptor) uiComponent;
  }
  public void selectedTRRowChanged(ActionEvent e) throws AbortProcessingException {
  	UIDataAdaptor table = selectedRowChanged(e);
    if (table != null) {
    	this.selectedTRBatch = (BatchMaintenance)table.getRowData();
    	this.selectedTRBatchIndex = table.getRowIndex();
    }
  }
  public void selectedGLRowChanged(ActionEvent e) throws AbortProcessingException {
  	UIDataAdaptor table = selectedRowChanged(e);
    if (table != null) {
    	this.selectedGLBatch = (BatchMaintenance)table.getRowData();
    	this.selectedGLBatchIndex = table.getRowIndex();
    }
  }

  public String viewTRAction() {
  	String filePath = this.selectedTRBatch.getVc01();
  	fileData = new ArrayList<String>();
  	if (filePath != null) {
      FileInputStream fstream = null;
      BufferedReader in = null;
      try {
        logger.debug("Current dir = " + new File(".").getAbsolutePath());
        fstream = new FileInputStream(filePath);
        in = new BufferedReader(new InputStreamReader(fstream));
        String line = null;
        int i = 0;

        while ((line = in.readLine()) != null) {
          fileData.add(line);
        }
        return "view_action";
      } catch (java.io.FileNotFoundException fe) {
      	FacesContext.getCurrentInstance().addMessage(null, new FacesMessage( FacesMessage.SEVERITY_ERROR, 
            "File not found: " + filePath, null));
      } catch (IOException fe) {
        fe.printStackTrace();
      	FacesContext.getCurrentInstance().addMessage(null, new FacesMessage( FacesMessage.SEVERITY_ERROR, 
  			"Error while loading file data: " + fe.getMessage(), null));
      } finally {
        try {
          if (fstream != null) fstream.close();
          if (in != null) in.close();
        } catch (IOException es) {
          es.printStackTrace();
          logger.debug("Exception while closing stream");
        }
      }
  	}
    return "";
  }

  public String cancelTrAction() {
    this.fileType = "";
    this.releaseDate = "";
    this.releaseVersion = "";
    this.displayUploadOK = false;

    this.selectedTRBatch = null;
    this.selectedTRBatchIndex = -1;

    estimatedEndDatetime = null;
    estimatedTimeToEnd = "";
    percentage = 0L;
    rowPerMinute = 0.0;
    elapsed = "";

    return "cancel_action";
  }

  public String errorsGLAction() {
	  batchErrorsBean.setBatchMaintenance(selectedGLBatch);
	  batchErrorsBean.setReturnView("glExtract_main");
	  
	  //0001701: Import errors make the system crash
	  batchErrorsBean.getBatchErrorDataModel().setBatchMaintenance(selectedGLBatch);
	  
	  return "batch_maintenance_errors";
  }
  public String errorsTRAction() {
	  batchErrorsBean.setBatchMaintenance(selectedTRBatch);
	  batchErrorsBean.setReturnView("taxrate_updates");
	  return "batch_maintenance_errors";
//     if (selectedTRBatch != null && selectedTRBatch.getBatchId() != null) {
//       this.taxRateErrorList = batchMaintenanceService.taxRateUpdateBatchErrors(selectedTRBatch
//           .getBatchId());
//       if (this.taxRateErrorList != null)
//         logger.debug("After getting data of errors " + this.taxRateErrorList.size());
//       return "taxrate_errors";
//     } else {
//       errorMessages.add("Please select a row");
//       errorMessage = true;
//       return "taxrate_updates";
//     }
  }

  @SuppressWarnings("deprecation")
  public String statisticsTRAction() {
    if (selectedTRBatch != null && selectedTRBatch.getBatchId() != null) {
      this.systemDate = new Date();
      this.statisticsTRBatch = (batchMaintenanceService.getBatchMaintananceData(selectedTRBatch
          .getBatchId())).getBatchMaintenanceDTO();

      Date actualStartTS = statisticsTRBatch.getActualStartTimestamp();
      Date actualEndTS = statisticsTRBatch.getActualEndTimestamp();
      Date sysdateTS = systemDate;
      Long totalRows = statisticsTRBatch.getTotalRows() != null ? statisticsTRBatch.getTotalRows()
          : 0L;
      Long processedRows = statisticsTRBatch.getProcessedRows() != null ? statisticsTRBatch
          .getProcessedRows() : 0L;

      // String estimatedEndDatetime = "TBD";

      // If processing has started, calculate fields
      if (actualStartTS != null && actualStartTS.getTime() != 0) {
        if (totalRows > 0) {
          this.percentage = (processedRows / totalRows) * 100;
        }
        if (actualEndTS != null && actualEndTS.getTime() != 0) {
          long processingTimeInMillis = actualEndTS.getTime() - actualStartTS.getTime();
          java.sql.Time t = new java.sql.Time(processingTimeInMillis + 21600000);
          elapsed = t.toString();
          estimatedTimeToEnd = elapsed;
          estimatedEndDatetime = statisticsTRBatch.getActualEndTimestamp();
          this.rowPerMinute = processedRows / (processingTimeInMillis / 60000.0);
        } else {
          long processingTimeInMillis = sysdateTS.getTime() - actualStartTS.getTime();
          java.sql.Time t = new java.sql.Time(processingTimeInMillis + 21600000);
          elapsed = t.toString();
          if (percentage > 0) {
            long i = (long) (processingTimeInMillis / (percentage / 100));
            estimatedTimeToEnd = new java.sql.Time(i + 21600000).toString();
            estimatedEndDatetime = new Date(sysdateTS.getTime() + i);
          }
          this.rowPerMinute = processedRows / (processingTimeInMillis / 60000.0);
        }
      }

      return "batch_statistics";
    } else {
      return "taxrate_updates";
    }
  }

//  public String processTRAction() {
//    if (selectedTRBatch != null && selectedTRBatch.getBatchId() != null) {
//      batchMaintenanceService.processTaxRateUpdateBatch(selectedTRBatch.getBatchId());
//      return "uploadSuccess";
//    } else {
//      return "taxrate_updates";
//    }
//
//  }

//  public String refreshTRAction() {
//  	trBatchMaintenanceDataModel.refreshData(false);
//    return "uploadSuccess";
//  }
//
//  public BatchMaintenanceDTO getTrAddDTO() {
//    return trAddDTO;
//  }

//  public void setTrAddDTO(BatchMaintenanceDTO trAddDTO) {
//    this.trAddDTO = trAddDTO;
//  }

  public String glAddAction() {
  	glExtractAetnaDataModel.setEnabled(false);
    return "glmarktrans_add";
  }

  /* --- Action handlers for GL Extract -- */
  public String extractAction() {
    Date d = (Date)myCalendarDate.getValue();
    DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
    String glDate = df.format(d);
    callGLExtractService.callGlExtractProc(glDate, fileName, 0, 0);
  	glExtractAetnaDataModel.setEnabled(true);
    return null;
  }

  public String saveAction() {
    Date d = (Date) myCalendarDate.getValue();
    DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
    String glDate = df.format(d);
    callGLExtractService.callGlExtractProc(glDate, fileName, 1, 0);
    FacesContext.getCurrentInstance().addMessage(null, 
    		new FacesMessage(FacesMessage.SEVERITY_INFO, fileName + " successfully extracted", null));
    return "glExtract_main";
  }
  public String closeAction() {
    return "glExtract_main";
  }


  public String baAddAction() {
    baAddDTO.setBatchTypeCode("A");
    baAddDTO.setBatchStatusCode("FA"); // how to determine batch status code
    // need business logic
    baAddDTO.setVc01("USER_TEXT_23"); // transaction column name.
    String buName = baAddDTO.getVc03();
    String[] bunameArr = buName.split("-");
    baAddDTO.setVc02(bunameArr[0].trim());
    baAddDTO.setEntryTimestamp(new Date());
    baAddDTO.setSchedStartTimestamp(new Date());
    baAddDTO.setActualStartTimestamp(new Date());
    baAddDTO.setActualEndTimestamp(new Date());
    batchMaintenanceService.save(baAddDTO);
    baAddDTO = new BatchMaintenanceDTO();
    return "batch_archive";
  }

  public String getFileName() {
    return createFilename();
  }

  public void setFileName(String fileName) {
    this.fileName = fileName;
  }

  public HtmlCalendar getMyCalendarDate() {
    DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
    HtmlCalendar myCalendarDate = new HtmlCalendar();
    // myCalendarDate.setCurrentDate(dateFormat.format(new Date()));
    myCalendarDate.setValue(dateFormat.format(new Date()));
    return myCalendarDate;
  }

  public void setMyCalendarDate(HtmlCalendar myCalendarDate) {
    this.myCalendarDate = myCalendarDate;
  }

  /**
   * 
   * @return default file name This name has to be created from the preferences,
   *         can be replaced with the preferences gl extract file name.
   */
  private String createFilename() {
    Date d = new Date();
    DateFormat dateFormat = new SimpleDateFormat("MMddyyyy");
    String fileString = "bw_" + dateFormat.format(d) + "-BU#.txt"; // why this
    // rule is
    // hard
    // coded..this
    // rule
    // should be
    // extracted
    // from
    // preferences.
    return fileString;
  }

  private void init() {
    // We need both batchMaintenanceService and listCodesService to be non-null
    if ((batchMaintenanceService == null) || (listCodesService == null)) {
      return;
    }

    initDescriptionMaps();
    glInit();
  }

  private void initDescriptionMaps() {
    batchtypeCodeCodeToDescriptionMap = getCodeCodeToDescriptionMap("BATCHTYPE");
    batchstatCodeCodeToDescriptionMap = getCodeCodeToDescriptionMap("BATCHSTAT");
    errorsevCodeCodeToDescriptionMap = getCodeCodeToDescriptionMap("ERRORSEV");
  }

  private void glInit() {
    // glBatchList = batchMaintenanceService.getAllBatchesByBatchTypeCode("GE",
    // new ArrayList<String>(batchtypeCodeCodeToDescriptionMap.keySet()),
    // new ArrayList<String>(batchstatCodeCodeToDescriptionMap.keySet()),
    // new ArrayList<String>(errorsevCodeCodeToDescriptionMap.keySet()));
    glBatchMetadata = batchMaintenanceService.findBatchMetadataById("GE");
		batchMaintenance = new BatchMaintenance();
		batchMaintenance.setEntryTimestamp(null);
		batchMaintenance.setBatchStatusCode(null);
		batchMaintenance.setBatchTypeCode("GE");
  	trBatchMaintenanceDataModel.setPageSize(50);
		glBatchMaintenanceDataModel.setCriteria(batchMaintenance);
  }

  /**
   * For the specified CODE_TYPE_CODE in the table TB_LIST_CODE, create a map of
   * CODE_CODE to DESCRPTION.
   * 
   * @param codeTypeCode The CODE_TYPE_CODE to filter on
   * @return For the specified CODE_TYPE_CODE, a map of CODE_CODE to
   *         DESCRIPTION.
   */
  private Map<String, String> getCodeCodeToDescriptionMap(String codeTypeCode) {
    Map<String, String> map = new HashMap<String, String>();
    List<ListCodes> listCodes = listCodesService.getListCodesByCodeTypeCode(codeTypeCode);

    for (ListCodes lc : listCodes) {
      map.put(lc.getCodeCode(), lc.getDescription());
    }

    return map;
  }

  public BatchMetadata getGlBatchMetadata() {
    if (glBatchMetadata == null) glRefreshView();
    return glBatchMetadata;
  }

  public String glRefreshView() {
    initDescriptionMaps();
    glInit();
  	glBatchMaintenanceDataModel.refreshData(false);
    return null;
  }

  /**
   * @param uploadFileName the uploadFileName to set
   */
  public void setUploadFileName(String uploadFileName) {
    this.uploadFileName = uploadFileName;
  }

  /**
   * @return the batchMaintenanceBean
   */
  public BatchMaintenanceBackingBean getBatchMaintenanceBean() {
    return this.batchMaintenanceBean;
  }

  /**
   * @param batchMaintenanceBean the batchMaintenanceBean to set
   */
  public void setBatchMaintenanceBean(BatchMaintenanceBackingBean batchMaintenanceBean) {
    this.batchMaintenanceBean = batchMaintenanceBean;
  }
  /**
   * @return the batchErrorsBean
   */
  public BatchErrorsBackingBean getBatchErrorsBean() {
    return this.batchErrorsBean;
  }
  /**
   * @param batchErrorsBean the batchErrorsBean to set
   */
  public void setBatchErrorsBean(BatchErrorsBackingBean batchErrorsBean) {
    this.batchErrorsBean = batchErrorsBean;
  }
	/**
	 * @return the glBatchMaintenanceDataModel
	 */
	public BatchMaintenanceDataModel getGlBatchMaintenanceDataModel() {
	    if (glBatchMetadata == null) {
	      initDescriptionMaps();
	      glInit();
	    }
		return this.glBatchMaintenanceDataModel;
	}
	/**
	 * @param glBatchMaintenanceDataModel the glBatchMaintenanceDataModel to set
	 */
	public void setGlBatchMaintenanceDataModel(BatchMaintenanceDataModel glBatchMaintenanceDataModel) {
		this.glBatchMaintenanceDataModel = glBatchMaintenanceDataModel;
	}

  /**
	 * @return the fileData
	 */
	public List<String> getFileData() {
		return this.fileData;
	}

	/**
	 * @param fileData the fileData to set
	 */
	public void setFileData(List<String> fileData) {
		this.fileData = fileData;
	}

	/**
   * @return the uploadFileName
   */
  public String getUploadFileName() {
    return this.uploadFileName;
  }

  public Boolean getDisplayUploadOK() {
    return displayUploadOK;
  }

  public void setDisplayUploadOK(Boolean display) {
    this.displayUploadOK = display;
  }

  public List<JurisdictionTaxrate> getTaxrateList() {
    return taxrateList;
  }

  public void setTaxrateList(List<JurisdictionTaxrate> taxrateList) {
    this.taxrateList = taxrateList;
  }

  public List<ListCodesDTO> getListCodesList() {
    return listCodesService.getListCodesByCodeCode("TR");
  }

  public String getTrBatchStatus() {
    return trBatchStatus;
  }

  public void setTrBatchStatus(String trBatchStatus) {
    this.trBatchStatus = trBatchStatus;
  }
  public String getTrBatchType() {
    List<ListCodesDTO> listCodes = listCodesService.getListCodesByCodeCode("TR");
    for (Iterator<ListCodesDTO> itr = listCodes.iterator(); itr.hasNext();) {
      ListCodesDTO lcd = itr.next();
      // if(lcd.getCodeTypeCode() == "BATCHTYPE")
      this.trBatchType = lcd.getDescription();
      // else if(lcd.getCodeTypeCode() == "BATCHSTAT")
      this.trBatchStatus = lcd.getDescription();
    }
    return trBatchType;
  }

  public void setTrBatchType(String trBatchType) {
    this.trBatchType = trBatchType;
  }

  public BatchMaintenanceDTO getStatisticsTRBatch() {
    return statisticsTRBatch;
  }

  public void setStatisticsTRBatch(BatchMaintenanceDTO statisticsTRBatch) {
    this.statisticsTRBatch = statisticsTRBatch;
  }

  /**
   * @return the uploadFile
   */
  public HtmlInputFileUpload getUploadFile() {
    return this.uploadFile;
  }

  /**
   * @param uploadFile the uploadFile to set
   */
  public void setUploadFile(HtmlInputFileUpload uploadFile) {
    this.uploadFile = uploadFile;
  }

  public Date getSystemDate() {
    return systemDate;
  }

  public void setSystemDate(Date systemDate) {
    this.systemDate = systemDate;
  }

  public String getElapsed() {
    return elapsed;
  }

  public void setElapsed(String elapsed) {
    this.elapsed = elapsed;
  }

  public Double getRowPerMinute() {
    return rowPerMinute;
  }

  public void setRowPerMinute(Double rowPerMinute) {
    this.rowPerMinute = rowPerMinute;
  }

  public Long getPercentage() {
    return percentage;
  }

  public void setPercentage(Long percentage) {
    this.percentage = percentage;
  }

  public String getEstimatedTimeToEnd() {
    return estimatedTimeToEnd;
  }

  public void setEstimatedTimeToEnd(String estimatedTimeToEnd) {
    this.estimatedTimeToEnd = estimatedTimeToEnd;
  }

  public Date getEstimatedEndDatetime() {
    return estimatedEndDatetime;
  }

  public void setEstimatedEndDatetime(Date estimatedEndDatetime) {
    this.estimatedEndDatetime = estimatedEndDatetime;
  }

  public EntityItemService getEntityItemService() {
    return entityItemService;
  }

  public void setEntityItemService(EntityItemService entityItemService) {
    this.entityItemService = entityItemService;
  }

  public CallGLExtractService getCallGLExtractService() {
    return callGLExtractService;
  }

  public void setCallGLExtractService(CallGLExtractService callGLExtractService) {
    this.callGLExtractService = callGLExtractService;
  }

  public BatchMaintenanceService getBatchMaintenanceService() {
    return batchMaintenanceService;
  }

  public void setBatchMaintenanceService(BatchMaintenanceService batchMaintenanceService) {
    this.batchMaintenanceService = batchMaintenanceService;
  }

  public ListCodesService getListCodesService() {
    return listCodesService;
  }

  public void setListCodesService(ListCodesService listCodesService) {
    this.listCodesService = listCodesService;
  }

  public Map<String, String> getBatchtypeCodeCodeToDescriptionMap() {
    return batchtypeCodeCodeToDescriptionMap;
  }

  public Map<String, String> getBatchstatCodeCodeToDescriptionMap() {
    return batchstatCodeCodeToDescriptionMap;
  }

  public Map<String, String> getErrorsevCodeCodeToDescriptionMap() {
    return errorsevCodeCodeToDescriptionMap;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getBatchStatus() {
    return batchStatus;
  }

  public void setBatchStatus(String batchStatus) {
    this.batchStatus = batchStatus;
  }
  public void setBuItemList(List<SelectItem> buItemList) {
    this.buItemList = buItemList;
  }

  public BatchMaintenanceDTO getBaAddDTO() {
    return baAddDTO;
  }

  public void setBaAddDTO(BatchMaintenanceDTO baAddDTO) {
    this.baAddDTO = baAddDTO;
  }

  public String getGlSelection() {
    return glSelection;
  }

  public void setGlSelection(String glSelection) {
    this.glSelection = glSelection;
  }

  public String getDescription() {
    return description;
  }

  public BatchMaintenance getSelectedTRBatch() {
    return selectedTRBatch;
  }
  public void setSelectedTRBatch(BatchMaintenance selectedTRBatch) {
    this.selectedTRBatch = selectedTRBatch;
  }
  public int getSelectedTRBatchIndex() {
    return selectedTRBatchIndex;
  }
  public void setSelectedTRBatchIndex(int selectedTRBatchIndex) {
    this.selectedTRBatchIndex = selectedTRBatchIndex;
  }

  public BatchMaintenance getSelectedGLBatch() {
    return selectedGLBatch;
  }
  public void setSelectedGLBatch(BatchMaintenance selectedGLBatch) {
    this.selectedGLBatch = selectedGLBatch;
  }

  public int getSelectedGLBatchIndex() {
    return selectedGLBatchIndex;
  }

  public void setSelectedGLBatchIndex(int selectedGLBatchIndex) {
    this.selectedGLBatchIndex = selectedGLBatchIndex;
  }

  public Boolean getDisplayProcessTR() {
  	if (selectedTRBatchIndex != -1) return true;
  	return false;
  }
  public Boolean getDisplayErrorTR() {
		return (selectedTRBatchIndex != -1) && (selectedTRBatch.getErrorSevCode() != null) &&
		(selectedTRBatch.getErrorSevCode().trim().length() > 0);
	}
	public Boolean getDisplayErrorGL() {
		return (selectedGLBatchIndex != -1) && (selectedGLBatch.getErrorSevCode() != null) &&
		(selectedGLBatch.getErrorSevCode().trim().length() > 0);
	}
  public String getFileType() {
    return fileType;
  }

  public void setFileType(String fileType) {
    this.fileType = fileType;
  }

  public String getReleaseDate() {
    return releaseDate;
  }

  public void setReleaseDate(String releaseDate) {
    this.releaseDate = releaseDate;
  }

  public String getReleaseVersion() {
    return releaseVersion;
  }

  public void setReleaseVersion(String releaseVersion) {
    this.releaseVersion = releaseVersion;
  }

  public String getLastTaxRateRelUpdate() {
  	return preferenceService.getSystemPreference(SystemPreference.LASTTAXRATEDATEUPDATE, "");
  }

	/**
	 * @return the preferenceService
	 */
	public PreferenceService getPreferenceService() {
		return this.preferenceService;
	}

	/**
	 * @param preferenceService the preferenceService to set
	 */
	public void setPreferenceService(PreferenceService preferenceService) {
		this.preferenceService = preferenceService;
	}

	/**
	 * @return the glExtractAetnaDataModel
	 */
	public GlExtractAetnaDataModel getGlExtractAetnaDataModel() {
		return this.glExtractAetnaDataModel;
	}

	/**
	 * @param glExtractAetnaDataModel the glExtractAetnaDataModel to set
	 */
	public void setGlExtractAetnaDataModel(GlExtractAetnaDataModel glExtractAetnaDataModel) {
		this.glExtractAetnaDataModel = glExtractAetnaDataModel;
	}

}
