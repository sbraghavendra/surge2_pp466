package com.ncsts.view.bean;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import javax.faces.component.UIInput;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;

import org.ajax4jsf.component.html.HtmlAjaxSupport;
import org.springframework.util.StringUtils;

import com.ncsts.common.CacheManager;
import com.ncsts.domain.Jurisdiction;
import com.ncsts.domain.Option;
import com.ncsts.domain.OptionCodePK;
import com.ncsts.domain.TaxCodeStatePK;
import com.ncsts.dto.NexusJurisdictionItemDTO;
import com.ncsts.dto.TaxCodeStateDTO;
import com.ncsts.dto.UserDTO;
import com.ncsts.services.JurisdictionService;
import com.ncsts.services.OptionService;
import com.ncsts.services.TaxCodeStateService;

public class JurisdictionUpdateHandler {
	private CacheManager cacheManager;
	private JurisdictionService jurisdictionService;
	private TaxCodeStateService taxCodeStateService;
	private OptionService optionService;
	
	private HtmlSelectOneMenu levelInput = new HtmlSelectOneMenu();
	private HtmlSelectOneMenu countryInput = new HtmlSelectOneMenu();
	private HtmlSelectOneMenu stateInput = new HtmlSelectOneMenu();
	private HtmlSelectOneMenu countyInput = new HtmlSelectOneMenu();	
	private HtmlSelectOneMenu cityInput = new HtmlSelectOneMenu();
	private HtmlSelectOneMenu stjInput = new HtmlSelectOneMenu();
	
	private String stj;
	private String city;
   	private String county;
   	private String state;
   	private String country;
   	private String level;
   	private String previousLevel = "";
   	
   	List<SelectItem> levelItems;
   	List<SelectItem> countryItems;
   	List<SelectItem> stateItems;
   	List<SelectItem> countyItems;
   	List<SelectItem> cityItems;
   	List<SelectItem> stjItems;

	public HtmlSelectOneMenu getCountryInput() {
		return countryInput;
	}

	public void setCountryInput(HtmlSelectOneMenu countryInput) {
		this.countryInput = countryInput;
	}

	public HtmlSelectOneMenu getStateInput() {
		return stateInput;
	}

	public void setStateInput(HtmlSelectOneMenu stateInput) {
		this.stateInput = stateInput;
	}

	public HtmlSelectOneMenu getCountyInput() {
		return countyInput;
	}

	public void setCountyInput(HtmlSelectOneMenu countyInput) {
		this.countyInput = countyInput;
	}

	public HtmlSelectOneMenu getCityInput() {
		return cityInput;
	}

	public void setCityInput(HtmlSelectOneMenu cityInput) {
		this.cityInput = cityInput;
	}
	
	public HtmlSelectOneMenu getStjInput() {
		return stjInput;
	}

	public void setStjInput(HtmlSelectOneMenu stjInput) {
		this.stjInput = stjInput;
	}
	
	public HtmlSelectOneMenu getLevelInput() {
		return levelInput;
	}

	public void setLevelInput(HtmlSelectOneMenu levelInput) {
		this.levelInput = levelInput;
	}
	
	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
		setInputValue(levelInput, level);
	}
	
	public String getStj() {
		return stj;
	}

	public void setStj(String stj) {
		this.stj = stj;
		setInputValue(stjInput, stj);
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
		setInputValue(cityInput, city);
	}

	public String getCounty() {
		return county;
	}

	public void setCounty(String county) {
		this.county = county;
		setInputValue(countyInput, county);
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
		setInputValue(stateInput, state);
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
		setInputValue(countryInput, country);
	}
	
	public boolean getDisableCountry() {
		if(level!=null && level.length()>0){
			return false;
		}
		else{
			return true;
		}
	}
	
	public boolean getDisableState() {
		if(level!=null && (level.equalsIgnoreCase("*STATE") || level.equalsIgnoreCase("*COUNTY") || 
				level.equalsIgnoreCase("*CITY") || level.equalsIgnoreCase("*STJ"))){
			return false;
		}
		else{
			return true;
		}
	}
	
	public boolean getDisableCounty() {
		if(level!=null && (level.equalsIgnoreCase("*COUNTY") || 
				level.equalsIgnoreCase("*CITY") || level.equalsIgnoreCase("*STJ"))){
			return false;
		}
		else{
			return true;
		}
	}
	
	public boolean getDisableForSTJ() {
		if(level.equalsIgnoreCase("*STJ"))
			return true;
		else
			return false;
	}
	
	public String getPreviousLevel() {
		return previousLevel;
	}

	public void setPreviousLevel(String previousLevel) {
		this.previousLevel = previousLevel;
	}

	public void levelSelected(ActionEvent e) {
		String level = (String) ((HtmlSelectOneMenu) ((HtmlAjaxSupport) e.getComponent()).getParent()).getSubmittedValue();
		
		setLevel(level);
		setCountry("");
		setState("");
		setCounty("*ALL");
		setCity("*ALL");
		setStj("*ALL");
		
		clearStateItems();
		clearCountyItems();	
		clearCityItems();
		clearStjItems();
		
		if(level!=null && (level.equalsIgnoreCase("*COUNTRY"))){
			setCountry(getDefUserCountry());
			
			stateItems = new ArrayList<SelectItem>();
			stateItems.add(new SelectItem("*ALL", "*ALL"));
			
			setState("*ALL");
		}
		else if(level!=null && (level.equalsIgnoreCase("*STATE") || level.equalsIgnoreCase("*COUNTY") || 
				level.equalsIgnoreCase("*CITY") || level.equalsIgnoreCase("*STJ"))){
			setCountry(getDefUserCountry());
			createStateItems();
		}

	}
	
	public void countrySelected(ActionEvent e) {
		String country = (String) ((HtmlSelectOneMenu) ((HtmlAjaxSupport) e.getComponent()).getParent()).getSubmittedValue();
		
		setCountry(country);
		setState("");
		setCounty("*ALL");
		setCity("*ALL");
		setStj("*ALL");
		
		createStateItems();
		
		clearCountyItems();	
		clearCityItems();
		clearStjItems();
	}
	
	public void stateSelected(ActionEvent e) {
		String state = (String) ((HtmlSelectOneMenu) ((HtmlAjaxSupport) e.getComponent()).getParent()).getSubmittedValue();
		
		setState(state);
		setCounty("*ALL");
		setCity("*ALL");
		setStj("*ALL");
		
		if(level!=null && level.equalsIgnoreCase("*STATE")){
			clearCountyItems();	
			clearCityItems();
			clearStjItems();
		}
		else if(level!=null && (level.equalsIgnoreCase("*COUNTY") || level.equalsIgnoreCase("*CITY") || level.equalsIgnoreCase("*STJ"))){
			createCountyItems();
			createCityItems();
			createStjItems();
		}
	}
	
	public void countySelected(ActionEvent e) {
		String county = (String) ((HtmlSelectOneMenu) ((HtmlAjaxSupport) e.getComponent()).getParent()).getSubmittedValue();	
		
		if(!this.county.equalsIgnoreCase("*ALL")){
			setCounty(county);
			
			//Change from a county to a new county, refresh other 2 combo boxes
			setCity("*ALL");
			setStj("*ALL");
			createCityItems();
			createStjItems();
		}
		else{
			setCounty(county);
			
			//Change from "ALL" to a new county, refresh combo boxes, which have "*ALL" 
			if(this.city.equalsIgnoreCase("*ALL")){
				createCityItems();
			}
			if(this.stj.equalsIgnoreCase("*ALL")){
				createStjItems();
			}	
		}
	}
	
	public void citySelected(ActionEvent e) {
		String city = (String) ((HtmlSelectOneMenu) ((HtmlAjaxSupport) e.getComponent()).getParent()).getSubmittedValue();
		
		if(!this.city.equalsIgnoreCase("*ALL")){
			setCity(city);
			
			//Change from a city to a new city, refresh other 2 combo boxes
			setCounty("*ALL");
			setStj("*ALL");
			createCountyItems();
			createStjItems();
		}
		else{
			setCity(city);
			
			//Change from "ALL" to a new city, refresh combo boxes, which have "*ALL" 
			if(this.county.equalsIgnoreCase("*ALL")){
				createCountyItems();
			}
			if(this.stj.equalsIgnoreCase("*ALL")){
				createStjItems();
			}	
		}
	}
	
	public void stjSelected(ActionEvent e) {
		String stj = (String) ((HtmlSelectOneMenu) ((HtmlAjaxSupport) e.getComponent()).getParent()).getSubmittedValue();
		
		if(!this.stj.equalsIgnoreCase("*ALL")){
			setStj(stj);
			
			//Change from a stj to a new stj, refresh other 2 combo boxes
			setCounty("*ALL");
			setCity("*ALL");
			createCountyItems();
			createCityItems();
		}
		else{
			setStj(stj);
			
			//Change from "ALL" to a new stj, refresh combo boxes, which have "*ALL" 
			if(this.county.equalsIgnoreCase("*ALL")){
				createCountyItems();
			}
			if(this.city.equalsIgnoreCase("*ALL")){
				createCityItems();
			}	
		}
	}
	
	public void setDisplayViewOnly(){
		
		stateItems = new ArrayList<SelectItem>();
		stateItems.add(new SelectItem(state,state));
		
		countyItems = new ArrayList<SelectItem>();
		countyItems.add(new SelectItem(county,county));
		
		cityItems = new ArrayList<SelectItem>();
		cityItems.add(new SelectItem(city,city));
		
		stjItems = new ArrayList<SelectItem>();
		stjItems.add(new SelectItem(stj,stj));
	}
	
	private void clearCountyItems() {
		countyItems = new ArrayList<SelectItem>();
		countyItems.add(new SelectItem("*ALL","*ALL"));
	}
	
	private void clearCityItems() {
		cityItems = new ArrayList<SelectItem>();
		cityItems.add(new SelectItem("*ALL","*ALL"));
	}
	
	private void clearStjItems(){
		stjItems = new ArrayList<SelectItem>();
		stjItems.add(new SelectItem("*ALL","*ALL"));
	}
	
	private void clearStateItems(){
		stateItems = new ArrayList<SelectItem>();
		stateItems.add(new SelectItem("","Select State"));
		stateItems.add(new SelectItem("*ALL", "*ALL"));
	}
	
	private void createStjItems(){
		List<String> listObject = jurisdictionService.findLocalStj(country, state, county, city);
		stjItems = new ArrayList<SelectItem>();
		stjItems.add(new SelectItem("*ALL", "*ALL"));
		
		String stjName = null;
		if(listObject != null) {	
			for(String n : listObject) {
				stjName = (String) n;
				stjItems.add(new SelectItem(stjName, stjName));
			}
		}	
	}
	
	private void createStjItemsBK(){
		Jurisdiction jurisdiction = new Jurisdiction();
		jurisdiction.setCountry(country);
		jurisdiction.setState(state);
		
		TaxCodeStatePK pk = new TaxCodeStatePK();
		pk.setCountry(country);
		pk.setTaxcodeStateCode(state);
		
		List<String>  listObject = null;
		boolean hasLocal = false;
		
		if(StringUtils.hasText(country) && StringUtils.hasText(state)) {
			TaxCodeStateDTO tcs = taxCodeStateService.findByPK(pk);
			if(tcs != null && "L".equalsIgnoreCase(tcs.getLocalTaxabilityCode())) {
				hasLocal = true;
			}
			
			if(hasLocal) {
			//	list = jurisdictionService.searchByExample(jurisdiction, true);
				
				
				listObject = jurisdictionService.findCountryStateStj(country, state);
			}
		}
		
		stjItems = new ArrayList<SelectItem>();
		stjItems.add(new SelectItem("*ALL", "*ALL"));
		
		Set<String> stjsSet = new TreeSet<String>();
		String stjName = null;
		if(listObject != null) {
			//if(listObject.size() > 0 || !hasLocal) {
			//	stjItems.add(new SelectItem("*ALL", "*ALL"));
			//}
			
			for(String n : listObject) {
				stjName = (String) n;
				stjItems.add(new SelectItem(stjName, stjName));
			}
		}	
	}
	
	private void createCountyCityItemsBK() {
		Jurisdiction jurisdiction = new Jurisdiction();
		jurisdiction.setCountry(country);
		jurisdiction.setState(state);
		
		TaxCodeStatePK pk = new TaxCodeStatePK();
		pk.setCountry(country);
		pk.setTaxcodeStateCode(state);
		
		List<Jurisdiction> list = null;
		boolean hasLocal = false;
		
		if(StringUtils.hasText(country) && StringUtils.hasText(state)) {
			TaxCodeStateDTO tcs = taxCodeStateService.findByPK(pk);
			if(tcs != null && "L".equalsIgnoreCase(tcs.getLocalTaxabilityCode())) {
				hasLocal = true;
			}
			
			if(hasLocal) {
				list = jurisdictionService.searchByExample(jurisdiction, true);
			}
		}
		
		countyItems = new ArrayList<SelectItem>();
		cityItems = new ArrayList<SelectItem>();
		countyItems.add(new SelectItem("*ALL", "*ALL"));
		cityItems.add(new SelectItem("*ALL", "*ALL"));
		
		Set<String> countiesSet = new TreeSet<String>();
		Set<String> citiesSet = new TreeSet<String>();
		
		if(list != null || !hasLocal) {
			if(hasLocal) {
				for(Jurisdiction j : list) {
					countiesSet.add(j.getCounty());
					citiesSet.add(j.getCity());
				}
			}
		
			for(String s : countiesSet) {
				countyItems.add(new SelectItem(s, s));
			}
			
			for(String s : citiesSet) {
				cityItems.add(new SelectItem(s, s));
			}
		}		
	}
	
	public void createCountyItems(){
		List<String> listObject = jurisdictionService.findLocalCounty(country, state, city, stj) ;
		countyItems = new ArrayList<SelectItem>();
		countyItems.add(new SelectItem("*ALL", "*ALL"));
		
		String name = null;
		if(listObject != null) {	
			for(String n : listObject) {
				name = (String) n;
				countyItems.add(new SelectItem(name, name));
			}
		}	
	}
	
	public void createCountyItemsIgnoreCity(){
		List<String> listObject = jurisdictionService.findLocalCounty(country, state,"*ALL","*ALL") ;
		countyItems = new ArrayList<SelectItem>();
		countyItems.add(new SelectItem("*ALL", "*ALL"));
		
		String name = null;
		if(listObject != null) {	
			for(String n : listObject) {
				name = (String) n;
				countyItems.add(new SelectItem(name, name));
			}
		}	
	}
	
	
	public void createCityItems(){
		List<String> listObject = jurisdictionService.findLocalCity(country, state, county, stj);
		cityItems = new ArrayList<SelectItem>();
		cityItems.add(new SelectItem("*ALL", "*ALL"));
		
		String name = null;
		if(listObject != null) {	
			for(String n : listObject) {
				name = (String) n;
				cityItems.add(new SelectItem(name, name));
			}
		}	
	}

	private void setInputValue(UIInput input, String value) {
		input.setLocalValueSet(false);
		input.setSubmittedValue(null);
		input.setValue(value);
	}
	
	private String getInputValue(UIInput input) {
		String value = (String) input.getSubmittedValue();
		return (value != null)? value:(String) input.getValue();
	}

	public CacheManager getCacheManager() {
		return cacheManager;
	}

	public void setCacheManager(CacheManager cacheManager) {
		this.cacheManager = cacheManager;
	}
	
	public String getDefUserCountry() {
		HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
		UserDTO userDTO = (UserDTO) session.getAttribute("user");
		String userCode = userDTO.getUserCode(); 
		Option option = optionService.findByPK(new OptionCodePK ("DEFUSERCOUNTRY", "USER", userCode));
		return option.getValue();
	}
	
	public List<SelectItem> getCountryItems() {
		if(countryItems==null){
			countryItems = cacheManager.createCountryItems();
		}
		return countryItems;
	}
	
	public void createStateItems() {
		String strCountry = country;
		stateItems = new ArrayList<SelectItem>();
		stateItems.add(new SelectItem("","Select State"));
		stateItems.add(new SelectItem("*ALL", "*ALL"));
		Collection<TaxCodeStateDTO> taxCodeStateDTOList = cacheManager.getTaxCodeStateMap().values();
		if(strCountry!=null && strCountry.length()>0){  
			for (TaxCodeStateDTO taxCodeStateDTO : taxCodeStateDTOList) {
			  if(strCountry.equalsIgnoreCase(taxCodeStateDTO.getCountry()) && ((taxCodeStateDTO.getActiveFlag() != null) && taxCodeStateDTO.getActiveFlag().equals("1")) ){
				  stateItems.add(new SelectItem(taxCodeStateDTO.getTaxCodeState(), 
						  taxCodeStateDTO.getName() + " (" + taxCodeStateDTO.getTaxCodeState() + ")"));
			  }
		  }
		}
	}
	
	public List<SelectItem> getStateItems() {
		if(stateItems==null){
			clearStateItems();
		}
	  
		return stateItems;
	}
	
	public JurisdictionService getJurisdictionService() {
		return jurisdictionService;
	}

	public void setJurisdictionService(JurisdictionService jurisdictionService) {
		this.jurisdictionService = jurisdictionService;
	}
	
	public OptionService getOptionService() {
		return optionService;
	}

	public void setOptionService(OptionService optionService) {
		this.optionService = optionService;
	}

	public List<SelectItem> getCountyItems() {
		if(countyItems == null) {
			clearCountyItems();
		}
		return countyItems;
	}

	public void setCountyItems(List<SelectItem> countyItems) {
		this.countyItems = countyItems;
	}

	public List<SelectItem> getCityItems() {
		if(cityItems == null) {
			clearCityItems();
		}
		
		return cityItems;
	}
	
	public List<SelectItem> getLevelItems() {
		if(levelItems == null) {
			levelItems = new ArrayList<SelectItem>();
			levelItems.add(new SelectItem("","Select Jurisdiction Level"));		
			levelItems.add(new SelectItem("*COUNTRY","*COUNTRY"));
			levelItems.add(new SelectItem("*STATE","*STATE"));
			levelItems.add(new SelectItem("*COUNTY","*COUNTY"));
			levelItems.add(new SelectItem("*CITY","*CITY"));
			levelItems.add(new SelectItem("*STJ","*STJ"));
		}
		
		return levelItems;
	}

	public void setCityItems(List<SelectItem> cityItems) {
		this.cityItems = cityItems;
	}
	
	public List<SelectItem> getStjItems() {
		if(stjItems == null) {
			clearStjItems();
		}
		
		return stjItems;
	}

	public void setStjItems(List<SelectItem> stjItems) {
		this.stjItems = stjItems;
	}
	
	public void reset() {
		setLevel("");
		setCountry("");
		setState("");
		setCounty("*ALL");
		setCity("*ALL");
		setStj("*ALL");
		clearStateItems();	
		clearCountyItems();	
		clearCityItems();
		clearStjItems();
	}

	public TaxCodeStateService getTaxCodeStateService() {
		return taxCodeStateService;
	}

	public void setTaxCodeStateService(TaxCodeStateService taxCodeStateService) {
		this.taxCodeStateService = taxCodeStateService;
	}
}
