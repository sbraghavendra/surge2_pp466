package com.ncsts.view.bean;

/* This is obsolete, not really used anymore... JMW */
/* up to 3 databases */
public enum DataBaseType {
	   DB1,
	   DB2,
	   DB3;
	   
	   public static DataBaseType get(String name) {
	  	 for (DataBaseType dbt : DataBaseType.values()) {
	  		 if (name.equalsIgnoreCase(dbt.name())) return dbt;
	  	 }
	  	 return null;
	   }
}
