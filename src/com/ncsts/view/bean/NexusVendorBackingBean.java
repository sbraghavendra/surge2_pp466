package com.ncsts.view.bean;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.faces.FacesException;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;

import org.richfaces.component.html.HtmlCalendar;
import org.richfaces.component.html.HtmlTree;
import org.richfaces.event.NodeExpandedEvent;
import org.richfaces.event.NodeSelectedEvent;
import org.richfaces.model.Ordering;
import org.richfaces.model.TreeNode;
import org.richfaces.model.TreeNodeImpl;
import org.richfaces.model.TreeRowKey;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIInput;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;

import org.ajax4jsf.component.html.HtmlAjaxSupport;
import org.ajax4jsf.model.DataVisitor;
import org.apache.log4j.Logger;

import javax.faces.component.html.HtmlSelectBooleanCheckbox;
import javax.faces.component.html.HtmlSelectOneMenu;

import org.richfaces.component.html.HtmlDataTable;
import org.richfaces.component.state.TreeState;
import org.richfaces.component.UIDataTable;
import org.richfaces.component.UIColumn;
import org.richfaces.component.UITree;

import com.ncsts.services.VendorNexusDetailService;
import com.ncsts.common.CacheManager;
import com.ncsts.common.FileReaderUtil;
import com.ncsts.common.LoggerFactory;
import com.ncsts.domain.BCPJurisdictionTaxRate;
import com.ncsts.domain.DataDefinitionColumn;
import com.ncsts.domain.DriverNames;
import com.ncsts.domain.EntityDefault;
import com.ncsts.domain.VendorItem;
import com.ncsts.domain.EntityLevel;
import com.ncsts.domain.EntityLocnSet;
import com.ncsts.domain.EntityLocnSetDtl;
import com.ncsts.domain.Jurisdiction;
import com.ncsts.domain.ListCodes;
import com.ncsts.domain.Menu;
import com.ncsts.domain.VendorNexus;
import com.ncsts.domain.VendorNexusDtl;
import com.ncsts.domain.Option;
import com.ncsts.domain.OptionCodePK;
import com.ncsts.domain.Registration;
import com.ncsts.domain.RegistrationDetail;
import com.ncsts.domain.Role;
import com.ncsts.domain.SitusMatrix;
import com.ncsts.domain.TaxMatrix;
import com.ncsts.domain.User;
import com.ncsts.domain.UserEntity;
import com.ncsts.dto.VendorItemDTO;
import com.ncsts.dto.EntityDefaultDTO;
import com.ncsts.dto.EntityLocnSetDTO;
import com.ncsts.dto.EntityLocnSetDtlDTO;
import com.ncsts.dto.NexusJurisdictionDTO;
import com.ncsts.dto.NexusJurisdictionItemDTO;
import com.ncsts.dto.SitusMatrixDTO;
import com.ncsts.dto.TaxCodeStateDTO;
import com.ncsts.management.UserSecurityBean;
import com.ncsts.services.DataDefinitionService;
import com.ncsts.services.VendorItemService;
import com.ncsts.services.EntityDefaultService;
import com.ncsts.services.EntityLocnSetService;
import com.ncsts.services.EntityLocnSetDtlService;
import com.ncsts.services.EntityLevelService;
import com.ncsts.services.JurisdictionService;
import com.ncsts.services.VendorNexusService;
import com.ncsts.services.OptionService;
import com.ncsts.services.RegistrationDetailService;
import com.ncsts.services.RegistrationService;
import com.ncsts.services.RoleService;
import com.ncsts.services.UserEntityService;
import com.ncsts.services.UserService;
import com.ncsts.services.ListCodesService;
import com.ncsts.view.util.ConfigSetting;
import com.ncsts.view.util.FacesUtils;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.ncsts.view.util.TogglePanelController;

public class NexusVendorBackingBean {

	private Logger logger = LoggerFactory.getInstance().getLogger(NexusVendorBackingBean.class);

//	@Autowired
//	private VendorNexusBackingBean nexusDefinitionBackingBean;

	@Autowired
	private RegistrationDetailService registrationDetailService;

	@Autowired
	private VendorNexusDetailService vendorNexusDetailService;

	@Autowired
	private VendorNexusService vendorNexusService;

	@Autowired
	private RegistrationService registrationService;

	@Autowired
	private OptionService optionService;

	protected MatrixCommonBean matrixCommonBean;

	private TaxJurisdictionBackingBean taxJurisdictionBean;

	private EditAction currentAction = EditAction.VIEW;

	public static String MAP_MAPPED = "mapped";
	public static String MAP_SELECTED = "selected";
	public static String MAP_NONE = "";
	public static String MAP_MAPPED_SELECTED = "mappedselected";

	private CacheManager cacheManager;
	private UserService userService;
	private RoleService roleService;
	private VendorItemService vendorItemService;
	private EntityDefaultService entityDefaultService;
	private EntityLocnSetService entityLocnSetService;
	private EntityLocnSetDtlService entityLocnSetDtlService;
	private EntityLevelService entityLevelService;
	private UserEntityService userEntityService;
	private DataDefinitionService dataDefinitionService;
	private ListCodesService listCodesService;
	private String rerender;
	private VendorItem selectedEntity;
	private EntityLevel selectedEntityLevel;
	private UserEntity selectedUserEntity;
	private String mode;
	private boolean transColDisplay = false;
	private boolean displayUpdateButton = false;
	private boolean displayAddButton = false;
	private boolean updateDisplay = false;
	private boolean disableDeleteButton = false;
	private boolean showUpdateButton = false;
	private boolean showOkButton = false;
	private VendorItemDTO entityItemDTO;
	private ConfigSetting userConfig;
	private int selectedDefinitionRowIndex = -1;
	private int selectedRegistrationRowIndex = -1;
	private int selectedEntityDefaultRowIndex = -1;
	private int selectedLocationListRowIndex = -1;
	private int selectedLocationListDetailRowIndex = -1;
	private Long currentAddChangeEntityId = 0L;
	private loginBean loginBean;
	private UserSecurityBean userSecurityBean;
	private TreeNode rootNode = null;
	private JurisdictionItem selectedVendorItem = null;
	private VendorItem updateVendorItem = null;
	private VendorItem selectedGridVendorItem = null;
	private EntityDefaultDTO entityDefaultDTO;
	private EntityLocnSetDTO entityLocnSetDTO;
	private EntityLocnSetDtlDTO entityLocnSetDtlDTO;
	private EntityDefault selectedEntityDefault = null;
	private EntityLocnSet selectedEntityLocnSet = null;
	private EntityLocnSetDtl selectedEntityLocnSetDtl = null;
	private Map<Long, VendorItem> globalEntityArray = new LinkedHashMap<Long, VendorItem>();
	private Map<Long, String> entityLevelMap = null;
	private Map<Long, String> entityItemMap = null;
	private SearchJurisdictionHandler filterHandler = new SearchJurisdictionHandler();
	private JurisdictionService jurisdictionService;

	private List<VendorNexusDtl> vendorNexusDtlList;
	private List<RegistrationDetail> registrationDetailList;

	private VendorNexusDtl selectedVendorNexusDtl;
	private VendorNexusDtl updateVendorNexusDtl;
	private RegistrationDetail selectedRegistrationDetail;
	private RegistrationDetail updateRegistrationDetail;
	private String selectedRowKey = null;

	private int entityItemId = 0;

	private NexusJurisdictionDTO selectedJurisdiction;

	private boolean countryChecked = true;
	private boolean stateChecked = true;

	private VendorNexus updateVendorNexus;
	private Registration updateRegistration;

	private List<SelectItem> nexusTypeItems = null;
	private List<SelectItem> regTypeItems = null;

	private String stj = "";

	private SearchJurisdictionHandler filterHandlerShipto = new SearchJurisdictionHandler(true);
	private TogglePanelController togglePanelController = null;
	
	private boolean nexusUsed = false;

	public TogglePanelController getTogglePanelController() {
		if (togglePanelController == null) {
			togglePanelController = new TogglePanelController();
			togglePanelController.addTogglePanel("JurisdictionInformationPanel", true);
		}
		return togglePanelController;
	}

	public void setTogglePanelController(
			TogglePanelController togglePanelController) {
		this.togglePanelController = togglePanelController;
	}

	public SearchJurisdictionHandler getFilterHandlerShipto() {
		return filterHandlerShipto;
	}

	public String getStj() {
		return stj;
	}

	public void setStj(String stj) {
		this.stj = stj;
	}
	
	public TaxJurisdictionBackingBean getTaxJurisdictionBean() {
		return taxJurisdictionBean;
	}
	
	public MatrixCommonBean getMatrixCommonBean() {
		return matrixCommonBean;
	}

	public void setMatrixCommonBean(MatrixCommonBean matrixCommonBean) {
		this.matrixCommonBean = matrixCommonBean;
		filterHandlerShipto.setJurisdictionService(matrixCommonBean.getJurisdictionService());
		filterHandlerShipto.setCacheManager(matrixCommonBean.getCacheManager());
		filterHandlerShipto.setTaxJurisdictionBean(taxJurisdictionBean);
		filterHandlerShipto.setCallbackScreen("nexusvendor_main");
		
		filterHandler.setJurisdictionService(matrixCommonBean.getJurisdictionService());
		filterHandler.setCacheManager(matrixCommonBean.getCacheManager());
		filterHandler.setTaxJurisdictionBean(taxJurisdictionBean);
		//filterHandler.setCallbackScreen("vendor_update");
	}
	
	public void setTaxJurisdictionBean(TaxJurisdictionBackingBean taxJurisdictionBean) {
		this.taxJurisdictionBean = taxJurisdictionBean;
	}

	public List<SelectItem> getNexusTypeItems() {
		if (nexusTypeItems == null) {
			nexusTypeItems = cacheManager.createNexusTypeItems();
		}

		return nexusTypeItems;
	}

	public List<SelectItem> getRegTypeItems() {
		if (regTypeItems == null) {
			regTypeItems = cacheManager.createRegistrationTypeItems();
		}

		return regTypeItems;
	}

	private ArrayList<String> countyArray = new ArrayList<String>();
	private ArrayList<String> cityArray = new ArrayList<String>();
	private ArrayList<String> stjArray = new ArrayList<String>();
	private boolean stateSelected = false;

	private List<JurisdictionItem> selectedJurisdictionList = new ArrayList<JurisdictionItem>();

	public void setSelectedJurisdictionList(
			List<JurisdictionItem> selectedJurisdictionList) {
		this.selectedJurisdictionList = selectedJurisdictionList;
	}

	public List<JurisdictionItem> getSelectedJurisdictionList() {
		return selectedJurisdictionList;
	}
	
	private List<JurisdictionItem> selectedTempJurisdictionList = new ArrayList<JurisdictionItem>();

	public void setSelectedTempJurisdictionList(
			List<JurisdictionItem> selectedTempJurisdictionList) {
		this.selectedTempJurisdictionList = selectedTempJurisdictionList;
	}

	public List<JurisdictionItem> getSelectedTempJurisdictionList() {
		return selectedTempJurisdictionList;
	}
	
	public boolean prepareRemoveSelectedJurisdiction() {
		if (selectedJurisdictionItemMap.size() == 0) {
			return false; // no checked, no selected
		}

		selectedTempJurisdictionList = new ArrayList<JurisdictionItem>();
		JurisdictionItem aVendorItem = null;
		JurisdictionItem missingVendorItem = null;

		Map<String, JurisdictionItem> missingJurisdictionMap = new LinkedHashMap<String, JurisdictionItem>();
		
		//Get unique state list
		Map<String, JurisdictionItem> stateJurisdictionMap = new LinkedHashMap<String, JurisdictionItem>();
		
		//Get unique state/county list
		Map<String, JurisdictionItem> statecountyJurisdictionMap = new LinkedHashMap<String, JurisdictionItem>();
		
		for (String entityKey : selectedJurisdictionItemMap.keySet()) {
			aVendorItem = (JurisdictionItem) selectedJurisdictionItemMap.get(entityKey);
			
			JurisdictionItem juriVendorItem = new JurisdictionItem();
			juriVendorItem.setCountry(aVendorItem.getCountry());
			juriVendorItem.setState(aVendorItem.getState());
			juriVendorItem.setCountyLocated(aVendorItem.getCountyLocated());
			juriVendorItem.setDescription(aVendorItem.getDescription());
			juriVendorItem.setItemType(aVendorItem.getItemType());
			
			if (aVendorItem.isItemType(JurisdictionItem.STATE_TYPE)) {
			} 
			else if (aVendorItem.isItemType(JurisdictionItem.COUNTY_TYPE)) {
				juriVendorItem.setCounty(aVendorItem.getDescription());
			}
			else {
				if (aVendorItem.isItemType(JurisdictionItem.CITY_TYPE)) {
			
					juriVendorItem.setCity(aVendorItem.getDescription());
					
				} else if (aVendorItem.isItemType(JurisdictionItem.STJ_TYPE)) {
					juriVendorItem.setStj(aVendorItem.getDescription());
				}
			}

			
			if (aVendorItem.isItemType(JurisdictionItem.STATE_TYPE) && stateJurisdictionMap.get(aVendorItem.getKey()) == null) {// Add state
				stateJurisdictionMap.put(aVendorItem.getKey(), aVendorItem);
			}
			else if (aVendorItem.isItemType(JurisdictionItem.COUNTY_TYPE) && statecountyJurisdictionMap.get(aVendorItem.getKey()) == null) {// Add state/country
				statecountyJurisdictionMap.put(aVendorItem.getKey(), aVendorItem);
			}
			
			selectedTempJurisdictionList.add(juriVendorItem);
		}
		
		
		//Retrieve Jurisdictions under a state to missing list
		for (String entityKey : stateJurisdictionMap.keySet()) {
			aVendorItem = (JurisdictionItem) stateJurisdictionMap.get(entityKey);
			
			Map<String, JurisdictionItem> stateList = retrieveCountyCitySTJ(aVendorItem.getCountry(), aVendorItem.getState(), null);
			
			//Individual state
			JurisdictionItem aStateItem = null;
			for (String stateKey : stateList.keySet()) {
				aStateItem = (JurisdictionItem) stateList.get(stateKey);
				
				//Add missing state/county/city
				missingVendorItem = new JurisdictionItem();
				missingVendorItem.setItemType(aStateItem.getItemType());
				missingVendorItem.setCountry(aStateItem.getCountry());
				missingVendorItem.setState(aStateItem.getState());
				missingVendorItem.setNewItemFlag(true);
				
				if (aStateItem.isItemType(JurisdictionItem.STATE_TYPE)) { 
					missingVendorItem.setDescription(aStateItem.getState());
				} 
				else if (aStateItem.isItemType(JurisdictionItem.COUNTY_TYPE)) {
					missingVendorItem.setCounty(aStateItem.getDescription());
					missingVendorItem.setDescription(aStateItem.getDescription());
				}
				else if (aStateItem.isItemType(JurisdictionItem.CITY_TYPE)) {
					missingVendorItem.setCity(aStateItem.getDescription());
					missingVendorItem.setDescription(aStateItem.getDescription());
					missingVendorItem.setCountyLocated(aStateItem.getCountyLocated());
				}
				else if (aStateItem.isItemType(JurisdictionItem.STJ_TYPE)) {
					missingVendorItem.setStj(aStateItem.getDescription());
					missingVendorItem.setDescription(aStateItem.getDescription());
					missingVendorItem.setCountyLocated(aStateItem.getCountyLocated());
				}

				if (missingJurisdictionMap.get(missingVendorItem.getKey()) == null) {
					missingJurisdictionMap.put(missingVendorItem.getKey(), missingVendorItem);
				}	
			}
		}
		
		//Retrieve Jurisdictions under a state/county to missing list
		for (String entityKey : statecountyJurisdictionMap.keySet()) {
			aVendorItem = (JurisdictionItem) statecountyJurisdictionMap.get(entityKey);
			
			Map<String, JurisdictionItem> statecountyList = retrieveCountyCitySTJ(aVendorItem.getCountry(), aVendorItem.getState(), aVendorItem.getDescription());
			
			//Individual state/county
			JurisdictionItem aStateCountyItem = null;
			for (String statecountyKey : statecountyList.keySet()) {
				aStateCountyItem = (JurisdictionItem) statecountyList.get(statecountyKey);
				
				//Add missing state/county/city
				missingVendorItem = new JurisdictionItem();
				missingVendorItem.setItemType(aStateCountyItem.getItemType());
				missingVendorItem.setCountry(aStateCountyItem.getCountry());
				missingVendorItem.setState(aStateCountyItem.getState());
				missingVendorItem.setNewItemFlag(true);
				
				if (aStateCountyItem.isItemType(JurisdictionItem.STATE_TYPE)) { //Will not get here
					missingVendorItem.setDescription(aStateCountyItem.getState());
				} 
				else if (aStateCountyItem.isItemType(JurisdictionItem.COUNTY_TYPE)) {
					missingVendorItem.setCounty(aStateCountyItem.getDescription());
					missingVendorItem.setDescription(aStateCountyItem.getDescription());
				}
				else if (aStateCountyItem.isItemType(JurisdictionItem.CITY_TYPE)) {
					missingVendorItem.setCity(aStateCountyItem.getDescription());
					missingVendorItem.setDescription(aStateCountyItem.getDescription());
					missingVendorItem.setCountyLocated(aStateCountyItem.getCountyLocated());
				}
				else if (aStateCountyItem.isItemType(JurisdictionItem.STJ_TYPE)) {
					missingVendorItem.setStj(aStateCountyItem.getDescription());
					missingVendorItem.setDescription(aStateCountyItem.getDescription());
					missingVendorItem.setCountyLocated(aStateCountyItem.getCountyLocated());
				}

				if (missingJurisdictionMap.get(missingVendorItem.getKey()) == null) {
					missingJurisdictionMap.put(missingVendorItem.getKey(), missingVendorItem);
				}	
			
			}
		}
		
		/*
		for (String entityKey : selectedJurisdictionItemMap.keySet()) {
			aEntityItem = (JurisdictionItem) selectedJurisdictionItemMap
					.get(entityKey);

			JurisdictionItem juriEntityItem = new JurisdictionItem();
			juriEntityItem.setCountry(aEntityItem.getCountry());
			juriEntityItem.setState(aEntityItem.getState());
			juriEntityItem.setCountyLocated(aEntityItem.getCountyLocated());
			juriEntityItem.setDescription(aEntityItem.getDescription());
			juriEntityItem.setItemType(aEntityItem.getItemType());
			
			if (aEntityItem.isItemType(JurisdictionItem.COUNTY_TYPE)) {
				juriEntityItem.setCounty(aEntityItem.getDescription());
			}
			else if (aEntityItem.isItemType(JurisdictionItem.CITY_TYPE)) {		
				juriEntityItem.setCity(aEntityItem.getDescription());
			} else if (aEntityItem.isItemType(JurisdictionItem.STJ_TYPE)) {
				juriEntityItem.setStj(aEntityItem.getDescription());
			}
			

			if (aEntityItem.isItemType(JurisdictionItem.STATE_TYPE)) {
				//Get all lower Jurisdictions under a state
				
			} 
			else if (aEntityItem.isItemType(JurisdictionItem.COUNTY_TYPE)) {
				//Add missing state
				missingEntityItem = new JurisdictionItem();
				missingEntityItem.setCountry(juriEntityItem.getCountry());
				missingEntityItem.setState(juriEntityItem.getState());
				missingEntityItem.setDescription(juriEntityItem.getState());
				missingEntityItem.setItemType(JurisdictionItem.STATE_TYPE);
				missingEntityItem.setNewItemFlag(true);

				if (missingJurisdictionMap.get(missingEntityItem.getKey()) == null) {
					missingJurisdictionMap.put(missingEntityItem.getKey(),
							missingEntityItem);
				}
			} 
			else {
				//Add missing state
				missingEntityItem = new JurisdictionItem();
				missingEntityItem.setCountry(juriEntityItem.getCountry());
				missingEntityItem.setState(juriEntityItem.getState());
				missingEntityItem.setDescription(juriEntityItem.getState());
				missingEntityItem.setItemType(JurisdictionItem.STATE_TYPE);
				missingEntityItem.setNewItemFlag(true);

				if (missingJurisdictionMap.get(missingEntityItem.getKey()) == null) {
					missingJurisdictionMap.put(missingEntityItem.getKey(),
							missingEntityItem);
				}

				//Add missing  county
				missingEntityItem = new JurisdictionItem();
				missingEntityItem.setCountry(juriEntityItem.getCountry());
				missingEntityItem.setState(juriEntityItem.getState());
				missingEntityItem.setCounty(juriEntityItem.getCountyLocated());
				missingEntityItem.setDescription(juriEntityItem.getCountyLocated());
				missingEntityItem.setItemType(JurisdictionItem.COUNTY_TYPE);
				missingEntityItem.setNewItemFlag(true);

				if (missingJurisdictionMap.get(missingEntityItem.getKey()) == null) {
					missingJurisdictionMap.put(missingEntityItem.getKey(),
							missingEntityItem);
				}
			}

			selectedTempJurisdictionList.add(juriEntityItem);
		}
		*/
		
		// Remove missing jurisdiction item if necessary
		JurisdictionItem testItem = null;
		for (int i = 0; i < selectedTempJurisdictionList.size(); i++) {
			testItem = (JurisdictionItem) selectedTempJurisdictionList.get(i);

			if (missingJurisdictionMap.get(testItem.getKey()) != null) {// Found one in missing list
				missingJurisdictionMap.remove(testItem.getKey());
			}
		}

		for (String entityKey : missingJurisdictionMap.keySet()) {
			aVendorItem = (JurisdictionItem) missingJurisdictionMap.get(entityKey);
			selectedTempJurisdictionList.add(aVendorItem);
		}
		
		return true;
	}

	public boolean prepareAddSelectedJurisdiction() {
		if (selectedJurisdictionItemMap.size() == 0) {
			return false; // no checked, no selected
		}

		selectedTempJurisdictionList = new ArrayList<JurisdictionItem>();

		countyArray = new ArrayList<String>();
		cityArray = new ArrayList<String>();
		stjArray = new ArrayList<String>();
		stateSelected = false;

		Map<String, JurisdictionItem> missingJurisdictionMap = new LinkedHashMap<String, JurisdictionItem>();

		JurisdictionItem aVendorItem = null;
		JurisdictionItem missingVendorItem = null;
		for (String entityKey : selectedJurisdictionItemMap.keySet()) {
			aVendorItem = (JurisdictionItem) selectedJurisdictionItemMap
					.get(entityKey);

			JurisdictionItem juriVendorItem = new JurisdictionItem();
			juriVendorItem.setCountry(aVendorItem.getCountry());
			juriVendorItem.setState(aVendorItem.getState());
			juriVendorItem.setCountyLocated(aVendorItem.getCountyLocated());
			juriVendorItem.setDescription(aVendorItem.getDescription());
			juriVendorItem.setItemType(aVendorItem.getItemType());

			if (aVendorItem.isItemType(JurisdictionItem.STATE_TYPE)) {
			} 
			else if (aVendorItem.isItemType(JurisdictionItem.COUNTY_TYPE)) {
				juriVendorItem.setCounty(aVendorItem.getDescription());

				//Add missing state
				missingVendorItem = new JurisdictionItem();
				missingVendorItem.setCountry(juriVendorItem.getCountry());
				missingVendorItem.setState(juriVendorItem.getState());
				missingVendorItem.setDescription(juriVendorItem.getState());
				missingVendorItem.setItemType(JurisdictionItem.STATE_TYPE);
				missingVendorItem.setNewItemFlag(true);

				if (missingJurisdictionMap.get(missingVendorItem.getKey()) == null) {
					missingJurisdictionMap.put(missingVendorItem.getKey(),
							missingVendorItem);
				}
			} 
			else {
				if (aVendorItem.isItemType(JurisdictionItem.CITY_TYPE)) {
			
					juriVendorItem.setCity(aVendorItem.getDescription());
					
				} else if (aVendorItem.isItemType(JurisdictionItem.STJ_TYPE)) {
					juriVendorItem.setStj(aVendorItem.getDescription());
				}

				//Add missing state
				missingVendorItem = new JurisdictionItem();
				missingVendorItem.setCountry(juriVendorItem.getCountry());
				missingVendorItem.setState(juriVendorItem.getState());
				missingVendorItem.setDescription(juriVendorItem.getState());
				missingVendorItem.setItemType(JurisdictionItem.STATE_TYPE);
				missingVendorItem.setNewItemFlag(true);

				if (missingJurisdictionMap.get(missingVendorItem.getKey()) == null) {
					missingJurisdictionMap.put(missingVendorItem.getKey(),
							missingVendorItem);
				}

				//Add missing  county
				missingVendorItem = new JurisdictionItem();
				missingVendorItem.setCountry(juriVendorItem.getCountry());
				missingVendorItem.setState(juriVendorItem.getState());
				missingVendorItem.setCounty(juriVendorItem.getCountyLocated());
				missingVendorItem.setDescription(juriVendorItem.getCountyLocated());
				missingVendorItem.setItemType(JurisdictionItem.COUNTY_TYPE);
				missingVendorItem.setNewItemFlag(true);

				if (missingJurisdictionMap.get(missingVendorItem.getKey()) == null) {
					missingJurisdictionMap.put(missingVendorItem.getKey(),
							missingVendorItem);
				}
			}

			selectedTempJurisdictionList.add(juriVendorItem);
		}

		// Remove missing jurisdiction item if necessary
		JurisdictionItem testItem = null;
		for (int i = 0; i < selectedTempJurisdictionList.size(); i++) {
			testItem = (JurisdictionItem) selectedTempJurisdictionList.get(i);

			if (missingJurisdictionMap.get(testItem.getKey()) != null) {// Found
																		// one
																		// in
																		// missing
																		// list
				missingJurisdictionMap.remove(testItem.getKey());
			}
		}

		for (String entityKey : missingJurisdictionMap.keySet()) {
			aVendorItem = (JurisdictionItem) missingJurisdictionMap
					.get(entityKey);
			selectedTempJurisdictionList.add(aVendorItem);
		}

		return true;
	}

	public boolean prepareSelectedJurisdictionBK() {

		String countryCode = null;
		String stateCode = null;
		String county = null;
		String city = null;
		String stj = null;
		countyArray = new ArrayList<String>();
		cityArray = new ArrayList<String>();
		stjArray = new ArrayList<String>();
		stateSelected = false;

		boolean countyAll = false;
		boolean cityAll = false;
		boolean stjAll = false;

		JurisdictionItem aVendorItem = null;
		List<JurisdictionItem> selectedList = new ArrayList<JurisdictionItem>();
		for (Long entityKey : globalJurisdictionArray.keySet()) {
			aVendorItem = (JurisdictionItem) globalJurisdictionArray
					.get(entityKey);

			if (aVendorItem.getSelectedFlag()) {
				selectedList.add(aVendorItem);// checked from tree
			}
		}

		if (selectedList.size() == 0) {
			if (selectedVendorItem == null
					|| selectedVendorItem
							.isItemType(JurisdictionItem.COUNTRY_TYPE)
					|| selectedVendorItem.getIsAllFlag()
					|| selectedVendorItem.getDisabledFlag()) {
				return false;
			}
			selectedList.add(selectedVendorItem);// selected from tree
		}

		if (selectedList.size() == 0) {
			return false; // no checked, no selected
		}

		for (JurisdictionItem entityItem : selectedList) {

			countryCode = entityItem.getCountry();
			stateCode = entityItem.getState();

			if (entityItem.isItemType(JurisdictionItem.STATE_TYPE)) {
				stateSelected = true;
			} else if (entityItem.isItemType(JurisdictionItem.COUNTY_TYPE)) {
				if (entityItem.getIsAllFlag()) {
					countyAll = true;
				} else {
					countyArray.add(entityItem.getDescription());
				}
			} else if (entityItem.isItemType(JurisdictionItem.CITY_TYPE)) {
				if (entityItem.getIsAllFlag()) {
					cityAll = true;
				} else {
					cityArray.add(entityItem.getDescription());
				}
			} else if (entityItem.isItemType(JurisdictionItem.STJ_TYPE)) {
				if (entityItem.getIsAllFlag()) {
					stjAll = true;
				} else {
					stjArray.add(entityItem.getDescription());
				}
			}
		}

		this.selectedJurisdiction = new NexusJurisdictionDTO();

		// If only a State is checked
		if (!countyAll && !cityAll && !stjAll && countyArray.size() == 0
				&& cityArray.size() == 0 && stjArray.size() == 0
				&& stateSelected) {
			county = "*STATE";
			city = "*STATE";
			stj = "*STATE";
		}
		// If only a County is checked
		else if (!countyAll && !cityAll && !stjAll && countyArray.size() == 1
				&& cityArray.size() == 0 && stjArray.size() == 0
				&& !stateSelected) {
			county = (String) countyArray.get(0);
			city = "*COUNTY";
			stj = "*COUNTY";
		}
		// If only a City is checked
		else if (!countyAll && !cityAll && !stjAll && countyArray.size() == 0
				&& cityArray.size() == 1 && stjArray.size() == 0
				&& !stateSelected) {
			county = "*CITY";
			city = (String) cityArray.get(0);
			stj = "*CITY";
		}
		// If only an STJ is checked
		else if (!countyAll && !cityAll && !stjAll && countyArray.size() == 0
				&& cityArray.size() == 0 && stjArray.size() == 1
				&& !stateSelected) {
			county = "*STJ";
			city = "*STJ";
			stj = (String) stjArray.get(0);
		}
		// If only �All Counties� is checked
		else if (countyAll && !cityAll && !stjAll && cityArray.size() == 0
				&& stjArray.size() == 0 && !stateSelected) {
			county = "*All Independent";
			city = "*COUNTY";
			stj = "*COUNTY";
		}
		// If only �All Cities� is checked
		else if (!countyAll && cityAll && !stjAll && countyArray.size() == 0
				&& stjArray.size() == 0 && !stateSelected) {
			county = "*CITY";
			city = "*All Independent";
			stj = "*CITY";
		}
		// If only �All STJs� is checked
		else if (!countyAll && !cityAll && stjAll && countyArray.size() == 0
				&& cityArray.size() == 0 && !stateSelected) {
			county = "*STJ";
			city = "*STJ";
			stj = "*All Independent";
		}
		// If any combination of Jurisdiction is checked
		else {
			county = "*MULTIPLE";
			city = "*MULTIPLE";
			stj = "*MULTIPLE";
		}

		this.selectedJurisdiction = new NexusJurisdictionDTO();
		this.selectedJurisdiction.setCountryCode(countryCode);
		this.selectedJurisdiction.setCountryName(cacheManager.getCountryMap()
				.get(countryCode));
		this.selectedJurisdiction.setStateCode(stateCode);
		this.selectedJurisdiction.setStateName("");

		this.selectedJurisdiction.setCounty(county);
		this.selectedJurisdiction.setCity(city);
		this.selectedJurisdiction.setStj(stj);

		if (countryCode == null || countryCode.length() == 0
				|| stateCode == null || stateCode.length() == 0
				|| county == null || county.length() == 0 || city == null
				|| city.length() == 0 || stj == null || stj.length() == 0) {
			return false;
		} else {
			return true;
		}
	}

	public boolean getReadOnlyAction() {
		return (nexusAction.equals(NexusDefRegAction.updateDefinition)
				|| nexusAction.equals(NexusDefRegAction.deleteDefinition)
				|| nexusAction.equals(NexusDefRegAction.updateRegistration) || nexusAction
					.equals(NexusDefRegAction.deleteRegistration));
	}

	public boolean getUpdateOnlyAction() {
		return (nexusAction.equals(NexusDefRegAction.updateDefinition)
				|| nexusAction.equals(NexusDefRegAction.addNexus)
				|| nexusAction.equals(NexusDefRegAction.addDefinition)
				|| nexusAction.equals(NexusDefRegAction.updateRegistration) || nexusAction
					.equals(NexusDefRegAction.addRegistration));
	}

	private SimpleDateFormat maxDate = new SimpleDateFormat("MM/dd/yyyy");

	public void validateEffectiveDate(FacesContext context,
			UIComponent toValidate, Object value) {
		// Ensure date > effectiveDate
		Date expDate = (Date) value;

		Date maxExpDate = null;
		try {
			maxExpDate = maxDate.parse("12/31/9999");
		} catch (ParseException e) {
		}

		if (expDate.after(maxExpDate)) {
			((UIInput) toValidate).setValid(false);
			FacesMessage message = new FacesMessage(
					"Effective Date: could not be understood as a date.");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage(toValidate.getClientId(context), message);
		}
	}

	public void validateExpirationDate(FacesContext context,
			UIComponent toValidate, Object value) {
		// Ensure date > effectiveDate
		Date expDate = (Date) value;

		Date maxExpDate = null;
		try {
			maxExpDate = maxDate.parse("12/31/9999");
		} catch (ParseException e) {
		}

		if (expDate.after(maxExpDate)) {
			((UIInput) toValidate).setValid(false);
			FacesMessage message = new FacesMessage(
					"Expiration Date: could not be understood as a date.");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage(toValidate.getClientId(context), message);
		}
	}

	private NexusDefRegAction nexusAction = NexusDefRegAction.cancelAction;

	private enum NexusDefRegAction {
		addNexus, removeNexus, addDefinition, updateDefinition, deleteDefinition, addRegistration, updateRegistration, deleteRegistration, cancelAction
	}

	public String getTaskHeaderLabel() {
		if (nexusAction.equals(NexusDefRegAction.addNexus)) {
			return "Add Vendor Nexus";
		} else if (nexusAction.equals(NexusDefRegAction.removeNexus)) {
			return "Remove Vendor Nexus";
		} else if (nexusAction.equals(NexusDefRegAction.addDefinition)) {
			return "Add a Nexus Definition";
		} else if (nexusAction.equals(NexusDefRegAction.updateDefinition)) {
			return "Update a Nexus Definition";
		} else if (nexusAction.equals(NexusDefRegAction.deleteDefinition)) {
			return "Delete a Nexus Definition";
		} else if (nexusAction.equals(NexusDefRegAction.addRegistration)) {
			return "Add a Registration";
		} else if (nexusAction.equals(NexusDefRegAction.updateRegistration)) {
			return "Update a Registration";
		} else if (nexusAction.equals(NexusDefRegAction.deleteRegistration)) {
			return "Delete a Registration";
		} else {
			return "None";
		}
	}

	public String okProcessAction() {
		String result = null;

		if (nexusAction.equals(NexusDefRegAction.addNexus)) {
			result = okAddNexusAction();
		} else if (nexusAction.equals(NexusDefRegAction.removeNexus)) {
			result = okRemoveNexusAction();
		} else if (nexusAction.equals(NexusDefRegAction.updateDefinition)) {
			result = okUpdateDefinitionAction();
		} else if (nexusAction.equals(NexusDefRegAction.deleteDefinition)) {
			result = okDeleteDefinitionAction();
		} else if (nexusAction.equals(NexusDefRegAction.addRegistration)) {
			result = okAddRegistrationAction();
		} else if (nexusAction.equals(NexusDefRegAction.updateRegistration)) {
			result = okUpdateRegistrationAction();
		} else if (nexusAction.equals(NexusDefRegAction.deleteRegistration)) {
			result = okDeleteRegistrationAction();
		} else {
			result = null;
		}

		return result;
	}

	public String cancelAction() {
		return "nexusvendor_main";
	}

	public String displayAction() {
		return "nexusvendor_update";
	}

	public boolean getRegistrationTask() {
		return (nexusAction.equals(NexusDefRegAction.updateRegistration) || nexusAction
				.equals(NexusDefRegAction.deleteRegistration));
	}

	public boolean getIsAddNexusTask() {
		return (nexusAction.equals(NexusDefRegAction.addNexus));
	}

	public boolean getIsRemoveNexusTask() {
		return (nexusAction.equals(NexusDefRegAction.removeNexus));
	}

	public boolean getDisableDefAdd() {
		return false;
	}

	public boolean getDisableRedAdd() {
		if (selectedGridVendorItem != null) {
			return false;
		} else {
			return true;
		}
	}
	
	private boolean isDisplayAuto = false;
	
	public boolean getIsDisplayAuto() {
		return isDisplayAuto;
	}
	
	public String getDisplayAutoMessage() {
		if(isDisplayAuto && getIsAddNexusTask()){
			return "Nexus added because lower level jurisdiction(s) added.";
		}
		else if(isDisplayAuto && getIsRemoveNexusTask()){
			return "Nexus added because higher level jurisdiction(s) added.";
		}
		else{
			return "";
		}
	}

	public boolean getDisableUpdate() {
		if (selectedDefinitionRowIndex >= 0
				&& selectedVendorNexusDtl != null) {
			return false;
		} else if (selectedRegistrationRowIndex >= 0
				&& selectedRegistrationDetail != null) {
			return false;
		} else {
			return true;
		}
	}
	
	public boolean getAddRemoveNexus() {
		if(selectedJurisdictionList!=null && selectedJurisdictionList.size()>0){
			return true;
		}
		else{
			return false;
		}
	}

	public boolean getDisableDelete() {
		if (selectedDefinitionRowIndex >= 0
				&& selectedVendorNexusDtl != null) {
			return false;
		} else if (selectedRegistrationRowIndex >= 0
				&& selectedRegistrationDetail != null) {
			return false;
		} else {
			return true;
		}
	}
	
	public String displayRemoveNexusAction() {
		
		updateSelectedNode();
		nexusAction = NexusDefRegAction.removeNexus;
		
		if (!prepareRemoveSelectedJurisdiction()) {
			FacesMessage message = new FacesMessage(
					"Check Jurisdiction(s) to Add/Remove Nexus.");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
			return null;
		}
		
		// Definition
		updateVendorNexus = new VendorNexus();
		// check NEXUS_DEF_ID
		updateVendorNexus.setVendorId(this.updateVendorItem.getVendorId());

		// Definition detail
		this.updateVendorNexusDtl = new VendorNexusDtl();

		verifyRemoveTempJurisdictionList();

		return "nexusvendor_update";
	}

	public String displayAddNexusAction() {

		// Export selected nodes to array
		updateSelectedNode();

		nexusAction = NexusDefRegAction.addNexus;

		if (!prepareAddSelectedJurisdiction()) {
			FacesMessage message = new FacesMessage(
					"Check Jurisdiction(s) to Add/Remove Nexus.");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
			return null;
		}

		// Definition
		updateVendorNexus = new VendorNexus();
		// check NEXUS_DEF_ID
		updateVendorNexus.setVendorId(this.updateVendorItem.getVendorId()); //Use TB_VENDOR.VENDOR_ID

		// Definition detail
		this.updateVendorNexusDtl = new VendorNexusDtl();
		this.updateVendorNexusDtl.setEffectiveDate(new com.ncsts.view.util.DateConverter().getUserLocalDate());
		this.updateVendorNexusDtl.setNexusTypeCode(getDefNexusType());
		try {
			this.updateVendorNexusDtl.setExpirationDate(maxDate.parse("12/31/9999"));
		} catch (ParseException e) {
		}
		this.updateVendorNexusDtl.setActiveFlag("1");
		
		filterHandler.setGeocode(updateVendorItem.getGeocode());
		filterHandler.setCountry(updateVendorItem.getCountry());
		filterHandler.setState(updateVendorItem.getState());
		filterHandler.setCounty(updateVendorItem.getCounty());
		filterHandler.setCity(updateVendorItem.getCity());
		filterHandler.setZip(updateVendorItem.getZip());
		filterHandler.setZipPlus4(updateVendorItem.getZipplus4());
		
		verifyAddTempJurisdictionList();
		
		return "nexusvendor_update";
	}
	
	public void effectiveDateChanged(ActionEvent e) throws AbortProcessingException { 
		HtmlCalendar newCalendar = (HtmlCalendar) (((HtmlAjaxSupport) e.getComponent()).getParent());
		Date newDate =((Date)newCalendar.getValue());
		updateVendorNexusDtl.setEffectiveDate(newDate);
		
		verifyAddTempJurisdictionList();
	}
	
	public void expirationDateChanged(ActionEvent e) throws AbortProcessingException { 
		HtmlCalendar newCalendar = (HtmlCalendar) (((HtmlAjaxSupport) e.getComponent()).getParent());
		Date newDate =((Date)newCalendar.getValue());
		updateVendorNexusDtl.setExpirationDate(newDate);
	}
	
	public void nexusTypeChanged(ActionEvent e) {
		String type = (String) ((HtmlSelectOneMenu) ((HtmlAjaxSupport) e.getComponent()).getParent()).getSubmittedValue();
		
		if(type==null || type.length()==0){
			type = "";
		}
		
		this.updateVendorNexusDtl.setNexusTypeCode(type);
		
		if(type.length()==0){
			selectedJurisdictionList = new ArrayList<JurisdictionItem>();
			return;
		}
		
		verifyAddTempJurisdictionList();
	}
	
	private boolean isIndependentCounty(JurisdictionItem jurisdictionItem){
		boolean retBoolean = false;
		
		String country = jurisdictionItem.getCountry();
		String state = jurisdictionItem.getState();
		String county = jurisdictionItem.getCounty();
		
		return vendorNexusService.isIndependentCounty(entityIdSearch, geocodeSearch, country, state, county, citySearch, zipSearch, stjSearch);
	}
	
	private void verifyAddTempJurisdictionList(){
		// Remove jurisdiction item which already have nexus definition
		selectedJurisdictionList = new ArrayList<JurisdictionItem>();
		
		Map<String, JurisdictionItem> unsortJurisdictionItemMap = new java.util.LinkedHashMap<String, JurisdictionItem>();

		JurisdictionItem addItem = null;
		boolean nExist = true;
		boolean auto = false;
		String nexusTypeCode = null;
		for (int i = 0; i < selectedTempJurisdictionList.size(); i++) {
			addItem = (JurisdictionItem) selectedTempJurisdictionList.get(i);
			
			if (addItem.isItemType(JurisdictionItem.COUNTY_TYPE) && !isIndependentCounty(addItem)) {
				//No need to add or remove
				continue;
			}
			
			if(addItem.getNewItemFlag()){
				//Verify auto
				nexusTypeCode = updateVendorNexusDtl.getNexusTypeCode();
			}
			else{
				nexusTypeCode = null;
			}
			
			if (addItem.isItemType(JurisdictionItem.STATE_TYPE)) {
				nExist = vendorNexusDetailService.isVendorNexusExist(updateVendorNexus.getVendorId(), addItem.getCountry(), addItem.getState(), "*STATE", "*STATE", "*STATE", 
						updateVendorNexusDtl.getEffectiveDate(), nexusTypeCode);
			} else if (addItem.isItemType(JurisdictionItem.COUNTY_TYPE)) {
				nExist = vendorNexusDetailService.isVendorNexusExist(updateVendorNexus.getVendorId(), addItem.getCountry(), addItem.getState(), addItem.getCounty(), "*COUNTY", "*COUNTY", 
						updateVendorNexusDtl.getEffectiveDate(), nexusTypeCode);
			} else if (addItem.isItemType(JurisdictionItem.CITY_TYPE)) {
				nExist = vendorNexusDetailService.isVendorNexusExist(updateVendorNexus.getVendorId(), addItem.getCountry(), addItem.getState(), "*CITY", addItem.getCity(), "*CITY", 
						updateVendorNexusDtl.getEffectiveDate(), nexusTypeCode);

			} else if (addItem.isItemType(JurisdictionItem.STJ_TYPE)) {
				nExist = vendorNexusDetailService.isVendorNexusExist(updateVendorNexus.getVendorId(), addItem.getCountry(), addItem.getState(), "*STJ", "*STJ", addItem.getStj(), 
						updateVendorNexusDtl.getEffectiveDate(), nexusTypeCode);
			}
		
			if(!nExist){
				//selectedJurisdictionList.add(addItem);
				unsortJurisdictionItemMap.put(addItem.getKey(), addItem);
			}
		}
		
		isDisplayAuto = false;
		JurisdictionItem aItem = null;
		
		Map<String, String> duplicateDetailMap = new java.util.LinkedHashMap<String, String>();
		
		if(unsortJurisdictionItemMap.size()>0){
			String[] keyArray = new String[unsortJurisdictionItemMap.size()];

			int i = 0;
			for (String entityKey : unsortJurisdictionItemMap.keySet()) {
				keyArray[i++] = entityKey;
			}
			
			java.util.Arrays.sort(keyArray); 	
			for(int j=0; j<keyArray.length; j++){
				aItem = (JurisdictionItem)unsortJurisdictionItemMap.get(keyArray[j]);
				
				//Remove duplicate city and stj, like US/IL/COOK/CHICAGO and US/IL/DUPAGE/CHICAGO
				if(duplicateDetailMap.get(aItem.getCombinationKey())==null){
					duplicateDetailMap.put(aItem.getCombinationKey(), aItem.getCombinationKey());
					
					if(aItem.getNewItemFlag()){
						isDisplayAuto = true;
					}
					selectedJurisdictionList.add(aItem);
				}
			}	
		}
	}
	
	private void verifyRemoveTempJurisdictionList(){
		// Remove jurisdiction item which already have nexus definition
		selectedJurisdictionList = new ArrayList<JurisdictionItem>();
		
		Map<String, JurisdictionItem> unsortJurisdictionItemMap = new java.util.LinkedHashMap<String, JurisdictionItem>();

		JurisdictionItem addItem = null;
		boolean nExist = true;
		boolean auto = false;
		String nexusTypeCode = null;
		
		for (int i = 0; i < selectedTempJurisdictionList.size(); i++) {
			addItem = (JurisdictionItem) selectedTempJurisdictionList.get(i);
			
			if (addItem.isItemType(JurisdictionItem.COUNTY_TYPE) && !isIndependentCounty(addItem)) {
				//No need to add or remove
				continue;
			}
			
			if (addItem.isItemType(JurisdictionItem.STATE_TYPE)) {
				nExist = vendorNexusDetailService.isVendorNexusActive(updateVendorNexus.getVendorId(), addItem.getCountry(), addItem.getState(), "*STATE", "*STATE", "*STATE");
			} else if (addItem.isItemType(JurisdictionItem.COUNTY_TYPE)) {
				nExist = vendorNexusDetailService.isVendorNexusActive(updateVendorNexus.getVendorId(), addItem.getCountry(), addItem.getState(), addItem.getCounty(), "*COUNTY", "*COUNTY");
			} else if (addItem.isItemType(JurisdictionItem.CITY_TYPE)) {
				nExist = vendorNexusDetailService.isVendorNexusActive(updateVendorNexus.getVendorId(), addItem.getCountry(), addItem.getState(), "*CITY", addItem.getCity(), "*CITY");

			} else if (addItem.isItemType(JurisdictionItem.STJ_TYPE)) {
				nExist = vendorNexusDetailService.isVendorNexusActive(updateVendorNexus.getVendorId(), addItem.getCountry(), addItem.getState(), "*STJ", "*STJ", addItem.getStj());
			}
		
			if(nExist){//Display for active
				//selectedJurisdictionList.add(addItem);
				unsortJurisdictionItemMap.put(addItem.getKey(), addItem);
			}
		}
		
		isDisplayAuto = false;
		JurisdictionItem aItem = null;
		
		Map<String, String> duplicateDetailMap = new java.util.LinkedHashMap<String, String>();
		
		if(unsortJurisdictionItemMap.size()>0){
			String[] keyArray = new String[unsortJurisdictionItemMap.size()];
			
			
			int i = 0;
			for (String entityKey : unsortJurisdictionItemMap.keySet()) {
				keyArray[i++] = entityKey;
			}
			
			java.util.Arrays.sort(keyArray); 
			for(int j=0; j<keyArray.length; j++){
				aItem = (JurisdictionItem)unsortJurisdictionItemMap.get(keyArray[j]);
				
				//Remove duplicate city and stj, like US/IL/COOK/CHICAGO and US/IL/DUPAGE/CHICAGO
				if(duplicateDetailMap.get(aItem.getCombinationKey())==null){
					duplicateDetailMap.put(aItem.getCombinationKey(), aItem.getCombinationKey());
				
					if(aItem.getNewItemFlag()){
						isDisplayAuto = true;
					}
					selectedJurisdictionList.add(aItem);
				}
			}
		}
	}

	public String displayAddRegAction() {

		nexusAction = NexusDefRegAction.addRegistration;

		if (selectedVendorItem == null
				|| selectedVendorItem.isItemType(JurisdictionItem.COUNTRY_TYPE)
				|| selectedVendorItem.getIsAllFlag()) {
			FacesMessage message = new FacesMessage(
					"Please highlight a State, County, City, or STJ.");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
			return null;
		}

		this.selectedJurisdiction = new NexusJurisdictionDTO();
		this.selectedJurisdiction.setCountryCode(selectedVendorItem
				.getCountry());
		this.selectedJurisdiction.setCountryName(cacheManager.getCountryMap()
				.get(selectedVendorItem.getCountry()));
		this.selectedJurisdiction.setStateCode(selectedVendorItem.getState());
		this.selectedJurisdiction.setStateName("xxxx");

		if (selectedVendorItem.isItemType(JurisdictionItem.STATE_TYPE)) {
			selectedJurisdiction.setCounty("*STATE");
			selectedJurisdiction.setCity("*STATE");
			selectedJurisdiction.setStj("*STATE");
		} else if (selectedVendorItem.isItemType(JurisdictionItem.COUNTY_TYPE)) {
			selectedJurisdiction.setCounty(selectedVendorItem.getDescription());
			selectedJurisdiction.setCity("*COUNTY");
			selectedJurisdiction.setStj("*COUNTY");
		} else if (selectedVendorItem.isItemType(JurisdictionItem.COUNTY_TYPE)) {
			selectedJurisdiction.setCounty("*CITY");
			selectedJurisdiction.setCity(selectedVendorItem.getDescription());
			selectedJurisdiction.setStj("*CITY");
		} else if (selectedVendorItem.isItemType(JurisdictionItem.STJ_TYPE)) {
			selectedJurisdiction.setCounty("*STJ");
			selectedJurisdiction.setCity("*STJ");
			selectedJurisdiction.setStj(selectedVendorItem.getDescription());
		}

		// Registration
		updateRegistration = new Registration();
		// check NEXUS_DEF_ID
		updateRegistration.setEntityId(this.updateVendorItem.getVendorId());
		updateRegistration.setRegCountryCode(selectedJurisdiction
				.getCountryCode());
		updateRegistration.setRegStateCode(selectedJurisdiction.getStateCode());
		updateRegistration.setRegCounty(selectedJurisdiction.getCounty());
		updateRegistration.setRegCity(selectedJurisdiction.getCity());
		updateRegistration.setRegStj(selectedJurisdiction.getStj());

		// Registration detail
		this.updateRegistrationDetail = new RegistrationDetail();
		this.updateRegistrationDetail
				.setEffectiveDate(new com.ncsts.view.util.DateConverter()
						.getUserLocalDate());
		// this.updateRegistrationDetail.setRegTypeCode("");
		try {
			this.updateRegistrationDetail.setExpirationDate(maxDate
					.parse("12/31/9999"));
		} catch (ParseException e) {
		}
		this.updateRegistrationDetail.setActiveFlag("1");

		return "nexusvendor_update";
	}

	public String getDefNexusType() {
		Option option = optionService.findByPK(new OptionCodePK(
				"DEFAULTNEXUSTYPE", "SYSTEM", "SYSTEM"));
		return option.getValue();
	}

	public String okAddRegistrationAction() {
		// Validate dates
		if (this.updateRegistrationDetail.getEffectiveDate() == null) {
			FacesMessage message = new FacesMessage(
					"Effective Date is required.");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
			return null;
		} else if (this.updateRegistrationDetail.getExpirationDate() == null) {
			FacesMessage message = new FacesMessage(
					"Expiration Date is required.");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
			return null;
		} else if (this.updateRegistrationDetail.getExpirationDate().getTime() < this.updateRegistrationDetail
				.getEffectiveDate().getTime()) {
			FacesMessage message = new FacesMessage(
					"Expiration Date must be equal or greater than the Effective Date.");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
			return null;
		} else if (this.updateRegistrationDetail.getRegTypeCode() == null
				|| this.updateRegistrationDetail.getRegTypeCode().length() == 0) {
			FacesMessage message = new FacesMessage(
					"Registration Type is mandatory. ");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
			return null;
		} else if (this.updateRegistrationDetail.getRegNo() == null
				|| this.updateRegistrationDetail.getRegNo().length() == 0) {
			FacesMessage message = new FacesMessage(
					"Registration No. is mandatory. ");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
			return null;
		}

		if (this.updateRegistration.getRegistrationId() == null) {
			try {
				registrationService.save(updateRegistration);
			} catch (Exception e) {
				FacesMessage message = new FacesMessage(
						"Error saving Registration: " + e.getMessage());
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage(null, message);
				return null;
			}
		} else {
			try {
				registrationService.update(updateRegistration);
			} catch (Exception e) {
				FacesMessage message = new FacesMessage(
						"Error updating Registration: " + e.getMessage());
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage(null, message);
				return null;
			}
		}

		this.updateRegistrationDetail.setRegistrationId(this.updateRegistration
				.getRegistrationId());
		try {
			registrationDetailService.save(updateRegistrationDetail);
		} catch (Exception e) {
			FacesMessage message = new FacesMessage(
					"Error saving Registration Detail: " + e.getMessage());
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
			return null;
		}

		loadDefinitionDetailList(selectedVendorItem,
				this.updateVendorItem.getVendorId());
		loadRegistrationDetailList(selectedVendorItem,
				this.updateVendorItem.getVendorId());

		return "nexusvendor_main";
	}

	private boolean insertDefinitionAndDetail(String country, String state,
			String county, String city, String stj, Boolean auto) {
		VendorNexus workingNexusDefinition = new VendorNexus();
		BeanUtils.copyProperties(updateVendorNexus, workingNexusDefinition);

		VendorNexusDtl workingNexusDefinitionDetail = new VendorNexusDtl();
		BeanUtils.copyProperties(updateVendorNexusDtl,
				workingNexusDefinitionDetail);

		workingNexusDefinition.setNexusCountryCode(country);
		workingNexusDefinition.setNexusStateCode(state);
		workingNexusDefinition.setNexusCounty(county);
		workingNexusDefinition.setNexusCity(city);
		workingNexusDefinition.setNexusStj(stj);

		// Check if the definition combination of ENTITY_ID, NEXUS_COUNTRY_CODE,
		// NEXUS_STATE_CODE, NEXUS_COUNTY, NEXUS_CITY, NEXUS_STJ already exists
		try {
			VendorNexus foundNexusDefinition = vendorNexusService
					.findOneByExample(workingNexusDefinition);
			if (foundNexusDefinition != null) {
				workingNexusDefinition.setVendorNexusId(foundNexusDefinition
						.getVendorNexusId());
			}

		} catch (Exception e) {
			FacesMessage message = new FacesMessage(
					"Error checking Nexus Definition : " + e.getMessage());
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
			return false;
		}

		if (workingNexusDefinition.getVendorNexusId() == null) {
			workingNexusDefinition.setNexusFlag("1");
			try {
				vendorNexusService.save(workingNexusDefinition);
			} catch (Exception e) {
				FacesMessage message = new FacesMessage(
						"Error saving Nexus Definition: " + e.getMessage());
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage(null, message);
				return false;
			}
		} else {
			if (!"1".equals(workingNexusDefinition.getNexusFlag())) {
				workingNexusDefinition.setNexusFlag("1");
				try {
					vendorNexusService.update(workingNexusDefinition);
				} catch (Exception e) {
					FacesMessage message = new FacesMessage(
							"Error updating Nexus Definition: "
									+ e.getMessage());
					message.setSeverity(FacesMessage.SEVERITY_ERROR);
					FacesContext.getCurrentInstance().addMessage(null, message);
					return false;
				}
			}
		}

		// Check if the definition detail combination of NEXUS_DEF_ID,
		// EFFECTIVE_DATE already exists
		// If auto, upper level, need to consider NexusTypeCode(
		try {
			if (auto) {
				if (vendorNexusDetailService.isRegistrationDetailExist(
						workingNexusDefinition.getVendorNexusId(),
						workingNexusDefinitionDetail.getNexusTypeCode(),
						workingNexusDefinitionDetail.getEffectiveDate())) {
					return false;
				}
			} else {
				if (vendorNexusDetailService.isRegistrationDetailExist(
						workingNexusDefinition.getVendorNexusId(), null,
						workingNexusDefinitionDetail.getEffectiveDate())) {
					return false;
				}
			}
		} catch (Exception e) {
			FacesMessage message = new FacesMessage(
					"Error checking Nexus Definition Detail: " + e.getMessage());
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
			return false;
		}

		workingNexusDefinitionDetail.setVendorNexusId(workingNexusDefinition
				.getVendorNexusId());
		try {
			vendorNexusDetailService.save(workingNexusDefinitionDetail);
		} catch (Exception e) {
			FacesMessage message = new FacesMessage(
					"Error saving Nexus Definition Detail: " + e.getMessage());
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
			return false;
		}

		return true;
	}
	
	public String getPageDescription() {
		if(selectedJurisdictionList!=null && selectedJurisdictionList.size()>0){
			
			return String.format("Displaying rows %d through %d (%d matching rows)",
				1, selectedJurisdictionList.size(), selectedJurisdictionList.size());
		}
		else{
			return String.format("Displaying rows %d through %d (%d matching rows)",
					0, 0, 0);
		}
	}
	
	public void selectActiveCheckChange(ActionEvent e) throws AbortProcessingException {
		HtmlSelectBooleanCheckbox chk = (HtmlSelectBooleanCheckbox) ((HtmlAjaxSupport) e.getComponent()).getParent();
	}
	
	public String okAddNexusAction() {
		// Validate dates
		if (this.updateVendorNexusDtl.getEffectiveDate() == null) {
			FacesMessage message = new FacesMessage(
					"Effective Date is required.");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
			return null;
		} else if (this.updateVendorNexusDtl.getExpirationDate() == null) {
			FacesMessage message = new FacesMessage(
					"Expiration Date is required.");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
			return null;
		} else if (this.updateVendorNexusDtl.getExpirationDate()
				.getTime() < this.updateVendorNexusDtl
				.getEffectiveDate().getTime()) {
			FacesMessage message = new FacesMessage(
					"Expiration Date must be equal or greater than the Effective Date.");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
			return null;
		} else if (this.updateVendorNexusDtl.getNexusTypeCode() == null
				|| this.updateVendorNexusDtl.getNexusTypeCode().length() == 0) {
			FacesMessage message = new FacesMessage("Nexus Type is mandatory. ");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
			return null;
		}
		
		List<VendorNexus> nexusDefinitionArray = new ArrayList<VendorNexus>();
		List<VendorNexusDtl> nexusDefinitionDetailArray = new ArrayList<VendorNexusDtl>();
		VendorNexus workingNexusDefinition = null;
		VendorNexusDtl workingNexusDefinitionDetail = null;
				
		JurisdictionItem addItem = null;
		Long entityId = updateVendorNexus.getVendorId();
		String country = "";
		String state = "";
		String county = "";
		String city = "";
		String stj = "";
		Boolean newFlag = false;
		for (int i = 0; i < selectedJurisdictionList.size(); i++) {
			addItem = (JurisdictionItem) selectedJurisdictionList.get(i);

			if (addItem.isItemType(JurisdictionItem.STATE_TYPE)) {
				country = addItem.getCountry();
				state = addItem.getState();
				county = "*STATE";
				city = "*STATE";
				stj = "*STATE";
				newFlag = addItem.getNewItemFlag();
				
			} else if (addItem.isItemType(JurisdictionItem.COUNTY_TYPE)) {
				country = addItem.getCountry();
				state = addItem.getState();
				county = addItem.getCounty();
				city = "*COUNTY";
				stj = "*COUNTY";
				newFlag = addItem.getNewItemFlag();
			} else if (addItem.isItemType(JurisdictionItem.CITY_TYPE)) {
				country = addItem.getCountry();
				state = addItem.getState();
				county = "*CITY";
				city = addItem.getCity();
				stj = "*CITY";
				newFlag = addItem.getNewItemFlag();
			} else if (addItem.isItemType(JurisdictionItem.STJ_TYPE)) {
				country = addItem.getCountry();
				state = addItem.getState();
				county = "*STJ";
				city = "*STJ";
				stj = addItem.getStj();
				newFlag = addItem.getNewItemFlag();
			}
			else{
				return "nexusvendor_main";
			}
			
			workingNexusDefinition = new VendorNexus();
			workingNexusDefinition.setVendorId(entityId);
			workingNexusDefinition.setNexusCountryCode(country);
			workingNexusDefinition.setNexusStateCode(state);
			workingNexusDefinition.setNexusCounty(county);
			workingNexusDefinition.setNexusCity(city);
			workingNexusDefinition.setNexusStj(stj);
			//workingNexusDefinition.setNewItemFlag(newFlag);
			nexusDefinitionArray.add(workingNexusDefinition);
			
			
			workingNexusDefinitionDetail = new VendorNexusDtl();
			workingNexusDefinitionDetail.setNexusTypeCode(updateVendorNexusDtl.getNexusTypeCode());
			workingNexusDefinitionDetail.setEffectiveDate(updateVendorNexusDtl.getEffectiveDate());
			workingNexusDefinitionDetail.setExpirationDate(updateVendorNexusDtl.getExpirationDate());
			workingNexusDefinitionDetail.setActiveBooleanFlag(updateVendorNexusDtl.getActiveBooleanFlag());
			nexusDefinitionDetailArray.add(workingNexusDefinitionDetail);
		}
		
		vendorNexusService.addVendorNexus(nexusDefinitionArray, nexusDefinitionDetailArray);

		// Need to rebuild tree
		rootNode = null;
		treeBinding = null;

		// Reset datagrid
		vendorNexusDtlList = null;
		selectedDefinitionRowIndex = -1;
		selectedVendorNexusDtl = null;

		// Remove checked jurisdiction from memory
		if (selectedJurisdictionItemMap != null) {
			selectedJurisdictionItemMap.clear();
		}
		selectedJurisdictionItemMap = new LinkedHashMap<String, JurisdictionItem>();

		return "nexusvendor_main";
	}
	
	public String okRemoveNexusAction() {

		List<VendorNexus> nexusDefinitionArray = new ArrayList<VendorNexus>();	
		VendorNexus workingNexusDefinition = null;
		JurisdictionItem addItem = null;
		
		Long entityId = updateVendorNexus.getVendorId();
		String country = "";
		String state = "";
		String county = "";
		String city = "";
		String stj = "";

		for (int i = 0; i < selectedJurisdictionList.size(); i++) {
			addItem = (JurisdictionItem) selectedJurisdictionList.get(i);

			if (addItem.isItemType(JurisdictionItem.STATE_TYPE)) {
				country = addItem.getCountry();
				state = addItem.getState();
				county = "*STATE";
				city = "*STATE";
				stj = "*STATE";			
			} else if (addItem.isItemType(JurisdictionItem.COUNTY_TYPE)) {
				country = addItem.getCountry();
				state = addItem.getState();
				county = addItem.getCounty();
				city = "*COUNTY";
				stj = "*COUNTY";
			} else if (addItem.isItemType(JurisdictionItem.CITY_TYPE)) {
				country = addItem.getCountry();
				state = addItem.getState();
				county = "*CITY";
				city = addItem.getCity();
				stj = "*CITY";
			} else if (addItem.isItemType(JurisdictionItem.STJ_TYPE)) {
				country = addItem.getCountry();
				state = addItem.getState();
				county = "*STJ";
				city = "*STJ";
				stj = addItem.getStj();
			}
			else{
				return "nexusvendor_main";
			}
			
			workingNexusDefinition = new VendorNexus();
			workingNexusDefinition.setVendorId(entityId);
			workingNexusDefinition.setNexusCountryCode(country);
			workingNexusDefinition.setNexusStateCode(state);
			workingNexusDefinition.setNexusCounty(county);
			workingNexusDefinition.setNexusCity(city);
			workingNexusDefinition.setNexusStj(stj);
			nexusDefinitionArray.add(workingNexusDefinition);
		}
		
		vendorNexusService.removeVendorNexus(nexusDefinitionArray);


		// Need to rebuild tree
		rootNode = null;
		treeBinding = null;

		// Reset datagrid
		vendorNexusDtlList = null;
		selectedDefinitionRowIndex = -1;
		selectedVendorNexusDtl = null;

		// Remove checked jurisdiction from memory
		if (selectedJurisdictionItemMap != null) {
			selectedJurisdictionItemMap.clear();
		}
		selectedJurisdictionItemMap = new LinkedHashMap<String, JurisdictionItem>();

		return "nexusvendor_main";
	}

	private boolean insertDefinitionAndDetail(String county, String city, String stj) {
		VendorNexus workingNexusDefinition = new VendorNexus();
		BeanUtils.copyProperties(updateVendorNexus, workingNexusDefinition);

		VendorNexusDtl workingNexusDefinitionDetail = new VendorNexusDtl();
		BeanUtils.copyProperties(updateVendorNexusDtl,
				workingNexusDefinitionDetail);

		workingNexusDefinition.setNexusCounty(county);
		workingNexusDefinition.setNexusCity(city);
		workingNexusDefinition.setNexusStj(stj);

		// Check if the definition combination of ENTITY_ID, NEXUS_COUNTRY_CODE,
		// NEXUS_STATE_CODE, NEXUS_COUNTY, NEXUS_CITY, NEXUS_STJ already exists
		try {
			VendorNexus foundNexusDefinition = vendorNexusService
					.findOneByExample(workingNexusDefinition);
			if (foundNexusDefinition != null) {
				workingNexusDefinition.setVendorNexusId(foundNexusDefinition.getVendorNexusId());
			}

		} catch (Exception e) {
			FacesMessage message = new FacesMessage(
					"Error checking Nexus Definition : " + e.getMessage());
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
			return false;
		}

		if (workingNexusDefinition.getVendorNexusId() == null) {
			workingNexusDefinition.setNexusFlag("1");
			try {
				vendorNexusService.save(workingNexusDefinition);
			} catch (Exception e) {
				FacesMessage message = new FacesMessage(
						"Error saving Nexus Definition: " + e.getMessage());
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage(null, message);
				return false;
			}
		} else {
			if (!"1".equals(workingNexusDefinition.getNexusFlag())) {
				workingNexusDefinition.setNexusFlag("1");
				try {
					vendorNexusService.update(workingNexusDefinition);
				} catch (Exception e) {
					FacesMessage message = new FacesMessage(
							"Error updating Nexus Definition: "
									+ e.getMessage());
					message.setSeverity(FacesMessage.SEVERITY_ERROR);
					FacesContext.getCurrentInstance().addMessage(null, message);
					return false;
				}
			}
		}

		// Check if the definition detail combination of NEXUS_DEF_ID,
		// EFFECTIVE_DATE already exists
		try {
			if (vendorNexusDetailService.isRegistrationDetailExist(
					workingNexusDefinition.getVendorNexusId(),
					workingNexusDefinitionDetail.getNexusTypeCode(),
					workingNexusDefinitionDetail.getEffectiveDate())) {
				return false;
			}
		} catch (Exception e) {
			FacesMessage message = new FacesMessage(
					"Error checking Nexus Definition Detail: " + e.getMessage());
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
			return false;
		}

		workingNexusDefinitionDetail.setVendorNexusId(workingNexusDefinition
				.getVendorNexusId());
		try {
			vendorNexusDetailService.save(workingNexusDefinitionDetail);
		} catch (Exception e) {
			FacesMessage message = new FacesMessage(
					"Error saving Nexus Definition Detail: " + e.getMessage());
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
			return false;
		}

		return true;
	}

	public String displayUpdateAction() {

		if (selectedDefinitionRowIndex >= 0
				&& selectedVendorNexusDtl != null) {
			nexusAction = NexusDefRegAction.updateDefinition;

			updateVendorNexus = vendorNexusService.findById(selectedVendorNexusDtl.getVendorNexusId());

			updateVendorNexusDtl = new VendorNexusDtl();
			BeanUtils.copyProperties(selectedVendorNexusDtl,updateVendorNexusDtl);

			this.selectedJurisdiction = new NexusJurisdictionDTO();
			this.selectedJurisdiction.setCountryCode(updateVendorNexus
					.getNexusCountryCode());
			this.selectedJurisdiction.setStateCode(updateVendorNexus
					.getNexusStateCode());
			this.selectedJurisdiction.setCounty(updateVendorNexus
					.getNexusCounty());
			this.selectedJurisdiction.setCity(updateVendorNexus
					.getNexusCity());
			this.selectedJurisdiction.setStj(updateVendorNexus
					.getNexusStj());
		} else if (selectedRegistrationRowIndex >= 0
				&& selectedRegistrationDetail != null) {
			nexusAction = NexusDefRegAction.updateRegistration;

			updateRegistration = registrationService
					.findById(selectedRegistrationDetail.getRegistrationId());

			updateRegistrationDetail = new RegistrationDetail();
			BeanUtils.copyProperties(selectedRegistrationDetail,
					updateRegistrationDetail);

			this.selectedJurisdiction = new NexusJurisdictionDTO();
			this.selectedJurisdiction.setCountryCode(updateRegistration
					.getRegCountryCode());
			this.selectedJurisdiction.setStateCode(updateRegistration
					.getRegStateCode());
			this.selectedJurisdiction.setCounty(updateRegistration
					.getRegCounty());
			this.selectedJurisdiction.setCity(updateRegistration.getRegCity());
			this.selectedJurisdiction.setStj(updateRegistration.getRegStj());
		}
		
		nexusUsed = false;
		if(vendorNexusService.isVendorNexusDetailUsed(selectedVendorNexusDtl.getVendorNexusDtlId())){
			nexusUsed = true;
		}
		
		filterHandler.setGeocode(updateVendorItem.getGeocode());
		filterHandler.setCountry(updateVendorItem.getCountry());
		filterHandler.setState(updateVendorItem.getState());
		filterHandler.setCounty(updateVendorItem.getCounty());
		filterHandler.setCity(updateVendorItem.getCity());
		filterHandler.setZip(updateVendorItem.getZip());
		filterHandler.setZipPlus4(updateVendorItem.getZipplus4());

		return "nexusvendor_update";
	}

	public String okUpdateDefinitionAction() {
		// Validate dates
		if (this.updateVendorNexusDtl.getEffectiveDate() == null) {
			FacesMessage message = new FacesMessage(
					"Effective Date is required.");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
			return null;
		} else if (this.updateVendorNexusDtl.getExpirationDate() == null) {
			FacesMessage message = new FacesMessage(
					"Expiration Date is required.");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
			return null;
		} else if (this.updateVendorNexusDtl.getExpirationDate()
				.getTime() < this.updateVendorNexusDtl
				.getEffectiveDate().getTime()) {
			FacesMessage message = new FacesMessage(
					"Expiration Date must be equal or greater than the Effective Date.");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
			return null;
		} else if (this.updateVendorNexusDtl.getNexusTypeCode() == null
				|| this.updateVendorNexusDtl.getNexusTypeCode().length() == 0) {
			FacesMessage message = new FacesMessage("Nexus Type is mandatory. ");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
			return null;
		}

		try {
			vendorNexusDetailService.update(updateVendorNexusDtl);
		} catch (Exception e) {
			FacesMessage message = new FacesMessage(
					"Error saving Nexus Definition Detail: " + e.getMessage());
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
			return null;
		}
		
		loadDefinitionDetailList(selectedVendorItem,this.updateVendorItem.getVendorId());

	//	loadDefinitionDetailList(selectedVendorItem,
	//			this.updateVendorItem.getEntityId());
	//	loadRegistrationDetailList(selectedVendorItem,
	//			this.updateVendorItem.getEntityId());

		return "nexusvendor_main";
	}

	public String okUpdateRegistrationAction() {
		// Validate dates
		if (this.updateRegistrationDetail.getEffectiveDate() == null) {
			FacesMessage message = new FacesMessage(
					"Effective Date is required.");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
			return null;
		} else if (this.updateRegistrationDetail.getExpirationDate() == null) {
			FacesMessage message = new FacesMessage(
					"Expiration Date is required.");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
			return null;
		} else if (this.updateRegistrationDetail.getExpirationDate().getTime() < this.updateRegistrationDetail
				.getEffectiveDate().getTime()) {
			FacesMessage message = new FacesMessage(
					"Expiration Date must be equal or greater than the Effective Date.");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
			return null;
		} else if (this.updateRegistrationDetail.getRegTypeCode() == null
				|| this.updateRegistrationDetail.getRegTypeCode().length() == 0) {
			FacesMessage message = new FacesMessage(
					"Registration Type is mandatory. ");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
			return null;
		} else if (this.updateRegistrationDetail.getRegNo() == null
				|| this.updateRegistrationDetail.getRegNo().length() == 0) {
			FacesMessage message = new FacesMessage(
					"Registration No. is mandatory. ");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
			return null;
		}

		try {
			registrationDetailService.update(updateRegistrationDetail);
		} catch (Exception e) {
			FacesMessage message = new FacesMessage(
					"Error saving Nexus Definition Detail: " + e.getMessage());
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
			return null;
		}

	//	loadDefinitionDetailList(selectedVendorItem,
	//			this.updateVendorItem.getEntityId());
	//	loadRegistrationDetailList(selectedVendorItem,
	//			this.updateVendorItem.getEntityId());

		return "nexusvendor_main";
	}
	
	public boolean getNexusUsed(){
		return nexusUsed;
	}

	public String displayDeleteAction() {

		displayUpdateAction();

		if (selectedDefinitionRowIndex >= 0
				&& selectedVendorNexusDtl != null) {
			nexusAction = NexusDefRegAction.deleteDefinition;
		} else if (selectedRegistrationRowIndex >= 0
				&& selectedRegistrationDetail != null) {
			nexusAction = NexusDefRegAction.deleteRegistration;
		}
	
		
		if(nexusUsed){
			//Update active_flag = ��
			FacesMessage message = new FacesMessage("Nexus used on Transaction(s) and cannot be deleted. Click Ok to set to Inactive.");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
			
			nexusUsed = true;
		}

		return "nexusvendor_update";
	}

	public String okDeleteDefinitionAction() {

		try {
			if(vendorNexusService.isVendorNexusDetailUsed(selectedVendorNexusDtl.getVendorNexusDtlId())){
				VendorNexusDtl vendorNexusDtl = (VendorNexusDtl)vendorNexusDetailService.findById(selectedVendorNexusDtl.getVendorNexusDtlId());
				vendorNexusDtl.setActiveFlag("0");		
				vendorNexusDetailService.update(vendorNexusDtl);
			}
			else{
				vendorNexusDetailService.remove(updateVendorNexusDtl);
			}
		} catch (Exception e) {
			FacesMessage message = new FacesMessage(
					"Error deletinging Nexus Definition Detail: "
							+ e.getMessage());
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
			return null;
		}

		//pp-23
		VendorNexusDtl ndd = new VendorNexusDtl();
		ndd.setVendorNexusId(this.updateVendorNexus.getVendorNexusId());
		ndd.setActiveFlag("1");

		List<VendorNexusDtl> list = vendorNexusDetailService.findByExample(ndd, 0);
		if (list == null || list.size() == 0) {
			VendorNexus nd = vendorNexusService.findById(this.selectedVendorNexusDtl.getVendorNexusId());
			if (nd != null) {
				nd.setNexusFlag("0");
				try {
					vendorNexusService.update(nd);
				} catch (Exception e) {
					FacesMessage message = new FacesMessage("Error updating Nexus Definition: " + e.getMessage());
					message.setSeverity(FacesMessage.SEVERITY_ERROR);
					FacesContext.getCurrentInstance().addMessage(null, message);
					return null;
				}
				
				// No VendorNexusDtl any more
				// Need to rebuild tree
				rootNode = null;
				treeBinding = null;

				// Reset datagrid
				vendorNexusDtlList = null;
				selectedDefinitionRowIndex = -1;
				selectedVendorNexusDtl = null;

				// Remove checked jurisdiction from memory
				if (selectedJurisdictionItemMap != null) {
					selectedJurisdictionItemMap.clear();
				}
				selectedJurisdictionItemMap = new LinkedHashMap<String, JurisdictionItem>();	
			}
		}
		else{
			loadDefinitionDetailList(selectedVendorItem,this.updateVendorItem.getVendorId());
		}

	//	loadDefinitionDetailList(selectedVendorItem,
	//			this.updateVendorItem.getEntityId());
	//	loadRegistrationDetailList(selectedVendorItem,
	//			this.updateVendorItem.getEntityId());

		return "nexusvendor_main";
	}

	public String okDeleteRegistrationAction() {
		this.updateRegistrationDetail.setActiveFlag("0");
		try {
			registrationDetailService.remove(updateRegistrationDetail);
		} catch (Exception e) {
			FacesMessage message = new FacesMessage(
					"Error deletinging Registration Detail: " + e.getMessage());
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
			return null;
		}

		RegistrationDetail ndd = new RegistrationDetail();
		ndd.setRegistrationId(this.updateRegistration.getRegistrationId());
		// ndd.setActiveFlag("1");

		List<RegistrationDetail> list = registrationDetailService
				.findByExample(ndd, 0);
		if (list == null || list.size() == 0) {
			Registration nd = registrationService
					.findById(this.updateRegistrationDetail.getRegistrationId());
			if (nd != null) {
				// nd.setNexusFlag("0");
				try {
					// Delete the tb_registration record if all
					// tb_registration_detail records are deleted.
					registrationService.remove(nd);
				} catch (Exception e) {
					FacesMessage message = new FacesMessage(
							"Error updating Registration: " + e.getMessage());
					message.setSeverity(FacesMessage.SEVERITY_ERROR);
					FacesContext.getCurrentInstance().addMessage(null, message);
					return null;
				}
			}
		}

//		loadDefinitionDetailList(selectedVendorItem,
//				this.updateVendorItem.getEntityId());
//		loadRegistrationDetailList(selectedVendorItem,
//				this.updateVendorItem.getEntityId());

		return "nexusvendor_main";
	}

	public VendorNexus getUpdateVendorNexus() {
		return this.updateVendorNexus;
	}

	public void setUpdateVendorNexus(VendorNexus updateVendorNexus) {
		this.updateVendorNexus = updateVendorNexus;
	}

	public NexusJurisdictionDTO getSelectedJurisdiction() {
		return this.selectedJurisdiction;
	}

	public void setSelectedJurisdiction(
			NexusJurisdictionDTO selectedJurisdiction) {
		this.selectedJurisdiction = selectedJurisdiction;
	}

	public Registration getUpdateRegistration() {
		return this.updateRegistration;
	}

	public void setUpdateNexusDefinition(Registration updateRegistration) {
		this.updateRegistration = updateRegistration;
	}

	public void onpicklistchangedAction(ActionEvent e) {
	}

	public NexusVendorBackingBean() {
	}

	public void init() {
		filterHandlerShipto.reset();
		filterHandlerShipto.setCountry("");
		filterHandlerShipto.setState("");
	}

	private Long selectedDefaultTypeList = 0L;

	public Long getSelectedDefaultTypeList() {
		return this.selectedDefaultTypeList;
	}

	public void setSelectedDefaultTypeList(Long selectedDefaultTypeList) {
		this.selectedDefaultTypeList = selectedDefaultTypeList;
	}

	public String defaultTypeChange() {
		return null;
	}

	public boolean getIsEntityDefaultTableMainPanel() {
		if (selectedDefaultTypeList == 0L) {
			return true;
		} else {
			return false;
		}
	}

	public boolean getIsEntityDefaultListTablePanel() {
		if (selectedDefaultTypeList == 1L) {
			return true;
		} else {
			return false;
		}
	}

	public boolean getIsLocationListTablePanel() {
		if (selectedDefaultTypeList == 2L) {
			return true;
		} else {
			return false;
		}
	}

	public JurisdictionService getJurisdictionService() {
		return jurisdictionService;
	}

	public void setJurisdictionService(JurisdictionService jurisdictionService) {
		this.jurisdictionService = jurisdictionService;

		filterHandlerShipto.setJurisdictionService(getJurisdictionService());
	}

	public SearchJurisdictionHandler getFilterHandler() {
		return filterHandler;
	}

	public void resetFilter() {
		filterHandler.reset();
		filterHandler.setCountry("");
		filterHandler.setState("");
	}

	public String resetFilterSearchAction() {
		stj = "";

		filterHandlerShipto.reset();
		filterHandlerShipto.setCountry(matrixCommonBean.getUserPreferenceDTO()
				.getUserCountry());
		filterHandlerShipto.setState("");

		return null;
	}

	public String retrieveFilterSearchAction() {
		// Export selected nodes to array
		updateSelectedNode();

		rootNode = null;

		getTreeNode();

		//Reset
		vendorNexusDtlList = null;
		selectedDefinitionRowIndex = -1;
		selectedVendorNexusDtl = null;
				
		return null;
	}

	public String getEntityNameText() {
		if (selectedVendorItem != null) {

			if (selectedVendorItem.isItemType(JurisdictionItem.COUNTRY_TYPE)
					|| selectedVendorItem
							.isItemType(JurisdictionItem.STATE_TYPE)) {
				return selectedVendorItem.getDescription();
			} else {
				TaxCodeStateDTO aTaxCodeStateDTO = (TaxCodeStateDTO) cacheManager
						.getTaxCodeStateMap()
						.get(selectedVendorItem.getState());
				return aTaxCodeStateDTO.getName() + " ("
						+ selectedVendorItem.getState() + "), "
						+ selectedVendorItem.getDescription();
			}
		} else {
			return "";
		}
	}

	public void selectedDefinitionRowChanged(ActionEvent e)
			throws AbortProcessingException {
		logger.info("enter selectedDefinitionRowChanged");
		HtmlDataTable table = (HtmlDataTable) ((HtmlAjaxSupport) e
				.getComponent()).getParent();

		selectedDefinitionRowIndex = table.getRowIndex();
		selectedVendorNexusDtl = (VendorNexusDtl) table.getRowData();

		selectedRegistrationRowIndex = -1;
		selectedRegistrationDetail = null;
	}

	public void selectedRegistrationRowChanged(ActionEvent e)
			throws AbortProcessingException {
		logger.info("enter selectedRegistrationRowChanged");
		HtmlDataTable table = (HtmlDataTable) ((HtmlAjaxSupport) e
				.getComponent()).getParent();

		selectedRegistrationRowIndex = table.getRowIndex();
		selectedRegistrationDetail = (RegistrationDetail) table.getRowData();

		selectedDefinitionRowIndex = -1;
		selectedVendorNexusDtl = null;
	}

	public void selectedEntityDefaultRowChanged(ActionEvent e)
			throws AbortProcessingException {
		logger.info("enter selectedEntityDefaultRowChanged");
		HtmlDataTable table = (HtmlDataTable) ((HtmlAjaxSupport) e
				.getComponent()).getParent();

		selectedEntityDefaultRowIndex = table.getRowIndex();
		selectedEntityDefault = (EntityDefault) table.getRowData();
	}

	public void selectedLocationListDetailRowChanged(ActionEvent e)
			throws AbortProcessingException {
		logger.info("enter selectedLocationListDetailRowChanged");
		HtmlDataTable table = (HtmlDataTable) ((HtmlAjaxSupport) e
				.getComponent()).getParent();

		selectedLocationListDetailRowIndex = table.getRowIndex();
		selectedEntityLocnSetDtl = (EntityLocnSetDtl) table.getRowData();
	}

	private Map<Long, JurisdictionItem> globalJurisdictionArray = null;
	private Map<String, Long> countryStateJurisdictionMap = null;

	private Map<String, String> filteredCountryMap = null;
	private Map<String, String> filteredCountryStateMap = null;
	private Map<String, Long> countryStateChildMap = null;

	private Long entityIdSearch = null;
	private String geocodeSearch = "";
	private String countrySearch = "";
	private String stateSearch = "";
	private String countySearch = "";
	private String citySearch = "";
	private String zipSearch = "";
	private String stjSearch = "";

	private Map<String, Long> getCountryStateJurisdictionMap() {

		if (filteredCountryMap != null) {
			filteredCountryMap.clear();
		}
		filteredCountryMap = new LinkedHashMap<String, String>();

		if (filteredCountryStateMap != null) {
			filteredCountryStateMap.clear();
		}
		filteredCountryStateMap = new LinkedHashMap<String, String>();

		if (countryStateChildMap != null) {
			countryStateChildMap.clear();
		}
		countryStateChildMap = new LinkedHashMap<String, Long>();

		if (countryStateJurisdictionMap == null) {
			Map<String, Long> aMap = new LinkedHashMap<String, Long>();

//			Long entityId = updateVendorItem.getEntityId();
			/*
			 * String geocode = "010036520801"; String country = "US"; String
			 * state = "AL"; String county = "BALDWIN"; String city =
			 * "ROBERTSDALE"; String zip = "36567"; String stj = "ROBERTSDALE%";
			 */

			// keep the criteria for expanding state
			entityIdSearch = updateVendorItem.getVendorId();//Use TB_VENDOR.VENDOR_ID
			geocodeSearch = filterHandlerShipto.getGeocode();
			countrySearch = filterHandlerShipto.getCountry();
			stateSearch = filterHandlerShipto.getState();
			countySearch = filterHandlerShipto.getCounty();
			citySearch = filterHandlerShipto.getCity();
			zipSearch = filterHandlerShipto.getZip();
			stjSearch = this.stj;

			List<Object[]> list = jurisdictionService.findJurisdictionForCountryState(entityIdSearch,
							geocodeSearch, countrySearch, stateSearch,
							countySearch, citySearch, zipSearch, stjSearch, "Vendor");

			String countrystate = null;
			BigDecimal hasNexus = null;
			Long lHasNexus = null;

			BigDecimal hasChild = null;
			Long lHasChild = null;

			if (list != null) {
				for (Object[] n : list) {
					countrystate = (String) n[0] + (String) n[1] + "*STATE"
							+ "*STATE" + "*STATE";

					hasNexus = (BigDecimal) n[5];
					lHasNexus = new Long(0);
					try {
						lHasNexus = hasNexus.longValue();
					} catch (Exception e) {
					}
					aMap.put(countrystate, lHasNexus);

					hasChild = (BigDecimal) n[7];
					lHasChild = new Long(0);
					try {
						lHasChild = hasChild.longValue();
					} catch (Exception e) {
					}
					countryStateChildMap.put(countrystate, lHasChild);

					filteredCountryMap.put((String) n[0], (String) n[0]);
					filteredCountryStateMap.put((String) n[0] + (String) n[1],
							(String) n[0] + (String) n[1]);
				}
			}
			countryStateJurisdictionMap = aMap;
		}

		return countryStateJurisdictionMap;
	}

	private void loadTree() {
		try {
			globalJurisdictionArray = Collections
					.synchronizedMap(new LinkedHashMap<Long, JurisdictionItem>());

			entityItemId = 0;

			rootNode = new TreeNodeImpl();
			int sqlcount = 0;

			if (countryStateJurisdictionMap != null) {
				countryStateJurisdictionMap.clear();
			}
			countryStateJurisdictionMap = null;
			Map<String, Long> aCountryStateJurisdictionMap = getCountryStateJurisdictionMap();

			// Load countries
			List<ListCodes> listCodes = listCodesService
					.getListCodesByCodeTypeCode("COUNTRY");
			for (ListCodes listCode : listCodes) {

				if (!filteredCountryMap.containsKey(listCode.getCodeCode())) {
					continue;
				}

				// Add country
				JurisdictionItem countryItem = new JurisdictionItem(new Long(
						entityItemId++));
				countryItem.setParentEntityId(new Long(0));
				countryItem.setItemType(JurisdictionItem.COUNTRY_TYPE);
				countryItem.setCountry(listCode.getCodeCode());
				countryItem.setDescription(listCode.getDescription() + " ("
						+ listCode.getCodeCode() + ")");
				countryItem.setDisabledFlag(true);

				globalJurisdictionArray.put(countryItem.getEntityId(),
						countryItem);

				TreeNodeImpl nodeCountry = new TreeNodeImpl();
				nodeCountry.setData(countryItem);
				rootNode.addChild(countryItem.getEntityId(), nodeCountry);

				// Add states
				String strCountry = listCode.getCodeCode();

				Collection<TaxCodeStateDTO> taxCodeStateDTOList = cacheManager
						.getTaxCodeStateMap().values();
				if (strCountry != null && strCountry.length() > 0) {
					for (TaxCodeStateDTO taxCodeStateDTO : taxCodeStateDTOList) {
						if (strCountry.equalsIgnoreCase(taxCodeStateDTO
								.getCountry())
								&& ((taxCodeStateDTO.getActiveFlag() != null) && taxCodeStateDTO
										.getActiveFlag().equals("1"))) {

							if (!filteredCountryStateMap.containsKey(strCountry
									+ taxCodeStateDTO.getTaxCodeState())) {
								continue;
							}

							JurisdictionItem stateItem = new JurisdictionItem(
									new Long(entityItemId++));
							stateItem.setParentEntityId(countryItem
									.getEntityId());
							stateItem.setItemType(JurisdictionItem.STATE_TYPE);
							stateItem.setCountry(countryItem.getCountry());
							stateItem.setState(taxCodeStateDTO
									.getTaxCodeState());
							stateItem.setDescription(taxCodeStateDTO.getName()
									+ " (" + taxCodeStateDTO.getTaxCodeState()
									+ ")");
							stateItem.setDisabledFlag(false);
							stateItem.setIsAllFlag(false);
							stateItem.setNeedExpandFlag(true);

							// Map tree node checked
							if (selectedJurisdictionItemMap.get(stateItem
									.getKey()) != null) {
								stateItem.setSelectedFlag(true);
							}

							globalJurisdictionArray.put(
									stateItem.getEntityId(), stateItem);

							TreeNodeImpl nodeState = new TreeNodeImpl();
							nodeState.setData(stateItem);
							nodeCountry.addChild(stateItem.getEntityId(),
									nodeState);

							Long lHasNexus = aCountryStateJurisdictionMap
									.get(stateItem.getCountry()
											+ stateItem.getState() + "*STATE"
											+ "*STATE" + "*STATE");
							if (lHasNexus != null) {
								// Need to query database when expand
								if (lHasNexus.intValue() > 0) {
									stateItem.setType(MAP_MAPPED);
								}
							}

							Long lHasChild = countryStateChildMap.get(stateItem
									.getCountry()
									+ stateItem.getState()
									+ "*STATE" + "*STATE" + "*STATE");
							if (lHasChild != null && lHasChild.intValue() > 0) {
								// Need to query database when expand
								stateItem.setIsAllFlag(true);

								JurisdictionItem dummyItem = new JurisdictionItem(
										new Long(entityItemId++));
								dummyItem.setParentEntityId(stateItem
										.getEntityId());
								dummyItem
										.setItemType(JurisdictionItem.COUNTY_TYPE);
								dummyItem.setCountry(countryItem.getCountry());
								dummyItem.setState(taxCodeStateDTO
										.getTaxCodeState());
								dummyItem.setIsAllFlag(true);
								dummyItem.setDescription("Dummy");
								dummyItem.setDisabledFlag(true);
								if (lHasNexus != null
										&& lHasNexus.intValue() > 0) {
									dummyItem.setType(MAP_MAPPED);
								}

								globalJurisdictionArray.put(
										dummyItem.getEntityId(), dummyItem);

								TreeNodeImpl nodeDummy = new TreeNodeImpl();
								nodeDummy.setData(dummyItem);
								nodeState.addChild(dummyItem.getEntityId(),
										nodeDummy);
							}
						}
					}
				}
			}

		} catch (Exception e) {
			throw new FacesException(e.getMessage(), e);
		} finally {

		}
	}

	private List<NexusJurisdictionItemDTO> buildList(List<Object[]> list) {
		List<NexusJurisdictionItemDTO> result = null;
		if (list != null) {
			result = new ArrayList<NexusJurisdictionItemDTO>();
			for (Object[] n : list) {
				NexusJurisdictionItemDTO sj = new NexusJurisdictionItemDTO();
				sj.setName((String) n[0]);
				if ("1".equals(n[1] + "")) {
					sj.setHasNexus(true);
				}
				result.add(sj);
			}
		}

		return result;
	}

	protected UITree treeBinding;

	public void setTreeBinding(UITree treeBinding) {
		this.treeBinding = treeBinding;
	}

	public UITree getTreeBinding() {
		return treeBinding;
	}

	private JurisdictionItem selectedAllJurisdictionItem = null;
	private Long curentSelectedAllId = null;

	public void selectAllClicked(ActionEvent e) throws AbortProcessingException {
		String command = (String) FacesContext.getCurrentInstance()
				.getExternalContext().getRequestParameterMap().get("command");
		selectedAllJurisdictionItem = (JurisdictionItem) globalJurisdictionArray
				.get(Long.valueOf(command));

		curentSelectedAllId = Long.valueOf(command);
		try {
			final TreeState state = (TreeState) treeBinding.getComponentState();
			treeBinding.walk(FacesContext.getCurrentInstance(),
					new DataVisitor() {
						@SuppressWarnings("unchecked")
						public void process(FacesContext context,
								Object rowKey, Object argument)
								throws IOException {
							TreeNode treeNode = treeBinding
									.getModelTreeNode(rowKey);
							if (treeNode != null) {
								JurisdictionItem entityItem = (JurisdictionItem) treeNode.getData();
								if (entityItem.getEntityId().intValue() == curentSelectedAllId.intValue()) {

									if (entityItem.isItemType(JurisdictionItem.STATE_TYPE) && entityItem.getNeedExpandFlag()) {
										// Remove dummy item
										Iterator<java.util.Map.Entry<Object, TreeNode>> childIter = treeNode.getChildren();
										while (childIter != null && childIter.hasNext()) {
											Map.Entry<Object, TreeNode> entry = childIter.next();
											Long entityId = (Long) entry.getKey();
											treeNode.removeChild(entityId);

											// Remove from map
											globalJurisdictionArray.remove(entityId);
										}
										buildCountyCitySTJTree(entityItem, treeNode);
										entityItem.setNeedExpandFlag(false);
									}

									// Expand current node
									state.expandNode(treeBinding,(TreeRowKey<Object>) rowKey);
									
									//No All Cities or All Counties or County with no independent
									if (!(entityItem.isItemType(JurisdictionItem.CITY_TYPE) && entityItem.getIsAllFlag()) && 
											!(entityItem.isItemType(JurisdictionItem.STJ_TYPE) && entityItem.getIsAllFlag()) &&
												!(entityItem.isItemType(JurisdictionItem.COUNTY_TYPE) && !entityItem.getIndepentdentFlag())) {
										// check current node
										entityItem.setSelectedFlag(true);
									}

									// check all children
									makeChecked(treeNode);

									return;
								}
							}
						}
					});
		} catch (IOException er) {
			er.printStackTrace();
		}
	}

	private Map<String, JurisdictionItem> selectedJurisdictionItemMap = new LinkedHashMap<String, JurisdictionItem>();

	public void checkboxChecked(ActionEvent e) throws AbortProcessingException {
		HtmlSelectBooleanCheckbox chk = (HtmlSelectBooleanCheckbox) ((HtmlAjaxSupport) e
				.getComponent()).getParent();

		String command = (String) FacesContext.getCurrentInstance()
				.getExternalContext().getRequestParameterMap().get("command");

		JurisdictionItem selectedJurisdictionItem = (JurisdictionItem) globalJurisdictionArray
				.get(Long.valueOf(command));

		if ((Boolean) chk.getValue()) {
			selectedJurisdictionItemMap.put(selectedJurisdictionItem.getKey(),
					selectedJurisdictionItem);
		} else {
			selectedJurisdictionItemMap.remove(selectedJurisdictionItem
					.getKey());
		}
	}

	public void makeChecked(TreeNode parent) {
		JurisdictionItem entityItem = null;
		Iterator<Map.Entry<Object, TreeNode>> it = parent.getChildren();
		while (it != null && it.hasNext()) {
			Map.Entry<Object, TreeNode> entry = it.next();

			entityItem = (JurisdictionItem) entry.getValue().getData();
			
			
			//No All Cities or All Counties
			if (!(entityItem.isItemType(JurisdictionItem.CITY_TYPE) && entityItem.getIsAllFlag()) && 
					!(entityItem.isItemType(JurisdictionItem.STJ_TYPE) && entityItem.getIsAllFlag()) &&
						!(entityItem.isItemType(JurisdictionItem.COUNTY_TYPE) && !entityItem.getIndepentdentFlag()) ) {
				// check current node
				entityItem.setSelectedFlag(true);
			}

			makeChecked(entry.getValue());
		}
	}

	public void clearChecked(TreeNode parent) {
		JurisdictionItem entityItem = null;
		Iterator<Map.Entry<Object, TreeNode>> it = parent.getChildren();
		while (it != null && it.hasNext()) {
			Map.Entry<Object, TreeNode> entry = it.next();

			entityItem = (JurisdictionItem) entry.getValue().getData();
			entityItem.setSelectedFlag(false);

			clearChecked(entry.getValue());
		}
	}

	public void clearAllSelected(ActionEvent e) throws AbortProcessingException {

		// Remove checked jurisdiction from memory
		if (selectedJurisdictionItemMap != null) {
			selectedJurisdictionItemMap.clear();
		}
		selectedJurisdictionItemMap = new LinkedHashMap<String, JurisdictionItem>();

		// Remove check from tree
		if (rootNode != null) {
			clearChecked(rootNode);
		}
	}

	// Update selectedJurisdictionItemMap when Add or Search
	public void updateChecked(TreeNode parent) {
		JurisdictionItem entityItem = null;
		Iterator<Map.Entry<Object, TreeNode>> it = parent.getChildren();
		while (it != null && it.hasNext()) {
			Map.Entry<Object, TreeNode> entry = it.next();
			entityItem = (JurisdictionItem) entry.getValue().getData();

			if (entityItem.getSelectedFlag()) {
				// Add to selectedJurisdictionItemMap
				selectedJurisdictionItemMap
						.put(entityItem.getKey(), entityItem);
			} else {
				// Remove from selectedJurisdictionItemMap
				selectedJurisdictionItemMap.remove(entityItem.getKey());
			}

			updateChecked(entry.getValue());

		}
	}

	public void updateSelectedNode() {
		// Remove check from tree
		if (rootNode != null) {
			updateChecked(rootNode);
		}
	}

	public void checkboxCheckedBK(ActionEvent e)
			throws AbortProcessingException {
		HtmlSelectBooleanCheckbox chk = (HtmlSelectBooleanCheckbox) ((HtmlAjaxSupport) e
				.getComponent()).getParent();

		String command = (String) FacesContext.getCurrentInstance()
				.getExternalContext().getRequestParameterMap().get("command");

		JurisdictionItem selectedJurisdictionItem = (JurisdictionItem) globalJurisdictionArray
				.get(Long.valueOf(command));

		if ((Boolean) chk.getValue()) {
			countryChecked = true;
			stateChecked = true;

			for (Long entityKey : globalJurisdictionArray.keySet()) {
				JurisdictionItem entityItem = (JurisdictionItem) globalJurisdictionArray
						.get(entityKey);

				if (entityItem.isItemType(JurisdictionItem.COUNTRY_TYPE)) {
					continue;
				}

				// Check all under a state
				if (selectedJurisdictionItem
						.isItemType(JurisdictionItem.STATE_TYPE)) {
					if (entityItem.getCountry().equals(
							selectedJurisdictionItem.getCountry())
							&& entityItem.getState().equals(
									selectedJurisdictionItem.getState())) {
						// In the same state
						if (entityItem.isItemType(JurisdictionItem.STATE_TYPE)) {
							entityItem.setSelectedFlag(true);
						} else {
							entityItem.setSelectedFlag(false);
						}
						entityItem.setDisabledFlag(false);
					} else {
						entityItem.setSelectedFlag(false);

						if (entityItem.isItemType(JurisdictionItem.STATE_TYPE)) {
							entityItem.setDisabledFlag(false);
						} else {
							// Disable jurisdictions without definition
							if (entityItem.getType().equalsIgnoreCase(
									MAP_MAPPED_SELECTED)
									|| entityItem.getType().equalsIgnoreCase(
											MAP_MAPPED)) {
								entityItem.setDisabledFlag(false);
							} else {
								entityItem.setDisabledFlag(true);
							}
						}
					}
				}
				// Check all under a county
				else if (selectedJurisdictionItem
						.isItemType(JurisdictionItem.COUNTY_TYPE)
						&& selectedJurisdictionItem.getIsAllFlag()) {
					if (entityItem.getCountry().equals(
							selectedJurisdictionItem.getCountry())
							&& entityItem.getState().equals(
									selectedJurisdictionItem.getState())
							&& entityItem
									.isItemType(JurisdictionItem.COUNTY_TYPE)) {

						entityItem.setSelectedFlag(true);
					}
				}
				// Check all under a city
				else if (selectedJurisdictionItem
						.isItemType(JurisdictionItem.CITY_TYPE)
						&& selectedJurisdictionItem.getIsAllFlag()) {
					if (entityItem.getCountry().equals(
							selectedJurisdictionItem.getCountry())
							&& entityItem.getState().equals(
									selectedJurisdictionItem.getState())
							&& entityItem
									.isItemType(JurisdictionItem.CITY_TYPE)) {

						entityItem.setSelectedFlag(true);
					}
				}
				// Check all under a STJ
				else if (selectedJurisdictionItem
						.isItemType(JurisdictionItem.STJ_TYPE)
						&& selectedJurisdictionItem.getIsAllFlag()) {
					if (entityItem.getCountry().equals(
							selectedJurisdictionItem.getCountry())
							&& entityItem.getState().equals(
									selectedJurisdictionItem.getState())
							&& entityItem.isItemType(JurisdictionItem.STJ_TYPE)) {

						entityItem.setSelectedFlag(true);
					}
				}

				if (!(entityItem.getCountry().equals(
						selectedJurisdictionItem.getCountry()) && entityItem
						.getState().equals(selectedJurisdictionItem.getState()))) {
					entityItem.setSelectedFlag(false);

					if (entityItem.isItemType(JurisdictionItem.COUNTY_TYPE)
							|| entityItem
									.isItemType(JurisdictionItem.CITY_TYPE)
							|| entityItem.isItemType(JurisdictionItem.STJ_TYPE)) {
						entityItem.setSelectedFlag(false);

						// Disable jurisdictions without definition
						if (entityItem.getType().equalsIgnoreCase(
								MAP_MAPPED_SELECTED)
								|| entityItem.getType().equalsIgnoreCase(
										MAP_MAPPED)) {
							entityItem.setDisabledFlag(false);
						} else {
							entityItem.setDisabledFlag(true);
						}
					}
				}
			}

		} else {
			countryChecked = false;
			stateChecked = false;

			for (Long entityKey : globalJurisdictionArray.keySet()) {
				JurisdictionItem entityItem = (JurisdictionItem) globalJurisdictionArray
						.get(entityKey);

				if (entityItem.isItemType(JurisdictionItem.COUNTRY_TYPE)) {
					continue;
				}

				// Uncheck state level
				// Disable jurisdictions without definition
				if (selectedJurisdictionItem
						.isItemType(JurisdictionItem.STATE_TYPE)) {
					if (entityItem.isItemType(JurisdictionItem.STATE_TYPE)) {
						entityItem.setSelectedFlag(false);
						entityItem.setDisabledFlag(false);
					} else {
						entityItem.setSelectedFlag(false);
						entityItem.setDisabledFlag(false);

						if (entityItem.getDescription().equalsIgnoreCase(
								"All Counties")
								|| entityItem.getDescription()
										.equalsIgnoreCase("All Cities")
								|| entityItem.getDescription()
										.equalsIgnoreCase("All STJs")) {
							entityItem.setDisabledFlag(true);
						}
					}
				}

				// Uncheck all under a county
				else if (selectedJurisdictionItem
						.isItemType(JurisdictionItem.COUNTY_TYPE)
						&& selectedJurisdictionItem.getIsAllFlag()) {
					if (entityItem.getCountry().equals(
							selectedJurisdictionItem.getCountry())
							&& entityItem.getState().equals(
									selectedJurisdictionItem.getState())
							&& entityItem
									.isItemType(JurisdictionItem.COUNTY_TYPE)) {

						entityItem.setSelectedFlag(false);
					}
				}
				// Uncheck all under a city
				else if (selectedJurisdictionItem
						.isItemType(JurisdictionItem.CITY_TYPE)
						&& selectedJurisdictionItem.getIsAllFlag()) {
					if (entityItem.getCountry().equals(
							selectedJurisdictionItem.getCountry())
							&& entityItem.getState().equals(
									selectedJurisdictionItem.getState())
							&& entityItem
									.isItemType(JurisdictionItem.CITY_TYPE)) {

						entityItem.setSelectedFlag(false);
					}
				}
				// Uncheck all under a STJ
				else if (selectedJurisdictionItem
						.isItemType(JurisdictionItem.STJ_TYPE)
						&& selectedJurisdictionItem.getIsAllFlag()) {
					if (entityItem.getCountry().equals(
							selectedJurisdictionItem.getCountry())
							&& entityItem.getState().equals(
									selectedJurisdictionItem.getState())
							&& entityItem.isItemType(JurisdictionItem.STJ_TYPE)) {

						entityItem.setSelectedFlag(false);
					}
				}

				// Uncheck all county
				else if (entityItem.isItemType(JurisdictionItem.COUNTY_TYPE)
						&& entityItem.getIsAllFlag()
						&& entityItem.getSelectedFlag()) {
					// If �All Counties� was checked and an individual County is
					// unchecked then �All Counties� is automatically unchecked.
					if (entityItem.getCountry().equals(
							selectedJurisdictionItem.getCountry())
							&& entityItem.getState().equals(
									selectedJurisdictionItem.getState())
							&& selectedJurisdictionItem
									.isItemType(JurisdictionItem.COUNTY_TYPE)) {

						entityItem.setSelectedFlag(false);
					}
				} else if (entityItem.isItemType(JurisdictionItem.CITY_TYPE)
						&& entityItem.getIsAllFlag()
						&& entityItem.getSelectedFlag()) {
					// If �All Counties� was checked and an individual County is
					// unchecked then �All Counties� is automatically unchecked.
					if (entityItem.getCountry().equals(
							selectedJurisdictionItem.getCountry())
							&& entityItem.getState().equals(
									selectedJurisdictionItem.getState())
							&& selectedJurisdictionItem
									.isItemType(JurisdictionItem.CITY_TYPE)) {

						entityItem.setSelectedFlag(false);
					}
				} else if (entityItem.isItemType(JurisdictionItem.STJ_TYPE)
						&& entityItem.getIsAllFlag()
						&& entityItem.getSelectedFlag()) {
					// If �All Counties� was checked and an individual County is
					// unchecked then �All Counties� is automatically unchecked.
					if (entityItem.getCountry().equals(
							selectedJurisdictionItem.getCountry())
							&& entityItem.getState().equals(
									selectedJurisdictionItem.getState())
							&& selectedJurisdictionItem
									.isItemType(JurisdictionItem.STJ_TYPE)) {

						entityItem.setSelectedFlag(false);
					}
				}
			}
		}
	}

	public void processExpansion(NodeExpandedEvent event) {
		HtmlTree tree = (HtmlTree) event.getComponent();
		JurisdictionItem entityItem = (JurisdictionItem) tree.getRowData();

		// get the row key i.e. id of the given node.
		Object rowKey = tree.getRowKey();
		// get the model node of this node.
		TreeRowKey key = (TreeRowKey) tree.getRowKey();
		TreeState state = (TreeState) tree.getComponentState();
		if (state.isExpanded(key)) {
			if (entityItem.isItemType(JurisdictionItem.STATE_TYPE)
					&& entityItem.getNeedExpandFlag()) {
				TreeNode nodeState = tree.getModelTreeNode(rowKey);

				// Remove dummy item
				Iterator<java.util.Map.Entry<Object, TreeNode>> childIter = nodeState.getChildren();
				while (childIter != null && childIter.hasNext()) {
					Map.Entry<Object, TreeNode> entry = childIter.next();
					Long entityId = (Long) entry.getKey();
					nodeState.removeChild(entityId);

					// Remove from map
					globalJurisdictionArray.remove(entityId);
				}
				buildCountyCitySTJTree(entityItem, nodeState);

				entityItem.setNeedExpandFlag(false);
			}
		} else {
		}
	}
	
	public void resetTableSorting() {
		UIDataTable table = (UIDataTable)FacesContext.getCurrentInstance().getViewRoot().findComponent("nexusDefForm:detailTable");
		for (UIComponent column : table.getChildren()) {
			((UIColumn)column).setSortOrder(Ordering.UNSORTED);
		}
	}


	public void processSelection(NodeSelectedEvent event) {
		HtmlTree tree = (HtmlTree) event.getComponent();
		JurisdictionItem entityItem = (JurisdictionItem) tree.getRowData();
		if (rootNode != null) {
			TreeNode treeNode = rootNode.getChild(entityItem.getEntityId());
			if (treeNode != null) {
				JurisdictionItem aVendorItem = (JurisdictionItem) treeNode
						.getData();
			}
		}
		
		//Reset
		vendorNexusDtlList = null;
		selectedDefinitionRowIndex = -1;
		selectedVendorNexusDtl = null;

		if (entityItem.getType().equalsIgnoreCase(MAP_MAPPED_SELECTED)
				|| entityItem.getType().equalsIgnoreCase(MAP_SELECTED)) {
		} else {
			if (selectedVendorItem != null) {
				if (selectedVendorItem.getType().equalsIgnoreCase(
						MAP_MAPPED_SELECTED)) {
					selectedVendorItem.setType(MAP_MAPPED);
				} else {
					selectedVendorItem.setType(MAP_NONE);
				}
			}

			if (entityItem.getType().equalsIgnoreCase(MAP_MAPPED)) {
				entityItem.setType(MAP_MAPPED_SELECTED);
			} else {
				entityItem.setType(MAP_SELECTED);
			}

			// Set rowKey only through tree selection
			selectedVendorItem = entityItem;
			Object rowKey = tree.getRowKey();
			selectedRowKey = rowKey.toString();
		}

		if (entityItem.isItemType(JurisdictionItem.STATE_TYPE)
				&& entityItem.getNeedExpandFlag()) {
			Object rowKey = tree.getRowKey();
			TreeNode nodeState = tree.getModelTreeNode(rowKey);

			// Remove dummy item
			Iterator<java.util.Map.Entry<Object, TreeNode>> childIter = nodeState
					.getChildren();
			while (childIter != null && childIter.hasNext()) {
				Map.Entry<Object, TreeNode> entry = childIter.next();
				Long entityId = (Long) entry.getKey();
				nodeState.removeChild(entityId);

				// Remove from map
				globalJurisdictionArray.remove(entityId);
			}
			buildCountyCitySTJTree(entityItem, nodeState);

			entityItem.setNeedExpandFlag(false);
		}

		loadDefinitionDetailList(selectedVendorItem,
				this.updateVendorItem.getVendorId());
		// loadRegistrationDetailList(selectedVendorItem,
		// this.updateVendorItem.getEntityId());
	}

	public List<VendorNexusDtl> getVendorNexusDtlList() {
		return vendorNexusDtlList;
	}

	public void setVendorNexusDtlList(List<VendorNexusDtl> vendorNexusDtlList) {
		this.vendorNexusDtlList = vendorNexusDtlList;
	}

	public List<RegistrationDetail> getRegistrationDetailList() {
		return registrationDetailList;
	}

	public void setRegistrationDetailList(
			List<RegistrationDetail> registrationDetailList) {
		this.registrationDetailList = registrationDetailList;
	}

	public VendorNexusDtl getUpdateVendorNexusDtl() {
		return updateVendorNexusDtl;
	}

	public void setUpdateVendorNexusDtl(VendorNexusDtl updateVendorNexusDtl) {
		this.updateVendorNexusDtl = updateVendorNexusDtl;
	}

	public RegistrationDetail getUpdateRegistrationDetail() {
		return updateRegistrationDetail;
	}

	public void setUpdateRegistrationDetail(
			RegistrationDetail updateRegistrationDetail) {
		this.updateRegistrationDetail = updateRegistrationDetail;
	}

	private void loadRegistrationDetailList(JurisdictionItem jurisdictionItem,
			Long entityId) {
		if (jurisdictionItem != null) {
			String country = jurisdictionItem.getCountry();
			String state = jurisdictionItem.getState();

			if (jurisdictionItem.isItemType(JurisdictionItem.STATE_TYPE)) {
				registrationDetailList = registrationDetailService
						.findAllRegistrationDetails(entityId, country, state,
								"*STATE", "*STATE", "*STATE", true);
			} else if (jurisdictionItem
					.isItemType(JurisdictionItem.COUNTY_TYPE)) {
				if (jurisdictionItem.getIsAllFlag()) {
					registrationDetailList = registrationDetailService
							.findAllRegistrationDetails(entityId, country,
									state, "*ALL", "*COUNTY", "*COUNTY", true);
				} else {
					registrationDetailList = registrationDetailService
							.findAllRegistrationDetails(entityId, country,
									state, jurisdictionItem.getDescription(),
									"*COUNTY", "*COUNTY", true);
				}

			} else if (jurisdictionItem.isItemType(JurisdictionItem.CITY_TYPE)) {
				if (jurisdictionItem.getIsAllFlag()) {
					registrationDetailList = registrationDetailService
							.findAllRegistrationDetails(entityId, country,
									state, "*CITY", "*ALL", "*CITY", true);
				} else {
					registrationDetailList = registrationDetailService
							.findAllRegistrationDetails(entityId, country,
									state, "*CITY",
									jurisdictionItem.getDescription(), "*CITY",
									true);
				}

			} else if (jurisdictionItem.isItemType(JurisdictionItem.STJ_TYPE)) {
				if (jurisdictionItem.getIsAllFlag()) {
					registrationDetailList = registrationDetailService
							.findAllRegistrationDetails(entityId, country,
									state, "*STJ", "*STJ", "*ALL", true);
				} else {
					registrationDetailList = registrationDetailService
							.findAllRegistrationDetails(entityId, country,
									state, "*STJ", "*STJ",
									jurisdictionItem.getDescription(), true);
				}

			}
		}

		selectedRegistrationRowIndex = -1;
		selectedRegistrationDetail = null;
	}

	private void loadDefinitionDetailList(JurisdictionItem jurisdictionItem,
			Long vendorId) {
		vendorNexusDtlList = null;
		if (jurisdictionItem != null) {
			String country = jurisdictionItem.getCountry();
			String state = jurisdictionItem.getState();

			if (jurisdictionItem.isItemType(JurisdictionItem.STATE_TYPE)) {
				vendorNexusDtlList = vendorNexusDetailService
						.findAllVendorNexusDetails(vendorId, country,
								state, "*STATE", "*STATE", "*STATE", true);
			} else if (jurisdictionItem
					.isItemType(JurisdictionItem.COUNTY_TYPE)) {
				vendorNexusDtlList = vendorNexusDetailService
						.findAllVendorNexusDetails(vendorId, country,
								state, jurisdictionItem.getDescription(),
								"*COUNTY", "*COUNTY", true);
			} else if (jurisdictionItem.isItemType(JurisdictionItem.CITY_TYPE)
					&& !jurisdictionItem.getDescription().equalsIgnoreCase(
							"All Cities")) {
				vendorNexusDtlList = vendorNexusDetailService
						.findAllVendorNexusDetails(vendorId, country,
								state, "*CITY",
								jurisdictionItem.getDescription(), "*CITY",
								true);

			} else if (jurisdictionItem.isItemType(JurisdictionItem.STJ_TYPE)
					&& !jurisdictionItem.getDescription().equalsIgnoreCase(
							"All STJs")) {
				vendorNexusDtlList = vendorNexusDetailService
						.findAllVendorNexusDetails(vendorId, country,
								state, "*STJ", "*STJ",
								jurisdictionItem.getDescription(), true);
			}
		}

		selectedDefinitionRowIndex = -1;
		selectedVendorNexusDtl = null;
	}

	private void buildCountyCitySTJ_NEW(JurisdictionItem parentVendorItem,
			TreeNode nodeState) {

		String country = parentVendorItem.getCountry();
		String state = parentVendorItem.getState();
		Long parentEntityId = parentVendorItem.getEntityId();
		boolean isSelected = parentVendorItem.getSelectedFlag();

		// Add bogus All Counties, All Cities, or All STJs if any

		List<Object[]> list = null;

		// Add bogus All Counties if any
		list = jurisdictionService.findNexusJurisdiction(new Long(0), country,
				state, "I", "county");
		List<NexusJurisdictionItemDTO> countyList = buildList(list);

		Map<String, Long> aCountryStateJurisdictionMap = getCountryStateJurisdictionMap();
		Long lHasNexus = null;

		if (countyList != null && countyList.size() > 0) {
			// JurisdictionItem countyItem = new JurisdictionItem(new
			// Long(entityItemId++));
			// countyItem.setParentEntityId(parentEntityId);
			// countyItem.setItemType(JurisdictionItem.COUNTY_TYPE);
			// countyItem.setCountry(country);
			// countyItem.setState(state);
			// countyItem.setIsAllFlag(true);
			// countyItem.setDescription("All Counties");
			// countyItem.setDisabledFlag(true);
			// if(parentVendorItem.getType().equalsIgnoreCase(MAP_MAPPED) ||
			// parentVendorItem.getType().equalsIgnoreCase(MAP_MAPPED_SELECTED)){
			// countyItem.setDisabledFlag(false);
			// }

			// if(isSelected){
			// countyItem.setDisabledFlag(false);
			// countyItem.setSelectedFlag(false);
			// }

			// globalJurisdictionArray.put(countyItem.getEntityId(),
			// countyItem);

			// TreeNodeImpl nodeCounty = new TreeNodeImpl();
			// nodeCounty.setData(countyItem);
			// nodeState.addChild(countyItem.getEntityId(), nodeCounty);

			for (int i = 0; i < countyList.size(); i++) {
				NexusJurisdictionItemDTO aNexusJurisdictionItemDTO = (NexusJurisdictionItemDTO) countyList
						.get(i);

				JurisdictionItem aCountyItem = new JurisdictionItem(new Long(
						entityItemId++));
				// aCountyItem.setParentEntityId(countyItem.getEntityId());
				aCountyItem.setParentEntityId(parentEntityId);

				aCountyItem.setItemType(JurisdictionItem.COUNTY_TYPE);
				aCountyItem.setCountry(country);
				aCountyItem.setState(state);
				aCountyItem.setDescription(aNexusJurisdictionItemDTO.getName());
				aCountyItem.setDisabledFlag(true);
				if (parentVendorItem.getType().equalsIgnoreCase(MAP_MAPPED)
						|| parentVendorItem.getType().equalsIgnoreCase(
								MAP_MAPPED_SELECTED)) {
					aCountyItem.setDisabledFlag(false);
				}

				lHasNexus = aCountryStateJurisdictionMap.get(country + state
						+ aNexusJurisdictionItemDTO.getName() + "*COUNTY"
						+ "*COUNTY");
				if (lHasNexus != null && lHasNexus.intValue() > 0) {
					aCountyItem.setType(MAP_MAPPED);
					aCountyItem.setDisabledFlag(false);
				}

				if (isSelected) {
					aCountyItem.setDisabledFlag(false);
					aCountyItem.setSelectedFlag(false);
				}

				globalJurisdictionArray.put(aCountyItem.getEntityId(),
						aCountyItem);

				TreeNodeImpl aNodeCounty = new TreeNodeImpl();
				aNodeCounty.setData(aCountyItem);
				// nodeCounty.addChild(aCountyItem.getEntityId(), aNodeCounty);
				nodeState.addChild(aCountyItem.getEntityId(), aNodeCounty);

				// Add bogus All Cities if any

				list = jurisdictionService.findNexusJurisdiction(new Long(0),
						country, state, "I", "city");
				List<NexusJurisdictionItemDTO> cityList = buildList(list);
				if (cityList != null && cityList.size() > 0) {
					JurisdictionItem cityItem = new JurisdictionItem(new Long(
							entityItemId++));
					cityItem.setParentEntityId(parentEntityId);
					cityItem.setItemType(JurisdictionItem.CITY_TYPE);
					cityItem.setCountry(country);
					cityItem.setState(state);
					cityItem.setIsAllFlag(true);
					cityItem.setDescription("All Cities");
					cityItem.setDisabledFlag(true);
					if (parentVendorItem.getType().equalsIgnoreCase(MAP_MAPPED)
							|| parentVendorItem.getType().equalsIgnoreCase(
									MAP_MAPPED_SELECTED)) {
						cityItem.setDisabledFlag(false);
					}

					if (isSelected) {
						cityItem.setDisabledFlag(false);
						cityItem.setSelectedFlag(false);
					}

					globalJurisdictionArray.put(cityItem.getEntityId(),
							cityItem);

					TreeNodeImpl nodeCity = new TreeNodeImpl();
					nodeCity.setData(cityItem);
					// nodeState.addChild(cityItem.getEntityId(), nodeCity);
					aNodeCounty.addChild(cityItem.getEntityId(), nodeCity);

					for (int j = 0; j < cityList.size(); j++) {
						NexusJurisdictionItemDTO cityNexusJurisdictionItemDTO = (NexusJurisdictionItemDTO) cityList
								.get(j);

						JurisdictionItem aCityItem = new JurisdictionItem(
								new Long(entityItemId++));
						aCityItem.setParentEntityId(cityItem.getEntityId());
						aCityItem.setItemType(JurisdictionItem.CITY_TYPE);
						aCityItem.setCountry(country);
						aCityItem.setState(state);
						aCityItem.setDescription(cityNexusJurisdictionItemDTO
								.getName());
						aCityItem.setDisabledFlag(true);
						if (parentVendorItem.getType().equalsIgnoreCase(
								MAP_MAPPED)
								|| parentVendorItem.getType().equalsIgnoreCase(
										MAP_MAPPED_SELECTED)) {
							aCityItem.setDisabledFlag(false);
						}
						lHasNexus = aCountryStateJurisdictionMap.get(country
								+ state + "*CITY"
								+ cityNexusJurisdictionItemDTO.getName()
								+ "*CITY");
						if (lHasNexus != null && lHasNexus.intValue() > 0) {
							aCityItem.setType(MAP_MAPPED);
							aCityItem.setDisabledFlag(false);
						}

						if (isSelected) {
							aCityItem.setDisabledFlag(false);
							aCityItem.setSelectedFlag(false);
						}

						globalJurisdictionArray.put(aCityItem.getEntityId(),
								aCityItem);

						TreeNodeImpl aNodeCity = new TreeNodeImpl();
						aNodeCity.setData(aCityItem);
						nodeCity.addChild(aCityItem.getEntityId(), aNodeCity);
					}
				}

				// Add bogus All STJs if any
				list = jurisdictionService.findNexusJurisdiction(new Long(0),
						country, state, "I", "stj");
				List<NexusJurisdictionItemDTO> stjList = buildList(list);
				if (stjList != null && stjList.size() > 0) {
					JurisdictionItem stjItem = new JurisdictionItem(new Long(
							entityItemId++));
					stjItem.setParentEntityId(parentEntityId);
					stjItem.setItemType(JurisdictionItem.STJ_TYPE);
					stjItem.setCountry(country);
					stjItem.setState(state);
					stjItem.setIsAllFlag(true);
					stjItem.setDescription("All STJs");
					stjItem.setDisabledFlag(true);
					if (parentVendorItem.getType().equalsIgnoreCase(MAP_MAPPED)
							|| parentVendorItem.getType().equalsIgnoreCase(
									MAP_MAPPED_SELECTED)) {
						stjItem.setDisabledFlag(false);
					}

					if (isSelected) {
						stjItem.setDisabledFlag(false);
						stjItem.setSelectedFlag(false);
					}

					globalJurisdictionArray.put(stjItem.getEntityId(), stjItem);

					TreeNodeImpl nodeSTJ = new TreeNodeImpl();
					nodeSTJ.setData(stjItem);
					// nodeState.addChild(stjItem.getEntityId(), nodeSTJ);
					aNodeCounty.addChild(stjItem.getEntityId(), nodeSTJ);

					for (int k = 0; k < stjList.size(); k++) {
						NexusJurisdictionItemDTO stjNexusJurisdictionItemDTO = (NexusJurisdictionItemDTO) stjList
								.get(k);

						JurisdictionItem aSTJItem = new JurisdictionItem(
								new Long(entityItemId++));
						aSTJItem.setParentEntityId(stjItem.getEntityId());
						aSTJItem.setItemType(JurisdictionItem.STJ_TYPE);
						aSTJItem.setCountry(country);
						aSTJItem.setState(state);
						aSTJItem.setDescription(stjNexusJurisdictionItemDTO
								.getName());
						aSTJItem.setDisabledFlag(true);
						if (parentVendorItem.getType().equalsIgnoreCase(
								MAP_MAPPED)
								|| parentVendorItem.getType().equalsIgnoreCase(
										MAP_MAPPED_SELECTED)) {
							aSTJItem.setDisabledFlag(false);
						}
						lHasNexus = aCountryStateJurisdictionMap.get(country
								+ state + "*STJ" + "*STJ"
								+ stjNexusJurisdictionItemDTO.getName());
						if (lHasNexus != null && lHasNexus.intValue() > 0) {
							aSTJItem.setType(MAP_MAPPED);
							aSTJItem.setDisabledFlag(false);
						}

						if (isSelected) {
							aSTJItem.setDisabledFlag(false);
							aSTJItem.setSelectedFlag(false);
						}

						globalJurisdictionArray.put(aSTJItem.getEntityId(),
								aSTJItem);

						TreeNodeImpl aNodeSTJ = new TreeNodeImpl();
						aNodeSTJ.setData(aSTJItem);
						nodeSTJ.addChild(aSTJItem.getEntityId(), aNodeSTJ);
					}
				}
			}
		}

	}

	private void buildCountyCitySTJTree(JurisdictionItem parentVendorItem,
			TreeNode nodeState) {

		String country = parentVendorItem.getCountry();
		String state = parentVendorItem.getState();
		Long parentEntityId = parentVendorItem.getEntityId();
		boolean isSelected = parentVendorItem.getSelectedFlag();

		// Loop county and then city/stj using tree's country/state
		List<Object[]> list = jurisdictionService.findJurisdictionForCountyCityStj(entityIdSearch,geocodeSearch, country, state, countySearch,citySearch, zipSearch, stjSearch, "Vendor");
		String countyPrevious = "";

		String countyNew = "";
		String cityNew = "";
		String stjNew = "";
		String countyLocated = "";
		String independentFlag = "0";

		JurisdictionItem countyItem = null;
		TreeNodeImpl nodeCounty = null;

		JurisdictionItem allCityItem = null;
		TreeNodeImpl nodeAllCity = null;

		JurisdictionItem allStjItem = null;
		TreeNodeImpl nodeAllStj = null;

		BigDecimal hasNexus = null;
		Long lHasNexus = null;

		if (list == null || list.size() == 0) {
			// No show 'Select All'
			parentVendorItem.setIsAllFlag(false);
		}

		if (list != null) {
			// result = new ArrayList<NexusJurisdictionItemDTO>();
			for (Object[] n : list) {

				countyNew = (String) n[2];
				if (countyNew != null && countyNew.length() > 0) {
					countyNew = countyNew.trim();
				} else {
					countyNew = "";
				}
				countyLocated = countyNew;

				cityNew = (String) n[3];
				if (cityNew != null && cityNew.length() > 0) {
					cityNew = cityNew.trim();
				} else {
					cityNew = "";
				}

				stjNew = (String) n[4];
				if (stjNew != null && stjNew.length() > 0) {
					stjNew = stjNew.trim();
				} else {
					stjNew = "";
				}

				hasNexus = (BigDecimal) n[5];
				lHasNexus = new Long(0);
				try {
					lHasNexus = hasNexus.longValue();
				} catch (Exception e) {
				}
				
				independentFlag = (String) n[7];
				if (independentFlag != null && independentFlag.length() > 0) {
					independentFlag = independentFlag.trim();
				} else {
					independentFlag = "";
				}

				if (!countyNew.equalsIgnoreCase(countyPrevious)) {
					// Add a new county
					countyItem = new JurisdictionItem(new Long(entityItemId++));
					countyItem.setParentEntityId(parentEntityId);
					countyItem.setItemType(JurisdictionItem.COUNTY_TYPE);
					countyItem.setCountry(country);
					countyItem.setState(state);
					
					if(independentFlag.equals("I")){
						countyItem.setIndepentdentFlag(true);
					}
					else{
						countyItem.setIndepentdentFlag(false);
					}
					
					countyItem.setDescription(countyNew);
					countyItem.setDisabledFlag(false);
					countyItem.setSelectedFlag(false);
					// Set no show 'Select All' first, reset later if needed
					countyItem.setIsAllFlag(false);
					
					if (countyItem.isItemType(JurisdictionItem.COUNTY_TYPE)){
						//county with index = 'I'
						if(independentFlag.equals("I") && lHasNexus.intValue() > 0){
							countyItem.setType(MAP_MAPPED);
						}
						
					}
					else if (lHasNexus.intValue() > 0) {
						countyItem.setType(MAP_MAPPED);
					}

					// Map tree node checked
					if (selectedJurisdictionItemMap.get(countyItem.getKey()) != null) {
						countyItem.setSelectedFlag(true);
					}

					globalJurisdictionArray.put(countyItem.getEntityId(),
							countyItem);

					nodeCounty = new TreeNodeImpl();
					nodeCounty.setData(countyItem);
					nodeState.addChild(countyItem.getEntityId(), nodeCounty);

					countyPrevious = countyNew;

					allCityItem = null;
					nodeAllCity = null;

					allStjItem = null;
					nodeAllStj = null;
				}

				if (cityNew.length() > 0) {
					if (allCityItem == null) {
						// Add AllCity
						allCityItem = new JurisdictionItem(new Long(
								entityItemId++));
						allCityItem.setParentEntityId(countyItem.getEntityId());
						allCityItem.setItemType(JurisdictionItem.CITY_TYPE);
						allCityItem.setCountry(country);
						allCityItem.setState(state);
						allCityItem.setIsAllFlag(true);
						allCityItem.setDescription("All Cities");
						allCityItem.setDisabledFlag(false);
						allCityItem.setSelectedFlag(false);

						globalJurisdictionArray.put(allCityItem.getEntityId(),
								allCityItem);

						nodeAllCity = new TreeNodeImpl();
						nodeAllCity.setData(allCityItem);
						nodeCounty.addChild(allCityItem.getEntityId(),
								nodeAllCity);
					}

					// Set 'Select All' if a child after county
					countyItem.setIsAllFlag(true);

					// Add City
					JurisdictionItem aCityItem = new JurisdictionItem(new Long(
							entityItemId++));
					aCityItem.setParentEntityId(allCityItem.getEntityId());
					aCityItem.setItemType(JurisdictionItem.CITY_TYPE);
					aCityItem.setCountry(country);
					aCityItem.setState(state);
					aCityItem.setIsAllFlag(false);
					aCityItem.setDescription(cityNew);
					aCityItem.setDisabledFlag(false);
					aCityItem.setSelectedFlag(false);
					aCityItem.setCountyLocated(countyLocated);
					if (lHasNexus.intValue() > 0) {
						aCityItem.setType(MAP_MAPPED);
					}

					// Map tree node checked
					if (selectedJurisdictionItemMap.get(aCityItem.getKey()) != null) {
						aCityItem.setSelectedFlag(true);
					}

					globalJurisdictionArray.put(aCityItem.getEntityId(),
							aCityItem);

					TreeNodeImpl nodeCity = new TreeNodeImpl();
					nodeCity.setData(aCityItem);
					nodeAllCity.addChild(aCityItem.getEntityId(), nodeCity);
				}

				if (stjNew.length() > 0) {
					if (allStjItem == null) {
						// Add AllSTJ
						allStjItem = new JurisdictionItem(new Long(
								entityItemId++));
						allStjItem.setParentEntityId(countyItem.getEntityId());
						allStjItem.setItemType(JurisdictionItem.STJ_TYPE);
						allStjItem.setCountry(country);
						allStjItem.setState(state);
						allStjItem.setIsAllFlag(true);
						allStjItem.setDescription("All STJs");

						globalJurisdictionArray.put(allStjItem.getEntityId(),
								allStjItem);

						nodeAllStj = new TreeNodeImpl();
						nodeAllStj.setData(allStjItem);
						nodeCounty.addChild(allStjItem.getEntityId(),
								nodeAllStj);
					}

					// Set 'Select All' if a child after county
					countyItem.setIsAllFlag(true);

					// Add STJ
					JurisdictionItem aStjItem = new JurisdictionItem(new Long(
							entityItemId++));
					aStjItem.setParentEntityId(allStjItem.getEntityId());
					aStjItem.setItemType(JurisdictionItem.STJ_TYPE);
					aStjItem.setCountry(country);
					aStjItem.setState(state);
					aStjItem.setIsAllFlag(false);
					aStjItem.setDescription(stjNew);
					aStjItem.setDisabledFlag(false);
					aStjItem.setSelectedFlag(false);
					aStjItem.setCountyLocated(countyLocated);
					if (lHasNexus.intValue() > 0) {
						aStjItem.setType(MAP_MAPPED);
					}

					// Map tree node checked
					if (selectedJurisdictionItemMap.get(aStjItem.getKey()) != null) {
						aStjItem.setSelectedFlag(true);
					}

					globalJurisdictionArray.put(aStjItem.getEntityId(),
							aStjItem);

					TreeNodeImpl nodeStj = new TreeNodeImpl();
					nodeStj.setData(aStjItem);
					nodeAllStj.addChild(aStjItem.getEntityId(), nodeStj);
				}
			}
		}

	}
	
	private Map<String, JurisdictionItem> retrieveCountyCitySTJ(String country, String state, String county) {
		
		Map<String, JurisdictionItem> retrievedJurisdictionItemMap = new LinkedHashMap<String, JurisdictionItem>();
		
		// Loop county and then city/stj using tree's country/state
		List<Object[]> list = jurisdictionService.findJurisdictionForCountyCityStj(entityIdSearch, geocodeSearch, country, state, county, citySearch, zipSearch, stjSearch, "Vendor");
		String countyPrevious = "";

		String countyNew = "";
		String cityNew = "";
		String stjNew = "";
		String countyLocated = "";

		JurisdictionItem countyItem = null;

		if (list != null) {
			for (Object[] n : list) {

				countyNew = (String) n[2];
				if (countyNew != null && countyNew.length() > 0) {
					countyNew = countyNew.trim();
				} else {
					countyNew = "";
				}
				countyLocated = countyNew;

				cityNew = (String) n[3];
				if (cityNew != null && cityNew.length() > 0) {
					cityNew = cityNew.trim();
				} else {
					cityNew = "";
				}

				stjNew = (String) n[4];
				if (stjNew != null && stjNew.length() > 0) {
					stjNew = stjNew.trim();
				} else {
					stjNew = "";
				}

				if (!countyNew.equalsIgnoreCase(countyPrevious)) {
					// Add a new county
					countyItem = new JurisdictionItem();
					countyItem.setItemType(JurisdictionItem.COUNTY_TYPE);
					countyItem.setCountry(country);
					countyItem.setState(state);
					countyItem.setDescription(countyNew);

					retrievedJurisdictionItemMap.put(countyItem.getKey(), countyItem);

					countyPrevious = countyNew;
				}

				if (cityNew.length() > 0) {
					// Add City
					JurisdictionItem aCityItem = new JurisdictionItem();
					aCityItem.setItemType(JurisdictionItem.CITY_TYPE);
					aCityItem.setCountry(country);
					aCityItem.setState(state);
					aCityItem.setDescription(cityNew);

					aCityItem.setCountyLocated(countyLocated);
					
					retrievedJurisdictionItemMap.put(aCityItem.getKey(), aCityItem);
				}

				if (stjNew.length() > 0) {
					// Add STJ
					JurisdictionItem aStjItem = new JurisdictionItem();
					aStjItem.setItemType(JurisdictionItem.STJ_TYPE);
					aStjItem.setCountry(country);
					aStjItem.setState(state);
					
					aStjItem.setDescription(stjNew);
					aStjItem.setCountyLocated(countyLocated);
					
					retrievedJurisdictionItemMap.put(aStjItem.getKey(), aStjItem);
				}
			}
		}
		
		return retrievedJurisdictionItemMap;
	}

	private void buildCountyCitySTJ(JurisdictionItem parentVendorItem,
			TreeNode nodeState) {

		String country = parentVendorItem.getCountry();
		String state = parentVendorItem.getState();
		Long parentEntityId = parentVendorItem.getEntityId();
		boolean isSelected = parentVendorItem.getSelectedFlag();

		// Add bogus All Counties, All Cities, or All STJs if any

		List<Object[]> list = null;

		// Add bogus All Counties if any
		list = jurisdictionService.findNexusJurisdiction(new Long(0), country,
				state, "I", "county");
		List<NexusJurisdictionItemDTO> countyList = buildList(list);

		Map<String, Long> aCountryStateJurisdictionMap = getCountryStateJurisdictionMap();
		Long lHasNexus = null;

		if (countyList != null && countyList.size() > 0) {
			JurisdictionItem countyItem = new JurisdictionItem(new Long(
					entityItemId++));
			countyItem.setParentEntityId(parentEntityId);
			countyItem.setItemType(JurisdictionItem.COUNTY_TYPE);
			countyItem.setCountry(country);
			countyItem.setState(state);
			countyItem.setIsAllFlag(true);
			countyItem.setDescription("All Counties");
			countyItem.setDisabledFlag(true);
			if (parentVendorItem.getType().equalsIgnoreCase(MAP_MAPPED)
					|| parentVendorItem.getType().equalsIgnoreCase(
							MAP_MAPPED_SELECTED)) {
				countyItem.setDisabledFlag(false);
			}

			if (isSelected) {
				countyItem.setDisabledFlag(false);
				countyItem.setSelectedFlag(false);
			}

			globalJurisdictionArray.put(countyItem.getEntityId(), countyItem);

			TreeNodeImpl nodeCounty = new TreeNodeImpl();
			nodeCounty.setData(countyItem);
			nodeState.addChild(countyItem.getEntityId(), nodeCounty);

			for (int i = 0; i < countyList.size(); i++) {
				NexusJurisdictionItemDTO aNexusJurisdictionItemDTO = (NexusJurisdictionItemDTO) countyList
						.get(i);

				JurisdictionItem aCountyItem = new JurisdictionItem(new Long(
						entityItemId++));
				aCountyItem.setParentEntityId(countyItem.getEntityId());
				aCountyItem.setParentEntityId(parentEntityId);

				aCountyItem.setItemType(JurisdictionItem.COUNTY_TYPE);
				aCountyItem.setCountry(country);
				aCountyItem.setState(state);
				aCountyItem.setDescription(aNexusJurisdictionItemDTO.getName());
				aCountyItem.setDisabledFlag(true);
				if (parentVendorItem.getType().equalsIgnoreCase(MAP_MAPPED)
						|| parentVendorItem.getType().equalsIgnoreCase(
								MAP_MAPPED_SELECTED)) {
					aCountyItem.setDisabledFlag(false);
				}

				lHasNexus = aCountryStateJurisdictionMap.get(country + state
						+ aNexusJurisdictionItemDTO.getName() + "*COUNTY"
						+ "*COUNTY");
				if (lHasNexus != null && lHasNexus.intValue() > 0) {
					aCountyItem.setType(MAP_MAPPED);
					aCountyItem.setDisabledFlag(false);
				}

				if (isSelected) {
					aCountyItem.setDisabledFlag(false);
					aCountyItem.setSelectedFlag(false);
				}

				globalJurisdictionArray.put(aCountyItem.getEntityId(),
						aCountyItem);

				TreeNodeImpl aNodeCounty = new TreeNodeImpl();
				aNodeCounty.setData(aCountyItem);
				nodeCounty.addChild(aCountyItem.getEntityId(), aNodeCounty);
			}
		}

		// Add bogus All Cities if any

		list = jurisdictionService.findNexusJurisdiction(new Long(0), country,
				state, "I", "city");
		List<NexusJurisdictionItemDTO> cityList = buildList(list);
		if (cityList != null && cityList.size() > 0) {
			JurisdictionItem cityItem = new JurisdictionItem(new Long(
					entityItemId++));
			cityItem.setParentEntityId(parentEntityId);
			cityItem.setItemType(JurisdictionItem.CITY_TYPE);
			cityItem.setCountry(country);
			cityItem.setState(state);
			cityItem.setIsAllFlag(true);
			cityItem.setDescription("All Cities");
			cityItem.setDisabledFlag(true);
			if (parentVendorItem.getType().equalsIgnoreCase(MAP_MAPPED)
					|| parentVendorItem.getType().equalsIgnoreCase(
							MAP_MAPPED_SELECTED)) {
				cityItem.setDisabledFlag(false);
			}

			if (isSelected) {
				cityItem.setDisabledFlag(false);
				cityItem.setSelectedFlag(false);
			}

			globalJurisdictionArray.put(cityItem.getEntityId(), cityItem);

			TreeNodeImpl nodeCity = new TreeNodeImpl();
			nodeCity.setData(cityItem);
			nodeState.addChild(cityItem.getEntityId(), nodeCity);

			for (int i = 0; i < cityList.size(); i++) {
				NexusJurisdictionItemDTO aNexusJurisdictionItemDTO = (NexusJurisdictionItemDTO) cityList
						.get(i);

				JurisdictionItem aCityItem = new JurisdictionItem(new Long(
						entityItemId++));
				aCityItem.setParentEntityId(cityItem.getEntityId());
				aCityItem.setItemType(JurisdictionItem.CITY_TYPE);
				aCityItem.setCountry(country);
				aCityItem.setState(state);
				aCityItem.setDescription(aNexusJurisdictionItemDTO.getName());
				aCityItem.setDisabledFlag(true);
				if (parentVendorItem.getType().equalsIgnoreCase(MAP_MAPPED)
						|| parentVendorItem.getType().equalsIgnoreCase(
								MAP_MAPPED_SELECTED)) {
					aCityItem.setDisabledFlag(false);
				}
				lHasNexus = aCountryStateJurisdictionMap.get(country + state
						+ "*CITY" + aNexusJurisdictionItemDTO.getName()
						+ "*CITY");
				if (lHasNexus != null && lHasNexus.intValue() > 0) {
					aCityItem.setType(MAP_MAPPED);
					aCityItem.setDisabledFlag(false);
				}

				if (isSelected) {
					aCityItem.setDisabledFlag(false);
					aCityItem.setSelectedFlag(false);
				}

				globalJurisdictionArray.put(aCityItem.getEntityId(), aCityItem);

				TreeNodeImpl aNodeCity = new TreeNodeImpl();
				aNodeCity.setData(aCityItem);
				nodeCity.addChild(aCityItem.getEntityId(), aNodeCity);
			}
		}

		// Add bogus All STJs if any
		list = jurisdictionService.findNexusJurisdiction(new Long(0), country,
				state, "I", "stj");
		List<NexusJurisdictionItemDTO> stjList = buildList(list);
		if (stjList != null && stjList.size() > 0) {
			JurisdictionItem stjItem = new JurisdictionItem(new Long(
					entityItemId++));
			stjItem.setParentEntityId(parentEntityId);
			stjItem.setItemType(JurisdictionItem.STJ_TYPE);
			stjItem.setCountry(country);
			stjItem.setState(state);
			stjItem.setIsAllFlag(true);
			stjItem.setDescription("All STJs");
			stjItem.setDisabledFlag(true);
			if (parentVendorItem.getType().equalsIgnoreCase(MAP_MAPPED)
					|| parentVendorItem.getType().equalsIgnoreCase(
							MAP_MAPPED_SELECTED)) {
				stjItem.setDisabledFlag(false);
			}

			if (isSelected) {
				stjItem.setDisabledFlag(false);
				stjItem.setSelectedFlag(false);
			}

			globalJurisdictionArray.put(stjItem.getEntityId(), stjItem);

			TreeNodeImpl nodeSTJ = new TreeNodeImpl();
			nodeSTJ.setData(stjItem);
			nodeState.addChild(stjItem.getEntityId(), nodeSTJ);

			for (int i = 0; i < stjList.size(); i++) {
				NexusJurisdictionItemDTO aNexusJurisdictionItemDTO = (NexusJurisdictionItemDTO) stjList
						.get(i);

				JurisdictionItem aSTJItem = new JurisdictionItem(new Long(
						entityItemId++));
				aSTJItem.setParentEntityId(stjItem.getEntityId());
				aSTJItem.setItemType(JurisdictionItem.STJ_TYPE);
				aSTJItem.setCountry(country);
				aSTJItem.setState(state);
				aSTJItem.setDescription(aNexusJurisdictionItemDTO.getName());
				aSTJItem.setDisabledFlag(true);
				if (parentVendorItem.getType().equalsIgnoreCase(MAP_MAPPED)
						|| parentVendorItem.getType().equalsIgnoreCase(
								MAP_MAPPED_SELECTED)) {
					aSTJItem.setDisabledFlag(false);
				}
				lHasNexus = aCountryStateJurisdictionMap
						.get(country + state + "*STJ" + "*STJ"
								+ aNexusJurisdictionItemDTO.getName());
				if (lHasNexus != null && lHasNexus.intValue() > 0) {
					aSTJItem.setType(MAP_MAPPED);
					aSTJItem.setDisabledFlag(false);
				}

				if (isSelected) {
					aSTJItem.setDisabledFlag(false);
					aSTJItem.setSelectedFlag(false);
				}

				globalJurisdictionArray.put(aSTJItem.getEntityId(), aSTJItem);

				TreeNodeImpl aNodeSTJ = new TreeNodeImpl();
				aNodeSTJ.setData(aSTJItem);
				nodeSTJ.addChild(aSTJItem.getEntityId(), aNodeSTJ);
			}
		}
	}

	public void setSelectedVendorItem(JurisdictionItem selectedVendorItem) {
		this.selectedVendorItem = selectedVendorItem;
		this.selectedRowKey = null;
	}

	public JurisdictionItem getSelectedVendorItem() {
		return this.selectedVendorItem;
	}

	public VendorItem findVendorItemById(TreeNode parent, Long ID) {
		VendorItem entityItem = null;
		Iterator<Map.Entry<Object, TreeNode>> it = parent.getChildren();
		while (it != null && it.hasNext()) {
			Map.Entry<Object, TreeNode> entry = it.next();

			if (ID.equals(entry.getKey())) {
				entityItem = (VendorItem) entry.getValue().getData();
				break;
			}

			entityItem = findVendorItemById(entry.getValue(), ID);
			if (entityItem != null) {
				break;
			}
		}

		return entityItem;
	}

	public String getSelectedRowKey() {
		return selectedRowKey;
	}

	private void loadEntity() {
	//	List<VendorItem> eList = vendorItemService.findAllVendorItems();// source
																		// list
		if (globalEntityArray != null) {
			globalEntityArray.clear();
		}
		globalEntityArray = new LinkedHashMap<Long, VendorItem>();

	//	if (eList != null && eList.size() > 0) {
	//		for (VendorItem entityItem : eList) {
	//			entityItem.setType(MAP_NONE);// Map first, later unmap.
	//			globalEntityArray.put(entityItem.getEntityId(), entityItem);
	//		}
	//	}

		selectedVendorItem = null;
		selectedRowKey = null;
	}

	public TreeNode getTreeNode() {
		if (rootNode == null) {
			loadTree();

			treeBinding.setValue(rootNode);

			/*
			 * try { treeBinding.queueCollapseAll(); } catch (final Exception
			 * ex) { ex.printStackTrace(); }
			 */

			try {
				final TreeState state = (TreeState) treeBinding
						.getComponentState();
				treeBinding.walk(FacesContext.getCurrentInstance(),
						new DataVisitor() {
							@SuppressWarnings("unchecked")
							public void process(FacesContext context,
									Object rowKey, Object argument)
									throws IOException {
								TreeNode treeNode = treeBinding
										.getModelTreeNode(rowKey);
								if (treeNode != null) {
									JurisdictionItem entityItem = (JurisdictionItem) treeNode
											.getData();
									if (entityItem
											.isItemType(JurisdictionItem.COUNTRY_TYPE)) {
										// Expand Country node
										state.expandNode(treeBinding,
												(TreeRowKey<Object>) rowKey);
									} else if (entityItem
											.isItemType(JurisdictionItem.STATE_TYPE)) {
										// Collapse State node
										state.collapseNode(treeBinding,
												(TreeRowKey<Object>) rowKey);

									}
								}
							}
						});
			} catch (IOException er) {
				er.printStackTrace();
			}

		}

		return rootNode;
	}

	public Map<Long, String> getEntityLevelMap() {
		if (entityLevelMap == null) {
			entityLevelMap = new LinkedHashMap<Long, String>();

			List<EntityLevel> aList = entityLevelService.findAllEntityLevels();
			for (EntityLevel entityLevel : aList) {
				entityLevelMap.put(entityLevel.getEntityLevelId(),
						entityLevel.getDescription());
			}
		}
		return entityLevelMap;
	}

	public String getUpdateVendorItemLabel() {
		if (updateVendorItem != null) {
			return "Vendor : " + updateVendorItem.getVendorCode() + " - "
					+ updateVendorItem.getVendorName();
		} else {
			return "EntityLevel: ";
		}
	}

	public void setSelectedVendorItemFromDefinition(VendorItem updateVendorItem) {

		this.updateVendorItem = updateVendorItem;

		rootNode = null;
		treeBinding = null;

		vendorNexusDtlList = null;
		registrationDetailList = null;

		// Remove checked jurisdiction from memory
		if (selectedJurisdictionItemMap != null) {
			selectedJurisdictionItemMap.clear();
		}
		selectedJurisdictionItemMap = new LinkedHashMap<String, JurisdictionItem>();
	}

	public boolean getAddAction() {
		return currentAction.equals(EditAction.ADD);
	}

	public boolean getDeleteAction() {
		return currentAction.equals(EditAction.DELETE);
	}

	public boolean getDisableActiveFlag() {
		return false;
	}

	//public boolean getIsEntitlLevel3() {
	//	if (entityItemDTO != null && entityItemDTO.getEntityLevelId() != null
	//			&& !entityItemDTO.getEntityLevelId().equals(3L)) {
	//		return false;
	//	} else {
	//		return true;
	//	}
	//}

	public boolean getUpdateAction() {
		return currentAction.equals(EditAction.UPDATE);
	}

	public boolean getCopyAddAction() {
		return currentAction.equals(EditAction.COPY_ADD);
	}

	public String getActionText() {
		if (currentAction.equals(EditAction.COPY_ADD)) {
			return "Copy/Add";
		} else if (currentAction.equals(EditAction.UPDATE)) {
			return "Update";
		} else if (currentAction.equals(EditAction.ADD)) {
			return "Add";
		} else if (currentAction.equals(EditAction.DELETE)) {
			return "Delete";
		} else {
			return "";
		}
	}

//	public boolean getSelectedEntityLocked() {
//		if (selectedGridVendorItem != null
//				&& selectedGridVendorItem.getLockBooleanFlag() == false) {
//			return false;
//		} else {
//			return true;
//		}
//	}

	public boolean getDisableCopyTo() {
		if (selectedGridVendorItem != null) {
			return false;
		} else {
			return true;
		}
	}

	public boolean getDisableDefaultUpdate() {
		if (selectedEntityDefault != null) {
			return false;
		} else {
			return true;
		}
	}

	public boolean getDisableDefaultDelete() {
		if (selectedEntityDefault != null) {
			return false;
		} else {
			return true;
		}
	}

	public boolean getDisableLocationUpdate() {
		if (selectedEntityLocnSet != null) {
			return false;
		} else {
			return true;
		}
	}

	public boolean getDisableLocationDtlAdd() {
		if (selectedEntityLocnSet != null) {
			return false;
		} else {
			return true;
		}
	}

	public boolean getDisableLocationDtlUpdate() {
		if (selectedEntityLocnSetDtl != null) {
			return false;
		} else {
			return true;
		}
	}

	public String displayRegistrationAction() {
		if (selectedVendorItem == null || selectedGridVendorItem == null) {
			return "";
		}

	//	nexusDefinitionBackingBean.setSelectedVendorItemFromRegistration(selectedGridVendorItem);

		return "nexus_def_states";
	}

	public VendorItem getUpdateVendorItem() {
		return updateVendorItem;
	}

	public String displayDefinitionAction() {
		if (selectedVendorItem == null || selectedGridVendorItem == null) {
			return "";
		}

	//	nexusDefinitionBackingBean.setSelectedVendorItemFromDefinition(selectedGridVendorItem);

		return "nexus_def_states";
	}

	public List<SelectItem> createValueList(String code) {
		List<SelectItem> selectItems = new ArrayList<SelectItem>();

		if (code != null && code.length() > 0) {
			selectItems.add(new SelectItem("", "Select a Value"));
			List<ListCodes> listCodes = cacheManager.getListCodesByType(code);
			if (listCodes != null) {
				for (ListCodes lc : listCodes) {
					selectItems.add(new SelectItem(lc.getCodeCode(), lc
							.getDescription()));
				}
			}
		}

		return selectItems;
	}

	public List<SelectItem> createCodeList(Map<String, String> mapExcluded) {
		List<SelectItem> selectItems = new ArrayList<SelectItem>();
		selectItems.add(new SelectItem("", "Select a Default Code"));
		List<ListCodes> listCodes = cacheManager.getListCodesByType("DEFCODES");
		if (listCodes != null) {
			for (ListCodes lc : listCodes) {
				String code = (String) mapExcluded.get(lc.getCodeCode());
				if (code == null || code.length() == 0) {
					selectItems.add(new SelectItem(lc.getCodeCode(), lc
							.getDescription()));
				}
			}
		}
		return selectItems;
	}

	public void setSelectedGridVendorItem(VendorItem selectedGridVendorItem) {
		this.selectedGridVendorItem = selectedGridVendorItem;
	}

	public VendorItem getSelectedGridVendorItem() {
		return this.selectedGridVendorItem;
	}

	public void SetSelectedEntityDefault(EntityDefault selectedEntityDefault) {
		this.selectedEntityDefault = selectedEntityDefault;
	}

	public EntityDefault getSelectedEntityDefault() {
		return this.selectedEntityDefault;
	}

	public void SetSelectedEntityLocnSet(EntityLocnSet selectedEntityLocnSet) {
		this.selectedEntityLocnSet = selectedEntityLocnSet;
	}

	public EntityLocnSet getSelectedEntityLocnSet() {
		return this.selectedEntityLocnSet;
	}

	public void SetSelectedEntityLocnSetDtl(
			EntityLocnSetDtl selectedEntityLocnSetDtl) {
		this.selectedEntityLocnSetDtl = selectedEntityLocnSetDtl;
	}

	public EntityLocnSetDtl getSelectedEntityLocnSetDtl() {
		return this.selectedEntityLocnSetDtl;
	}

	public UserSecurityBean getUserSecurityBean() {
		return userSecurityBean;
	}

	public void setUserSecurityBean(UserSecurityBean userSecurityBean) {
		this.userSecurityBean = userSecurityBean;
	}

	public int getSelectedDefinitionRowIndex() {
		return selectedDefinitionRowIndex;
	}

	public void setSelectedDefinitionRowIndex(int selectedDefinitionRowIndex) {
		this.selectedDefinitionRowIndex = selectedDefinitionRowIndex;
	}

	public int getSelectedRegistrationRowIndex() {
		return selectedRegistrationRowIndex;
	}

	public void setSelectedRegistrationRowIndex(int selectedRegistrationRowIndex) {
		this.selectedRegistrationRowIndex = selectedRegistrationRowIndex;
	}

	public int getSelectedEntityDefaultRowIndex() {
		return selectedEntityDefaultRowIndex;
	}

	public void setSelectedEntityDefaultRowIndex(
			int selectedEntityDefaultRowIndex) {
		this.selectedEntityDefaultRowIndex = selectedEntityDefaultRowIndex;
	}

	public int getSelectedLocationListRowIndex() {
		return selectedLocationListRowIndex;
	}

	public void setSelectedLocationListRowIndex(int selectedLocationListRowIndex) {
		this.selectedLocationListRowIndex = selectedLocationListRowIndex;
	}

	public int getSelectedLocationListDetailRowIndex() {
		return selectedLocationListDetailRowIndex;
	}

	public void setSelectedLocationListDetailRowIndex(
			int selectedLocationListDetailRowIndex) {
		this.selectedLocationListDetailRowIndex = selectedLocationListDetailRowIndex;
	}

	public boolean isShowUpdateButton() {
		return showUpdateButton;
	}

	public void setShowUpdateButton(boolean showUpdateButton) {
		this.showUpdateButton = showUpdateButton;
	}

	public boolean isDisableDeleteButton() {
		return disableDeleteButton;
	}

	public void setDisableDeleteButton(boolean disableDeleteButton) {
		this.disableDeleteButton = disableDeleteButton;
	}

	public boolean isUpdateDisplay() {
		return updateDisplay;
	}

	public void setUpdateDisplay(boolean updateDisplay) {
		this.updateDisplay = updateDisplay;
	}

	public boolean isDisplayAddButton() {
		return displayAddButton;
	}

	public void setDisplayAddButton(boolean displayAddButton) {
		this.displayAddButton = displayAddButton;
	}

	public boolean isDisplayUpdateButton() {
		return displayUpdateButton;
	}

	public void setDisplayUpdateButton(boolean displayUpdateButton) {
		this.displayUpdateButton = displayUpdateButton;
	}

	public boolean isTransColDisplay() {
		return transColDisplay;
	}

	public void setTransColDisplay(boolean transColDisplay) {
		this.transColDisplay = transColDisplay;
	}

	public DataDefinitionService getDataDefinitionService() {
		return dataDefinitionService;
	}

	public void setDataDefinitionService(
			DataDefinitionService dataDefinitionService) {
		this.dataDefinitionService = dataDefinitionService;
	}

	public CacheManager getCacheManager() {
		return cacheManager;
	}

	public void setCacheManager(CacheManager cacheManager) {
		this.cacheManager = cacheManager;

		filterHandlerShipto.setCacheManager(getCacheManager());
	}

	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	public RoleService getRoleService() {
		return roleService;
	}

	public void setRoleService(RoleService roleService) {
		this.roleService = roleService;
	}

	public VendorItemService getVendorItemService() {
		return vendorItemService;
	}

	public void setVendorItemService(VendorItemService vendorItemService) {
		this.vendorItemService = vendorItemService;
	}

	public EntityDefaultService getEntityDefaultService() {
		return entityDefaultService;
	}

	public void setEntityDefaultService(
			EntityDefaultService entityDefaultService) {
		this.entityDefaultService = entityDefaultService;
	}

	public EntityLocnSetService getEntityLocnSetService() {
		return entityLocnSetService;
	}

	public void setEntityLocnSetService(
			EntityLocnSetService entityLocnSetService) {
		this.entityLocnSetService = entityLocnSetService;
	}

	public EntityLocnSetDtlService getEntityLocnSetDtlService() {
		return entityLocnSetDtlService;
	}

	public void setEntityLocnSetDtlService(
			EntityLocnSetDtlService entityLocnSetDtlService) {
		this.entityLocnSetDtlService = entityLocnSetDtlService;
	}

	public EntityLevelService getEntityLevelService() {
		return entityLevelService;
	}

	public void setEntityLevelService(EntityLevelService entityLevelService) {
		this.entityLevelService = entityLevelService;
	}

	public UserEntityService getUserEntityService() {
		return userEntityService;
	}

	public void setUserEntityService(UserEntityService userEntityService) {
		this.userEntityService = userEntityService;
	}

	Calendar calendar = Calendar.getInstance();

	private boolean noAdd = false;

	private int selectedFilterIndex;

	public boolean isNoAdd() {
		return noAdd;
	}

	public void setNoAdd(boolean noAdd) {
		this.noAdd = noAdd;
	}

	public int getSelectedFilterIndex() {
		return selectedFilterIndex;
	}

	public void setSelectedFilterIndex(int selectedFilterIndex) {
		this.selectedFilterIndex = selectedFilterIndex;
	}

	public String getRerender() {
		return rerender;
	}

	public void setRerender(String rerender) {
		this.rerender = rerender;
	}

	public VendorItem getSelectedEntity() {
		return selectedEntity;
	}

	public void setSelectedEntity(VendorItem selectedEntity) {
		this.selectedEntity = selectedEntity;
	}

	public String getMode() {
		return this.mode;
	}

	public void setMode(String mode) {
		this.mode = mode;
	}

	public EntityLevel getSelectedEntityLevel() {
		return this.selectedEntityLevel;
	}

	public void setSelectedEntityLevel(EntityLevel entityLevel) {
		this.selectedEntityLevel = entityLevel;
	}

	public UserEntity getSelectedUserEntity() {
		return this.selectedUserEntity;
	}

	public void setSelectedUserEntity(UserEntity userEntity) {
		this.selectedUserEntity = userEntity;
	}

	public VendorItemDTO getVendorItemDTO() {
		return entityItemDTO;
	}

	public EntityDefaultDTO getEntityDefaultDTO() {
		return entityDefaultDTO;
	}

	public EntityLocnSetDtlDTO getEntityLocnSetDtlDTO() {
		return entityLocnSetDtlDTO;
	}

	public EntityLocnSetDTO getEntityLocnSetDTO() {
		return entityLocnSetDTO;
	}

	public ListCodesService getListCodesService() {
		return listCodesService;
	}

	public void setListCodesService(ListCodesService listCodesService) {
		this.listCodesService = listCodesService;
	}

	public void validateUserCode(FacesContext context, UIComponent toValidate,
			Object value) {
		// Ensure code is unique
		String code = (String) value;
		if (currentAction.isAddAction() && (userService.findById(code) != null)) {
			((UIInput) toValidate).setValid(false);
			FacesMessage message = new FacesMessage("User code already exists.");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage(toValidate.getClientId(context), message);
		}
	}

	public boolean validateEntity(String code, String name) {
		// Ensure code is unique and not empty
		logger.debug("code : " + code + "name : " + name);
		if (name == null || code == null || name.length() <= 0
				|| code.length() <= 0) {
			FacesMessage message = new FacesMessage(
					"Entity code and Entity name cannot be empty.");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
			return false;
		} else if (currentAction.isAddAction()
				&& (vendorItemService.findByCode(code) != null)) {
			FacesMessage message = new FacesMessage(
					"Entity code already exists.");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
			return false;
		}
		return true;
	}

	public Long getBusinessUnitEntityLevelId() {
		Map<String, DriverNames> transactionDriverNamesMap = cacheManager
				.getTransactionColumnDriverNamesMap(TaxMatrix.DRIVER_CODE);

		List<EntityLevel> entityLevelList = entityLevelService
				.findAllEntityLevels();
		Long tempId = 0l;
		DriverNames driverNames = null;
		if (!entityLevelList.isEmpty()) {
			for (EntityLevel entityLevel : entityLevelList) {
				driverNames = transactionDriverNamesMap.get(entityLevel
						.getTransDetailColumnName());
				if (driverNames != null
						&& (driverNames.getBusinessUnitFlag() != null)
						&& driverNames.getBusinessUnitFlag().equals("1")) {
					tempId = entityLevel.getEntityLevelId();
					break;
				}
			}
		}

		return tempId;
	}

	public boolean isShowOkButton() {
		return showOkButton;
	}

	public void setShowOkButton(boolean showOkButton) {
		this.showOkButton = showOkButton;
	}

	public void setUserConfig(String file) {
		if (this.userConfig == null) {
			try {
				this.userConfig = new ConfigSetting(file);
			} catch (IOException e) {
				logger.error(e);
			}
		}
	}

	public loginBean getLoginBean() {
		return loginBean;
	}

	public void setLoginBean(loginBean loginBean) {
		this.loginBean = loginBean;
	}
}
