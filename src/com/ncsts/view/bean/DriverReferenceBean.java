package com.ncsts.view.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;

import org.ajax4jsf.component.UIDataAdaptor;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.ncsts.common.CacheManager;
import com.ncsts.common.LoggerFactory;
import com.ncsts.domain.DataDefinitionColumn;
import com.ncsts.domain.DriverNames;
import com.ncsts.domain.DriverReference;
import com.ncsts.domain.EntityLevel;
import com.ncsts.domain.ListCodes;
import com.ncsts.domain.User;
import com.ncsts.dto.DriverNamesDTO;
import com.ncsts.dto.DriverReferenceDTO;
import com.ncsts.services.DataDefinitionService;
import com.ncsts.services.DriverReferenceService;
import com.ncsts.services.EntityLevelService;
//import com.ncsts.services.ListCodesService;

/**
 * 
 * This is the backing bean for the maintainDriverReference.jsp page.
 * 
 */
public class DriverReferenceBean implements Serializable {
	public enum DriverReferenceAction {
		addAction("Add a Driver Reference Value"),
		updateAction("Update a Driver Reference Value"),
		viewAction("View a Driver Reference Value"),
		deleteAction("Delete a Driver Reference Value");
		
		private String actionText;

		DriverReferenceAction(String actionText) {
			this.actionText = actionText;
		}

		public String getActionText() {
			return actionText;
		}
	}
    
	protected DataDefinitionService dataDefinitionService;
	protected DriverReferenceService driverService;
	protected EntityLevelService entityLevelService;
//	protected ListCodesService listCodesService;
	/**
	 * 
	 */
	private static final long serialVersionUID = -3075553018257650775L;

	private Logger logger = LoggerFactory.getInstance().getLogger(DriverReferenceBean.class);
	
	private List<SelectItem> categoriesMenuItems;
	private List<DriverNamesDTO> driverNames = new ArrayList<DriverNamesDTO>();
	private List<DriverReferenceDTO> driverReferences = new ArrayList<DriverReferenceDTO>();
	private String selectedCategory = "";
	// private FacesContext fcontext = null;
	// private Application app = null;
	private DriverNamesDTO selectedDriverName;
	private DriverReferenceDTO selectedReferenceName;
	private DriverReferenceDTO editReferenceName;
	private DriverReferenceAction currentAction;
	
	private Map<String, String> transDtlColNameToDescriptionMap;
	
	private int selectedNameIndex = -1;
	private int selectedReferenceIndex = -1;
	
	private CacheManager cacheManager;
	
	@Autowired
	private loginBean loginBean;


	public DriverReferenceBean() {
		// fcontext = FacesContext.getCurrentInstance();
		// app = fcontext.getApplication();
	}

	public void onCategoryChange(ValueChangeEvent eventValue) {
		logger.debug("Caught value change from category drop down");
		String newValue = (String) eventValue.getNewValue();
		String oldValue = (String) eventValue.getOldValue();
		selectedCategory = newValue;
		if (StringUtils.isNotEmpty(newValue) && !newValue.equals(oldValue)) {
			populateDriverNames(newValue);
		} else if (StringUtils.isEmpty(newValue)) {
			driverNames = new ArrayList<DriverNamesDTO>();
		}

		// Clear out driver references
		driverReferences = new ArrayList<DriverReferenceDTO>();
		
		selectedNameIndex = -1;
		selectedReferenceIndex = -1;
	}

	// TODO : Add "description" to DTO from TB_ENTITY_LEVEL
	private void populateDriverNames(String driverCode) {
		if (driverService != null) {
			List<DriverNames> dn = driverService.getDriverNamesByCode(driverCode);
			if (dn != null && !dn.isEmpty()) {
				driverNames = new ArrayList<DriverNamesDTO>();
				for (DriverNames driverName : dn) {
					String columnName = driverName.getTransDtlColName();
					List<EntityLevel> entityLevels = entityLevelService.findByTransDtlColName(columnName);
					DriverNamesDTO dto = new DriverNamesDTO();
					if(entityLevels != null && !entityLevels.isEmpty()) {
						dto.setDescription(entityLevels.get(0).getDescription());
					}
					try {
						BeanUtils.copyProperties(dto, driverName);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					driverNames.add(dto);
				}
			} else {
				driverNames = new ArrayList<DriverNamesDTO>();
			}
		}

		// Empty driver references
		driverReferences = new ArrayList<DriverReferenceDTO>();

		// Reset selections
		selectedDriverName = null;
		selectedReferenceName = null;
	}

	public List<SelectItem> getCategoriesMenuItems() {
		//if (categoriesMenuItems == null) {
			categoriesMenuItems = new ArrayList<SelectItem>();
			categoriesMenuItems.add(new SelectItem("", "Select"));
			//if (listCodesService != null) {
				//List<ListCodes> listCodesList = listCodesService.getListCodesByCodeTypeCode("MDTYPE");
				List<ListCodes> listCodesList = cacheManager.getListCodesByType("MDTYPE");
				for (ListCodes listCodes : listCodesList) {
					if (listCodes != null && !StringUtils.isEmpty(listCodes.getCodeCode()) && !StringUtils.isEmpty(listCodes.getDescription())) {
						categoriesMenuItems.add(new SelectItem(listCodes.getCodeCode(), listCodes.getDescription()));
					}
				}
			//}
		//}
		
		return categoriesMenuItems;
	}
	 
	public int getSelectedNameIndex() {
		return this.selectedNameIndex;
	}

	public void setSelectedNameIndex(int selectedNameIndex) {
		this.selectedNameIndex = selectedNameIndex;
	}
	
	public int getSelectedReferenceIndex() {
		return this.selectedReferenceIndex;
	}

	public void setSelectedReferenceIndex(int selectedReferenceIndex) {
		this.selectedReferenceIndex = selectedReferenceIndex;
	}

	public void selectedNameRowChanged(ActionEvent e) throws AbortProcessingException {
		UIComponent uiComponent = e.getComponent().getParent();
		if (!(uiComponent instanceof UIDataAdaptor)) {
			logger.info("Invalid class to event listener: " + uiComponent.getClass().getName());
			
			return;
		}

		UIDataAdaptor table = (UIDataAdaptor) uiComponent;
		selectedNameIndex = table.getRowIndex();
		
		// Reset selection, if necessary
		if (table.getRowData() != null) {
			this.selectedDriverName = (DriverNamesDTO) table.getRowData();
        } else {
        	logger.debug("no selected driver name");
        }

        // Populate the references now that a driver name has been selected
        populateReferenceNames(selectedDriverName); 
	}

	public void selectedReferenceRowChanged(ActionEvent e) throws AbortProcessingException {
		UIComponent uiComponent = e.getComponent().getParent();
		if (!(uiComponent instanceof UIDataAdaptor)) {
			logger.info("Invalid class to event listener: " + uiComponent.getClass().getName());
			
			return;
		}

		UIDataAdaptor table = (UIDataAdaptor) uiComponent;
		selectedReferenceIndex = table.getRowIndex();
		if (table.getRowData() != null) {
			selectedReferenceName = (DriverReferenceDTO) table.getRowData();
		} else {
        	logger.debug("no selected driver reference");
		}
	}

	private void populateReferenceNames(DriverNamesDTO selectedDriverName) {
		List<DriverReference> references = 
			driverService.findByTransactionDetailColumnName(selectedDriverName.getTransDtlColName(), 100);
		driverReferences = new ArrayList<DriverReferenceDTO>();
		for (DriverReference dr : references) {
			driverReferences.add(dr.getDriverReferenceDTO());
		}

		// Reset selected reference name
		selectedReferenceName = null;
		selectedReferenceIndex = -1;
	}

	public String processActionCommand() {
		String command = (String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("command");
		logger.debug("command: " + command);
		
		try {
			currentAction = DriverReferenceAction.valueOf(command);
			logger.debug("currentAction: " + currentAction);
			switch (currentAction) {
			case addAction:
				if (selectedDriverName != null) {
					// Create a new DriverReferenceDTO with the same TransDtlColName
					editReferenceName = new DriverReferenceDTO();
					editReferenceName.setTransDtlColName(selectedDriverName.getTransDtlColName());
				}
				break;
			case updateAction:
			case deleteAction:
			case viewAction:
				if (selectedReferenceName == null) {
					throw new IllegalArgumentException("No Driver Reference selected");
				}
				editReferenceName = new DriverReferenceDTO(selectedReferenceName);
				break;
			}
			return "editDriverReference";
		} catch (Exception e) {
			logger.warn("Failed to process command: " + command);
		}

		return null;
	}
	
	public String cancelAction() {
	    return "driverReferenceModified";
	}

	public String processActionCommandResults() {
		String command = (String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("command");
		logger.debug("command: " + command);
		
		try {
			DriverReferenceAction action = DriverReferenceAction.valueOf(command);
			logger.debug("action: " + action);
			logger.debug("editReferenceName: " + editReferenceName);

			DriverReference dr = editReferenceName.getDriverReference();

			switch (action) {
			case addAction:
			case updateAction:
				dr.setUpdateUserId("STSCORP");
				dr.setUpdateTimestamp(new Date());
				driverService.saveOrUpdate(dr);
				break;
			case deleteAction:
				driverService.delete(dr);
				break;
			
			case viewAction: 
				return "driverReferenceModified";

			}
				// repopulate, since items may have been added or deleted
			populateReferenceNames(this.selectedDriverName);

			return "driverReferenceModified";
		} catch (Exception e) {
			logger.warn("Failed to process command: " + command + ", " + e);
		}

		return null;
	}

	public List<DriverNamesDTO> getDriverNames() {
		return driverNames;
	}

	public void setDriverNames(List<DriverNamesDTO> driverNames) {
		this.driverNames = driverNames;
	}

	public List<DriverReferenceDTO> getDriverReferences() {
		return driverReferences;
	}

	public void setDriverReferences(List<DriverReferenceDTO> driverReferences) {
		this.driverReferences = driverReferences;
	}

	public DataDefinitionService getDataDefinitionService() {
		return dataDefinitionService;
	}
	
	public void setDataDefinitionService(DataDefinitionService dataDefinitionService) {
		this.dataDefinitionService = dataDefinitionService;
		
		dataDefinitionServiceInit();
	}

	public DriverReferenceService getDriverService() {
		return driverService;
	}

	public void setDriverService(DriverReferenceService driverService) {
		this.driverService = driverService;
	}

	public String getSelectedCategory() {
		return selectedCategory;
	}

	public void setSelectedCategory(String selectedCategory) {
		this.selectedCategory = selectedCategory;
	}

	public DriverNamesDTO getSelectedDriverName() {
		return selectedDriverName;
	}

	public void setSelectedDriverName(DriverNamesDTO selectedDriverName) {
		this.selectedDriverName = selectedDriverName;
	}

	public DriverReferenceDTO getSelectedReferenceName() {
		return selectedReferenceName;
	}

	public void setSelectedReferenceName(DriverReferenceDTO selectedReferenceName) {
		this.selectedReferenceName = selectedReferenceName;
	}

	public EntityLevelService getEntityLevelService() {
		return entityLevelService;
	}

	public void setEntityLevelService(EntityLevelService entityLevelService) {
		this.entityLevelService = entityLevelService;
	}

	
	private void dataDefinitionServiceInit() {
		if (dataDefinitionService == null) {
			return;
		}
		
		transDtlColNameToDescriptionMap = getColumnNameToDescriptionMap("TB_PURCHTRANS");
	}
	
	private Map<String, String> getColumnNameToDescriptionMap(String tableName) {
		Map<String, String> map = new HashMap<String,String>();
		List<DataDefinitionColumn> dataDefinitionColumns = dataDefinitionService.getAllDataDefinitionColumnByTable(tableName);
		
		for (DataDefinitionColumn ddc : dataDefinitionColumns) {
			String columnName = ddc.getDataDefinitionColumnPK().getColumnName();
			String description = ddc.getDescription();
			if (StringUtils.isNotEmpty(description)) {
				map.put(columnName, description);
			}
		}
		
		return map;
	}
	
	public Map<String, String> getTransDtlColNameToDescriptionMap() {
		return transDtlColNameToDescriptionMap;
	}

	public Boolean getValidSelectedDriverName() {
		return (selectedDriverName != null);
	}

	public Boolean getValidSelectedReferenceName() {
		return (selectedReferenceName != null);
	}
	
	public String getCurrentAction() {
		return currentAction.toString();
	}

	public String getCurrentActionText() {
		return currentAction.getActionText();
	}
	
	public Boolean getIsCurrentActionAdd() {
		return (currentAction == DriverReferenceAction.addAction);
	}

	public Boolean getIsCurrentActionUpdate() {
		return (currentAction == DriverReferenceAction.updateAction);
	}

	public Boolean getIsCurrentActionDelete() {
		return (currentAction == DriverReferenceAction.deleteAction);
	}
	
	public Boolean getIsCurrentActionView() {
		return (currentAction == DriverReferenceAction.viewAction);
	}

	public DriverReferenceDTO getEditReferenceName() {
		return editReferenceName;
	}

	public void setEditReferenceName(DriverReferenceDTO editReferenceName) {
		this.editReferenceName = editReferenceName;
	}

	public CacheManager getCacheManager() {
		return cacheManager;
	}

	public void setCacheManager(CacheManager cacheManager) {
		this.cacheManager = cacheManager;
	}
	
	public User getCurrentUser() {
		return loginBean.getUser();
   } 
}
