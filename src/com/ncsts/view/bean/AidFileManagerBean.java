package com.ncsts.view.bean;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.component.html.HtmlSelectBooleanCheckbox;
import javax.faces.context.FacesContext;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.ajax4jsf.component.html.HtmlAjaxSupport;
import org.apache.myfaces.custom.fileupload.HtmlInputFileUpload;
import org.apache.myfaces.custom.fileupload.UploadedFile;
import org.richfaces.component.html.HtmlDataTable;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import com.ncsts.common.CacheManager;
import com.ncsts.common.Util;
import com.ncsts.domain.ListCodes;
import com.ncsts.domain.User;
import com.ncsts.dto.DbPropertiesDTO;
import com.ncsts.dto.ImportSpecDTO;
import com.ncsts.jsf.model.BaseExtendedDataModel;
import com.ncsts.management.DbPropertiesBean;
import com.ncsts.services.ImportDefAndSpecsService;
import com.ncsts.view.util.FacesUtils;
import com.ncsts.view.util.TogglePanelController;

public class AidFileManagerBean {
	private loginBean loginBean;
	private FilePreferenceBackingBean filePreferenceBean;
	private CacheManager cacheManager;
	private ImportDefAndSpecsService importDefAndSpecsService;
	private DbPropertiesBean dbPropertiesBean;
	public String aidRootFolder = "";
	public String selectedDataFolder = "";
	public String selectedSeparator = null;
	public static String IMPORTED_FOLDER = "Imported";
	public static String LOG_FOLDER = "Log";
	public static String SCRIPTS_FOLDER = "Scripts";
	public static String POSTIMPORT_FOLDER = "Post-Import";
	public static String PREIMPORT_FOLDER = "Pre-Import";
	private int selectedSpecRowIndex = -1;
	private int selectedFileRowIndex = -1;
	private List<SelectItem> specTypeList = null;
	private List<SelectItem> specList = null;
	private List<SelectItem> fileDisplayItems;
	private String selectedSpecType = "";
	private String selectedSpec = "";
	private String selectedDescription = "";
	private String selectedName = "";
	private Date selectedAsOfDate;
	private ImportSpecDTO selectedImportSpecDTO = null;
	private FileItem selectedFileItem = null;
	private List<ImportSpecDTO> importSpecList = new ArrayList<ImportSpecDTO>();
	private List<FileItem> fileItemList = new ArrayList<FileItem>();
	private Map<String,String> sortOrder;
	
	private static String QUEUED = "QUEUED";
	private static String IMPORTED = "IMPORTED";
	private static String POSTSCRIPT = "POSTSCRIPT";
	private static String PRESCRIPT = "PRESCRIPT";
	private String selectedFileDisplay = QUEUED;
	
	private String actionText;
	private String fileNameText;
	private String fileContent;
	private String filePrefix;
	
	private HtmlInputFileUpload uploadFile;
	private String uploadedFileName = null;
	private String uploadedFileSize = null;
	
	private int defaultRowNumber = 100;
	
	private TogglePanelController togglePanelController = null;
	
	public void init() {
		aidRootFolder = filePreferenceBean.getAidInstallation();
	}
	
	public User getCurrentUser() {
		return loginBean.getUser();
   }
	
	public  TogglePanelController getTogglePanelController(){
		if(togglePanelController==null){
			togglePanelController = new TogglePanelController();
			togglePanelController.addTogglePanel("AIDFileManagerSPecPanel", true);
			togglePanelController.addTogglePanel("AIDFileManagerFilePanel", false);
		}
		return togglePanelController;
	}
	
	public void setTogglePanelController(TogglePanelController togglePanelController){
		this.togglePanelController = togglePanelController;;
	}
	
	public String getUploadedFileName() {
		return uploadedFileName;
	}

	public void setUploadedFileName(String uploadedFileName) {
		this.uploadedFileName = uploadedFileName;
	}
	
	public String getUploadedFileSize() {
		return uploadedFileSize;
	}

	public void setUploadedFileSize(String uploadedFileSize) {
		this.uploadedFileSize = uploadedFileSize;
	}

	public HtmlInputFileUpload getUploadFile() {
		return uploadFile;
	}

	public void setUploadFile(HtmlInputFileUpload uploadFile) {
		this.uploadFile = uploadFile;
	}

	public String getFilePrefix() {
		return filePrefix;
	}
	
	public String getActionText() {
		return actionText;
	}
	
	public String getFileNameText() {
		return fileNameText;
	}
	
	public String getFileContent() {
		return fileContent;
	}
	
	public int getSelectedSpecRowIndex() {
		return selectedSpecRowIndex;
	}
	
	public void setSelectedSpecRowIndex(int selectedSpecRowIndex) {
		this.selectedSpecRowIndex = selectedSpecRowIndex;
	}
	
	public int getSelectedFileRowIndex() {
		return selectedFileRowIndex;
	}
	
	public void setSelectedFileRowIndex(int selectedFileRowIndex) {
		this.selectedFileRowIndex = selectedFileRowIndex;
	}
	
	public void setDbPropertiesBean(DbPropertiesBean dbPropertiesBean) {
		this.dbPropertiesBean = dbPropertiesBean;
	}
	
	public CacheManager getCacheManager() {
		return this.cacheManager;
	}
	
	public void setCacheManager(CacheManager cacheManager) {
		this.cacheManager = cacheManager;
	}
	
	public ImportDefAndSpecsService getImportDefAndSpecsService() {
		return importDefAndSpecsService;
	}

	public void setImportDefAndSpecsService(
			ImportDefAndSpecsService importDefAndSpecsService) {
		this.importDefAndSpecsService = importDefAndSpecsService;
	}
	
	public Date getSelectedAsOfDate() {
		return selectedAsOfDate;
	}

	public void setSelectedAsOfDate(Date selectedAsOfDate) {
		this.selectedAsOfDate = selectedAsOfDate;
	}
	
	public String getSelectedSpecType() {
		return selectedSpecType;
	}

	public void setSelectedSpecType(String selectedSpecType) {
		this.selectedSpecType = selectedSpecType;
	}
	
	public String getSelectedSpec() {
		return selectedSpec;
	}

	public void setSelectedSpec(String selectedSpec) {
		this.selectedSpec = selectedSpec;
	}
	
	public String getSelectedDescription() {
		return selectedDescription;
	}

	public void setSelectedDescription(String selectedDescription) {
		this.selectedDescription = selectedDescription;
	}
	
	public String getSelectedName() {
		return selectedName;
	}

	public void setSelectedName(String selectedName) {
		this.selectedName = selectedName;
	}
	
	public String getSelectedFileDisplay() {
		return selectedFileDisplay;
	}

	public boolean getIsQueued() {
		return (selectedFileDisplay!=null &&  selectedFileDisplay.equalsIgnoreCase(QUEUED));
	}
	
	public boolean getIsImported() {
		return (selectedFileDisplay!=null &&  selectedFileDisplay.equalsIgnoreCase(IMPORTED));
	}

	public void setSelectedFileDisplay(String selectedFileDisplay) {
		this.selectedFileDisplay = selectedFileDisplay;
	}
	
	public void setImportSpecList(List<ImportSpecDTO> importSpecList) {
		this.importSpecList = importSpecList;
	}	
	
	public List<ImportSpecDTO> getImportSpecList() {
		return importSpecList;
	}
	
	public void setFileItemList(List<FileItem> fileItemList) {
		this.fileItemList = fileItemList;
	}	
	
	public List<FileItem> getFileItemList() {
		return fileItemList;
	}
	
	public int getRowCount() {
		if(importSpecList==null){
			return 0;
		}
		else{
			return importSpecList.size();
		}
	}
	
	private int getFirstRow() {
		if(importSpecList==null || importSpecList.size()==0){
			return 0;
		}
		else{
			return 1;
		}
	}

	private int getLastRow() {
		if(importSpecList==null || importSpecList.size()==0){
			return 0;
		}
		else{
			return importSpecList.size();
		}
	}
	
	public String getPageDescription() {		
		return String.format("Displaying rows %d through %d (%d matching rows)",
				getFirstRow(), getLastRow(), getRowCount());
	}
	
	public String getFilPageDescription() {		
		int size = 0;
		if(fileItemList!=null){
			size = fileItemList.size();
		}
		
		if(size>0){
			return String.format("Displaying rows %d through %d (%d matching rows)",1, size, size);
		}
		else{
			return String.format("Displaying rows %d through %d (%d matching rows)",0, 0, 0);
		}	
	}
	
	public int getPageSize() {
		if(importSpecList==null){
			return 0;
		}
		else{
			return importSpecList.size();
		}
	}
	
	public void selectedSpecRowChanged(ActionEvent e) throws AbortProcessingException {
		HtmlDataTable table = (HtmlDataTable) ((HtmlAjaxSupport) e.getComponent()).getParent(); 
		setSelectedSpecRowIndex(table.getRowIndex());
		selectedImportSpecDTO = (ImportSpecDTO)table.getRowData();
		
		if(selectionMap.containsKey(selectedImportSpecDTO.getImportSpecCode())){
			DatabaseItem  databaseItem  = (DatabaseItem)selectionMap.get(selectedImportSpecDTO.getImportSpecCode());	
			setSelectedDataFolder(databaseItem.getFolder());
		}
		else{
			setSelectedDataFolder("");
		}
		
		if(selectedFileDisplay.equalsIgnoreCase(QUEUED)){
			fileItemList = this.getFileNames(getQueuedFolderPath()); 
		}
		else if(selectedFileDisplay.equalsIgnoreCase(IMPORTED)){
			fileItemList = this.getFileNames(getImportedFolderPath()); 
		}
		else if(selectedFileDisplay.equalsIgnoreCase(PRESCRIPT)){
			fileItemList = this.getFileNames(getPreimportScriptFolderPath()); 
		}
		else if(selectedFileDisplay.equalsIgnoreCase(POSTSCRIPT)){
			fileItemList = this.getFileNames(getPostimportScriptFolderPath()); 
		}
		else{
			if(fileItemList!=null){
				fileItemList.clear();
			}
			fileItemList = new ArrayList<FileItem>();
		}	
		
		selectedFileRowIndex = -1;
	}
	
	public void selectedFileRowChanged(ActionEvent e) throws AbortProcessingException {
		HtmlDataTable table = (HtmlDataTable) ((HtmlAjaxSupport) e.getComponent()).getParent(); 
		setSelectedFileRowIndex(table.getRowIndex());
		selectedFileItem = (FileItem)table.getRowData();
	}
	
	private Map<String, DatabaseItem> selectionMap = new LinkedHashMap<String, DatabaseItem>();
	
	public String resetFilterSearchAction(){
		//clear filter on maintenance
		selectedSpec = "";
		selectedSpecType = "";
		selectedDescription = "";	
		selectedName = "";
		selectedAsOfDate = null;
		
		return null;
	}
	
	public void retrieveFilterSpec() {
		if(selectionMap!=null){
			selectionMap.clear();
		}
		selectionMap = new LinkedHashMap<String, DatabaseItem>();
		
		//Get current database setting
		String selectedDb = ChangeContextHolder.getDataBaseType();	
		DbPropertiesDTO dbPropertiesDTO = dbPropertiesBean.getDbPropertiesDTO(selectedDb);
		List<DatabaseItem> databaseItems = ParseConfigFile(dbPropertiesDTO.getUrl());
		for (DatabaseItem databaseItem : databaseItems) {
			//Get all database entry from xml
			selectionMap.put(databaseItem.getSpecCode().toUpperCase(), databaseItem);
		}
		
		selectedSpecRowIndex = -1;
		if(importSpecList!=null){
			importSpecList.clear();
		}
		importSpecList = new ArrayList<ImportSpecDTO>();
		
		List<ImportSpecDTO> importSpeAll = importDefAndSpecsService.getAllImportSpec();	
		boolean criteriaMeet = true;
		for (ImportSpecDTO importSpecDTO : importSpeAll) {		
			if(selectionMap.containsKey(importSpecDTO.getImportSpecCode().toUpperCase())){//Meet filter criteria
				criteriaMeet = true;
				
				if(selectedSpecType!=null && selectedSpecType.length()>0 && !selectedSpecType.equalsIgnoreCase(importSpecDTO.getImportSpecType())){
					criteriaMeet = false;
					continue;
				}
				
				if(selectedSpec!=null && selectedSpec.length()>0 && !selectedSpec.equalsIgnoreCase(importSpecDTO.getImportSpecCode().toUpperCase())){
					criteriaMeet = false;
					continue;
				}
				
				if(selectedDescription!=null && selectedDescription.trim().length()>0){
					String searchDesc = selectedDescription.trim().toLowerCase();
					if(!searchDesc.endsWith("%") && !searchDesc.equalsIgnoreCase(importSpecDTO.getDescription().toLowerCase())){
						criteriaMeet = false;
					}
					else if(searchDesc.endsWith("%") && searchDesc.length()>1){
						//Do wildcard search				
						String subDesc = searchDesc.substring(0,searchDesc.length()-1);
						if(importSpecDTO.getDescription().toLowerCase().startsWith(subDesc)==false){
							criteriaMeet = false;
						}
					}
				}
								
				if(criteriaMeet){
					importSpecList.add(importSpecDTO);
				}
			}
		}
		
		//Reset file items
		selectedFileRowIndex = -1;
		if(fileItemList!=null){
			fileItemList.clear();
		}
		fileItemList = new ArrayList<FileItem>();
		
		selectedDataFolder = "";
		selectedSeparator = null;			
	}
	
	public List<SelectItem> getImportSpecItems() {
		if(specList==null){
			List<ImportSpecDTO> importSpeAll = importDefAndSpecsService.getAllImportSpec();		
			specList = new ArrayList<SelectItem>();		
			specList.add(new SelectItem("","Select an Import Spec Code"));		
			for (ImportSpecDTO importSpecDTO : importSpeAll) {					
				specList.add(new SelectItem(importSpecDTO.getImportSpecCode(), importSpecDTO.getImportSpecCode()+"-"+importSpecDTO.getDescription()));
			}
		}
		
		return specList;
	}
	
	public List<SelectItem> getFileDisplayItems() {
		if(fileDisplayItems==null){
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
	    	selectItems.add(new SelectItem(QUEUED,"Queued Files"));
	    	selectItems.add(new SelectItem(IMPORTED,"Imported Files"));
	    	selectItems.add(new SelectItem(POSTSCRIPT,"Post-Import Scripts"));
	    	selectItems.add(new SelectItem(PRESCRIPT,"Pre-Import Scripts"));
	
	    	fileDisplayItems= selectItems;
		}
 
		return fileDisplayItems;
	}
	
	public void fileDisplayChanged(ActionEvent e) {
		if(selectedSpecRowIndex<0){
			return;
		}
		
		selectedFileRowIndex =-1;
		resetSortOrder();
		
		if(selectedFileDisplay.equalsIgnoreCase(QUEUED)){
			//default ASCENDING
			Map<String,String> resetOrder = getSortOrder();
			resetOrder.put("fileDate", BaseExtendedDataModel.SORT_ASCENDING);
			fileItemList = this.getFileNames(getQueuedFolderPath()); 
		}
		else if(selectedFileDisplay.equalsIgnoreCase(IMPORTED)){
			//default DESCENDING
			Map<String,String> resetOrder = getSortOrder();
			resetOrder.put("fileDate", BaseExtendedDataModel.SORT_DESCENDING);
			fileItemList = this.getFileNames(getImportedFolderPath()); 
		}
		else if(selectedFileDisplay.equalsIgnoreCase(PRESCRIPT)){
			fileItemList = this.getFileNames(getPreimportScriptFolderPath()); 
		}
		else if(selectedFileDisplay.equalsIgnoreCase(POSTSCRIPT)){
			fileItemList = this.getFileNames(getPostimportScriptFolderPath()); 
		}
		else{
			if(fileItemList!=null){
				fileItemList.clear();
			}
			fileItemList = new ArrayList<FileItem>();
		}
	}
	
	public List<SelectItem> getImportSpecTypeItems() {
		if(specTypeList==null){
			specTypeList = new ArrayList<SelectItem>();		
			specTypeList.add(new SelectItem("","Select an Import Spec Type"));		
			Map<String, ListCodes> listCodeList = 	cacheManager.getListCodesMapByType("BATCHTYPE");
			for (ListCodes listcodes : listCodeList.values()) {			
				if(listcodes != null && listcodes.getCodeTypeCode()!=null && listcodes.getDescription()!=null){				
					specTypeList.add(new SelectItem(listcodes.getCodeCode(),listcodes.getCodeCode()+"-"+listcodes.getDescription()));
				}
			}
		}
		
		return specTypeList;
	}
	
	public Boolean getDisplayDownloadButtons() {
		if(selectedFileDisplay.equalsIgnoreCase(IMPORTED) && (selectedFileRowIndex!=-1)){
			return true;
		}

		return false;
	}
	
	public Boolean getDisplayAddButtons() {
		if(selectedFileDisplay.equalsIgnoreCase(QUEUED) && (selectedSpecRowIndex!=-1)  && 
				filePreferenceBean.getAllowAidAddFile()){
			return true;
		}
		else if((selectedFileDisplay.equalsIgnoreCase(PRESCRIPT) || selectedFileDisplay.equalsIgnoreCase(POSTSCRIPT)) && (selectedSpecRowIndex!=-1) && 
				isAdminUser()){
			return true;
		}

		return false;
	}
	
	public Boolean getDisplayViewButtons() {
		if(selectedFileDisplay.equalsIgnoreCase(QUEUED) && (selectedFileRowIndex!=-1)){
			return true;
		}
		else if(selectedFileDisplay.equalsIgnoreCase(IMPORTED) && (selectedFileRowIndex!=-1)){
			return true;
		}
		else if(selectedFileDisplay.equalsIgnoreCase(PRESCRIPT) && (selectedFileRowIndex!=-1)){
			return true;
		}
		else if(selectedFileDisplay.equalsIgnoreCase(POSTSCRIPT) && (selectedFileRowIndex!=-1)){
			return true;
		}

		return false;
	}
	
	public Boolean getDisplayDeleteButtons() {
		if(selectedFileDisplay.equalsIgnoreCase(QUEUED) && (selectedFileRowIndex!=-1) && 
				filePreferenceBean.getAllowAidDeleteFile()){
			return true;
		}
		else if((selectedFileDisplay.equalsIgnoreCase(POSTSCRIPT) || selectedFileDisplay.equalsIgnoreCase(PRESCRIPT)) && (selectedFileRowIndex!=-1)  && 
				isAdminUser()){
			return true;
		}
		
		return false;
	}
	
	public Boolean getDisplayLogButtons() {
		if(selectedFileDisplay.equalsIgnoreCase(IMPORTED) && (selectedFileRowIndex!=-1)){
			return true;
		}
		
		return false;
	}
	
	public Boolean getDisplayReimportButtons() {
		if(selectedFileDisplay.equalsIgnoreCase(IMPORTED) && (selectedFileRowIndex!=-1) && 
				filePreferenceBean.getAllowAidReimportFile()){
			return true;
		}

		return false;
	}
	
	public String displayDownloadAction() {
		if(selectedFileDisplay.equalsIgnoreCase(IMPORTED) && (selectedSpecRowIndex!=-1)){
			String filePath = null;
			boolean fileChecked = false;
			for (FileItem fileItem : fileItemList ) {
				if(fileItem.getSelected()){
					fileChecked = true;
					filePath = this.getImportedFolderPath() + this.selectedSeparator + fileItem.getFileName();		
					fileNameText = fileItem.getFileName();
					break;// Use the first one
				}		
			}
			
			if(!fileChecked){
				filePath = this.getImportedFolderPath() + this.selectedSeparator + selectedFileItem.getFileName();		
			}
			
			if(filePath!=null){
				//Doing download
				FacesContext context = FacesContext.getCurrentInstance();
				File file = new File(filePath);
				try {
					Util.downloadFile(file, context);
				} 
				catch (IOException e) {
					e.printStackTrace();
				}			
			}
		}
		
		return "";
	}
	
	public String displayViewAction() {
		if(selectedFileDisplay.equalsIgnoreCase(QUEUED) && (selectedFileRowIndex!=-1)){
			String filePath = null;
			StringBuffer buffer = null;
			boolean fileChecked = false;
			for (FileItem fileItem : fileItemList ) {
				if(fileItem.getSelected()){
					fileChecked = true;
					filePath = this.getQueuedFolderPath() + this.selectedSeparator + fileItem.getFileName();		
					buffer = retrieveFirstLastRows(filePath,defaultRowNumber);
					fileNameText = fileItem.getFileName();
					break;// Use the first one
				}		
			}
			
			if(!fileChecked){
				filePath = this.getQueuedFolderPath() + this.selectedSeparator + selectedFileItem.getFileName();		
				buffer = retrieveFirstLastRows(filePath,defaultRowNumber);
				fileNameText = selectedFileItem.getFileName();
			}
					
			fileContent = buffer.toString();
			actionText = "View AID Queued File";
		}
		else if(selectedFileDisplay.equalsIgnoreCase(IMPORTED) && (selectedFileRowIndex!=-1)){
			String filePath = null;
			StringBuffer buffer = null;
			boolean fileChecked = false;
			for (FileItem fileItem : fileItemList ) {
				if(fileItem.getSelected()){
					fileChecked = true;
					filePath = this.getImportedFolderPath() + this.selectedSeparator + fileItem.getFileName();		
					buffer = retrieveFirstLastRows(filePath,defaultRowNumber);	
					fileNameText = fileItem.getFileName();
					break;// Use the first one
				}		
			}
			
			if(!fileChecked){
				filePath = this.getImportedFolderPath() + this.selectedSeparator + selectedFileItem.getFileName();		
				buffer = retrieveFirstLastRows(filePath,defaultRowNumber);
				fileNameText = selectedFileItem.getFileName();
			}		
			fileContent = buffer.toString();
			actionText = "View AID Imported File";
		}
		else if(selectedFileDisplay.equalsIgnoreCase(POSTSCRIPT) && (selectedFileRowIndex!=-1)){
			String filePath = null;
			StringBuffer buffer = null;
			boolean fileChecked = false;
			for (FileItem fileItem : fileItemList ) {
				if(fileItem.getSelected()){
					fileChecked = true;
					filePath = this.getPostimportScriptFolderPath() + this.selectedSeparator + fileItem.getFileName();		
					buffer = retrieveFirstLastRows(filePath,defaultRowNumber);
					fileNameText = fileItem.getFileName();
					break;// Use the first one
				}		
			}
			
			if(!fileChecked){
				filePath = this.getPostimportScriptFolderPath() + this.selectedSeparator + selectedFileItem.getFileName();		
				buffer = retrieveFirstLastRows(filePath,defaultRowNumber);	
				fileNameText = selectedFileItem.getFileName();
			}
			fileContent = buffer.toString();
			actionText = "View AID Post Script";
		}
		else if(selectedFileDisplay.equalsIgnoreCase(PRESCRIPT) && (selectedFileRowIndex!=-1)){
			String filePath = null;
			StringBuffer buffer = null;
			boolean fileChecked = false;
			for (FileItem fileItem : fileItemList ) {
				if(fileItem.getSelected()){
					fileChecked = true;
					filePath = this.getPreimportScriptFolderPath() + this.selectedSeparator + fileItem.getFileName();		
					buffer = retrieveFirstLastRows(filePath,defaultRowNumber);
					fileNameText = fileItem.getFileName();
					break;// Use the first one
				}		
			}
			
			if(!fileChecked){
				filePath = this.getPreimportScriptFolderPath() + this.selectedSeparator + selectedFileItem.getFileName();		
				buffer = retrieveFirstLastRows(filePath,defaultRowNumber);	
				fileNameText = selectedFileItem.getFileName();
			}		
			fileContent = buffer.toString();
			actionText = "View AID Pre Script";
		}
		
		return "aid_file_view";
	}
	
	public String closeAction() {
		selectedFileRowIndex =-1;
		
		if(selectedFileDisplay.equalsIgnoreCase(QUEUED)){
			fileItemList = this.getFileNames(getQueuedFolderPath()); 
		}
		else if(selectedFileDisplay.equalsIgnoreCase(IMPORTED)){
			fileItemList = this.getFileNames(getImportedFolderPath()); 
		}
		else if(selectedFileDisplay.equalsIgnoreCase(PRESCRIPT)){
			fileItemList = this.getFileNames(getPreimportScriptFolderPath()); 
		}
		else if(selectedFileDisplay.equalsIgnoreCase(POSTSCRIPT)){
			fileItemList = this.getFileNames(getPostimportScriptFolderPath()); 
		}
		else{
			if(fileItemList!=null){
				fileItemList.clear();
			}
			fileItemList = new ArrayList<FileItem>();
		}
		
		return "aid_file_manager_main";
	}
	

	public String okViewAction() {
		
		return "aid_file_manager_main";
	}
	
	private StringBuffer retrieveFirstLastRows(String fileName, int lineNumber){
		StringBuffer sbFirst =  new StringBuffer(); 
		StringBuffer sbSecond =  new StringBuffer(); 

		BufferedReader br = null;
		try {
			String sCurrentLine = null;
			int size = lineNumber;
			int firstArrayIndex = -1;
			int secondArrayIndex = -1;	
			String [] firstArray = new String[size]; 
			String [] secondArray = new String[size];  

			br = new BufferedReader(new FileReader(fileName));
			int line = 0;
			while ((sCurrentLine = br.readLine()) != null) {
				line++;
				if(secondArrayIndex == size-1){
					secondArrayIndex = -1;
				}
				
				if(firstArrayIndex<size-1){
					firstArrayIndex++;//index from 0 to size-1				
					firstArray[firstArrayIndex] = sCurrentLine;
					continue;
				}
				
				if(secondArrayIndex<size-1){
					secondArrayIndex++;//index from 0 to size-1				
					secondArray[secondArrayIndex] = sCurrentLine;
					continue;
				}
			}
			
			if(firstArrayIndex>-1){//At least one record			
				for(int i=0; i< firstArrayIndex+1; i++){
					sbFirst.append(firstArray[i]+"\n");
				}
			}
			
			int actualLines = 0;
			if(secondArrayIndex>-1){			
				if(secondArrayIndex<secondArray.length-1 && secondArray[secondArrayIndex+1] !=null){
					for(int i=secondArrayIndex+1; i< secondArray.length; i++){
						//for second part
						 sbSecond.append(secondArray[i]+"\n");
						actualLines++;
					}
				}
				
				for(int i=0; i< secondArrayIndex+1; i++){
					//for first part
					 sbSecond.append(secondArray[i]+"\n");
					actualLines++;
				}
			}
			
			filePrefix = "Displaying ";
			if((firstArrayIndex+1)==0){
				//No row display
				filePrefix = filePrefix + "lines " + 0 + " through " + 0;
			}
			else if(line <= (lineNumber*2)){
				//First part only
				filePrefix = filePrefix + "lines " + 1 + " through " + line;
				sbFirst.append(sbSecond.toString());
			}
			else{
				filePrefix = filePrefix + "lines 1 through " + (firstArrayIndex+1) + " and " + "lines " + (line-actualLines+1) + " through " + line;
				sbFirst.append("... ... ..."+"\n");
				sbFirst.append(sbSecond.toString());	
			}
			
			filePrefix = filePrefix + " of:";
		} catch (IOException e) {
			e.printStackTrace();
			filePrefix = "";
			sbFirst.append(e.getMessage()+"\n");
		} finally {
			try {
				if (br != null)br.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		
		return sbFirst;
	}

	public void fileUploadAction() throws IOException {
		FileOutputStream fw = null;
	    BufferedOutputStream fos = null;
	    InputStream stream = null;
	    
		if (this.uploadFile.getUploadedFile() != null) {
			try{
				UploadedFile myFile = this.getUploadFile().getUploadedFile();
				stream = myFile.getInputStream();	

				int contentLength = (int) myFile.getSize(); // get the file size (in bytes)
				byte[] data = new byte[contentLength]; // allocate byte array of right size
				stream.read( data, 0, contentLength ); // read into byte array


				String filePath = null;
				if(selectedFileDisplay.equalsIgnoreCase(QUEUED)){
					filePath = this.getQueuedFolderPath() + this.selectedSeparator;		
				}
				else if(selectedFileDisplay.equalsIgnoreCase(POSTSCRIPT)){
					filePath = this.getPostimportScriptFolderPath() + this.selectedSeparator;		
				}
				
				String fileName = FacesUtils.getName(myFile);
			    if (fileName == null) fileName = myFile.getName();

			    new File(filePath).mkdirs();
			    File inputFile = new File(filePath + fileName);
			    fw = new FileOutputStream(inputFile);
			    fos = new BufferedOutputStream(fw);
			    fos.write(data);
			      
			     this.uploadedFileName = fileName;
			     this.uploadedFileSize = ((int)(contentLength/1024)+1) + " KB";
			}catch(FileNotFoundException fe){
				FacesMessage msg = new FacesMessage("Path not found");
				msg.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage(null, msg);
			}catch(IOException e){
				e.printStackTrace();
			}
			finally {
				try {
					if (fos != null) fos.close();
					if (fw != null) fw.close();
					if (stream != null) stream.close();
				} 
				catch (IOException e) {
					e.printStackTrace();
				}
		    }
		}
		else{
			//"Certificate
			FacesMessage msg = new FacesMessage("File name is required.");
			msg.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, msg);
		}
	}
	
	public String displayAddAction() {
		if(selectedFileDisplay.equalsIgnoreCase(QUEUED)){
			actionText = "Add Import File";
		} 
	  //else if(selectedFileDisplay.equalsIgnoreCase(this.POSTSCRIPT)){
	  //actionText = "Add Post Script";
	  //}
		
		uploadFile = new HtmlInputFileUpload();
		this.uploadedFileName = "";
		this.uploadedFileSize = "";
		
		return "aid_file_add";
	}
	
	public String displayDeleteAction() {
		String filePath = null;
		if(selectedFileDisplay.equalsIgnoreCase(QUEUED) && (selectedFileRowIndex!=-1)){
			boolean fileChecked = false;
			for (FileItem fileItem : fileItemList ) {
				//Get all selected files and detete them
				if(fileItem.getSelected()){
					filePath = this.getQueuedFolderPath() + this.selectedSeparator + fileItem.getFileName();
					fileChecked = true;
					deleteFile(filePath);
				}		
			}
			
			if(!fileChecked){
				filePath = this.getQueuedFolderPath() + this.selectedSeparator + selectedFileItem.getFileName();	
				deleteFile(filePath);
			}
		}
		else if(selectedFileDisplay.equalsIgnoreCase(IMPORTED) && (selectedFileRowIndex!=-1)){

		}
		else if(selectedFileDisplay.equalsIgnoreCase(POSTSCRIPT) && (selectedFileRowIndex!=-1)){
			boolean fileChecked = false;
			for (FileItem fileItem : fileItemList ) {
				//Get all selected files and detete them
				if(fileItem.getSelected()){
					filePath = this.getPostimportScriptFolderPath() + this.selectedSeparator + fileItem.getFileName();
					fileChecked = true;
					deleteFile(filePath);
				}		
			}
			
			if(!fileChecked){
				filePath = this.getPostimportScriptFolderPath() + this.selectedSeparator + selectedFileItem.getFileName();	
				deleteFile(filePath);
			}
		}
		
		//Refresh data
		if(selectedFileDisplay.equalsIgnoreCase(QUEUED)){
			fileItemList = this.getFileNames(getQueuedFolderPath()); 
		}
		else if(selectedFileDisplay.equalsIgnoreCase(IMPORTED)){
			fileItemList = this.getFileNames(getImportedFolderPath()); 
		}
		else if(selectedFileDisplay.equalsIgnoreCase(PRESCRIPT)){
			fileItemList = this.getFileNames(getPreimportScriptFolderPath()); 
		}
		else if(selectedFileDisplay.equalsIgnoreCase(POSTSCRIPT)){
			fileItemList = this.getFileNames(getPostimportScriptFolderPath()); 
		}
		else{
			if(fileItemList!=null){
				fileItemList.clear();
			}
			fileItemList = new ArrayList<FileItem>();
		}	
		
		selectedFileRowIndex = -1;
		
		return "";
	}
	
	public String displayLogAction(){
		if(selectedFileDisplay.equalsIgnoreCase(IMPORTED) && (selectedFileRowIndex!=-1)){
			String logFile =  "";
			String filePath = "";	
			
			boolean fileChecked = false;
			for (FileItem fileItem : fileItemList ) {
				if(fileItem.getSelected()){
					fileChecked = true;
					logFile =  fileItem.getFileName().substring(0, fileItem.getFileName().length()-3) + "log";
					filePath = this.getLogFolderPath() + this.selectedSeparator + logFile;	
					break;// Use the first one
				}		
			}
			
			if(!fileChecked){
				logFile =  selectedFileItem.getFileName().substring(0, selectedFileItem.getFileName().length()-3) + "log";
				filePath = this.getLogFolderPath() + this.selectedSeparator + logFile;	
			}
	
			//Replace imported path with log path
			StringBuffer a = retrieveFirstLastRows(filePath,defaultRowNumber);			
			fileContent = a.toString();
			actionText = "View AID Import Log File";
			fileNameText = logFile;
		}
		
		return "aid_file_view";
	}
	
	public String displayReimportAction() {
		if(selectedFileDisplay.equalsIgnoreCase(IMPORTED) && (selectedFileRowIndex!=-1)){
			boolean fileChecked = false;
			for (FileItem fileItem : fileItemList ) {
				if(fileItem.getSelected()){
					fileChecked = true;
					reImportFile(fileItem.getFileName());
					//Remove the checks after clicking the re-import button
					fileItem.setSelected(false);
				}		
			}
			
			if(!fileChecked){
				reImportFile(selectedFileItem.getFileName());
			}	
		}
		
		return null;
	}

	boolean isAdminRole(){
		return (loginBean.getRole().getRoleCode().equalsIgnoreCase("ADMIN"));
	}
	
	boolean isAdminUser(){
		//PP-174
		return (loginBean.getUserDTO().getUserCode().equalsIgnoreCase("STSCORP") || loginBean.getUserDTO().getAdminFlag().equals("2"));
	}
	
	public loginBean getLoginBean() {
		return loginBean;
	}

	public void setLoginBean(loginBean loginBean) {
		this.loginBean = loginBean;
	}
	
	public FilePreferenceBackingBean getFilePreferenceBean() {
		return filePreferenceBean;
	}

	public void setFilePreferenceBean(FilePreferenceBackingBean filePreferenceBean) {
		this.filePreferenceBean = filePreferenceBean;
	}
	
	public void setSelectedDataFolder(String selectedDataFolder) {
		this.selectedDataFolder = selectedDataFolder;
		
		File dir = new File(selectedDataFolder);
        FileFilter fileFilter = new FileFilter() {
            public boolean accept(File file) {
                return file.isFile();
            }
        };
        File[] files = dir.listFiles(fileFilter);

        if (files == null) {
        	selectedSeparator = null;
        }
        else{
        	selectedSeparator = dir.separator;
        }
	}
	
	public String getSelectedDataFolder() {
		return this.selectedDataFolder;
	}
	
	public static void main(String[] args) throws Exception{
		//String aLog = "pco_test_import.txt.20140908185522.txt";
		//aLog = aLog.substring(0, aLog.length()-3);
		//AidFileManagerBean aidFileManagerBean = new AidFileManagerBean();
		//StringBuffer a = aidFileManagerBean.retrieveFirstLastRows("C:\\AP_IMPORT\\STSTEST\\dummy1.txt",100);
		
		
		String fromFile = "C:\\temp\\NOBL_PNPT_AP_from.txt";
		String toFile = "C:\\temp\\NOBL_PNPT_AP_to.txt";
		
		byte[] buffer = new byte[1000];     
		java.io.BufferedInputStream in = null;
		java.io.BufferedOutputStream out = null;
		
		
	    try {
	    	//java.io.FileInputStream fis = new java.io.FileInputStream(fromFile);
	    	in = new java.io.BufferedInputStream(new java.io.FileInputStream(fromFile));

	    	//java.io.FileOutputStream fos = new java.io.FileOutputStream(toFile);
	        out = new java.io.BufferedOutputStream(new java.io.FileOutputStream(toFile));

	        int numBytes;
	        while ((numBytes = in.read(buffer))!= -1){
	        	out.write(buffer, 0, numBytes);
	        }

	        System.out.println(fromFile+ " is successfully copied to "+toFile);
	    }
	    catch (IOException e){
	        e.printStackTrace();
	    }
	    finally {
	    	if(in!= null){try{in.close();}catch(Exception ex){}}
	    	if(out!= null){try{out.close();}catch(Exception ex){}}
	    }
	}
	
	public void sortAction(ActionEvent e) {
		toggleSortOrder(e.getComponent().getParent().getId());
	}
	
	public void resetSortOrder() {
    	Map<String,String> resetOrder = getSortOrder();	
    	for (String key : resetOrder.keySet()) {
			resetOrder.put(key, BaseExtendedDataModel.SORT_NONE);
		}
    }
	
	public void toggleSortOrder(String selectedField) {
    	Map<String,String> sortOrder = getSortOrder();
    	processToggleSortOrder(sortOrder, selectedField);
    	
    	fileItemList = getSortItemList(fileItemList);
    }
	
	public List<FileItem> getSortItemList(List<FileItem> fileList) {
		if(sortOrder!=null && fileItemList!=null){
			for (String field : sortOrder.keySet()) {
				if (!BaseExtendedDataModel.SORT_NONE.equals(sortOrder.get(field))) {
					if(BaseExtendedDataModel.SORT_ASCENDING.equals(sortOrder.get(field))){
						System.out.println(BaseExtendedDataModel.SORT_ASCENDING + " for " + field);
						if("id".equalsIgnoreCase(field)){
							Collections.sort(fileList, new FileItemComparator(FileItemComparator.COMPARE_BY_ID));
						}
						else if("selected".equalsIgnoreCase(field)){
							Collections.sort(fileList, new FileItemComparator(FileItemComparator.COMPARE_BY_SELECTED));
						}
						else if("fileSize".equalsIgnoreCase(field)){
							Collections.sort(fileList, new FileItemComparator(FileItemComparator.COMPARE_BY_FILESIZE));
						}
						else if("fileName".equalsIgnoreCase(field)){
							Collections.sort(fileList, new FileItemComparator(FileItemComparator.COMPARE_BY_FILENAME));
						}
						else if("fileDate".equalsIgnoreCase(field)){
							Collections.sort(fileList, new FileItemComparator(FileItemComparator.COMPARE_BY_FILEDATE));
						}						
					}
					else{
						System.out.println(BaseExtendedDataModel.SORT_DESCENDING + " for " + field);
						if("id".equalsIgnoreCase(field)){
							Collections.sort(fileList, Collections.reverseOrder(new FileItemComparator(FileItemComparator.COMPARE_BY_ID)));
						}
						else if("selected".equalsIgnoreCase(field)){
							Collections.sort(fileList, Collections.reverseOrder(new FileItemComparator(FileItemComparator.COMPARE_BY_SELECTED)));
						}
						else if("fileSize".equalsIgnoreCase(field)){
							Collections.sort(fileList, Collections.reverseOrder(new FileItemComparator(FileItemComparator.COMPARE_BY_FILESIZE)));
						}
						else if("fileName".equalsIgnoreCase(field)){
							Collections.sort(fileList, Collections.reverseOrder(new FileItemComparator(FileItemComparator.COMPARE_BY_FILENAME)));
						}
						else if("fileDate".equalsIgnoreCase(field)){
							Collections.sort(fileList, Collections.reverseOrder(new FileItemComparator(FileItemComparator.COMPARE_BY_FILEDATE)));
						}
					}
					break;
				}
			}
		}

		return fileList;
	}
	
	public Map<String,String> getSortOrder() {
    	if (sortOrder == null) {
        	sortOrder = new LinkedHashMap<String,String>();
        	sortOrder.put("id", BaseExtendedDataModel.SORT_NONE);
        	sortOrder.put("selected", BaseExtendedDataModel.SORT_NONE);
        	sortOrder.put("fileSize", BaseExtendedDataModel.SORT_NONE);
        	sortOrder.put("fileName", BaseExtendedDataModel.SORT_NONE);
        	sortOrder.put("fileDate", BaseExtendedDataModel.SORT_NONE);
    	}
    	return sortOrder;
    }
	
	private void processToggleSortOrder(Map<String,String> localSortOrder, String field) {
		if (localSortOrder.containsKey(field) && BaseExtendedDataModel.SORT_ASCENDING.equals(localSortOrder.get(field))) {
			localSortOrder.put(field, BaseExtendedDataModel.SORT_DESCENDING);
		} else {
			localSortOrder.put(field, BaseExtendedDataModel.SORT_ASCENDING);
		}
		
		// Only single column sort (for now) so we
		// set all the others back to none
		for (String key : localSortOrder.keySet()) {
			if (!key.equals(field)) {
				localSortOrder.put(key, BaseExtendedDataModel.SORT_NONE);
			}
		}
    }
	
	//PP-408, Cannot copy large files using AID File Manager Re-Import function
	public boolean copyFile(String sourceFileName, String destFileName){
		byte[] buffer = new byte[1000];     
		java.io.BufferedInputStream in = null;
		java.io.BufferedOutputStream out = null;
	    try {
	    	in = new java.io.BufferedInputStream(new java.io.FileInputStream(sourceFileName));
	        out = new java.io.BufferedOutputStream(new java.io.FileOutputStream(destFileName));
	        int numBytes;
	        while ((numBytes = in.read(buffer))!= -1){
	        	out.write(buffer, 0, numBytes);
	        }
	        System.out.println(sourceFileName + " is successfully copied to " + destFileName);
	    } 
	    catch (IOException e){
	        e.printStackTrace();
	        return false;
	    }
	    finally {
	    	if(in!= null){try{in.close();}catch(Exception ex){return false;}}
	    	if(out!= null){try{out.close();}catch(Exception ex){return false;}}
	    }
	    
	    return true;  
	}
	public boolean deleteFile(String sourceFileName){	
    	try{
    		File file = new File(sourceFileName);
    		if(file.delete()){
    			return true;
    		}else{
    			System.out.println("Delete operation is failed.");
    			return false;
    		}
    	}catch(Exception e){
    		e.printStackTrace();
    		return false;
    	}
    }
	
	public boolean reImportFile(String fileName){
		String newFileName = fileName + "-REIMPORT.txt";
		
		String importedfilePath = getImportedFolderPath() + selectedSeparator + fileName;
		String queuedfilePath = getQueuedFolderPath() + selectedSeparator + newFileName;
		
		return copyFile(importedfilePath, queuedfilePath);
	}
	
	public String getQueuedFolderPath(){
        return selectedDataFolder;
	}
	
	public String getImportedFolderPath(){
        return (selectedDataFolder + selectedSeparator + IMPORTED_FOLDER);
	}
	
	public String getLogFolderPath(){
        return (selectedDataFolder + selectedSeparator + LOG_FOLDER);
	}
	
	public String getPostimportScriptFolderPath(){
        return (selectedDataFolder + selectedSeparator + SCRIPTS_FOLDER + selectedSeparator + POSTIMPORT_FOLDER);
	}
	
	public String getPreimportScriptFolderPath(){
        return (selectedDataFolder + selectedSeparator + SCRIPTS_FOLDER + selectedSeparator + PREIMPORT_FOLDER);
	}
	
	Date dateFilter = null;  
	String nameFilter = null;
	int numberFilter = 0;
	
	public List<FileItem> getFileNames(String folder){
		List<FileItem> fileItems = new ArrayList<FileItem>();
		Long id = 0L;

		dateFilter = null;
		nameFilter = null;
		numberFilter = 0;
        if(selectedAsOfDate!=null){
	        try{		        
		        dateFilter = new Date(selectedAsOfDate.getTime());
	        }
	        catch(Exception e){
	        	e.printStackTrace();
	        	return fileItems;
	        }
        }
        
        if(selectedName!=null && selectedName.trim().length()>0){
        	nameFilter = selectedName.trim().toLowerCase();
        }

		File dir = new File(folder);
        FileFilter fileFilter = new FileFilter() {
            public boolean accept(File file) {
            	if(!file.isFile() || numberFilter>=1000){
            		return false; //File only & Max number of files to display
            	}
            	
            	// Get filename of file or directory
        		if((dateFilter==null) || (dateFilter!=null && file.lastModified() >= dateFilter.getTime())){      			
        		}
        		else{
        			return false;
        		}

            	if(nameFilter!=null){
					if(!nameFilter.endsWith("%") && !nameFilter.equalsIgnoreCase(file.getName().toLowerCase())){
						return false;
					}
					else if(nameFilter.endsWith("%") && nameFilter.length()>1){
						//Do wildcard search					
						String subName = nameFilter.substring(0,nameFilter.length()-1);
						if(file.getName().toLowerCase().startsWith(subName)==false){
							return false;
						}
					}
        		}
            	   
            	numberFilter++;
                return true;
            }
        };
        File[] files = dir.listFiles(fileFilter);

        if (files == null) {
            //############# Either dir does not exist or is not a directory. #############
        	System.out.println("No folder found in " + folder);
        }else if(files.length==0){
        	//############# If no file in folder, skip it. #############
        	System.out.println("No file found in " + folder);
        }
        else{
        	File file = null;
     
        	FileItem fileItem = null;
        	boolean criteriaMeet = true;
        	for (int i=0; i<files.length; i++) {
        		file = (File)files[i];
    			fileItem = new FileItem(id++);   			
    			fileItem.setFileName(file.getName());
    			fileItem.setFileSize(((int)(file.length()/1024)+1));
    			fileItem.setFileDate(new Date(file.lastModified()));
    			fileItems.add(fileItem);
        	}
        }
        
        fileItems = getSortItemList(fileItems);
		
		return fileItems;
	}
	
	public List<DatabaseItem> ParseConfigFile(String dbUrlFilter){
		List<DatabaseItem> databaseItems = new ArrayList<DatabaseItem>();
    	try {		
            DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
            
            aidRootFolder = filePreferenceBean.getAidInstallation();
            Document doc = docBuilder.parse (new File(aidRootFolder + "\\conf\\AIDSetting.xml"));
            
            // normalize text representation
            doc.getDocumentElement ().normalize ();
            System.out.println ("Root element of the doc is " + doc.getDocumentElement().getNodeName());

            NodeList listOfPersons = doc.getElementsByTagName("database");
            int totalPersons = listOfPersons.getLength();
            System.out.println("Total # of databases : " + totalPersons);

            for(int s=0; s<listOfPersons.getLength() ; s++){
                Node firstPersonNode = listOfPersons.item(s);
                if(firstPersonNode.getNodeType() == Node.ELEMENT_NODE){
                	
                	DatabaseItem databaseItem = new DatabaseItem();
                	
                    Element firstPersonElement = (Element)firstPersonNode;
                    
                    //-------
                    NodeList importTypeList = firstPersonElement.getElementsByTagName("ImportType");
                    Element importTypeElement = (Element)importTypeList.item(0);

                    NodeList textImportTypeList = importTypeElement.getChildNodes();
                    String importType = ((Node)textImportTypeList.item(0)).getNodeValue().trim();
                    if(importType==null || importType.equalsIgnoreCase("Update Files")){
                    	continue;
                    }

                    //-------
                    NodeList firstNameList = firstPersonElement.getElementsByTagName("Folder");
                    Element folderElement = (Element)firstNameList.item(0);

                    NodeList textFolderList = folderElement.getChildNodes();
                    String folder = ((Node)textFolderList.item(0)).getNodeValue().trim();
                    databaseItem.setFolder(folder);

                    //-------
                    NodeList specCodeList = firstPersonElement.getElementsByTagName("SpecCode");
                    Element specCodeElement = (Element)specCodeList.item(0);

                    NodeList textSpecCodeList = specCodeElement.getChildNodes();
                    String specCode = ((Node)textSpecCodeList.item(0)).getNodeValue().trim();
                    databaseItem.setSpecCode(specCode);

                    //----
                    NodeList dbUserNameList = firstPersonElement.getElementsByTagName("DbUserName");
                    Element dbUserNameElement = (Element)dbUserNameList.item(0);

                    NodeList textDbUserNameList = dbUserNameElement.getChildNodes();
                    String dbUserName = ((Node)textDbUserNameList.item(0)).getNodeValue().trim();
                    databaseItem.setDbUserName(dbUserName);
                    
                    //----
                    NodeList dbPasswordList = firstPersonElement.getElementsByTagName("DbPassword");
                    Element dbPasswordElement = (Element)dbPasswordList.item(0);

                    NodeList textDbPasswordList = dbPasswordElement.getChildNodes();
                    String dbPassword = ((Node)textDbPasswordList.item(0)).getNodeValue().trim();
                    databaseItem.setDbPassword(dbPassword);
                    
                    //----
                    NodeList dbDriverList = firstPersonElement.getElementsByTagName("DbDriver");
                    Element dbDriverElement = (Element)dbDriverList.item(0);

                    NodeList textDbDriverList = dbDriverElement.getChildNodes();
                    String dbDriver = ((Node)textDbDriverList.item(0)).getNodeValue().trim();
                    databaseItem.setDbDriver(dbDriver);
                    
                    //----
                    NodeList dbUrlList = firstPersonElement.getElementsByTagName("DbUrl");
                    Element dbUrlElement = (Element)dbUrlList.item(0);

                    NodeList textDbUrlList = dbUrlElement.getChildNodes();
                    String dbUrl = ((Node)textDbUrlList.item(0)).getNodeValue().trim();
                    databaseItem.setDbUrl(dbUrl);
                              
                    //----
                    /*
                    boolean autoProcess = true;
                    NodeList autoProcessList = firstPersonElement.getElementsByTagName("AutoProcess");
                    if(autoProcessList!=null){
                    	Element autoProcessElement = (Element)autoProcessList.item(0);

                    	NodeList textAutoProcessList = autoProcessElement.getChildNodes();
                    	String strAutoProcess = ((Node)textAutoProcessList.item(0)).getNodeValue().trim();
                    	
                    	if(strAutoProcess.equalsIgnoreCase("0")){
                    		autoProcess = false;
                    	}
                    }
                    databaseItem.setAutoProcess(autoProcess);
                    */
                    
                    if(dbUrlFilter.equalsIgnoreCase(databaseItem.getDbUrl())){
                    	databaseItems.add(databaseItem);
                    }        
                }//end of if clause

            }//end of for loop with s var
        }
    	catch (SAXParseException err) {
        	System.out.println ("** Parsing error" + ", line " + err.getLineNumber () + ", uri " + err.getSystemId ());
        	System.out.println(" " + err.getMessage ());
        }
    	catch (SAXException e) {

        }
    	catch (Throwable t) {
        }
    	
    	return databaseItems;
    }
	
	public void selectTableCheckChange(ActionEvent e) throws AbortProcessingException {
		HtmlSelectBooleanCheckbox chk = (HtmlSelectBooleanCheckbox) ((HtmlAjaxSupport) e.getComponent()).getParent();
	}
	
	public class FileItem{
		private Long id = null;
		private Boolean selected = false;
		private int fileSize = 0;
		private String fileName = "";
		private Date fileDate = null;
		
		FileItem(){		
		}
		
		FileItem(Long id){	
			this.id = id;
		}
		
		public Long getId() {
			return this.id;
		}

		public void setId(Long id) {
			this.id = id;
		}
		
		public Boolean getSelected() {
			return this.selected;
		}

		public void setSelected(Boolean selected) {
			this.selected = selected;
		}
		
		public int getFileSize() {
			return this.fileSize;
		}

		public void setFileSize(int fileSize) {
			this.fileSize= fileSize;
		}
		
		public String getFileName() {
			return this.fileName;
		}

		public void setFileName(String fileName) {
			this.fileName = fileName;
		}

		public Date getFileDate() {
			return this.fileDate;
		}

		public void setFileDate(Date fileDate) {
			this.fileDate = fileDate;
		}
	}

	public class DatabaseItem{
		private Boolean selected = false;
		private String folder = "";
		private String specCode = "";
		private String dbUserName = "";
		private String dbPassword = "";
		private String dbDriver = "";
		private String dbUrl = "";
		private Boolean autoProcess = false;
		
		public Boolean getSelected() {
			return this.selected;
		}

		public void setSelected(Boolean selected) {
			this.selected = selected;
		}
		
		public String getSpecCode() {
			return this.specCode;
		}

		public void setSpecCode(String specCode) {
			this.specCode = specCode;
		}

		public String getFolder() {
			return this.folder;
		}

		public void setFolder(String folder) {
			this.folder= folder;
		}
		
		public String getDbUserName() {
			return this.dbUserName;
		}

		public void setDbUserName(String dbUserName) {
			this.dbUserName = dbUserName;
		}
		
		public String getDbPassword() {
			return this.dbPassword;
		}

		public void setDbPassword(String dbPassword) {
			this.dbPassword = dbPassword;
		}
		
		public String getDbDriver() {
			return this.dbDriver;
		}

		public void setDbDriver(String dbDriver) {
			this.dbDriver = dbDriver;
		}
		
		public String getDbUrl() {
			return this.dbUrl;
		}

		public void setDbUrl(String dbUrl) {
			this.dbUrl = dbUrl;
		}

		public Boolean getAutoProcess() {
			return this.autoProcess;
		}

		public void setAutoProcess(Boolean autoProcess) {
			this.autoProcess = autoProcess;
		}
	}
	
	public class FileItemComparator implements Comparator<FileItem> {
		public static final int COMPARE_BY_ID = 0;
		public static final int COMPARE_BY_SELECTED = 1;
		public static final int COMPARE_BY_FILESIZE = 2;
		public static final int COMPARE_BY_FILENAME = 3;
		public static final int COMPARE_BY_FILEDATE = 4;
		
		private int compare_mode = COMPARE_BY_ID;
		
		public FileItemComparator() {
		}

		public FileItemComparator(int compare_mode) {
		    this.compare_mode = compare_mode;
		}

		@Override
		public int compare(FileItem o1, FileItem o2) {
			switch (compare_mode) {
			    case COMPARE_BY_ID:
			        return o1.getId().compareTo(o2.getId());
			    case COMPARE_BY_SELECTED:
			        return o1.getSelected().compareTo(o2.getSelected());
			    case COMPARE_BY_FILESIZE:
			    	if (o1.getFileSize() < o2.getFileSize())
						return -1;
					else if (o1.getFileSize() > o2.getFileSize())
						return 1;
					else {
						return 0;
					}
			    case COMPARE_BY_FILENAME:
			        return o1.getFileName().compareTo(o2.getFileName());
			    case COMPARE_BY_FILEDATE:
			    	if (o1.getFileDate().getTime() < o2.getFileDate().getTime())
						return -1;
					else if (o1.getFileDate().getTime() > o2.getFileDate().getTime())
						return 1;
					else {
						return 0;
					}
			    default:
			    	return o1.getId().compareTo(o2.getId());
		    }
		}

	}
}
