package com.ncsts.view.bean;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.ncsts.common.LoggerFactory;
import com.ncsts.common.Util;
import com.ncsts.domain.DataDefinitionColumn;
import com.ncsts.domain.DriverNames;
import com.ncsts.domain.DwFilter;
import com.ncsts.domain.LocationMatrix;
import com.ncsts.domain.TaxMatrix;
import com.ncsts.domain.User;
import com.ncsts.dto.UserDTO;
import com.ncsts.services.DwFilterService;
import com.ncsts.services.UserService;

@Component
@Scope("session")
public class SelectionFilterBean implements InitializingBean {

	static private Logger logger = LoggerFactory.getInstance().getLogger(
			SelectionFilterBean.class);
	
	@Autowired
	private DwFilterService dwFilterService;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private MatrixCommonBean matrixCommonBean;

	private UserDTO userDTO;
	public String NEW_FILTER = "*NEWFILTER";
	private List<DwFilter> dwFilterList = new ArrayList<DwFilter>();
	private String screenName = "";
	private String selectedUserName = "";
	private String selectedFilterName = "";
	private String createdFilterName = "";
	private String currentFilterName = "";
	private boolean initialStateChanged = false;
	private TransactionDetailBean transactionDetailBean = null;
	private DatabaseStatisticsBean databaseStatisticsBean = null;
	private List<SelectItem> userNameList = null;
	private List<SelectItem> filterNameList = null;
	private Map<String,String> columnDescriptionMap;
	private String callingScreen;
	
	private static String TRANSACTION_PROCESS = "transactionProcess";
	private static String TRANSACTION_STATISTICS = "transactionStatitics";
	
	private List<DwFilter> dwFilters = new ArrayList<DwFilter>(); 
	private String[] transactionStatiticsArray = new String[]{"geocode", "city", "county", "state", "country", "zip"};
	private String[] transactionProcessArray   = new String[] {"transactionCountryCode", "transactionStateCode","transactionStatus",
														  "transactionDetailId", "jurisdictionId", "glExtractBatchNo", "splitSubtransId",
														  "multiTransCode", "taxabilityCode"};
	

	public List<DwFilter> getDwFilters() {
		return dwFilters;
	}

	public void setDwFilters(List<DwFilter> dwFilters) {
		this.dwFilters = dwFilters;
	}

	public String getCallingScreen() {
		return callingScreen;
	}

	public void setCallingScreen(String callingScreen) {
		this.callingScreen = callingScreen;
	}

	
	@Override
	public void afterPropertiesSet() throws Exception {
	}
	
	public SelectionFilterBean() {
	}

	public void userNameChangeListener(ActionEvent e) {	
		initialStateChanged = true;
		filterNameList = null;
		selectedFilterName = "";
	}
	
	public Boolean isGlobalNoPermission(){
		return ((userDTO.getGlobalViewFlag()==null || !userDTO.getGlobalViewFlag().equals("1")) && 
				(this.selectedUserName!=null && this.selectedUserName.equalsIgnoreCase("*GLOBAL")));
		//DO NOT add "*NEW Filter" selection, disable add, update, delete buttons when "*GLOBAL" selected, but no global permission
	}
	
	public Boolean getDisplayUpdateButton() {
		return (!initialStateChanged && !selectedFilterName.equalsIgnoreCase(NEW_FILTER)) && !isGlobalNoPermission();		
	}
	
	public Boolean getDisplayAddButton() {
		return (selectedFilterName.equalsIgnoreCase(NEW_FILTER)) && !isGlobalNoPermission();
	}
	
	public Boolean getDisplayDeleteButton() {
		return !(selectedFilterName.equalsIgnoreCase(NEW_FILTER) || selectedFilterName.length()==0)  && !isGlobalNoPermission();
	}

	public void filterNameChangeListener(ActionEvent e) {
		initialStateChanged = true;
		if(selectedFilterName.length()==0 || selectedFilterName.equalsIgnoreCase(NEW_FILTER)){	
		}
		else{
			dwFilterList = dwFilterService.getColumnsByFilterNameandUser("TB_PURCHTRANS", selectedUserName, this.selectedFilterName);
		}
	}
	
	public final List<SelectItem> getUserNameList() {
		if(userNameList==null){
			userNameList = new ArrayList<SelectItem>();
			
			userNameList.add(new SelectItem("*GLOBAL", "*GLOBAL"));
			if(userDTO.getGlobalViewFlag()==null || !userDTO.getGlobalViewFlag().equals("1")){
				userNameList.add(new SelectItem(userDTO.getUserCode().toUpperCase(), userDTO.getUserCode().toUpperCase()));
			}
			else{
				List<User> users = userService.findAllUsers();				
				if(users != null && users.size() > 0){
	    	    	for(User user : users){
	    	    		userNameList.add(new SelectItem(user.getUserCode(), user.getUserCode()));		
	    	    	}
	    	    } 	
			}
		}
		return userNameList;
	}
	
	public final List<SelectItem> getFilterNameList() {
		if (filterNameList == null) {
			List<String> filterNames = dwFilterService.getFilterNamesPerUserCode("TB_PURCHTRANS", this.selectedUserName);
			
			filterNameList = new ArrayList<SelectItem>();
			filterNameList.add(new SelectItem("","Select a View Name"));	
			if(!isGlobalNoPermission()){
				filterNameList.add(new SelectItem(NEW_FILTER, "*NEW Filter"));	
			}
			for(String filtername : filterNames){
				filterNameList.add(new SelectItem(filtername, filtername));
			}
		}
		return filterNameList;
	}
	
	public List<DwFilter> getDwFilterList() {
		return dwFilterList;
	}

	public void setDwFilterList(List<DwFilter> dwFilterList) {
		this.dwFilterList = dwFilterList;
	}
	
	public String getSelectedUserName() {
		return selectedUserName;
	}

	public void setSelectedUserName(String selectedUserName) {
		this.selectedUserName = selectedUserName;
	}
	
	public String getScreenName() {
		return screenName;
	}

	public void setUserDTO(UserDTO userDTO){
		this.userDTO = userDTO;
	}

	public void setScreenName(String screenName) {
		this.screenName = screenName;
	}
	
	public String getScreenNameDescription() {
		if(screenName!=null && screenName.equalsIgnoreCase("TB_PURCHTRANS")){
			return "Purchase Transactions";
		}
		
		return "";
	}
	
	public String getSelectedFilterName() {
		return selectedFilterName;
	}
	
	public void setCurrentFilterName(String selectedFilterName) {
		initialStateChanged = false;
		currentFilterName = selectedFilterName;
		createdFilterName = "";
		if(selectedFilterName!=null && selectedFilterName.equalsIgnoreCase("")){
			this.selectedFilterName = selectedFilterName = NEW_FILTER; //if this pass in from transaction maintenance
			this.selectedUserName = userDTO.getUserCode();
		}
		else{
			this.selectedUserName = selectedFilterName.substring(0, selectedFilterName.indexOf("/"));
			this.selectedFilterName = selectedFilterName.substring(selectedFilterName.indexOf("/") + 1 ,selectedFilterName.length());
			//based on the selected view to decide the user
			//String userCode = dwFilterService.getUserByFilterName(screenName, this.selectedFilterName); 
		}
		
		filterNameList = null;
	}
	
	public void setSelectedFilterName(String selectedFilterName) {
		this.selectedFilterName = selectedFilterName;
	}

	public String getCreatedFilterName() {
		return createdFilterName;
	}

	public void setCreatedFilterName(String createdFilterName) {
		this.createdFilterName = createdFilterName;
	}
	
	public String okAddAction() {	
		if(createdFilterName==null || createdFilterName.trim().length()==0){
			FacesMessage msg = new FacesMessage("Filter Name is mandatory.");
	        msg.setSeverity(FacesMessage.SEVERITY_ERROR);
	        FacesContext.getCurrentInstance().addMessage(null, msg);
	        return null;
		}
		
		String userCode = dwFilterService.getUserByFilterNameandUserCode(screenName, createdFilterName, selectedUserName);
		if(userCode!=null && userCode.length()>0){
			FacesMessage msg = new FacesMessage("Filter Name already exists.");
	        msg.setSeverity(FacesMessage.SEVERITY_ERROR);
	        FacesContext.getCurrentInstance().addMessage(null, msg);
	        return null;
		}
		
		List<DwFilter> filterList = new ArrayList<DwFilter>();	
		if(dwFilterList!=null){
			for(DwFilter dwFilter : dwFilterList){
				dwFilter.setUserCode(selectedUserName);
				dwFilter.setFilterName(createdFilterName);
				filterList.add(dwFilter);
			}
			
			dwFilterService.addColumnsByFilterName(screenName, createdFilterName, filterList);
		}
		
		//TODO: refresh filter combo
		if(transactionDetailBean!=null && TRANSACTION_PROCESS.equals(getCallingScreen())){ 
			transactionDetailBean.setFilterItems(null);
		}
		
		if(databaseStatisticsBean!=null && TRANSACTION_STATISTICS.equals(getCallingScreen())){
			databaseStatisticsBean.setFilterItems(null);
		}
		
		if(userDTO.getUserCode().equalsIgnoreCase(selectedUserName) || selectedUserName.equalsIgnoreCase("*GLOBAL")){
			if(transactionDetailBean!=null && TRANSACTION_PROCESS.equals(getCallingScreen())){
				transactionDetailBean.setSelectedFilter(selectedUserName + "/" + createdFilterName);
				transactionDetailBean.filterChangeListener(null);
			}
			
			if(databaseStatisticsBean!=null && TRANSACTION_STATISTICS.equals(getCallingScreen())){
				databaseStatisticsBean.setSelectedFilter(selectedUserName + "/" + createdFilterName);
				databaseStatisticsBean.filterChangeListener(null);
			}
		}
	
		if(TRANSACTION_PROCESS.equals(getCallingScreen()))	 {
	        	return "transactions_process";
	    }
        else {
        	return "transactions_db_stats";
        }
	}
	
	public String okUpdateAction() {
		List<DwFilter> filterList = new ArrayList<DwFilter>();	
		LinkedHashSet<DwFilter> linkedHashSet = new LinkedHashSet<DwFilter>(); 
		List<String> dbStatiticsJurisdictionlist = Arrays.asList(transactionStatiticsArray);
		List<String> transactionprocessList = Arrays.asList(transactionProcessArray);
		if(dwFilterList!=null){
			for(DwFilter dwFilter : dwFilterList){
				dwFilter.setUserCode(selectedUserName);
				dwFilter.setFilterName(selectedFilterName);
				filterList.add(dwFilter);
			}
			// check the dwFilters list if any value is not present in the dwFilterList means 
			for(int i =0; i<dwFilters.size(); i++) {
				DwFilter dwFilter = dwFilters.get(i);
				//if calling screen is transaction process screen then do not delete db stats screen items if exist already
				if(TRANSACTION_PROCESS.equals(getCallingScreen())) {
					if(dbStatiticsJurisdictionlist.contains(dwFilter.getColumnName())) {
						continue;
					}
				}
				
				//if calling screen is db statitics screen then do not delete trans process screen items if exist already
				if(TRANSACTION_STATISTICS.equals(getCallingScreen())) {
					if(transactionprocessList.contains(dwFilter.getColumnName())) {
						continue;
					}
				}
				
				
				if(dwFilterList.contains(dwFilter)) {
					continue ;
				}
				
				else {
					dwFilters.remove(dwFilter);
				}
			}
			
			if(dwFilters != null){
				filterList.addAll(dwFilters);
			}
			linkedHashSet.addAll(filterList);
			filterList.clear();
			filterList.addAll(linkedHashSet);
			dwFilterService.updateColumnsByFilterNameandUser(screenName, selectedFilterName, selectedUserName, filterList);
		}
		
		if(TRANSACTION_PROCESS.equals(getCallingScreen())) {
		    return "transactions_process";
	    }
	    else {
		    return "transactions_db_stats";
	    }
	}
	
	public boolean getIsSelectUserNameEnabled(){
		return true;
	}
	
	public String deleteAction() {	
		dwFilterService.deleteColumnsByFilterNameandUserCode(screenName, selectedFilterName, selectedUserName);
		
		//TODO: refresh filter combo
		if(transactionDetailBean!=null && TRANSACTION_PROCESS.equals(getCallingScreen())){
			transactionDetailBean.setFilterItems(null);
		}
		
		if(databaseStatisticsBean!=null && TRANSACTION_STATISTICS.equals(getCallingScreen())){
			databaseStatisticsBean.setFilterItems(null); 
		}
		
		if(currentFilterName.equalsIgnoreCase(selectedFilterName)){
			//TODO: reset to "*NOFILTER"
			if(transactionDetailBean!=null && TRANSACTION_PROCESS.equals(getCallingScreen())){
				transactionDetailBean.setSelectedFilter("");
				transactionDetailBean.filterChangeListener(null);
			}
			if(databaseStatisticsBean!=null && TRANSACTION_STATISTICS.equals(getCallingScreen())){
				databaseStatisticsBean.setSelectedFilter("");
				databaseStatisticsBean.filterChangeListener(null);
			}
		}
		
		if(TRANSACTION_PROCESS.equals(getCallingScreen())) {
		    return "transactions_process";
	    }
	    else {
		    return "transactions_db_stats";
	    }
	}
	
	public String cancelAction() {
		
	    if(TRANSACTION_PROCESS.equals(getCallingScreen())) {
		    return "transactions_process";
	    }
	    else {
		    return "transactions_db_stats";
	    }
	}
	
	public void setallbackBeanC(TransactionDetailBean transactionDetailBean){
		this.transactionDetailBean = transactionDetailBean;
	}
	
	public void setallbackBeanDbStats(DatabaseStatisticsBean databaseStatisticsBean){
		this.databaseStatisticsBean = databaseStatisticsBean;
	}

	public Map<String,String> getColumnDescriptionMap() {
		if(columnDescriptionMap==null){
			columnDescriptionMap = new LinkedHashMap<String,String>();
		
			Map<String,DataDefinitionColumn> transactionPropertyMap = matrixCommonBean.getCacheManager().getTransactionPropertyMap(); 
			String transactionColumnname = "";
			String name = "";
			String description = "";
			Map<String,DriverNames> taxDriversMap =  matrixCommonBean.getCacheManager().getMatrixColDriverNamesMap(TaxMatrix.DRIVER_CODE);
			for (String matrixColumnName : taxDriversMap.keySet()) {
				DriverNames driverNames = taxDriversMap.get(matrixColumnName);
				transactionColumnname = driverNames.getTransDtlColName();
				
				name = Util.columnToProperty(transactionColumnname);// like glCompanyNbr			
				if(transactionPropertyMap.get(name)!=null){
					description = ((DataDefinitionColumn)transactionPropertyMap.get(name)).getAbbrDesc(); //like Gl Company Name
				}
				else{
					description = name;
				}		
				columnDescriptionMap.put(name, description);
			}
			
			Map<String,DriverNames> locationDriversMap =  matrixCommonBean.getCacheManager().getMatrixColDriverNamesMap(LocationMatrix.DRIVER_CODE);
			for (String matrixColumnName : locationDriversMap.keySet()) {
				DriverNames driverNames = locationDriversMap.get(matrixColumnName);
				transactionColumnname = driverNames.getTransDtlColName();
				
				name = Util.columnToProperty(transactionColumnname);// like glCompanyNbr			
				if(transactionPropertyMap.get(name)!=null){
					description = ((DataDefinitionColumn)transactionPropertyMap.get(name)).getAbbrDesc(); //like Gl Company Name
				}
				else{
					description = name;
				}		
				columnDescriptionMap.put(name, description);
			}
			
			columnDescriptionMap.put("transactionCountryCode", "Transaction Country Code");	
			columnDescriptionMap.put("transactionStateCode", "Transaction State Code");	
			columnDescriptionMap.put("transactionStatus", "Trans Status");	
			columnDescriptionMap.put("purchtransId", "Purchtrans ID");	
			columnDescriptionMap.put("taxMatrixId", "Tax Matrix ID");		
			columnDescriptionMap.put("locationMatrixId", "Locn Matrix ID");
			columnDescriptionMap.put("jurisdictionId", "Jurisdiction ID");
			columnDescriptionMap.put("glExtractBatchNo", "G/L Ext No");
			columnDescriptionMap.put("processBatchNo", "Process Batch No");
			columnDescriptionMap.put("splitSubtransId", "Sub-Trans ID");
			columnDescriptionMap.put("multiTransCode", "Multi-Trans");	
			columnDescriptionMap.put("taxabilityCode", "Taxability");		
			columnDescriptionMap.put("taxcodeCode", "TaxCode");			
			columnDescriptionMap.put("fromGLDate", "From G/L Date");
			columnDescriptionMap.put("toGLDate", "To G/L Date");
			columnDescriptionMap.put("*ADVANCED", "*ADVANCED");
			columnDescriptionMap.put("geocode", "GeoCode");
			columnDescriptionMap.put("city", "City");
			columnDescriptionMap.put("county", "County");
			columnDescriptionMap.put("state", "State");
			columnDescriptionMap.put("country", "Country");
			columnDescriptionMap.put("zip", "Zip");
		}

		return columnDescriptionMap;
	}
}