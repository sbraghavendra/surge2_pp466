package com.ncsts.view.bean;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import javax.faces.component.UIComponent;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlPanelGrid;
import javax.faces.component.html.HtmlSelectBooleanCheckbox;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;

import org.ajax4jsf.component.UIDataAdaptor;
import org.ajax4jsf.component.html.HtmlAjaxSupport;
import org.apache.log4j.Logger;
import org.richfaces.component.html.HtmlDataTable;
import org.springframework.beans.factory.annotation.Autowired;

import com.ncsts.domain.BCPBillTransaction;
import com.ncsts.domain.TaxMatrix;
import com.ncsts.jsf.model.BCPSaleTransactionDataModel;
import com.ncsts.view.util.StateObservable;
import com.ncsts.view.util.TogglePanelController;

public class BCPSaleTransactionBean implements Observer, HttpSessionBindingListener {
	private static final Logger logger = Logger.getLogger(BCPSaleTransactionBean.class);
	
	private StateObservable ov = null;
	
	private Long batchId;
	
	private HtmlDataTable trDetailTable;
	private BCPSaleTransactionDataModel bcpSaleTransactionDataModel;
	private MatrixCommonBean matrixCommonBean;
	
	private boolean displayAllFields = false;
	
	private HtmlPanelGrid filterSelectionPanel;
	private TaxMatrix filterSelectionTaxMatrix = new TaxMatrix();
	private SearchDriverHandler driverHandler = new SearchDriverHandler();
	
	private int selectedRowIndex = -1;
	private BCPBillTransaction selectedTransaction;
	
	@Autowired
	private DriverHandlerBean driverHandlerBean;

	public Long getBatchId() {
		return batchId;
	}

	public void setBatchId(Long batchId) {
		this.batchId = batchId;
		refreshData();
		bcpSaleTransactionDataModel.setDefaultSortOrder("bcpSaletransId", BCPSaleTransactionDataModel.SORT_DESCENDING);
	}

	private void refreshData() {
		filterSelectionTaxMatrix = new TaxMatrix();
		selectedRowIndex = -1;
		selectedTransaction = null;
		
		BCPBillTransaction bcpBillTransaction = new BCPBillTransaction();
		bcpBillTransaction.setProcessBatchNo(batchId);
		bcpSaleTransactionDataModel.setFilterCriteria(bcpBillTransaction);
	}

	public HtmlDataTable getTrDetailTable() {
		if (trDetailTable == null) {
			trDetailTable = new HtmlDataTable();
			trDetailTable.setId("aMDetailList");
			trDetailTable.setVar("trdetail");
			
			matrixCommonBean.createBCPSaleTransactionDetailTable(trDetailTable,
					"bcpSaleTransactionBean", "bcpSaleTransactionBean.bcpSaleTransactionDataModel", 
					matrixCommonBean.getBCPSaleTransactionPropertyMap(), false, displayAllFields, false, false, true);
		}
		
		return trDetailTable;
	}

	public void setTrDetailTable(HtmlDataTable trDetailTable) {
		this.trDetailTable = trDetailTable;
	}

	public BCPSaleTransactionDataModel getBcpSaleTransactionDataModel() {
		return bcpSaleTransactionDataModel;
	}

	public void setBcpSaleTransactionDataModel(
			BCPSaleTransactionDataModel bcpSaleTransactionDataModel) {
		this.bcpSaleTransactionDataModel = bcpSaleTransactionDataModel;
	}

	public MatrixCommonBean getMatrixCommonBean() {
		return matrixCommonBean;
	}

	public void setMatrixCommonBean(MatrixCommonBean matrixCommonBean) {
		this.matrixCommonBean = matrixCommonBean;
		
		driverHandler.setDriverReferenceService(matrixCommonBean.getDriverReferenceService());
		driverHandler.setCacheManager(matrixCommonBean.getCacheManager());
	}
	
	public Boolean getMultiSelect() {
		return false;
	}
	
	public void selectedTransactionChanged (ActionEvent e) throws AbortProcessingException {
		UIComponent uiComponent = e.getComponent().getParent();
		UIDataAdaptor table = (UIDataAdaptor) uiComponent;
		selectedTransaction = (BCPBillTransaction) table.getRowData();
		selectedRowIndex = table.getRowIndex();
	}
	
	public void displayChange(ActionEvent e) {
		HtmlSelectBooleanCheckbox check = (HtmlSelectBooleanCheckbox) ((HtmlAjaxSupport) e.getComponent()).getParent();
	    displayAllFields = (check.getSubmittedValue() == null)? 
	    		((check.getValue() == null)? false:(Boolean) check.getValue()):Boolean.parseBoolean((String) check.getSubmittedValue());

		matrixCommonBean.createBCPSaleTransactionDetailTable(trDetailTable,
				"bcpSaleTransactionBean", "bcpSaleTransactionBean.bcpSaleTransactionDataModel", 
				matrixCommonBean.getBCPSaleTransactionPropertyMap(), false, displayAllFields, false, false, true);
	}
	
	public String resetFilter() {
		filterSelectionTaxMatrix = new TaxMatrix();
		
		return null;
	}
	
	public String listTransactions() {
		BCPBillTransaction trDetail = new BCPBillTransaction();
		trDetail.setProcessBatchNo(batchId);
		
		//matrixCommonBean.setBCPTransactionProperties(filterSelectionTaxMatrix, trDetail);
		bcpSaleTransactionDataModel.setFilterCriteria(trDetail);
		
		return null;
	}
	
	public HtmlPanelGrid getFilterSelectionPanel() {
		filterSelectionPanel = MatrixCommonBean.createDriverPanel(
				TaxMatrix.DRIVER_CODE, matrixCommonBean.getCacheManager(), 
				matrixCommonBean.getUserEntityService().getEntityItemDTO(),
				"filterPanel", 
				"bcpSaleTransactionBean", "filterSelectionTaxMatrix", "matrixCommonBean", 
				false, false, false, true, true);
		
		matrixCommonBean.prepareTaxDriverPanel(filterSelectionPanel);
		return filterSelectionPanel;
	}
	
	public void setFilterSelectionPanel(HtmlPanelGrid filterSelectionPanel) {
		this.filterSelectionPanel = filterSelectionPanel;
	}
	
	public TaxMatrix getFilterSelectionTaxMatrix() {
		return filterSelectionTaxMatrix;
	}
	
	public SearchDriverHandler getDriverHandler() {
		return driverHandler;
	}
	
	public void searchDriver(ActionEvent e) {
		driverHandlerBean.initSearch(
				filterSelectionTaxMatrix, 
				(HtmlInputText) e.getComponent().getParent().getChildren().get(0), e.getComponent());
		driverHandlerBean.setCallBackScreen(null);
	}
	
	public String closeAction() {
		this.batchId = null;
		filterSelectionTaxMatrix = new TaxMatrix();
		selectedRowIndex = -1;
		selectedTransaction = null;
		
		return "importmap_main";
	}

	public int getSelectedRowIndex() {
		return selectedRowIndex;
	}

	public void setSelectedRowIndex(int selectedRowIndex) {
		this.selectedRowIndex = selectedRowIndex;
	}
	
	public BCPBillTransaction getSelectedTransaction() {
		return selectedTransaction;
	}
	
	public void sortAction(ActionEvent e) {
		bcpSaleTransactionDataModel.toggleSortOrder(e.getComponent().getId().replace("s_trans_",""));
	}

	@Override
	public void valueBound(HttpSessionBindingEvent arg0) {
		StateObservable ov = StateObservable.getInstance();
		this.ov = ov;
		this.ov.addObserver(this);
	}

	@Override
	public void valueUnbound(HttpSessionBindingEvent arg0) {
		if(ov!=null){
			ov.deleteObserver(this);
		}
	}

	@Override
	public void update(Observable o, Object arg) {
		if (o == ov) {
			
		}
	}

	public boolean isDisplayAllFields() {
		return displayAllFields;
	}

	public void setDisplayAllFields(boolean displayAllFields) {
		this.displayAllFields = displayAllFields;
	}

}
