package com.ncsts.view.event;


import javax.faces.context.FacesContext;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;
import javax.servlet.http.HttpSession;

import com.ncsts.view.bean.ChangeContextHolder;

/**
 *
 * @author Paul Govindan
 *
 */
public class FacesContextListener implements PhaseListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 0L;

	/**
	 * 
	 */
	public FacesContextListener() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.faces.event.PhaseListener#afterPhase(javax.faces.event.PhaseEvent)
	 */    
	public void afterPhase(PhaseEvent e) {
	}

    /*
	 * (non-Javadoc)
	 * 
	 * @see javax.faces.event.PhaseListener#beforePhase(javax.faces.event.PhaseEvent)
	 */
	public void beforePhase(PhaseEvent e) {
		FacesContext fc = e.getFacesContext();	
		HttpSession session = (HttpSession)fc.getExternalContext().getSession(false);
        if (session != null) {
        	String selectedDb = (String) session.getAttribute("selectedDb");
        	if (selectedDb != null) {
    		    ChangeContextHolder.setDataBaseType(selectedDb);    
        	}
        }
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.faces.event.PhaseListener#getPhaseId()
	 */
	public PhaseId getPhaseId() {
		return PhaseId.RESTORE_VIEW;
	}

}
