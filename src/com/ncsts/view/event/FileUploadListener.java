/*
 * Author: Jim M. Wilson
 * Created: Aug 9, 2008
 * $Date: 2006-09-21 22:51:26 $: - $Revision: 1711 $: 
 */

package com.ncsts.view.event;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.ncsts.common.LoggerFactory;

/**
 * Checks for exception attribute (from tomahawk) if a file was attempted to be uploaded that was
 * bigger than max size in web.xml:  <param-name>uploadMaxFileSize</param-name>
 */
public class FileUploadListener implements PhaseListener {
  private Logger logger = LoggerFactory.getInstance().getLogger(FileUploadListener.class); 
  
  /* (non-Javadoc)
   * @see javax.faces.event.PhaseListener#afterPhase(javax.faces.event.PhaseEvent)
   */
  public void afterPhase(PhaseEvent arg0) {
    FacesContext facesContext = FacesContext.getCurrentInstance();
    HttpServletRequest req = (HttpServletRequest)facesContext.getExternalContext().getRequest();
    if (req.getAttribute("org.apache.myfaces.custom.fileupload.exception") != null) {
        Integer maxSize = (Integer)req.getAttribute("org.apache.myfaces.custom.fileupload.maxSize");
        logger.debug("Trapped exception: " + req.getAttribute("org.apache.myfaces.custom.fileupload.exception"));
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Cannot upload files larger than " + maxSize + " bytes.", "File too big");
        FacesContext.getCurrentInstance().addMessage(null, message);
    }
  }

  /* (non-Javadoc)
   * @see javax.faces.event.PhaseListener#beforePhase(javax.faces.event.PhaseEvent)
   */
  public void beforePhase(PhaseEvent arg0) {
  }

  /* (non-Javadoc)
   * @see javax.faces.event.PhaseListener#getPhaseId()
   */
  public PhaseId getPhaseId() {
      return PhaseId.RESTORE_VIEW;
  }

}
