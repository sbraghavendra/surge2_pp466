package com.ncsts.view.event;


import javax.faces.component.UIViewRoot;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.ncsts.common.LoggerFactory;
import com.ncsts.dto.UserDTO;

public class LoggedInCheck implements PhaseListener {

	private static final long serialVersionUID = 1L;

    private Logger logger = LoggerFactory.getInstance().getLogger(LoggedInCheck.class);	
	
	private static final String[] allowedPages = {
		"stylesheets/",
		"images/",
		"Cutups II/",
		"scripts/",
		"richfaces",
		"/index.",
		"/NCSTS.",
		"/NCSTSError.",
		"/NCSTSErrors.",
		"/NCSTSLogin.",
		"/NewCoSTS.",
		"/stsLogin.",
		"/developerLogin."
	};
	
	public void afterPhase(PhaseEvent arg0) {
		FacesContext facesContext = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession)(facesContext.getExternalContext().getSession(false));
		Object obj = (session == null)? null:session.getAttribute("user");
		UIViewRoot root = facesContext.getViewRoot();
		String viewId = (root == null)? "<NONE>":root.getViewId();

		try {

			if (!isViewInList(viewId, allowedPages)) {
				
				// Check if logged in
				if ((obj == null) || !(obj instanceof UserDTO)) {
					handleRedirect(facesContext, session, 
							(obj == null)? "unauthenticated request":"unknown user object type", 
									viewId, null);
				} else {
					// We're logged in, just need to
					// check if sessionid matches
					// UserDTO curUser = (UserDTO) obj;
		
					// TODO!

					// Check
					/*
					if (!session.getId().equalsIgnoreCase(user.getSessionId())) {
						handleRedirect(facesContext, session, "sessionid mismatch", viewId, user);
					} else {
						// Update the session with the just fetched user
						// so it can be used freely throughout the request
						session.setAttribute("user", user);
					}
					*/
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			handleRedirect(facesContext, session, "Exception: " + ex.getMessage(), viewId, null);
		}
	}
	
	private boolean isViewInList(String viewId, String[] list) {
		for (int i = 0; i < list.length; i++) {
			if (viewId.lastIndexOf(list[i]) > -1) {
				return true;
			}
		}
		
		return false;
	}
	
	private void handleRedirect(FacesContext facesContext, HttpSession session, String msg, String viewId, UserDTO user) {
		logger.info(((HttpServletRequest)facesContext.getExternalContext().getRequest()).getRemoteAddr() + 
					((user == null)? "":(" - " + user.toString())) + 
					" - redirecting - " + msg + ": " + viewId );
		if (session != null) {
			session.invalidate();
		}
		facesContext.getApplication().getNavigationHandler().handleNavigation(facesContext,null,"logout");
	}

	public void beforePhase(PhaseEvent arg0) {
	}

	public PhaseId getPhaseId() {
		return PhaseId.RESTORE_VIEW;
	}

}