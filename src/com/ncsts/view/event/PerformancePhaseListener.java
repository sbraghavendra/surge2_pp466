package com.ncsts.view.event;

import java.util.HashMap;
import java.util.Map;

import javax.faces.context.FacesContext;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.jamonapi.Monitor;
import com.jamonapi.MonitorFactory;
import com.ncsts.common.LoggerFactory;

public class PerformancePhaseListener implements PhaseListener {
	private static final long serialVersionUID = 1L;
	
	private Logger logger = LoggerFactory.getInstance().getLogger(PerformancePhaseListener.class);
	private static Map<PhaseId, String> phases;
	
	static {
		phases = new HashMap<PhaseId, String>();
		phases.put(PhaseId.ANY_PHASE, "ANY_PHASE");
		phases.put(PhaseId.APPLY_REQUEST_VALUES, "APPLY_REQUEST_VALUES");
		phases.put(PhaseId.INVOKE_APPLICATION, "INVOKE_APPLICATION");
		phases.put(PhaseId.PROCESS_VALIDATIONS, "PROCESS_VALIDATIONS");
		phases.put(PhaseId.RENDER_RESPONSE, "RENDER_RESPONSE");
		phases.put(PhaseId.RESTORE_VIEW, "RESTORE_VIEW");
		phases.put(PhaseId.UPDATE_MODEL_VALUES, "UPDATE_MODEL_VALUES");
	}

	@Override
	public void afterPhase(PhaseEvent phaseEvent) {
		if(logger.isDebugEnabled()) {
			HttpServletRequest request = getRequest(phaseEvent);
			Monitor monitor = (Monitor) request.getAttribute("monitor");
			monitor.stop();
			logger.debug(monitor);
		}
	}

	@Override
	public void beforePhase(PhaseEvent phaseEvent) {
		if(logger.isDebugEnabled()) {
			String phaseName = phases.get(phaseEvent.getPhaseId());
			HttpServletRequest request = getRequest(phaseEvent);
			Monitor monitor = MonitorFactory.start(phaseName + "-" + request.getRequestURI());
			request.setAttribute("monitor", monitor);
		}
	}

	private HttpServletRequest getRequest(PhaseEvent phaseEvent) {
		FacesContext facesContext = FacesContext.getCurrentInstance();
		return (HttpServletRequest)facesContext.getExternalContext().getRequest();
	}

	@Override
	public PhaseId getPhaseId() {
		return PhaseId.ANY_PHASE;
	}
}
