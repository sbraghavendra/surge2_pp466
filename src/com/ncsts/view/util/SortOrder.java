/*
 * Author: Jim M. Wilson
 * Created: Sep 8, 2008
 * $Date: 2008-09-08 19:00:06 -0500 (Mon, 08 Sep 2008) $: - $Revision: 2106 $: 
 */

package com.ncsts.view.util;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.ncsts.common.Util;

/**
 * Useful class to keep track of sort ordering 
 */
public class SortOrder<T> {
	private Logger logger = Logger.getLogger(SortOrder.class);
	
	public enum Order {
		None, Ascending, Descending;
	}

	public static final List<Order> NoneAscendingDescending = 
		new ArrayList<Order>(Arrays.asList(new Order[] { Order.None, Order.Ascending, Order.Descending }));

	public static final List<Order> AscendingDescending = 
		new ArrayList<Order>(Arrays.asList(new Order[] { Order.Ascending, Order.Descending }));

	private Map<String, Order> sortOrder;
	private Map<String, Method> methods;

	public SortOrder() {
		this(NoneAscendingDescending, null);
	}

	private List<Order> cycle;

	public SortOrder(List<Order> cycle, T t) {
		super();
		this.cycle = cycle;
		sortOrder = new HashMap<String, Order>();
		methods = new HashMap<String, Method>();
		if (t != null) {
			try {
				for (Method m : t.getClass().getMethods()) {
					logger.debug("method name: " + m.getName() );
					if (m.getName().startsWith("get")) {
						String field = Util.lowerCaseFirst(m.getName().substring(3));
						sortOrder.put(field, Order.None);
						methods.put(field, m);
					}
				}
			} catch (Exception e) {
				logger.debug(e);
			}
		}
	}

	/**
	 * Get the current value for a sorted field of a class instance
	 */
	public Object getValue(T instance, String field) {
		try {
			if (methods.containsKey(field)) {
				return methods.get(field).invoke(instance);
			} else {
				logger.error("No method for " + field);
			}
		} catch (Exception e) {
			logger.error(e);
		}
		return null;
	}

	/**
	 * Get the next ordering in the cycle
	 */
	public Order next(Order order) {
		if (cycle.indexOf(order) == cycle.size()-1) {
			return cycle.get(0);
		} else {
			return cycle.get(cycle.indexOf(order) + 1);
		}
	}

	public Map<String, String> get() {
		Map<String, String> map = new HashMap<String, String>();
		for (String key : sortOrder.keySet()) {
			map.put(key, sortOrder.get(key).toString().toLowerCase());
		}
		return map;
	}
	
	/**
	 * Assumes single column sorting, so set other columns to None 
	 */
	public void toggle(String field) {
		Order next =next(sortOrder.get(field));
		for (Map.Entry<String, Order> entry: sortOrder.entrySet()) {
			entry.setValue(Order.None);
		}
		sortOrder.put(field, next);
	}
	
	public Map.Entry<String, Order> getCurrentOrder() {
		for (Map.Entry<String, Order> entry: sortOrder.entrySet()) {
			if (entry.getValue() != Order.None) {
				return entry;
			}
		}
		return null;
	}
}
