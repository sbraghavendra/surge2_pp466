package com.ncsts.view.util;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.ncsts.common.CacheManager;
import com.ncsts.domain.BCPTaxCodeRule;
import com.ncsts.domain.BatchErrorLog;
import com.ncsts.domain.ListCodes;
import com.ncsts.view.bean.TaxCodeBackingBean;
import com.ncsts.view.bean.UpdateFilesBackingBean;

public class TaxCodeRulesUploadParser extends AbstractFileUploader implements
		Iterator<BCPTaxCodeRule> {

	private Logger logger = Logger.getLogger(TaxCodeRulesUploadParser.class);

	private String fileType;
	private Date releaseDate;
	private String releaseVersion;

	private int lineNumber;
	private int linesWritten;
	private String nextLine;

	private List<String> errors;
	private List<BatchErrorLog> batchErrorLogs;
	private BufferedReader bufferedReader;
	private boolean abort;
	private BigDecimal specialRateTotal;
	private Map<String, String> errorListCodes = new LinkedHashMap<String,String>();

	private static final SimpleDateFormat sdf = new SimpleDateFormat(
			"MM/dd/yyyy");
	private static final SimpleDateFormat sdfRelease = new SimpleDateFormat(
			"yyyy_MM");
	private static final String[] COLUMNS = { 
		"TAXCODE_COUNTRY_CODE", "LC_DESCRIPTION", "TAXCODE_STATE_CODE", "TS_GEOCODE", "TS_NAME", "TS_LOCAL_TAXABILITY_CODE", 
		"TS_ACTIVE_FLAG", "TAXCODE_CODE", "TC_DESCRIPTION", "TC_COMMENTS", "TC_ACTIVE_FLAG", "TD_JUR_LEVEL", "TD_TAXCODE_COUNTY", 
		"TD_TAXCODE_CITY", "TD_TAXCODE_STJ", "TD_EFFECTIVE_DATE", "TD_EXPIRATION_DATE", "TD_TAXCODE_TYPE_CODE", "TD_OVERRIDE_TAXTYPE_CODE", 
		"TD_RATETYPE_CODE", "TD_TAXABLE_THRESHOLD_AMT", "TD_TAXABLE_LIMITATION_AMT", "TD_TAX_LIMITATION_AMT", "TD_CAP_AMT", "TD_BASE_CHANGE_PCT", 
		"TD_SPECIAL_RATE", "TD_ACTIVE_FLAG", "TC_STATUS_IND", "UPDATE_CODE"
	};
	
	private boolean processHeaderMissing; 
	

	private CacheManager cacheManager;
	
	public void setCacheManager(CacheManager cacheManager) {
		this.cacheManager = cacheManager;
	}

	public boolean isprocessHeaderMissing() {
		return processHeaderMissing;
	}

	public void setprocessHeaderMissing(boolean processHeaderMissing) {
		this.processHeaderMissing = processHeaderMissing;
	}

	public TaxCodeRulesUploadParser(String path) {
		super(path);
		deleteOnExit = true;
	}
	
	public void setErrorListCodes(Map<String, String> errorListCodes) {
		this.errorListCodes = errorListCodes;
	}

	public void clear() {
		errors = new ArrayList<String>();
		batchErrorLogs = new ArrayList<BatchErrorLog>();
		this.fileType = null;
		this.releaseDate = null;
		this.releaseVersion = null;
		this.uploadFileName = null;
		this.uploadFilePath = null;
		this.totalBytes = 0;
		this.nextLine = null;
		this.linesInFile = 0;
		this.lineNumber = 0;
		this.specialRateTotal = ArithmeticUtils.ZERO;
	}

	public String getUploadFileName() {
		return this.uploadFileName;
	}

	public String getFileType() {
		return this.fileType;
	}

	public Date getReleaseDate() {
		return this.releaseDate;
	}

	public String getReleaseVersion() {
		return this.releaseVersion;
	}

	public Double getProgress() {
		if (lineNumber < 1)
			return -0.01;
		try {
			logger.debug("Line: " + linesWritten + ", lines: " + linesInFile
					+ ", " + linesWritten / new Double(linesInFile));
			return linesWritten / new Double(linesInFile);
		} catch (Exception e) {
			return -0.01;
		}
	}

	public List<String> getErrors() {
		return this.errors;
	}

	private void addError(String s) {
		errors.add(s);
	}

	private void addError(int lineNumber, String line, String code) {
		addError(lineNumber, null, null, line, null, code);
	}

	private void addError(int lineNumber, Integer columnNumber, String column,
			String columnName, String line, String code) {
		BatchErrorLog errorLog = new BatchErrorLog();
		errorLog.setErrorDefCode(code);
		errorLog.setColumnNo(columnNumber == null ? null : new Long(
				columnNumber));
		errorLog.setImportColumnValue(column);
		errorLog.setTransDtlColumnName(columnName);
		errorLog.setImportRow(line);
		errorLog.setRowNo(new Long(lineNumber));
		batchErrorLogs.add(errorLog);
	}

	@Override
	public void processHeader(BufferedReader reader) throws IOException {
		
		String firstLine = reader.readLine();
		
		try {
			String type = UpdateFilesBackingBean.getHeaderValueOfType(firstLine, UpdateFilesBackingBean.TYPE_VALUE);
			if(type==null) {
				addError(1, firstLine, "IP12");
				processHeaderMissing =  true;
				return;
			}
			this.fileType = type;

			String s = UpdateFilesBackingBean.getHeaderValueOfType(firstLine, UpdateFilesBackingBean.DATE_VALUE);
			if(s==null) {
				addError(1, firstLine, "IP12");
				processHeaderMissing =  true;
				return;
			}
			this.releaseDate = sdfRelease.parse(s);

			String release = UpdateFilesBackingBean.getHeaderValueOfType(firstLine, UpdateFilesBackingBean.REL_VALUE);
			if(release==null) {
				addError(1, firstLine, "IP12");
				processHeaderMissing =  true;
				return;
			}
			this.releaseVersion = release;

			String typecode = UpdateFilesBackingBean.getHeaderValueOfType(firstLine, UpdateFilesBackingBean.FILE_VALUE);
			if(typecode==null) {
				addError(1, firstLine, "IP12");
				processHeaderMissing =  true;
				return;
			}
			
		} catch (ParseException e) {
			addError(1, firstLine, "IP12");
			this.releaseDate = null;
			return; 
		}
	}

	public void upload(String fileName, InputStream stream) {
		clear();
		try {
			super.upload(fileName, stream);
		} catch (Exception e) {
			addError(1, e.getMessage(), "IP12");
			logger.error(e);
		}
	}

	@Override
	public boolean hasNext() {
		if (bufferedReader == null)
			return false;
		try {
			nextLine = bufferedReader.readLine();
			if (nextLine == null) {
				bufferedReader.close();
				bufferedReader = null;
				return false;
			}
			return nextLine.charAt(0) != '~';
		} catch (IOException e) {
			addError(e.getMessage());
			return false;
		}
	}

	@Override
	public BCPTaxCodeRule next() {
		if ((bufferedReader == null) || (nextLine == null))
			return null;
		if ((lineNumber % 200) == 0) {
			logger.debug("Line: " + lineNumber + ", errorCount:"
					+ batchErrorLogs.size());
		}
		String tokens[] = nextLine.split("\\t");
		BCPTaxCodeRule r = new BCPTaxCodeRule();
		if (tokens.length != COLUMNS.length) {
			addError(lineNumber, null, "Expected " + COLUMNS.length
					+ ", actual: " + tokens.length, null, nextLine, "IP1");
		} else {
			int i = 0;
			String parsedColumn = null;
			try {
				r.setLine(new Long(lineNumber));
				r.setText(nextLine);
				
				String country = tokens[i++];
				if(country==null || country.trim().length()==0){
					country = "*ALL";
				}
				else{
					country = country.trim();
				}			
				r.setTaxcodeCountryCode(country);
				
				
				r.setLcDescription(parsedColumn = tokens[i++]);
				
				String state = tokens[i++];
				if(state==null || state.trim().length()==0){
					state = "*ALL";
				}
				else{
					state = state.trim();
				}
				r.setTaxcodeStateCode(state);
				
				r.setTsGeocode(parsedColumn = tokens[i++]);
				r.setTsName(parsedColumn = tokens[i++]);
				r.setTsLocalTaxabilityCode(parsedColumn = tokens[i++]);
				r.setTsActiveFlag(parsedColumn = tokens[i++]);
				r.setTaxcodeCode(parsedColumn = tokens[i++]);
				r.setTcDescription(parsedColumn = tokens[i++]);
				r.setTcComments(parsedColumn = tokens[i++]);
				r.setTcActiveFlag(parsedColumn = tokens[i++]);
				r.setTdJurLevel(parsedColumn = tokens[i++]);
				
				String county = tokens[i++]; 
				if(county==null || county.trim().length()==0){
					county = "*ALL";
				}
				else{
					county = county.trim();
				}
				r.setTdTaxcodeCounty(county);
				
				String city = tokens[i++]; 
				if(city==null || city.trim().length()==0){
					city = "*ALL";
				}
				else{
					city = city.trim();
				}
				r.setTdTaxcodeCity(city);
				
				String stj = tokens[i++];
				if(stj==null || stj.trim().length()==0){
					stj = "*ALL";
				}
				else{
					stj = stj.trim();
				}
				r.setTdTaxcodeStj(stj);
				
				Long sortNo = TaxCodeBackingBean.CalculateSortNo(r.getTaxcodeCountryCode(), r.getTaxcodeStateCode(), r.getTdTaxcodeCounty(), r.getTdTaxcodeCity(), r.getTdTaxcodeStj());		
				if(sortNo!=null){
					r.setTdSortNo(BigDecimal.valueOf(sortNo));
				}
				
				try {
					parsedColumn = tokens[i++];
					if(parsedColumn!=null && parsedColumn.trim().length()>0){
						r.setTdEffectiveDate(sdf.parse(parsedColumn));
					}
					else{
						String dateString = sdf.format(new Date());//today
						r.setTdEffectiveDate(sdf.parse(dateString)); 
					}
				}
				catch(Exception e) {
					r.setTdEffectiveDate(null);
					throw e;
				}
				
				try {
					parsedColumn = tokens[i++];
					if(parsedColumn!=null && parsedColumn.trim().length()>0){
						r.setTdExpirationDate(sdf.parse(parsedColumn));
					}
					else{
						r.setTdExpirationDate(sdf.parse("12/31/9999"));
					}
				} catch (Exception e) {
					r.setTdExpirationDate(null);
					throw e;
				}
				
				r.setTdTaxcodeTypeCode(parsedColumn = tokens[i++]);
				r.setTdOverrideTaxtypeCode(parsedColumn = tokens[i++]);
				r.setTdRatetypeCode(parsedColumn = tokens[i++]);
				r.setTdTaxableThresholdAmt(parseBigDecimal(parsedColumn = tokens[i++]));			
				r.setTdTaxableLimitationAmt(parseBigDecimal(parsedColumn = tokens[i++]));		
				r.setTdTaxLimitationAmt(parseBigDecimal(parsedColumn = tokens[i++]));
				r.setTdCapAmt(parseBigDecimal(parsedColumn = tokens[i++]));
				r.setTdBaseChangePct(parseBigDecimal(parsedColumn = tokens[i++]));
				r.setTdSpecialRate(parseBigDecimal(parsedColumn = tokens[i++]));
				r.setTdActiveFlag(parsedColumn = tokens[i++]);	
				r.setTcStatusInd(parsedColumn = tokens[i++]); 
				r.setUpdateCode(parsedColumn = tokens[i++]);
				
				if (r.getTdSpecialRate() != null) {
					specialRateTotal = ArithmeticUtils.add(specialRateTotal,
							r.getTdSpecialRate());
				}
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
				addError(lineNumber, i, parsedColumn, COLUMNS[i - 1], nextLine,
						"TCR23");
			}

			lineNumber++;
		}

		return r;
	}

	@Override
	public void remove() {
	}

	public Iterator<BCPTaxCodeRule> iterator() {
		lineNumber = 2;
		linesWritten = 0;
		nextLine = null;
		try {
			bufferedReader = new BufferedReader(new FileReader(uploadFilePath));
		} catch (FileNotFoundException e) {
			logger.error(e.getMessage(), e);
			addError(e.getMessage());
		}
		return this;
	}
	
	public void resetBatchErrorLogs() {
		if(batchErrorLogs!=null){
			batchErrorLogs.clear();
		}
		batchErrorLogs = new ArrayList<BatchErrorLog>();;
	}

	public void checksum() {
		
		if (nextLine == null || nextLine.trim().length() == 0) {
			addIP6Error();
			return;
		}
		String footer[] = nextLine.split(" +");
		if(footer.length == 2) {
			//check for existance of # or $
			if(footer[1].contains("#")) {
				Long lines = Long.decode((footer[1].replaceAll("\\#", "")).trim());
				if (lines != getRows()) {
					addIP4Error(lines);
					addIP8Error();
				}
				else {
					addIP8Error();
				}
			}
			else if(footer[1].contains("$")) {
				// checksum specialRates
				BigDecimal total = parseBigDecimal((footer[1].replaceAll("\\$", "")).trim());
				NumberFormat nf = NumberFormat.getNumberInstance();
				nf.setMaximumFractionDigits(6);
				if (!nf.format(specialRateTotal).equals(nf.format(total))) {
					addIP5Error(nf,total);
					addIP7Error();
				}
				else {
					addIP7Error();
				}
			}
			return;
		}
		
		if(footer.length == 3) {
			if(footer[1].contains("#")) {
				Long lines = Long.decode((footer[1].replaceAll("\\#", "")).trim());
				if (lines != getRows()) {
					addIP4Error(lines);
				}
			}
			if(footer[2].contains("$")) {
				// checksum specialRates
				BigDecimal total = parseBigDecimal((footer[2].replaceAll("\\$", "")).trim());
				NumberFormat nf = NumberFormat.getNumberInstance();
				nf.setMaximumFractionDigits(6);
				if (!nf.format(specialRateTotal).equals(nf.format(total))) {
					addIP5Error(nf,total);
			
				}
			}
			return;
		}
		
		if(footer.length == 1) {
			addIP7Error();
			addIP8Error();
			
		}
	}
	
	public void addIP4Error(Long lines) {
		String s = "EOF Marker: " + lines + ", actual count: " + getRows();
		String errorCodeDesc = errorListCodes.get("IP4");
		if(errorCodeDesc != null && errorCodeDesc.length()>0) {
			addError(lineNumber, null, s, errorCodeDesc, nextLine, "IP4");
		}
		else {
			addError(lineNumber, null, s, "IP4", nextLine, "IP4");
		}
	}
	
	public void addIP5Error(NumberFormat nf, BigDecimal total) {
		String s = "EOF Marker: " + nf.format(total) + ", actual total: " + nf.format(specialRateTotal);
		String errorCodeDesc = errorListCodes.get("IP5");
		if(errorCodeDesc != null && errorCodeDesc.length()>0) {
			addError(lineNumber, null, s, errorCodeDesc, nextLine, "IP5");
		}
		else {
			addError(lineNumber, null, s, "IP5", nextLine, "IP5");
		}
	}
	
	public void addIP6Error() {
		String errorCodeDesc = errorListCodes.get("IP6");
		if(errorCodeDesc != null && errorCodeDesc.length()>0) {
			addError(lineNumber, null, errorCodeDesc, errorCodeDesc, "", "IP6");
		}
		else {
			addError(lineNumber, null, "IP6", "IP6", "", "IP6");
		}
	}
	
	public void addIP7Error() {
		String errorCodeDesc = errorListCodes.get("IP7");
		if(errorCodeDesc != null && errorCodeDesc.length()>0) {
			addError(lineNumber, null, errorCodeDesc, errorCodeDesc, nextLine,"IP7");
		}
		else {
			addError(lineNumber, null, "IP7", "IP7", nextLine,"IP7");
		}
	}
	
	public void addIP8Error() {
		String errorCodeDesc = errorListCodes.get("IP8");
		if(errorCodeDesc != null && errorCodeDesc.length()>0) {
			addError(lineNumber, null, errorCodeDesc, errorCodeDesc, nextLine, "IP8");
		}
		else {
			addError(lineNumber, null, "IP8", "IP8", nextLine,"IP8");
		}
	}
	
	public BigDecimal parseBigDecimal(String s) {
		if ((s == null) || (s.trim().length() == 0))
			return null;
		return new BigDecimal(s);
	}

	public Long getTotalBytes() {
		return this.totalBytes;
	}

	public List<BatchErrorLog> getBatchErrorLogs() {
		return this.batchErrorLogs;
	}

	public BigDecimal getSpecialRateTotal() {
		return specialRateTotal;
	}

	public int getRows() {
		return lineNumber - 2;
	}

	public void setLinesWritten(int linesWritten) {
		this.linesWritten = linesWritten;
	}
	
	public boolean isAbort() {
		return abort;
	}

	public void setAbort(boolean abort) {
		this.abort = abort;
	}
}
