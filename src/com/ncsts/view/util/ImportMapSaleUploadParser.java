/*
 * Author: Jim M. Wilson
 * Created: Sep 3, 2008
 * $Date: 2009-12-24 00:27:33 -0600 (Thu, 24 Dec 2009) $: - $Revision: 4453 $: 
 */

package com.ncsts.view.util;

import java.beans.PropertyDescriptor;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

import com.ncsts.common.CacheManager;
import com.ncsts.common.LoggerFactory;
import com.ncsts.domain.BCPBillTransaction;
import com.ncsts.domain.BatchErrorLog;
import com.ncsts.domain.DataDefinitionColumn;
import com.ncsts.domain.DriverNames;
import com.ncsts.domain.ImportDefinition;
import com.ncsts.domain.ImportDefinitionDetail;
import com.ncsts.domain.ListCodes;
import com.ncsts.dto.ImportDefDTO;
import com.ncsts.dto.ImportMapProcessPreferenceDTO;
import com.ncsts.exception.DateFormatException;
import com.seconddecimal.billing.util.ArithmeticUtils;

/**
 * Stolen from STSWeb FileImportTask.java
 */
public class ImportMapSaleUploadParser implements Iterator<BCPBillTransaction> {
	private Logger logger = LoggerFactory.getInstance().getLogger(
			ImportMapSaleUploadParser.class);

	private ImportMapProcessPreferenceDTO preferences;
	private BufferedReader bufferedReader;
	private String nextLine;
	private int lineNumber;
	private int linesWritten;
	private long fileSize;
	private long estimatedRows;
	private long bytesRead;
	private long startTime;
	private double computedTotalAmount = 0.0;
	private List<BatchErrorLog> batchErrorLogs;
	private List<String> errors;
	private int maxErrorSeen;
	private boolean hitMaxErrors;
	private int errorCount;
	private int errorCountTotal;
	private boolean abort;
	private boolean completed;

	private ImportDefinition spec;
	private String uploadPath;
	private Map<String, Integer> headers = null;
	private List<String> headerNames = null;
	private Map<String, ArrayList<ImportDefinitionDetail>> importMap;

	private Map<String, PropertyDescriptor> columnProperties;
	private CacheManager cacheManager;
	private SimpleDateFormat formatter;

	private Map<String, DriverNames> transactionColumnDriverNamesMap;
	private Map<String, DataDefinitionColumn> bCPTransactionsColumnMap;
	private Map<String, ListCodes> errorListCodes;

	public void setTransactionColumnDriverNamesMap(
			Map<String, DriverNames> transactionColumnDriverNamesMap) {
		this.transactionColumnDriverNamesMap = transactionColumnDriverNamesMap;
	}

	public void setBCPTransactionsColumnMap(
			Map<String, DataDefinitionColumn> bCPTransactionsColumnMap) {
		this.bCPTransactionsColumnMap = bCPTransactionsColumnMap;
	}

	public void setErrorListCodes(Map<String, ListCodes> errorListCodes) {
		this.errorListCodes = errorListCodes;
	}

	public ImportMapSaleUploadParser(ImportDefDTO spec) {
		this.spec = new ImportDefinition(spec);
	}

	public ImportMapSaleUploadParser(ImportDefinition spec) {
		this.spec = spec;
	}

	public ImportMapSaleUploadParser(ImportDefinition spec,
			ImportMapProcessPreferenceDTO preferences)
			throws DateFormatException {
		this(spec);
		setPreferences(preferences);
	}

	public String getFilePath() {
		if ((uploadPath == null)
				|| spec.getSampleFileName().contains(File.separator)) {
			return spec.getSampleFileName();
		} else {
			return this.uploadPath + File.separator + spec.getSampleFileName();
		}
	}

	public List<String> getHeaderNames() {
		return headerNames;
	}

	public void parseFile() throws FileNotFoundException, IOException {
		headerNames = new ArrayList<String>();
		BufferedReader input = new BufferedReader(new FileReader(getFilePath()));
		this.fileSize = new File(getFilePath()).length();
		parseHeader(input.readLine());
		String firstLine = input.readLine();
		input.close();
	}

	public void parseHeader() throws FileNotFoundException, IOException {
		if (headerNames == null) {
			headerNames = new ArrayList<String>();
			BufferedReader input = null;
			input = new BufferedReader(new FileReader(getFilePath()));
			String line = input.readLine();
			input.close();
			parseHeader(line);
		}
	}

	public void parseHeader(String line) throws FileNotFoundException,
			IOException {
		bytesRead = line.length() + getLineEnd();
		if (spec.hasHeaders()) {
			headerNames = split(line);
		} else {
			headerNames = makeHeaderColumns(split(line).size());
		}
		headers = new HashMap<String, Integer>();
		for (int i = 0; i < headerNames.size(); i++) {
			headers.put(headerNames.get(i), i);
		}
		this.estimatedRows = this.fileSize / line.length();
	}

	public boolean hasNext() {
		if (abort || (bufferedReader == null))
			return false;
		try {
			nextLine = bufferedReader.readLine();
			if (nextLine == null) {
				bufferedReader.close();
				bufferedReader = null;
				return false;
			}
			bytesRead += nextLine.length() + getLineEnd();

			String endOfFileMarker = "";
			if (preferences.getEndOfFileMarker() != null
					&& preferences.getEndOfFileMarker().length() > 0) {
				endOfFileMarker = preferences.getEndOfFileMarker();
			}

			if (endOfFileMarker.length() == 0) {
				return true;
			}

			return !nextLine.startsWith(endOfFileMarker);
		} catch (IOException e) {
			errors.add(e.getMessage());
			return false;
		}
	}

	public void remove() {
		// not implemented
	}

	private void openReader() throws IOException {
		FileInputStream inputStream = new FileInputStream(getFilePath());
		bufferedReader = new BufferedReader(new InputStreamReader(inputStream,
				"ISO-8859-1"));
		this.fileSize = new File(getFilePath()).length();
	}

	public void closeReader() {
		if (bufferedReader != null) {
			try {
				bufferedReader.close();
			} catch (Exception e) {
			}
			bufferedReader = null;
		}
	}

	private void initReader() throws IOException {
		openReader();
		bufferedReader.mark(5000);
		parseHeader(bufferedReader.readLine());
		if (spec.hasHeaders()) {
			lineNumber = 2;
		} else {
			bufferedReader.reset();
			lineNumber = 1;
		}
	}

	public Double getProgress() {
		if (lineNumber < 1)
			return -0.01;
		if (abort)
			return 101.0;
		try {
			logger.debug("Line: " + linesWritten + ", lines: " + estimatedRows
					+ ", " + bytesRead / new Double(fileSize));
			return bytesRead / new Double(fileSize);
		} catch (Exception e) {
			return -0.01;
		}
	}

	public Iterator<BCPBillTransaction> iterator() {
		computedTotalAmount = 0.0;
		linesWritten = 0;
		abort = false;
		maxErrorSeen = 0;
		hitMaxErrors = false;
		errorCount = 0;
		errorCountTotal = 0;
		nextLine = null;
		bytesRead = 0;
		startTime = System.currentTimeMillis();
		batchErrorLogs = new ArrayList<BatchErrorLog>();
		errors = new ArrayList<String>();
		try {
			initReader();
		} catch (IOException e) {
			logger.error(e.getMessage(), e);
			errors.add(e.getMessage());
		}
		return this;
	}

	public BCPBillTransaction next() {
		if ((bufferedReader == null) || (nextLine == null))
			return null;
		return nextRecord(nextLine);
	}

	public BCPBillTransaction nextRecord(String line) {
		BCPBillTransaction td = new BCPBillTransaction();
		List<String> values = split(line);

		if (headerNames.size() != values.size()) {
			addError(new BatchErrorLog("IP1", lineNumber, line,
					"Batch Column Count", values.size() + ""));
		}

		boolean writeOnError = true;
		for (int i = 0; i < values.size(); i++) {
			if (i >= headerNames.size()) {
				// already caught error at batch column count above
				continue;
			}

			// 4850 For mapping a header to multiple transaction columns.
			ArrayList<ImportDefinitionDetail> columnArray = this.importMap
					.get(headerNames.get(i).toUpperCase());
			if (columnArray == null) {
				continue;
			}

			List<ImportDefinitionDetail> columnList = columnArray;
			for (ImportDefinitionDetail detail : columnList) {

				// ImportDefinitionDetail detail =
				// importMap.get(headerNames.get(i));
				if (detail == null) {
					continue;
				}
				String mappedCol = detail.getTransDtlColumnName();

				// truncate data to max length of column
				DataDefinitionColumn ddc = bCPTransactionsColumnMap
						.get(mappedCol);

				int maxLen = ddc.getDatalength().intValue();
				String val = "";
				if (values.get(i) != null) {
					val = values.get(i).trim();
				}
				if (ddc.getDataType().startsWith("VARCHAR")
						&& (val.length() > maxLen)) {
					val = val.substring(0, maxLen - 1);
				}

				// major magic here...
				// convert the string to its class
				// then find the setter method from the column name and just
				// call it.
				// piece of cake.
				try {
					Object o = convert(val.trim(),
							columnProperties.get(mappedCol).getPropertyType());

					if (o == null) {
						// Don't do anything
						continue;
					}

					// do the import mapping
					if (o instanceof String) {
						o = substitute((String) o, mappedCol);

						// Convert to upper-case for all drivers
						if (transactionColumnDriverNamesMap
								.containsKey(mappedCol)) {
							o = ((String) o).toUpperCase();
						}
					}

					columnProperties.get(mappedCol).getWriteMethod()
							.invoke(td, o);
				} catch (Exception e) {
					logger.error(e);
					BatchErrorLog bel = new BatchErrorLog();
					bel.setColumnNo(new Long(i + 1));
					bel.setErrorDefCode("IP2");
					bel.setRowNo(new Long(lineNumber));
					bel.setImportHeaderColumn(headerNames.get(i).toUpperCase());
					bel.setImportColumnValue(val);
					bel.setTransDtlColumnName(mappedCol);
					bel.setImportRow(line);
					bel.setTransDtlDatatype(ddc.getDataType());
					if (!addError(bel))
						writeOnError = false;
				}
			}
		}
		//td.setUpdateTimestamp(new Date());
		update(td);
		lineNumber++;
		logger.debug("On line: " + lineNumber + ", errors:" + errorCount);
		return writeOnError ? td : null;
	}

	private Object convert(String s, Class<?> clazz) throws ParseException {
		if ((s == null) || (s.length() == 0)) {
			return null;
		}

		// 0001417: Import with comma
		String preValue = s;
		if (preValue.contains(",")) {
			if ((clazz == Long.class) || (clazz == Double.class)
					|| (clazz == Integer.class)) {
				preValue = preValue.replaceAll(",", "");
			}
		}

		if (clazz == Long.class) {
			return Long.valueOf(preValue);
		} else if (clazz == Double.class) {
			return Double.valueOf(preValue);
		} else if (clazz == BigDecimal.class) {			
			return ArithmeticUtils.toBigDecimal(preValue);
		} else if (clazz == Integer.class) {
			return Integer.valueOf(preValue);
		} else if (clazz == java.util.Date.class) {
			if ((preValue != null) && (preValue.length() > 0)) {
				return formatter.parse(preValue);
			} else {
				return null;
			}
		}
		return new java.lang.String(preValue);
	}

	public List<List<String>> parseLines(int max) throws IOException {
		List<List<String>> list = new ArrayList<List<String>>();
		try {
			initReader();
			String line;
			int i = 0;
			while ((line = bufferedReader.readLine()) != null) {
				if (isEOF(line))
					break;
				if (i++ < max)
					list.add(split(line));
			}
		} finally {
			bufferedReader.close();
		}
		return list;
	}

	public List<String> readLines(int max) throws IOException {
		List<String> list = new ArrayList<String>();
		String line;
		int i = 0;
		try {
			openReader();
			while (((line = bufferedReader.readLine()) != null)) {
				if (isEOF(line))
					break;
				if (i++ < max)
					list.add(line);
			}
		} finally {
			bufferedReader.close();
		}
		// add in the footer
		if (i >= max)
			list.add(".....");
		if (line != null)
			list.add(line);
		return list;
	}

	public void update(BCPBillTransaction transactionDetail) {
		if (transactionDetail.getGlLineItmDistAmt() != null) {
			computedTotalAmount = ArithmeticUtils.add(computedTotalAmount,
					transactionDetail.getGlLineItmDistAmt().doubleValue()).doubleValue();
		}
	}

	/**
	 * This method makes the artificial Column name if the import data file does
	 * not come with the expected headers. Beware, there is a check box (flag)
	 * in the import definition page to let the user confirm if headers come
	 * with the file.
	 * 
	 * @param arraySize
	 *            int
	 * @return String[]
	 */
	private List<String> makeHeaderColumns(int arraySize) {
		List<String> headers = new ArrayList<String>();
		NumberFormat nf = NumberFormat.getIntegerInstance();
		for (int i = 0; i < arraySize; i++) {
			headers.add("COLUMN_" + nf.format(i));
		}
		return headers;
	}

	/**
	 * Don't validate EOF if we are aborting
	 */
	public void checksum() {
		if (!abort)
			processLastLine(nextLine);
	}

	private void processLastLine(String line) throws NumberFormatException,
			NoSuchElementException {

		String endOfFileMarker = "";
		String batchCountMarker = "";
		String batchTotalMarker = "";
		if (preferences.getEndOfFileMarker() != null
				&& preferences.getEndOfFileMarker().length() > 0) {
			endOfFileMarker = preferences.getEndOfFileMarker();
		}
		if (preferences.getBatchCountMarker() != null
				&& preferences.getBatchCountMarker().length() > 0) {
			batchCountMarker = preferences.getBatchCountMarker();
		}
		if (preferences.getBatchTotalMarker() != null
				&& preferences.getBatchTotalMarker().length() > 0) {
			batchTotalMarker = preferences.getBatchTotalMarker();
		}

		String lastLine = (line != null) ? line.trim() : "";
		boolean sawEOF = false;

		// If no end of file marker
		if (endOfFileMarker.length() == 0) {
			// If an end-of-file marker is not defined in Preferences, then it
			// should not be an "Bad or Missing..." error to
			// have one in the file.
			return;
		}

		// Search endOfFileMarker
		if (endOfFileMarker.length() > 0
				&& lastLine.indexOf(endOfFileMarker) != -1) {
			sawEOF = true;
			if (lastLine.indexOf(endOfFileMarker) + endOfFileMarker.length() < lastLine
					.length()) {
				lastLine = lastLine.substring(
						lastLine.indexOf(endOfFileMarker)
								+ endOfFileMarker.length()).trim();
			}
		}

		// Search batchCountMarker
		if (batchCountMarker.length() > 0
				&& lastLine.indexOf(batchCountMarker) != -1) {
			String batchCount = "";
			int batchCountMarkerFirstIndex = lastLine.indexOf(batchCountMarker);
			int batchCountMarkerLastIndex = batchCountMarkerFirstIndex;

			for (int i = batchCountMarkerFirstIndex + batchCountMarker.length(); i < lastLine
					.length(); i++) {
				if ((lastLine.charAt(i) >= 48 && lastLine.charAt(i) <= 57)
						|| lastLine.charAt(i) == 44) {
					batchCount = batchCount + lastLine.charAt(i);
					batchCountMarkerLastIndex = i;
				} else {
					break;
				}
			}

			System.out.println("batchCount: " + batchCount);

			Integer totalRows = null;
			if (batchCount.length() > 0) {
				try {
					System.out
							.println("###########     batchCount without comma: "
									+ batchCount.replaceAll(",", ""));
					totalRows = Integer
							.parseInt(batchCount.replaceAll(",", ""));
				} catch (Exception e) {
				}
			}

			int headerLines = spec.hasHeaders() ? 1 : 0;
			if ((totalRows != null)
					&& ((lineNumber - 1 - headerLines) != totalRows)) {
				addError(new BatchErrorLog("IP7", lineNumber, line,
						"Batch Import Line Count Marker",
						(lineNumber - 1 - headerLines) + ""));
			}

			// In order to read sequential for the same batchCountMarker and
			// batchTotalMarker
			if (batchTotalMarker.equals(batchCountMarker)) {
				if (batchCountMarkerLastIndex + 1 < lastLine.length()) {
					lastLine = lastLine
							.substring(batchCountMarkerLastIndex + 1);
				} else {
					lastLine = "";
				}
			}
		} else if (batchCountMarker.length() > 0) {
			addError(new BatchErrorLog("IP7", lineNumber, line,
					"Batch Import Line Count Marker", null));
		}

		// Search batchTotalMarker
		if (batchTotalMarker.length() > 0
				&& lastLine.indexOf(batchTotalMarker) != -1) {
			String batchTotal = "";
			int batchTotalMarkerFirstIndex = lastLine.indexOf(batchTotalMarker);
			int batchTotalMarkerLastIndex = batchTotalMarkerFirstIndex;

			// Also search for +(43), -(45), ,(44), and .(46)
			boolean gotDot = false;
			for (int i = batchTotalMarkerFirstIndex + batchTotalMarker.length(); i < lastLine
					.length(); i++) {

				// Check for +, - signs
				if (lastLine.charAt(i) == 43 || lastLine.charAt(i) == 45) {
					if (i == (batchTotalMarkerFirstIndex + batchTotalMarker
							.length())) {
						batchTotal = batchTotal + lastLine.charAt(i);
						batchTotalMarkerLastIndex = i;
						continue;
					} else {
						break;
					}
				}

				// Check for . sign
				if (lastLine.charAt(i) == 46) {
					if (!gotDot) {
						batchTotal = batchTotal + lastLine.charAt(i);
						batchTotalMarkerLastIndex = i;
						gotDot = true;
						continue;
					} else {
						break;
					}
				}

				if ((lastLine.charAt(i) >= 48 && lastLine.charAt(i) <= 57)
						|| lastLine.charAt(i) == 43 || lastLine.charAt(i) == 45
						|| lastLine.charAt(i) == 46 || lastLine.charAt(i) == 44) {

					batchTotal = batchTotal + lastLine.charAt(i);
					batchTotalMarkerLastIndex = i;
					continue;
				} else {
					break;
				}
			}

			Double totalAmount = null;
			if (batchTotal.trim().length() > 0) {
				try {
					System.out
							.println("###########     batchTotal without comma: "
									+ batchTotal.trim().replaceAll(",", ""));
					totalAmount = Double.parseDouble(batchTotal.trim()
							.replaceAll(",", ""));
				} catch (Exception e) {
				}
			}

			// 5252
			if (totalAmount != null) {
				String importAmount = batchTotal.trim().replaceAll(",", "");

				int lastDot = importAmount.lastIndexOf(".");
				int decimalDigit = 0;
				if (lastDot >= 0) {
					decimalDigit = (importAmount.length() - 1) - lastDot;
				}

				double computedTotalAmountNew = computedTotalAmount;
				double around = Double.parseDouble("1.0E" + decimalDigit);
				computedTotalAmountNew = Math.round(computedTotalAmountNew
						* around)
						/ around;

				System.out.println("###########     import file totalAmount: "
						+ totalAmount.doubleValue());
				System.out.println("###########  actual computedTotalAmount: "
						+ computedTotalAmount);
				System.out.println("########### rounded computedTotalAmount: "
						+ computedTotalAmountNew);

				if (totalAmount.doubleValue() != computedTotalAmountNew) {
					NumberFormat nf = NumberFormat.getNumberInstance();
					nf.setMaximumFractionDigits(decimalDigit);
					nf.setMinimumFractionDigits(decimalDigit);
					nf.setGroupingUsed(false);
					String computedTotalAmountStr = nf
							.format(computedTotalAmountNew);

					addError(new BatchErrorLog("IP8", lineNumber, line,
							"Batch Total Marker", computedTotalAmountStr));
				}
			}

			// 5252
			// if ((totalAmount != null) && (totalAmount.doubleValue() !=
			// computedTotalAmount)) {
			// addError(new BatchErrorLog("IP8", lineNumber, line,
			// "Batch Total Marker", nf.format(computedTotalAmount)));
			// }
		} else if (batchTotalMarker.length() > 0) {
			addError(new BatchErrorLog("IP8", lineNumber, line,
					"Batch Total Marker", null));
		}

		if (endOfFileMarker.length() > 0 && !sawEOF) {
			addError(new BatchErrorLog("IP6", lineNumber, line,
					"Batch End-of-File Marker", null));
		}
	}

	private String parserSelectClause(String sClause) {
		String returnValue = "";
		if ((sClause.charAt(0) == '\'' || sClause.charAt(0) == '"')
				&& (sClause.charAt(sClause.length() - 1) == '\'' || sClause
						.charAt(sClause.length() - 1) == '"')) {// Get the Quote
																// String
			returnValue = sClause.substring(1, sClause.length() - 1);
		} else if (sClause.toUpperCase().startsWith("IF")
				|| sClause.toUpperCase().startsWith("DECODE")) {
			returnValue = sClause.trim();
		} else if (sClause.toUpperCase().startsWith("IIF")) {
			// different syntax from "IF"
		} else if (sClause.toUpperCase().startsWith("UPPER")) {
			returnValue = sClause.trim();
		} else if (sClause.toUpperCase().startsWith("LOWER")) {
			returnValue = sClause.trim();
		} else {
			returnValue = sClause.trim();
		}
		return returnValue;
	}

	// // Call method to process the databean with Select Clauses
	// processSelectClause(columnNameList,
	// MappedColumnDatabeanMap);
	//
	// DataRow bcpRow = new DataRow(bcpDataSet);
	// HashMap aRowData = formMappedDatabeans(
	// MappedColumnDatabeanMap);
	// }
	// if (!errorHandler.getWriteImportLineFlag()) {
	// // bcpDataSet.addRow(bcpRow);
	// /*
	// * logger.info( "This Row can not be imported because
	// * the WriteImportLineFlag is false.\n" + line);
	// */
	// continue;
	// }
	//

	// private void processSelectClause(java.util.List columnNameList, HashMap
	// MappedColumnDatabeanMap){
	// int count = columnNameList.size();
	// String keyColumn = null;
	// String selectClause = null;
	// String testValue = null;
	// //HashMap decodeList = new HashMap();
	// try{
	// while (count > 0) {
	// for (int i = 0; i < columnNameList.size(); i++) {
	// keyColumn = (String) columnNameList.get(i);
	// HeaderNameValueBean databean = (HeaderNameValueBean)
	// MappedColumnDatabeanMap.get(
	// keyColumn);
	// selectClause = databean.getSelectClause().trim();
	// if(selectClause == null) continue;
	// //System.out.println(keyColumn+" <--clause is--> "+selectClause);
	// if (selectClause.toUpperCase().startsWith("UPPER"))
	// {//Hanging_For_Next_Turn
	// if(isAColumnRelyonAnother(getLowerUpperParameter(selectClause),
	// columnNameList)){
	// continue;
	// }else{
	// testValue = getValue(
	// MappedColumnDatabeanMap, selectClause, 1);
	//
	// databean.setValueBySelectClause(testValue);
	// count--;
	// columnNameList.remove(keyColumn);
	// }
	// } else if (selectClause.toUpperCase().startsWith("LOWER")) {
	// if(isAColumnRelyonAnother(getLowerUpperParameter(selectClause),
	// columnNameList)){
	// continue;
	// }else{
	// testValue = getValue(
	// MappedColumnDatabeanMap, selectClause, 0);
	//
	// databean.setValueBySelectClause(testValue);
	// count--;
	// columnNameList.remove(keyColumn);
	// }
	// } else if (selectClause.toUpperCase().startsWith("DECODE")) {
	// //Set a linked List if there relationship between these databean DECODE
	// select clause
	// //decodeList.put(keyColumn, databean);
	// if(isStillHasActiveColumn(selectClause, columnNameList)){
	// continue;
	// }else{
	// testValue = parserDecode(selectClause,
	// MappedColumnDatabeanMap);
	// databean.setValueBySelectClause(testValue);
	// count--;
	// columnNameList.remove(keyColumn);
	// }
	// } else { //Regulor hardcode String Value
	// String expression = null;
	// if (selectClause.trim().startsWith("'") ||
	// selectClause.trim().startsWith("\"")) {
	// expression = selectClause.
	// substring(1, selectClause.trim().length() - 1);
	// }else{
	// expression = selectClause.trim();
	// }
	// if(isAColumnRelyonAnother(expression, columnNameList)){
	// continue;
	// }else if(isAColumn(expression)){
	// testValue = getDatabeanValueField(expression, MappedColumnDatabeanMap);
	// //System.out.println("Set hardcode value for bean ::"+testValue);
	// databean.setValueBySelectClause(testValue);
	// count--;
	// columnNameList.remove(keyColumn);
	//
	// } else {
	// //System.out.println("Set hardcode value for bean ::"+expression);
	// databean.setValueBySelectClause(expression);
	// count--;
	// columnNameList.remove(keyColumn);
	// }
	// }
	// //count--;
	// }
	// }
	// }catch(Exception e){
	// e.printStackTrace();
	// }
	// //processDecodeList( decodeList, MappedColumnDatabeanMap );
	// if(columnNameList.size()>0 || count != 0){
	// //System.out.println("There are still some not found match DECODE existing ::"
	// +columnNameList.size());
	// }else{
	// //System.out.println(">>>>>> All select caluse (DECODE) value were checked and found.........");
	// }
	// }

	// private boolean isAColumnRelyonAnother(String keyColumn, java.util.List
	// selectClauseKeyList){
	// if(selectClauseKeyList.contains(keyColumn.toUpperCase())){
	// //System.out.println(keyColumn.toUpperCase() + " is in the List");
	// return true;
	// }
	// return false;
	// }
	//
	// private boolean isStillHasActiveColumn(String decodeExpression,
	// java.util.List keyColumnList){
	// int startIndex = "decode(".length();
	// int endIndex = decodeExpression.length()-1;//Skip ')' character
	// String expression = decodeExpression.substring(startIndex, endIndex);
	// String[] elements = expression.split(",");
	// for(int i=0; i<elements.length; i++){
	// elements[i] = elements[i].trim();
	// if((elements[i].startsWith("'") || elements[i].startsWith("\"")) &&
	// (elements[i].endsWith("'") || elements[i].endsWith("\""))){
	// elements[i] = elements[i].substring(1, elements[i].length()-1).trim();
	// }
	// if(keyColumnList.contains(elements[i].toUpperCase())){
	// //System.out.println(elements[i].toUpperCase() + " is in the List");
	// return true;
	// }
	// }
	// return false;
	// }

	/**
	 * Beware of there is a recurserve call in the following two methods.
	 * 
	 * @param decodeExpression
	 *            String
	 * @param MappedColumnDatabeanMap
	 *            HashMap
	 * @return String
	 */
	// private String parserDecode(String decodeExpression, HashMap
	// MappedColumnDatabeanMap){
	// //System.out.println("*******************************\n"+decodeExpression);
	// int startIndex = "decode(".length();
	// int endIndex = decodeExpression.length()-1;
	// String expression = decodeExpression.substring(startIndex, endIndex);
	// //System.out.println("---->"+expression);
	// String[] elements = expression.split(",");
	// for(int i=0; i<elements.length; i++){
	// elements[i] = elements[i].trim();
	// if((elements[i].startsWith("'") || elements[i].startsWith("\"")) &&
	// (elements[i].endsWith("'") || elements[i].endsWith("\""))){
	// elements[i] = elements[i].substring(1, elements[i].length()-1).trim();
	// }
	// }
	// int len = elements.length;
	// String returnValue = elements[len-1];//Last element is the default value
	// String conditionValue = elements[0];
	// //System.out.println("The condition value is :: "+conditionValue);
	// String tempValue = null;
	// if(isAColumn(conditionValue)){
	// //System.out.println(conditionValue + " is a column....." );
	// tempValue = getDatabeanValueField(conditionValue,
	// MappedColumnDatabeanMap);
	// if(!tempValue.equals("Hanging_For_Next_Turn")){
	// conditionValue = tempValue;
	// //System.out.println("The value of this column is ::"+conditionValue);
	// }else{
	// //System.out.println("Waiting for next turn......");
	// }
	// }
	// for(int i=1; i<len-1; i=i+2){
	// //If there are columns appeared here process it
	// //System.out.println("\nTry to match condition value of ( "+conditionValue+" ) with ( "+
	// elements[i] +" )");
	// if(isAColumn( elements[i] )){
	// tempValue = getDatabeanValueField(elements[i], MappedColumnDatabeanMap);
	// if(!tempValue.equals("Hanging_For_Next_Turn")){
	// elements[i] = tempValue;
	// }
	// }
	// if(conditionValue.equals( elements[i] )){
	// returnValue = elements[i+1];
	// if(isAColumn(returnValue)){
	// returnValue = getDatabeanValueField(returnValue,
	// MappedColumnDatabeanMap);
	// }
	// break;
	// }
	// }
	// //System.out.println(returnValue+"\n*******************************");
	// return returnValue.trim();
	// }
	//
	// private String getDatabeanValueField(String conditionValue, HashMap
	// MappedColumnDatabeanMap){
	// HeaderNameValueBean bean =
	// (HeaderNameValueBean)MappedColumnDatabeanMap.get( getMapKey(
	// conditionValue ));
	// if(bean != null && bean.getValueFIeld() != null){//Take the value as it
	// is
	// //System.out.println("1");
	// return bean.getValueFIeld().trim();
	// }else if(bean != null && bean.getValueFIeld() == null &&
	// bean.getSelectClause().trim().toUpperCase().startsWith("DECODE")){//Meet
	// a nested DECODE
	// //if(bean != null && bean.getValueFIeld() == null &&
	// bean.getSelectClause().trim().length() > 0){
	// //if(bean != null && bean.getValueFIeld() == null &&
	// bean.getSelectClause().trim().toUpperCase().startsWith("DECODE")){//Meet
	// a nested DECODE
	// //System.out.println("2");
	// return parserDecode(bean.getSelectClause().trim(),
	// MappedColumnDatabeanMap);
	// //return "Hanging_For_Next_Turn";
	// }else{
	// //System.out.println("3");
	// return "Hanging_For_Next_Turn";
	// }
	// }

	private String getLowerUpperParameter(String selectClause) {
		return selectClause.substring(6, selectClause.trim().length() - 1);
	}

	// private String getValue(HashMap MappedColumnDatabeanMap, String
	// selectClause, int flag){
	// String selectClauseValue = selectClause.substring(6,
	// selectClause.trim().length()-1);//Take out the parameter value
	// //System.out.println("The found value of select clause is ::"+
	// selectClauseValue);
	// String returnValue = null;
	// if(isAColumn(selectClauseValue)){
	// HeaderNameValueBean bean =
	// (HeaderNameValueBean)MappedColumnDatabeanMap.get(getMapKey(selectClauseValue));
	// if(bean.getValueFIeld() == null && bean.getSelectClause() != null){
	// return "Hanging_For_Next_Turn";
	// }
	// if(flag == 1){
	// returnValue = bean.getValueFIeld().toUpperCase();
	// }else{
	// returnValue = bean.getValueFIeld().toLowerCase();
	// }
	// }else{
	// if(flag == 1){
	// returnValue = selectClauseValue.toUpperCase();
	// }else{
	// returnValue = selectClauseValue.toLowerCase();
	// }
	// }
	// //System.out.println("Return value for this select clause is ::"+returnValue);
	// return returnValue;
	// }
	//
	// private boolean isAColumn(String value){
	// boolean returnValue = false;
	// for(int i=0; i<mappedTransDTLColumn.length; i++){
	// if(value.equalsIgnoreCase(mappedTransDTLColumn[i])){
	// //System.out.println(value +
	// " is a Column Name....find column value for it.....");
	// returnValue = true;
	// break;
	// }
	// }
	// return returnValue;
	// }
	//

	/**
	 * add the error and set abort if necessary also set the max error seen and
	 * the count of actual errors
	 */
	public boolean addError(BatchErrorLog errorLog) {

		batchErrorLogs.add(errorLog);
		errorCountTotal++;

		ListCodes listCode = errorListCodes.get(errorLog.getErrorDefCode());

		maxErrorSeen = listCode.getSeverityLevelAsInt() > maxErrorSeen ? listCode
				.getSeverityLevelAsInt() : maxErrorSeen;
		if (!listCode.isWarning())
			errorCount++;

		if (listCode.isAbortEnabled()) {
			abort = true;
		}
		if ((!hitMaxErrors) && (errorCount >= preferences.getMaxErrors())) {
			BatchErrorLog maxErrorLog = new BatchErrorLog();
			maxErrorLog.setErrorDefCode("IP3");
			batchErrorLogs.add(maxErrorLog);
			errorCountTotal++;
			errorCount++;
			hitMaxErrors = true;
			if (errorListCodes.get("IP3").isAbortEnabled()) {
				abort = true;
			}

			// Assign maxErrorSeen again
			listCode = errorListCodes.get(maxErrorLog.getErrorDefCode());
			maxErrorSeen = listCode.getSeverityLevelAsInt() > maxErrorSeen ? listCode
					.getSeverityLevelAsInt() : maxErrorSeen;
		}
		return listCode.isWriteImportLineEnabled();
	}

	public int getMaxErrorSeen() {
		return maxErrorSeen;
	}

	public List<String> split(String s) {
		List<String> list = new ArrayList<String>();
		String delims = spec.getFlatDelCharDecoded();
		if (spec.getFlatDelTextQual() != null)
			delims += spec.getFlatDelTextQual();
		StringTokenizer st = new StringTokenizer(s, delims, true);
		boolean sawDelim = true;
		try {
			while (st.hasMoreTokens()) {
				String t = st.nextToken(delims);
				if (t.equalsIgnoreCase(spec.getFlatDelCharDecoded())) {
					if (sawDelim) {
						list.add(null);
					}
					sawDelim = true;
				} else if (t.equalsIgnoreCase(spec.getFlatDelTextQual())) {
					list.add(st.nextToken(spec.getFlatDelTextQual()));
					st.nextToken(spec.getFlatDelTextQual()); // swallow trailing
																// delim
					sawDelim = false;
				} else {
					list.add(t);
					sawDelim = false;
				}
			}
		} catch (NoSuchElementException e) {
		}
		if (sawDelim) {
			list.add(null);
		}
		return list;
	}

	public String substitute(String s, String transCol) {
		String val = s;
		String markers = preferences.getDoNotImportMarkers();
		String replacement = preferences.getReplaceWithMarkers();

		if (markers == null) {
			return val;
		}

		if (transactionColumnDriverNamesMap.containsKey(transCol)
				&& (transactionColumnDriverNamesMap.get(transCol)
						.getDrvNamesCode().equalsIgnoreCase("T") || transactionColumnDriverNamesMap
						.get(transCol).getDrvNamesCode().equalsIgnoreCase("L"))) {
			for (int i = 0; i < markers.length(); i++) {
				String to = replacement.substring(i * 2, (i * 2) + 2);
				// if just spaces, then strip it
				if (to.trim().length() == 0)
					to = "";
				val = val.replaceAll(
						Pattern.quote(markers.substring(i, i + 1)),
						replacementCodes(to));
				logger.debug(s + " -> " + val);
			}
		}
		return val;
	}

	public String replacementCodes(String s) {
		if (s.equalsIgnoreCase("~s"))
			return " ";
		if (s.equalsIgnoreCase("~a"))
			return "\u00A4";
		if (s.equalsIgnoreCase("~b"))
			return "\u00A7";
		if (s.equalsIgnoreCase("~c"))
			return "\u00B7";
		if (s.equalsIgnoreCase("~d"))
			return "\u00A8";
		return s;
	}

	private boolean isEOF(String line) {
		String endOfFileMarker = "";
		if (preferences.getEndOfFileMarker() != null
				&& preferences.getEndOfFileMarker().length() > 0) {
			endOfFileMarker = preferences.getEndOfFileMarker();
		}

		if (endOfFileMarker.length() == 0) { // no EOF character defined? this
												// line can't EOF then :-)
			return false;
		}

		return line.startsWith(endOfFileMarker);

		// return line.startsWith(preferences.getEndOfFileMarker());
	}

	public List<String> getHeaders() {
		try {
			parseHeader();
		} catch (IOException e) {
		}
		return this.headerNames;
	}

	public void setImportDefinitionDetail(List<ImportDefinitionDetail> detail) {
		this.importMap = new HashMap<String, ArrayList<ImportDefinitionDetail>>();
		for (ImportDefinitionDetail d : detail) {
			// For mapping a header to multiple transaction columns.
			ArrayList<ImportDefinitionDetail> columnArray = this.importMap
					.get(d.getImportColumnName());
			if (columnArray == null) {
				columnArray = new ArrayList<ImportDefinitionDetail>();
				this.importMap.put(d.getImportColumnName(), columnArray);
			}
			columnArray.add(d);
		}
	}

	public ImportMapProcessPreferenceDTO getPreferences() {
		return this.preferences;
	}

	public void setPreferences(ImportMapProcessPreferenceDTO preferences)
			throws DateFormatException {
		this.preferences = preferences;
		if (preferences != null) {
			String format = preferences.getDefaultImportDateMap();
			if ((format == null || (format.trim().length() == 0)))
				format = "MM/dd/yyyy";
			formatter = DateHelper.getImportDateFormat(format);
		}
	}

	public long getFileSize() {
		if (fileSize == 0) {
			this.fileSize = new File(getFilePath()).length();
		}
		return this.fileSize;
	}

	public long getEstimatedRows() {
		return this.estimatedRows;
	}

	public String getUploadPath() {
		return this.uploadPath;
	}

	public void setUploadPath(String uploadPath) {
		this.uploadPath = uploadPath;
	}

	public Map<String, PropertyDescriptor> getColumnProperties() {
		return this.columnProperties;
	}

	public void setColumnProperties(
			Map<String, PropertyDescriptor> columnProperties) {
		this.columnProperties = columnProperties;
	}

	public double getComputedTotalAmount() {
		return this.computedTotalAmount;
	}

	public List<BatchErrorLog> getBatchErrorLogs() {
		return this.batchErrorLogs;
	}

	public void resetBatchErrorLogs() {
		if (batchErrorLogs != null) {
			batchErrorLogs.clear();
		}
		batchErrorLogs = new ArrayList<BatchErrorLog>();
		;
	}

	public int getLinesWritten() {
		return this.linesWritten;
	}

	public void setLinesWritten(int linesWritten) {
		this.linesWritten = linesWritten;
	}

	public int getTotalRows() {
		// subtract out the EOF marker
		return (spec.hasHeaders() ? lineNumber - 1 : lineNumber) - 1;
	}

	public void setCacheManager(CacheManager cacheManager) {
		this.cacheManager = cacheManager;
	}

	public boolean wasAborted() {
		return this.abort;
	}

	public void setAbort(boolean abort) {
		this.abort = abort;
	}

	public void setCompleted(boolean completed) {
		this.completed = completed;
	}

	public boolean wasCompleted() {
		return this.completed;
	}

	public int getErrorCount() {
		return this.errorCount;
	}

	public int getErrorCountTotal() {
		return this.errorCountTotal;
	}

	public int getLineEnd() {
		return System.getProperty("line.separator").length();
	}
}
