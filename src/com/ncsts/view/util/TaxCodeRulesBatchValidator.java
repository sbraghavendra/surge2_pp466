package com.ncsts.view.util;

import com.ncsts.domain.BatchMaintenance;
import com.ncsts.domain.TaxCodeRulesBatch;
import com.ncsts.dto.ImportMapProcessPreferenceDTO;
import com.ncsts.jsf.model.BatchMaintenanceDataModel;

public class TaxCodeRulesBatchValidator {
	private BatchMaintenanceDataModel dataModel;
	private TaxCodeRulesBatch current;
	
	public TaxCodeRulesBatchValidator(BatchMaintenanceDataModel dataModel, ImportMapProcessPreferenceDTO importMapProcessPreferenceDTO) {
		this.dataModel = dataModel;
		
		current = new TaxCodeRulesBatch();
		current.setReleaseVersion(importMapProcessPreferenceDTO.getLastTaxCodeRulesRelUpdateAsInt());
		current.setReleaseDate(importMapProcessPreferenceDTO.getLastTaxCodeRulesDateUpdateAsDate());
		/* if there is a running batch, give that precedence */
		TaxCodeRulesBatch running = getRunningBatch();
		if (running != null) {
			current.setReleaseVersion(running.getReleaseVersion());
			current.setReleaseDate(running.getReleaseDate());
		}
	}
	
	private TaxCodeRulesBatch getRunningBatch() {
		BatchMaintenance example = new BatchMaintenance();
		example.setBatchStatusCode(TaxCodeRulesBatch.PROCESSING);
		example.setBatchTypeCode(TaxCodeRulesBatch.BATCH_CODE);
		BatchMaintenance bm = dataModel.getBatchMaintenanceDAO().find(example);
		if ((bm != null) && (bm.getBatchId() != null)) 
			return new TaxCodeRulesBatch(bm);
		else
			return null;
	}

	public TaxCodeRulesBatch validate(TaxCodeRulesBatch bu) {
		BatchMaintenance batch = bu.getBatchMaintenance();

		if (!bu.isRunning() && !bu.isFlagged() && !bu.isImported()) {
			bu.setError("Not an imported batch");
			return bu;
		}
		if (bu.isHeld()) {
			bu.setError("On Hold");
			return bu;
		}
		BatchMaintenance b = dataModel.getBatchMaintenanceDAO().getPreviousBatchInSequence(batch);
		if (b != null) {
			if (!bu.isNextInSequence(current)) {
				bu.setError("Batch is out of sequence");
				return bu;
			}
		}
		current = bu;
		return bu;
	}
}
