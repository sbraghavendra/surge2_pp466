/**
 * 
 */
package com.ncsts.view.util;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

import com.ncsts.domain.TransactionHeader;

// Class added for Ordering List. Bug 3987
public class TransactionHeaderConvertor implements Converter {

	public Object getAsObject(FacesContext context, UIComponent component,
			String value) {

		int index = value.indexOf(':');
		
		return new TransactionHeader(value.substring(0, index), value.substring(index + 1));
	}

public String getAsString(FacesContext context, UIComponent component,
			Object value) {

		TransactionHeader th = (TransactionHeader) value;
		return th.getColumnName() + ":" + th.getColumnLabel();
	}

}
