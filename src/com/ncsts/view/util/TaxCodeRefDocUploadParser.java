package com.ncsts.view.util;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.ncsts.common.CacheManager;
import com.ncsts.domain.BCPTaxCodeRefDoc;
import com.ncsts.domain.BatchErrorLog;
import com.ncsts.domain.ListCodes;
import com.ncsts.view.bean.UpdateFilesBackingBean;

public class TaxCodeRefDocUploadParser extends AbstractFileUploader implements
		Iterator<BCPTaxCodeRefDoc> {

	private Logger logger = Logger.getLogger(TaxCodeRefDocUploadParser.class);
	
	private String fileType;
	private Date releaseDate;
	private String releaseVersion;

	private int lineNumber;
	private int linesWritten;
	private String nextLine;

	private List<String> errors;
	private List<BatchErrorLog> batchErrorLogs;
	private BufferedReader bufferedReader;
	private boolean abort;
	private Map<String, String> errorListCodes = new LinkedHashMap<String,String>();
	
	private static final SimpleDateFormat sdf = new SimpleDateFormat(
			"MM/dd/yyyy");
	private static final SimpleDateFormat sdfRelease = new SimpleDateFormat(
			"yyyy_MM");


	private static final String[] COLUMNS = { "TAXCODE", "TAXCODE_COUNTRY_CODE", "TAXCODE_STATE_CODE", "TAXCODE_COUNTY", "TAXCODE_CITY",  
	                                          "TAXCODE_STJ", "EFFECTIVE_DATE", "CUSTOM_FLAG", "REFERENCE_DOCUMENT_CODE", "REFERENCE_TYPE_CODE",
	                                          "DESCRIPTION", "KEYWORDS", "DOCUMENT_NAME", "URL", "UPDATE_CODE"   };
	                                        
	
	private boolean processHeaderMissing; 
	
	private CacheManager cacheManager;
	
	public void setCacheManager(CacheManager cacheManager) {
		this.cacheManager = cacheManager;
	}
	
	public void setErrorListCodes(Map<String, String> errorListCodes) {
		this.errorListCodes = errorListCodes;
	}

	public boolean isprocessHeaderMissing() {
		return processHeaderMissing;
	}

	public void setprocessHeaderMissing(boolean processHeaderMissing) {
		this.processHeaderMissing = processHeaderMissing;
	}

	public TaxCodeRefDocUploadParser(String path) {
		super(path);
		deleteOnExit = true;
	}
	
	public void clear() {
		errors = new ArrayList<String>();
		batchErrorLogs = new ArrayList<BatchErrorLog>();
		this.fileType = null;
		this.releaseDate = null;
		this.releaseVersion = null;
		this.uploadFileName = null;
		this.uploadFilePath = null;
		this.totalBytes = 0;
		this.nextLine = null;
		this.linesInFile = 0;
		this.lineNumber = 0;
	}

	public String getUploadFileName() {
		return this.uploadFileName;
	}

	public String getFileType() {
		return this.fileType;
	}

	public Date getReleaseDate() {
		return this.releaseDate;
	}

	public String getReleaseVersion() {
		return this.releaseVersion;
	}

	public Double getProgress() {
		if (lineNumber < 1)
			return -0.01;
		try {
			logger.debug("Line: " + linesWritten + ", lines: " + linesInFile
					+ ", " + linesWritten / new Double(linesInFile));
			return linesWritten / new Double(linesInFile);
		} catch (Exception e) {
			return -0.01;
		}
	}

	public List<String> getErrors() {
		return this.errors;
	}

	private void addError(String s) {
		errors.add(s);
	}

	private void addError(int lineNumber, String line, String code) {
		addError(lineNumber, null, null, line, null, code);
	}

	private void addError(int lineNumber, Integer columnNumber, String column,
			String columnName, String line, String code) {
		BatchErrorLog errorLog = new BatchErrorLog();
		errorLog.setErrorDefCode(code);
		errorLog.setColumnNo(columnNumber == null ? null : new Long(
				columnNumber));
		errorLog.setImportColumnValue(column);
		errorLog.setTransDtlColumnName(columnName);
		errorLog.setImportRow(line);
		errorLog.setRowNo(new Long(lineNumber));
		batchErrorLogs.add(errorLog);
	}

	@Override
	public void processHeader(BufferedReader reader) throws IOException {
		String firstLine = reader.readLine();
		String ar[] = firstLine.split(",");
		
		try {
			String type = UpdateFilesBackingBean.getHeaderValueOfType(firstLine, UpdateFilesBackingBean.TYPE_VALUE);
			if(type==null) {
				addError(1, firstLine, "IP12");
				processHeaderMissing =  true;
				return;
			}
			this.fileType = type;
				
			String s = UpdateFilesBackingBean.getHeaderValueOfType(firstLine, UpdateFilesBackingBean.DATE_VALUE);
			if(s==null) {
				addError(1, firstLine, "IP12");
				processHeaderMissing =  true;
				return;
			}
			this.releaseDate = sdfRelease.parse(s);

			String release = UpdateFilesBackingBean.getHeaderValueOfType(firstLine, UpdateFilesBackingBean.REL_VALUE);
			if(release==null) {
				addError(1, firstLine, "IP12");
				processHeaderMissing =  true;
				return;
			}
			this.releaseVersion = release;
			
			String typecode = UpdateFilesBackingBean.getHeaderValueOfType(firstLine, UpdateFilesBackingBean.FILE_VALUE);
			if(typecode==null) {
				addError(1, firstLine, "IP12");
				processHeaderMissing =  true;
				return;
			}
			
		} catch (ParseException e) {
			addError(1, firstLine, "IP12");
			this.releaseDate = null;
			return; 
		}
	}

	public void upload(String fileName, InputStream stream) {
		clear();
		try {
			super.upload(fileName, stream);
		} catch (Exception e) {
			addError(1, e.getMessage(), "IP12");
			logger.error(e);
		}
	}

	@Override
	public boolean hasNext() {
		if (bufferedReader == null)
			return false;
		try {
			nextLine = bufferedReader.readLine();
			if (nextLine == null) {
				bufferedReader.close();
				bufferedReader = null;
				return false;
			}
			return nextLine.charAt(0) != '~';
		} catch (IOException e) {
			addError(e.getMessage());
			return false;
		}
	}

	@Override
	public BCPTaxCodeRefDoc next() {
		if ((bufferedReader == null) || (nextLine == null))
			return null;
		if ((lineNumber % 200) == 0) {
			logger.debug("Line: " + lineNumber + ", errorCount:"
					+ batchErrorLogs.size());
		}
		String tokens[] = nextLine.split("\\t");
		BCPTaxCodeRefDoc r = new BCPTaxCodeRefDoc();
		if (tokens.length != COLUMNS.length) {
			addError(lineNumber, null, "Expected " + COLUMNS.length
					+ ", actual: " + tokens.length, null, nextLine, "IP1");
		} else {
			int i = 0;
			String parsedColumn = null;
			try {
				r.setLine(new Long(lineNumber));
				r.setText(nextLine);
				
				String taxcode = tokens[i++];
				r.setTaxcode(taxcode);
				
				String country = tokens[i++]; 
				r.setTaxcodeCountryCode(country);
				
				String state = tokens[i++];
				r.setTaxcodeStateCode(state);
				
				String county = tokens[i++];
				r.setTaxcodeCounty(county);
				
				String city = tokens[i++];
				r.setTaxcodeCity(city);
				
				String stj = tokens[i++];
				r.setTaxcodeStj(stj);
				
				try {
					parsedColumn = tokens[i++];
					if(parsedColumn!=null && parsedColumn.trim().length()>0){
						r.setEffectiveDate(sdf.parse(parsedColumn));
					}
					else{
						String dateString = sdf.format(new Date());//today
						r.setEffectiveDate(sdf.parse(dateString)); 
					}
				}
				catch(Exception e) {
					r.setEffectiveDate(null);
					throw e;
				}
				
				r.setCustomFlag(parsedColumn = tokens[i++]);	
				r.setReferenceDocumentCode(parsedColumn = tokens[i++]);
				r.setReferenceTypeCode(parsedColumn = tokens[i++]);
				r.setDescription(parsedColumn = tokens[i++]);
				r.setKeywords(parsedColumn = tokens[i++]);
				r.setDocumentName(parsedColumn = tokens[i++]);
				r.setUrl(parsedColumn = tokens[i++]); 
				
				String updateCode = tokens[i++];
				r.setUpdateCode(updateCode);
				
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
				addError(lineNumber, i, parsedColumn, COLUMNS[i - 1], nextLine,
						"TCR23");
			}

			lineNumber++;
		}

		return r;
	}

	@Override
	public void remove() {
	}

	public Iterator<BCPTaxCodeRefDoc> iterator() {
		lineNumber = 2;
		linesWritten = 0;
		nextLine = null;
		try {
			bufferedReader = new BufferedReader(new FileReader(uploadFilePath));
		} catch (FileNotFoundException e) {
			logger.error(e.getMessage(), e);
			addError(e.getMessage());
		}
		return this;
	}
	
	public void resetBatchErrorLogs() {
		if(batchErrorLogs!=null){
			batchErrorLogs.clear();
		}
		batchErrorLogs = new ArrayList<BatchErrorLog>();;
	}

	public void checksum() {
		
		if (nextLine == null || nextLine.trim().length() == 0) {
			addIP6Error();
			return;
		}
		String footer[] = nextLine.split(" +");
		if(footer.length == 2) {
			if(footer[1].contains("#")) {
				Long lines = Long.decode((footer[1].replaceAll("\\#", "")).trim());
				if (lines != getRows()) {
					addIP4Error(lines);
				}
			}
			return;
		}
		
		if(footer.length == 3) {
			if(footer[2].contains("$")) {
				return;
			}
		}
		if(footer.length == 1) {
			addIP7Error(); 
			
		}
	}

	public void addIP4Error(Long lines) {
		String s = "EOF Marker: " + lines + ", actual count: " + getRows();
		String errorCodeDesc = errorListCodes.get("IP4");
		if(errorCodeDesc != null && errorCodeDesc.length()>0) {
			addError(lineNumber, null, s, errorCodeDesc, nextLine, "IP4");
		}
		else {
			addError(lineNumber, null, s, "IP4", nextLine, "IP4");
		}
	}

	public void addIP6Error() {
		String errorCodeDesc = errorListCodes.get("IP6");
		if(errorCodeDesc != null && errorCodeDesc.length()>0) {
			addError(lineNumber, null, errorCodeDesc, errorCodeDesc, "", "IP6");
		}
		else {
			addError(lineNumber, null, "IP6", "IP6", "", "IP6");
		}
	}

	public void addIP7Error() {
		String errorCodeDesc = errorListCodes.get("IP7");
		if(errorCodeDesc != null && errorCodeDesc.length()>0) {
			addError(lineNumber, null, errorCodeDesc, errorCodeDesc, nextLine,"IP7");
		}
		else {
			addError(lineNumber, null, "IP7", "IP7", nextLine,"IP7");
		}
	}

	public void addIP8Error() {
		String errorCodeDesc = errorListCodes.get("IP8");
		if(errorCodeDesc != null && errorCodeDesc.length()>0) {
			addError(lineNumber, null, errorCodeDesc, errorCodeDesc, nextLine, "IP8");
		}
		else {
			addError(lineNumber, null, "IP8", "IP8", nextLine,"IP8");
		}
	}
	
	public BigDecimal parseBigDecimal(String s) {
		if ((s == null) || (s.trim().length() == 0))
			return null;
		return new BigDecimal(s);
	}

	public Long getTotalBytes() {
		return this.totalBytes;
	}

	public List<BatchErrorLog> getBatchErrorLogs() {
		return this.batchErrorLogs;
	}

	public int getRows() {
		return lineNumber - 2;
	}

	public void setLinesWritten(int linesWritten) {
		this.linesWritten = linesWritten;
	}
	
	public boolean isAbort() {
		return abort;
	}

	public void setAbort(boolean abort) {
		this.abort = abort;
	}


}
