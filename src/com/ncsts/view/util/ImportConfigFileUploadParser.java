package com.ncsts.view.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.myfaces.custom.fileupload.HtmlInputFileUpload;
import org.springframework.beans.factory.annotation.Autowired;

import com.ncsts.domain.BatchErrorLog;
import com.ncsts.domain.BatchMaintenance;
import com.ncsts.domain.ListCodes;
import com.ncsts.domain.Option;
import com.ncsts.domain.OptionCodePK;
import com.ncsts.services.OptionService;
import com.ncsts.services.impl.ConfigImportServiceImpl;

public abstract class ImportConfigFileUploadParser extends AbstractFileUploader {
	

	
	@SuppressWarnings("unused")
	private Logger logger = Logger
			.getLogger(ImportConfigFileUploadParser.class);
	private String fileType;
	private Date releaseDate;
	private Double releaseVersion;

	private List<BatchErrorLog> batchErrorLogs;
	private List<String> errors;
	private int lineNumber;
	private long lineNumberTotal;
	private BufferedReader bufferedReader;
	private String nextLine;
	private int linesWritten;
	private int errorCountTotal;
	private int criticalErrorCount;
	private long totalBytes;
	private HashMap<String, ListCodes> knownErrors;
	private int errorTreshold;
	private boolean errorTresholdReached = false;
	
	
	public long getTotalBytes() {
		return totalBytes;
	}

	public void setTotalBytes(long totalBytes) {
		this.totalBytes = totalBytes;
	}

	private boolean abort;

	private static final SimpleDateFormat sdf = new SimpleDateFormat(
			"yyyy/MM/dd");

	public ImportConfigFileUploadParser(String uploadPath) {
		super(uploadPath);
		deleteOnExit = true;

	}

	@Override
	public void processHeader(BufferedReader reader) throws IOException {
	}
	
	
	public void upload(HtmlInputFileUpload uploadFile, InputStream stream, OptionService optionService) {
		
		Option delimiter = optionService.findByPK(new OptionCodePK("EXPORTDELIMITER", "SYSTEM", "SYSTEM"));
		
		
		
		if (delimiter==null || delimiter.getValue()==null || delimiter.getValue().equals("")) {
			addError("Delimiter is not set in the system. <br> Please contact pinpoint.support@ryan.com.");
			abort = true;
			return ;
		} 
		
		
		BufferedReader br = null;

		try {
			br = new BufferedReader(new InputStreamReader(stream));
			totalBytes = uploadFile.getUploadedFile().getBytes().length;
			// headerLine
			long headerLineLen = br.readLine().getBytes().length;
			
			// headerColumn names row
			String headerLine = "";
			while (headerLine.equalsIgnoreCase(""))
				headerLine = br.readLine();
			
			
			String strDelimiter = (ConfigImportServiceImpl.delimeterMap.containsKey(delimiter.getValue()))?ConfigImportServiceImpl.delimeterMap.get(delimiter.getValue()) : delimiter.getValue();
			
			
			if(!headerLine.contains(strDelimiter)) {
				addError(String.format("Delimiter (%s) not found in file.", strDelimiter));
				abort = true;
				return ;
			}
			
			long headerColLen = headerLine.getBytes().length;
			this.uploadFileName = uploadFile.getUploadedFile().getName();
			long rowNum = 0;
			totalBytes = totalBytes - (headerLineLen + headerColLen);
			long firstThreeRowsLen = 0;
			// to calculate the no of estimated rows in file
			// to calculate first three data rows length
			while (br != null && br.ready() && rowNum < 3) {
				
				String line = "";
				while (line.equalsIgnoreCase(""))
					line = br.readLine();
				
				if (!line.equals("")) {
				
					firstThreeRowsLen = firstThreeRowsLen
							+ line.getBytes().length;
					rowNum++;
				}

			}

			long avgBytesForEachRow = firstThreeRowsLen / rowNum;
			linesInFile = totalBytes / avgBytesForEachRow;
			this.lineNumberTotal = linesInFile;

		} catch (Exception e) {
			addError(e.getMessage());
			logger.error(e);
		} finally {
			try {
				if (br != null)
					br.close();
			} catch (IOException e) {
				e.printStackTrace();
				addError(e.getMessage());
				logger.debug("Exception while closing buffer reader");
			}

		}
	}

	public void clear() {
		errors = new ArrayList<String>();
		batchErrorLogs = new ArrayList<BatchErrorLog>();
		this.errorCountTotal = 0;
		this.criticalErrorCount = 0;
		this.errorTreshold = 0;
		this.errorTresholdReached = false;
		this.fileType = null;
		this.releaseDate = null;
		this.releaseVersion = null;
		this.uploadFileName = null;
		this.uploadFilePath = null;
		this.totalBytes = 0;
		this.nextLine = null;
		this.linesInFile = 0;
		this.lineNumber = 0;
		this.lineNumberTotal = 0;
		this.linesWritten = 0;
		this.abort = false;		
	}

	private void addError(String s) {
		errors.add(s);
	}

	public void addError(int lineNumber, String line, String code) {
		addError(lineNumber, null, null, null, line, code);
	}

	public void addError(int lineNumber, Integer columnNumber,
			String columnName, String columnValue, String line, String code) {
		BatchErrorLog errorLog = new BatchErrorLog();
		errorLog.setErrorDefCode(code);
		errorLog.setColumnNo(columnNumber == null ? null : new Long(
				columnNumber));
		errorLog.setImportColumnValue(columnValue);
		errorLog.setTransDtlColumnName(columnName);
		errorLog.setImportRow(line);
		errorLog.setRowNo(new Long(lineNumber));
		errorLog.setImportHeaderColumn(columnName);
		batchErrorLogs.add(errorLog);
		errorCountTotal++;
		
		ListCodes errorCode = knownErrors.get(code);
		if (errorCode!=null) {
			if(errorCode.getSeverityLevelAsInt()>Integer.parseInt(BatchMaintenance.ErrorState.Warning.getText())) {
				criticalErrorCount++;
				
			}
		}
		if (errorTreshold!=-1 && criticalErrorCount>=errorTreshold)
			errorTresholdReached = true;
		
	}

	public Double getProgress() {
		if (linesWritten < 1)
			return -0.01;
		try {
			logger.debug("Line: " + linesWritten + ", lines: " + linesInFile
					+ ", " + linesWritten / new Double(linesInFile));
			return linesWritten / new Double(linesInFile);
		} catch (Exception e) {
			return -0.01;
		}
	}

	public Logger getLogger() {
		return logger;
	}

	public void setLogger(Logger logger) {
		this.logger = logger;
	}

	public String getFileType() {
		return fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	public Date getReleaseDate() {
		return releaseDate;
	}

	public void setReleaseDate(Date releaseDate) {
		this.releaseDate = releaseDate;
	}

	public Double getReleaseVersion() {
		return releaseVersion;
	}

	public void setReleaseVersion(Double releaseVersion) {
		this.releaseVersion = releaseVersion;
	}

	public List<BatchErrorLog> getBatchErrorLogs() {
		return batchErrorLogs;
	}

	public void setBatchErrorLogs(List<BatchErrorLog> batchErrorLogs) {
		this.batchErrorLogs = batchErrorLogs;
	}

	public List<String> getErrors() {
		return errors;
	}

	public void setErrors(List<String> errors) {
		this.errors = errors;
	}

	public int getLineNumber() {
		return lineNumber;
	}

	public void setLineNumber(int lineNumber) {
		this.lineNumber = lineNumber;
	}

	public long getLineNumberTotal() {
		return lineNumberTotal;
	}

	public void setLineNumberTotal(long lineNumberTotal) {
		this.lineNumberTotal = lineNumberTotal;
	}

	public BufferedReader getBufferedReader() {
		return bufferedReader;
	}

	public void setBufferedReader(BufferedReader bufferedReader) {
		this.bufferedReader = bufferedReader;
	}

	public String getNextLine() {
		return nextLine;
	}

	public void setNextLine(String nextLine) {
		this.nextLine = nextLine;
	}

	public int getLinesWritten() {
		return linesWritten;
	}

	public void setLinesWritten(int linesWritten) {
		this.linesWritten = linesWritten;
	}

	public int getErrorCountTotal() {
		return errorCountTotal;
	}

	public void setErrorCountTotal(int errorCountTotal) {
		this.errorCountTotal = errorCountTotal;
	}

	public boolean isAbort() {
		return abort;
	}

	public void setAbort(boolean abort) {
		this.abort = abort;
	}

	public static SimpleDateFormat getSdf() {
		return sdf;
	}

	public void resetBatchErrorLogs() {
		if (batchErrorLogs != null) {
			batchErrorLogs.clear();
		}
		batchErrorLogs = new ArrayList<BatchErrorLog>();
		;
	}

	public void setKnownErrors(HashMap<String, ListCodes> knownErrors) {
		this.knownErrors = knownErrors;
	}

	public int getErrorTreshold() {
		return errorTreshold;
	}

	public void setErrorTreshold(int errorTreshold) {
		this.errorTreshold = errorTreshold;
	}

	public boolean isErrorTresholdReached() {
		return errorTresholdReached;
	}

	public void setErrorTresholdReached(boolean errorTresholdReached) {
		this.errorTresholdReached = errorTresholdReached;
	}

	public int getCriticalErrorCount() {
		return criticalErrorCount;
	}

}
