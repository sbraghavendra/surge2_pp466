package com.ncsts.view.util;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

import com.ncsts.domain.EntityItem;

public class EntityItemConveter implements Converter {

	public Object getAsObject(FacesContext context, UIComponent component,
			String value) {
		
		String[] index = value.split("~");
		
		return new EntityItem(index[0], index[1], index[2]);
	}

	public String getAsString(FacesContext context, UIComponent component,
			Object value) {

		EntityItem optionItem = (EntityItem) value;
		return optionItem.getEntityId() + "~" + optionItem.getEntityCode() + "~" + optionItem.getEntityName();
	}

}
