package com.ncsts.view.util;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;

import com.ncsts.domain.Menu;
	
public class MenuConverter implements javax.faces.convert.Converter{

	public Object getAsObject(FacesContext context, UIComponent component,
			String value) {

		int index = value.indexOf(':');
		
		return new Menu(value.substring(0, index), value.substring(index + 1));
	}

	public String getAsString(FacesContext context, UIComponent component,
			Object value) {

		Menu optionItem = (Menu) value;
		return optionItem.getMenuCode() + ":" + optionItem.getOptionName();
	}

}

