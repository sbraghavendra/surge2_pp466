/*
 * Author: Jim M. Wilson
 * Created: Sep 19, 2008
 * $Date: 2008-09-21 15:36:22 -0500 (Sun, 21 Sep 2008) $: - $Revision: 2361 $: 
 */
package com.ncsts.view.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

public class DateTimeConverter extends  javax.faces.convert.DateTimeConverter {
	private static Logger log = Logger.getLogger(DateTimeConverter.class);

	public static final String CONVERTER_ID = "dateTime";
	private static final String DEFAULT_PATTERN = "MM/dd/yyy hh:mm:ss a";
	private static final String[] PATTERNS = {"MM/dd/yyy h:mm:ss a", "MM/dd/yyy h:mm a", "M/d/yyyy"};

	public DateTimeConverter() {
		setPattern(DEFAULT_PATTERN);
	}
	
	 public String getAsString(FacesContext facesContext, UIComponent uiComponent, Object value) {
		 HttpSession session = (HttpSession)FacesContext.getCurrentInstance().getExternalContext().getSession(false);
		 if (session.getAttribute("timezone") != null) {
			 setTimeZone((TimeZone)session.getAttribute("timezone"));
		 }
		 return super.getAsString(facesContext, uiComponent, value);
	 }

	 public Object getAsObject(FacesContext facesContext, UIComponent uiComponent, String value) {
		 HttpSession session = (HttpSession)FacesContext.getCurrentInstance().getExternalContext().getSession(false);
		 if (session.getAttribute("timezone") != null) {
			 setTimeZone((TimeZone)session.getAttribute("timezone"));
		 }
		 // try our patterns, then if all else fails, yield to superclass converter 
		 /// that way we get good error messages without effort
		 for (String s : PATTERNS) {
			 try {
				 SimpleDateFormat f = new SimpleDateFormat(s);
				 f.setTimeZone(getTimeZone());
				 Date d = f.parse(value);
				 log.debug("Parsed date: " + f.format(d));
				 return d;
			 } catch (ParseException e) {}
		 }
		 return super.getAsObject(facesContext, uiComponent, value);
	 }
	 
	 public Date getUserLocalDateTime() {
			HttpSession session = (HttpSession)FacesContext.getCurrentInstance().getExternalContext().getSession(false);
			if (session.getAttribute("timezone") != null) {
				setTimeZone((TimeZone)session.getAttribute("timezone"));
			}
			 
			TimeZone userTimeZone = getTimeZone();
			TimeZone tomcatTimeZone = TimeZone.getDefault();//Tomcat Timezone
				
			long tomcatTime = new Date().getTime();
			long userTimeInMillis = tomcatTime + (userTimeZone.getOffset(tomcatTime)-tomcatTimeZone.getOffset(tomcatTime));
			
			Calendar cal = Calendar.getInstance();   
			cal.setTimeInMillis(userTimeInMillis); 
			
			return cal.getTime();
		}
}
