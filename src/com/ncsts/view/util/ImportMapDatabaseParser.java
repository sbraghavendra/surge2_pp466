package com.ncsts.view.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;

import javax.faces.model.SelectItem;

import org.apache.log4j.Logger;

import com.ncsts.common.LoggerFactory;
import com.ncsts.domain.DataDefinitionColumn;
import com.ncsts.domain.ImportConnection;
import com.ncsts.domain.ImportDefinition;
import com.ncsts.domain.ImportSpec;
import com.ncsts.domain.ListCodes;
import com.ncsts.dto.ImportDefDTO;
import com.ncsts.dto.ImportMapProcessPreferenceDTO;
import com.ncsts.exception.DateFormatException;

public class ImportMapDatabaseParser extends ImportMapUploadParser implements
		Iterator<BCPTransaction> {
	private Logger logger = LoggerFactory.getInstance().getLogger(ImportMapDatabaseParser.class);
	
	final String delims = "�";
	public int nextLoopNumber = 0;
	public final int rowsPerRequest = 1000;
	public int currentRowIndex = -1;
	public ArrayList<String> databaseRowList = new ArrayList<String>();
	private Connection rowConn = null;
	private String verificationMessage = "";
	private ImportConnection importConnection;

	public ImportMapDatabaseParser(ImportDefDTO importDefinition) {
		super(importDefinition);
	}

	public ImportMapDatabaseParser(ImportDefinition importDefinition) {
		super(importDefinition);
	}

	public ImportMapDatabaseParser(ImportDefinition importDefinition, ImportMapProcessPreferenceDTO preferences) throws DateFormatException {
		this(importDefinition);
		setPreferences(preferences);
	}

	public String getImportDateFormat() {
		return "yyyy-MM-dd";
	}

	public void setImportConnection(ImportConnection importConnection) {
		this.importConnection = importConnection;
	}

	public void setImportSpec(ImportSpec importSpec) {
		this.importSpec = importSpec;

		if (importSpec != null) {
			String format = "yyyy-MM-dd";
			if ((format != null && (format.trim().length() > 0))) {
				try {
					formatter = DateHelper.getImportDateFormat(format);
					return;
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void resetDatabaseRows() {
		nextLoopNumber = 0;
		currentRowIndex = -1;
		rowConn = null;
		databaseRowList = new ArrayList<String>();
		fileSize = -1;//Need database verification to set to 0
	}

	public String nextRow() {
		return (String) databaseRowList.get(currentRowIndex);
	}

	private boolean hasNextDatabaseRow() {
		if (nextLoopNumber == 0) {
			try {
				Class.forName(importConnection.getDbDriverClassName());
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
				return false;
			}

			rowConn = null;
			try {
				rowConn = DriverManager.getConnection(importConnection.getDbUrl(),
						importConnection.getDbUserName(), importConnection.getDbPassword());
				rowConn.setAutoCommit(false);
				
				System.out.println("############ Database Import connection created for: " + importConnection.getDbUrl());

				// Get a total count
				String sqlCount = "select count(*) from " + importDefinition.getDbTableName();
				if (this.importSpec.getWhereClause() != null && this.importSpec.getWhereClause().length() > 0) {
					sqlCount = sqlCount + " where " + this.importSpec.getWhereClause();
				}

				Statement statement = rowConn.createStatement();
				ResultSet rs1 = statement.executeQuery(sqlCount);

				if (rs1.next()) {
					fileSize = ((Number) rs1.getObject(1)).longValue();
				}

				if (fileSize == 0) {// No matched row
					clearDatabaseConnection();
					return false;
				}
			} catch (Throwable e) {
				e.printStackTrace();
				clearDatabaseConnection();
				return false;
			}
		}

		// Check if data already in array or need to retrieve
		currentRowIndex++;
		if (nextLoopNumber > 0) {
			if (currentRowIndex < (databaseRowList.size())) {
				return true;
			} else if ((currentRowIndex == databaseRowList.size()) && (databaseRowList.size() < rowsPerRequest)) {
				clearDatabaseConnection();
				return false; // Finish
			}
		}

		try {
			// Retrieve data
			Statement p = null;
			ResultSet rs = null;
			String whereClause = "1=1";
			if (this.importSpec.getWhereClause() != null && this.importSpec.getWhereClause().length() > 0) {
				whereClause = this.importSpec.getWhereClause();
			}

			String columnSelect = "";
			for (int i = 0; i < getHeaderNames().size(); i++) {
				if (columnSelect.length() > 0) {
					columnSelect = columnSelect + ",";
				}
				columnSelect = columnSelect + getHeaderNames().get(i);
			}

			String sql = "select " + columnSelect + " from (Select rownum AS RowNumber, "
					+ columnSelect + " FROM "
					+ importDefinition.getDbTableName() + " WHERE "
					+ whereClause + " ) WHERE RowNumber BETWEEN "
					+ (nextLoopNumber * rowsPerRequest + 1) + " AND "
					+ ((nextLoopNumber + 1) * rowsPerRequest);

			System.out.println("sql : " + sql);

			p = rowConn.createStatement();
			rs = p.executeQuery(sql);
			nextLoopNumber++;

			databaseRowList = new ArrayList<String>();
			String rowString = "";
			while (rs.next()) {
				rowString = "";
				String value = "";
				/* good
				for (String header : getHeaderNames()) {
					if (rs.getObject(header) != null) {
						value = rs.getObject(header).toString();
					} else {
						value = "";
					}
					if (rowString.length() > 0) {
						rowString = rowString + delims;
					}
					rowString = rowString + value;
				}
				databaseRowList.add(rowString);
				*/
				String headerString = "";
				boolean foundFirst = false;
				for (String header : getHeaderNames()) {
					if (rs.getObject(header) != null) {
						value = rs.getObject(header).toString();
					} else {
						value = "";
					}
					//if (rowString.length() > 0) {
					//	rowString = rowString + delims;
					//}
					if(foundFirst){
						rowString = rowString + delims;
					}
					rowString = rowString + value;
					
					foundFirst = true;
					
					if (headerString.length() > 0) {
						headerString = headerString + delims;
					}
					headerString = headerString + header;
				}
				databaseRowList.add(rowString);
				
				System.out.println("############ headerString: " + headerString);
				System.out.println("############ RrowString: " + rowString);
			}
			rs.close();
			p.close();

			System.out.println("############ Retrieved count of rows: " + databaseRowList.size());

			if (databaseRowList.size() == 0) {
				clearDatabaseConnection();
				return false; // No more data
			} else {
				currentRowIndex = 0;
				return true;
			}
		} catch (Throwable e) {
			e.printStackTrace();
			clearDatabaseConnection();
			return false;
		}
	}

	public void clearDatabaseConnection() {
		if (rowConn != null) {
			try {
				rowConn.close();
				System.out.println("############ Database Import connection closed for: " + importConnection.getDbUrl());
			} catch (SQLException ignore) {
			}
		}
	}

	public boolean hasNext() {
		if (this.abort) {
			clearDatabaseConnection();
			return false;
		}

		return hasNextDatabaseRow();	
	}

	public Double getProgress() {
		if (fileSize < 0) return -0.01;
		if (abort) return 101.0;
		try {
			if (fileSize == 0) {
				return 101.0;
			} else {
				return lineNumber / new Double(fileSize);
			}
		} catch (Exception e) {
			return -0.01;
		}
	}

	public BCPTransaction next() {
		return nextRecord(nextRow());
	}

	public List<String> split(String s) {
		List<String> list = new ArrayList<String>();
		
		StringTokenizer st = new StringTokenizer(s, delims, true);
		boolean sawDelim = true;
		try {
			while (st.hasMoreTokens()) {
				String t = st.nextToken(delims);
				if (t.equalsIgnoreCase(delims)) {
					if (sawDelim) {
						list.add(null);
					}
					sawDelim = true;
				} else {
					list.add(t);
					sawDelim = false;
				}
			}
		} catch (NoSuchElementException e) {
		}
		if (sawDelim) {
			list.add(null);
		}
		return list;
	}

	public int getTotalRows() {
		return lineNumber;
	}
	
	public boolean postImport() {
		try {
			Class.forName(importConnection.getDbDriverClassName());
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			return false;
		}

		Connection conn = null;
		try {
			conn = DriverManager.getConnection(importConnection.getDbUrl(),
					importConnection.getDbUserName(), importConnection.getDbPassword());
			conn.setAutoCommit(false);

			String sqlDelete = "delete from " + importDefinition.getDbTableName();
			if (this.importSpec.getWhereClause() != null && this.importSpec.getWhereClause().length() > 0) {
				sqlDelete = sqlDelete + " where " + this.importSpec.getWhereClause();
			}

			Statement statement = conn.createStatement();
			int countDelete = statement.executeUpdate(sqlDelete);
			conn.commit();
			
			System.out.println("############ Database Import: postImport(): " + countDelete + " rows deleted");

			return true;
		} catch (Throwable e) {
			e.printStackTrace();
			return false;
		}
		finally{
			if(conn != null){try{conn.close();}catch(SQLException ignore){}}
		}
	}

	public String getVerificationMessage(){
		return verificationMessage;
	}
	
	public boolean verifyImportConnection() {
		verificationMessage = "";
		
		
		try {
			Class.forName(importConnection.getDbDriverClassName());
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			return false;
		}

		Connection conn = null;
		try {
			conn = DriverManager.getConnection(importConnection.getDbUrl(),
					importConnection.getDbUserName(), importConnection.getDbPassword());
			conn.setAutoCommit(false);

			String sqlDelete = "delete from " + importDefinition.getDbTableName();
			if (this.importSpec.getWhereClause() != null && this.importSpec.getWhereClause().length() > 0) {
				sqlDelete = sqlDelete + " where " + this.importSpec.getWhereClause();
			}

			// Get a total count
			String sqlCount = "select count(*) from " + importDefinition.getDbTableName();
			if (this.importSpec.getWhereClause() != null && this.importSpec.getWhereClause().length() > 0) {
				sqlCount = sqlCount + " where " + this.importSpec.getWhereClause();
			}

			Statement statement = conn.createStatement();
			ResultSet rs1 = statement.executeQuery(sqlCount);

			if (rs1.next()) {
				fileSize = ((Number) rs1.getObject(1)).longValue();
			}
			
			if (fileSize == 0) {// No matched row
				verificationMessage = "No row will be imported.";
				return false;
			}		
			verificationMessage = fileSize + " rows will be imported.";

			return true;
		} catch (Exception e) {
			e.printStackTrace();
			String message = e.getMessage();
			if(message!=null){
				if(message.length()<=100){
					verificationMessage = message;
				}
				else{
					verificationMessage = message.substring(0, 100);
				}		
			}
			return false;
		}
		finally{
			if(conn != null){try{conn.close();}catch(SQLException ignore){}}
		}
	}
	
	public static void main(String[] args) {
		
		String line = "COMMENTS,ENTITY_CODE,GL_DATE,GL_FULL_ACCT_NAME,GL_FULL_ACCT_NBR,GL_LOCAL_ACCT_NAME,GL_LOCAL_ACCT_NBR,GL_LOCAL_SUB_ACCT_NAME,GL_LOCAL_SUB_ACCT_NBR,INVENTORY_NAME,INVENTORY_NBR,INVOICE_DATE,INVOICE_DESC,INVOICE_FREIGHT_AMT,INVOICE_LINE_NBR,INVOICE_NBR,MANUAL_TAXCODE_IND,USER_DATE_01,USER_DATE_02,USER_DATE_03,USER_DATE_04,USER_DATE_05,USER_DATE_06,USER_DATE_07,USER_DATE_08,USER_DATE_09,USER_DATE_10,USER_TEXT_01,USER_TEXT_02,USER_TEXT_03,USER_TEXT_04,USER_TEXT_05,USER_TEXT_06,USER_TEXT_07,USER_TEXT_08,USER_TEXT_09,USER_TEXT_10,USER_TEXT_11,USER_TEXT_12,USER_TEXT_13,USER_TEXT_14,USER_TEXT_15,USER_TEXT_16,USER_TEXT_17,USER_TEXT_18,USER_TEXT_19,USER_TEXT_20,USER_TEXT_21,USER_TEXT_22,USER_TEXT_23,USER_TEXT_24,USER_TEXT_25,USER_TEXT_26,USER_TEXT_27,USER_TEXT_28,USER_TEXT_29,USER_TEXT_30,AFE_CATEGORY_NAME,AFE_CATEGORY_NBR,AFE_CONTRACT_STRUCTURE,AFE_CONTRACT_TYPE,AFE_PROJECT_NAME,AFE_PROJECT_NBR,AFE_PROPERTY_CAT,AFE_SUB_CAT_NAME,AFE_SUB_CAT_NBR,AFE_USE,AUDIT_FLAG,AUDIT_TIMESTAMP,AUDIT_USER_ID,CHECK_AMT,CHECK_DATE,CHECK_DESC,CHECK_NBR,CHECK_NO,COMMENTS,COUNTRY_USE_AMOUNT,COUNTRY_USE_RATE,CP_FILING_ENTITY_CODE,ENTERED_DATE,ENTITY_CODE,GL_CC_NBR_DEPT_ID,GL_CC_NBR_DEPT_NAME,GL_COMPANY_NAME,GL_COMPANY_NBR,GL_DATE,GL_DIVISION_NAME,GL_DIVISION_NBR,GL_EXTRACT_AMT,GL_EXTRACT_BATCH_NO,GL_EXTRACT_TIMESTAMP,GL_EXTRACT_UPDATER,GL_FULL_ACCT_NAME,GL_FULL_ACCT_NBR,GL_LINE_ITM_DIST_AMT,GL_LOCAL_ACCT_NAME,GL_LOCAL_ACCT_NBR,GL_LOCAL_SUB_ACCT_NAME,GL_LOCAL_SUB_ACCT_NBR,INVENTORY_CLASS,INVENTORY_CLASS_NAME,INVENTORY_NAME,INVENTORY_NBR,INVOICE_DATE,INVOICE_DESC,INVOICE_DISCOUNT_AMT,INVOICE_FREIGHT_AMT,INVOICE_LINE_AMT,INVOICE_LINE_NAME,INVOICE_LINE_NBR,INVOICE_LINE_TAX,INVOICE_LINE_TYPE,INVOICE_LINE_TYPE_NAME,INVOICE_NBR,INVOICE_TAX_AMT,INVOICE_TAX_FLG,INVOICE_TOTAL_AMT,MODIFY_TIMESTAMP,MODIFY_USER_ID,PO_DATE,PO_LINE_NAME,PO_LINE_NBR,PO_LINE_TYPE,PO_LINE_TYPE_NAME,PO_NAME,PO_NBR,PRODUCT_CODE,REJECT_FLAG,SHIP_TO_ADDRESS_CITY,SHIP_TO_ADDRESS_COUNTRY,SHIP_TO_ADDRESS_COUNTY,SHIP_TO_ADDRESS_LINE_1,SHIP_TO_ADDRESS_LINE_2,SHIP_TO_ADDRESS_LINE_3,SHIP_TO_ADDRESS_LINE_4,SHIP_TO_ADDRESS_STATE,SHIP_TO_ADDRESS_ZIP,SHIP_TO_LOCATION,SHIP_TO_LOCATION_NAME,SOURCE_TRANSACTION_ID,STATE_MAXTAX_AMOUNT,STATE_SPLIT_AMOUNT,STJ1_AMOUNT,STJ1_RATE,TRANSACTION_COUNTRY_CODE,TRANSACTION_STATE_CODE,TRANSACTION_STATUS,USER_DATE_01,USER_DATE_02,USER_DATE_03,USER_DATE_04,USER_DATE_05,USER_DATE_06,USER_DATE_07,USER_DATE_08,USER_DATE_09,USER_DATE_10,USER_NUMBER_01,USER_NUMBER_02,USER_NUMBER_03,USER_NUMBER_04,USER_NUMBER_05,USER_NUMBER_06,USER_NUMBER_07,USER_NUMBER_08,USER_NUMBER_09,USER_NUMBER_10,USER_TEXT_01,USER_TEXT_02,USER_TEXT_03,USER_TEXT_04,USER_TEXT_05,USER_TEXT_06,USER_TEXT_07,USER_TEXT_08,USER_TEXT_09,USER_TEXT_10,USER_TEXT_11,USER_TEXT_12,USER_TEXT_13,USER_TEXT_14,USER_TEXT_15,USER_TEXT_16,USER_TEXT_17,USER_TEXT_18,USER_TEXT_19,USER_TEXT_20,USER_TEXT_21,USER_TEXT_22,USER_TEXT_23,USER_TEXT_24,USER_TEXT_25,USER_TEXT_26,USER_TEXT_27,USER_TEXT_28,USER_TEXT_29,USER_TEXT_30,VENDOR_ADDRESS_CITY,VENDOR_ADDRESS_COUNTRY,VENDOR_ADDRESS_COUNTY,VENDOR_ADDRESS_LINE_1,VENDOR_ADDRESS_LINE_2,VENDOR_ADDRESS_LINE_3,VENDOR_ADDRESS_LINE_4,VENDOR_ADDRESS_STATE,VENDOR_ADDRESS_ZIP,VENDOR_NAME,VENDOR_NBR,VENDOR_TYPE,VENDOR_TYPE_NAME,VOUCHER_DATE,VOUCHER_ID,VOUCHER_LINE_DESC,VOUCHER_LINE_NBR,VOUCHER_NAME,WO_CLASS,WO_CLASS_DESC,WO_DATE,WO_ENTITY,WO_ENTITY_DESC,WO_LINE_NAME,WO_LINE_NBR,WO_LINE_TYPE,WO_LINE_TYPE_DESC,WO_NAME,WO_NBR,WO_SHUT_DOWN_CD,WO_SHUT_DOWN_CD_DESC,WO_TYPE,WO_TYPE_DESC";
		
		List<String> list = new ArrayList<String>();
		
		Map<String, Integer> result = new LinkedHashMap<String, Integer>();
		
		StringTokenizer st = new StringTokenizer(line, ",", true);

		try {
			while (st.hasMoreTokens()) {
				String t = st.nextToken(",");
				if (t.equalsIgnoreCase(",")) {
				} else {
					if(result.get(t)!=null){
						int count = result.get(t);
						count++;
						
						result.put(t, count);
					}
					else{
						result.put(t, 1);
					}
					
					
					list.add(t);
				}
			}
		} catch (NoSuchElementException e) {
		}

		System.out.println(list.size());
		
		System.out.println("result.keySet().size() : " + result.keySet().size());
		
		for (String name : result.keySet()) {
			int icount = result.get(name);
	
			System.out.println(name + " : " + icount);
	
		}
		
	}
}
