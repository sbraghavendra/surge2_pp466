package com.ncsts.view.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ConcurrentModificationException;
import java.util.Properties;

import org.apache.log4j.Logger;



public class ConfigSetting {
	private static Logger log = Logger.getLogger(ConfigSetting.class);

	private Properties properties;
	private String filePath;
	
/**
 * Load the properties from either a direct file path outside of application
 * or as a resource within the application. 
*/
	public ConfigSetting(String file) throws IOException {
		filePath = file;
		properties = new Properties();
		properties.load(new FileInputStream(getResource(filePath)));
	}
	
	protected String getResource(String file) {
		File f = new File(file);
		if (f.exists()) {
			return file;
		} else {
			return ConfigSetting.class.getResource(file).getFile();
		}
	}
	 
	public String getProperty(String propName) {
		String result = properties.getProperty(propName);
		if (result == null) {
			throw new RuntimeException("Config property not found: " + propName);
		}
		return result;
	}
	
	public String getPropertyAsString(String propName, String defaultValue) {
		String value = properties.getProperty(propName);
		return (value == null)? defaultValue:value;
	}
	
	public long getPropertyAsLong(String propName, long defaultValue) {
		long result = defaultValue;
		String value = properties.getProperty(propName);
		if (value != null) {
			result = Long.parseLong(value);
		}
		return result;
	}
	
	public int getPropertyAsInt(String propName, int defaultValue) {
		int result = defaultValue;
		String value = properties.getProperty(propName);
		if (value != null) {
			result = Integer.parseInt(value);
		}
		return result;
	}
	
	public boolean getPropertyAsBoolean(String propName, boolean defaultValue) {
		boolean result = defaultValue;
		String value = properties.getProperty(propName);
		if (value != null) {
			result = Boolean.parseBoolean(value);
		}
		return result;
	}
	
	public Boolean propertyExists(String propName) {
		return properties.containsKey(propName);
	}
	
	public void setProperty (String propName, String propValue){
		properties.setProperty(propName,propValue);
	}
	
	public void saveProperties() {
		String header = " ##**** STEPS TO ADD A NEW DATABASE REFERENCE ****" + "\r\n";
		header = header + "# Increase db.count" + "\r\n";
		header = header + "# Add each of the following lines ..." + "\r\n";
		header = header + "#   db.<db count #>.name=<Name you want displayed in the login database drop down> (EXAMPLE: db.3.name=My Database)";
		header = header + "#   db.<db count #>.driverClassName=<Name of the driver class> (EXAMPLE: db.3.driverClassName=oracle.jdbc.driver.OracleDriver)" + "\r\n";
		header = header + "#   db.<db count #>.url=<Url of the database> (EXAMPLE: db.3.url=jdbc\\:oracle\\:thin\\:@10.10.4.3\\:1521\\:DB3)" + "\r\n";
		header = header + "#   db.<db count #>.username=<Database Username> (EXAMPLE: db.3.username=mycorp)" + "\r\n";
		header = header + "#   db.<db count #>.password=<Database Password> (EXAMPLE: db.3.password=mypassword)" + "\r\n";
		header = header + "\r\n";
		header = header + " ###**** STEPS TO ADD A NEW EMAIL SERVER ****" + "\r\n";
		header = header + "#   mail.smtpHost=<Email server host name> (EXAMPLE: mail.smtpHost=mail.DOMAIN.COM)" + "\r\n";
		header = header + "#   mail.smtpPort=<Port> (EXAMPLE: mail.smtpPort=25)" + "\r\n";
		header = header + "#   mail.from=<Email from) (EXAMPLE: mail.from=admin@DOMAIN.COM)" + "\r\n";
		header = header + "#   mail.subject=<Email subject) (EXAMPLE: mail.subject=Your STS Corporate Password Reminder)" + "\r\n";
		header = header + "#   mail.content=<Email content) (EXAMPLE: mail.content=Your current password is)" + "\r\n";
		header = header + "\r\n";
		header = header + " ###**** STEPS TO ADD A NEW USER ****" + "\r\n";
		header = header + "# Add each of the following lines ..." + "\r\n";
		header = header + "#   user.<User Code>.display_name=<A more friendly name> (EXAMPLE: user.KWILLIAMS.display_name=KASEY WILLIAMS)" + "\r\n";
		header = header + "#   user.<User Code>.password=<password> (EXAMPLE: user.KWILLIAMS.password=myPassword1)" + "\r\n";
		header = header + "#   user.<User Code>.dbs=<Comma seperated list of db count #s) (EXAMPLE: user.KWILLIAMS.dbs=1,2)" + "\r\n";
		header = header + "#   user.<User Code>.email=<User's email address) (EXAMPLE: user.KWILLIAMS.email=KWILLIAMS@DOMAIN.COM)" + "\r\n";
		header = header + "\r\n";
		header = header + " ###**** SPECIAL NOTES ****" + "\r\n";
		header = header + " # One, Passwords are case sensitive." + "\r\n";
		header = header + " # Two, Listing more dbs in the comma seperated list than are defined will generate an error." + "\r\n";
		header = header + "\r\n";
		
		// Write properties file ...
		try {
			properties.store(new FileOutputStream(getResource(filePath)), header);
		} catch (IOException  e) {
			log.error(e);
		}
		catch(ConcurrentModificationException e) {
			log.error("ConcurrentModificationException "+e.getMessage());
		}
	}
}
