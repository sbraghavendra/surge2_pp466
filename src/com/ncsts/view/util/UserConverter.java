package com.ncsts.view.util;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

import com.ncsts.domain.User;

public class UserConverter implements Converter {

	public Object getAsObject(FacesContext context, UIComponent component,
			String value) {

		int index = value.indexOf(':');
		
		return new User(value.substring(0, index), value.substring(index + 1));
	}

	public String getAsString(FacesContext context, UIComponent component,
			Object value) {

	
		User optionItem = (User) value;
		return optionItem.getUserCode()+ ":" + optionItem.getUserName();
	}

}
