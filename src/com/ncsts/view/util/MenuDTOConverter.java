package com.ncsts.view.util;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

import com.ncsts.dto.MenuDTO;

public class MenuDTOConverter implements Converter {

	public Object getAsObject(FacesContext context, UIComponent component,
			String value) {

		int index = value.indexOf(':');
		
		return new MenuDTO(value.substring(0, index), value.substring(index + 1));
	}

	public String getAsString(FacesContext context, UIComponent component,
			Object value) {

		MenuDTO optionItem = (MenuDTO) value;
		return optionItem.getMenuCode() + ":" + optionItem.getOptionName();
	}

}
