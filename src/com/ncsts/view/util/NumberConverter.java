package com.ncsts.view.util;

import java.math.RoundingMode;
import java.text.DecimalFormat;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;

public class NumberConverter extends javax.faces.convert.NumberConverter {
	public static final String CONVERTER_ID = "number";
	private static final String DEFAULT_PATTERN = "#0.00";
	private static final RoundingMode ROUNDING_MODE = RoundingMode.HALF_UP;
	
	public NumberConverter() {
		setPattern(DEFAULT_PATTERN);
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component,
            Object value) {
		
		if(context != null && component != null && value != null && !(value instanceof String)) {
			if((getPattern() != null && getPattern().length() != 0) && "number".equalsIgnoreCase(getType())) {
				
				if(value instanceof Float) {
	            	try {
	            		value = Double.parseDouble(String.valueOf(value));
	            	}
	            	catch(Exception e){}
	            }
				
				DecimalFormat formatter = new DecimalFormat(getPattern());
				formatter.setRoundingMode(ROUNDING_MODE);
				
				return (formatter.format(value));
			}
		}
		
		return super.getAsString(context, component, value);
	}
}