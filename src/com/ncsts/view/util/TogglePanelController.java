package com.ncsts.view.util;

import java.util.LinkedHashMap;
import java.util.Map;

import javax.faces.event.AbortProcessingException;
import javax.faces.event.ActionEvent;

import org.richfaces.component.html.HtmlSimpleTogglePanel;

public class TogglePanelController {
	
	private Boolean isHideSearchPanel = false;
	
	private Map<String,Boolean> togglePanels = new LinkedHashMap<String,Boolean>();
	
	
	public void addTogglePanel(String key, boolean isOpened){
		togglePanels.put(key, isOpened);
	}
	
	public void removeTogglePanel(String key, boolean isOpened){
		togglePanels.remove(key);
	}
	
	public Map<String,Boolean> getTogglePanels() {
		return togglePanels;
	}
	
	public void togglePanelChanged(ActionEvent event) {
		//HtmlSimpleTogglePanel togglePanel = (HtmlSimpleTogglePanel) (((HtmlAjaxSupport) event.getComponent()).getParent());
		
		HtmlSimpleTogglePanel togglePanel = (HtmlSimpleTogglePanel) (event.getComponent());
		
		String key = togglePanel.getId();
		boolean isOpened = togglePanel.isOpened();
		togglePanels.put(key, isOpened);
	}
	
	public void collapseTogglePanels(ActionEvent e) throws AbortProcessingException {
    	for (String key : togglePanels.keySet()) {
    		togglePanels.put(key,false);
    	}
	}
	
	public void expandTogglePanels(ActionEvent e) throws AbortProcessingException {
    	for (String key : togglePanels.keySet()) {
    		togglePanels.put(key,true);
    	}
	}
	
	public void toggleHideSearchPanel(ActionEvent event){
		isHideSearchPanel = !isHideSearchPanel;
	}
	
	public Boolean getIsHideSearchPanel() {
		return isHideSearchPanel;
	}

}
