package com.ncsts.view.util;

import com.ncsts.domain.BatchMaintenance;
import com.ncsts.domain.CustLocnBatch;
import com.ncsts.dto.ImportMapProcessPreferenceDTO;
import com.ncsts.jsf.model.BatchMaintenanceDataModel;

public class CustLocnBatchValidator {
	private BatchMaintenanceDataModel dataModel;
	private CustLocnBatch current;
	
	public CustLocnBatchValidator(BatchMaintenanceDataModel dataModel, ImportMapProcessPreferenceDTO importMapProcessPreferenceDTO) {
		this.dataModel = dataModel;
		
		current = new CustLocnBatch();
		//current.setReleaseVersion(importMapProcessPreferenceDTO.getLastCustLocnRelUpdateAsInt());
		//current.setReleaseDate(importMapProcessPreferenceDTO.getLastCustLocnDateUpdateAsDate());
		/* if there is a running batch, give that precedence */
		CustLocnBatch running = getRunningBatch();
		if (running != null) {
			//current.setReleaseVersion(running.getReleaseVersion());
			//current.setReleaseDate(running.getReleaseDate());
		}
	}
	
	private CustLocnBatch getRunningBatch() {
		BatchMaintenance example = new BatchMaintenance();
		example.setBatchStatusCode(CustLocnBatch.PROCESSING);
		example.setBatchTypeCode(CustLocnBatch.BATCH_CODE);
		BatchMaintenance bm = dataModel.getBatchMaintenanceDAO().find(example);
		if ((bm != null) && (bm.getBatchId() != null)) 
			return new CustLocnBatch(bm);
		else
			return null;
	}

	public CustLocnBatch validate(CustLocnBatch bu) {
		BatchMaintenance batch = bu.getBatchMaintenance();

		if (!bu.isRunning() && !bu.isFlagged() && !bu.isImported()) {
			bu.setError("Not an imported batch");
			return bu;
		}
		if (bu.isHeld()) {
			bu.setError("On Hold");
			return bu;
		}
		BatchMaintenance b = dataModel.getBatchMaintenanceDAO().getPreviousBatchInSequence(batch);
		if (b != null) {
			if (!bu.isNextInSequence(current)) {
				bu.setError("Batch is out of sequence");
				return bu;
			}
		}
		current = bu;
		return bu;
	}
}
