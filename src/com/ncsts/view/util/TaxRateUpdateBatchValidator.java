/*
 * Author: Jim M. Wilson
 * Created: Sep 24, 2008
 * $Date: 2009-11-06 13:51:24 -0600 (Fri, 06 Nov 2009) $: - $Revision: 4404 $: 
 */

package com.ncsts.view.util;

import java.util.Date;

import com.ncsts.dao.JurisdictionDAO;
import com.ncsts.domain.BatchMaintenance;
import com.ncsts.domain.TaxRateUpdatesBatch;
import com.ncsts.dto.ImportMapProcessPreferenceDTO;
import com.ncsts.jsf.model.BatchMaintenanceDataModel;

/**
 * takes the current set of selected batches and tests for validity
 */
public class TaxRateUpdateBatchValidator {
	private BatchMaintenanceDataModel dataModel;
	private TaxRateUpdatesBatch current;
	
	public TaxRateUpdateBatchValidator(BatchMaintenanceDataModel dataModel, JurisdictionDAO jurisdictionDAO,
			int lastTaxRateRelUpdate, Date lastTaxRateDateUpdate) {
		this.dataModel = dataModel;

		current = new TaxRateUpdatesBatch();
		current.setReleaseVersion(lastTaxRateRelUpdate);
		current.setReleaseDate(lastTaxRateDateUpdate);
	
		/* if there is a running batch, give that precedence */
		TaxRateUpdatesBatch running = getRunningBatch();
		if (running != null) {
			current.setReleaseVersion(running.getReleaseVersion());
			current.setReleaseDate(running.getReleaseDate());
		}
	}

	/**
	 * see if there is a running batch
	 */
	private TaxRateUpdatesBatch getRunningBatch() {
		BatchMaintenance example = new BatchMaintenance();
		example.setBatchStatusCode(TaxRateUpdatesBatch.PROCESSING);
		example.setBatchTypeCode(TaxRateUpdatesBatch.BATCH_CODE);
		BatchMaintenance bm = dataModel.getBatchMaintenanceDAO().find(example);
		if ((bm != null) && (bm.getBatchId() != null)) 
			return new TaxRateUpdatesBatch(bm);
		else
			return null;
	}
	
	public TaxRateUpdatesBatch validate(TaxRateUpdatesBatch bu) {
		BatchMaintenance batch = bu.getBatchMaintenance();

		if (!bu.isRunning() && !bu.isFlagged() && !bu.isImported()) {
			bu.setError("Not an imported batch");
			return bu;
		}
		if (bu.isHeld()) {
			bu.setError("On Hold");
			return bu;
		}
		BatchMaintenance b = dataModel.getBatchMaintenanceDAO().getPreviousBatchInSequence(batch);
		if (b != null) {
			if (!bu.isNextInSequence(current)) {
				bu.setError("Batch is out of sequence");
				return bu;
			}
		}
		current = bu;
		return bu;
	}
}
