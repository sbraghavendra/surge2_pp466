package com.ncsts.view.util;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.ncsts.common.CacheManager;
import com.ncsts.domain.BCPSitusRule;
import com.ncsts.domain.BatchErrorLog;
import com.ncsts.domain.ListCodes;
import com.ncsts.view.bean.UpdateFilesBackingBean;

public class SitusRulesUploadParser extends AbstractFileUploader implements
		Iterator<BCPSitusRule> {

	private Logger logger = Logger.getLogger(SitusRulesUploadParser.class);

	private String fileType;
	private Date releaseDate;
	private String releaseVersion;

	private int lineNumber;
	private int linesWritten;
	private String nextLine;

	private List<String> errors;
	private List<BatchErrorLog> batchErrorLogs;
	private BufferedReader bufferedReader;

	private BigDecimal specialRateTotal;
	private Map<String, String> errorListCodes = new LinkedHashMap<String,String>();

	private static final SimpleDateFormat sdf = new SimpleDateFormat(
			"yyyy/MM/dd");
	private static final SimpleDateFormat sdfRelease = new SimpleDateFormat(
			"yyyy_MM");
	private static final String[] COLUMNS = { 
		"TAXCODE_COUNTRY_CODE", "LC_DESCRIPTION", "TAXCODE_STATE_CODE", "TS_GEOCODE", "TS_NAME", "TS_LOCAL_TAXABILITY_CODE", 
		"TS_ACTIVE_FLAG", "TAXCODE_CODE", "TC_DESCRIPTION", "TC_COMMENTS", "TC_ACTIVE_FLAG", "TD_TAXCODE_COUNTY", 
		"TD_TAXCODE_CITY", "TD_EFFECTIVE_DATE", "TD_EXPIRATION_DATE", "TD_TAXCODE_TYPE_CODE", "TD_OVERRIDE_TAXTYPE_CODE", 
		"TD_RATETYPE_CODE", "TD_TAXABLE_THRESHOLD_AMT", "TD_TAX_LIMITATION_AMT", "TD_CAP_AMT", "TD_BASE_CHANGE_PCT", 
		"TD_SPECIAL_RATE", "TD_ACTIVE_FLAG", "UPDATE_CODE"
	};
	
	private static final String[] COLUMNS_SD = { 
		"RECORD_TYPE", "SDD_SITUS_DRIVER_ID", "SDD_SITUS_DRIVER_DETAIL_ID", "TRANSACTION_TYPE_CODE", "RATETYPE_CODE", "METHOD_DELIVERY_CODE",
		"SDD_LOCATION_TYPE_CODE", "SDD_DRIVER_ID", "SDD_MANDATORY_FLAG", "UPDATE_CODE"
	};
	
	private static final String[] COLUMNS_SM = { 
		"RECORD_TYPE", "SM_SITUS_MATRIX_ID", "TRANSACTION_TYPE_CODE", "RATETYPE_CODE", "METHOD_DELIVERY_CODE",
		"SM_SITUS_COUNTRY_CODE", "SM_SITUS_STATE_CODE", "SM_JUR_LEVEL", 
		"SM_DRIVER_01", "SM_DRIVER_02", "SM_DRIVER_03", "SM_DRIVER_04", "SM_DRIVER_05", "SM_DRIVER_06", "SM_DRIVER_07", "SM_DRIVER_08", 
		"SM_DRIVER_09", "SM_DRIVER_10", "SM_DRIVER_11", "SM_DRIVER_12", "SM_DRIVER_13", "SM_DRIVER_14", "SM_DRIVER_15", "SM_DRIVER_16", 
		"SM_DRIVER_17", "SM_DRIVER_18", "SM_DRIVER_19", "SM_DRIVER_20", "SM_DRIVER_21", "SM_DRIVER_22", "SM_DRIVER_23", "SM_DRIVER_24", 
		"SM_DRIVER_25", "SM_DRIVER_26", "SM_DRIVER_27", "SM_DRIVER_28", "SM_DRIVER_29", "SM_DRIVER_30", 
		"SM_BINARY_WEIGHT", "SM_EFFECTIVE_DATE", "SM_EXPIRATION_DATE", "SM_PRIMARY_SITUS_CODE", "SM_PRIMARY_TAXTYPE_CODE", 
		"SM_SECONDARY_SITUS_CODE", "SM_SECONDARY_TAXTYPE_CODE", "SM_COMMENTS", "SM_ALL_LEVELS_FLAG", "SM_ACTIVE_FLAG", "UPDATE_CODE"
	};
	
	private CacheManager cacheManager;
		
	public void setCacheManager(CacheManager cacheManager) {
		this.cacheManager = cacheManager;
	}
	 
	public void setErrorListCodes(Map<String, String> errorListCodes) {
		this.errorListCodes = errorListCodes;
	}

	public SitusRulesUploadParser(String path) {
		super(path);
		deleteOnExit = true;
	}

	public void clear() {
		errors = new ArrayList<String>();
		batchErrorLogs = new ArrayList<BatchErrorLog>();
		this.fileType = null;
		this.releaseDate = null;
		this.releaseVersion = null;
		this.uploadFileName = null;
		this.uploadFilePath = null;
		this.totalBytes = 0;
		this.nextLine = null;
		this.linesInFile = 0;
		this.lineNumber = 0;
//		this.specialRateTotal = ArithmeticUtils.ZERO;
	}

	public String getUploadFileName() {
		return this.uploadFileName;
	}

	public String getFileType() {
		return this.fileType;
	}

	public Date getReleaseDate() {
		return this.releaseDate;
	}

	public String getReleaseVersion() {
		return this.releaseVersion;
	}

	public Double getProgress() {
		if (lineNumber < 1)
			return -0.01;
		try {
			logger.debug("Line: " + linesWritten + ", lines: " + linesInFile
					+ ", " + linesWritten / new Double(linesInFile));
			return linesWritten / new Double(linesInFile);
		} catch (Exception e) {
			return -0.01;
		}
	}

	public List<String> getErrors() {
		return this.errors;
	}

	private void addError(String s) {
		errors.add(s);
	}

	private void addError(int lineNumber, String line, String code) {
		addError(lineNumber, null, null, line, null, code);
	}

	private void addError(int lineNumber, Integer columnNumber, String column,
			String columnName, String line, String code) {
		BatchErrorLog errorLog = new BatchErrorLog();
		errorLog.setErrorDefCode(code);
		errorLog.setColumnNo(columnNumber == null ? null : new Long(
				columnNumber));
		errorLog.setImportColumnValue(column);
		errorLog.setTransDtlColumnName(columnName);
		errorLog.setImportRow(line);
		errorLog.setRowNo(new Long(lineNumber));
		batchErrorLogs.add(errorLog);
	}

	@Override
	public void processHeader(BufferedReader reader) throws IOException {
		String firstLine = reader.readLine();

		this.fileType = UpdateFilesBackingBean.getHeaderValueOfType(firstLine, UpdateFilesBackingBean.TYPE_VALUE);
		if(this.fileType==null) {
			this.fileType = "Update";
		}

		String dateStr = UpdateFilesBackingBean.getHeaderValueOfType(firstLine, UpdateFilesBackingBean.DATE_VALUE);
		if(dateStr==null) {
			addError(1, firstLine, "SDM21");
			this.releaseDate = null;
		}
		else {
			try {
				this.releaseDate = sdfRelease.parse(dateStr);
			} catch (ParseException e) {
				addError(1, firstLine, "SDM21");
				this.releaseDate = null;
			}
		}

		String release = UpdateFilesBackingBean.getHeaderValueOfType(firstLine, UpdateFilesBackingBean.REL_VALUE);
		if(release!=null) {
			this.releaseVersion = release;
		}
		else {
			this.releaseVersion = "1"; 
		}
	}

	public void upload(String fileName, InputStream stream) {
		clear();
		try {
			super.upload(fileName, stream);
		} catch (Exception e) {
			addError(1, e.getMessage(), "SDM26");
			logger.error(e);
		}
	}

	@Override
	public boolean hasNext() {
		if (bufferedReader == null)
			return false;
		try {
			nextLine = bufferedReader.readLine();
			if (nextLine == null) {
				bufferedReader.close();
				bufferedReader = null;
				return false;
			}
			return nextLine.charAt(0) != '~';
		} catch (IOException e) {
			addError(e.getMessage());
			return false;
		}
	}

	@Override
	public BCPSitusRule next() {
		if ((bufferedReader == null) || (nextLine == null))
			return null;
		if ((lineNumber % 200) == 0) {
			logger.debug("Line: " + lineNumber + ", errorCount:"
					+ batchErrorLogs.size());
		}
		String tokens[] = nextLine.split("\\t");
		
		BCPSitusRule r = new BCPSitusRule();
		
		if(tokens.length==0){
			return r;
		}
		
		String recordType = (String)tokens[0];
		if(recordType.equalsIgnoreCase("SD")){
			if (tokens.length != COLUMNS_SD.length) {
				addError(lineNumber, null, "Expected " + COLUMNS_SD.length
						+ ", actual: " + tokens.length, null, nextLine, "SDM27"); // Not enough columns for SD
			} else {
				int i = 0;
				String parsedColumn = null;
				try {
					r.setLine(new Long(lineNumber));
					r.setText(nextLine);
					
					r.setRecordType(parsedColumn = tokens[i++]);
					r.setSddSitusDriverId(parseLong(parsedColumn = tokens[i++]));
					r.setSddSitusDriverDetailId(parseLong(parsedColumn = tokens[i++]));
					r.setTransactionTypeCode(parsedColumn = tokens[i++]);
					r.setRatetypeCode(parsedColumn = tokens[i++]);
					r.setMethodDeliveryCode(parsedColumn = tokens[i++]);
					r.setSddLocationTypeCode(parsedColumn = tokens[i++]);
					r.setSddDriverId(parseLong(parsedColumn = tokens[i++]));
					r.setSddMandatoryFlag(parsedColumn = tokens[i++]);
					r.setUpdateCode(parsedColumn = tokens[i++]);
					
				} catch (Exception e) {
					logger.error(e.getMessage(), e);
					addError(lineNumber, i, parsedColumn, COLUMNS_SD[i - 1], nextLine,
							"SDM23");
				}

				lineNumber++;
			}
		}
		else if(recordType.equalsIgnoreCase("SM")){
			if (tokens.length != COLUMNS_SM.length) {
				addError(lineNumber, null, "Expected " + COLUMNS_SM.length
						+ ", actual: " + tokens.length, null, nextLine, "SDM28"); // Not enough columns for SD
			} else {
				int i = 0;
				String parsedColumn = null;
				try {
					r.setLine(new Long(lineNumber));
					r.setText(nextLine);
					
					r.setRecordType(parsedColumn = tokens[i++]);
					r.setSmSitusMatrixId(parseLong(parsedColumn = tokens[i++]));
					
					r.setTransactionTypeCode(parsedColumn = tokens[i++]);
					r.setRatetypeCode(parsedColumn = tokens[i++]);
					r.setMethodDeliveryCode(parsedColumn = tokens[i++]);
					r.setSmSitusCountryCode(parsedColumn = tokens[i++]);
					r.setSmSitusStateCode(parsedColumn = tokens[i++]);
					r.setSmJurLevel(parsedColumn = tokens[i++]);	
					
					r.setSmDriver01(parsedColumn = tokens[i++]);
					r.setSmDriver02(parsedColumn = tokens[i++]);
					r.setSmDriver03(parsedColumn = tokens[i++]);
					r.setSmDriver04(parsedColumn = tokens[i++]);
					r.setSmDriver05(parsedColumn = tokens[i++]);
					r.setSmDriver06(parsedColumn = tokens[i++]);
					r.setSmDriver07(parsedColumn = tokens[i++]);
					r.setSmDriver08(parsedColumn = tokens[i++]);
					r.setSmDriver09(parsedColumn = tokens[i++]);
					r.setSmDriver10(parsedColumn = tokens[i++]);
					
					r.setSmDriver11(parsedColumn = tokens[i++]);
					r.setSmDriver12(parsedColumn = tokens[i++]);
					r.setSmDriver13(parsedColumn = tokens[i++]);
					r.setSmDriver14(parsedColumn = tokens[i++]);
					r.setSmDriver15(parsedColumn = tokens[i++]);
					r.setSmDriver16(parsedColumn = tokens[i++]);
					r.setSmDriver17(parsedColumn = tokens[i++]);
					r.setSmDriver18(parsedColumn = tokens[i++]);
					r.setSmDriver19(parsedColumn = tokens[i++]);
					r.setSmDriver20(parsedColumn = tokens[i++]);
					
					r.setSmDriver21(parsedColumn = tokens[i++]);
					r.setSmDriver22(parsedColumn = tokens[i++]);
					r.setSmDriver23(parsedColumn = tokens[i++]);
					r.setSmDriver24(parsedColumn = tokens[i++]);
					r.setSmDriver25(parsedColumn = tokens[i++]);
					r.setSmDriver26(parsedColumn = tokens[i++]);
					r.setSmDriver27(parsedColumn = tokens[i++]);
					r.setSmDriver28(parsedColumn = tokens[i++]);
					r.setSmDriver29(parsedColumn = tokens[i++]);
					r.setSmDriver30(parsedColumn = tokens[i++]);
	
					r.setSmBinaryWeight(parseBigDecimal(parsedColumn = tokens[i++]));
					try {
						r.setSmEffectiveDate(sdf.parse(parsedColumn = tokens[i++]));
					}
					catch(Exception e) {
						r.setSmEffectiveDate(null);
						throw e;
					}
					
					try {
						r.setSmExpirationDate(sdf.parse(parsedColumn = tokens[i++]));
					} catch (Exception e) {
						r.setSmExpirationDate(sdf.parse("9999/12/31"));
					}
					
					r.setSmPrimarySitusCode(parsedColumn = tokens[i++]);
					r.setSmPrimaryTaxtypeCode(parsedColumn = tokens[i++]);
					r.setSmSecondarySitusCode(parsedColumn = tokens[i++]);
					r.setSmSecondaryTaxtypeCode(parsedColumn = tokens[i++]);
					
					r.setSmComments(parsedColumn = tokens[i++]);
					r.setSmAllLevelsFlag(parsedColumn = tokens[i++]);
					r.setSmActiveFlag(parsedColumn = tokens[i++]);	
					r.setUpdateCode(parsedColumn = tokens[i++]);
					
				} catch (Exception e) {
					logger.error(e.getMessage(), e);
					addError(lineNumber, i, parsedColumn, COLUMNS_SM[i - 1], nextLine,
							"SDM23");
				}

				lineNumber++;
			}	
		}

		return r;
	}

	@Override
	public void remove() {
	}

	public Iterator<BCPSitusRule> iterator() {
		lineNumber = 2;
		linesWritten = 0;
		nextLine = null;
		try {
			bufferedReader = new BufferedReader(new FileReader(uploadFilePath));
		} catch (FileNotFoundException e) {
			logger.error(e.getMessage(), e);
			addError(e.getMessage());
		}
		return this;
	}

	public void checksum() {
		if (nextLine == null || nextLine.trim().length() == 0) {
			addIP6Error();
			return;
		}
		
		
		String footer[] = nextLine.split(" +");
		if(footer.length == 2) {
			if(footer[1].contains("#")) {
				Long lines = Long.decode((footer[1].replaceAll("\\#", "")).trim());
				if (lines != getRows()) {
					addIP4Error(lines);
				}
			}
			return;
		}
		
		if(footer.length == 1) {
			addIP7Error(); 
			
		}

		// checksum specialRates
//		BigDecimal total = parseBigDecimal(footer[2].replaceAll("\\$", ""));
///		NumberFormat nf = NumberFormat.getNumberInstance();
//		nf.setMaximumFractionDigits(6);
//		if (!nf.format(specialRateTotal).equals(nf.format(total))) {
//			String s = "EOF Marker: " + nf.format(total) + ", actual total: "
////					+ nf.format(specialRateTotal);
///			addError(lineNumber, null, s, "Invalid Special Rate Total",
//					nextLine, "SDM15");
//		}
	}
	
	public void addIP4Error(Long lines) {
		String s = "EOF Marker: " + lines + ", actual count: " + getRows();
		String errorCodeDesc = errorListCodes.get("IP4");
		if(errorCodeDesc != null && errorCodeDesc.length()>0) {
			addError(lineNumber, null, s, errorCodeDesc, nextLine, "IP4");
		}
		else {
			addError(lineNumber, null, s, "IP4", nextLine, "IP4");
		}
	}

	public void addIP6Error() {
		String errorCodeDesc = errorListCodes.get("IP6");
		if(errorCodeDesc != null && errorCodeDesc.length()>0) {
			addError(lineNumber, null, errorCodeDesc, errorCodeDesc, "", "IP6");
		}
		else {
			addError(lineNumber, null, "IP6", "IP6", "", "IP6");
		}
	}

	public void addIP7Error() {
		String errorCodeDesc = errorListCodes.get("IP7");
		if(errorCodeDesc != null && errorCodeDesc.length()>0) {
			addError(lineNumber, null, errorCodeDesc, errorCodeDesc, nextLine,"IP7");
		}
		else {
			addError(lineNumber, null, "IP7", "IP7", nextLine,"IP7");
		}
	}

	public BigDecimal parseBigDecimal(String s) {
		if ((s == null) || (s.trim().length() == 0))
			return null;
		return new BigDecimal(s);
	}
	
	private Long parseLong(String s) {
		if ((s == null) || (s.trim().length() == 0)) return null;
		return Long.parseLong(s);
	}

	public Long getTotalBytes() {
		return this.totalBytes;
	}

	public List<BatchErrorLog> getBatchErrorLogs() {
		return this.batchErrorLogs;
	}

	public BigDecimal getSpecialRateTotal() {
		return specialRateTotal;
	}

	public int getRows() {
		return lineNumber - 2;
	}

	public void setLinesWritten(int linesWritten) {
		this.linesWritten = linesWritten;
	}
}
