package com.ncsts.view.util;

import java.net.URL;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

import org.apache.myfaces.component.html.ext.HtmlOutputText;

public class LinkConverter implements Converter {

	@Override
	public Object getAsObject(FacesContext context, UIComponent component,
			String value) {
		return value;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component,
			Object value) {
		String sValue = "";
		if(value != null) {
			sValue = value.toString();
			if(component instanceof HtmlOutputText || component instanceof javax.faces.component.html.HtmlOutputText) {
				String lower = sValue.toLowerCase().trim(); 
				if(lower.startsWith("http://") || lower.startsWith("https://") || lower.startsWith("file:") || lower.startsWith("\\\\")) {
					try {
						URL url = null;
						try {
							url = new URL(sValue);
						}
						catch(Exception ex) {}
						if(url != null || lower.startsWith("\\\\")) {
							if(component instanceof HtmlOutputText) {
								((HtmlOutputText) component).setEscape(false);
								((HtmlOutputText) component).decode(context);
							}
							else if(component instanceof javax.faces.component.html.HtmlOutputText) {
								((javax.faces.component.html.HtmlOutputText) component).setEscape(false);
							}
							sValue = "<a href=\"" + sValue + "\" target=\"_blank\">" + sValue + "</a>";
						}
					}
					catch(Exception e){}					
				}
			}
		}
		
		return sValue;
	}

}
