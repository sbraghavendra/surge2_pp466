package com.ncsts.view.util;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.ncsts.ws.PinPointWebServiceConstants;

@XmlRootElement(namespace= PinPointWebServiceConstants.MESSAGES_NAMESPACE)
@XmlType(namespace=PinPointWebServiceConstants.MESSAGES_NAMESPACE)
@XmlAccessorType(XmlAccessType.FIELD)

public class BatchTypeSpecific {
	
	private String id;
	private String objectHeading;
	private String batchFieldname;
	private String batchFieldtype;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getObjectHeading() {
		return objectHeading;
	}
	public void setObjectHeading(String objectHeading) {
		this.objectHeading = objectHeading;
	}
	public String getBatchFieldname() {
		return batchFieldname;
	}
	public void setBatchFieldname(String batchFieldname) {
		this.batchFieldname = batchFieldname;
	}
	public String getBatchFieldtype() {
		return batchFieldtype;
	}
	public void setBatchFieldtype(String batchFieldtype) {
		this.batchFieldtype = batchFieldtype;
	}
}
