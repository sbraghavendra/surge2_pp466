package com.ncsts.view.util;

import java.text.DecimalFormat;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.NumberConverter;

public class NumberToBlankConverter extends NumberConverter{
	
	private static final String FLOATZERO = "0.0";
	private static final String ZERO = "0";
	private static final DecimalFormat formatter = new DecimalFormat("#0.#################");
	
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
 		Double decimal = null;
 		
 		if(value!=null){
 			value = value.replaceAll(",", "");
 			if(value.equals("")){
 				decimal=null;
 			}else{
 				decimal = Double.valueOf(value);
 				return formatter.format(decimal);
 			}
 		}
 	    return decimal;
 	}
 	
 	public String getAsString(FacesContext context, UIComponent component, Object value) {
 		
 		String decimal= value.toString();
 		if(FLOATZERO.equals(value.toString())) { 
 			if(FLOATZERO.compareTo(value.toString()) != 0) {
 		 			decimal = value.toString();
 		 	}else{
 		 		decimal="";
 		 	}
 		}
 		
 		if(ZERO.equals(value.toString())) {
 			if(ZERO.compareTo(value.toString()) != 0) {
 		 			decimal = value.toString();
 		 	}else{
 		 		decimal="";
 		 	}
 		}
 		if(decimal != null && !"".equals(decimal)) {
 			return formatter.format(Double.valueOf(decimal));
 		}
 		return decimal;
 		
 	}
}