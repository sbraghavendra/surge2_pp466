package com.ncsts.view.util;

import javax.el.MethodExpression;
import javax.el.ValueExpression;
import java.text.DecimalFormat;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.validator.MethodExpressionValidator;

public class JsfHelper {
	
	
	final static String DATE_FORMAT_PART        		= "yyMM";
	final static String SEQUENCE_FORMAT_PART    		= "0000";
	final static SimpleDateFormat   datePart    		=  new SimpleDateFormat(DATE_FORMAT_PART);
	final static DecimalFormat     sequencePart    		=  new DecimalFormat(SEQUENCE_FORMAT_PART);
	final static String            CUSTOMER_ID_FORMAT   = "Y{0}{1}";
	final static MessageFormat customerPattern 			= new MessageFormat(CUSTOMER_ID_FORMAT);

  public static long getId(String parameterName) {
    long value = -1;
    String valueText = (String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get(parameterName);
    try {
      Long id = new Long(valueText);
      value = id.longValue();
    } catch (NumberFormatException e) {
      throw new RuntimeException("couldn't parse '"+parameterName+"'='"+valueText+"' as a long");
    }
    return value;
  }
  
  public static void addMessage(UIComponent c, String msg, FacesMessage.Severity severity) {
    FacesContext.getCurrentInstance().addMessage(c.getClientId(FacesContext.getCurrentInstance()),
    		new FacesMessage(severity, msg, null));
  }
  
  public static void addWarning(UIComponent c, String msg) {
  	addWarning(c.getClientId(FacesContext.getCurrentInstance()), msg);
  }
  public static void addWarning(String id, String msg) {
    FacesContext.getCurrentInstance().addMessage(id, new FacesMessage(FacesMessage.SEVERITY_WARN, msg, null));
  }
  public static void addWarning(String msg) {
  	addWarning((String)null, msg);
  }

  public static void addError(UIComponent c, String msg) {
  	addError(c.getClientId(FacesContext.getCurrentInstance()), msg);
  }
  public static void addError(String id, String msg) {
    FacesContext.getCurrentInstance().addMessage(id, new FacesMessage(FacesMessage.SEVERITY_ERROR, msg, null));
  }
  public static void addError(String msg) {
  	addError((String)null, msg);
  }
  public static void addMessage(String msg) {
    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(msg));
  }
  
  public static void addMessage(FacesMessage fmsg) {
	    FacesContext.getCurrentInstance().addMessage(null, fmsg );
	  }

  public static void setSessionAttribute(String key, Object value) {
    FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put(key, value);
  }

  public static Object getSessionAttribute(String key) {
    return FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get(key);
  }

  public static void removeSessionAttribute(String key) {
    FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove(key);
  }

  public static String getParameter(String name) {
    return (String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get(name);
  }
  
  
  public static FacesContext getFaceContext() {
	    return FacesContext.getCurrentInstance();
	  }
  
  public static ValueExpression createValueExpression(String valueExpression, Class<?> valueType) {
      FacesContext facesContext = FacesContext.getCurrentInstance();
      return facesContext.getApplication().getExpressionFactory().createValueExpression(
          facesContext.getELContext(), valueExpression, valueType);
  }

  public static MethodExpression createActionExpression(String actionExpression, Class<?> returnType) {
      FacesContext facesContext = FacesContext.getCurrentInstance();
      return facesContext.getApplication().getExpressionFactory().createMethodExpression(
          facesContext.getELContext(), actionExpression, returnType, new Class[0]);
  }

  public static MethodExpression createListenerExpression(String listenerExpression, Class<?> argType) {
  	Class<?>[] argList = { argType };
      FacesContext facesContext = FacesContext.getCurrentInstance();
      return facesContext.getApplication().getExpressionFactory().createMethodExpression(
          facesContext.getELContext(), listenerExpression, null, argList);
  }

  public static MethodExpression createSuggestionActionExpression(String actionExpression) {
	Class<?>[] argList = { Object.class };
      FacesContext facesContext = FacesContext.getCurrentInstance();
      return facesContext.getApplication().getExpressionFactory().createMethodExpression(
          facesContext.getELContext(), actionExpression, Object.class, argList);
  }
  
  public static MethodExpressionValidator createValidatorExpression(String validatorExpression) {
		Class<?>[] argList = { FacesContext.class, UIComponent.class, Object.class };
	      FacesContext facesContext = FacesContext.getCurrentInstance();
	      return new MethodExpressionValidator(
	    		  facesContext.getApplication().getExpressionFactory().createMethodExpression(
	              facesContext.getELContext(), validatorExpression, Object.class, argList));
  }

  // private static final Log log = LogFactory.getLog(JsfHelper.class);
  // PDB - commenting out unused methods that use deprecated function calls
/*
  /**
   * Get the value of bean through EL
   * <pre>
   * SSN.getValueBinding("#{searchBean.code}")
   * </pre>
   *
   * @param el String  EL presentation of bean
   * @see javax.faces.context.FacesContext
   *
  public static Object getValueBinding(String el) {
      FacesContext fc = FacesContext.getCurrentInstance();
      return fc.getApplication().createValueBinding(el).getValue(fc);
  }

  /**
   * Set the value of bean through EL
   * <pre>
   * SSN.setValueBinding("#{searchBean.code}")
   * </pre>
   *
   * @param el String  EL presentation of bean
   * @see javax.faces.context.FacesContext
   *
  public static void setValueBinding(String el, Object newValue) {
      FacesContext fc = FacesContext.getCurrentInstance();
      fc.getApplication().createValueBinding(el).setValue(fc, newValue);
  }

  /**
   * Set the value of bean through EL
   * <pre>
   * SSN.setMethodBinding("#{searchBean.search}")
   * </pre>
   *
   * @param el String  EL presentation of bean
   * @see javax.faces.context.FacesContext
   *
  public static void setMethodBinding(String el) {
      FacesContext fc = FacesContext.getCurrentInstance();
      fc.getApplication().createMethodBinding(el, null).invoke(fc, null);
  }

  public static Object getPageBean(String beanReference) {
      FacesContext fc = FacesContext.getCurrentInstance();
      VariableResolver vr = fc.getApplication().getVariableResolver();
      return vr.resolveVariable(fc, beanReference);
  }
*/  
  
  public static final String  getGeneratedCustomerId(int sequence)
  {
	  String yymm = datePart.format(Calendar.getInstance().getTime());
	  String seq  = sequencePart.format(sequence);
	  return customerPattern.format(new Object[]{yymm, seq});
  }
  
  public static final int  getNextSequence(int previousSequence)
  {
	   int day = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
	   if (day==1)  // first of the month 
		   return 1;
	   else
	       return previousSequence+1;
	   
	   
  }
  
  public static int getRowIndex(String clientId)
	{
		String[] parsedId  = clientId.split(":");
		return Integer.parseInt(parsedId[parsedId.length-2]);
	}
  
  public static int getRowIndex(UIInput uiInput)
	{
	   return getRowIndex(uiInput.getClientId(JsfHelper.getFaceContext()));
	 
	}
  
}
