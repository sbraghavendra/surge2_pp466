package com.ncsts.view.util;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.NumberFormat;

public class ArithmeticUtils {
	public static final NumberFormat DEFAULT_DECIMAL_FORMAT = new DecimalFormat("#.0#################");
	public static final BigDecimal ZERO = new BigDecimal ("0");
	
	public static BigDecimal toBigDecimal(Object o) {
		BigDecimal bd = ZERO;
		if(o != null) {
			try {
				bd = new BigDecimal(o.toString()).setScale(7, RoundingMode.HALF_EVEN);
			}
			catch(Exception e) {
				e.printStackTrace();
			}
		}
		
		return bd;
	}
	
	public static BigDecimal toBigDecimal(String n) {
		return new BigDecimal(n).setScale(7, RoundingMode.HALF_EVEN);
	}
	
	public static BigDecimal decimalFormat(BigDecimal bd) {
		return bd.setScale(7, RoundingMode.HALF_EVEN);
	}
	
	public static BigDecimal toBigDecimal(float n) {
		String s = DEFAULT_DECIMAL_FORMAT.format(n);
		return new BigDecimal(s).setScale(7, RoundingMode.HALF_EVEN);
	}
	
	public static BigDecimal toBigDecimal(double n) {
		String s = DEFAULT_DECIMAL_FORMAT.format(n);
		return new BigDecimal(s).setScale(7, RoundingMode.HALF_EVEN);
	}
	
	public static BigDecimal toBigDecimal(long n) {
		String s = DEFAULT_DECIMAL_FORMAT.format(n);
		return new BigDecimal(s).setScale(7, RoundingMode.HALF_EVEN);
	}
	
	public static BigDecimal add (double a, double b) {
	    BigDecimal bd = toBigDecimal(a);
	    return add (bd, b);
	}

	public static BigDecimal add (BigDecimal a, double b) {
	    BigDecimal bd = toBigDecimal(b);
	    return add (a, bd);
	}
	
	public static BigDecimal add (BigDecimal a, Float b) {
	    BigDecimal bd = toBigDecimal(b);
	    return add (a, bd);
	}
	
	public static BigDecimal add (BigDecimal a, Double b) {
	    BigDecimal bd = toBigDecimal(b);
	    return add (a, bd);
	}
	
	public static BigDecimal add (BigDecimal a, BigDecimal b) {
	    if (a == null) a = ZERO;
	    if (b == null) b = ZERO;
	    
	    a = a.setScale(7, RoundingMode.HALF_EVEN);
	    b = b.setScale(7, RoundingMode.HALF_EVEN);
	    
	    return a.add(b).setScale(7, RoundingMode.HALF_EVEN);
	}
	
	public static BigDecimal subtract (double a, double b) {
	    BigDecimal bd = toBigDecimal(a);
	    return subtract (bd, b);
	}

	public static BigDecimal subtract (BigDecimal a, double b) {
	    BigDecimal bd = toBigDecimal(b);
	    return subtract (a, bd);
	}
	
	public static BigDecimal subtract (BigDecimal a, Float b) {
	    BigDecimal bd = toBigDecimal(b);
	    return subtract (a, bd);
	}
	
	public static BigDecimal subtract (BigDecimal a, Double b) {
	    BigDecimal bd = toBigDecimal(b);
	    return subtract (a, bd);
	}

	public static BigDecimal subtract (BigDecimal a, BigDecimal b) {
		if (a == null) a = ZERO;
	    if (b == null) b = ZERO;
	    
	    a = a.setScale(7, RoundingMode.HALF_EVEN);
	    b = b.setScale(7, RoundingMode.HALF_EVEN);
	    
	    return a.subtract(b).setScale(7, RoundingMode.HALF_EVEN);
	}
	
	public static BigDecimal convert (double n) {
		return toBigDecimal(n);
	}
	
	public static BigDecimal divide (BigDecimal a, BigDecimal b) {
		if (a == null) a = ZERO;
	    if (b == null) b = ZERO;
	    
	    if(b.compareTo(ZERO) == 0) {
	    	return ZERO;
	    }
	    else {
	    	a = a.setScale(7, RoundingMode.HALF_EVEN);
		    b = b.setScale(7, RoundingMode.HALF_EVEN);
	    	
		    return a.divide(b, 7, RoundingMode.HALF_EVEN);
	    }
	}
	
	public static BigDecimal multiply (BigDecimal a, BigDecimal b) {
		if (a == null) a = ZERO;
	    if (b == null) b = ZERO;
	    
	    if(b.compareTo(ZERO) == 0) {
	    	return ZERO;
	    }
	    else {
	    	a = a.setScale(7, RoundingMode.HALF_EVEN);
		    b = b.setScale(7, RoundingMode.HALF_EVEN);
		    
		    return a.multiply(b).setScale(7, RoundingMode.HALF_EVEN);
	    }
	}
}
