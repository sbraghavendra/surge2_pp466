/*
 * Author: Jim M. Wilson
 * Created: Sep 15, 2008
 * $Date: $: - $Revision: 2239 $: 
 */

package com.ncsts.view.util;

import java.beans.PropertyDescriptor;
import java.util.HashMap;

import org.hibernate.ejb.HibernateEntityManager;
import org.hibernate.ejb.HibernateEntityManagerFactory;
import org.hibernate.persister.entity.AbstractEntityPersister;
import org.springframework.beans.BeanUtils;

/**
 * Useful hibernate utility methods
 */
public class HibernateUtils {

	private HibernateEntityManager entityManager;
	
	public HibernateUtils(HibernateEntityManagerFactory hem) {
		this.entityManager = (HibernateEntityManager)hem.createEntityManager();
	}
	
	/**
	 * Get all the mappings from column name to setter method for a class
	 */
  public HashMap<String, PropertyDescriptor> getColumnToProperties(Class<?> clazz) {
 	 AbstractEntityPersister metadata = (AbstractEntityPersister) entityManager.getSession().getSessionFactory().getClassMetadata(clazz);
 	 HashMap<String, PropertyDescriptor> map = new HashMap<String, PropertyDescriptor>();
 	 int i = 0;
 	 for (String s : metadata.getPropertyNames()) {
 		 map.put(metadata.getPropertyColumnNames(i++)[0], BeanUtils.getPropertyDescriptor(clazz, s));
 	 }
 	 return map;
  }

  /**
   * get  the table name for a class
   */
  public String getTableName(Class<?> clazz) {
  	AbstractEntityPersister metadata = (AbstractEntityPersister) entityManager.getSession().getSessionFactory().getClassMetadata(clazz);
  	return metadata.getTableName();
  }

	public HibernateEntityManager getEntityManager() {
		return this.entityManager;
	}

	public void setEntityManager(HibernateEntityManager entityManager) {
		this.entityManager = entityManager;
	}
  
  
}
