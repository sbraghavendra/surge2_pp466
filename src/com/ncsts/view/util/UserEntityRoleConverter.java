package com.ncsts.view.util;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

import com.ncsts.domain.EntityItem;
import com.ncsts.domain.Role;
import com.ncsts.domain.User;
import com.ncsts.dto.UserEntityRoleDTO;

public class UserEntityRoleConverter implements Converter {

	public Object getAsObject(FacesContext context, UIComponent component,
			String value) {

		System.out.println("Get index of : " + value);
		String[] index = value.split("~");
		
		String ent[] = index[0].split("::"); 
		String rl[] = index[1].split("::");
		String usr[] = index[2].split("::");
		
		EntityItem entity = new EntityItem(ent[1] , ent[2] , ent[3]);
		Role role = new Role(rl[1],rl[2]);
		User user = new User(usr[1],usr[2]);
		
		return new UserEntityRoleDTO(entity, user, role);
	}

	public String getAsString(FacesContext context, UIComponent component,
			Object value) {
		UserEntityRoleDTO optionItem = (UserEntityRoleDTO)value;
		EntityItem entity = optionItem.getEntityItem();
		Role role = optionItem.getRole();
		User user = optionItem.getUser();
		
		 return entity.toString() + "~" + role.toString() + "~" + user.toString();
		}


}
