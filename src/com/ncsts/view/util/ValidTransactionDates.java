package com.ncsts.view.util;

import java.text.SimpleDateFormat;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

public class ValidTransactionDates {
	
    private Object accountingGLDate;
	
	private Object accountingvoucherDate;
	
	private Object accountingCheckDate;
	
	private Object transactionEnteredDate;
	
	private Object documentationInvoiceDate;
	
	private Object documentationPoDate;
	
	private Object documentationWoDate;
	
	private Object userFieldsDate01;
	
	private Object userFieldsDate02;
	
	private Object userFieldsDate03;
	
	private Object userFieldsDate04;
	
	private Object userFieldsDate05;
	
	private Object userFieldsDate06;
	
	private Object userFieldsDate07;
	
	private Object userFieldsDate08;
	
	private Object userFieldsDate09;
	
	private Object userFieldsDate10;
	
	private Object userFieldsDate11;
	
	private Object userFieldsDate12;
	
	private Object userFieldsDate13;
	
	private Object userFieldsDate14;
	
	private Object userFieldsDate15;
	
	private Object userFieldsDate16;
	
	private Object userFieldsDate17;
	
	private Object userFieldsDate18;
	
	private Object userFieldsDate19;
	
	private Object userFieldsDate20;
	
	private Object userFieldsDate21;
	
	private Object userFieldsDate22;
	
	private Object userFieldsDate23;
	
	private Object userFieldsDate24;
	
	private Object userFieldsDate25;
	
	private SimpleDateFormat dateformat = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy");
	
	boolean error  = false;
	
	public String validateDates() {
		error = false;
		validateAcctingDate();
		validatetransEnteredDate();
		validateDocumentDate();
		validateUserFieldsDate();
		
		if(error){
			return null;
		}
		return "OK";
	}
	
	public void validateAcctingDate(){
		validateAcctingGlDate();
		validateAcctingVoucherDate();
		validateAcctingCheckDate();
	}
	
	public void validateUserFieldsDate(){
		validateUserField01Date();
		validateUserField02Date();
		validateUserField03Date();
		validateUserField04Date();
		validateUserField05Date();
		validateUserField06Date();
		validateUserField07Date();
		validateUserField08Date();
		validateUserField09Date();
		validateUserField10Date();
		validateUserField11Date();
		validateUserField12Date();
		validateUserField13Date();
		validateUserField14Date();
		validateUserField15Date();
		validateUserField16Date();
		validateUserField17Date();
		validateUserField18Date();
		validateUserField19Date();
		validateUserField20Date();
		validateUserField21Date();
		validateUserField22Date();
		validateUserField23Date();
		validateUserField24Date();
		validateUserField25Date();
	}
	
	public void validateDocumentDate(){
		validateDocumentInvoiceDate();
		validateDocumentPoDate();
		validateDocumentWoDate();
	}
	
	public void validateAcctingGlDate(){
		validateDateAndSetErrorMessage(accountingGLDate,"Error on \"Accounting\" Page: invalid GL Date.");
	}
	
	public void validateAcctingVoucherDate() {
		validateDateAndSetErrorMessage(accountingvoucherDate,"Error on \"Accounting\" Page: invalid Voucher Date.");
	}
	
	public void validateAcctingCheckDate(){
		validateDateAndSetErrorMessage(accountingCheckDate,"Error on \"Accounting\" Page: invalid Check Date.");
	}
	
	public void validatetransEnteredDate() {
		validateDateAndSetErrorMessage(transactionEnteredDate,"Error on \"Transaction\" Page: invalid Entered Date.");
	}
	
	public void validateDocumentInvoiceDate() {
		validateDateAndSetErrorMessage(documentationInvoiceDate,"Error on \"Documentation\" Page: invalid Invoice Date.");
	}
	
	public void validateDocumentPoDate(){
		validateDateAndSetErrorMessage(documentationPoDate,"Error on \"Documentation\" Page: invalid Po Date.");
	}
	
	public void validateDocumentWoDate() {
		validateDateAndSetErrorMessage(documentationWoDate,"Error on \"Documentation\" Page: invalid Wo Date.");
	}
	
	public void validateUserField01Date(){
		validateDateAndSetErrorMessage(userFieldsDate01,"Error on \"User Fields\" Page: invalid CEO's Birthday.");
	}
	
	public void validateUserField02Date(){
		validateDateAndSetErrorMessage(userFieldsDate02,"Error on \"User Fields\" Page: invalid User Date02.");
	}
	
	public void validateUserField03Date(){
		validateDateAndSetErrorMessage(userFieldsDate03,"Error on \"User Fields\" Page: invalid User Date03.");
	}
	
	public void validateUserField04Date(){
		validateDateAndSetErrorMessage(userFieldsDate04,"Error on \"User Fields\" Page: invalid User Date04.");
	}
	
	public void validateUserField05Date(){
		validateDateAndSetErrorMessage(userFieldsDate05,"Error on \"User Fields\" Page: invalid User Date05.");
	}
	
	public void validateUserField06Date(){
		validateDateAndSetErrorMessage(userFieldsDate06,"Error on \"User Fields\" Page: invalid User Date06.");
	}
	
	public void validateUserField07Date(){
		validateDateAndSetErrorMessage(userFieldsDate07,"Error on \"User Fields\" Page: invalid User Date07.");
	}
	
	public void validateUserField08Date(){
		validateDateAndSetErrorMessage(userFieldsDate08,"Error on \"User Fields\" Page: invalid User Date08.");
	}
	
	public void validateUserField09Date(){
		validateDateAndSetErrorMessage(userFieldsDate09,"Error on \"User Fields\" Page: invalid User Date09.");
	}
	
	public void validateUserField10Date(){
		validateDateAndSetErrorMessage(userFieldsDate10,"Error on \"User Fields\" Page: invalid User Date10.");
	}
	
	public void validateUserField11Date(){
		validateDateAndSetErrorMessage(userFieldsDate11,"Error on \"User Fields\" Page: invalid User Date11.");
	}
	
	public void validateUserField12Date(){
		validateDateAndSetErrorMessage(userFieldsDate12,"Error on \"User Fields\" Page: invalid User Date12.");
	}
	
	public void validateUserField13Date(){
		validateDateAndSetErrorMessage(userFieldsDate13,"Error on \"User Fields\" Page: invalid User Date13.");
	}
	
	public void validateUserField14Date(){
		validateDateAndSetErrorMessage(userFieldsDate14,"Error on \"User Fields\" Page: invalid User Date14.");
	}
	
	public void validateUserField15Date(){
		validateDateAndSetErrorMessage(userFieldsDate15,"Error on \"User Fields\" Page: invalid User Date15.");
	}
	
	public void validateUserField16Date(){
		validateDateAndSetErrorMessage(userFieldsDate16,"Error on \"User Fields\" Page: invalid User Date16.");
	}
	
	public void validateUserField17Date(){
		validateDateAndSetErrorMessage(userFieldsDate17,"Error on \"User Fields\" Page: invalid User Date17.");
	}
	
	public void validateUserField18Date(){
		validateDateAndSetErrorMessage(userFieldsDate18,"Error on \"User Fields\" Page: invalid User Date18.");
	}
	
	public void validateUserField19Date(){
		validateDateAndSetErrorMessage(userFieldsDate19,"Error on \"User Fields\" Page: invalid User Date19.");
	}
	
	public void validateUserField20Date(){
		validateDateAndSetErrorMessage(userFieldsDate20,"Error on \"User Fields\" Page: invalid User Date20.");
	}
	
	public void validateUserField21Date(){
		validateDateAndSetErrorMessage(userFieldsDate21,"Error on \"User Fields\" Page: invalid User Date21.");
	}
	
	public void validateUserField22Date(){
		validateDateAndSetErrorMessage(userFieldsDate22,"Error on \"User Fields\" Page: invalid User Date22.");
	}
	
	public void validateUserField23Date(){
		validateDateAndSetErrorMessage(userFieldsDate23,"Error on \"User Fields\" Page: invalid User Date23.");
	}
	
	public void validateUserField24Date(){
		validateDateAndSetErrorMessage(userFieldsDate24,"Error on \"User Fields\" Page: invalid User Date24.");
	}
	
	public void validateUserField25Date(){
		validateDateAndSetErrorMessage(userFieldsDate25,"Error on \"User Fields\" Page:  invalid User Date25.");
	}

	public void validateDateAndSetErrorMessage(Object date, String errorMessage) {
		org.richfaces.component.html.HtmlCalendar dateCal = (org.richfaces.component.html.HtmlCalendar)date;
		try{
			if(dateCal !=null) {
				Object dateObject= (Object)dateCal.getSubmittedValue();	
				if(dateObject != null){
					dateformat.parse(dateObject.toString());
				}
			}
		}
		catch(Exception e) {
			FacesMessage message = new FacesMessage(errorMessage); 
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
			error = true;
		}
	}
	
	public Object getAccountingCheckDate() {
		return accountingCheckDate;
	}

	public Object getTransactionEnteredDate() {
		return transactionEnteredDate;
	}

	public Object getDocumentationInvoiceDate() {
		return documentationInvoiceDate;
	}

	public Object getDocumentationPoDate() {
		return documentationPoDate;
	}

	public Object getDocumentationWoDate() {
		return documentationWoDate;
	}

	public Object getUserFieldsDate01() {
		return userFieldsDate01;
	}

	public Object getUserFieldsDate02() {
		return userFieldsDate02;
	}

	public Object getUserFieldsDate03() {
		return userFieldsDate03;
	}

	public Object getUserFieldsDate04() {
		return userFieldsDate04;
	}

	public Object getUserFieldsDate05() {
		return userFieldsDate05;
	}

	public Object getUserFieldsDate06() {
		return userFieldsDate06;
	}

	public Object getUserFieldsDate07() {
		return userFieldsDate07;
	}

	public Object getUserFieldsDate08() {
		return userFieldsDate08;
	}

	public Object getUserFieldsDate09() {
		return userFieldsDate09;
	}

	public Object getUserFieldsDate10() {
		return userFieldsDate10;
	}

	public Object getUserFieldsDate11() {
		return userFieldsDate11;
	}

	public Object getUserFieldsDate12() {
		return userFieldsDate12;
	}

	public Object getUserFieldsDate13() {
		return userFieldsDate13;
	}

	public Object getUserFieldsDate14() {
		return userFieldsDate14;
	}

	public Object getUserFieldsDate15() {
		return userFieldsDate15;
	}

	public Object getUserFieldsDate16() {
		return userFieldsDate16;
	}

	public Object getUserFieldsDate17() {
		return userFieldsDate17;
	}

	public Object getUserFieldsDate18() {
		return userFieldsDate18;
	}

	public Object getUserFieldsDate19() {
		return userFieldsDate19;
	}

	public Object getUserFieldsDate20() {
		return userFieldsDate20;
	}

	public Object getUserFieldsDate21() {
		return userFieldsDate21;
	}

	public Object getUserFieldsDate22() {
		return userFieldsDate22;
	}

	public Object getUserFieldsDate23() {
		return userFieldsDate23;
	}

	public Object getUserFieldsDate24() {
		return userFieldsDate24;
	}

	public Object getUserFieldsDate25() {
		return userFieldsDate25;
	}

	public void setAccountingCheckDate(Object accountingCheckDate) {
		this.accountingCheckDate = accountingCheckDate;
	}

	public void setTransactionEnteredDate(Object transactionEnteredDate) {
		this.transactionEnteredDate = transactionEnteredDate;
	}

	public void setDocumentationInvoiceDate(Object documentationInvoiceDate) {
		this.documentationInvoiceDate = documentationInvoiceDate;
	}

	public void setDocumentationPoDate(Object documentationPoDate) {
		this.documentationPoDate = documentationPoDate;
	}

	public void setDocumentationWoDate(Object documentationWoDate) {
		this.documentationWoDate = documentationWoDate;
	}

	public void setUserFieldsDate01(Object userFieldsDate01) {
		this.userFieldsDate01 = userFieldsDate01;
	}

	public void setUserFieldsDate02(Object userFieldsDate02) {
		this.userFieldsDate02 = userFieldsDate02;
	}

	public void setUserFieldsDate03(Object userFieldsDate03) {
		this.userFieldsDate03 = userFieldsDate03;
	}

	public void setUserFieldsDate04(Object userFieldsDate04) {
		this.userFieldsDate04 = userFieldsDate04;
	}

	public void setUserFieldsDate05(Object userFieldsDate05) {
		this.userFieldsDate05 = userFieldsDate05;
	}

	public void setUserFieldsDate06(Object userFieldsDate06) {
		this.userFieldsDate06 = userFieldsDate06;
	}

	public void setUserFieldsDate07(Object userFieldsDate07) {
		this.userFieldsDate07 = userFieldsDate07;
	}

	public void setUserFieldsDate08(Object userFieldsDate08) {
		this.userFieldsDate08 = userFieldsDate08;
	}

	public void setUserFieldsDate09(Object userFieldsDate09) {
		this.userFieldsDate09 = userFieldsDate09;
	}

	public void setUserFieldsDate10(Object userFieldsDate10) {
		this.userFieldsDate10 = userFieldsDate10;
	}

	public void setUserFieldsDate11(Object userFieldsDate11) {
		this.userFieldsDate11 = userFieldsDate11;
	}

	public void setUserFieldsDate12(Object userFieldsDate12) {
		this.userFieldsDate12 = userFieldsDate12;
	}

	public void setUserFieldsDate13(Object userFieldsDate13) {
		this.userFieldsDate13 = userFieldsDate13;
	}

	public void setUserFieldsDate14(Object userFieldsDate14) {
		this.userFieldsDate14 = userFieldsDate14;
	}

	public void setUserFieldsDate15(Object userFieldsDate15) {
		this.userFieldsDate15 = userFieldsDate15;
	}

	public void setUserFieldsDate16(Object userFieldsDate16) {
		this.userFieldsDate16 = userFieldsDate16;
	}

	public void setUserFieldsDate17(Object userFieldsDate17) {
		this.userFieldsDate17 = userFieldsDate17;
	}

	public void setUserFieldsDate18(Object userFieldsDate18) {
		this.userFieldsDate18 = userFieldsDate18;
	}

	public void setUserFieldsDate19(Object userFieldsDate19) {
		this.userFieldsDate19 = userFieldsDate19;
	}

	public void setUserFieldsDate20(Object userFieldsDate20) {
		this.userFieldsDate20 = userFieldsDate20;
	}

	public void setUserFieldsDate21(Object userFieldsDate21) {
		this.userFieldsDate21 = userFieldsDate21;
	}

	public void setUserFieldsDate22(Object userFieldsDate22) {
		this.userFieldsDate22 = userFieldsDate22;
	}

	public void setUserFieldsDate23(Object userFieldsDate23) {
		this.userFieldsDate23 = userFieldsDate23;
	}

	public void setUserFieldsDate24(Object userFieldsDate24) {
		this.userFieldsDate24 = userFieldsDate24;
	}

	public void setUserFieldsDate25(Object userFieldsDate25) {
		this.userFieldsDate25 = userFieldsDate25;
	}

	public Object getAccountingGLDate() {
		return accountingGLDate;
	}

	public Object getAccountingvoucherDate() {
		return accountingvoucherDate;
	}

	public void setAccountingGLDate(Object accountingGLDate) {
		this.accountingGLDate = accountingGLDate;
	}

	public void setAccountingvoucherDate(Object accountingvoucherDate) {
		this.accountingvoucherDate = accountingvoucherDate;
	}
	
}
