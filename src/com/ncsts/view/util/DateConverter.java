/*
 * Author: Jim M. Wilson
 * Created: Sep 19, 2008
 * $Date: 2008-09-21 15:36:22 -0500 (Sun, 21 Sep 2008) $: - $Revision: 2361 $: 
 */
package com.ncsts.view.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

public class DateConverter extends  javax.faces.convert.DateTimeConverter {
	private static Logger log = Logger.getLogger(DateConverter.class);

	public static final String CONVERTER_ID = "date";
	private static final String DEFAULT_PATTERN = "MM/dd/yyyy";

	public DateConverter() {
		setPattern(DEFAULT_PATTERN);
	}
	
	 public String getAsString(FacesContext facesContext, UIComponent uiComponent, Object value) {
		 return super.getAsString(facesContext, uiComponent, value);
	 }

	 public Object getAsObject(FacesContext facesContext, UIComponent uiComponent, String value) {
		 return super.getAsObject(facesContext, uiComponent, value);
	 }
	 
	 public Date getUserLocalDate() {
		HttpSession session = (HttpSession)FacesContext.getCurrentInstance().getExternalContext().getSession(false);
		if (session.getAttribute("timezone") != null) {
			setTimeZone((TimeZone)session.getAttribute("timezone"));
		}
		 
		TimeZone userTimeZone = getTimeZone();
		TimeZone tomcatTimeZone = TimeZone.getDefault();//Tomcat Timezone
			
		long tomcatTime = new Date().getTime();
		long userTimeInMillis = tomcatTime + (userTimeZone.getOffset(tomcatTime)-tomcatTimeZone.getOffset(tomcatTime));
		
		Calendar cal = Calendar.getInstance();   
		cal.setTimeInMillis(userTimeInMillis);     
		Date userDate = null;
		
		SimpleDateFormat format = new SimpleDateFormat(DEFAULT_PATTERN);
		try {
    		int year = cal.get(java.util.Calendar.YEAR);             // 2002
    		int month = cal.get(java.util.Calendar.MONTH);           // 0=Jan, 1=Feb, ...
    		int day = cal.get(java.util.Calendar.DAY_OF_MONTH);      // 1...
    		userDate = format.parse( (month+1) + "/" + day + "/" + year);
    	}
    	catch(ParseException pe) {
    		userDate = new Date(userTimeInMillis);
    	}

		return userDate;	
	}
}
