/*
 * Author: Jim M. Wilson
 * Created: Sep 16, 2008
 * $Date: 2010-01-05 18:47:40 -0600 (Tue, 05 Jan 2010) $: - $Revision: 4461 $: 
 */

package com.ncsts.view.util;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import org.apache.log4j.Logger;

import com.ncsts.exception.DateFormatException;

/**
 * Stolen from STSWEB
 */
public class DateHelper {
	private static Logger logger = Logger.getLogger(DateHelper.class);

	protected Date contents;

	public DateHelper(Date aDate) {
		this.contents = aDate;
	}

	public static java.sql.Date getSqlDate(String format, String dateString) {
		java.util.Date utilDate = null;
		java.sql.Date sqlDate = null;
		Format formatter;
		formatter = new SimpleDateFormat(format); // like "M/d/yyyy"
		try {
			utilDate = (java.util.Date) formatter.parseObject(dateString);
			sqlDate = new java.sql.Date(utilDate.getTime());
		} catch (Exception e) {
			System.out.println("Date: Failed");
		}
		return sqlDate;
	}

	public static SimpleDateFormat getImportDateFormat(String dateFormat) throws DateFormatException {
		String errorMessage = "The import date format pattern: " + dateFormat + " is not supported.";
		SimpleDateFormat aSimpleDateFormat = null;
		String importDateMap = dateFormat.toLowerCase();

		// day -- "dd" = day number with leading zero if appropriate
		// "d" = day number with no leading zero
		boolean dayFound = false;
		int dayStart = importDateMap.indexOf("dd");
		if (dayStart >= 0) {
			dayFound = true;
		} else {
			dayStart = importDateMap.indexOf("d");
			if (dayStart >= 0) {
				dayFound = true;
			}
		}

		// Day format not supported
		if (!dayFound) {
			logger.error("Day format not supported: " + dateFormat);
			throw new DateFormatException(errorMessage);
		}

		// Month
		// "mmmm" = month name
		// "month" = month name
		// "mmm" = month name abbreviation
		// "mon" = month name abbreviation
		// "mm" = month number with leading zero if appropriate
		// "m" = month number with no leading zero
		boolean monthFound = false;
		int monthStart = importDateMap.indexOf("mmmm");
		if (monthStart >= 0) {
			importDateMap = importDateMap.replaceFirst("mmmm", "MMMM");
			monthFound = true;
		} else {
			monthStart = importDateMap.indexOf("month");
			if (monthStart >= 0) {
				// Replace month to mmmm
				importDateMap = importDateMap.replaceFirst("month", "MMMM");
				monthFound = true;
			} else {
				monthStart = importDateMap.indexOf("mmm");
				if (monthStart >= 0) {
					importDateMap = importDateMap.replaceFirst("mmm", "MMM");
					monthFound = true;
				} else {
					monthStart = importDateMap.indexOf("mon");
					if (monthStart >= 0) {
						// Replace mon to mmm
						importDateMap = importDateMap.replaceFirst("mon", "MMM");
						monthFound = true;
					} else {
						monthStart = importDateMap.indexOf("mm");
						if (monthStart >= 0) {
							importDateMap = importDateMap.replaceFirst("mm", "MM");
							monthFound = true;
						} else {
							monthStart = importDateMap.indexOf("m");
							if (monthStart >= 0) {
								importDateMap = importDateMap.replaceFirst("m", "M");
								monthFound = true;
							}
						}
					}
				}
			}
		}

		// Month format not supported
		if (!monthFound) {
			logger.error("Month format not supported: " + dateFormat);
			throw new DateFormatException(errorMessage);
		}

		// Year
		// "yyyy" = four-digit year
		// "year" = four-digit year
		// "yy" = two-digit year
		boolean yearFound = false;
		int yearStart = importDateMap.indexOf("yyyy");
		if (yearStart >= 0) {
			yearFound = true;
		} else {
			yearStart = importDateMap.indexOf("year");
			if (yearStart >= 0) {
				// Replace year to yyyy
				importDateMap = importDateMap.replaceFirst("year", "yyyy");
				yearFound = true;
			} else {
				yearStart = importDateMap.indexOf("yy");
				if (yearStart >= 0) {
					yearFound = true;
				} else {
				}
			}
		}

		// Year format not supported
		if (!yearFound) {
			logger.error("Year format not supported: " + dateFormat);
			throw new DateFormatException(errorMessage);
		}

		try {
			aSimpleDateFormat = new java.text.SimpleDateFormat(importDateMap);
		} catch (Exception e) {
			throw new DateFormatException(errorMessage);
		}

		return aSimpleDateFormat;
	}
	
	public static long getUserTimeInMillis(String userTimeZoneID, long systemTime) {
		TimeZone userTimeZone = TimeZone.getTimeZone(userTimeZoneID);//User Timezone
		TimeZone tomcatTimeZone = TimeZone.getDefault();//Tomcat Timezone
		
		//Calendar currentTime = Calendar.getInstance();
		long tomcatTime = systemTime;
		long userTimeInMillis = tomcatTime + (userTimeZone.getOffset(tomcatTime)-tomcatTimeZone.getOffset(tomcatTime));
		
		return userTimeInMillis;
	}
	
	public static long getUserTimeInMillis(String userTimeZoneID) {
		TimeZone userTimeZone = TimeZone.getTimeZone(userTimeZoneID);//User Timezone
		TimeZone tomcatTimeZone = TimeZone.getDefault();//Tomcat Timezone
		
		//Calendar currentTime = Calendar.getInstance();
		long tomcatTime = Calendar.getInstance().getTimeInMillis();
		long userTimeInMillis = tomcatTime + (userTimeZone.getOffset(tomcatTime)-tomcatTimeZone.getOffset(tomcatTime));
		
		return userTimeInMillis;
	}
	
	public static long getSystemTimeInMillis(String userTimeZoneID, long userTime) {
		TimeZone userTimeZone = TimeZone.getTimeZone(userTimeZoneID);//User Timezone
		TimeZone tomcatTimeZone = TimeZone.getDefault();//Tomcat Timezone
		
		long tomcatTime = Calendar.getInstance().getTimeInMillis();
		long systemTimeInMillis = userTime - (userTimeZone.getOffset(tomcatTime)-tomcatTimeZone.getOffset(tomcatTime));
		
		return systemTimeInMillis;
	}

}
