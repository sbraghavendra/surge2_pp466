package com.ncsts.view.util;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;

public class PercentageConverter extends javax.faces.convert.NumberConverter {
	public static final String CONVERTER_ID = "percentage";
	private static final String DEFAULT_PATTERN = "#0.00%";
	
	
	public PercentageConverter() {
		setPattern(DEFAULT_PATTERN);
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component,
            Object value) {
		
		if(context != null && component != null && value != null && !(value instanceof String)) {
			String percentageStr = "";
			if(value instanceof Float || value instanceof Double) {
            	try {
            		String valueString = String.valueOf(value);
            		BigDecimal aFloat = new BigDecimal(valueString);
            		DecimalFormat formatter = new DecimalFormat(DEFAULT_PATTERN);
            	    formatter.setMaximumFractionDigits(9);
            		formatter.setMinimumFractionDigits(2);
            		percentageStr = formatter.format(aFloat);
              		percentageStr = formatter.format(percentageStr);
            	

            	}
            	catch(Exception e){}
            }

			return percentageStr;
		}
		
		return super.getAsString(context, component, value);
	}
}