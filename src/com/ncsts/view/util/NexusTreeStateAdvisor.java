package com.ncsts.view.util;

import java.io.IOException;

import org.richfaces.component.UITree;
import org.richfaces.component.state.TreeState;
import org.richfaces.component.state.TreeStateAdvisor;
import org.richfaces.model.TreeNode;
import org.richfaces.model.TreeRowKey;

import com.ncsts.domain.EntityItem;
import com.ncsts.view.bean.JurisdictionItem;
import com.ncsts.view.bean.NexusDefRegBackingBean;
import com.ncsts.view.util.EntityPostbackPhaseListener;

public class NexusTreeStateAdvisor implements TreeStateAdvisor {
	
	private NexusDefRegBackingBean nexusDefRegBackingBean;
	
	public void setNexusDefRegBackingBean(NexusDefRegBackingBean nexusDefRegBackingBean){
		this.nexusDefRegBackingBean = nexusDefRegBackingBean;
	}
	
	public NexusDefRegBackingBean getNexusDefRegBackingBean(){
		return this.nexusDefRegBackingBean;
	}

    public Boolean adviseNodeOpened(UITree tree) {
        if (!JurisdictionPostbackPhaseListener.isPostback()) {
            Object key = tree.getRowKey();
            TreeRowKey treeRowKey = (TreeRowKey) key;
            if (treeRowKey == null || treeRowKey.depth() <= 0) {
                return Boolean.TRUE;
            }    
            
            if(nexusDefRegBackingBean.getSelectedRowKey()!=null){
            	if(nexusDefRegBackingBean.getSelectedRowKey().startsWith(key.toString())){
            		return Boolean.TRUE;
            	}
            } 
        }
        
        return null;
    }

    public Boolean adviseNodeSelected(UITree tree) {
        return null;
    }
}