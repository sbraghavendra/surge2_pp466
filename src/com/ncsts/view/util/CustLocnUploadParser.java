package com.ncsts.view.util;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;

import com.ncsts.common.FileReaderUtil;
import com.ncsts.domain.BCPCustLocn;
import com.ncsts.domain.BatchErrorLog;

public class CustLocnUploadParser extends AbstractFileUploader implements
		Iterator<BCPCustLocn> {

	private Logger logger = Logger.getLogger(CustLocnUploadParser.class);

	private String fileType;
	private Date releaseDate;
	private String releaseVersion;

	private int lineNumber;
	private int linesWritten;
	private String nextLine;

	private List<String> errors;
	private List<BatchErrorLog> batchErrorLogs;
	private BufferedReader bufferedReader;

	private BigDecimal specialRateTotal;

	private static final SimpleDateFormat sdf = new SimpleDateFormat(
			"yyyy/MM/dd");
	private static final SimpleDateFormat sdfRelease = new SimpleDateFormat(
			"yyyy_MM");
	
	private static final String[] COLUMNS_CUSTOMER = { 
		"RECORD_TYPE", "CUST_NBR", "CUST_NAME", "CUST_LOCN_CODE", "LOCATION_NAME", "GEOCODE", "ADDRESS_LINE_1", "ADDRESS_LINE_2", 
		"CITY", "COUNTY", "STATE", "ZIP", "ZIPPLUS4", "COUNTRY", "ACTIVE_FLAG", "UPDATE_CODE"
	};
	
	private static final String[] COLUMNS_ENTITY = { 
		"RECORD_TYPE", "CUST_NBR", "ENTITY_CODE", "UPDATE_CODE"
	};

	public CustLocnUploadParser(String path) {
		super(path);
		deleteOnExit = true;
	}

	public void clear() {
		errors = new ArrayList<String>();
		batchErrorLogs = new ArrayList<BatchErrorLog>();
		this.fileType = null;
		this.releaseDate = null;
		this.releaseVersion = null;
		this.uploadFileName = null;
		this.uploadFilePath = null;
		this.totalBytes = 0;
		this.nextLine = null;
		this.linesInFile = 0;
		this.lineNumber = 0;
//		this.specialRateTotal = ArithmeticUtils.ZERO;
	}

	public String getUploadFileName() {
		return this.uploadFileName;
	}

	public String getFileType() {
		return this.fileType;
	}

	public Date getReleaseDate() {
		return this.releaseDate;
	}

	public String getReleaseVersion() {
		return this.releaseVersion;
	}

	public Double getProgress() {
		if (lineNumber < 1)
			return -0.01;
		try {
			logger.debug("Line: " + linesWritten + ", lines: " + linesInFile
					+ ", " + linesWritten / new Double(linesInFile));
			return linesWritten / new Double(linesInFile);
		} catch (Exception e) {
			return -0.01;
		}
	}

	public List<String> getErrors() {
		return this.errors;
	}

	private void addError(String s) {
		errors.add(s);
	}

	private void addError(int lineNumber, String line, String code) {
		addError(lineNumber, null, null, line, null, code);
	}

	private void addError(int lineNumber, Integer columnNumber, String column,
			String columnName, String line, String code) {
		BatchErrorLog errorLog = new BatchErrorLog();
		errorLog.setErrorDefCode(code);
		errorLog.setColumnNo(columnNumber == null ? null : new Long(
				columnNumber));
		errorLog.setImportColumnValue(column);
		errorLog.setTransDtlColumnName(columnName);
		errorLog.setImportRow(line);
		errorLog.setRowNo(new Long(lineNumber));
		batchErrorLogs.add(errorLog);
	}

	@Override
	public void processHeader(BufferedReader reader) throws IOException {
		String firstLine = reader.readLine();
		String ar[] = firstLine.split(",");
		this.fileType = ar[0].substring(6);
		
		/*
		String s = ar[1].substring(7);
		try {
			this.releaseDate = sdfRelease.parse(s);
		} catch (ParseException e) {
			addError(1, firstLine, "CL21");
			this.releaseDate = null;
		}
		this.releaseVersion = ar[2].substring(6);*/
		
	}

	public void upload(String fileName, InputStream stream) {
		clear();
		try {
			super.upload(fileName, stream);
		} catch (Exception e) {
			addError(1, e.getMessage(), "CL21");
			logger.error(e);
		}
	}

	@Override
	public boolean hasNext() {
		if (bufferedReader == null)
			return false;
		try {
			nextLine = bufferedReader.readLine();
			if (nextLine == null) {
				bufferedReader.close();
				bufferedReader = null;
				return false;
			}
			return nextLine.charAt(0) != '~';
		} catch (IOException e) {
			addError(e.getMessage());
			return false;
		}
	}

	@Override
	public BCPCustLocn next() {
		if ((bufferedReader == null) || (nextLine == null))
			return null;
		if ((lineNumber % 200) == 0) {
			logger.debug("Line: " + lineNumber + ", errorCount:"
					+ batchErrorLogs.size());
		}
		
		//Type of "CSV" or "TAB"
		String delimited = "\\t";
		if(this.fileType.equalsIgnoreCase("CSV")){
			delimited = ",";
		}
		
		String tokens[] = nextLine.split(delimited);
		
		BCPCustLocn r = new BCPCustLocn();
		
		if(tokens.length==0){
			return r;
		}
		
		String recordType = (String)tokens[0];
		if(recordType.equalsIgnoreCase("A")){
			if (tokens.length != COLUMNS_CUSTOMER.length) {
				addError(lineNumber, null, "Expected " + COLUMNS_CUSTOMER.length
						+ ", actual: " + tokens.length, null, nextLine, "CL21"); // ??? not enough columns for SD
			} else {
				int i = 0;
				String parsedColumn = null;
				try {
					r.setLine(new Long(lineNumber));
					r.setText(nextLine);
					
					parsedColumn = tokens[i++];
					if(parsedColumn!=null){
						parsedColumn = parsedColumn.trim();
						if(parsedColumn.length()>10){
							parsedColumn = parsedColumn.substring(0, 10);
						}
					}
					r.setRecordType(parsedColumn);
					
					parsedColumn = tokens[i++];
					if(parsedColumn!=null){
						parsedColumn = parsedColumn.trim();
						if(parsedColumn.length()>100){
							parsedColumn = parsedColumn.substring(0, 100);
						}
					}
					r.setCustNbr(parsedColumn);
					
					parsedColumn = tokens[i++];
					if(parsedColumn!=null){
						parsedColumn = parsedColumn.trim();
						if(parsedColumn.length()>100){
							parsedColumn = parsedColumn.substring(0, 100);
						}
					}
					r.setCustName(parsedColumn);
					
					parsedColumn = tokens[i++];
					if(parsedColumn!=null){
						parsedColumn = parsedColumn.trim();
						if(parsedColumn.length()>100){
							parsedColumn = parsedColumn.substring(0, 100);
						}
					}
					r.setCustLocnCode(parsedColumn);
					
					parsedColumn = tokens[i++];
					if(parsedColumn!=null){
						parsedColumn = parsedColumn.trim();
						if(parsedColumn.length()>100){
							parsedColumn = parsedColumn.substring(0, 100);
						}
					}
					r.setLocationName(parsedColumn);
					
					parsedColumn = tokens[i++];
					if(parsedColumn!=null){
						parsedColumn = parsedColumn.trim();
						if(parsedColumn.length()>12){
							parsedColumn = parsedColumn.substring(0, 12);
						}
					}
					r.setGeocode(parsedColumn);
					
					parsedColumn = tokens[i++];
					if(parsedColumn!=null){
						parsedColumn = parsedColumn.trim();
						if(parsedColumn.length()>100){
							parsedColumn = parsedColumn.substring(0, 100);
						}
					}
					r.setAddressLine1(parsedColumn);
					
					parsedColumn = tokens[i++];
					if(parsedColumn!=null){
						parsedColumn = parsedColumn.trim();
						if(parsedColumn.length()>100){
							parsedColumn = parsedColumn.substring(0, 100);
						}
					}
					r.setAddressLine2(parsedColumn);
					
					parsedColumn = tokens[i++];
					if(parsedColumn!=null){
						parsedColumn = parsedColumn.trim();
						if(parsedColumn.length()>50){
							parsedColumn = parsedColumn.substring(0, 50);
						}
					}
					r.setCity(parsedColumn);
					
					parsedColumn = tokens[i++];
					if(parsedColumn!=null){
						parsedColumn = parsedColumn.trim();
						if(parsedColumn.length()>50){
							parsedColumn = parsedColumn.substring(0, 50);
						}
					}
					r.setCounty(parsedColumn);
					
					parsedColumn = tokens[i++];
					if(parsedColumn!=null){
						parsedColumn = parsedColumn.trim();
						if(parsedColumn.length()>10){
							parsedColumn = parsedColumn.substring(0, 10);
						}
					}
					r.setState(parsedColumn);
					
					//Special handling for zip and zipPlus4
					String zip = "";
					String zipPlus4 = "";
					String zipcode = "";
					
					parsedColumn = tokens[i++];
					if(parsedColumn!=null){
						parsedColumn = parsedColumn.trim();
						//Will check on length later
					}
					zipcode = parsedColumn;
					
					parsedColumn = tokens[i++];
					if(parsedColumn!=null){
						parsedColumn = parsedColumn.trim();			
						if(parsedColumn.length()>4){
							parsedColumn = parsedColumn.substring(0, 4);
						}
					}
					zipPlus4 = parsedColumn;
			
					if(zipcode!=null && zipcode.length()>5 && zipcode.indexOf("-")==5){
						zip = zipcode.substring(0, 5);
						
						if(zipPlus4==null || zipPlus4.length()==0){
							if(zipcode.length()>10){
								zipPlus4 = zipcode.substring(6, 10);
							}
							else{
								zipPlus4 = zipcode.substring(6, zipcode.length());
							}
						}
					}
					else{
						if(zipcode!=null && zipcode.length()>5){
							zip = zipcode.substring(0, 5);
						}
						else{
							zip = zipcode; 
						}
					}
					
					r.setZip(zip);
					r.setZipplus4(zipPlus4);
					
					
					parsedColumn = tokens[i++];
					if(parsedColumn!=null){
						parsedColumn = parsedColumn.trim();
						if(parsedColumn.length()>10){
							parsedColumn = parsedColumn.substring(0, 10);
						}
					}
					r.setCountry(parsedColumn);
					
					parsedColumn = tokens[i++];
					if(parsedColumn!=null){
						parsedColumn = parsedColumn.trim();
						if(parsedColumn.length()>1){
							parsedColumn = parsedColumn.substring(0, 1);
						}
					}
					r.setActiveFlag(parsedColumn);
					
					parsedColumn = tokens[i++];
					if(parsedColumn!=null){
						parsedColumn = parsedColumn.trim();
						if(parsedColumn.length()>10){
							parsedColumn = parsedColumn.substring(0, 10);
						}
					}
					r.setUpdateCode(parsedColumn);
					
				} catch (Exception e) {
					logger.error(e.getMessage(), e);
					addError(lineNumber, i, parsedColumn, COLUMNS_CUSTOMER[i - 1], nextLine,
							"CL23");
				}

				lineNumber++;
			}
		}
		else if(recordType.equalsIgnoreCase("B")){
			if (tokens.length != COLUMNS_ENTITY.length) {
				addError(lineNumber, null, "Expected " + COLUMNS_ENTITY.length
						+ ", actual: " + tokens.length, null, nextLine, "CL21"); // ??? not enough columns for SD
			} else {
				int i = 0;
				String parsedColumn = null;
				try {
					r.setLine(new Long(lineNumber));
					r.setText(nextLine);
					
					parsedColumn = tokens[i++];
					if(parsedColumn!=null){
						parsedColumn = parsedColumn.trim();
						if(parsedColumn.length()>10){
							parsedColumn = parsedColumn.substring(0, 10);
						}
					}
					r.setRecordType(parsedColumn);
					
					parsedColumn = tokens[i++];
					if(parsedColumn!=null){
						parsedColumn = parsedColumn.trim();
						if(parsedColumn.length()>100){
							parsedColumn = parsedColumn.substring(0, 100);
						}
					}
					r.setCustNbr(parsedColumn);
					
					parsedColumn = tokens[i++];
					if(parsedColumn!=null){
						parsedColumn = parsedColumn.trim();
						if(parsedColumn.length()>40){
							parsedColumn = parsedColumn.substring(0, 40);
						}
					}
					r.setEntityCode(parsedColumn);
					
					parsedColumn = tokens[i++];
					if(parsedColumn!=null){
						parsedColumn = parsedColumn.trim();
						if(parsedColumn.length()>10){
							parsedColumn = parsedColumn.substring(0, 10);
						}
					}
					r.setUpdateCode(parsedColumn);
					
				} catch (Exception e) {
					logger.error(e.getMessage(), e);
					addError(lineNumber, i, parsedColumn, COLUMNS_ENTITY[i - 1], nextLine,
							"CL23");
				}

				lineNumber++;
			}	
		}
		else{
			addError(lineNumber, null, "Record Type not found ", null, nextLine, "CL29");
		}

		return r;
	}

	@Override
	public void remove() {
	}

	public Iterator<BCPCustLocn> iterator() {
		lineNumber = 2;
		linesWritten = 0;
		nextLine = null;
		try {
			bufferedReader = new BufferedReader(new FileReader(uploadFilePath));
		} catch (FileNotFoundException e) {
			logger.error(e.getMessage(), e);
			addError(e.getMessage());
		}
		return this;
	}

	public void checksum() {
		if (nextLine == null || nextLine.trim().length() == 0) {
			addError(lineNumber, null, "Bad or Missing End-of-File Marker",
					"Bad or Missing End-of-File Marker", "", "CL16");
			return;
		}

		String footer[] = nextLine.split(" +");
		Long lines = Long.decode(footer[1].replaceAll("\\#", ""));
		if (lines != getRows()) {
			String s = "EOF Marker: " + lines + ", actual count: " + getRows();
			addError(lineNumber, null, s, "Invalid Batch Count", nextLine,
					"CL14");
		}
	}

	public BigDecimal parseBigDecimal(String s) {
		if ((s == null) || (s.trim().length() == 0))
			return null;
		return new BigDecimal(s);
	}
	
	private Long parseLong(String s) {
		if ((s == null) || (s.trim().length() == 0)) return null;
		return Long.parseLong(s);
	}

	public Long getTotalBytes() {
		return this.totalBytes;
	}

	public List<BatchErrorLog> getBatchErrorLogs() {
		return this.batchErrorLogs;
	}

	public BigDecimal getSpecialRateTotal() {
		return specialRateTotal;
	}

	public int getRows() {
		return lineNumber - 2;
	}

	public void setLinesWritten(int linesWritten) {
		this.linesWritten = linesWritten;
	}

}
