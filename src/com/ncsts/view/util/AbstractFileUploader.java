/*
 * Author: Jim M. Wilson
 * Created: Sep 13, 2008
 * $Date: $: - $Revision: 2207 $: 
 */

package com.ncsts.view.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.log4j.Logger;

/**
 * Base class for actual file uploaders. 
 */
public abstract class AbstractFileUploader {
	private static Logger logger = Logger.getLogger(AbstractFileUploader.class);

	protected String uploadPath;
	protected String uploadFileName;
	protected String uploadFilePath;
	protected long totalBytes;
	protected long linesInFile;

	protected boolean deleteOnExit = false;
	
	public AbstractFileUploader() {
		super();
	}
	public AbstractFileUploader(String path) {
		this();
		this.uploadPath = path;
	}
	
	public abstract void processHeader(BufferedReader reader) throws IOException;

	public void upload(String fileName, InputStream stream) throws IOException {
		BufferedWriter bw = null;
		BufferedReader din = null;
		try {
			din = new BufferedReader(new InputStreamReader(stream));
			processHeader(din);
			
			/* Get context root and create file dir(s) if they do not exist */
			new File(uploadPath).mkdirs();

			// some browsers return full path, others just the name... oh well
			logger.debug("Uploaded file name: " + fileName);
			if (fileName.contains("\\")) {
				fileName = fileName.substring(fileName.lastIndexOf('\\') + 1);
			} else if (fileName.contains("/")) {
				fileName = fileName.substring(fileName.lastIndexOf('/') + 1);
			}
			this.uploadFileName = fileName;
			logger.debug("Cleaned up file name: " + fileName);
			this.uploadFilePath = uploadPath + fileName;
			File outputFile = new File(this.uploadFilePath);
			if (deleteOnExit) {
				outputFile.deleteOnExit();
			}
			bw = new BufferedWriter(new FileWriter(outputFile));
			while (din.ready()) {
				bw.write(din.readLine());
				bw.newLine();
				bw.flush();
				linesInFile++;
			}
			totalBytes = outputFile.length();
		} finally {
			try {
				if (bw != null) bw.close();
				if (stream != null) stream.close();
				if (din != null) din.close();
			} catch (IOException e) {
				e.printStackTrace();
				logger.debug("Exception while closing stream");
			}
		}
	}

}
