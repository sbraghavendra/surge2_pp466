package com.ncsts.view.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
public class DesEncrypter {
	
	private static DesEncrypter singleton = null;

	public static final DesEncrypter getInstance() {
		if (singleton == null) {
			singleton = new DesEncrypter();
		}
		return singleton;
	}
	
	private DesEncrypter() {
	}

	// SD/PinPoint!2010
	private static byte[] keyBytes = new byte[] { 0x53, 0x44, 0x2f, 0x50, 0x69, 0x6e, 0x50, 0x6f, 
		0x69, 0x6e, 0x74, 0x21, 0x32, 0x30, 0x31, 0x30 };
	
	// 062314:51:56.234
	private static byte[] ivBytes = new byte[] { 0x30, 0x36, 0x32, 0x33, 0x31, 0x34, 0x3a, 0x35, 
		0x31, 0x3a, 0x35, 0x36, 0x2e, 0x32, 0x33, 0x34 };
	
	private static int shalen = 32;
	private SecretKeySpec secretKeySpec = new SecretKeySpec(keyBytes, "AES");
	private IvParameterSpec ivParameterSpec = new IvParameterSpec(ivBytes);
	
	public String encrypt(String plaintext) {
		try {	
			byte[] text = plaintext.getBytes();
			ByteArrayOutputStream stream = new ByteArrayOutputStream();

			// Encrypt text
			Cipher cipher = Cipher.getInstance("AES/CFB/NoPadding");
			cipher.init(Cipher.ENCRYPT_MODE, this.secretKeySpec,
			this.ivParameterSpec);
			stream.write(cipher.doFinal(text));
			
			// Hash text
			MessageDigest digest = MessageDigest.getInstance("SHA-256");
			digest.update(text);
			stream.write(digest.digest());
			byte[] bytes = stream.toByteArray();
			stream.close();
			return new sun.misc.BASE64Encoder().encode(bytes);
		
		} catch (IOException e) {
        } catch (NoSuchAlgorithmException e) {
        } catch (NoSuchPaddingException e) {
        } catch (InvalidKeyException e) {
        } catch (InvalidAlgorithmParameterException e) {	
        } catch (IllegalBlockSizeException e) {	
        } catch (BadPaddingException e) {
        }
        
        return null;
	}
	
	public String decrypt(String encryptedtext) {
		try {
			byte[] bytes = new sun.misc.BASE64Decoder().decodeBuffer(encryptedtext);
			ByteBuffer buf = ByteBuffer.wrap(bytes);

			int aeslen = bytes.length - shalen;
			byte[] aes = new byte[aeslen];
			buf.get(aes);
			
			// Decrypt text
			Cipher cipher = Cipher.getInstance("AES/CFB/NoPadding");
			cipher.init(Cipher.DECRYPT_MODE, this.secretKeySpec,
			this.ivParameterSpec);
			byte[] text = cipher.doFinal(aes);
			
			// Compute hash
			MessageDigest digest = MessageDigest.getInstance("SHA-256");
			digest.update(text);
			byte[] hash = digest.digest();
			byte[] hash2 = new byte[shalen];
			buf.get(hash2);
			if (!Arrays.equals(hash, hash2))
			throw new InvalidHashException(
			"Verification failed. Decryption aborted.");
			
			return new String(text);
			
		} catch (IOException e) {
        } catch (NoSuchAlgorithmException e) {
        } catch (NoSuchPaddingException e) {
        } catch (InvalidKeyException e) {
        } catch (InvalidAlgorithmParameterException e) {	
        } catch (IllegalBlockSizeException e) {	
        } catch (BadPaddingException e) {
        } catch (InvalidHashException e) {
        }
        
        return null;
	}
	
	class InvalidHeaderException extends Exception {
		private static final long serialVersionUID = 1L;
		public InvalidHeaderException(String string) {
			super(string);
		}
	}
	
	class InvalidHashException extends Exception {
		private static final long serialVersionUID = 1L;
		public InvalidHashException(String string) {
			super(string);
		}
	}
	
	public static void main(String[] args) throws Exception {
		DesEncrypter c = new DesEncrypter();
		String password = "Admin01!";
		
		// Encrypt
		String encrypted = c.encrypt(password);
		System.out.println("xxxxxx encrypted: " + encrypted);
		
        //Decrypt
	    String decrypted = c.decrypt(encrypted);    
	    System.out.println("xxxxxx decrypted: " + decrypted);
	}
}