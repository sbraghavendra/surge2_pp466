package com.ncsts.view.util;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

import com.ncsts.domain.User;
import com.ncsts.domain.DbProperties;

public class DbPropertiesConverter implements Converter {

	public Object getAsObject(FacesContext context, UIComponent component,
			String value) {

		return new DbProperties(value);
	}

	public String getAsString(FacesContext context, UIComponent component,
			Object value) {

		DbProperties dbProperties = (DbProperties) value;
		return dbProperties.getDatabaseName();
	}

}
