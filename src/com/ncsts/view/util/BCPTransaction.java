package com.ncsts.view.util;

import java.util.Observable;

import com.ncsts.domain.BCPBillTransaction;
import com.ncsts.domain.BCPPurchaseTransaction;

public class BCPTransaction {
	
	private BCPPurchaseTransaction bcpPurchaseTransaction = null;
	private BCPBillTransaction bcpSaleTransaction = null;

	public BCPTransaction() {
	}
	
	public void setBcpPurchaseTransaction(BCPPurchaseTransaction bcpPurchaseTransaction) {
		this.bcpPurchaseTransaction = bcpPurchaseTransaction;
	}

	public BCPPurchaseTransaction getBcpPurchaseTransaction() {
		return bcpPurchaseTransaction;
	}

	public void setBcpSaleTransaction(BCPBillTransaction bcpSaleTransaction) {
		this.bcpSaleTransaction = bcpSaleTransaction;
	}

	public BCPBillTransaction getBcpSaleTransaction() {
		return bcpSaleTransaction;
	}
}
