/*
 * Author: Jim M. Wilson
 * Created: Sep 18, 2008. Refactored from BatchMaintenanceBackingBean
 * $Date: 2010-01-06 20:55:41 -0600 (Wed, 06 Jan 2010) $: - $Revision: 4466 $: 
 */
package com.ncsts.view.util;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import org.apache.log4j.Logger;

import com.ncsts.common.CacheManager;
import com.ncsts.dao.BatchMaintenanceDAO;
import com.ncsts.domain.BatchMaintenance;
import com.ncsts.dto.BatchPreferenceDTO;

public class BatchStatistics {

	private Logger logger = Logger.getLogger(BatchStatistics.class);

	private Long processedRows;

	private Date estimatedEndDatetime;
	private Long processingTime; 

	private Date now;

	private CacheManager cacheManager;

	private BatchMaintenance batchMaintenance;

	private BatchMaintenanceDAO batchMaintenanceDAO;

	private final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("M/d/yyyy HH:mm:ss a");
	private final SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
	private NumberFormat numberFormat;
	private NumberFormat integerFormat;

	private BatchPreferenceDTO batchPreferenceDTO;
	
	private Date lastPoll;
	
	public BatchStatistics(BatchMaintenance batch, BatchMaintenanceDAO dao, BatchPreferenceDTO preference) {
		this.batchMaintenance = batch;
		this.batchMaintenanceDAO = dao;
		integerFormat = NumberFormat.getIntegerInstance();
		integerFormat.setMinimumIntegerDigits(2);
		numberFormat = NumberFormat.getNumberInstance();
		numberFormat.setMinimumFractionDigits(4);
		numberFormat.setMaximumFractionDigits(4);
		timeFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
		batchPreferenceDTO = preference;
	}

	public long getActualProcessedRows() {
		// batch status code like XA, XAL, XE, XI, XITR and few others
		if (batchMaintenance.isInProgress()) {
			Long last_num = batchMaintenanceDAO.getBatchSequence(batchMaintenance.getBatchTypeCode()).longValue();
			if (batchMaintenance.getStartRow() != null) { 
				long totalRows = batchMaintenance.getTotalRows();
				long estimateProcessedRow = last_num - batchMaintenance.getStartRow();

				if ((estimateProcessedRow - totalRows) > 0)  {
					if( batchMaintenance.getProcessedRows() != null)
						return batchMaintenance.getProcessedRows();
				}
				else{
					return estimateProcessedRow;
				}
			}
		} else {
			if( batchMaintenance.getProcessedRows() != null)
				return batchMaintenance.getProcessedRows();
		}
		return 0;
	}
	
	private void clear() {
		now = new Date();
		processedRows = getActualProcessedRows();
		estimatedEndDatetime = null;
		processingTime = null;
	}

	public void calcStatistics() {
		if ((lastPoll == null) || (lastPoll.before(new Date()))) {
			clear();
			batchMaintenance = batchMaintenanceDAO.getBatchMaintananceData(batchMaintenance.getBatchId());

			if (batchMaintenance.getActualEndTimestamp() != null && batchMaintenance
					.getActualEndTimestamp().getTime() != 0) {
				if (batchMaintenance.getActualStartTimestamp() != null && batchMaintenance
						.getActualStartTimestamp().getTime() != 0) {
					processingTime = batchMaintenance.getActualEndTimestamp().getTime() - batchMaintenance
							.getActualStartTimestamp().getTime();
					estimatedEndDatetime = batchMaintenance.getActualEndTimestamp();
				}
			} else {
				if (batchMaintenance.getActualStartTimestamp() != null && batchMaintenance
						.getActualStartTimestamp().getTime() != 0) {
					processingTime = now.getTime() - batchMaintenance.getActualStartTimestamp().getTime();

					if (getProgress() != null) {
						long remaining = (long) (processingTime / (getProgress()));
						estimatedEndDatetime = new Date(now.getTime() + remaining);
					}
				}
			}
			// never poll more than once per sec
			lastPoll = new Date(new Date().getTime() + 1000);
		}
	}
	
	public Double getProgress() {
		if (batchMaintenance.getTotalRows() != null) {
			double totalRows = batchMaintenance.getTotalRows();
			if ((totalRows > 0.0) && (processedRows > 0) )  
				return (processedRows / totalRows);
		}
		return null;
	}

	public Double getRowsPerMinute() {
		if ((processingTime != null) && (processedRows > 0) && (processingTime > 0.0)) {
			return processedRows / (processingTime / 60000.0);
		} else {
			if (batchMaintenance.isInProgress()) 
				return 0.0;
			else
				return null;
		}
	}
	public String getProcessingTime() {
		if (processingTime != null)
			return toHHSS(processingTime);
		else
			if (batchMaintenance.isInProgress()) 
				return "0.0";
			else
				return "";
	}
	
	public Date getNow() {
		return now;
	}
	
	public String getPercentComplete() {
		if (batchMaintenance.isInProgress()) calcStatistics();
		if (getProgress() != null)
			return numberFormat.format(getProgress()*100);
		else
			return "-1";
	}

	public String getEstimatedTimeToEnd() {
		Double aProgress = getProgress();
		if(aProgress==null){
			return "";
		}

		if (batchMaintenance.isInProgress() 
				&& (processingTime != null) 
				&& (aProgress > 0.0)) {
			return toHHSS((long) (processingTime / (aProgress)));
		}
		return "";
	}
	
	private String toHHSS(long time) {
		long hours = time/ (1000 * 60 * 60);
		int minutes = new Long((time/(1000*60)) - (hours * 60)).intValue();
		long seconds = (time/1000) - (hours * 60 * 60) - (minutes*60);
		return integerFormat.format(hours) + ":" +
		integerFormat.format(minutes) + ":" + integerFormat.format(seconds);
	}

	public Date getEstimatedEndDatetime() {
		return this.estimatedEndDatetime;
	}

	public BatchMaintenance getBatchMaintenance() {
		return this.batchMaintenance;
	}

	public BatchPreferenceDTO getBatchPreferenceDTO() {
		return this.batchPreferenceDTO;
	}

	public Long getProcessedRows() {
		return this.processedRows;
	}

	public Double getPercent() {
		if (batchMaintenance.isInProgress()) {
			if (getProgress() != null) {
				return getProgress();
			} else {
				return 0.0;
			}
		} else if (batchMaintenance.getActualEndTimestamp() != null) {
			return 1.0;
		} 
		return null;
	}
	
	public Boolean getInProgress() {
		return batchMaintenance.isInProgress() && batchPreferenceDTO.getHasRefreshInterval();
	}
}
