/*
 * Author: Jim M. Wilson
 * Created: Sep 3, 2008
 * $Date: 2009-12-24 00:27:33 -0600 (Thu, 24 Dec 2009) $: - $Revision: 4453 $: 
 */

package com.ncsts.view.util;

import java.beans.PropertyDescriptor;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.charset.Charset;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

import com.ncsts.common.CacheManager;
import com.ncsts.common.LoggerFactory;
import com.ncsts.common.helper.FieldSortOrder;
import com.ncsts.domain.BCPBillTransaction;
import com.ncsts.domain.BCPPurchaseTransaction;
import com.ncsts.domain.BatchErrorLog;
import com.ncsts.domain.DataDefinitionColumn;
import com.ncsts.domain.DriverNames;
import com.ncsts.domain.ImportDefinition;
import com.ncsts.domain.ImportDefinitionDetail;
import com.ncsts.domain.ImportSpec;
import com.ncsts.domain.ListCodes;
import com.ncsts.dto.ImportDefDTO;
import com.ncsts.dto.ImportMapProcessPreferenceDTO;
import com.ncsts.dto.TaxCodeStateDTO;
import com.ncsts.exception.DateFormatException;

/**
 * Stolen from STSWeb FileImportTask.java
 */
public class ImportMapUploadParser implements Iterator<BCPTransaction> {
	private Logger logger = LoggerFactory.getInstance().getLogger(
			ImportMapUploadParser.class);

	private ImportMapProcessPreferenceDTO preferences;
	private BufferedReader bufferedReader;
	protected int lineNumber;

	private String nextLine;
	private int linesWritten;
	protected long fileSize;
	private long estimatedRows;
	private long bytesRead;
	private long startTime;
	private BigDecimal computedTotalAmount = new BigDecimal(0.0);
	private List<BatchErrorLog> batchErrorLogs;
	private List<String> errors;
	private int maxErrorSeen;
	private boolean hitMaxErrors;
	private int errorCount;
	private int errorCountTotal;
	protected boolean abort;
	private boolean completed;

	protected ImportDefinition importDefinition;
	protected ImportSpec importSpec;
	private String uploadPath;
	private Map<String, Integer> headers = null;
	private List<String> headerNames = null;
	private Map<String, ArrayList<ImportDefinitionDetail>> importMap;
	private Map<Integer, Integer> nullHeaders = null;

	private Map<String, PropertyDescriptor> columnProperties;
	private Map<String, PropertyDescriptor> columnPropertiesSale;
	private CacheManager cacheManager;
	protected SimpleDateFormat formatter;

	private Map<String, DriverNames> transactionColumnDriverNamesMap;
	private Map<String, DataDefinitionColumn> bCPTransactionsColumnMap;
	private Map<String, DataDefinitionColumn> bCPTransactionsColumnMapSale;
	private Map<String, ListCodes> errorListCodes;
	private Map<String, ListCodes> errorSeverityCodes;

	public void setTransactionColumnDriverNamesMap(
			Map<String, DriverNames> transactionColumnDriverNamesMap) {
		this.transactionColumnDriverNamesMap = transactionColumnDriverNamesMap;
	}

	public void setBCPTransactionsColumnMap(
			Map<String, DataDefinitionColumn> bCPTransactionsColumnMap) {
		this.bCPTransactionsColumnMap = bCPTransactionsColumnMap;
	}

	public void setBCPTransactionsColumnMapSale(
			Map<String, DataDefinitionColumn> bCPTransactionsColumnMapSale) {
		this.bCPTransactionsColumnMapSale = bCPTransactionsColumnMapSale;
	}

	public void setErrorSeverityCodes(Map<String, ListCodes> errorSeverityCodes) {
		this.errorSeverityCodes = errorSeverityCodes;
	}

	public Map<String, ListCodes> getErrorSeverityCodes() {
		return this.errorSeverityCodes;
	}

	public void setErrorListCodes(Map<String, ListCodes> errorListCodes) {
		this.errorListCodes = errorListCodes;
	}

	public Map<String, ListCodes> getErrorListCodes() {
		return this.errorListCodes;
	}

	public ImportMapUploadParser(ImportDefDTO importDefinition) {
		this.importDefinition = new ImportDefinition(importDefinition);
	}

	public ImportMapUploadParser(ImportDefinition importDefinition) {
		this.importDefinition = importDefinition;
	}

	public ImportMapUploadParser(ImportDefinition importDefinition,
			ImportMapProcessPreferenceDTO preferences)
			throws DateFormatException {
		this(importDefinition);
		setPreferences(preferences);
	}

	public String getImportDateFormat() {
		return importSpec.getImportdatemap();
	}

	public void setImportSpec(ImportSpec importSpec) {
		this.importSpec = importSpec;

		if (importSpec != null) {
			String format = importSpec.getImportdatemap();

			if ((format != null && (format.trim().length() > 0))) {
				try {
					formatter = DateHelper.getImportDateFormat(format);
					return;
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			// Use default data format
			format = "MM/dd/yyyy";
			try {
				formatter = DateHelper.getImportDateFormat(format);
				return;
			} catch (Exception e) {
			}
		}

	}

	public String getFilePath() {
		if ((uploadPath == null)
				|| importDefinition.getSampleFileName()
						.contains(File.separator)) {
			return importDefinition.getSampleFileName();
		} else {
			return this.uploadPath + File.separator
					+ importDefinition.getSampleFileName();
		}
	}

	public List<String> getHeaderNames() {
		return headerNames;
	}

	public void setHeaderNames(List<String> headerNames) {
		this.headerNames = headerNames;
	}

	public void parseFile() throws FileNotFoundException, IOException {
		headerNames = new ArrayList<String>();
		//0005391
		String encoding = importDefinition.getEncoding();
		if(encoding==null || encoding.length()==0){
			encoding = "Windows-1252";
		}
		BufferedReader input = new BufferedReader(new InputStreamReader(new FileInputStream(getFilePath()), encoding));
		this.fileSize = new File(getFilePath()).length();
		String line = input.readLine();
		line = line.replace("\uFEFF", "");
		parseHeader(line);
		String firstLine = input.readLine();
		input.close();
	}

	public void parseHeader() throws FileNotFoundException, IOException {
		if (headerNames == null) {
			headerNames = new ArrayList<String>();

			//0005391
			String encoding = importDefinition.getEncoding();
			if(encoding==null || encoding.length()==0){
				encoding = "Windows-1252";
			}
			
			BufferedReader input = new BufferedReader(new InputStreamReader(new FileInputStream(getFilePath()), encoding));
			String line = input.readLine();	
			line = line.replace("\uFEFF", "");//BOM character at beginning of the file	
			input.close();
			parseHeader(line);
		}
	}

	public void parseHeader(String line) throws FileNotFoundException,
			IOException {
		bytesRead = line.length() + getLineEnd();
		if (importDefinition.hasHeaders()) {
			headerNames = split(line);
		} else {
			headerNames = makeHeaderColumns(split(line).size());
		}

		// Make sure all columns not null
		nullHeaders = new HashMap<Integer, Integer>();
		List<String> headerNamesTemp = new ArrayList<String>();
		for (int i = 0; i < headerNames.size(); i++) {
			if (headerNames.get(i) == null) {
				nullHeaders.put(new Integer(i), new Integer(i));
			} else {
				if(headerNames.get(i).trim().length()>0){ //PP-82
					headerNamesTemp.add(headerNames.get(i).trim());
				}
				else{
					nullHeaders.put(new Integer(i), new Integer(i));
				}
			}
		}
		headerNames = headerNamesTemp;

		headers = new HashMap<String, Integer>();
		for (int i = 0; i < headerNames.size(); i++) {
			headers.put(headerNames.get(i), i);
		}
		this.estimatedRows = this.fileSize / line.length();
	}

	public boolean hasNext() {
		if (abort || (bufferedReader == null))
			return false;
		try {
			nextLine = bufferedReader.readLine();
			if (nextLine == null) {
				bufferedReader.close();
				bufferedReader = null;
				return false;
			}
			bytesRead += nextLine.length() + getLineEnd();

			String endOfFileMarker = "";
			if (importSpec.getBatcheofmarker() != null
					&& importSpec.getBatcheofmarker().length() > 0) {
				endOfFileMarker = importSpec.getBatcheofmarker();
			}

			if (endOfFileMarker.length() == 0) {
				return true;
			}

			return !nextLine.startsWith(endOfFileMarker);
		} catch (IOException e) {
			errors.add(e.getMessage());
			return false;
		}
	}

	public void remove() {
		// not implemented
	}

	private void openReader() throws IOException {
		//0005391
		String encoding = importDefinition.getEncoding();
		if(encoding==null || encoding.length()==0){
			encoding = "Windows-1252";
		}
		
		bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(getFilePath()), encoding));

		this.fileSize = new File(getFilePath()).length();
	}

	public void closeReader() {
		if (bufferedReader != null) {
			try {
				bufferedReader.close();
			} catch (Exception e) {
			}
			bufferedReader = null;
		}
	}

	private void initReader() throws IOException {
		openReader();
		bufferedReader.mark(5000);
		String line = bufferedReader.readLine();
		line = line.replace("\uFEFF", "");
		parseHeader(line);
		if (importDefinition.hasHeaders()) {
			lineNumber = 2;
		} else {
			bufferedReader.reset();
			lineNumber = 1;
		}
	}

	public Double getProgress() {
		if (lineNumber < 1)
			return -0.01;
		if (abort)
			return 101.0;
		try {
			logger.debug("Line: " + linesWritten + ", lines: " + estimatedRows
					+ ", " + bytesRead / new Double(fileSize));
			return bytesRead / new Double(fileSize);
		} catch (Exception e) {
			return -0.01;
		}
	}

	public Iterator<BCPTransaction> iterator() {
		initVariables();

		try {
			initReader();
		} catch (IOException e) {
			logger.error(e.getMessage(), e);
			errors.add(e.getMessage());
		}
		return this;
	}

	// JJ: need to call this from AID to log pre-import script errors
	public void initVariables() {
		computedTotalAmount = new BigDecimal(0.0);
		linesWritten = 0;
		abort = false;
		maxErrorSeen = 0;
		hitMaxErrors = false;
		errorCount = 0;
		errorCountTotal = 0;
		nextLine = null;
		bytesRead = 0;
		startTime = System.currentTimeMillis();
		batchErrorLogs = new ArrayList<BatchErrorLog>();
		errors = new ArrayList<String>();
	}

	public BCPTransaction next() {
		if ((bufferedReader == null) || (nextLine == null))
			return null;
		return nextRecord(nextLine);
	}

	public BCPTransaction nextRecord(String line) {
		BCPTransaction bcpTransaction = new BCPTransaction();
		BCPPurchaseTransaction td = new BCPPurchaseTransaction();
		
		BCPBillTransaction tdSale = new BCPBillTransaction();
		//tdSale.setBilltransId(new Long(lineNumber));

		List<String> values = split(line);

		// Remove any value with corresponding null column
		if (nullHeaders != null && nullHeaders.size() > 0) {
			List<String> valuesTemp = new ArrayList<String>();
			for (int i = 0; i < values.size(); i++) {
				if (!nullHeaders.containsKey(new Integer(i))) {
					valuesTemp.add(values.get(i));
				}
			}
			values = valuesTemp;
		}

		if (headerNames.size() != values.size()) {
			addError(new BatchErrorLog("IP1", lineNumber, line,
					"Batch Column Count", values.size() + ""));
		}

		boolean writeOnError = true;
		for (int i = 0; i < values.size(); i++) {
			if (i >= headerNames.size()) {
				// already caught error at batch column count above
				continue;
			}

			// 4850 For mapping a header to multiple transaction columns.
			ArrayList<ImportDefinitionDetail> columnArray = this.importMap
					.get(headerNames.get(i).toUpperCase());
			if (columnArray == null) {
				continue;
			}

			List<ImportDefinitionDetail> columnList = columnArray;
			for (ImportDefinitionDetail detail : columnList) {

				// ImportDefinitionDetail detail =
				// importMap.get(headerNames.get(i));
				if (detail == null) {
					continue;
				}
				String mappedCol = detail.getTransDtlColumnName();
				if (detail.getTableName().equalsIgnoreCase(
						"TB_PURCHTRANS")) {
					if (bcpTransaction.getBcpPurchaseTransaction() == null) {
						bcpTransaction.setBcpPurchaseTransaction(td);
					}

					// truncate data to max length of column
					DataDefinitionColumn ddc = bCPTransactionsColumnMap
							.get(mappedCol);

					// 0005203
					if (ddc == null) {
						addError(new BatchErrorLog("IPXX", lineNumber,
								"Column mapping not found", mappedCol, null));
						writeOnError = false;
						break;
					}

					int maxLen = ddc.getDatalength().intValue();
					String val = "";
					if (values.get(i) != null) {
						val = values.get(i).trim();
					}
					if (ddc.getDataType().startsWith("VARCHAR")
							&& (val.length() > maxLen)) {
						val = val.substring(0, maxLen);
					}
					// "USER_NUMBER_XX" in BcpSaleTransaction
					if (mappedCol != null && mappedCol.length() > 0) {
						if (mappedCol.indexOf("USER_NBR_") >= 0) {
							mappedCol = mappedCol.replaceAll("USER_NBR_",
									"USER_NUMBER_");
						}
					}
					PropertyDescriptor propertyDescriptor = columnProperties
							.get(mappedCol);
					if (propertyDescriptor == null) {
						addError(new BatchErrorLog("IPXX", lineNumber,
								"Column Java Domain Map not found", mappedCol,
								null));
						writeOnError = false;
						break;
					}

					// major magic here...
					// convert the string to its class
					// then find the setter method from the column name and just
					// call it.
					// piece of cake.
					try {
						Object o = convert(val.trim(),
								propertyDescriptor.getPropertyType());

						if (o == null) {
							// Don't do anything
							continue;
						}

						// do the import mapping
						if (o instanceof String) {
							o = substitute((String) o, mappedCol);

							// Convert to upper-case for all drivers
							if (transactionColumnDriverNamesMap
									.containsKey(mappedCol)) {
								o = ((String) o).toUpperCase();
							}
						}

						columnProperties.get(mappedCol).getWriteMethod()
								.invoke(td, o);
					} catch (Exception e) {
						logger.error(e);
						BatchErrorLog bel = new BatchErrorLog();
						bel.setColumnNo(new Long(i + 1));
						bel.setErrorDefCode("IP2");
						bel.setRowNo(new Long(lineNumber));
						bel.setImportHeaderColumn(headerNames.get(i)
								.toUpperCase());
						bel.setImportColumnValue(val);
						bel.setTransDtlColumnName(mappedCol);
						bel.setImportRow(line);
						bel.setTransDtlDatatype(ddc.getDataType());
						if (!addError(bel))
							writeOnError = false;
					}
				} else {
					if (detail.getTableName().equalsIgnoreCase("TB_BILLTRANS")) {
						if (bcpTransaction.getBcpSaleTransaction() == null) {
							bcpTransaction.setBcpSaleTransaction(tdSale);
						}

						// truncate data to max length of column
						DataDefinitionColumn ddc = bCPTransactionsColumnMapSale
								.get(mappedCol);

						// 0005203
						if (ddc == null) {
							addError(new BatchErrorLog("IPXX", lineNumber,
									"Column mapping not found", mappedCol, null));
							writeOnError = false;
							break;
						}

						int maxLen = ddc.getDatalength().intValue();
						String val = "";
						if (values.get(i) != null) {
							val = values.get(i).trim();
						}
						if (ddc.getDataType().startsWith("VARCHAR")
								&& (val.length() > maxLen)) {
							val = val.substring(0, maxLen);
						}

						// "USER_NUMBER_XX" in BcpSaleTransaction
						if (mappedCol != null && mappedCol.length() > 0) {
							if (mappedCol.indexOf("USER_NBR_") >= 0) {
								mappedCol = mappedCol.replaceAll("USER_NBR_",
										"USER_NUMBER_");
							}
						}

						PropertyDescriptor propertyDescriptor = columnPropertiesSale
								.get(mappedCol);
						if (propertyDescriptor == null) {
							addError(new BatchErrorLog("IPXX", lineNumber,
									"Column Java Domain Map not found",
									mappedCol, null));
							writeOnError = false;
							break;
						}

						// major magic here...
						// convert the string to its class
						// then find the setter method from the column name and
						// just call it.
						// piece of cake.
						try {
							Object o = convert(val.trim(),
									propertyDescriptor.getPropertyType());

							if (o == null) {
								// Don't do anything
								continue;
							}

							// do the import mapping
							if (o instanceof String) {
								o = substitute((String) o, mappedCol);

								// Convert to upper-case for all drivers
								if (transactionColumnDriverNamesMap
										.containsKey(mappedCol)) {// Need to
																	// change
																	// drivers
																	// ????????
									o = ((String) o).toUpperCase();
								}
							}

							columnPropertiesSale.get(mappedCol)
									.getWriteMethod().invoke(tdSale, o);
						} catch (Exception e) {
							logger.error(e);
							BatchErrorLog bel = new BatchErrorLog();
							bel.setColumnNo(new Long(i + 1));
							bel.setErrorDefCode("IP2");
							bel.setRowNo(new Long(lineNumber));
							bel.setImportHeaderColumn(headerNames.get(i)
									.toUpperCase());
							bel.setImportColumnValue(val);
							bel.setTransDtlColumnName(mappedCol);
							bel.setImportRow(line);
							bel.setTransDtlDatatype(ddc.getDataType());
							if (!addError(bel))
								writeOnError = false;
						}
					}
				}
			}
		}
		td.setLoadTimestamp(new Date());
		tdSale.setGlExtractTimestamp(new Date());

		update(td, tdSale);
		lineNumber++;
		logger.debug("On line: " + lineNumber + ", errors:" + errorCount);
		return writeOnError ? bcpTransaction : null;
	}

	private Object convert(String s, Class<?> clazz) throws ParseException {
		if ((s == null) || (s.length() == 0)) {
			return null;
		}

		// 0001417: Import with comma
		String preValue = s;
		if (preValue.contains(",")) {
			if ((clazz == Long.class) || (clazz == Double.class)
					|| (clazz == Integer.class) || (clazz == BigDecimal.class)) {
				preValue = preValue.replaceAll(",", "");
			}
		}

		if (clazz == Long.class) {
			return Long.valueOf(preValue);
		} else if (clazz == Double.class) {
			return Double.valueOf(preValue);
		} else if (clazz == BigDecimal.class) {
			return new BigDecimal(preValue);
		} else if (clazz == Integer.class) {
			return Integer.valueOf(preValue);
		} else if (clazz == java.util.Date.class) {
			if ((preValue != null) && (preValue.length() > 0)) {
				return formatter.parse(preValue);
			} else {
				return null;
			}
		}
		return new java.lang.String(preValue);
	}

	public List<List<String>> parseLines(int max) throws IOException {
		List<List<String>> list = new ArrayList<List<String>>();
		try {
			initReader();
			String line;
			int i = 0;
			while ((line = bufferedReader.readLine()) != null) {
				// if (isEOF(line)) break;
				if (i++ < max)
					list.add(split(line));
			}
		} finally {
			bufferedReader.close();
		}
		return list;
	}

	public List<String> readLines(int max) throws IOException {
		List<String> list = new ArrayList<String>();
		String line;
		int i = 0;
		try {
			openReader();
			while (((line = bufferedReader.readLine()) != null)) {
				// if (isEOF(line)) break;
				if (i++ < max)
					list.add(line);
			}
		} finally {
			bufferedReader.close();
		}
		// add in the footer
		if (i >= max)
			list.add(".....");
		if (line != null)
			list.add(line);
		return list;
	}

	// Check & populate INVOICE_TAX_FLG
	public void update(BCPPurchaseTransaction transactionDetail,
			BCPBillTransaction transactionDetailSale) {
		if (importSpec.getImportSpecType().equalsIgnoreCase("IP")
				|| importSpec.getImportSpecType().equalsIgnoreCase("PCO")) {
			if (transactionDetail.getGlLineItmDistAmt() != null) {
				computedTotalAmount = computedTotalAmount.add(transactionDetail.getGlLineItmDistAmt());
			}
		} else {
			if (transactionDetailSale.getGlLineItmDistAmt() != null) {
				computedTotalAmount = computedTotalAmount.add(new BigDecimal(transactionDetailSale.getGlLineItmDistAmt().toString()));
			}
		}

		if (transactionDetail.getInvoiceTaxFlg() == null) {
			if ((transactionDetail.getInvoiceTaxAmt() == null)
					|| (transactionDetail.getInvoiceTaxAmt().compareTo(
							BigDecimal.ZERO) == 0)) {
				transactionDetail.setInvoiceTaxFlg("N");
			} else {
				transactionDetail.setInvoiceTaxFlg("Y");
			}
		}
	}

	/**
	 * This method makes the artificial Column name if the import data file does
	 * not come with the expected headers. Beware, there is a check box (flag)
	 * in the import definition page to let the user confirm if headers come
	 * with the file.
	 * 
	 * @param arraySize
	 *            int
	 * @return String[]
	 */
	private List<String> makeHeaderColumns(int arraySize) {
		List<String> headers = new ArrayList<String>();
		NumberFormat nf = NumberFormat.getIntegerInstance();
		for (int i = 0; i < arraySize; i++) {
			headers.add("COLUMN_" + nf.format(i));
		}
		return headers;
	}

	/**
	 * Don't validate EOF if we are aborting
	 */
	public void checksum() {
		if (!abort)
			processLastLine(nextLine);
	}

	private void processLastLine(String line) throws NumberFormatException,
			NoSuchElementException {

		String endOfFileMarker = "";
		String batchCountMarker = "";
		String batchTotalMarker = "";
		String batchTotalTolerance = "";
		if (importSpec.getBatcheofmarker() != null
				&& importSpec.getBatcheofmarker().length() > 0) {
			endOfFileMarker = importSpec.getBatcheofmarker();
		}
		if (importSpec.getBatchcountmarker() != null
				&& importSpec.getBatchcountmarker().length() > 0) {
			batchCountMarker = importSpec.getBatchcountmarker();
		}
		if (importSpec.getBatchtotalmarker() != null
				&& importSpec.getBatchtotalmarker().length() > 0) {
			batchTotalMarker = importSpec.getBatchtotalmarker();
		}
		if (preferences.getBatchTotalTolerance() != null
				&& preferences.getBatchTotalTolerance().length() > 0) {
			batchTotalTolerance = preferences.getBatchTotalTolerance();
		}

		String lastLine = (line != null) ? line.trim() : "";
		boolean sawEOF = false;

		// If no end of file marker
		if (endOfFileMarker.length() == 0) {
			// If an end-of-file marker is not defined in Preferences, then it
			// should not be an "Bad or Missing..." error to
			// have one in the file.
			return;
		}

		// Search endOfFileMarker
		if (endOfFileMarker.length() > 0
				&& lastLine.indexOf(endOfFileMarker) != -1) {
			sawEOF = true;
			if (lastLine.indexOf(endOfFileMarker) + endOfFileMarker.length() < lastLine
					.length()) {
				lastLine = lastLine.substring(
						lastLine.indexOf(endOfFileMarker)
								+ endOfFileMarker.length()).trim();
			}
		}

		// Search batchCountMarker
		if (batchCountMarker.length() > 0
				&& lastLine.indexOf(batchCountMarker) != -1) {
			String batchCount = "";
			int batchCountMarkerFirstIndex = lastLine.indexOf(batchCountMarker);
			int batchCountMarkerLastIndex = batchCountMarkerFirstIndex;

			for (int i = batchCountMarkerFirstIndex + batchCountMarker.length(); i < lastLine
					.length(); i++) {
				if ((lastLine.charAt(i) >= 48 && lastLine.charAt(i) <= 57)
						|| lastLine.charAt(i) == 44) {
					batchCount = batchCount + lastLine.charAt(i);
					batchCountMarkerLastIndex = i;
				} else {
					break;
				}
			}

			System.out.println("batchCount: " + batchCount);

			Integer totalRows = null;
			if (batchCount.length() > 0) {
				try {
					System.out
							.println("###########     batchCount without comma: "
									+ batchCount.replaceAll(",", ""));
					totalRows = Integer
							.parseInt(batchCount.replaceAll(",", ""));
				} catch (Exception e) {
				}
			}

			int headerLines = importDefinition.hasHeaders() ? 1 : 0;
			if ((totalRows != null)
					&& ((lineNumber - 1 - headerLines) != totalRows)) {
				addError(new BatchErrorLog("IP7", lineNumber, line,
						"Batch Import Line Count Marker",
						(lineNumber - 1 - headerLines) + ""));
			}

			// In order to read sequential for the same batchCountMarker and
			// batchTotalMarker
			if (batchTotalMarker.equals(batchCountMarker)) {
				if (batchCountMarkerLastIndex + 1 < lastLine.length()) {
					lastLine = lastLine
							.substring(batchCountMarkerLastIndex + 1);
				} else {
					lastLine = "";
				}
			}
		} else if (batchCountMarker.length() > 0) {
			addError(new BatchErrorLog("IP7", lineNumber, line,
					"Batch Import Line Count Marker", null));
		}

		// Search batchTotalMarker
		if (batchTotalMarker.length() > 0
				&& lastLine.indexOf(batchTotalMarker) != -1) {
			String batchTotal = "";
			int batchTotalMarkerFirstIndex = lastLine.indexOf(batchTotalMarker);
			int batchTotalMarkerLastIndex = batchTotalMarkerFirstIndex;

			// Also search for +(43), -(45), ,(44), and .(46)
			boolean gotDot = false;
			for (int i = batchTotalMarkerFirstIndex + batchTotalMarker.length(); i < lastLine.length(); i++) {

				// Check for +, - signs
				if (lastLine.charAt(i) == 43 || lastLine.charAt(i) == 45) {
					if (i == (batchTotalMarkerFirstIndex + batchTotalMarker
							.length())) {
						batchTotal = batchTotal + lastLine.charAt(i);
						batchTotalMarkerLastIndex = i;
						continue;
					} else {
						break;
					}
				}

				// Check for . sign
				if (lastLine.charAt(i) == 46) {
					if (!gotDot) {
						batchTotal = batchTotal + lastLine.charAt(i);
						batchTotalMarkerLastIndex = i;
						gotDot = true;
						continue;
					} else {
						break;
					}
				}

				if ((lastLine.charAt(i) >= 48 && lastLine.charAt(i) <= 57)
						|| lastLine.charAt(i) == 43 || lastLine.charAt(i) == 45
						|| lastLine.charAt(i) == 46 || lastLine.charAt(i) == 44) {

					batchTotal = batchTotal + lastLine.charAt(i);
					batchTotalMarkerLastIndex = i;
					continue;
				} else {
					break;
				}
			}

			BigDecimal totalAmount = null;
			//int maxScale = 10;
			if (batchTotal.trim().length() > 0) {
				try {
					System.out.println("###########     batchTotal without comma: " + batchTotal.trim().replaceAll(",", ""));
					totalAmount = new BigDecimal(batchTotal.trim().replaceAll(",", ""));
				} catch (Exception e) {
				}
			}

			// 5252
			if (totalAmount != null) {			
				// 0003596
				System.out.println("###########     import file totalAmount: " + totalAmount);
				System.out.println("###########  actual computedTotalAmount: " + computedTotalAmount);

				if(totalAmount.compareTo(computedTotalAmount)!=0){
					addError(new BatchErrorLog("IP8", lineNumber, line,
							"Batch Total Marker", computedTotalAmount + ""));//Display the original value
				}
			}

		} else if (batchTotalMarker.length() > 0) {
			addError(new BatchErrorLog("IP8", lineNumber, line,
					"Batch Total Marker", null));
		}

		if (endOfFileMarker.length() > 0 && !sawEOF) {
			addError(new BatchErrorLog("IP6", lineNumber, line,
					"Batch End-of-File Marker", null));
		}
	}

	private String parserSelectClause(String sClause) {
		String returnValue = "";
		if ((sClause.charAt(0) == '\'' || sClause.charAt(0) == '"')
				&& (sClause.charAt(sClause.length() - 1) == '\'' || sClause
						.charAt(sClause.length() - 1) == '"')) {// Get the Quote
																// String
			returnValue = sClause.substring(1, sClause.length() - 1);
		} else if (sClause.toUpperCase().startsWith("IF")
				|| sClause.toUpperCase().startsWith("DECODE")) {
			returnValue = sClause.trim();
		} else if (sClause.toUpperCase().startsWith("IIF")) {
			// different syntax from "IF"
		} else if (sClause.toUpperCase().startsWith("UPPER")) {
			returnValue = sClause.trim();
		} else if (sClause.toUpperCase().startsWith("LOWER")) {
			returnValue = sClause.trim();
		} else {
			returnValue = sClause.trim();
		}
		return returnValue;
	}

	// // Call method to process the databean with Select Clauses
	// processSelectClause(columnNameList,
	// MappedColumnDatabeanMap);
	//
	// DataRow bcpRow = new DataRow(bcpDataSet);
	// HashMap aRowData = formMappedDatabeans(
	// MappedColumnDatabeanMap);
	// }
	// if (!errorHandler.getWriteImportLineFlag()) {
	// // bcpDataSet.addRow(bcpRow);
	// /*
	// * logger.info( "This Row can not be imported because
	// * the WriteImportLineFlag is false.\n" + line);
	// */
	// continue;
	// }
	//

	// private void processSelectClause(java.util.List columnNameList, HashMap
	// MappedColumnDatabeanMap){
	// int count = columnNameList.size();
	// String keyColumn = null;
	// String selectClause = null;
	// String testValue = null;
	// //HashMap decodeList = new HashMap();
	// try{
	// while (count > 0) {
	// for (int i = 0; i < columnNameList.size(); i++) {
	// keyColumn = (String) columnNameList.get(i);
	// HeaderNameValueBean databean = (HeaderNameValueBean)
	// MappedColumnDatabeanMap.get(
	// keyColumn);
	// selectClause = databean.getSelectClause().trim();
	// if(selectClause == null) continue;
	// //System.out.println(keyColumn+" <--clause is--> "+selectClause);
	// if (selectClause.toUpperCase().startsWith("UPPER"))
	// {//Hanging_For_Next_Turn
	// if(isAColumnRelyonAnother(getLowerUpperParameter(selectClause),
	// columnNameList)){
	// continue;
	// }else{
	// testValue = getValue(
	// MappedColumnDatabeanMap, selectClause, 1);
	//
	// databean.setValueBySelectClause(testValue);
	// count--;
	// columnNameList.remove(keyColumn);
	// }
	// } else if (selectClause.toUpperCase().startsWith("LOWER")) {
	// if(isAColumnRelyonAnother(getLowerUpperParameter(selectClause),
	// columnNameList)){
	// continue;
	// }else{
	// testValue = getValue(
	// MappedColumnDatabeanMap, selectClause, 0);
	//
	// databean.setValueBySelectClause(testValue);
	// count--;
	// columnNameList.remove(keyColumn);
	// }
	// } else if (selectClause.toUpperCase().startsWith("DECODE")) {
	// //Set a linked List if there relationship between these databean DECODE
	// select clause
	// //decodeList.put(keyColumn, databean);
	// if(isStillHasActiveColumn(selectClause, columnNameList)){
	// continue;
	// }else{
	// testValue = parserDecode(selectClause,
	// MappedColumnDatabeanMap);
	// databean.setValueBySelectClause(testValue);
	// count--;
	// columnNameList.remove(keyColumn);
	// }
	// } else { //Regulor hardcode String Value
	// String expression = null;
	// if (selectClause.trim().startsWith("'") ||
	// selectClause.trim().startsWith("\"")) {
	// expression = selectClause.
	// substring(1, selectClause.trim().length() - 1);
	// }else{
	// expression = selectClause.trim();
	// }
	// if(isAColumnRelyonAnother(expression, columnNameList)){
	// continue;
	// }else if(isAColumn(expression)){
	// testValue = getDatabeanValueField(expression, MappedColumnDatabeanMap);
	// //System.out.println("Set hardcode value for bean ::"+testValue);
	// databean.setValueBySelectClause(testValue);
	// count--;
	// columnNameList.remove(keyColumn);
	//
	// } else {
	// //System.out.println("Set hardcode value for bean ::"+expression);
	// databean.setValueBySelectClause(expression);
	// count--;
	// columnNameList.remove(keyColumn);
	// }
	// }
	// //count--;
	// }
	// }
	// }catch(Exception e){
	// e.printStackTrace();
	// }
	// //processDecodeList( decodeList, MappedColumnDatabeanMap );
	// if(columnNameList.size()>0 || count != 0){
	// //System.out.println("There are still some not found match DECODE existing ::"
	// +columnNameList.size());
	// }else{
	// //System.out.println(">>>>>> All select caluse (DECODE) value were checked and found.........");
	// }
	// }

	// private boolean isAColumnRelyonAnother(String keyColumn, java.util.List
	// selectClauseKeyList){
	// if(selectClauseKeyList.contains(keyColumn.toUpperCase())){
	// //System.out.println(keyColumn.toUpperCase() + " is in the List");
	// return true;
	// }
	// return false;
	// }
	//
	// private boolean isStillHasActiveColumn(String decodeExpression,
	// java.util.List keyColumnList){
	// int startIndex = "decode(".length();
	// int endIndex = decodeExpression.length()-1;//Skip ')' character
	// String expression = decodeExpression.substring(startIndex, endIndex);
	// String[] elements = expression.split(",");
	// for(int i=0; i<elements.length; i++){
	// elements[i] = elements[i].trim();
	// if((elements[i].startsWith("'") || elements[i].startsWith("\"")) &&
	// (elements[i].endsWith("'") || elements[i].endsWith("\""))){
	// elements[i] = elements[i].substring(1, elements[i].length()-1).trim();
	// }
	// if(keyColumnList.contains(elements[i].toUpperCase())){
	// //System.out.println(elements[i].toUpperCase() + " is in the List");
	// return true;
	// }
	// }
	// return false;
	// }

	/**
	 * Beware of there is a recurserve call in the following two methods.
	 * 
	 * @param decodeExpression
	 *            String
	 * @param MappedColumnDatabeanMap
	 *            HashMap
	 * @return String
	 */
	// private String parserDecode(String decodeExpression, HashMap
	// MappedColumnDatabeanMap){
	// //System.out.println("*******************************\n"+decodeExpression);
	// int startIndex = "decode(".length();
	// int endIndex = decodeExpression.length()-1;
	// String expression = decodeExpression.substring(startIndex, endIndex);
	// //System.out.println("---->"+expression);
	// String[] elements = expression.split(",");
	// for(int i=0; i<elements.length; i++){
	// elements[i] = elements[i].trim();
	// if((elements[i].startsWith("'") || elements[i].startsWith("\"")) &&
	// (elements[i].endsWith("'") || elements[i].endsWith("\""))){
	// elements[i] = elements[i].substring(1, elements[i].length()-1).trim();
	// }
	// }
	// int len = elements.length;
	// String returnValue = elements[len-1];//Last element is the default value
	// String conditionValue = elements[0];
	// //System.out.println("The condition value is :: "+conditionValue);
	// String tempValue = null;
	// if(isAColumn(conditionValue)){
	// //System.out.println(conditionValue + " is a column....." );
	// tempValue = getDatabeanValueField(conditionValue,
	// MappedColumnDatabeanMap);
	// if(!tempValue.equals("Hanging_For_Next_Turn")){
	// conditionValue = tempValue;
	// //System.out.println("The value of this column is ::"+conditionValue);
	// }else{
	// //System.out.println("Waiting for next turn......");
	// }
	// }
	// for(int i=1; i<len-1; i=i+2){
	// //If there are columns appeared here process it
	// //System.out.println("\nTry to match condition value of ( "+conditionValue+" ) with ( "+
	// elements[i] +" )");
	// if(isAColumn( elements[i] )){
	// tempValue = getDatabeanValueField(elements[i], MappedColumnDatabeanMap);
	// if(!tempValue.equals("Hanging_For_Next_Turn")){
	// elements[i] = tempValue;
	// }
	// }
	// if(conditionValue.equals( elements[i] )){
	// returnValue = elements[i+1];
	// if(isAColumn(returnValue)){
	// returnValue = getDatabeanValueField(returnValue,
	// MappedColumnDatabeanMap);
	// }
	// break;
	// }
	// }
	// //System.out.println(returnValue+"\n*******************************");
	// return returnValue.trim();
	// }
	//
	// private String getDatabeanValueField(String conditionValue, HashMap
	// MappedColumnDatabeanMap){
	// HeaderNameValueBean bean =
	// (HeaderNameValueBean)MappedColumnDatabeanMap.get( getMapKey(
	// conditionValue ));
	// if(bean != null && bean.getValueFIeld() != null){//Take the value as it
	// is
	// //System.out.println("1");
	// return bean.getValueFIeld().trim();
	// }else if(bean != null && bean.getValueFIeld() == null &&
	// bean.getSelectClause().trim().toUpperCase().startsWith("DECODE")){//Meet
	// a nested DECODE
	// //if(bean != null && bean.getValueFIeld() == null &&
	// bean.getSelectClause().trim().length() > 0){
	// //if(bean != null && bean.getValueFIeld() == null &&
	// bean.getSelectClause().trim().toUpperCase().startsWith("DECODE")){//Meet
	// a nested DECODE
	// //System.out.println("2");
	// return parserDecode(bean.getSelectClause().trim(),
	// MappedColumnDatabeanMap);
	// //return "Hanging_For_Next_Turn";
	// }else{
	// //System.out.println("3");
	// return "Hanging_For_Next_Turn";
	// }
	// }

	private String getLowerUpperParameter(String selectClause) {
		return selectClause.substring(6, selectClause.trim().length() - 1);
	}

	// private String getValue(HashMap MappedColumnDatabeanMap, String
	// selectClause, int flag){
	// String selectClauseValue = selectClause.substring(6,
	// selectClause.trim().length()-1);//Take out the parameter value
	// //System.out.println("The found value of select clause is ::"+
	// selectClauseValue);
	// String returnValue = null;
	// if(isAColumn(selectClauseValue)){
	// HeaderNameValueBean bean =
	// (HeaderNameValueBean)MappedColumnDatabeanMap.get(getMapKey(selectClauseValue));
	// if(bean.getValueFIeld() == null && bean.getSelectClause() != null){
	// return "Hanging_For_Next_Turn";
	// }
	// if(flag == 1){
	// returnValue = bean.getValueFIeld().toUpperCase();
	// }else{
	// returnValue = bean.getValueFIeld().toLowerCase();
	// }
	// }else{
	// if(flag == 1){
	// returnValue = selectClauseValue.toUpperCase();
	// }else{
	// returnValue = selectClauseValue.toLowerCase();
	// }
	// }
	// //System.out.println("Return value for this select clause is ::"+returnValue);
	// return returnValue;
	// }
	//
	// private boolean isAColumn(String value){
	// boolean returnValue = false;
	// for(int i=0; i<mappedTransDTLColumn.length; i++){
	// if(value.equalsIgnoreCase(mappedTransDTLColumn[i])){
	// //System.out.println(value +
	// " is a Column Name....find column value for it.....");
	// returnValue = true;
	// break;
	// }
	// }
	// return returnValue;
	// }
	//

	/**
	 * add the error and set abort if necessary also set the max error seen and
	 * the count of actual errors
	 */
	public boolean addError(BatchErrorLog errorLog) {
		batchErrorLogs.add(errorLog);
		errorCountTotal++;

		if (errorLog.getErrorDefCode().equalsIgnoreCase("IPXX")) {
			maxErrorSeen = 30;
			abort = true;
			return false;
		}

		ListCodes listCode = errorListCodes.get(errorLog.getErrorDefCode());

		if (listCode == null) {
			BatchErrorLog erL = new BatchErrorLog("IPXX", errorLog.getRowNo()
					.intValue(), "Error code not found",
					errorLog.getErrorDefCode(), null);

			batchErrorLogs.add(erL);
			errorCountTotal++;

			// Can continue
			return false;
		}

		maxErrorSeen = listCode.getSeverityLevelAsInt() > maxErrorSeen ? listCode
				.getSeverityLevelAsInt() : maxErrorSeen;
		if (!listCode.isWarning())
			errorCount++;

		if (listCode.isAbortEnabled()) {
			abort = true;
		}
		if ((!hitMaxErrors) && (errorCount >= preferences.getMaxErrors())) {
			BatchErrorLog maxErrorLog = new BatchErrorLog();
			maxErrorLog.setErrorDefCode("IP3");
			batchErrorLogs.add(maxErrorLog);
			errorCountTotal++;
			errorCount++;
			hitMaxErrors = true;
			if (errorListCodes.get("IP3").isAbortEnabled()) {
				abort = true;
			}

			// Assign maxErrorSeen again
			listCode = errorListCodes.get(maxErrorLog.getErrorDefCode());
			maxErrorSeen = listCode.getSeverityLevelAsInt() > maxErrorSeen ? listCode
					.getSeverityLevelAsInt() : maxErrorSeen;
		}
		return listCode.isWriteImportLineEnabled();
	}

	public int getMaxErrorSeen() {
		return maxErrorSeen;
	}

	public List<String> split(String s) {
	  	List<String> list = new ArrayList<String>();
	  	String delims = importDefinition.getFlatDelCharDecoded();

	  	StringTokenizer st = new StringTokenizer(s, delims, true);
		boolean sawDelim = true;
	  	try {
	  		while (st.hasMoreTokens()) {
	  			String t = st.nextToken(delims); 			
	  			if (sawDelim && t.equalsIgnoreCase(importDefinition.getFlatDelCharDecoded())) {
	  				list.add("");
	  			} 
	  			else{
	  				if (t.equalsIgnoreCase(importDefinition.getFlatDelCharDecoded())) {
	  					sawDelim = true;
	  				}
	  				else{
		  				list.add(t);
		  				sawDelim = false;
	  				}
	  			}
	  		}
	  	}
	  	catch (NoSuchElementException e) {  		
	  	}
		
	  	if (sawDelim) {
	  		list.add("");
		}
	  	
	  	if(importDefinition.getFlatDelTextQual()==null || importDefinition.getFlatDelTextQual().length()==0){
			return list;
		}
		else{
			List<String> listFinal = new ArrayList<String>();
		  	String tempValue = "";
		  	String value = "";
		  	boolean foundFirstTextQual = false;
			boolean foundTextQual = false;
			
			for (int i = 0; i < list.size(); i++) {
				tempValue = list.get(i);
				if(foundFirstTextQual==false){
					foundTextQual = tempValue.startsWith(importDefinition.getFlatDelTextQual());			
					if(foundTextQual==false){ // 12345, ok
						value = tempValue;
						listFinal.add(value);
						foundFirstTextQual = false;
						value = "";
					}
					else{
						foundFirstTextQual = true;
						value = tempValue;
						
						if(!value.equalsIgnoreCase(importDefinition.getFlatDelTextQual())){
							foundTextQual = value.endsWith(importDefinition.getFlatDelTextQual());
							if(foundTextQual==true){ //"12345,"							
								if(value.endsWith(importDefinition.getFlatDelTextQual())){//Trim end of FlatDelTextQual
									if(value.length()>0){
										value = value.substring(0, value.length()-1);
									}
								}
								if(value.startsWith(importDefinition.getFlatDelTextQual())){//Trim end of FlatDelTextQual
									if(value.length()>0){
										value = value.substring(1, value.length());
									}
								}							
								listFinal.add(value);
								foundFirstTextQual = false;
								value = "";
							}	
						}
					}
				}
				else{
					foundTextQual = tempValue.endsWith(importDefinition.getFlatDelTextQual());
					if(foundTextQual==false){// "12345 + 678			
						value = value + delims + tempValue;					
					}
					else{//"12345", ok
						value = value + delims + tempValue;
						if(value.endsWith(importDefinition.getFlatDelTextQual())){//Trim end of FlatDelTextQual
							if(value.length()>0){
								value = value.substring(0, value.length()-1);
							}
						}
						if(value.startsWith(importDefinition.getFlatDelTextQual())){//Trim end of FlatDelTextQual
							if(value.length()>0){
								value = value.substring(1, value.length());
							}
						}							
						listFinal.add(value);
						foundFirstTextQual = false;
						value = "";
					}
				}
			}
			
			return listFinal;
		}
	}

	public String substitute(String s, String transCol) {
		String val = s;
		String markers = importSpec.getDonotimport();
		String replacement = importSpec.getReplacewith();

		if (markers == null || replacement == null) {
			return val;
		}

		if (transactionColumnDriverNamesMap.containsKey(transCol)
				&& (transactionColumnDriverNamesMap.get(transCol)
						.getDrvNamesCode().equalsIgnoreCase("T") || transactionColumnDriverNamesMap
						.get(transCol).getDrvNamesCode().equalsIgnoreCase("L"))) {
			for (int i = 0; i < markers.length(); i++) {
				String to = replacement.substring(i * 2, (i * 2) + 2);
				// if just spaces, then strip it
				if (to.trim().length() == 0)
					to = "";
				// fixed for issue 0001273
				if (to.length() == 2
						&& to.substring(1, 2).equalsIgnoreCase(" "))
					to = to.replaceAll("\\s+", "");
				if (to.length() == 2 && to.contains("  "))
					to = to.replaceAll("\\s+", "");
				val = val.replaceAll(
						Pattern.quote(markers.substring(i, i + 1)),
						replacementCodes(to));
				logger.debug(s + " -> " + val);
			}
		}
		return val;
	}

	public String replacementCodes(String s) {
		if (s.equalsIgnoreCase("~s"))
			return " ";
		if (s.equalsIgnoreCase("~a"))
			return "\u00A4";
		if (s.equalsIgnoreCase("~b"))
			return "\u00A7";
		if (s.equalsIgnoreCase("~c"))
			return "\u2666";
		if (s.equalsIgnoreCase("~d"))
			return "\u2022";
		return s;
	}

	private boolean isEOF(String line) {
		String endOfFileMarker = "";
		if (importSpec.getBatcheofmarker() != null
				&& importSpec.getBatcheofmarker().length() > 0) {
			endOfFileMarker = importSpec.getBatcheofmarker();
		}

		if (endOfFileMarker.length() == 0) { // no EOF character defined? this
												// line can't EOF then :-)
			return false;
		}

		return line.startsWith(endOfFileMarker);

		// return line.startsWith(preferences.getEndOfFileMarker());
	}

	public List<String> getHeaders() {
		try {
			parseHeader();
		} catch (IOException e) {
		}
		return this.headerNames;
	}

	public void setImportDefinitionDetail(List<ImportDefinitionDetail> detail) {
		this.importMap = new HashMap<String, ArrayList<ImportDefinitionDetail>>();
		for (ImportDefinitionDetail d : detail) {
			// For mapping a header to multiple transaction columns.
			ArrayList<ImportDefinitionDetail> columnArray = this.importMap
					.get(d.getImportColumnName());
			if (columnArray == null) {
				columnArray = new ArrayList<ImportDefinitionDetail>();
				this.importMap.put(d.getImportColumnName().toUpperCase(),
						columnArray);
			}
			columnArray.add(d);
		}
	}

	public ImportMapProcessPreferenceDTO getPreferences() {
		return this.preferences;
	}

	public void setPreferences(ImportMapProcessPreferenceDTO preferences)
			throws DateFormatException {
		this.preferences = preferences;
	}

	public long getFileSize() {
		if (fileSize == 0) {
			this.fileSize = new File(getFilePath()).length();
		}
		return this.fileSize;
	}

	public long getEstimatedRows() {
		return this.estimatedRows;
	}

	public String getUploadPath() {
		return this.uploadPath;
	}

	public void setUploadPath(String uploadPath) {
		this.uploadPath = uploadPath;
	}

	public Map<String, PropertyDescriptor> getColumnProperties() {
		return this.columnProperties;
	}

	public void setColumnProperties(
			Map<String, PropertyDescriptor> columnProperties) {
		this.columnProperties = columnProperties;
	}

	public Map<String, PropertyDescriptor> getColumnPropertiesSale() {
		return this.columnPropertiesSale;
	}

	public void setColumnPropertiesSale(
			Map<String, PropertyDescriptor> columnPropertiesSale) {
		this.columnPropertiesSale = columnPropertiesSale;
	}

	public BigDecimal getComputedTotalAmount() {
		return this.computedTotalAmount;
	}

	public List<BatchErrorLog> getBatchErrorLogs() {
		return this.batchErrorLogs;
	}

	public void resetBatchErrorLogs() {
		if (batchErrorLogs != null) {
			batchErrorLogs.clear();
		}
		batchErrorLogs = new ArrayList<BatchErrorLog>();
		;
	}

	public int getLinesWritten() {
		return this.linesWritten;
	}

	public void setLinesWritten(int linesWritten) {
		this.linesWritten = linesWritten;
	}

	public int getTotalRows() {
		// subtract out the EOF marker
		return (importDefinition.hasHeaders() ? lineNumber - 1 : lineNumber) - 1;
	}

	public void setCacheManager(CacheManager cacheManager) {
		this.cacheManager = cacheManager;
	}

	public boolean wasAborted() {
		return this.abort;
	}

	public void setAbort(boolean abort) {
		this.abort = abort;
	}

	public void setCompleted(boolean completed) {
		this.completed = completed;
	}

	public boolean wasCompleted() {
		return this.completed;
	}

	public int getErrorCount() {
		return this.errorCount;
	}

	public int getErrorCountTotal() {
		return this.errorCountTotal;
	}

	public int getLineEnd() {
		return System.getProperty("line.separator").length();
	}

	public boolean postImport() {
		return true;
	}
	
	public static Map<String, String> splitparse(String s) {
		Map<String, String> listMap = new LinkedHashMap<String,String>();
		String delims = "�";

		StringTokenizer st = new StringTokenizer(s, delims, true);
		boolean sawDelim = true;
		try {
			while (st.hasMoreTokens()) {
				String t = st.nextToken(delims);
				if (t != null) {
					t = t.trim().toUpperCase();
				}
				if (t.equalsIgnoreCase(delims)) {
					if (sawDelim) {
						listMap.put(null, null);
					}
					sawDelim = true;
				} else {
					listMap.put(t, t);
					sawDelim = false;
				}
			}
		} catch (NoSuchElementException e) {
		}
		if (sawDelim) {
			listMap.put(null, null);
		}
		return listMap;
	}
	
	public static void main(String[] str){
		
			Character flagStr = '1';
			if(flagStr!=null && flagStr.equals('1')){
				int x = 0;
			}
			else{
				int y = 0;
			}

		
		String fileNameANSI = "pco_albert_import_ANSI.txt";
		String fileNameUTF8 = "pco_michael_import__UTF8.txt";
		String filePath = "C:\\QTP_Testing\\PinPoint.Phoenix\\Import_Files\\" + fileNameUTF8;
		
		try{
			BufferedReader input = new BufferedReader(new InputStreamReader(new FileInputStream(filePath), "UTF-8"));
			//BufferedReader input = new BufferedReader(new InputStreamReader(new FileInputStream(filePath), "US-ASCII"));
			//BufferedReader input = new BufferedReader(new InputStreamReader(new FileInputStream(filePath), "Windows-1252"));//current import
			//BufferedReader input = new BufferedReader(new InputStreamReader(new FileInputStream(filePath), inputCharset));
			
			long fileSize = (new File(filePath)).length();	
			System.out.println("fileSize: " + fileSize);
			String firstLine = input.readLine();
			System.out.println("First: " + firstLine);
			
			if(firstLine.contains("\uFEFF")){
				System.out.println("It has \uFEFF");
				
			}
			
			firstLine = firstLine.replace("\uFEFF", "");
			System.out.println("Second: " + firstLine);
			
			Map<String, String> listmap =  ImportMapUploadParser.splitparse(firstLine);
			
			for(String key : listmap.keySet()){
				System.out.println(listmap.get(key));
			}
			
			String findKey = "ENTITYCODE".toUpperCase();
			if(listmap.containsKey(findKey)){
				System.out.println("Yes");
			}

			System.out.println(new String(firstLine.getBytes(), "UTF-16"));
			System.out.println(new String(firstLine.getBytes("UTF-8"), "UTF-16"));

			byte[] bytes = firstLine.getBytes();
			String stringConverted = new String(bytes, "UTF-16");

			char[] aChar = firstLine.toCharArray();
			System.out.println(aChar);
			
			if(stringConverted.contains("entityCode")){
				System.out.println("Yes");
			}
			
			input.close();
		}
		catch(IOException e){
			e.printStackTrace();
		}
		finally{
			
		}
	}
}
