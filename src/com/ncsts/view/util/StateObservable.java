package com.ncsts.view.util;

import java.util.Observable;

public class StateObservable extends Observable {

	private static StateObservable singleton = null;

	public static final StateObservable getInstance() {
		if (singleton == null) {
			singleton = new StateObservable();
		}
		return singleton;
	}

	private StateObservable() {
	}

	public void processUpdate() {
		setChanged();
		notifyObservers();
	}
}
