package com.ncsts.view.util;

import com.ncsts.domain.BatchMaintenance;
import com.ncsts.domain.SitusRulesBatch;
import com.ncsts.dto.ImportMapProcessPreferenceDTO;
import com.ncsts.jsf.model.BatchMaintenanceDataModel;

public class SitusRulesBatchValidator {
	private BatchMaintenanceDataModel dataModel;
	private SitusRulesBatch current;
	
	public SitusRulesBatchValidator(BatchMaintenanceDataModel dataModel, ImportMapProcessPreferenceDTO importMapProcessPreferenceDTO) {
		this.dataModel = dataModel;
		
		current = new SitusRulesBatch();
		current.setReleaseVersion(importMapProcessPreferenceDTO.getLastSitusRulesRelUpdateAsInt());
		current.setReleaseDate(importMapProcessPreferenceDTO.getLastSitusRulesDateUpdateAsDate());
		/* if there is a running batch, give that precedence */
		SitusRulesBatch running = getRunningBatch();
		if (running != null) {
			current.setReleaseVersion(running.getReleaseVersion());
			current.setReleaseDate(running.getReleaseDate());
		}
	}
	
	private SitusRulesBatch getRunningBatch() {
		BatchMaintenance example = new BatchMaintenance();
		example.setBatchStatusCode(SitusRulesBatch.PROCESSING);
		example.setBatchTypeCode(SitusRulesBatch.BATCH_CODE);
		BatchMaintenance bm = dataModel.getBatchMaintenanceDAO().find(example);
		if ((bm != null) && (bm.getBatchId() != null)) 
			return new SitusRulesBatch(bm);
		else
			return null;
	}

	public SitusRulesBatch validate(SitusRulesBatch bu) {
		BatchMaintenance batch = bu.getBatchMaintenance();

		if (!bu.isRunning() && !bu.isFlagged() && !bu.isImported()) {
			bu.setError("Not an imported batch");
			return bu;
		}
		if (bu.isHeld()) {
			bu.setError("On Hold");
			return bu;
		}
		BatchMaintenance b = dataModel.getBatchMaintenanceDAO().getPreviousBatchInSequence(batch);
		if (b != null) {
			if (!bu.isNextInSequence(current)) {
				bu.setError("Batch is out of sequence");
				return bu;
			}
		}
		current = bu;
		return bu;
	}
}
