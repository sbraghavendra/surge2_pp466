package com.ncsts.view.util;

public class PathResolver {
	public static String getSampleFileUploadPath() {
		String path = System.getProperty("import.home", "").trim();
		if(path == null || path.length() == 0) {
			path = System.getProperty("catalina.home", "").trim();
			
			if(path == null || path.length() == 0) {
				path = FacesUtils.getServletContext().getRealPath("");
			}
		}
		
		path = path + "/impsample/";
		
		return path;
	}
}
