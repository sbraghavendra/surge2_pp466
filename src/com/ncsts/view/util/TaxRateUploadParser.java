package com.ncsts.view.util;
/*
 * Author: Jim M. Wilson
 * Created: Aug 22, 2008
 * $Date: 2008-08-28 11:48:23 -0600 (Thu, 28 Aug 2008) $: - $Revision: 1998 $: 
 */

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.ncsts.common.CacheManager;
import com.ncsts.domain.BCPJurisdictionTaxRate;
import com.ncsts.domain.BatchErrorLog;
import com.ncsts.domain.ListCodes;
import com.ncsts.view.bean.UpdateFilesBackingBean;

public class TaxRateUploadParser extends AbstractFileUploader implements Iterator<BCPJurisdictionTaxRate> {
  @SuppressWarnings("unused")
  private Logger logger = Logger.getLogger(TaxRateUploadParser.class);
  
  private String fileType;
  private Date releaseDate;
//FIXED FOR ISSUE 0002743
  private String releaseVersion;
  private String fileFormat;
  private List<BCPJurisdictionTaxRate> list;

  private List<BatchErrorLog> batchErrorLogs;
  private List<String> errors;
  private int lineNumber;
  private int lineNumberTotal;
  private BufferedReader bufferedReader;
  private Iterator<BCPJurisdictionTaxRate> iter;
  private String nextLine;
  private int linesWritten;
  private double stateUseRateTotal;
  private int errorCountTotal;
//fixed for issue 0002743 
  private boolean abort;
  private Map<String, String> errorListCodes = new LinkedHashMap<String,String>();

  private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
  private static final SimpleDateFormat sdfRelease = new SimpleDateFormat("yyyy_MM");
  
  private static final String[] COLUMNS = {"COUNTRY","GEOCODE","STATE","COUNTY","CITY","REPORTING_CITY","REPORTING_CITY_FIPS","STJ1_NAME",
		  "STJ2_NAME","STJ3_NAME","STJ4_NAME","STJ5_NAME","ZIP","ZIPPLUS4","IN_OUT","JUR_EFFECTIVE_DATE",
		  "JUR_EXPIRATION_DATE","DESCRIPTION","COUNTRY_NEXUSIND_CODE","STATE_NEXUSIND_CODE","COUNTY_NEXUSIND_CODE",
		  "CITY_NEXUSIND_CODE","STJ1_NEXUSIND_CODE","STJ2_NEXUSIND_CODE","STJ3_NEXUSIND_CODE","STJ4_NEXUSIND_CODE",
		  "STJ5_NEXUSIND_CODE","MEASURE_TYPE_CODE","RATE_EFFECTIVE_DATE","RATE_EXPIRATION_DATE","COUNTRY_SALES_RATE",
		  "COUNTRY_USE_RATE","STATE_SALES_RATE","STATE_USE_RATE","STATE_SALES_TIER2_RATE","STATE_USE_TIER2_RATE",
		  "STATE_SALES_TIER3_RATE","STATE_USE_TIER3_RATE","STATE_SPLIT_AMOUNT","STATE_TIER2_MIN_AMOUNT","STATE_TIER2_MAX_AMOUNT",
		  "STATE_MAXTAX_AMOUNT","COUNTY_SALES_RATE","COUNTY_USE_RATE","COUNTY_SPLIT_AMOUNT","COUNTY_MAXTAX_AMOUNT",
		  "COUNTY_SINGLE_FLAG","COUNTY_DEFAULT_FLAG","CITY_SALES_RATE","CITY_USE_RATE","CITY_SPLIT_AMOUNT","CITY_SPLIT_SALES_RATE",
		  "CITY_SPLIT_USE_RATE","CITY_SINGLE_FLAG","CITY_MAXTAX_AMOUNT","CITY_DEFAULT_FLAG","STJ1_SALES_RATE","STJ1_USE_RATE","STJ2_SALES_RATE",
		  "STJ2_USE_RATE","STJ3_SALES_RATE","STJ3_USE_RATE","STJ4_SALES_RATE","STJ4_USE_RATE","STJ5_SALES_RATE","STJ5_USE_RATE",
		  "COMBINED_SALES_RATE","COMBINED_USE_RATE","COUNTY_LOCAL_SALES_RATE","COUNTY_LOCAL_USE_RATE",
		  "CITY_LOCAL_SALES_RATE","CITY_LOCAL_USE_RATE","UPDATE_FLAG"};
  
	String[] COLUMNSV7 = {
			"COUNTRY",
			"GEOCODE",
			"STATE",
			"COUNTY",
			"CITY",
			"REPORTING_CITY",
			"REPORTING_CITY_FIPS",
			"STJ1_NAME",
			"STJ2_NAME",
			"STJ3_NAME",
			"STJ4_NAME",
			"STJ5_NAME",
			"ZIP",
			"ZIPPLUS4",
			"IN_OUT",
			"JUR_EFFECTIVE_DATE",
			"JUR_EXPIRATION_DATE",
			"DESCRIPTION",
			"COUNTRY_NEXUSIND_CODE",
			"STATE_NEXUSIND_CODE",
			"COUNTY_NEXUSIND_CODE",
			"CITY_NEXUSIND_CODE",
			"STJ1_NEXUSIND_CODE",
			"STJ2_NEXUSIND_CODE",
			"STJ3_NEXUSIND_CODE",
			"STJ4_NEXUSIND_CODE",
			"STJ5_NEXUSIND_CODE",
			"STJ1_LOCAL_CODE",
			"STJ2_LOCAL_CODE",
			"STJ3_LOCAL_CODE",
			"STJ4_LOCAL_CODE",
			"STJ5_LOCAL_CODE",
			"RATETYPE_CODE",
			"RATE_EFFECTIVE_DATE",
			"RATE_EXPIRATION_DATE",
			"COUNTRY_SALES_TIER1_RATE",
			"COUNTRY_SALES_TIER2_RATE",
			"COUNTRY_SALES_TIER3_RATE",
			"COUNTRY_SALES_TIER1_SETAMT",
			"COUNTRY_SALES_TIER2_SETAMT",
			"COUNTRY_SALES_TIER3_SETAMT",
			"COUNTRY_SALES_TIER1_MAX_AMT",
			"COUNTRY_SALES_TIER2_MIN_AMT",
			"COUNTRY_SALES_TIER2_MAX_AMT",
			"COUNTRY_SALES_TIER2_ENTAM_FLAG",
			"COUNTRY_SALES_TIER3_ENTAM_FLAG",
			"COUNTRY_SALES_MAXTAX_AMT",
			"COUNTRY_USE_TIER1_RATE",
			"COUNTRY_USE_TIER2_RATE",
			"COUNTRY_USE_TIER3_RATE",
			"COUNTRY_USE_TIER1_SETAMT",
			"COUNTRY_USE_TIER2_SETAMT",
			"COUNTRY_USE_TIER3_SETAMT",
			"COUNTRY_USE_TIER1_MAX_AMT",
			"COUNTRY_USE_TIER2_MIN_AMT",
			"COUNTRY_USE_TIER2_MAX_AMT",
			"COUNTRY_USE_TIER2_ENTAM_FLAG",
			"COUNTRY_USE_TIER3_ENTAM_FLAG",
			"COUNTRY_USE_MAXTAX_AMT",
			"COUNTRY_USE_SAME_SALES",
			"STATE_SALES_TIER1_RATE",
			"STATE_SALES_TIER2_RATE",
			"STATE_SALES_TIER3_RATE",
			"STATE_SALES_TIER1_SETAMT",
			"STATE_SALES_TIER2_SETAMT",
			"STATE_SALES_TIER3_SETAMT",
			"STATE_SALES_TIER1_MAX_AMT",
			"STATE_SALES_TIER2_MIN_AMT",
			"STATE_SALES_TIER2_MAX_AMT",
			"STATE_SALES_TIER2_ENTAM_FLAG",
			"STATE_SALES_TIER3_ENTAM_FLAG",
			"STATE_SALES_MAXTAX_AMT",
			"STATE_USE_TIER1_RATE",
			"STATE_USE_TIER2_RATE",
			"STATE_USE_TIER3_RATE",
			"STATE_USE_TIER1_SETAMT",
			"STATE_USE_TIER2_SETAMT",
			"STATE_USE_TIER3_SETAMT",
			"STATE_USE_TIER1_MAX_AMT",
			"STATE_USE_TIER2_MIN_AMT",
			"STATE_USE_TIER2_MAX_AMT",
			"STATE_USE_TIER2_ENTAM_FLAG",
			"STATE_USE_TIER3_ENTAM_FLAG",
			"STATE_USE_MAXTAX_AMT",
			"STATE_USE_SAME_SALES",
			"COUNTY_SALES_TIER1_RATE",
			"COUNTY_SALES_TIER2_RATE",
			"COUNTY_SALES_TIER3_RATE",
			"COUNTY_SALES_TIER1_SETAMT",
			"COUNTY_SALES_TIER2_SETAMT",
			"COUNTY_SALES_TIER3_SETAMT",
			"COUNTY_SALES_TIER1_MAX_AMT",
			"COUNTY_SALES_TIER2_MIN_AMT",
			"COUNTY_SALES_TIER2_MAX_AMT",
			"COUNTY_SALES_TIER2_ENTAM_FLAG",
			"COUNTY_SALES_TIER3_ENTAM_FLAG",
			"COUNTY_SALES_MAXTAX_AMT",
			"COUNTY_USE_TIER1_RATE",
			"COUNTY_USE_TIER2_RATE",
			"COUNTY_USE_TIER3_RATE",
			"COUNTY_USE_TIER1_SETAMT",
			"COUNTY_USE_TIER2_SETAMT",
			"COUNTY_USE_TIER3_SETAMT",
			"COUNTY_USE_TIER1_MAX_AMT",
			"COUNTY_USE_TIER2_MIN_AMT",
			"COUNTY_USE_TIER2_MAX_AMT",
			"COUNTY_USE_TIER2_ENTAM_FLAG",
			"COUNTY_USE_TIER3_ENTAM_FLAG",
			"COUNTY_USE_MAXTAX_AMT",
			"COUNTY_USE_SAME_SALES",
			"CITY_SALES_TIER1_RATE",
			"CITY_SALES_TIER2_RATE",
			"CITY_SALES_TIER3_RATE",
			"CITY_SALES_TIER1_SETAMT",
			"CITY_SALES_TIER2_SETAMT",
			"CITY_SALES_TIER3_SETAMT",
			"CITY_SALES_TIER1_MAX_AMT",
			"CITY_SALES_TIER2_MIN_AMT",
			"CITY_SALES_TIER2_MAX_AMT",
			"CITY_SALES_TIER2_ENTAM_FLAG",
			"CITY_SALES_TIER3_ENTAM_FLAG",
			"CITY_SALES_MAXTAX_AMT",
			"CITY_USE_TIER1_RATE",
			"CITY_USE_TIER2_RATE",
			"CITY_USE_TIER3_RATE",
			"CITY_USE_TIER1_SETAMT",
			"CITY_USE_TIER2_SETAMT",
			"CITY_USE_TIER3_SETAMT",
			"CITY_USE_TIER1_MAX_AMT",
			"CITY_USE_TIER2_MIN_AMT",
			"CITY_USE_TIER2_MAX_AMT",
			"CITY_USE_TIER2_ENTAM_FLAG",
			"CITY_USE_TIER3_ENTAM_FLAG",
			"CITY_USE_MAXTAX_AMT",
			"CITY_USE_SAME_SALES",
			"STJ1_SALES_RATE",
			"STJ1_SALES_SETAMT",
			"STJ2_SALES_RATE",
			"STJ2_SALES_SETAMT",
			"STJ3_SALES_RATE",
			"STJ3_SALES_SETAMT",
			"STJ4_SALES_RATE",
			"STJ4_SALES_SETAMT",
			"STJ5_SALES_RATE",
			"STJ5_SALES_SETAMT",
			"STJ1_USE_RATE",
			"STJ1_USE_SETAMT",
			"STJ2_USE_RATE",
			"STJ2_USE_SETAMT",
			"STJ3_USE_RATE",
			"STJ3_USE_SETAMT",
			"STJ4_USE_RATE",
			"STJ4_USE_SETAMT",
			"STJ5_USE_RATE",
			"STJ5_USE_SETAMT",
			"STJ_USE_SAME_SALES",
			"COMBINED_USE_RATE",
			"COMBINED_SALES_RATE",
			"UPDATE_FLAG"
	};

	 private boolean processHeaderMissing; 
	  
	 public boolean isprocessHeaderMissing() {
		return processHeaderMissing;
	 }
  
	 private CacheManager cacheManager;
	
	 public void setCacheManager(CacheManager cacheManager) {
		 this.cacheManager = cacheManager;
	 }
	 
	public void setErrorListCodes(Map<String, String> errorListCodes) {
		this.errorListCodes = errorListCodes;
	}

	public void setprocessHeaderMissing(boolean processHeaderMissing) {
		this.processHeaderMissing = processHeaderMissing;
	}
	public TaxRateUploadParser(String uploadPath) {
		super(uploadPath);
		deleteOnExit = true;
	}

	public void clear() {
		errors = new ArrayList<String>();
		batchErrorLogs = new ArrayList<BatchErrorLog>();
		this.errorCountTotal = 0;
	    this.fileType = null;
	    this.releaseDate = null;
	    this.releaseVersion = null;
	    this.fileFormat = null;
	    this.uploadFileName = null;
	    this.uploadFilePath = null;
	    this.totalBytes = 0;
	    this.nextLine = null;
	    this.linesInFile = 0;
	    this.lineNumber = 0;
	    this.lineNumberTotal = 0;
	    this.stateUseRateTotal = 0.0;
	    // FIXED FOR ISSUE 0002743
	    this.abort=false;
	}
	
public void processHeader(BufferedReader reader) throws IOException {
		
		String firstLine = reader.readLine();

		try {
			String type = UpdateFilesBackingBean.getHeaderValueOfType(firstLine, UpdateFilesBackingBean.TYPE_VALUE);
			if(type==null) {
				addError(1, firstLine, "IP12");
				processHeaderMissing =  true;
				return;
			}
			this.fileType = type;
				
			String s = UpdateFilesBackingBean.getHeaderValueOfType(firstLine, UpdateFilesBackingBean.DATE_VALUE);
			if(s==null) {
				addError(1, firstLine, "IP12");
				processHeaderMissing =  true;
				return;
			}
			this.releaseDate = sdfRelease.parse(s);

			String release = UpdateFilesBackingBean.getHeaderValueOfType(firstLine, UpdateFilesBackingBean.REL_VALUE);
			if(release==null) {
				addError(1, firstLine, "IP12");
				processHeaderMissing =  true;
				return;
			}
			this.releaseVersion = release;
			
			String typecode = UpdateFilesBackingBean.getHeaderValueOfType(firstLine, UpdateFilesBackingBean.FILE_VALUE);
			if(typecode==null) {
				addError(1, firstLine, "IP12");
				processHeaderMissing =  true;
				return;
			}			
			
		} catch (ParseException e) {
			addError(1, firstLine, "IP12");
			this.releaseDate = null;
		}
		catch (NullPointerException e) {
			logger.error("ReleaseDate is Null: " + e.getMessage());
			addError(1, firstLine, "IP12");
		 }
		
		try {
			this.releaseVersion = UpdateFilesBackingBean.getHeaderValueOfType(firstLine, UpdateFilesBackingBean.REL_VALUE);
		} catch (NumberFormatException e) {
			addError(1, e.getMessage(), "IP12");
			logger.error("ReleaseVersion should be in Number" + e);
		} catch (NullPointerException e) {
			addError(1, e.getMessage(), "IP12");
			logger.error("ReleaseVersion not be NULL" + e);

		} catch (Exception e) {
			addError(1, e.getMessage(), "IP12");
			logger.error("ReleaseVersion should be Number" + e);
		}
		
	}
	
  public void upload(String fileName, InputStream stream) {
  	clear();
    try {
    	super.upload(fileName, stream);
    } catch (Exception e) {
      addError(1, e.getMessage(), "TR21");
      logger.error(e);
    }
  }
  
  public boolean hasNext() {
  	if (bufferedReader == null) return false;
  	try {
  		nextLine = bufferedReader.readLine();
  		if (nextLine == null) {
  			bufferedReader.close();
  			bufferedReader = null;
  			return false;
  		}
  		return nextLine.charAt(0) != '~';
  	} catch (IOException e) {
			addError(e.getMessage());
			return false;
  	}
  }
  
  public void remove() {
  	// not implemented
  }
  
  public Iterator<BCPJurisdictionTaxRate> iterator() {
		lineNumber = 2;
		lineNumberTotal = 2;
		linesWritten = 0;
		stateUseRateTotal = 0.0;
		nextLine = null;
		try {
			bufferedReader = new BufferedReader(new FileReader(uploadFilePath));
		} catch (FileNotFoundException e) {
			logger.error(e.getMessage(), e);
			addError(e.getMessage());
		}catch(NullPointerException e){
			logger.error(e.getMessage(), e);
			addError(e.getMessage());
		}
		return this;
  }

  public Double getProgress() {
  	if (lineNumber < 1) return -0.01;
  	try {
  		logger.debug("Line: " + linesWritten + ", lines: " + linesInFile + ", " + linesWritten / new Double(linesInFile));
    	return linesWritten / new Double(linesInFile);
  	} catch (Exception e) {
  		return -0.01;
  	}
  }
  
  public BCPJurisdictionTaxRate next() {
	  if(fileFormat!=null && fileFormat.equalsIgnoreCase("TaxRateV7")){
		  return nextTaxRateV7();
	  }
	  else{
		  return nextTaxRate();
	  }
  }
  
  public BCPJurisdictionTaxRate nextTaxRate() {
		if ((bufferedReader == null) || (nextLine == null)) return null;
		// this really slows things down, so only enable during debugging...
		
		if ((lineNumber % 200) == 0) {
			logger.debug("Line: " + lineNumber + ", errorCount:" + errorCountTotal);
		}
		String tokens[] = nextLine.split("\\t");
		BCPJurisdictionTaxRate r = new BCPJurisdictionTaxRate();
		if (fileType.equals("Update") && (tokens.length != COLUMNS.length)) {
			addError(lineNumberTotal, null, "Expected " + COLUMNS.length + ", actual: " + tokens.length, null, nextLine, "TR21");
		} else if (fileType.equals("Full") && (tokens.length != COLUMNS.length - 1)) {
			addError(lineNumberTotal, null, "Expected " + (COLUMNS.length-1) + ", actual: " + tokens.length, null, nextLine, "TR21");
		} else {
			int i = 0;
			String parsedColumn=null;
			try {
				r.setLine(new Long(lineNumber));
				r.setText(nextLine);
				
				r.setCountry(parsedColumn = tokens[i++]);
				r.setGeocode(parsedColumn = tokens[i++]);
				if(parsedColumn==null || parsedColumn.length()!=12){
					throw new Exception("Length of the Geocode should be 12.");
				}
				r.setState(parsedColumn = tokens[i++]);
				r.setCounty(parsedColumn = tokens[i++]);
				r.setCity(parsedColumn = tokens[i++]);
				r.setReportingCity(parsedColumn = tokens[i++]);
				r.setReportingCityFips(parsedColumn = tokens[i++]);
				r.setStj1Name(parsedColumn = tokens[i++]);
				r.setStj2Name(parsedColumn = tokens[i++]);
				r.setStj3Name(parsedColumn = tokens[i++]);
				r.setStj4Name(parsedColumn = tokens[i++]);
				r.setStj5Name(parsedColumn = tokens[i++]);
				r.setZip(parsedColumn = tokens[i++]);
				r.setZipplus4(parsedColumn = tokens[i++]);
				r.setInOut(truncateToByte(parsedColumn = tokens[i++]));
				try{
					r.setJurEffectiveDate(sdf.parse(parsedColumn = tokens[i++]));
				} catch (Exception e) {
					r.setJurEffectiveDate(sdf.parse("9999/12/31"));
					throw e;
				}
				
				try{
					r.setJurExpirationDate(sdf.parse(parsedColumn = tokens[i++]));
				} catch (Exception e) {
					r.setJurExpirationDate(sdf.parse("9999/12/31"));
					throw e;
				}
				r.setDescription(parsedColumn = tokens[i++]);
				r.setCountryNexusindCode(parsedColumn = tokens[i++]);
				r.setStateNexusindCode(parsedColumn = tokens[i++]);
				r.setCountyNexusindCode(parsedColumn = tokens[i++]);
				r.setCityNexusindCode(parsedColumn = tokens[i++]);
				r.setStj1NexusindCode(parsedColumn = tokens[i++]);
				r.setStj2NexusindCode(parsedColumn = tokens[i++]);
				r.setStj3NexusindCode(parsedColumn = tokens[i++]);
				r.setStj4NexusindCode(parsedColumn = tokens[i++]);
				r.setStj5NexusindCode(parsedColumn = tokens[i++]);
				r.setRatetypeCode(parsedColumn = tokens[i++]);
				try{
					r.setRateEffectiveDate(sdf.parse(parsedColumn = tokens[i++]));
				} catch (Exception e) {
					//It should never be effective.
					r.setRateEffectiveDate(sdf.parse("9999/12/31"));
					throw e;
				}
				try{
					r.setRateExpirationDate(sdf.parse(parsedColumn = tokens[i++]));
				} catch (Exception e) {
					//It should never be effective.
					r.setRateExpirationDate(sdf.parse("9999/12/31"));
					throw e;
				}
				r.setCountrySalesTier1Rate(parseDouble(parsedColumn = tokens[i++]));//
				r.setCountryUseTier1Rate(parseDouble(parsedColumn = tokens[i++]));//
				r.setStateSalesTier1Rate(parseDouble(parsedColumn = tokens[i++]));//
				r.setStateUseTier1Rate(parseDouble(parsedColumn = tokens[i++]));//
				r.setStateSalesTier2Rate(parseDouble(parsedColumn = tokens[i++]));
				r.setStateUseTier2Rate(parseDouble(parsedColumn = tokens[i++]));
				r.setStateSalesTier3Rate(parseDouble(parsedColumn = tokens[i++]));
				r.setStateUseTier3Rate(parseDouble(parsedColumn = tokens[i++]));
				
				r.setStateSalesTier1MaxAmt(parseDouble(parsedColumn = tokens[i++]));//
				r.setStateUseTier1MaxAmt(r.getStateSalesTier1MaxAmt());//Same as use
				r.setStateSalesTier2MinAmt(parseDouble(parsedColumn = tokens[i++]));//
				r.setStateUseTier2MinAmt(r.getStateSalesTier2MinAmt());//Same as use
				r.setStateSalesTier2MaxAmt(parseDouble(parsedColumn = tokens[i++]));//
				r.setStateUseTier2MaxAmt(r.getStateSalesTier2MaxAmt());//Same as use
				r.setStateSalesMaxtaxAmt(parseDouble(parsedColumn = tokens[i++]));//
				r.setStateUseMaxtaxAmt(r.getStateSalesMaxtaxAmt());//Same as use
				
				r.setCountySalesTier1Rate(parseDouble(parsedColumn = tokens[i++]));//
				r.setCountyUseTier1Rate(parseDouble(parsedColumn = tokens[i++]));//

				r.setCountySalesTier1MaxAmt(parseDouble(parsedColumn = tokens[i++]));//
				r.setCountyUseTier1MaxAmt(r.getCountySalesTier1MaxAmt());//Same as use
				
				r.setCountySalesMaxtaxAmt(parseDouble(parsedColumn = tokens[i++]));//
				r.setCountyUseMaxtaxAmt(r.getCountySalesMaxtaxAmt());//Same as use
				
				i++;
				//Skip: r.setCountySingleFlag(truncateToByte(parsedColumn = tokens[i++]));
				i++;
				//Skip: r.setCountyDefaultFlag(truncateToByte(parsedColumn = tokens[i++]));	
				
				r.setCitySalesTier1Rate(parseDouble(parsedColumn = tokens[i++]));//
				r.setCityUseTier1Rate(parseDouble(parsedColumn = tokens[i++]));//
				
				r.setCitySalesTier1MaxAmt(parseDouble(parsedColumn = tokens[i++]));//
				r.setCityUseTier1MaxAmt(r.getCitySalesTier1MaxAmt());//Same as use
				
				
				r.setCitySalesTier2Rate(parseDouble(parsedColumn = tokens[i++]));//
				r.setCityUseTier2Rate(parseDouble(parsedColumn = tokens[i++]));//
				
				i++;
				//Skip: r.setCitySingleFlag(truncateToByte(parsedColumn = tokens[i++]));
				
				r.setCitySalesMaxtaxAmt(parseDouble(parsedColumn = tokens[i++]));//
				r.setCityUseMaxtaxAmt(r.getCitySalesMaxtaxAmt());//
				
				i++;
				//Skip: r.setCityDefaultFlag(truncateToByte(parsedColumn = tokens[i++]));
				
				r.setStj1SalesRate(parseDouble(parsedColumn = tokens[i++]));
				r.setStj1UseRate(parseDouble(parsedColumn = tokens[i++]));
				r.setStj2SalesRate(parseDouble(parsedColumn = tokens[i++]));
				r.setStj2UseRate(parseDouble(parsedColumn = tokens[i++]));
				r.setStj3SalesRate(parseDouble(parsedColumn = tokens[i++]));
				r.setStj3UseRate(parseDouble(parsedColumn = tokens[i++]));
				r.setStj4SalesRate(parseDouble(parsedColumn = tokens[i++]));
				r.setStj4UseRate(parseDouble(parsedColumn = tokens[i++]));
				r.setStj5SalesRate(parseDouble(parsedColumn = tokens[i++]));
				r.setStj5UseRate(parseDouble(parsedColumn = tokens[i++]));
				r.setCombinedSalesRate(parseDouble(parsedColumn = tokens[i++]));
				r.setCombinedUseRate(parseDouble(parsedColumn = tokens[i++]));
				
				i++;
				//Skip: r.setCountyLocalSalesRate(parseDouble(parsedColumn = tokens[i++]));
				i++;
				//Skip: r.setCountyLocalUseRate(parseDouble(parsedColumn = tokens[i++]));
				i++;
				//Skip: r.setCityLocalSalesRate(parseDouble(parsedColumn = tokens[i++]));
				i++;
				//Skip: r.setCityLocalUseRate(parseDouble(parsedColumn = tokens[i++]));
				
				if (fileType.equals("Update")) {
					r.setUpdateFlag(truncateToByte(parsedColumn = tokens[i++]));
				}
				
				//The following to set default to new TaxRate
				r.setStj1LocalCode("UNKNOWN");
				r.setStj2LocalCode("UNKNOWN");
				r.setStj3LocalCode("UNKNOWN");
				r.setStj4LocalCode("UNKNOWN");
				r.setStj5LocalCode("UNKNOWN");
				
				r.setCountrySalesTier2Rate(0d);
				r.setCountrySalesTier3Rate(0d);
				r.setCountrySalesTier1Setamt(0d);
				r.setCountrySalesTier2Setamt(0d);
				r.setCountrySalesTier3Setamt(0d);
				r.setCountrySalesTier1MaxAmt(999999999d);
				r.setCountrySalesTier2MinAmt(0d);
				r.setCountrySalesTier2MaxAmt(0d);
				r.setCountrySalesTier2EntamFlag("0");
				r.setCountrySalesTier3EntamFlag("0");
				r.setCountrySalesMaxtaxAmt(0d);
				
				r.setCountryUseTier2Rate(0d);
				r.setCountryUseTier3Rate(0d);
				r.setCountryUseTier1Setamt(0d);
				r.setCountryUseTier2Setamt(0d);
				r.setCountryUseTier3Setamt(0d);
				r.setCountryUseTier1MaxAmt(999999999d);
				r.setCountryUseTier2MinAmt(0d);
				r.setCountryUseTier2MaxAmt(0d);
				r.setCountryUseTier2EntamFlag("0");
				r.setCountryUseTier3EntamFlag("0");
				r.setCountryUseMaxtaxAmt(0d);
				r.setCountryUseSameSales("0");
				
				r.setStateSalesTier1Setamt(0d);
				r.setStateSalesTier2Setamt(0d);
				r.setStateSalesTier3Setamt(0d);
				
				r.setStateSalesTier2EntamFlag("0");
				r.setStateSalesTier3EntamFlag("0");
				
				r.setStateUseTier1Setamt(0d);
				r.setStateUseTier2Setamt(0d);
				r.setStateUseTier3Setamt(0d);
				
				r.setStateUseTier2EntamFlag("0");
				r.setStateUseTier3EntamFlag("0");
				
				r.setStateUseSameSales("0");
				
				r.setCountySalesTier2Rate(0d);
				r.setCountySalesTier3Rate(0d);
				r.setCountySalesTier1Setamt(0d);
				r.setCountySalesTier2Setamt(0d);
				r.setCountySalesTier3Setamt(0d);
				
				r.setCountySalesTier2MinAmt(0d);
				r.setCountySalesTier2MaxAmt(0d);
				r.setCountySalesTier2EntamFlag("0");
				r.setCountySalesTier3EntamFlag("0");
				
				r.setCountyUseTier2Rate(0d);
				r.setCountyUseTier3Rate(0d);
				r.setCountyUseTier1Setamt(0d);
				r.setCountyUseTier2Setamt(0d);
				r.setCountyUseTier3Setamt(0d);
				
				r.setCountyUseTier2MinAmt(0d);
				r.setCountyUseTier2MaxAmt(0d);
				r.setCountyUseTier2EntamFlag("0");
				r.setCountyUseTier3EntamFlag("0");
				
				r.setCountyUseSameSales("0");
				
				r.setCitySalesTier3Rate(0d);
				r.setCitySalesTier1Setamt(0d);
				r.setCitySalesTier2Setamt(0d);
				r.setCitySalesTier3Setamt(0d);
				
				r.setCitySalesTier2MinAmt(0d);
				r.setCitySalesTier2MaxAmt(0d);
				r.setCitySalesTier2EntamFlag("0");
				r.setCitySalesTier3EntamFlag("0");
				r.setCitySalesMaxtaxAmt(0d);
				
				r.setCityUseTier3Rate(0d);
				r.setCityUseTier1Setamt(0d);
				r.setCityUseTier2Setamt(0d);
				r.setCityUseTier3Setamt(0d);
				
				r.setCityUseTier2MinAmt(0d);
				r.setCityUseTier2MaxAmt(0d);
				r.setCityUseTier2EntamFlag("0");
				r.setCityUseTier3EntamFlag("0");
				r.setCityUseMaxtaxAmt(0d);
				r.setCityUseSameSales("0");
				r.setStj1SalesSetamt(0d);
				r.setStj2SalesSetamt(0d);
				r.setStj3SalesSetamt(0d);
				r.setStj4SalesSetamt(0d);
				r.setStj5SalesSetamt(0d);
				r.setStj1UseSetamt(0d);
				r.setStj2UseSetamt(0d);
				r.setStj3UseSetamt(0d);
				r.setStj4UseSetamt(0d);
				r.setStj5UseSetamt(0d);
				r.setStjUseSameSales("0");
	
				stateUseRateTotal += r.getStateUseTier1Rate();
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
				addError(lineNumberTotal, i, parsedColumn, COLUMNS[i - 1], nextLine, "TR21");
				
				//0004213, do not write to database
				r = null;
			}
			
			lineNumber++;
		}
		
		//0001701: Import errors make the system crash
		lineNumberTotal++;
		return r;
	}

  public BCPJurisdictionTaxRate nextTaxRateV7() {
		if ((bufferedReader == null) || (nextLine == null)) return null;
		// this really slows things down, so only enable during debugging...
		
		if ((lineNumber % 200) == 0) {
			logger.debug("Line: " + lineNumber + ", errorCount:" + errorCountTotal);
		}
		String tokens[] = nextLine.split("\\t");
		BCPJurisdictionTaxRate r = new BCPJurisdictionTaxRate();
		if (fileType.equals("Update") && (tokens.length != COLUMNSV7.length)) {
			addError(lineNumberTotal, null, "Expected " + COLUMNSV7.length + ", actual: " + tokens.length, null, nextLine, "TR21");
		} else if (fileType.equals("Full") && (tokens.length != COLUMNSV7.length - 1)) {
			addError(lineNumberTotal, null, "Expected " + (COLUMNSV7.length-1) + ", actual: " + tokens.length, null, nextLine, "TR21");
		} else {
			int i = 0;
			String parsedColumn=null;
			try {
				r.setLine(new Long(lineNumber));
				r.setText(nextLine);
					
				r.setCountry(parsedColumn = tokens[i++]);
				r.setGeocode(parsedColumn = tokens[i++]);
				if(parsedColumn==null || parsedColumn.length()!=12){
					throw new Exception("Length of the Geocode should be 12.");
				}
				r.setState(parsedColumn = tokens[i++]);			
				r.setCounty(parsedColumn = tokens[i++]);				
				r.setCity(parsedColumn = tokens[i++]);	
				r.setReportingCity(parsedColumn = tokens[i++]);
				r.setReportingCityFips(parsedColumn = tokens[i++]);
				r.setStj1Name(parsedColumn = tokens[i++]);
				r.setStj2Name(parsedColumn = tokens[i++]);
				r.setStj3Name(parsedColumn = tokens[i++]);
				r.setStj4Name(parsedColumn = tokens[i++]);
				r.setStj5Name(parsedColumn = tokens[i++]);			
				r.setZip(parsedColumn = tokens[i++]);		
				r.setZipplus4(parsedColumn = tokens[i++]);
				r.setInOut(parsedColumn = tokens[i++]);	
				try{
					r.setJurEffectiveDate(sdf.parse(parsedColumn = tokens[i++]));
				} catch (Exception e) {
					r.setJurEffectiveDate(sdf.parse("9999/12/31"));
					throw e;
				}	
				try{
					r.setJurExpirationDate(sdf.parse(parsedColumn = tokens[i++]));
				} catch (Exception e) {
					r.setJurExpirationDate(sdf.parse("9999/12/31"));
					throw e;
				}	
				r.setDescription(parsedColumn = tokens[i++]);
				r.setCountryNexusindCode(parsedColumn = tokens[i++]);
				r.setStateNexusindCode(parsedColumn = tokens[i++]);
				r.setCountyNexusindCode(parsedColumn = tokens[i++]);
				r.setCityNexusindCode(parsedColumn = tokens[i++]);
				r.setStj1NexusindCode(parsedColumn = tokens[i++]);
				r.setStj2NexusindCode(parsedColumn = tokens[i++]);
				r.setStj3NexusindCode(parsedColumn = tokens[i++]);
				r.setStj4NexusindCode(parsedColumn = tokens[i++]);
				r.setStj5NexusindCode(parsedColumn = tokens[i++]);
				r.setStj1LocalCode(parsedColumn = tokens[i++]);
				r.setStj2LocalCode(parsedColumn = tokens[i++]);
				r.setStj3LocalCode(parsedColumn = tokens[i++]);
				r.setStj4LocalCode(parsedColumn = tokens[i++]);
				r.setStj5LocalCode(parsedColumn = tokens[i++]);
				r.setRatetypeCode(parsedColumn = tokens[i++]);
				try{
					r.setRateEffectiveDate(sdf.parse(parsedColumn = tokens[i++]));
				} catch (Exception e) {
					//It should never be effective.
					r.setRateEffectiveDate(sdf.parse("9999/12/31"));
					throw e;
				}
				try{
					r.setRateExpirationDate(sdf.parse(parsedColumn = tokens[i++]));
				} catch (Exception e) {
					//It should never be effective.
					r.setRateExpirationDate(sdf.parse("9999/12/31"));
					throw e;
				}
				r.setCountrySalesTier1Rate(parseDouble(parsedColumn = tokens[i++]));
				r.setCountrySalesTier2Rate(parseDouble(parsedColumn = tokens[i++]));
				r.setCountrySalesTier3Rate(parseDouble(parsedColumn = tokens[i++]));
				r.setCountrySalesTier1Setamt(parseDouble(parsedColumn = tokens[i++]));
				r.setCountrySalesTier2Setamt(parseDouble(parsedColumn = tokens[i++]));
				r.setCountrySalesTier3Setamt(parseDouble(parsedColumn = tokens[i++]));
				r.setCountrySalesTier1MaxAmt(parseDouble(parsedColumn = tokens[i++]));
				r.setCountrySalesTier2MinAmt(parseDouble(parsedColumn = tokens[i++]));
				r.setCountrySalesTier2MaxAmt(parseDouble(parsedColumn = tokens[i++]));
				r.setCountrySalesTier2EntamFlag(truncateToByte(parsedColumn = tokens[i++]));
				r.setCountrySalesTier3EntamFlag(truncateToByte(parsedColumn = tokens[i++]));
				r.setCountrySalesMaxtaxAmt(parseDouble(parsedColumn = tokens[i++]));
				r.setCountryUseTier1Rate(parseDouble(parsedColumn = tokens[i++]));
				r.setCountryUseTier2Rate(parseDouble(parsedColumn = tokens[i++]));
				r.setCountryUseTier3Rate(parseDouble(parsedColumn = tokens[i++]));
				r.setCountryUseTier1Setamt(parseDouble(parsedColumn = tokens[i++]));
				r.setCountryUseTier2Setamt(parseDouble(parsedColumn = tokens[i++]));
				r.setCountryUseTier3Setamt(parseDouble(parsedColumn = tokens[i++]));
				r.setCountryUseTier1MaxAmt(parseDouble(parsedColumn = tokens[i++]));
				r.setCountryUseTier2MinAmt(parseDouble(parsedColumn = tokens[i++]));
				r.setCountryUseTier2MaxAmt(parseDouble(parsedColumn = tokens[i++]));
				r.setCountryUseTier2EntamFlag(truncateToByte(parsedColumn = tokens[i++]));
				r.setCountryUseTier3EntamFlag(truncateToByte(parsedColumn = tokens[i++]));
				r.setCountryUseMaxtaxAmt(parseDouble(parsedColumn = tokens[i++]));
				r.setCountryUseSameSales(parsedColumn = tokens[i++]);
				r.setStateSalesTier1Rate(parseDouble(parsedColumn = tokens[i++]));
				r.setStateSalesTier2Rate(parseDouble(parsedColumn = tokens[i++]));
				r.setStateSalesTier3Rate(parseDouble(parsedColumn = tokens[i++]));
				r.setStateSalesTier1Setamt(parseDouble(parsedColumn = tokens[i++]));
				r.setStateSalesTier2Setamt(parseDouble(parsedColumn = tokens[i++]));
				r.setStateSalesTier3Setamt(parseDouble(parsedColumn = tokens[i++]));
				r.setStateSalesTier1MaxAmt(parseDouble(parsedColumn = tokens[i++]));
				r.setStateSalesTier2MinAmt(parseDouble(parsedColumn = tokens[i++]));
				r.setStateSalesTier2MaxAmt(parseDouble(parsedColumn = tokens[i++]));
				r.setStateSalesTier2EntamFlag(truncateToByte(parsedColumn = tokens[i++]));
				r.setStateSalesTier3EntamFlag(truncateToByte(parsedColumn = tokens[i++]));
				r.setStateSalesMaxtaxAmt(parseDouble(parsedColumn = tokens[i++]));
				r.setStateUseTier1Rate(parseDouble(parsedColumn = tokens[i++]));
				r.setStateUseTier2Rate(parseDouble(parsedColumn = tokens[i++]));
				r.setStateUseTier3Rate(parseDouble(parsedColumn = tokens[i++]));
				r.setStateUseTier1Setamt(parseDouble(parsedColumn = tokens[i++]));
				r.setStateUseTier2Setamt(parseDouble(parsedColumn = tokens[i++]));
				r.setStateUseTier3Setamt(parseDouble(parsedColumn = tokens[i++]));
				r.setStateUseTier1MaxAmt(parseDouble(parsedColumn = tokens[i++]));
				r.setStateUseTier2MinAmt(parseDouble(parsedColumn = tokens[i++]));
				r.setStateUseTier2MaxAmt(parseDouble(parsedColumn = tokens[i++]));
				r.setStateUseTier2EntamFlag(truncateToByte(parsedColumn = tokens[i++]));
				r.setStateUseTier3EntamFlag(truncateToByte(parsedColumn = tokens[i++]));
				r.setStateUseMaxtaxAmt(parseDouble(parsedColumn = tokens[i++]));
				r.setStateUseSameSales(parsedColumn = tokens[i++]);
				r.setCountySalesTier1Rate(parseDouble(parsedColumn = tokens[i++]));
				r.setCountySalesTier2Rate(parseDouble(parsedColumn = tokens[i++]));
				r.setCountySalesTier3Rate(parseDouble(parsedColumn = tokens[i++]));
				r.setCountySalesTier1Setamt(parseDouble(parsedColumn = tokens[i++]));
				r.setCountySalesTier2Setamt(parseDouble(parsedColumn = tokens[i++]));
				r.setCountySalesTier3Setamt(parseDouble(parsedColumn = tokens[i++]));
				r.setCountySalesTier1MaxAmt(parseDouble(parsedColumn = tokens[i++]));
				r.setCountySalesTier2MinAmt(parseDouble(parsedColumn = tokens[i++]));
				r.setCountySalesTier2MaxAmt(parseDouble(parsedColumn = tokens[i++]));
				r.setCountySalesTier2EntamFlag(truncateToByte(parsedColumn = tokens[i++]));
				r.setCountySalesTier3EntamFlag(truncateToByte(parsedColumn = tokens[i++]));
				r.setCountySalesMaxtaxAmt(parseDouble(parsedColumn = tokens[i++]));
				r.setCountyUseTier1Rate(parseDouble(parsedColumn = tokens[i++]));
				r.setCountyUseTier2Rate(parseDouble(parsedColumn = tokens[i++]));
				r.setCountyUseTier3Rate(parseDouble(parsedColumn = tokens[i++]));
				r.setCountyUseTier1Setamt(parseDouble(parsedColumn = tokens[i++]));
				r.setCountyUseTier2Setamt(parseDouble(parsedColumn = tokens[i++]));
				r.setCountyUseTier3Setamt(parseDouble(parsedColumn = tokens[i++]));
				r.setCountyUseTier1MaxAmt(parseDouble(parsedColumn = tokens[i++]));
				r.setCountyUseTier2MinAmt(parseDouble(parsedColumn = tokens[i++]));
				r.setCountyUseTier2MaxAmt(parseDouble(parsedColumn = tokens[i++]));
				r.setCountyUseTier2EntamFlag(truncateToByte(parsedColumn = tokens[i++]));
				r.setCountyUseTier3EntamFlag(truncateToByte(parsedColumn = tokens[i++]));
				r.setCountyUseMaxtaxAmt(parseDouble(parsedColumn = tokens[i++]));
				r.setCountyUseSameSales(parsedColumn = tokens[i++]);
				r.setCitySalesTier1Rate(parseDouble(parsedColumn = tokens[i++]));
				r.setCitySalesTier2Rate(parseDouble(parsedColumn = tokens[i++]));
				r.setCitySalesTier3Rate(parseDouble(parsedColumn = tokens[i++]));
				r.setCitySalesTier1Setamt(parseDouble(parsedColumn = tokens[i++]));
				r.setCitySalesTier2Setamt(parseDouble(parsedColumn = tokens[i++]));
				r.setCitySalesTier3Setamt(parseDouble(parsedColumn = tokens[i++]));
				r.setCitySalesTier1MaxAmt(parseDouble(parsedColumn = tokens[i++]));
				r.setCitySalesTier2MinAmt(parseDouble(parsedColumn = tokens[i++]));
				r.setCitySalesTier2MaxAmt(parseDouble(parsedColumn = tokens[i++]));
				r.setCitySalesTier2EntamFlag(truncateToByte(parsedColumn = tokens[i++]));
				r.setCitySalesTier3EntamFlag(truncateToByte(parsedColumn = tokens[i++]));
				r.setCitySalesMaxtaxAmt(parseDouble(parsedColumn = tokens[i++]));
				r.setCityUseTier1Rate(parseDouble(parsedColumn = tokens[i++]));
				r.setCityUseTier2Rate(parseDouble(parsedColumn = tokens[i++]));
				r.setCityUseTier3Rate(parseDouble(parsedColumn = tokens[i++]));
				r.setCityUseTier1Setamt(parseDouble(parsedColumn = tokens[i++]));
				r.setCityUseTier2Setamt(parseDouble(parsedColumn = tokens[i++]));
				r.setCityUseTier3Setamt(parseDouble(parsedColumn = tokens[i++]));
				r.setCityUseTier1MaxAmt(parseDouble(parsedColumn = tokens[i++]));
				r.setCityUseTier2MinAmt(parseDouble(parsedColumn = tokens[i++]));
				r.setCityUseTier2MaxAmt(parseDouble(parsedColumn = tokens[i++]));
				r.setCityUseTier2EntamFlag(truncateToByte(parsedColumn = tokens[i++]));
				r.setCityUseTier3EntamFlag(truncateToByte(parsedColumn = tokens[i++]));
				r.setCityUseMaxtaxAmt(parseDouble(parsedColumn = tokens[i++]));
				r.setCityUseSameSales(parsedColumn = tokens[i++]);
				r.setStj1SalesRate(parseDouble(parsedColumn = tokens[i++]));
				r.setStj1SalesSetamt(parseDouble(parsedColumn = tokens[i++]));
				r.setStj2SalesRate(parseDouble(parsedColumn = tokens[i++]));
				r.setStj2SalesSetamt(parseDouble(parsedColumn = tokens[i++]));
				r.setStj3SalesRate(parseDouble(parsedColumn = tokens[i++]));
				r.setStj3SalesSetamt(parseDouble(parsedColumn = tokens[i++]));
				r.setStj4SalesRate(parseDouble(parsedColumn = tokens[i++]));
				r.setStj4SalesSetamt(parseDouble(parsedColumn = tokens[i++]));
				r.setStj5SalesRate(parseDouble(parsedColumn = tokens[i++]));
				r.setStj5SalesSetamt(parseDouble(parsedColumn = tokens[i++]));
				r.setStj1UseRate(parseDouble(parsedColumn = tokens[i++]));
				r.setStj1UseSetamt(parseDouble(parsedColumn = tokens[i++]));
				r.setStj2UseRate(parseDouble(parsedColumn = tokens[i++]));
				r.setStj2UseSetamt(parseDouble(parsedColumn = tokens[i++]));
				r.setStj3UseRate(parseDouble(parsedColumn = tokens[i++]));
				r.setStj3UseSetamt(parseDouble(parsedColumn = tokens[i++]));
				r.setStj4UseRate(parseDouble(parsedColumn = tokens[i++]));
				r.setStj4UseSetamt(parseDouble(parsedColumn = tokens[i++]));
				r.setStj5UseRate(parseDouble(parsedColumn = tokens[i++]));
				r.setStj5UseSetamt(parseDouble(parsedColumn = tokens[i++]));
				r.setStjUseSameSales(parsedColumn = tokens[i++]);
				r.setCombinedSalesRate(parseDouble(parsedColumn = tokens[i++]));
				r.setCombinedUseRate(parseDouble(parsedColumn = tokens[i++]));
		
				if (fileType.equals("Update")) {
					r.setUpdateFlag(truncateToByte(parsedColumn = tokens[i++]));
				}
				
				stateUseRateTotal += r.getStateUseTier1Rate();
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
				addError(lineNumberTotal, i, parsedColumn, COLUMNS[i - 1], nextLine, "TR21");
				
				//0004213, do not write to database
				r = null;
			}
			
			lineNumber++;
		}
		
		//0001701: Import errors make the system crash
		lineNumberTotal++;
		return r;
	}

  	public String truncateToByte(String toByte){
  		if(toByte==null){
  			return "";
  		}
  		else if(toByte!=null && toByte.length()>1){
  			return toByte.substring(0, 1);
  		}
  		else{
  			return toByte;
  		}
  	}

	public void resetBatchErrorLogs() {
		if(batchErrorLogs!=null){
			batchErrorLogs.clear();
		}
		batchErrorLogs = new ArrayList<BatchErrorLog>();;
	}

//	public void checksum() {
//		if(nextLine==null || nextLine.trim().length()==0){
//			addError(lineNumberTotal, null, "Bad or Missing End-of-File Marker", "Bad or Missing End-of-File Marker", "", "TR16");
//			return;
//		}
//		
//		String footer[] = nextLine.split(" +");
//		Long lines = Long.decode(footer[1].replaceAll("\\#", ""));
//		if (lines != getRows()) {
//			String s = "EOF Marker: " + lines + ", actual imported: " + getRows();
//			addError(lineNumberTotal, null, s, "Batch Import Line Count", nextLine, "TR14");
//		}
//		// checksum stateUseRate
//		Double total = Double.valueOf(footer[2].replaceAll("\\$", ""));
//		NumberFormat nf = NumberFormat.getNumberInstance();
//		nf.setMaximumFractionDigits(6);
//		if (!nf.format(stateUseRateTotal).equals(nf.format(total))) {
//			String s = "EOF Marker: " + nf.format(total)+ ", actual total: " + nf.format(stateUseRateTotal);
//			addError(lineNumberTotal, null, s, "Batch Distribution Amount Total", nextLine, "TR15");
//		}
//	}  raghu commented 
	
	public void checksum() {
		
		if (nextLine == null || nextLine.trim().length() == 0) {
			addIP6Error();
			return;
		}
		String footer[] = nextLine.split(" +");
		if(footer.length == 2  ) {
			//check for existance of # or $
			if(footer[1].contains("#")) {
				Long lines = Long.decode((footer[1].replaceAll("\\#", "")).trim());
				if (lines != getRows()) {
					addIP4Error(lines);
					addIP8Error();
				}
				else {
					addIP8Error();
				}
			}
			else if(footer[1].contains("$")) {
				// checksum specialRates
				BigDecimal total = parseBigDecimal((footer[1].replaceAll("\\$", "")).trim());
				NumberFormat nf = NumberFormat.getNumberInstance();
				nf.setMaximumFractionDigits(6);
				if (!nf.format(stateUseRateTotal).equals(nf.format(total))) {
					addIP5Error(nf,total);
					addIP7Error();
				}
				else {
					addIP7Error();
				}
			}
			return;
		}
		
		if(footer.length == 3) {
			if(footer[1].contains("#")) {
				Long lines = Long.decode((footer[1].replaceAll("\\#", "")).trim());
				if (lines != getRows()) {
					addIP4Error(lines);
				}
			}
			if(footer[2].contains("$")) {
				// checksum specialRates
				BigDecimal total = parseBigDecimal((footer[2].replaceAll("\\$", "")).trim());
				NumberFormat nf = NumberFormat.getNumberInstance();
				nf.setMaximumFractionDigits(6);
				if (!nf.format(stateUseRateTotal).equals(nf.format(total))) {
					addIP5Error(nf,total);
			
				}
			}
			return;
		}
		if(footer.length == 1) {
			addIP7Error();
			addIP8Error();
			
		}
	}
	
	public void addIP4Error(Long lines) {
		String s = "EOF Marker: " + lines + ", actual count: " + getRows();
		String errorCodeDesc = errorListCodes.get("IP4");
		if(errorCodeDesc != null && errorCodeDesc.length()>0) {
			addError(lineNumber, null, s, errorCodeDesc, nextLine, "IP4");
		}
		else {
			addError(lineNumber, null, s, "IP4", nextLine, "IP4");
		}
	}

	public void addIP5Error(NumberFormat nf, BigDecimal total) {
		String s = "EOF Marker: " + nf.format(total) + ", actual total: " + nf.format(stateUseRateTotal);
		String errorCodeDesc = errorListCodes.get("IP5");
		if(errorCodeDesc != null && errorCodeDesc.length()>0) {
			addError(lineNumber, null, s, errorCodeDesc, nextLine, "IP5");
		}
		else {
			addError(lineNumber, null, s, "IP5", nextLine, "IP5");
		}
	}

	public void addIP6Error() {
		String errorCodeDesc = errorListCodes.get("IP6");
		if(errorCodeDesc != null && errorCodeDesc.length()>0) {
			addError(lineNumber, null, errorCodeDesc, errorCodeDesc, "", "IP6");
		}
		else {
			addError(lineNumber, null, "IP6", "IP6", "", "IP6");
		}
	}

	public void addIP7Error() {
		String errorCodeDesc = errorListCodes.get("IP7");
		if(errorCodeDesc != null && errorCodeDesc.length()>0) {
			addError(lineNumber, null, errorCodeDesc, errorCodeDesc, nextLine,"IP7");
		}
		else {
			addError(lineNumber, null, "IP7", "IP7", nextLine,"IP7");
		}
	}

	public void addIP8Error() {
		String errorCodeDesc = errorListCodes.get("IP8");
		if(errorCodeDesc != null && errorCodeDesc.length()>0) {
			addError(lineNumber, null, errorCodeDesc, errorCodeDesc, nextLine, "IP8");
		}
		else {
			addError(lineNumber, null, "IP8", "IP8", nextLine,"IP8");
		}
	}
	
	public BigDecimal parseBigDecimal(String s) {
		if ((s == null) || (s.trim().length() == 0))
			return null;
		return new BigDecimal(s);
	}

	
		
	/**
	 * Parse a tab delimited tax rate jurisdiction file
	 */
//	private List<BCPJurisdictionTaxRate> parse() throws IOException, FileNotFoundException {
//		// since the header line is line 1, then first parsed line is 2
//		lineNumber = 2;
//		BufferedReader br = new BufferedReader(new FileReader(uploadFilePath));
//		logger.debug("Parsing: " + uploadFilePath);
//		String s;
//		while ((s = br.readLine()) != null) {
//			String tokens[] = s.split("\\t");
//			if (s.charAt(0) == '~') {
//				String footer[] = s.split(" +");
//				Long lines = Long.decode(footer[1].replaceAll("\\#", ""));
//				if (lines != lineNumber-2) {
//					addError(lineNumber, null, null, s, "TR14");
//					break;
//				}
//				// checksum row
//				Double total = Double.valueOf(footer[2].replaceAll("\\$", ""));
//				Double sum=0.0;
//				for (BCPJurisdictionTaxRate tr : list) { 
//					sum += tr.getStateSalesRate();
//				}
//				NumberFormat nf = NumberFormat.getNumberInstance();
//				nf.setMaximumFractionDigits(6);
//				if (! nf.format(sum).equals(nf.format(total))) {
//					addError(lineNumber, null, null, s, "TR15");
//				}
//				break;
//			}
//			BCPJurisdictionTaxRate r = new BCPJurisdictionTaxRate();
//			if (fileType.equals("Update") && (tokens.length != COLUMNS.length)) {
//				addError(lineNumber, null, null, s, "TR21");
//			} else if (fileType.equals("Full") && (tokens.length != COLUMNS.length-1)) {
//					addError(lineNumber, null, null, s, "TR21");
//			} else {
//				int i = 0;
//				try {
//					r.setGeocode(tokens[i++]);
//					r.setZip(tokens[i++]);
//					r.setState(tokens[i++]);
//					r.setCounty(tokens[i++]);
//					r.setCity(tokens[i++]);
//					r.setZipPlus4(tokens[i++]);
//					r.setInOut(tokens[i++]);
//					r.setStateSalesRate(parseDouble(tokens[i++]));
//					r.setStateUseRate(parseDouble(tokens[i++]));
//					r.setCountySalesRate(parseDouble(tokens[i++]));
//					r.setCountyUseRate(parseDouble(tokens[i++]));
//					r.setCountyLocalUseRate(parseDouble(tokens[i++]));
//					r.setCountySplitAmount(parseDouble(tokens[i++]));
//					r.setCountyMaxtaxAmount(parseDouble(tokens[i++]));
//					r.setCountySingleFlag(tokens[i++]);
//					r.setCountyDefaultFlag(tokens[i++]);
//					r.setCitySalesRate(parseDouble(tokens[i++]));
//					r.setCityUseRate(parseDouble(tokens[i++]));
//					r.setCitySplitAmount(parseDouble(tokens[i++]));
//					r.setCitySplitSalesRate(parseDouble(tokens[i++]));
//					r.setCitySplitUseRate(parseDouble(tokens[i++]));
//					r.setCitySingleFlag(tokens[i++]);
//					r.setCityDefaultFlag(tokens[i++]);
//					r.setEffectiveDate(sdf.parse(tokens[i++]));
//					if (fileType.equals("Update")) {
//						r.setUpdateFlag(tokens[i++]);
//					}
//					list.add(r);
//					lineNumber++;
//				} catch (Exception e) {
//					logger.error(e.getMessage(), e);
//					addError(lineNumber, i, COLUMNS[i - 1], s, "TR21");
//				}
//			}
//		}
//		br.close();
//		return list;
//	}
	
//  public void loadFileData(String filePath) {
//  	clear();
//  	int i = 0;
//  	if (filePath != null) {
//      try {
//    		bufferedReader = new BufferedReader(new FileReader(filePath));
//        logger.debug("Reading: " + new File(filePath).getAbsolutePath());
//    		String line = "";
//    		while ((line = bufferedReader.readLine()) != null) {
//    			if (i++ > 1000) {
//    				addError("File truncated to first 500 lines");
//    				break;
//    			}
//          fileData.add(line);
//        }
//      } catch (java.io.FileNotFoundException fe) {
//      	addError("File not found: " + filePath);
//      } catch (IOException fe) {
//        fe.printStackTrace();
//        addError("Error while loading file data: " + fe.getMessage());
//      } finally {
//        try {
//        	if (bufferedReader != null) bufferedReader.close();
//        } catch (IOException es) {
//          es.printStackTrace();
//          logger.debug("Exception while closing stream");
//        }
//      }
//  	}
//  }
  
  private void addError(String s) {
		errors.add(s);
  }
  private void addError(int lineNumber, String line, String code) {
  	addError(lineNumber, null, null, line, null, code);
  }
  private void addError(int lineNumber, Integer columnNumber, String column, String columnName, String line, String code) {
  	BatchErrorLog errorLog = new BatchErrorLog();
  	errorLog.setErrorDefCode(code);
  	errorLog.setColumnNo(columnNumber==null ? null : new Long(columnNumber));
  	errorLog.setImportColumnValue(column);
  	errorLog.setTransDtlColumnName(columnName);
  	errorLog.setImportRow(line);
  	errorLog.setRowNo(new Long(lineNumber));
		batchErrorLogs.add(errorLog);
		errorCountTotal++;
	}
	
	private Double parseDouble(String s) {
		if ((s == null) || (s.trim().length() == 0)) return null;
		return Double.parseDouble(s);
	}

	public boolean hasValidData() {
		return (fileType != null) && (fileType.equalsIgnoreCase("Update") || fileType.equalsIgnoreCase("Full"));
	}

	public List<String> getErrors() {
		return this.errors;
	}

  public Double getStateUseRateTotal() {
  	return stateUseRateTotal;
  }

	public int getRows() {
		return lineNumber - 2;
	}

	public int getLineNumber() {
		return this.lineNumber;
	}

	public String getUploadFileName() {
		return this.uploadFileName;
	}

	public String getUploadFilePath() {
		return this.uploadFilePath;
	}
	public String getFileType() {
		return this.fileType;
	}

	public Date getReleaseDate() {
		return this.releaseDate;
	}

	public String getReleaseVersion() {
		return this.releaseVersion;
	}
	
	public String getFileFormat() {
		return this.fileFormat;
	}

	public Long getTotalBytes() {
		return this.totalBytes;
	}

	public List<BatchErrorLog> getBatchErrorLogs() {
		return this.batchErrorLogs;
	}

	public long getLinesInFile() {
		return this.linesInFile;
	}

	public void setLinesWritten(int linesWritten) {
		this.linesWritten = linesWritten;
	}
	
	public int getErrorCountTotal() {
		return this.errorCountTotal;
	}
	// FIXED FOR ISSUE 0002743
	public boolean isAbort() {
		return abort;
	}

	public void setAbort(boolean abort) {
		this.abort = abort;
	}
	
	
}