package com.ncsts.view.util;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

import com.ncsts.domain.User;
import com.ncsts.domain.UserSecurity;

public class UserSecurityConverter implements Converter {

	public Object getAsObject(FacesContext context, UIComponent component,
			String value) {

		return new UserSecurity(value);
	}

	public String getAsString(FacesContext context, UIComponent component,
			Object value) {

		UserSecurity userSecurity = (UserSecurity) value;
		return userSecurity.getUserName();
	}

}
