package com.ncsts.view.util;

import org.richfaces.component.UITree;
import org.richfaces.component.state.TreeStateAdvisor;
import org.richfaces.model.TreeRowKey;

import com.ncsts.domain.EntityItem;
import com.ncsts.view.bean.EntityBackingBean;
import com.ncsts.view.util.EntityPostbackPhaseListener;

public class EntityTreeStateAdvisor implements TreeStateAdvisor {
	
	private EntityBackingBean entityBackingBean;
	
	public void setEntityBackingBean(EntityBackingBean entityBackingBean){
		this.entityBackingBean = entityBackingBean;
	}
	
	public EntityBackingBean getEntityBackingBean(){
		return this.entityBackingBean;
	}

    public Boolean adviseNodeOpened(UITree tree) {
        if (!EntityPostbackPhaseListener.isPostback()) {
            Object key = tree.getRowKey();
            TreeRowKey treeRowKey = (TreeRowKey) key;
            
            if (treeRowKey == null || treeRowKey.depth() <= 1) {
                return Boolean.TRUE;
            }
            
            if(entityBackingBean.getSelectedRowKey()!=null){
            	if(entityBackingBean.getSelectedRowKey().startsWith(key.toString())){
            		return Boolean.TRUE;
            	}
            }
            
            /*
            EntityItem entityItem = (EntityItem) tree.getRowData();
            
            EntityItem selectedEntityItem = entityBackingBean.getSelectedEntityItem();
            if(selectedEntityItem!=null){
            	if(entityItem.getEntityId().intValue()==selectedEntityItem.getEntityId().intValue() || 
            			entityItem.getEntityId().intValue()==selectedEntityItem.getParentEntityId().intValue()){	             	
            		return Boolean.TRUE;
               }
            }
            */
        }
        
        return null;
    }

    public Boolean adviseNodeSelected(UITree tree) {
        return null;
    }
    
    public EntityTreeStateAdvisor() {
	}
}


