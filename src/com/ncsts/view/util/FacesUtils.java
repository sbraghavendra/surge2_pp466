package com.ncsts.view.util;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.myfaces.custom.fileupload.UploadedFile;

import com.ncsts.domain.DriverReference;

/**
 * Utility class for JavaServer Faces.
 * 
 */
public class FacesUtils {
	/**
	 * Get servlet context.
	 * 
	 * @return the servlet context
	 */
	public static ServletContext getServletContext() {
		return (ServletContext) FacesContext.getCurrentInstance()
				.getExternalContext().getContext();
	}

	/**
	 * Get HttpServletRequest.
	 * 
	 * @return the HttpServletRequest
	 */

	public static HttpServletRequest getServletRequest() {
		return (HttpServletRequest) FacesContext.getCurrentInstance()
				.getExternalContext().getRequest();
	}

	/**
	 * Get HttpSession.
	 * 
	 * @return the HttpSession
	 */

	public static HttpSession getHttpSession() {
		return (HttpSession) FacesContext.getCurrentInstance()
				.getExternalContext().getSession(false);
	}

	
	public static String getTaxMatrixDriverValue(String value){
		
		if(value==null || value.trim().equals("") || value.trim().equals("-1")){
			return new String("*ALL");
		}else{
			return value;
		}
		
	}

	public static String getTaxMatrixDriverValueAsNull(String value){

		if(value==null || value.trim().equals("")){
			return null;
		}else{
			return value;
		}
		
	}
	
	/**
	 * This would convert List of Object[] to List of select item type
	 * 
	 * @param selectList
	 * @return
	 */
	public static List<SelectItem> getSelectList(List<DriverReference> selectList){
		List<SelectItem> listSelectItem=new ArrayList<SelectItem>();
		//SelectItem defaultSelect=new SelectItem("","-- Select --");
		//listSelectItem.add(defaultSelect);
		
		/*Iterator itr=selectList.iterator();
    	while(itr.hasNext())
    	{
    		String name=null;
    		String value=null;
    		Object[] object=(Object[])itr.next();
    		if(object!=null)
    		{
    			if(object[0] !=null && object[1] !=null)
    			{
    				name=(String)object[0];
    				value=(String)object[1];
    				SelectItem selectItem=new SelectItem(name,value);
    				listSelectItem.add(selectItem);
    			}
    			
    		}
    		
    	}*/
		for (DriverReference driver : selectList) {
			if(driver != null && driver.getDriverDesc()!=null && driver.getDriverValue()!=null){
				listSelectItem.add(new SelectItem(driver.getDriverValue(),driver.getDriverDesc()));
			}
		}
		return listSelectItem;
		
	}
	
	public static List<SelectItem> getSelectList(List<Object[]> selectList,String defaultLine){
		List<SelectItem> listSelectItem=new ArrayList<SelectItem>();
		SelectItem defaultSelect=new SelectItem("",defaultLine);
		listSelectItem.add(defaultSelect);
		
		Iterator<Object[]> itr =selectList.iterator();
    	while(itr.hasNext())
    	{
    		String name=null;
    		String value=null;
    		Object[] object= itr.next();
    		if(object!=null)
    		{
    			if(object[0] !=null && object[1] !=null)
    			{
    				name=(String)object[0];
    				value=(String)object[1];
    				SelectItem selectItem=new SelectItem(name,value);
    				listSelectItem.add(selectItem);
    			}
    			
    		}
    		
    	}
		
		return listSelectItem;
		
	}
	
	
	public static List<SelectItem> getMonthDays(String defaultLine){
		List<SelectItem> listSelectItem=new ArrayList<SelectItem>();
		SelectItem defaultSelect=new SelectItem("",defaultLine);
		listSelectItem.add(defaultSelect);
		for(int i=1;i<=31;i++){
			SelectItem selectItem=new SelectItem(Integer.toString(i),Integer.toString(i));
			listSelectItem.add(selectItem);
		}
		return listSelectItem;
	}
	public static List<SelectItem> getYears(int start,int end,String defaultLine){
		List<SelectItem> listSelectItem=new ArrayList<SelectItem>();
		SelectItem defaultSelect=new SelectItem("",defaultLine);
		listSelectItem.add(defaultSelect);
		for(int i=start;i<=end;i++){
			SelectItem selectItem=new SelectItem(Integer.toString(i),Integer.toString(i));
			listSelectItem.add(selectItem);
		}
		return listSelectItem;
	}
	
	
	public static List<SelectItem> getMonthNames(String defaultLine){
		List<SelectItem> listSelectItem=new ArrayList<SelectItem>();
		SelectItem defaultSelect=new SelectItem("",defaultLine);
		listSelectItem.add(defaultSelect);
		SelectItem selectItem=new SelectItem("01","Jan");
		listSelectItem.add(selectItem);
		selectItem=new SelectItem("02","Feb");
		listSelectItem.add(selectItem);
		
		selectItem=new SelectItem("03","Mar");
		listSelectItem.add(selectItem);
		selectItem=new SelectItem("04","Apr");
		listSelectItem.add(selectItem);
		
		selectItem=new SelectItem("05","May");
		listSelectItem.add(selectItem);
		
		selectItem=new SelectItem("06","Jun");
		listSelectItem.add(selectItem);
		
		selectItem=new SelectItem("07","Jul");
		listSelectItem.add(selectItem);
		
		selectItem=new SelectItem("08","Aug");
		listSelectItem.add(selectItem);
		selectItem=new SelectItem("09","Sep");
		listSelectItem.add(selectItem);
		
		selectItem=new SelectItem("10","Oct");
		listSelectItem.add(selectItem);
		
		selectItem=new SelectItem("11","Nov");
		listSelectItem.add(selectItem);
		
		selectItem=new SelectItem("12","Dec");
		listSelectItem.add(selectItem);
		
		return listSelectItem;
	}
	
/*	public static String getExtension(UploadedFile f) {
        String ext = null;
        String s = f.getName();
        int i = s.lastIndexOf('.');

        if (i > 0 &&  i < s.length() - 1) {
            ext = s.substring(i+1).toLowerCase();
        }
        return ext;
    }
*/    
	public static String getName(UploadedFile f) {
        String ext = null;
        String s = f.getName();
        int i = s.lastIndexOf("\\");

        if (i > 0 &&  i < s.length() - 1) {
            ext = s.substring(i+1).toLowerCase();
        }
        return ext;
    }
	
    public static String getTimeStampName(String prefix,String extension){
    	Date now = new Date();
    	String name = new String(prefix+now.getTime()+"."+extension);  
    	return name;
    }
    
    public static String getFileName(String full_path){
    	int whereSlash = full_path.lastIndexOf('\\');        
    	if ( whereSlash >0 ) 
    	{            
    		return full_path.substring(whereSlash+1,full_path.length() );
    	}
    	return "";
    }
    
	public static void refreshView() {
		FacesContext context = FacesContext.getCurrentInstance();
		refreshComponentTree(context.getViewRoot());
	}
	
	public static void refreshForm(String form) {
		FacesContext context = FacesContext.getCurrentInstance();
		refreshComponentTree(context.getViewRoot().findComponent(form));
	}
	
	public static void refreshComponentTree(UIComponent parent) {
		if (parent != null) {
			for (UIComponent component : parent.getChildren()) {
				refreshComponent(component);
				// Recurse
				if (component.getChildCount() > 0) {
					refreshComponentTree(component);
				}
			}
		}
	}

	public static void refreshComponent(UIComponent component) {
		if (component instanceof UIInput) {
			UIInput input = (UIInput) component;

			// do not clear so marked items
			if (input.getAttributes().containsKey("DONOTCLEAR") &&
					(Boolean)input.getAttributes().get("DONOTCLEAR")) return;
			
			input.setSubmittedValue(null);
			// The following is only needed for immediate input components 
			// but it won't do any harm in other situations..
			input.setValue(null);
			input.setLocalValueSet(false);
		}
	}
}

