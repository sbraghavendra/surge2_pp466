package com.ncsts.view.handler;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.el.ELException;
import javax.faces.FacesException;
import javax.faces.application.ViewHandler;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.ncsts.domain.Error;
import com.sun.facelets.FaceletViewHandler;

public class FaceletRedirectionViewHandler extends FaceletViewHandler{

	public FaceletRedirectionViewHandler(ViewHandler parent) {
		super(parent);
		// TODO Auto-generated constructor stub
	}

	@Override
    protected void handleRenderException(FacesContext context, Exception ex) throws IOException, ELException, FacesException {
        try {
			HttpSession session = (HttpSession) context.getExternalContext().getSession(false);
			HttpServletRequest req = (HttpServletRequest) context.getExternalContext().getRequest();
			String uri = req.getRequestURI().toString();
			String appName = req.getContextPath();
            if (context.getViewRoot().getViewId().equals(appName + "/error.jsp")) {
                /*
                 * This is to protect from infinite redirects if the error
                 * page itself is updated in the future and has an error
                 */
                System.out.println("Redirected to ourselves, there must be a problem with the " + appName + "/error.jsp");
                ex.printStackTrace();
                return;
            }
            
			Map<String, Object> errorMap = new HashMap<String, Object>();
			errorMap.put("javax.servlet.error.request_uri", uri);
			errorMap.put("javax.servlet.error.exception", ex);
			errorMap.put("javax.servlet.error.status_code", "UI Rendering Exception");
			errorMap.put("javax.servlet.error.servlet_name", "FaceletRedirectionViewHandler");
			Error e = new Error();
			e.setErrorMap(errorMap);	
			session.setAttribute("pinpoint.error", e);

			String getUrl =  appName + "/error.jsf;jsessionid=" + session.getId();
			context.getExternalContext().redirect(getUrl);
			context.responseComplete();
			
			if(ex!=null){
				ex.printStackTrace();
			}
        }
        catch (Exception ioe) {
            System.out.println("FaceletRedirectionViewHandler: Could not process redirect to handle application error");
            ioe.printStackTrace();;
        }
    }
}
