package com.ncsts.common;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;

public class FileReaderUtil {
	
	private static Logger logger = LoggerFactory.getInstance().getLogger(FileReaderUtil.class);

	//	this will have header info
	HashMap<Integer,String>  mapHeader=new HashMap<Integer,String>();
	
	// this will have the data to be persisted.
	//HashMap<Integer,TransactionDetailDTO> mapBCPData= new HashMap<Integer,TransactionDetailDTO>();
	List<HashMap<Integer,String>> listBCPData= new ArrayList<HashMap<Integer,String>>();

	/**
	 * 
	 * @param fileName
	 * @param validDatabase
	 */
	public void readFile(String fileName,boolean validDatabase){
		
	     try {
				
	    	 	File file=new File(fileName);
				String thisLine;
		        String[] fullText = new String[1000]; //number of possible lines in your file
		        int counter = 0;
		        //String fName = "<your *.csv file>";
		        FileInputStream fis = new FileInputStream(file);
		        BufferedReader dis = new BufferedReader(new InputStreamReader(fis));

		        // DataInputStream dis = new DataInputStream(fis);
		        // TransactionDetailDTO  transactionDetail;

		        
		            while ((thisLine = dis.readLine()) != null) {
		            //logger.debug("---------------------------------\nBegin Parsing");
		            
		            int sPos = 0;
		            int ePos = 1;
		            int tDelim = 0;
		            String fieldValue = "";
		            int colCount = 0;
		            int collCount = 0;
		            boolean fMe = false;
		            boolean finalCol = false;
		            //transactionDetail=new TransactionDetailDTO();
		            HashMap<Integer,String> mapBCPData=new HashMap<Integer,String>();
		            
		                while ( ePos < thisLine.length()) {
		                sPos = thisLine.indexOf("|", ePos);
		                tDelim = thisLine.indexOf("|\"", ePos);
		                


		                    if (tDelim == ePos) {
		                    fMe = true;
		                    collCount = ePos + 1;


		                        while (collCount <= thisLine.indexOf("\"|")) {
		                        collCount += 1;
		                    }

		                    colCount += 1;
		                    sPos = collCount;
		                }

		                
		                if (colCount == 150) { //Get Max count of columns
		                finalCol = true;
		                collCount = ePos + 1;

		                    while (collCount <= thisLine.lastIndexOf("\"")) {
		                    collCount += 1;
		                }

		                colCount += 1;
		                sPos = collCount;
		            }
		                if (sPos == -1) {
		                	fieldValue = thisLine.substring(ePos,thisLine.length()); 
			                logger.debug(fieldValue);
			                break;
		                }
		                else if(ePos == 1) {

			                fieldValue = thisLine.substring(ePos-1,sPos);
			                //logger.debug(fieldValue);
		                }
		                else{
		                    if (fMe) {
		                    	fieldValue = thisLine.substring(ePos + 1, sPos);
		                    	fMe = false;
		                    }else if (finalCol) {

		                    	fieldValue = thisLine.substring(ePos, sPos);
		                    finalCol = false;
		                    }else{

		                    	fieldValue = thisLine.substring(ePos, sPos);
		                }

		                //logger.debug(fieldValue);
		            }

			        if(counter==0)//header info
			        {
			        	mapHeader.put(colCount,fieldValue);
			        }
			        else{//start filling the data
			        	mapBCPData.put(colCount, fieldValue);
			        }
		            colCount += 1;
		            ePos = sPos + 1;			        

		        }
                //logger.debug("mapHeader " +mapHeader.size());
                if(counter==0)
                setMapHeader(mapHeader);
		        //if it is not valid data base , quit the process
		        if(validDatabase)        
		        	counter++;
		        else
		        	break;
		        fullText[counter] = thisLine;
		        
		        //logger.debug("\nEnd Parsing\n---------------------------------\n\n");
	        	listBCPData.add(mapBCPData);
		    }
	              setListBCPData(listBCPData);
	              logger.debug(" listBCPData ***** "+listBCPData.size());	              
	     }catch(Exception e){
	    	 e.printStackTrace();
	     }
		
	}

	
	public List<HashMap<Integer,String>> getListBCPData() {
		return listBCPData;
	}

	public void setListBCPData(List<HashMap<Integer,String>>  listBCPData) {
		this.listBCPData = listBCPData;
	}

	public HashMap<Integer,String> getMapHeader() {
		return mapHeader;
	}

	public void setMapHeader(HashMap<Integer,String> mapHeader) {
		this.mapHeader = mapHeader;
	}
	
	public static void main(String args[]){
		String fileName="C:/Users/Documents/LincolnPeak/LPP_Test_02.11.2008_02.17.2008.txt";
		FileReaderUtil file=new FileReaderUtil();
		file.readFile(fileName,true);
			
	}
	

}
