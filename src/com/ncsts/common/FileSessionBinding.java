package com.ncsts.common;

import java.io.File;

import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;

public class FileSessionBinding implements HttpSessionBindingListener {
	
	private String downloadDirectory = "";

	public FileSessionBinding() {
		// TODO Auto-generated constructor stub
	}
	
	public FileSessionBinding(String downloadDirectory) {
		// TODO Auto-generated constructor stub
		this.downloadDirectory = downloadDirectory;
		
		boolean exists = (new File(downloadDirectory)).exists();
		if (exists) {
		    // File or directory exists
		} else {
		    // File or directory does not exist
			// Create a directory; all non-existent ancestor directories are
			// automatically created
			boolean success = (new File(downloadDirectory)).mkdirs();
			if (!success) {
			    // Directory creation failed
			}
		}
	}

	@Override
	public void valueBound(HttpSessionBindingEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void valueUnbound(HttpSessionBindingEvent arg0) {
		// TODO Auto-generated method stub
		boolean exists = (new File(downloadDirectory)).exists();
		if (exists) {
		    // File or directory exists
			boolean success = deleteDir(new File(downloadDirectory));
		} else {
		    // File or directory does not exist
		}

	}
	
	public boolean deleteDir(File dir) {
	    if (dir.isDirectory()) {
	        String[] children = dir.list();
	        for (int i=0; i<children.length; i++) {
	            boolean success = deleteDir(new File(dir, children[i]));
	            if (!success) {
	                return false;
	            }
	        }
	    }

	    // The directory is now empty so delete it
	    return dir.delete();
	}
}
