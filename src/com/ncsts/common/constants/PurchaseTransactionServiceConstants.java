package com.ncsts.common.constants;


import com.ncsts.ws.PinPointWebServiceConstants;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;

public class PurchaseTransactionServiceConstants {
    public static final String ENTITY_CODE_ALL = "*ALL";

    public static final String PROCESSING_ERROR_PS0 = "P20";
    public static final String PROCESSING_ERROR_ENTITY_NOT_FOUND_A = "P10A";
    public static final String PROCESSING_ERROR_PARENT_ENTITY_NOT_FOUND = "P10B";
    public static final String PROCESSING_ERROR_NO_LOCATION_DRIVERS = "P2";
    public static final String PROCESSING_ERROR_NO_TAX_DRIVERS = "P3";
    public static final String BAD_RETURN_ON_TRANSACTION_DETAIL_WRITE = "P7";
    public static final String PROCESSING_ERROR_P21 = "P21";
    public static final String PROCESSING_ERROR_P22 = "P22";
    public static final String PROCESSING_ERROR_P23 = "P23";
    public static final String PROCESSING_ERROR_P24 = "P24";
    public static final String PROCESSING_ERROR_P30 = "P30";
    public static final String PROCESSING_ERROR_P31 = "P31";
    public static final String PROCESSING_ERROR_MISSING_OR_INVALID_OPTION = "P25";
    public static final String PROCESSING_ERROR_P26_TAX_MATRIX_NOT_FOUND = "P26";
    public static final String PROCESSING_ERROR_COUNTRY_TAX_RATE_NOT_FOUND = "P27A";
    public static final String PROCESSING_ERROR_STATE_TAX_RATE_NOT_FOUND = "P27B";
    public static final String PROCESSING_ERROR_COUNTY_TAX_RATE_NOT_FOUND = "P27C";
    public static final String PROCESSING_ERROR_CITY_TAX_RATE_NOT_FOUND = "P27D";
    public static final String PROCESSING_ERROR_STJ1_TAX_RATE_NOT_FOUND = "P27E1";
    public static final String PROCESSING_ERROR_STJ2_TAX_RATE_NOT_FOUND = "P27E2";
    public static final String PROCESSING_ERROR_STJ3_TAX_RATE_NOT_FOUND = "P27E3";
    public static final String PROCESSING_ERROR_STJ4_TAX_RATE_NOT_FOUND = "P27E4";
    public static final String PROCESSING_ERROR_STJ5_TAX_RATE_NOT_FOUND = "P27E5";
    public static final String PROCESSING_ERROR_STJ6_TAX_RATE_NOT_FOUND = "P27E6";
    public static final String PROCESSING_ERROR_STJ7_TAX_RATE_NOT_FOUND = "P27E7";
    public static final String PROCESSING_ERROR_STJ8_TAX_RATE_NOT_FOUND = "P27E8";
    public static final String PROCESSING_ERROR_STJ9_TAX_RATE_NOT_FOUND = "P27E9";
    public static final String PROCESSING_ERROR_STJ10_TAX_RATE_NOT_FOUND = "P27E10";
    public static final String PROCESSING_ERROR_INVALID_COUNTRY_SITUS_CODE = "P28A";
    public static final String PROCESSING_ERROR_INVALID_STATE_SITUS_CODE = "P28B";
    public static final String PROCESSING_ERROR_INVALID_COUNTY_SITUS_CODE = "P28C";
    public static final String PROCESSING_ERROR_INVALID_CITY_SITUS_CODE = "P28D";
    public static final String PROCESSING_ERROR_INVALID_STJ1_SITUS_CODE = "P28E1";
    public static final String PROCESSING_ERROR_INVALID_STJ2_SITUS_CODE = "P28E2";
    public static final String PROCESSING_ERROR_INVALID_STJ3_SITUS_CODE = "P28E3";
    public static final String PROCESSING_ERROR_INVALID_STJ4_SITUS_CODE = "P28E4";
    public static final String PROCESSING_ERROR_INVALID_STJ5_SITUS_CODE = "P28E5";
    public static final String PROCESSING_ERROR_INVALID_STJ6_SITUS_CODE = "P28E6";
    public static final String PROCESSING_ERROR_INVALID_STJ7_SITUS_CODE = "P28E7";
    public static final String PROCESSING_ERROR_INVALID_STJ8_SITUS_CODE = "P28E8";
    public static final String PROCESSING_ERROR_INVALID_STJ9_SITUS_CODE = "P28E9";
    public static final String PROCESSING_ERROR_INVALID_STJ10_SITUS_CODE = "P28E10";
    public static final String PROCESSING_ERROR_PROCESS_BATCH_NO = "P29E1";


    public static final String TRANS_ERROR_SUSPENDFLAG_NOT_SET = "P41";
    public static final String TRANS_ERROR_TRANSACTIONIND_HELD_NOT_SET = "P42";
    public static final String TRANS_ERROR_PURCHTRANSID_NOT_FOUND = "P43";


    public static final String ENTITY_DEFAULT_FRTINCITEM = "FRTINCITEM";
    public static final String ENTITY_DEFAULT_DISINCITEM = "DISINCITEM";
    public static final String LOCATIONPREFERENCE_RATEPOINT = "ratepoint";
    public static final String LOCATIONPREFERENCE_LOCATIONMATRIX = "locnmatrix";
    public static final String DRIVER_NAMES_TYPE_LOCATION = "L";
    public static final String DRIVER_NAMES_TYPE_TAX = "T";
    public static final String DRIVER_NAMES_TYPE_GOODSNSERVICES = "GSB";
    public static final String MODULE_CODE_SALE = "BILLSALE";
    public static final String MODULE_CODE_PURCHASE = "PURCHUSE";
    public static final String TRANSACTION_TYPE_CODE_PURCHASE = "PURCHASE";
    public static final String TRANSACTION_TYPE_CODE_BILL = "SALE";
    public static final String TRANS_IND_SUSPENDED = "S";
    public static final String TRANS_IND_PROCESSED = "P";
    public static final String TRANS_IND_ERROR = "E";
    public static final String TRANS_IND_HELD = "H";
    public static final String TRANS_IND_ORIGINAL = "O";
    public static final String TRANS_IND_PUSH_ALL_TAX = "P1";
    public static final String TRANS_IND_PUSH_PREFERENCE_TAX = "P2";
    public static final String TRANS_IND_PUSH_HARD_TAX = "P3";
    public static final String TRANS_SUSPEND_IND_LOCATION = "L";
    public static final String TRANS_SUSPEND_IND_GSB = "T";
    public static final String TRANS_SUSPEND_IND_TAXCODE_DTL = "D";
    public static final String TRANS_SUSPEND_IND_RATE = "R";
    public static final String TRANS_SUSPEND_IND_UNKNOWN = "?";
    public static final String MANUAL_IND_OVERRIDE_ENTERED_TAXCODE = "OETC";
    public static final String MANUAL_IND_OVERRIDE_ENTERED_JURISDICTION = "OEJ";
    public static final String MANUAL_IND_OVERRIDE_ENTERED_LOCATION = "OELM";
    public static final String MANUAL_IND_OVERRIDE_ENTERED_TAXMATRIX = "OETM";
    public static final String MANUAL_IND_OVERRIDE_ENTERED_AUTOPROC =  "AP";
    public static final String SITUS_TYPE_CODE_SHIPTO = "ST";
    public static final String TAXTYPE_CODE_SALE = "S";
    public static final String TAXTYPE_CODE_VENDOR_USE = "V";
    public static final String TAXTYPE_CODE_USE = "U";


    public static final String TAXTYPE_CODE_GST_INPUT = "GI";
    public static final String TAXTYPE_CODE_GST_OUTPUT = "GO";
    public static final String TAXTYPE_CODE_HST_INPUT = "HI";
    public static final String TAXTYPE_CODE_HST_OUTPUT = "HO";
    public static final String TAXTYPE_CODE_PST_INPUT = "PI";
    public static final String TAXTYPE_CODE_PST_OUTPUT = "PO";
    public static final String TAXTYPE_CODE_QST_INPUT = "QI";
    public static final String TAXTYPE_CODE_QST_OUTPUT = "QO";

    public static final String GOOD_SERVICE_TYPE_CODE_GOOD = "GOOD";
    public static final String GOOD_SERVICE_TYPE_CODE_SERVICE = "SERVICE";
    public static final String TAXCODE_TYPE_CODE_TAXABLE = "T";
    public static final String TAXCODE_TYPE_CODE_EXEMPT = "E";
    public static final String TAXCODE_TYPE_CODE_DISTRIBUTE = "D";
    public static final String TRANSACTION_STAGE_PURCHASE_PROCESSED = "P";
    public static final String TRANSACTION_STAGE_BILL_PROCESSED = "S";

    public enum ProcessStep {billTransactionProcess, purchaseTransactionProcess, init, transEntity, transLocation,
        transGoodSvc, transTaxCodeDtl, transTaxRates, transCalcAllTaxes, transStatus, transSitus, transVendorNexus, transEntityNexus}

    public enum RatepointCallType {latlon, street, generic}

    @XmlType(namespace = PinPointWebServiceConstants.MESSAGES_NAMESPACE)
    @XmlEnum(String.class)
    public enum SuspendReason {SUSPEND_LOCN, SUSPEND_GS, SUSPEND_TAXCODE_DTL, SUSPEND_RATE}
    
    //raghu
    public enum LogSourceEnum {BillTransactionProcessBatch, TestingTool, BillTransactionProcess, EvaluateForAccrual, InvoiceVerification,
    	LoadAndReprocess, applyLocationMatrixAction, applyTaxMatrixAction, processTransaction,PushPreference, PushProrate, PushSpecific, 
    	TaxEstimate, LoadAndReverse, Reverse, TransactionProcess, PurchaseTransactionTaxEstimate, normalProcess, processReverse, 
    	getPurchaseTransactionProcessBatch, getPurchaseTransactionReprocess, processSuspendedTransactions,
    	getPurchaseTransactionEvaluateForAccrual, getPurchaseTransactionInvoiceVerification, getPurchaseTransactionLoadAndReprocess};

    public enum LocationType {
        BILLTO("BT"),	SHIPTO("ST"), TITLETRANSFER("TT"), FIRSTUSE("UL"), SHIPFROM("SF"), ORDERORIGIN("OO"), ORDERACCEPT("OA");
        private final String shortCode;
        LocationType(String shortCode) {
            this.shortCode = shortCode;
        }
        public String shortCode() {
            return shortCode;
        }
    }
    
    public enum LocationTypeCalcFlags {
        BT("BT"),	ST("ST"), TT("TT"), UL("UL"), SF("SF"), OO("OO"), OA("OA");
        private final String shortCode;
        LocationTypeCalcFlags(String shortCode) {
            this.shortCode = shortCode;
        }
        public String shortCode() {
            return shortCode;
        }
    }


    public static enum JurisdictionLevel {
        COUNTRY, STATE, COUNTY, CITY, STJ1, STJ2, STJ3, STJ4, STJ5, STJ6, STJ7, STJ8, STJ9, STJ10
    }
    
    @XmlType(namespace = PinPointWebServiceConstants.MESSAGES_NAMESPACE)
    @XmlEnum(String.class)
    public enum ModuleCode {PURCHUSE, BILLSALE
    }
}
