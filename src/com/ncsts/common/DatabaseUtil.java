package com.ncsts.common;

import java.lang.reflect.InvocationTargetException;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;

import javax.persistence.EntityManager;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.springframework.jdbc.datasource.ConnectionHandle;
import org.springframework.orm.jpa.JpaDialect;
import org.springframework.orm.jpa.JpaTemplate;
import java.text.ParseException;

public class DatabaseUtil {
	private static final Logger logger = LoggerFactory.getInstance().getLogger(DatabaseUtil.class);

	public static final String ORACLE = "oracle";
	public static final String POSTGRESQL = "postgresql";
	public static final String MSSQL = "microsoft sql server";
	public static final String MSSQL_SHORT = "sqlserver";

	public static HashMap<String, Integer> SQLSERVERDATEMAPPINGS = new HashMap<String, Integer>();

	static {
		//http://msdn.microsoft.com/en-us/library/ms187928.aspx
		SQLSERVERDATEMAPPINGS.put("mon dd yyyy hh:mi", 0);
		SQLSERVERDATEMAPPINGS.put("mon dd yyyy hh:miam", 0);
		SQLSERVERDATEMAPPINGS.put("mon dd yyyy hh:mipm", 0);

		SQLSERVERDATEMAPPINGS.put("mm/dd/yy", 1);
		SQLSERVERDATEMAPPINGS.put("mm/dd/yyyy", 101);

		SQLSERVERDATEMAPPINGS.put("yy.mm.dd", 2);
		SQLSERVERDATEMAPPINGS.put("yyyy.mm.dd", 102);

		SQLSERVERDATEMAPPINGS.put("dd/mm/yy", 3);
		SQLSERVERDATEMAPPINGS.put("dd/mm/yyyy", 103);

		SQLSERVERDATEMAPPINGS.put("dd.mm.yy", 4);
		SQLSERVERDATEMAPPINGS.put("dd.mm.yyyy", 104);

		SQLSERVERDATEMAPPINGS.put("dd-mm-yy", 5);
		SQLSERVERDATEMAPPINGS.put("dd-mm-yyyy", 105);

		SQLSERVERDATEMAPPINGS.put("dd mon yy", 6);
		SQLSERVERDATEMAPPINGS.put("dd mon yyyy", 106);

		SQLSERVERDATEMAPPINGS.put("Mon dd, yy", 7);
		SQLSERVERDATEMAPPINGS.put("Mon dd, yyyy", 107);

		SQLSERVERDATEMAPPINGS.put("hh:mi:ss", 8);

		SQLSERVERDATEMAPPINGS.put("mon dd yyyy hh:mi:ss:mmm", 9);
		SQLSERVERDATEMAPPINGS.put("mon dd yyyy hh:mi:ss:mmmam", 9);
		SQLSERVERDATEMAPPINGS.put("mon dd yyyy hh:mi:ss:mmmpm", 9);

		SQLSERVERDATEMAPPINGS.put("mm-dd-yy", 10);
		SQLSERVERDATEMAPPINGS.put("mm-dd-yyyy", 110);

		SQLSERVERDATEMAPPINGS.put("yy/mm/dd", 11);
		SQLSERVERDATEMAPPINGS.put("yyyy/mm/dd", 111);

		SQLSERVERDATEMAPPINGS.put("yymmdd", 12);
		SQLSERVERDATEMAPPINGS.put("yyyymmdd", 112);

		SQLSERVERDATEMAPPINGS.put("dd mon yyyy hh:mi:ss:mmm", 13);

		SQLSERVERDATEMAPPINGS.put("hh:mi:ss:mmm", 14);
		SQLSERVERDATEMAPPINGS.put("yyyy-mm-dd hh:mi:ss", 20);
		SQLSERVERDATEMAPPINGS.put("yyyy-mm-dd hh:mi:ss.mmm", 21);
		SQLSERVERDATEMAPPINGS.put("yyyy-mm-ddThh:mi:ss.mmm", 126);
		SQLSERVERDATEMAPPINGS.put("yyyy-mm-ddThh:mi:ss.mmmz", 127);
	}


	public static String getDatabaseProductName(Connection connection) {
		String name = null;
		try {
			name = connection.getMetaData().getDatabaseProductName();
		}
		catch(SQLException e) {
			logger.error(e.getMessage(), e);
		}
		
		return name;
	}
	
	public static String getDatabaseProductName(Statement stmt) {
		String name = null;
		try {
			name = stmt.getConnection().getMetaData().getDatabaseProductName();
		}
		catch(SQLException e) {
			logger.error(e.getMessage(), e);
		}
		
		return name;
	}
	
	public static String getDatabaseProductName(JpaTemplate template, EntityManager entityManager) {
		String name = null;
		try {
			JpaDialect dialect = template.getJpaDialect();
			ConnectionHandle handle = dialect.getJdbcConnection(entityManager, true);
			name = handle.getConnection().getMetaData().getDatabaseProductName();
			dialect.releaseJdbcConnection(handle, entityManager);
		}
		catch(SQLException e) {
			logger.error(e.getMessage(), e);
		}
		
		return name;
	}
	
	public static final String getSequenceQuery(String database, String sequence) {
		return getSequenceQuery(database, sequence, null);
	}
	
	public static final String getSequenceQuery(String database, String sequence, String alias) {
		String sql = null;
		if(database != null) {
			if(ORACLE.equalsIgnoreCase(database)) {
				sql = (alias == null || alias.length() == 0) ? "select " + sequence + ".nextval from dual" :
					"select " + sequence + ".nextval " + alias + " from dual";
			}
			else if(POSTGRESQL.equalsIgnoreCase(database)) {
				sql = (alias == null || alias.length() == 0) ? "select nextval('" + sequence + "')" :
					"select nextval('" + sequence + "') AS " + alias;
			}
		}
			
		return sql;
	}
	
	public static final String getCalculateTaxesQuery(String database) {
		String sql = null;
		if(database != null) {
			if(ORACLE.equalsIgnoreCase(database)) {
				sql = "{ ? = call f_calc_taxes(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}"; 
			}
			else if(POSTGRESQL.equalsIgnoreCase(database)) {
				sql = "{ ? = call f_calc_taxes(?::NUMERIC,?::DATE,?::VARCHAR,?::NUMERIC,?::VARCHAR,?::VARCHAR,?::CHAR,?::CHAR,?::CHAR,?::CHAR,?::CHAR,?::CHAR,?::NUMERIC,?::NUMERIC,?::VARCHAR,?::VARCHAR,?::VARCHAR,?::VARCHAR)}";
			}
		}
		
		return sql;
	}

	public static String getToDateExpression(String databaseProductName, String value, String format) throws ParseException {
		String result = null;
		if (format!=null) {
			format = format.replaceAll("\'", "");
		}
		if (MSSQL.equalsIgnoreCase(databaseProductName) || MSSQL_SHORT.equalsIgnoreCase(databaseProductName)) {
			Integer convCode = SQLSERVERDATEMAPPINGS.get(format.toLowerCase());
			if (convCode==null) throw new ParseException("Cant find matching MSSQL format mapping. Format string: : "+format, 0);
			result = " CONVERT(datetime, "+value+", "+convCode+") ";

		} else if (ORACLE.equalsIgnoreCase(databaseProductName)) {
			result = " TO_DATE("+value+",'"+format+"') ";
		} else throw new ParseException("Cant find matching DatabaseProductName: "+databaseProductName, 0);
		return result;
	}

	public static String getConcatSign(String dbProduct) {
		if (MSSQL.equalsIgnoreCase(dbProduct)) {
			return "+";
		} else
			return "||";
	}

	public static String getProductNameViaSession (Session session) {
		String result = null;
		String dialect = getDialectViaSession(session);
		if (dialect==null) return null;
		if (dialect.toLowerCase().contains(DatabaseUtil.MSSQL_SHORT)) {
			result = DatabaseUtil.MSSQL;
		} else if (dialect.toLowerCase().contains(DatabaseUtil.ORACLE))
			result = DatabaseUtil.ORACLE;
		return result;
	}

	public static String getDialectViaSession(Session session) {
		String result = null;
		try {
			Object dialect = org.apache.commons.beanutils.PropertyUtils.getProperty(
					session.getSessionFactory(), "dialect");
			if (dialect!=null) result = dialect.toString();
			//System.out.println(dialect);
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		}
		return result;
	}

}
