package com.ncsts.common;

import java.lang.reflect.Method;

import org.apache.log4j.Logger;
import org.springframework.aop.AfterReturningAdvice;
import org.springframework.aop.MethodBeforeAdvice;

/**
 * 
 * @author Paul Govindan
 *
 */

public class RyanCoLoggingInterceptor implements MethodBeforeAdvice,
		AfterReturningAdvice {

	private static Logger log = LoggerFactory.getInstance().getLogger("loggingInterceptor");

	public void before(Method arg0, Object[] arg1, Object arg2)
			throws Throwable {
		log.debug("");
		log.debug("LoggingInterceptor - Beginning method: Method name = " + arg0.getDeclaringClass().getName() +
				" : " + arg0.getName());
		for (int i =0; i< arg1.length; i++) {
			log.debug("LoggingInterceptor - method argument" +i+ ": = " + arg1[i]);			
		}
	}

	public void afterReturning(Object arg0, Method arg1, Object[] arg2,
			Object arg3) throws Throwable {
		log.debug("LoggingInterceptor - Ending method: return value = " + arg0);		
		log.debug("LoggingInterceptor - Ending method: Method name = " 
				+ arg1.getDeclaringClass().getName() +
				" : " + arg1.getName()
		);
		log.debug("");
	}

}
