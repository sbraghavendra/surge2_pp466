package com.ncsts.common;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.StringReader;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import javax.faces.context.FacesContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;

public class Util {
	
	private static Logger logger = LoggerFactory.getInstance().getLogger(Util.class);
	
	// Convert string to proper case
	public static String makeProper(String theString) {
		StringReader in = new StringReader(theString.toLowerCase());
		boolean precededBySpace = true;
		StringBuffer properCase = new StringBuffer();    
		try {
			while(true) {      
				int i = in.read();
				if (i == -1) break;
				char c = (char)i;
				if (c == '_') { c = ' '; }
				if (c == ' ' || c == '"' || c == '(' || c == '.' || c == '/' || c == '\\' || c == ',') {
					properCase.append(c);
					precededBySpace = true;
				} else {
					if (precededBySpace) { 
						properCase.append(Character.toUpperCase(c));
					} else { 
						properCase.append(c); 
					}
					precededBySpace = false;
				}
			}
		} catch (IOException e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}
 
		return properCase.toString();    
	}

	// Turn property name into label by adding spaces
	public static String propertyToLabel(String theString) {
		StringReader in = new StringReader(theString);
		StringBuffer label = new StringBuffer();
		boolean inNumber = false;
		try {
			while(true) {      
				int i = in.read();
				if (i == -1) break;
				char c = (char) i;
				
				if (Character.isUpperCase(c)) {
					// word boundary, add a space
					label.append(' ');
				} else if (Character.isDigit(c)) {
					if (!inNumber) {
						// transition into number, add a space 
						label.append(' ');
						inNumber = true;
					}
				} else if (inNumber) {
					// transition out of number, add a space
					label.append(' ');
				}
				
				if (label.length() == 0) {
					label.append(Character.toUpperCase(c));
				} else {
					label.append(c);
				}
			}
		} catch (IOException e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}
 
		return label.toString();    
	}
	
	// Upper case the first character only
	public static String upperCaseFirst(String theString) {
		return (theString == null)? null:theString.substring(0,1).toUpperCase() + theString.substring(1);
	}
	
	// Lower case the first character only
	public static String lowerCaseFirst(String theString) {
		return (theString == null)? null:theString.substring(0,1).toLowerCase() + theString.substring(1);
	}
	
	// Change a DB column name into a Java property name
	public static String columnToProperty(String column) {
		return (column == null)? null:lowerCaseFirst(makeProper(column.replace("_", " ")).replace(" ", ""));
	}

	// get/set by reflection
	public static void setProperty(Object o, String property, Object value, Class<?> clazz) {
		String setterName = "set" + upperCaseFirst(property);

		try {
			Method setterMethod = o.getClass().getMethod(
												setterName, 
												new Class[] { clazz } );
			setterMethod.invoke(o, new Object[] { value } );	
		} catch (InvocationTargetException e) {
			Exception t = (Exception) e.getTargetException();
			logger.error(t.getMessage());
			throw new RuntimeException("Method invocation error: " + 
					o.getClass().getSimpleName() + "." + setterName + 
					" - " + t.getMessage());
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new RuntimeException("Method invocation error: " + 
					o.getClass().getSimpleName() + "." + setterName + 
					" - " + e.getMessage());
		}
	}

	public static Object getProperty(Object o, String property) {
		String getterName = "get" + upperCaseFirst(property);
		Object value = null;
		try {
			Method getterMethod = o.getClass().getMethod(
												getterName, 
												new Class[] { } );
			value = getterMethod.invoke(o, new Object[] { } );
		} catch (InvocationTargetException e) {
			Exception t = (Exception) e.getTargetException();
			logger.error(t.getMessage());
			throw new RuntimeException("Method invocation error: " + 
					o.getClass().getSimpleName() + "." + getterName + 
					" - " + t.getMessage());
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new RuntimeException("Method invocation error: " + 
					o.getClass().getSimpleName() + "." + getterName + 
					" - " + e.getMessage());
		}
		return value;
	}
	
	public static String getPropertyAsString(Object o, String property) {
		Object obj = getProperty(o, property);
		if ((obj == null) || (obj instanceof String)) {
			return (String) obj;
		}
		return obj.toString();
	}

	public static Map<String,Object> getPropertyMap(Object o) {
		Map<String,Object> result = new HashMap<String,Object>();
		for (Method m : o.getClass().getMethods()) {
			String name = m.getName();
			// Look for properties only
			if (name.startsWith("get") && 
					(m.getParameterTypes().length == 0) &&
					!name.equals("getPropertyMap")) {
				String field = Util.lowerCaseFirst(name.substring(3));
				try {
					result.put(field, m.invoke(o, new Object[] { }));
				} catch (InvocationTargetException e) {
					Exception t = (Exception) e.getTargetException();
					logger.error(t.getMessage());
					throw new RuntimeException("Method invocation error: " + 
							o.getClass().getSimpleName() + "." + name + 
							" - " + t.getMessage());
				} catch (Exception e) {
					logger.error(e.getMessage());
					throw new RuntimeException("Method invocation error: " + 
							o.getClass().getSimpleName() + "." + name + 
							" - " + e.getMessage());
				}
			}
		}
		return result;
	}

	public static void setPropertyMap(Object o, Map<String,Object> values) {
		for (Method m : o.getClass().getMethods()) {
			String name = m.getName();
			// Look for properties only
			if (name.startsWith("set") && 
					(m.getParameterTypes().length == 1) &&
					!name.equals("setPropertyMap")) {
				String field = Util.lowerCaseFirst(name.substring(3));
				if (values.containsKey(field)) {
					try {
						Object value = values.get(field);
						m.invoke(o, new Object[] { value } );
					} catch (InvocationTargetException e) {
						Exception t = (Exception) e.getTargetException();
						logger.error(t.getMessage());
						throw new RuntimeException("Method invocation error: " + 
								o.getClass().getSimpleName() + "." + name + 
								" - " + t.getMessage());
					} catch (Exception e) {
						logger.error(e.getMessage());
						throw new RuntimeException("Method invocation error: " + 
								o.getClass().getSimpleName() + "." + name + 
								" - " + e.getMessage());
					}
				}
			}
		}
	}
	
	public static void writeToFile(StringBuffer buffer , FacesContext context) throws IOException{
		byte[] bytes = buffer.toString().getBytes();
		int contentLength  = bytes.length;
		logger.debug("Inside util " + contentLength);
        HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
        //application/octet-stream
        response.setContentType("text/plain");
        response.setContentLength(contentLength);
        response.setHeader("Content-disposition", "attachment; filename=\"" + "download.txt" + "\"");
        ServletOutputStream out = response.getOutputStream(); 
        out.write(bytes);
        out.flush();
        // Finalize task.
        context.responseComplete();
	}
	
	public static void writeToExcelFile(StringBuffer buffer , FacesContext context) throws IOException{
		byte[] bytes = buffer.toString().getBytes();
		int contentLength  = bytes.length;
		logger.debug("Inside util " + contentLength);
        HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
        //application/octet-stream
        response.setContentType("application/vnd.ms-excel");
        response.setContentLength(contentLength);
        response.setHeader("Content-disposition", "attachment; filename=\"" + "download.xls" + "\"");
        ServletOutputStream out = response.getOutputStream(); 
        out.write(bytes);
        out.flush();
        // Finalize task.
        context.responseComplete();
	}
	
	public static void downloadFile(File file, FacesContext context)
			throws IOException {
		if (file != null && file.canRead()) {
			long contentLength = file.length();
			logger.debug("Inside util " + contentLength);
			HttpServletResponse response = (HttpServletResponse) context
					.getExternalContext().getResponse();
			// application/octet-stream
			response.setContentType("text/plain");
			response.setContentLength((int) contentLength);
			response.setHeader("Content-disposition", "attachment; filename=\""
					+ file.getName() + "\"");
			ServletOutputStream out = response.getOutputStream();
				
			try
			{
				FileInputStream reader = new FileInputStream(file);
				byte[] buffer = new byte[1024];
				int bytesread = 0, bytesBuffered = 0;	
				while ((bytesread = reader.read(buffer)) != -1) {
					out.write(buffer, 0, bytesread);	
					bytesBuffered += bytesread;
			        if (bytesBuffered > 1024 * 1024) { //flush after 1MB
			            bytesBuffered = 0;
			            out.flush();
			        }
				}
			}
			finally {
			    if (out != null) {
			        out.flush();
			    }
			}

			// Finalize task.
			context.responseComplete();
		}
	}
	
	private static String alphabet = "1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	
	public static void downloadFileUrl(String filePath){	
		try{
			FacesContext context = FacesContext.getCurrentInstance();
			HttpSession session = (HttpSession) context.getExternalContext().getSession(false);
			HttpServletRequest req = (HttpServletRequest) context.getExternalContext().getRequest();
			String uri = req.getRequestURI().toString();
			String appName = req.getContextPath();
			
			if(filePath!=null){
				//Generate a random id
				java.util.Random r = new java.util.Random();
			    String uid = "";
			    for (int i = 0; i < 16; i++) {
			        uid = uid + alphabet.charAt(r.nextInt(alphabet.length()));
			    }
			    
			    //Set uid::filepath in session
				session.setAttribute("downloadFilename", uid + "::" + filePath);
				String encodedUri = java.net.URLEncoder.encode(uri, "UTF-8");
				
				String getUrl = "";
				if(appName!=null && appName.length()>0){
					getUrl = appName;
				}
	
				getUrl =  getUrl + "/FileDownload;jsessionid=" + session.getId() + "?uid=" + uid + "&uri=" + encodedUri;
				context.getExternalContext().redirect(getUrl);
				context.responseComplete();
			}
			else{
				session.removeAttribute("downloadFilename");
			}
		}
		catch(IOException e){
			e.printStackTrace();
		}
	}
	public static Criteria generateStringwildCardCriteria(String inputFieldName, String inputFieldValue, Criteria criteria){
    	inputFieldName = Util.lowerCaseFirst(inputFieldName);
		boolean startWild = inputFieldValue.startsWith("%");
 		boolean endWild = inputFieldValue.endsWith("%");
 		 if(startWild && endWild){ 
 			 criteria.add(Restrictions.ilike(inputFieldName, inputFieldValue.replaceAll("%", ""), MatchMode.ANYWHERE));
 		 }
 		 else if(startWild && !endWild){ 
 			 criteria.add(Restrictions.ilike(inputFieldName, inputFieldValue.replaceAll("%", ""), MatchMode.END));
 		 }
 		 else if(!startWild && endWild && (inputFieldValue.length() >= 2)){ 
 			 criteria.add(Restrictions.ilike(inputFieldName, inputFieldValue.replaceAll("%", ""), MatchMode.START));
 		 }
 		 else{
 			 criteria.add(Restrictions.eq(inputFieldName, inputFieldValue).ignoreCase()); 
 		 }
 		 return criteria;
	
    }
}
