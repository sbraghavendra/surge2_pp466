package com.ncsts.common;

import org.apache.log4j.Logger;

/**
 * @author Paul Govindan
 */

public class LoggerFactory {
	
	// This class is a singleton

	private static LoggerFactory logger_factory = new LoggerFactory();

	private LoggerFactory() {
	}

	public static LoggerFactory getInstance() {
		return logger_factory;
	}
	
/*
 * methods to change the location of log4j.properties to another location
 * 	
 * 
 * 	
	private static boolean configured = false;	
	
	private void configure() {
		if (!configured) {
			PropertyConfigurator.configure( getPropFile() );
			configured = true;
		}
	}   
   
    private String getPropFile() {
		return "C:\\ryan\\log4j.properties";
	}
	
*/

	public Logger getLogger(String name) {
		//configure();
		return Logger.getLogger(name);
	}	
	
	public Logger getLogger(Class<?> clazz) {
		return getLogger(clazz.getName());
	}
}