package com.ncsts.common;

import java.math.BigDecimal;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.hibernate.criterion.DetachedCriteria;

public class SequenceManager {

	/**
	 * This function was created as part of the move to ANSI-SQL. Since ANSI does not define sequences, we need a per-RDBMS solution.
	 * Notice that at it's current version (April 2013) this contains a 'known bug', by which the query returns the "last_number" which is actually the last cached number rather than the current value.
	 * As part of converting to ANSI, I am keeping 'bugs' as-is, and we will fix them later on. Not to mention that in this case the resolution depends on what we will end up using instead of sequences in SQL-Server-2008...
	 */
	public static String getQuerySequenceCurrentValue(String sequenceName) {
		//TODO: ANSI-SQL: Sequences need to be removed or made cross-platform...
		return "SELECT last_number FROM user_sequences WHERE sequence_name = '"+sequenceName+"'";
	}

	public static String getNextValQuery(String sequenceName) {
		//TODO: ANSI-SQL: Sequences need to be removed or made cross-platform... 
		return "SELECT "+sequenceName+".NEXTVAL FROM dual"; //in Oracle the FROM keyword must be present, and hence we use the dummy table dual.
	}

	public static long getNextVal(String sequenceName, EntityManager entityManager) {
		String nextValSql = SequenceManager.getNextValQuery(sequenceName);
		Query nextValQuery = entityManager.createNativeQuery(nextValSql);
		return ((Number)nextValQuery.getSingleResult()).longValue();
	}
	
}
