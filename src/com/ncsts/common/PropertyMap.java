package com.ncsts.common;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;

// A means of exposing all properties of an object
// as a Map interface
public class PropertyMap<T> implements Map<String,Object> {

	private Logger logger = LoggerFactory.getInstance().getLogger(PropertyMap.class);

	static Map<Class<?>,Map<String,Method>> getters = new HashMap<Class<?>,Map<String,Method>>();
	static Map<Class<?>,Map<String,Method>> setters = new HashMap<Class<?>,Map<String,Method>>();
	
	private T object;
	
	public PropertyMap(T object) {
		this.object = object;

		// Check if the static mappings exist yet for this class
		if (!getters.containsKey(object.getClass())) {
			Map<String,Method> objGetters = new HashMap<String,Method>();
			Map<String,Method> objSetters = new HashMap<String,Method>();
			for (Method m : object.getClass().getMethods()) {
				String name = m.getName();
				String field = Util.lowerCaseFirst(name.substring(3));
				// Look for properties only
				if (name.startsWith("get") && 
						(m.getParameterTypes().length == 0) &&
						!name.equals("getPropertyMap")) {
					objGetters.put(field, m);
				}
				if (name.startsWith("set") && 
						(m.getParameterTypes().length == 1) &&
						!name.equals("setPropertyMap")) {
					objSetters.put(field, m);
				}
			}
			
			// Save for later
			getters.put(object.getClass(), objGetters);
			setters.put(object.getClass(), objSetters);
		}
	}
	
	public void clear() {
		// Not supported
		logger.error("Unsupported method: clear");
	}

	public boolean containsValue(Object value) {
		// Not supported
		logger.error("Unsupported method: containsValue");
		return false;
	}

	public Collection<Object> values() {
		// Not supported
		logger.error("Unsupported method: values");
		return null;
	}

	public Object remove(Object key) {
		// Not supported
		logger.error("Unsupported method: remove");
		return null;
	}

	public Set<Map.Entry<String, Object>> entrySet() {
		// Not supported
		logger.error("Unsupported method: entrySet");
		return null;
	}

	public Set<String> keySet() {
		return getters.get(object.getClass()).keySet();
	}

	public boolean containsKey(Object key) {
		return getters.get(object.getClass()).containsKey(key);
	}
	
	public Object get(Object key) {
		return Util.getProperty(object, (String) key);
	}

	public boolean isEmpty() {
		// Never empty
		return false;
	}

	public Object put(String key, Object value) {
		Method m = setters.get(object.getClass()).get(key);
		if (m != null) {
			try {
				m.invoke(object, new Object[] { value });
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				e.printStackTrace();
			}
		} else {
			logger.error("Method not found!");
		}

		return null;
	}

	public void putAll(Map<? extends String, ? extends Object> t) {
		for (String key : t.keySet()) {
			put(key, t.get(key));
		}
	}
	
	public int size() {
		return getters.get(object.getClass()).size();
	}
}
