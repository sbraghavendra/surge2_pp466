package com.ncsts.common.helper;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.*;

import com.ncsts.domain.UserEntity;

/**
 * <b>Created:</b> Mar 18, 2008<br>
 * <b>Title:</b> SortOrder<br>
 * <b>Description:</b><br>
 * <b>Copyright:</b> Copyright (c) 2008<br>
 * @author MTyson
 * <p>
 * A simple VO object to pass around a sorting order.  This is mostly taken from 
 * org.richfaces.model.SortOrder, to avoid backend dep. on JSF implmenetation.  
 */

public class OrderBy {
    private static final long serialVersionUID = 2423450441570551363L;
    
    private static final transient Log log = LogFactory.getLog(com.ncsts.common.helper.OrderBy.class);

    private List<FieldSortOrder> fields = new ArrayList<FieldSortOrder>();
    
    /**default empty constructor*/
    public OrderBy() {
    }
    
    public OrderBy(FieldSortOrder[] fields) {
        super();
        for (FieldSortOrder field : fields) {
        	this.fields.add(field);
        }
    }
    
    public int getSortOrderSize(){
    	return fields.size();
    }
    public void addFieldSortOrder(FieldSortOrder field){
    	fields.add(field);
    }
    
    /**
     * Convenience method for constructing a new SortOrder from a org.richfaces.model.SortOrder.
     * @param newFields
     */
    public OrderBy(org.richfaces.model.SortOrder richFacesSortOrder) {
        if (richFacesSortOrder != null){
            org.richfaces.model.SortField[] rfFields = richFacesSortOrder.getFields();
            int i = 0;
            if (rfFields != null){
                for (org.richfaces.model.SortField aNewField : rfFields){
                	fields.add(new FieldSortOrder(aNewField.getName(), i++, aNewField.getAscending()));
                }
            }
        }            
    }

    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        final OrderBy other = (OrderBy) obj;
        if (!fields.equals(other.fields))
            return false;
        return true;
    }

    public List<FieldSortOrder> getFields() {
        return fields;
    }

    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + fields.hashCode();
        return result;
    }

    public void setFields(FieldSortOrder[] fields) {
    	this.fields.clear();
        for (FieldSortOrder field : fields) {
        	this.fields.add(field);
        }
    }
    
    /**
     * Returns a string for ordering an HQL query.
     * @param entityAlias
     * @param entityName This is only required in order to resolve composite key fields correctly. 
     * @return
     */
    public String getHQLOrderByClause(String entityName, String entityAlias){
        StringBuffer sb = new StringBuffer("");
        if (this.fields != null && this.fields.size() > 0){
            sb.append("order by ");
            int fieldCount = this.fields.size() - 1;
            for (FieldSortOrder field : this.fields){
                String direction = field.getAscending() ? " asc " : " desc ";
                sb.append(entityAlias + "." + resolveCompositeKeys(entityName, field.getName()) + direction);
                fieldCount--;
                if (fieldCount > 0){ // There's another field to add, so insert comma
                    sb.append(", ");
                }
            }
        }
        if (log.isInfoEnabled()) { log.info("RETURNING sb: " + sb); }
        return sb.toString();
    }
    
    /**
     * This is a work around.  The direct accessors of composite keys when used in the order by clause don't work in HQL.
     * This method manually figures out what the actually HQL reference should be.  This requires hardcoded
     * knowledge about the domain objects here, and so this is pretty hacky.
     * @param entityName
     * @param fieldName
     * @return A string representing the correct HQL reference to the given fieldName.
     * 
     * For example, if the entityName is "UserEntity" and the fieldName is "UserCode", the return value
     * will be "userEntityPK.userCode"
     */
    protected String resolveCompositeKeys(String entityName, String fieldName){
        if (log.isTraceEnabled()) { log.trace("BEGIN resolveCompositeKeys() - entityName: " + entityName + " | fieldName: " + fieldName); }
        String fieldReference = fieldName; // By default, the ref is just the fieldName
        if (UserEntity.class.getCanonicalName().equals(entityName)){
            if ("userCode".equals(fieldName) || "entityId".equals(fieldName)){
                fieldReference = "userEntityPK." + fieldName;
            }
        }
        if (log.isInfoEnabled()) { log.info("RETURNING fieldReference: " + fieldReference); }
        return fieldReference;
    }

    public String toString(){
        StringBuffer sb = new StringBuffer("SortOrder - ");
        for (Object o : fields){
            sb.append("Field: " + o + " | ");
        }
        return sb.toString();
    }
}
