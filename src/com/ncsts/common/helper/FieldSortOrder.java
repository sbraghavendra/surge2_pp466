package com.ncsts.common.helper;

//import org.apache.commons.logging.*;

/**
 * <b>Created:</b> Mar 18, 2008<br>
 * <b>Title:</b> SortField<br>
 * <b>Description:</b><br>
 * <b>Copyright:</b> Copyright (c) 2008<br>
 * @author MTyson
 * <p>
 * A simple VO object to pass around the sort order of a field.  This is mostly taken from 
 * org.richfaces.model.SortField, to avoid backend dep. on JSF implmenetation.  
 */

public class FieldSortOrder {
    //private static final transient Log log = LogFactory.getLog(com.ncsts.common.helper.SortField.class);

    /**default empty constructor*/
    public FieldSortOrder() {
    }

    private static final long serialVersionUID = 1L;
    private String name = null;
    private int index = -1;
    private Boolean ascending = null;
    
    public FieldSortOrder(String name, int index, Boolean ascending) {
        super();
        this.name = name;
        this.index = index;
        this.ascending = ascending;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public Boolean getAscending() {
        return ascending;
    }

    public void setAscending(Boolean ascending) {
        this.ascending = ascending;
    }

    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((ascending == null) ? 0 : ascending.hashCode());
        result = prime * result + index;
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        return result;
    }

    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        final FieldSortOrder other = (FieldSortOrder) obj;
        if (ascending == null) {
            if (other.ascending != null)
                return false;
        } else if (!ascending.equals(other.ascending))
            return false;
        if (index != other.index)
            return false;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        return true;
    }

    public String toString(){
        return "[SortField - '" + name + "', " + index + ", asc: " + ascending + "]";
    }
}
