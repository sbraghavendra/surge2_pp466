package com.ncsts.common.comparator;

import java.util.Comparator;
import java.util.Date;
import java.util.Map;

/*Copied from billing*/

@SuppressWarnings("rawtypes")
public class StringDateComparator implements Comparator {
    public int compare(Object obj1, Object obj2) {
        int result = 0;
        Map.Entry e1 = (Map.Entry) obj1;
        Map.Entry e2 = (Map.Entry) obj2;// Sort based on values.

        Date value1 = (Date) e1.getValue();
        Date value2 = (Date) e2.getValue();

        if (value1 != null && value1.compareTo(value2) == 0) {
            String word1 = (String) e1.getKey();
            String word2 = (String) e2.getKey();

            // Sort String in an alphabetical order
            result = word1.compareToIgnoreCase(word2);

        } else {
            // Sort values in a ascending order
            result = value1 == null ? -1 : value1.compareTo(value2);
        }
        return result;
    }
}