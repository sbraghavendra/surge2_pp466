package com.ncsts.common;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;

import com.ncsts.domain.ListCodes;

public class ListCodeMap implements Map<String, Map<String, ListCodes>> {

	static private Logger logger = Logger.getLogger(ListCodeMap.class);
	
	private CacheManager cacheManager;
	
	public ListCodeMap(CacheManager cacheManager) {
		this.cacheManager = cacheManager;
	}
	
	public void clear() {
		// Not relevant
		logger.error("clear");
	}

	public boolean containsValue(Object value) {
		// Not relevant
		logger.error("containsValue");
		return false;
	}

	public Set<java.util.Map.Entry<String, Map<String, ListCodes>>> entrySet() {
		// Not relevant
		logger.error("entrySet");
		return null;
	}

	public Map<String, ListCodes> put(String key, Map<String, ListCodes> value) {
		// Not relevant
		logger.error("put");
		return null;
	}

	public void putAll(Map<? extends String, ? extends Map<String, ListCodes>> t) {
		// Not relevant
		logger.error("putAll");
	}

	public Map<String, ListCodes> remove(Object key) {
		// Not relevant
		logger.error("remove");
		return null;
	}

	public int size() {
		// Not relevant
		logger.error("size");
		return 0;
	}

	public Collection<Map<String, ListCodes>> values() {
		// Not relevant
		logger.error("values");
		return null;
	}

	public Set<String> keySet() {
		// Not relevant - but could be implemented
		logger.error("keySet");
		return null;
	}
	
	public boolean containsKey(Object key) {
		// Fake that we return all keys
		logger.debug("containsKey: " + key);
		return true;
	}

	public Map<String, ListCodes> get(Object key) {
		return cacheManager.getListCodesMapByType((String) key);
	}

	public boolean isEmpty() {
		return false;
	}
}
