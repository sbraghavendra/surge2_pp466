/*
 * Author: Jim M. Wilson
 * Created: Aug 28, 2008
 * $Date: 2008-08-28 12:49:02 -0500 (Thu, 28 Aug 2008) $: - $Revision: 1999 $: 
 */

package com.ncsts.common;

public class LogMemory {
  public static String get(){
    Runtime rt = Runtime.getRuntime();
    rt.gc();
    Thread.yield();
    String availMemory = rt.freeMemory()/1024 + "k";
    String totalMemory = rt.totalMemory()/1024 + "k";
    String usedMemory = (rt.totalMemory()-rt.freeMemory())/1024 + "k";
    return "Memory: "+usedMemory + ", avail: " + availMemory +" / " + totalMemory;
  }  
  
  public static void executeGC(){
    Runtime rt = Runtime.getRuntime();
    rt.gc();
    Thread.yield();
  }  
  
  /**
   * Get # of K of used memory
   * @return
   */
  public static double getUsed(){
    Runtime rt = Runtime.getRuntime();
    rt.gc();
    Thread.yield();
    return (rt.totalMemory()-rt.freeMemory())/1024 ;
  }
}
