package com.ncsts.common;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.faces.model.SelectItem;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.ncsts.domain.CCHCode;
import com.ncsts.domain.CCHTaxMatrix;
import com.ncsts.domain.DataDefinitionColumn;
import com.ncsts.domain.DataDefinitionColumnDrivers;
import com.ncsts.domain.DriverNames;
import com.ncsts.domain.EntityItem;
import com.ncsts.domain.ListCodes;
import com.ncsts.domain.OptionCodePK;
import com.ncsts.domain.TaxCode;
import com.ncsts.domain.User;
import com.ncsts.dto.PurchaseTransactionOptions;
import com.ncsts.dto.TaxCodeStateDTO;
import com.ncsts.services.DataDefinitionService;
import com.ncsts.services.DriverReferenceService;
import com.ncsts.services.EntityItemService;
import com.ncsts.services.EntityLevelService;
import com.ncsts.services.ListCodesService;
import com.ncsts.services.OptionService;
import com.ncsts.services.TaxCodeDetailService;
import com.ncsts.services.TaxCodeStateService;
import com.ncsts.services.UserService;
import com.ncsts.view.bean.ChangeContextHolder;
import com.opensymphony.oscache.base.NeedsRefreshException;
import com.opensymphony.oscache.general.GeneralCacheAdministrator;
import com.opensymphony.oscache.web.filter.ExpiresRefreshPolicy;
public class CacheManager {

	static private Logger logger = Logger.getLogger(CacheManager.class);
	
	static private final int CACHE_EXPIRATION_SECONDS = 1800;	// 30 minutes
	
	static GeneralCacheAdministrator cacheAdministrator;
	
	@Autowired
	private EntityItemService entityItemService;
	
	@Autowired
	private EntityLevelService entityLevelService;

	@Autowired
	private OptionService optionService;
	
	private DriverReferenceService driverReferenceService;
	private DataDefinitionService dataDefinitionService;
	private ListCodesService listCodesService;
	private TaxCodeStateService taxCodeStateService;
	private TaxCodeDetailService taxCodeDetailService;
	private ListCodeMap listCodeMap;
	
	@Autowired
	private UserService userService;
	
	private CacheManager(){
		listCodeMap = new ListCodeMap(this);
	}

	public GeneralCacheAdministrator getCacheAdministrator() {
		return cacheAdministrator;
	}

	public void setCacheAdministrator(GeneralCacheAdministrator cacheAdministrator) {
		CacheManager.cacheAdministrator = cacheAdministrator;
	}

	public DriverReferenceService getDriverReferenceService() {
		return driverReferenceService;
	}

	public void setDriverReferenceService(DriverReferenceService driverReferenceService) {
		this.driverReferenceService = driverReferenceService;
	}

	public DataDefinitionService getDataDefinitionService() {
		return dataDefinitionService;
	}

	public void setDataDefinitionService(DataDefinitionService dataDefinitionService) {
		this.dataDefinitionService = dataDefinitionService;
	}

	public ListCodesService getListCodesService() {
		return listCodesService;
	}

	public void setListCodesService(ListCodesService listCodesService) {
		this.listCodesService = listCodesService;
	}

	public TaxCodeStateService getTaxCodeStateService() {
		return taxCodeStateService;
	}

	public void setTaxCodeStateService(TaxCodeStateService taxCodeStateService) {
		this.taxCodeStateService = taxCodeStateService;
	}

	public TaxCodeDetailService getTaxCodeDetailService() {
		return taxCodeDetailService;
	}

	public void setTaxCodeDetailService(TaxCodeDetailService taxCodeDetailService) {
		this.taxCodeDetailService = taxCodeDetailService;
	}
	
	public void flushEntryCache(String key) {
		cacheAdministrator.flushEntry(getKeyPerDb(key));
	}

	
	public void flushMatrixColDriverNames(String driverCode){
		String key = driverCode.toUpperCase() + "_MATRIXCOL_DN_MAP";
		cacheAdministrator.flushEntry(getKeyPerDb(key));
		
	}
	
	public void flushTaxCodeCountryStateMap() {
		String key = "TAXCODECOUNTRYSTATE_MAP";
		cacheAdministrator.flushEntry(getKeyPerDb(key));
	}

	@SuppressWarnings("unchecked")
	public Map<String,DriverNames> getMatrixColDriverNamesMap(String driverCode) {
		Map<String, DriverNames> result = null;
		String key = driverCode.toUpperCase() + "_MATRIXCOL_DN_MAP";
		
		try {
			
			result = (Map<String, DriverNames>)cacheAdministrator.getFromCache(getKeyPerDb(key));
			
		} catch (NeedsRefreshException e) {
			
			try{
				result = new LinkedHashMap<String,DriverNames>();
				
				for (DriverNames dn : driverReferenceService.getDriverNamesByCode(driverCode.toUpperCase())) {
					result.put(dn.getMatrixColName(), dn);
				}
				
				logger.debug("Caching " + key + " Size: " + result.size());
				cacheAdministrator.putInCache(getKeyPerDb(key), result, new ExpiresRefreshPolicy(CACHE_EXPIRATION_SECONDS));
			
			} catch(Exception ex){
				ex.printStackTrace();
			}
		}
		
		return result;
	}
	
	public void flushTransactionDriverPropertyNamesMap(String driverCode){
		String key = driverCode.toUpperCase() + "_TRANSDTLCOL_DN_MAP";
		cacheAdministrator.flushEntry(getKeyPerDb(key));
	}
	
	@SuppressWarnings("unchecked")
	public Map<String, Long> getParentEntityLevel(Long entityId) {
		Map<String,Long> parentEntityLevelMap = new HashMap<String,Long>();
		
		EntityItem entityItem = entityItemService.findById(entityId);
		if(entityItem==null || entityItem.getEntityLevelId()==0L){
			return parentEntityLevelMap;
		}
		
		parentEntityLevelMap.put(entityLevelService.findById(entityItem.getEntityLevelId()).getTransDetailColumnName(), entityItem.getEntityLevelId());
				
		int topLevel = entityItem.getEntityLevelId().intValue();
		
		for(int i=topLevel; i>1 ; i--) {
			entityItem = entityItemService.findById(entityItem.getParentEntityId());
			if(entityItem!=null){
				parentEntityLevelMap.put(entityLevelService.findById(entityItem.getEntityLevelId()).getTransDetailColumnName(), entityItem.getEntityLevelId());
			}
			else{
				break;
			}
		}
		
		return parentEntityLevelMap;
	}
	
	@SuppressWarnings("unchecked")
	public Map<String, String> getParentEntityLevelMap(Long entityId) {
		Map<String,String> parentEntityLevelMap = new HashMap<String,String>();
		
		EntityItem entityItem = entityItemService.findById(entityId);
		if(entityItem==null || entityItem.getEntityLevelId()==0L){
			return parentEntityLevelMap;
		}
		
		parentEntityLevelMap.put(entityLevelService.findById(entityItem.getEntityLevelId()).getTransDetailColumnName(), entityItem.getEntityCode().toUpperCase());
				
		int topLevel = entityItem.getEntityLevelId().intValue();
		
		for(int i=topLevel; i>1 ; i--) {
			entityItem = entityItemService.findById(entityItem.getParentEntityId());
			if(entityItem!=null){
				parentEntityLevelMap.put(entityLevelService.findById(entityItem.getEntityLevelId()).getTransDetailColumnName(), entityItem.getEntityCode().toUpperCase());
			}
			else{
				break;
			}
		}
		
		return parentEntityLevelMap;
	}

	@SuppressWarnings("unchecked")
	public Map<String,DriverNames> getTransactionPropertyDriverNamesMap(String driverCode) {
		Map<String, DriverNames> result = null;
		String key = ((driverCode == null)? "ALL":driverCode.toUpperCase()) + "_TRANSDTLCOL_DN_MAP";
		
		try {
			
			result = (Map<String, DriverNames>)cacheAdministrator.getFromCache(getKeyPerDb(key));
			
		} catch (NeedsRefreshException e) {
			
			try{
				result = new LinkedHashMap<String,DriverNames>();
				
				for (DriverNames dn : driverReferenceService.getDriverNamesByCode(driverCode)) {
					// NOTE:  Mapping by property name - not column name!!
					result.put(Util.columnToProperty(dn.getTransDtlColName()), dn);
				}
				
				logger.debug("Caching " + key + " Size: " + result.size());
				cacheAdministrator.putInCache(getKeyPerDb(key), result, new ExpiresRefreshPolicy(CACHE_EXPIRATION_SECONDS));
			
			} catch(Exception ex){
				ex.printStackTrace();
			}
		}
		
		return result;
	}
	public Map<String,DriverNames> getTransactionColumnDriverNamesMap(String driverCode) {
		Map<String, DriverNames> result = null;
		String key = ((driverCode == null)? "ALL":driverCode.toUpperCase()) + "_TRANSDTLCOL_COL_MAP";
		
		try {
			result = (Map<String, DriverNames>)cacheAdministrator.getFromCache(getKeyPerDb(key));
		} catch (NeedsRefreshException e) {
			try{
				result = new LinkedHashMap<String,DriverNames>();
				for (DriverNames dn : driverReferenceService.getDriverNamesByCode(driverCode)) {
					result.put(dn.getTransDtlColName(), dn);
				}
				
				logger.debug("Caching " + key + " Size: " + result.size());
				cacheAdministrator.putInCache(getKeyPerDb(key), result, new ExpiresRefreshPolicy(CACHE_EXPIRATION_SECONDS));
			} catch(Exception ex){
				ex.printStackTrace();
			}
		}
		return result;
	}


	public Map<String,DataDefinitionColumnDrivers> getValidDriversMap(String driverCode, String tableName) {
		Map<String, DataDefinitionColumnDrivers> result = null;
		String key = ((driverCode == null)? "ALL":driverCode.toUpperCase()) +((tableName == null)? "ALL":tableName.toUpperCase()) + "_VALID_DRIVERS_MAP";

		try {
			result = (Map<String, DataDefinitionColumnDrivers>)cacheAdministrator.getFromCache(getKeyPerDb(key));
		} catch (NeedsRefreshException e) {
			try{
				result = driverReferenceService.getValidDrivers(driverCode, tableName);

				logger.debug("Caching " + key + " Size: " + result.size());
				cacheAdministrator.putInCache(getKeyPerDb(key), result, new ExpiresRefreshPolicy(CACHE_EXPIRATION_SECONDS));
			} catch(Exception ex){
				ex.printStackTrace();
			}
		}
		return result;
	}
	
	@SuppressWarnings("unchecked")
	public Map<String,User> getAllUsersMap() {
		Map<String,User> result = null;
		String key = "ALLUSERS_MAP";
		
		try {
			result = (Map<String,User>)cacheAdministrator.getFromCache(getKeyPerDb(key));
		} catch (NeedsRefreshException e) {
			try{
				result = new LinkedHashMap<String, User>();
				for (User ur : userService.findAllUsers()) {
					result.put(ur.getUserCode(), ur);
				}
				
				logger.debug("Caching " + key + " Size: " + result.size());
				cacheAdministrator.putInCache(getKeyPerDb(key), result, new ExpiresRefreshPolicy(CACHE_EXPIRATION_SECONDS));
			} catch(Exception ex){
				ex.printStackTrace();
			}
		}
		return result;
	}


	public void flushAllUsersMap() {
		String key = "ALLUSERS_MAP";
		cacheAdministrator.flushEntry(getKeyPerDb(key));
	}

	public Map<String,DataDefinitionColumn> getTransactionPropertyMap() {
		return getDataDefinitionPropertyMap("TB_PURCHTRANS");
	}
	
	public Map<String,DataDefinitionColumn> getBCPSaleTransactionsColumnMap() {
		return getDataDefinitionColumnMap("TB_BCP_BILLTRANS");
	}
	
	public Map<String,DataDefinitionColumn> getSaleTransactionPropertyMap() {
		return getDataDefinitionPropertyMap("TB_BILLTRANS");
	}
	public Map<String,DataDefinitionColumn> getPCOTransactionPropertyMap() {
		return getDataDefinitionPropertyMapPCO();
	}
	
	public Map<String,DataDefinitionColumn> getTransactionsColumnMap() {
		return getDataDefinitionColumnMap("TB_PURCHTRANS");
    }
	
	public Map<String,DataDefinitionColumn> getSaleTransactionsColumnMap() {
		return getDataDefinitionColumnMap("TB_BILLTRANS");
	}
	
 

public Map<String,DataDefinitionColumn> getBCPTransactionsColumnMap() {
		return getDataDefinitionColumnMap("TB_BCP_TRANSACTIONS");
  }
@SuppressWarnings("unchecked")
  public Map<String,DataDefinitionColumn> getDataDefinitionColumnMap(String tableName) {
		Map<String, DataDefinitionColumn> result = null;
		String key = tableName.toUpperCase() + "_DDC_MAP";

		try {
			result = (Map<String, DataDefinitionColumn>)cacheAdministrator.getFromCache(getKeyPerDb(key));	
		} catch (NeedsRefreshException e) {
			try{
				result = new LinkedHashMap<String,DataDefinitionColumn>();
				for (DataDefinitionColumn col : dataDefinitionService.getAllDataDefinitionColumnByTable(tableName)) {
					// Do some fixups to make sure AbbrDesc and Desc always exist!
					String property = Util.columnToProperty(col.getColumnName());
					String abbr = col.getAbbrDesc();
					if ((abbr == null) || abbr.equals("")) {
						// jmw - This is trying to set value to something > the max field size
						// rather than try to get the size for tb_data_def_column from tb_data_def_column
						// I am hardcoding for now
						String s = Util.propertyToLabel(property);
						col.setAbbrDesc(s.substring(0, Math.min(20, s.length())-1));
					}
					String desc = col.getDescription();
					if ((desc == null) || desc.equals("")) {
						String s = Util.propertyToLabel(property);
						col.setDescription(s.substring(0, Math.min(40, s.length())-1));
					}
					result.put(col.getColumnName(), col);
				}
				
				if(tableName.equalsIgnoreCase("TB_PURCHTRANS")){
					//CCH
					result.remove("CCH_TAXCAT_CODE");
					result.remove("CCH_GROUP_CODE");
					result.remove("CCH_ITEM_CODE");
				}
				else if(tableName.equalsIgnoreCase("TB_BCP_TRANSACTIONS")){
					//CCH
					result.remove("CCH_TAXCAT_CODE");
					result.remove("CCH_GROUP_CODE");
					result.remove("CCH_ITEM_CODE");
				}
				
				logger.debug("Caching " + key + " Size: " + result.size());
				cacheAdministrator.putInCache(getKeyPerDb(key), result, new ExpiresRefreshPolicy(CACHE_EXPIRATION_SECONDS));
			} catch(Exception ex){
				ex.printStackTrace();
			}
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		
		return result;
	}
	
	@SuppressWarnings("unchecked")
	public Map<String,DataDefinitionColumn> getDataDefinitionPropertyMap(String tableName) {
		Map<String, DataDefinitionColumn> result = null;
		String key = tableName.toUpperCase() + "_DDC_P_MAP";
		
		try {
			
			result = (Map<String, DataDefinitionColumn>)cacheAdministrator.getFromCache(getKeyPerDb(key));
			
		} catch (NeedsRefreshException e) {
			
			try{
				result = new LinkedHashMap<String,DataDefinitionColumn>();
				
				for (DataDefinitionColumn col : getDataDefinitionColumnMap(tableName).values()) {
					result.put(Util.columnToProperty(col.getColumnName()), col);
				}
				
				logger.debug("Caching " + key + " Size: " + result.size());
				cacheAdministrator.putInCache(getKeyPerDb(key), result, new ExpiresRefreshPolicy(CACHE_EXPIRATION_SECONDS));
			
			} catch(Exception ex){
				ex.printStackTrace();
			}
		}
		
		return result;
	}
	
	
	public void flushDataDefinitionMap(String tableName) {
		String key = tableName.toUpperCase() + "_DDC_P_MAP";
		cacheAdministrator.flushEntry(getKeyPerDb(key));

		key = tableName.toUpperCase() + "_DDC_MAP";
		cacheAdministrator.flushEntry(getKeyPerDb(key));
		
		key = "PCO"+ "_DDC_P_MAP_P";
		cacheAdministrator.flushEntry(getKeyPerDb(key));
	}
	
	public void flushListCodesByType(String codeType) {
		String key = codeType + "_LISTCODES";
		cacheAdministrator.flushEntry(getKeyPerDb(key));

		key = codeType + "_LISTCODES_MAP";
		cacheAdministrator.flushEntry(getKeyPerDb(key));
	}
	
	@SuppressWarnings("unchecked")
	public Map<String,DataDefinitionColumn>  getDataDefinitionPropertyMapPCO() {
		Map<String, DataDefinitionColumn> result = null;
		String key = "PCO"+ "_DDC_P_MAP_P";
		try {
			
			result = (Map<String, DataDefinitionColumn>)cacheAdministrator.getFromCache(getKeyPerDb(key));
			
		} catch (NeedsRefreshException e) {
			
			try{
				result = new LinkedHashMap<String,DataDefinitionColumn>();
				
				for (DataDefinitionColumn col : getDataDefinitionColumnMap("TB_BILLTRANS").values()) {
					result.put(Util.columnToProperty(col.getColumnName()), col);
				}
				
		        for (DataDefinitionColumn col : getDataDefinitionColumnMap("TB_PURCHTRANS").values()) {
					result.put(Util.columnToProperty(col.getColumnName()), col);
				}
				logger.debug("Caching " + key + " Size: " + result.size());
				cacheAdministrator.putInCache(getKeyPerDb(key), result, new ExpiresRefreshPolicy(CACHE_EXPIRATION_SECONDS));
			
			} catch(Exception ex){
				ex.printStackTrace();
			}
		}
		
		return result;
		
	}
	@SuppressWarnings("unchecked")
	public List<ListCodes> getListCodesByType(String codeType) {
		List<ListCodes> result = null;
		String key = codeType + "_LISTCODES";

		try {
			
			result = (List<ListCodes>)cacheAdministrator.getFromCache(getKeyPerDb(key));
			
		} catch (NeedsRefreshException e) {
			
			try{
				result = listCodesService.getListCodesByCodeTypeCode(codeType);
				
				logger.debug("Caching " + key + " Size: " + result.size());
				cacheAdministrator.putInCache(getKeyPerDb(key), result, new ExpiresRefreshPolicy(CACHE_EXPIRATION_SECONDS));
			
			} catch(Exception ex){
				ex.printStackTrace();
			}
		}
		
		return result;
	}
	
	@SuppressWarnings("unchecked")
	public Map<String,ListCodes> getListCodesMapByType(String codeType) {
		Map<String,ListCodes> result = null;
		String key = codeType + "_LISTCODES_MAP";

		try {
			result = (Map<String,ListCodes>)cacheAdministrator.getFromCache(getKeyPerDb(key));	
		} catch (NeedsRefreshException e) {
			try{
				result = new LinkedHashMap<String,ListCodes>();		
				for (ListCodes listCode : listCodesService.getListCodesByCodeTypeCode(codeType)) {
					result.put(listCode.getCodeCode(), listCode);
				}
				
				logger.debug("Caching " + key + " Size: " + result.size());

				cacheAdministrator.putInCache(getKeyPerDb(key), result, new ExpiresRefreshPolicy(CACHE_EXPIRATION_SECONDS));
			} catch(Exception ex){
				ex.printStackTrace();
			}
		}
		
		return result;
	}
	
	public Map<String,Map<String,ListCodes>> getListCodeMap() {
		return listCodeMap;
	}
	
	public Map<String, ListCodes> getErrorListCodes() {
		return getListCodesMapByType("ERRORCODE");
	}
	
	public void flushTaxCodeStateMap() {
		String key = "TAXCODESTATE_MAP";
		cacheAdministrator.flushEntry(getKeyPerDb(key));
	}
	
	@SuppressWarnings("unchecked")
	public Map<String,TaxCodeStateDTO> getTaxCodeStateMap() {
		Map<String,TaxCodeStateDTO> result = null;
		String key = "TAXCODESTATE_MAP";

		try {
			
			result = (Map<String,TaxCodeStateDTO>)cacheAdministrator.getFromCache(getKeyPerDb(key));
			
		} catch (NeedsRefreshException e) {
			
			try{
				Map<String, String> localTaxMap = getLocalTaxabilityCodeMap();
				
				result = new LinkedHashMap<String,TaxCodeStateDTO>();
				
				for (TaxCodeStateDTO state : taxCodeStateService.getAllTaxCodeStateDTO()) {
					state.setLocalTaxabilityCodeDesc(localTaxMap.get(state.getLocalTaxabilityCode()));
					state.setCpCertLevelCodeDesc(localTaxMap.get(state.getCpCertLevelCode()));
					result.put(state.getTaxCodeState(), state);
				}
				
				logger.debug("Caching " + key + " Size: " + result.size());
				cacheAdministrator.putInCache(getKeyPerDb(key), result, new ExpiresRefreshPolicy(CACHE_EXPIRATION_SECONDS));
			
			} catch(Exception ex){
				ex.printStackTrace();
			}
		}
		
		return result;
	}
	
	@SuppressWarnings("unchecked")
	public Map<String,String> getCountryMap() {
		Map<String,String> result = null;
		String key = "COUNTRYCODE_MAP";

		try {
			
			result = (Map<String,String>)cacheAdministrator.getFromCache(getKeyPerDb(key));
			
		} catch (NeedsRefreshException e) {
			
			try{
				result = new LinkedHashMap<String,String>();
				
				List<ListCodes> listCodes = listCodesService.getListCodesByCodeTypeCode("COUNTRY");
				for(ListCodes listCode : listCodes){
					result.put(listCode.getCodeCode(), listCode.getDescription());
		    	}
				
				logger.debug("Caching " + key + " Size: " + result.size());
				cacheAdministrator.putInCache(getKeyPerDb(key), result, new ExpiresRefreshPolicy(CACHE_EXPIRATION_SECONDS));
			
			} catch(Exception ex){
				ex.printStackTrace();
			}
		}
		
		return result;
	}
	
	@SuppressWarnings("unchecked")
	public Map<String,String> getRateTypeMap() {
		Map<String,String> result = null;
		String key = "RATETYPE_MAP";

		try {
			
			result = (Map<String,String>)cacheAdministrator.getFromCache(getKeyPerDb(key));
			
		} catch (NeedsRefreshException e) {
			
			try{
				result = new LinkedHashMap<String,String>();
				
				List<ListCodes> listCodes = getListCodesByType("RATETYPE");
				   if(listCodes != null) {
					   for(ListCodes listCode : listCodes) {
						   result.put(listCode.getCodeCode(), listCode.getDescription());
						  
					   }
				   }
				
				logger.debug("Caching " + key + " Size: " + result.size());
				cacheAdministrator.putInCache(getKeyPerDb(key), result, new ExpiresRefreshPolicy(CACHE_EXPIRATION_SECONDS));
			
			} catch(Exception ex){
				ex.printStackTrace();
			}
		}
		
		return result;
	}
	
	@SuppressWarnings("unchecked")
	public Map<String,String> getRateTypeMapByCode() {
		Map<String,String> result = null;
		String key = "RATETYPEBYCODE_MAP";

		try {
			
			result = (Map<String,String>)cacheAdministrator.getFromCache(getKeyPerDb(key));
			
		} catch (NeedsRefreshException e) {
			
			try{
				result = new LinkedHashMap<String,String>();
				
				List<ListCodes> listCodes = getListCodesByType("RATETYPE");
				   if(listCodes != null) {
					   for(ListCodes listCode : listCodes) {
						   result.put(listCode.getListCodesPK().getCodeTypeCode(), listCode.getDescription());
						  
					   }
				   }
				
				logger.debug("Caching " + key + " Size: " + result.size());
				cacheAdministrator.putInCache(getKeyPerDb(key), result, new ExpiresRefreshPolicy(CACHE_EXPIRATION_SECONDS));
			
			} catch(Exception ex){
				ex.printStackTrace();
			}
		}
		
		return result;
	}
	
	
	@SuppressWarnings("unchecked")
	public Map<String,String> getModMap() {
		Map<String,String> result = null;
		String key = "MOD_MAP";

		try {
			
			result = (Map<String,String>)cacheAdministrator.getFromCache(getKeyPerDb(key));
			
		} catch (NeedsRefreshException e) {
			
			try{
				result = new LinkedHashMap<String,String>();
				
				List<ListCodes> listCodes = getListCodesByType("MOD");
				   if(listCodes != null) {
					   for(ListCodes listCode : listCodes) {
						   result.put(listCode.getCodeCode(), listCode.getDescription());
					   }
				   }
				
				logger.debug("Caching " + key + " Size: " + result.size());
				cacheAdministrator.putInCache(getKeyPerDb(key), result, new ExpiresRefreshPolicy(CACHE_EXPIRATION_SECONDS));
			
			} catch(Exception ex){
				ex.printStackTrace();
			}
		}
		
		return result;
	}
	
	@SuppressWarnings("unchecked")
	public Map<Long,String> getEntityIdMap() {
		Map<Long,String> result = null;
		String key = "ENTITYID_MAP";

		try {		
			result = (Map<Long,String>)cacheAdministrator.getFromCache(getKeyPerDb(key));
			
		} catch (NeedsRefreshException e) {
			
			try{
				result = new LinkedHashMap<Long,String>();
				
		    	List<EntityItem> eList =entityItemService.findAllEntityItems();
	    	    if(eList != null && eList.size() > 0){
	    	    	for(EntityItem entityItem:eList){
	    	    		if(entityItem.getEntityId()>0L){
	    	    			result.put(entityItem.getEntityId(),   entityItem.getEntityCode() + " - " + entityItem.getEntityName());	
	    	    		}		
	    	    	}
	    	    } 	

				logger.debug("Caching " + key + " Size: " + result.size());
				cacheAdministrator.putInCache(getKeyPerDb(key), result, new ExpiresRefreshPolicy(CACHE_EXPIRATION_SECONDS));
			
			} catch(Exception ex){
				ex.printStackTrace();
			}
		}
		
		return result;
	}
	
	@SuppressWarnings("unchecked")
	public Map<String,String> getEntityCodeMap() {
		Map<String,String> result = null;
		String key = "ENTITYCODE_MAP";

		try {		
			result = (Map<String,String>)cacheAdministrator.getFromCache(getKeyPerDb(key));
			
		} catch (NeedsRefreshException e) {
			
			try{
				result = new LinkedHashMap<String,String>();
				
		    	List<EntityItem> eList =entityItemService.findAllEntityItems();
	    	    if(eList != null && eList.size() > 0){
	    	    	for(EntityItem entityItem:eList){
	    	    			result.put(entityItem.getEntityCode(), entityItem.getEntityName());		
	    	    	}
	    	    } 	

				logger.debug("Caching " + key + " Size: " + result.size());
				cacheAdministrator.putInCache(getKeyPerDb(key), result, new ExpiresRefreshPolicy(CACHE_EXPIRATION_SECONDS));
			
			} catch(Exception ex){
				ex.printStackTrace();
			}
		}
		
		return result;
	}
	
	@SuppressWarnings("unchecked")
	public Map<String,String> getGeoReconMap() {
		Map<String,String> result = null;
		String key = "GEORECON_MAP";

		try {
			
			result = (Map<String,String>)cacheAdministrator.getFromCache(getKeyPerDb(key));
			
		} catch (NeedsRefreshException e) {
			
			try{
				result = new LinkedHashMap<String,String>();
				
				List<ListCodes> listCodes = getListCodesByType("GEORECON");
				   if(listCodes != null) {
					   for(ListCodes listCode : listCodes) {
						   result.put(listCode.getCodeCode(), listCode.getDescription());
					   }
				   }
				
				logger.debug("Caching " + key + " Size: " + result.size());
				cacheAdministrator.putInCache(getKeyPerDb(key), result, new ExpiresRefreshPolicy(CACHE_EXPIRATION_SECONDS));
			
			} catch(Exception ex){
				ex.printStackTrace();
			}
		}
		
		return result;
	}
	
	@SuppressWarnings("unchecked")
	public Map<String,String> getTransTypeMap() {
		Map<String,String> result = null;
		String key = "TRANSTYPE_MAP";

		try {
			
			result = (Map<String,String>)cacheAdministrator.getFromCache(getKeyPerDb(key));
			
		} catch (NeedsRefreshException e) {
			
			try{
				result = new LinkedHashMap<String,String>();
				
				List<ListCodes> listCodes = getListCodesByType("TRANSTYPE");
				   if(listCodes != null) {
					   for(ListCodes listCode : listCodes) {
						   result.put(listCode.getCodeCode(), listCode.getDescription());
					   }
				   }
				
				logger.debug("Caching " + key + " Size: " + result.size());
				cacheAdministrator.putInCache(getKeyPerDb(key), result, new ExpiresRefreshPolicy(CACHE_EXPIRATION_SECONDS));
			
			} catch(Exception ex){
				ex.printStackTrace();
			}
		}
		
		return result;
	}
	
	@SuppressWarnings("unchecked")
	public Map<String,String> getExReasonMap() {
		Map<String,String> result = null;
		String key = "EXREASON_MAP";

		try {
			
			result = (Map<String,String>)cacheAdministrator.getFromCache(getKeyPerDb(key));
			
		} catch (NeedsRefreshException e) {
			
			try{
				result = new LinkedHashMap<String,String>();
				
				List<ListCodes> listCodes = getListCodesByType("EXREASON");
				   if(listCodes != null) {
					   for(ListCodes listCode : listCodes) {
						   result.put(listCode.getCodeCode(), listCode.getDescription());
					   }
				   }
				
				logger.debug("Caching " + key + " Size: " + result.size());
				cacheAdministrator.putInCache(getKeyPerDb(key), result, new ExpiresRefreshPolicy(CACHE_EXPIRATION_SECONDS));
			
			} catch(Exception ex){
				ex.printStackTrace();
			}
		}
		
		return result;
	}
	
	@SuppressWarnings("unchecked")
	public Map<String,String> getDiscTypeMap() {
		Map<String,String> result = null;
		String key = "DISCTYPE_MAP";

		try {
			
			result = (Map<String,String>)cacheAdministrator.getFromCache(getKeyPerDb(key));
			
		} catch (NeedsRefreshException e) {
			
			try{
				result = new LinkedHashMap<String,String>();
				
				List<ListCodes> listCodes = getListCodesByType("DISCTYPE");
				   if(listCodes != null) {
					   for(ListCodes listCode : listCodes) {
						   result.put(listCode.getCodeCode(), listCode.getDescription());
					   }
				   }
				
				logger.debug("Caching " + key + " Size: " + result.size());
				cacheAdministrator.putInCache(getKeyPerDb(key), result, new ExpiresRefreshPolicy(CACHE_EXPIRATION_SECONDS));
			
			} catch(Exception ex){
				ex.printStackTrace();
			}
		}
		
		return result;
	}
	
	@SuppressWarnings("unchecked")
	public Map<String,String> getProcMethodMap() {
		Map<String,String> result = null;
		String key = "PROCMETHOD_MAP";

		try {
			
			result = (Map<String,String>)cacheAdministrator.getFromCache(getKeyPerDb(key));
			
		} catch (NeedsRefreshException e) {
			
			try{
				result = new LinkedHashMap<String,String>();
				
				List<ListCodes> listCodes = getListCodesByType("PROCMETHOD");
				   if(listCodes != null) {
					   for(ListCodes listCode : listCodes) {
						   result.put(listCode.getCodeCode(), listCode.getDescription());
					   }
				   }
				
				logger.debug("Caching " + key + " Size: " + result.size());
				cacheAdministrator.putInCache(getKeyPerDb(key), result, new ExpiresRefreshPolicy(CACHE_EXPIRATION_SECONDS));
			
			} catch(Exception ex){
				ex.printStackTrace();
			}
		}
		
		return result;
	}
	
	@SuppressWarnings("unchecked")
	public Map<String,String> getBooleanMap() {
		Map<String,String> result = null;
		String key = "BOOLEAN_MAP";

		try {
			
			result = (Map<String,String>)cacheAdministrator.getFromCache(getKeyPerDb(key));
			
		} catch (NeedsRefreshException e) {
			
			try{
				result = new LinkedHashMap<String,String>();
				result.put("1", "True");
				result.put("0", "False");
				result.put("", "False");

				logger.debug("Caching " + key + " Size: " + result.size());
				cacheAdministrator.putInCache(getKeyPerDb(key), result, new ExpiresRefreshPolicy(CACHE_EXPIRATION_SECONDS));
			
			} catch(Exception ex){
				ex.printStackTrace();
			}
		}
		
		return result;
	}
	
	@SuppressWarnings("unchecked")
	public Map<String,String> getCertTypeMap() {
		Map<String,String> result = null;
		String key = "CERTTYPE_MAP";

		try {
			
			result = (Map<String,String>)cacheAdministrator.getFromCache(getKeyPerDb(key));
			
		} catch (NeedsRefreshException e) {
			
			try{
				result = new LinkedHashMap<String,String>();
				
				List<ListCodes> listCodes = getListCodesByType("CERTTYPE");
				   if(listCodes != null) {
					   for(ListCodes listCode : listCodes) {
						   result.put(listCode.getCodeCode(), listCode.getDescription());
					   }
				   }
				
				logger.debug("Caching " + key + " Size: " + result.size());
				cacheAdministrator.putInCache(getKeyPerDb(key), result, new ExpiresRefreshPolicy(CACHE_EXPIRATION_SECONDS));
			
			} catch(Exception ex){
				ex.printStackTrace();
			}
		}
		
		return result;
	}
	
	@SuppressWarnings("unchecked")
	public Map<String,String> getTaxtypeMap() {
		Map<String,String> result = null;
		String key = "TAXTYPE_MAP";

		try {
			
			result = (Map<String,String>)cacheAdministrator.getFromCache(getKeyPerDb(key));
			
		} catch (NeedsRefreshException e) {
			
			try{
				result = new LinkedHashMap<String,String>();
				
				List<ListCodes> listCodes = getListCodesByType("TAXTYPE");
				   if(listCodes != null) {
					   for(ListCodes listCode : listCodes) {
						   result.put(listCode.getCodeCode(), listCode.getDescription());
					   }
				   }
				
				logger.debug("Caching " + key + " Size: " + result.size());
				cacheAdministrator.putInCache(getKeyPerDb(key), result, new ExpiresRefreshPolicy(CACHE_EXPIRATION_SECONDS));
			
			} catch(Exception ex){
				ex.printStackTrace();
			}
		}
		
		return result;
	}


	public Map<OptionCodePK, String> getPurchaseEngineOptions(Boolean forceRefresh) {
		Map<OptionCodePK, String> result = null;
		String key = "PURCHASING_OPTS";

		try {
			if (forceRefresh!=null && forceRefresh) throw new NeedsRefreshException(null);
			result = (Map<OptionCodePK,String>)cacheAdministrator.getFromCache(getKeyPerDb(key));
		} catch(NeedsRefreshException nre) {
			result = new HashMap<OptionCodePK,String>();

			try {
				result.put(PurchaseTransactionOptions.PK_RATEPOINT_ENABLED, optionService.findByPKSafe(PurchaseTransactionOptions.PK_RATEPOINT_ENABLED));
				result.put(PurchaseTransactionOptions.PK_RATEPOINT_SERVER, optionService.findByPKSafe(PurchaseTransactionOptions.PK_RATEPOINT_SERVER));
				result.put(PurchaseTransactionOptions.PK_RATEPOINT_USER_ID, optionService.findByPKSafe(PurchaseTransactionOptions.PK_RATEPOINT_USER_ID));
				result.put(PurchaseTransactionOptions.PK_RATEPOINT_PASSWORD, optionService.findByPKSafe(PurchaseTransactionOptions.PK_RATEPOINT_PASSWORD));
				result.put(PurchaseTransactionOptions.PK_RATEPOINT_USELATLONLOOKUP, optionService.findByPKSafe(PurchaseTransactionOptions.PK_RATEPOINT_USELATLONLOOKUP));
				result.put(PurchaseTransactionOptions.PK_RATEPOINT_USEGENERICJURAPI, optionService.findByPKSafe(PurchaseTransactionOptions.PK_RATEPOINT_USEGENERICJURAPI));
				result.put(PurchaseTransactionOptions.PK_RATEPOINT_LATLONLOOKUP_URL, optionService.findByPKSafe(PurchaseTransactionOptions.PK_RATEPOINT_LATLONLOOKUP_URL));
				result.put(PurchaseTransactionOptions.PK_RATEPOINT_GENERICJURAPI_URL, optionService.findByPKSafe(PurchaseTransactionOptions.PK_RATEPOINT_GENERICJURAPI_URL));
				result.put(PurchaseTransactionOptions.PK_RATEPOINT_STREETLEVELRATE_URL, optionService.findByPKSafe(PurchaseTransactionOptions.PK_RATEPOINT_STREETLEVELRATE_URL));
				result.put(PurchaseTransactionOptions.PK_LOCATIONPREFERENCE, optionService.findByPKSafe(PurchaseTransactionOptions.PK_LOCATIONPREFERENCE));
				result.put(PurchaseTransactionOptions.PK_AUTOPROCFLAG, optionService.findByPKSafe(PurchaseTransactionOptions.PK_AUTOPROCFLAG));
				result.put(PurchaseTransactionOptions.PK_AUTOPROCID, optionService.findByPKSafe(PurchaseTransactionOptions.PK_AUTOPROCID));
				result.put(PurchaseTransactionOptions.PK_AUTOPROCAMT, optionService.findByPKSafe(PurchaseTransactionOptions.PK_AUTOPROCAMT));
				result.put(PurchaseTransactionOptions.PK_HOLDTRANSFLAG, optionService.findByPKSafe(PurchaseTransactionOptions.PK_HOLDTRANSFLAG));
				result.put(PurchaseTransactionOptions.PK_HOLDTRANSAMT, optionService.findByPKSafe(PurchaseTransactionOptions.PK_HOLDTRANSAMT));
				result.put(PurchaseTransactionOptions.PK_DELETE0AMT, optionService.findByPKSafe(PurchaseTransactionOptions.PK_DELETE0AMT));
				result.put(PurchaseTransactionOptions.PK_PROCRULEENABLED, optionService.findByPKSafe(PurchaseTransactionOptions.PK_PROCRULEENABLED));

				result.put(PurchaseTransactionOptions.PK_DEFAULTRATETYPE, optionService.findByPKSafe(PurchaseTransactionOptions.PK_DEFAULTRATETYPE));
				result.put(PurchaseTransactionOptions.PK_DEFAULTMOD, optionService.findByPKSafe(PurchaseTransactionOptions.PK_DEFAULTMOD));
				result.put(PurchaseTransactionOptions.PK_DEFAULTGEORECON, optionService.findByPKSafe(PurchaseTransactionOptions.PK_DEFAULTGEORECON));
				result.put(PurchaseTransactionOptions.PK_DEFAULTFRTINCITEM, optionService.findByPKSafe(PurchaseTransactionOptions.PK_DEFAULTFRTINCITEM));
				result.put(PurchaseTransactionOptions.PK_DEFAULTDISCINCITEM, optionService.findByPKSafe(PurchaseTransactionOptions.PK_DEFAULTDISCINCITEM));



				cacheAdministrator.putInCache(getKeyPerDb(key), result);
			}
			catch (Exception e) {
				logger.error(e);
			}
		}


		return result;
	}
	
	public List<SelectItem> createStateItems(String strCountry){
		List<SelectItem> selectItems = new ArrayList<SelectItem>();
		selectItems.add(new SelectItem("","Select State"));
		Collection<TaxCodeStateDTO> taxCodeStateDTOList = getTaxCodeStateMap().values();
		if(strCountry!=null && strCountry.length()>0){  
			for (TaxCodeStateDTO taxCodeStateDTO : taxCodeStateDTOList) {
			  if(strCountry.equalsIgnoreCase(taxCodeStateDTO.getCountry()) && ((taxCodeStateDTO.getActiveFlag() != null) && taxCodeStateDTO.getActiveFlag().equals("1")) ){
				  selectItems.add(new SelectItem(taxCodeStateDTO.getTaxCodeState(), 
						  taxCodeStateDTO.getName() + " (" + taxCodeStateDTO.getTaxCodeState() + ")"));
			  }
		  }
		}
	  
		return selectItems;
    }
  
    public List<SelectItem> createCountryItems() {
    	List<SelectItem> selectItems = new ArrayList<SelectItem>();
    	selectItems.add(new SelectItem("","Select Country"));
    	Map<String,String> countryMap = getCountryMap();
    	for (String countryCode : countryMap.keySet()) {
    		String countryName = countryMap.get(countryCode);
    		selectItems.add(new SelectItem(countryCode, countryName + " (" + countryCode + ")"));
    	}

    	return selectItems;
    }

	public void flushTaxCodeMap() {
		String key = "TAXCODE_MAP";
		cacheAdministrator.flushEntry(getKeyPerDb(key));
	}
	
	public String getKeyPerDb(String key){
		String dbKey = key;
		String selectedDb = ChangeContextHolder.getDataBaseType();
        if (selectedDb != null && selectedDb.length()>0) {
        	dbKey = selectedDb.toUpperCase() + "_" + key;     
        }
        
		return dbKey;
	}
	
	@SuppressWarnings("unchecked")
	public Map<String,TaxCode> getTaxCodeMap() {
		Map<String,TaxCode> result = null;
		String key = "TAXCODE_MAP";

		try {
			
			result = (Map<String,TaxCode>)cacheAdministrator.getFromCache(getKeyPerDb(key));
			
		} catch (NeedsRefreshException e) {
			
			try{
				result = new LinkedHashMap<String,TaxCode>();
				
				for (TaxCode code : taxCodeDetailService.getAllTaxCode()) {
					result.put(code.getTaxCodePK().getTaxcodeCode(), code);
				}
				
				logger.debug("Caching " + key + " Size: " + result.size());
				cacheAdministrator.putInCache(getKeyPerDb(key), result, new ExpiresRefreshPolicy(CACHE_EXPIRATION_SECONDS));
			
			} catch(Exception ex){
				ex.printStackTrace();
			}
		}
		
		return result;
	}

	public void flushCchCodeMap() {
		String key = "CCHCODE_MAP";
		cacheAdministrator.flushEntry(getKeyPerDb(key));
	}
	
	@SuppressWarnings("unchecked")
	public Map<String,CCHCode> getCchCodeMap() {
		Map<String,CCHCode> result = null;
		String key = "CCHCODE_MAP";

		try {
			
			result = (Map<String,CCHCode>)cacheAdministrator.getFromCache(getKeyPerDb(key));
			
		} catch (NeedsRefreshException e) {
			
			try{
				result = new LinkedHashMap<String,CCHCode>();
				
				for (CCHCode code : taxCodeDetailService.getAllCCHCode()) {
					result.put(code.getCodePK().getCode(), code);
				}
				
				logger.debug("Caching " + key + " Size: " + result.size());
				cacheAdministrator.putInCache(getKeyPerDb(key), result, new ExpiresRefreshPolicy(CACHE_EXPIRATION_SECONDS));
			
			} catch(Exception ex){
				ex.printStackTrace();
			}
		}
		
		return result;
	}

	public void flushCchGroupMap() {
		String key = "CCHGROUP_MAP";
		cacheAdministrator.flushEntry(getKeyPerDb(key));
	}
	
	@SuppressWarnings("unchecked")
	public Map<String,CCHTaxMatrix> getCchGroupMap() {
		Map<String,CCHTaxMatrix> result = null;
		String key = "CCHGROUP_MAP";

		try {
			
			result = (Map<String,CCHTaxMatrix>)cacheAdministrator.getFromCache(getKeyPerDb(key));
			
		} catch (NeedsRefreshException e) {
			
			try{
				result = new LinkedHashMap<String,CCHTaxMatrix>();
				
				for (CCHTaxMatrix code : taxCodeDetailService.getAllCCHGroup()) {
					result.put(code.getCchTaxMatrixPK().getGroupCode(), code);
				}
				
				logger.debug("Caching " + key + " Size: " + result.size());
				cacheAdministrator.putInCache(getKeyPerDb(key), result, new ExpiresRefreshPolicy(CACHE_EXPIRATION_SECONDS));
			
			} catch(Exception ex){
				ex.printStackTrace();
			}
		}
		
		return result;
	}

	@SuppressWarnings("unchecked")
	public Map<String,String> getLocalTaxabilityCodeMap() {
		Map<String,String> result = null;
		String key = "LOCALTAXABILITYCODE_MAP";
		try {
			result = (Map<String,String>)cacheAdministrator.getFromCache(getKeyPerDb(key));
			
		} catch (NeedsRefreshException e) {			
			try{
				result = new LinkedHashMap<String,String>();
				
				List<ListCodes> listCodes = listCodesService.getListCodesByCodeTypeCode("LOCALTAX");
				for(ListCodes listCode : listCodes){
					result.put(listCode.getCodeCode(), listCode.getDescription());
		    	}
				
				logger.debug("Caching " + key + " Size: " + result.size());
				cacheAdministrator.putInCache(getKeyPerDb(key), result, new ExpiresRefreshPolicy(CACHE_EXPIRATION_SECONDS));
			
			} catch(Exception ex){
				ex.printStackTrace();
			}
		}
		
		return result;
	}
	@SuppressWarnings("unchecked")
	public Map<String,String> getTaxCodeTaxabilityCodeMap() {
		Map<String,String> result = null;
		String key = "TAXABILITYCODE_MAP";
		try {
			result = (Map<String,String>)cacheAdministrator.getFromCache(getKeyPerDb(key));
			
		} catch (NeedsRefreshException e) {			
			try{
				result = new LinkedHashMap<String,String>();
				
				List<ListCodes> listCodes = listCodesService.getListCodesByCodeTypeCode("TCTYPE");
				for(ListCodes listCode : listCodes){
					result.put(listCode.getCodeCode(), listCode.getDescription());
		    	}
				
				logger.debug("Caching " + key + " Size: " + result.size());
				cacheAdministrator.putInCache(getKeyPerDb(key), result, new ExpiresRefreshPolicy(CACHE_EXPIRATION_SECONDS));
			
			} catch(Exception ex){
				ex.printStackTrace();
			}
		}
		
		return result;
	}
	public List<SelectItem> createTaxabilityCodeItems() {
    	List<SelectItem> selectItems = new ArrayList<SelectItem>();
    	selectItems.add(new SelectItem("","Select Taxability"));
    	Map<String,String> taxabilityCodeMap = getTaxCodeTaxabilityCodeMap();
    	for (String taxabilityCode : taxabilityCodeMap.keySet()) {
    		String description = taxabilityCodeMap.get(taxabilityCode);
    		selectItems.add(new SelectItem(taxabilityCode, description + " (" + taxabilityCode + ")"));
    	}

    	return selectItems;
    }

	public Map<String, String> getRefTypeCodeDescMap() {
		Map<String, String> refTypeCodeDescMap = new LinkedHashMap<String, String>();
		for (ListCodes code : getListCodesByType("REFTYPE")) {
			refTypeCodeDescMap.put(code.getCodeCode(), code.getDescription());
		}
		return refTypeCodeDescMap;
	}
	
	public List<SelectItem> createRefTypeItems() {
    	List<SelectItem> selectItems = new ArrayList<SelectItem>();
    	selectItems.add(new SelectItem("","Select Reference Type"));
    	Map<String,String> refTypeCodeDescMap = getRefTypeCodeDescMap();
    	for (Map.Entry<String, String> e : refTypeCodeDescMap.entrySet()) {
    		selectItems.add(new SelectItem(e.getKey(), e.getKey() + " - " + e.getValue()));
    	}

    	return selectItems;
    }
	
	public List<SelectItem> createMeasureTypeItems() {
		List<SelectItem> selectItems = new ArrayList<SelectItem>();
		selectItems.add(new SelectItem("","Select Measure Type"));
		List<ListCodes> listCodes = getListCodesByType("MEASURETYP");
		if(listCodes != null) {
			for(ListCodes lc : listCodes) {
				selectItems.add(new SelectItem(lc.getCodeCode(), lc.getDescription()));
			}
		}
		return selectItems;
	}
	
	public List<SelectItem> createCountyLocalItems() {
		List<SelectItem> selectItems = new ArrayList<SelectItem>();
		List<ListCodes> listCodes = getListCodesByType("STJLOCALCD");
		if(listCodes != null) {
			for(ListCodes lc : listCodes) {
				selectItems.add(new SelectItem(lc.getCodeCode(), lc.getDescription()));
			}
		}
		return selectItems;
	}
	
	public List<SelectItem> createRateTypeItems() {
		List<SelectItem> selectItems = new ArrayList<SelectItem>();
		selectItems.add(new SelectItem("","Select Rate Type"));
		List<ListCodes> listCodes = getListCodesByType("RATETYPE");
		if(listCodes != null) {
			for(ListCodes lc : listCodes) {
				selectItems.add(new SelectItem(lc.getCodeCode(), lc.getDescription()));
			}
		}
		return selectItems;
	}
	
	public List<SelectItem> createNexusTypeItems() {
		List<SelectItem> nexusTypes = new ArrayList<SelectItem>();
		List<ListCodes> listCodes = getListCodesByType("NEXUSTYPE");
		nexusTypes.add(new SelectItem("", "Select Default Nexus Type"));
		if(listCodes != null) {
			for(ListCodes lc : listCodes) {
				nexusTypes.add(new SelectItem(lc.getCodeCode(), lc.getDescription()));
			}
		}
		
		return nexusTypes;
	}
	
	public List<SelectItem> createNexusIndItems() {
		List<SelectItem> nexusTypes = new ArrayList<SelectItem>();
		List<ListCodes> listCodes = getListCodesByType("NEXUSIND");
		//0003089
		//nexusTypes.add(new SelectItem("", "Select Nexus"));
		if(listCodes != null) {
			for(ListCodes lc : listCodes) {
				nexusTypes.add(new SelectItem(lc.getCodeCode(), lc.getDescription()));
			}
		}
		
		return nexusTypes;
	}
	
	public List<SelectItem> createRegistrationTypeItems() {
		List<SelectItem> nexusTypes = new ArrayList<SelectItem>();
		List<ListCodes> listCodes = getListCodesByType("REGTYPE");
		nexusTypes.add(new SelectItem("", "Select a Registration Type"));
		if(listCodes != null) {
			for(ListCodes lc : listCodes) {
				nexusTypes.add(new SelectItem(lc.getCodeCode(), lc.getDescription()));
			}
		}
		
		return nexusTypes;
	}
	
	public List<SelectItem> createImportDefTypeItems() {
		List<SelectItem> importDefTypes = new ArrayList<SelectItem>();
		List<ListCodes> listCodes = getListCodesByType("BATCHTYPE");
		importDefTypes.add(new SelectItem("", "Select an Import Definition Type"));
		if(listCodes != null) {
			for(ListCodes lc : listCodes) {
				importDefTypes.add(new SelectItem(lc.getCodeCode(), lc.getCodeCode()+ "-" + lc.getDescription()));
			}
		}
		
		return importDefTypes;
	}
	public List<SelectItem> createImportDefTypeItemsChange() {
		List<SelectItem> importDefTypes = new ArrayList<SelectItem>();
		List<ListCodes> listCodes = getListCodesByType("IMPDEFTYPE");
		
		importDefTypes.add(new SelectItem("", "Select an Import Definition Type"));
		if(listCodes != null) {
			for(ListCodes lc : listCodes) {
				
				importDefTypes.add(new SelectItem(lc.getCodeCode(), lc.getCodeCode()+ "-" + lc.getDescription()));
			}
		}
		
		return importDefTypes;
	}
	
	@SuppressWarnings("unchecked")
	public Map<String,TaxCodeStateDTO> getTaxCodeCountryStateMap() {
		Map<String,TaxCodeStateDTO> result = null;
		String key = "TAXCODECOUNTRYSTATE_MAP";

		try {
			
			result = (Map<String,TaxCodeStateDTO>)cacheAdministrator.getFromCache(getKeyPerDb(key));
			
		} catch (NeedsRefreshException e) {
			
			try{
				Map<String, String> localTaxMap = getLocalTaxabilityCodeMap();
				
				result = new LinkedHashMap<String,TaxCodeStateDTO>();
				
				for (TaxCodeStateDTO state : taxCodeStateService.getAllTaxCodeStateDTO()) {
					state.setLocalTaxabilityCodeDesc(localTaxMap.get(state.getLocalTaxabilityCode()));
					state.setCpCertLevelCodeDesc(localTaxMap.get(state.getCpCertLevelCode()));
					result.put(state.getCountry() + ":" + state.getTaxCodeState(), state);
				}
				
				logger.debug("Caching " + key + " Size: " + result.size());
				cacheAdministrator.putInCache(getKeyPerDb(key), result, new ExpiresRefreshPolicy(CACHE_EXPIRATION_SECONDS));
			
			} catch(Exception ex){
				ex.printStackTrace();
			}
		}
		
		return result;
	}
	
	public List<SelectItem> createCountryStateItems(String strCountry){
		List<SelectItem> selectItems = new ArrayList<SelectItem>();
		
		if(strCountry!=null && strCountry.equalsIgnoreCase("*ALL")) {
			selectItems.add(new SelectItem("*ALL","All States (*ALL)"));
			return selectItems;
		}
		
		selectItems.add(new SelectItem("","Select State"));
		Collection<TaxCodeStateDTO> taxCodeStateDTOList = getTaxCodeCountryStateMap().values();
		if(strCountry!=null && strCountry.length()>0){  
			for (TaxCodeStateDTO taxCodeStateDTO : taxCodeStateDTOList) {
			  if(strCountry.equalsIgnoreCase(taxCodeStateDTO.getCountry()) && ((taxCodeStateDTO.getActiveFlag() != null) && taxCodeStateDTO.getActiveFlag().equals("1")) ){
				  
				  if(taxCodeStateDTO.getTaxCodeState()!=null && taxCodeStateDTO.getTaxCodeState().equalsIgnoreCase("*ALL")) {
					  selectItems.add(1,new SelectItem(taxCodeStateDTO.getTaxCodeState(), 
							  taxCodeStateDTO.getName() + " (" + taxCodeStateDTO.getTaxCodeState() + ")"));
				  }
				  else {
					  selectItems.add(new SelectItem(taxCodeStateDTO.getTaxCodeState(), 
							  taxCodeStateDTO.getName() + " (" + taxCodeStateDTO.getTaxCodeState() + ")"));
				  }
			  }
		  }
		}
	  
		return selectItems;
    }
}