package com.ncsts.common;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class VirtualMap<K,V> implements Map<K,V> {

	private Map<K,V> map = new HashMap<K,V>();
	private V defaultValue;
	
	public VirtualMap(V defaultValue) {
		this.defaultValue = defaultValue;
	}
	
	public void clear() {
		map.clear();
	}

	public boolean containsKey(Object key) {
		// We pretend to contain all keys
		return true;
	}

	public boolean containsValue(Object value) {
		// Nonsense method for our purposes
		return true;
	}

	public Set<Map.Entry<K, V>> entrySet() {
		return map.entrySet();
	}

	public V get(Object key) {
		return map.containsKey(key)? map.get(key):defaultValue;
	}

	public boolean isEmpty() {
		return map.isEmpty();
	}

	public Set<K> keySet() {
		return map.keySet();
	}

	public V put(K key, V value) {
		V result = get(key);
		if ((value == null) || (value.equals(defaultValue))) {
			map.remove(key);
		} else {
			map.put(key, value);
		}
		return result;
	}

	public void putAll(Map<? extends K, ? extends V> t) {
		for (K key : t.keySet()) {
			put(key, t.get(key));
		}
	}

	public V remove(Object key) {
		return map.remove(key); 
	}

	public int size() {
		return map.size();
	}

	public Collection<V> values() {
		// Nonsense method for us
		return null;
	}
}
