package com.ncsts.management;

import java.sql.*;
import java.util.*;
import java.util.Date;
import java.io.*;

import org.quartz.SchedulerException;
import org.quartz.SimpleTrigger;
import org.quartz.impl.StdScheduler;

import com.ncsts.domain.Option;
import com.ncsts.domain.OptionCodePK;

public class SQLiteConnectionPool {

   private String name;
   private String URL;
   private String driverClassName;
   private String user;
   private String password;
   private int maxConn;
   private int checkedOut;
   
   final String CHECK_SQL_QUERY = "SELECT 1"; 				//PostgreSQL, SQLite 
   final String CHECK_ORACLE_QUERY = "SELECT 1 FROM DUAL"; 	//Oracle
   
   private Vector<Connection> freeConnections;

   public SQLiteConnectionPool(String name, String URL, String driverClassName, String user,
           String password, int maxConn) {
	   this.name = name;
       this.URL = URL;
       this.driverClassName = driverClassName;
       this.user = user;
       this.password = password;
       this.maxConn = maxConn;
       
       init();
   }
   
   private void init(){
	   freeConnections = new Vector<Connection>(maxConn);
       
       try {
			Class.forName(driverClassName);
	   } catch(java.lang.ClassNotFoundException e) {
			System.out.println("############ ClassNotFoundException: " + e.getMessage());
	   }
   }
   
   public boolean isDbConnected(Connection con) {
	    boolean isConnected = false;
	    Statement statement = null;
	    try {
	    	statement = con.createStatement();     
	    	statement.execute((name.toLowerCase().indexOf("oracle")>=0) ? CHECK_ORACLE_QUERY : CHECK_SQL_QUERY);
	        isConnected = true;
	    } catch (SQLException e) {
	        // handle SQL error here!
	    	System.out.println("############ Cannot execute " + ((name.toLowerCase().indexOf("oracle")>=0) ? CHECK_ORACLE_QUERY : CHECK_SQL_QUERY) + " : " 
	    			+ e.getMessage() + ", found on " + new Date().toString());
	    	System.out.println("############ Cannot connect to " + URL);
	    }
	    finally{
	    	if(statement!=null){try{statement.close();}catch(Exception e){}}
	    }
	    
	    return isConnected;
   }

   public synchronized Connection getConnection() {
       Connection con = null;
       if (freeConnections.size() > 0) {
           // Pick the first Connection in the Vector
           // to get round-robin usage
           con = (Connection) freeConnections.firstElement();
           freeConnections.removeElementAt(0);
           try {
        	   if (con.isClosed()) {
                   System.out.println("############ Connection is closed via checking isClosed().");
                   con=null;
                   release();
                   con = newConnection();
               }
        	   else if (!isDbConnected(con)) {
                   System.out.println("############ Connection is invalid via checking isDbConnected().");
                   con=null;
                   release();
                   con = newConnection();
               }
           }
           catch (SQLException e) {
        	   con=null;
               release();
               con = newConnection();
           }
       }
       else if (maxConn == 0 || checkedOut < maxConn) {
           con = newConnection();
       }
       
       if (con != null) {
           checkedOut++;
       }
       
       return con;
   }
   
   public synchronized Connection getConnection(long timeout) {
       long startTime = new Date().getTime();
       Connection con;
       while ((con = getConnection()) == null) {
           try {
               wait(timeout);
           }
           catch (InterruptedException e) {}
           if ((new Date().getTime() - startTime) >= timeout) {
               // Timeout has expired
               return null;
           }
       }
       return con;
   }
 
   private Connection newConnection() {
	   System.out.println("############ Current # of connections checkout:  " + checkedOut);
	   System.out.println("############ Current # of connections in cache:  " + freeConnections.size());  
       Connection con = null;
       try {
           if (user == null || user.length()==0) {
               con = DriverManager.getConnection(URL);
           }
           else {
               con = DriverManager.getConnection(URL, user, password);
           }
           System.out.println("############ Created a new connection in pool " + name + " on " + new Date().toString());
       }
       catch (SQLException e) {
    	   System.out.println("############ Cannot create a new connection for " + URL + " : " + e.getMessage());
    	   
           return null;
       }
       return con;
   }

   public synchronized void freeConnection(Connection con) {
       // Put the connection at the end of the Vector
       freeConnections.addElement(con);
       checkedOut--;
       notifyAll();
   }
   
   public synchronized Connection renewConnection(Connection con) {
	   try{
		   if (!con.isClosed()){
			   con.close();	   
			   System.out.println("############ Renew connection from " + name);
		   }
	   }
	   catch (SQLException e) {
       }
	   
	   return newConnection();
   }
   
   protected void finalize() throws Throwable {
	    try {
	    	release(); // close open connections
	    } finally {
	        super.finalize();
	    }
   }

   public synchronized void release() {
	   System.out.println("############ Current # of connections checkout:  " + checkedOut);
	   System.out.println("############ Current # of connections in cache released:  " + freeConnections.size());
	   
       Enumeration<Connection> allConnections = freeConnections.elements();
       while (allConnections.hasMoreElements()) {
           Connection con = (Connection) allConnections.nextElement();
           try {
               con.close();
               con=null;
           }
           catch (SQLException e) {
           }
       }
       freeConnections.removeAllElements();
       checkedOut=0;
       notifyAll();
   }
   
   public synchronized void connectionValidation() {
	   if(freeConnections==null){
		   return;
	   }

       Enumeration<Connection> allConnections = freeConnections.elements();
       boolean needRelease = false;
       while (allConnections.hasMoreElements()) {
           Connection con = (Connection) allConnections.nextElement(); 	   
           if (!isDbConnected(con)) {
        	   System.out.println("############ Connection is invalid via checking SQLiteConnectionPool.connectionValidation() at " + new Date());               
               needRelease = true;
               break;
           }
       }
       
       if(needRelease){
    	   release();
       }
   }

}
