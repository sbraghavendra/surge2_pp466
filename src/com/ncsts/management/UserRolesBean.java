package com.ncsts.management;

import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.security.SecureRandom;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;
import java.util.Date;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.component.html.HtmlInputSecret;
import javax.faces.context.FacesContext;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.ActionEvent;
import javax.servlet.http.HttpSession;

import org.ajax4jsf.component.html.HtmlAjaxSupport;
import org.apache.log4j.Logger;
import org.richfaces.component.html.HtmlScrollableDataTable;
import org.richfaces.model.selection.Selection;
import org.richfaces.model.selection.SimpleSelection;
import org.richfaces.component.html.HtmlDataTable;
import org.springframework.dao.DataAccessException;

import com.ncsts.common.LoggerFactory;
import com.ncsts.domain.DataStatistics;
import com.ncsts.domain.TaxMatrix;
import com.ncsts.dto.DataStatisticsDTO;
import com.ncsts.dto.DbPropertiesDTO;
import com.ncsts.dto.PasswordHistoryDTO;
import com.ncsts.dto.SmtpPropertiesDTO;
import com.ncsts.dto.UserDTO;
import com.ncsts.dto.UserRolesDTO;
import com.ncsts.dto.UserSecurityDTO;
import com.ncsts.services.DataStatisticsService;
import com.ncsts.view.bean.EditAction;
import com.ncsts.view.bean.RoutingDataSource;

public class UserRolesBean {
	
    private Logger logger = LoggerFactory.getInstance().getLogger(UserRolesBean.class);	
    
    private EditAction currentUserRolesAction = EditAction.VIEW;
    private List<UserRolesDTO> userRolesList;
    private UserRolesDTO selectedUserRoles;
    private UserRolesDTO userRolesDTOWork; 
    private int selectedUserRolesRowIndex = -1;
    
    private RoutingDataSource dataSource;

    public void setDataSource (RoutingDataSource dataSource) {
		this.dataSource = dataSource;
	}
    
    public UserRolesDTO getUserRolesDTOWork() {
		return userRolesDTOWork;
	}

	public void setUserRolesDTOWork(UserRolesDTO userRolesDTOWork) {
		this.userRolesDTOWork = userRolesDTOWork;
	}

    public String getActionUserRolesText() {
		if(currentUserRolesAction.equals(EditAction.ADD)){
			return "Add";
		}
		else if(currentUserRolesAction.equals(EditAction.UPDATE)){
			return "Update";
		}
		else if(currentUserRolesAction.equals(EditAction.DELETE)){
			return "Delete";
		}
		
		return "";
	}
    
    public boolean getIsUserRolesAddAction() {
		return currentUserRolesAction.equals(EditAction.ADD);
	}
    
    public boolean getIsUserRolesUpdateAction() {
		return currentUserRolesAction.equals(EditAction.UPDATE);
	}
    
    public boolean getIsUserRolesDeleteAction() {
		return currentUserRolesAction.equals(EditAction.DELETE);
	}
    
    public boolean getIsUserRolesViewAction() {
  		return currentUserRolesAction.equals(EditAction.VIEW);
  	}
    
    public int getSelectedUserRolesRowIndex() {
		return selectedUserRolesRowIndex;
	}

	public void setSelectedUserRolesRowIndex(int selectedUserRolesRowIndex) {
		this.selectedUserRolesRowIndex = selectedUserRolesRowIndex;
	}

    public void selectedRowChanged(ActionEvent e) throws AbortProcessingException { 
		HtmlDataTable table = (HtmlDataTable) ((HtmlAjaxSupport) e.getComponent()).getParent(); 
		selectedUserRolesRowIndex = table.getRowIndex();
		selectedUserRoles = (UserRolesDTO)table.getRowData();
	}
    
    public boolean isShowUserRolesButtons() {
		return (selectedUserRolesRowIndex != -1);
	}
    
    public String displayAddUserRolesAction() {
    	currentUserRolesAction = EditAction.ADD;
    	userRolesDTOWork = new UserRolesDTO(); 
		if(selectedUserRoles!=null){
			userRolesDTOWork.setAdmin(0);
		}
		
		return "userroles_update";
	}
    
    public String displayUpdateUserRolesAction() {
    	currentUserRolesAction = EditAction.UPDATE;
    	userRolesDTOWork = new UserRolesDTO(); 
		if(selectedUserRoles!=null){
			userRolesDTOWork.setRoleId(selectedUserRoles.getRoleId());
			userRolesDTOWork.setRoleName(selectedUserRoles.getRoleName());	
			userRolesDTOWork.setPasswordHistory(selectedUserRoles.getPasswordHistory());
			userRolesDTOWork.setInvalidAttempts(selectedUserRoles.getInvalidAttempts());
			userRolesDTOWork.setNotificationDays(selectedUserRoles.getNotificationDays());
			userRolesDTOWork.setExpirationDays(selectedUserRoles.getExpirationDays());
			userRolesDTOWork.setAdmin(selectedUserRoles.getAdmin());
		}
		
		return "userroles_update";
	}
    
    public String displayViewUserRolesAction() {
    	String returnScreen =  displayUpdateUserRolesAction();
    	currentUserRolesAction = EditAction.VIEW;
    	return returnScreen;
    	
    }
    
    public String displayDeleteUserRolesAction() {
    	currentUserRolesAction = EditAction.DELETE;
    	userRolesDTOWork = new UserRolesDTO(); 
		if(selectedUserRoles!=null){
			userRolesDTOWork.setRoleId(selectedUserRoles.getRoleId());
			userRolesDTOWork.setRoleName(selectedUserRoles.getRoleName());	
			userRolesDTOWork.setPasswordHistory(selectedUserRoles.getPasswordHistory());
			userRolesDTOWork.setInvalidAttempts(selectedUserRoles.getInvalidAttempts());
			userRolesDTOWork.setNotificationDays(selectedUserRoles.getNotificationDays());
			userRolesDTOWork.setExpirationDays(selectedUserRoles.getExpirationDays());
			userRolesDTOWork.setAdmin(selectedUserRoles.getAdmin());
		}
		
		return "userroles_update";
	}

    public String okUserRolesAction() {    
    	if(currentUserRolesAction.equals(EditAction.ADD)){
	    	
	    	if(userRolesDTOWork.getRoleName()==null || userRolesDTOWork.getRoleName().length()==0){
				addMessage("Role Name can not be empty.");
				return null;
			}
	    	
	    	UserRolesDTO userRolesDTO = getUserRoles(userRolesDTOWork.getRoleName());
			if(userRolesDTO!=null && userRolesDTO.getRoleId()!=userRolesDTOWork.getRoleId()){
				addMessage("Role Name has already existed.");
				return null;
			}

	    	String errorMessage = addUserRoles(userRolesDTOWork);
	    	if(errorMessage!=null && errorMessage.length()>0){
	    		addMessage(errorMessage);
	    		return null;
	    	}

			logger.info("end updateAction");		
	    }
		else if(currentUserRolesAction.equals(EditAction.UPDATE)){
	    	
	    	if(userRolesDTOWork.getRoleName()==null || userRolesDTOWork.getRoleName().length()==0){
				addMessage("Role Name can not be empty.");
				return null;
			}
	    	
	    	UserRolesDTO userRolesDTO = getUserRoles(userRolesDTOWork.getRoleName());
			if(userRolesDTO!=null && userRolesDTO.getRoleId()!=userRolesDTOWork.getRoleId()){
				addMessage("Role Name has already existed.");
				return null;
			}

	    	String errorMessage = updateUserRoles(userRolesDTOWork);
	    	if(errorMessage!=null && errorMessage.length()>0){
	    		addMessage(errorMessage);
	    		return null;
	    	}

			logger.info("end updateAction");		
	    }
		else if(currentUserRolesAction.equals(EditAction.DELETE)){
			if(isUserRolesInUse(userRolesDTOWork.getRoleId())){
				addMessage("Role Name is in use. Cannot be deleted.");
				return null;
			}

	    	String errorMessage = deleteUserRoles(userRolesDTOWork);
	    	if(errorMessage!=null && errorMessage.length()>0){
	    		addMessage(errorMessage);
	    		return null;
	    	}

			logger.info("end updateAction");		
	    }
	
	    userRolesList = null;
	    selectedUserRoles = null;
		selectedUserRolesRowIndex = -1;
	      
	    return "userroles_main"; 
	}
    
    public UserRolesDTO getUserRoles(String roleName) throws DataAccessException {
    	UserRolesDTO userRolesDTO = null;
		Connection conn = dataSource.CreateSQLiteConnection();
		if(conn==null){
			logger.error("############ JDBC Connection failed");
			return null;
		}
		
		Statement stat = null;
		try{
			stat = conn.createStatement();
	    	ResultSet rs = stat.executeQuery("select * from TB_USER_ROLES where UPPER(ROLE_NAME) = UPPER('" + roleName + "')");
	    	if (rs.next()) {
	    		userRolesDTO = new UserRolesDTO(); 
	    		userRolesDTO.setPasswordHistory(rs.getInt("PASSWORD_HISTORY"));
	    		userRolesDTO.setInvalidAttempts(rs.getInt("INVALID_ATTEMPTS"));
	    		userRolesDTO.setNotificationDays(rs.getInt("NOTIFICATION_DAYS"));
	    		userRolesDTO.setExpirationDays(rs.getInt("EXPIRATION_DAYS"));
	    		userRolesDTO.setRoleId(rs.getInt("ROLE_ID"));
	    		userRolesDTO.setRoleName(rs.getString("ROLE_NAME"));
	    		userRolesDTO.setAdmin(rs.getInt("ADMIN"));
	    	}
	    	rs.close();	
		}
		catch (Exception e){
			logger.error("############ getUserRoles() failed: " + e.toString()); 
			conn = dataSource.RenewSQLiteConnection(conn);	 
		}
		finally{
			if(stat!= null){try{stat.close();}catch(Exception ex){}}
			dataSource.CloseSQLiteConnection(conn);
		}
		
		return userRolesDTO;
	}
    
    public String cancelUserRolesAction() {
		return "userroles_main";
	}
    
    private void addMessage(String errorMessage){
		FacesMessage message = new FacesMessage(errorMessage);
		message.setSeverity(FacesMessage.SEVERITY_ERROR);
		FacesContext.getCurrentInstance().addMessage(null, message);
	}
    
    public String addUserRoles(UserRolesDTO userRolesDTO) throws DataAccessException {
    	String errorMessage = ""; 
    	Connection conn = dataSource.CreateSQLiteConnection();
    	if(conn==null){
    		logger.error("############ JDBC Connection failed");
    		return "Connection failed";
    	}
    	
    	PreparedStatement prep = null;
		 try{
			 prep = conn.prepareStatement("insert into TB_USER_ROLES (PASSWORD_HISTORY, INVALID_ATTEMPTS, NOTIFICATION_DAYS, EXPIRATION_DAYS, ROLE_NAME, ADMIN) values (?, ?, ?, ?, ?, ?)");

			 prep.setInt(1, userRolesDTO.getPasswordHistory());
			 prep.setInt(2, userRolesDTO.getInvalidAttempts());
			 prep.setInt(3, userRolesDTO.getNotificationDays());
			 prep.setInt(4, userRolesDTO.getExpirationDays());
			 prep.setString(5, userRolesDTO.getRoleName());
			 prep.setInt(6, userRolesDTO.getAdmin());

			 conn.setAutoCommit(false);
			 prep.executeUpdate();
			 
			 conn.commit();
	    	 conn.setAutoCommit(true);
		 }
		 catch (Exception e){
			 errorMessage = "Cannot add role, please try again!"; 
			 try{
				 conn.rollback();
			 }
			 catch(Exception ex){
			 }
			 logger.error("############ addUserRoles() failed: " + e.toString()); 
			 conn = dataSource.RenewSQLiteConnection(conn);	 
		 }
		 finally{
			 if(prep!=null){try{prep.close();}catch(Exception ex){}}
			 dataSource.CloseSQLiteConnection(conn);
		 }
		 
		 return errorMessage;
	}
    
    public String updateUserRoles(UserRolesDTO userRolesDTO) throws DataAccessException {
    	String errorMessage = ""; 
    	Connection conn = dataSource.CreateSQLiteConnection();
    	if(conn==null){
    		logger.error("############ JDBC Connection failed");
    		return "Connection failed";
    	}
    	
    	PreparedStatement prep = null;
		 try{
			 prep = conn.prepareStatement("UPDATE TB_USER_ROLES SET PASSWORD_HISTORY = ? , INVALID_ATTEMPTS = ? , NOTIFICATION_DAYS = ? , EXPIRATION_DAYS = ? , ROLE_NAME = ? , ADMIN = ? WHERE ROLE_ID = ?");

			 prep.setInt(1, userRolesDTO.getPasswordHistory());
			 prep.setInt(2, userRolesDTO.getInvalidAttempts());
			 prep.setInt(3, userRolesDTO.getNotificationDays());
			 prep.setInt(4, userRolesDTO.getExpirationDays());
			 prep.setString(5, userRolesDTO.getRoleName());
			 prep.setInt(6, userRolesDTO.getAdmin());
			 prep.setInt(7, userRolesDTO.getRoleId());

			 conn.setAutoCommit(false);
			 prep.executeUpdate();
			 
			 conn.commit();
	    	 conn.setAutoCommit(true);
		 }
		 catch (Exception e){
			 errorMessage = "Cannot update role, please try again!";  
			 try{
				 conn.rollback();
			 }
			 catch(Exception ex){
			 }
			 logger.error("############ updateUserRoles() failed: " + e.toString()); 
			 conn = dataSource.RenewSQLiteConnection(conn);	 
		 }
		 finally{
			 if(prep!=null){try{prep.close();}catch(Exception ex){}}
			 dataSource.CloseSQLiteConnection(conn);
		 }
		 return errorMessage;
	}
    
    public String deleteUserRoles(UserRolesDTO userRolesDTO) throws DataAccessException {
		String errorMessage = ""; 
		Connection conn = dataSource.CreateSQLiteConnection();
		if(conn==null){
			logger.error("############ JDBC Connection failed");
			return "Connection failed";
		}
		
		PreparedStatement prep = null;
		 try{
			 conn.setAutoCommit(false);
			 
			 //Delete user role
			 prep = conn.prepareStatement("DELETE FROM TB_USER_ROLES WHERE ROLE_ID = ?");
			 prep.setInt(1, userRolesDTO.getRoleId());		 
			 prep.executeUpdate();

			 conn.commit();
	    	 conn.setAutoCommit(true);
		 }
		 catch (Exception e){
			 errorMessage = "Cannot delete role, please try again!"; 
			 try{
				 conn.rollback();
			 }
			 catch(Exception ex){
			 }
			 logger.error("############ deleteUserRoles() failed: " + e.toString()); 
			 conn = dataSource.RenewSQLiteConnection(conn);	 
		 }
		 finally{
			 if(prep!=null){try{prep.close();}catch(Exception ex){}}
			 dataSource.CloseSQLiteConnection(conn);
		 }
		 return errorMessage;
	}
    
    public boolean isUserRolesInUse(int roleId) throws DataAccessException {
    	boolean roleInUse = false;
		Connection conn = dataSource.CreateSQLiteConnection();
		if(conn==null){
			logger.error("############ JDBC Connection failed");
			return true;
		}
		
		Statement stat = null;
		try{
			stat = conn.createStatement();
	    	ResultSet rs = stat.executeQuery("select * from TB_USER_SECURITY where ROLE_ID = " + roleId);
	    	if (rs.next()) {
	    		roleInUse = true;
	    	}
	    	rs.close();	
		}
		catch (Exception e){
			logger.error("############ getUserRoles() failed: " + e.toString()); 
			conn = dataSource.RenewSQLiteConnection(conn);	 
		}
		finally{
			if(stat!= null){try{stat.close();}catch(Exception ex){}}
			dataSource.CloseSQLiteConnection(conn);
		}
		
		return roleInUse;
	}
    
    public List<UserRolesDTO> getUserRolesList() {
		if (userRolesList == null) {
			userRolesList = findAllRolesList();
			selectedUserRolesRowIndex = -1;
		}
		return userRolesList;
	}

    public void setUserRolesList(List<UserRolesDTO> userRolesList) {
		this.userRolesList = userRolesList;
	}

    public List<UserRolesDTO> findAllRolesList() throws DataAccessException {
		List<UserRolesDTO> result = new ArrayList<UserRolesDTO>();
		Connection conn = dataSource.CreateSQLiteConnection();
		if(conn==null){
			logger.error("############ JDBC Connection failed");
			return result;
		}
		
		Statement stat = null;
		try{
			stat = conn.createStatement();
	    	ResultSet rs = stat.executeQuery("select * from TB_USER_ROLES order by ROLE_ID asc");
	    	while (rs.next()) {
	    		UserRolesDTO userRolesDTO = new UserRolesDTO(); 
	    		userRolesDTO.setPasswordHistory(rs.getInt("PASSWORD_HISTORY"));
	    		userRolesDTO.setInvalidAttempts(rs.getInt("INVALID_ATTEMPTS"));
	    		userRolesDTO.setNotificationDays(rs.getInt("NOTIFICATION_DAYS"));
	    		userRolesDTO.setExpirationDays(rs.getInt("EXPIRATION_DAYS"));
	    		userRolesDTO.setRoleId(rs.getInt("ROLE_ID"));
	    		userRolesDTO.setRoleName(rs.getString("ROLE_NAME"));
	    		userRolesDTO.setAdmin(rs.getInt("ADMIN"));
	    		result.add(userRolesDTO);   
	    	}
	    	rs.close();	
		}
		catch (Exception e){
			logger.error("############ findAllRolesList() failed: " + e.toString()); 
			conn = dataSource.RenewSQLiteConnection(conn);	 
		}
		finally{
			if(stat!= null){try{stat.close();}catch(Exception ex){}}
			dataSource.CloseSQLiteConnection(conn);
		}
		
		return result;
	}
}
