package com.ncsts.management;

import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.security.SecureRandom;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;
import java.util.Date;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.component.html.HtmlInputSecret;
import javax.faces.context.FacesContext;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;

import oracle.jdbc.OracleTypes;

import org.ajax4jsf.component.html.HtmlAjaxSupport;
import org.apache.log4j.Logger;
import org.richfaces.component.html.HtmlScrollableDataTable;
import org.richfaces.model.selection.Selection;
import org.richfaces.model.selection.SimpleSelection;
import org.richfaces.component.html.HtmlDataTable;
import org.springframework.dao.DataAccessException;

import com.ncsts.common.LoggerFactory;
import com.ncsts.domain.DataStatistics;
import com.ncsts.domain.TaxMatrix;
import com.ncsts.domain.UserSecurity;
import com.ncsts.dto.DataStatisticsDTO;
import com.ncsts.dto.DbPropertiesDTO;
import com.ncsts.dto.PasswordHistoryDTO;
import com.ncsts.dto.UserDTO;
import com.ncsts.dto.UserRolesDTO;
import com.ncsts.dto.SmtpPropertiesDTO;
import com.ncsts.dto.UserSecurityDTO;
import com.ncsts.services.DataStatisticsService;
import com.ncsts.view.bean.EditAction;
import com.ncsts.view.bean.RoutingDataSource;

public class DbPropertiesBean {
	
    private Logger logger = LoggerFactory.getInstance().getLogger(DbPropertiesBean.class);	
    
    private EditAction currentDbPropertiesAction = EditAction.VIEW;
    private List<DbPropertiesDTO> dbPropertiesList;
    private DbPropertiesDTO selectedDbProperties;
    private DbPropertiesDTO dbPropertiesDTOWork; 
    private int selectedDbPropertiesRowIndex = -1;
    private String verificationStatus;
    private List<UserSecurity> grantedList = new ArrayList<UserSecurity>();	
    private List<UserSecurity> deniedList = new ArrayList<UserSecurity>();
    
    private RoutingDataSource dataSource;

    public void setDataSource (RoutingDataSource dataSource) {
		this.dataSource = dataSource;
	}
    
    public List<UserSecurity> getGrantedList() {
		return grantedList;
	}

	public List<UserSecurity> getDeniedList() {
		return deniedList;
	}

	public void setGrantedList(List<UserSecurity> grantedList) {
		this.grantedList = grantedList;
	}

	public void setDeniedList(List<UserSecurity> deniedList) {
		this.deniedList = deniedList;
	}

    public DbPropertiesDTO getDbPropertiesDTOWork() {
		return dbPropertiesDTOWork;
	}

	public void setDbPropertiesDTOWork(DbPropertiesDTO dbPropertiesDTOWork) {
		this.dbPropertiesDTOWork = dbPropertiesDTOWork;
	}

    public String getActionDbPropertiesText() {
		if(currentDbPropertiesAction.equals(EditAction.ADD)){
			return "Add";
	    }
		else if(currentDbPropertiesAction.equals(EditAction.COPY_ADD)){
			return "Copy/Add";
	    }
	    else if(currentDbPropertiesAction.equals(EditAction.UPDATE)){
	    	return "Update";	
	    }
	    else if(currentDbPropertiesAction.equals(EditAction.DELETE)){
	    	return "Delete";
	    }
	    else if(currentDbPropertiesAction.equals(EditAction.MAP)){
	    	return "Map";
	    }
	      
	    return ""; 
	}
    
    public int getSelectedDbPropertiesRowIndex() {
		return selectedDbPropertiesRowIndex;
	}

	public void setSelectedDbPropertiesRowIndex(int selectedDbPropertiesRowIndex) {
		this.selectedDbPropertiesRowIndex = selectedDbPropertiesRowIndex;
	}

    public void selectedRowChanged(ActionEvent e) throws AbortProcessingException { 
		HtmlDataTable table = (HtmlDataTable) ((HtmlAjaxSupport) e.getComponent()).getParent(); 
		selectedDbPropertiesRowIndex = table.getRowIndex();
		selectedDbProperties = (DbPropertiesDTO)table.getRowData();
	}
    
    public boolean isShowDbPropertiesButtons() {
		return (selectedDbPropertiesRowIndex != -1);
	}
    
    public String displayCopyAddDbPropertiesAction() {
    	verificationStatus = "";
    	currentDbPropertiesAction = EditAction.COPY_ADD;
    	dbPropertiesDTOWork = new DbPropertiesDTO(); 
		if(selectedDbProperties!=null){
			dbPropertiesDTOWork.setPassword(selectedDbProperties.getPassword());
			dbPropertiesDTOWork.setUserName(selectedDbProperties.getUserName());
			dbPropertiesDTOWork.setDatabaseName(selectedDbProperties.getDatabaseName());
			dbPropertiesDTOWork.setDriverClassName(selectedDbProperties.getDriverClassName());
			dbPropertiesDTOWork.setUrl(selectedDbProperties.getUrl());
		}
		
		return "dbproperties_update";
	}
    
    public String displayUpdateDbPropertiesAction() {
    	verificationStatus = "";
    	currentDbPropertiesAction = EditAction.UPDATE;
    	dbPropertiesDTOWork = new DbPropertiesDTO(); 
		if(selectedDbProperties!=null){
			dbPropertiesDTOWork.setDbId(selectedDbProperties.getDbId());
			dbPropertiesDTOWork.setPassword(selectedDbProperties.getPassword());
			dbPropertiesDTOWork.setUserName(selectedDbProperties.getUserName());
			dbPropertiesDTOWork.setDatabaseName(selectedDbProperties.getDatabaseName());
			dbPropertiesDTOWork.setDriverClassName(selectedDbProperties.getDriverClassName());
			dbPropertiesDTOWork.setUrl(selectedDbProperties.getUrl());
		}
		
		return "dbproperties_update";
	}
    
    
    public String displayAddDbPropertiesAction() {
    	verificationStatus = "";
    	currentDbPropertiesAction = EditAction.ADD;
    	dbPropertiesDTOWork = new DbPropertiesDTO();
	    return "dbproperties_update";
	}
	
	public String displayDeleteDbPropertiesAction() {
		verificationStatus = "";
		currentDbPropertiesAction = EditAction.DELETE;
		dbPropertiesDTOWork = new DbPropertiesDTO(); 
		if(selectedDbProperties!=null){
			dbPropertiesDTOWork.setDbId(selectedDbProperties.getDbId());
			dbPropertiesDTOWork.setPassword(selectedDbProperties.getPassword());
			dbPropertiesDTOWork.setUserName(selectedDbProperties.getUserName());
			dbPropertiesDTOWork.setDatabaseName(selectedDbProperties.getDatabaseName());
			dbPropertiesDTOWork.setDriverClassName(selectedDbProperties.getDriverClassName());
			dbPropertiesDTOWork.setUrl(selectedDbProperties.getUrl());
		}
		
		return "dbproperties_update";
	}
	
	public boolean getIsAddAction() {
		return currentDbPropertiesAction.equals(EditAction.ADD) || currentDbPropertiesAction.equals(EditAction.COPY_ADD);
	}
	
	public boolean getIsDeleteAction() {
		if(currentDbPropertiesAction.equals(EditAction.DELETE)){
			return true;
		}
    	
    	return false;
	}
	
	public String  getVerificationStatus(){
		return verificationStatus;
	}
	
	
	public String verifyDbPropertiesAction(){
		verificationStatus = "Connection failed: unknown";
		
		try {
			Class.forName(dbPropertiesDTOWork.getDriverClassName());
	    } catch (ClassNotFoundException e) {
	    	verificationStatus =  "Can't find class " + dbPropertiesDTOWork.getDriverClassName();
	    	return "dbproperties_update";
	    }

	    Connection conn = null;
	    Statement stmt = null;
	    ResultSet rset = null;
	    try {
	    	conn = DriverManager.getConnection(dbPropertiesDTOWork.getUrl(), dbPropertiesDTOWork.getUserName(), dbPropertiesDTOWork.getPassword());

	    	stmt = conn.createStatement();
	    	rset = stmt.executeQuery("select PURCHTRANS_ID from TB_PURCHTRANS where PURCHTRANS_ID = -1");
	    	
	    	rset.close();rset = null;
	    	stmt.close();stmt = null;
	    	conn.close();conn = null;
	    	
	    	verificationStatus = "Connection succeeded.";
	    } catch (SQLException e) {
	    	verificationStatus =  "SQL error: " + e.getMessage();
	    } finally {
	      if (rset != null)
	        try {
	          rset.close();
	        } catch (SQLException ignore) {
	        }
	      if (stmt != null)
	        try {
	          stmt.close();
	        } catch (SQLException ignore) {
	        }
	      if (conn != null)
	        try {
	          conn.close();
	        } catch (SQLException ignore) {
	        }
	    }

		return "dbproperties_update";
	}
	
	private int getCursor(java.sql.Connection con) throws SQLException {
		int cursor = 0;		
		if (con.getMetaData().getDatabaseProductName().equalsIgnoreCase("Oracle")) {
			cursor =  OracleTypes.CURSOR;					
		} else if (con.getMetaData().getDatabaseProductName().equalsIgnoreCase("PostgreSQL")) {
			cursor = Types.OTHER;					
		}			
		return cursor;
	}
	
	public DbPropertiesDTO getDbPropertiesDTO(String databaseName) throws DataAccessException {
		DbPropertiesDTO dbPropertiesDTO = null;
		Connection conn = dataSource.CreateSQLiteConnection();
		if(conn==null){
			logger.error("############ JDBC Connection failed"); 
			return null;
		}
		
		Statement stat = null;
		try{
			stat = conn.createStatement();
	    	ResultSet rs = stat.executeQuery("select * from TB_DB_PROPERTIES where UPPER(DATABASE_NAME) = UPPER('" + databaseName + "')");
	    	if (rs.next()) {
	    		dbPropertiesDTO = new DbPropertiesDTO(); 
	    		dbPropertiesDTO.setDbId(rs.getInt("DB_ID"));
	    		dbPropertiesDTO.setPassword(rs.getString(RoutingDataSource.getColumnHeader(conn) + "PASSWORD"));
	    		dbPropertiesDTO.setUserName(rs.getString("USER_NAME"));
	    		dbPropertiesDTO.setDatabaseName(rs.getString("DATABASE_NAME"));
	    		dbPropertiesDTO.setDriverClassName(rs.getString("DRIVER_CLASS_NAME"));
	    		dbPropertiesDTO.setUrl(rs.getString("URL"));
	    	}
	    	rs.close();	
		}
		catch (Exception e){
			logger.error("############ getDbPropertiesDTO() failed: " + e.toString()); 
			conn = dataSource.RenewSQLiteConnection(conn);
		}
		finally{
			if(stat!= null){try{stat.close();}catch(Exception ex){}}
			dataSource.CloseSQLiteConnection(conn);
		}
		
		return dbPropertiesDTO;
	}
	
	private void addMessage(String errorMessage){
		FacesMessage message = new FacesMessage(errorMessage);
		message.setSeverity(FacesMessage.SEVERITY_ERROR);
		FacesContext.getCurrentInstance().addMessage(null, message);
	}

    public String okDbPropertiesAction() {    
	    if(currentDbPropertiesAction.equals(EditAction.ADD) || currentDbPropertiesAction.equals(EditAction.COPY_ADD)){
			DbPropertiesDTO dbPropertiesDTO = getDbPropertiesDTO(dbPropertiesDTOWork.getDatabaseName());
			if(dbPropertiesDTO!=null){
				addMessage("Database name has already existed.");
				return null;
			}
	    	
	    	String errorMessage = addDbProperties(dbPropertiesDTOWork);
	    	if(errorMessage!=null && errorMessage.length()>0){
		    	addMessage(errorMessage);
				return null;
		    }
	    	
	    	logger.info("end addAction");
	    }
	    else if(currentDbPropertiesAction.equals(EditAction.UPDATE)){
	    	String errorMessage = updateDbProperties(dbPropertiesDTOWork);
	    	if(errorMessage!=null && errorMessage.length()>0){
		    	addMessage(errorMessage);
				return null;
		    }
			logger.info("end updateAction");		
	    }
	    else if(currentDbPropertiesAction.equals(EditAction.DELETE)){
	    	String errorMessage = deleteDbProperties(dbPropertiesDTOWork);
	    	if(errorMessage!=null && errorMessage.length()>0){
		    	addMessage(errorMessage);
				return null;
		    }
			logger.info("end deleteAction");		
	    }
	    else if(currentDbPropertiesAction.equals(EditAction.MAP)){
	    	mapDbProperties();
			logger.info("end mapAction");		
	    }
	
	    dbPropertiesList = null;
	    selectedDbProperties = null;
		selectedDbPropertiesRowIndex = -1;
	      
	    return "dbproperties_main"; 
	}
    
    public String cancelDbPropertiesAction() {
		return "dbproperties_main";
	}
    
    public String deleteDbProperties(DbPropertiesDTO dbPropertiesDTO) throws DataAccessException {
    	 String errorMessage = "";
		 Connection conn = dataSource.CreateSQLiteConnection();
		 if(conn==null){
			 logger.error("############ JDBC Connection failed");
			 return "Connection failed";
		 }
		 
		 PreparedStatement prep = null;
		 try{
			 conn.setAutoCommit(false);
			 
			 prep = conn.prepareStatement("DELETE FROM TB_DB_PROPERTIES WHERE DB_ID = ?");
			 prep.setInt(1, dbPropertiesDTO.getDbId());
			 prep.executeUpdate();
			 
			 //Delete db mapping
			 prep = conn.prepareStatement("delete from TB_USER_DB where DB_ID = ?");
			 prep.setInt(1, dbPropertiesDTO.getDbId());
			 prep.executeUpdate();
			 
			 conn.commit();
	    	 conn.setAutoCommit(true);
		 }
		 catch (Exception e){
			 errorMessage = "Cannot delete database property, please try again!"; 
			 try{
				 conn.rollback();
			 }
			 catch(Exception ex){
			 }
			 logger.error("############ deleteDbProperties() failed: " + e.toString()); 
			 conn = dataSource.RenewSQLiteConnection(conn);	 
		 }
		 finally{
			 if(prep!=null){try{prep.close();}catch(Exception ex){}}
			 dataSource.CloseSQLiteConnection(conn);
		 }
		 
		 refreshDbProperties();
		 
		 return errorMessage;
	}
    
    public String updateDbProperties(DbPropertiesDTO dbPropertiesDTO) throws DataAccessException {
    	 String errorMessage = "";
		 Connection conn = dataSource.CreateSQLiteConnection();
		 if(conn==null){
			 logger.error("############ JDBC Connection failed");
			 return "Connection failed";
		 }
		 
		 PreparedStatement prep = null;
		 try{
			 prep = conn.prepareStatement("UPDATE TB_DB_PROPERTIES SET " + RoutingDataSource.getColumnHeader(conn) + "PASSWORD = ? , USER_NAME = ? , DATABASE_NAME = ? , DRIVER_CLASS_NAME = ? , URL = ?  WHERE DB_ID = ?");

			 prep.setString(1, dbPropertiesDTO.getPassword());
			 prep.setString(2, dbPropertiesDTO.getUserName());
			 prep.setString(3, dbPropertiesDTO.getDatabaseName());
			 prep.setString(4, dbPropertiesDTO.getDriverClassName()); 
			 prep.setString(5, dbPropertiesDTO.getUrl());
			 prep.setInt(6, dbPropertiesDTO.getDbId());

			 conn.setAutoCommit(false);
			 prep.executeUpdate();
			 
			 conn.commit();
	    	 conn.setAutoCommit(true);
		 }
		 catch (Exception e){
			 errorMessage = "Cannot update database property, please try again!"; 
			 try{
				 conn.rollback();
			 }
			 catch(Exception ex){
			 }
			 logger.error("############ updateDbProperties() failed: " + e.toString()); 
			 conn = dataSource.RenewSQLiteConnection(conn);	 
		 }
		 finally{
			 if(prep!=null){try{prep.close();}catch(Exception ex){}}
			 dataSource.CloseSQLiteConnection(conn);
		 }
		 
		 refreshDbProperties();
		 
		 return errorMessage;
	}

    public String addDbProperties(DbPropertiesDTO dbPropertiesDTO) throws DataAccessException {
    	 String errorMessage = "";
		 Connection conn = dataSource.CreateSQLiteConnection();
		 if(conn==null){
			 logger.error("############ JDBC Connection failed");
			 return "Connection failed";
		 }
		 
		 PreparedStatement prep = null;
		 try{
			 prep = conn.prepareStatement("insert into TB_DB_PROPERTIES (" + RoutingDataSource.getColumnHeader(conn) + "PASSWORD, USER_NAME, DATABASE_NAME, DRIVER_CLASS_NAME, URL) values (?, ?, ?, ?, ?)");

			 prep.setString(1, dbPropertiesDTO.getPassword());
			 prep.setString(2, dbPropertiesDTO.getUserName());
			 prep.setString(3, dbPropertiesDTO.getDatabaseName());
			 prep.setString(4, dbPropertiesDTO.getDriverClassName());
			 prep.setString(5, dbPropertiesDTO.getUrl());

			 prep.addBatch();

			 conn.setAutoCommit(false);
			 prep.executeBatch();
			 
			 conn.commit();
	    	 conn.setAutoCommit(true);
		 }
		 catch (Exception e){
			 errorMessage = "Cannot add database property, please try again!"; 
			 logger.error("############ addDbProperties() failed: " + e.toString());  
			 try{
				 conn.rollback();
			 }
			 catch(Exception ex){
			 }
			 conn = dataSource.RenewSQLiteConnection(conn);	 
		 }
		 finally{
			 if(prep!=null){try{prep.close();}catch(Exception ex){}}
			 dataSource.CloseSQLiteConnection(conn);
		 }
		 
		 refreshDbProperties();
		 
		 return errorMessage;
	}

    public List<DbPropertiesDTO> getDbPropertiesList() {
		if (dbPropertiesList == null) {
			dbPropertiesList = findAllDbPropertiesList();
			selectedDbPropertiesRowIndex = -1;
		}
		return dbPropertiesList;
	}

    public void setDbPropertiesList(List<DbPropertiesDTO> dbPropertiesList) {
		this.dbPropertiesList = dbPropertiesList;
	}

    public List<DbPropertiesDTO> findAllDbPropertiesList() throws DataAccessException {
		List<DbPropertiesDTO> result = new ArrayList<DbPropertiesDTO>();
		Connection conn = dataSource.CreateSQLiteConnection();
		if(conn==null){
			logger.error("############ JDBC Connection failed");
			return result;
		}
		
		Statement stat = null;
		try{
			stat = conn.createStatement();
	    	ResultSet rs = stat.executeQuery("select * from TB_DB_PROPERTIES order by DB_ID asc");
	    	while (rs.next()) {
	    		DbPropertiesDTO dbPropertiesDTO = new DbPropertiesDTO(); 
	    		dbPropertiesDTO.setDbId(rs.getInt("DB_ID"));
	    		dbPropertiesDTO.setPassword(rs.getString(RoutingDataSource.getColumnHeader(conn) + "PASSWORD"));
	    		dbPropertiesDTO.setUserName(rs.getString("USER_NAME"));
	    		dbPropertiesDTO.setDatabaseName(rs.getString("DATABASE_NAME"));
	    		dbPropertiesDTO.setDriverClassName(rs.getString("DRIVER_CLASS_NAME"));
	    		dbPropertiesDTO.setUrl(rs.getString("URL"));

	    		result.add(dbPropertiesDTO);   
	    	}
	    	rs.close();	
		}
		catch (Exception e){
			logger.error("############ findAllDbPropertiesList() failed: " + e.toString()); 
			conn = dataSource.RenewSQLiteConnection(conn);	 
		}
		finally{
			if(stat!= null){try{stat.close();}catch(Exception ex){}}
			dataSource.CloseSQLiteConnection(conn);
		}
		
		return result;
	}
    
    public List<UserSecurity> findUserDbList(Integer dbId) throws DataAccessException {
		List<UserSecurity> result = new ArrayList<UserSecurity>();
		Connection conn = dataSource.CreateSQLiteConnection();
		if(conn==null){
			logger.error("############ JDBC Connection failed");
			return result;
		}
		
		Statement stat = null;
		try{
			stat = conn.createStatement();
	    	ResultSet rs = stat.executeQuery("select TB_USER_SECURITY.USER_NAME from TB_USER_SECURITY, TB_DB_PROPERTIES, TB_USER_DB where TB_DB_PROPERTIES.DB_ID=" + dbId + " and TB_USER_SECURITY.USER_ID=TB_USER_DB.USER_ID and TB_USER_DB.DB_ID=TB_DB_PROPERTIES.DB_ID");
	    	while (rs.next()) {
	    		String userName = new String(rs.getString("USER_NAME"));
	    		UserSecurity userSecurity = new UserSecurity(userName);
	    		result.add(userSecurity);   
	    	}
	    	rs.close();	
		}
		catch (Exception e){
			logger.error("############ findUserDbList() failed: " + e.toString()); 
			conn = dataSource.RenewSQLiteConnection(conn);	 
		}
		finally{
			if(stat!= null){try{stat.close();}catch(Exception ex){}}
			dataSource.CloseSQLiteConnection(conn);
		}
		
		return result;
	}
    
    public List<UserSecurity> findAllUserList() throws DataAccessException {
		List<UserSecurity> result = new ArrayList<UserSecurity>();
		Connection conn = dataSource.CreateSQLiteConnection();
		if(conn==null){
			logger.error("############ JDBC Connection failed");
			return result;
		}
		
		Statement stat = null;
		try{
			stat = conn.createStatement();
	    	ResultSet rs = stat.executeQuery("select * from TB_USER_SECURITY order by USER_ID desc");
	    	while (rs.next()) {
	    		UserSecurity userSecurity = new UserSecurity(rs.getString("USER_NAME")); 		    	
	    		result.add(userSecurity);   
	    	}
	    	rs.close();	
		}
		catch (Exception e){
			logger.error("############ findAllUserList() failed: " + e.toString()); 
			conn = dataSource.RenewSQLiteConnection(conn);	 
		}
		finally{
			if(stat!= null){try{stat.close();}catch(Exception ex){}}
			dataSource.CloseSQLiteConnection(conn);
		}
		
		return result;
	}
    
    public String displayMapDbPropertiesAction() {
		currentDbPropertiesAction = EditAction.MAP;
		dbPropertiesDTOWork = new DbPropertiesDTO(); 
		if(selectedDbProperties!=null){
			dbPropertiesDTOWork.setDbId(selectedDbProperties.getDbId());
			dbPropertiesDTOWork.setPassword(selectedDbProperties.getPassword());
			dbPropertiesDTOWork.setUserName(selectedDbProperties.getUserName());
			dbPropertiesDTOWork.setDatabaseName(selectedDbProperties.getDatabaseName());
			dbPropertiesDTOWork.setDriverClassName(selectedDbProperties.getDriverClassName());
			dbPropertiesDTOWork.setUrl(selectedDbProperties.getUrl());
			
			if(!mapDbProperties()){
				return null;
			}
		}
		
		return "dbproperties_map";
	}	
    
    public boolean mapDbProperties() {
		try {
			List<UserSecurity> allList = findAllUserList();
	    	List<UserSecurity> grantedList = findUserDbList(dbPropertiesDTOWork.getDbId());
				
			allList.removeAll(grantedList);
		    this.setDeniedList(allList);
		    this.setGrantedList(grantedList);
		    
		    return true;    
		} catch (Exception ex) {
			ex.printStackTrace();
			return false;
		}
	}
    
    public String mapAction() {
		logger.info("start mapAction"); 

		String errorMessage = "";
		List<UserSecurity> addList = new ArrayList<UserSecurity>();	
		List<UserSecurity> removeList = new ArrayList<UserSecurity>();		
		List<UserSecurity> existRMList = findUserDbList(dbPropertiesDTOWork.getDbId());
			
		List<UserSecurity> newGrantedList = this.getGrantedList();
		for(Iterator<UserSecurity> itr = newGrantedList.iterator();itr.hasNext();){
			UserSecurity userSecurity = itr.next();		
			if(!existRMList.contains(userSecurity)){
				addList.add(userSecurity);
			}
		}
		
		List<UserSecurity> newDeniedList = this.getDeniedList();
		for(Iterator<UserSecurity> itr = newDeniedList.iterator();itr.hasNext();){
			UserSecurity userSecurity = itr.next();		
			if(existRMList.contains(userSecurity)){
				removeList.add(userSecurity);
			}
		}
		
		Connection conn = dataSource.CreateSQLiteConnection();
		if(conn==null){
			addMessage("Connection failed");
			logger.error("############ JDBC Connection failed");
			return null;
		}
		
		PreparedStatement prepDelete = null;
		PreparedStatement prepAdd = null;
		try{
			conn.setAutoCommit(false);
			
			//Remove mapping
			prepDelete = conn.prepareStatement("delete from TB_USER_DB where DB_ID = ? and USER_ID = ?");
			for(Iterator<UserSecurity> itr = removeList.iterator();itr.hasNext();){
				UserSecurity userSecurity = itr.next();	
				Statement stat = conn.createStatement();
				ResultSet rs = stat.executeQuery("select * from TB_USER_SECURITY where UPPER(USER_NAME) = UPPER('" + userSecurity.getUserName() + "')");
				int userId = 0;
				if (rs.next()) {	
					userId = ((Integer)rs.getInt("USER_ID")).intValue();
				}
				rs.close();		
				
				prepDelete.setInt(1, dbPropertiesDTOWork.getDbId().intValue());
				prepDelete.setInt(2, userId);
				prepDelete.addBatch();
			}
			prepDelete.executeBatch();
			
			//Add mapping
			prepAdd = conn.prepareStatement("insert into TB_USER_DB (DB_ID, USER_ID) values (?, ?)");
			for(Iterator<UserSecurity> itr = addList.iterator();itr.hasNext();){
				UserSecurity userSecurity = itr.next();	
				Statement stat = conn.createStatement();
				ResultSet rs = stat.executeQuery("select * from TB_USER_SECURITY where UPPER(USER_NAME) = UPPER('" + userSecurity.getUserName() + "')");
				int userId = 0;
				if (rs.next()) {	
					userId = ((Integer)rs.getInt("USER_ID")).intValue();
				}
				rs.close();		

				prepAdd.setInt(1, dbPropertiesDTOWork.getDbId().intValue());
				prepAdd.setInt(2, userId);
				prepAdd.addBatch();
			}
			prepAdd.executeBatch();
			
			conn.commit();
	    	conn.setAutoCommit(true);
		 }
		 catch (Exception e){
			 errorMessage = "Cannot update database property/user mapping, please try again!";
			 try{
				 conn.rollback();
			 }
			 catch(Exception ex){
			 }
			 logger.error("############ mapAction() failed: " + e.toString()); 
			 conn = dataSource.RenewSQLiteConnection(conn);	 
		 }
		 finally{
			 if(prepDelete!=null){try{prepDelete.close();}catch(Exception ex){}}
			 if(prepAdd!=null){try{prepAdd.close();}catch(Exception ex){}}
			 dataSource.CloseSQLiteConnection(conn);
		 }
		 
		 if(errorMessage!=null && errorMessage.length()>0){
			addMessage(errorMessage);
			return null;
		 }
	      
	     return "dbproperties_main"; 
	}
    
    public String cancelMapAction() {

		return "dbproperties_main";
	}
    
    public void refreshDbProperties() {
    	try {
			 if(dataSource.getSecurityDbFile() != null) {
				 dataSource.setProperties(dataSource.getSecurityDbFile());
				 dataSource.afterPropertiesSet();
			 }
		 }
		 catch(Exception e) {
			 e.printStackTrace();
		 }
    }
}
