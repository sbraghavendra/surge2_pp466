package com.ncsts.management;

import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.security.SecureRandom;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;
import java.util.Date;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.component.html.HtmlInputSecret;
import javax.faces.context.FacesContext;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.ActionEvent;
import javax.servlet.http.HttpSession;

import org.ajax4jsf.component.html.HtmlAjaxSupport;
import org.apache.log4j.Logger;
import org.richfaces.component.html.HtmlScrollableDataTable;
import org.richfaces.model.selection.Selection;
import org.richfaces.model.selection.SimpleSelection;
import org.richfaces.component.html.HtmlDataTable;
import org.springframework.dao.DataAccessException;

import com.ncsts.common.LoggerFactory;
import com.ncsts.domain.DataStatistics;
import com.ncsts.domain.TaxMatrix;
import com.ncsts.dto.DataStatisticsDTO;
import com.ncsts.dto.DbPropertiesDTO;
import com.ncsts.dto.PasswordHistoryDTO;
import com.ncsts.dto.UserDTO;
import com.ncsts.dto.UserRolesDTO;
import com.ncsts.dto.SmtpPropertiesDTO;
import com.ncsts.dto.UserSecurityDTO;
import com.ncsts.services.DataStatisticsService;
import com.ncsts.view.bean.EditAction;
import com.ncsts.view.bean.RoutingDataSource;

public class SmtpPropertiesBean {
	
    private Logger logger = LoggerFactory.getInstance().getLogger(SmtpPropertiesBean.class);	
    
    private EditAction currentSmtpPropertiesAction = EditAction.VIEW;
    private List<SmtpPropertiesDTO> smtpPropertiesList;
    private SmtpPropertiesDTO selectedSmtpProperties;
    private SmtpPropertiesDTO smtpPropertiesDTOWork; 
    private int selectedSmtpPropertiesRowIndex = -1;
    
    private RoutingDataSource dataSource;

    public void setDataSource (RoutingDataSource dataSource) {
		this.dataSource = dataSource;
	}
    
    public SmtpPropertiesDTO getSmtpPropertiesDTOWork() {
		return smtpPropertiesDTOWork;
	}

	public void setSmtpPropertiesDTOWork(SmtpPropertiesDTO smtpPropertiesDTOWork) {
		this.smtpPropertiesDTOWork = smtpPropertiesDTOWork;
	}
	
	public boolean getIsSmtpDeleteAction() {
		if(currentSmtpPropertiesAction.equals(EditAction.DELETE)){
			return true;
		}
    	
    	return false;
	}

    public String getActionSmtpPropertiesText() {
		if(currentSmtpPropertiesAction.equals(EditAction.ADD)){
			return "Add";
		}
		else if(currentSmtpPropertiesAction.equals(EditAction.UPDATE)){
			return "Update";
		}
		else if(currentSmtpPropertiesAction.equals(EditAction.DELETE)){
			return "Delete";
		}
		
		return "";
	}
    
    public int getSelectedSmtpPropertiesRowIndex() {
		return selectedSmtpPropertiesRowIndex;
	}

	public void setSelectedSmtpPropertiesRowIndex(int selectedSmtpPropertiesRowIndex) {
		this.selectedSmtpPropertiesRowIndex = selectedSmtpPropertiesRowIndex;
	}

    public void selectedRowChanged(ActionEvent e) throws AbortProcessingException { 
		HtmlDataTable table = (HtmlDataTable) ((HtmlAjaxSupport) e.getComponent()).getParent(); 
		selectedSmtpPropertiesRowIndex = table.getRowIndex();
		selectedSmtpProperties = (SmtpPropertiesDTO)table.getRowData();
	}
    
    public boolean isShowSmtpPropertiesButtons() {
		return (selectedSmtpPropertiesRowIndex != -1);
	}
    
    public String displayAddSmtpPropertiesAction() {
    	currentSmtpPropertiesAction = EditAction.ADD;
    	smtpPropertiesDTOWork = new SmtpPropertiesDTO(); 
		
		return "smtpproperties_update";
	}
    
    public String displayUpdateSmtpPropertiesAction() {
    	currentSmtpPropertiesAction = EditAction.UPDATE;
    	smtpPropertiesDTOWork = new SmtpPropertiesDTO(); 
		if(selectedSmtpProperties!=null){
			smtpPropertiesDTOWork.setId(selectedSmtpProperties.getId());
			smtpPropertiesDTOWork.setAction(selectedSmtpProperties.getAction());
			
			smtpPropertiesDTOWork.setSmtpSubject(selectedSmtpProperties.getSmtpSubject());
			smtpPropertiesDTOWork.setSmtpFrom(selectedSmtpProperties.getSmtpFrom());
			smtpPropertiesDTOWork.setSmtpContent(selectedSmtpProperties.getSmtpContent());
			smtpPropertiesDTOWork.setSmtpHostName(selectedSmtpProperties.getSmtpHostName());
			smtpPropertiesDTOWork.setSmtpPort(selectedSmtpProperties.getSmtpPort());
		}
		
		return "smtpproperties_update";
	}
    
    public String displayDeleteSmtpPropertiesAction() {
    	currentSmtpPropertiesAction = EditAction.DELETE;
    	smtpPropertiesDTOWork = new SmtpPropertiesDTO(); 
		if(selectedSmtpProperties!=null){
			smtpPropertiesDTOWork.setId(selectedSmtpProperties.getId());
			smtpPropertiesDTOWork.setAction(selectedSmtpProperties.getAction());
			
			smtpPropertiesDTOWork.setSmtpSubject(selectedSmtpProperties.getSmtpSubject());
			smtpPropertiesDTOWork.setSmtpFrom(selectedSmtpProperties.getSmtpFrom());
			smtpPropertiesDTOWork.setSmtpContent(selectedSmtpProperties.getSmtpContent());
			smtpPropertiesDTOWork.setSmtpHostName(selectedSmtpProperties.getSmtpHostName());
			smtpPropertiesDTOWork.setSmtpPort(selectedSmtpProperties.getSmtpPort());
		}
		
		return "smtpproperties_update";
	}

    public String okSmtpPropertiesAction() {    
    	if(currentSmtpPropertiesAction.equals(EditAction.ADD)){
    		
    		if(smtpPropertiesDTOWork.getAction()==null || smtpPropertiesDTOWork.getAction().length()==0){
				addMessage("SMTP Action can not be empty.");
				return null;
			}
    		
    		
    		SmtpPropertiesDTO smtpPropertiesDTO = getSmtpProperties(smtpPropertiesDTOWork.getAction());
			if(smtpPropertiesDTO!=null){
				addMessage("SMTP Action has already existed.");
				return null;
			}

			String errorMessage = addSmtpProperties(smtpPropertiesDTOWork);
	    	if(errorMessage!=null && errorMessage.length()>0){
	    		addMessage(errorMessage);
	    		return null;
	    	}
	    }
	    else if(currentSmtpPropertiesAction.equals(EditAction.UPDATE)){
	    	
	    	if(smtpPropertiesDTOWork.getAction()==null || smtpPropertiesDTOWork.getAction().length()==0){
				addMessage("SMTP Action can not be empty.");
				return null;
			}
	    	
	    	SmtpPropertiesDTO smtpPropertiesDTO = getSmtpProperties(smtpPropertiesDTOWork.getAction());
			if(smtpPropertiesDTO!=null && smtpPropertiesDTOWork.getId()!=smtpPropertiesDTO.getId()){
				addMessage("SMTP Action has already existed.");
				return null;
			}

	    	String errorMessage = updateSmtpProperties(smtpPropertiesDTOWork);
	    	if(errorMessage!=null && errorMessage.length()>0){
	    		addMessage(errorMessage);
	    		return null;
	    	}
	    	
			logger.info("end updateAction");		
	    }
	    else if(currentSmtpPropertiesAction.equals(EditAction.DELETE)){

	    	String errorMessage = deleteSmtpProperties(smtpPropertiesDTOWork);
	    	if(errorMessage!=null && errorMessage.length()>0){
	    		addMessage(errorMessage);
	    		return null;
	    	}
	    	
			logger.info("end deleteAction");		
	    }
	
	    smtpPropertiesList = null;
	    selectedSmtpProperties = null;
		selectedSmtpPropertiesRowIndex = -1;
	      
	    return "smtpproperties_main"; 
	}
    
    public String cancelSmtpPropertiesAction() {
		return "smtpproperties_main";
	}
    
    private void addMessage(String errorMessage){
		FacesMessage message = new FacesMessage(errorMessage);
		message.setSeverity(FacesMessage.SEVERITY_ERROR);
		FacesContext.getCurrentInstance().addMessage(null, message);
	}
    
    public String addSmtpProperties(SmtpPropertiesDTO smtpPropertiesDTO) throws DataAccessException {
    	String errorMessage = ""; 
    	Connection conn = dataSource.CreateSQLiteConnection();
    	if(conn==null){
    		logger.error("############ JDBC Connection failed");
    		return "Connection failed";
    	}
    	
    	PreparedStatement prep = null;
		try{
			 prep = conn.prepareStatement("insert into TB_SMTP_PROPERTIES (ACTION, SMTP_SUBJECT, SMTP_FROM, SMTP_CONTENT, SMTP_HOST_NAME, SMTP_PORT) values (?, ?, ?, ?, ?, ?)");

			 prep.setString(1, smtpPropertiesDTO.getAction());
			 prep.setString(2, smtpPropertiesDTO.getSmtpSubject());
			 prep.setString(3, smtpPropertiesDTO.getSmtpFrom());
			 prep.setString(4, smtpPropertiesDTO.getSmtpContent());
			 prep.setString(5, smtpPropertiesDTO.getSmtpHostName()); 
			 prep.setInt(6, smtpPropertiesDTO.getSmtpPort());

			 conn.setAutoCommit(false);
			 prep.executeUpdate();
			 
			 conn.commit();
	    	 conn.setAutoCommit(true);
		 }
		 catch (Exception e){
			 errorMessage = "Cannot add smtp property, please try again!"; 
			 try{
				 conn.rollback();
			 }
			 catch(Exception ex){
			 }
			 logger.error("############ addSmtpProperties() failed: " + e.toString()); 
			 conn = dataSource.RenewSQLiteConnection(conn);	 
		 }
		 finally{
			 if(prep!=null){try{prep.close();}catch(Exception ex){}}
			 dataSource.CloseSQLiteConnection(conn);
		 }
		 
		 return errorMessage;
	}
    
    public String updateSmtpProperties(SmtpPropertiesDTO smtpPropertiesDTO) throws DataAccessException {
    	String errorMessage = ""; 
    	Connection conn = dataSource.CreateSQLiteConnection();
    	if(conn==null){
    		logger.error("############ JDBC Connection failed");
    		return "Connection failed";
    	}
    	
    	PreparedStatement prep = null;
		 try{
			 prep = conn.prepareStatement("UPDATE TB_SMTP_PROPERTIES SET ACTION = ? , SMTP_SUBJECT = ? , SMTP_FROM = ? , SMTP_CONTENT = ? , SMTP_HOST_NAME = ? , SMTP_PORT = ?  WHERE ID = ?");

			 prep.setString(1, smtpPropertiesDTO.getAction());
			 prep.setString(2, smtpPropertiesDTO.getSmtpSubject());
			 prep.setString(3, smtpPropertiesDTO.getSmtpFrom());
			 prep.setString(4, smtpPropertiesDTO.getSmtpContent());
			 prep.setString(5, smtpPropertiesDTO.getSmtpHostName()); 
			 prep.setInt(6, smtpPropertiesDTO.getSmtpPort());
			 prep.setInt(7, smtpPropertiesDTO.getId());

			 conn.setAutoCommit(false);
			 prep.executeUpdate();
			 
			 conn.commit();
	    	 conn.setAutoCommit(true);
		 }
		 catch (Exception e){
			 errorMessage = "Cannot update smtp property, please try again!";
			 try{
				 conn.rollback();
			 }
			 catch(Exception ex){
			 }
			 logger.error("############ updateSmtpProperties() failed: " + e.toString()); 
			 conn = dataSource.RenewSQLiteConnection(conn);	 
		 }
		 finally{
			 if(prep!=null){try{prep.close();}catch(Exception ex){}}
			 dataSource.CloseSQLiteConnection(conn);
		 }
		 
		 return errorMessage;
	}
    
    
    public String deleteSmtpProperties(SmtpPropertiesDTO smtpPropertiesDTO) throws DataAccessException {
		String errorMessage = ""; 
		Connection conn = dataSource.CreateSQLiteConnection();
		if(conn==null){
			logger.error("############ JDBC Connection failed");
			return "Connection failed";
		}
		
		PreparedStatement prep = null;
		 try{
			 conn.setAutoCommit(false);
			 
			 //Delete Smtp Properties
			 prep = conn.prepareStatement("DELETE FROM TB_SMTP_PROPERTIES WHERE ID = ?");
			 prep.setInt(1, smtpPropertiesDTO.getId());		 
			 prep.executeUpdate();

			 conn.commit();
	    	 conn.setAutoCommit(true);
		 }
		 catch (Exception e){
			 errorMessage = "Cannot delete smtp property, please try again!"; 
			 try{
				 conn.rollback();
			 }
			 catch(Exception ex){
			 }
			 logger.error("############ deleteSmtpProperties() failed: " + e.toString()); 
			 conn = dataSource.RenewSQLiteConnection(conn);	 
		 }
		 finally{
			 if(prep!=null){try{prep.close();}catch(Exception ex){}}
			 dataSource.CloseSQLiteConnection(conn);
		 }
		 return errorMessage;
	}
    
    public SmtpPropertiesDTO getSmtpProperties(String actionName) throws DataAccessException {
    	SmtpPropertiesDTO smtpPropertiesDTO = null;
		Connection conn = dataSource.CreateSQLiteConnection();
		if(conn==null){
			logger.error("############ JDBC Connection failed");
			return null;
		}
		
		Statement stat = null;
		try{
			stat = conn.createStatement();
	    	ResultSet rs = stat.executeQuery("select * from TB_SMTP_PROPERTIES where UPPER(ACTION) = UPPER('" + actionName + "')");
	    	if (rs.next()) {
	    		smtpPropertiesDTO = new SmtpPropertiesDTO(); 
	    		smtpPropertiesDTO.setId(rs.getInt("ID"));
	    		smtpPropertiesDTO.setSmtpPort(rs.getInt("SMTP_PORT"));
	    		smtpPropertiesDTO.setAction(rs.getString("ACTION"));
	    		smtpPropertiesDTO.setSmtpSubject(rs.getString("SMTP_SUBJECT"));
	    		smtpPropertiesDTO.setSmtpFrom(rs.getString("SMTP_FROM"));
	    		smtpPropertiesDTO.setSmtpContent(rs.getString("SMTP_CONTENT"));
	    		smtpPropertiesDTO.setSmtpHostName(rs.getString("SMTP_HOST_NAME"));
	    	}
	    	rs.close();	
		}
		catch (Exception e){
			logger.error("############ getSmtpProperties() failed: " + e.toString()); 
			conn = dataSource.RenewSQLiteConnection(conn);	 
		}
		finally{
			if(stat!= null){try{stat.close();}catch(Exception ex){}}
			dataSource.CloseSQLiteConnection(conn);
		}
		
		return smtpPropertiesDTO;
	}

    public List<SmtpPropertiesDTO> getSmtpPropertiesList() {
		if (smtpPropertiesList == null) {
			smtpPropertiesList = findAllSmtpPropertiesList();
			selectedSmtpPropertiesRowIndex = -1;
		}
		return smtpPropertiesList;
	}

    public void setSmtpPropertiesList(List<SmtpPropertiesDTO> smtpPropertiesList) {
		this.smtpPropertiesList = smtpPropertiesList;
	}

    public List<SmtpPropertiesDTO> findAllSmtpPropertiesList() throws DataAccessException {
		List<SmtpPropertiesDTO> result = new ArrayList<SmtpPropertiesDTO>();
		Connection conn = dataSource.CreateSQLiteConnection();
		if(conn==null){
			logger.error("############ JDBC Connection failed");
			return result;
		}
		
		Statement stat = null;
		try{
			stat = conn.createStatement();
	    	ResultSet rs = stat.executeQuery("select * from TB_SMTP_PROPERTIES order by ID asc");
	    	while (rs.next()) {
	    		SmtpPropertiesDTO smtpPropertiesDTO = new SmtpPropertiesDTO(); 
	    		smtpPropertiesDTO.setId(rs.getInt("ID"));
	    		smtpPropertiesDTO.setSmtpPort(rs.getInt("SMTP_PORT"));
	    		smtpPropertiesDTO.setAction(rs.getString("ACTION"));
	    		smtpPropertiesDTO.setSmtpSubject(rs.getString("SMTP_SUBJECT"));
	    		smtpPropertiesDTO.setSmtpFrom(rs.getString("SMTP_FROM"));
	    		smtpPropertiesDTO.setSmtpContent(rs.getString("SMTP_CONTENT"));
	    		smtpPropertiesDTO.setSmtpHostName(rs.getString("SMTP_HOST_NAME"));
	    		result.add(smtpPropertiesDTO);   
	    	}
	    	rs.close();	
		}
		catch (Exception e){
			logger.error("############ findAllSmtpPropertiesList() failed: " + e.toString()); 
			conn = dataSource.RenewSQLiteConnection(conn);	 
		}
		finally{
			if(stat!= null){try{stat.close();}catch(Exception ex){}}
			dataSource.CloseSQLiteConnection(conn);
		}
		
		return result;
	}
}
