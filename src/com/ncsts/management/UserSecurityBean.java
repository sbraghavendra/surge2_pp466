package com.ncsts.management;

import java.security.SecureRandom;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.html.HtmlInputSecret;
import javax.faces.context.FacesContext;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.ajax4jsf.component.html.HtmlAjaxSupport;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.log4j.Logger;
import org.richfaces.component.html.HtmlDataTable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;

import com.ncsts.common.CacheManager;
import com.ncsts.common.LoggerFactory;
import com.ncsts.dao.JPAUserDAO;
import com.ncsts.domain.DbProperties;
import com.ncsts.dto.DbPropertiesDTO;
import com.ncsts.dto.PasswordHistoryDTO;
import com.ncsts.dto.SmtpPropertiesDTO;
import com.ncsts.dto.UserDTO;
import com.ncsts.dto.UserRolesDTO;
import com.ncsts.dto.UserSecurityDTO;
import com.ncsts.services.UserService;
import com.ncsts.view.bean.EditAction;
import com.ncsts.view.bean.RoutingDataSource;
import com.ncsts.view.util.DesEncrypter;

public class UserSecurityBean {
	
    private Logger logger = LoggerFactory.getInstance().getLogger(UserSecurityBean.class);	
    
    private EditAction currentUserSecurityAction = EditAction.VIEW;

	private UserSecurityDTO userSecurityDTO = new UserSecurityDTO(); //this is declared for search
	private UserSecurityDTO userSecurityDTOWork = new UserSecurityDTO(); //this is declared for updating DB Statistics line
	private List<UserSecurityDTO> userSecurityList; // this list is declared for displaying the table rows
	private UserSecurityDTO selectedUserSecurity = new UserSecurityDTO(); //this is declared for capturing one selected row from the table i.e by default 0th index of selected rows
	
	private int selectedUserSecurityRowIndex = -1;
	
	private HtmlInputSecret pwdControl = new HtmlInputSecret(); 
    private HtmlInputSecret newPwdControl = new HtmlInputSecret();  
    private HtmlInputSecret confirmPwdControl = new HtmlInputSecret(); 
    private String pwdChangeMessage;
    
    private List<SelectItem> firstComboItems  = new ArrayList<SelectItem>();
    private String selectedFirstComboValue="0";
    
    private List<DbProperties> grantedList = new ArrayList<DbProperties>();	
    private List<DbProperties> deniedList = new ArrayList<DbProperties>();
    
    private RoutingDataSource dataSource;
    
    private UserRolesBean userRolesBean;
    
    private boolean sendEmailFailed = false;
    
    @Autowired
    private UserService userService;
    
    @Autowired
    private CacheManager cacheManager;
    
    private static final String ZERO = "0";
    
    public CacheManager getCacheManager() {
		return cacheManager;
	}

	public void setCacheManager(CacheManager cacheManager) {
		this.cacheManager = cacheManager;
	}

	public UserService getUserService() {
		return userService;
	}

    public UserRolesBean getUserRolesBean() {
		return userRolesBean;
	}

	public void setUserRolesBean(UserRolesBean userRolesBean) {
		this.userRolesBean= userRolesBean;
	}

    public void setDataSource (RoutingDataSource dataSource) {
		this.dataSource = dataSource;
	}
    
    public List<DbProperties> getGrantedList() {
		return grantedList;
	}

	public List<DbProperties> getDeniedList() {
		return deniedList;
	}

	public void setGrantedList(List<DbProperties> grantedList) {
		this.grantedList = grantedList;
	}

	public void setDeniedList(List<DbProperties> deniedList) {
		this.deniedList = deniedList;
	}
    
    public List<SelectItem> getFirstComboItems() {
		return firstComboItems;
	}
    
    public void firstComboValueChanged(ValueChangeEvent e){
    	selectedFirstComboValue = (String)e.getNewValue();
    }
    
    public String getSelectedFirstComboValue() {
		return selectedFirstComboValue;
	}
    
    public void setSelectedFirstComboValue(String selectedFirstComboValue) {
		this.selectedFirstComboValue = selectedFirstComboValue;
	}

    private boolean donePasswordFlag = false;

    public boolean isDonePasswordFlag() {
        return donePasswordFlag;
    }

    public void setDonePasswordFlag(boolean donePasswordFlag) {
    	this.donePasswordFlag = donePasswordFlag;
    }

	public static String SQLITE_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss.SSS"; 
	
	public HtmlInputSecret getPwdControl() {
		return pwdControl;
	}

	public void setPwdControl(HtmlInputSecret pw) {
		this.pwdControl = pw;
	}

	public HtmlInputSecret getNewPwdControl() {
		return newPwdControl;
	}

	public void setNewPwdControl(HtmlInputSecret PW) {
		this.newPwdControl = PW;
	}

	public HtmlInputSecret getConfirmPwdControl() {
		return confirmPwdControl;
	}

	public void setConfirmPwdControl(HtmlInputSecret PW) {
		this.confirmPwdControl = PW;
	}

	public String getPwdChangeMessage() {
    	return this.pwdChangeMessage;
    }
    
    public void setPwdChangeMessage(String msg) {
    	this.pwdChangeMessage = msg;
    }

	
	public int getSelectedUserSecurityRowIndex() {
		return selectedUserSecurityRowIndex;
	}

	public void setSelectedUserSecurityRowIndex(int selectedUserSecurityRowIndex) {
		this.selectedUserSecurityRowIndex = selectedUserSecurityRowIndex;
	}
		
	public UserSecurityDTO getUserSecurityDTO() {
		return userSecurityDTO;
	}

	public void setUserSecurityDTO(UserSecurityDTO userSecurityDTO) {
		this.userSecurityDTO = userSecurityDTO;
	}
	
	
	/**
	 * @return the selectedDBStatistics
	 */
	public UserSecurityDTO getSelectedUserSecurity() {
		return selectedUserSecurity;
	}

	/**
	 * @param selectedDBStatistics the selectedDBStatistics to set
	 */
	public void setSelectedUserSecurity(UserSecurityDTO selectedUserSecurity) {
		this.selectedUserSecurity = selectedUserSecurity;
	}
	
	/**
	 * @return the dBStatisticsDTONew
	 */
	public UserSecurityDTO getUserSecurityDTOWork() {
		return userSecurityDTOWork;
	}

	/**
	 * @param dBStatisticsDTONew the dBStatisticsDTONew to set
	 */
	public void setUserSecurityDTOWork(UserSecurityDTO userSecurityDTOWork) {
		this.userSecurityDTOWork = userSecurityDTOWork;
	}

	public void selectedRowChanged(ActionEvent e) throws AbortProcessingException { 
		HtmlDataTable table = (HtmlDataTable) ((HtmlAjaxSupport) e.getComponent()).getParent(); 
		selectedUserSecurityRowIndex = table.getRowIndex();
		selectedUserSecurity = (UserSecurityDTO)table.getRowData();
	}
	
	public void filterSaveAction(ActionEvent e) {
    }

	public String takeSelection() {
		if(selectedUserSecurity==null){
			this.selectedUserSecurity = new UserSecurityDTO();
		}
		return "db_statistics_view";
	}
	
	public String changePasswordAction() {
		this.pwdChangeMessage = null;
		donePasswordFlag = false;
		
        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
        UserDTO user = (UserDTO) session.getAttribute("user");
        
        String userCode = user.getUserCode();
        UserSecurityDTO userSecurityDTO = getUserSecurity(userCode.toUpperCase());
		if(userSecurityDTO==null){	
			this.pwdChangeMessage = "User name not found.";
        	return null;  
		}
		String oldPwd = DesEncrypter.getInstance().decrypt(userSecurityDTO.getPassword());
        String pwd = (String) this.pwdControl.getValue();
        String newPwd = (String) this.newPwdControl.getValue();
        String confirmPwd = (String) this.confirmPwdControl.getValue();
        
        // Check for blank passwords
        if (pwd == null || "".equalsIgnoreCase(pwd.trim())) {
        	this.pwdChangeMessage = "Please enter current password.";
        	return null;        	
        }
        
        // Check entered current password against saved current password 
        if (!pwd.equals(oldPwd)){
        	this.pwdChangeMessage = "Password does not match user password.  Please correct.";
        	return null;
        }
        
        //Change password
        boolean passwordChange = false;
        if(!((newPwd == null || "".equalsIgnoreCase(newPwd.trim())) && 
        		(confirmPwd == null || "".equalsIgnoreCase(confirmPwd.trim())))) {
        	//Not all empty
        	if (newPwd == null || "".equalsIgnoreCase(newPwd.trim()) || 
        		confirmPwd == null || "".equalsIgnoreCase(confirmPwd.trim())) {
        		this.pwdChangeMessage = "Please enter all fields.";
        		return null;        	
        	}
            
            //check Password Complexity
            String returnMessage = isValidNewPassword(userSecurityDTO.getUserId(), pwd, newPwd, confirmPwd);
            if(returnMessage!=null){
            	this.pwdChangeMessage = returnMessage;
                return null;
            }
            
            passwordChange = true;
        }
        
        //Update password
        if(passwordChange){
        	if(!updateUserPassword(userCode.toUpperCase(), DesEncrypter.getInstance().encrypt(newPwd))){
        		this.pwdChangeMessage = "Cannot update password, please try again!";
                return null;
        	}
        }
 		
        pwdControl.setValue("");	
        newPwdControl.setValue("");
        confirmPwdControl.setValue("");
        donePasswordFlag = true;

        this.pwdChangeMessage = "Your password has been changed.";
        return null;
	}
	
	public void clearPasswordAction(){
		   pwdControl.setValue("");	
	       newPwdControl.setValue("");
	       confirmPwdControl.setValue("");
	       donePasswordFlag = false;
	}
	
	public String getActionUserSecurityText() {
		if(currentUserSecurityAction.equals(EditAction.ADD)){
			return "Add";
		}
		else if(currentUserSecurityAction.equals(EditAction.UPDATE)){
			return "Update";
		}
		else if(currentUserSecurityAction.equals(EditAction.VIEW)){
			return "View";
		}
		else if(currentUserSecurityAction.equals(EditAction.DELETE)){
			return "Delete";
		}
		else if(currentUserSecurityAction.equals(EditAction.RESET)){
			return "Reset";
		}
		else if(currentUserSecurityAction.equals(EditAction.UNLOCK)){
			return "Unlock";
		}
		else if(currentUserSecurityAction.equals(EditAction.LOCK)){
			return "Lock";
		}
		else if(currentUserSecurityAction.equals(EditAction.SEND)){
			return "Sent";
		}
		
		return "";
	}
	
	public boolean isShowEmailButtons() {
		return sendEmailFailed;
	}
	
	public boolean isShowUserLockButtons() {
		return (selectedUserSecurityRowIndex != -1 && selectedUserSecurity!=null && selectedUserSecurity.getLock().intValue()!=1);
	}
	
	public boolean isShowUserUnLockButtons() {
		return (selectedUserSecurityRowIndex != -1 && selectedUserSecurity!=null && selectedUserSecurity.getLock().intValue()==1);
	}
	
	public boolean getIsUserResetPasswordAction() {
		return currentUserSecurityAction.equals(EditAction.RESET);
	}
	
	public boolean getIsUserLockPasswordAction() {
		return currentUserSecurityAction.equals(EditAction.LOCK);
	}
	
	public boolean getIsUserSecurityAddAction() {
		return currentUserSecurityAction.equals(EditAction.ADD);
	}
	
	public boolean getIsUserSecurityDeleteAction() {
		if(currentUserSecurityAction.equals(EditAction.DELETE)){
			return true;
		}
    	
    	return false;
	}
	
	public boolean getIsUserSecurityUpdateAction() {
		if(currentUserSecurityAction.equals(EditAction.UPDATE)){
			return true;
		}
    	
    	return false;
	}
	
	public boolean getIsUserSecurityViewAction() {
		if(currentUserSecurityAction.equals(EditAction.VIEW)){
			return true;
		}
    	
    	return false;
	}

	public String okSendPasswordAction() {
		if(currentUserSecurityAction.equals(EditAction.SEND)){
		    if(userSecurityDTOWork.getPassword()==null || userSecurityDTOWork.getPassword().length()==0){	    	
		    	addMessage("Current password is empty.");
				return null;
		    }
		    
		    String errorMessage = sendEmailNotification(userSecurityDTOWork, "PASSWORD_REMINDER");
		    if(errorMessage!=null && errorMessage.length()>0){
		    	addMessage(errorMessage);
				return null;
		    }
		}
	   
		userSecurityDTOWork =  new UserSecurityDTO();
		
	    userSecurityList = null;
		selectedUserSecurity = null;
		selectedUserSecurityRowIndex = -1;
	      
	    return "usersecurity_main"; 
	}
	
	public String okResetPasswordAction() {
		if(currentUserSecurityAction.equals(EditAction.RESET)){
		    if(userSecurityDTOWork.getPassword()==null || userSecurityDTOWork.getPassword().length()<8){
		    	
		    	addMessage("New password is invalid.");
				return null;
		    }
		    
		    String errorMessage = sendEmailNotification(userSecurityDTOWork, "PASSWORD_RESET");
		    if(errorMessage!=null && errorMessage.length()>0){
		    	addMessage(errorMessage);
				return null;
		    }
		    else{
		    	//password has been encrypted already.
		    	if(!resetPassword(userSecurityDTOWork.getUserName(), userSecurityDTOWork.getPassword())){
		    		addMessage("Cannot update password, please try again!");
	                return null;
		    	}
		    }
		}
		else if(currentUserSecurityAction.equals(EditAction.UNLOCK)){
			String errorMessage = sendEmailNotification(userSecurityDTOWork, "PASSWORD_UNLOCKED");
		    if(errorMessage!=null && errorMessage.length()>0){
		    	addMessage(errorMessage);
				return null;
		    }
		    else{
		    	if(!unLockPassword(userSecurityDTOWork.getUserName())){
		    		addMessage("Cannot unlock password, please try again!");
	                return null;
		    	}
		    } 
		}
		else if(currentUserSecurityAction.equals(EditAction.LOCK)){
		    if(!lockPassword(userSecurityDTOWork.getUserName())){
		    	addMessage("Cannot lock password, please try again!");
	            return null;
		    }
		}
	   
		userSecurityDTOWork =  new UserSecurityDTO();
		
	    userSecurityList = null;
		selectedUserSecurity = null;
		selectedUserSecurityRowIndex = -1;
	      
	    return "usersecurity_main"; 
	}
	
	public String cancelUserSecurityActionAction() {
		return "usersecurity_main";
	}
	
	public boolean isShowUserSecurityButtons() {
		return (selectedUserSecurityRowIndex != -1);
	}
	
	
	public String refreshUserAction(){
		userSecurityList = null;
		selectedUserSecurity = null;
		selectedUserSecurityRowIndex = -1;
	      
	    return "usersecurity_main"; 
	}
	
	public String okUserSecurityAction() {
	    if(currentUserSecurityAction.equals(EditAction.ADD)){
	    	
	    	if(userSecurityDTOWork.getUserName()==null || userSecurityDTOWork.getUserName().trim().length()==0){
				addMessage("User name can not be empty.");
				return null;
			}
	    	
	    	UserSecurityDTO userSecurityDTO = getUserSecurity(userSecurityDTOWork.getUserName().trim());
	    	
			if(userSecurityDTO!=null){
				addMessage("User name has already existed.");
				return null;
			}
			
			userSecurityDTOWork.setRoleId(Integer.valueOf(selectedFirstComboValue));
			
			if(userSecurityDTOWork.getRoleId().intValue()==-1){
				addMessage("Please select a Role.");
				return null;
			}
			
			userSecurityDTOWork.setUserName(userSecurityDTOWork.getUserName().trim());
			String errorMessage = addUserSecurity(userSecurityDTOWork);
	    	if(errorMessage!=null && errorMessage.length()>0){
	    		addMessage(errorMessage);
	    		return null;
	    	}
	    	
	    	//Send email
	    	errorMessage = sendEmailNotification(userSecurityDTOWork, "ACCOUNT_CREATE");
			if(errorMessage!=null && errorMessage.length()>0){
			    addMessage("Account created, but send email failed. " + errorMessage);
			    sendEmailFailed = true;
				return null;
			}
		   
			userSecurityDTOWork =  new UserSecurityDTO();
	    }
	    else if(currentUserSecurityAction.equals(EditAction.UPDATE)){
	    	userSecurityDTOWork.setRoleId(Integer.valueOf(selectedFirstComboValue));
	    	
	    	if(userSecurityDTOWork.getRoleId().intValue()==-1){
				addMessage("Please select a Role.");
				return null;
			}
	    	
	    	String errorMessage = updateUserSecurity(userSecurityDTOWork);
	    	if(errorMessage!=null && errorMessage.length()>0){
	    		addMessage(errorMessage);
	    		return null;
	    	}
	    	
			logger.info("end updateAction");		
	    }
	    else if(currentUserSecurityAction.equals(EditAction.DELETE)){

	    	String errorMessage = deleteUserSecurity(userSecurityDTOWork);
	    	if(errorMessage!=null && errorMessage.length()>0){
	    		addMessage(errorMessage);
	    		return null;
	    	}
	    }
	    
	    userSecurityList = null;
		selectedUserSecurity = null;
		selectedUserSecurityRowIndex = -1;
	      
	    return "usersecurity_main"; 
	}
	
	public String okEmailAction() {
		sendEmailFailed = false;	
	    userSecurityList = null;
		selectedUserSecurity = null;
		selectedUserSecurityRowIndex = -1;
	      
	    return "usersecurity_main"; 
	}
	
	public String displayUserResetPasswordAction() {
		currentUserSecurityAction = EditAction.RESET;
		userSecurityDTOWork = new UserSecurityDTO(); 
		if(selectedUserSecurity!=null){
			userSecurityDTOWork.setUserId(selectedUserSecurity.getUserId());
			userSecurityDTOWork.setUserName(selectedUserSecurity.getUserName());
			userSecurityDTOWork.setEmail(selectedUserSecurity.getEmail());
		//	userSecurityDTOWork.setPassword(this.getNewPassword());
			
			userSecurityDTOWork.setPassword(DesEncrypter.getInstance().encrypt(this.getNewPassword()));
		}
		
		return "userresetpassword_update";
	}
	
	
	//displayUserSendPasswordAction
	public String displayUserSendPasswordAction() {
		currentUserSecurityAction = EditAction.SEND;
		userSecurityDTOWork = new UserSecurityDTO(); 
		if(selectedUserSecurity!=null){
			userSecurityDTOWork.setUserId(selectedUserSecurity.getUserId());
			userSecurityDTOWork.setUserName(selectedUserSecurity.getUserName());
			userSecurityDTOWork.setEmail(selectedUserSecurity.getEmail());
			userSecurityDTOWork.setPassword(selectedUserSecurity.getPassword());
		}
		
		return "userresetpassword_send";
	}
	
	public String displayUnlockUserSecurityAction() {
		currentUserSecurityAction = EditAction.UNLOCK;
		userSecurityDTOWork = new UserSecurityDTO(); 
		if(selectedUserSecurity!=null){
			userSecurityDTOWork.setUserId(selectedUserSecurity.getUserId());
			userSecurityDTOWork.setUserName(selectedUserSecurity.getUserName());
			userSecurityDTOWork.setEmail(selectedUserSecurity.getEmail());
		}
		
		return "userresetpassword_update";
	}
	
	public String displayLockUserSecurityAction() {
		currentUserSecurityAction = EditAction.LOCK;
		userSecurityDTOWork = new UserSecurityDTO(); 
		if(selectedUserSecurity!=null){
			userSecurityDTOWork.setUserId(selectedUserSecurity.getUserId());
			userSecurityDTOWork.setUserName(selectedUserSecurity.getUserName());
			userSecurityDTOWork.setEmail(selectedUserSecurity.getEmail());
		}
		
		return "userresetpassword_update";
	}
	
	public String displayAddUserSecurityAction() {
		sendEmailFailed = false;
		firstComboItems.clear();
		firstComboItems.add(new SelectItem("-1", "Select Role"));
    	selectedFirstComboValue = "-1";
    	List<UserRolesDTO> result = userRolesBean.findAllRolesList();
    	for(UserRolesDTO ul : result){
    		firstComboItems.add(new SelectItem(ul.getRoleId()+"", ul.getRoleName()));
    	}
		
		currentUserSecurityAction = EditAction.ADD;
		userSecurityDTOWork = new UserSecurityDTO();
	    return "usersecurity_update";
	}
	
	public String displayUpdateUserSecurityAction() {
		sendEmailFailed = false;
		firstComboItems.clear();
		firstComboItems.add(new SelectItem("-1", "Select Role"));
    	selectedFirstComboValue = "-1";
    	List<UserRolesDTO> result = userRolesBean.findAllRolesList();
    	for(UserRolesDTO ul : result){
    		firstComboItems.add(new SelectItem(ul.getRoleId()+"", ul.getRoleName()));
    	}
    	
    	selectedFirstComboValue =  selectedUserSecurity.getRoleId() + "";
		currentUserSecurityAction = EditAction.UPDATE;
		userSecurityDTOWork = new UserSecurityDTO(); 
		if(selectedUserSecurity!=null){
			userSecurityDTOWork.setUserId(selectedUserSecurity.getUserId());
			userSecurityDTOWork.setUserName(selectedUserSecurity.getUserName());
			userSecurityDTOWork.setEmail(selectedUserSecurity.getEmail());
			userSecurityDTOWork.setRoleId(selectedUserSecurity.getRoleId());
		}
		
		return "usersecurity_update";
	}
	
	public String displayViewUserSecurityAction() {
		String returnScreen =  displayUpdateUserSecurityAction();
		currentUserSecurityAction = EditAction.VIEW;
		return returnScreen;
		
	}
	
	public String displayDeleteUserSecurityAction() {
		sendEmailFailed = false;
		firstComboItems.clear();
		firstComboItems.add(new SelectItem("-1", "Select Role"));
    	selectedFirstComboValue = "-1";
    	List<UserRolesDTO> result = userRolesBean.findAllRolesList();
    	for(UserRolesDTO ul : result){
    		firstComboItems.add(new SelectItem(ul.getRoleId()+"", ul.getRoleName()));
    	}
    	
    	selectedFirstComboValue =  selectedUserSecurity.getRoleId() + "";
		currentUserSecurityAction = EditAction.DELETE;
		userSecurityDTOWork = new UserSecurityDTO(); 
		if(selectedUserSecurity!=null){
			userSecurityDTOWork.setUserId(selectedUserSecurity.getUserId());
			userSecurityDTOWork.setUserName(selectedUserSecurity.getUserName());
			userSecurityDTOWork.setEmail(selectedUserSecurity.getEmail());
			userSecurityDTOWork.setRoleId(selectedUserSecurity.getRoleId());
		}
	    return "usersecurity_update"; 
	}	
	
	public List<UserSecurityDTO> getUserSecurityList() {
		if (userSecurityList == null) {
			userSecurityList = findAllUserSecurityRoleList();
			selectedUserSecurityRowIndex = -1;
		}
		return userSecurityList;
	}
	
	public List<DbPropertiesDTO> findAllDbPropertiesList() throws DataAccessException {
		List<DbPropertiesDTO> result = new ArrayList<DbPropertiesDTO>();
		Connection conn = dataSource.CreateSQLiteConnection();
		if(conn==null){
			logger.error("############ JDBC Connection failed");
			return result;
		}
		
		Statement stat = null;
		try{
			stat = conn.createStatement();
	    	ResultSet rs = stat.executeQuery("select * from TB_DB_PROPERTIES order by DB_ID desc");
	    	while (rs.next()) {
	    		DbPropertiesDTO dbPropertiesDTO = new DbPropertiesDTO(); 
	    		dbPropertiesDTO.setDbId(rs.getInt("DB_ID"));
	    		dbPropertiesDTO.setUserName(rs.getString("USER_NAME"));
	    		dbPropertiesDTO.setPassword(rs.getString(RoutingDataSource.getColumnHeader(conn) + "PASSWORD"));
	    		dbPropertiesDTO.setDatabaseName(rs.getString("DATABASE_NAME"));
	    		dbPropertiesDTO.setDriverClassName(rs.getString("DRIVER_CLASS_NAME"));
	    		dbPropertiesDTO.setUrl(rs.getString("URL"));
	    		result.add(dbPropertiesDTO);   
	    	}
	    	rs.close();	
		}
		catch (Exception e){
			logger.error("############ findAllDbPropertiesList() failed: " + e.toString()); 
			conn = dataSource.RenewSQLiteConnection(conn);	 
		}
		finally{
			if(stat!= null){try{stat.close();}catch(Exception ex){}}
			dataSource.CloseSQLiteConnection(conn);
		}
		
		return result;
	}
	
	public List<String> findUserDbPropertiesList(Integer userId) throws DataAccessException {
		List<String> result = new ArrayList<String>();
		Connection conn = dataSource.CreateSQLiteConnection();
		if(conn==null){
			logger.error("############ JDBC Connection failed");
			return result;
		}
		
		Statement stat = null;
		try{
			stat = conn.createStatement();
	    	ResultSet rs = stat.executeQuery("select TB_DB_PROPERTIES.DATABASE_NAME from TB_USER_SECURITY, TB_DB_PROPERTIES, TB_USER_DB where TB_USER_SECURITY.USER_ID=" + userId + " and TB_USER_SECURITY.USER_ID=TB_USER_DB.USER_ID and TB_USER_DB.DB_ID=TB_DB_PROPERTIES.DB_ID");
	    	while (rs.next()) {
	    		String sDatabaseName = new String(rs.getString("DATABASE_NAME"));
	    		result.add(sDatabaseName);   
	    	}
	    	rs.close();	
		}
		catch (Exception e){
			logger.error("############ findUserDbPropertiesList() failed: " + e.toString()); 
			conn = dataSource.RenewSQLiteConnection(conn);	 
		}
		finally{
			if(stat!= null){try{stat.close();}catch(Exception ex){}}
			dataSource.CloseSQLiteConnection(conn);
		}
		
		return result;
	}
	
	private void addMessage(String errorMessage){
		FacesMessage message = new FacesMessage(errorMessage);
		message.setSeverity(FacesMessage.SEVERITY_ERROR);
		FacesContext.getCurrentInstance().addMessage(null, message);
	}
	
	public boolean removeUserDbMapping(String userCode, String dbName){		
		UserSecurityDTO userSecurityDTO = null;
		DbPropertiesDTO dbPropertiesDTO = null;	
		Connection conn = dataSource.CreateSQLiteConnection();
		if(conn==null){
			logger.error("############ JDBC Connection failed"); 
			return false;
		}
		
		ResultSet rs = null;
		Statement stat = null;
		PreparedStatement prep = null;
		try{		
			conn.setAutoCommit(false);
			
			//Find user ID
			stat = conn.createStatement();
	    	rs = stat.executeQuery("select * from TB_USER_SECURITY where UPPER(USER_NAME) = UPPER('" + userCode + "')");
	    	if (rs.next()) {
	    		userSecurityDTO = new UserSecurityDTO(); 
	    		userSecurityDTO.setUserId(rs.getInt("USER_ID"));
	    	}
	    	rs.close();	
	    	rs = null;
	    	stat.close();
	    	stat = null;
	
			//Find db ID
	    	stat = conn.createStatement();
	    	rs = stat.executeQuery("select * from TB_DB_PROPERTIES where UPPER(DATABASE_NAME) = UPPER('" + dbName + "')");
	    	if (rs.next()) {
	    		dbPropertiesDTO = new DbPropertiesDTO(); 
	    		dbPropertiesDTO.setDbId(rs.getInt("DB_ID"));
	    	}
	    	rs.close();	
	    	rs = null;
	    	stat.close();
	    	stat = null;

	    	if(userSecurityDTO==null || dbPropertiesDTO==null){
	    		conn.setAutoCommit(true);
	    		return false;
	    	}
	    	
	    	//Delete db mapping
	    	prep = conn.prepareStatement("delete from TB_USER_DB where DB_ID = ? and USER_ID = ?");
	    	prep.setInt(1, dbPropertiesDTO.getDbId().intValue());
	    	prep.setInt(2, userSecurityDTO.getUserId());
			prep.executeUpdate();
			prep.close();
			prep = null;
			
			//If there is no db mapping for user, delete user
			prep = conn.prepareStatement("select * from TB_USER_DB where USER_ID = ?");
	    	prep.setInt(1, userSecurityDTO.getUserId());
	    	rs = prep.executeQuery();
	    	boolean dbMapping = false;
	    	if (rs.next()) {
	    		dbMapping = true;
	    	}
	    	rs.close();	
	    	rs = null;
	    	prep.close();
	    	prep = null;
	    	
	    	if(!dbMapping){//Delete user
				prep = conn.prepareStatement("DELETE FROM TB_USER_SECURITY WHERE USER_ID = ?");
				prep.setInt(1, userSecurityDTO.getUserId());		 
				prep.executeUpdate();
				prep.close();
			    prep = null;
	    	}
	 	 
			conn.commit();
	    	conn.setAutoCommit(true); 	
		}
		catch (Exception e){
			logger.error("############ getDbPropertiesDTO() failed: " + e.toString()); 
			conn = dataSource.RenewSQLiteConnection(conn);
			return false;
		}
		finally{
			if(stat!= null){try{stat.close();}catch(Exception ex){}}		
			if(prep!=null){try{prep.close();}catch(Exception ex){}}
			
			dataSource.CloseSQLiteConnection(conn);
		}
		
		return true;
	}
	
	public boolean updateUserPassword(String username, String password){
		return updateUserPassword(username, password, false);
	}
	
	public boolean updateUserPassword(String username, String password, boolean resetPassword) throws DataAccessException {
		 Connection conn = dataSource.CreateSQLiteConnection();
		 if(conn==null){
			 logger.error("############ JDBC Connection failed");
			 return false;
		 }
		 
		 boolean result = true;
		 
		 PreparedStatement prep = null;
		 try{
			 conn.setAutoCommit(false);
			 
			 Date d = new Date(System.currentTimeMillis());
			 DateFormat df = new SimpleDateFormat(SQLITE_DATE_FORMAT);
			 String currentDateTime = df.format(d);
			 
			 UserSecurityDTO userSecurityDTO = getUserSecurity(username);
			 if(userSecurityDTO==null){
				 return false;
			 }
			 
			 //Check number of PASSWORD_HISTORY in TB_USER_ROLES
			 UserRolesDTO userRolesDTO = getUserRoles(userSecurityDTO.getRoleId());
			 if(userRolesDTO==null){
				 return false;
			 }
			  
			 if(userSecurityDTO.getChange().intValue()!=1 && userSecurityDTO.getPassword()!=null && userSecurityDTO.getPassword().length()>0){
				 //Update password history
				 int historyCount = userRolesDTO.getPasswordHistory();
				 List<PasswordHistoryDTO> passwordHistoryList = findUserPasswordHistoryList(userSecurityDTO.getUserId());
				 if(passwordHistoryList.size()>=historyCount){
					 //Need to update history
					 PasswordHistoryDTO passwordHistoryDTO = (PasswordHistoryDTO)passwordHistoryList.get(0);
					 prep = conn.prepareStatement("update TB_PASSWORD_HISTORY set LAST_SUCCESSFUL_LOGIN=?, PASSWORD_HISTORY=?,USER_ID=? where ID =?");
					
					 prep.setString(1, userSecurityDTO.getLastUpdateDate());
					 prep.setString(2, userSecurityDTO.getPassword());
					 prep.setInt(3, userSecurityDTO.getUserId());
					 prep.setInt(4, passwordHistoryDTO.getId());
					 prep.executeUpdate();
					 if(prep!=null){try{prep.close();}catch(Exception ex){}}
				 }
				 else{
					 //Need to insert into history
					 prep = conn.prepareStatement("insert into TB_PASSWORD_HISTORY (LAST_SUCCESSFUL_LOGIN,PASSWORD_HISTORY,USER_ID) values (?,?,?)");
					 prep.setString(1, userSecurityDTO.getLastUpdateDate());
					 prep.setString(2, userSecurityDTO.getPassword());
					 prep.setInt(3, userSecurityDTO.getUserId());
					 prep.executeUpdate();
					 if(prep!=null){try{prep.close();}catch(Exception ex){}}
				 }
			 }
			 
			 //Update TB_USER_SECURITY
			 int forcePasswordChange = (resetPassword==true) ? 1 : 0;
			 prep = conn.prepareStatement("update TB_USER_SECURITY set CURRENT_ATTEMPTS=0, " + RoutingDataSource.getColumnHeader(conn)+ "LOCK=0, " + RoutingDataSource.getColumnHeader(conn) + "CHANGE=?, LAST_UPDATE_DATE=?, " + RoutingDataSource.getColumnHeader(conn) + "PASSWORD=?  where USER_ID =?");		 
			 prep.setInt(1, forcePasswordChange); 
			 prep.setString(2, currentDateTime);
			 prep.setString(3, password);
			 prep.setInt(4, userSecurityDTO.getUserId());
			 prep.executeUpdate();

			 conn.commit();
	    	 conn.setAutoCommit(true);
		 }
		 catch (Exception e){
			 result = false;
			 try{
				 conn.rollback();
			 }
				 catch(Exception ex){
			 }
			 logger.error("############ updateUserSecurity() failed: " + e.toString()); 
			 conn = dataSource.RenewSQLiteConnection(conn);	 
		 }
		 finally{
			 if(prep!=null){try{prep.close();}catch(Exception ex){}}
			 dataSource.CloseSQLiteConnection(conn);
		 }
		 
		 return result;
	}
	
	public boolean isPasswordInPasswordHistory(Integer userId, String password) throws DataAccessException {
		boolean result = false;
		Connection conn = dataSource.CreateSQLiteConnection();
		if(conn==null){
			logger.error("############ JDBC Connection failed");
			return true;
		}
		
		Statement stat = null;
		try{
			stat = conn.createStatement();
			String encrypted = DesEncrypter.getInstance().encrypt(password); 			
	    	ResultSet rs = stat.executeQuery("select * from TB_PASSWORD_HISTORY where USER_ID="+ userId.intValue() + " and PASSWORD_HISTORY='"+ encrypted + "'");
	    	while (rs.next()) {
	    		result = true;
	    	}
	    	rs.close();	
		}
		catch (Exception e){
			logger.error("############ isPasswordInPasswordHistory() failed: " + e.toString()); 
			conn = dataSource.RenewSQLiteConnection(conn);	 
		}
		finally{
			if(stat!= null){try{stat.close();}catch(Exception ex){}}
			dataSource.CloseSQLiteConnection(conn);
		}
		
		return result;
	}
	
	public List<PasswordHistoryDTO> findUserPasswordHistoryList(Integer userId) throws DataAccessException {
		List<PasswordHistoryDTO> result = new ArrayList<PasswordHistoryDTO>();
		Connection conn = dataSource.CreateSQLiteConnection();
		if(conn==null){
			logger.error("############ JDBC Connection failed");
			return result;
		}
		
		Statement stat = null;
		try{
			stat = conn.createStatement();
	    	ResultSet rs = stat.executeQuery("select ID,  LAST_SUCCESSFUL_LOGIN, PASSWORD_HISTORY, USER_ID from TB_PASSWORD_HISTORY where USER_ID="+ userId.intValue() + " order by LAST_SUCCESSFUL_LOGIN asc");
	    	while (rs.next()) {
	    		PasswordHistoryDTO passwordHistoryDTO = new PasswordHistoryDTO(); 
	    		passwordHistoryDTO.setId(rs.getInt("ID"));
	    		passwordHistoryDTO.setLastSuccessfulLogin(rs.getString("LAST_SUCCESSFUL_LOGIN"));
	    		passwordHistoryDTO.setPasswordHistory(rs.getString("PASSWORD_HISTORY"));
	    		passwordHistoryDTO.setUserId(rs.getInt("USER_ID"));
	    		result.add(passwordHistoryDTO);   
	    	}
	    	rs.close();	
		}
		catch (Exception e){
			logger.error("############ findUserPasswordHistoryList() failed: " + e.toString()); 
			conn = dataSource.RenewSQLiteConnection(conn);	 
		}
		finally{
			if(stat!= null){try{stat.close();}catch(Exception ex){}}
			dataSource.CloseSQLiteConnection(conn);
		}
		
		return result;
	}
	
	public List<UserSecurityDTO> findAllUserSecurityList() throws DataAccessException {
		List<UserSecurityDTO> result = new ArrayList<UserSecurityDTO>();
		Connection conn = dataSource.CreateSQLiteConnection();
		if(conn==null){
			logger.error("############ JDBC Connection failed");
			return result;
		}
		
		Statement stat = null;
		try{
			stat = conn.createStatement();
	    	ResultSet rs = stat.executeQuery("select * from TB_USER_SECURITY order by USER_ID desc");
	    	while (rs.next()) {
	    		UserSecurityDTO userSecurityDTO = new UserSecurityDTO(); 
	    		userSecurityDTO.setUserId(rs.getInt("USER_ID"));
	    		userSecurityDTO.setUserName(rs.getString("USER_NAME"));
	    		userSecurityDTO.setPassword(rs.getString(RoutingDataSource.getColumnHeader(conn) + "PASSWORD"));
	    		userSecurityDTO.setLastUpdateDate(rs.getString("LAST_UPDATE_DATE"));
	    		userSecurityDTO.setRoleId(rs.getInt("ROLE_ID"));
	    		userSecurityDTO.setCurrentAttempts(rs.getInt("CURRENT_ATTEMPTS"));
	    		userSecurityDTO.setLock(rs.getInt(RoutingDataSource.getColumnHeader(conn) + "LOCK"));
	    		userSecurityDTO.setChange(rs.getInt(RoutingDataSource.getColumnHeader(conn) + "CHANGE"));
	    		userSecurityDTO.setCreateDate(rs.getString("CREATE_DATE"));
	    		userSecurityDTO.setEmail(rs.getString("EMAIL"));
	    		result.add(userSecurityDTO);   
	    	}
	    	rs.close();	
		}
		catch (Exception e){
			logger.error("############ findAllUserSecurityList() failed: " + e.toString()); 
			conn = dataSource.RenewSQLiteConnection(conn);	 
		}
		finally{
			if(stat!= null){try{stat.close();}catch(Exception ex){}}
			dataSource.CloseSQLiteConnection(conn);
		}
		
		return result;
	}
	
	public List<UserSecurityDTO> findAllUserSecurityRoleList() throws DataAccessException {
		List<UserSecurityDTO> result = new ArrayList<UserSecurityDTO>();
		Connection conn = dataSource.CreateSQLiteConnection();
		if(conn==null){
			logger.error("############ JDBC Connection failed");
			return result;
		}
		
		Statement stat = null;
		try{
			stat = conn.createStatement();
	    	ResultSet rs = stat.executeQuery("select TB_USER_SECURITY.* , TB_USER_ROLES.ROLE_NAME from TB_USER_SECURITY, TB_USER_ROLES where TB_USER_ROLES.ROLE_ID = TB_USER_SECURITY.ROLE_ID  order by USER_ID desc");
	    	while (rs.next()) {
	    		UserSecurityDTO userSecurityDTO = new UserSecurityDTO(); 
	    		userSecurityDTO.setUserId(rs.getInt("USER_ID"));
	    		userSecurityDTO.setUserName(rs.getString("USER_NAME"));
	    		userSecurityDTO.setPassword(rs.getString(RoutingDataSource.getColumnHeader(conn) + "PASSWORD"));
	    		userSecurityDTO.setLastUpdateDate(rs.getString("LAST_UPDATE_DATE"));
	    		userSecurityDTO.setRoleId(rs.getInt("ROLE_ID"));
	    		userSecurityDTO.setCurrentAttempts(rs.getInt("CURRENT_ATTEMPTS"));
	    		userSecurityDTO.setLock(rs.getInt(RoutingDataSource.getColumnHeader(conn) + "LOCK"));
	    		userSecurityDTO.setChange(rs.getInt(RoutingDataSource.getColumnHeader(conn) + "CHANGE"));
	    		userSecurityDTO.setCreateDate(rs.getString("CREATE_DATE"));
	    		userSecurityDTO.setEmail(rs.getString("EMAIL"));
	    		userSecurityDTO.setRoleName(rs.getString("ROLE_NAME"));
	    		result.add(userSecurityDTO);   
	    	}
	    	rs.close();	
		}
		catch (Exception e){
			logger.error("############ findAllUserSecurityRoleList() failed: " + e.toString()); 
			conn = dataSource.RenewSQLiteConnection(conn);	 
		}
		finally{
			if(stat!= null){try{stat.close();}catch(Exception ex){}}
			dataSource.CloseSQLiteConnection(conn);
		}
		
		return result;
	}
	
	public UserSecurityDTO getUserSecurity(String username) throws DataAccessException {
		UserSecurityDTO userSecurityDTO = null;
		Connection conn = dataSource.CreateSQLiteConnection();
		if(conn==null){
			logger.error("############ JDBC Connection failed");
			return null;
		}
		Statement stat = null;
		try{
			stat = conn.createStatement();
	    	ResultSet rs = stat.executeQuery("select * from TB_USER_SECURITY where UPPER(USER_NAME) = UPPER('" + username + "')");
	    	if (rs.next()) {
	    		userSecurityDTO = new UserSecurityDTO(); 
	    		userSecurityDTO.setUserId(rs.getInt("USER_ID"));
	    		userSecurityDTO.setUserName(rs.getString("USER_NAME"));
	    		userSecurityDTO.setPassword(rs.getString(RoutingDataSource.getColumnHeader(conn) + "PASSWORD"));
	    		userSecurityDTO.setLastUpdateDate(rs.getString("LAST_UPDATE_DATE"));
	    		userSecurityDTO.setRoleId(rs.getInt("ROLE_ID"));
	    		userSecurityDTO.setCurrentAttempts(rs.getInt("CURRENT_ATTEMPTS"));
	    		userSecurityDTO.setLock(rs.getInt(RoutingDataSource.getColumnHeader(conn) + "LOCK"));
	    		userSecurityDTO.setChange(rs.getInt(RoutingDataSource.getColumnHeader(conn) + "CHANGE"));
	    		userSecurityDTO.setCreateDate(rs.getString("CREATE_DATE"));
	    		userSecurityDTO.setEmail(rs.getString("EMAIL"));
	    	}
	    	rs.close();	
		}
		catch (Exception e){
			logger.error("############ getUserSecurity() failed: " + e.toString()); 
			conn = dataSource.RenewSQLiteConnection(conn);	 
		}
		finally{
			if(stat!= null){try{
				//stat.clearBatch();
				stat.close();}
			catch(Exception ex){}}
			dataSource.CloseSQLiteConnection(conn);
		}
		
		return userSecurityDTO;
	}
	
	public UserRolesDTO getUserRoles(Integer roleId) throws DataAccessException {
		UserRolesDTO userRolesDTO = null;
		Connection conn = dataSource.CreateSQLiteConnection();
		if(conn==null){
			logger.error("############ JDBC Connection failed");
			return null;
		}
		
		Statement stat = null;
		try{
			stat = conn.createStatement();
	    	ResultSet rs = stat.executeQuery("select * from TB_USER_ROLES where ROLE_ID=" + roleId);
	    	if (rs.next()) {
	    		userRolesDTO = new UserRolesDTO(); 
	    		userRolesDTO.setPasswordHistory(rs.getInt("PASSWORD_HISTORY"));
	    		userRolesDTO.setInvalidAttempts(rs.getInt("INVALID_ATTEMPTS"));
	    		userRolesDTO.setNotificationDays(rs.getInt("NOTIFICATION_DAYS"));
	    		userRolesDTO.setExpirationDays(rs.getInt("EXPIRATION_DAYS"));
	    		userRolesDTO.setRoleId(rs.getInt("ROLE_ID"));
	    		userRolesDTO.setRoleName(rs.getString("ROLE_NAME"));
	    		userRolesDTO.setAdmin(rs.getInt("ADMIN"));
	    	}
	    	rs.close();	
		}
		catch (Exception e){
			logger.error("############ getUserRoles() failed: " + e.toString()); 
			conn = dataSource.RenewSQLiteConnection(conn);	 
		}
		finally{
			if(stat!= null){
				try{
					stat.close();
				}catch(Exception ex){}}
			dataSource.CloseSQLiteConnection(conn);
		}
		
		return userRolesDTO;
	}
	
	public String addUserSecurity(UserSecurityDTO userSecurityDTO) throws DataAccessException {
		 String errorMessage = "";
		 Connection conn = dataSource.CreateSQLiteConnection();
		 if(conn==null){
			 logger.error("############ JDBC Connection failed");
			 return "Connection failed";
		 }
		 
		 PreparedStatement prep = null;
		 try{
			 prep = conn.prepareStatement("insert into TB_USER_SECURITY (ROLE_ID, USER_NAME, EMAIL, CREATE_DATE, LAST_UPDATE_DATE, " + RoutingDataSource.getColumnHeader(conn) + "LOCK, CURRENT_ATTEMPTS) values (?, ?, ?, ?, ?, 0, 0)");
			 Date d = new Date(System.currentTimeMillis());
			 DateFormat df = new SimpleDateFormat(SQLITE_DATE_FORMAT);
			 String currentDateTime = df.format(d);
			 
			 prep.setInt(1, userSecurityDTO.getRoleId());
			 prep.setString(2, userSecurityDTO.getUserName());
			 prep.setString(3, userSecurityDTO.getEmail());
			 prep.setString(4, currentDateTime);
			 prep.setString(5, currentDateTime);
			 prep.addBatch();

			 conn.setAutoCommit(false);
			 prep.executeBatch();
			 
			 conn.commit();
	    	 conn.setAutoCommit(true);
		 }
		 catch (Exception e){
			 try{
				 conn.rollback();
			 }
			 catch(Exception ex){
			 }
			 logger.error("############ addUserSecurity() failed: " + e.toString()); 
			 errorMessage = "Cannot add user, please try again!"; 
			 conn = dataSource.RenewSQLiteConnection(conn);	 
		 }
		 finally{
			 if(prep!=null){try{prep.close();}catch(Exception ex){}}
			 dataSource.CloseSQLiteConnection(conn);
		 }
		 
		 return errorMessage;
	}
	
	public String updateUserSecurity(UserSecurityDTO userSecurityDTO) throws DataAccessException {
		String errorMessage = ""; 
		Connection conn = dataSource.CreateSQLiteConnection();
		if(conn==null){
			logger.error("############ JDBC Connection failed");
			return "Connection failed";
		}
		
		PreparedStatement prep = null;
		 try{
			 prep = conn.prepareStatement("UPDATE TB_USER_SECURITY SET ROLE_ID = ? ,  EMAIL = ?   WHERE USER_ID = ?");

			 prep.setInt(1, userSecurityDTO.getRoleId());
			 prep.setString(2, userSecurityDTO.getEmail());
			 prep.setInt(3, userSecurityDTO.getUserId());

			 conn.setAutoCommit(false);
			 prep.executeUpdate();
			 
			 conn.commit();
	    	 conn.setAutoCommit(true);
		 }
		 catch (Exception e){
			 errorMessage = "Failed to update user, please try again!"; 
			 try{
				 conn.rollback();
			 }
			 catch(Exception ex){
			 }
			 logger.error("############ updateUserSecurity() failed: " + e.toString()); 
			 
			 conn = dataSource.RenewSQLiteConnection(conn);	 
		 }
		 finally{
			 if(prep!=null){try{prep.close();}catch(Exception ex){}}
			 dataSource.CloseSQLiteConnection(conn);
		 }
		 return errorMessage;
	}
	
	public String deleteUserSecurity(UserSecurityDTO userSecurityDTO) throws DataAccessException {
		
		//Delete user information in PinPoint db
		List<DbProperties> grantedList = findDbUserList(userSecurityDTO.getUserId());
		for (DbProperties dbProperties : grantedList){
			try {
				Class.forName(dbProperties.getDriverClassName());
			}
			catch (ClassNotFoundException e) {
				System.out.println("xxxxxxxxxxxxxx Can't load class: " + dbProperties.getDriverClassName());
				return "";
			}

			Connection conn = null;
			try {
				//conn = DriverManager.getConnection("jdbc:oracle:thin:@10.1.106.14:1521:TESTDB4", "STSCORP","STSCORP");
				conn = DriverManager.getConnection(dbProperties.getUrl(), dbProperties.getUser(),dbProperties.getPassword());	
				JPAUserDAO.deleteUserInfo(conn, userSecurityDTO.getUserName());
				conn.close();
				conn = null;
			}
			catch (Throwable e) {
				e.printStackTrace();
			} finally {
				if (conn != null){
					try {
						conn.close();
					}
					catch (SQLException ignore) {
					}
				}
			}
		}

		String errorMessage = ""; 
		Connection conn = dataSource.CreateSQLiteConnection();
		if(conn==null){
			logger.error("############ JDBC Connection failed");
			return "Connection failed";
		}
		
		PreparedStatement prep = null;
		 try{
			 conn.setAutoCommit(false);
			 
			 //Delete user
			 prep = conn.prepareStatement("DELETE FROM TB_USER_SECURITY WHERE USER_ID = ?");
			 prep.setInt(1, userSecurityDTO.getUserId());		 
			 prep.executeUpdate();
			 if(prep!=null){try{prep.close();}catch(Exception ex){}}
			 
			 //Delete db mapping
			 prep = conn.prepareStatement("delete from TB_USER_DB where USER_ID = ?");
			 prep.setInt(1, userSecurityDTO.getUserId());
			 prep.executeUpdate();
			 if(prep!=null){try{prep.close();}catch(Exception ex){}}
			 
			 //Delete password history
			 prep = conn.prepareStatement("delete from TB_PASSWORD_HISTORY where USER_ID = ?");
			 prep.setInt(1, userSecurityDTO.getUserId());
			 prep.executeUpdate();

			 conn.commit();
	    	 conn.setAutoCommit(true);
		 }
		 catch (Exception e){
			 errorMessage = "Cannot delete user, please try again!"; 
			 try{
				 conn.rollback();
			 }
			 catch(Exception ex){
			 }
			 logger.error("############ deleteUserSecurity() failed: " + e.toString()); 
			 conn = dataSource.RenewSQLiteConnection(conn);	 
		 }
		 finally{
			 if(prep!=null){try{prep.close();}catch(Exception ex){}}
			 dataSource.CloseSQLiteConnection(conn);
		 }
		 return errorMessage;
	}
	
	public void setUserSecurityList(List<UserSecurityDTO> userSecurityList) {
		this.userSecurityList = userSecurityList;
	}
	
	public boolean unLockPassword(String username){
		 //set CURRENT_ATTEMPTS to 0, set LOCK to 0;
		 UserSecurityDTO userSecurityDTO = getUserSecurity(username);
		 if(userSecurityDTO==null){
			 return false;
		 }
		
		 Connection conn = dataSource.CreateSQLiteConnection();
		 if(conn==null){
			 logger.error("############ JDBC Connection failed");
			 return false;
		 }
		 boolean result = true;
		 
		 PreparedStatement prep = null;
		 try{
			 conn.setAutoCommit(false);

			 //Update TB_USER_SECURITY
			 prep = conn.prepareStatement("update TB_USER_SECURITY set CURRENT_ATTEMPTS=0, " + RoutingDataSource.getColumnHeader(conn) + "LOCK=0 where USER_ID =?");
			 prep.setInt(1, userSecurityDTO.getUserId());
			 prep.executeUpdate();

			 conn.commit();
	    	 conn.setAutoCommit(true);
		 }
		 catch (Exception e){
			 result = false;
			 try{
				 conn.rollback();
			 }
				 catch(Exception ex){
			 }
			 logger.error("############ unLockPassword() failed: " + e.toString()); 
			 conn = dataSource.RenewSQLiteConnection(conn);	 
		 }
		 finally{
			 if(prep!=null){try{prep.close();}catch(Exception ex){}}
			 dataSource.CloseSQLiteConnection(conn);
		 }
		 
		 return result;
	}
	
	public boolean lockPassword(String username){
		//set LOCK to 1;
		Connection conn = dataSource.CreateSQLiteConnection();
		if(conn==null){
			logger.error("############ JDBC Connection failed");
			return false;
		}
		
		 boolean result = true;
		 
		 PreparedStatement prep = null;
		 try{
			 conn.setAutoCommit(false);
			 
			 Date d = new Date(System.currentTimeMillis());
			 DateFormat df = new SimpleDateFormat(SQLITE_DATE_FORMAT);
			 String currentDateTime = df.format(d);
			 
			 UserSecurityDTO userSecurityDTO = getUserSecurity(username);
			 if(userSecurityDTO==null){
				 return false;
			 }

			 //Update TB_USER_SECURITY
			 prep = conn.prepareStatement("update TB_USER_SECURITY set " + RoutingDataSource.getColumnHeader(conn) + "LOCK=1 where USER_ID =?");
			 prep.setInt(1, userSecurityDTO.getUserId());
			 prep.executeUpdate();

			 conn.commit();
	    	 conn.setAutoCommit(true);
		 }
		 catch (Exception e){
			 result = false;
			 try{
				 conn.rollback();
			 }
				 catch(Exception ex){
			 }
			 logger.error("############ lockPassword() failed: " + e.toString()); 
			 conn = dataSource.RenewSQLiteConnection(conn);	 
		 }
		 finally{
			 if(prep!=null){try{prep.close();}catch(Exception ex){}}
			 dataSource.CloseSQLiteConnection(conn);
		 }
		 
		 return result;
	}
	
	public boolean resetPassword(String username, String newPassword){
		//set CURRENT_ATTEMPTS to 0, set LOCK to 0, copy PASSWORD to history, generate new PASSWORD ;
		//String password = getNewPassword();
		String password = newPassword;
		
		if(password==null || password.length()<8){
			return false;
		}
		
		return updateUserPassword(username, password, true);
	}
	
	public String getNewPassword(){
	
		String newPassword = "";
		int random = 1;
        SecureRandom wheel = null;
        
        try{
        	wheel = SecureRandom.getInstance("SHA1PRNG");
        }
        catch(Exception e){
        	return null;
        }

        char[] lowerCase = new char[]{'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'};
        char[] upperCase = new char[]{'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'};
        char[] numeric = new char[]{'1', '2', '3', '4', '5', '6', '7', '8', '9', '0'};

        char[] printableAscii = new char[]{'!', '\"', '#', '$', '%', '&', '\'', '(', ')', '*', '+', ',', '-', '.', '/',
            '1', '2', '3', '4', '5', '6', '7', '8', '9', '0', ':', ';', '<', '=', '>', '?', '@',
            'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
            '[', '\\', ']', '^', '_', '`', '{', '|', '}', '~',
            'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'};

        char[] alphaNumberic = new char[]{'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
            'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
            '1', '2', '3', '4', '5', '6', '7', '8', '9', '0'};

        int iOption = wheel.nextInt(alphaNumberic.length)%3;
        if(iOption==1){
        	random = wheel.nextInt(alphaNumberic.length);
	        newPassword += alphaNumberic[random];
	        
	        random = wheel.nextInt(printableAscii.length);
	        newPassword += printableAscii[random];
	        
	        random = wheel.nextInt(numeric.length);
	        newPassword += numeric[random];
	        
	        random = wheel.nextInt(printableAscii.length);
	        newPassword += printableAscii[random];
	        
	        random = wheel.nextInt(upperCase.length);
	        newPassword += upperCase[random];
	        
	        random = wheel.nextInt(printableAscii.length);
	        newPassword += printableAscii[random];
	        
	        random = wheel.nextInt(lowerCase.length);
	        newPassword += lowerCase[random];
	        
	        random = wheel.nextInt(alphaNumberic.length);
	        newPassword += alphaNumberic[random];
        }
        else if(iOption==0){
            random = wheel.nextInt(alphaNumberic.length);
            newPassword += alphaNumberic[random];
            
            random = wheel.nextInt(printableAscii.length);
            newPassword += printableAscii[random];
            
            random = wheel.nextInt(lowerCase.length);
            newPassword += lowerCase[random];
            
            random = wheel.nextInt(printableAscii.length);
            newPassword += printableAscii[random];
            
            random = wheel.nextInt(printableAscii.length);
	        newPassword += printableAscii[random];
            
            random = wheel.nextInt(upperCase.length);
            newPassword += upperCase[random];
            
            random = wheel.nextInt(printableAscii.length);
            newPassword += printableAscii[random];
            
            random = wheel.nextInt(numeric.length);
            newPassword += numeric[random];
            
            random = wheel.nextInt(alphaNumberic.length);
            newPassword += alphaNumberic[random];
        
        }
        else{
        	random = wheel.nextInt(alphaNumberic.length);
	        newPassword += alphaNumberic[random];
	        
	        random = wheel.nextInt(printableAscii.length);
	        newPassword += printableAscii[random];
	        
	        random = wheel.nextInt(printableAscii.length);
	        newPassword += printableAscii[random];
	        
	        random = wheel.nextInt(upperCase.length);
	        newPassword += upperCase[random];
	        
	        random = wheel.nextInt(printableAscii.length);
	        newPassword += printableAscii[random];
	        
	        random = wheel.nextInt(numeric.length);
	        newPassword += numeric[random];
	        
	        random = wheel.nextInt(printableAscii.length);
	        newPassword += printableAscii[random];
	        
	        random = wheel.nextInt(lowerCase.length);
	        newPassword += lowerCase[random];
	        
	        random = wheel.nextInt(printableAscii.length);
	        newPassword += printableAscii[random];
	        
	        random = wheel.nextInt(alphaNumberic.length);
	        newPassword += alphaNumberic[random];
        }

		return newPassword;
	}
	
	public boolean addAttemptToUserSecurity(String username){
		//set CURRENT_ATTEMPTS to 0, set LOCK to 0;
		
		Connection conn = dataSource.CreateSQLiteConnection();
		if(conn==null){
			logger.error("############ JDBC Connection failed");
			return false;
		}
		
		 boolean result = true;
		 PreparedStatement prep = null;
		 try{
			 conn.setAutoCommit(false);
			 
			 UserSecurityDTO userSecurityDTO = getUserSecurity(username);
			 if(userSecurityDTO==null){
				 return false;
			 }

			//Check number of PASSWORD_HISTORY in TB_USER_ROLES
			 UserRolesDTO userRolesDTO = getUserRoles(userSecurityDTO.getRoleId());
			 if(userRolesDTO==null){
				 return false;
			 }
			 
			 //Locked? 
			 if(userSecurityDTO.getLock()!=null && userSecurityDTO.getLock()==1){
				 return true; //No change needed
			 }
			 
			 boolean needLocked = true;
			 if(userSecurityDTO.getCurrentAttempts()+1 < userRolesDTO.getInvalidAttempts()){
				 needLocked = false;
			 }
			 
			 //Update TB_USER_SECURITY
			 prep = conn.prepareStatement("update TB_USER_SECURITY set CURRENT_ATTEMPTS=?, " + RoutingDataSource.getColumnHeader(conn) + "LOCK=? where USER_ID =?");
			 
			 prep.setInt(1, userSecurityDTO.getCurrentAttempts()+1);
			 prep.setInt(2, (needLocked) ? 1:0); 
			 prep.setInt(3, userSecurityDTO.getUserId());
			 prep.executeUpdate();

			 conn.commit();
	    	 conn.setAutoCommit(true);
		 }
		 catch (Exception e){
			 result = false;
			 try{
				 conn.rollback();
			 }
				 catch(Exception ex){
			 }
			 logger.error("############ addAttemptToUserSecurity() failed: " + e.toString()); 
			 conn = dataSource.RenewSQLiteConnection(conn);	 
		 }
		 finally{
			 if(prep!=null){try{prep.clearParameters();prep.close();}catch(Exception ex){}}
			 dataSource.CloseSQLiteConnection(conn);
		 }
		 
		 return result;
	}
	
	public boolean resetAttemptToUserSecurity(String username){
		UserSecurityDTO userSecurityDTO = getUserSecurity(username);
		if(userSecurityDTO==null){
			return false;
		}
		 
		//set CURRENT_ATTEMPTS to 0
		Connection conn = dataSource.CreateSQLiteConnection();
		if(conn==null){
			logger.error("############ JDBC Connection failed");
			return false;
		}
		
		 boolean result = true;
		 PreparedStatement prep = null;
		 try{
			 conn.setAutoCommit(false);

			 prep = conn.prepareStatement("update TB_USER_SECURITY set CURRENT_ATTEMPTS=? where USER_ID =?");
			 
			 prep.setInt(1, 0);
			 prep.setInt(2, userSecurityDTO.getUserId());
			 prep.executeUpdate();

			 conn.commit();
	    	 conn.setAutoCommit(true);
		 }
		 catch (Exception e){
			 result = false;
			 try{
				 conn.rollback();
			 }
				 catch(Exception ex){
			 }
			 logger.error("############ resetAttemptToUserSecurity() failed: " + e.toString());
			 conn = dataSource.RenewSQLiteConnection(conn);	 
		 }
		 finally{
			 if(prep!=null){try{prep.close();}catch (Exception ex){}}
			 
			 dataSource.CloseSQLiteConnection(conn);
		 }
		 
		 return result;
	}
	
	public static void main(String[] args) throws Exception {
	}
	
	public String processAppLogon(String username, String password){
		
		UserSecurityDTO userSecurityDTO = getUserSecurity(username);
		if(userSecurityDTO==null){
			 return "User name is not found.";
		}

		UserRolesDTO userRolesDTO = getUserRoles(userSecurityDTO.getRoleId());
		if(userRolesDTO==null){
			return "User role is not found.";
		}
		
		//Check if the user has been locked.
		if(userSecurityDTO.getLock()!=null && userSecurityDTO.getLock()==1){
			return "Password has been locked. Please check with the administrator.";
		}
		
		if(!userSecurityDTO.getPassword().equals(password)){
			addAttemptToUserSecurity(username);
			return "Password is invalid.";
		}
		
		return "";
	}
	
	public int passwordExpiredInDays(String username){
		//return -2: in good standing
		//return -1: password expired
		//return greater than or equal 0: expired in days
		
		UserSecurityDTO userSecurityDTO = getUserSecurity(username);
		if(userSecurityDTO==null){
			return -1;
		}

		UserRolesDTO userRolesDTO = getUserRoles(userSecurityDTO.getRoleId());
		if(userRolesDTO==null){
			return -1;
		}
		
		long currentDate = System.currentTimeMillis();
		DateFormat df = new SimpleDateFormat(SQLITE_DATE_FORMAT);
		long lastLogonDate = 0;
		try{
			lastLogonDate = df.parse(userSecurityDTO.getLastUpdateDate()).getTime();
		}
		catch(Exception e){
		}
		
		if(lastLogonDate==0){
			return -1;
		}
		
		long expiredDate = lastLogonDate + (86400 * 1000 * userRolesDTO.getExpirationDays().longValue());
		if(currentDate >= expiredDate){
			return -1;
		}
		
		long willExpiredDate = lastLogonDate + 86400 * 1000 * (userRolesDTO.getExpirationDays().longValue() - userRolesDTO.getNotificationDays().longValue());
		
		if(currentDate >= willExpiredDate){
			double doubleDays = (expiredDate-currentDate)/(86400 * 1000);
			int iDays = (int)doubleDays;
			return iDays;
		}
		
		return -2;
	}
	
	public String isValidNewPassword(Integer userId, String oldPassword, String newPassword, String newPasswordConfirmation){
		if (newPassword==null || newPassword.length()<8) {
			return "Must have at least 8 characters in length.";
		}
		
		if (newPassword.equals(oldPassword)) {
			return "New Password can not be the same as Current Password.";
		}

		if (!passwordCheck(newPassword)) {
			return "Must contain at least three of the four: upper-case, lower-case, number, special character.";
		}
		
		if (!newPassword.equals(newPasswordConfirmation)) {
			return "New Password and ReEnter New Password do not match.";
		}
		
		//Check for password history
		if(isPasswordInPasswordHistory(userId, newPassword)){
			return "Password does not meet the password history requirements.";
		}
		
		return null;
	}	

	public boolean passwordCheck(String password){

		boolean foundUpper = false;
		boolean foundLower = false;
		boolean foundNumber = false;
		boolean foundSpecial = false;
    
		for(int i=0; i<password.length(); i++ ){
			int ansi = (char)password.charAt(i);
       
			if((ansi>=33 && ansi<=47) || (ansi>=58 && ansi<=64) || (ansi>=91 && ansi<=96) || (ansi>=123 && ansi<=126)){
				//for special characters, no white space
				//!"#$%&'()*+,-./
				//:;<=>?@
				//[\]^_`
				//{|}~
				foundSpecial = true;
			}
			else if(ansi>=48 && ansi<=57){
				//0~9
				foundNumber = true;
			}
			else if(ansi>=65 && ansi<=90){
				//A~Z
				foundUpper = true;
			}
			else if(ansi>=97 && ansi<=122){
				//a~z
				foundLower = true;
			}
			else{
				//Invalid character
				return false;
			}
		}
    
		int count = 0;
		if(foundSpecial) count++;
		if(foundNumber) count++;
		if(foundUpper) count++;
		if(foundLower) count++;

		if(count<3){
			return false;
		}

		return true;
	}
	
	private void addErrorMessage(String strError){
		FacesMessage message = new FacesMessage(strError);
		message.setSeverity(FacesMessage.SEVERITY_ERROR);
		FacesContext.getCurrentInstance().addMessage(null, message);
	}
	
	public String sendEmailNotification(UserSecurityDTO userSecurityDTO, String actionStr) {
    	
    	SmtpPropertiesDTO smtpPropertiesDTO = null;
		try {
			smtpPropertiesDTO = getSmtpProperties(actionStr);
		} catch (Exception e) {
			return e.getMessage();
		}
		
		if(smtpPropertiesDTO==null){
			return "SMTP: " + actionStr + " action not found.";
		}
    	
    	String username = userSecurityDTO.getUserName();
    	String userEmail = userSecurityDTO.getEmail();
	    String userConfigPassword = userSecurityDTO.getPassword();
    	String smtpHost = smtpPropertiesDTO.getSmtpHostName();
		String from = smtpPropertiesDTO.getSmtpFrom();
		String subject = smtpPropertiesDTO.getSmtpSubject();
	    String content = smtpPropertiesDTO.getSmtpContent();
	    FacesContext context = FacesContext.getCurrentInstance();
	    HttpServletRequest req = (HttpServletRequest) context.getExternalContext().getRequest();
	    String uri = req.getScheme() + "://" + req.getServerName() + 
             	("http".equals(req.getScheme()) && req.getServerPort() == 80 || 
             	"https".equals(req.getScheme()) && req.getServerPort() == 443 ? "" : ":" + req.getServerPort() ) +
             	"/ncsts/";
	    int smtpPortNumber = smtpPropertiesDTO.getSmtpPort().intValue();
	    
	    if(actionStr.equalsIgnoreCase("PASSWORD_REMINDER")){
	    	StringBuilder contentBuilder = new StringBuilder();
	    	contentBuilder.append("<table border='0' align='left'>");
			contentBuilder.append("<tr><td align='left'>" + "Your current password at " + uri + " is:  " + "&#160;&#160;</td></tr>");
			contentBuilder.append("<tr><td align='left'>" + "&#160;&#160;</td></tr>");
			contentBuilder.append("<tr><td align='left'>" + StringEscapeUtils.escapeHtml(DesEncrypter.getInstance().decrypt(userConfigPassword)));
			contentBuilder.append("<tr><td align='left'>" + "&#160;&#160;</td></tr>");
			contentBuilder.append("<tr><td align='left'>" + "If you copy/paste the password from the email, please take care to not accidentally include any leading or trailing spaces." + "&#160;&#160;</td></tr>");	
			contentBuilder.append("<tr><td align='left'>" + "Please let us know if you have any issues logging in or need any assistance at pinpoint.support@ryan.com." + "&#160;&#160;</td></tr>");	

			contentBuilder.append("</table>");

	    	content = contentBuilder.toString();
	    }
	    else if(actionStr.equalsIgnoreCase("PASSWORD_RESET")){
	    	//PP-196
	    	//Need to call StringEscapeUtils.escapeHtml if the content is html table
	    	StringBuilder contentBuilder = new StringBuilder(); 
	    	contentBuilder.append("<table border='0' align='left'>");
			contentBuilder.append("<tr><td align='left'>" + "Your temporary password at " + uri + " is:  " + "&#160;&#160;</td></tr>");
			contentBuilder.append("<tr><td align='left'>" + "&#160;&#160;</td></tr>");
			contentBuilder.append("<tr><td align='left'>" + StringEscapeUtils.escapeHtml(DesEncrypter.getInstance().decrypt(userConfigPassword)));
			contentBuilder.append("<tr><td align='left'>" + "&#160;&#160;</td></tr>");
			contentBuilder.append("<tr><td align='left'>" + "You will be required to change this password the first time you login to PinPoint. If you copy/paste the password from the email, please take care to not accidentally include any leading or trailing spaces." + "&#160;&#160;</td></tr>");	
			contentBuilder.append("<tr><td align='left'>" + "Please let us know if you have any issues logging in or need any assistance at pinpoint.support@ryan.com." + "&#160;&#160;</td></tr>");	
			contentBuilder.append("</table>");
			
			content = contentBuilder.toString();
	    }
	    else if(actionStr.equalsIgnoreCase("ACCOUNT_CREATE")){
	    	//0002611
	    	//PP-196	    	

			System.out.println("req.getRequestURI().toString()  : " + req.getRequestURI().toString());
			System.out.println("req.getContextPath()  : " + req.getContextPath());
			System.out.println("request.getScheme()  : " + req.getScheme());//https
			System.out.println("request.getServerName()  : " + req.getServerName());//localhost
			System.out.println("request.getServerPort()  : " + req.getServerPort());//8443
			System.out.println("request.getQueryString()  : " +req.getQueryString());//null

	    	StringBuilder contentBuilder = new StringBuilder(); 
	    	contentBuilder.append("<table border='0' align='left'>");
			contentBuilder.append("<tr><td align='left'>" + "Your PinPoint User account has been created at " + uri + " as User Name: "  + username + "." + "&#160;&#160;</td></tr>");
			contentBuilder.append("<tr><td align='left'>" + "&#160;&#160;</td></tr>");
			contentBuilder.append("<tr><td align='left'>" + "Your temporary credentials will arrive in a separate email." + "&#160;&#160;</td></tr>");
			contentBuilder.append("<tr><td align='left'>" + "You will be required to change this password the first time you login to PinPoint. If you copy/paste the password from the email, please take care to not accidentally include any leading or trailing spaces." + "&#160;&#160;</td></tr>");	
			contentBuilder.append("<tr><td align='left'>" + "&#160;&#160;</td></tr>");
			contentBuilder.append("<tr><td align='left'>" + "Please let us know if you have any issues logging in or need any assistance at pinpoint.support@ryan.com." + "&#160;&#160;</td></tr>");	
			contentBuilder.append("</table>");

			System.out.println(contentBuilder.toString());
			content = contentBuilder.toString();
	    }
	
		try{
			sendEmail(smtpHost, smtpPortNumber, from, userEmail, subject, content);
		//	addErrorMessage("An  has been sent to your mail box!");
		//	logger.info("An Email Notification has been sent to your mail box!");
		}
		catch(Exception e){
			//addErrorMessage("Could not connect to SMTP host!");
			logger.error("########## Could not connect to SMTP host: ", e);
			
			return "Cannot connect to SMTP host, please try again!";
		}
		
		return "";
	}

	private SmtpPropertiesDTO getSmtpProperties(String actionStr) throws Exception {
		SmtpPropertiesDTO smtpPropertiesDTO = null;
		Connection conn = dataSource.CreateSQLiteConnection();
		if(conn==null){
			logger.error("############ JDBC Connection failed");
			throw new Exception("Connection failed");
		}
		
		Statement stat = null;
		try{
			stat = conn.createStatement();
	    	ResultSet rs = stat.executeQuery("select * from TB_SMTP_PROPERTIES where ACTION = '" + actionStr + "'");
	    	if (rs.next()) {
	    		smtpPropertiesDTO = new SmtpPropertiesDTO(); 
	    		smtpPropertiesDTO.setId(rs.getInt("ID"));
	    		smtpPropertiesDTO.setSmtpPort(rs.getInt("SMTP_PORT"));
	    		smtpPropertiesDTO.setAction(rs.getString("ACTION"));
	    		smtpPropertiesDTO.setSmtpSubject(rs.getString("SMTP_SUBJECT"));
	    		smtpPropertiesDTO.setSmtpFrom(rs.getString("SMTP_FROM"));
	    		smtpPropertiesDTO.setSmtpContent(rs.getString("SMTP_CONTENT"));
	    		smtpPropertiesDTO.setSmtpHostName(rs.getString("SMTP_HOST_NAME")); 
	    	}
	    	rs.close();	
		}
		catch (Exception e){
			logger.error("############ findAllRolesList() failed: " + e.toString()); 
			conn = dataSource.RenewSQLiteConnection(conn);	 
		}
		finally{
			if(stat!= null){try{stat.close();}catch(Exception ex){}}
			dataSource.CloseSQLiteConnection(conn);
		}
		return smtpPropertiesDTO;
	}
	
	public void sendEmail(String smtpHost, int smtpPort, String from, String to, String subject, String content)
			throws AddressException, MessagingException {
		// Create a mail session
		java.util.Properties props = new java.util.Properties();
		props.put("mail.smtp.host", smtpHost);
		props.put("mail.smtp.port", ""+smtpPort);
		Session session = Session.getInstance(props, null);
		
		// Construct the message
		Message msg = new MimeMessage(session);
		msg.setFrom(new InternetAddress(from));
		msg.setRecipient(Message.RecipientType.TO, new InternetAddress(to));
		msg.setSubject(subject);
		msg.setContent(content, "text/html");
		
		// Send the message
		Transport.send(msg);
	}
	
	///////////////////////////////////////////////////////////////////////////////////////////////////////////
	public List<DbProperties> findDbUserList(Integer userId) throws DataAccessException {
		List<DbProperties> result = new ArrayList<DbProperties>();
		Connection conn = dataSource.CreateSQLiteConnection();
		if(conn==null){
			logger.error("############ JDBC Connection failed");
			return result;
		}
		
		Statement stat = null;
		try{
			stat = conn.createStatement();
	    	ResultSet rs = stat.executeQuery("select TB_DB_PROPERTIES.DATABASE_NAME, TB_DB_PROPERTIES.DRIVER_CLASS_NAME, TB_DB_PROPERTIES.URL, TB_DB_PROPERTIES.USER_NAME, TB_DB_PROPERTIES.PASSWORD from TB_USER_SECURITY, TB_DB_PROPERTIES, TB_USER_DB where TB_USER_SECURITY.USER_ID=" + userId + " and TB_USER_SECURITY.USER_ID=TB_USER_DB.USER_ID and TB_USER_DB.DB_ID=TB_DB_PROPERTIES.DB_ID");
	    	while (rs.next()) {
	    		DbProperties dbProperties = new DbProperties();
	    		dbProperties.setDatabaseName(rs.getString("DATABASE_NAME"));
	    		dbProperties.setDriverClassName(rs.getString("DRIVER_CLASS_NAME"));
	    		dbProperties.setUrl(rs.getString("URL"));
	    		dbProperties.setUser(rs.getString("USER_NAME"));
	    		dbProperties.setPassword(rs.getString("PASSWORD")); 		    	
	    		result.add(dbProperties);   
	    	}
	    	rs.close();	
		}
		catch (Exception e){
			logger.error("############ findDbUserList() failed: " + e.toString());
			conn = dataSource.RenewSQLiteConnection(conn);	 
		}
		finally{
			if(stat!= null){try{stat.close();}catch(Exception ex){}}
			dataSource.CloseSQLiteConnection(conn);
		}
		
		return result;
	}


	public List<DbProperties> findAllDbList() throws DataAccessException {
		List<DbProperties> result = new ArrayList<DbProperties>();
		Connection conn = dataSource.CreateSQLiteConnection();
		if(conn==null){
			logger.error("############ JDBC Connection failed");
			return result;
		}
		
		Statement stat = null;
		try{
			stat = conn.createStatement();
	    	ResultSet rs = stat.executeQuery("select * from TB_DB_PROPERTIES order by DB_ID desc");
	    	while (rs.next()) {
	    		DbProperties dbProperties = new DbProperties();
	    		dbProperties.setDatabaseName(rs.getString("DATABASE_NAME"));
	    		dbProperties.setDriverClassName(rs.getString("DRIVER_CLASS_NAME"));
	    		dbProperties.setUrl(rs.getString("URL"));
	    		dbProperties.setUser(rs.getString("USER_NAME"));
	    		dbProperties.setPassword(rs.getString("PASSWORD")); 		    	
	    		result.add(dbProperties);   
	    	}
	    	rs.close();	
		}
		catch (Exception e){
			logger.error("############ findAllDbList() failed: " + e.toString()); 
			conn = dataSource.RenewSQLiteConnection(conn);	 
		}
		finally{
			if(stat!= null){try{stat.close();}catch(Exception ex){}}
			dataSource.CloseSQLiteConnection(conn);
		}
		
		return result;
	}
    
    public String displayMapUsersAction() {
    	currentUserSecurityAction = EditAction.MAP;
    	userSecurityDTOWork = new UserSecurityDTO(); 
		if(selectedUserSecurity!=null){
			userSecurityDTOWork.setUserId(selectedUserSecurity.getUserId());
			userSecurityDTOWork.setUserName(selectedUserSecurity.getUserName());
			userSecurityDTOWork.setEmail(selectedUserSecurity.getEmail());
			

			if(!mapDbProperties()){
				return null;
			}
		}
		
		return "users_map";
	}	
    
    public boolean mapDbProperties() {
		try {
			List<DbProperties> allList = findAllDbList();
	    	List<DbProperties> grantedList = findDbUserList(userSecurityDTOWork.getUserId());
				
			allList.removeAll(grantedList);
		    this.setDeniedList(allList);
		    this.setGrantedList(grantedList);
		    
		    return true;    
		} catch (Exception ex) {
			ex.printStackTrace();
			return false;
		}
	}
    
    public String mapAction() {
		logger.info("start mapAction"); 

		String errorMessage = "";
		List<DbProperties> addList = new ArrayList<DbProperties>();	
		List<DbProperties> removeList = new ArrayList<DbProperties>();		
		List<DbProperties> existRMList = findDbUserList(userSecurityDTOWork.getUserId());
			
		List<DbProperties> newGrantedList = this.getGrantedList();
		for(Iterator<DbProperties> itr = newGrantedList.iterator();itr.hasNext();){
			DbProperties dbProperties = itr.next();		
			if(!existRMList.contains(dbProperties)){
				addList.add(dbProperties);
			}
		}
		
		List<DbProperties> newDeniedList = this.getDeniedList();
		for(Iterator<DbProperties> itr = newDeniedList.iterator();itr.hasNext();){
			DbProperties dbProperties = itr.next();		
			if(existRMList.contains(dbProperties)){
				removeList.add(dbProperties);
			}
		}
		
		Connection conn = dataSource.CreateSQLiteConnection();
		if(conn==null){
			addMessage("Connection failed");
			logger.error("############ JDBC Connection failed");
			return null;
		}
		
		PreparedStatement prepDelete = null;
		PreparedStatement prepAdd = null;
		try{
			conn.setAutoCommit(false);
			
			//Remove mapping
			prepDelete = conn.prepareStatement("delete from TB_USER_DB where DB_ID = ? and USER_ID = ?");
			for(Iterator<DbProperties> itr = removeList.iterator();itr.hasNext();){
				DbProperties dbProperties = itr.next();	
				Statement stat = conn.createStatement();
				ResultSet rs = stat.executeQuery("select * from TB_DB_PROPERTIES where UPPER(DATABASE_NAME) = UPPER('" + dbProperties.getDatabaseName() + "')");
				int dbId = 0;
				if (rs.next()) {	
					dbId = ((Integer)rs.getInt("DB_ID")).intValue();
				}
				rs.close();		
				
				prepDelete.setInt(1, dbId);
				prepDelete.setInt(2, userSecurityDTOWork.getUserId().intValue());
				prepDelete.addBatch();
			}
			prepDelete.executeBatch();
			
			//Add mapping
			prepAdd = conn.prepareStatement("insert into TB_USER_DB (DB_ID, USER_ID) values (?, ?)");
			for(Iterator<DbProperties> itr = addList.iterator();itr.hasNext();){
				DbProperties dbProperties = itr.next();	
				Statement stat = conn.createStatement();
				ResultSet rs = stat.executeQuery("select * from TB_DB_PROPERTIES where UPPER(DATABASE_NAME) = UPPER('" + dbProperties.getDatabaseName() + "')");
				int dbId = 0;
				if (rs.next()) {	
					dbId = ((Integer)rs.getInt("DB_ID")).intValue();
				}
				rs.close();		

				prepAdd.setInt(1, dbId);
				prepAdd.setInt(2, userSecurityDTOWork.getUserId().intValue());
				prepAdd.addBatch();
			}
			prepAdd.executeBatch();
			
			conn.commit();
	    	conn.setAutoCommit(true);
		 }
		 catch (Exception e){
			 errorMessage = "Cannot update user/database property mapping, please try again!"; 
			 try{
				 conn.rollback();
			 }
			 catch(Exception ex){
			 }
			 logger.error("############ mapAction() failed: " + e.toString()); 
			 conn = dataSource.RenewSQLiteConnection(conn);	 
		 }
		 finally{
			 if(prepDelete!=null){try{prepDelete.close();}catch(Exception ex){}}
			 if(prepAdd!=null){try{prepAdd.close();}catch(Exception ex){}}
			 dataSource.CloseSQLiteConnection(conn);
		 }
		 
		 if(errorMessage!=null && errorMessage.length()>0){
			addMessage(errorMessage);
			return null;
		 }
	      
		 List<DbProperties> addpinpointList = findAllDbList();
		 List<DbProperties> removepinpointList = new ArrayList<DbProperties>();
		 
		 for(DbProperties dbProperties : addpinpointList)
		 { 
			 removepinpointList.add(dbProperties);
		 }
		 
		 List<DbProperties> deniedList = this.deniedList;
		 Iterator<DbProperties> itera =  removepinpointList.iterator();
		 while(itera.hasNext()){
			 DbProperties dbProperties  = (DbProperties)itera.next();
			 if(!deniedList.contains(dbProperties)) {
				 itera.remove();
			 }
		 }
		 
		 List<DbProperties> grantedList = this.grantedList;
		 Iterator<DbProperties> iter = addpinpointList.iterator();
		 while(iter.hasNext()){
			 DbProperties dbProperties  = (DbProperties)iter.next();
			 if(!grantedList.contains(dbProperties)) {
				 iter.remove();
			 }
		 }
		 
		 for(DbProperties dbProperties : removepinpointList) {
				 Statement deleteStatement = null;
				 Connection pinpointconn = null;
				 try{
					 Class.forName(dbProperties.getDriverClassName());
		    		 pinpointconn = DriverManager.getConnection(dbProperties.getUrl(), dbProperties.getUser(), dbProperties.getPassword()); 
		    		 //PP-263, set the user to inactive
		    		 //String delsql = "delete FROM TB_USER WHERE UPPER (USER_CODE) = "+ "UPPER(" + "'" +userSecurityDTOWork.getUserName() + "'" + ")";
		    		 String delsql = "UPDATE TB_USER SET ACTIVE_FLAG = '0' WHERE UPPER (USER_CODE) = "+ "UPPER(" + "'" +userSecurityDTOWork.getUserName() + "'" + ")";
		    		 deleteStatement = pinpointconn.createStatement();
		    		 deleteStatement.execute(delsql);
				 }catch(Exception e ) {
					 
				 } 
				 finally{
				      try{
				          if(deleteStatement!=null) {  deleteStatement.close(); }
				       }catch(SQLException se){
				    	   se.printStackTrace();
				      }
				      try{
				         if(pinpointconn!=null) {pinpointconn.close(); }
				      }catch(SQLException se){
				         se.printStackTrace();
				      }
					 }
		 }
		 
		 for(DbProperties dbProperties : addpinpointList) {
			
			 PreparedStatement insertStatement = null;
			 Connection pinpointconn = null;
			 Statement findUserStatement = null;
			 try{
				 Class.forName(dbProperties.getDriverClassName());
	    		 pinpointconn = DriverManager.getConnection(dbProperties.getUrl(), dbProperties.getUser(), dbProperties.getPassword());  
	    		 
	    		 String findUserSql = "SELECT * FROM TB_USER WHERE UPPER(USER_CODE)  = "+ "UPPER(" + "'" +userSecurityDTOWork.getUserName() + "'" + ")"; 
	    		 findUserStatement = pinpointconn.createStatement();
	    		 ResultSet rs = findUserStatement.executeQuery(findUserSql);
	    		 if(rs.next()){
	    			 continue;
	    		 }
	    		 String insertSql = "INSERT INTO TB_USER(USER_CODE, USER_NAME, GLOBAL_BU_FLAG, DEFAULT_MATRIX_LINE_FLAG, ADMIN_FLAG, "
	    				+ "ACTIVE_FLAG, UPDATE_USER_ID, UPDATE_TIMESTAMP) VALUES(?, ?, ?, ?, ?, ?, ?, ?)";
	    		 insertStatement = pinpointconn.prepareStatement(insertSql);
	    		 insertStatement.setString(1, userSecurityDTOWork.getUserName().toUpperCase());
	    		 insertStatement.setString(2, userSecurityDTOWork.getUserName());
	    		 insertStatement.setString(3, ZERO);
	    		 insertStatement.setString(4, ZERO);
	    		 insertStatement.setString(5, ZERO);
	    		 insertStatement.setString(6, "X");
	    		 HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
	    		 UserDTO user = (UserDTO) session.getAttribute("user");
	    		 insertStatement.setString(7, user.getUpdateUserId());
	    		 insertStatement.setDate(8, new java.sql.Date(new Date().getTime()));
	    		 insertStatement.executeUpdate();
	    		 cacheManager.flushAllUsersMap();// updating the cache after adding new user.
			 }catch(Exception e) {
		    		e.printStackTrace();
		     }
			 finally{
			      try{
			         if(insertStatement!=null)  {   insertStatement.close();  }
			         if(findUserStatement!=null)  {   findUserStatement.close();  }
			      }catch(SQLException se){
			    	  se.printStackTrace();
			      }
			      try{
			         if(pinpointconn!=null) {  pinpointconn.close(); }
			      }catch(SQLException se){
			         se.printStackTrace();
			      }
			 }
		 }
	      
	     return "usersecurity_main"; 
	}
    
    public void sendErrorReport(String to, String subject, String content) throws Exception {
    	SmtpPropertiesDTO smtp = getSmtpProperties("ACCOUNT_CREATE");
    	if(smtp == null) {
    		throw new Exception("SMTP properties not found");
    	}
    	
    	sendEmail(smtp.getSmtpHostName(), smtp.getSmtpPort(), smtp.getSmtpFrom(), to, subject, content);
    }
    
    public String cancelMapAction() {

		return "usersecurity_main";
	}	

}
