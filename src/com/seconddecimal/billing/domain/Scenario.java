//package com.seconddecimal.billing.domain;
//
//import java.util.Date;
//
//import javax.persistence.Column;
//import javax.persistence.Entity;
//import javax.persistence.GeneratedValue;
//import javax.persistence.GenerationType;
//import javax.persistence.Id;
//import javax.persistence.PrimaryKeyJoinColumn;
//import javax.persistence.SecondaryTable;
//import javax.persistence.SecondaryTables;
//import javax.persistence.SequenceGenerator;
//import javax.persistence.Table;
//
//import com.ncsts.domain.Auditable;
//import com.ncsts.domain.Idable;
//
//@Entity
//@Table(name="TB_BILLSCENARIO")
//@SecondaryTables({
//	@SecondaryTable(name="TB_BILLSCENARIO_JURDTL", pkJoinColumns = { @PrimaryKeyJoinColumn(name="BILLSCENARIO_JURDTL_ID",referencedColumnName="BILLSCENARIO_ID")}),
//	@SecondaryTable(name="TB_BILLSCENARIO_USER", pkJoinColumns = { @PrimaryKeyJoinColumn(name="BILLSCENARIO_USER_ID", referencedColumnName="BILLSCENARIO_ID")}),
//	@SecondaryTable(name="TB_BILLSCENARIO_LOCN", pkJoinColumns = { @PrimaryKeyJoinColumn(name="BILLSCENARIO_LOCN_ID", referencedColumnName="BILLSCENARIO_ID")})
//})
//public class Scenario extends Auditable implements java.io.Serializable, Idable<Long> {
//	private static final long serialVersionUID = 1L;
//
//	@Id
//    @Column(name="BILLSCENARIO_ID")
//	@GeneratedValue (strategy = GenerationType.SEQUENCE , generator="scenario_sequence")
//	@SequenceGenerator(name="scenario_sequence" , allocationSize = 1, sequenceName="SQ_TB_BILLSCENARIO_ID")
//	private Long scenarioId;
//	
//	@Column(name="SCENARIO_NAME")
//	private String scenarioName;
//
//	@Column(name="ENTERED_DATE")
//	private Date enteredDate;
//
//	@Column(name="TRANSACTION_TYPE")
//	private String transactionType;
//
//	@Column(name="TRANSACTION_TYPE_CODE")
//	private String transactionTypeCode;
//
//	@Column(name="ENTITY_ID")
//	private Long entityId;
//
//	@Column(name="ENTITY_CODE")
//	private String entityCode;
//
//	@Column(name="RATETYPE")
//	private String ratetype;
//
//	@Column(name="RATETYPE_CODE")
//	private String ratetypeCode;
//
//	@Column(name="METHOD_DELIVERY")
//	private String methodDelivery;
//
//	@Column(name="METHOD_DELIVERY_CODE")
//	private String methodDeliveryCode;
//
//	@Column(name="GEO_RECON_CODE")
//	private String geoReconCode;
//
//	@Column(name="DISCOUNT_TYPE")
//	private String discountType;
//
//	@Column(name="FREIGHT_ITEM_FLAG")
//	private String freightItemFlag;
//
//	@Column(name="DISCOUNT_ITEM_FLAG")
//	private String discountItemFlag;
//
//	@Column(name="COMPANY_NBR")
//	private String companyNbr;
//
//	@Column(name="DIVISION_NBR")
//	private String divisionNbr;
//
//	@Column(name="SHIPFROM_ENTITY_ID")
//	private Long shipfromEntityId;
//
//	@Column(name="SHIPFROM_ENTITY_CODE")
//	private String shipfromEntityCode;
//
//	@Column(name="SHIPFROM_JURISDICTION_ID")
//	private Long shipfromJurisdictionId;
//
//	@Column(name="SHIPFROM_GEOCODE")
//	private String shipfromGeocode;
//
//	@Column(name="SHIPFROM_ADDRESS_LINE_1")
//	private String shipfromAddressLine1;
//
//	@Column(name="SHIPFROM_ADDRESS_LINE_2")
//	private String shipfromAddressLine2;
//
//	@Column(name="SHIPFROM_CITY")
//	private String shipfromCity;
//
//	@Column(name="SHIPFROM_COUNTY")
//	private String shipfromCounty;
//
//	@Column(name="SHIPFROM_STATE_CODE")
//	private String shipfromStateCode;
//
//	@Column(name="SHIPFROM_ZIP")
//	private String shipfromZip;
//
//	@Column(name="SHIPFROM_ZIPPLUS4")
//	private String shipfromZipplus4;
//
//	@Column(name="SHIPFROM_COUNTRY_CODE")
//	private String shipfromCountryCode;
//
//	@Column(name="SHIPFROM_STJ1_NAME")
//	private String shipfromStj1Name;
//
//	@Column(name="SHIPFROM_STJ2_NAME")
//	private String shipfromStj2Name;
//
//	@Column(name="SHIPFROM_STJ3_NAME")
//	private String shipfromStj3Name;
//
//	@Column(name="SHIPFROM_STJ4_NAME")
//	private String shipfromStj4Name;
//
//	@Column(name="SHIPFROM_STJ5_NAME")
//	private String shipfromStj5Name;
//
//	@Column(name="CUST_NBR")
//	private String custNbr;
//
//	@Column(name="CUST_NAME")
//	private String custName;
//
//	@Column(name="SHIPTO_ENTITY_ID")
//	private Long shiptoEntityId;
//
//	@Column(name="SHIPTO_ENTITY_CODE")
//	private String shiptoEntityCode;
//
//	@Column(name="SHIPTO_JURISDICTION_ID")
//	private Long shiptoJurisdictionId;
//
//	@Column(name="SHIPTO_GEOCODE")
//	private String shiptoGeocode;
//
//	@Column(name="SHIPTO_ADDRESS_LINE_1")
//	private String shiptoAddressLine1;
//
//	@Column(name="SHIPTO_ADDRESS_LINE_2")
//	private String shiptoAddressLine2;
//
//	@Column(name="SHIPTO_CITY")
//	private String shiptoCity;
//
//	@Column(name="SHIPTO_COUNTY")
//	private String shiptoCounty;
//
//	@Column(name="SHIPTO_STATE_CODE")
//	private String shiptoStateCode;
//
//	@Column(name="SHIPTO_ZIP")
//	private String shiptoZip;
//
//	@Column(name="SHIPTO_ZIPPLUS4")
//	private String shiptoZipplus4;
//
//	@Column(name="SHIPTO_COUNTRY_CODE")
//	private String shiptoCountryCode;
//
//	@Column(name="SHIPTO_STJ1_NAME")
//	private String shiptoStj1Name;
//
//	@Column(name="SHIPTO_STJ2_NAME")
//	private String shiptoStj2Name;
//
//	@Column(name="SHIPTO_STJ3_NAME")
//	private String shiptoStj3Name;
//
//	@Column(name="SHIPTO_STJ4_NAME")
//	private String shiptoStj4Name;
//
//	@Column(name="SHIPTO_STJ5_NAME")
//	private String shiptoStj5Name;
//
//	@Column(name="ORDRACPT_ENTITY_ID")
//	private Long ordracptEntityId;
//
//	@Column(name="ORDRACPT_ENTITY_CODE")
//	private String ordracptEntityCode;
//
//	@Column(name="ORDRACPT_JURISDICTION_ID")
//	private Long ordracptJurisdictionId;
//
//	@Column(name="ORDRACPT_GEOCODE")
//	private String ordracptGeocode;
//
//	@Column(name="ORDRACPT_ADDRESS_LINE_1")
//	private String ordracptAddressLine1;
//
//	@Column(name="ORDRACPT_ADDRESS_LINE_2")
//	private String ordracptAddressLine2;
//
//	@Column(name="ORDRACPT_CITY")
//	private String ordracptCity;
//
//	@Column(name="ORDRACPT_COUNTY")
//	private String ordracptCounty;
//
//	@Column(name="ORDRACPT_STATE_CODE")
//	private String ordracptStateCode;
//
//	@Column(name="ORDRACPT_ZIP")
//	private String ordracptZip;
//
//	@Column(name="ORDRACPT_ZIPPLUS4")
//	private String ordracptZipplus4;
//
//	@Column(name="ORDRACPT_COUNTRY_CODE")
//	private String ordracptCountryCode;
//
//	@Column(name="ORDRACPT_STJ1_NAME")
//	private String ordracptStj1Name;
//
//	@Column(name="ORDRACPT_STJ2_NAME")
//	private String ordracptStj2Name;
//
//	@Column(name="ORDRACPT_STJ3_NAME")
//	private String ordracptStj3Name;
//
//	@Column(name="ORDRACPT_STJ4_NAME")
//	private String ordracptStj4Name;
//
//	@Column(name="ORDRACPT_STJ5_NAME")
//	private String ordracptStj5Name;
//
//	@Column(name="ORDRORGN_ENTITY_ID")
//	private Long ordrorgnEntityId;
//
//	@Column(name="ORDRORGN_ENTITY_CODE")
//	private String ordrorgnEntityCode;
//
//	@Column(name="ORDRORGN_JURISDICTION_ID")
//	private Long ordrorgnJurisdictionId;
//
//	@Column(name="ORDRORGN_GEOCODE")
//	private String ordrorgnGeocode;
//
//	@Column(name="ORDRORGN_ADDRESS_LINE_1")
//	private String ordrorgnAddressLine1;
//
//	@Column(name="ORDRORGN_ADDRESS_LINE_2")
//	private String ordrorgnAddressLine2;
//
//	@Column(name="ORDRORGN_CITY")
//	private String ordrorgnCity;
//
//	@Column(name="ORDRORGN_COUNTY")
//	private String ordrorgnCounty;
//
//	@Column(name="ORDRORGN_STATE_CODE")
//	private String ordrorgnStateCode;
//
//	@Column(name="ORDRORGN_ZIP")
//	private String ordrorgnZip;
//
//	@Column(name="ORDRORGN_ZIPPLUS4")
//	private String ordrorgnZipplus4;
//
//	@Column(name="ORDRORGN_COUNTRY_CODE")
//	private String ordrorgnCountryCode;
//
//	@Column(name="ORDRORGN_STJ1_NAME")
//	private String ordrorgnStj1Name;
//
//	@Column(name="ORDRORGN_STJ2_NAME")
//	private String ordrorgnStj2Name;
//
//	@Column(name="ORDRORGN_STJ3_NAME")
//	private String ordrorgnStj3Name;
//
//	@Column(name="ORDRORGN_STJ4_NAME")
//	private String ordrorgnStj4Name;
//
//	@Column(name="ORDRORGN_STJ5_NAME")
//	private String ordrorgnStj5Name;
//
//	@Column(name="FIRSTUSE_ENTITY_ID")
//	private Long firstuseEntityId;
//
//	@Column(name="FIRSTUSE_ENTITY_CODE")
//	private String firstuseEntityCode;
//
//	@Column(name="FIRSTUSE_JURISDICTION_ID")
//	private Long firstuseJurisdictionId;
//
//	@Column(name="FIRSTUSE_GEOCODE")
//	private String firstuseGeocode;
//
//	@Column(name="FIRSTUSE_ADDRESS_LINE_1")
//	private String firstuseAddressLine1;
//
//	@Column(name="FIRSTUSE_ADDRESS_LINE_2")
//	private String firstuseAddressLine2;
//
//	@Column(name="FIRSTUSE_CITY")
//	private String firstuseCity;
//
//	@Column(name="FIRSTUSE_COUNTY")
//	private String firstuseCounty;
//
//	@Column(name="FIRSTUSE_STATE_CODE")
//	private String firstuseStateCode;
//
//	@Column(name="FIRSTUSE_ZIP")
//	private String firstuseZip;
//
//	@Column(name="FIRSTUSE_ZIPPLUS4")
//	private String firstuseZipplus4;
//
//	@Column(name="FIRSTUSE_COUNTRY_CODE")
//	private String firstuseCountryCode;
//
//	@Column(name="FIRSTUSE_STJ1_NAME")
//	private String firstuseStj1Name;
//
//	@Column(name="FIRSTUSE_STJ2_NAME")
//	private String firstuseStj2Name;
//
//	@Column(name="FIRSTUSE_STJ3_NAME")
//	private String firstuseStj3Name;
//
//	@Column(name="FIRSTUSE_STJ4_NAME")
//	private String firstuseStj4Name;
//
//	@Column(name="FIRSTUSE_STJ5_NAME")
//	private String firstuseStj5Name;
//
//	@Column(name="BILLTO_ENTITY_ID")
//	private Long billtoEntityId;
//
//	@Column(name="BILLTO_ENTITY_CODE")
//	private String billtoEntityCode;
//	
//	//TB_BILLSCENARIO_LOCN
//
//	@Column(name="BILLTO_JURISDICTION_ID")
//	private Long billtoJurisdictionId;
//
//	@Column(name="BILLTO_GEOCODE")
//	private String billtoGeocode;
//
//	@Column(name="BILLTO_ADDRESS_LINE_1")
//	private String billtoAddressLine1;
//
//	@Column(name="BILLTO_ADDRESS_LINE_2")
//	private String billtoAddressLine2;
//
//	@Column(name="BILLTO_CITY")
//	private String billtoCity;
//
//	@Column(name="BILLTO_COUNTY")
//	private String billtoCounty;
//
//	@Column(name="BILLTO_STATE_CODE")
//	private String billtoStateCode;
//
//	@Column(name="BILLTO_ZIP")
//	private String billtoZip;
//
//	@Column(name="BILLTO_ZIPPLUS4")
//	private String billtoZipplus4;
//
//	@Column(name="BILLTO_COUNTRY_CODE")
//	private String billtoCountryCode;
//
//	@Column(name="BILLTO_STJ1_NAME")
//	private String billtoStj1Name;
//
//	@Column(name="BILLTO_STJ2_NAME")
//	private String billtoStj2Name;
//
//	@Column(name="BILLTO_STJ3_NAME")
//	private String billtoStj3Name;
//
//	@Column(name="BILLTO_STJ4_NAME")
//	private String billtoStj4Name;
//
//	@Column(name="BILLTO_STJ5_NAME")
//	private String billtoStj5Name;
//
//	@Column(name="GL_DATE")
//	private Date glDate;
//
//	@Column(name="COMMENTS")
//	private String comments;
//
//	@Column(name="PROCESS_METHOD")
//	private String processMethod;
//
//	@Column(name="ENTITY_LOCN_SET_DTL_ID")
//	private Long entityLocnSetDtlId;
//
//	@Column(name="COUNTRY_SITUS_MATRIX_ID")
//	private Long countrySitusMatrixId;
//
//	@Column(name="STATE_SITUS_MATRIX_ID")
//	private Long stateSitusMatrixId;
//
//	@Column(name="COUNTY_SITUS_MATRIX_ID")
//	private Long countySitusMatrixId;
//
//	@Column(name="CITY_SITUS_MATRIX_ID")
//	private Long citySitusMatrixId;
//
//	@Column(name="STJ1_SITUS_MATRIX_ID")
//	private Long stj1SitusMatrixId;
//
//	@Column(name="STJ2_SITUS_MATRIX_ID")
//	private Long stj2SitusMatrixId;
//
//	@Column(name="STJ3_SITUS_MATRIX_ID")
//	private Long stj3SitusMatrixId;
//
//	@Column(name="STJ4_SITUS_MATRIX_ID")
//	private Long stj4SitusMatrixId;
//
//	@Column(name="STJ5_SITUS_MATRIX_ID")
//	private Long stj5SitusMatrixId;
//
//	@Column(name="COUNTRY_NEXUS_DEF_DETAIL_ID")
//	private Long countryNexusDefDetailId;
//
//	@Column(name="STATE_NEXUS_DEF_DETAIL_ID")
//	private Long stateNexusDefDetailId;
//
//	@Column(name="COUNTY_NEXUS_DEF_DETAIL_ID")
//	private Long countyNexusDefDetailId;
//
//	@Column(name="CITY_NEXUS_DEF_DETAIL_ID")
//	private Long cityNexusDefDetailId;
//
//	@Column(name="STJ1_NEXUS_DEF_DETAIL_ID")
//	private Long stj1NexusDefDetailId;
//
//	@Column(name="STJ2_NEXUS_DEF_DETAIL_ID")
//	private Long stj2NexusDefDetailId;
//
//	@Column(name="STJ3_NEXUS_DEF_DETAIL_ID")
//	private Long stj3NexusDefDetailId;
//
//	@Column(name="STJ4_NEXUS_DEF_DETAIL_ID")
//	private Long stj4NexusDefDetailId;
//
//	@Column(name="STJ5_NEXUS_DEF_DETAIL_ID")
//	private Long stj5NexusDefDetailId;
//
//	@Column(name="STJ6_NEXUS_DEF_DETAIL_ID")
//	private Long stj6NexusDefDetailId;
//
//	@Column(name="STJ7_NEXUS_DEF_DETAIL_ID")
//	private Long stj7NexusDefDetailId;
//
//	@Column(name="STJ8_NEXUS_DEF_DETAIL_ID")
//	private Long stj8NexusDefDetailId;
//
//	@Column(name="STJ9_NEXUS_DEF_DETAIL_ID")
//	private Long stj9NexusDefDetailId;
//
//	@Column(name="STJ10_NEXUS_DEF_DETAIL_ID")
//	private Long stj10NexusDefDetailId;
//
//	@Column(name="COUNTRY_TAXCODE_DETAIL_ID")
//	private Long countryTaxcodeDetailId;
//
//	@Column(name="STATE_TAXCODE_DETAIL_ID")
//	private Long stateTaxcodeDetailId;
//
//	@Column(name="COUNTY_TAXCODE_DETAIL_ID")
//	private Long countyTaxcodeDetailId;
//
//	@Column(name="CITY_TAXCODE_DETAIL_ID")
//	private Long cityTaxcodeDetailId;
//
//	@Column(name="STJ1_TAXCODE_DETAIL_ID")
//	private Long stj1TaxcodeDetailId;
//
//	@Column(name="STJ2_TAXCODE_DETAIL_ID")
//	private Long stj2TaxcodeDetailId;
//
//	@Column(name="STJ3_TAXCODE_DETAIL_ID")
//	private Long stj3TaxcodeDetailId;
//
//	@Column(name="STJ4_TAXCODE_DETAIL_ID")
//	private Long stj4TaxcodeDetailId;
//
//	@Column(name="STJ5_TAXCODE_DETAIL_ID")
//	private Long stj5TaxcodeDetailId;
//
//	@Column(name="STJ6_TAXCODE_DETAIL_ID")
//	private Long stj6TaxcodeDetailId;
//
//	@Column(name="STJ7_TAXCODE_DETAIL_ID")
//	private Long stj7TaxcodeDetailId;
//
//	@Column(name="STJ8_TAXCODE_DETAIL_ID")
//	private Long stj8TaxcodeDetailId;
//
//	@Column(name="STJ9_TAXCODE_DETAIL_ID")
//	private Long stj9TaxcodeDetailId;
//
//	@Column(name="STJ10_TAXCODE_DETAIL_ID")
//	private Long stj10TaxcodeDetailId;
//
//	@Column(name="COUNTRY_TAXRATE_ID")
//	private Long countryTaxrateId;
//
//	@Column(name="STATE_TAXRATE_ID")
//	private Long stateTaxrateId;
//
//	@Column(name="COUNTY_TAXRATE_ID")
//	private Long countyTaxrateId;
//
//	@Column(name="CITY_TAXRATE_ID")
//	private Long cityTaxrateId;
//
//	@Column(name="STJ1_TAXRATE_ID")
//	private Long stj1TaxrateId;
//
//	@Column(name="STJ2_TAXRATE_ID")
//	private Long stj2TaxrateId;
//
//	@Column(name="STJ3_TAXRATE_ID")
//	private Long stj3TaxrateId;
//
//	@Column(name="STJ4_TAXRATE_ID")
//	private Long stj4TaxrateId;
//
//	@Column(name="STJ5_TAXRATE_ID")
//	private Long stj5TaxrateId;
//
//	@Column(name="STJ6_TAXRATE_ID")
//	private Long stj6TaxrateId;
//
//	@Column(name="STJ7_TAXRATE_ID")
//	private Long stj7TaxrateId;
//
//	@Column(name="STJ8_TAXRATE_ID")
//	private Long stj8TaxrateId;
//
//	@Column(name="STJ9_TAXRATE_ID")
//	private Long stj9TaxrateId;
//
//	@Column(name="STJ10_TAXRATE_ID")
//	private Long stj10TaxrateId;
//
//	@Column(name="CREDIT_IND")
//	private String creditInd;
//
//	@Column(name="EXEMPT_IND")
//	private String exemptInd;
//
//	@Column(name="CERT_NBR")
//	private String certNbr;
//
//	@Column(name="CERT_TYPE")
//	private String certType;
//
//	@Column(name="CERT_SIGN_DATE")
//	private Date certSignDate;
//
//	@Column(name="CERT_EXPIRATION_DATE")
//	private Date certExpirationDate;
//
//	@Column(name="CERT_IMAGE")
//	private String certImage;
//
//	@Column(name="EXEMPT_REASON")
//	private String exemptReason;
//
//	@Column(name="EXEMPT_CODE")
//	private String exemptCode;
//
//	@Column(name="EXEMPT_AMT")
//	private Double exemptAmt;
//
//	@Column(name="EXCLUSION_AMT")
//	private Double exclusionAmt;
//
//	@Column(name="INVOICE_DATE")
//	private Date invoiceDate;
//
//	@Column(name="INVOICE_NBR")
//	private String invoiceNbr;
//
//	@Column(name="INVOICE_DESC")
//	private String invoiceDesc;
//
//	@Column(name="INVOICE_GROSS_AMT")
//	private Double invoiceGrossAmt;
//
//	@Column(name="INVOICE_FREIGHT_AMT")
//	private Double invoiceFreightAmt;
//
//	@Column(name="INVOICE_DISC_AMT")
//	private Double invoiceDiscAmt;
//
//	@Column(name="INVOICE_DISC_TYPE_CODE")
//	private String invoiceDiscTypeCode;
//
//	@Column(name="INVOICE_LINE_NBR")
//	private String invoiceLineNbr;
//
//	@Column(name="INVOICE_LINE_DESC")
//	private String invoiceLineDesc;
//
//	@Column(name="INVOICE_LINE_ITEM_COUNT")
//	private Long invoiceLineItemCount;
//
//	@Column(name="INVOICE_LINE_ITEM_AMT")
//	private Double invoiceLineItemAmt;
//
//	@Column(name="INVOICE_LINE_GROSS_AMT")
//	private Double invoiceLineGrossAmt;
//
//	@Column(name="INVOICE_LINE_FREIGHT_AMT")
//	private Double invoiceLineFreightAmt;
//
//	@Column(name="INVOICE_LINE_DISC_AMT")
//	private Double invoiceLineDiscAmt;
//
//	@Column(name="INVOICE_LINE_DISC_TYPE_CODE")
//	private String invoiceLineDiscTypeCode;
//
//	@Column(name="INVOICE_LINE_PRODUCT_CODE")
//	private String invoiceLineProductCode;
//
//	@Column(name="TAXCODE_CODE")
//	private String taxcodeCode;
//
//	@Column(name="TAXABLE_AMT")
//	private Double taxableAmt;
//
//	@Column(name="COUNTRY_TAXABLE_AMT")
//	private Double countryTaxableAmt;
//
//	@Column(name="STATE_TAXABLE_AMT")
//	private Double stateTaxableAmt;
//
//	@Column(name="STATE_TIER1_TAXABLE_AMT")
//	private Double stateTier1TaxableAmt;
//
//	@Column(name="STATE_TIER2_TAXABLE_AMT")
//	private Double stateTier2TaxableAmt;
//
//	@Column(name="STATE_TIER3_TAXABLE_AMT")
//	private Double stateTier3TaxableAmt;
//
//	@Column(name="COUNTY_TAXABLE_AMT")
//	private Double countyTaxableAmt;
//
//	@Column(name="CITY_TAXABLE_AMT")
//	private Double cityTaxableAmt;
//
//	@Column(name="STJ1_TAXABLE_AMT")
//	private Double stj1TaxableAmt;
//
//	@Column(name="STJ2_TAXABLE_AMT")
//	private Double stj2TaxableAmt;
//
//	@Column(name="STJ3_TAXABLE_AMT")
//	private Double stj3TaxableAmt;
//
//	@Column(name="STJ4_TAXABLE_AMT")
//	private Double stj4TaxableAmt;
//
//	@Column(name="STJ5_TAXABLE_AMT")
//	private Double stj5TaxableAmt;
//
//	@Column(name="STJ6_TAXABLE_AMT")
//	private Double stj6TaxableAmt;
//
//	@Column(name="STJ7_TAXABLE_AMT")
//	private Double stj7TaxableAmt;
//
//	@Column(name="STJ8_TAXABLE_AMT")
//	private Double stj8TaxableAmt;
//
//	@Column(name="STJ9_TAXABLE_AMT")
//	private Double stj9TaxableAmt;
//
//	@Column(name="STJ10_TAXABLE_AMT")
//	private Double stj10TaxableAmt;
//
//	@Column(name="TAX_AMT")
//	private Double taxAmt;
//
//	@Column(name="COUNTRY_TAX_AMT")
//	private Double countryTaxAmt;
//
//	@Column(name="STATE_TAX_AMT")
//	private Double stateTaxAmt;
//
//	@Column(name="STATE_TIER1_TAX_AMT")
//	private Double stateTier1TaxAmt;
//
//	@Column(name="STATE_TIER2_TAX_AMT")
//	private Double stateTier2TaxAmt;
//
//	@Column(name="STATE_TIER3_TAX_AMT")
//	private Double stateTier3TaxAmt;
//
//	@Column(name="COUNTY_TAX_AMT")
//	private Double countyTaxAmt;
//
//	@Column(name="CITY_TAX_AMT")
//	private Double cityTaxAmt;
//
//	@Column(name="STJ1_TAX_AMT")
//	private Double stj1TaxAmt;
//
//	@Column(name="STJ2_TAX_AMT")
//	private Double stj2TaxAmt;
//
//	@Column(name="STJ3_TAX_AMT")
//	private Double stj3TaxAmt;
//
//	@Column(name="STJ4_TAX_AMT")
//	private Double stj4TaxAmt;
//
//	@Column(name="STJ5_TAX_AMT")
//	private Double stj5TaxAmt;
//
//	@Column(name="STJ6_TAX_AMT")
//	private Double stj6TaxAmt;
//
//	@Column(name="STJ7_TAX_AMT")
//	private Double stj7TaxAmt;
//
//	@Column(name="STJ8_TAX_AMT")
//	private Double stj8TaxAmt;
//
//	@Column(name="STJ9_TAX_AMT")
//	private Double stj9TaxAmt;
//
//	@Column(name="STJ10_TAX_AMT")
//	private Double stj10TaxAmt;
//
//	@Column(name="DO_NOT_CALC_FLAG")
//	private String doNotCalcFlag;
//
//	@Column(name="STATE_FREIGHT_DETAIL_ID")
//	private Long stateFreightDetailId;
//
//	@Column(name="COMPANY_DESC")
//	private String companyDesc;
//
//	@Column(name="CUST_ADDRESS_LINE_1")
//	private String custAddressLine1;
//
//	@Column(name="CUST_ADDRESS_LINE_2")
//	private String custAddressLine2;
//
//	@Column(name="CUST_CITY")
//	private String custCity;
//
//	@Column(name="CUST_COUNTRY_CODE")
//	private String custCountryCode;
//
//	@Column(name="CUST_COUNTY")
//	private String custCounty;
//
//	@Column(name="CUST_ENTITY_CODE")
//	private String custEntityCode;
//
//	@Column(name="CUST_ENTITY_ID")
//	private Long custEntityId;
//
//	@Column(name="CUST_GEOCODE")
//	private String custGeocode;
//
//	@Column(name="CUST_JURISDICTION_ID")
//	private Long custJurisdictionId;
//
//	@Column(name="CUST_LOCN_CODE")
//	private String custLocnCode;
//
//	@Column(name="CUST_STATE_CODE")
//	private String custStateCode;
//
//	@Column(name="CUST_ZIP")
//	private String custZip;
//
//	@Column(name="CUST_ZIPPLUS4")
//	private String custZipplus4;
//
//	@Column(name="DIVISION_DESC")
//	private String divisionDesc;
//
//	@Column(name="LOCATION_NAME")
//	private String locationName;
//
//	@Column(name="TAX_PAID_TO_VENDOR_FLAG")
//	private String taxPaidToVendorFlag;
//
//	@Column(name="USER_DATE_01")
//	private Date userDate01;
//
//	@Column(name="USER_DATE_02")
//	private Date userDate02;
//
//	@Column(name="USER_DATE_03")
//	private Date userDate03;
//
//	@Column(name="USER_DATE_04")
//	private Date userDate04;
//
//	@Column(name="USER_DATE_05")
//	private Date userDate05;
//
//	@Column(name="USER_DATE_06")
//	private Date userDate06;
//
//	@Column(name="USER_DATE_07")
//	private Date userDate07;
//
//	@Column(name="USER_DATE_08")
//	private Date userDate08;
//
//	@Column(name="USER_DATE_09")
//	private Date userDate09;
//
//	@Column(name="USER_DATE_10")
//	private Date userDate10;
/////0005726 USER_NBR_XX ---> USER_NUMBER_XX
//	@Column(name="USER_NUMBER_01")
//	private Double userNumber01;
//
//	@Column(name="USER_NUMBER_02")
//	private Double userNumber02;
//
//	@Column(name="USER_NUMBER_03")
//	private Double userNumber03;
//
//	@Column(name="USER_NUMBER_04")
//	private Double userNumber04;
//
//	@Column(name="USER_NUMBER_05")
//	private Double userNumber05;
//
//	@Column(name="USER_NUMBER_06")
//	private Double userNumber06;
//
//	@Column(name="USER_NUMBER_07")
//	private Double userNumber07;
//
//	@Column(name="USER_NUMBER_08")
//	private Double userNumber08;
//
//	@Column(name="USER_NUMBER_09")
//	private Double userNumber09;
//
//	@Column(name="USER_NUMBER_10")
//	private Double userNumber10;
//
//	@Column(name="USER_TEXT_01")
//	private String userText01;
//
//	@Column(name="USER_TEXT_02")
//	private String userText02;
//
//	@Column(name="USER_TEXT_03")
//	private String userText03;
//
//	@Column(name="USER_TEXT_04")
//	private String userText04;
//
//	@Column(name="USER_TEXT_05")
//	private String userText05;
//
//	@Column(name="USER_TEXT_06")
//	private String userText06;
//
//	@Column(name="USER_TEXT_07")
//	private String userText07;
//
//	@Column(name="USER_TEXT_08")
//	private String userText08;
//
//	@Column(name="USER_TEXT_09")
//	private String userText09;
//
//	@Column(name="USER_TEXT_10")
//	private String userText10;
//
//	@Column(name="USER_TEXT_11")
//	private String userText11;
//
//	@Column(name="USER_TEXT_12")
//	private String userText12;
//
//	@Column(name="USER_TEXT_13")
//	private String userText13;
//
//	@Column(name="USER_TEXT_14")
//	private String userText14;
//
//	@Column(name="USER_TEXT_15")
//	private String userText15;
//
//	@Column(name="USER_TEXT_16")
//	private String userText16;
//
//	@Column(name="USER_TEXT_17")
//	private String userText17;
//
//	@Column(name="USER_TEXT_18")
//	private String userText18;
//
//	@Column(name="USER_TEXT_19")
//	private String userText19;
//
//	@Column(name="USER_TEXT_20")
//	private String userText20;
//
//	@Column(name="USER_TEXT_21")
//	private String userText21;
//
//	@Column(name="USER_TEXT_22")
//	private String userText22;
//
//	@Column(name="USER_TEXT_23")
//	private String userText23;
//
//	@Column(name="USER_TEXT_24")
//	private String userText24;
//
//	@Column(name="USER_TEXT_25")
//	private String userText25;
//
//	@Column(name="USER_TEXT_26")
//	private String userText26;
//
//	@Column(name="USER_TEXT_27")
//	private String userText27;
//
//	@Column(name="USER_TEXT_28")
//	private String userText28;
//
//	@Column(name="USER_TEXT_29")
//	private String userText29;
//
//	@Column(name="USER_TEXT_30")
//	private String userText30;
//
//	@Column(name="VENDOR_TAX_AMT")
//	private Double vendorTaxAmt;
//	
//	@Column(name="COUNTRY_CODE")
//	private String countryCode;
//
//	@Column(name="STATE_CODE")
//	private String stateCode;
//
//	@Column(name="COUNTY_NAME")
//	private String countyName;
//
//	@Column(name="CITY_NAME")
//	private String cityName;
//
//	@Column(name="STJ1_NAME")
//	private String stj1Name;
//
//	@Column(name="STJ2_NAME")
//	private String stj2Name;
//
//	@Column(name="STJ3_NAME")
//	private String stj3Name;
//
//	@Column(name="STJ4_NAME")
//	private String stj4Name;
//
//	@Column(name="STJ5_NAME")
//	private String stj5Name;
//
//	@Column(name="STJ6_NAME")
//	private String stj6Name;
//
//	@Column(name="STJ7_NAME")
//	private String stj7Name;
//
//	@Column(name="STJ8_NAME")
//	private String stj8Name;
//
//	@Column(name="STJ9_NAME")
//	private String stj9Name;
//
//	@Column(name="STJ10_NAME")
//	private String stj10Name;
//
//	@Column(name="COUNTRY_EXEMPT_AMT")
//	private Double countryExemptAmt;
//
//	@Column(name="STATE_EXEMPT_AMT")
//	private Double stateExemptAmt;
//
//	@Column(name="COUNTY_EXEMPT_AMT")
//	private Double countyExemptAmt;
//
//	@Column(name="CITY_EXEMPT_AMT")
//	private Double cityExemptAmt;
//
//	@Column(name="STJ1_EXEMPT_AMT")
//	private Double stj1ExemptAmt;
//
//	@Column(name="STJ2_EXEMPT_AMT")
//	private Double stj2ExemptAmt;
//
//	@Column(name="STJ3_EXEMPT_AMT")
//	private Double stj3ExemptAmt;
//
//	@Column(name="STJ4_EXEMPT_AMT")
//	private Double stj4ExemptAmt;
//
//	@Column(name="STJ5_EXEMPT_AMT")
//	private Double stj5ExemptAmt;
//
//	@Column(name="STJ6_EXEMPT_AMT")
//	private Double stj6ExemptAmt;
//
//	@Column(name="STJ7_EXEMPT_AMT")
//	private Double stj7ExemptAmt;
//
//	@Column(name="STJ8_EXEMPT_AMT")
//	private Double stj8ExemptAmt;
//
//	@Column(name="STJ9_EXEMPT_AMT")
//	private Double stj9ExemptAmt;
//
//	@Column(name="STJ10_EXEMPT_AMT")
//	private Double stj10ExemptAmt;
//
//	@Column(name="COUNTRY_RATE")
//	private Double countryRate;
//
//	@Column(name="STATE_RATE")
//	private Double stateRate;
//
//	@Column(name="STATE_TIER2_RATE")
//	private Double stateTier2Rate;
//
//	@Column(name="STATE_TIER3_RATE")
//	private Double stateTier3Rate;
//
//	@Column(name="STATE_TIER1_MAX_AMOUNT")
//	private Double stateTier1MaxAmount;
//
//	@Column(name="STATE_TIER2_MIN_AMOUNT")
//	private Double stateTier2MinAmount;
//
//	@Column(name="STATE_TIER2_MAX_AMOUNT")
//	private Double stateTier2MaxAmount;
//
//	@Column(name="STATE_MAXTAX_AMOUNT")
//	private Double stateMaxtaxAmount;
//
//	@Column(name="COUNTY_RATE")
//	private Double countyRate;
//
//	@Column(name="COUNTY_SPLIT_AMOUNT")
//	private Double countySplitAmount;
//
//	@Column(name="COUNTY_MAXTAX_AMOUNT")
//	private Double countyMaxtaxAmount;
//
//	@Column(name="CITY_RATE")
//	private Double cityRate;
//
//	@Column(name="CITY_SPLIT_AMOUNT")
//	private Double citySplitAmount;
//
//	@Column(name="CITY_SPLIT_RATE")
//	private Double citySplitRate;
//
//	@Column(name="STJ1_RATE")
//	private Double stj1Rate;
//
//	@Column(name="STJ2_RATE")
//	private Double stj2Rate;
//
//	@Column(name="STJ3_RATE")
//	private Double stj3Rate;
//
//	@Column(name="STJ4_RATE")
//	private Double stj4Rate;
//
//	@Column(name="STJ5_RATE")
//	private Double stj5Rate;
//
//	@Column(name="STJ6_RATE")
//	private Double stj6Rate;
//
//	@Column(name="STJ7_RATE")
//	private Double stj7Rate;
//
//	@Column(name="STJ8_RATE")
//	private Double stj8Rate;
//
//	@Column(name="STJ9_RATE")
//	private Double stj9Rate;
//
//	@Column(name="STJ10_RATE")
//	private Double stj10Rate;
//
//	@Column(name="COMBINED_RATE")
//	private Double combinedRate;
//
//	@Column(name="COUNTRY_TAXTYPE_CODE")
//	private String countryTaxtypeCode;
//
//	@Column(name="STATE_TAXTYPE_CODE")
//	private String stateTaxtypeCode;
//
//	@Column(name="COUNTY_TAXTYPE_CODE")
//	private String countyTaxtypeCode;
//
//	@Column(name="CITY_TAXTYPE_CODE")
//	private String cityTaxtypeCode;
//
//	@Column(name="STJ1_TAXTYPE_CODE")
//	private String stj1TaxtypeCode;
//
//	@Column(name="STJ2_TAXTYPE_CODE")
//	private String stj2TaxtypeCode;
//
//	@Column(name="STJ3_TAXTYPE_CODE")
//	private String stj3TaxtypeCode;
//
//	@Column(name="STJ4_TAXTYPE_CODE")
//	private String stj4TaxtypeCode;
//
//	@Column(name="STJ5_TAXTYPE_CODE")
//	private String stj5TaxtypeCode;
//
//	@Column(name="STJ6_TAXTYPE_CODE")
//	private String stj6TaxtypeCode;
//
//	@Column(name="STJ7_TAXTYPE_CODE")
//	private String stj7TaxtypeCode;
//
//	@Column(name="STJ8_TAXTYPE_CODE")
//	private String stj8TaxtypeCode;
//
//	@Column(name="STJ9_TAXTYPE_CODE")
//	private String stj9TaxtypeCode;
//
//	@Column(name="STJ10_TAXTYPE_CODE")
//	private String stj10TaxtypeCode;
//	
//	@Column(name="INVOICE_TAX_PAID_AMT")
//	private Double invoiceTaxPaidAmt;
//	
//	@Column(name="INVOICE_DIFF_AMT")
//	private Double invoiceDiffAmt;
//	
//	@Column(name="INVOICE_LINE_TAX_PAID_AMT")
//	private Double invoiceLineTaxPaidAmt;
//	
//	@Column(name="INVOICE_LINE_DIFF_AMT")
//	private Double invoiceLineDiffAmt;
//	
//	@Column(name="INVOICE_DIFF_BATCH_NO")
//	private Long invoiceDiffBatchNo;
//	
//	@Column(name="INVOICE_TAX_AMT")
//	private Double invoiceTaxAmt;
//	
//	@Column(name="INVOICE_FREIGHTABLE_AMT")
//	private Double invoiceFreightableAmt;
//	
//	@Column(name="INVOICE_MISCABLE_AMT")
//	private Double invoiceMiscableAmt;
//	
//	@Column(name="INVOICE_PROFRT_FLAG")
//	private String invoiceProfrtFlag;
//	
//	@Column(name="INVOICE_PROMISC_FLAG")
//	private String invoicePromiscFlag;
//	
//	@Column(name="INVOICE_MISC_AMT")
//	private Double invoiceMiscAmt;
//	
//	@Column(name="INVOICE_LINE_MISC_AMT")
//	private Double invoiceLineMiscAmt;
//	
//	@Column(name="PROCRULE_SESSION_ID")
//	private String procruleSessionId;
//	
//	@Column(name="REPROCESS_CODE")
//	private String reprocessCode;
//	
//	
//	
//	public String getProcruleSessionId() {
//		return procruleSessionId;
//	}
//
//	public void setProcruleSessionId(String procruleSessionId) {
//		this.procruleSessionId = procruleSessionId;
//	}
//
//	public Date getEnteredDate() {
//		return enteredDate;
//	}
//
//	public void setEnteredDate(Date enteredDate) {
//		this.enteredDate = enteredDate;
//	}
//
//	public String getTransactionTypeCode() {
//		return transactionTypeCode;
//	}
//
//	public void setTransactionTypeCode(String transactionTypeCode) {
//		this.transactionTypeCode = transactionTypeCode;
//	}
//
//	public String getEntityCode() {
//		return entityCode;
//	}
//
//	public void setEntityCode(String entityCode) {
//		this.entityCode = entityCode;
//	}
//
//	public String getRatetypeCode() {
//		return ratetypeCode;
//	}
//
//	public void setRatetypeCode(String ratetypeCode) {
//		this.ratetypeCode = ratetypeCode;
//	}
//
//	public String getMethodDeliveryCode() {
//		return methodDeliveryCode;
//	}
//
//	public void setMethodDeliveryCode(String methodDeliveryCode) {
//		this.methodDeliveryCode = methodDeliveryCode;
//	}
//
//	public String getGeoReconCode() {
//		return geoReconCode;
//	}
//
//	public void setGeoReconCode(String geoReconCode) {
//		this.geoReconCode = geoReconCode;
//	}
//
//	public String getDiscountType() {
//		return discountType;
//	}
//
//	public void setDiscountType(String discountType) {
//		this.discountType = discountType;
//	}
//
//	public String getFreightItemFlag() {
//		return freightItemFlag;
//	}
//
//	public void setFreightItemFlag(String freightItemFlag) {
//		this.freightItemFlag = freightItemFlag;
//	}
//
//	public String getDiscountItemFlag() {
//		return discountItemFlag;
//	}
//
//	public void setDiscountItemFlag(String discountItemFlag) {
//		this.discountItemFlag = discountItemFlag;
//	}
//
//	public String getCompanyNbr() {
//		return companyNbr;
//	}
//
//	public void setCompanyNbr(String companyNbr) {
//		this.companyNbr = companyNbr;
//	}
//
//	public String getDivisionNbr() {
//		return divisionNbr;
//	}
//
//	public void setDivisionNbr(String divisionNbr) {
//		this.divisionNbr = divisionNbr;
//	}
//
//	public Long getShipfromEntityId() {
//		return shipfromEntityId;
//	}
//
//	public void setShipfromEntityId(Long shipfromEntityId) {
//		this.shipfromEntityId = shipfromEntityId;
//	}
//
//	public String getShipfromEntityCode() {
//		return shipfromEntityCode;
//	}
//
//	public void setShipfromEntityCode(String shipfromEntityCode) {
//		this.shipfromEntityCode = shipfromEntityCode;
//	}
//
//	public Long getShipfromJurisdictionId() {
//		return shipfromJurisdictionId;
//	}
//
//	public void setShipfromJurisdictionId(Long shipfromJurisdictionId) {
//		this.shipfromJurisdictionId = shipfromJurisdictionId;
//	}
//
//	public String getShipfromGeocode() {
//		return shipfromGeocode;
//	}
//
//	public void setShipfromGeocode(String shipfromGeocode) {
//		this.shipfromGeocode = shipfromGeocode;
//	}
//
//	public String getShipfromAddressLine1() {
//		return shipfromAddressLine1;
//	}
//
//	public void setShipfromAddressLine1(String shipfromAddressLine1) {
//		this.shipfromAddressLine1 = shipfromAddressLine1;
//	}
//
//	public String getShipfromAddressLine2() {
//		return shipfromAddressLine2;
//	}
//
//	public void setShipfromAddressLine2(String shipfromAddressLine2) {
//		this.shipfromAddressLine2 = shipfromAddressLine2;
//	}
//
//	public String getShipfromCity() {
//		return shipfromCity;
//	}
//
//	public void setShipfromCity(String shipfromCity) {
//		this.shipfromCity = shipfromCity;
//	}
//
//	public String getShipfromCounty() {
//		return shipfromCounty;
//	}
//
//	public void setShipfromCounty(String shipfromCounty) {
//		this.shipfromCounty = shipfromCounty;
//	}
//
//	public String getShipfromStateCode() {
//		return shipfromStateCode;
//	}
//
//	public void setShipfromStateCode(String shipfromStateCode) {
//		this.shipfromStateCode = shipfromStateCode;
//	}
//
//	public String getShipfromZip() {
//		return shipfromZip;
//	}
//
//	public void setShipfromZip(String shipfromZip) {
//		this.shipfromZip = shipfromZip;
//	}
//
//	public String getShipfromZipplus4() {
//		return shipfromZipplus4;
//	}
//
//	public void setShipfromZipplus4(String shipfromZipplus4) {
//		this.shipfromZipplus4 = shipfromZipplus4;
//	}
//
//	public String getShipfromCountryCode() {
//		return shipfromCountryCode;
//	}
//
//	public void setShipfromCountryCode(String shipfromCountryCode) {
//		this.shipfromCountryCode = shipfromCountryCode;
//	}
//
//	public String getShipfromStj1Name() {
//		return shipfromStj1Name;
//	}
//
//	public void setShipfromStj1Name(String shipfromStj1Name) {
//		this.shipfromStj1Name = shipfromStj1Name;
//	}
//
//	public String getShipfromStj2Name() {
//		return shipfromStj2Name;
//	}
//
//	public void setShipfromStj2Name(String shipfromStj2Name) {
//		this.shipfromStj2Name = shipfromStj2Name;
//	}
//
//	public String getShipfromStj3Name() {
//		return shipfromStj3Name;
//	}
//
//	public void setShipfromStj3Name(String shipfromStj3Name) {
//		this.shipfromStj3Name = shipfromStj3Name;
//	}
//
//	public String getShipfromStj4Name() {
//		return shipfromStj4Name;
//	}
//
//	public void setShipfromStj4Name(String shipfromStj4Name) {
//		this.shipfromStj4Name = shipfromStj4Name;
//	}
//
//	public String getShipfromStj5Name() {
//		return shipfromStj5Name;
//	}
//
//	public void setShipfromStj5Name(String shipfromStj5Name) {
//		this.shipfromStj5Name = shipfromStj5Name;
//	}
//
//	public String getCustNbr() {
//		return custNbr;
//	}
//
//	public void setCustNbr(String custNbr) {
//		this.custNbr = custNbr;
//	}
//
//	public String getCustName() {
//		return custName;
//	}
//
//	public void setCustName(String custName) {
//		this.custName = custName;
//	}
//
//	public Long getShiptoEntityId() {
//		return shiptoEntityId;
//	}
//
//	public void setShiptoEntityId(Long shiptoEntityId) {
//		this.shiptoEntityId = shiptoEntityId;
//	}
//
//	public String getShiptoEntityCode() {
//		return shiptoEntityCode;
//	}
//
//	public void setShiptoEntityCode(String shiptoEntityCode) {
//		this.shiptoEntityCode = shiptoEntityCode;
//	}
//
//	public Long getShiptoJurisdictionId() {
//		return shiptoJurisdictionId;
//	}
//
//	public void setShiptoJurisdictionId(Long shiptoJurisdictionId) {
//		this.shiptoJurisdictionId = shiptoJurisdictionId;
//	}
//
//	public String getShiptoGeocode() {
//		return shiptoGeocode;
//	}
//
//	public void setShiptoGeocode(String shiptoGeocode) {
//		this.shiptoGeocode = shiptoGeocode;
//	}
//
//	public String getShiptoAddressLine1() {
//		return shiptoAddressLine1;
//	}
//
//	public void setShiptoAddressLine1(String shiptoAddressLine1) {
//		this.shiptoAddressLine1 = shiptoAddressLine1;
//	}
//
//	public String getShiptoAddressLine2() {
//		return shiptoAddressLine2;
//	}
//
//	public void setShiptoAddressLine2(String shiptoAddressLine2) {
//		this.shiptoAddressLine2 = shiptoAddressLine2;
//	}
//
//	public String getShiptoCity() {
//		return shiptoCity;
//	}
//
//	public void setShiptoCity(String shiptoCity) {
//		this.shiptoCity = shiptoCity;
//	}
//
//	public String getShiptoCounty() {
//		return shiptoCounty;
//	}
//
//	public void setShiptoCounty(String shiptoCounty) {
//		this.shiptoCounty = shiptoCounty;
//	}
//
//	public String getShiptoStateCode() {
//		return shiptoStateCode;
//	}
//
//	public void setShiptoStateCode(String shiptoStateCode) {
//		this.shiptoStateCode = shiptoStateCode;
//	}
//
//	public String getShiptoZip() {
//		return shiptoZip;
//	}
//
//	public void setShiptoZip(String shiptoZip) {
//		this.shiptoZip = shiptoZip;
//	}
//
//	public String getShiptoZipplus4() {
//		return shiptoZipplus4;
//	}
//
//	public void setShiptoZipplus4(String shiptoZipplus4) {
//		this.shiptoZipplus4 = shiptoZipplus4;
//	}
//
//	public String getShiptoCountryCode() {
//		return shiptoCountryCode;
//	}
//
//	public void setShiptoCountryCode(String shiptoCountryCode) {
//		this.shiptoCountryCode = shiptoCountryCode;
//	}
//
//	public String getShiptoStj1Name() {
//		return shiptoStj1Name;
//	}
//
//	public void setShiptoStj1Name(String shiptoStj1Name) {
//		this.shiptoStj1Name = shiptoStj1Name;
//	}
//
//	public String getShiptoStj2Name() {
//		return shiptoStj2Name;
//	}
//
//	public void setShiptoStj2Name(String shiptoStj2Name) {
//		this.shiptoStj2Name = shiptoStj2Name;
//	}
//
//	public String getShiptoStj3Name() {
//		return shiptoStj3Name;
//	}
//
//	public void setShiptoStj3Name(String shiptoStj3Name) {
//		this.shiptoStj3Name = shiptoStj3Name;
//	}
//
//	public String getShiptoStj4Name() {
//		return shiptoStj4Name;
//	}
//
//	public void setShiptoStj4Name(String shiptoStj4Name) {
//		this.shiptoStj4Name = shiptoStj4Name;
//	}
//
//	public String getShiptoStj5Name() {
//		return shiptoStj5Name;
//	}
//
//	public void setShiptoStj5Name(String shiptoStj5Name) {
//		this.shiptoStj5Name = shiptoStj5Name;
//	}
//
//	public Long getOrdracptEntityId() {
//		return ordracptEntityId;
//	}
//
//	public void setOrdracptEntityId(Long ordracptEntityId) {
//		this.ordracptEntityId = ordracptEntityId;
//	}
//
//	public String getOrdracptEntityCode() {
//		return ordracptEntityCode;
//	}
//
//	public void setOrdracptEntityCode(String ordracptEntityCode) {
//		this.ordracptEntityCode = ordracptEntityCode;
//	}
//
//	public Long getOrdracptJurisdictionId() {
//		return ordracptJurisdictionId;
//	}
//
//	public void setOrdracptJurisdictionId(Long ordracptJurisdictionId) {
//		this.ordracptJurisdictionId = ordracptJurisdictionId;
//	}
//
//	public String getOrdracptGeocode() {
//		return ordracptGeocode;
//	}
//
//	public void setOrdracptGeocode(String ordracptGeocode) {
//		this.ordracptGeocode = ordracptGeocode;
//	}
//
//	public String getOrdracptAddressLine1() {
//		return ordracptAddressLine1;
//	}
//
//	public void setOrdracptAddressLine1(String ordracptAddressLine1) {
//		this.ordracptAddressLine1 = ordracptAddressLine1;
//	}
//
//	public String getOrdracptAddressLine2() {
//		return ordracptAddressLine2;
//	}
//
//	public void setOrdracptAddressLine2(String ordracptAddressLine2) {
//		this.ordracptAddressLine2 = ordracptAddressLine2;
//	}
//
//	public String getOrdracptCity() {
//		return ordracptCity;
//	}
//
//	public void setOrdracptCity(String ordracptCity) {
//		this.ordracptCity = ordracptCity;
//	}
//
//	public String getOrdracptCounty() {
//		return ordracptCounty;
//	}
//
//	public void setOrdracptCounty(String ordracptCounty) {
//		this.ordracptCounty = ordracptCounty;
//	}
//
//	public String getOrdracptStateCode() {
//		return ordracptStateCode;
//	}
//
//	public void setOrdracptStateCode(String ordracptStateCode) {
//		this.ordracptStateCode = ordracptStateCode;
//	}
//
//	public String getOrdracptZip() {
//		return ordracptZip;
//	}
//
//	public void setOrdracptZip(String ordracptZip) {
//		this.ordracptZip = ordracptZip;
//	}
//
//	public String getOrdracptZipplus4() {
//		return ordracptZipplus4;
//	}
//
//	public void setOrdracptZipplus4(String ordracptZipplus4) {
//		this.ordracptZipplus4 = ordracptZipplus4;
//	}
//
//	public String getOrdracptCountryCode() {
//		return ordracptCountryCode;
//	}
//
//	public void setOrdracptCountryCode(String ordracptCountryCode) {
//		this.ordracptCountryCode = ordracptCountryCode;
//	}
//
//	public String getOrdracptStj1Name() {
//		return ordracptStj1Name;
//	}
//
//	public void setOrdracptStj1Name(String ordracptStj1Name) {
//		this.ordracptStj1Name = ordracptStj1Name;
//	}
//
//	public String getOrdracptStj2Name() {
//		return ordracptStj2Name;
//	}
//
//	public void setOrdracptStj2Name(String ordracptStj2Name) {
//		this.ordracptStj2Name = ordracptStj2Name;
//	}
//
//	public String getOrdracptStj3Name() {
//		return ordracptStj3Name;
//	}
//
//	public void setOrdracptStj3Name(String ordracptStj3Name) {
//		this.ordracptStj3Name = ordracptStj3Name;
//	}
//
//	public String getOrdracptStj4Name() {
//		return ordracptStj4Name;
//	}
//
//	public void setOrdracptStj4Name(String ordracptStj4Name) {
//		this.ordracptStj4Name = ordracptStj4Name;
//	}
//
//	public String getOrdracptStj5Name() {
//		return ordracptStj5Name;
//	}
//
//	public void setOrdracptStj5Name(String ordracptStj5Name) {
//		this.ordracptStj5Name = ordracptStj5Name;
//	}
//
//	public Long getOrdrorgnEntityId() {
//		return ordrorgnEntityId;
//	}
//
//	public void setOrdrorgnEntityId(Long ordrorgnEntityId) {
//		this.ordrorgnEntityId = ordrorgnEntityId;
//	}
//
//	public String getOrdrorgnEntityCode() {
//		return ordrorgnEntityCode;
//	}
//
//	public void setOrdrorgnEntityCode(String ordrorgnEntityCode) {
//		this.ordrorgnEntityCode = ordrorgnEntityCode;
//	}
//
//	public Long getOrdrorgnJurisdictionId() {
//		return ordrorgnJurisdictionId;
//	}
//
//	public void setOrdrorgnJurisdictionId(Long ordrorgnJurisdictionId) {
//		this.ordrorgnJurisdictionId = ordrorgnJurisdictionId;
//	}
//
//	public String getOrdrorgnGeocode() {
//		return ordrorgnGeocode;
//	}
//
//	public void setOrdrorgnGeocode(String ordrorgnGeocode) {
//		this.ordrorgnGeocode = ordrorgnGeocode;
//	}
//
//	public String getOrdrorgnAddressLine1() {
//		return ordrorgnAddressLine1;
//	}
//
//	public void setOrdrorgnAddressLine1(String ordrorgnAddressLine1) {
//		this.ordrorgnAddressLine1 = ordrorgnAddressLine1;
//	}
//
//	public String getOrdrorgnAddressLine2() {
//		return ordrorgnAddressLine2;
//	}
//
//	public void setOrdrorgnAddressLine2(String ordrorgnAddressLine2) {
//		this.ordrorgnAddressLine2 = ordrorgnAddressLine2;
//	}
//
//	public String getOrdrorgnCity() {
//		return ordrorgnCity;
//	}
//
//	public void setOrdrorgnCity(String ordrorgnCity) {
//		this.ordrorgnCity = ordrorgnCity;
//	}
//
//	public String getOrdrorgnCounty() {
//		return ordrorgnCounty;
//	}
//
//	public void setOrdrorgnCounty(String ordrorgnCounty) {
//		this.ordrorgnCounty = ordrorgnCounty;
//	}
//
//	public String getOrdrorgnStateCode() {
//		return ordrorgnStateCode;
//	}
//
//	public void setOrdrorgnStateCode(String ordrorgnStateCode) {
//		this.ordrorgnStateCode = ordrorgnStateCode;
//	}
//
//	public String getOrdrorgnZip() {
//		return ordrorgnZip;
//	}
//
//	public void setOrdrorgnZip(String ordrorgnZip) {
//		this.ordrorgnZip = ordrorgnZip;
//	}
//
//	public String getOrdrorgnZipplus4() {
//		return ordrorgnZipplus4;
//	}
//
//	public void setOrdrorgnZipplus4(String ordrorgnZipplus4) {
//		this.ordrorgnZipplus4 = ordrorgnZipplus4;
//	}
//
//	public String getOrdrorgnCountryCode() {
//		return ordrorgnCountryCode;
//	}
//
//	public void setOrdrorgnCountryCode(String ordrorgnCountryCode) {
//		this.ordrorgnCountryCode = ordrorgnCountryCode;
//	}
//
//	public String getOrdrorgnStj1Name() {
//		return ordrorgnStj1Name;
//	}
//
//	public void setOrdrorgnStj1Name(String ordrorgnStj1Name) {
//		this.ordrorgnStj1Name = ordrorgnStj1Name;
//	}
//
//	public String getOrdrorgnStj2Name() {
//		return ordrorgnStj2Name;
//	}
//
//	public void setOrdrorgnStj2Name(String ordrorgnStj2Name) {
//		this.ordrorgnStj2Name = ordrorgnStj2Name;
//	}
//
//	public String getOrdrorgnStj3Name() {
//		return ordrorgnStj3Name;
//	}
//
//	public void setOrdrorgnStj3Name(String ordrorgnStj3Name) {
//		this.ordrorgnStj3Name = ordrorgnStj3Name;
//	}
//
//	public String getOrdrorgnStj4Name() {
//		return ordrorgnStj4Name;
//	}
//
//	public void setOrdrorgnStj4Name(String ordrorgnStj4Name) {
//		this.ordrorgnStj4Name = ordrorgnStj4Name;
//	}
//
//	public String getOrdrorgnStj5Name() {
//		return ordrorgnStj5Name;
//	}
//
//	public void setOrdrorgnStj5Name(String ordrorgnStj5Name) {
//		this.ordrorgnStj5Name = ordrorgnStj5Name;
//	}
//
//	public Long getFirstuseEntityId() {
//		return firstuseEntityId;
//	}
//
//	public void setFirstuseEntityId(Long firstuseEntityId) {
//		this.firstuseEntityId = firstuseEntityId;
//	}
//
//	public String getFirstuseEntityCode() {
//		return firstuseEntityCode;
//	}
//
//	public void setFirstuseEntityCode(String firstuseEntityCode) {
//		this.firstuseEntityCode = firstuseEntityCode;
//	}
//
//	public Long getFirstuseJurisdictionId() {
//		return firstuseJurisdictionId;
//	}
//
//	public void setFirstuseJurisdictionId(Long firstuseJurisdictionId) {
//		this.firstuseJurisdictionId = firstuseJurisdictionId;
//	}
//
//	public String getFirstuseGeocode() {
//		return firstuseGeocode;
//	}
//
//	public void setFirstuseGeocode(String firstuseGeocode) {
//		this.firstuseGeocode = firstuseGeocode;
//	}
//
//	public String getFirstuseAddressLine1() {
//		return firstuseAddressLine1;
//	}
//
//	public void setFirstuseAddressLine1(String firstuseAddressLine1) {
//		this.firstuseAddressLine1 = firstuseAddressLine1;
//	}
//
//	public String getFirstuseAddressLine2() {
//		return firstuseAddressLine2;
//	}
//
//	public void setFirstuseAddressLine2(String firstuseAddressLine2) {
//		this.firstuseAddressLine2 = firstuseAddressLine2;
//	}
//
//	public String getFirstuseCity() {
//		return firstuseCity;
//	}
//
//	public void setFirstuseCity(String firstuseCity) {
//		this.firstuseCity = firstuseCity;
//	}
//
//	public String getFirstuseCounty() {
//		return firstuseCounty;
//	}
//
//	public void setFirstuseCounty(String firstuseCounty) {
//		this.firstuseCounty = firstuseCounty;
//	}
//
//	public String getFirstuseStateCode() {
//		return firstuseStateCode;
//	}
//
//	public void setFirstuseStateCode(String firstuseStateCode) {
//		this.firstuseStateCode = firstuseStateCode;
//	}
//
//	public String getFirstuseZip() {
//		return firstuseZip;
//	}
//
//	public void setFirstuseZip(String firstuseZip) {
//		this.firstuseZip = firstuseZip;
//	}
//
//	public String getFirstuseZipplus4() {
//		return firstuseZipplus4;
//	}
//
//	public void setFirstuseZipplus4(String firstuseZipplus4) {
//		this.firstuseZipplus4 = firstuseZipplus4;
//	}
//
//	public String getFirstuseCountryCode() {
//		return firstuseCountryCode;
//	}
//
//	public void setFirstuseCountryCode(String firstuseCountryCode) {
//		this.firstuseCountryCode = firstuseCountryCode;
//	}
//
//	public String getFirstuseStj1Name() {
//		return firstuseStj1Name;
//	}
//
//	public void setFirstuseStj1Name(String firstuseStj1Name) {
//		this.firstuseStj1Name = firstuseStj1Name;
//	}
//
//	public String getFirstuseStj2Name() {
//		return firstuseStj2Name;
//	}
//
//	public void setFirstuseStj2Name(String firstuseStj2Name) {
//		this.firstuseStj2Name = firstuseStj2Name;
//	}
//
//	public String getFirstuseStj3Name() {
//		return firstuseStj3Name;
//	}
//
//	public void setFirstuseStj3Name(String firstuseStj3Name) {
//		this.firstuseStj3Name = firstuseStj3Name;
//	}
//
//	public String getFirstuseStj4Name() {
//		return firstuseStj4Name;
//	}
//
//	public void setFirstuseStj4Name(String firstuseStj4Name) {
//		this.firstuseStj4Name = firstuseStj4Name;
//	}
//
//	public String getFirstuseStj5Name() {
//		return firstuseStj5Name;
//	}
//
//	public void setFirstuseStj5Name(String firstuseStj5Name) {
//		this.firstuseStj5Name = firstuseStj5Name;
//	}
//
//	public Long getBilltoEntityId() {
//		return billtoEntityId;
//	}
//
//	public void setBilltoEntityId(Long billtoEntityId) {
//		this.billtoEntityId = billtoEntityId;
//	}
//
//	public String getBilltoEntityCode() {
//		return billtoEntityCode;
//	}
//
//	public void setBilltoEntityCode(String billtoEntityCode) {
//		this.billtoEntityCode = billtoEntityCode;
//	}
//
//	public Long getBilltoJurisdictionId() {
//		return billtoJurisdictionId;
//	}
//
//	public void setBilltoJurisdictionId(Long billtoJurisdictionId) {
//		this.billtoJurisdictionId = billtoJurisdictionId;
//	}
//
//	public String getBilltoGeocode() {
//		return billtoGeocode;
//	}
//
//	public void setBilltoGeocode(String billtoGeocode) {
//		this.billtoGeocode = billtoGeocode;
//	}
//
//	public String getBilltoAddressLine1() {
//		return billtoAddressLine1;
//	}
//
//	public void setBilltoAddressLine1(String billtoAddressLine1) {
//		this.billtoAddressLine1 = billtoAddressLine1;
//	}
//
//	public String getBilltoAddressLine2() {
//		return billtoAddressLine2;
//	}
//
//	public void setBilltoAddressLine2(String billtoAddressLine2) {
//		this.billtoAddressLine2 = billtoAddressLine2;
//	}
//
//	public String getBilltoCity() {
//		return billtoCity;
//	}
//
//	public void setBilltoCity(String billtoCity) {
//		this.billtoCity = billtoCity;
//	}
//
//	public String getBilltoCounty() {
//		return billtoCounty;
//	}
//
//	public void setBilltoCounty(String billtoCounty) {
//		this.billtoCounty = billtoCounty;
//	}
//
//	public String getBilltoStateCode() {
//		return billtoStateCode;
//	}
//
//	public void setBilltoStateCode(String billtoStateCode) {
//		this.billtoStateCode = billtoStateCode;
//	}
//
//	public String getBilltoZip() {
//		return billtoZip;
//	}
//
//	public void setBilltoZip(String billtoZip) {
//		this.billtoZip = billtoZip;
//	}
//
//	public String getBilltoZipplus4() {
//		return billtoZipplus4;
//	}
//
//	public void setBilltoZipplus4(String billtoZipplus4) {
//		this.billtoZipplus4 = billtoZipplus4;
//	}
//
//	public String getBilltoCountryCode() {
//		return billtoCountryCode;
//	}
//
//	public void setBilltoCountryCode(String billtoCountryCode) {
//		this.billtoCountryCode = billtoCountryCode;
//	}
//
//	public String getBilltoStj1Name() {
//		return billtoStj1Name;
//	}
//
//	public void setBilltoStj1Name(String billtoStj1Name) {
//		this.billtoStj1Name = billtoStj1Name;
//	}
//
//	public String getBilltoStj2Name() {
//		return billtoStj2Name;
//	}
//
//	public void setBilltoStj2Name(String billtoStj2Name) {
//		this.billtoStj2Name = billtoStj2Name;
//	}
//
//	public String getBilltoStj3Name() {
//		return billtoStj3Name;
//	}
//
//	public void setBilltoStj3Name(String billtoStj3Name) {
//		this.billtoStj3Name = billtoStj3Name;
//	}
//
//	public String getBilltoStj4Name() {
//		return billtoStj4Name;
//	}
//
//	public void setBilltoStj4Name(String billtoStj4Name) {
//		this.billtoStj4Name = billtoStj4Name;
//	}
//
//	public String getBilltoStj5Name() {
//		return billtoStj5Name;
//	}
//
//	public void setBilltoStj5Name(String billtoStj5Name) {
//		this.billtoStj5Name = billtoStj5Name;
//	}
//
//	public Date getGlDate() {
//		return glDate;
//	}
//
//	public void setGlDate(Date glDate) {
//		this.glDate = glDate;
//	}
//
//	public String getComments() {
//		return comments;
//	}
//
//	public void setComments(String comments) {
//		this.comments = comments;
//	}
//
//	public String getProcessMethod() {
//		return processMethod;
//	}
//
//	public void setProcessMethod(String processMethod) {
//		this.processMethod = processMethod;
//	}
//
//	public Long getEntityLocnSetDtlId() {
//		return entityLocnSetDtlId;
//	}
//
//	public void setEntityLocnSetDtlId(Long entityLocnSetDtlId) {
//		this.entityLocnSetDtlId = entityLocnSetDtlId;
//	}
//
//	public Long getCountrySitusMatrixId() {
//		return countrySitusMatrixId;
//	}
//
//	public void setCountrySitusMatrixId(Long countrySitusMatrixId) {
//		this.countrySitusMatrixId = countrySitusMatrixId;
//	}
//
//	public Long getStateSitusMatrixId() {
//		return stateSitusMatrixId;
//	}
//
//	public void setStateSitusMatrixId(Long stateSitusMatrixId) {
//		this.stateSitusMatrixId = stateSitusMatrixId;
//	}
//
//	public Long getCountySitusMatrixId() {
//		return countySitusMatrixId;
//	}
//
//	public void setCountySitusMatrixId(Long countySitusMatrixId) {
//		this.countySitusMatrixId = countySitusMatrixId;
//	}
//
//	public Long getCitySitusMatrixId() {
//		return citySitusMatrixId;
//	}
//
//	public void setCitySitusMatrixId(Long citySitusMatrixId) {
//		this.citySitusMatrixId = citySitusMatrixId;
//	}
//
//	public Long getStj1SitusMatrixId() {
//		return stj1SitusMatrixId;
//	}
//
//	public void setStj1SitusMatrixId(Long stj1SitusMatrixId) {
//		this.stj1SitusMatrixId = stj1SitusMatrixId;
//	}
//
//	public Long getStj2SitusMatrixId() {
//		return stj2SitusMatrixId;
//	}
//
//	public void setStj2SitusMatrixId(Long stj2SitusMatrixId) {
//		this.stj2SitusMatrixId = stj2SitusMatrixId;
//	}
//
//	public Long getStj3SitusMatrixId() {
//		return stj3SitusMatrixId;
//	}
//
//	public void setStj3SitusMatrixId(Long stj3SitusMatrixId) {
//		this.stj3SitusMatrixId = stj3SitusMatrixId;
//	}
//
//	public Long getStj4SitusMatrixId() {
//		return stj4SitusMatrixId;
//	}
//
//	public void setStj4SitusMatrixId(Long stj4SitusMatrixId) {
//		this.stj4SitusMatrixId = stj4SitusMatrixId;
//	}
//
//	public Long getStj5SitusMatrixId() {
//		return stj5SitusMatrixId;
//	}
//
//	public void setStj5SitusMatrixId(Long stj5SitusMatrixId) {
//		this.stj5SitusMatrixId = stj5SitusMatrixId;
//	}
//
//	public Long getCountryNexusDefDetailId() {
//		return countryNexusDefDetailId;
//	}
//
//	public void setCountryNexusDefDetailId(Long countryNexusDefDetailId) {
//		this.countryNexusDefDetailId = countryNexusDefDetailId;
//	}
//
//	public Long getStateNexusDefDetailId() {
//		return stateNexusDefDetailId;
//	}
//
//	public void setStateNexusDefDetailId(Long stateNexusDefDetailId) {
//		this.stateNexusDefDetailId = stateNexusDefDetailId;
//	}
//
//	public Long getCountyNexusDefDetailId() {
//		return countyNexusDefDetailId;
//	}
//
//	public void setCountyNexusDefDetailId(Long countyNexusDefDetailId) {
//		this.countyNexusDefDetailId = countyNexusDefDetailId;
//	}
//
//	public Long getCityNexusDefDetailId() {
//		return cityNexusDefDetailId;
//	}
//
//	public void setCityNexusDefDetailId(Long cityNexusDefDetailId) {
//		this.cityNexusDefDetailId = cityNexusDefDetailId;
//	}
//
//	public Long getStj1NexusDefDetailId() {
//		return stj1NexusDefDetailId;
//	}
//
//	public void setStj1NexusDefDetailId(Long stj1NexusDefDetailId) {
//		this.stj1NexusDefDetailId = stj1NexusDefDetailId;
//	}
//
//	public Long getStj2NexusDefDetailId() {
//		return stj2NexusDefDetailId;
//	}
//
//	public void setStj2NexusDefDetailId(Long stj2NexusDefDetailId) {
//		this.stj2NexusDefDetailId = stj2NexusDefDetailId;
//	}
//
//	public Long getStj3NexusDefDetailId() {
//		return stj3NexusDefDetailId;
//	}
//
//	public void setStj3NexusDefDetailId(Long stj3NexusDefDetailId) {
//		this.stj3NexusDefDetailId = stj3NexusDefDetailId;
//	}
//
//	public Long getStj4NexusDefDetailId() {
//		return stj4NexusDefDetailId;
//	}
//
//	public void setStj4NexusDefDetailId(Long stj4NexusDefDetailId) {
//		this.stj4NexusDefDetailId = stj4NexusDefDetailId;
//	}
//
//	public Long getStj5NexusDefDetailId() {
//		return stj5NexusDefDetailId;
//	}
//
//	public void setStj5NexusDefDetailId(Long stj5NexusDefDetailId) {
//		this.stj5NexusDefDetailId = stj5NexusDefDetailId;
//	}
//
//	public Long getCountryTaxcodeDetailId() {
//		return countryTaxcodeDetailId;
//	}
//
//	public void setCountryTaxcodeDetailId(Long countryTaxcodeDetailId) {
//		this.countryTaxcodeDetailId = countryTaxcodeDetailId;
//	}
//
//	public Long getStateTaxcodeDetailId() {
//		return stateTaxcodeDetailId;
//	}
//
//	public void setStateTaxcodeDetailId(Long stateTaxcodeDetailId) {
//		this.stateTaxcodeDetailId = stateTaxcodeDetailId;
//	}
//
//	public Long getCountyTaxcodeDetailId() {
//		return countyTaxcodeDetailId;
//	}
//
//	public void setCountyTaxcodeDetailId(Long countyTaxcodeDetailId) {
//		this.countyTaxcodeDetailId = countyTaxcodeDetailId;
//	}
//
//	public Long getCityTaxcodeDetailId() {
//		return cityTaxcodeDetailId;
//	}
//
//	public void setCityTaxcodeDetailId(Long cityTaxcodeDetailId) {
//		this.cityTaxcodeDetailId = cityTaxcodeDetailId;
//	}
//
//	public Long getStj1TaxcodeDetailId() {
//		return stj1TaxcodeDetailId;
//	}
//
//	public void setStj1TaxcodeDetailId(Long stj1TaxcodeDetailId) {
//		this.stj1TaxcodeDetailId = stj1TaxcodeDetailId;
//	}
//
//	public Long getStj2TaxcodeDetailId() {
//		return stj2TaxcodeDetailId;
//	}
//
//	public void setStj2TaxcodeDetailId(Long stj2TaxcodeDetailId) {
//		this.stj2TaxcodeDetailId = stj2TaxcodeDetailId;
//	}
//
//	public Long getStj3TaxcodeDetailId() {
//		return stj3TaxcodeDetailId;
//	}
//
//	public void setStj3TaxcodeDetailId(Long stj3TaxcodeDetailId) {
//		this.stj3TaxcodeDetailId = stj3TaxcodeDetailId;
//	}
//
//	public Long getStj4TaxcodeDetailId() {
//		return stj4TaxcodeDetailId;
//	}
//
//	public void setStj4TaxcodeDetailId(Long stj4TaxcodeDetailId) {
//		this.stj4TaxcodeDetailId = stj4TaxcodeDetailId;
//	}
//
//	public Long getStj5TaxcodeDetailId() {
//		return stj5TaxcodeDetailId;
//	}
//
//	public void setStj5TaxcodeDetailId(Long stj5TaxcodeDetailId) {
//		this.stj5TaxcodeDetailId = stj5TaxcodeDetailId;
//	}
//
//	public Long getCountryTaxrateId() {
//		return countryTaxrateId;
//	}
//
//	public void setCountryTaxrateId(Long countryTaxrateId) {
//		this.countryTaxrateId = countryTaxrateId;
//	}
//
//	public Long getStateTaxrateId() {
//		return stateTaxrateId;
//	}
//
//	public void setStateTaxrateId(Long stateTaxrateId) {
//		this.stateTaxrateId = stateTaxrateId;
//	}
//
//	public Long getCountyTaxrateId() {
//		return countyTaxrateId;
//	}
//
//	public void setCountyTaxrateId(Long countyTaxrateId) {
//		this.countyTaxrateId = countyTaxrateId;
//	}
//
//	public Long getCityTaxrateId() {
//		return cityTaxrateId;
//	}
//
//	public void setCityTaxrateId(Long cityTaxrateId) {
//		this.cityTaxrateId = cityTaxrateId;
//	}
//
//	public Long getStj1TaxrateId() {
//		return stj1TaxrateId;
//	}
//
//	public void setStj1TaxrateId(Long stj1TaxrateId) {
//		this.stj1TaxrateId = stj1TaxrateId;
//	}
//
//	public Long getStj2TaxrateId() {
//		return stj2TaxrateId;
//	}
//
//	public void setStj2TaxrateId(Long stj2TaxrateId) {
//		this.stj2TaxrateId = stj2TaxrateId;
//	}
//
//	public Long getStj3TaxrateId() {
//		return stj3TaxrateId;
//	}
//
//	public void setStj3TaxrateId(Long stj3TaxrateId) {
//		this.stj3TaxrateId = stj3TaxrateId;
//	}
//
//	public Long getStj4TaxrateId() {
//		return stj4TaxrateId;
//	}
//
//	public void setStj4TaxrateId(Long stj4TaxrateId) {
//		this.stj4TaxrateId = stj4TaxrateId;
//	}
//
//	public Long getStj5TaxrateId() {
//		return stj5TaxrateId;
//	}
//
//	public void setStj5TaxrateId(Long stj5TaxrateId) {
//		this.stj5TaxrateId = stj5TaxrateId;
//	}
//
//	public String getCreditInd() {
//		return creditInd;
//	}
//
//	public void setCreditInd(String creditInd) {
//		this.creditInd = creditInd;
//	}
//
//	public String getExemptInd() {
//		return exemptInd;
//	}
//
//	public void setExemptInd(String exemptInd) {
//		this.exemptInd = exemptInd;
//	}
//
//	public String getCertNbr() {
//		return certNbr;
//	}
//
//	public void setCertNbr(String certNbr) {
//		this.certNbr = certNbr;
//	}
//
//	public String getCertType() {
//		return certType;
//	}
//
//	public void setCertType(String certType) {
//		this.certType = certType;
//	}
//
//	public Date getCertSignDate() {
//		return certSignDate;
//	}
//
//	public void setCertSignDate(Date certSignDate) {
//		this.certSignDate = certSignDate;
//	}
//
//	public Date getCertExpirationDate() {
//		return certExpirationDate;
//	}
//
//	public void setCertExpirationDate(Date certExpirationDate) {
//		this.certExpirationDate = certExpirationDate;
//	}
//
//	public String getCertImage() {
//		return certImage;
//	}
//
//	public void setCertImage(String certImage) {
//		this.certImage = certImage;
//	}
//
//	public String getExemptReason() {
//		return exemptReason;
//	}
//
//	public void setExemptReason(String exemptReason) {
//		this.exemptReason = exemptReason;
//	}
//
//	public String getExemptCode() {
//		return exemptCode;
//	}
//
//	public void setExemptCode(String exemptCode) {
//		this.exemptCode = exemptCode;
//	}
//
//	public Double getExemptAmt() {
//		return exemptAmt;
//	}
//
//	public void setExemptAmt(Double exemptAmt) {
//		this.exemptAmt = exemptAmt;
//	}
//
//	public Double getExclusionAmt() {
//		return exclusionAmt;
//	}
//
//	public void setExclusionAmt(Double exclusionAmt) {
//		this.exclusionAmt = exclusionAmt;
//	}
//
//	public Date getInvoiceDate() {
//		return invoiceDate;
//	}
//
//	public void setInvoiceDate(Date invoiceDate) {
//		this.invoiceDate = invoiceDate;
//	}
//
//	public String getInvoiceNbr() {
//		return invoiceNbr;
//	}
//
//	public void setInvoiceNbr(String invoiceNbr) {
//		this.invoiceNbr = invoiceNbr;
//	}
//
//	public String getInvoiceDesc() {
//		return invoiceDesc;
//	}
//
//	public void setInvoiceDesc(String invoiceDesc) {
//		this.invoiceDesc = invoiceDesc;
//	}
//
//	public Double getInvoiceGrossAmt() {
//		return invoiceGrossAmt;
//	}
//
//	public void setInvoiceGrossAmt(Double invoiceGrossAmt) {
//		this.invoiceGrossAmt = invoiceGrossAmt;
//	}
//
//	public Double getInvoiceFreightAmt() {
//		return invoiceFreightAmt;
//	}
//
//	public void setInvoiceFreightAmt(Double invoiceFreightAmt) {
//		this.invoiceFreightAmt = invoiceFreightAmt;
//	}
//
//	public Double getInvoiceDiscAmt() {
//		return invoiceDiscAmt;
//	}
//
//	public void setInvoiceDiscAmt(Double invoiceDiscAmt) {
//		this.invoiceDiscAmt = invoiceDiscAmt;
//	}
//
//	public String getInvoiceDiscTypeCode() {
//		return invoiceDiscTypeCode;
//	}
//
//	public void setInvoiceDiscTypeCode(String invoiceDiscTypeCode) {
//		this.invoiceDiscTypeCode = invoiceDiscTypeCode;
//	}
//
//	public String getInvoiceLineNbr() {
//		return invoiceLineNbr;
//	}
//
//	public void setInvoiceLineNbr(String invoiceLineNbr) {
//		this.invoiceLineNbr = invoiceLineNbr;
//	}
//
//	public String getInvoiceLineDesc() {
//		return invoiceLineDesc;
//	}
//
//	public void setInvoiceLineDesc(String invoiceLineDesc) {
//		this.invoiceLineDesc = invoiceLineDesc;
//	}
//
//	public Long getInvoiceLineItemCount() {
//		return invoiceLineItemCount;
//	}
//
//	public void setInvoiceLineItemCount(Long invoiceLineItemCount) {
//		this.invoiceLineItemCount = invoiceLineItemCount;
//	}
//
//	public Double getInvoiceLineItemAmt() {
//		return invoiceLineItemAmt;
//	}
//
//	public void setInvoiceLineItemAmt(Double invoiceLineItemAmt) {
//		this.invoiceLineItemAmt = invoiceLineItemAmt;
//	}
//
//	public Double getInvoiceLineGrossAmt() {
//		return invoiceLineGrossAmt;
//	}
//
//	public void setInvoiceLineGrossAmt(Double invoiceLineGrossAmt) {
//		this.invoiceLineGrossAmt = invoiceLineGrossAmt;
//	}
//
//	public Double getInvoiceLineFreightAmt() {
//		return invoiceLineFreightAmt;
//	}
//
//	public void setInvoiceLineFreightAmt(Double invoiceLineFreightAmt) {
//		this.invoiceLineFreightAmt = invoiceLineFreightAmt;
//	}
//
//	public Double getInvoiceLineDiscAmt() {
//		return invoiceLineDiscAmt;
//	}
//
//	public void setInvoiceLineDiscAmt(Double invoiceLineDiscAmt) {
//		this.invoiceLineDiscAmt = invoiceLineDiscAmt;
//	}
//
//	public String getInvoiceLineDiscTypeCode() {
//		return invoiceLineDiscTypeCode;
//	}
//
//	public void setInvoiceLineDiscTypeCode(String invoiceLineDiscTypeCode) {
//		this.invoiceLineDiscTypeCode = invoiceLineDiscTypeCode;
//	}
//
//	public String getInvoiceLineProductCode() {
//		return invoiceLineProductCode;
//	}
//
//	public void setInvoiceLineProductCode(String invoiceLineProductCode) {
//		this.invoiceLineProductCode = invoiceLineProductCode;
//	}
//
//	public String getTaxcodeCode() {
//		return taxcodeCode;
//	}
//
//	public void setTaxcodeCode(String taxcodeCode) {
//		this.taxcodeCode = taxcodeCode;
//	}
//
//	public Double getTaxableAmt() {
//		return taxableAmt;
//	}
//
//	public void setTaxableAmt(Double taxableAmt) {
//		this.taxableAmt = taxableAmt;
//	}
//
//	public Double getCountryTaxableAmt() {
//		return countryTaxableAmt;
//	}
//
//	public void setCountryTaxableAmt(Double countryTaxableAmt) {
//		this.countryTaxableAmt = countryTaxableAmt;
//	}
//
//	public Double getStateTaxableAmt() {
//		return stateTaxableAmt;
//	}
//
//	public void setStateTaxableAmt(Double stateTaxableAmt) {
//		this.stateTaxableAmt = stateTaxableAmt;
//	}
//
//	public Double getStateTier1TaxableAmt() {
//		return stateTier1TaxableAmt;
//	}
//
//	public void setStateTier1TaxableAmt(Double stateTier1TaxableAmt) {
//		this.stateTier1TaxableAmt = stateTier1TaxableAmt;
//	}
//
//	public Double getStateTier2TaxableAmt() {
//		return stateTier2TaxableAmt;
//	}
//
//	public void setStateTier2TaxableAmt(Double stateTier2TaxableAmt) {
//		this.stateTier2TaxableAmt = stateTier2TaxableAmt;
//	}
//
//	public Double getStateTier3TaxableAmt() {
//		return stateTier3TaxableAmt;
//	}
//
//	public void setStateTier3TaxableAmt(Double stateTier3TaxableAmt) {
//		this.stateTier3TaxableAmt = stateTier3TaxableAmt;
//	}
//
//	public Double getCountyTaxableAmt() {
//		return countyTaxableAmt;
//	}
//
//	public void setCountyTaxableAmt(Double countyTaxableAmt) {
//		this.countyTaxableAmt = countyTaxableAmt;
//	}
//
//	public Double getCityTaxableAmt() {
//		return cityTaxableAmt;
//	}
//
//	public void setCityTaxableAmt(Double cityTaxableAmt) {
//		this.cityTaxableAmt = cityTaxableAmt;
//	}
//
//	public Double getStj1TaxableAmt() {
//		return stj1TaxableAmt;
//	}
//
//	public void setStj1TaxableAmt(Double stj1TaxableAmt) {
//		this.stj1TaxableAmt = stj1TaxableAmt;
//	}
//
//	public Double getStj2TaxableAmt() {
//		return stj2TaxableAmt;
//	}
//
//	public void setStj2TaxableAmt(Double stj2TaxableAmt) {
//		this.stj2TaxableAmt = stj2TaxableAmt;
//	}
//
//	public Double getStj3TaxableAmt() {
//		return stj3TaxableAmt;
//	}
//
//	public void setStj3TaxableAmt(Double stj3TaxableAmt) {
//		this.stj3TaxableAmt = stj3TaxableAmt;
//	}
//
//	public Double getStj4TaxableAmt() {
//		return stj4TaxableAmt;
//	}
//
//	public void setStj4TaxableAmt(Double stj4TaxableAmt) {
//		this.stj4TaxableAmt = stj4TaxableAmt;
//	}
//
//	public Double getStj5TaxableAmt() {
//		return stj5TaxableAmt;
//	}
//
//	public void setStj5TaxableAmt(Double stj5TaxableAmt) {
//		this.stj5TaxableAmt = stj5TaxableAmt;
//	}
//
//	public Double getTaxAmt() {
//		return taxAmt;
//	}
//
//	public void setTaxAmt(Double taxAmt) {
//		this.taxAmt = taxAmt;
//	}
//
//	public Double getCountryTaxAmt() {
//		return countryTaxAmt;
//	}
//
//	public void setCountryTaxAmt(Double countryTaxAmt) {
//		this.countryTaxAmt = countryTaxAmt;
//	}
//
//	public Double getStateTaxAmt() {
//		return stateTaxAmt;
//	}
//
//	public void setStateTaxAmt(Double stateTaxAmt) {
//		this.stateTaxAmt = stateTaxAmt;
//	}
//
//	public Double getStateTier1TaxAmt() {
//		return stateTier1TaxAmt;
//	}
//
//	public void setStateTier1TaxAmt(Double stateTier1TaxAmt) {
//		this.stateTier1TaxAmt = stateTier1TaxAmt;
//	}
//
//	public Double getStateTier2TaxAmt() {
//		return stateTier2TaxAmt;
//	}
//
//	public void setStateTier2TaxAmt(Double stateTier2TaxAmt) {
//		this.stateTier2TaxAmt = stateTier2TaxAmt;
//	}
//
//	public Double getStateTier3TaxAmt() {
//		return stateTier3TaxAmt;
//	}
//
//	public void setStateTier3TaxAmt(Double stateTier3TaxAmt) {
//		this.stateTier3TaxAmt = stateTier3TaxAmt;
//	}
//
//	public Double getCountyTaxAmt() {
//		return countyTaxAmt;
//	}
//
//	public void setCountyTaxAmt(Double countyTaxAmt) {
//		this.countyTaxAmt = countyTaxAmt;
//	}
//
//	public Double getCityTaxAmt() {
//		return cityTaxAmt;
//	}
//
//	public void setCityTaxAmt(Double cityTaxAmt) {
//		this.cityTaxAmt = cityTaxAmt;
//	}
//
//	public Double getStj1TaxAmt() {
//		return stj1TaxAmt;
//	}
//
//	public void setStj1TaxAmt(Double stj1TaxAmt) {
//		this.stj1TaxAmt = stj1TaxAmt;
//	}
//
//	public Double getStj2TaxAmt() {
//		return stj2TaxAmt;
//	}
//
//	public void setStj2TaxAmt(Double stj2TaxAmt) {
//		this.stj2TaxAmt = stj2TaxAmt;
//	}
//
//	public Double getStj3TaxAmt() {
//		return stj3TaxAmt;
//	}
//
//	public void setStj3TaxAmt(Double stj3TaxAmt) {
//		this.stj3TaxAmt = stj3TaxAmt;
//	}
//
//	public Double getStj4TaxAmt() {
//		return stj4TaxAmt;
//	}
//
//	public void setStj4TaxAmt(Double stj4TaxAmt) {
//		this.stj4TaxAmt = stj4TaxAmt;
//	}
//
//	public Double getStj5TaxAmt() {
//		return stj5TaxAmt;
//	}
//
//	public void setStj5TaxAmt(Double stj5TaxAmt) {
//		this.stj5TaxAmt = stj5TaxAmt;
//	}
//
//	@Override
//	public String toString() {
//		StringBuilder builder = new StringBuilder();
//		builder.append("Scenario [scenarioId=").append(scenarioId)
//				.append(", scenarioName=").append(scenarioName)
//				.append(", enteredDate=").append(enteredDate)
//				.append(", transactionType=").append(transactionType)
//				.append(", transactionTypeCode=").append(transactionTypeCode)
//				.append(", entityId=").append(entityId).append(", entityCode=")
//				.append(entityCode).append(", ratetype=").append(ratetype)
//				.append(", ratetypeCode=").append(ratetypeCode)
//				.append(", methodDelivery=").append(methodDelivery)
//				.append(", methodDeliveryCode=").append(methodDeliveryCode)
//				.append(", geoReconCode=").append(geoReconCode)
//				.append(", discountType=").append(discountType)
//				.append(", freightItemFlag=").append(freightItemFlag)
//				.append(", discountItemFlag=").append(discountItemFlag)
//				.append(", companyNbr=").append(companyNbr)
//				.append(", divisionNbr=").append(divisionNbr)
//				.append(", shipfromEntityId=").append(shipfromEntityId)
//				.append(", shipfromEntityCode=").append(shipfromEntityCode)
//				.append(", shipfromJurisdictionId=")
//				.append(shipfromJurisdictionId).append(", shipfromGeocode=")
//				.append(shipfromGeocode).append(", shipfromAddressLine1=")
//				.append(shipfromAddressLine1).append(", shipfromAddressLine2=")
//				.append(shipfromAddressLine2).append(", shipfromCity=")
//				.append(shipfromCity).append(", shipfromCounty=")
//				.append(shipfromCounty).append(", shipfromStateCode=")
//				.append(shipfromStateCode).append(", shipfromZip=")
//				.append(shipfromZip).append(", shipfromZipplus4=")
//				.append(shipfromZipplus4).append(", shipfromCountryCode=")
//				.append(shipfromCountryCode).append(", shipfromStj1Name=")
//				.append(shipfromStj1Name).append(", shipfromStj2Name=")
//				.append(shipfromStj2Name).append(", shipfromStj3Name=")
//				.append(shipfromStj3Name).append(", shipfromStj4Name=")
//				.append(shipfromStj4Name).append(", shipfromStj5Name=")
//				.append(shipfromStj5Name).append(", custNbr=").append(custNbr)
//				.append(", custName=").append(custName)
//				.append(", shiptoEntityId=").append(shiptoEntityId)
//				.append(", shiptoEntityCode=").append(shiptoEntityCode)
//				.append(", shiptoJurisdictionId=").append(shiptoJurisdictionId)
//				.append(", shiptoGeocode=").append(shiptoGeocode)
//				.append(", shiptoAddressLine1=").append(shiptoAddressLine1)
//				.append(", shiptoAddressLine2=").append(shiptoAddressLine2)
//				.append(", shiptoCity=").append(shiptoCity)
//				.append(", shiptoCounty=").append(shiptoCounty)
//				.append(", shiptoStateCode=").append(shiptoStateCode)
//				.append(", shiptoZip=").append(shiptoZip)
//				.append(", shiptoZipplus4=").append(shiptoZipplus4)
//				.append(", shiptoCountryCode=").append(shiptoCountryCode)
//				.append(", shiptoStj1Name=").append(shiptoStj1Name)
//				.append(", shiptoStj2Name=").append(shiptoStj2Name)
//				.append(", shiptoStj3Name=").append(shiptoStj3Name)
//				.append(", shiptoStj4Name=").append(shiptoStj4Name)
//				.append(", shiptoStj5Name=").append(shiptoStj5Name)
//				.append(", ordracptEntityId=").append(ordracptEntityId)
//				.append(", ordracptEntityCode=").append(ordracptEntityCode)
//				.append(", ordracptJurisdictionId=")
//				.append(ordracptJurisdictionId).append(", ordracptGeocode=")
//				.append(ordracptGeocode).append(", ordracptAddressLine1=")
//				.append(ordracptAddressLine1).append(", ordracptAddressLine2=")
//				.append(ordracptAddressLine2).append(", ordracptCity=")
//				.append(ordracptCity).append(", ordracptCounty=")
//				.append(ordracptCounty).append(", ordracptStateCode=")
//				.append(ordracptStateCode).append(", ordracptZip=")
//				.append(ordracptZip).append(", ordracptZipplus4=")
//				.append(ordracptZipplus4).append(", ordracptCountryCode=")
//				.append(ordracptCountryCode).append(", ordracptStj1Name=")
//				.append(ordracptStj1Name).append(", ordracptStj2Name=")
//				.append(ordracptStj2Name).append(", ordracptStj3Name=")
//				.append(ordracptStj3Name).append(", ordracptStj4Name=")
//				.append(ordracptStj4Name).append(", ordracptStj5Name=")
//				.append(ordracptStj5Name).append(", ordrorgnEntityId=")
//				.append(ordrorgnEntityId).append(", ordrorgnEntityCode=")
//				.append(ordrorgnEntityCode).append(", ordrorgnJurisdictionId=")
//				.append(ordrorgnJurisdictionId).append(", ordrorgnGeocode=")
//				.append(ordrorgnGeocode).append(", ordrorgnAddressLine1=")
//				.append(ordrorgnAddressLine1).append(", ordrorgnAddressLine2=")
//				.append(ordrorgnAddressLine2).append(", ordrorgnCity=")
//				.append(ordrorgnCity).append(", ordrorgnCounty=")
//				.append(ordrorgnCounty).append(", ordrorgnStateCode=")
//				.append(ordrorgnStateCode).append(", ordrorgnZip=")
//				.append(ordrorgnZip).append(", ordrorgnZipplus4=")
//				.append(ordrorgnZipplus4).append(", ordrorgnCountryCode=")
//				.append(ordrorgnCountryCode).append(", ordrorgnStj1Name=")
//				.append(ordrorgnStj1Name).append(", ordrorgnStj2Name=")
//				.append(ordrorgnStj2Name).append(", ordrorgnStj3Name=")
//				.append(ordrorgnStj3Name).append(", ordrorgnStj4Name=")
//				.append(ordrorgnStj4Name).append(", ordrorgnStj5Name=")
//				.append(ordrorgnStj5Name).append(", firstuseEntityId=")
//				.append(firstuseEntityId).append(", firstuseEntityCode=")
//				.append(firstuseEntityCode).append(", firstuseJurisdictionId=")
//				.append(firstuseJurisdictionId).append(", firstuseGeocode=")
//				.append(firstuseGeocode).append(", firstuseAddressLine1=")
//				.append(firstuseAddressLine1).append(", firstuseAddressLine2=")
//				.append(firstuseAddressLine2).append(", firstuseCity=")
//				.append(firstuseCity).append(", firstuseCounty=")
//				.append(firstuseCounty).append(", firstuseStateCode=")
//				.append(firstuseStateCode).append(", firstuseZip=")
//				.append(firstuseZip).append(", firstuseZipplus4=")
//				.append(firstuseZipplus4).append(", firstuseCountryCode=")
//				.append(firstuseCountryCode).append(", firstuseStj1Name=")
//				.append(firstuseStj1Name).append(", firstuseStj2Name=")
//				.append(firstuseStj2Name).append(", firstuseStj3Name=")
//				.append(firstuseStj3Name).append(", firstuseStj4Name=")
//				.append(firstuseStj4Name).append(", firstuseStj5Name=")
//				.append(firstuseStj5Name).append(", billtoEntityId=")
//				.append(billtoEntityId).append(", billtoEntityCode=")
//				.append(billtoEntityCode).append(", billtoJurisdictionId=")
//				.append(billtoJurisdictionId).append(", billtoGeocode=")
//				.append(billtoGeocode).append(", billtoAddressLine1=")
//				.append(billtoAddressLine1).append(", billtoAddressLine2=")
//				.append(billtoAddressLine2).append(", billtoCity=")
//				.append(billtoCity).append(", billtoCounty=")
//				.append(billtoCounty).append(", billtoStateCode=")
//				.append(billtoStateCode).append(", billtoZip=")
//				.append(billtoZip).append(", billtoZipplus4=")
//				.append(billtoZipplus4).append(", billtoCountryCode=")
//				.append(billtoCountryCode).append(", billtoStj1Name=")
//				.append(billtoStj1Name).append(", billtoStj2Name=")
//				.append(billtoStj2Name).append(", billtoStj3Name=")
//				.append(billtoStj3Name).append(", billtoStj4Name=")
//				.append(billtoStj4Name).append(", billtoStj5Name=")
//				.append(billtoStj5Name).append(", glDate=").append(glDate)
//				.append(", comments=").append(comments)
//				.append(", processMethod=").append(processMethod)
//				.append(", entityLocnSetDtlId=").append(entityLocnSetDtlId)
//				.append(", countrySitusMatrixId=").append(countrySitusMatrixId)
//				.append(", stateSitusMatrixId=").append(stateSitusMatrixId)
//				.append(", countySitusMatrixId=").append(countySitusMatrixId)
//				.append(", citySitusMatrixId=").append(citySitusMatrixId)
//				.append(", stj1SitusMatrixId=").append(stj1SitusMatrixId)
//				.append(", stj2SitusMatrixId=").append(stj2SitusMatrixId)
//				.append(", stj3SitusMatrixId=").append(stj3SitusMatrixId)
//				.append(", stj4SitusMatrixId=").append(stj4SitusMatrixId)
//				.append(", stj5SitusMatrixId=").append(stj5SitusMatrixId)
//				.append(", countryNexusDefDetailId=")
//				.append(countryNexusDefDetailId)
//				.append(", stateNexusDefDetailId=")
//				.append(stateNexusDefDetailId)
//				.append(", countyNexusDefDetailId=")
//				.append(countyNexusDefDetailId)
//				.append(", cityNexusDefDetailId=").append(cityNexusDefDetailId)
//				.append(", stj1NexusDefDetailId=").append(stj1NexusDefDetailId)
//				.append(", stj2NexusDefDetailId=").append(stj2NexusDefDetailId)
//				.append(", stj3NexusDefDetailId=").append(stj3NexusDefDetailId)
//				.append(", stj4NexusDefDetailId=").append(stj4NexusDefDetailId)
//				.append(", stj5NexusDefDetailId=").append(stj5NexusDefDetailId)
//				.append(", stj6NexusDefDetailId=").append(stj6NexusDefDetailId)
//				.append(", stj7NexusDefDetailId=").append(stj7NexusDefDetailId)
//				.append(", stj8NexusDefDetailId=").append(stj8NexusDefDetailId)
//				.append(", stj9NexusDefDetailId=").append(stj9NexusDefDetailId)
//				.append(", stj10NexusDefDetailId=")
//				.append(stj10NexusDefDetailId)
//				.append(", countryTaxcodeDetailId=")
//				.append(countryTaxcodeDetailId)
//				.append(", stateTaxcodeDetailId=").append(stateTaxcodeDetailId)
//				.append(", countyTaxcodeDetailId=")
//				.append(countyTaxcodeDetailId).append(", cityTaxcodeDetailId=")
//				.append(cityTaxcodeDetailId).append(", stj1TaxcodeDetailId=")
//				.append(stj1TaxcodeDetailId).append(", stj2TaxcodeDetailId=")
//				.append(stj2TaxcodeDetailId).append(", stj3TaxcodeDetailId=")
//				.append(stj3TaxcodeDetailId).append(", stj4TaxcodeDetailId=")
//				.append(stj4TaxcodeDetailId).append(", stj5TaxcodeDetailId=")
//				.append(stj5TaxcodeDetailId).append(", stj6TaxcodeDetailId=")
//				.append(stj6TaxcodeDetailId).append(", stj7TaxcodeDetailId=")
//				.append(stj7TaxcodeDetailId).append(", stj8TaxcodeDetailId=")
//				.append(stj8TaxcodeDetailId).append(", stj9TaxcodeDetailId=")
//				.append(stj9TaxcodeDetailId).append(", stj10TaxcodeDetailId=")
//				.append(stj10TaxcodeDetailId).append(", countryTaxrateId=")
//				.append(countryTaxrateId).append(", stateTaxrateId=")
//				.append(stateTaxrateId).append(", countyTaxrateId=")
//				.append(countyTaxrateId).append(", cityTaxrateId=")
//				.append(cityTaxrateId).append(", stj1TaxrateId=")
//				.append(stj1TaxrateId).append(", stj2TaxrateId=")
//				.append(stj2TaxrateId).append(", stj3TaxrateId=")
//				.append(stj3TaxrateId).append(", stj4TaxrateId=")
//				.append(stj4TaxrateId).append(", stj5TaxrateId=")
//				.append(stj5TaxrateId).append(", stj6TaxrateId=")
//				.append(stj6TaxrateId).append(", stj7TaxrateId=")
//				.append(stj7TaxrateId).append(", stj8TaxrateId=")
//				.append(stj8TaxrateId).append(", stj9TaxrateId=")
//				.append(stj9TaxrateId).append(", stj10TaxrateId=")
//				.append(stj10TaxrateId).append(", creditInd=")
//				.append(creditInd).append(", exemptInd=").append(exemptInd)
//				.append(", certNbr=").append(certNbr).append(", certType=")
//				.append(certType).append(", certSignDate=")
//				.append(certSignDate).append(", certExpirationDate=")
//				.append(certExpirationDate).append(", certImage=")
//				.append(certImage).append(", exemptReason=")
//				.append(exemptReason).append(", exemptCode=")
//				.append(exemptCode).append(", exemptAmt=").append(exemptAmt)
//				.append(", exclusionAmt=").append(exclusionAmt)
//				.append(", invoiceDate=").append(invoiceDate)
//				.append(", invoiceNbr=").append(invoiceNbr)
//				.append(", invoiceDesc=").append(invoiceDesc)
//				.append(", invoiceGrossAmt=").append(invoiceGrossAmt)
//				.append(", invoiceFreightAmt=").append(invoiceFreightAmt)
//				.append(", invoiceDiscAmt=").append(invoiceDiscAmt)
//				.append(", invoiceDiscTypeCode=").append(invoiceDiscTypeCode)
//				.append(", invoiceLineNbr=").append(invoiceLineNbr)
//				.append(", invoiceLineDesc=").append(invoiceLineDesc)
//				.append(", invoiceLineItemCount=").append(invoiceLineItemCount)
//				.append(", invoiceLineItemAmt=").append(invoiceLineItemAmt)
//				.append(", invoiceLineGrossAmt=").append(invoiceLineGrossAmt)
//				.append(", invoiceLineFreightAmt=")
//				.append(invoiceLineFreightAmt).append(", invoiceLineDiscAmt=")
//				.append(invoiceLineDiscAmt)
//				.append(", invoiceLineDiscTypeCode=")
//				.append(invoiceLineDiscTypeCode)
//				.append(", invoiceLineProductCode=")
//				.append(invoiceLineProductCode).append(", taxcodeCode=")
//				.append(taxcodeCode).append(", taxableAmt=").append(taxableAmt)
//				.append(", countryTaxableAmt=").append(countryTaxableAmt)
//				.append(", stateTaxableAmt=").append(stateTaxableAmt)
//				.append(", stateTier1TaxableAmt=").append(stateTier1TaxableAmt)
//				.append(", stateTier2TaxableAmt=").append(stateTier2TaxableAmt)
//				.append(", stateTier3TaxableAmt=").append(stateTier3TaxableAmt)
//				.append(", countyTaxableAmt=").append(countyTaxableAmt)
//				.append(", cityTaxableAmt=").append(cityTaxableAmt)
//				.append(", stj1TaxableAmt=").append(stj1TaxableAmt)
//				.append(", stj2TaxableAmt=").append(stj2TaxableAmt)
//				.append(", stj3TaxableAmt=").append(stj3TaxableAmt)
//				.append(", stj4TaxableAmt=").append(stj4TaxableAmt)
//				.append(", stj5TaxableAmt=").append(stj5TaxableAmt)
//				.append(", stj6TaxableAmt=").append(stj6TaxableAmt)
//				.append(", stj7TaxableAmt=").append(stj7TaxableAmt)
//				.append(", stj8TaxableAmt=").append(stj8TaxableAmt)
//				.append(", stj9TaxableAmt=").append(stj9TaxableAmt)
//				.append(", stj10TaxableAmt=").append(stj10TaxableAmt)
//				.append(", taxAmt=").append(taxAmt).append(", countryTaxAmt=")
//				.append(countryTaxAmt).append(", stateTaxAmt=")
//				.append(stateTaxAmt).append(", stateTier1TaxAmt=")
//				.append(stateTier1TaxAmt).append(", stateTier2TaxAmt=")
//				.append(stateTier2TaxAmt).append(", stateTier3TaxAmt=")
//				.append(stateTier3TaxAmt).append(", countyTaxAmt=")
//				.append(countyTaxAmt).append(", cityTaxAmt=")
//				.append(cityTaxAmt).append(", stj1TaxAmt=").append(stj1TaxAmt)
//				.append(", stj2TaxAmt=").append(stj2TaxAmt)
//				.append(", stj3TaxAmt=").append(stj3TaxAmt)
//				.append(", stj4TaxAmt=").append(stj4TaxAmt)
//				.append(", stj5TaxAmt=").append(stj5TaxAmt)
//				.append(", stj6TaxAmt=").append(stj6TaxAmt)
//				.append(", stj7TaxAmt=").append(stj7TaxAmt)
//				.append(", stj8TaxAmt=").append(stj8TaxAmt)
//				.append(", stj9TaxAmt=").append(stj9TaxAmt)
//				.append(", stj10TaxAmt=").append(stj10TaxAmt)
//				.append(", doNotCalcFlag=").append(doNotCalcFlag)
//				.append(", stateFreightDetailId=").append(stateFreightDetailId)
//				.append(", companyDesc=").append(companyDesc)
//				.append(", custAddressLine1=").append(custAddressLine1)
//				.append(", custAddressLine2=").append(custAddressLine2)
//				.append(", custCity=").append(custCity)
//				.append(", custCountryCode=").append(custCountryCode)
//				.append(", custCounty=").append(custCounty)
//				.append(", custEntityCode=").append(custEntityCode)
//				.append(", custEntityId=").append(custEntityId)
//				.append(", custGeocode=").append(custGeocode)
//				.append(", custJurisdictionId=").append(custJurisdictionId)
//				.append(", custLocnCode=").append(custLocnCode)
//				.append(", custStateCode=").append(custStateCode)
//				.append(", custZip=").append(custZip).append(", custZipplus4=")
//				.append(custZipplus4).append(", divisionDesc=")
//				.append(divisionDesc).append(", locationName=")
//				.append(locationName).append(", taxPaidToVendorFlag=")
//				.append(taxPaidToVendorFlag).append(", userDate01=")
//				.append(userDate01).append(", userDate02=").append(userDate02)
//				.append(", userDate03=").append(userDate03)
//				.append(", userDate04=").append(userDate04)
//				.append(", userDate05=").append(userDate05)
//				.append(", userDate06=").append(userDate06)
//				.append(", userDate07=").append(userDate07)
//				.append(", userDate08=").append(userDate08)
//				.append(", userDate09=").append(userDate09)
//				.append(", userDate10=").append(userDate10)
//				.append(", userNumber01=").append(userNumber01)
//				.append(", userNumber02=").append(userNumber02)
//				.append(", userNumber03=").append(userNumber03)
//				.append(", userNumber04=").append(userNumber04)
//				.append(", userNumber05=").append(userNumber05)
//				.append(", userNumber06=").append(userNumber06)
//				.append(", userNumber07=").append(userNumber07)
//				.append(", userNumber08=").append(userNumber08)
//				.append(", userNumber09=").append(userNumber09)
//				.append(", userNumber10=").append(userNumber10)
//				.append(", userText01=").append(userText01)
//				.append(", userText02=").append(userText02)
//				.append(", userText03=").append(userText03)
//				.append(", userText04=").append(userText04)
//				.append(", userText05=").append(userText05)
//				.append(", userText06=").append(userText06)
//				.append(", userText07=").append(userText07)
//				.append(", userText08=").append(userText08)
//				.append(", userText09=").append(userText09)
//				.append(", userText10=").append(userText10)
//				.append(", userText11=").append(userText11)
//				.append(", userText12=").append(userText12)
//				.append(", userText13=").append(userText13)
//				.append(", userText14=").append(userText14)
//				.append(", userText15=").append(userText15)
//				.append(", userText16=").append(userText16)
//				.append(", userText17=").append(userText17)
//				.append(", userText18=").append(userText18)
//				.append(", userText19=").append(userText19)
//				.append(", userText20=").append(userText20)
//				.append(", userText21=").append(userText21)
//				.append(", userText22=").append(userText22)
//				.append(", userText23=").append(userText23)
//				.append(", userText24=").append(userText24)
//				.append(", userText25=").append(userText25)
//				.append(", userText26=").append(userText26)
//				.append(", userText27=").append(userText27)
//				.append(", userText28=").append(userText28)
//				.append(", userText29=").append(userText29)
//				.append(", userText30=").append(userText30)
//				.append(", vendorTaxAmt=").append(vendorTaxAmt)
//				.append(", countryCode=").append(countryCode)
//				.append(", stateCode=").append(stateCode)
//				.append(", countyName=").append(countyName)
//				.append(", cityName=").append(cityName).append(", stj1Name=")
//				.append(stj1Name).append(", stj2Name=").append(stj2Name)
//				.append(", stj3Name=").append(stj3Name).append(", stj4Name=")
//				.append(stj4Name).append(", stj5Name=").append(stj5Name)
//				.append(", stj6Name=").append(stj6Name).append(", stj7Name=")
//				.append(stj7Name).append(", stj8Name=").append(stj8Name)
//				.append(", stj9Name=").append(stj9Name).append(", stj10Name=")
//				.append(stj10Name).append(", countryExemptAmt=")
//				.append(countryExemptAmt).append(", stateExemptAmt=")
//				.append(stateExemptAmt).append(", countyExemptAmt=")
//				.append(countyExemptAmt).append(", cityExemptAmt=")
//				.append(cityExemptAmt).append(", stj1ExemptAmt=")
//				.append(stj1ExemptAmt).append(", stj2ExemptAmt=")
//				.append(stj2ExemptAmt).append(", stj3ExemptAmt=")
//				.append(stj3ExemptAmt).append(", stj4ExemptAmt=")
//				.append(stj4ExemptAmt).append(", stj5ExemptAmt=")
//				.append(stj5ExemptAmt).append(", stj6ExemptAmt=")
//				.append(stj6ExemptAmt).append(", stj7ExemptAmt=")
//				.append(stj7ExemptAmt).append(", stj8ExemptAmt=")
//				.append(stj8ExemptAmt).append(", stj9ExemptAmt=")
//				.append(stj9ExemptAmt).append(", stj10ExemptAmt=")
//				.append(stj10ExemptAmt).append(", countryRate=")
//				.append(countryRate).append(", stateRate=").append(stateRate)
//				.append(", stateTier2Rate=").append(stateTier2Rate)
//				.append(", stateTier3Rate=").append(stateTier3Rate)
//				.append(", stateTier1MaxAmount=").append(stateTier1MaxAmount)
//				.append(", stateTier2MinAmount=").append(stateTier2MinAmount)
//				.append(", stateTier2MaxAmount=").append(stateTier2MaxAmount)
//				.append(", stateMaxtaxAmount=").append(stateMaxtaxAmount)
//				.append(", countyRate=").append(countyRate)
//				.append(", countySplitAmount=").append(countySplitAmount)
//				.append(", countyMaxtaxAmount=").append(countyMaxtaxAmount)
//				.append(", cityRate=").append(cityRate)
//				.append(", citySplitAmount=").append(citySplitAmount)
//				.append(", citySplitRate=").append(citySplitRate)
//				.append(", stj1Rate=").append(stj1Rate).append(", stj2Rate=")
//				.append(stj2Rate).append(", stj3Rate=").append(stj3Rate)
//				.append(", stj4Rate=").append(stj4Rate).append(", stj5Rate=")
//				.append(stj5Rate).append(", stj6Rate=").append(stj6Rate)
//				.append(", stj7Rate=").append(stj7Rate).append(", stj8Rate=")
//				.append(stj8Rate).append(", stj9Rate=").append(stj9Rate)
//				.append(", stj10Rate=").append(stj10Rate)
//				.append(", combinedRate=").append(combinedRate)
//				.append(", countryTaxtypeCode=").append(countryTaxtypeCode)
//				.append(", stateTaxtypeCode=").append(stateTaxtypeCode)
//				.append(", countyTaxtypeCode=").append(countyTaxtypeCode)
//				.append(", cityTaxtypeCode=").append(cityTaxtypeCode)
//				.append(", stj1TaxtypeCode=").append(stj1TaxtypeCode)
//				.append(", stj2TaxtypeCode=").append(stj2TaxtypeCode)
//				.append(", stj3TaxtypeCode=").append(stj3TaxtypeCode)
//				.append(", stj4TaxtypeCode=").append(stj4TaxtypeCode)
//				.append(", stj5TaxtypeCode=").append(stj5TaxtypeCode)
//				.append(", stj6TaxtypeCode=").append(stj6TaxtypeCode)
//				.append(", stj7TaxtypeCode=").append(stj7TaxtypeCode)
//				.append(", stj8TaxtypeCode=").append(stj8TaxtypeCode)
//				.append(", stj9TaxtypeCode=").append(stj9TaxtypeCode)
//				.append(", stj10TaxtypeCode=").append(stj10TaxtypeCode)
//				.append(", invoiceTaxPaidAmt=").append(invoiceTaxPaidAmt)
//				.append(", invoiceDiffAmt=").append(invoiceDiffAmt)
//				.append(", procruleSessionId=").append(procruleSessionId)				
//				.append(", invoiceLineTaxPaidAmt=")
//				.append(invoiceLineTaxPaidAmt).append(", invoiceLineDiffAmt=")
//				.append(invoiceLineDiffAmt).append(", invoiceDiffBatchNo=")
//				.append(invoiceDiffBatchNo).append(", invoiceTaxAmt=")
//				.append(invoiceTaxAmt).append(", invoiceFreightableAmt=")
//				.append(invoiceFreightableAmt).append(", invoiceMiscableAmt=")
//				.append(invoiceMiscableAmt).append(", invoiceProfrtFlag=")
//				.append(invoiceProfrtFlag).append(", invoicePromiscFlag=")
//				.append(invoicePromiscFlag).append(", invoiceMiscAmt=")
//				.append(invoiceMiscAmt).append(", invoiceLineMiscAmt=")
//				.append(invoiceMiscAmt).append(", invoiceLineMiscAmt=")
//				.append(invoiceLineMiscAmt)
//				.append(", reprocessCode=").append(reprocessCode)				
//				.append("]");
//		return builder.toString();
//	}
//
//	public Long getEntityId() {
//		return entityId;
//	}
//
//	public void setEntityId(Long entityId) {
//		this.entityId = entityId;
//	}
//
//	public String getDoNotCalcFlag() {
//		return doNotCalcFlag;
//	}
//
//	public void setDoNotCalcFlag(String doNotCalcFlag) {
//		this.doNotCalcFlag = doNotCalcFlag;
//	}
//
//	public String getTransactionType() {
//		return transactionType;
//	}
//
//	public void setTransactionType(String transactionType) {
//		this.transactionType = transactionType;
//	}
//
//	public String getRatetype() {
//		return ratetype;
//	}
//
//	public void setRatetype(String ratetype) {
//		this.ratetype = ratetype;
//	}
//
//	public String getMethodDelivery() {
//		return methodDelivery;
//	}
//
//	public void setMethodDelivery(String methodDelivery) {
//		this.methodDelivery = methodDelivery;
//	}
//
//	public Long getStateFreightDetailId() {
//		return stateFreightDetailId;
//	}
//
//	public void setStateFreightDetailId(Long stateFreightDetailId) {
//		this.stateFreightDetailId = stateFreightDetailId;
//	}
//
//	public Long getStj6NexusDefDetailId() {
//		return stj6NexusDefDetailId;
//	}
//
//	public void setStj6NexusDefDetailId(Long stj6NexusDefDetailId) {
//		this.stj6NexusDefDetailId = stj6NexusDefDetailId;
//	}
//
//	public Long getStj7NexusDefDetailId() {
//		return stj7NexusDefDetailId;
//	}
//
//	public void setStj7NexusDefDetailId(Long stj7NexusDefDetailId) {
//		this.stj7NexusDefDetailId = stj7NexusDefDetailId;
//	}
//
//	public Long getStj8NexusDefDetailId() {
//		return stj8NexusDefDetailId;
//	}
//
//	public void setStj8NexusDefDetailId(Long stj8NexusDefDetailId) {
//		this.stj8NexusDefDetailId = stj8NexusDefDetailId;
//	}
//
//	public Long getStj9NexusDefDetailId() {
//		return stj9NexusDefDetailId;
//	}
//
//	public void setStj9NexusDefDetailId(Long stj9NexusDefDetailId) {
//		this.stj9NexusDefDetailId = stj9NexusDefDetailId;
//	}
//
//	public Long getStj10NexusDefDetailId() {
//		return stj10NexusDefDetailId;
//	}
//
//	public void setStj10NexusDefDetailId(Long stj10NexusDefDetailId) {
//		this.stj10NexusDefDetailId = stj10NexusDefDetailId;
//	}
//
//	public Long getStj6TaxcodeDetailId() {
//		return stj6TaxcodeDetailId;
//	}
//
//	public void setStj6TaxcodeDetailId(Long stj6TaxcodeDetailId) {
//		this.stj6TaxcodeDetailId = stj6TaxcodeDetailId;
//	}
//
//	public Long getStj7TaxcodeDetailId() {
//		return stj7TaxcodeDetailId;
//	}
//
//	public void setStj7TaxcodeDetailId(Long stj7TaxcodeDetailId) {
//		this.stj7TaxcodeDetailId = stj7TaxcodeDetailId;
//	}
//
//	public Long getStj8TaxcodeDetailId() {
//		return stj8TaxcodeDetailId;
//	}
//
//	public void setStj8TaxcodeDetailId(Long stj8TaxcodeDetailId) {
//		this.stj8TaxcodeDetailId = stj8TaxcodeDetailId;
//	}
//
//	public Long getStj9TaxcodeDetailId() {
//		return stj9TaxcodeDetailId;
//	}
//
//	public void setStj9TaxcodeDetailId(Long stj9TaxcodeDetailId) {
//		this.stj9TaxcodeDetailId = stj9TaxcodeDetailId;
//	}
//
//	public Long getStj10TaxcodeDetailId() {
//		return stj10TaxcodeDetailId;
//	}
//
//	public void setStj10TaxcodeDetailId(Long stj10TaxcodeDetailId) {
//		this.stj10TaxcodeDetailId = stj10TaxcodeDetailId;
//	}
//
//	public Double getStj6TaxAmt() {
//		return stj6TaxAmt;
//	}
//
//	public void setStj6TaxAmt(Double stj6TaxAmt) {
//		this.stj6TaxAmt = stj6TaxAmt;
//	}
//
//	public Double getStj7TaxAmt() {
//		return stj7TaxAmt;
//	}
//
//	public void setStj7TaxAmt(Double stj7TaxAmt) {
//		this.stj7TaxAmt = stj7TaxAmt;
//	}
//
//	public Double getStj8TaxAmt() {
//		return stj8TaxAmt;
//	}
//
//	public void setStj8TaxAmt(Double stj8TaxAmt) {
//		this.stj8TaxAmt = stj8TaxAmt;
//	}
//
//	public Double getStj9TaxAmt() {
//		return stj9TaxAmt;
//	}
//
//	public void setStj9TaxAmt(Double stj9TaxAmt) {
//		this.stj9TaxAmt = stj9TaxAmt;
//	}
//
//	public Double getStj10TaxAmt() {
//		return stj10TaxAmt;
//	}
//
//	public void setStj10TaxAmt(Double stj10TaxAmt) {
//		this.stj10TaxAmt = stj10TaxAmt;
//	}
//
//	public Double getStj6TaxableAmt() {
//		return stj6TaxableAmt;
//	}
//
//	public void setStj6TaxableAmt(Double stj6TaxableAmt) {
//		this.stj6TaxableAmt = stj6TaxableAmt;
//	}
//
//	public Double getStj7TaxableAmt() {
//		return stj7TaxableAmt;
//	}
//
//	public void setStj7TaxableAmt(Double stj7TaxableAmt) {
//		this.stj7TaxableAmt = stj7TaxableAmt;
//	}
//
//	public Double getStj8TaxableAmt() {
//		return stj8TaxableAmt;
//	}
//
//	public void setStj8TaxableAmt(Double stj8TaxableAmt) {
//		this.stj8TaxableAmt = stj8TaxableAmt;
//	}
//
//	public Double getStj9TaxableAmt() {
//		return stj9TaxableAmt;
//	}
//
//	public void setStj9TaxableAmt(Double stj9TaxableAmt) {
//		this.stj9TaxableAmt = stj9TaxableAmt;
//	}
//
//	public Double getStj10TaxableAmt() {
//		return stj10TaxableAmt;
//	}
//
//	public void setStj10TaxableAmt(Double stj10TaxableAmt) {
//		this.stj10TaxableAmt = stj10TaxableAmt;
//	}
//
//	public String getCompanyDesc() {
//		return companyDesc;
//	}
//
//	public void setCompanyDesc(String companyDesc) {
//		this.companyDesc = companyDesc;
//	}
//
//	public String getCustAddressLine1() {
//		return custAddressLine1;
//	}
//
//	public void setCustAddressLine1(String custAddressLine1) {
//		this.custAddressLine1 = custAddressLine1;
//	}
//
//	public String getCustAddressLine2() {
//		return custAddressLine2;
//	}
//
//	public void setCustAddressLine2(String custAddressLine2) {
//		this.custAddressLine2 = custAddressLine2;
//	}
//
//	public String getCustCity() {
//		return custCity;
//	}
//
//	public void setCustCity(String custCity) {
//		this.custCity = custCity;
//	}
//
//	public String getCustCountryCode() {
//		return custCountryCode;
//	}
//
//	public void setCustCountryCode(String custCountryCode) {
//		this.custCountryCode = custCountryCode;
//	}
//
//	public String getCustCounty() {
//		return custCounty;
//	}
//
//	public void setCustCounty(String custCounty) {
//		this.custCounty = custCounty;
//	}
//
//	public String getCustEntityCode() {
//		return custEntityCode;
//	}
//
//	public void setCustEntityCode(String custEntityCode) {
//		this.custEntityCode = custEntityCode;
//	}
//
//	public Long getCustEntityId() {
//		return custEntityId;
//	}
//
//	public void setCustEntityId(Long custEntityId) {
//		this.custEntityId = custEntityId;
//	}
//
//	public String getCustGeocode() {
//		return custGeocode;
//	}
//
//	public void setCustGeocode(String custGeocode) {
//		this.custGeocode = custGeocode;
//	}
//
//	public Long getCustJurisdictionId() {
//		return custJurisdictionId;
//	}
//
//	public void setCustJurisdictionId(Long custJurisdictionId) {
//		this.custJurisdictionId = custJurisdictionId;
//	}
//
//	public String getCustLocnCode() {
//		return custLocnCode;
//	}
//
//	public void setCustLocnCode(String custLocnCode) {
//		this.custLocnCode = custLocnCode;
//	}
//
//	public String getCustStateCode() {
//		return custStateCode;
//	}
//
//	public void setCustStateCode(String custStateCode) {
//		this.custStateCode = custStateCode;
//	}
//
//	public String getCustZip() {
//		return custZip;
//	}
//
//	public void setCustZip(String custZip) {
//		this.custZip = custZip;
//	}
//
//	public String getCustZipplus4() {
//		return custZipplus4;
//	}
//
//	public void setCustZipplus4(String custZipplus4) {
//		this.custZipplus4 = custZipplus4;
//	}
//
//	public String getDivisionDesc() {
//		return divisionDesc;
//	}
//
//	public void setDivisionDesc(String divisionDesc) {
//		this.divisionDesc = divisionDesc;
//	}
//
//	public String getLocationName() {
//		return locationName;
//	}
//
//	public void setLocationName(String locationName) {
//		this.locationName = locationName;
//	}
//
//	public String getTaxPaidToVendorFlag() {
//		return taxPaidToVendorFlag;
//	}
//
//	public void setTaxPaidToVendorFlag(String taxPaidToVendorFlag) {
//		this.taxPaidToVendorFlag = taxPaidToVendorFlag;
//	}
//
//	public Date getUserDate01() {
//		return userDate01;
//	}
//
//	public void setUserDate01(Date userDate01) {
//		this.userDate01 = userDate01;
//	}
//
//	public Date getUserDate02() {
//		return userDate02;
//	}
//
//	public void setUserDate02(Date userDate02) {
//		this.userDate02 = userDate02;
//	}
//
//	public Date getUserDate03() {
//		return userDate03;
//	}
//
//	public void setUserDate03(Date userDate03) {
//		this.userDate03 = userDate03;
//	}
//
//	public Date getUserDate04() {
//		return userDate04;
//	}
//
//	public void setUserDate04(Date userDate04) {
//		this.userDate04 = userDate04;
//	}
//
//	public Date getUserDate05() {
//		return userDate05;
//	}
//
//	public void setUserDate05(Date userDate05) {
//		this.userDate05 = userDate05;
//	}
//
//	public Date getUserDate06() {
//		return userDate06;
//	}
//
//	public void setUserDate06(Date userDate06) {
//		this.userDate06 = userDate06;
//	}
//
//	public Date getUserDate07() {
//		return userDate07;
//	}
//
//	public void setUserDate07(Date userDate07) {
//		this.userDate07 = userDate07;
//	}
//
//	public Date getUserDate08() {
//		return userDate08;
//	}
//
//	public void setUserDate08(Date userDate08) {
//		this.userDate08 = userDate08;
//	}
//
//	public Date getUserDate09() {
//		return userDate09;
//	}
//
//	public void setUserDate09(Date userDate09) {
//		this.userDate09 = userDate09;
//	}
//
//	public Date getUserDate10() {
//		return userDate10;
//	}
//
//	public void setUserDate10(Date userDate10) {
//		this.userDate10 = userDate10;
//	}
//
//	public Double getUserNumber01() {
//		return userNumber01;
//	}
//
//	public void setUserNumber01(Double userNumber01) {
//		this.userNumber01 = userNumber01;
//	}
//
//	public Double getUserNumber02() {
//		return userNumber02;
//	}
//
//	public void setUserNumber02(Double userNumber02) {
//		this.userNumber02 = userNumber02;
//	}
//
//	public Double getUserNumber03() {
//		return userNumber03;
//	}
//
//	public void setUserNumber03(Double userNumber03) {
//		this.userNumber03 = userNumber03;
//	}
//
//	public Double getUserNumber04() {
//		return userNumber04;
//	}
//
//	public void setUserNumber04(Double userNumber04) {
//		this.userNumber04 = userNumber04;
//	}
//
//	public Double getUserNumber05() {
//		return userNumber05;
//	}
//
//	public void setUserNumber05(Double userNumber05) {
//		this.userNumber05 = userNumber05;
//	}
//
//	public Double getUserNumber06() {
//		return userNumber06;
//	}
//
//	public void setUserNumber06(Double userNumber06) {
//		this.userNumber06 = userNumber06;
//	}
//
//	public Double getUserNumber07() {
//		return userNumber07;
//	}
//
//	public void setUserNumber07(Double userNumber07) {
//		this.userNumber07 = userNumber07;
//	}
//
//	public Double getUserNumber08() {
//		return userNumber08;
//	}
//
//	public void setUserNumber08(Double userNumber08) {
//		this.userNumber08 = userNumber08;
//	}
//
//	public Double getUserNumber09() {
//		return userNumber09;
//	}
//
//	public void setUserNumber09(Double userNumber09) {
//		this.userNumber09 = userNumber09;
//	}
//
//	public Double getUserNumber10() {
//		return userNumber10;
//	}
//
//	public void setUserNumber10(Double userNumber10) {
//		this.userNumber10 = userNumber10;
//	}
//
//	public String getUserText01() {
//		return userText01;
//	}
//
//	public void setUserText01(String userText01) {
//		this.userText01 = userText01;
//	}
//
//	public String getUserText02() {
//		return userText02;
//	}
//
//	public void setUserText02(String userText02) {
//		this.userText02 = userText02;
//	}
//
//	public String getUserText03() {
//		return userText03;
//	}
//
//	public void setUserText03(String userText03) {
//		this.userText03 = userText03;
//	}
//
//	public String getUserText04() {
//		return userText04;
//	}
//
//	public void setUserText04(String userText04) {
//		this.userText04 = userText04;
//	}
//
//	public String getUserText05() {
//		return userText05;
//	}
//
//	public void setUserText05(String userText05) {
//		this.userText05 = userText05;
//	}
//
//	public String getUserText06() {
//		return userText06;
//	}
//
//	public void setUserText06(String userText06) {
//		this.userText06 = userText06;
//	}
//
//	public String getUserText07() {
//		return userText07;
//	}
//
//	public void setUserText07(String userText07) {
//		this.userText07 = userText07;
//	}
//
//	public String getUserText08() {
//		return userText08;
//	}
//
//	public void setUserText08(String userText08) {
//		this.userText08 = userText08;
//	}
//
//	public String getUserText09() {
//		return userText09;
//	}
//
//	public void setUserText09(String userText09) {
//		this.userText09 = userText09;
//	}
//
//	public String getUserText10() {
//		return userText10;
//	}
//
//	public void setUserText10(String userText10) {
//		this.userText10 = userText10;
//	}
//
//	public String getUserText11() {
//		return userText11;
//	}
//
//	public void setUserText11(String userText11) {
//		this.userText11 = userText11;
//	}
//
//	public String getUserText12() {
//		return userText12;
//	}
//
//	public void setUserText12(String userText12) {
//		this.userText12 = userText12;
//	}
//
//	public String getUserText13() {
//		return userText13;
//	}
//
//	public void setUserText13(String userText13) {
//		this.userText13 = userText13;
//	}
//
//	public String getUserText14() {
//		return userText14;
//	}
//
//	public void setUserText14(String userText14) {
//		this.userText14 = userText14;
//	}
//
//	public String getUserText15() {
//		return userText15;
//	}
//
//	public void setUserText15(String userText15) {
//		this.userText15 = userText15;
//	}
//
//	public String getUserText16() {
//		return userText16;
//	}
//
//	public void setUserText16(String userText16) {
//		this.userText16 = userText16;
//	}
//
//	public String getUserText17() {
//		return userText17;
//	}
//
//	public void setUserText17(String userText17) {
//		this.userText17 = userText17;
//	}
//
//	public String getUserText18() {
//		return userText18;
//	}
//
//	public void setUserText18(String userText18) {
//		this.userText18 = userText18;
//	}
//
//	public String getUserText19() {
//		return userText19;
//	}
//
//	public void setUserText19(String userText19) {
//		this.userText19 = userText19;
//	}
//
//	public String getUserText20() {
//		return userText20;
//	}
//
//	public void setUserText20(String userText20) {
//		this.userText20 = userText20;
//	}
//
//	public String getUserText21() {
//		return userText21;
//	}
//
//	public void setUserText21(String userText21) {
//		this.userText21 = userText21;
//	}
//
//	public String getUserText22() {
//		return userText22;
//	}
//
//	public void setUserText22(String userText22) {
//		this.userText22 = userText22;
//	}
//
//	public String getUserText23() {
//		return userText23;
//	}
//
//	public void setUserText23(String userText23) {
//		this.userText23 = userText23;
//	}
//
//	public String getUserText24() {
//		return userText24;
//	}
//
//	public void setUserText24(String userText24) {
//		this.userText24 = userText24;
//	}
//
//	public String getUserText25() {
//		return userText25;
//	}
//
//	public void setUserText25(String userText25) {
//		this.userText25 = userText25;
//	}
//
//	public String getUserText26() {
//		return userText26;
//	}
//
//	public void setUserText26(String userText26) {
//		this.userText26 = userText26;
//	}
//
//	public String getUserText27() {
//		return userText27;
//	}
//
//	public void setUserText27(String userText27) {
//		this.userText27 = userText27;
//	}
//
//	public String getUserText28() {
//		return userText28;
//	}
//
//	public void setUserText28(String userText28) {
//		this.userText28 = userText28;
//	}
//
//	public String getUserText29() {
//		return userText29;
//	}
//
//	public void setUserText29(String userText29) {
//		this.userText29 = userText29;
//	}
//
//	public String getUserText30() {
//		return userText30;
//	}
//
//	public void setUserText30(String userText30) {
//		this.userText30 = userText30;
//	}
//
//	public Double getVendorTaxAmt() {
//		return vendorTaxAmt;
//	}
//
//	public void setVendorTaxAmt(Double vendorTaxAmt) {
//		this.vendorTaxAmt = vendorTaxAmt;
//	}
//
//	public Long getStj6TaxrateId() {
//		return stj6TaxrateId;
//	}
//
//	public void setStj6TaxrateId(Long stj6TaxrateId) {
//		this.stj6TaxrateId = stj6TaxrateId;
//	}
//
//	public Long getStj7TaxrateId() {
//		return stj7TaxrateId;
//	}
//
//	public void setStj7TaxrateId(Long stj7TaxrateId) {
//		this.stj7TaxrateId = stj7TaxrateId;
//	}
//
//	public Long getStj8TaxrateId() {
//		return stj8TaxrateId;
//	}
//
//	public void setStj8TaxrateId(Long stj8TaxrateId) {
//		this.stj8TaxrateId = stj8TaxrateId;
//	}
//
//	public Long getStj9TaxrateId() {
//		return stj9TaxrateId;
//	}
//
//	public void setStj9TaxrateId(Long stj9TaxrateId) {
//		this.stj9TaxrateId = stj9TaxrateId;
//	}
//
//	public Long getStj10TaxrateId() {
//		return stj10TaxrateId;
//	}
//
//	public void setStj10TaxrateId(Long stj10TaxrateId) {
//		this.stj10TaxrateId = stj10TaxrateId;
//	}
//
//	public String getCountryCode() {
//		return countryCode;
//	}
//
//	public void setCountryCode(String countryCode) {
//		this.countryCode = countryCode;
//	}
//
//	public String getStateCode() {
//		return stateCode;
//	}
//
//	public void setStateCode(String stateCode) {
//		this.stateCode = stateCode;
//	}
//
//	public String getCountyName() {
//		return countyName;
//	}
//
//	public void setCountyName(String countyName) {
//		this.countyName = countyName;
//	}
//
//	public String getCityName() {
//		return cityName;
//	}
//
//	public void setCityName(String cityName) {
//		this.cityName = cityName;
//	}
//
//	public String getStj1Name() {
//		return stj1Name;
//	}
//
//	public void setStj1Name(String stj1Name) {
//		this.stj1Name = stj1Name;
//	}
//
//	public String getStj2Name() {
//		return stj2Name;
//	}
//
//	public void setStj2Name(String stj2Name) {
//		this.stj2Name = stj2Name;
//	}
//
//	public String getStj3Name() {
//		return stj3Name;
//	}
//
//	public void setStj3Name(String stj3Name) {
//		this.stj3Name = stj3Name;
//	}
//
//	public String getStj4Name() {
//		return stj4Name;
//	}
//
//	public void setStj4Name(String stj4Name) {
//		this.stj4Name = stj4Name;
//	}
//
//	public String getStj5Name() {
//		return stj5Name;
//	}
//
//	public void setStj5Name(String stj5Name) {
//		this.stj5Name = stj5Name;
//	}
//
//	public String getStj6Name() {
//		return stj6Name;
//	}
//
//	public void setStj6Name(String stj6Name) {
//		this.stj6Name = stj6Name;
//	}
//
//	public String getStj7Name() {
//		return stj7Name;
//	}
//
//	public void setStj7Name(String stj7Name) {
//		this.stj7Name = stj7Name;
//	}
//
//	public String getStj8Name() {
//		return stj8Name;
//	}
//
//	public void setStj8Name(String stj8Name) {
//		this.stj8Name = stj8Name;
//	}
//
//	public String getStj9Name() {
//		return stj9Name;
//	}
//
//	public void setStj9Name(String stj9Name) {
//		this.stj9Name = stj9Name;
//	}
//
//	public String getStj10Name() {
//		return stj10Name;
//	}
//
//	public void setStj10Name(String stj10Name) {
//		this.stj10Name = stj10Name;
//	}
//
//	public Double getCountryExemptAmt() {
//		return countryExemptAmt;
//	}
//
//	public void setCountryExemptAmt(Double countryExemptAmt) {
//		this.countryExemptAmt = countryExemptAmt;
//	}
//
//	public Double getStateExemptAmt() {
//		return stateExemptAmt;
//	}
//
//	public void setStateExemptAmt(Double stateExemptAmt) {
//		this.stateExemptAmt = stateExemptAmt;
//	}
//
//	public Double getCountyExemptAmt() {
//		return countyExemptAmt;
//	}
//
//	public void setCountyExemptAmt(Double countyExemptAmt) {
//		this.countyExemptAmt = countyExemptAmt;
//	}
//
//	public Double getCityExemptAmt() {
//		return cityExemptAmt;
//	}
//
//	public void setCityExemptAmt(Double cityExemptAmt) {
//		this.cityExemptAmt = cityExemptAmt;
//	}
//
//	public Double getStj1ExemptAmt() {
//		return stj1ExemptAmt;
//	}
//
//	public void setStj1ExemptAmt(Double stj1ExemptAmt) {
//		this.stj1ExemptAmt = stj1ExemptAmt;
//	}
//
//	public Double getStj2ExemptAmt() {
//		return stj2ExemptAmt;
//	}
//
//	public void setStj2ExemptAmt(Double stj2ExemptAmt) {
//		this.stj2ExemptAmt = stj2ExemptAmt;
//	}
//
//	public Double getStj3ExemptAmt() {
//		return stj3ExemptAmt;
//	}
//
//	public void setStj3ExemptAmt(Double stj3ExemptAmt) {
//		this.stj3ExemptAmt = stj3ExemptAmt;
//	}
//
//	public Double getStj4ExemptAmt() {
//		return stj4ExemptAmt;
//	}
//
//	public void setStj4ExemptAmt(Double stj4ExemptAmt) {
//		this.stj4ExemptAmt = stj4ExemptAmt;
//	}
//
//	public Double getStj5ExemptAmt() {
//		return stj5ExemptAmt;
//	}
//
//	public void setStj5ExemptAmt(Double stj5ExemptAmt) {
//		this.stj5ExemptAmt = stj5ExemptAmt;
//	}
//
//	public Double getStj6ExemptAmt() {
//		return stj6ExemptAmt;
//	}
//
//	public void setStj6ExemptAmt(Double stj6ExemptAmt) {
//		this.stj6ExemptAmt = stj6ExemptAmt;
//	}
//
//	public Double getStj7ExemptAmt() {
//		return stj7ExemptAmt;
//	}
//
//	public void setStj7ExemptAmt(Double stj7ExemptAmt) {
//		this.stj7ExemptAmt = stj7ExemptAmt;
//	}
//
//	public Double getStj8ExemptAmt() {
//		return stj8ExemptAmt;
//	}
//
//	public void setStj8ExemptAmt(Double stj8ExemptAmt) {
//		this.stj8ExemptAmt = stj8ExemptAmt;
//	}
//
//	public Double getStj9ExemptAmt() {
//		return stj9ExemptAmt;
//	}
//
//	public void setStj9ExemptAmt(Double stj9ExemptAmt) {
//		this.stj9ExemptAmt = stj9ExemptAmt;
//	}
//
//	public Double getStj10ExemptAmt() {
//		return stj10ExemptAmt;
//	}
//
//	public void setStj10ExemptAmt(Double stj10ExemptAmt) {
//		this.stj10ExemptAmt = stj10ExemptAmt;
//	}
//
//	public Double getCountryRate() {
//		return countryRate;
//	}
//
//	public void setCountryRate(Double countryRate) {
//		this.countryRate = countryRate;
//	}
//
//	public Double getStateRate() {
//		return stateRate;
//	}
//
//	public void setStateRate(Double stateRate) {
//		this.stateRate = stateRate;
//	}
//
//	public Double getStateTier2Rate() {
//		return stateTier2Rate;
//	}
//
//	public void setStateTier2Rate(Double stateTier2Rate) {
//		this.stateTier2Rate = stateTier2Rate;
//	}
//
//	public Double getStateTier3Rate() {
//		return stateTier3Rate;
//	}
//
//	public void setStateTier3Rate(Double stateTier3Rate) {
//		this.stateTier3Rate = stateTier3Rate;
//	}
//
//	public Double getStateTier1MaxAmount() {
//		return stateTier1MaxAmount;
//	}
//
//	public void setStateTier1MaxAmount(Double stateTier1MaxAmount) {
//		this.stateTier1MaxAmount = stateTier1MaxAmount;
//	}
//
//	public Double getStateTier2MinAmount() {
//		return stateTier2MinAmount;
//	}
//
//	public void setStateTier2MinAmount(Double stateTier2MinAmount) {
//		this.stateTier2MinAmount = stateTier2MinAmount;
//	}
//
//	public Double getStateTier2MaxAmount() {
//		return stateTier2MaxAmount;
//	}
//
//	public void setStateTier2MaxAmount(Double stateTier2MaxAmount) {
//		this.stateTier2MaxAmount = stateTier2MaxAmount;
//	}
//
//	public Double getStateMaxtaxAmount() {
//		return stateMaxtaxAmount;
//	}
//
//	public void setStateMaxtaxAmount(Double stateMaxtaxAmount) {
//		this.stateMaxtaxAmount = stateMaxtaxAmount;
//	}
//
//	public Double getCountyRate() {
//		return countyRate;
//	}
//
//	public void setCountyRate(Double countyRate) {
//		this.countyRate = countyRate;
//	}
//
//	public Double getCountySplitAmount() {
//		return countySplitAmount;
//	}
//
//	public void setCountySplitAmount(Double countySplitAmount) {
//		this.countySplitAmount = countySplitAmount;
//	}
//
//	public Double getCountyMaxtaxAmount() {
//		return countyMaxtaxAmount;
//	}
//
//	public void setCountyMaxtaxAmount(Double countyMaxtaxAmount) {
//		this.countyMaxtaxAmount = countyMaxtaxAmount;
//	}
//
//	public Double getCityRate() {
//		return cityRate;
//	}
//
//	public void setCityRate(Double cityRate) {
//		this.cityRate = cityRate;
//	}
//
//	public Double getCitySplitAmount() {
//		return citySplitAmount;
//	}
//
//	public void setCitySplitAmount(Double citySplitAmount) {
//		this.citySplitAmount = citySplitAmount;
//	}
//
//	public Double getCitySplitRate() {
//		return citySplitRate;
//	}
//
//	public void setCitySplitRate(Double citySplitRate) {
//		this.citySplitRate = citySplitRate;
//	}
//
//	public Double getStj1Rate() {
//		return stj1Rate;
//	}
//
//	public void setStj1Rate(Double stj1Rate) {
//		this.stj1Rate = stj1Rate;
//	}
//
//	public Double getStj2Rate() {
//		return stj2Rate;
//	}
//
//	public void setStj2Rate(Double stj2Rate) {
//		this.stj2Rate = stj2Rate;
//	}
//
//	public Double getStj3Rate() {
//		return stj3Rate;
//	}
//
//	public void setStj3Rate(Double stj3Rate) {
//		this.stj3Rate = stj3Rate;
//	}
//
//	public Double getStj4Rate() {
//		return stj4Rate;
//	}
//
//	public void setStj4Rate(Double stj4Rate) {
//		this.stj4Rate = stj4Rate;
//	}
//
//	public Double getStj5Rate() {
//		return stj5Rate;
//	}
//
//	public void setStj5Rate(Double stj5Rate) {
//		this.stj5Rate = stj5Rate;
//	}
//
//	public Double getStj6Rate() {
//		return stj6Rate;
//	}
//
//	public void setStj6Rate(Double stj6Rate) {
//		this.stj6Rate = stj6Rate;
//	}
//
//	public Double getStj7Rate() {
//		return stj7Rate;
//	}
//
//	public void setStj7Rate(Double stj7Rate) {
//		this.stj7Rate = stj7Rate;
//	}
//
//	public Double getStj8Rate() {
//		return stj8Rate;
//	}
//
//	public void setStj8Rate(Double stj8Rate) {
//		this.stj8Rate = stj8Rate;
//	}
//
//	public Double getStj9Rate() {
//		return stj9Rate;
//	}
//
//	public void setStj9Rate(Double stj9Rate) {
//		this.stj9Rate = stj9Rate;
//	}
//
//	public Double getStj10Rate() {
//		return stj10Rate;
//	}
//
//	public void setStj10Rate(Double stj10Rate) {
//		this.stj10Rate = stj10Rate;
//	}
//
//	public Double getCombinedRate() {
//		return combinedRate;
//	}
//
//	public void setCombinedRate(Double combinedRate) {
//		this.combinedRate = combinedRate;
//	}
//
//	public String getCountryTaxtypeCode() {
//		return countryTaxtypeCode;
//	}
//
//	public void setCountryTaxtypeCode(String countryTaxtypeCode) {
//		this.countryTaxtypeCode = countryTaxtypeCode;
//	}
//
//	public String getStateTaxtypeCode() {
//		return stateTaxtypeCode;
//	}
//
//	public void setStateTaxtypeCode(String stateTaxtypeCode) {
//		this.stateTaxtypeCode = stateTaxtypeCode;
//	}
//
//	public String getCountyTaxtypeCode() {
//		return countyTaxtypeCode;
//	}
//
//	public void setCountyTaxtypeCode(String countyTaxtypeCode) {
//		this.countyTaxtypeCode = countyTaxtypeCode;
//	}
//
//	public String getCityTaxtypeCode() {
//		return cityTaxtypeCode;
//	}
//
//	public void setCityTaxtypeCode(String cityTaxtypeCode) {
//		this.cityTaxtypeCode = cityTaxtypeCode;
//	}
//
//	public String getStj1TaxtypeCode() {
//		return stj1TaxtypeCode;
//	}
//
//	public void setStj1TaxtypeCode(String stj1TaxtypeCode) {
//		this.stj1TaxtypeCode = stj1TaxtypeCode;
//	}
//
//	public String getStj2TaxtypeCode() {
//		return stj2TaxtypeCode;
//	}
//
//	public void setStj2TaxtypeCode(String stj2TaxtypeCode) {
//		this.stj2TaxtypeCode = stj2TaxtypeCode;
//	}
//
//	public String getStj3TaxtypeCode() {
//		return stj3TaxtypeCode;
//	}
//
//	public void setStj3TaxtypeCode(String stj3TaxtypeCode) {
//		this.stj3TaxtypeCode = stj3TaxtypeCode;
//	}
//
//	public String getStj4TaxtypeCode() {
//		return stj4TaxtypeCode;
//	}
//
//	public void setStj4TaxtypeCode(String stj4TaxtypeCode) {
//		this.stj4TaxtypeCode = stj4TaxtypeCode;
//	}
//
//	public String getStj5TaxtypeCode() {
//		return stj5TaxtypeCode;
//	}
//
//	public void setStj5TaxtypeCode(String stj5TaxtypeCode) {
//		this.stj5TaxtypeCode = stj5TaxtypeCode;
//	}
//
//	public String getStj6TaxtypeCode() {
//		return stj6TaxtypeCode;
//	}
//
//	public void setStj6TaxtypeCode(String stj6TaxtypeCode) {
//		this.stj6TaxtypeCode = stj6TaxtypeCode;
//	}
//
//	public String getStj7TaxtypeCode() {
//		return stj7TaxtypeCode;
//	}
//
//	public void setStj7TaxtypeCode(String stj7TaxtypeCode) {
//		this.stj7TaxtypeCode = stj7TaxtypeCode;
//	}
//
//	public String getStj8TaxtypeCode() {
//		return stj8TaxtypeCode;
//	}
//
//	public void setStj8TaxtypeCode(String stj8TaxtypeCode) {
//		this.stj8TaxtypeCode = stj8TaxtypeCode;
//	}
//
//	public String getStj9TaxtypeCode() {
//		return stj9TaxtypeCode;
//	}
//
//	public void setStj9TaxtypeCode(String stj9TaxtypeCode) {
//		this.stj9TaxtypeCode = stj9TaxtypeCode;
//	}
//
//	public String getStj10TaxtypeCode() {
//		return stj10TaxtypeCode;
//	}
//
//	public void setStj10TaxtypeCode(String stj10TaxtypeCode) {
//		this.stj10TaxtypeCode = stj10TaxtypeCode;
//	}
//
//	public Double getInvoiceTaxPaidAmt() {
//		return invoiceTaxPaidAmt;
//	}
//
//	public void setInvoiceTaxPaidAmt(Double invoiceTaxPaidAmt) {
//		this.invoiceTaxPaidAmt = invoiceTaxPaidAmt;
//	}
//
//	public Double getInvoiceDiffAmt() {
//		return invoiceDiffAmt;
//	}
//
//	public void setInvoiceDiffAmt(Double invoiceDiffAmt) {
//		this.invoiceDiffAmt = invoiceDiffAmt;
//	}
//
//	public Double getInvoiceLineTaxPaidAmt() {
//		return invoiceLineTaxPaidAmt;
//	}
//
//	public void setInvoiceLineTaxPaidAmt(Double invoiceLineTaxPaidAmt) {
//		this.invoiceLineTaxPaidAmt = invoiceLineTaxPaidAmt;
//	}
//
//	public Double getInvoiceLineDiffAmt() {
//		return invoiceLineDiffAmt;
//	}
//
//	public void setInvoiceLineDiffAmt(Double invoiceLineDiffAmt) {
//		this.invoiceLineDiffAmt = invoiceLineDiffAmt;
//	}
//
//	public Long getInvoiceDiffBatchNo() {
//		return invoiceDiffBatchNo;
//	}
//
//	public void setInvoiceDiffBatchNo(Long invoiceDiffBatchNo) {
//		this.invoiceDiffBatchNo = invoiceDiffBatchNo;
//	}
//
//	public Double getInvoiceTaxAmt() {
//		return invoiceTaxAmt;
//	}
//
//	public void setInvoiceTaxAmt(Double invoiceTaxAmt) {
//		this.invoiceTaxAmt = invoiceTaxAmt;
//	}
//
//	public Double getInvoiceFreightableAmt() {
//		return invoiceFreightableAmt;
//	}
//
//	public void setInvoiceFreightableAmt(Double invoiceFreightableAmt) {
//		this.invoiceFreightableAmt = invoiceFreightableAmt;
//	}
//
//	public Double getInvoiceMiscableAmt() {
//		return invoiceMiscableAmt;
//	}
//
//	public void setInvoiceMiscableAmt(Double invoiceMiscableAmt) {
//		this.invoiceMiscableAmt = invoiceMiscableAmt;
//	}
//
//	public String getInvoiceProfrtFlag() {
//		return invoiceProfrtFlag;
//	}
//
//	public void setInvoiceProfrtFlag(String invoiceProfrtFlag) {
//		this.invoiceProfrtFlag = invoiceProfrtFlag;
//	}
//
//	public String getInvoicePromiscFlag() {
//		return invoicePromiscFlag;
//	}
//
//	public void setInvoicePromiscFlag(String invoicePromiscFlag) {
//		this.invoicePromiscFlag = invoicePromiscFlag;
//	}
//
//	public Double getInvoiceMiscAmt() {
//		return invoiceMiscAmt;
//	}
//
//	public void setInvoiceMiscAmt(Double invoiceMiscAmt) {
//		this.invoiceMiscAmt = invoiceMiscAmt;
//	}
//
//	public Double getInvoiceLineMiscAmt() {
//		return invoiceLineMiscAmt;
//	}
//
//	public void setInvoiceLineMiscAmt(Double invoiceLineMiscAmt) {
//		this.invoiceLineMiscAmt = invoiceLineMiscAmt;
//	}
//
//	// Idable interface
//	public Long getId() {
//		return getScenarioId();
//	}
//
//    public void setId(Long id) {
//    	this.setScenarioId(id);
//    }
//    
//    public String getIdPropertyName() {
//    	return "scenarioId";
//    }
//
//	public Long getScenarioId() {
//		return scenarioId;
//	}
//
//	public void setScenarioId(Long scenarioId) {
//		this.scenarioId = scenarioId;
//	}
//
//	public String getScenarioName() {
//		return scenarioName;
//	}
//
//	public void setScenarioName(String scenarioName) {
//		this.scenarioName = scenarioName;
//	}
//
//	public String getReprocessCode() {
//		return reprocessCode;
//	}
//
//	public void setReprocessCode(String reprocessCode) {
//		this.reprocessCode = reprocessCode;
//	}
//}
