echo off
REM Ex: PinPointCertificate.bat 127.0.0.1


if "%OS%" == "Windows_NT" setlocal


set CURRENT_DIR=%cd%
if not "%CATALINA_HOME%" == "" goto gotHome

set CATALINA_HOME=%cd%
if exist "%CATALINA_HOME%\bin\tomcat6.exe" goto okHome
rem CD to the upper dir
cd ..
set CATALINA_HOME=%cd%

:gotHome
if exist "%CATALINA_HOME%\bin\tomcat6.exe" goto okHome
echo The tomcat.exe was not found...
echo The CATALINA_HOME environment variable is not defined correctly.
echo This environment variable is needed to run this program
goto end


rem Make sure prerequisite environment variables are set
if not "%JAVA_HOME%" == "" goto okHome
echo The JAVA_HOME environment variable is not defined
echo This environment variable is needed to run this program
goto end 

:okHome
if not "%CATALINA_BASE%" == "" goto gotBase
set CATALINA_BASE=%CATALINA_HOME%
:gotBase
 




REM if "%1" == "" goto doNotFound
REM set HOST_NAME=%1

ECHO Host name: %computername%
set HOST_NAME=%computername%


:doInstall

rem
rem PinPoint setting
rem 
set JAVA_HOME=%CATALINA_HOME%\jdk1.6.0_12

echo JAVA_HOME = %JAVA_HOME%

set EXECUTABLE=%JAVA_HOME%\bin\keytool.exe

cd %CATALINA_HOME%\conf\ryanco\

Del stscorp.keystore

"%EXECUTABLE%" -genkeypair -dname "cn=%HOST_NAME%, o=Second Decimal, c=US" -alias stskey -keypass stscorp -keystore stscorp.keystore -storepass stscorp -validity 7300
echo Certificate created
goto end

:doNotFound
rem Host name not found
echo Unknown Host Name
goto end



:end
cd %CURRENT_DIR%
