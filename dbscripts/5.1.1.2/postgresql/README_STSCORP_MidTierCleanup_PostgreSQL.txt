
:: Connect to the postdb1 database as stscorp

	CMD> psql "postdb1" stscorp

:: Run the 5.1.1.2 (MidTier cleanup) database patches:

	postdb1=> \i ./code/triggers/disableTriggers.sql

	postdb1=> \i ./object/LPP-MidTier-2.3_SchemaMods.sql

	postdb1=> \i ./code/procedures/createFromMidTierStoredProcs.sql

	postdb1=> \i ./code/procedures/createProcedureSP_GEN_LOCATION_DRIVER.sql
	postdb1=> \i ./code/procedures/createProcedureSP_GEN_TAX_DRIVER.sql

	postdb1=> \i ./code/triggers/createTriggersAsProcs.sql
	postdb1=> \i ./code/triggers/createProcedureSP_TB_LOCATION_MATRIX_A_I.sql
	postdb1=> \i ./code/triggers/createProcedureSP_TB_TAX_MATRIX_A_I.sql
	postdb1=> \i ./code/triggers/createProcedureSP_GL_LOGGING.sql
	postdb1=> \i ./code/triggers/createProcedureSP_TB_TRANSACTION_DETAIL_B_U.sql

	postdb1=> \i ./code/misc/createProcedureSP_MAX_TB_BCP_JURIS_TAXRATE_ID.sql
	postdb1=> \i ./code/misc/createProcedureSP_MAX_ALL.sql
	postdb1=> \i ./code/misc/createViews.sql

	postdb1=> \i ./code/procedures/createProcedureSP_BATCH_PROCESS.sql

	postdb1=> \i ./code/procedures/createProcedureSP_TRUNCATE_BCP_TABLES.sql
	postdb1=> \i ./code/procedures/createProcedureSP_BATCH_TAXRATE_UPDATE.sql

	postdb1=> \i ./code/procedures/createProcedureSP_DB_STATISTICS.sql
