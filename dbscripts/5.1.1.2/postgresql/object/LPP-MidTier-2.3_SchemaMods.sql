

/*****************
 ** Assumptions **
 *****************
 
 -- 1) References to 2.3.<TableName> are as noted from section 2.3 in "Ryan MidTier Technical Design (11-14 Final).doc"

 *****************/

----------------------------------
-- Drop Foreign Key Constraints --
----------------------------------


-- Create the function
CREATE FUNCTION drop_stscorp_fk_constraints() RETURNS VOID
AS
$$
DECLARE
	x RECORD;
BEGIN
	FOR x IN (
		SELECT	class.relname	AS "table_name",
			cons.conname	AS "constraint_name"
		FROM	pg_user		u,
			pg_class	class,
			pg_constraint	cons
		WHERE	u.usename	= 'stscorp'
		AND	u.usesysid	= class.relowner
		AND	class.relkind	= 'r' -- Table type
		AND	class.oid	= cons.conrelid
		AND	cons.contype	= 'f' -- Foreign Key
		ORDER BY class.relname
	) LOOP
		RAISE NOTICE '%, %', x.table_name, x.constraint_name;
		EXECUTE 'ALTER TABLE '||x.table_name||' DROP CONSTRAINT '||x.constraint_name;
	END LOOP;
END;
$$
LANGUAGE plpgsql;
-- Run the function
SELECT drop_stscorp_fk_constraints();
-- Drop the function
DROP FUNCTION drop_stscorp_fk_constraints();

--------------------
-- Modify Objects --
--------------------

-- 2.3.MESSAGES 
--
-- Table to be dropped
--
DROP TABLE messages;

-- 2.3.TB_ALLOCATION_MATRIX 
--
--  Default values to be set for date columns
ALTER TABLE tb_allocation_matrix ALTER COLUMN effective_date  SET DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE tb_allocation_matrix ALTER COLUMN expiration_date SET DEFAULT TO_TIMESTAMP('12/31/9999','mm/dd/yyyy');

-- 2.3.TB_ALLOCATION_MATRIX_DETAIL
--
-- Primary key ALREADY EXISTS in schema [ALLOCATION_MATRIX_ID, JURISDICTION_ID].
--
-- When allocation is deleted, should be deleted from detail table as well.
-- Implement cascade delete for allocation_matrix_id in DB:
ALTER TABLE tb_allocation_matrix_detail ADD 
   CONSTRAINT tb_allocation_matrix_dtl_fk01 FOREIGN KEY (allocation_matrix_id)
   REFERENCES tb_allocation_matrix (allocation_matrix_id)
   ON DELETE CASCADE ;
-- Implement FK constraint WITHOUT cascade delete in DB for jusrisdiction_id.
ALTER TABLE tb_allocation_matrix_detail ADD 
   CONSTRAINT tb_allocation_matrix_dtl_fk02 FOREIGN KEY (jurisdiction_id)
   REFERENCES tb_jurisdiction (jurisdiction_id) ;

-- 2.3.ANSWER
-- 
-- Table to be dropped
-- 
DROP TABLE tb_answer;

-- 2.3.TB_ARCHIVE_TRANSACTION_DETAIL
--
-- Table to be dropped
-- 
CREATE FUNCTION drop_table_tb_arc_trans_dtl() RETURNS VOID
AS
$$
BEGIN
 EXECUTE IMMEDIATE 'DROP TABLE tb_archive_transaction_detail';
EXCEPTION WHEN OTHERS THEN
 NULL;
END;
$$
LANGUAGE plpgsql;
SELECT drop_table_tb_arc_trans_dtl();
DROP FUNCTION drop_table_tb_arc_trans_dtl();

-- 2.3.BATCH
--
-- Primary key in ALREADY EXISTS in schema [BATCH_ID]
-- 
-- Implement FK constraint WITHOUT cascade delete for tb_batch_metadata (batch_type_code) in DB. (NOTE: skipping index creation due to lack of cascade delete)
ALTER TABLE tb_batch ADD
   CONSTRAINT tb_batch_fk01 FOREIGN KEY (batch_type_code)
   REFERENCES tb_batch_metadata (batch_type_code);

-- 2.3.TB_BATCH_ERROR_LOG
-- 
-- Primary key in ALREADY EXISTS in schema [BATCH_ERROR_LOG_ID]
--
-- Column trans_dtl_column_name data type size change to varchar2(40).
-- Column trans_dtl_datatype    data type size change to varchar2(10).
ALTER TABLE tb_batch_error_log ALTER COLUMN trans_dtl_column_name TYPE varchar(40);
ALTER TABLE tb_batch_error_log ALTER COLUMN trans_dtl_datatype     TYPE varchar(10);

-- 2.3.TB_BATCH_METADATA
--
-- Implement mid tier FK constraint for batch_type_code = tb_list_code (code_code) where code_type_code = 'BATCHTYPE'.
--
-- NO DB CHANGES TO BE IMPLEMENTED 
-- 

-- 2.3.TB_BCP_CCH_CODE
-- 
-- Table to be dropped
-- 
DROP TABLE tb_bcp_cch_code;

-- 2.3.TB_BCP_CCH_TXMATRIX
-- 
-- Table to be dropped
-- 
DROP TABLE tb_bcp_cch_txmatrix;

-- 2.3.TB_BCP_JURISDICTION_TAXRATE
--
-- Add sequence for primary key.
CREATE SEQUENCE stscorp.sq_tb_bcp_juris_taxrate_id 
	START WITH 1 
	INCREMENT BY 1 
	NO MAXVALUE 
	MINVALUE 1 
	NO CYCLE;

-- Add bcp_jurisdiction_taxrate_id column, populate with sequence values, create Primary Key
ALTER TABLE tb_bcp_jurisdiction_taxrate ADD
 bcp_jurisdiction_taxrate_id NUMERIC;

UPDATE tb_bcp_jurisdiction_taxrate SET 
 bcp_jurisdiction_taxrate_id = nextval('sq_tb_bcp_juris_taxrate_id');

ALTER TABLE tb_bcp_jurisdiction_taxrate ADD CONSTRAINT
 pk_tb_bcp_jurisdiction_taxrate PRIMARY KEY (bcp_jurisdiction_taxrate_id);
-- Add sp_max for new sequence. (see createProcedureSP_MAX_TB_BCP_JURIS_TAXRATE_ID.sql)
-- Recreate sp_max_all to add new call to: sp_max_tb_bcp_juris_taxrate_id(). (createProcedureSP_MAX_ALL.sql)

-- 2.3.TB_BCP_JURI_TAXRATE_TEXT
-- 
-- Primary key in domain class.
-- 
-- NO DB CHANGES TO BE IMPLEMENTED 
-- 

-- 2.3.TB_BCP_TRANSACTIONS
-- Primary key in DB and domain class.
-- This table is mapped to BCPTransactionDetail.java.	
-- PK did NOT EXIST in Database, add column, sequence & PK constraint
-- Add sequence for primary key.
CREATE SEQUENCE stscorp.sq_tb_bcp_transactions 
	START WITH 1 
	INCREMENT BY 1 
	NO MAXVALUE 
	MINVALUE 1 
	NO CYCLE;

-- Add bcp_transaction_id column, populate with sequence values, create Primary Key
ALTER TABLE tb_bcp_transactions ADD
 bcp_transaction_id NUMERIC;

UPDATE tb_bcp_transactions SET 
 bcp_transaction_id = nextval('sq_tb_bcp_transactions');


ALTER TABLE tb_bcp_transactions ADD CONSTRAINT
 pk_tb_bcp_transactions PRIMARY KEY (bcp_transaction_id);

-- 2.3.TB_CCH_CODE
-- 
-- This table is used by 3 items; backingbean, Cachemanager and mapping in the domain class CCHCode.java and it has PK in domain class and pk in db.
--
-- Primary key ALREADY EXISTS in schema [FILENAME, FIELDNAME, CODE].
-- 
-- NO DB CHANGES TO BE IMPLEMENTED
-- 

-- 2.3.TB_DATA_DEF_COLUMN	
--
-- In the case of delete when a particular table name is deleted from the tb_data_def_table it�s also removed from this table. This is in DB. 
-- NOTE: skipping index creation due to PK already indexing TABLE_NAME column
ALTER TABLE tb_data_def_column ADD 
   CONSTRAINT tb_data_def_column_fk01 FOREIGN KEY (table_name)
   REFERENCES tb_data_def_table (table_name)
	ON DELETE CASCADE ;
-- Primary key ALREADY EXISTS in schema [TABLE_NAME, COLUMN_NAME].
-- 

-- 2.3.TB_DATA_DEF_TABLE	
--
-- Primary key ALREADY EXISTS in schema [TABLE_NAME].
-- 
-- NO DB CHANGES TO BE IMPLEMENTED
--

-- 2.3.TB_DB_STATISTICS
-- 
-- Primary key ALREADY EXISTS in schema [DB_STATISTICS_ID].
-- 
-- NO DB CHANGES TO BE IMPLEMENTED
--

-- 2.3.TB_DRIVER_NAMES	
--
-- Primary key ALREADY EXISTS in schema [DRIVER_NAMES_CODE, DRIVER_ID].
-- 
-- Implement mid tier constraint on trans_dtl_column_name = tb_date_def_column (column_name) where table_name = 'TB_TRANSACTION_DETAIL'.
--
-- Delete the following columns; vendor_flag, tax_paid_to_vendor_flag, estimator_driver_flag, estimator_header_index.
ALTER TABLE tb_driver_names DROP COLUMN vendor_flag;
ALTER TABLE tb_driver_names DROP COLUMN tax_paid_to_vendor_flag;
ALTER TABLE tb_driver_names DROP COLUMN estimator_driver_flag;
ALTER TABLE tb_driver_names DROP COLUMN estimator_header_index;

-- 2.3.TB_DRIVER_REFERENCE
-- 
-- Primary key ALREADY EXISTS in schema [TRANS_DTL_COLUMN_NAME, DRIVER_VALUE].
-- 
-- Implement mid tier constraint on trans_detail_column_name = tb_data_def_column (column_name) where table_name = 'TB_TRANSACTION_DETAIL'.
-- 
-- NO DB CHANGES TO BE IMPLEMENTED
-- 

-- 2.3.TB_DW_COLUMN
-- 
-- Table to be removed
DROP TABLE tb_dw_column;

-- 2.3.TB_DW_SYNTAX
-- 
-- Table to be removed
DROP TABLE tb_dw_syntax;

-- 2.3.TB_ENTITY
-- 
-- Primary key ALREADY EXISTS in schema [ENTITY_ID].
-- 
-- Implement no cascade delete constraint on entity_level_id = tb_entity_level (entity_level_id).
/*  Entities  *************************************************************************************/
ALTER TABLE tb_entity ADD 
   CONSTRAINT tb_entity_fk01 FOREIGN KEY (entity_level_id)
   REFERENCES tb_entity_level (entity_level_id) ;

-- 2.3.TB_ENTITY_LEVEL
--
-- Primary key ALREADY EXISTS in schema [ENTITY_LEVEL_ID].
-- 
-- Implement mid tier constraint on trans_dtl_column_name = tb_date_def_column (column_name) where table_name = 'TB_TRANSACTION_DETAIL'.
--
-- NO DB CHANGES TO BE IMPLEMENTED 
--

-- 2.3.TB_GL_EXTRACT_MAP
--
-- Primary key ALREADY EXISTS in schema [GL_EXTRACT_MAP_ID].
-- 
-- Implement no cascade delete constraints in DB to be implemented for the following columns; taxcode_state_code, taxcode_type_code, taxcode_code.
-- Originally failed in DB2

ALTER TABLE tb_gl_extract_map ADD 
   CONSTRAINT tb_gl_extract_map_fk01 FOREIGN KEY (taxcode_state_code)
   REFERENCES tb_taxcode_state (taxcode_state_code) 
;

-- Originally failed in DB2
ALTER TABLE tb_gl_extract_map ADD 
   CONSTRAINT tb_gl_extract_map_fk02 FOREIGN KEY (taxcode_type_code, taxcode_code)
   REFERENCES tb_taxcode (taxcode_type_code, taxcode_code) 
;
-- Implement mid tier constraint on tax_juris_type_code = tb_list_code (code_code) where  code_type_code = 'JURISTYPE'.
--

-- 2.3.TB_IMPORT_DEFINITION
-- 
-- Primary key ALREADY EXISTS in schema [IMPORT_DEFINITION_CODE].
-- 
-- Implement mid tier constraint on import_file_type_code = tb_list_code (code_code) where code_type_code = 'IMPFILETYP'.
--
-- Delete the following columns; import_loader, flat_fixed_record_length, db_table_name, xml_dom_name, stored_proc_name.
ALTER TABLE tb_import_definition DROP COLUMN import_loader;
ALTER TABLE tb_import_definition DROP COLUMN flat_fixed_record_length;
ALTER TABLE tb_import_definition DROP COLUMN db_table_name;
ALTER TABLE tb_import_definition DROP COLUMN xml_dom_name;
ALTER TABLE tb_import_definition DROP COLUMN stored_proc_name;
--  Change column sample_file_name to varchar2(4000).
ALTER TABLE tb_import_definition ALTER COLUMN sample_file_name TYPE VARCHAR(4000);

-- 2.3.TB_IMPORT_DEFINITION_DETAIL
--
-- Primary key ALREADY EXISTS in schema [IMPORT_DEFINITION_CODE, TRANS_DTL_COLUMN_NAME].
-- Implement DB constraint cascade delete constraint on import_definition_code.
ALTER TABLE tb_import_definition_detail ADD
   CONSTRAINT tb_import_defintion_dtl_fk01 FOREIGN KEY (import_definition_code)
   REFERENCES tb_import_definition (import_definition_code)
   ON DELETE CASCADE ;
-- Implement mid tier constraint on trans_dtl_column_name  = tb_date_def_column (column_name) where table_name = 'TB_TRANSACTION_DETAIL'.
-- Delete the following columns; import_column_start, import_column_length, import_column_decimals.
ALTER TABLE tb_import_definition_detail DROP COLUMN import_column_start;
ALTER TABLE tb_import_definition_detail DROP COLUMN import_column_length;
ALTER TABLE tb_import_definition_detail DROP COLUMN import_column_decimals;

-- 2.3.TB_IMPORT_SPEC	
--
-- Primary key ALREADY EXISTS in schema [IMPORT_SPEC_TYPE, IMPORT_SPEC_CODE].
-- Implement mid tier constraint on import_spec_type  = tb_list_code (code_code) where code_type_code = 'BATCHTYPE'
-- Implement no cascade delete DB constraint on import_definition_code.
ALTER TABLE tb_import_spec ADD
   CONSTRAINT fk_tb_import_spec_1 FOREIGN KEY (import_definition_code)
   REFERENCES tb_import_definition (import_definition_code) ;
-- Delete the following columns; submit_job_flag.
ALTER TABLE tb_import_spec DROP COLUMN submit_job_flag;

-- 2.3.TB_IMPORT_SPEC_USER
--
-- Primary key ALREADY EXISTS in schema [IMPORT_SPEC_TYPE, IMPORT_SPEC_CODE, USER_CODE].
-- Implement cascade delete DB constraint on import_spec_type, import_spec_code, user_code.
ALTER TABLE tb_import_spec_user ADD
   CONSTRAINT tb_import_spec_user_fk01 FOREIGN KEY (import_spec_type, import_spec_code)
   REFERENCES tb_import_spec (import_spec_type, import_spec_code)
   ON DELETE CASCADE ;
ALTER TABLE tb_import_spec_user ADD
   CONSTRAINT tb_import_spec_user_fk02 FOREIGN KEY (user_code)
   REFERENCES tb_user (user_code)
   ON DELETE CASCADE ;

-- 2.3.TB_JURISDICTION
-- 
-- Primary key ALREADY EXISTS in schema [JURISDICTION_ID].
-- Implement no cascade delete DB constraint on state.
ALTER TABLE tb_jurisdiction ADD 
   CONSTRAINT tb_jurisdiction_fk01 FOREIGN KEY (state)
   REFERENCES tb_taxcode_state (taxcode_state_code) ;

-- 2.3.TB_JURISDICTION_TAXRATE
-- 
-- Index on jurisdiction_id ALREADY EXISTS in schema [IDX_TB_JUR_TAXRATE_JUR_ID].
-- Implement cascade delete DB constraint on jurisdiction_id
ALTER TABLE tb_jurisdiction_taxrate ADD
   CONSTRAINT tb_jurisdiction_taxrate_fk01 FOREIGN KEY (jurisdiction_id)
   REFERENCES tb_jurisdiction (jurisdiction_id)
   ON DELETE CASCADE ;
-- Implement mid tier constraint on measure_type_code = tb_list_code (code_code) where code_type_code = 'MEASURETYP'.
-- Modify column effective_date to DEFAULT SYSDATE, column expiration_date to DEFAULT TO_DATE('12/31/9999','mm/dd/yyyy')
ALTER TABLE tb_jurisdiction_taxrate ALTER COLUMN effective_date  SET DEFAULT current_timestamp;
ALTER TABLE tb_jurisdiction_taxrate ALTER COLUMN expiration_date SET DEFAULT TO_TIMESTAMP('12/31/9999','mm/dd/yyyy') ;

-- 2.3.TB_LIST_CODE
-- 
-- Primary key is defined in the domain class ListCodes.java.
--
-- Primary key ALREADY EXISTS in schema [CODE_TYPE_CODE, CODE_CODE].
-- 

-- 2.3.TB_LOCATION_MATRIX	
--
-- Primary key is defined in the domain class LocationMatrix.
-- Primary key ALREADY EXISTS in schema [LOCATION_MATRIX_ID].
-- For columns driver01 to driver10, driver01 = tb_driver_reference (driver_value) where trans_dtl_column_name = '{driver_trans_dtl_column_name}'.  Default '*ALL'
-- Implement no cascade delete DB constraint on jurisdiction_id.
ALTER TABLE tb_location_matrix ADD
   CONSTRAINT tb_location_matrix_fk01 FOREIGN KEY (jurisdiction_id)
   REFERENCES tb_jurisdiction (jurisdiction_id) ;
-- Implement mid tier constraint on override_taxtype_code = tb_list_code (code_code) where code_type_code = 'TAXTYPE'.
-- The default values for effective_date and expiration_date are specified in the initializeDefaults() method.
-- For override_taxcode_type a default value '*NO' is being set which is not present in the tb_list_code table.
-- Modify column effective_date to DEFAULT SYSDATE, column expiration_date to DEFAULT TO_DATE(�12/31/9999�,�mm/dd/yyyy�)
ALTER TABLE tb_location_matrix ALTER COLUMN effective_date  SET DEFAULT current_timestamp;
ALTER TABLE tb_location_matrix ALTER COLUMN expiration_date SET DEFAULT TO_TIMESTAMP('12/31/9999','mm/dd/yyyy');

-- 2.3.TB_MAIN_MENU	
--
-- Domain object needs to be created with primary key.
-- Primary key ALREADY EXISTS in schema [MENU_CODE].

-- 2.3.TB_MENU	
-- Primary key is defined in the domain class Menu.java. Constraints need to be added.
-- Primary key ALREADY EXISTS in schema [MENU_CODE].
-- Implement no cascade delete DB constraint on main_menu_code.
ALTER TABLE tb_menu ADD
   CONSTRAINT tb_menu_fk01 FOREIGN KEY (main_menu_code)
   REFERENCES tb_main_menu (menu_code) ;

-- 2.3.TB_OPTION	
-- Primary key is defined in the domain class Option.java. Constraints need to be added.
-- Primary key ALREADY EXISTS in schema [OPTION_CODE, OPTION_TYPE_CODE, USER_CODE].

-- 2.3.TB_PAGE_ATTRIBUTES
-- Used in class JPAUtilitiesMenuDAO.java line number 74.
-- Correct column upadte_timestamp to update_timestamp.
ALTER TABLE tb_page_attributes RENAME COLUMN upadte_timestamp TO update_timestamp;

ALTER TABLE tb_page_attributes ALTER COLUMN update_user_id TYPE varchar(40);
ALTER TABLE tb_page_attributes ALTER COLUMN update_user_id SET DEFAULT current_user;

ALTER TABLE tb_page_attributes ALTER COLUMN update_timestamp SET DEFAULT current_timestamp;


-- 2.3.TB_QUESTIONANSWER_LOG	
-- Table to be removed
DROP TABLE tb_questionanswer_log;

-- 2.3.TB_QUESTION_DETAIL
-- Table to be removed
DROP TABLE tb_question_detail;

-- 2.3.TB_REFERENCE_DETAIL
-- Primary key and constraints are defined in the domain class ReferenceDetail.java.ReferenceDetailPK.java line numbers 17 and 21.
-- Primary key ALREADY EXISTS in schema [TAXCODE_DETAIL_ID,REFERENCE_DOCUMENT_CODE].
-- Implement cascade delete DB constraint on taxcode_detail_id and reference_document_code.
ALTER TABLE tb_reference_detail ADD
   CONSTRAINT tb_reference_detail_fk01 FOREIGN KEY (taxcode_detail_id)
   REFERENCES tb_taxcode_detail (taxcode_detail_id)
   ON DELETE CASCADE ;
	   -- FAILED INITIALLY, CHANGED REFERENCES COLUMN FROM reference_detail_id, TO taxcode_detail_id

ALTER TABLE tb_reference_detail ADD
   CONSTRAINT tb_reference_detail_fk02 FOREIGN KEY (reference_document_code)
   REFERENCES tb_reference_document (reference_document_code)
   ON DELETE CASCADE;

-- 2.3.TB_REFERENCE_DOCUMENT	
-- Primary key is defined in the domain class ReferenceDocument.java. Constraints need to be added.
-- Primary key ALREADY EXISTS in schema [REFERENCE_DOCUMENT_CODE].
-- Implement mid tier constraint on reference_type_code = tb_list_code (code_code) where code_type_code = 'REFTYPE'.
--
-- NO DB CHANGES TO BE IMPLEMENTED

-- 2.3.TB_RESTORE_TRANSACTION_DETAIL
-- Table to be removed
CREATE FUNCTION drop_table_tb_rest_trans_dtl() RETURNS VOID
AS
$$
BEGIN
 EXECUTE IMMEDIATE 'DROP TABLE tb_restore_transaction_detail';
EXCEPTION WHEN OTHERS THEN
 NULL;
END;
$$
LANGUAGE plpgsql;
SELECT drop_table_tb_rest_trans_dtl();
DROP FUNCTION drop_table_tb_rest_trans_dtl();

-- 2.3.TB_ROLE	
-- Primary key is defined in the domain class Role.java.
-- Primary key ALREADY EXISTS in schema [ROLE_CODE].

-- 2.3.TB_ROLE_MENU
-- Primary key is defined in the domain class RoleMenu.java. Constraints need to be added.
-- Primary key ALREADY EXISTS in schema [ROLE_CODE, MENU_CODE].
-- Implement cascade delete DB constraint on role_code and menu_code.
ALTER TABLE tb_role_menu ADD
   CONSTRAINT tb_role_menu_fk01 FOREIGN KEY (role_code)
   REFERENCES tb_role (role_code)
   ON DELETE CASCADE ;

-- Originally failed in DB2
ALTER TABLE tb_role_menu ADD
   CONSTRAINT tb_role_menu_fk02 FOREIGN KEY (menu_code)
   REFERENCES tb_menu (menu_code)
   ON DELETE CASCADE ;

-- 2.3.TB_SQL	
-- Table to be removed
DROP TABLE tb_sql;

-- 2.3.TB_TAXCODE
-- Primary key is defined in the domain class TaxCode.java. Constraints need to be added.
-- Primary key ALREADY EXISTS in schema [TAXCODE_CODE, TAXCODE_TYPE_CODE].
-- Implement mid tier constraint on taxcode_type_code = tb_list_code (code_code) where code_type_code = 'TCTYPE'.
-- Implement mid tier constraint on taxtype_code = tb_list_code (code_code) where code_type_code = 'TAXTYPE'.
-- Implement mid tier constraint on measure_type_code = tb_list_code (code_code) where code_type_code = 'MEASURETYP'.
-- NO DB CHANGES TO BE IMPLEMENTED

-- 2.3.TB_TAXCODE_DETAIL
-- Primary key is defined in the domain class TaxCodeDetail.java. Constraints need to be added.
-- Primary key ALREADY EXISTS in schema [TAXCODE_DETAIL_ID].
-- Implement no cascade delete DB constraint on taxcode_state_code, taxcode_type_code, taxcode_code, jurisdiction_id.
ALTER TABLE tb_taxcode_detail ADD
   CONSTRAINT tb_taxcode_dtl_fk01 FOREIGN KEY (taxcode_state_code)
   REFERENCES tb_taxcode_state (taxcode_state_code) ;
ALTER TABLE tb_taxcode_detail ADD
   CONSTRAINT tb_taxcode_detail_fk02 FOREIGN KEY (taxcode_code, taxcode_type_code)
   REFERENCES tb_taxcode (taxcode_code, taxcode_type_code) ;
-- Originally failed in DB2
ALTER TABLE tb_taxcode_detail ADD
   CONSTRAINT tb_taxcode_detail_fk03 FOREIGN KEY (jurisdiction_id)
   REFERENCES tb_jurisdiction (jurisdiction_id) ;
-- Jurisdiction_id constraint added through annotation (i.e. we will try to enforce the constraint through Hibernate xml file rather than java code).

-- 2.3.TB_TAXCODE_STATE	
-- Primary key is defined in the domain class TaxCodeState.java.
-- Primary key ALREADY EXISTS in schema [TAXCODE_STATE_CODE].
-- NO DB CHANGES TO BE IMPLEMENTED

-- 2.3.TB_TAX_MATRIX
-- Primary key is defined in the domain class TaxMatrix.java.Constraints need to be added.
-- Primary key ALREADY EXISTS in schema [TAX_MATRIX_ID].
-- Implement no cascade delete DB constraint on matrix_state_code.
-- Originally failed in DB2
ALTER TABLE tb_tax_matrix ADD
   CONSTRAINT tb_tax_matrix_fk01 FOREIGN KEY (matrix_state_code)
   REFERENCES tb_taxcode_state (taxcode_state_code) ;
-- For columns driver01 to driver30, driver01 = tb_driver_reference (driver_value) where trans_dtl_column_name = '{driver_trans_dtl_column_name}'. 
-- Implement no cascade delete constraint on then_taxcode_detail_id and else_taxcode_detail_id.
ALTER TABLE tb_tax_matrix ADD
   CONSTRAINT tb_tax_matrix_fk02 FOREIGN KEY (then_taxcode_detail_id)
   REFERENCES tb_taxcode_detail (taxcode_detail_id) ;
ALTER TABLE tb_tax_matrix ADD
   CONSTRAINT tb_tax_matrix_fk03 FOREIGN KEY (else_taxcode_detail_id)
   REFERENCES tb_taxcode_detail (taxcode_detail_id) ;
-- Implement mid tier constraint on relation_sign = tb_list_code (code_code) where code_type_code = 'RELCODE'.
-- Implement mid tier constraint on then_cch_taxcat_code = tb_cch_code (code) where filename = 'DETAIL' & fieldname = 'TAXCAT'.
-- Implement mid tier constraint on then_cch_group_code = tb_cch_txmatrix (groupcode) where rectype = 'G'.
-- Implement mid tier constraint on else_cch_taxcat_code  = tb_cch_code (code) where filename = 'DETAIL' & fieldname = 'TAXCAT'.
-- Implement mid tier constraint on else_cch_group_code = tb_cch_txmatrix (groupcode) where rectype = 'G'.
-- Default '*ALL' is set for nullDriver fields by initNullDriverFields() of Matrix.java. Need to add for other fields also.
-- Default '*NA' is set to all *_thru fields by initializeDefaults() of TaxMatrix.java.
-- expiration_date date default values are set using initializeDefaults() method in Matrix.java.
-- then_taxcode_detail_id Impact on TaxMatrixViewBean.java.
-- else_cch_item_code impact on TaxMatrixViewBean.java.
-- Modify column effective_date to DEFAULT SYSDATE, column expiration_date to DEFAULT TO_DATE('12/31/9999','mm/dd/yyyy')
ALTER TABLE tb_tax_matrix ALTER COLUMN effective_date  SET DEFAULT current_timestamp;
ALTER TABLE tb_tax_matrix ALTER COLUMN expiration_date SET DEFAULT TO_TIMESTAMP('12/31/9999','mm/dd/yyyy') ;

-- 2.3.TB_TMP_RIA_RETURNS
-- Table to be removed
DROP TABLE tb_tmp_ria_returns;

-- 2.3.TB_TMP_TP_RETURNS
-- Table to be removed
DROP TABLE tb_tmp_tp_returns;

-- 2.3.TB_TRANSACTION_DETAIL
-- Test with and without foreign keys.
-- Implement in DB FK�s for process_batch_no, allocation_matrix_id, transaction_state_code, taxcode_detail_id, tax_matrix_id, location_matrix_id, jurisdiction_id, jurisdiction_taxrate_id.
-- Originally failed in DB2
ALTER TABLE tb_transaction_detail ADD 
   CONSTRAINT tb_transaction_detail_fk01 FOREIGN KEY (process_batch_no)
   REFERENCES tb_batch (batch_id) 
;
-- Originally failed in DB2
ALTER TABLE tb_transaction_detail ADD
   CONSTRAINT tb_transaction_detail_fk02 FOREIGN KEY (allocation_matrix_id)
   REFERENCES tb_allocation_matrix (allocation_matrix_id) ;

-- Add new row for *DEF, per MBF
INSERT INTO TB_TAXCODE_STATE ( TAXCODE_STATE_CODE, GEOCODE, NAME, COUNTRY, MEASURE_TYPE_WEIGHT, ACTIVE_FLAG, UPDATE_USER_ID, UPDATE_TIMESTAMP )
SELECT '*DEF', 'XX', 'Default', 'Default', 0, '0', 'STSCORP', current_timestamp WHERE '*DEF' NOT IN
 (SELECT taxcode_state_code FROM tb_taxcode_state WHERE TAXCODE_STATE_CODE = '*DEF'); 

-- Originally failed in DB2
ALTER TABLE tb_transaction_detail ADD
   CONSTRAINT tb_transaction_detail_fk03 FOREIGN KEY (transaction_state_code)
   REFERENCES tb_taxcode_state (taxcode_state_code) 
;
ALTER TABLE tb_transaction_detail ADD 
   CONSTRAINT tb_transaction_detail_fk04 FOREIGN KEY (taxcode_detail_id)
   REFERENCES tb_taxcode_detail (taxcode_detail_id) ;
-- Originally failed in DB2
ALTER TABLE tb_transaction_detail ADD
   CONSTRAINT tb_transaction_detail_fk05 FOREIGN KEY (tax_matrix_id)
   REFERENCES tb_tax_matrix (tax_matrix_id) 
;
-- Originally failed in DB2
ALTER TABLE tb_transaction_detail ADD
   CONSTRAINT tb_transaction_detail_fk06 FOREIGN KEY (location_matrix_id)
   REFERENCES tb_location_matrix (location_matrix_id) 
;
-- Originally failed in DB2
ALTER TABLE tb_transaction_detail ADD
   CONSTRAINT tb_transaction_detail_fk07 FOREIGN KEY (jurisdiction_id)
   REFERENCES tb_jurisdiction (jurisdiction_id) 
;
-- Originally failed in DB2
ALTER TABLE tb_transaction_detail ADD
   CONSTRAINT tb_transaction_detail_fk08 FOREIGN KEY (jurisdiction_taxrate_id)
   REFERENCES tb_jurisdiction_taxrate (jurisdiction_taxrate_id) 
;
-- Implement mid tier constraint on transaction_ind = tb_list_code (code_code)  where code_type_code = 'TRANSIND'.
-- Implement mid tier constraint on suspend_ind = tb_list_code (code_code) where code_type_code = 'SUSPENDIND'.
-- Implement mid tier constraint on cch_taxcat_code = tb_cch_code (code) where filename = 'DETAIL' & fieldname = 'TAXCAT'.
-- Implement mid tier constraint on cch_group_code = tb_cch_txmatrix (groupcode) where rectype = 'G'.
-- Implement mid tier constraint on manual_taxcode_ind = tb_list_code (code_code) where code_type_code = 'MANUAL'.
-- Implement mid tier constraint on manual_jurisdiction_ind = tb_list_code (code_code) where code_type_code = 'MANUAL'.
-- Implement mid tier constraint on measure_type_code = tb_list_code (code_code) where code_type_code = 'MEASURETYP'.

-- 2.3.TB_TRANSACTION_DETAIL_EXCP	
-- Create
-- No domain object is defined.
-- This table is not used directly through java code but through trigger presently.
-- NO DB CHANGES TO BE IMPLEMENTED (TABLE ALREADY EXISTS IN DB)

-- 2.3.TB_USER_ENTITY
-- Primary key is defined in the domain class UserEntity.java. Constraints need to be added.
-- Primary key ALREADY EXISTS in schema [USER_CODE, ENTITY_ID].
-- Implement cascade delete DB constraint on user_code and entity_id.
ALTER TABLE tb_user_entity ADD
   CONSTRAINT tb_user_entity_fk01 FOREIGN KEY (user_code)
   REFERENCES tb_user (user_code)
   ON DELETE CASCADE;
ALTER TABLE tb_user_entity ADD
   CONSTRAINT tb_user_entity_fk02 FOREIGN KEY (entity_id)
   REFERENCES tb_entity (entity_id)
   ON DELETE CASCADE;
-- Implement no cascade delete DB constraint on role_code.
ALTER TABLE tb_user_entity ADD
   CONSTRAINT tb_user_entity_fk03 FOREIGN KEY (role_code)
   REFERENCES tb_role (role_code) ;

-- 2.3.TB_USER_MENU_EX	
-- Primary key is defined in the domain class UserMenuEx.java. Constraints need to be added.
-- Primary key ALREADY EXISTS in schema [USER_CODE, ENTITY_ID, MENU_CODE].
-- Implement cascade delete DB constraint on user_code, entity_id and menu_code.
-- Originally failed in DB2
ALTER TABLE tb_user_menu_ex ADD
   CONSTRAINT tb_user_menu_ex_fk01 FOREIGN KEY (user_code, entity_id)
   REFERENCES tb_user_entity (user_code, entity_id)
   ON DELETE CASCADE 
;
ALTER TABLE tb_user_menu_ex ADD
   CONSTRAINT tb_user_menu_ex_fk02 FOREIGN KEY (menu_code)
   REFERENCES tb_menu (menu_code)
   ON DELETE CASCADE;

-- 2.3.TB_CCH_TXMATRIX
	
-- This table is used by 3 items; backingbean, Cachemanager and mapping in the domain class CCHCode.java and it has PK in domain class and pk in db.
-- There was no PK in the DB.  Per discussion with PG & JF, add composite PK [GROUPCODE,RECTYPE,ITEM]
UPDATE tb_cch_txmatrix SET item = '000' WHERE item IS NULL; -- Per MBF
ALTER TABLE tb_cch_txmatrix ADD
 CONSTRAINT pk_tb_cch_txmatrix PRIMARY KEY (groupcode,rectype,item)
;
