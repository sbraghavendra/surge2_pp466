CREATE OR REPLACE FUNCTION sp_batch_taxrate_update(an_batch_id NUMERIC)
RETURNS VOID 
AS
$$
DECLARE
-- Table defined variables
   v_batch_status_code             tb_batch.batch_status_code%TYPE                   := NULL;
   v_error_sev_code                tb_batch.error_sev_code%TYPE                      := ' ';
   v_starting_sequence             tb_batch.start_row%TYPE                           := 0;
   v_process_count                 tb_batch.start_row%TYPE                           := 0;
   v_processed_rows                tb_batch.start_row%TYPE                           := 0;
   v_severity_level                tb_list_code.severity_level%TYPE                  := ' ';
   v_write_import_line_flag        tb_list_code.write_import_line_flag%TYPE          := '1';
   v_abort_import_flag             tb_list_code.abort_import_flag%TYPE               := '0';
   v_jurisdiction_id               tb_jurisdiction.jurisdiction_id%TYPE              := NULL;
   v_jur_custom_flag               tb_jurisdiction.custom_flag%TYPE                  := NULL;
   v_jurisdiction_taxrate_id       tb_jurisdiction_taxrate.jurisdiction_taxrate_id%TYPE := NULL;
   v_rate_custom_flag              tb_jurisdiction_taxrate.custom_flag%TYPE          := NULL;
   v_rate_modified_flag            tb_jurisdiction_taxrate.modified_flag%TYPE        := NULL;
   v_sysdate                       tb_bcp_transactions.load_timestamp%TYPE           := CURRENT_TIMESTAMP;

-- Program defined variables
   vc_taxrate_update_type          VARCHAR(10)                                       := NULL;
   vi_release_number               INTEGER                                           := 0;
   vi_sessions                     INTEGER                                           := 0;
   vd_update_date                  DATE                                              := NULL;
   vi_cursor_id                    INTEGER                                           := 0;
   vi_error_count                  INTEGER                                           := 0;

   bcp                             RECORD;

-- ****** Program starts ****** --------------------------------------------------------------------
BEGIN
   -- Confirm batch exists and is flagged for processing
   BEGIN
      SELECT batch_status_code
        INTO STRICT v_batch_status_code
        FROM tb_batch
       WHERE batch_id = an_batch_id;
     
      IF v_batch_status_code != 'FTR' THEN
         vi_error_count := vi_error_count + 1;
         EXECUTE sp_geterrorcode('TR9', an_batch_id, 'TR', v_sysdate, v_severity_level, v_write_import_line_flag, v_abort_import_flag);
         IF v_severity_level > v_error_sev_code THEN
            v_error_sev_code := v_severity_level;
         END IF;        
      END IF;
   EXCEPTION
      WHEN OTHERS THEN
         vi_error_count := vi_error_count + 1;
         EXECUTE sp_geterrorcode('TR8', an_batch_id, 'TR', v_sysdate, v_severity_level, v_write_import_line_flag, v_abort_import_flag);
         IF v_severity_level > v_error_sev_code THEN
            v_error_sev_code := v_severity_level;
         END IF;
   END;
   -- Abort Procedure?
   IF v_abort_import_flag = '1' THEN
      v_batch_status_code := 'ABTR';
      RAISE EXCEPTION 'Abort Procedure: %', v_batch_status_code;
   END IF;

   -- Disable triggers
   --execute immediate 'ALTER TRIGGER tb_jurisdiction_taxrate_a_i DISABLE';

   -- Get starting sequence no.
   BEGIN
      SELECT CURRVAL('sq_tb_process_count')
        INTO STRICT v_starting_sequence;
   EXCEPTION
      WHEN OTHERS THEN
         NULL;
   END;

   -- Get batch information
   SELECT UPPER(VC02),
          NU01,
          TS01
     INTO STRICT
          vc_taxrate_update_type,
          vi_release_number,
          vd_update_date
     FROM tb_batch
    WHERE batch_id = an_batch_id;

   -- Update batch as processing
   BEGIN
      UPDATE tb_batch
         SET batch_status_code = 'XTR',
             error_sev_code = '',
             start_row = v_starting_sequence,
             actual_start_timestamp = v_sysdate
       WHERE batch_id = an_batch_id;
   EXCEPTION
      WHEN OTHERS THEN
         vi_error_count := vi_error_count + 1;
         EXECUTE sp_geterrorcode('TR1', an_batch_id, 'TR', v_sysdate, v_severity_level, v_write_import_line_flag, v_abort_import_flag);
         IF v_severity_level > v_error_sev_code THEN
            v_error_sev_code := v_severity_level;
         END IF;
   END;
   -- Abort Procedure?
   IF v_abort_import_flag = '1' THEN
      v_batch_status_code := 'ABTR';
      RAISE EXCEPTION 'Abort Procedure: %', v_batch_status_code;
   END IF;

   --COMMIT;

   -- ****** Read and Process Transactions ****** -----------------------------------------
   FOR bcp IN
      SELECT
         batch_id,
         geocode,
         zip,
         state,
         county,
         city,
         zipplus4,
         in_out,
         state_sales_rate,
         state_use_rate,
         county_sales_rate,
         county_use_rate,
         county_local_sales_rate,
         county_local_use_rate,
         county_split_amount,
         county_maxtax_amount,
         county_single_flag,
         county_default_flag,
         city_sales_rate,
         city_use_rate,
         city_local_sales_rate,
         city_local_use_rate,
         city_split_amount,
         city_split_sales_rate,
         city_split_use_rate,
         city_single_flag,
         city_default_flag,
         effective_date
    FROM tb_bcp_jurisdiction_taxrate
   WHERE batch_id = an_batch_id
   LOOP

      -- Increment Process Count
      SELECT NEXTVAL('sq_tb_process_count')
        INTO STRICT v_process_count
        FROM DUAL;

      v_processed_rows := v_processed_rows + 1;

      -- Find jurisdiction
      BEGIN
         SELECT jur.jurisdiction_id,
                jur.custom_flag
           INTO STRICT
		v_jurisdiction_id,
                v_jur_custom_flag
           FROM tb_jurisdiction jur
          WHERE ( jur.geocode = bcp.geocode ) AND
                ( jur.state = bcp.state ) AND
                ( jur.county = bcp.county ) AND
                ( jur.city = bcp.city ) AND
                ( jur.zip = bcp.zip );
      EXCEPTION
         WHEN OTHERS THEN
            v_jurisdiction_id := 0;
            v_jur_custom_flag := '0';
      END;

      -- If it does not exist, add it
      IF v_jurisdiction_id IS NULL or v_jurisdiction_id = 0 THEN
         -- Insert new jurisdiction
         INSERT INTO tb_jurisdiction(
            jurisdiction_id,
            geocode,
            state,
            county,
            city,
            zip,
            zipplus4,
            in_out,
            custom_flag,
            description )
         VALUES(
            0,
            bcp.geocode,
            bcp.state,
            bcp.county,
            bcp.city,
            bcp.zip,
            bcp.zipplus4,
            bcp.in_out,
            '0',
            'CCH Supplied Jurisdiction/Tax Rate' );
         -- Get last inserted jurisdiction id
         SELECT MAX(jurisdiction_id)
           INTO STRICT
		v_jurisdiction_id
           FROM tb_jurisdiction;
      ELSE
         -- If it does exist and is custom then write errog log warning
         IF v_jur_custom_flag = '1' THEN
            v_error_sev_code := '10';
            INSERT INTO tb_batch_error_log (
               batch_error_log_id,
               batch_id,
               process_type,
               process_timestamp,
               row_no,
               column_no,
               error_def_code,
               import_header_column,
               import_column_value,
               trans_dtl_column_name,
               trans_dtl_datatype,
               import_row )
            VALUES (
               0,
               bcp.batch_id,
               'TR',
               v_sysdate,
               0,
               0,
               'TR1',
               'GeoCode|State|County|City|Zip',
               bcp.geocode || '|' || bcp.state || '|' || bcp.county || '|' || bcp.city || '|' || bcp.zip,
               'n/a',
               'n/a',
               'n/a' );
         END IF;
      END IF;

      -- Find jurisdiction taxrate
      BEGIN
         SELECT rate.jurisdiction_taxrate_id,
                rate.custom_flag,
                rate.modified_flag
           INTO STRICT
		v_jurisdiction_taxrate_id,
                v_rate_custom_flag,
                v_rate_modified_flag
           FROM tb_jurisdiction_taxrate rate
          WHERE ( rate.jurisdiction_id = v_jurisdiction_id ) AND
                ( rate.effective_date = bcp.effective_date ) AND
                ( rate.measure_type_code = '0' );
      EXCEPTION
         WHEN OTHERS THEN
            v_jurisdiction_taxrate_id := 0;
            v_rate_custom_flag := '0';
            v_rate_modified_flag := '0';
      END;

      -- If it does not exist, add it
      IF v_jurisdiction_taxrate_id IS NULL or v_jurisdiction_taxrate_id = 0 THEN
         INSERT INTO tb_jurisdiction_taxrate(
            jurisdiction_taxrate_id,
            jurisdiction_id,
            measure_type_code,
            effective_date,
            expiration_date,
            custom_flag,
            modified_flag,
            state_sales_rate,
            state_use_rate,
            county_sales_rate,
            county_use_rate,
            county_local_sales_rate,
            county_local_use_rate,
            county_split_amount,
            county_maxtax_amount,
            county_single_flag,
            county_default_flag,
            city_sales_rate,
            city_use_rate,
            city_local_sales_rate,
            city_local_use_rate,
            city_split_amount,
            city_split_sales_rate,
            city_split_use_rate,
            city_single_flag,
            city_default_flag )
         VALUES(
            0,
            v_jurisdiction_id,
            '0',
            bcp.effective_date,
            to_date('12/31/9999', 'mm/dd/yyyy'),
            '0',
            '0',
            bcp.state_sales_rate,
            bcp.state_use_rate,
            bcp.county_sales_rate,
            bcp.county_use_rate,
            bcp.county_local_sales_rate,
            bcp.county_local_use_rate,
            bcp.county_split_amount,
            bcp.county_maxtax_amount,
            bcp.county_single_flag,
            bcp.county_default_flag,
            bcp.city_sales_rate,
            bcp.city_use_rate,
            bcp.city_local_sales_rate,
            bcp.city_local_use_rate,
            bcp.city_split_amount,
            bcp.city_split_sales_rate,
            bcp.city_split_use_rate,
            bcp.city_single_flag,
            bcp.city_default_flag );
      ELSE
         -- If it does exist and jurisdiction is not custom but rate is custom (modified or not) then write errog log warning
         IF (( v_jur_custom_flag IS NULL ) OR ( v_jur_custom_flag = '0' )) AND
            ( v_rate_custom_flag = '1' ) THEN
            v_error_sev_code := '10';
            INSERT INTO tb_batch_error_log (
               batch_error_log_id,
               batch_id,
               process_type,
               process_timestamp,
               row_no,
               column_no,
               error_def_code,
               import_header_column,
               import_column_value,
               trans_dtl_column_name,
               trans_dtl_datatype,
               import_row )
            VALUES (
               0,
               bcp.batch_id,
               'TR',
               v_sysdate,
               rownum,
               0,
               'TR2',
               'GeoCode|State|County|City|Zip|Effective Date',
               bcp.geocode || '|' || bcp.state || '|' || bcp.county || '|' || bcp.city || '|' || bcp.zip || '|' || to_char(bcp.effective_date, 'mm/dd/yyyy'),
               'n/a',
               'n/a',
               'n/a' );
         ELSE
            -- If it does exist and jurisdiction is not custom and rate is not custom but rate is modified then write error log warning
            IF (( v_jur_custom_flag IS NULL ) OR ( v_jur_custom_flag = '0' )) AND
               (( v_rate_custom_flag IS NULL ) OR ( v_rate_custom_flag = '0' )) AND
               ( v_rate_modified_flag = '1' ) THEN
               v_error_sev_code := '10';
               INSERT INTO tb_batch_error_log (
                  batch_error_log_id,
                  batch_id,
                  process_type,
                  process_timestamp,
                  row_no,
                  column_no,
                  error_def_code,
                  import_header_column,
                  import_column_value,
                  trans_dtl_column_name,
                  trans_dtl_datatype,
                  import_row )
               VALUES (
                  0,
                  bcp.batch_id,
                  'TR',
                  v_sysdate,
                  rownum,
                  0,
                  'TR3',
                  'GeoCode|State|County|City|Zip|Effective Date',
                  bcp.geocode || '|' || bcp.state || '|' || bcp.county || '|' || bcp.city || '|' || bcp.zip || '|' || to_char(bcp.effective_date, 'mm/dd/yyyy'),
                  'n/a',
                  'n/a',
                  'n/a' );
            END IF;
         END IF;
         -- Update TaxRate
         UPDATE tb_jurisdiction_taxrate rate SET
            rate.modified_flag = 0,
            rate.state_sales_rate = bcp.state_sales_rate,
            rate.state_use_rate =  bcp.state_use_rate,
            rate.county_sales_rate = bcp.county_sales_rate,
            rate.county_use_rate = bcp.county_use_rate,
            rate.county_local_sales_rate = bcp.county_local_sales_rate,
            rate.county_local_use_rate = bcp.county_local_use_rate,
            rate.county_split_amount = bcp.county_split_amount,
            rate.county_maxtax_amount = bcp.county_maxtax_amount,
            rate.county_single_flag = bcp.county_single_flag,
            rate.county_default_flag = bcp.county_default_flag,
            rate.city_sales_rate = bcp.city_sales_rate,
            rate.city_use_rate = bcp.city_use_rate,
            rate.city_local_sales_rate = bcp.city_local_sales_rate,
            rate.city_local_use_rate = bcp.city_local_use_rate,
            rate.city_split_amount = bcp.city_split_amount,
            rate.city_split_sales_rate = bcp.city_split_sales_rate,
            rate.city_split_use_rate = bcp.city_split_use_rate,
            rate.city_single_flag = bcp.city_single_flag,
            rate.city_default_flag = bcp.city_default_flag
         WHERE rate.jurisdiction_taxrate_id = v_jurisdiction_taxrate_id;
      END IF;
   END LOOP;

   -- Remove Batch from BCP Transactions Table
   DELETE
     FROM tb_bcp_jurisdiction_taxrate
    WHERE batch_id = an_batch_id;

   --  Update Options table with last month/year updated
   UPDATE tb_option
      SET value = to_char(vd_update_date, 'yyyy_mm')
    WHERE option_code = 'LASTTAXRATEDATE' || vc_taxrate_update_type AND
          option_type_code = 'SYSTEM' AND
          user_code = 'SYSTEM';

   -- Update Options table with last release number
   UPDATE tb_option
      SET value = vi_release_number
    WHERE option_code = 'LASTTAXRATEREL' || vc_taxrate_update_type AND
          option_type_code = 'SYSTEM' AND
          user_code = 'SYSTEM';

   -- Update batch with final totals
   UPDATE tb_batch
      SET batch_status_code = 'TR',
          error_sev_code = v_error_sev_code,
          processed_rows = v_processed_rows,
          actual_end_timestamp = CURRENT_TIMESTAMP
    WHERE batch_id = an_batch_id;

   -- Update CCH Code table
   DELETE FROM tb_cch_code WHERE custom_flag <> '1';

   /*INSERT INTO tb_cch_code (
      filename,
      fieldname,
      fieldpos,
      fieldlen,
      fieldno,
      code,
      description,
      custom_flag,
      modified_flag )
   SELECT
      filename,
      fieldname,
      fieldpos,
      fieldlen,
      fieldno,
      code,
      description,
      '0',
      '0'
   FROM tb_bcp_cch_code
   WHERE batch_id = an_batch_id;

   DELETE
     FROM tb_bcp_cch_code
    WHERE batch_id = an_batch_id;*/

   -- Update CCH TxMatrix table
   DELETE FROM tb_cch_txmatrix WHERE custom_flag <> '1';

   /*INSERT INTO tb_cch_txmatrix (
      city,
      county,
      state,
      local,
      geocode,
      groupcode,
      groupdesc,
      item,
      itemdesc,
      provider,
      customer,
      taxtypet,
      taxcatt,
      taxable,
      taxtype,
      taxcat,
      effdate,
      rectype,
      custom_flag,
      modified_flag )
   SELECT
      city,
      county,
      state,
      local,
      geocode,
      groupcode,
      groupdesc,
      item,
      itemdesc,
      provider,
      customer,
      taxtypet,
      taxcatt,
      taxable,
      taxtype,
      taxcat,
      to_date(effdate,'yyyymmdd'),
      rectype,
     '0',
     '0'
   FROM tb_bcp_cch_txmatrix
   WHERE batch_id = an_batch_id;

   DELETE
     FROM tb_bcp_cch_txmatrix
    WHERE batch_id = an_batch_id;*/

   -- commit all updates
   --COMMIT;

   -- Enable triggers
   --execute immediate 'ALTER TRIGGER tb_jurisdiction_taxrate_a_i ENABLE';

EXCEPTION
   WHEN OTHERS THEN
      -- Update batch with error codes
      UPDATE tb_batch
         SET batch_status_code = v_batch_status_code,
             error_sev_code = v_error_sev_code,
             actual_end_timestamp = CURRENT_TIMESTAMP
       WHERE batch_id = an_batch_id;
      --COMMIT;
      -- Enable triggers
      --execute immediate 'ALTER TRIGGER tb_jurisdiction_taxrate_a_i ENABLE';

END;
$$ LANGUAGE plpgsql;
