CREATE OR REPLACE FUNCTION sp_truncate_bcp_tables(return_code OUT NUMERIC)
AS
$$
DECLARE
-- Program defined variables
   v_count                         NUMERIC := 0;
   --e_halt                          exception;

-- ****** Program starts ****** --------------------------------------------------------------------
BEGIN
   -- *** Check for Users in STS? ***
   /*SELECT count(*)
     INTO v_count
     FROM v$session
    WHERE audsid <> 0
      AND audsid NOT IN
         (SELECT sys_context('USERENV','SESSIONID') FROM DUAL);
   IF v_count > 0 Then
      RAISE e_halt;
   END IF;
   v_count := 0;*/

   SELECT count(*)
     INTO STRICT v_count
     FROM pg_stat_activity
    WHERE database = CURRENT_DATABASE();

   IF v_count > 1 Then
     return_code := 1;
     RETURN;
   END IF;

   v_count := 0;

   -- *** Import/Process Transactions ***
   -- Delete batches
   DELETE
     FROM tb_bcp_transactions
    WHERE tb_bcp_transactions.process_batch_no IN (
      SELECT DISTINCT tb_bcp_transactions.process_batch_no
        FROM tb_bcp_transactions, tb_batch
       WHERE tb_bcp_transactions.process_batch_no = tb_batch.batch_id
         AND tb_batch.batch_status_code IN ('ABI','ABP','D','P') );

   -- Truncate tb_bcp_transactions
   SELECT count(*)
   INTO STRICT v_count
   FROM tb_bcp_transactions;

   IF v_count IS NULL OR v_count = 0 THEN
      EXECUTE 'TRUNCATE TABLE tb_bcp_transactions';
   END IF;

   -- *** Tax Rate Updates - Tax Rates ***
   -- Delete batches
   DELETE
     FROM tb_bcp_jurisdiction_taxrate
    WHERE tb_bcp_jurisdiction_taxrate.batch_id IN (
      SELECT DISTINCT tb_bcp_jurisdiction_taxrate.batch_id
        FROM tb_bcp_jurisdiction_taxrate, tb_batch
       WHERE tb_bcp_jurisdiction_taxrate.batch_id = tb_batch.batch_id
         AND tb_batch.batch_status_code IN ('ABTR','D','TR') );

   -- Truncate tb_bcp_jurisdiction_taxrate
   SELECT count(*)
   INTO STRICT v_count
   FROM tb_bcp_jurisdiction_taxrate;

   IF v_count IS NULL OR v_count = 0 THEN
      EXECUTE 'TRUNCATE TABLE tb_bcp_jurisdiction_taxrate';
   END IF;

   -- *** Tax Rate Updates - CCH Tax Codes ***
   -- Delete batches
   /*DELETE
     FROM tb_bcp_cch_code
    WHERE tb_bcp_cch_code.batch_id IN (
      SELECT DISTINCT tb_bcp_cch_code.batch_id
        FROM tb_bcp_cch_code, tb_batch
       WHERE tb_bcp_cch_code.batch_id = tb_batch.batch_id
         AND tb_batch.batch_status_code IN ('ABTR','D','TR') );*/

   -- Truncate tb_bcp_cch_code
   /*SELECT count(*)
   INTO v_count
   FROM tb_bcp_cch_code;

   IF v_count IS NULL OR v_count = 0 THEN
      EXECUTE IMMEDIATE 'TRUNCATE TABLE tb_bcp_cch_code';
   END IF;*/

   -- *** Tax Rate Updates - CCH Tax Matrix ***
   -- Delete batches
   /*DELETE
     FROM tb_bcp_cch_txmatrix
    WHERE tb_bcp_cch_txmatrix.batch_id IN (
      SELECT DISTINCT tb_bcp_cch_txmatrix.batch_id
        FROM tb_bcp_cch_txmatrix, tb_batch
       WHERE tb_bcp_cch_txmatrix.batch_id = tb_batch.batch_id
         AND tb_batch.batch_status_code IN ('ABTR','D','TR') );*/

   -- Truncate tb_bcp_cch_code
   /*SELECT count(*)
   INTO v_count
   FROM tb_bcp_cch_txmatrix;*/

   /*IF v_count IS NULL OR v_count = 0 THEN
      EXECUTE IMMEDIATE 'TRUNCATE TABLE tb_bcp_cch_txmatrix';
   END IF;*/

   return_code := 0;
-- EXCEPTION
  -- WHEN e_halt THEN
    --  return_code := 1;
END;
$$
LANGUAGE plpgsql;
