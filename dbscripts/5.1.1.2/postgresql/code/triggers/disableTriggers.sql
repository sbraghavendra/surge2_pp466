CREATE OR REPLACE FUNCTION sp_disable_all_triggers()
RETURNS VOID 
AS
$$
DECLARE
	current_row RECORD;
BEGIN
	FOR current_row IN 
		select trigger_name, event_object_table from information_schema.triggers
		where trigger_schema = current_user
	 LOOP
		EXECUTE 'ALTER TABLE '|| current_row.event_object_table ||' DISABLE TRIGGER '||current_row.trigger_name;
	END LOOP;
END;
$$
LANGUAGE plpgsql;

SELECT sp_disable_all_triggers();

DROP FUNCTION sp_disable_all_triggers();
