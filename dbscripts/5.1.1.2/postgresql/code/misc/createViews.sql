CREATE OR REPLACE VIEW TB_VIEW_COLUMN_NAMES (TABLE_NAME, COLUMN_NAME)
AS 
SELECT	columns.table_name, 
	columns.column_name 
FROM	information_schema.columns 
WHERE (columns.table_schema = 'stscorp');

CREATE OR REPLACE VIEW TB_VIEW_TABLE_NAMES (TABLE_NAME)
AS 
SELECT	tables.table_name 
FROM	information_schema.tables 
WHERE (tables.table_schema = 'stscorp');
