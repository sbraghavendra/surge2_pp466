
CREATE USER stscorp                                                             
IDENTIFIED BY VALUES 'C3BE6EBB0F0CD483'                                         
DEFAULT TABLESPACE USERS                                                        
TEMPORARY TABLESPACE TEMP                                                       
PROFILE DEFAULT;                                                                
GRANT ALTER ANY ROLE TO STSCORP;                                                
GRANT CONNECT TO STSCORP;                                                       
GRANT CREATE ANY SEQUENCE TO STSCORP;                                           
GRANT CREATE PUBLIC SYNONYM TO STSCORP;                                         
GRANT CREATE ROLLBACK SEGMENT TO STSCORP;                                       
GRANT CREATE SESSION TO STSCORP;                                                
GRANT CREATE SYNONYM TO STSCORP;                                                
GRANT CREATE VIEW TO STSCORP;                                                   
GRANT DBA TO STSCORP;                                                           
GRANT EXECUTE ANY PROCEDURE TO STSCORP;                                         
GRANT EXP_FULL_DATABASE TO STSCORP;                                             
GRANT IMP_FULL_DATABASE TO STSCORP;                                             
GRANT QUERY REWRITE TO STSCORP;                                                 
GRANT RESOURCE TO STSCORP;                                                      
GRANT UNLIMITED TABLESPACE TO STSCORP;                                          
GRANT SELECT ON sys.v_$session TO stscorp;                                            

exit;
