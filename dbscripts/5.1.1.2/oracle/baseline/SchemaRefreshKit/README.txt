--------------------------------------------
-- Running the STSCORP Schema Refresh Kit --
--------------------------------------------

1. What you need to do before running the kit:

 a. Determine the ORACLE_SID of the database you will be conducting the refresh in

 b. Determine the password for the SYSTEM user in the database

 c. Identify the file (.dmp) that will be used for import

 d. Confirm that it is OK to drop/recreate the STSCORP user & schema

2. How to run the kit:

 a. Copy the contents of this directory to the database machine

 b. Open a COMMAND Prompt and cd to this directory

 c. The Schema Refresh Kit is driven by the refreshSchema.bat script.  
    This script is parameter driven and should be executed as follows:

    **** MAKE SURE THE VALUE OF <ORACLE_SID> IS SET TO THE CORRECT DATABASE ****

     CMD> refreshSchema.bat <ORACLE_SID> system/<SYSTEM_DB_Password> "<FullPathToFile>"

    **** MAKE SURE THE VALUE OF <ORACLE_SID> IS SET TO THE CORRECT DATABASE ****

    NOTE: Be patient while the import is underway, this could take on the order of a 
    few hours.  Once the import has completed, review the import log:
    <ORACLE_SID>_schemaImport_STSCORP.log for any errors.