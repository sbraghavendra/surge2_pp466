
-- 1 --
--DROP PROCEDURE SP_TB_TAX_MATRIX_MASTER_LIST;
CREATE OR REPLACE function SP_TB_TAX_MATRIX_MASTER_LIST (
	p_where_clause_token	IN VARCHAR2,
	p_order_by_token	IN VARCHAR2,
	p_start_index		IN INTEGER,
	p_max_rows		IN INTEGER
) RETURN sys_refcursor
AS
	l_cursor	sys_refcursor;
BEGIN
	OPEN l_cursor FOR
	'SELECT * FROM ( SELECT B.*, rownum rnum  
	  FROM (  
	  SELECT DISTINCT  
		T.DEFAULT_FLAG,T.MATRIX_STATE_CODE,T.DRIVER_GLOBAL_FLAG, 
		T.DRIVER_01,T.DRIVER_02,T.DRIVER_03,T.DRIVER_04,T.DRIVER_05,T.DRIVER_06,        
		T.DRIVER_07,T.DRIVER_08,T.DRIVER_09,T.DRIVER_10,T.DRIVER_11,T.DRIVER_12,             
		T.DRIVER_13,T.DRIVER_14,T.DRIVER_15,T.DRIVER_16,T.DRIVER_17,T.DRIVER_18,        
		T.DRIVER_19,T.DRIVER_20,T.DRIVER_21,T.DRIVER_22,T.DRIVER_23,T.DRIVER_24,            
		T.DRIVER_25,T.DRIVER_26,T.DRIVER_27,T.DRIVER_28,T.DRIVER_29,T.DRIVER_30,         
		T.BINARY_WEIGHT,T.DEFAULT_BINARY_WEIGHT, T.DRIVER_01_THRU,T.DRIVER_02_THRU,T.DRIVER_03_THRU,T.DRIVER_04_THRU,T.DRIVER_05_THRU,         
		T.DRIVER_06_THRU,T.DRIVER_07_THRU,T.DRIVER_08_THRU,T.DRIVER_09_THRU,T.DRIVER_10_THRU,         
		T.DRIVER_11_THRU,T.DRIVER_12_THRU,T.DRIVER_13_THRU,T.DRIVER_14_THRU,T.DRIVER_15_THRU,         
		T.DRIVER_16_THRU,T.DRIVER_17_THRU,T.DRIVER_18_THRU,T.DRIVER_19_THRU,T.DRIVER_20_THRU,         
		T.DRIVER_21_THRU,T.DRIVER_22_THRU,T.DRIVER_23_THRU,T.DRIVER_24_THRU,T.DRIVER_25_THRU,         
		T.DRIVER_26_THRU,T.DRIVER_27_THRU,T.DRIVER_28_THRU,T.DRIVER_29_THRU,T.DRIVER_30_THRU      
	FROM	TB_TAX_MATRIX T 
	left outer join TB_TAXCODE_DETAIL TB_TAXCODE_DETAIL_A on (T.THEN_TAXCODE_DETAIL_ID = TB_TAXCODE_DETAIL_A.TAXCODE_DETAIL_ID)         
	left outer join TB_TAXCODE_DETAIL TB_TAXCODE_DETAIL_B on (T.ELSE_TAXCODE_DETAIL_ID = TB_TAXCODE_DETAIL_B.TAXCODE_DETAIL_ID)        
	WHERE 1=1 '
	||p_where_clause_token||' '
	||p_order_by_token    ||' ) B WHERE rownum <= '|| (p_start_index + p_max_rows) ||' ) WHERE rnum > '||p_start_index;

	RETURN l_cursor;
END;
/

-- 2 --
--DROP PROCEDURE SP_TB_TAX_MATRIX_MASTER_COUNT;
CREATE OR REPLACE FUNCTION SP_TB_TAX_MATRIX_MASTER_COUNT (
	p_where_clause_token	IN VARCHAR2
) RETURN sys_refcursor
AS
	l_cursor sys_refcursor;
BEGIN

	OPEN l_cursor FOR
	'SELECT COUNT(*) FROM ( SELECT DISTINCT 
	     T.DEFAULT_FLAG,T.MATRIX_STATE_CODE,T.DRIVER_GLOBAL_FLAG,T.DRIVER_01,T.DRIVER_02,T.DRIVER_03,T.DRIVER_04,T.DRIVER_05,T.DRIVER_06, 
	     T.DRIVER_07,T.DRIVER_08,T.DRIVER_09,T.DRIVER_10,T.DRIVER_11,T.DRIVER_12,T.DRIVER_13,T.DRIVER_14,T.DRIVER_15,T.DRIVER_16,T.DRIVER_17,T.DRIVER_18, 
	     T.DRIVER_19,T.DRIVER_20,T.DRIVER_21,T.DRIVER_22,T.DRIVER_23,T.DRIVER_24,T.DRIVER_25,T.DRIVER_26,T.DRIVER_27,T.DRIVER_28,T.DRIVER_29,T.DRIVER_30, 
	     T.BINARY_WEIGHT,T.DEFAULT_BINARY_WEIGHT,T.DRIVER_01_THRU,T.DRIVER_02_THRU,T.DRIVER_03_THRU,T.DRIVER_04_THRU,T.DRIVER_05_THRU, 
	     T.DRIVER_06_THRU,T.DRIVER_07_THRU,T.DRIVER_08_THRU,T.DRIVER_09_THRU,T.DRIVER_10_THRU, 
	     T.DRIVER_11_THRU,T.DRIVER_12_THRU,T.DRIVER_13_THRU,T.DRIVER_14_THRU,T.DRIVER_15_THRU, 
	     T.DRIVER_16_THRU,T.DRIVER_17_THRU,T.DRIVER_18_THRU,T.DRIVER_19_THRU,T.DRIVER_20_THRU, 
	     T.DRIVER_21_THRU,T.DRIVER_22_THRU,T.DRIVER_23_THRU,T.DRIVER_24_THRU,T.DRIVER_25_THRU, 
	     T.DRIVER_26_THRU,T.DRIVER_27_THRU,T.DRIVER_28_THRU,T.DRIVER_29_THRU,T.DRIVER_30_THRU 
	FROM TB_TAX_MATRIX T 
	 left outer join TB_TAXCODE_DETAIL TB_TAXCODE_DETAIL_A on (T.THEN_TAXCODE_DETAIL_ID = TB_TAXCODE_DETAIL_A.TAXCODE_DETAIL_ID) 
	 left outer join TB_TAXCODE_DETAIL TB_TAXCODE_DETAIL_B on (T.ELSE_TAXCODE_DETAIL_ID = TB_TAXCODE_DETAIL_B.TAXCODE_DETAIL_ID) 
	WHERE 1=1 '|| p_where_clause_token ||' ) B '; 

	RETURN l_cursor;
 
END;
/

-- 3 --
-- JF: renamed proc for length
--DROP procedure SP_TB_LOCATION_MTRX_MASTR_LIST;
CREATE OR REPLACE FUNCTION SP_TB_LOCATION_MTRX_MASTR_LIST (
	p_where_clause_token	IN VARCHAR2,
	p_order_by_token	IN VARCHAR2,
	p_start_index		IN INTEGER,
	p_max_rows		IN INTEGER
) RETURN SYS_REFCURSOR
AS
	l_cursor sys_refcursor;
BEGIN
	OPEN l_cursor FOR
	'SELECT * FROM ( SELECT B.*, rownum rnum  
	FROM ( 
	    SELECT DISTINCT 
	    TB_LOCATION_MATRIX.DRIVER_01, TB_LOCATION_MATRIX.DRIVER_02, TB_LOCATION_MATRIX.DRIVER_03,TB_LOCATION_MATRIX.DRIVER_04, TB_LOCATION_MATRIX.DRIVER_05, TB_LOCATION_MATRIX.DRIVER_06,TB_LOCATION_MATRIX.DRIVER_07, TB_LOCATION_MATRIX.DRIVER_08, TB_LOCATION_MATRIX.DRIVER_09, TB_LOCATION_MATRIX.DRIVER_10, TB_LOCATION_MATRIX.BINARY_WEIGHT,TB_LOCATION_MATRIX.DEFAULT_FLAG, TB_LOCATION_MATRIX.DEFAULT_BINARY_WEIGHT
	    FROM TB_LOCATION_MATRIX left outer join TB_JURISDICTION
	     on (TB_LOCATION_MATRIX.JURISDICTION_ID = TB_JURISDICTION.JURISDICTION_ID)        
	WHERE 1=1 '||p_where_clause_token||' '||p_order_by_token||' ) B WHERE rownum <= '|| (p_start_index + p_max_rows) ||' ) WHERE rnum > '||p_start_index;

	RETURN l_cursor;

END;
/

-- 4 --
--DROP procedure SP_ALLOCATION_MTRX_MASTR_LIST;
CREATE OR REPLACE FUNCTION SP_ALLOCATION_MTRX_MASTR_LIST (
	p_where_clause_token	IN VARCHAR2,
	p_order_by_token	IN VARCHAR2
) RETURN sys_refcursor 
AS
	l_cursor sys_refcursor;
BEGIN
	OPEN l_cursor FOR
	'SELECT * FROM (SELECT DISTINCT         
	TB_ALLOCATION_MATRIX.DRIVER_01,TB_ALLOCATION_MATRIX.DRIVER_02,TB_ALLOCATION_MATRIX.DRIVER_03,TB_ALLOCATION_MATRIX.DRIVER_04,TB_ALLOCATION_MATRIX.DRIVER_05,  
	TB_ALLOCATION_MATRIX.DRIVER_06,TB_ALLOCATION_MATRIX.DRIVER_07,TB_ALLOCATION_MATRIX.DRIVER_08,TB_ALLOCATION_MATRIX.DRIVER_09,TB_ALLOCATION_MATRIX.DRIVER_10,  
	TB_ALLOCATION_MATRIX.DRIVER_11,TB_ALLOCATION_MATRIX.DRIVER_12,TB_ALLOCATION_MATRIX.DRIVER_13,TB_ALLOCATION_MATRIX.DRIVER_14,TB_ALLOCATION_MATRIX.DRIVER_15,  
	TB_ALLOCATION_MATRIX.DRIVER_16,TB_ALLOCATION_MATRIX.DRIVER_17,TB_ALLOCATION_MATRIX.DRIVER_18,TB_ALLOCATION_MATRIX.DRIVER_19,TB_ALLOCATION_MATRIX.DRIVER_20,  
	TB_ALLOCATION_MATRIX.DRIVER_21,TB_ALLOCATION_MATRIX.DRIVER_22,TB_ALLOCATION_MATRIX.DRIVER_23,TB_ALLOCATION_MATRIX.DRIVER_24,TB_ALLOCATION_MATRIX.DRIVER_25,  
	TB_ALLOCATION_MATRIX.DRIVER_26,TB_ALLOCATION_MATRIX.DRIVER_27,TB_ALLOCATION_MATRIX.DRIVER_28,TB_ALLOCATION_MATRIX.DRIVER_29,TB_ALLOCATION_MATRIX.DRIVER_30,  
	TB_ALLOCATION_MATRIX.BINARY_WEIGHT  
	FROM TB_ALLOCATION_MATRIX_DETAIL 
	left outer join TB_ALLOCATION_MATRIX on (TB_ALLOCATION_MATRIX_DETAIL.ALLOCATION_MATRIX_ID = TB_ALLOCATION_MATRIX.ALLOCATION_MATRIX_ID)     
	left outer join TB_JURISDICTION on      (TB_ALLOCATION_MATRIX_DETAIL.JURISDICTION_ID      = TB_JURISDICTION.JURISDICTION_ID)     
	WHERE 1=1 '||p_where_clause_token||' '||p_order_by_token||' ) B ';

	RETURN l_cursor;
END;
/

-- 5 --
-- DROP PROCEDURE SP_LOCATION_MTRX_MASTR_COUNT;
CREATE OR REPLACE FUNCTION SP_LOCATION_MTRX_MASTR_COUNT (
	p_where_clause_token IN VARCHAR2
) RETURN sys_refcursor
AS
	l_cursor sys_refcursor;
BEGIN
	OPEN l_cursor FOR
	'SELECT COUNT(*) 
	FROM (
		SELECT DISTINCT  
			TB_LOCATION_MATRIX.DRIVER_01, TB_LOCATION_MATRIX.DRIVER_02, TB_LOCATION_MATRIX.DRIVER_03, TB_LOCATION_MATRIX.DRIVER_04, 
			TB_LOCATION_MATRIX.DRIVER_05, TB_LOCATION_MATRIX.DRIVER_06, TB_LOCATION_MATRIX.DRIVER_07, TB_LOCATION_MATRIX.DRIVER_08, 
			TB_LOCATION_MATRIX.DRIVER_09, TB_LOCATION_MATRIX.DRIVER_10, 
			TB_LOCATION_MATRIX.BINARY_WEIGHT, TB_LOCATION_MATRIX.DEFAULT_FLAG, TB_LOCATION_MATRIX.DEFAULT_BINARY_WEIGHT  
	FROM TB_LOCATION_MATRIX left outer join TB_JURISDICTION  
	on (TB_LOCATION_MATRIX.JURISDICTION_ID = TB_JURISDICTION.JURISDICTION_ID)        WHERE 1=1 '||p_where_clause_token||' ) B ';

	RETURN l_cursor;
END;
/

-- 6 --
-- DROP PROCEDURE SP_ALLOCATION_MTRX_MASTR_COUNT;
CREATE OR REPLACE FUNCTION SP_ALLOCATION_MTRX_MASTR_COUNT (
	p_where_clause_token	IN VARCHAR2
) RETURN sys_refcursor
AS
	l_cursor sys_refcursor;
BEGIN
	OPEN l_cursor FOR
	'SELECT COUNT(*)  
	    FROM (       
	    SELECT DISTINCT           
		TB_ALLOCATION_MATRIX.DRIVER_01,TB_ALLOCATION_MATRIX.DRIVER_02,TB_ALLOCATION_MATRIX.DRIVER_03,TB_ALLOCATION_MATRIX.DRIVER_04,TB_ALLOCATION_MATRIX.DRIVER_05,  
		TB_ALLOCATION_MATRIX.DRIVER_06,TB_ALLOCATION_MATRIX.DRIVER_07,TB_ALLOCATION_MATRIX.DRIVER_08,TB_ALLOCATION_MATRIX.DRIVER_09,TB_ALLOCATION_MATRIX.DRIVER_10,  
		TB_ALLOCATION_MATRIX.DRIVER_11,TB_ALLOCATION_MATRIX.DRIVER_12,TB_ALLOCATION_MATRIX.DRIVER_13,TB_ALLOCATION_MATRIX.DRIVER_14,TB_ALLOCATION_MATRIX.DRIVER_15,  
		TB_ALLOCATION_MATRIX.DRIVER_16,TB_ALLOCATION_MATRIX.DRIVER_17,TB_ALLOCATION_MATRIX.DRIVER_18,TB_ALLOCATION_MATRIX.DRIVER_19,TB_ALLOCATION_MATRIX.DRIVER_20,  
		TB_ALLOCATION_MATRIX.DRIVER_21,TB_ALLOCATION_MATRIX.DRIVER_22,TB_ALLOCATION_MATRIX.DRIVER_23,TB_ALLOCATION_MATRIX.DRIVER_24,TB_ALLOCATION_MATRIX.DRIVER_25,  
		TB_ALLOCATION_MATRIX.DRIVER_26,TB_ALLOCATION_MATRIX.DRIVER_27,TB_ALLOCATION_MATRIX.DRIVER_28,TB_ALLOCATION_MATRIX.DRIVER_29,TB_ALLOCATION_MATRIX.DRIVER_30,  
		TB_ALLOCATION_MATRIX.BINARY_WEIGHT      
	FROM TB_ALLOCATION_MATRIX_DETAIL 
	left outer join TB_ALLOCATION_MATRIX on (TB_ALLOCATION_MATRIX_DETAIL.ALLOCATION_MATRIX_ID = TB_ALLOCATION_MATRIX.ALLOCATION_MATRIX_ID)        
	left outer join TB_JURISDICTION      on (TB_ALLOCATION_MATRIX_DETAIL.JURISDICTION_ID      = TB_JURISDICTION.JURISDICTION_ID)   
	WHERE 1=1 '||p_where_clause_token||' ) B';

	RETURN l_cursor;
END;
/

-- 7 --
-- DROP PROCEDURE SP_TAXCODE_DETAIL_LIST ;
CREATE OR REPLACE FUNCTION SP_TAXCODE_DETAIL_LIST (
	p_where_clause_token	IN VARCHAR2
) RETURN sys_refcursor
AS
	l_cursor sys_refcursor;
BEGIN
	OPEN l_cursor FOR
	'SELECT	TB_TAXCODE_DETAIL.TAXCODE_CODE, TB_TAXCODE_DETAIL.TAXCODE_STATE_CODE,
		TB_TAXCODE_STATE.NAME, TB_TAXCODE_DETAIL.TAXCODE_TYPE_CODE,
		TB_LIST_CODE.DESCRIPTION AS TAXCODE_TYPE_DESCRIPTION,
		TB_TAXCODE.DESCRIPTION AS TAXCODE_DESCRIPTION, TB_TAXCODE.COMMENTS,
		TB_TAXCODE.TAXTYPE_CODE, TB_TAXCODE.ERP_TAXCODE,
		TB_LIST_CODE1.DESCRIPTION AS TAX_TYPE_DESCRIPTION,
		TB_TAXCODE_DETAIL.TAXCODE_DETAIL_ID,
		TB_TAXCODE_DETAIL.JURISDICTION_ID, TB_JURISDICTION.GEOCODE,
		TB_JURISDICTION.CITY, TB_JURISDICTION.COUNTY, TB_JURISDICTION.STATE,
		TB_JURISDICTION.ZIP,TB_TAXCODE.MEASURE_TYPE_CODE,TB_TAXCODE.MEASURE_TYPE_CODE 
	FROM ((((TB_TAXCODE_DETAIL INNER JOIN TB_TAXCODE_STATE ON TB_TAXCODE_DETAIL.TAXCODE_STATE_CODE = TB_TAXCODE_STATE.TAXCODE_STATE_CODE) 
	INNER JOIN TB_TAXCODE ON TB_TAXCODE_DETAIL.TAXCODE_CODE = TB_TAXCODE.TAXCODE_CODE AND TB_TAXCODE_DETAIL.TAXCODE_TYPE_CODE = TB_TAXCODE.TAXCODE_TYPE_CODE) 
	INNER JOIN TB_LIST_CODE ON TB_TAXCODE_DETAIL.TAXCODE_TYPE_CODE = TB_LIST_CODE.CODE_CODE) 
	LEFT JOIN  TB_LIST_CODE TB_LIST_CODE1 ON TB_LIST_CODE1.CODE_TYPE_CODE = ''TAXTYPE'' AND TB_LIST_CODE1.CODE_CODE = TB_TAXCODE.TAXTYPE_CODE) 
	LEFT JOIN  TB_JURISDICTION ON TB_JURISDICTION.JURISDICTION_ID = TB_TAXCODE_DETAIL.JURISDICTION_ID 
	WHERE      TB_TAXCODE_DETAIL.ACTIVE_FLAG = ''1'' 
	AND        TB_LIST_CODE.CODE_TYPE_CODE = ''TCTYPE'' '||
	p_where_clause_token
	; 

	RETURN l_cursor;
END;
/

-- 8 --
-- DROP PROCEDURE SP_ACTIVE_MATRIX_LINES ;
CREATE OR REPLACE FUNCTION SP_ACTIVE_MATRIX_LINES (
	p_where_clause_token	IN VARCHAR2,
	p_effective_date	IN DATE,
	p_expiration_date	IN DATE
) RETURN sys_refcursor
AS
	l_cursor sys_refcursor;
BEGIN
	OPEN l_cursor FOR
	'SELECT	COUNT (*) AS active_matrix_lines 
	FROM TB_TAX_MATRIX 
	left outer join TB_TAXCODE_DETAIL TB_TAXCODE_DETAIL_A on (TB_TAX_MATRIX.then_taxcode_detail_id = tb_taxcode_detail_a.taxcode_detail_id)    
	left outer join TB_TAXCODE_DETAIL TB_TAXCODE_DETAIL_B on (TB_TAX_MATRIX.else_taxcode_detail_id = tb_taxcode_detail_b.taxcode_detail_id)  
	where (TB_TAX_MATRIX.EFFECTIVE_DATE <= SYSDATE) AND (TB_TAX_MATRIX.EXPIRATION_DATE >= SYSDATE) '||p_where_clause_token;

	RETURN l_cursor;
END;
/

-- 9 --
-- DROP PROCEDURE SP_TAX_MTRX_TOTAL_MTRX_LINES ;
CREATE OR REPLACE FUNCTION SP_TAX_MTRX_TOTAL_MTRX_LINES (
	p_where_clause_token IN VARCHAR2
) RETURN sys_refcursor
AS
	l_cursor sys_refcursor;
BEGIN
		OPEN l_cursor FOR 
		'SELECT COUNT (*) AS total_matrix_lines FROM TB_TAX_MATRIX 
		left outer join TB_TAXCODE_DETAIL TB_TAXCODE_DETAIL_A  on (TB_TAX_MATRIX.then_taxcode_detail_id = tb_taxcode_detail_a.taxcode_detail_id) 
		left outer join TB_TAXCODE_DETAIL TB_TAXCODE_DETAIL_B  on (TB_TAX_MATRIX.else_taxcode_detail_id = tb_taxcode_detail_b.taxcode_detail_id) '||p_where_clause_token;

	RETURN l_cursor;
END;
/

-- 10 --
-- DROP PROCEDURE sp_tax_mtrx_dstnct_mtrx_lines ;
CREATE OR REPLACE FUNCTION sp_tax_mtrx_dstnct_mtrx_lines (
	 p_where_clause_token IN VARCHAR2
) RETURN sys_refcursor
AS
	 l_cursor sys_refcursor;
BEGIN
	OPEN l_cursor for
	'SELECT COUNT (*) AS distinct_matrix_lines 
		FROM (
		SELECT COUNT (*) FROM TB_TAX_MATRIX 
		left outer join TB_TAXCODE_DETAIL TB_TAXCODE_DETAIL_A on (TB_TAX_MATRIX.then_taxcode_detail_id = tb_taxcode_detail_a.taxcode_detail_id) 
		left outer join TB_TAXCODE_DETAIL TB_TAXCODE_DETAIL_B on (TB_TAX_MATRIX.else_taxcode_detail_id = tb_taxcode_detail_b.taxcode_detail_id) 
		WHERE 1=1 '||p_where_clause_token||'
		GROUP BY matrix_state_code,driver_01,driver_02,driver_03,driver_04,driver_05,
		driver_06,driver_07,driver_08,driver_09, driver_10,driver_11,
		driver_12,driver_13,driver_14,driver_15, driver_16,driver_17,
		driver_18,driver_19,driver_20,driver_21,driver_22,driver_23,
		driver_24,driver_25,driver_26,driver_27,driver_28,driver_29,
		driver_30) driver_group_by ';

	RETURN l_cursor;
END;
/

-- NEEDS TMP TABLE
-- 11 --  
CREATE GLOBAL TEMPORARY TABLE tb_tmp_tax_mtrx_drivers_count (
	DRIVER_01                                          NUMBER,
	DRIVER_02                                          NUMBER,
	DRIVER_03                                          NUMBER,
	DRIVER_04                                          NUMBER,
	DRIVER_05                                          NUMBER,
	DRIVER_06                                          NUMBER,
	DRIVER_07                                          NUMBER,
	DRIVER_08                                          NUMBER,
	DRIVER_09                                          NUMBER,
	DRIVER_10                                          NUMBER,
	DRIVER_11                                          NUMBER,
	DRIVER_12                                          NUMBER,
	DRIVER_13                                          NUMBER,
	DRIVER_14                                          NUMBER,
	DRIVER_15                                          NUMBER,
	DRIVER_16                                          NUMBER,
	DRIVER_17                                          NUMBER,
	DRIVER_18                                          NUMBER,
	DRIVER_19                                          NUMBER,
	DRIVER_20                                          NUMBER,
	DRIVER_21                                          NUMBER,
	DRIVER_22                                          NUMBER,
	DRIVER_23                                          NUMBER,
	DRIVER_24                                          NUMBER,
	DRIVER_25                                          NUMBER,
	DRIVER_26                                          NUMBER,
	DRIVER_27                                          NUMBER,
	DRIVER_28                                          NUMBER,
	DRIVER_29                                          NUMBER,
	DRIVER_30                                          NUMBER
) ON COMMIT PRESERVE ROWS;

CREATE OR REPLACE PROCEDURE SP_TAX_MTRX_DRIVERS_COUNT (  
	p_where_clause_token IN VARCHAR2
)
AS
BEGIN
	 EXECUTE IMMEDIATE 
	 'INSERT INTO tb_tmp_tax_mtrx_drivers_count
	 SELECT * FROM
	 (
		SELECT COUNT (DECODE (driver_01, ''*ALL'', NULL, driver_01)) AS driver_01,
		       COUNT (DECODE (driver_02, ''*ALL'', NULL, driver_02)) AS driver_02,
		       COUNT (DECODE (driver_03, ''*ALL'', NULL, driver_03)) AS driver_03,
		       COUNT (DECODE (driver_04, ''*ALL'', NULL, driver_04)) AS driver_04,
		       COUNT (DECODE (driver_05, ''*ALL'', NULL, driver_05)) AS driver_05,
		       COUNT (DECODE (driver_06, ''*ALL'', NULL, driver_06)) AS driver_06,
		       COUNT (DECODE (driver_07, ''*ALL'', NULL, driver_07)) AS driver_07,
		       COUNT (DECODE (driver_08, ''*ALL'', NULL, driver_08)) AS driver_08,
		       COUNT (DECODE (driver_09, ''*ALL'', NULL, driver_09)) AS driver_09,
		       COUNT (DECODE (driver_10, ''*ALL'', NULL, driver_10)) AS driver_10,
		       COUNT (DECODE (driver_11, ''*ALL'', NULL, driver_11)) AS driver_11,
		       COUNT (DECODE (driver_12, ''*ALL'', NULL, driver_12)) AS driver_12,
		       COUNT (DECODE (driver_13, ''*ALL'', NULL, driver_13)) AS driver_13,
		       COUNT (DECODE (driver_14, ''*ALL'', NULL, driver_14)) AS driver_14,
		       COUNT (DECODE (driver_15, ''*ALL'', NULL, driver_15)) AS driver_15,
		       COUNT (DECODE (driver_16, ''*ALL'', NULL, driver_16)) AS driver_16,
		       COUNT (DECODE (driver_17, ''*ALL'', NULL, driver_17)) AS driver_17,
		       COUNT (DECODE (driver_18, ''*ALL'', NULL, driver_18)) AS driver_18,
		       COUNT (DECODE (driver_19, ''*ALL'', NULL, driver_19)) AS driver_19,
		       COUNT (DECODE (driver_20, ''*ALL'', NULL, driver_20)) AS driver_20,
		       COUNT (DECODE (driver_21, ''*ALL'', NULL, driver_21)) AS driver_21,
		       COUNT (DECODE (driver_22, ''*ALL'', NULL, driver_22)) AS driver_22,
		       COUNT (DECODE (driver_23, ''*ALL'', NULL, driver_23)) AS driver_23,
		       COUNT (DECODE (driver_24, ''*ALL'', NULL, driver_24)) AS driver_24,
		       COUNT (DECODE (driver_25, ''*ALL'', NULL, driver_25)) AS driver_25,
		       COUNT (DECODE (driver_26, ''*ALL'', NULL, driver_26)) AS driver_26,
		       COUNT (DECODE (driver_27, ''*ALL'', NULL, driver_27)) AS driver_27,
		       COUNT (DECODE (driver_28, ''*ALL'', NULL, driver_28)) AS driver_28,
		       COUNT (DECODE (driver_29, ''*ALL'', NULL, driver_29)) AS driver_29,
		       COUNT (DECODE (driver_30, ''*ALL'', NULL, driver_30)) AS driver_30
		  FROM "TB_TAX_MATRIX",
		       "TB_TAXCODE_DETAIL" "TB_TAXCODE_DETAIL_A",
		       "TB_TAXCODE_DETAIL" "TB_TAXCODE_DETAIL_B"
		 WHERE (TB_TAX_MATRIX.then_taxcode_detail_id = tb_taxcode_detail_a.taxcode_detail_id(+))
		   AND (TB_TAX_MATRIX.else_taxcode_detail_id = tb_taxcode_detail_b.taxcode_detail_id(+))
		   AND ( "TB_TAX_MATRIX"."EFFECTIVE_DATE" <= sysdate )
		   AND ( "TB_TAX_MATRIX"."EXPIRATION_DATE" >= sysdate ) '||p_where_clause_token||'
	 ) WHERE 1=1 ';
END;
/

-- 12 -- 
-- DROP PROCEDURE sp_loc_mtrx_tot_mtrx_lines ;
CREATE OR REPLACE FUNCTION sp_loc_mtrx_tot_mtrx_lines (
	p_where_clause_token	IN VARCHAR2
) RETURN sys_refcursor
AS
	l_cursor sys_refcursor;
BEGIN
	OPEN l_cursor FOR
	'SELECT COUNT (*) AS total_matrix_lines FROM TB_LOCATION_MATRIX 
	left outer join TB_JURISDICTION on (TB_LOCATION_MATRIX.JURISDICTION_ID = TB_JURISDICTION.JURISDICTION_ID) '||p_where_clause_token;

	RETURN l_cursor;
END;
/

-- 13 -- 
-- DROP PROCEDURE sp_distinct_loc_mtrx_lines ;
CREATE OR REPLACE FUNCTION sp_distinct_loc_mtrx_lines (
	p_where_clause_token IN VARCHAR2
) RETURN sys_refcursor
AS
	l_cursor sys_refcursor;
BEGIN
	OPEN l_cursor FOR
	'SELECT COUNT (*) AS distinct_matrix_lines 
	FROM (
		SELECT COUNT (*) 
		FROM TB_LOCATION_MATRIX left outer join TB_JURISDICTION 
		on (TB_LOCATION_MATRIX.JURISDICTION_ID = TB_JURISDICTION.JURISDICTION_ID)  
		WHERE 1=1 '||p_where_clause_token||'
		GROUP BY driver_01,driver_02,driver_03,driver_04,driver_05,driver_06,
		driver_07,driver_08,driver_09,driver_10) driver_group_by';

	RETURN l_cursor;
END;
/

-- 14 --
-- DROP PROCEDURE SP_ACTIVE_LOCATION_MTRX_LINES ;
CREATE OR REPLACE FUNCTION SP_ACTIVE_LOCATION_MTRX_LINES (
	p_where_clause_token	IN VARCHAR2,
	p_effective_date	IN DATE,
	p_expiration_date	IN DATE
) RETURN sys_refcursor
AS
	l_cursor sys_refcursor;
BEGIN
	OPEN l_cursor FOR
	'SELECT COUNT (*) AS active_matrix_lines FROM TB_LOCATION_MATRIX left outer join TB_JURISDICTION 
	ON (TB_LOCATION_MATRIX.JURISDICTION_ID = TB_JURISDICTION.JURISDICTION_ID)
	WHERE (TB_LOCATION_MATRIX.EFFECTIVE_DATE  <= SYSDATE) 
	AND (TB_LOCATION_MATRIX.EXPIRATION_DATE >= SYSDATE) '||p_where_clause_token;

	RETURN l_cursor;
END;
/

-- NEEDS TMP TABLE
-- 15 --
CREATE GLOBAL TEMPORARY TABLE tb_tmp_loc_mtrx_drivers_count (
	DRIVER_01                                          NUMBER,
	DRIVER_02                                          NUMBER,
	DRIVER_03                                          NUMBER,
	DRIVER_04                                          NUMBER,
	DRIVER_05                                          NUMBER,
	DRIVER_06                                          NUMBER,
	DRIVER_07                                          NUMBER,
	DRIVER_08                                          NUMBER,
	DRIVER_09                                          NUMBER,
	DRIVER_10                                          NUMBER
) ON COMMIT PRESERVE ROWS;

CREATE OR REPLACE PROCEDURE sp_loc_mtrx_drivers_count (
	p_where_clause_token	IN VARCHAR2
)
AS
BEGIN
	EXECUTE IMMEDIATE 
	'INSERT INTO tb_tmp_loc_mtrx_drivers_count
	SELECT * FROM (
		SELECT COUNT (DECODE (driver_01, ''*ALL'', NULL, driver_01)) AS driver_01,
		       COUNT (DECODE (driver_02, ''*ALL'', NULL, driver_02)) AS driver_02,
		       COUNT (DECODE (driver_03, ''*ALL'', NULL, driver_03)) AS driver_03,
		       COUNT (DECODE (driver_04, ''*ALL'', NULL, driver_04)) AS driver_04,
		       COUNT (DECODE (driver_05, ''*ALL'', NULL, driver_05)) AS driver_05,
		       COUNT (DECODE (driver_06, ''*ALL'', NULL, driver_06)) AS driver_06,
		       COUNT (DECODE (driver_07, ''*ALL'', NULL, driver_07)) AS driver_07,
		       COUNT (DECODE (driver_08, ''*ALL'', NULL, driver_08)) AS driver_08,
		       COUNT (DECODE (driver_09, ''*ALL'', NULL, driver_09)) AS driver_09,
		       COUNT (DECODE (driver_10, ''*ALL'', NULL, driver_10)) AS driver_10
		  FROM TB_LOCATION_MATRIX, TB_JURISDICTION
		 WHERE (TB_LOCATION_MATRIX.JURISDICTION_ID = TB_JURISDICTION.JURISDICTION_ID(+))
		   AND ( "TB_LOCATION_MATRIX"."EFFECTIVE_DATE"  <= sysdate )
		   AND ( "TB_LOCATION_MATRIX"."EXPIRATION_DATE" >= sysdate ) '||p_where_clause_token||' 
	)WHERE 1=1 '; 
END;
/

-- NEEDS TMP TABLE
-- 16 --
CREATE GLOBAL TEMPORARY TABLE tb_tmp_compute_distamt_1 (
 COMPUTE_COUNT                                      NUMBER,
 COMPUTE_DISTAMT                                    NUMBER,
 COMPUTE_TAXAMT                                     NUMBER,
 COMPUTE_EFFTAXRATE                                 NUMBER,
 COMPUTE_MAXDISTAMT                                 NUMBER,
 COMPUTE_AVGDISTAMT                                 NUMBER
) ON COMMIT PRESERVE ROWS;

CREATE OR REPLACE PROCEDURE sp_compute_distamt_1(
	p_group_by		  IN VARCHAR2,
	p_where_clause_token	  IN VARCHAR2
)
AS
BEGIN
  EXECUTE IMMEDIATE '
          INSERT INTO tb_tmp_compute_distamt_1(COMPUTE_COUNT,COMPUTE_DISTAMT,COMPUTE_TAXAMT,COMPUTE_EFFTAXRATE,COMPUTE_MAXDISTAMT,COMPUTE_AVGDISTAMT) 
	  SELECT COMPUTE_COUNT,COMPUTE_DISTAMT,COMPUTE_TAXAMT,COMPUTE_EFFTAXRATE,COMPUTE_MAXDISTAMT,COMPUTE_AVGDISTAMT FROM (
	  SELECT '||p_group_by||',
		COUNT (*) AS COMPUTE_COUNT, 
		SUM (GL_LINE_ITM_DIST_AMT) AS COMPUTE_DISTAMT, 
		SUM (TB_CALC_TAX_AMT) AS COMPUTE_TAXAMT, 
		CASE 
			SUM(GL_LINE_ITM_DIST_AMT) 
			WHEN 0 
			THEN 0
			ELSE (SUM (TB_CALC_TAX_AMT) / SUM (GL_LINE_ITM_DIST_AMT)) 
		END AS  COMPUTE_EFFTAXRATE, 
		MAX (GL_LINE_ITM_DIST_AMT) AS COMPUTE_MAXDISTAMT,
		AVG (GL_LINE_ITM_DIST_AMT) AS COMPUTE_AVGDISTAMT      
	 FROM TB_TRANSACTION_DETAIL 
	 LEFT JOIN TB_JURISDICTION ON (TB_TRANSACTION_DETAIL.jurisdiction_id = TB_JURISDICTION.jurisdiction_id)
	 GROUP BY '||p_group_by||' ) WHERE 1=1 '||p_where_clause_token;
END;
/

-- NEEDS TMP TABLE
-- 17 --
CREATE GLOBAL TEMPORARY TABLE tb_tmp_compute_distamt_2 (
 COMPUTE_COUNT                                      NUMBER,
 COMPUTE_DISTAMT                                    NUMBER,
 COMPUTE_TAXAMT                                     NUMBER,
 COMPUTE_EFFTAXRATE                                 NUMBER,
 COMPUTE_MAXDISTAMT                                 NUMBER,
 COMPUTE_AVGDISTAMT                                 NUMBER
) ON COMMIT PRESERVE ROWS;

CREATE PROCEDURE sp_compute_distamt_2 (
           p_group_by             IN VARCHAR2,
	   p_where_clause_token	  IN VARCHAR2
)
AS
BEGIN

  EXECUTE IMMEDIATE '
          INSERT INTO tb_tmp_compute_distamt_2(COMPUTE_COUNT,COMPUTE_DISTAMT,COMPUTE_TAXAMT,COMPUTE_EFFTAXRATE,COMPUTE_MAXDISTAMT,COMPUTE_AVGDISTAMT) 
	  SELECT COMPUTE_COUNT,COMPUTE_DISTAMT,COMPUTE_TAXAMT,COMPUTE_EFFTAXRATE,COMPUTE_MAXDISTAMT,COMPUTE_AVGDISTAMT FROM (
	  SELECT COUNT (*) AS COMPUTE_COUNT, 
		 SUM (GL_LINE_ITM_DIST_AMT) AS COMPUTE_DISTAMT, 
		 SUM (TB_CALC_TAX_AMT) AS COMPUTE_TAXAMT, 
		 CASE SUM(GL_LINE_ITM_DIST_AMT) WHEN 0 THEN 0 ELSE (SUM (TB_CALC_TAX_AMT) / SUM (GL_LINE_ITM_DIST_AMT)) END AS COMPUTE_EFFTAXRATE, 
		 MAX (GL_LINE_ITM_DIST_AMT) AS COMPUTE_MAXDISTAMT, 
		 AVG (GL_LINE_ITM_DIST_AMT) AS COMPUTE_AVGDISTAMT, 
		'||p_group_by||' 
	  FROM	  TB_TRANSACTION_DETAIL 
	  LEFT JOIN TB_JURISDICTION ON (TB_TRANSACTION_DETAIL.jurisdiction_id = TB_JURISDICTION.jurisdiction_id)
	  GROUP BY '||p_group_by||' ) WHERE 1=1 '||p_where_clause_token;

END;
/
