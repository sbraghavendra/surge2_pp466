
CREATE OR REPLACE PROCEDURE sp_gen_tax_driver (
	p_generate_driver_reference	IN	VARCHAR2,
	p_an_batch_id			IN	NUMBER,
	p_transaction_table_name	IN	VARCHAR2,
	p_tax_table_name		IN	VARCHAR2,
	p_vc_state_driver_flag		IN OUT  CHAR,
	p_vc_tax_matrix_where		IN OUT	VARCHAR2
)
AS
-- Define Tax Driver Names Cursor (tb_driver_names)
   CURSOR tax_driver_cursor
   IS
        SELECT tb_driver_names.trans_dtl_column_name, tb_driver_names.matrix_column_name, tb_driver_names.null_driver_flag, tb_driver_names.wildcard_flag, tb_driver_names.range_flag, tb_driver_names.to_number_flag,
               datadefcol.desc_column_name, datadefcol.data_type
          FROM tb_driver_names,
               (SELECT *
                  FROM tb_data_def_column
                 WHERE table_name = 'TB_TRANSACTION_DETAIL') datadefcol
         WHERE tb_driver_names.driver_names_code = 'T' AND
               tb_driver_names.trans_dtl_column_name = datadefcol.column_name
      ORDER BY tb_driver_names.driver_id;
      tax_driver                   tax_driver_cursor%ROWTYPE;
   
   vi_rows_processed INTEGER := 0;

   vc_driver_ref_ins_stmt          VARCHAR2(5000)              := NULL;
   vi_cursor_id                    INTEGER                     := 0;

BEGIN
   
   -- ***** Create Dynamic WHERE Clause for Matrix SQLs ***** --
   -- Tax Matrix Drivers
   OPEN tax_driver_cursor;
   LOOP
      FETCH tax_driver_cursor INTO tax_driver;
      EXIT WHEN tax_driver_cursor%NOTFOUND;
      IF tax_driver.range_flag = '1' THEN
         IF tax_driver.to_number_flag = '1' THEN
            IF tax_driver.null_driver_flag = '1' THEN
               -- Range and ToNumber and NULL
               IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                  UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                  p_vc_tax_matrix_where := p_vc_tax_matrix_where || 'AND ( ' ||
                  '( UPPER('|| p_tax_table_name ||'.' || tax_driver.matrix_column_name || '_THRU) = ''*NA'' AND ' ||
                     '( '|| p_transaction_table_name ||'.' || tax_driver.trans_dtl_column_name || ' IS NULL AND ( UPPER('|| p_tax_table_name ||'.' || tax_driver.matrix_column_name || ') = ''*ALL'' OR UPPER('|| p_tax_table_name ||'.' || tax_driver.matrix_column_name || ') = ''*NULL'' )) OR ' ||
                     '( '|| p_transaction_table_name ||'.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND ( UPPER('|| p_tax_table_name ||'.' || tax_driver.matrix_column_name || ') = ''*ALL'' OR DECODE(isnumber('|| p_transaction_table_name ||'.' || tax_driver.trans_dtl_column_name || '),1,to_char('|| p_transaction_table_name ||'.' || tax_driver.trans_dtl_column_name || ',''00000000000000000000.00000000000000000000''),UPPER('|| p_transaction_table_name ||'.' || tax_driver.trans_dtl_column_name || ')) = DECODE(isnumber(UPPER('|| p_tax_table_name ||'.' || tax_driver.matrix_column_name || ')),1,to_char(UPPER('|| p_tax_table_name ||'.' || tax_driver.matrix_column_name || '),''00000000000000000000.00000000000000000000''),UPPER('|| p_tax_table_name ||'.' || tax_driver.matrix_column_name || ') ))) OR ' ||
                  '( UPPER('|| p_tax_table_name ||'.' || tax_driver.matrix_column_name || '_THRU) <> ''*NA'' AND ' ||
                     '( '|| p_transaction_table_name ||'.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND (DECODE(isnumber('|| p_transaction_table_name ||'.' || tax_driver.trans_dtl_column_name || '),1,to_char('|| p_transaction_table_name ||'.' || tax_driver.trans_dtl_column_name || ',''00000000000000000000.00000000000000000000''),UPPER('|| p_transaction_table_name ||'.' || tax_driver.trans_dtl_column_name || ')) >= DECODE(isnumber(UPPER('|| p_tax_table_name ||'.' || tax_driver.matrix_column_name || ')),1,to_char(UPPER('|| p_tax_table_name ||'.' || tax_driver.matrix_column_name || '),''00000000000000000000.00000000000000000000''),UPPER('|| p_tax_table_name ||'.' || tax_driver.matrix_column_name || ') ) AND ' ||
                                                                                                       'DECODE(isnumber('|| p_transaction_table_name ||'.' || tax_driver.trans_dtl_column_name || '),1,to_char('|| p_transaction_table_name ||'.' || tax_driver.trans_dtl_column_name || ',''00000000000000000000.00000000000000000000''),UPPER('|| p_transaction_table_name ||'.' || tax_driver.trans_dtl_column_name || ')) <= DECODE(isnumber(UPPER('|| p_tax_table_name ||'.' || tax_driver.matrix_column_name || '_THRU)),1,to_char(UPPER('|| p_tax_table_name ||'.' || tax_driver.matrix_column_name || '_THRU),''00000000000000000000.00000000000000000000''),UPPER('|| p_tax_table_name ||'.' || tax_driver.matrix_column_name || '_THRU) ))))) ';
               END IF;
            ELSE
               -- Range and ToNumber and NOT NULL
               IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                  UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                  p_vc_tax_matrix_where := p_vc_tax_matrix_where || 'AND ( ' ||
                  '( UPPER('|| p_tax_table_name ||'.' || tax_driver.matrix_column_name || '_THRU) = ''*NA'' AND (UPPER('|| p_tax_table_name ||'.' || tax_driver.matrix_column_name || ') = ''*ALL'' OR DECODE(isnumber('|| p_transaction_table_name ||'.' || tax_driver.trans_dtl_column_name || '),1,to_char('|| p_transaction_table_name ||'.' || tax_driver.trans_dtl_column_name || ',''00000000000000000000.00000000000000000000''),UPPER('|| p_transaction_table_name ||'.' || tax_driver.trans_dtl_column_name || ')) = DECODE(isnumber(UPPER('|| p_tax_table_name ||'.' || tax_driver.matrix_column_name || ')),1,to_char(UPPER('|| p_tax_table_name ||'.' || tax_driver.matrix_column_name || '),''00000000000000000000.00000000000000000000''),UPPER('|| p_tax_table_name ||'.' || tax_driver.matrix_column_name || ')))) OR ' ||
                  '( UPPER('|| p_tax_table_name ||'.' || tax_driver.matrix_column_name || '_THRU) <> ''*NA'' AND (DECODE(isnumber('|| p_transaction_table_name ||'.' || tax_driver.trans_dtl_column_name || '),1,to_char('|| p_transaction_table_name ||'.' || tax_driver.trans_dtl_column_name || ',''00000000000000000000.00000000000000000000''),UPPER('|| p_transaction_table_name ||'.' || tax_driver.trans_dtl_column_name || ')) >= DECODE(isnumber(UPPER('|| p_tax_table_name ||'.' || tax_driver.matrix_column_name || ')),1,to_char(UPPER('|| p_tax_table_name ||'.' || tax_driver.matrix_column_name || '),''00000000000000000000.00000000000000000000''),UPPER('|| p_tax_table_name ||'.' || tax_driver.matrix_column_name || ')) AND ' ||
                                                                                        'DECODE(isnumber('|| p_transaction_table_name ||'.' || tax_driver.trans_dtl_column_name || '),1,to_char('|| p_transaction_table_name ||'.' || tax_driver.trans_dtl_column_name || ',''00000000000000000000.00000000000000000000''),UPPER('|| p_transaction_table_name ||'.' || tax_driver.trans_dtl_column_name || ')) <= DECODE(isnumber(UPPER('|| p_tax_table_name ||'.' || tax_driver.matrix_column_name || '_THRU )),1,to_char(UPPER('|| p_tax_table_name ||'.' || tax_driver.matrix_column_name || '_THRU),''00000000000000000000.00000000000000000000''),UPPER('|| p_tax_table_name ||'.' || tax_driver.matrix_column_name || '_THRU) ))) ';
               END IF;
            END IF;
         ELSE
            IF tax_driver.null_driver_flag = '1' THEN
               IF tax_driver.wildcard_flag = '1' THEN
                  -- Range and NOT ToNumber and NULL and Wildcard
                  IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                     UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                     p_vc_tax_matrix_where := p_vc_tax_matrix_where || 'AND ( ' ||
                     '( UPPER('|| p_tax_table_name ||'.' || tax_driver.matrix_column_name || '_THRU) = ''*NA'' AND ' ||
                        '( '|| p_transaction_table_name ||'.' || tax_driver.trans_dtl_column_name || ' IS NULL AND (UPPER('|| p_tax_table_name ||'.' || tax_driver.matrix_column_name || ') = ''*ALL'' OR UPPER('|| p_tax_table_name ||'.' || tax_driver.matrix_column_name || ') = ''*NULL'' )) OR ' ||
                        '( '|| p_transaction_table_name ||'.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND (UPPER('|| p_tax_table_name ||'.' || tax_driver.matrix_column_name || ') = ''*ALL'' OR UPPER('|| p_transaction_table_name ||'.' || tax_driver.trans_dtl_column_name || ') LIKE UPPER('|| p_tax_table_name ||'.' || tax_driver.matrix_column_name || ')))) OR ' ||
                     '( UPPER('|| p_tax_table_name ||'.' || tax_driver.matrix_column_name || '_THRU) <> ''*NA'' AND ('|| p_transaction_table_name ||'.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND (UPPER('|| p_transaction_table_name ||'.' || tax_driver.trans_dtl_column_name || ') >= UPPER('|| p_tax_table_name ||'.' || tax_driver.matrix_column_name || ') AND UPPER('|| p_transaction_table_name ||'.' || tax_driver.trans_dtl_column_name || ') <= UPPER('|| p_tax_table_name ||'.' || tax_driver.matrix_column_name || '_THRU) ))) ';
                  END IF;
               ELSE
                  -- Range and NOT ToNumber and NULL and NOT Wildcard
                  IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                     UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                     p_vc_tax_matrix_where := p_vc_tax_matrix_where || 'AND ( ' ||
                     '( UPPER('|| p_tax_table_name ||'.' || tax_driver.matrix_column_name || '_THRU) = ''*NA'' AND ' ||
                        '( '|| p_transaction_table_name ||'.' || tax_driver.trans_dtl_column_name || ' IS NULL AND (UPPER('|| p_tax_table_name ||'.' || tax_driver.matrix_column_name || ') = ''*ALL'' OR UPPER('|| p_tax_table_name ||'.' || tax_driver.matrix_column_name || ') = ''*NULL'' )) OR ' ||
                        '( '|| p_transaction_table_name ||'.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND (UPPER('|| p_tax_table_name ||'.' || tax_driver.matrix_column_name || ') = ''*ALL'' OR UPPER('|| p_transaction_table_name ||'.' || tax_driver.trans_dtl_column_name || ') = UPPER('|| p_tax_table_name ||'.' || tax_driver.matrix_column_name || ')))) OR ' ||
                     '( UPPER('|| p_tax_table_name ||'.' || tax_driver.matrix_column_name || '_THRU) <> ''*NA'' AND ('|| p_transaction_table_name ||'.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND (UPPER('|| p_transaction_table_name ||'.' || tax_driver.trans_dtl_column_name || ') >= UPPER('|| p_tax_table_name ||'.' || tax_driver.matrix_column_name || ') AND UPPER('|| p_transaction_table_name ||'.' || tax_driver.trans_dtl_column_name || ') <= UPPER('|| p_tax_table_name ||'.' || tax_driver.matrix_column_name || '_THRU) ))) ';
                  END IF;
               END IF;
            ELSE
               IF tax_driver.wildcard_flag = '1' THEN
                  -- Range and NOT ToNumber and NOT NULL and Wildcard
                  IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                     UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                     p_vc_tax_matrix_where := p_vc_tax_matrix_where || 'AND ( ' ||
                     '( UPPER('|| p_tax_table_name ||'.' || tax_driver.matrix_column_name || '_THRU) = ''*NA'' AND ' ||
                        '(UPPER('|| p_tax_table_name ||'.' || tax_driver.matrix_column_name || ') = ''*ALL'' OR UPPER('|| p_transaction_table_name ||'.' || tax_driver.trans_dtl_column_name || ') LIKE UPPER('|| p_tax_table_name ||'.' || tax_driver.matrix_column_name || '))) OR ' ||
                     '( UPPER('|| p_tax_table_name ||'.' || tax_driver.matrix_column_name || '_THRU) <> ''*NA'' AND (UPPER('|| p_transaction_table_name ||'.' || tax_driver.trans_dtl_column_name || ') >= UPPER('|| p_tax_table_name ||'.' || tax_driver.matrix_column_name || ') AND UPPER('|| p_transaction_table_name ||'.' || tax_driver.trans_dtl_column_name || ') <= UPPER('|| p_tax_table_name ||'.' || tax_driver.matrix_column_name || '_THRU) )) ';
                  END IF;
               ELSE
                  -- Range and NOT ToNumber and NOT NULL and NOT Wildcard
                  IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                     UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                     p_vc_tax_matrix_where := p_vc_tax_matrix_where || 'AND ( ' ||
                     '( UPPER('|| p_tax_table_name ||'.' || tax_driver.matrix_column_name || '_THRU) = ''*NA'' AND ' ||
                        '(UPPER('|| p_tax_table_name ||'.' || tax_driver.matrix_column_name || ') = ''*ALL'' OR UPPER('|| p_transaction_table_name ||'.' || tax_driver.trans_dtl_column_name || ') = UPPER('|| p_tax_table_name ||'.' || tax_driver.matrix_column_name || '))) OR ' ||
                     '( UPPER('|| p_tax_table_name ||'.' || tax_driver.matrix_column_name || '_THRU) <> ''*NA'' AND (UPPER('|| p_transaction_table_name ||'.' || tax_driver.trans_dtl_column_name || ') >= UPPER('|| p_tax_table_name ||'.' || tax_driver.matrix_column_name || ') AND UPPER('|| p_transaction_table_name ||'.' || tax_driver.trans_dtl_column_name || ') <= UPPER('|| p_tax_table_name ||'.' || tax_driver.matrix_column_name || '_THRU) )) ';
                  END IF;
               END IF;
            END IF;
         END IF;
      ELSE
         IF tax_driver.null_driver_flag = '1' THEN
            IF tax_driver.wildcard_flag = '1' THEN
               -- NOT Range and NULL and Wildcard
               IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                  UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                     p_vc_tax_matrix_where := p_vc_tax_matrix_where || 'AND ( ' ||
                     '(( '|| p_transaction_table_name ||'.' || tax_driver.trans_dtl_column_name || ' IS NULL AND (UPPER('|| p_tax_table_name ||'.' || tax_driver.matrix_column_name || ') = ''*ALL'' OR UPPER('|| p_tax_table_name ||'.' || tax_driver.matrix_column_name || ') = ''*NULL'' )) OR ' ||
                     '( '|| p_transaction_table_name ||'.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND (UPPER('|| p_tax_table_name ||'.' || tax_driver.matrix_column_name || ') = ''*ALL'' OR UPPER('|| p_transaction_table_name ||'.' || tax_driver.trans_dtl_column_name || ') LIKE UPPER('|| p_tax_table_name ||'.' || tax_driver.matrix_column_name || ')))) ';
               END IF;
            ELSE
               -- NOT Range and NULL and NOT Wildcard
               IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                  UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                     p_vc_tax_matrix_where := p_vc_tax_matrix_where || 'AND ( ' ||
                     '(( '|| p_transaction_table_name ||'.' || tax_driver.trans_dtl_column_name || ' IS NULL AND (UPPER('|| p_tax_table_name ||'.' || tax_driver.matrix_column_name || ') = ''*ALL'' OR UPPER('|| p_tax_table_name ||'.' || tax_driver.matrix_column_name || ') = ''*NULL'' )) OR ' ||
                     '( '|| p_transaction_table_name ||'.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND (UPPER('|| p_tax_table_name ||'.' || tax_driver.matrix_column_name || ') = ''*ALL'' OR UPPER('|| p_transaction_table_name ||'.' || tax_driver.trans_dtl_column_name || ') = UPPER('|| p_tax_table_name ||'.' || tax_driver.matrix_column_name || ')))) ';
               END IF;
            END IF;
         ELSE
            IF tax_driver.wildcard_flag = '1' THEN
               -- NOT Range and NOT NULL and Wildcard
               IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                  UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                     p_vc_tax_matrix_where := p_vc_tax_matrix_where || 'AND ( ' ||
                     '( UPPER('|| p_tax_table_name ||'.' || tax_driver.matrix_column_name || ') = ''*ALL'' OR UPPER('|| p_transaction_table_name ||'.' || tax_driver.trans_dtl_column_name || ') LIKE UPPER('|| p_tax_table_name ||'.' || tax_driver.matrix_column_name || ')) ';
               END IF;
            ELSE
               -- NOT Range and NOT NULL and NOT Wildcard
               IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                  UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                  IF tax_driver.trans_dtl_column_name = 'TRANSACTION_STATE_CODE' THEN
                     IF p_vc_state_driver_flag <> '1' THEN
                        p_vc_state_driver_flag := '1';
                     END IF;
                     p_vc_tax_matrix_where := p_vc_tax_matrix_where || 'AND ( ' ||
                     '( UPPER('|| p_tax_table_name ||'.' || tax_driver.matrix_column_name || ') = ''*ALL'' OR UPPER(:v_transaction_state_code) = UPPER('|| p_tax_table_name ||'.' || tax_driver.matrix_column_name || ')) ';
                  ELSE
                     p_vc_tax_matrix_where := p_vc_tax_matrix_where || 'AND ( ' ||
                     '( UPPER('|| p_tax_table_name ||'.' || tax_driver.matrix_column_name || ') = ''*ALL'' OR UPPER('|| p_transaction_table_name ||'.' || tax_driver.trans_dtl_column_name || ') = UPPER('|| p_tax_table_name ||'.' || tax_driver.matrix_column_name || ')) ';
                  END IF;
               END IF;
            END IF;
         END IF;
      END IF;
      p_vc_tax_matrix_where := p_vc_tax_matrix_where || ') ';
      
      -- Check to confirm whether we're running driver reference portion. 
      -- NOTE: This is done ONLY for Batch Processing.
      IF (p_generate_driver_reference = 'Y') THEN

	      -- Insert Driver Reference Values for this driver
	      -- Create Insert SQL
	      vc_driver_ref_ins_stmt :=
		 'INSERT INTO tb_driver_reference (trans_dtl_column_name, driver_value, driver_description) ' ||
		    'SELECT ''' || Upper(tax_driver.trans_dtl_column_name) ||
			    ''', ' || tax_driver.trans_dtl_column_name || ', ';
	      IF tax_driver.desc_column_name IS NOT NULL THEN
		 vc_driver_ref_ins_stmt := vc_driver_ref_ins_stmt || 'min(' || tax_driver.desc_column_name || ') ';
	      ELSE
		 vc_driver_ref_ins_stmt := vc_driver_ref_ins_stmt || 'NULL ';
	      END IF;
	      vc_driver_ref_ins_stmt := vc_driver_ref_ins_stmt ||
		      'FROM tb_bcp_transactions ' ||
		     'WHERE process_batch_no = ' || TO_CHAR(p_an_batch_id) || ' AND ' ||
			    tax_driver.trans_dtl_column_name || ' IS NOT NULL AND ' ||
			    tax_driver.trans_dtl_column_name || ' NOT IN ' ||
		       '(SELECT driver_value ' ||
			  'FROM tb_driver_reference ' ||
			 'WHERE trans_dtl_column_name = ''' || Upper(tax_driver.trans_dtl_column_name) || ''') ' ||
		      'GROUP BY ''' || Upper(tax_driver.trans_dtl_column_name) ||
				''', ' || tax_driver.trans_dtl_column_name;
	      -- Execute Insert SQL
	      vi_cursor_id := DBMS_SQL.open_cursor;
	      DBMS_SQL.parse (vi_cursor_id, vc_driver_ref_ins_stmt, DBMS_SQL.v7);
	      vi_rows_processed := DBMS_SQL.execute (vi_cursor_id);
	      DBMS_SQL.close_cursor (vi_cursor_id);
      END IF;

   END LOOP;
   CLOSE tax_driver_cursor;
      
END;
/
