CREATE OR REPLACE PROCEDURE sp_max_tb_bcp_juris_taxrate_id
IS
   v_max_id number;
   v_count number;
BEGIN
   SELECT MAX( bcp_jurisdiction_taxrate_id )
   INTO v_max_id
   FROM tb_bcp_jurisdiction_taxrate;

   IF v_max_id IS NULL THEN
      v_max_id := 1;
   ELSE
      v_max_id := v_max_id + 1;
   END IF;

   SELECT count(*)
   INTO v_count
   FROM all_sequences
   WHERE sequence_name = 'SQ_TB_BCP_JURIS_TAXRATE_ID'
   AND sequence_owner = 'STSCORP';

   IF v_count > 0 THEN
      EXECUTE IMMEDIATE 'DROP SEQUENCE STSCORP.sq_tb_bcp_juris_taxrate_id';
   END IF;

   EXECUTE IMMEDIATE 'CREATE SEQUENCE STSCORP.sq_tb_bcp_juris_taxrate_id INCREMENT BY 1 START WITH ' ||
   v_max_id || ' MAXVALUE 1.0E27 MINVALUE 1 NOCYCLE NOCACHE NOORDER';

   EXECUTE IMMEDIATE 'GRANT SELECT ON STSCORP.sq_tb_bcp_juris_taxrate_id TO STSUSER';
END;
/

