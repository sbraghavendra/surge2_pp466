CREATE OR REPLACE PROCEDURE sp_max_all
IS
-- Program starts **********************************************************************************
BEGIN
   sp_max_tb_allocation_matrix_id();
   sp_max_gl_extract_batch_id();
   sp_max_tb_batch_error_log_id();
   sp_max_tb_batch_id();
   sp_max_tb_entity_id();
   sp_max_tb_jurisdiction_id();
   sp_max_tb_juris_taxrate_id();
   sp_max_tb_location_matrix_id();
 --sp_max_tb_questans_log_id();
   sp_max_tb_taxcode_detail_id();
   sp_max_tb_tax_matrix_id();
   sp_max_tb_trans_detail_id();
   sp_max_tb_trans_detail_excp_id();
   sp_max_tb_bcp_juris_taxrate_id();
END;
/