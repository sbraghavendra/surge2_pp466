
-- 1 --
CREATE OR REPLACE PROCEDURE sp_tb_jurisdiction_taxrate_a_i(   
p_jurisdiction_taxrate_id IN number
--p_tb_jurisdiction_taxrate_type IN tb_jurisdiction_taxrate%ROWTYPE
)
AS
   -- Table defined variables
   v_sysdate                       tb_transaction_detail.load_timestamp%TYPE         := SYSDATE;
   v_taxtype_code                  tb_taxcode.taxtype_code%TYPE                      := NULL;
   v_override_taxtype_code         tb_taxcode.taxtype_code%TYPE                      := NULL;
   v_taxtype_used                  tb_taxcode.taxtype_code%TYPE                      := NULL;
   v_state_flag                    tb_location_matrix.state_flag%TYPE                := '0';
   v_county_flag                   tb_location_matrix.county_flag%TYPE               := '0';
   v_county_local_flag             tb_location_matrix.county_local_flag%TYPE         := '0';
   v_city_flag                     tb_location_matrix.city_flag%TYPE                 := '0';
   v_city_local_flag               tb_location_matrix.city_local_flag%TYPE           := '0';

   -- temporary
   vn_taxable_amt                  NUMBER                                            := 0;
   
   l_tb_jurisdiction_taxrate_type tb_jurisdiction_taxrate%ROWTYPE;

   -- Define Exceptions
   --e_badupdate                     exception;

-- ****** Program starts ****** --------------------------------------------------------------------
BEGIN
   
   SELECT	* 
		INTO l_tb_jurisdiction_taxrate_type 
   FROM		tb_jurisdiction_taxrate
   WHERE	jurisdiction_taxrate_id = p_jurisdiction_taxrate_id;
   
   DECLARE
	   -- Define Transaction Detail Cursor (tb_transaction_detail)
	   CURSOR transaction_detail_cursor
	   IS
	      SELECT tb_transaction_detail.*
		FROM tb_transaction_detail
	       WHERE ( tb_transaction_detail.jurisdiction_id = l_tb_jurisdiction_taxrate_type.jurisdiction_id ) AND
		     ( l_tb_jurisdiction_taxrate_type.effective_date <= tb_transaction_detail.gl_date ) AND ( l_tb_jurisdiction_taxrate_type.expiration_date >= tb_transaction_detail.gl_date ) AND
		     ( l_tb_jurisdiction_taxrate_type.measure_type_code = tb_transaction_detail.measure_type_code ) AND
		     ( tb_transaction_detail.transaction_ind = 'S' ) AND ( tb_transaction_detail.suspend_ind = 'R' )
	      FOR UPDATE;
	      transaction_detail           transaction_detail_cursor%ROWTYPE;
   BEGIN
	   -- ****** Read and Process Transactions ****** -----------------------------------------
	   OPEN transaction_detail_cursor;
	   LOOP
	      FETCH transaction_detail_cursor
	       INTO transaction_detail;
	       EXIT WHEN transaction_detail_cursor%NOTFOUND;

	      -- Get Tax Type Codes
	      SELECT taxtype_code
		INTO v_taxtype_code
		FROM tb_taxcode
	       WHERE taxcode_code = transaction_detail.taxcode_code
		 AND taxcode_type_code = transaction_detail.taxcode_type_code;

	      -- Get Location Matrix Codes if NOT applied or overriden          -- MBF01
	      IF (transaction_detail.location_matrix_id IS NOT NULL AND transaction_detail.location_matrix_id <> 0) AND
		 (transaction_detail.manual_jurisdiction_ind IS NULL) THEN
		 -- Get Override Tax Type Code -- 3351
		 -- Get 5 Tax Calc Flags -- 3411 -- MBF02
		 SELECT override_taxtype_code,
			state_flag, county_flag, county_local_flag, city_flag, city_local_flag
		   INTO v_override_taxtype_code,
			v_state_flag,
			v_county_flag,
			v_county_local_flag,
			v_city_flag,
			v_city_local_flag
		   FROM tb_location_matrix
		  WHERE tb_location_matrix.location_matrix_id = transaction_detail.location_matrix_id;
	      ELSE
		 v_override_taxtype_code := '*NO';
		 v_state_flag := '1';                           -- MBF01
		 v_county_flag := '1';                          -- MBF01
		 v_county_local_flag := '1';                    -- MBF01
		 v_city_flag := '1';                            -- MBF01
		 v_city_local_flag := '1';                      -- MBF01
	      END IF;

	      -- Determine Tax Type - Use or Sales
	      v_taxtype_used := v_override_taxtype_code;
	      IF v_taxtype_used is NULL OR TRIM(v_taxtype_used) = '' OR TRIM(v_taxtype_used) = '*NO' THEN
		 v_taxtype_used := SUBSTR(TRIM(v_taxtype_code),1,1);
		 IF v_taxtype_used is NULL or Trim(v_taxtype_used) = '' THEN
		    v_taxtype_used := 'U';
		 END IF;
	      END IF;

	      -- Populate and/or Clear Transaction Detail working fields
	      transaction_detail.state_use_amount := 0;
	      transaction_detail.state_use_tier2_amount := 0;
	      transaction_detail.state_use_tier3_amount := 0;
	      transaction_detail.county_use_amount := 0;
	      transaction_detail.county_local_use_amount := 0;
	      transaction_detail.city_use_amount := 0;
	      transaction_detail.city_local_use_amount := 0;
	      transaction_detail.tb_calc_tax_amt := 0;
	      transaction_detail.gl_extract_amt := 0;
	-- future? --      transaction_detail.taxable_amt := 0;
	-- future? --      transactoin_detail.taxtype_used := v_taxtype_used;
	      transaction_detail.update_user_id := USER;
	      transaction_detail.update_timestamp := v_sysdate;

	      -- Move Rates to parameters
	      transaction_detail.transaction_ind := 'P';
	      transaction_detail.suspend_ind := NULL;
	      transaction_detail.jurisdiction_id := l_tb_jurisdiction_taxrate_type.jurisdiction_id;
	      transaction_detail.jurisdiction_taxrate_id := l_tb_jurisdiction_taxrate_type.jurisdiction_taxrate_id;
	      transaction_detail.county_split_amount := l_tb_jurisdiction_taxrate_type.county_split_amount;
	      transaction_detail.county_maxtax_amount := l_tb_jurisdiction_taxrate_type.county_maxtax_amount;
	      transaction_detail.county_single_flag := l_tb_jurisdiction_taxrate_type.county_single_flag;
	      transaction_detail.county_default_flag := l_tb_jurisdiction_taxrate_type.county_default_flag;
	      transaction_detail.city_split_amount := l_tb_jurisdiction_taxrate_type.city_split_amount;
	      transaction_detail.city_single_flag := l_tb_jurisdiction_taxrate_type.city_single_flag;
	      transaction_detail.city_default_flag := l_tb_jurisdiction_taxrate_type.city_default_flag;

	      IF v_taxtype_used = 'U' THEN
		 -- Use Tax Rates
		 transaction_detail.state_use_rate := l_tb_jurisdiction_taxrate_type.state_use_rate;
		 transaction_detail.state_use_tier2_rate := l_tb_jurisdiction_taxrate_type.state_use_tier2_rate;
		 transaction_detail.state_use_tier3_rate := l_tb_jurisdiction_taxrate_type.state_use_tier3_rate;
		 transaction_detail.state_split_amount := l_tb_jurisdiction_taxrate_type.state_split_amount;
		 transaction_detail.state_tier2_min_amount := l_tb_jurisdiction_taxrate_type.state_tier2_min_amount;
		 transaction_detail.state_tier2_max_amount := l_tb_jurisdiction_taxrate_type.state_tier2_max_amount;
		 transaction_detail.state_maxtax_amount := l_tb_jurisdiction_taxrate_type.state_maxtax_amount;
		 transaction_detail.county_use_rate := l_tb_jurisdiction_taxrate_type.county_use_rate;
		 transaction_detail.county_local_use_rate := l_tb_jurisdiction_taxrate_type.county_local_use_rate;
		 transaction_detail.city_use_rate := l_tb_jurisdiction_taxrate_type.city_use_rate;
		 transaction_detail.city_local_use_rate := l_tb_jurisdiction_taxrate_type.city_local_use_rate;
		 transaction_detail.city_split_use_rate := l_tb_jurisdiction_taxrate_type.city_split_use_rate;
	      ELSIF v_taxtype_used = 'S' THEN
		 -- Sales Tax Rates
		 transaction_detail.state_use_rate := l_tb_jurisdiction_taxrate_type.state_sales_rate;
		 transaction_detail.state_use_tier2_rate := 0;
		 transaction_detail.state_use_tier3_rate := 0;
		 transaction_detail.state_split_amount := 0;
		 transaction_detail.state_tier2_min_amount := 0;
		 transaction_detail.state_tier2_max_amount := 999999999;
		 transaction_detail.state_maxtax_amount := 999999999;
		 transaction_detail.county_use_rate := l_tb_jurisdiction_taxrate_type.county_sales_rate;
		 transaction_detail.county_local_use_rate := l_tb_jurisdiction_taxrate_type.county_local_sales_rate;
		 transaction_detail.city_use_rate := l_tb_jurisdiction_taxrate_type.city_sales_rate;
		 transaction_detail.city_local_use_rate := l_tb_jurisdiction_taxrate_type.city_local_sales_rate;
		 transaction_detail.city_split_use_rate := l_tb_jurisdiction_taxrate_type.city_split_sales_rate;
	      ELSE
		 -- Unknown!
		 transaction_detail.state_use_rate := 0;
		 transaction_detail.state_use_tier2_rate := 0;
		 transaction_detail.state_use_tier3_rate := 0;
		 transaction_detail.state_split_amount := 0;
		 transaction_detail.state_tier2_min_amount := 0;
		 transaction_detail.state_tier2_max_amount := 999999999;
		 transaction_detail.state_maxtax_amount := 999999999;
		 transaction_detail.county_use_rate := 0;
		 transaction_detail.county_local_use_rate := 0;
		 transaction_detail.city_use_rate := 0;
		 transaction_detail.city_local_use_rate := 0;
		 transaction_detail.city_split_use_rate := 0;
	      END IF;

	      -- Get Jurisdiction TaxRates and calculate Tax Amounts
	      sp_calcusetax(transaction_detail.gl_line_itm_dist_amt,
			    transaction_detail.invoice_freight_amt,
			    transaction_detail.invoice_discount_amt,
			    transaction_detail.transaction_state_code,
	-- future? --             transaction_detail.import_definition_code,
			    'importdefn',
			    transaction_detail.taxcode_code,
			    v_state_flag,
			    v_county_flag,
			    v_county_local_flag,
			    v_city_flag,
			    v_city_local_flag,
			    transaction_detail.state_use_rate,
			    transaction_detail.state_use_tier2_rate,
			    transaction_detail.state_use_tier3_rate,
			    transaction_detail.state_split_amount,
			    transaction_detail.state_tier2_min_amount,
			    transaction_detail.state_tier2_max_amount,
			    transaction_detail.state_maxtax_amount,
			    transaction_detail.county_use_rate,
			    transaction_detail.county_local_use_rate,
			    transaction_detail.county_split_amount,
			    transaction_detail.county_maxtax_amount,
			    transaction_detail.county_single_flag,
			    transaction_detail.county_default_flag,
			    transaction_detail.city_use_rate,
			    transaction_detail.city_local_use_rate,
			    transaction_detail.city_split_amount,
			    transaction_detail.city_split_use_rate,
			    transaction_detail.city_single_flag,
			    transaction_detail.city_default_flag,
			    transaction_detail.state_use_amount,
			    transaction_detail.state_use_tier2_amount,
			    transaction_detail.state_use_tier3_amount,
			    transaction_detail.county_use_amount,
			    transaction_detail.county_local_use_amount,
			    transaction_detail.city_use_amount,
			    transaction_detail.city_local_use_amount,
			    transaction_detail.tb_calc_tax_amt,
	-- future? --             transaction_detail.taxable_amt);
	-- Targa? --            transaction_detail.user_number_10 );
			    vn_taxable_amt );

	      transaction_detail.combined_use_rate := transaction_detail.state_use_rate +
						      transaction_detail.county_use_rate +
						      transaction_detail.county_local_use_rate +
						      transaction_detail.city_use_rate +
						      transaction_detail.city_local_use_rate;

	      -- Check for Taxable Amount -- 3351
	      IF vn_taxable_amt  <> transaction_detail.gl_line_itm_dist_amt THEN
		 transaction_detail.gl_extract_amt := transaction_detail.gl_line_itm_dist_amt;
		 transaction_detail.gl_line_itm_dist_amt := vn_taxable_amt;
	      END IF;

	      -- Update transaction detail row
	      BEGIN
		 -- 3411 - add taxable amount and tax type used.
		 UPDATE tb_transaction_detail
		    SET gl_line_itm_dist_amt = transaction_detail.gl_line_itm_dist_amt,
			tb_calc_tax_amt = transaction_detail.tb_calc_tax_amt,
			state_use_amount = transaction_detail.state_use_amount,
			state_use_tier2_amount = transaction_detail.state_use_tier2_amount,
			state_use_tier3_amount = transaction_detail.state_use_tier3_amount,
			county_use_amount = transaction_detail.county_use_amount,
			county_local_use_amount = transaction_detail.county_local_use_amount,
			city_use_amount = transaction_detail.city_use_amount,
			city_local_use_amount = transaction_detail.city_local_use_amount,
			transaction_ind = transaction_detail.transaction_ind,
			suspend_ind = transaction_detail.suspend_ind,
			jurisdiction_id = transaction_detail.jurisdiction_id,
			jurisdiction_taxrate_id = transaction_detail.jurisdiction_taxrate_id,
			state_use_rate = transaction_detail.state_use_rate,
			state_use_tier2_rate = transaction_detail.state_use_tier2_rate,
			state_use_tier3_rate = transaction_detail.state_use_tier3_rate,
			state_split_amount = transaction_detail.state_split_amount,
			state_tier2_min_amount = transaction_detail.state_tier2_min_amount,
			state_tier2_max_amount = transaction_detail.state_tier2_max_amount,
			state_maxtax_amount = transaction_detail.state_maxtax_amount,
			county_use_rate = transaction_detail.county_use_rate,
			county_local_use_rate = transaction_detail.county_local_use_rate,
			county_split_amount = transaction_detail.county_split_amount,
			county_maxtax_amount = transaction_detail.county_maxtax_amount,
			county_single_flag = transaction_detail.county_single_flag,
			county_default_flag = transaction_detail.county_default_flag,
			city_use_rate = transaction_detail.city_use_rate,
			city_local_use_rate = transaction_detail.city_local_use_rate,
			city_split_amount = transaction_detail.city_split_amount,
			city_split_use_rate = transaction_detail.city_split_use_rate,
			city_single_flag = transaction_detail.city_single_flag,
			city_default_flag = transaction_detail.city_default_flag,
			combined_use_rate = transaction_detail.combined_use_rate,
			gl_extract_amt = transaction_detail.gl_extract_amt,
			update_user_id = transaction_detail.update_user_id,
			update_timestamp = transaction_detail.update_timestamp
		  WHERE transaction_detail_id = transaction_detail.transaction_detail_id;
		 -- Error Checking
	--         IF SQLCODE != 0 THEN
	--            RAISE e_badupdate;
	--         END IF;
	--      EXCEPTION
	--         WHEN e_badupdate THEN
	--            NULL;
	      END;
	   END LOOP;
	   CLOSE transaction_detail_cursor;
END;

--EXCEPTION
--   WHEN OTHERS THEN
--      NULL;
END;
/
	
-- 2 -- 
CREATE OR REPLACE PROCEDURE sp_tb_taxcode_a_u (
	p_new_erp_taxcode IN VARCHAR2,
	p_new_taxtype_code IN VARCHAR2,
	p_new_measure_type_code IN VARCHAR2,
	p_new_pct_dist_amt IN VARCHAR2,
	p_new_taxcode_type_code IN VARCHAR2,
	p_new_taxcode_code IN VARCHAR2 )
	
AS
BEGIN
   UPDATE tb_taxcode_detail
      SET tb_taxcode_detail.erp_taxcode = p_new_erp_taxcode,
	  tb_taxcode_detail.taxtype_code = p_new_taxtype_code,
	  tb_taxcode_detail.measure_type_code = p_new_measure_type_code,
	  tb_taxcode_detail.pct_dist_amt = p_new_pct_dist_amt
    WHERE tb_taxcode_detail.taxcode_type_code = p_new_taxcode_type_code
      AND tb_taxcode_detail.taxcode_code = p_new_taxcode_code;
END;
/

-- 3 --
CREATE OR REPLACE PROCEDURE sp_tb_taxcode_state_a_u( 
  p_new_active_flag IN VARCHAR2,
  p_new_taxcode_state_code IN VARCHAR2          
)
AS
BEGIN
   UPDATE tb_taxcode_detail dtl
      SET dtl.active_flag = p_new_active_flag
    WHERE dtl.taxcode_state_code = p_new_taxcode_state_code;
END;
/

-- 4 --
CREATE OR REPLACE PROCEDURE sp_tb_taxcode_detail_b_iu
	(  p_new_taxcode_type_code IN VARCHAR2,
	   p_new_taxcode_code IN VARCHAR2,
	   p_new_erp_taxcode IN OUT VARCHAR2,
	   p_new_taxtype_code IN OUT VARCHAR2,
	   p_new_measure_type_code IN OUT VARCHAR2,
	   p_new_pct_dist_amt IN OUT VARCHAR2
	)
AS
BEGIN
      SELECT a.erp_taxcode, a.taxtype_code, a.measure_type_code, a.pct_dist_amt
	INTO p_new_erp_taxcode, p_new_taxtype_code, p_new_measure_type_code, p_new_pct_dist_amt
	FROM tb_taxcode a
       WHERE ( a.taxcode_type_code = p_new_taxcode_type_code )
	 AND ( a.taxcode_code = p_new_taxcode_code);

END;
/

-- 5 --
CREATE OR REPLACE PROCEDURE sp_tb_trans_detail_excp_b_iu ( 
	p_new_trans_detail_excp_id IN OUT VARCHAR2 
)
AS
BEGIN
      SELECT sq_tb_trans_detail_excp_id.NEXTVAL
	INTO p_new_trans_detail_excp_id
	FROM DUAL;
END;
/

-- 6 -- 
CREATE OR REPLACE PROCEDURE sp_tb_transaction_detail_a_u
		(
		 p_transaction_detail_id IN tb_transaction_detail_excp.transaction_detail_id%TYPE,
		 p_old_taxcode_detail_id IN tb_transaction_detail_excp.old_taxcode_detail_id%TYPE,
		 p_old_taxcode_state_code IN tb_transaction_detail_excp.old_taxcode_state_code%TYPE,
		 p_old_taxcode_type_code IN tb_transaction_detail_excp.old_taxcode_type_code%TYPE,
		 p_old_taxcode_code IN tb_transaction_detail_excp.old_taxcode_code%TYPE,
		 p_old_jurisdiction_id IN tb_transaction_detail_excp.old_jurisdiction_id%TYPE,
		 p_old_jurisdiction_taxrate_id IN tb_transaction_detail_excp.old_jurisdiction_taxrate_id%TYPE,
		 p_old_cch_taxcat_code IN tb_transaction_detail_excp.old_cch_taxcat_code%TYPE,
		 p_old_cch_group_code IN tb_transaction_detail_excp.old_cch_group_code%TYPE,
		 p_old_cch_item_code IN tb_transaction_detail_excp.old_cch_item_code%TYPE,
		 p_new_taxcode_detail_id IN tb_transaction_detail_excp.new_taxcode_detail_id%TYPE,
		 p_new_taxcode_state_code IN tb_transaction_detail_excp.new_taxcode_state_code%TYPE,
		 p_new_taxcode_type_code IN tb_transaction_detail_excp.new_taxcode_type_code%TYPE,
		 p_new_taxcode_code IN tb_transaction_detail_excp.new_taxcode_code%TYPE,
		 p_new_jurisdiction_id IN tb_transaction_detail_excp.new_jurisdiction_id%TYPE,
		 p_new_jurisdiction_taxrate_id IN tb_transaction_detail_excp.new_jurisdiction_taxrate_id%TYPE,
		 p_new_cch_taxcat_code IN tb_transaction_detail_excp.new_cch_taxcat_code%TYPE,
		 p_new_cch_group_code IN tb_transaction_detail_excp.new_cch_group_code%TYPE,
		 p_new_cch_item_code IN tb_transaction_detail_excp.new_cch_item_code%TYPE
		)

	AS
	BEGIN
	      INSERT INTO tb_transaction_detail_excp (
		 transaction_detail_excp_id,
		 transaction_detail_id,
		 old_taxcode_detail_id,
		 old_taxcode_state_code,
		 old_taxcode_type_code,
		 old_taxcode_code,
		 old_jurisdiction_id,
		 old_jurisdiction_taxrate_id,
		 old_cch_taxcat_code,
		 old_cch_group_code,
		 old_cch_item_code,
		 new_taxcode_detail_id,
		 new_taxcode_state_code,
		 new_taxcode_type_code,
		 new_taxcode_code,
		 new_jurisdiction_id,
		 new_jurisdiction_taxrate_id,
		 new_cch_taxcat_code,
		 new_cch_group_code,
		 new_cch_item_code,
		 update_user_id,
		 update_timestamp )
	      VALUES (
		 SQ_TB_TRANS_DETAIL_EXCP_ID.NEXTVAL,
		 p_transaction_detail_id,
		 p_old_taxcode_detail_id,
		 p_old_taxcode_state_code,
		 p_old_taxcode_type_code,
		 p_old_taxcode_code,
		 p_old_jurisdiction_id,
		 p_old_jurisdiction_taxrate_id,
		 p_old_cch_taxcat_code,
		 p_old_cch_group_code,
		 p_old_cch_item_code,
		 p_new_taxcode_detail_id,
		 p_new_taxcode_state_code,
		 p_new_taxcode_type_code,
		 p_new_taxcode_code,
		 p_new_jurisdiction_id,
		 p_new_jurisdiction_taxrate_id,
		 p_new_cch_taxcat_code,
		 p_new_cch_group_code,
		 p_new_cch_item_code,
		 USER,
		 SYSDATE );
	END;
/


-- 7 --
CREATE OR REPLACE PROCEDURE sp_tb_user_a_d(p_old_user_code IN VARCHAR2)
AS
BEGIN
   DELETE
     FROM tb_option
    WHERE user_code = p_old_user_code;
   --DELETE
   --FROM tb_dw_syntax
   --WHERE user_code = p_old_user_code;
END;
/

-- 8 --
CREATE OR REPLACE PROCEDURE sp_tb_data_def_column_a_d (p_old_table_name IN VARCHAR2, p_old_column_name IN VARCHAR2)
AS
BEGIN
   If Lower(p_old_table_name) = 'tb_transaction_detail' Then
      DELETE
	FROM tb_driver_reference
       WHERE trans_dtl_column_name = p_old_column_name;
   End If;
END;
/
