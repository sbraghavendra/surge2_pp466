
BEGIN
	FOR current_row IN (select trigger_name, status from user_triggers order by 1) LOOP
		EXECUTE IMMEDIATE 'ALTER TRIGGER '||current_row.trigger_name||' DISABLE';
	END LOOP;
END;
/
