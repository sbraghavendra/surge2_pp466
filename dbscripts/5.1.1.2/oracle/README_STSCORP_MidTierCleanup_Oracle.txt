
(THIS FILE IS BEST VIEWED FROM EDITPLUS WITH WORDWRAP TURNED ON)

-----------------
-- Assumptions --
-----------------

1) This document assumes that the environment already has BOTH 
	- Oracle installed 
	- Database is created
	- The STSUSER role created 
		("CREATE ROLE stsuser;" as SYSTEM)
	- The USERS tablespace is set to AUTOEXTEND=YES 
		("SELECT file_name, autoextensible FROM dba_data_files ORDER BY 1;" as SYSTEM)

2) The MidTier Cleanup scripts are to be applied to version 5.1.1.1 of the STSCORP schema

3) The MidTier Cleanup scripts will take the STSCORP schema to version 5.1.1.2

-------------------------------
-- Importing baseline Schema --
-------------------------------

NOTE: The baseline schema WITH data is several hundred MB in size after compression and so it is NOT bundled as part of this deliverable.  The schema to be used here is compressed and can be found in either:

	- DB2_schemaExport_STSCORP_20081110_MidTierBaseline-NODATA.zip	(    180KB, WITHOUT DATA)
	- DB2_schemaExport_STSCORP_20090110_MidTierBaseline.zip		(742,324KB, WITH    DATA)

Follow instructions from baseline\SchemaRefreshKit\README.txt to conduct the baseline schema import to load the midtier baseline schema into your local environment.

Additionally, these .zip files were compressed with Winzip on Windows environments and will need to be extracted with Winzip.

-------------------------------------
-- Running MidTier Upgrade Scripts --
-------------------------------------

1) Connect to the Database as the STSCORP user to run the following scripts

	SQL> conn stscorp/<Password>

2) Disable Triggers

	SQL> @upgradeScripts\code\triggers\disableTriggers.sql

3) Miscellaneous Schema modifications (2.3)

	SQL> @upgradeScripts\object\LPP-MidTier-2.3_SchemaMods.sql

4) 17 MidTier SQL modifications as new stored procs (2.2)

	SQL> @upgradeScripts\code\procedures\createFromMidTierStoredProcs.sql

5) 11 Triggers as new stored procs (2.1) [Note: dependency on objects from 2.4]
	
	SQL> @upgradeScripts\code\packages\createPackageMATRIX_RECORD_PKG.sql
	SQL> @upgradeScripts\code\procedures\createProcedureSP_GEN_LOCATION_DRIVER.sql
	SQL> @upgradeScripts\code\procedures\createProcedureSP_GEN_TAX_DRIVER.sql

	SQL> @upgradeScripts\code\triggers\createTriggersAsProcs.sql
	SQL> @upgradeScripts\code\triggers\createProcedureSP_TB_LOCATION_MATRIX_A_I.sql
	SQL> @upgradeScripts\code\triggers\createProcedureSP_TB_TAX_MATRIX_A_I.sql
	SQL> @upgradeScripts\code\triggers\createProcedureSP_GL_LOGGING.sql
	SQL> @upgradeScripts\code\triggers\createProcedureSP_TB_TRANSACTION_DETAIL_B_U.sql

6) Miscellaneous objects created per request of the apps team

	SQL> @upgradeScripts\code\misc\createProcedureSP_MAX_TB_BCP_JURIS_TAXRATE_ID.sql
	SQL> @upgradeScripts\code\misc\createProcedureSP_MAX_ALL.sql
	SQL> @upgradeScripts\code\misc\createViews.sql

7) SP_BATCH Modularization

	SQL> @upgradeScripts\code\procedures\createProcedureSP_BATCH_PROCESS.sql

8) Miscellaneous objects updated per dropped objects or otherwise

	SQL> @upgradeScripts\code\procedures\createProcedureSP_TRUNCATE_BCP_TABLES.sql
	SQL> @upgradeScripts\code\procedures\createProcedureSP_BATCH_TAXRATE_UPDATE.sql
