/**  Taxability - TaxCode Details  ****************************************************************/
CREATE OR REPLACE FUNCTION tb_taxcode_detail_b_iu()
  RETURNS trigger AS
$BODY$
BEGIN
   IF (TG_OP = 'INSERT') THEN
      SELECT a.erp_taxcode, a.taxtype_code, a.measure_type_code, a.pct_dist_amt
        INTO NEW.erp_taxcode, NEW.taxtype_code, NEW.measure_type_code, NEW.pct_dist_amt
        FROM tb_taxcode a
       WHERE ( a.taxcode_type_code = NEW.taxcode_type_code )
         AND ( a.taxcode_code = NEW.taxcode_code);
   END IF;
	RETURN NEW;
END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
ALTER FUNCTION tb_taxcode_detail_b_iu() OWNER TO stscorp;

CREATE TRIGGER tb_taxcode_detail_b_iu
  BEFORE INSERT OR UPDATE
  ON tb_taxcode_detail
  FOR EACH ROW
  EXECUTE PROCEDURE tb_taxcode_detail_b_iu();