/**  Security - Users  ****************************************************************************/
CREATE OR REPLACE FUNCTION tb_user_a_d()
  RETURNS trigger AS
$BODY$
BEGIN
   DELETE
     FROM tb_option
    WHERE user_code = OLD.user_code;
   DELETE
     FROM tb_dw_syntax
    WHERE user_code = OLD.user_code;
		RETURN NULL;
END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
ALTER FUNCTION tb_user_a_d() OWNER TO stscorp;

CREATE TRIGGER tb_user_a_d
  AFTER DELETE
  ON tb_user
  FOR EACH ROW
  EXECUTE PROCEDURE tb_user_a_d();