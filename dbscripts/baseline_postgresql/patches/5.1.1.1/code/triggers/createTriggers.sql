---------------------------------------------------------------------------------------- 
--  Copyright (c) Lincoln Peak Partners (2008) All rights reserved 
    
--  Name         : createTriggers.sql

--  Description  : This file will create triggers in the stscorp schema

--  Usage        : => \i createTriggers.sql

--  Notes        Run this script while connected as the stscorp user to the postdb1 DB
---------------------------------------------------------------------------------------- 

\i ./tb_allocation_matrix_b_iu.sql
\i ./tb_batch_error_log_b_i.sql
\i ./tb_gl_extract_map_b_iu.sql
\i ./tb_jurisdiction_taxrate_a_i.sql
\i ./tb_location_matrix_a_i.sql
\i ./tb_location_matrix_b_iu.sql
\i ./tb_questionanswer_log_b_iu.sql
\i ./tb_tax_matrix_a_i.sql
\i ./tb_tax_matrix_b_ui.sql
\i ./tb_taxcode_a_u.sql
\i ./tb_taxcode_detail_b_iu.sql
\i ./tb_taxcode_state_a_u.sql
\i ./tb_transaction_detail_a_u.sql
\i ./tb_transaction_detail_b_u.sql
\i ./tb_transaction_detail_excp_b_iu.sql
\i ./tb_user_a_d.sql