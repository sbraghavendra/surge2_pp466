/**  Transaction Detail Excp  *********************************************************************/
CREATE OR REPLACE FUNCTION tb_transaction_detail_excp_b_iu()
  RETURNS trigger AS
$BODY$
BEGIN
   IF (TG_OP = 'INSERT') THEN
		SELECT NEXTVAL('sq_tb_trans_detail_excp_id')
		INTO STRICT NEW.transaction_detail_excp_id;
   END IF;
	RETURN NEW;
END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
ALTER FUNCTION tb_transaction_detail_excp_b_iu() OWNER TO stscorp;

CREATE TRIGGER tb_transaction_detail_excp_b_iu
  BEFORE INSERT OR UPDATE
  ON tb_transaction_detail_excp
  FOR EACH ROW
  EXECUTE PROCEDURE tb_transaction_detail_excp_b_iu();
