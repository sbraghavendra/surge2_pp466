/**  Taxability - TaxCodes  ***********************************************************************/
CREATE OR REPLACE FUNCTION tb_taxcode_a_u()
  RETURNS trigger AS
$BODY$
BEGIN
   UPDATE tb_taxcode_detail
      SET tb_taxcode_detail.erp_taxcode = NEW.erp_taxcode,
          tb_taxcode_detail.taxtype_code = NEW.taxtype_code,
          tb_taxcode_detail.measure_type_code = NEW.measure_type_code,
          tb_taxcode_detail.pct_dist_amt = NEW.pct_dist_amt
    WHERE tb_taxcode_detail.taxcode_type_code = NEW.taxcode_type_code
      AND tb_taxcode_detail.taxcode_code = NEW.taxcode_code;
	  	RETURN NEW;
END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
ALTER FUNCTION tb_taxcode_a_u() OWNER TO stscorp;

CREATE TRIGGER tb_taxcode_a_u
  AFTER UPDATE
  ON tb_taxcode
  FOR EACH ROW
  EXECUTE PROCEDURE tb_taxcode_a_u();
