/**  Tax Matrix  **********************************************************************************/
CREATE OR REPLACE FUNCTION tb_tax_matrix_b_iu()
  RETURNS trigger AS
$BODY$
DECLARE
-- Table defined variables
   v_binary_weight                 tb_tax_matrix.binary_weight%TYPE                  := 0;
   v_significant_digits            tb_tax_matrix.significant_digits%TYPE             := NULL;

-- ****** Program starts ****** --------------------------------------------------------------------
BEGIN
   IF (TG_OP = 'INSERT') THEN
      NEW.driver_01 := NVL(NEW.driver_01,'*ALL');
      NEW.driver_02 := NVL(NEW.driver_02,'*ALL');
      NEW.driver_03 := NVL(NEW.driver_03,'*ALL');
      NEW.driver_04 := NVL(NEW.driver_04,'*ALL');
      NEW.driver_05 := NVL(NEW.driver_05,'*ALL');
      NEW.driver_06 := NVL(NEW.driver_06,'*ALL');
      NEW.driver_07 := NVL(NEW.driver_07,'*ALL');
      NEW.driver_08 := NVL(NEW.driver_08,'*ALL');
      NEW.driver_09 := NVL(NEW.driver_09,'*ALL');
      NEW.driver_10 := NVL(NEW.driver_10,'*ALL');
      NEW.driver_11 := NVL(NEW.driver_11,'*ALL');
      NEW.driver_12 := NVL(NEW.driver_12,'*ALL');
      NEW.driver_13 := NVL(NEW.driver_13,'*ALL');
      NEW.driver_14 := NVL(NEW.driver_14,'*ALL');
      NEW.driver_15 := NVL(NEW.driver_15,'*ALL');
      NEW.driver_16 := NVL(NEW.driver_16,'*ALL');
      NEW.driver_17 := NVL(NEW.driver_17,'*ALL');
      NEW.driver_18 := NVL(NEW.driver_18,'*ALL');
      NEW.driver_19 := NVL(NEW.driver_19,'*ALL');
      NEW.driver_20 := NVL(NEW.driver_20,'*ALL');
      NEW.driver_21 := NVL(NEW.driver_21,'*ALL');
      NEW.driver_22 := NVL(NEW.driver_22,'*ALL');
      NEW.driver_23 := NVL(NEW.driver_23,'*ALL');
      NEW.driver_24 := NVL(NEW.driver_24,'*ALL');
      NEW.driver_25 := NVL(NEW.driver_25,'*ALL');
      NEW.driver_26 := NVL(NEW.driver_26,'*ALL');
      NEW.driver_27 := NVL(NEW.driver_27,'*ALL');
      NEW.driver_28 := NVL(NEW.driver_28,'*ALL');
      NEW.driver_29 := NVL(NEW.driver_29,'*ALL');
      NEW.driver_30 := NVL(NEW.driver_30,'*ALL');

      SELECT
         DECODE(NVL(NEW.driver_30,'*ALL'),'*ALL',0,POWER(2,0)) +
         DECODE(NVL(NEW.driver_29,'*ALL'),'*ALL',0,POWER(2,1)) +
         DECODE(NVL(NEW.driver_28,'*ALL'),'*ALL',0,POWER(2,2)) +
         DECODE(NVL(NEW.driver_27,'*ALL'),'*ALL',0,POWER(2,3)) +
         DECODE(NVL(NEW.driver_26,'*ALL'),'*ALL',0,POWER(2,4)) +
         DECODE(NVL(NEW.driver_25,'*ALL'),'*ALL',0,POWER(2,5)) +
         DECODE(NVL(NEW.driver_24,'*ALL'),'*ALL',0,POWER(2,6)) +
         DECODE(NVL(NEW.driver_23,'*ALL'),'*ALL',0,POWER(2,7)) +
         DECODE(NVL(NEW.driver_22,'*ALL'),'*ALL',0,POWER(2,8)) +
         DECODE(NVL(NEW.driver_21,'*ALL'),'*ALL',0,POWER(2,9)) +
         DECODE(NVL(NEW.driver_20,'*ALL'),'*ALL',0,POWER(2,10)) +
         DECODE(NVL(NEW.driver_19,'*ALL'),'*ALL',0,POWER(2,11)) +
         DECODE(NVL(NEW.driver_18,'*ALL'),'*ALL',0,POWER(2,12)) +
         DECODE(NVL(NEW.driver_17,'*ALL'),'*ALL',0,POWER(2,13)) +
         DECODE(NVL(NEW.driver_16,'*ALL'),'*ALL',0,POWER(2,14)) +
         DECODE(NVL(NEW.driver_15,'*ALL'),'*ALL',0,POWER(2,15)) +
         DECODE(NVL(NEW.driver_14,'*ALL'),'*ALL',0,POWER(2,16)) +
         DECODE(NVL(NEW.driver_13,'*ALL'),'*ALL',0,POWER(2,17)) +
         DECODE(NVL(NEW.driver_12,'*ALL'),'*ALL',0,POWER(2,18)) +
         DECODE(NVL(NEW.driver_11,'*ALL'),'*ALL',0,POWER(2,19)) +
         DECODE(NVL(NEW.driver_10,'*ALL'),'*ALL',0,POWER(2,20)) +
         DECODE(NVL(NEW.driver_09,'*ALL'),'*ALL',0,POWER(2,21)) +
         DECODE(NVL(NEW.driver_08,'*ALL'),'*ALL',0,POWER(2,22)) +
         DECODE(NVL(NEW.driver_07,'*ALL'),'*ALL',0,POWER(2,23)) +
         DECODE(NVL(NEW.driver_06,'*ALL'),'*ALL',0,POWER(2,24)) +
         DECODE(NVL(NEW.driver_05,'*ALL'),'*ALL',0,POWER(2,25)) +
         DECODE(NVL(NEW.driver_04,'*ALL'),'*ALL',0,POWER(2,26)) +
         DECODE(NVL(NEW.driver_03,'*ALL'),'*ALL',0,POWER(2,27)) +
         DECODE(NVL(NEW.driver_02,'*ALL'),'*ALL',0,POWER(2,28)) +
         DECODE(NVL(NEW.driver_01,'*ALL'),'*ALL',0,POWER(2,29)) +
         DECODE(NVL(NEW.matrix_state_code,'*ALL'),'*ALL',0,POWER(2,30)) +
         DECODE(NVL(NEW.driver_global_flag,'0'),'1',POWER(2,31),0)
      INTO v_binary_weight
      FROM DUAL;

      SELECT
         TRIM(DECODE(INSTR(NVL(NEW.driver_01,' '),'%'),0,DECODE(NVL(NEW.driver_01,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(NEW.driver_01,' '),'%')-1,'000'))||'.') ||
              DECODE(INSTR(NVL(NEW.driver_02,' '),'%'),0,DECODE(NVL(NEW.driver_02,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(NEW.driver_02,' '),'%')-1,'000'))||'.') ||
              DECODE(INSTR(NVL(NEW.driver_03,' '),'%'),0,DECODE(NVL(NEW.driver_03,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(NEW.driver_03,' '),'%')-1,'000'))||'.') ||
              DECODE(INSTR(NVL(NEW.driver_04,' '),'%'),0,DECODE(NVL(NEW.driver_04,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(NEW.driver_04,' '),'%')-1,'000'))||'.') ||
              DECODE(INSTR(NVL(NEW.driver_05,' '),'%'),0,DECODE(NVL(NEW.driver_05,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(NEW.driver_05,' '),'%')-1,'000'))||'.') ||
              DECODE(INSTR(NVL(NEW.driver_06,' '),'%'),0,DECODE(NVL(NEW.driver_06,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(NEW.driver_06,' '),'%')-1,'000'))||'.') ||
              DECODE(INSTR(NVL(NEW.driver_07,' '),'%'),0,DECODE(NVL(NEW.driver_07,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(NEW.driver_07,' '),'%')-1,'000'))||'.') ||
              DECODE(INSTR(NVL(NEW.driver_08,' '),'%'),0,DECODE(NVL(NEW.driver_08,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(NEW.driver_08,' '),'%')-1,'000'))||'.') ||
              DECODE(INSTR(NVL(NEW.driver_09,' '),'%'),0,DECODE(NVL(NEW.driver_09,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(NEW.driver_09,' '),'%')-1,'000'))||'.') ||
              DECODE(INSTR(NVL(NEW.driver_10,' '),'%'),0,DECODE(NVL(NEW.driver_10,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(NEW.driver_10,' '),'%')-1,'000'))||'.') ||
              DECODE(INSTR(NVL(NEW.driver_11,' '),'%'),0,DECODE(NVL(NEW.driver_11,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(NEW.driver_11,' '),'%')-1,'000'))||'.') ||
              DECODE(INSTR(NVL(NEW.driver_12,' '),'%'),0,DECODE(NVL(NEW.driver_12,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(NEW.driver_12,' '),'%')-1,'000'))||'.') ||
              DECODE(INSTR(NVL(NEW.driver_13,' '),'%'),0,DECODE(NVL(NEW.driver_13,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(NEW.driver_13,' '),'%')-1,'000'))||'.') ||
              DECODE(INSTR(NVL(NEW.driver_14,' '),'%'),0,DECODE(NVL(NEW.driver_14,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(NEW.driver_14,' '),'%')-1,'000'))||'.') ||
              DECODE(INSTR(NVL(NEW.driver_15,' '),'%'),0,DECODE(NVL(NEW.driver_15,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(NEW.driver_15,' '),'%')-1,'000'))||'.') ||
              DECODE(INSTR(NVL(NEW.driver_16,' '),'%'),0,DECODE(NVL(NEW.driver_16,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(NEW.driver_16,' '),'%')-1,'000'))||'.') ||
              DECODE(INSTR(NVL(NEW.driver_17,' '),'%'),0,DECODE(NVL(NEW.driver_17,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(NEW.driver_17,' '),'%')-1,'000'))||'.') ||
              DECODE(INSTR(NVL(NEW.driver_18,' '),'%'),0,DECODE(NVL(NEW.driver_18,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(NEW.driver_18,' '),'%')-1,'000'))||'.') ||
              DECODE(INSTR(NVL(NEW.driver_19,' '),'%'),0,DECODE(NVL(NEW.driver_19,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(NEW.driver_19,' '),'%')-1,'000'))||'.') ||
              DECODE(INSTR(NVL(NEW.driver_20,' '),'%'),0,DECODE(NVL(NEW.driver_20,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(NEW.driver_20,' '),'%')-1,'000'))||'.') ||
              DECODE(INSTR(NVL(NEW.driver_21,' '),'%'),0,DECODE(NVL(NEW.driver_21,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(NEW.driver_21,' '),'%')-1,'000'))||'.') ||
              DECODE(INSTR(NVL(NEW.driver_22,' '),'%'),0,DECODE(NVL(NEW.driver_22,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(NEW.driver_22,' '),'%')-1,'000'))||'.') ||
              DECODE(INSTR(NVL(NEW.driver_23,' '),'%'),0,DECODE(NVL(NEW.driver_23,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(NEW.driver_23,' '),'%')-1,'000'))||'.') ||
              DECODE(INSTR(NVL(NEW.driver_24,' '),'%'),0,DECODE(NVL(NEW.driver_24,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(NEW.driver_24,' '),'%')-1,'000'))||'.') ||
              DECODE(INSTR(NVL(NEW.driver_25,' '),'%'),0,DECODE(NVL(NEW.driver_25,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(NEW.driver_25,' '),'%')-1,'000'))||'.') ||
              DECODE(INSTR(NVL(NEW.driver_26,' '),'%'),0,DECODE(NVL(NEW.driver_26,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(NEW.driver_26,' '),'%')-1,'000'))||'.') ||
              DECODE(INSTR(NVL(NEW.driver_27,' '),'%'),0,DECODE(NVL(NEW.driver_27,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(NEW.driver_27,' '),'%')-1,'000'))||'.') ||
              DECODE(INSTR(NVL(NEW.driver_28,' '),'%'),0,DECODE(NVL(NEW.driver_28,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(NEW.driver_28,' '),'%')-1,'000'))||'.') ||
              DECODE(INSTR(NVL(NEW.driver_29,' '),'%'),0,DECODE(NVL(NEW.driver_29,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(NEW.driver_29,' '),'%')-1,'000'))||'.') ||
              DECODE(INSTR(NVL(NEW.driver_30,' '),'%'),0,DECODE(NVL(NEW.driver_30,'*ALL'),'*ALL','000','999'),TRIM(TO_CHAR(INSTR(NVL(NEW.driver_30,' '),'%')-1,'000'))))
      INTO v_significant_digits
      FROM DUAL;

      NEW.default_flag := NVL(NEW.default_flag,'0');
      IF NEW.default_flag = '1' THEN
         NEW.binary_weight := 0;
         NEW.significant_digits := NULL;
         NEW.default_binary_weight := v_binary_weight;
         NEW.default_significant_digits := v_significant_digits;
      ELSE
         NEW.binary_weight := v_binary_weight;
         NEW.significant_digits := v_significant_digits;
         NEW.default_binary_weight := 0;
         NEW.default_significant_digits := NULL;
      END IF;
   END IF;

END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
ALTER FUNCTION tb_tax_matrix_b_iu() OWNER TO stscorp;

CREATE TRIGGER tb_tax_matrix_b_iu
  BEFORE INSERT OR UPDATE
  ON tb_tax_matrix
  FOR EACH ROW
  EXECUTE PROCEDURE tb_tax_matrix_b_iu();