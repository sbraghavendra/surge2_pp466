/**  Location Matrix  *****************************************************************************/
CREATE OR REPLACE FUNCTION tb_location_matrix_b_iu()
  RETURNS trigger AS
$BODY$
DECLARE
-- Table defined variables
   v_binary_weight                 tb_location_matrix.binary_weight%TYPE             := 0;
   v_significant_digits            tb_location_matrix.significant_digits%TYPE        := NULL;

   
-- ****** Program starts ****** --------------------------------------------------------------------
BEGIN

   IF (TG_OP = 'INSERT') THEN
      NEW.driver_01 := NVL(NEW.driver_01,'*ALL');
      NEW.driver_02 := NVL(NEW.driver_02,'*ALL');
      NEW.driver_03 := NVL(NEW.driver_03,'*ALL');
      NEW.driver_04 := NVL(NEW.driver_04,'*ALL');
      NEW.driver_05 := NVL(NEW.driver_05,'*ALL');
      NEW.driver_06 := NVL(NEW.driver_06,'*ALL');
      NEW.driver_07 := NVL(NEW.driver_07,'*ALL');
      NEW.driver_08 := NVL(NEW.driver_08,'*ALL');
      NEW.driver_09 := NVL(NEW.driver_09,'*ALL');
      NEW.driver_10 := NVL(NEW.driver_10,'*ALL');

      SELECT
	     CASE NVL(NEW.driver_10,'*ALL') WHEN '*ALL' THEN 0 ELSE POWER(2,0) END +
         CASE NVL(NEW.driver_09,'*ALL') WHEN '*ALL' THEN 0 ELSE POWER(2,1) END +
         CASE NVL(NEW.driver_08,'*ALL') WHEN '*ALL' THEN 0 ELSE POWER(2,2) END +
         CASE NVL(NEW.driver_07,'*ALL') WHEN '*ALL' THEN 0 ELSE POWER(2,3) END +
         CASE NVL(NEW.driver_06,'*ALL') WHEN '*ALL' THEN 0 ELSE POWER(2,4) END +
         CASE NVL(NEW.driver_05,'*ALL') WHEN '*ALL' THEN 0 ELSE POWER(2,5) END +
		 CASE NVL(NEW.driver_04,'*ALL') WHEN '*ALL' THEN 0 ELSE POWER(2,6) END +
		 CASE NVL(NEW.driver_03,'*ALL') WHEN '*ALL' THEN 0 ELSE POWER(2,7) END +
		 CASE NVL(NEW.driver_02,'*ALL') WHEN '*ALL' THEN 0 ELSE POWER(2,8) END +
		 CASE NVL(NEW.driver_01,'*ALL') WHEN '*ALL' THEN 0 ELSE POWER(2,9) END 
      INTO v_binary_weight;

      SELECT
         TRIM(CASE INSTR(NVL(NEW.driver_01,' '),'%') WHEN 0 THEN CASE NVL(NEW.driver_01,'*ALL') WHEN '*ALL' THEN '000.' ELSE '999.' END ELSE TRIM(TO_CHAR(INSTR(NVL(NEW.driver_01,' '),'%')-1,'000'))||'.' END ||
              CASE INSTR(NVL(NEW.driver_02,' '),'%') WHEN 0 THEN CASE NVL(NEW.driver_02,'*ALL') WHEN '*ALL' THEN '000.' ELSE '999.' END ELSE TRIM(TO_CHAR(INSTR(NVL(NEW.driver_02,' '),'%')-1,'000'))||'.' END ||
              CASE INSTR(NVL(NEW.driver_03,' '),'%') WHEN 0 THEN CASE NVL(NEW.driver_03,'*ALL') WHEN '*ALL' THEN '000.' ELSE '999.' END ELSE TRIM(TO_CHAR(INSTR(NVL(NEW.driver_03,' '),'%')-1,'000'))||'.' END ||
              CASE INSTR(NVL(NEW.driver_04,' '),'%') WHEN 0 THEN CASE NVL(NEW.driver_04,'*ALL') WHEN '*ALL' THEN '000.' ELSE '999.' END ELSE TRIM(TO_CHAR(INSTR(NVL(NEW.driver_04,' '),'%')-1,'000'))||'.' END ||
              CASE INSTR(NVL(NEW.driver_05,' '),'%') WHEN 0 THEN CASE NVL(NEW.driver_05,'*ALL') WHEN '*ALL' THEN '000.' ELSE '999.' END ELSE TRIM(TO_CHAR(INSTR(NVL(NEW.driver_05,' '),'%')-1,'000'))||'.' END ||
              CASE INSTR(NVL(NEW.driver_06,' '),'%') WHEN 0 THEN CASE NVL(NEW.driver_06,'*ALL') WHEN '*ALL' THEN '000.' ELSE '999.' END ELSE TRIM(TO_CHAR(INSTR(NVL(NEW.driver_06,' '),'%')-1,'000'))||'.' END ||
              CASE INSTR(NVL(NEW.driver_07,' '),'%') WHEN 0 THEN CASE NVL(NEW.driver_07,'*ALL') WHEN '*ALL' THEN '000.' ELSE '999.' END ELSE TRIM(TO_CHAR(INSTR(NVL(NEW.driver_07,' '),'%')-1,'000'))||'.' END ||
              CASE INSTR(NVL(NEW.driver_08,' '),'%') WHEN 0 THEN CASE NVL(NEW.driver_08,'*ALL') WHEN '*ALL' THEN '000.' ELSE '999.' END ELSE TRIM(TO_CHAR(INSTR(NVL(NEW.driver_08,' '),'%')-1,'000'))||'.' END ||
              CASE INSTR(NVL(NEW.driver_09,' '),'%') WHEN 0 THEN CASE NVL(NEW.driver_09,'*ALL') WHEN '*ALL' THEN '000.' ELSE '999.' END ELSE TRIM(TO_CHAR(INSTR(NVL(NEW.driver_09,' '),'%')-1,'000'))||'.' END ||
              CASE INSTR(NVL(NEW.driver_10,' '),'%') WHEN 0 THEN CASE NVL(NEW.driver_10,'*ALL') WHEN '*ALL' THEN '000.' ELSE '999.' END ELSE TRIM(TO_CHAR(INSTR(NVL(NEW.driver_10,' '),'%')-1,'000')) END )
      INTO v_significant_digits;

      NEW.default_flag := NVL(NEW.default_flag,'0');
      IF NEW.default_flag = '1' THEN
         NEW.binary_weight := 0;
         NEW.significant_digits := NULL;
         NEW.default_binary_weight := v_binary_weight;
         NEW.default_significant_digits := v_significant_digits;
      ELSE
         NEW.binary_weight := v_binary_weight;
         NEW.significant_digits := v_significant_digits;
         NEW.default_binary_weight := 0;
         NEW.default_significant_digits := NULL;
      END IF;
   END IF;
   RETURN NEW;
END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
ALTER FUNCTION tb_location_matrix_b_iu() OWNER TO stscorp;

CREATE TRIGGER tb_location_matrix_b_iu
  BEFORE INSERT OR UPDATE
  ON tb_location_matrix
  FOR EACH ROW
  EXECUTE PROCEDURE tb_location_matrix_b_iu();