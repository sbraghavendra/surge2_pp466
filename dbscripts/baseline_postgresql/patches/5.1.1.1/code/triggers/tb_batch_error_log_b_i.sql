CREATE OR REPLACE FUNCTION tb_batch_error_log_b_i()
  RETURNS trigger AS
$BODY$
BEGIN
	SELECT NEXTVAL('sq_tb_batch_error_log_id')
	INTO STRICT NEW.batch_error_log_id;	
	 	RETURN NEW;
END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
ALTER FUNCTION tb_batch_error_log_b_i() OWNER TO stscorp;

CREATE TRIGGER tb_batch_error_log_b_i
  BEFORE INSERT OR UPDATE
  ON tb_batch_error_log
  FOR EACH ROW
  EXECUTE PROCEDURE tb_batch_error_log_b_i();