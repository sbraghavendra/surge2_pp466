/**  Transaction Detail  **************************************************************************/
CREATE OR REPLACE FUNCTION tb_transaction_detail_a_u()
  RETURNS trigger AS
$BODY$

-- ****** Program starts ****** --------------------------------------------------------------------
BEGIN
   IF (( OLD.taxcode_detail_id IS NOT NULL ) AND OLD.taxcode_detail_id <> NEW.taxcode_detail_id ) OR
      (( OLD.jurisdiction_id IS NOT NULL ) AND OLD.jurisdiction_id <> NEW.jurisdiction_id ) OR
      (( OLD.jurisdiction_taxrate_id IS NOT NULL ) AND OLD.jurisdiction_taxrate_id <> NEW.jurisdiction_taxrate_id ) THEN

      INSERT INTO tb_transaction_detail_excp (
         transaction_detail_excp_id,
         transaction_detail_id,
         old_taxcode_detail_id,
         old_taxcode_state_code,
         old_taxcode_type_code,
         old_taxcode_code,
         old_jurisdiction_id,
         old_jurisdiction_taxrate_id,
         old_cch_taxcat_code,
         old_cch_group_code,
         old_cch_item_code,
         new_taxcode_detail_id,
         new_taxcode_state_code,
         new_taxcode_type_code,
         new_taxcode_code,
         new_jurisdiction_id,
         new_jurisdiction_taxrate_id,
         new_cch_taxcat_code,
         new_cch_group_code,
         new_cch_item_code,
         update_user_id,
         update_timestamp )
      VALUES (
         0,
         OLD.transaction_detail_id,
         OLD.taxcode_detail_id,
         OLD.taxcode_state_code,
         OLD.taxcode_type_code,
         OLD.taxcode_code,
         OLD.jurisdiction_id,
         OLD.jurisdiction_taxrate_id,
         OLD.cch_taxcat_code,
         OLD.cch_group_code,
         OLD.cch_item_code,
         NEW.taxcode_detail_id,
         NEW.taxcode_state_code,
         NEW.taxcode_type_code,
         NEW.taxcode_code,
         NEW.jurisdiction_id,
         NEW.jurisdiction_taxrate_id,
         NEW.cch_taxcat_code,
         NEW.cch_group_code,
         NEW.cch_item_code,
         USER,
         SYSDATE );
   END IF;
   
      RETURN NEW;
END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
ALTER FUNCTION tb_transaction_detail_a_u() OWNER TO stscorp;

CREATE TRIGGER tb_transaction_detail_a_u
  AFTER UPDATE
  ON tb_transaction_detail
  FOR EACH ROW
  EXECUTE PROCEDURE tb_transaction_detail_a_u();
