/**  Question/Answer Log  *************************************************************************/
CREATE OR REPLACE FUNCTION tb_questionanswer_log_b_iu()
  RETURNS trigger AS
$BODY$
BEGIN
   IF (TG_OP = 'INSERT') THEN
		SELECT NEXTVAL('sq_tb_questionanswer_log_id')
		INTO STRICT NEW.jurisdiction_taxrate_id;
   END IF;
	RETURN NEW;
END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
ALTER FUNCTION tb_questionanswer_log_b_iu() OWNER TO stscorp;


CREATE TRIGGER tb_questionanswer_log_b_iu
  BEFORE INSERT OR UPDATE
  ON tb_questionanswer_log
  FOR EACH ROW
  EXECUTE PROCEDURE tb_questionanswer_log_b_iu();