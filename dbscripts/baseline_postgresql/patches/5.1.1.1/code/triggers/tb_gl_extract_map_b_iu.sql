/**  Data Definitions - Columns  ******************************************************************/
CREATE OR REPLACE FUNCTION tb_data_def_column_a_d()
  RETURNS trigger AS
$BODY$
BEGIN
   If Lower(old.table_name) = 'tb_transaction_detail' Then
      DELETE
        FROM tb_driver_reference
       WHERE trans_dtl_column_name = old.column_name;
   End If;
END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
ALTER FUNCTION tb_data_def_column_a_d() OWNER TO stscorp;


CREATE TRIGGER tb_data_def_column_a_d
  AFTER DELETE
  ON tb_data_def_column
  FOR EACH ROW
  EXECUTE PROCEDURE tb_data_def_column_a_d();
