---------------------------------------------------------------------------------------- 
--  Copyright (c) Lincoln Peak Partners (2008) All rights reserved 
    
--  Name         : Patch_v3421.sql

--  Description  : This file will patch the STSCORP schema to v3.4.2.1

--  Usage        : => \i Patch_v3421.sql

--  Notes        Run this script while connected as the stscorp user to the postdb1 DB
---------------------------------------------------------------------------------------- 

------------------------
-- Run object patches --
------------------------

\i ./objects/Alter_v3421_PostgreSQL.sql

----------------------
-- Run code patches --
----------------------

-- Functions
\i ./code/functions/F_PROCESS_USEREXIT.sql
\i ./code/functions/SP_BATCH_PROCESS.sql
\i ./code/functions/SP_CALCUSETAX.sql
\i ./code/functions/SP_GETUSETAX.sql
\i ./code/functions/SP_TRUNCATE_BCP_TABLES.sql

-- Triggers
DROP FUNCTION tb_allocation_matrix_b_iu()	CASCADE;
DROP FUNCTION tb_jurisdiction_taxrate_a_i()	CASCADE;
DROP FUNCTION tb_tax_matrix_a_i()		CASCADE;
DROP FUNCTION tb_transaction_detail_b_u()	CASCADE;

\i ./code/triggers/TB_ALLOCATION_MATRIX_B_IU.sql
\i ./code/triggers/TB_JURISDICTION_TAXRATE_A_I.sql
\i ./code/triggers/TB_LOCATION_MATRIX_A_I.sql
\i ./code/triggers/TB_LOCATION_MATRIX_B_IU.sql
\i ./code/triggers/TB_TAX_MATRIX_A_I.sql
\i ./code/triggers/TB_TRANSACTION_DETAIL_B_U.sql

