CREATE OR REPLACE FUNCTION tb_tax_matrix_a_i() RETURNS TRIGGER AS $$
/* ************************************************************************************************/
/* Object Type/Name: Trigger/After Insert - tb_tax_matrix_a_i                                     */
/* Author:           Michael B. Fuller                                                            */
/* Date:             a long time ago                                                              */
/* Description:      A Tax Matrix line has been added                                             */
/* Arguments:        None                                                                         */
/* Returns:          None                                                                         */
/* ************************************************************************************************/
/* Maintenance History:                                                                           */
/* Scan  Author       Date     Release    Description                                  Ticket #   */
/* ----- ---------- ---------- ---------- -------------------------------------------- ---------- */
/*       M. Fuller  06/02/2006 3.3.5.0    Fix processing an allocated line - Temporary 871        */
/*       M. Fuller  01/04/2007 3.3.5.1    Fix processing an allocated line - Permenant 871        */
/* MBF01 M. Fuller  08/13/2007 3.4.1.1    Add 5 Calc Tax Flags                         883        */
/* ************************************************************************************************/
DECLARE
-- Transaction Detail Selection SQL
   vc_transaction_detail_select    VARCHAR(2000) :=
      'SELECT tb_transaction_detail.* ' ||
        'FROM tb_tmp_tax_matrix, ' ||
             'tb_transaction_detail ' ||
       'WHERE ( tb_tmp_tax_matrix.tax_matrix_id = ' || NEW.tax_matrix_id || ') AND ' ||
             '(( tb_tmp_tax_matrix.matrix_state_code = ''*ALL'' ) OR ( tb_tmp_tax_matrix.matrix_state_code = tb_transaction_detail.transaction_state_code )) AND ' ||
             '( tb_tmp_tax_matrix.effective_date <= tb_transaction_detail.gl_date ) AND ( tb_tmp_tax_matrix.expiration_date >= tb_transaction_detail.gl_date ) AND ' ||
             '( tb_transaction_detail.transaction_ind = ''S'' ) AND ( tb_transaction_detail.suspend_ind = ''T'' ) ';

   vc_transaction_detail_where     VARCHAR(5000) := '';
   vc_transaction_detail_update    VARCHAR(100) := 'FOR UPDATE';
   vc_transaction_detail_stmt      VARCHAR(7100);
   
   transaction_detail_cursor       REFCURSOR;
   transaction_detail              RECORD;

-- Location Matrix Selection SQL
   vc_location_matrix_select       VARCHAR(2000) :=
      'SELECT tb_location_matrix.location_matrix_id, ' ||
             'tb_location_matrix.jurisdiction_id, ' ||
             'tb_location_matrix.override_taxtype_code, ' ||
             'tb_location_matrix.state_flag,' ||
             'tb_location_matrix.county_flag,' ||
             'tb_location_matrix.county_local_flag,' ||
             'tb_location_matrix.city_flag,' ||
             'tb_location_matrix.city_local_flag,' ||
             'tb_jurisdiction.state, ' ||
             'tb_jurisdiction.county, ' ||
             'tb_jurisdiction.city, ' ||
             'tb_jurisdiction.zip, ' ||
             'tb_jurisdiction.in_out ' ||
        'FROM tb_location_matrix, ' ||
             'tb_jurisdiction, ' ||
             'tb_transaction_detail ' ||
      'WHERE ( tb_transaction_detail.transaction_detail_id = (SELECT CAST (param_value AS NUMERIC) FROM tb_tax_matrix_a_i_tmp WHERE param_name = ''v_transaction_detail_id'') ) AND ' ||
            '( tb_location_matrix.default_flag = ''0'' AND tb_location_matrix.binary_weight > 0 ) AND ' ||
            '( tb_location_matrix.effective_date <= tb_transaction_detail.gl_date ) AND ( tb_location_matrix.expiration_date >= tb_transaction_detail.gl_date ) AND ' ||
            '( tb_location_matrix.jurisdiction_id = tb_jurisdiction.jurisdiction_id ) ';
   vc_location_matrix_where        VARCHAR(5000) := '';
   vc_location_matrix_orderby      VARCHAR(1000) :=
      'ORDER BY tb_location_matrix.binary_weight DESC, tb_location_matrix.significant_digits DESC, tb_location_matrix.effective_date DESC';
   vc_location_matrix_stmt         VARCHAR (8000);
   
   location_matrix_cursor          REFCURSOR;
   location_matrix                 RECORD;

-- Table defined variables
   v_relation_amount               tb_tax_matrix.relation_amount%TYPE                := NULL;
   v_hold_code_flag                tb_tax_matrix.then_hold_code_flag%TYPE            := NULL;
   v_less_hold_code_flag           tb_tax_matrix.then_hold_code_flag%TYPE            := NULL;
   v_less_taxcode_detail_id        tb_tax_matrix.then_taxcode_detail_id%TYPE         := NULL;
   v_less_cch_taxcat_code          tb_tax_matrix.then_cch_taxcat_code%TYPE           := NULL;
   v_less_cch_group_code           tb_tax_matrix.then_cch_group_code%TYPE            := NULL;
   v_equal_hold_code_flag          tb_tax_matrix.then_hold_code_flag%TYPE            := NULL;
   v_equal_taxcode_detail_id       tb_tax_matrix.then_taxcode_detail_id%TYPE         := NULL;
   v_equal_cch_taxcat_code         tb_tax_matrix.then_cch_taxcat_code%TYPE           := NULL;
   v_equal_cch_group_code          tb_tax_matrix.then_cch_group_code%TYPE            := NULL;
   v_greater_hold_code_flag        tb_tax_matrix.then_hold_code_flag%TYPE            := NULL;
   v_greater_taxcode_detail_id     tb_tax_matrix.then_taxcode_detail_id%TYPE         := NULL;
   v_greater_cch_taxcat_code       tb_tax_matrix.then_cch_taxcat_code%TYPE           := NULL;
   v_greater_cch_group_code        tb_tax_matrix.then_cch_group_code%TYPE            := NULL;
   v_then_taxcode_state_code       tb_taxcode_detail.taxcode_state_code%TYPE         := NULL;
   v_then_taxcode_type_code        tb_taxcode_detail.taxcode_type_code%TYPE          := NULL;
   v_then_taxcode_code             tb_taxcode_detail.taxcode_code%TYPE               := NULL;
   v_then_jurisdiction_id          tb_taxcode_detail.jurisdiction_id%TYPE            := NULL;
   v_then_measure_type_code        tb_taxcode_detail.measure_type_code%TYPE          := NULL;
   v_then_taxtype_code             tb_taxcode_detail.taxtype_code%TYPE               := NULL;
   v_else_taxcode_state_code       tb_taxcode_detail.taxcode_state_code%TYPE         := NULL;
   v_else_taxcode_type_code        tb_taxcode_detail.taxcode_type_code%TYPE          := NULL;
   v_else_taxcode_code             tb_taxcode_detail.taxcode_code%TYPE               := NULL;
   v_else_jurisdiction_id          tb_taxcode_detail.jurisdiction_id%TYPE            := NULL;
   v_else_measure_type_code        tb_taxcode_detail.measure_type_code%TYPE          := NULL;
   v_else_taxtype_code             tb_taxcode_detail.taxtype_code%TYPE               := NULL;
   v_less_taxcode_state_code       tb_taxcode_detail.taxcode_state_code%TYPE         := NULL;
   v_less_taxcode_type_code        tb_taxcode_detail.taxcode_type_code%TYPE          := NULL;
   v_less_taxcode_code             tb_taxcode_detail.taxcode_code%TYPE               := NULL;
   v_less_jurisdiction_id          tb_taxcode_detail.jurisdiction_id%TYPE            := NULL;
   v_less_measure_type_code        tb_taxcode_detail.measure_type_code%TYPE          := NULL;
   v_less_taxtype_code             tb_taxcode_detail.taxtype_code%TYPE               := NULL;
   v_equal_taxcode_state_code      tb_taxcode_detail.taxcode_state_code%TYPE         := NULL;
   v_equal_taxcode_type_code       tb_taxcode_detail.taxcode_type_code%TYPE          := NULL;
   v_equal_taxcode_code            tb_taxcode_detail.taxcode_code%TYPE               := NULL;
   v_equal_jurisdiction_id         tb_taxcode_detail.jurisdiction_id%TYPE            := NULL;
   v_equal_measure_type_code       tb_taxcode_detail.measure_type_code%TYPE          := NULL;
   v_equal_taxtype_code            tb_taxcode_detail.taxtype_code%TYPE               := NULL;
   v_greater_taxcode_state_code    tb_taxcode_detail.taxcode_state_code%TYPE         := NULL;
   v_greater_taxcode_type_code     tb_taxcode_detail.taxcode_type_code%TYPE          := NULL;
   v_greater_taxcode_code          tb_taxcode_detail.taxcode_code%TYPE               := NULL;
   v_greater_jurisdiction_id       tb_taxcode_detail.jurisdiction_id%TYPE            := NULL;
   v_greater_measure_type_code     tb_taxcode_detail.measure_type_code%TYPE          := NULL;
   v_greater_taxtype_code          tb_taxcode_detail.taxtype_code%TYPE               := NULL;
   v_transaction_detail_id         tb_transaction_detail.transaction_detail_id%TYPE  := NULL;
   v_override_jurisdiction_id      tb_bcp_transactions.jurisdiction_id%TYPE          := NULL;
   v_sysdate                       tb_transaction_detail.load_timestamp%TYPE         := CURRENT_TIMESTAMP;
   v_taxtype_code                  tb_taxcode.taxtype_code%TYPE                      := NULL;
   v_taxtype_used                  tb_taxcode.taxtype_code%TYPE                      := NULL;

-- Program defined variables
   vn_hold_trans_amount            NUMERIC                                            := 0;
   vn_fetch_rows                   NUMERIC                                            := 0;
   vi_error_count                  INTEGER                                           := 0;
   vn_actual_rowno                 NUMERIC                                            := 0;
-- temporary
   vn_taxable_amt                  NUMERIC                                            := 0;

   vc_test1 varchar(4000);
   vc_test2 varchar(4000);

-- Define Location Driver Names Cursor (tb_driver_names)
   location_driver_cursor CURSOR 
    FOR
        SELECT driver.trans_dtl_column_name, driver.matrix_column_name, driver.null_driver_flag, driver.wildcard_flag, driver.range_flag, driver.to_number_flag,
               datadefcol.desc_column_name, datadefcol.data_type
          FROM tb_driver_names driver,
               (SELECT *
                  FROM tb_data_def_column
                 WHERE table_name = 'TB_TRANSACTION_DETAIL') datadefcol
         WHERE driver.driver_names_code = 'L' AND
               driver.trans_dtl_column_name = datadefcol.column_name
      ORDER BY driver.driver_id;
   location_driver              RECORD;

-- Define Tax Driver Names Cursor (tb_driver_names)
   tax_driver_cursor CURSOR
    FOR
        SELECT driver.trans_dtl_column_name, driver.matrix_column_name, driver.null_driver_flag, driver.wildcard_flag, driver.range_flag, driver.to_number_flag,
               datadefcol.desc_column_name, datadefcol.data_type
          FROM tb_driver_names driver,
               (SELECT *
                  FROM tb_data_def_column
                 WHERE table_name = 'TB_TRANSACTION_DETAIL') datadefcol
         WHERE driver.driver_names_code = 'T' AND
               driver.trans_dtl_column_name = datadefcol.column_name
      ORDER BY driver.driver_id;
   tax_driver                   RECORD;

-- ****** Program starts ****** --------------------------------------------------------------------
BEGIN
IF NEW.default_flag <> '1' AND NEW.binary_weight > 0 THEN

   -- *******************************************************************************************************************************************
   -- NEW BATCH PROCESS CODE --------------------------------------------------------------------------------------------------------------------
   -- ***** Create Dynamic WHERE Clause for Transaction Detail SQL ***** --
   -- Tax Matrix Drivers
   OPEN tax_driver_cursor;
   LOOP
      FETCH tax_driver_cursor INTO tax_driver;

      IF (NOT FOUND) THEN 
	EXIT;
      END IF;
      
      IF tax_driver.range_flag = '1' THEN
         IF tax_driver.to_number_flag = '1' THEN
            IF tax_driver.null_driver_flag = '1' THEN
               -- Range and ToNumber and NULL
               IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                  UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                  vc_transaction_detail_where := vc_transaction_detail_where || 'AND ( ' ||
                  '( tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || '_THRU = ''*NA'' AND ' ||
                     '( tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NULL AND ( tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*NULL'' )) OR ' ||
                     '( tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND ( tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR CASE isnumber(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') WHEN 1 THEN to_char(to_number(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ' ,''00000000000000000000.00000000000000000000''),''00000000000000000000.00000000000000000000'') ELSE UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') END = CASE isnumber(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ') WHEN 1 THEN to_char(to_number(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ' ,''00000000000000000000.00000000000000000000''),''00000000000000000000.00000000000000000000'') ELSE UPPER(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ') END )) OR ' ||
                  '( tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || '_THRU <> ''*NA'' AND ' ||
                     '( tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND (CASE isnumber(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') WHEN 1 THEN to_char(to_number(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ' ,''00000000000000000000.00000000000000000000''),''00000000000000000000.00000000000000000000'') ELSE UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') END >= CASE isnumber(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ')       WHEN 1 THEN to_char(to_number(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ',      ''00000000000000000000.00000000000000000000'') ELSE UPPER(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ')      END AND ' ||
                                                                                            'CASE isnumber(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') WHEN 1 THEN to_char(to_number(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ' ,''00000000000000000000.00000000000000000000''),''00000000000000000000.00000000000000000000'') ELSE UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') END <= CASE isnumber(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || '_THRU ) WHEN 1 THEN to_char(to_number(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || '_THRU, ''00000000000000000000.00000000000000000000'') ELSE UPPER(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || '_THRU) END )))) ';
               END IF;
            ELSE
               -- Range and ToNumber and NOT NULL
               IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                  UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                  vc_transaction_detail_where := vc_transaction_detail_where || 'AND ( ' ||
                  '( tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || '_THRU = ''*NA'' AND (tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR CASE isnumber(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') WHEN 1 THEN to_char(to_number(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ' ,''00000000000000000000.00000000000000000000''),''00000000000000000000.00000000000000000000'') ELSE UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') END = CASE isnumber(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ') WHEN 1 THEN to_char(to_number(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ' ,''00000000000000000000.00000000000000000000''),''00000000000000000000.00000000000000000000'') ELSE UPPER(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ') END)) OR ' ||
                  '( tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || '_THRU <> ''*NA'' AND (CASE isnumber(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') WHEN 1 THEN to_char(to_number(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ' ,''00000000000000000000.00000000000000000000''),''00000000000000000000.00000000000000000000'') ELSE UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ')) >= CASE isnumber(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ') WHEN 1 THEN to_char(to_number(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ' ,''00000000000000000000.00000000000000000000''),''00000000000000000000.00000000000000000000'') ELSE UPPER(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ') END AND ' ||
                                                                                        'CASE isnumber(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') WHEN 1 THEN to_char(to_number(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ' ,''00000000000000000000.00000000000000000000''),''00000000000000000000.00000000000000000000'') ELSE UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ')) <= CASE isnumber(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || '_THRU ) WHEN 1 THEN to_char(to_number(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || '_THRU ,''00000000000000000000.00000000000000000000''),''00000000000000000000.00000000000000000000'') ELSE UPPER(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || '_THRU) END )) ';
               END IF;
            END IF;
         ELSE
            IF tax_driver.null_driver_flag = '1' THEN
               IF tax_driver.wildcard_flag = '1' THEN
                  -- Range and NOT ToNumber and NULL and Wildcard
                  IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                     UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                     vc_transaction_detail_where := vc_transaction_detail_where || 'AND ( ' ||
                     '( tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || '_THRU = ''*NA'' AND ' ||
                        '( tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NULL AND (tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*NULL'' )) OR ' ||
                        '( tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND (tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') LIKE UPPER(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ')))) OR ' ||
                     '( tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || '_THRU <> ''*NA'' AND (tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND (UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') >= UPPER(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ') AND UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') <= UPPER(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || '_THRU) ))) ';
                  END IF;
               ELSE
                  -- Range and NOT ToNumber and NULL and NOT Wildcard
                  IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                     UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                     vc_transaction_detail_where := vc_transaction_detail_where || 'AND ( ' ||
                     '( tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || '_THRU = ''*NA'' AND ' ||
                        '( tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NULL AND (tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*NULL'' )) OR ' ||
                        '( tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND (tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') = UPPER(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ')))) OR ' ||
                     '( tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || '_THRU <> ''*NA'' AND (tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND (UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') >= UPPER(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ') AND UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') <= UPPER(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || '_THRU) ))) ';
                  END IF;
               END IF;
            ELSE
               IF tax_driver.wildcard_flag = '1' THEN
                  -- Range and NOT ToNumber and NOT NULL and Wildcard
                  IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                     UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                     vc_transaction_detail_where := vc_transaction_detail_where || 'AND ( ' ||
                     '( tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || '_THRU = ''*NA'' AND ' ||
                        '(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') LIKE UPPER(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || '))) OR ' ||
                     '( tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || '_THRU <> ''*NA'' AND (UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') >= UPPER(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ') AND UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') <= UPPER(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || '_THRU) )) ';
                  END IF;
               ELSE
                  -- Range and NOT ToNumber and NOT NULL and NOT Wildcard
                  IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                     UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                     vc_transaction_detail_where := vc_transaction_detail_where || 'AND ( ' ||
                     '( tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || '_THRU = ''*NA'' AND ' ||
                        '(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') = UPPER(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || '))) OR ' ||
                     '( tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || '_THRU <> ''*NA'' AND (UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') >= UPPER(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ') AND UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') <= UPPER(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || '_THRU) )) ';
                  END IF;
               END IF;
            END IF;
         END IF;
      ELSE
         IF tax_driver.null_driver_flag = '1' THEN
            IF tax_driver.wildcard_flag = '1' THEN
               -- NOT Range and NULL and Wildcard
               IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                  UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                     vc_transaction_detail_where := vc_transaction_detail_where || 'AND ( ' ||
                     '( tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NULL AND (tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*NULL'' )) OR ' ||
                     '( tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND (tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') LIKE UPPER(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ')) ) ';
               END IF;
            ELSE
               -- NOT Range and NULL and NOT Wildcard
               IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                  UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                     vc_transaction_detail_where := vc_transaction_detail_where || 'AND ( ' ||
                     '( tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NULL AND (tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*NULL'' )) OR ' ||
                     '( tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND (tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') = UPPER(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ')) ) ';
               END IF;
            END IF;
         ELSE
            IF tax_driver.wildcard_flag = '1' THEN
               -- NOT Range and NOT NULL and Wildcard
               IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                  UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                     vc_transaction_detail_where := vc_transaction_detail_where || 'AND ( ' ||
                     '( tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') LIKE UPPER(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ') ) ';
               END IF;
            ELSE
               -- NOT Range and NOT NULL and NOT Wildcard
               IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                  UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                  vc_transaction_detail_where := vc_transaction_detail_where || 'AND ( ' ||
                     '( tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') = UPPER(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ') ) ';
               END IF;
            END IF;
         END IF;
      END IF;
      vc_transaction_detail_where := vc_transaction_detail_where || ') ';
   END LOOP;
   CLOSE tax_driver_cursor;
   -- NEW BATCH PROCESS CODE --------------------------------------------------------------------------------------------------------------------
   -- *******************************************************************************************************************************************

   -- If no drivers found raise error, else create transaction detail sql statement
   IF vc_transaction_detail_where IS NULL THEN
      NULL;
   ELSE
      vc_transaction_detail_stmt := vc_transaction_detail_select || vc_transaction_detail_where || vc_transaction_detail_update;
   END IF;

   -- ***** Create Dynamic WHERE Clause for Matrix SQLs ***** --
   -- Location Matrix Drivers
   OPEN location_driver_cursor;
   LOOP
      FETCH location_driver_cursor INTO location_driver;
      
      IF (NOT FOUND) THEN 
	EXIT;
      END IF;
      
      IF location_driver.null_driver_flag = '1' THEN
         IF location_driver.wildcard_flag = '1' THEN
            IF UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'CHAR' OR
               UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'VARC' THEN
                  vc_location_matrix_where := vc_location_matrix_where || 'AND ' ||
                  '((tb_transaction_detail.' || location_driver.trans_dtl_column_name || ' IS NULL AND (tb_location_matrix.' || location_driver.matrix_column_name || ' = ''*ALL'' OR tb_location_matrix.' || location_driver.matrix_column_name || ' = ''*NULL'' )) OR ' ||
                  '(tb_transaction_detail.' || location_driver.trans_dtl_column_name || ' IS NOT NULL AND (tb_location_matrix.' || location_driver.matrix_column_name || ' = ''*ALL'' OR UPPER(tb_transaction_detail.' || location_driver.trans_dtl_column_name || ') LIKE UPPER(tb_location_matrix.' || location_driver.matrix_column_name || ') ))) ';
            END IF;
         ELSE
            IF UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'CHAR' OR
               UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'VARC' THEN
                  vc_location_matrix_where := vc_location_matrix_where || 'AND ' ||
                  '((tb_transaction_detail.' || location_driver.trans_dtl_column_name || ' IS NULL AND (tb_location_matrix.' || location_driver.matrix_column_name || ' = ''*ALL'' OR tb_location_matrix.' || location_driver.matrix_column_name || ' = ''*NULL'' )) OR ' ||
                  '(tb_transaction_detail.' || location_driver.trans_dtl_column_name || ' IS NOT NULL AND (tb_location_matrix.' || location_driver.matrix_column_name || ' = ''*ALL'' OR UPPER(tb_transaction_detail.' || location_driver.trans_dtl_column_name || ') = UPPER(tb_location_matrix.' || location_driver.matrix_column_name || ') ))) ';
            END IF;
         END IF;
      ELSE
         IF location_driver.wildcard_flag = '1' THEN
            IF UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'CHAR' OR
               UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'VARC' THEN
                  vc_location_matrix_where := vc_location_matrix_where || 'AND ' ||
                  '(tb_location_matrix.' || location_driver.matrix_column_name || ' = ''*ALL'' OR UPPER(tb_transaction_detail.' || location_driver.trans_dtl_column_name || ') LIKE UPPER(tb_location_matrix.' || location_driver.matrix_column_name || ') ) ';
            END IF;
         ELSE
            IF UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'CHAR' OR
               UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'VARC' THEN
                  vc_location_matrix_where := vc_location_matrix_where || 'AND ' ||
                  '(tb_location_matrix.' || location_driver.matrix_column_name || ' = ''*ALL'' OR UPPER(tb_transaction_detail.' || location_driver.trans_dtl_column_name || ') = UPPER(tb_location_matrix.' || location_driver.matrix_column_name || ') ) ';
            END IF;
         END IF;
      END IF;
   END LOOP;
   CLOSE location_driver_cursor;
   -- If no drivers found raise error, else create transaction detail sql statement
   IF vc_location_matrix_where IS NULL THEN
      NULL;
   ELSE
      vc_location_matrix_stmt := vc_location_matrix_select || vc_location_matrix_where || vc_location_matrix_orderby;
   END IF;

BEGIN
	EXECUTE 'CREATE GLOBAL TEMPORARY TABLE TB_TMP_TAX_MATRIX
	(
	  TAX_MATRIX_ID               NUMERIC,
	  MATRIX_STATE_CODE           VARCHAR(10),
	  DRIVER_GLOBAL_FLAG          CHAR(1),
	  DRIVER_01                   VARCHAR(100),
	  DRIVER_01_DESC              VARCHAR(100),
	  DRIVER_01_THRU              VARCHAR(100),
	  DRIVER_01_THRU_DESC         VARCHAR(100),
	  DRIVER_02                   VARCHAR(100),
	  DRIVER_02_DESC              VARCHAR(100),
	  DRIVER_02_THRU              VARCHAR(100),
	  DRIVER_02_THRU_DESC         VARCHAR(100),
	  DRIVER_03                   VARCHAR(100),
	  DRIVER_03_DESC              VARCHAR(100),
	  DRIVER_03_THRU              VARCHAR(100),
	  DRIVER_03_THRU_DESC         VARCHAR(100),
	  DRIVER_04                   VARCHAR(100),
	  DRIVER_04_DESC              VARCHAR(100),
	  DRIVER_04_THRU              VARCHAR(100),
	  DRIVER_04_THRU_DESC         VARCHAR(100),
	  DRIVER_05                   VARCHAR(100),
	  DRIVER_05_DESC              VARCHAR(100),
	  DRIVER_05_THRU              VARCHAR(100),
	  DRIVER_05_THRU_DESC         VARCHAR(100),
	  DRIVER_06                   VARCHAR(100),
	  DRIVER_06_DESC              VARCHAR(100),
	  DRIVER_06_THRU              VARCHAR(100),
	  DRIVER_06_THRU_DESC         VARCHAR(100),
	  DRIVER_07                   VARCHAR(100),
	  DRIVER_07_DESC              VARCHAR(100),
	  DRIVER_07_THRU              VARCHAR(100),
	  DRIVER_07_THRU_DESC         VARCHAR(100),
	  DRIVER_08                   VARCHAR(100),
	  DRIVER_08_DESC              VARCHAR(100),
	  DRIVER_08_THRU              VARCHAR(100),
	  DRIVER_08_THRU_DESC         VARCHAR(100),
	  DRIVER_09                   VARCHAR(100),
	  DRIVER_09_DESC              VARCHAR(100),
	  DRIVER_09_THRU              VARCHAR(100),
	  DRIVER_09_THRU_DESC         VARCHAR(100),
	  DRIVER_10                   VARCHAR(100),
	  DRIVER_10_DESC              VARCHAR(100),
	  DRIVER_10_THRU              VARCHAR(100),
	  DRIVER_10_THRU_DESC         VARCHAR(100),
	  DRIVER_11                   VARCHAR(100),
	  DRIVER_11_DESC              VARCHAR(100),
	  DRIVER_11_THRU              VARCHAR(100),
	  DRIVER_11_THRU_DESC         VARCHAR(100),
	  DRIVER_12                   VARCHAR(100),
	  DRIVER_12_DESC              VARCHAR(100),
	  DRIVER_12_THRU              VARCHAR(100),
	  DRIVER_12_THRU_DESC         VARCHAR(100),
	  DRIVER_13                   VARCHAR(100),
	  DRIVER_13_DESC              VARCHAR(100),
	  DRIVER_13_THRU              VARCHAR(100),
	  DRIVER_13_THRU_DESC         VARCHAR(100),
	  DRIVER_14                   VARCHAR(100),
	  DRIVER_14_DESC              VARCHAR(100),
	  DRIVER_14_THRU              VARCHAR(100),
	  DRIVER_14_THRU_DESC         VARCHAR(100),
	  DRIVER_15                   VARCHAR(100),
	  DRIVER_15_DESC              VARCHAR(100),
	  DRIVER_15_THRU              VARCHAR(100),
	  DRIVER_15_THRU_DESC         VARCHAR(100),
	  DRIVER_16                   VARCHAR(100),
	  DRIVER_16_DESC              VARCHAR(100),
	  DRIVER_16_THRU              VARCHAR(100),
	  DRIVER_16_THRU_DESC         VARCHAR(100),
	  DRIVER_17                   VARCHAR(100),
	  DRIVER_17_DESC              VARCHAR(100),
	  DRIVER_17_THRU              VARCHAR(100),
	  DRIVER_17_THRU_DESC         VARCHAR(100),
	  DRIVER_18                   VARCHAR(100),
	  DRIVER_18_DESC              VARCHAR(100),
	  DRIVER_18_THRU              VARCHAR(100),
	  DRIVER_18_THRU_DESC         VARCHAR(100),
	  DRIVER_19                   VARCHAR(100),
	  DRIVER_19_DESC              VARCHAR(100),
	  DRIVER_19_THRU              VARCHAR(100),
	  DRIVER_19_THRU_DESC         VARCHAR(100),
	  DRIVER_20                   VARCHAR(100),
	  DRIVER_20_DESC              VARCHAR(100),
	  DRIVER_20_THRU              VARCHAR(100),
	  DRIVER_20_THRU_DESC         VARCHAR(100),
	  DRIVER_21                   VARCHAR(100),
	  DRIVER_21_DESC              VARCHAR(100),
	  DRIVER_21_THRU              VARCHAR(100),
	  DRIVER_21_THRU_DESC         VARCHAR(100),
	  DRIVER_22                   VARCHAR(100),
	  DRIVER_22_DESC              VARCHAR(100),
	  DRIVER_22_THRU              VARCHAR(100),
	  DRIVER_22_THRU_DESC         VARCHAR(100),
	  DRIVER_23                   VARCHAR(100),
	  DRIVER_23_DESC              VARCHAR(100),
	  DRIVER_23_THRU              VARCHAR(100),
	  DRIVER_23_THRU_DESC         VARCHAR(100),
	  DRIVER_24                   VARCHAR(100),
	  DRIVER_24_DESC              VARCHAR(100),
	  DRIVER_24_THRU              VARCHAR(100),
	  DRIVER_24_THRU_DESC         VARCHAR(100),
	  DRIVER_25                   VARCHAR(100),
	  DRIVER_25_DESC              VARCHAR(100),
	  DRIVER_25_THRU              VARCHAR(100),
	  DRIVER_25_THRU_DESC         VARCHAR(100),
	  DRIVER_26                   VARCHAR(100),
	  DRIVER_26_DESC              VARCHAR(100),
	  DRIVER_26_THRU              VARCHAR(100),
	  DRIVER_26_THRU_DESC         VARCHAR(100),
	  DRIVER_27                   VARCHAR(100),
	  DRIVER_27_DESC              VARCHAR(100),
	  DRIVER_27_THRU              VARCHAR(100),
	  DRIVER_27_THRU_DESC         VARCHAR(100),
	  DRIVER_28                   VARCHAR(100),
	  DRIVER_28_DESC              VARCHAR(100),
	  DRIVER_28_THRU              VARCHAR(100),
	  DRIVER_28_THRU_DESC         VARCHAR(100),
	  DRIVER_29                   VARCHAR(100),
	  DRIVER_29_DESC              VARCHAR(100),
	  DRIVER_29_THRU              VARCHAR(100),
	  DRIVER_29_THRU_DESC         VARCHAR(100),
	  DRIVER_30                   VARCHAR(100),
	  DRIVER_30_DESC              VARCHAR(100),
	  DRIVER_30_THRU              VARCHAR(100),
	  DRIVER_30_THRU_DESC         VARCHAR(100),
	  BINARY_WEIGHT               NUMERIC,
	  SIGNIFICANT_DIGITS          VARCHAR(119),
	  DEFAULT_FLAG                CHAR(1),
	  DEFAULT_BINARY_WEIGHT       NUMERIC,
	  DEFAULT_SIGNIFICANT_DIGITS  VARCHAR(119),
	  EFFECTIVE_DATE              TIMESTAMP(0),
	  EXPIRATION_DATE             TIMESTAMP(0),
	  RELATION_SIGN               VARCHAR(3),
	  RELATION_AMOUNT             NUMERIC,
	  THEN_HOLD_CODE_FLAG         CHAR(1),
	  THEN_TAXCODE_DETAIL_ID      NUMERIC,
	  THEN_TAXCODE_TYPE_CODE      VARCHAR(10),
	  THEN_CCH_TAXCAT_CODE        VARCHAR(2),
	  THEN_CCH_GROUP_CODE         VARCHAR(4),
	  THEN_CCH_ITEM_CODE          VARCHAR(3),
	  ELSE_HOLD_CODE_FLAG         CHAR(1),
	  ELSE_TAXCODE_DETAIL_ID      NUMERIC,
	  ELSE_TAXCODE_TYPE_CODE      VARCHAR(10),
	  ELSE_CCH_TAXCAT_CODE        VARCHAR(2),
	  ELSE_CCH_GROUP_CODE         VARCHAR(4),
	  ELSE_CCH_ITEM_CODE          VARCHAR(3),
	  COMMENTS                    VARCHAR(255),
	  LAST_USED_TIMESTAMP         TIMESTAMP(0),
	  UPDATE_USER_ID              VARCHAR(40),
	  UPDATE_TIMESTAMP            TIMESTAMP(0)
	)
	ON COMMIT DELETE ROWS';
EXCEPTION
 WHEN OTHERS THEN NULL;
END;

EXECUTE 'TRUNCATE TABLE tb_tmp_tax_matrix';

   -- Insert New Tax Matrix columns into temporary table -------------------------------------------
   INSERT INTO tb_tmp_tax_matrix (
      tax_matrix_id, matrix_state_code, driver_global_flag,
      driver_01, driver_01_desc,
      driver_01_thru, driver_01_thru_desc,
      driver_02, driver_02_desc,
      driver_02_thru, driver_02_thru_desc,
      driver_03, driver_03_desc,
      driver_03_thru, driver_03_thru_desc,
      driver_04, driver_04_desc,
      driver_04_thru, driver_04_thru_desc,
      driver_05, driver_05_desc,
      driver_05_thru, driver_05_thru_desc,
      driver_06, driver_06_desc,
      driver_06_thru, driver_06_thru_desc,
      driver_07, driver_07_desc,
      driver_07_thru, driver_07_thru_desc,
      driver_08, driver_08_desc,
      driver_08_thru, driver_08_thru_desc,
      driver_09, driver_09_desc,
      driver_09_thru, driver_09_thru_desc,
      driver_10, driver_10_desc,
      driver_10_thru, driver_10_thru_desc,
      driver_11, driver_11_desc,
      driver_11_thru, driver_11_thru_desc,
      driver_12, driver_12_desc,
      driver_12_thru, driver_12_thru_desc,
      driver_13, driver_13_desc,
      driver_13_thru, driver_13_thru_desc,
      driver_14, driver_14_desc,
      driver_14_thru, driver_14_thru_desc,
      driver_15, driver_15_desc,
      driver_15_thru, driver_15_thru_desc,
      driver_16, driver_16_desc,
      driver_16_thru, driver_16_thru_desc,
      driver_17, driver_17_desc,
      driver_17_thru, driver_17_thru_desc,
      driver_18, driver_18_desc,
      driver_18_thru, driver_18_thru_desc,
      driver_19, driver_19_desc,
      driver_19_thru, driver_19_thru_desc,
      driver_20, driver_20_desc,
      driver_20_thru, driver_20_thru_desc,
      driver_21, driver_21_desc,
      driver_21_thru, driver_21_thru_desc,
      driver_22, driver_22_desc,
      driver_22_thru, driver_22_thru_desc,
      driver_23, driver_23_desc,
      driver_23_thru, driver_23_thru_desc,
      driver_24, driver_24_desc,
      driver_24_thru, driver_24_thru_desc,
      driver_25, driver_25_desc,
      driver_25_thru, driver_25_thru_desc,
      driver_26, driver_26_desc,
      driver_26_thru, driver_26_thru_desc,
      driver_27, driver_27_desc,
      driver_27_thru, driver_27_thru_desc,
      driver_28, driver_28_desc,
      driver_28_thru, driver_28_thru_desc,
      driver_29, driver_29_desc,
      driver_29_thru, driver_29_thru_desc,
      driver_30, driver_30_desc,
      driver_30_thru, driver_30_thru_desc,
      binary_weight, significant_digits,
      effective_date, expiration_date,
      relation_sign, relation_amount,
      then_hold_code_flag, then_taxcode_detail_id, then_cch_taxcat_code, then_cch_group_code, then_cch_item_code,
      else_hold_code_flag, else_taxcode_detail_id, else_cch_taxcat_code, else_cch_group_code, else_cch_item_code,
      comments,
      last_used_timestamp, update_user_id, update_timestamp )
   VALUES (
      NEW.tax_matrix_id, NEW.matrix_state_code, NEW.driver_global_flag,
      NEW.driver_01, NEW.driver_01_desc,
      NEW.driver_01_thru, NEW.driver_01_thru_desc,
      NEW.driver_02, NEW.driver_02_desc,
      NEW.driver_02_thru, NEW.driver_02_thru_desc,
      NEW.driver_03, NEW.driver_03_desc,
      NEW.driver_03_thru, NEW.driver_03_thru_desc,
      NEW.driver_04, NEW.driver_04_desc,
      NEW.driver_04_thru, NEW.driver_04_thru_desc,
      NEW.driver_05, NEW.driver_05_desc,
      NEW.driver_05_thru, NEW.driver_05_thru_desc,
      NEW.driver_06, NEW.driver_06_desc,
      NEW.driver_06_thru, NEW.driver_06_thru_desc,
      NEW.driver_07, NEW.driver_07_desc,
      NEW.driver_07_thru, NEW.driver_07_thru_desc,
      NEW.driver_08, NEW.driver_08_desc,
      NEW.driver_08_thru, NEW.driver_08_thru_desc,
      NEW.driver_09, NEW.driver_09_desc,
      NEW.driver_09_thru, NEW.driver_09_thru_desc,
      NEW.driver_10, NEW.driver_10_desc,
      NEW.driver_10_thru, NEW.driver_10_thru_desc,
      NEW.driver_11, NEW.driver_11_desc,
      NEW.driver_11_thru, NEW.driver_11_thru_desc,
      NEW.driver_12, NEW.driver_12_desc,
      NEW.driver_12_thru, NEW.driver_12_thru_desc,
      NEW.driver_13, NEW.driver_13_desc,
      NEW.driver_13_thru, NEW.driver_13_thru_desc,
      NEW.driver_14, NEW.driver_14_desc,
      NEW.driver_14_thru, NEW.driver_14_thru_desc,
      NEW.driver_15, NEW.driver_15_desc,
      NEW.driver_15_thru, NEW.driver_15_thru_desc,
      NEW.driver_16, NEW.driver_16_desc,
      NEW.driver_16_thru, NEW.driver_16_thru_desc,
      NEW.driver_17, NEW.driver_17_desc,
      NEW.driver_17_thru, NEW.driver_17_thru_desc,
      NEW.driver_18, NEW.driver_18_desc,
      NEW.driver_18_thru, NEW.driver_18_thru_desc,
      NEW.driver_19, NEW.driver_19_desc,
      NEW.driver_19_thru, NEW.driver_19_thru_desc,
      NEW.driver_20, NEW.driver_20_desc,
      NEW.driver_20_thru, NEW.driver_20_thru_desc,
      NEW.driver_21, NEW.driver_21_desc,
      NEW.driver_21_thru, NEW.driver_21_thru_desc,
      NEW.driver_22, NEW.driver_22_desc,
      NEW.driver_22_thru, NEW.driver_22_thru_desc,
      NEW.driver_23, NEW.driver_23_desc,
      NEW.driver_23_thru, NEW.driver_23_thru_desc,
      NEW.driver_24, NEW.driver_24_desc,
      NEW.driver_24_thru, NEW.driver_24_thru_desc,
      NEW.driver_25, NEW.driver_25_desc,
      NEW.driver_25_thru, NEW.driver_25_thru_desc,
      NEW.driver_26, NEW.driver_26_desc,
      NEW.driver_26_thru, NEW.driver_26_thru_desc,
      NEW.driver_27, NEW.driver_27_desc,
      NEW.driver_27_thru, NEW.driver_27_thru_desc,
      NEW.driver_28, NEW.driver_28_desc,
      NEW.driver_28_thru, NEW.driver_28_thru_desc,
      NEW.driver_29, NEW.driver_29_desc,
      NEW.driver_29_thru, NEW.driver_29_thru_desc,
      NEW.driver_30, NEW.driver_30_desc,
      NEW.driver_30_thru, NEW.driver_30_thru_desc,
      NEW.binary_weight, NEW.significant_digits,
      NEW.effective_date, NEW.expiration_date,
      NEW.relation_sign, NEW.relation_amount,
      NEW.then_hold_code_flag, NEW.then_taxcode_detail_id, NEW.then_cch_taxcat_code,  NEW.then_cch_group_code,  NEW.then_cch_item_code,
      NEW.else_hold_code_flag, NEW.else_taxcode_detail_id, NEW.else_cch_taxcat_code,  NEW.else_cch_group_code,  NEW.else_cch_item_code,
      NEW.comments,
      NEW.last_used_timestamp, NEW.update_user_id, NEW.update_timestamp );

   -- ****** CREATE Temporary Table to hold "Bind" Values ****** -----------------------------------------
   BEGIN
    EXECUTE 'CREATE TEMPORARY TABLE tb_tax_matrix_a_i_tmp (param_name varchar(30), param_value varchar(30))';
   EXCEPTION 
     -- Table may already exist if using Connection/Session Pooling, Ignore
     WHEN OTHERS THEN NULL;
   END;

   -- ****** Read and Process Transactions ****** -----------------------------------------
    OPEN transaction_detail_cursor
    FOR EXECUTE vc_transaction_detail_stmt;
    LOOP
      FETCH transaction_detail_cursor
       INTO transaction_detail;
      
      IF (NOT FOUND) THEN 
	EXIT;
      END IF;

      -- Populate and/or Clear Transaction Detail working fields
      transaction_detail.transaction_ind := NULL;
      transaction_detail.suspend_ind := NULL;
      transaction_detail.tax_matrix_id := NEW.tax_matrix_id;
      transaction_detail.update_user_id := USER;
      transaction_detail.update_timestamp := v_sysdate;

      -- Get "Then" TaxCode Elements
      BEGIN
         SELECT taxcode_state_code,
                taxcode_type_code,
                taxcode_code,
                jurisdiction_id,
                measure_type_code,
                taxtype_code
           INTO STRICT
	        v_then_taxcode_state_code,
                v_then_taxcode_type_code,
                v_then_taxcode_code,
                v_then_jurisdiction_id,
                v_then_measure_type_code,
                v_then_taxtype_code
           FROM tb_taxcode_detail
          WHERE taxcode_detail_id = NEW.then_taxcode_detail_id;
      EXCEPTION
         WHEN OTHERS THEN
            v_then_taxcode_state_code := NULL;
            v_then_taxcode_type_code := NULL;
            v_then_taxcode_code := NULL;
            v_then_jurisdiction_id := NULL;
            v_then_measure_type_code := NULL;
            v_then_taxtype_code := NULL;
      END;

      -- Get "Else" TaxCode Elements
      BEGIN
         SELECT taxcode_state_code,
                taxcode_type_code,
                taxcode_code,
                jurisdiction_id,
                measure_type_code,
                taxtype_code
           INTO v_else_taxcode_state_code,
                v_else_taxcode_type_code,
                v_else_taxcode_code,
                v_else_jurisdiction_id,
                v_else_measure_type_code,
                v_else_taxtype_code
           FROM tb_taxcode_detail
          WHERE taxcode_detail_id = NEW.else_taxcode_detail_id;
      EXCEPTION
         WHEN OTHERS THEN
            v_else_taxcode_state_code := NULL;
            v_else_taxcode_type_code := NULL;
            v_else_taxcode_code := NULL;
            v_else_jurisdiction_id := NULL;
            v_else_measure_type_code := NULL;
            v_else_taxtype_code := NULL;
      END;

      -- Continue if Elements Found
      IF ( v_then_taxcode_state_code IS NOT NULL ) AND
         (( NEW.else_taxcode_detail_id IS NULL OR NEW.else_taxcode_detail_id = 0 ) OR
          ( NEW.else_taxcode_detail_id IS NOT NULL AND NEW.else_taxcode_detail_id <> 0 AND v_else_taxcode_state_code IS NOT NULL )) THEN
         -- Determine "Then" or "Else" Results
         v_relation_amount := NEW.relation_amount;
         IF NEW.relation_sign = 'na' THEN
            v_relation_amount := 0;
            v_less_hold_code_flag := NEW.then_hold_code_flag;
            v_less_taxcode_detail_id := NEW.then_taxcode_detail_id;
            v_less_taxcode_state_code := v_then_taxcode_state_code;
            v_less_taxcode_type_code := v_then_taxcode_type_code;
            v_less_taxcode_code := v_then_taxcode_code;
            v_less_jurisdiction_id := v_then_jurisdiction_id;
            v_less_measure_type_code := v_then_measure_type_code;
            v_less_taxtype_code := v_then_taxtype_code;
            v_less_cch_taxcat_code := NEW.then_cch_taxcat_code;
            v_less_cch_group_code := NEW.then_cch_group_code;
            -- v_less_cch_item_code := NEW.then_cch_item_code;
            v_equal_hold_code_flag := NEW.then_hold_code_flag;
            v_equal_taxcode_detail_id := NEW.then_taxcode_detail_id;
            v_equal_taxcode_state_code := v_then_taxcode_state_code;
            v_equal_taxcode_type_code := v_then_taxcode_type_code;
            v_equal_taxcode_code := v_then_taxcode_code;
            v_equal_jurisdiction_id := v_then_jurisdiction_id;
            v_equal_measure_type_code := v_then_measure_type_code;
            v_equal_taxtype_code := v_then_taxtype_code;
            v_equal_cch_taxcat_code := NEW.then_cch_taxcat_code;
            v_equal_cch_group_code := NEW.then_cch_group_code;
            -- v_equal_cch_item_code := NEW.then_cch_item_code;
            v_greater_hold_code_flag := NEW.then_hold_code_flag;
            v_greater_taxcode_detail_id := NEW.then_taxcode_detail_id;
            v_greater_taxcode_state_code := v_then_taxcode_state_code;
            v_greater_taxcode_type_code := v_then_taxcode_type_code;
            v_greater_taxcode_code := v_then_taxcode_code;
            v_greater_jurisdiction_id := v_then_jurisdiction_id;
            v_greater_measure_type_code := v_then_measure_type_code;
            v_greater_taxtype_code := v_then_taxtype_code;
            v_greater_cch_taxcat_code := NEW.then_cch_taxcat_code;
            v_greater_cch_group_code := NEW.then_cch_group_code;
            -- v_greater_cch_item_code := NEW.then_cch_item_code;
         ELSIF NEW.relation_sign = '=' THEN
            v_less_hold_code_flag := NEW.else_hold_code_flag;
            v_less_taxcode_detail_id := NEW.else_taxcode_detail_id;
            v_less_taxcode_state_code := v_else_taxcode_state_code;
            v_less_taxcode_type_code := v_else_taxcode_type_code;
            v_less_taxcode_code := v_else_taxcode_code;
            v_less_jurisdiction_id := v_else_jurisdiction_id;
            v_less_measure_type_code := v_else_measure_type_code;
            v_less_taxtype_code := v_else_taxtype_code;
            v_less_cch_taxcat_code := NEW.else_cch_taxcat_code;
            v_less_cch_group_code := NEW.else_cch_group_code;
            -- v_less_cch_item_code := NEW.else_cch_item_code;
            v_equal_hold_code_flag := NEW.then_hold_code_flag;
            v_equal_taxcode_detail_id := NEW.then_taxcode_detail_id;
            v_equal_taxcode_state_code := v_then_taxcode_state_code;
            v_equal_taxcode_type_code := v_then_taxcode_type_code;
            v_equal_taxcode_code := v_then_taxcode_code;
            v_equal_jurisdiction_id := v_then_jurisdiction_id;
            v_equal_measure_type_code := v_then_measure_type_code;
            v_equal_taxtype_code := v_then_taxtype_code;
            v_equal_cch_taxcat_code := NEW.then_cch_taxcat_code;
            v_equal_cch_group_code := NEW.then_cch_group_code;
            -- v_equal_cch_item_code := NEW.then_cch_item_code;
            v_greater_hold_code_flag := NEW.else_hold_code_flag;
            v_greater_taxcode_detail_id := NEW.else_taxcode_detail_id;
            v_greater_taxcode_state_code := v_else_taxcode_state_code;
            v_greater_taxcode_type_code := v_else_taxcode_type_code;
            v_greater_taxcode_code := v_else_taxcode_code;
            v_greater_jurisdiction_id := v_else_jurisdiction_id;
            v_greater_measure_type_code := v_else_measure_type_code;
            v_greater_taxtype_code := v_else_taxtype_code;
            v_greater_cch_taxcat_code := NEW.else_cch_taxcat_code;
            v_greater_cch_group_code := NEW.else_cch_group_code;
            -- v_greater_cch_item_code := NEW.else_cch_item_code;
         ELSIF NEW.relation_sign = '<' THEN
            v_less_hold_code_flag := NEW.then_hold_code_flag;
            v_less_taxcode_detail_id := NEW.then_taxcode_detail_id;
            v_less_taxcode_state_code := v_then_taxcode_state_code;
            v_less_taxcode_type_code := v_then_taxcode_type_code;
            v_less_taxcode_code := v_then_taxcode_code;
            v_less_jurisdiction_id := v_then_jurisdiction_id;
            v_less_measure_type_code := v_then_measure_type_code;
            v_less_taxtype_code := v_then_taxtype_code;
            v_less_cch_taxcat_code := NEW.then_cch_taxcat_code;
            v_less_cch_group_code := NEW.then_cch_group_code;
            -- v_less_cch_item_code := NEW.then_cch_item_code;
            v_equal_hold_code_flag := NEW.else_hold_code_flag;
            v_equal_taxcode_detail_id := NEW.else_taxcode_detail_id;
            v_equal_taxcode_state_code := v_else_taxcode_state_code;
            v_equal_taxcode_type_code := v_else_taxcode_type_code;
            v_equal_taxcode_code := v_else_taxcode_code;
            v_equal_jurisdiction_id := v_else_jurisdiction_id;
            v_equal_measure_type_code := v_else_measure_type_code;
            v_equal_taxtype_code := v_else_taxtype_code;
            v_equal_cch_taxcat_code := NEW.else_cch_taxcat_code;
            v_equal_cch_group_code := NEW.else_cch_group_code;
            -- v_equal_cch_item_code := NEW.else_cch_item_code;
            v_greater_hold_code_flag := NEW.else_hold_code_flag;
            v_greater_taxcode_detail_id := NEW.else_taxcode_detail_id;
            v_greater_taxcode_state_code := v_else_taxcode_state_code;
            v_greater_taxcode_type_code := v_else_taxcode_type_code;
            v_greater_taxcode_code := v_else_taxcode_code;
            v_greater_jurisdiction_id := v_else_jurisdiction_id;
            v_greater_measure_type_code := v_else_measure_type_code;
            v_greater_taxtype_code := v_else_taxtype_code;
            v_greater_cch_taxcat_code := NEW.else_cch_taxcat_code;
            v_greater_cch_group_code := NEW.else_cch_group_code;
            -- v_greater_cch_item_code := NEW.else_cch_item_code;
         ELSIF NEW.relation_sign = '<=' THEN
            v_less_hold_code_flag := NEW.then_hold_code_flag;
            v_less_taxcode_detail_id := NEW.then_taxcode_detail_id;
            v_less_taxcode_state_code := v_then_taxcode_state_code;
            v_less_taxcode_type_code := v_then_taxcode_type_code;
            v_less_taxcode_code := v_then_taxcode_code;
            v_less_jurisdiction_id := v_then_jurisdiction_id;
            v_less_measure_type_code := v_then_measure_type_code;
            v_less_taxtype_code := v_then_taxtype_code;
            v_less_cch_taxcat_code := NEW.then_cch_taxcat_code;
            v_less_cch_group_code := NEW.then_cch_group_code;
            -- v_less_cch_item_code := NEW.then_cch_item_code;
            v_equal_hold_code_flag := NEW.then_hold_code_flag;
            v_equal_taxcode_detail_id := NEW.then_taxcode_detail_id;
            v_equal_taxcode_state_code := v_then_taxcode_state_code;
            v_equal_taxcode_type_code := v_then_taxcode_type_code;
            v_equal_taxcode_code := v_then_taxcode_code;
            v_equal_jurisdiction_id := v_then_jurisdiction_id;
            v_equal_measure_type_code := v_then_measure_type_code;
            v_equal_taxtype_code := v_then_taxtype_code;
            v_equal_cch_taxcat_code := NEW.then_cch_taxcat_code;
            v_equal_cch_group_code := NEW.then_cch_group_code;
            -- v_equal_cch_item_code := NEW.then_cch_item_code;
            v_greater_hold_code_flag := NEW.else_hold_code_flag;
            v_greater_taxcode_detail_id := NEW.else_taxcode_detail_id;
            v_greater_taxcode_state_code := v_else_taxcode_state_code;
            v_greater_taxcode_type_code := v_else_taxcode_type_code;
            v_greater_taxcode_code := v_else_taxcode_code;
            v_greater_jurisdiction_id := v_else_jurisdiction_id;
            v_greater_measure_type_code := v_else_measure_type_code;
            v_greater_taxtype_code := v_else_taxtype_code;
            v_greater_cch_taxcat_code := NEW.else_cch_taxcat_code;
            v_greater_cch_group_code := NEW.else_cch_group_code;
            -- v_greater_cch_item_code := NEW.else_cch_item_code;
         ELSIF NEW.relation_sign = '>' THEN
            v_less_hold_code_flag := NEW.else_hold_code_flag;
            v_less_taxcode_detail_id := NEW.else_taxcode_detail_id;
            v_less_taxcode_state_code := v_else_taxcode_state_code;
            v_less_taxcode_type_code := v_else_taxcode_type_code;
            v_less_taxcode_code := v_else_taxcode_code;
            v_less_jurisdiction_id := v_else_jurisdiction_id;
            v_less_measure_type_code := v_else_measure_type_code;
            v_less_taxtype_code := v_else_taxtype_code;
            v_less_cch_taxcat_code := NEW.else_cch_taxcat_code;
            v_less_cch_group_code := NEW.else_cch_group_code;
            -- v_less_cch_item_code := NEW.else_cch_item_code;
            v_equal_hold_code_flag := NEW.else_hold_code_flag;
            v_equal_taxcode_detail_id := NEW.else_taxcode_detail_id;
            v_equal_taxcode_state_code := v_else_taxcode_state_code;
            v_equal_taxcode_type_code := v_else_taxcode_type_code;
            v_equal_taxcode_code := v_else_taxcode_code;
            v_equal_jurisdiction_id := v_else_jurisdiction_id;
            v_equal_measure_type_code := v_else_measure_type_code;
            v_equal_taxtype_code := v_else_taxtype_code;
            v_equal_cch_taxcat_code := NEW.else_cch_taxcat_code;
            v_equal_cch_group_code := NEW.else_cch_group_code;
            -- v_equal_cch_item_code := NEW.else_cch_item_code;
            v_greater_hold_code_flag := NEW.then_hold_code_flag;
            v_greater_taxcode_detail_id := NEW.then_taxcode_detail_id;
            v_greater_taxcode_state_code := v_then_taxcode_state_code;
            v_greater_taxcode_type_code := v_then_taxcode_type_code;
            v_greater_taxcode_code := v_then_taxcode_code;
            v_greater_jurisdiction_id := v_then_jurisdiction_id;
            v_greater_measure_type_code := v_then_measure_type_code;
            v_greater_taxtype_code := v_then_taxtype_code;
            v_greater_cch_taxcat_code := NEW.then_cch_taxcat_code;
            v_greater_cch_group_code := NEW.then_cch_group_code;
            -- v_greater_cch_item_code := NEW.then_cch_item_code;
         ELSIF NEW.relation_sign = '>=' THEN
            v_less_hold_code_flag := NEW.else_hold_code_flag;
            v_less_taxcode_detail_id := NEW.else_taxcode_detail_id;
            v_less_taxcode_state_code := v_else_taxcode_state_code;
            v_less_taxcode_type_code := v_else_taxcode_type_code;
            v_less_taxcode_code := v_else_taxcode_code;
            v_less_jurisdiction_id := v_else_jurisdiction_id;
            v_less_measure_type_code := v_else_measure_type_code;
            v_less_taxtype_code := v_else_taxtype_code;
            v_less_cch_taxcat_code := NEW.else_cch_taxcat_code;
            v_less_cch_group_code := NEW.else_cch_group_code;
            -- v_less_cch_item_code := NEW.else_cch_item_code;
            v_equal_hold_code_flag := NEW.then_hold_code_flag;
            v_equal_taxcode_detail_id := NEW.then_taxcode_detail_id;
            v_equal_taxcode_state_code := v_then_taxcode_state_code;
            v_equal_taxcode_type_code := v_then_taxcode_type_code;
            v_equal_taxcode_code := v_then_taxcode_code;
            v_equal_jurisdiction_id := v_then_jurisdiction_id;
            v_equal_measure_type_code := v_then_measure_type_code;
            v_equal_taxtype_code := v_then_taxtype_code;
            v_equal_cch_taxcat_code := NEW.then_cch_taxcat_code;
            v_equal_cch_group_code := NEW.then_cch_group_code;
            -- v_equal_cch_item_code := NEW.then_cch_item_code;
            v_greater_hold_code_flag := NEW.then_hold_code_flag;
            v_greater_taxcode_detail_id := NEW.then_taxcode_detail_id;
            v_greater_taxcode_state_code := v_then_taxcode_state_code;
            v_greater_taxcode_type_code := v_then_taxcode_type_code;
            v_greater_taxcode_code := v_then_taxcode_code;
            v_greater_jurisdiction_id := v_then_jurisdiction_id;
            v_greater_measure_type_code := v_then_measure_type_code;
            v_greater_taxtype_code := v_then_taxtype_code;
            v_greater_cch_taxcat_code := NEW.then_cch_taxcat_code;
            v_greater_cch_group_code := NEW.then_cch_group_code;
            -- v_greater_cch_item_code := NEW.then_cch_item_code;
         ELSIF NEW.relation_sign = '<>' THEN
            v_less_hold_code_flag := NEW.then_hold_code_flag;
            v_less_taxcode_detail_id := NEW.then_taxcode_detail_id;
            v_less_taxcode_state_code := v_then_taxcode_state_code;
            v_less_taxcode_type_code := v_then_taxcode_type_code;
            v_less_taxcode_code := v_then_taxcode_code;
            v_less_jurisdiction_id := v_then_jurisdiction_id;
            v_less_measure_type_code := v_then_measure_type_code;
            v_less_taxtype_code := v_then_taxtype_code;
            v_less_cch_taxcat_code := NEW.then_cch_taxcat_code;
            v_less_cch_group_code := NEW.then_cch_group_code;
            -- v_less_cch_item_code := NEW.then_cch_item_code;
            v_equal_hold_code_flag := NEW.else_hold_code_flag;
            v_equal_taxcode_detail_id := NEW.else_taxcode_detail_id;
            v_equal_taxcode_state_code := v_else_taxcode_state_code;
            v_equal_taxcode_type_code := v_else_taxcode_type_code;
            v_equal_taxcode_code := v_else_taxcode_code;
            v_equal_jurisdiction_id := v_else_jurisdiction_id;
            v_equal_measure_type_code := v_else_measure_type_code;
            v_equal_taxtype_code := v_else_taxtype_code;
            v_equal_cch_taxcat_code := NEW.else_cch_taxcat_code;
            v_equal_cch_group_code := NEW.else_cch_group_code;
            -- v_equal_cch_item_code := NEW.else_cch_item_code;
            v_greater_hold_code_flag := NEW.then_hold_code_flag;
            v_greater_taxcode_detail_id := NEW.then_taxcode_detail_id;
            v_greater_taxcode_state_code := v_then_taxcode_state_code;
            v_greater_taxcode_type_code := v_then_taxcode_type_code;
            v_greater_taxcode_code := v_then_taxcode_code;
            v_greater_jurisdiction_id := v_then_jurisdiction_id;
            v_greater_measure_type_code := v_then_measure_type_code;
            v_greater_taxtype_code := v_then_taxtype_code;
            v_greater_cch_taxcat_code := NEW.then_cch_taxcat_code;
            v_greater_cch_group_code := NEW.then_cch_group_code;
            -- v_greater_cch_item_code := NEW.then_cch_item_code;
         END IF;

         SELECT CASE SIGN(transaction_detail.gl_line_itm_dist_amt - v_relation_amount) WHEN -1 THEN v_less_hold_code_flag WHEN 0 THEN v_equal_hold_code_flag WHEN 1 THEN v_greater_hold_code_flag END,
                CASE SIGN(transaction_detail.gl_line_itm_dist_amt - v_relation_amount) WHEN -1 THEN v_less_taxcode_detail_id WHEN 0 THEN v_equal_taxcode_detail_id WHEN 1 THEN v_greater_taxcode_detail_id END,
                CASE SIGN(transaction_detail.gl_line_itm_dist_amt - v_relation_amount) WHEN -1 THEN v_less_taxcode_state_code WHEN 0 THEN v_equal_taxcode_state_code WHEN 1 THEN v_greater_taxcode_state_code END,
                CASE SIGN(transaction_detail.gl_line_itm_dist_amt - v_relation_amount) WHEN -1 THEN v_less_taxcode_type_code WHEN 0 THEN v_equal_taxcode_type_code WHEN 1 THEN v_greater_taxcode_type_code END,
                CASE SIGN(transaction_detail.gl_line_itm_dist_amt - v_relation_amount) WHEN -1 THEN v_less_taxcode_code WHEN 0 THEN v_equal_taxcode_code WHEN 1 THEN v_greater_taxcode_code END,
                CASE SIGN(transaction_detail.gl_line_itm_dist_amt - v_relation_amount) WHEN -1 THEN v_less_jurisdiction_id WHEN 0 THEN v_equal_jurisdiction_id WHEN 1 THEN v_greater_jurisdiction_id END,
                CASE SIGN(transaction_detail.gl_line_itm_dist_amt - v_relation_amount) WHEN -1 THEN v_less_measure_type_code WHEN 0 THEN v_equal_measure_type_code WHEN 1 THEN v_greater_measure_type_code END,
                CASE SIGN(transaction_detail.gl_line_itm_dist_amt - v_relation_amount) WHEN -1 THEN v_less_taxtype_code WHEN 0 THEN v_equal_taxtype_code WHEN 1 THEN v_greater_taxtype_code END,
                CASE SIGN(transaction_detail.gl_line_itm_dist_amt - v_relation_amount) WHEN -1 THEN v_less_cch_taxcat_code WHEN 0 THEN v_equal_cch_taxcat_code WHEN 1 THEN v_greater_cch_taxcat_code END,
                CASE SIGN(transaction_detail.gl_line_itm_dist_amt - v_relation_amount) WHEN -1 THEN v_less_cch_group_code WHEN 0 THEN v_equal_cch_group_code WHEN 1 THEN v_greater_cch_group_code END
           INTO STRICT v_hold_code_flag, transaction_detail.taxcode_detail_id, transaction_detail.taxcode_state_code, transaction_detail.taxcode_type_code, transaction_detail.taxcode_code, v_override_jurisdiction_id, transaction_detail.measure_type_code, v_taxtype_code, transaction_detail.cch_taxcat_code, transaction_detail.cch_group_code
           ;

         -- ****** DETERMINE TYPE of TAXCODE ****** --
         -- The Tax Code is Taxable
         IF transaction_detail.taxcode_type_code = 'T' THEN
            -- If Override jurisdiction id is not blank or 0 then use it instead of location matrix
            IF v_override_jurisdiction_id IS NOT NULL AND v_override_jurisdiction_id <> 0 THEN
               transaction_detail.jurisdiction_id := v_override_jurisdiction_id;
               transaction_detail.manual_jurisdiction_ind := 'AO';
               location_matrix.override_taxtype_code := '*NO';
               location_matrix.state_flag := '1';                             -- MBF01
               location_matrix.county_flag := '1';                            -- MBF01
               location_matrix.county_local_flag := '1';                      -- MBF01
               location_matrix.city_flag := '1';                              -- MBF01
               location_matrix.city_local_flag := '1';                        -- MBF01
            ELSE
               -- Search for Location Matrix if not already found AND not already manually applied!!
               IF transaction_detail.location_matrix_id IS NULL AND transaction_detail.jurisdiction_id IS NULL THEN
                  v_transaction_detail_id := transaction_detail.transaction_detail_id;
                  -- Search Location Matrix for matches
                  BEGIN
		     -- Prep TMP table with values
		     DELETE FROM tb_tax_matrix_a_i_tmp;
		     INSERT INTO tb_tax_matrix_a_i_tmp(param_name, param_value) VALUES ('v_transaction_detail_id',  v_transaction_detail_id);

		     OPEN location_matrix_cursor
                       FOR EXECUTE vc_location_matrix_stmt;

                     FETCH location_matrix_cursor
                      INTO location_matrix;
                     -- Rows found?
                     IF (FOUND) THEN
                        vn_fetch_rows := 1;
                     ELSE
                        vn_fetch_rows := 0;
                     END IF;

                     CLOSE location_matrix_cursor;
                  EXCEPTION
                     WHEN OTHERS THEN
                        vn_fetch_rows := 0;
                  END;
                  -- Location Matrix Line Found
                  IF vn_fetch_rows > 0 THEN
                     transaction_detail.location_matrix_id := location_matrix.location_matrix_id;
                     transaction_detail.jurisdiction_id := location_matrix.jurisdiction_id;
                  ELSE
                     transaction_detail.transaction_ind := 'S';
                     transaction_detail.suspend_ind := 'L';
                  END IF;

               -- Get Location Matrix if it exists AND not already manually applied!!
               ELSIF transaction_detail.location_matrix_id IS NOT NULL AND (v_override_jurisdiction_id IS NULL OR v_override_jurisdiction_id = 0) THEN
                  BEGIN
                     SELECT override_taxtype_code,
                            state_flag, county_flag, county_local_flag, city_flag, city_local_flag
                       INTO STRICT location_matrix.override_taxtype_code,
                            location_matrix.state_flag,
                            location_matrix.county_flag,
                            location_matrix.county_local_flag,
                            location_matrix.city_flag,
                            location_matrix.city_local_flag
                       FROM tb_location_matrix
                      WHERE tb_location_matrix.location_matrix_id = transaction_detail.location_matrix_id;
                  EXCEPTION
                     WHEN NO_DATA_FOUND THEN
                        location_matrix.override_taxtype_code := '*NO';
                        location_matrix.state_flag := '1';
                        location_matrix.county_flag := '1';
                        location_matrix.county_local_flag := '1';
                        location_matrix.city_flag := '1';
                        location_matrix.city_local_flag := '1';
                  END;
               ELSE
                  location_matrix.override_taxtype_code := '*NO';
                  location_matrix.state_flag := '1';
                  location_matrix.county_flag := '1';
                  location_matrix.county_local_flag := '1';
                  location_matrix.city_flag := '1';
                  location_matrix.city_local_flag := '1';
               END IF;
            END IF;

            -- Continue if not Suspended
            IF transaction_detail.transaction_ind IS NULL OR transaction_detail.transaction_ind <> 'S' THEN
               -- Get Jurisdiction TaxRates and calculate Tax Amounts
               EXECUTE sp_getusetax(transaction_detail.jurisdiction_id,
                            transaction_detail.gl_date,
                            transaction_detail.measure_type_code,
                            transaction_detail.gl_line_itm_dist_amt,
                            v_taxtype_code,
                            location_matrix.override_taxtype_code,
                            transaction_detail.invoice_freight_amt,
                            transaction_detail.invoice_discount_amt,
                            transaction_detail.transaction_state_code,
-- future? --                     transaction_detail.import_definition_code,
			    'importdefn',
                            transaction_detail.taxcode_code,
                            transaction_detail.jurisdiction_taxrate_id,
                            transaction_detail.state_use_amount,
                            transaction_detail.state_use_tier2_amount,
                            transaction_detail.state_use_tier3_amount,
                            transaction_detail.county_use_amount,
                            transaction_detail.county_local_use_amount,
                            transaction_detail.city_use_amount,
                            transaction_detail.city_local_use_amount,
                            transaction_detail.state_use_rate,
                            transaction_detail.state_use_tier2_rate,
                            transaction_detail.state_use_tier3_rate,
                            transaction_detail.state_split_amount,
                            transaction_detail.state_tier2_min_amount,
                            transaction_detail.state_tier2_max_amount,
                            transaction_detail.state_maxtax_amount,
                            transaction_detail.county_use_rate,
                            transaction_detail.county_local_use_rate,
                            transaction_detail.county_split_amount,
                            transaction_detail.county_maxtax_amount,
                            transaction_detail.county_single_flag,
                            transaction_detail.county_default_flag,
                            transaction_detail.city_use_rate,
                            transaction_detail.city_local_use_rate,
                            transaction_detail.city_split_amount,
                            transaction_detail.city_split_use_rate,
                            transaction_detail.city_single_flag,
                            transaction_detail.city_default_flag,
                            transaction_detail.combined_use_rate,
                            transaction_detail.tb_calc_tax_amt,
-- future? --                     transaction_detail.taxable_amt,
-- Targa? --                transaction_detail.user_number_10,
                            vn_taxable_amt,
                            v_taxtype_used );
               -- Suspend if tax rate id is null
               IF transaction_detail.jurisdiction_taxrate_id IS NULL OR transaction_detail.jurisdiction_taxrate_id = 0 THEN
                  transaction_detail.transaction_ind := 'S';
                  transaction_detail.suspend_ind := 'R';
               ELSE
                  -- Check for Taxable Amount -- 3351
                  IF vn_taxable_amt <> transaction_detail.gl_line_itm_dist_amt THEN
                     transaction_detail.gl_extract_amt := transaction_detail.gl_line_itm_dist_amt;
                     transaction_detail.gl_line_itm_dist_amt := vn_taxable_amt;
                  END IF;
               END IF;
            END IF;

         -- The Tax Code is Exempt
         ELSIF transaction_detail.taxcode_type_code = 'E' THEN
            transaction_detail.transaction_ind := 'P';

         -- The Tax Code is a Question
         ELSIF transaction_detail.taxcode_type_code = 'Q' THEN
           transaction_detail.transaction_ind := 'Q';

         -- The Tax Code is Unrecognized - Suspend
         ELSE
            transaction_detail.transaction_ind := 'S';
            transaction_detail.suspend_ind := '?';
         END IF;

         -- Continue if not Suspended
         IF transaction_detail.transaction_ind IS NULL OR transaction_detail.transaction_ind <> 'S' THEN
            -- Check for matrix "H"old
            IF ( v_hold_code_flag = '1' ) AND
               ( transaction_detail.taxcode_type_code = 'T' OR transaction_detail.taxcode_type_code = 'E') THEN
                  transaction_detail.transaction_ind := 'H';
            ELSE
                transaction_detail.transaction_ind := 'P';
            END IF;
         END IF;

      ELSE
         -- Elements NOT Found
         transaction_detail.transaction_ind := 'S';
         transaction_detail.suspend_ind := '?';
      END IF;

      -- Update transaction detail row
      BEGIN
         -- 3411 - add taxable amount & tax type used.
         UPDATE tb_transaction_detail
            SET gl_line_itm_dist_amt = transaction_detail.gl_line_itm_dist_amt,
                tb_calc_tax_amt = transaction_detail.tb_calc_tax_amt,
                state_use_amount = transaction_detail.state_use_amount,
                state_use_tier2_amount = transaction_detail.state_use_tier2_amount,
                state_use_tier3_amount = transaction_detail.state_use_tier3_amount,
                county_use_amount = transaction_detail.county_use_amount,
                county_local_use_amount = transaction_detail.county_local_use_amount,
                city_use_amount = transaction_detail.city_use_amount,
                city_local_use_amount = transaction_detail.city_local_use_amount,
                transaction_state_code = transaction_detail.transaction_state_code,
                transaction_ind = transaction_detail.transaction_ind,
                suspend_ind = transaction_detail.suspend_ind,
                taxcode_detail_id = transaction_detail.taxcode_detail_id,
                taxcode_state_code = transaction_detail.taxcode_state_code,
                taxcode_type_code = transaction_detail.taxcode_type_code,
                taxcode_code = transaction_detail.taxcode_code,
                cch_taxcat_code = transaction_detail.cch_taxcat_code,
                cch_group_code = transaction_detail.cch_group_code,
                tax_matrix_id = transaction_detail.tax_matrix_id,
                location_matrix_id = transaction_detail.location_matrix_id,
                jurisdiction_id = transaction_detail.jurisdiction_id,
                jurisdiction_taxrate_id = transaction_detail.jurisdiction_taxrate_id,
                manual_jurisdiction_ind = transaction_detail.manual_jurisdiction_ind,
                measure_type_code = transaction_detail.measure_type_code,
                state_use_rate = transaction_detail.state_use_rate,
                state_use_tier2_rate = transaction_detail.state_use_tier2_rate,
                state_use_tier3_rate = transaction_detail.state_use_tier3_rate,
                state_split_amount = transaction_detail.state_split_amount,
                state_tier2_min_amount = transaction_detail.state_tier2_min_amount,
                state_tier2_max_amount = transaction_detail.state_tier2_max_amount,
                state_maxtax_amount = transaction_detail.state_maxtax_amount,
                county_use_rate = transaction_detail.county_use_rate,
                county_local_use_rate = transaction_detail.county_local_use_rate,
                county_split_amount = transaction_detail.county_split_amount,
                county_maxtax_amount = transaction_detail.county_maxtax_amount,
                county_single_flag = transaction_detail.county_single_flag,
                county_default_flag = transaction_detail.county_default_flag,
                city_use_rate = transaction_detail.city_use_rate,
                city_local_use_rate = transaction_detail.city_local_use_rate,
                city_split_amount = transaction_detail.city_split_amount,
                city_split_use_rate = transaction_detail.city_split_use_rate,
                city_single_flag = transaction_detail.city_single_flag,
                city_default_flag = transaction_detail.city_default_flag,
                combined_use_rate = transaction_detail.combined_use_rate,
                gl_extract_amt = transaction_detail.gl_extract_amt,
                update_user_id = transaction_detail.update_user_id,
                update_timestamp = transaction_detail.update_timestamp
          WHERE transaction_detail_id = transaction_detail.transaction_detail_id;
         -- Error Checking
--         IF SQLCODE != 0 THEN
--            RAISE e_badupdate;
--         END IF;
--      EXCEPTION
--         WHEN e_badupdate THEN
--            NULL;
      END;
   END LOOP;
   CLOSE transaction_detail_cursor;

END IF;

--EXCEPTION
--  WHEN OTHERS THEN
--vc_test1 := substr(vc_transaction_detail_stmt,1,4000);
--vc_test2 := substr(vc_transaction_detail_stmt,4001,4000);
--insert into tb_debug(row_joe, row_bob) values(vc_test1, vc_test2);
--      NULL;
END;
$$ LANGUAGE plpgsql;


CREATE TRIGGER tb_tax_matrix_a_i
AFTER INSERT ON tb_tax_matrix
FOR EACH ROW
EXECUTE PROCEDURE tb_tax_matrix_a_i();

