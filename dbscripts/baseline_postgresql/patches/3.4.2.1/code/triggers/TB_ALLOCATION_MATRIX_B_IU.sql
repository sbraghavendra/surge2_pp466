CREATE OR REPLACE FUNCTION tb_allocation_matrix_b_iu() RETURNS TRIGGER AS $$
/* ************************************************************************************************/
/* Object Type/Name: Trigger/Before Insert/Update - tb_allocation_matrix_b_iu                     */
/* Author:           Michael B. Fuller                                                            */
/* Date:             a long time ago                                                              */
/* Description:      An Allocation Matrix line has been inserted or updated                       */
/* Arguments:        None                                                                         */
/* Returns:          None                                                                         */
/* ************************************************************************************************/
/* Maintenance History:                                                                           */
/* Scan  Author       Date     Release    Description                                  Ticket #   */
/* ----- ---------- ---------- ---------- -------------------------------------------- ---------- */
/* MBF01 M. Fuller  10/05/2007 3.4.1.1    Add Calcs for Binary Weight and Sign. Digits 899        */
/* ************************************************************************************************/
DECLARE
-- Table defined variables
   v_binary_weight                 tb_allocation_matrix.binary_weight%TYPE           := 0;
   v_significant_digits            tb_allocation_matrix.significant_digits%TYPE      := NULL;

-- ****** Program starts ****** --------------------------------------------------------------------
BEGIN
	IF INSERTING THEN
		SELECT NEXTVAL('sq_tb_allocation_matrix_id')
		INTO STRICT NEW.allocation_matrix_id;
	
		NEW.driver_01 := COALESCE(NEW.driver_01,'*ALL');
		NEW.driver_02 := COALESCE(NEW.driver_02,'*ALL');
		NEW.driver_03 := COALESCE(NEW.driver_03,'*ALL');
		NEW.driver_04 := COALESCE(NEW.driver_04,'*ALL');
		NEW.driver_05 := COALESCE(NEW.driver_05,'*ALL');
		NEW.driver_06 := COALESCE(NEW.driver_06,'*ALL');
		NEW.driver_07 := COALESCE(NEW.driver_07,'*ALL');
		NEW.driver_08 := COALESCE(NEW.driver_08,'*ALL');
		NEW.driver_09 := COALESCE(NEW.driver_09,'*ALL');
		NEW.driver_10 := COALESCE(NEW.driver_10,'*ALL');
		NEW.driver_11 := COALESCE(NEW.driver_11,'*ALL');
		NEW.driver_12 := COALESCE(NEW.driver_12,'*ALL');
		NEW.driver_13 := COALESCE(NEW.driver_13,'*ALL');
		NEW.driver_14 := COALESCE(NEW.driver_14,'*ALL');
		NEW.driver_15 := COALESCE(NEW.driver_15,'*ALL');
		NEW.driver_16 := COALESCE(NEW.driver_16,'*ALL');
		NEW.driver_17 := COALESCE(NEW.driver_17,'*ALL');
		NEW.driver_18 := COALESCE(NEW.driver_18,'*ALL');
		NEW.driver_19 := COALESCE(NEW.driver_19,'*ALL');
		NEW.driver_20 := COALESCE(NEW.driver_20,'*ALL');
		NEW.driver_21 := COALESCE(NEW.driver_21,'*ALL');
		NEW.driver_22 := COALESCE(NEW.driver_22,'*ALL');
		NEW.driver_23 := COALESCE(NEW.driver_23,'*ALL');
		NEW.driver_24 := COALESCE(NEW.driver_24,'*ALL');
		NEW.driver_25 := COALESCE(NEW.driver_25,'*ALL');
		NEW.driver_26 := COALESCE(NEW.driver_26,'*ALL');
		NEW.driver_27 := COALESCE(NEW.driver_27,'*ALL');
		NEW.driver_28 := COALESCE(NEW.driver_28,'*ALL');
		NEW.driver_29 := COALESCE(NEW.driver_29,'*ALL');
		NEW.driver_30 := COALESCE(NEW.driver_30,'*ALL');

	      SELECT
		 CASE COALESE(NEW.driver_30,'*ALL') WHEN '*ALL' THEN 0 ELSE POWER(2,0)  END +
		 CASE COALESE(NEW.driver_29,'*ALL') WHEN '*ALL' THEN 0 ELSE POWER(2,1)  END +
		 CASE COALESE(NEW.driver_28,'*ALL') WHEN '*ALL' THEN 0 ELSE POWER(2,2)  END +
		 CASE COALESE(NEW.driver_27,'*ALL') WHEN '*ALL' THEN 0 ELSE POWER(2,3)  END +
		 CASE COALESE(NEW.driver_26,'*ALL') WHEN '*ALL' THEN 0 ELSE POWER(2,4)  END +
		 CASE COALESE(NEW.driver_25,'*ALL') WHEN '*ALL' THEN 0 ELSE POWER(2,5)  END +
		 CASE COALESE(NEW.driver_24,'*ALL') WHEN '*ALL' THEN 0 ELSE POWER(2,6)  END +
		 CASE COALESE(NEW.driver_23,'*ALL') WHEN '*ALL' THEN 0 ELSE POWER(2,7)  END +
		 CASE COALESE(NEW.driver_22,'*ALL') WHEN '*ALL' THEN 0 ELSE POWER(2,8)  END +
		 CASE COALESE(NEW.driver_21,'*ALL') WHEN '*ALL' THEN 0 ELSE POWER(2,9)  END +
		 CASE COALESE(NEW.driver_20,'*ALL') WHEN '*ALL' THEN 0 ELSE POWER(2,10) END +
		 CASE COALESE(NEW.driver_19,'*ALL') WHEN '*ALL' THEN 0 ELSE POWER(2,11) END +
		 CASE COALESE(NEW.driver_18,'*ALL') WHEN '*ALL' THEN 0 ELSE POWER(2,12) END +
		 CASE COALESE(NEW.driver_17,'*ALL') WHEN '*ALL' THEN 0 ELSE POWER(2,13) END +
		 CASE COALESE(NEW.driver_16,'*ALL') WHEN '*ALL' THEN 0 ELSE POWER(2,14) END +
		 CASE COALESE(NEW.driver_15,'*ALL') WHEN '*ALL' THEN 0 ELSE POWER(2,15) END +
		 CASE COALESE(NEW.driver_14,'*ALL') WHEN '*ALL' THEN 0 ELSE POWER(2,16) END +
		 CASE COALESE(NEW.driver_13,'*ALL') WHEN '*ALL' THEN 0 ELSE POWER(2,17) END +
		 CASE COALESE(NEW.driver_12,'*ALL') WHEN '*ALL' THEN 0 ELSE POWER(2,18) END +
		 CASE COALESE(NEW.driver_11,'*ALL') WHEN '*ALL' THEN 0 ELSE POWER(2,19) END +
		 CASE COALESE(NEW.driver_10,'*ALL') WHEN '*ALL' THEN 0 ELSE POWER(2,20) END +
		 CASE COALESE(NEW.driver_09,'*ALL') WHEN '*ALL' THEN 0 ELSE POWER(2,21) END +
		 CASE COALESE(NEW.driver_08,'*ALL') WHEN '*ALL' THEN 0 ELSE POWER(2,22) END +
		 CASE COALESE(NEW.driver_07,'*ALL') WHEN '*ALL' THEN 0 ELSE POWER(2,23) END +
		 CASE COALESE(NEW.driver_06,'*ALL') WHEN '*ALL' THEN 0 ELSE POWER(2,24) END +
		 CASE COALESE(NEW.driver_05,'*ALL') WHEN '*ALL' THEN 0 ELSE POWER(2,25) END +
		 CASE COALESE(NEW.driver_04,'*ALL') WHEN '*ALL' THEN 0 ELSE POWER(2,26) END +
		 CASE COALESE(NEW.driver_03,'*ALL') WHEN '*ALL' THEN 0 ELSE POWER(2,27) END +
		 CASE COALESE(NEW.driver_02,'*ALL') WHEN '*ALL' THEN 0 ELSE POWER(2,28) END +
		 CASE COALESE(NEW.driver_01,'*ALL') WHEN '*ALL' THEN 0 ELSE POWER(2,29) END
	      INTO STRICT v_binary_weight;

	      SELECT
		 TRIM(
			CASE INSTR(COALESCE(NEW.driver_01,' '),'%') WHEN 0 THEN	CASE COALESCE(NEW.driver_01,'*ALL') WHEN '*ALL' THEN '000.' ELSE '999.' END ELSE TRIM(TO_CHAR(INSTR(COALESCE(NEW.driver_01,' '),'%')-1,'000'))||'.' END ||
			CASE INSTR(COALESCE(NEW.driver_02,' '),'%') WHEN 0 THEN	CASE COALESCE(NEW.driver_02,'*ALL') WHEN '*ALL' THEN '000.' ELSE '999.' END ELSE TRIM(TO_CHAR(INSTR(COALESCE(NEW.driver_02,' '),'%')-1,'000'))||'.' END ||
			CASE INSTR(COALESCE(NEW.driver_03,' '),'%') WHEN 0 THEN	CASE COALESCE(NEW.driver_03,'*ALL') WHEN '*ALL' THEN '000.' ELSE '999.' END ELSE TRIM(TO_CHAR(INSTR(COALESCE(NEW.driver_03,' '),'%')-1,'000'))||'.' END ||
			CASE INSTR(COALESCE(NEW.driver_04,' '),'%') WHEN 0 THEN	CASE COALESCE(NEW.driver_04,'*ALL') WHEN '*ALL' THEN '000.' ELSE '999.' END ELSE TRIM(TO_CHAR(INSTR(COALESCE(NEW.driver_04,' '),'%')-1,'000'))||'.' END ||
			CASE INSTR(COALESCE(NEW.driver_05,' '),'%') WHEN 0 THEN	CASE COALESCE(NEW.driver_05,'*ALL') WHEN '*ALL' THEN '000.' ELSE '999.' END ELSE TRIM(TO_CHAR(INSTR(COALESCE(NEW.driver_05,' '),'%')-1,'000'))||'.' END ||
			CASE INSTR(COALESCE(NEW.driver_06,' '),'%') WHEN 0 THEN	CASE COALESCE(NEW.driver_06,'*ALL') WHEN '*ALL' THEN '000.' ELSE '999.' END ELSE TRIM(TO_CHAR(INSTR(COALESCE(NEW.driver_06,' '),'%')-1,'000'))||'.' END ||
			CASE INSTR(COALESCE(NEW.driver_07,' '),'%') WHEN 0 THEN	CASE COALESCE(NEW.driver_07,'*ALL') WHEN '*ALL' THEN '000.' ELSE '999.' END ELSE TRIM(TO_CHAR(INSTR(COALESCE(NEW.driver_07,' '),'%')-1,'000'))||'.' END ||
			CASE INSTR(COALESCE(NEW.driver_08,' '),'%') WHEN 0 THEN	CASE COALESCE(NEW.driver_08,'*ALL') WHEN '*ALL' THEN '000.' ELSE '999.' END ELSE TRIM(TO_CHAR(INSTR(COALESCE(NEW.driver_08,' '),'%')-1,'000'))||'.' END ||
			CASE INSTR(COALESCE(NEW.driver_09,' '),'%') WHEN 0 THEN	CASE COALESCE(NEW.driver_09,'*ALL') WHEN '*ALL' THEN '000.' ELSE '999.' END ELSE TRIM(TO_CHAR(INSTR(COALESCE(NEW.driver_09,' '),'%')-1,'000'))||'.' END ||
			CASE INSTR(COALESCE(NEW.driver_10,' '),'%') WHEN 0 THEN	CASE COALESCE(NEW.driver_10,'*ALL') WHEN '*ALL' THEN '000.' ELSE '999.' END ELSE TRIM(TO_CHAR(INSTR(COALESCE(NEW.driver_10,' '),'%')-1,'000'))||'.' END ||
			CASE INSTR(COALESCE(NEW.driver_11,' '),'%') WHEN 0 THEN	CASE COALESCE(NEW.driver_11,'*ALL') WHEN '*ALL' THEN '000.' ELSE '999.' END ELSE TRIM(TO_CHAR(INSTR(COALESCE(NEW.driver_11,' '),'%')-1,'000'))||'.' END ||
			CASE INSTR(COALESCE(NEW.driver_12,' '),'%') WHEN 0 THEN	CASE COALESCE(NEW.driver_12,'*ALL') WHEN '*ALL' THEN '000.' ELSE '999.' END ELSE TRIM(TO_CHAR(INSTR(COALESCE(NEW.driver_12,' '),'%')-1,'000'))||'.' END ||
			CASE INSTR(COALESCE(NEW.driver_13,' '),'%') WHEN 0 THEN	CASE COALESCE(NEW.driver_13,'*ALL') WHEN '*ALL' THEN '000.' ELSE '999.' END ELSE TRIM(TO_CHAR(INSTR(COALESCE(NEW.driver_13,' '),'%')-1,'000'))||'.' END ||
			CASE INSTR(COALESCE(NEW.driver_14,' '),'%') WHEN 0 THEN	CASE COALESCE(NEW.driver_14,'*ALL') WHEN '*ALL' THEN '000.' ELSE '999.' END ELSE TRIM(TO_CHAR(INSTR(COALESCE(NEW.driver_14,' '),'%')-1,'000'))||'.' END ||
			CASE INSTR(COALESCE(NEW.driver_15,' '),'%') WHEN 0 THEN	CASE COALESCE(NEW.driver_15,'*ALL') WHEN '*ALL' THEN '000.' ELSE '999.' END ELSE TRIM(TO_CHAR(INSTR(COALESCE(NEW.driver_15,' '),'%')-1,'000'))||'.' END ||
			CASE INSTR(COALESCE(NEW.driver_16,' '),'%') WHEN 0 THEN	CASE COALESCE(NEW.driver_16,'*ALL') WHEN '*ALL' THEN '000.' ELSE '999.' END ELSE TRIM(TO_CHAR(INSTR(COALESCE(NEW.driver_16,' '),'%')-1,'000'))||'.' END ||
			CASE INSTR(COALESCE(NEW.driver_17,' '),'%') WHEN 0 THEN	CASE COALESCE(NEW.driver_17,'*ALL') WHEN '*ALL' THEN '000.' ELSE '999.' END ELSE TRIM(TO_CHAR(INSTR(COALESCE(NEW.driver_17,' '),'%')-1,'000'))||'.' END ||
			CASE INSTR(COALESCE(NEW.driver_18,' '),'%') WHEN 0 THEN	CASE COALESCE(NEW.driver_18,'*ALL') WHEN '*ALL' THEN '000.' ELSE '999.' END ELSE TRIM(TO_CHAR(INSTR(COALESCE(NEW.driver_18,' '),'%')-1,'000'))||'.' END ||
			CASE INSTR(COALESCE(NEW.driver_19,' '),'%') WHEN 0 THEN	CASE COALESCE(NEW.driver_19,'*ALL') WHEN '*ALL' THEN '000.' ELSE '999.' END ELSE TRIM(TO_CHAR(INSTR(COALESCE(NEW.driver_19,' '),'%')-1,'000'))||'.' END ||
			CASE INSTR(COALESCE(NEW.driver_20,' '),'%') WHEN 0 THEN	CASE COALESCE(NEW.driver_20,'*ALL') WHEN '*ALL' THEN '000.' ELSE '999.' END ELSE TRIM(TO_CHAR(INSTR(COALESCE(NEW.driver_20,' '),'%')-1,'000'))||'.' END ||
			CASE INSTR(COALESCE(NEW.driver_21,' '),'%') WHEN 0 THEN	CASE COALESCE(NEW.driver_21,'*ALL') WHEN '*ALL' THEN '000.' ELSE '999.' END ELSE TRIM(TO_CHAR(INSTR(COALESCE(NEW.driver_21,' '),'%')-1,'000'))||'.' END ||
			CASE INSTR(COALESCE(NEW.driver_22,' '),'%') WHEN 0 THEN	CASE COALESCE(NEW.driver_22,'*ALL') WHEN '*ALL' THEN '000.' ELSE '999.' END ELSE TRIM(TO_CHAR(INSTR(COALESCE(NEW.driver_22,' '),'%')-1,'000'))||'.' END ||
			CASE INSTR(COALESCE(NEW.driver_23,' '),'%') WHEN 0 THEN	CASE COALESCE(NEW.driver_23,'*ALL') WHEN '*ALL' THEN '000.' ELSE '999.' END ELSE TRIM(TO_CHAR(INSTR(COALESCE(NEW.driver_23,' '),'%')-1,'000'))||'.' END ||
			CASE INSTR(COALESCE(NEW.driver_24,' '),'%') WHEN 0 THEN	CASE COALESCE(NEW.driver_24,'*ALL') WHEN '*ALL' THEN '000.' ELSE '999.' END ELSE TRIM(TO_CHAR(INSTR(COALESCE(NEW.driver_24,' '),'%')-1,'000'))||'.' END ||
			CASE INSTR(COALESCE(NEW.driver_25,' '),'%') WHEN 0 THEN	CASE COALESCE(NEW.driver_25,'*ALL') WHEN '*ALL' THEN '000.' ELSE '999.' END ELSE TRIM(TO_CHAR(INSTR(COALESCE(NEW.driver_25,' '),'%')-1,'000'))||'.' END ||
			CASE INSTR(COALESCE(NEW.driver_26,' '),'%') WHEN 0 THEN	CASE COALESCE(NEW.driver_26,'*ALL') WHEN '*ALL' THEN '000.' ELSE '999.' END ELSE TRIM(TO_CHAR(INSTR(COALESCE(NEW.driver_26,' '),'%')-1,'000'))||'.' END ||
			CASE INSTR(COALESCE(NEW.driver_27,' '),'%') WHEN 0 THEN	CASE COALESCE(NEW.driver_27,'*ALL') WHEN '*ALL' THEN '000.' ELSE '999.' END ELSE TRIM(TO_CHAR(INSTR(COALESCE(NEW.driver_27,' '),'%')-1,'000'))||'.' END ||
			CASE INSTR(COALESCE(NEW.driver_28,' '),'%') WHEN 0 THEN	CASE COALESCE(NEW.driver_28,'*ALL') WHEN '*ALL' THEN '000.' ELSE '999.' END ELSE TRIM(TO_CHAR(INSTR(COALESCE(NEW.driver_28,' '),'%')-1,'000'))||'.' END ||
			CASE INSTR(COALESCE(NEW.driver_29,' '),'%') WHEN 0 THEN	CASE COALESCE(NEW.driver_29,'*ALL') WHEN '*ALL' THEN '000.' ELSE '999.' END ELSE TRIM(TO_CHAR(INSTR(COALESCE(NEW.driver_29,' '),'%')-1,'000'))||'.' END ||
			CASE INSTR(COALESCE(NEW.driver_30,' '),'%') WHEN 0 THEN	CASE COALESCE(NEW.driver_30,'*ALL') WHEN '*ALL' THEN '000'  ELSE '999'  END ELSE TRIM(TO_CHAR(INSTR(COALESCE(NEW.driver_30,' '),'%')-1,'000'))      END)
	      INTO STRICT v_significant_digits;

	      NEW.binary_weight := v_binary_weight;
	      NEW.significant_digits := v_significant_digits;

	END IF;

	NEW.update_user_id	:= CURRENT_USER;
	NEW.update_timestamp	:= CURRENT_DATE;
	
	RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER tb_allocation_matrix_b_iu
BEFORE INSERT OR UPDATE ON tb_allocation_matrix
FOR EACH ROW
EXECUTE PROCEDURE tb_allocation_matrix_b_iu();

