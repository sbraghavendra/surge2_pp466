CREATE OR REPLACE FUNCTION tb_transaction_detail_b_u() RETURNS TRIGGER AS $$
/* ************************************************************************************************/
/* Object Type/Name: Trigger/Before Update - tb_transaction_detail_b_u                            */
/* Author:           Michael B. Fuller                                                            */
/* Date:             a long time ago                                                              */
/* Description:      A transaction has been changed                                               */
/* Arguments:        None                                                                         */
/* Returns:          None                                                                         */
/* ************************************************************************************************/
/* Maintenance History:                                                                           */
/* Scan  Author       Date     Release    Description                                  Ticket #   */
/* ----- ---------- ---------- ---------- -------------------------------------------- ---------- */
/*       M. Fuller  06/02/2006 3.3.5.0    Fix processing an allocated line - Temporary 871        */
/*       M. Fuller  01/04/2007 3.3.5.1    Fix processing an allocated line - Permenant 871        */
/* MBF01 M. Fuller  02/09/2007 3.3.5.1    Correct allocated lines with no locn matrix id          */
/* MBF02 M. Fuller  08/13/2007 3.4.1.1    Add 5 Calc Tax Flags                         883        */
/* MBF03 M. Fuller  03/06/2008 3.4.2.1    Correct If/Then/Else's for "AO"              913        */
/* ************************************************************************************************/
DECLARE

-- Location Matrix Selection SQL      // MBF02
   vc_location_matrix_select       VARCHAR(1000) :=
     'SELECT tb_location_matrix.location_matrix_id, ' ||
             'tb_location_matrix.jurisdiction_id, ' ||
             'tb_location_matrix.override_taxtype_code, ' ||
             'tb_location_matrix.state_flag,' ||
             'tb_location_matrix.county_flag,' ||
             'tb_location_matrix.county_local_flag,' ||
             'tb_location_matrix.city_flag,' ||
             'tb_location_matrix.city_local_flag,' ||
             'tb_jurisdiction.state, ' ||
             'tb_jurisdiction.county, ' ||
             'tb_jurisdiction.city, ' ||
             'tb_jurisdiction.zip, ' ||
             'tb_jurisdiction.in_out ' ||
        'FROM tb_location_matrix, ' ||
             'tb_jurisdiction, ' ||
             'tb_tmp_transaction_detail ' ||
      'WHERE ( tb_tmp_transaction_detail.transaction_detail_id = (SELECT param_value FROM tb_transaction_detail_b_u_tmp WHERE param_name = ''v_transaction_detail_id'') ) AND ' ||
            '( tb_location_matrix.default_flag = ''0'' AND tb_location_matrix.binary_weight > 0 ) AND ' ||
            '( tb_location_matrix.effective_date <= tb_tmp_transaction_detail.gl_date ) AND ( tb_location_matrix.expiration_date >= tb_tmp_transaction_detail.gl_date ) AND ' ||
            '( tb_location_matrix.jurisdiction_id = tb_jurisdiction.jurisdiction_id ) ';

   vc_location_matrix_where        VARCHAR(3000) := '';
   vc_location_matrix_orderby      VARCHAR(1000) :=
      'ORDER BY tb_location_matrix.binary_weight DESC, tb_location_matrix.significant_digits DESC, tb_location_matrix.effective_date DESC';
   vc_location_matrix_stmt         VARCHAR (5000);
   
   location_matrix_cursor          REFCURSOR;
   location_matrix                 RECORD;

-- Tax Matrix Selection SQL
   vc_tax_matrix_select            VARCHAR(2000) :=	
      'SELECT tb_tax_matrix.tax_matrix_id, tb_tax_matrix.relation_sign, tb_tax_matrix.relation_amount, ' ||
             'tb_tax_matrix.then_hold_code_flag, tb_tax_matrix.else_hold_code_flag, ' ||
             'tb_tax_matrix.then_taxcode_detail_id, tb_tax_matrix.else_taxcode_detail_id, ' ||
             'tb_tax_matrix.then_cch_taxcat_code, tb_tax_matrix.then_cch_group_code, tb_tax_matrix.then_cch_item_code, ' ||
             'tb_tax_matrix.else_cch_taxcat_code, tb_tax_matrix.else_cch_group_code, tb_tax_matrix.else_cch_item_code, ' ||
	     'thentaxcddtl.taxcode_state_code, thentaxcddtl.taxcode_type_code, thentaxcddtl.taxcode_code,  ' ||
	     'thentaxcddtl.jurisdiction_id, thentaxcddtl.measure_type_code, thentaxcddtl.taxtype_code,  ' ||
	     'elsetaxcddtl.taxcode_state_code, elsetaxcddtl.taxcode_type_code, elsetaxcddtl.taxcode_code,   ' ||
	     'elsetaxcddtl.jurisdiction_id, elsetaxcddtl.measure_type_code, elsetaxcddtl.taxtype_code  ' ||
	'FROM tb_tmp_transaction_detail ' ||
	'JOIN tb_tax_matrix ON ( ' ||
		'tb_tmp_transaction_detail.transaction_detail_id = (SELECT CAST (param_value AS NUMERIC) FROM tb_transaction_detail_b_u_tmp WHERE ' ||
		'param_name = ''v_transaction_detail_id'') AND  ' ||
		'( tb_tax_matrix.effective_date <=  ' ||
		'tb_tmp_transaction_detail.gl_date ) AND ( tb_tax_matrix.expiration_date >= tb_tmp_transaction_detail.gl_date ) AND  ' ||
		'( tb_tax_matrix.default_flag = ''0'' AND tb_tax_matrix.binary_weight > 0 )	AND ' ||
		'(( tb_tax_matrix.matrix_state_code = ''*ALL'' ) OR ( tb_tax_matrix.matrix_state_code = (SELECT param_value FROM  ' ||
		'tb_transaction_detail_b_u_tmp WHERE param_name = ''v_transaction_state_code'') ))  ' ||
	')	 ' ||
	'LEFT OUTER JOIN tb_taxcode_detail thentaxcddtl ON ( tb_tax_matrix.then_taxcode_detail_id = thentaxcddtl.taxcode_detail_id ) ' ||
	'LEFT OUTER JOIN tb_taxcode_detail elsetaxcddtl ON ( tb_tax_matrix.else_taxcode_detail_id = elsetaxcddtl.taxcode_detail_id ) ';
   
   vc_tax_matrix_where             VARCHAR(6000) := '';
   vc_tax_matrix_orderby           VARCHAR(1000) :=
      'ORDER BY tb_tax_matrix.binary_weight DESC, tb_tax_matrix.significant_digits DESC, tb_tax_matrix.effective_date DESC';
   vc_tax_matrix_stmt              VARCHAR (9000);

   tax_matrix_cursor               REFCURSOR;
   tax_matrix                      RECORD;

-- Table defined variables
   v_hold_code_flag                tb_tax_matrix.then_hold_code_flag%TYPE            := NULL;
   v_less_hold_code_flag           tb_tax_matrix.then_hold_code_flag%TYPE            := NULL;
   v_less_taxcode_detail_id        tb_tax_matrix.then_taxcode_detail_id%TYPE         := NULL;
   v_less_cch_taxcat_code          tb_tax_matrix.then_cch_taxcat_code%TYPE           := NULL;
   v_less_cch_group_code           tb_tax_matrix.then_cch_group_code%TYPE            := NULL;
   v_equal_hold_code_flag          tb_tax_matrix.then_hold_code_flag%TYPE            := NULL;
   v_equal_taxcode_detail_id       tb_tax_matrix.then_taxcode_detail_id%TYPE         := NULL;
   v_equal_cch_taxcat_code         tb_tax_matrix.then_cch_taxcat_code%TYPE           := NULL;
   v_equal_cch_group_code          tb_tax_matrix.then_cch_group_code%TYPE            := NULL;
   v_greater_hold_code_flag        tb_tax_matrix.then_hold_code_flag%TYPE            := NULL;
   v_greater_taxcode_detail_id     tb_tax_matrix.then_taxcode_detail_id%TYPE         := NULL;
   v_greater_cch_taxcat_code       tb_tax_matrix.then_cch_taxcat_code%TYPE           := NULL;
   v_greater_cch_group_code        tb_tax_matrix.then_cch_group_code%TYPE            := NULL;
   v_jurisdiction_id               tb_jurisdiction.jurisdiction_id%TYPE              := NULL;
   v_less_taxcode_state_code       tb_taxcode_detail.taxcode_state_code%TYPE         := NULL;
   v_less_taxcode_type_code        tb_taxcode_detail.taxcode_type_code%TYPE          := NULL;
   v_less_taxcode_code             tb_taxcode_detail.taxcode_code%TYPE               := NULL;
   v_less_jurisdiction_id          tb_taxcode_detail.jurisdiction_id%TYPE            := NULL;
   v_less_measure_type_code        tb_taxcode_detail.measure_type_code%TYPE          := NULL;
   v_less_taxtype_code             tb_taxcode_detail.taxtype_code%TYPE               := NULL;
   v_equal_taxcode_state_code      tb_taxcode_detail.taxcode_state_code%TYPE         := NULL;
   v_equal_taxcode_type_code       tb_taxcode_detail.taxcode_type_code%TYPE          := NULL;
   v_equal_taxcode_code            tb_taxcode_detail.taxcode_code%TYPE               := NULL;
   v_equal_jurisdiction_id         tb_taxcode_detail.jurisdiction_id%TYPE            := NULL;
   v_equal_measure_type_code       tb_taxcode_detail.measure_type_code%TYPE          := NULL;
   v_equal_taxtype_code            tb_taxcode_detail.taxtype_code%TYPE               := NULL;
   v_greater_taxcode_state_code    tb_taxcode_detail.taxcode_state_code%TYPE         := NULL;
   v_greater_taxcode_type_code     tb_taxcode_detail.taxcode_type_code%TYPE          := NULL;
   v_greater_taxcode_code          tb_taxcode_detail.taxcode_code%TYPE               := NULL;
   v_greater_jurisdiction_id       tb_taxcode_detail.jurisdiction_id%TYPE            := NULL;
   v_greater_measure_type_code     tb_taxcode_detail.measure_type_code%TYPE          := NULL;
   v_greater_taxtype_code          tb_taxcode_detail.taxtype_code%TYPE               := NULL;
   v_transaction_detail_id         tb_transaction_detail.transaction_detail_id%TYPE  := NULL;
   v_transaction_state_code        tb_transaction_detail.transaction_state_code%TYPE := NULL;
   v_override_jurisdiction_id      tb_transaction_detail.jurisdiction_id%TYPE        := NULL;
   v_sysdate                       tb_transaction_detail.load_timestamp%TYPE         := CURRENT_TIMESTAMP;
   v_sysdate_plus                  tb_transaction_detail.load_timestamp%TYPE         := v_sysdate + INTERVAL '1 second';
   v_taxtype_code                  tb_taxcode.taxtype_code%TYPE                      := NULL;
   v_taxtype_used                  tb_taxcode.taxtype_code%TYPE                      := NULL;

-- Program defined variables
   vn_fetch_rows                   NUMERIC                                           := 0;
   vc_state_driver_flag            CHAR(1)                                           := '0';
-- temporary
   vn_taxable_amt                  NUMERIC                                           := 0;

-- Define Location Driver Names Cursor (tb_driver_names)
   location_driver_cursor CURSOR
   FOR
        SELECT driver.trans_dtl_column_name, driver.matrix_column_name, driver.null_driver_flag, driver.wildcard_flag,
               datadefcol.desc_column_name, datadefcol.data_type
          FROM tb_driver_names driver,
               (SELECT *
                  FROM tb_data_def_column
                 WHERE table_name = 'TB_TRANSACTION_DETAIL') datadefcol
         WHERE driver.driver_names_code = 'L' AND
               driver.trans_dtl_column_name = datadefcol.column_name
      ORDER BY driver.driver_id;
   location_driver              RECORD;

-- Define Tax Driver Names Cursor (tb_driver_names)
   tax_driver_cursor CURSOR
   FOR
        SELECT driver.trans_dtl_column_name, driver.matrix_column_name, driver.null_driver_flag, driver.wildcard_flag, driver.range_flag, to_number_flag,
               datadefcol.desc_column_name, datadefcol.data_type
          FROM tb_driver_names driver,
               (SELECT *
                  FROM tb_data_def_column
                 WHERE table_name = 'TB_TRANSACTION_DETAIL') datadefcol
         WHERE driver.driver_names_code = 'T' AND
               driver.trans_dtl_column_name = datadefcol.column_name
      ORDER BY driver.driver_id;
   tax_driver                   RECORD;

-- ****** Program starts ****** --------------------------------------------------------------------
BEGIN
   NEW.update_user_id := CURRENT_USER;
   NEW.update_timestamp := v_sysdate;

BEGIN

	EXECUTE 'CREATE GLOBAL TEMPORARY TABLE TB_TMP_TRANSACTION_DETAIL
	(
	  TRANSACTION_DETAIL_ID        NUMERIC,
	  SOURCE_TRANSACTION_ID        VARCHAR(100),
	  PROCESS_BATCH_NO             NUMERIC,
	  GL_EXTRACT_BATCH_NO          NUMERIC,
	  ARCHIVE_BATCH_NO             NUMERIC,
	  ALLOCATION_MATRIX_ID         NUMERIC,
	  ALLOCATION_SUBTRANS_ID       NUMERIC,
	  ENTERED_DATE                 TIMESTAMP(0),
	  TRANSACTION_STATUS           VARCHAR(10),
	  GL_DATE                      TIMESTAMP(0),
	  GL_COMPANY_NBR               VARCHAR(100),
	  GL_COMPANY_NAME              VARCHAR(100),
	  GL_DIVISION_NBR              VARCHAR(100),
	  GL_DIVISION_NAME             VARCHAR(100),
	  GL_CC_NBR_DEPT_ID            VARCHAR(100),
	  GL_CC_NBR_DEPT_NAME          VARCHAR(100),
	  GL_LOCAL_ACCT_NBR            VARCHAR(100),
	  GL_LOCAL_ACCT_NAME           VARCHAR(100),
	  GL_LOCAL_SUB_ACCT_NBR        VARCHAR(100),
	  GL_LOCAL_SUB_ACCT_NAME       VARCHAR(100),
	  GL_FULL_ACCT_NBR             VARCHAR(100),
	  GL_FULL_ACCT_NAME            VARCHAR(100),
	  GL_LINE_ITM_DIST_AMT         NUMERIC,
	  ORIG_GL_LINE_ITM_DIST_AMT    NUMERIC,
	  VENDOR_NBR                   VARCHAR(100),
	  VENDOR_NAME                  VARCHAR(100),
	  VENDOR_ADDRESS_LINE_1        VARCHAR(100),
	  VENDOR_ADDRESS_LINE_2        VARCHAR(100),
	  VENDOR_ADDRESS_LINE_3        VARCHAR(100),
	  VENDOR_ADDRESS_LINE_4        VARCHAR(100),
	  VENDOR_ADDRESS_CITY          VARCHAR(100),
	  VENDOR_ADDRESS_COUNTY        VARCHAR(100),
	  VENDOR_ADDRESS_STATE         VARCHAR(100),
	  VENDOR_ADDRESS_ZIP           VARCHAR(100),
	  VENDOR_ADDRESS_COUNTRY       VARCHAR(100),
	  VENDOR_TYPE                  VARCHAR(100),
	  VENDOR_TYPE_NAME             VARCHAR(100),
	  INVOICE_NBR                  VARCHAR(100),
	  INVOICE_DESC                 VARCHAR(100),
	  INVOICE_DATE                 TIMESTAMP(0),
	  INVOICE_FREIGHT_AMT          NUMERIC,
	  INVOICE_DISCOUNT_AMT         NUMERIC,
	  INVOICE_TAX_AMT              NUMERIC,
	  INVOICE_TOTAL_AMT            NUMERIC,
	  INVOICE_TAX_FLG              VARCHAR(100),
	  INVOICE_LINE_NBR             VARCHAR(100),
	  INVOICE_LINE_NAME            VARCHAR(100),
	  INVOICE_LINE_TYPE            VARCHAR(100),
	  INVOICE_LINE_TYPE_NAME       VARCHAR(100),
	  INVOICE_LINE_AMT             NUMERIC,
	  INVOICE_LINE_TAX             NUMERIC,
	  AFE_PROJECT_NBR              VARCHAR(100),
	  AFE_PROJECT_NAME             VARCHAR(100),
	  AFE_CATEGORY_NBR             VARCHAR(100),
	  AFE_CATEGORY_NAME            VARCHAR(100),
	  AFE_SUB_CAT_NBR              VARCHAR(100),
	  AFE_SUB_CAT_NAME             VARCHAR(100),
	  AFE_USE                      VARCHAR(100),
	  AFE_CONTRACT_TYPE            VARCHAR(100),
	  AFE_CONTRACT_STRUCTURE       VARCHAR(100),
	  AFE_PROPERTY_CAT             VARCHAR(100),
	  INVENTORY_NBR                VARCHAR(100),
	  INVENTORY_NAME               VARCHAR(100),
	  INVENTORY_CLASS              VARCHAR(100),
	  INVENTORY_CLASS_NAME         VARCHAR(100),
	  PO_NBR                       VARCHAR(100),
	  PO_NAME                      VARCHAR(100),
	  PO_DATE                      TIMESTAMP(0),
	  PO_LINE_NBR                  VARCHAR(100),
	  PO_LINE_NAME                 VARCHAR(100),
	  PO_LINE_TYPE                 VARCHAR(100),
	  PO_LINE_TYPE_NAME            VARCHAR(100),
	  SHIP_TO_LOCATION             VARCHAR(100),
	  SHIP_TO_LOCATION_NAME        VARCHAR(100),
	  SHIP_TO_ADDRESS_LINE_1       VARCHAR(100),
	  SHIP_TO_ADDRESS_LINE_2       VARCHAR(100),
	  SHIP_TO_ADDRESS_LINE_3       VARCHAR(100),
	  SHIP_TO_ADDRESS_LINE_4       VARCHAR(100),
	  SHIP_TO_ADDRESS_CITY         VARCHAR(100),
	  SHIP_TO_ADDRESS_COUNTY       VARCHAR(100),
	  SHIP_TO_ADDRESS_STATE        VARCHAR(100),
	  SHIP_TO_ADDRESS_ZIP          VARCHAR(100),
	  SHIP_TO_ADDRESS_COUNTRY      VARCHAR(100),
	  WO_NBR                       VARCHAR(100),
	  WO_NAME                      VARCHAR(100),
	  WO_DATE                      TIMESTAMP(0),
	  WO_TYPE                      VARCHAR(100),
	  WO_TYPE_DESC                 VARCHAR(100),
	  WO_CLASS                     VARCHAR(100),
	  WO_CLASS_DESC                VARCHAR(100),
	  WO_ENTITY                    VARCHAR(100),
	  WO_ENTITY_DESC               VARCHAR(100),
	  WO_LINE_NBR                  VARCHAR(100),
	  WO_LINE_NAME                 VARCHAR(100),
	  WO_LINE_TYPE                 VARCHAR(100),
	  WO_LINE_TYPE_DESC            VARCHAR(100),
	  WO_SHUT_DOWN_CD              VARCHAR(100),
	  WO_SHUT_DOWN_CD_DESC         VARCHAR(100),
	  VOUCHER_ID                   VARCHAR(100),
	  VOUCHER_NAME                 VARCHAR(100),
	  VOUCHER_DATE                 TIMESTAMP(0),
	  VOUCHER_LINE_NBR             VARCHAR(100),
	  VOUCHER_LINE_DESC            VARCHAR(100),
	  CHECK_NBR                    VARCHAR(100),
	  CHECK_NO                     NUMERIC,
	  CHECK_DATE                   TIMESTAMP(0),
	  CHECK_AMT                    NUMERIC,
	  CHECK_DESC                   VARCHAR(100),
	  USER_TEXT_01                 VARCHAR(100),
	  USER_TEXT_02                 VARCHAR(100),
	  USER_TEXT_03                 VARCHAR(100),
	  USER_TEXT_04                 VARCHAR(100),
	  USER_TEXT_05                 VARCHAR(100),
	  USER_TEXT_06                 VARCHAR(100),
	  USER_TEXT_07                 VARCHAR(100),
	  USER_TEXT_08                 VARCHAR(100),
	  USER_TEXT_09                 VARCHAR(100),
	  USER_TEXT_10                 VARCHAR(100),
	  USER_TEXT_11                 VARCHAR(100),
	  USER_TEXT_12                 VARCHAR(100),
	  USER_TEXT_13                 VARCHAR(100),
	  USER_TEXT_14                 VARCHAR(100),
	  USER_TEXT_15                 VARCHAR(100),
	  USER_TEXT_16                 VARCHAR(100),
	  USER_TEXT_17                 VARCHAR(100),
	  USER_TEXT_18                 VARCHAR(100),
	  USER_TEXT_19                 VARCHAR(100),
	  USER_TEXT_20                 VARCHAR(100),
	  USER_TEXT_21                 VARCHAR(100),
	  USER_TEXT_22                 VARCHAR(100),
	  USER_TEXT_23                 VARCHAR(100),
	  USER_TEXT_24                 VARCHAR(100),
	  USER_TEXT_25                 VARCHAR(100),
	  USER_TEXT_26                 VARCHAR(100),
	  USER_TEXT_27                 VARCHAR(100),
	  USER_TEXT_28                 VARCHAR(100),
	  USER_TEXT_29                 VARCHAR(100),
	  USER_TEXT_30                 VARCHAR(100),
	  USER_NUMBER_01               NUMERIC,
	  USER_NUMBER_02               NUMERIC,
	  USER_NUMBER_03               NUMERIC,
	  USER_NUMBER_04               NUMERIC,
	  USER_NUMBER_05               NUMERIC,
	  USER_NUMBER_06               NUMERIC,
	  USER_NUMBER_07               NUMERIC,
	  USER_NUMBER_08               NUMERIC,
	  USER_NUMBER_09               NUMERIC,
	  USER_NUMBER_10               NUMERIC,
	  USER_DATE_01                 TIMESTAMP(0),
	  USER_DATE_02                 TIMESTAMP(0),
	  USER_DATE_03                 TIMESTAMP(0),
	  USER_DATE_04                 TIMESTAMP(0),
	  USER_DATE_05                 TIMESTAMP(0),
	  USER_DATE_06                 TIMESTAMP(0),
	  USER_DATE_07                 TIMESTAMP(0),
	  USER_DATE_08                 TIMESTAMP(0),
	  USER_DATE_09                 TIMESTAMP(0),
	  USER_DATE_10                 TIMESTAMP(0),
	  COMMENTS                     VARCHAR(4000),
	  TB_CALC_TAX_AMT              NUMERIC,
	  STATE_USE_AMOUNT             NUMERIC,
	  STATE_USE_TIER2_AMOUNT       NUMERIC,
	  STATE_USE_TIER3_AMOUNT       NUMERIC,
	  COUNTY_USE_AMOUNT            NUMERIC,
	  COUNTY_LOCAL_USE_AMOUNT      NUMERIC,
	  CITY_USE_AMOUNT              NUMERIC,
	  CITY_LOCAL_USE_AMOUNT        NUMERIC,
	  TRANSACTION_STATE_CODE       VARCHAR(10),
	  AUTO_TRANSACTION_STATE_CODE  VARCHAR(10),
	  TRANSACTION_IND              VARCHAR(10),
	  SUSPEND_IND                  VARCHAR(10),
	  TAXCODE_DETAIL_ID            NUMERIC,
	  TAXCODE_STATE_CODE           VARCHAR(10),
	  TAXCODE_TYPE_CODE            VARCHAR(10),
	  TAXCODE_CODE                 VARCHAR(40),
	  CCH_TAXCAT_CODE              VARCHAR(2),
	  CCH_GROUP_CODE               VARCHAR(4),
	  CCH_ITEM_CODE                VARCHAR(3),
	  MANUAL_TAXCODE_IND           VARCHAR(10),
	  TAX_MATRIX_ID                NUMERIC,
	  LOCATION_MATRIX_ID           NUMERIC,
	  JURISDICTION_ID              NUMERIC,
	  JURISDICTION_TAXRATE_ID      NUMERIC,
	  MANUAL_JURISDICTION_IND      VARCHAR(10),
	  MEASURE_TYPE_CODE            VARCHAR(10),
	  STATE_USE_RATE               NUMERIC,
	  STATE_USE_TIER2_RATE         NUMERIC,
	  STATE_USE_TIER3_RATE         NUMERIC,
	  STATE_SPLIT_AMOUNT           NUMERIC,
	  STATE_TIER2_MIN_AMOUNT       NUMERIC,
	  STATE_TIER2_MAX_AMOUNT       NUMERIC,
	  STATE_MAXTAX_AMOUNT          NUMERIC,
	  COUNTY_USE_RATE              NUMERIC,
	  COUNTY_LOCAL_USE_RATE        NUMERIC,
	  COUNTY_SPLIT_AMOUNT          NUMERIC,
	  COUNTY_MAXTAX_AMOUNT         NUMERIC,
	  COUNTY_SINGLE_FLAG           CHAR(1),
	  COUNTY_DEFAULT_FLAG          CHAR(1),
	  CITY_USE_RATE                NUMERIC,
	  CITY_LOCAL_USE_RATE          NUMERIC,
	  CITY_SPLIT_AMOUNT            NUMERIC,
	  CITY_SPLIT_USE_RATE          NUMERIC,
	  CITY_SINGLE_FLAG             CHAR(1),
	  CITY_DEFAULT_FLAG            CHAR(1),
	  COMBINED_USE_RATE            NUMERIC,
	  LOAD_TIMESTAMP               TIMESTAMP(0),
	  GL_EXTRACT_UPDATER           VARCHAR(40),
	  GL_EXTRACT_TIMESTAMP         TIMESTAMP(0),
	  GL_EXTRACT_FLAG              NUMERIC,
	  GL_LOG_FLAG                  NUMERIC,
	  GL_EXTRACT_AMT               NUMERIC,
	  AUDIT_FLAG                   CHAR(1),
	  AUDIT_USER_ID                VARCHAR(40),
	  AUDIT_TIMESTAMP              TIMESTAMP(0),
	  MODIFY_USER_ID               VARCHAR(40),
	  MODIFY_TIMESTAMP             TIMESTAMP(0),
	  UPDATE_USER_ID               VARCHAR(40),
	  UPDATE_TIMESTAMP             TIMESTAMP(0)
	)
	ON COMMIT DELETE ROWS';
EXCEPTION WHEN OTHERS THEN NULL;
END;

EXECUTE 'TRUNCATE TABLE TB_TMP_TRANSACTION_DETAIL';

   -- If was processed and now processed OR
   --    was tax suspended and now processed and now manual taxcode indicator not null OR
   --    was locn suspended and now processed and now manual jurisdiction indicator not null THEN
--   IF ( UPPER(OLD.transaction_ind) = 'P' AND UPPER(NEW.transaction_ind) = 'P' ) OR
--      ( UPPER(OLD.transaction_ind) = 'S' AND UPPER(OLD.suspend_ind) = 'T' AND NEW.manual_taxcode_ind IS NOT NULL ) OR
--      ( UPPER(OLD.transaction_ind) = 'S' AND UPPER(OLD.suspend_ind) = 'L' AND NEW.manual_jurisdiction_ind IS NOT NULL ) THEN
   IF (( NOT ( UPPER(OLD.transaction_ind) = 'P' AND UPPER(NEW.transaction_ind) = 'S' )) AND
      ((( OLD.taxcode_detail_id IS NOT NULL AND OLD.taxcode_detail_id <> NEW.taxcode_detail_id ) OR
        ( OLD.taxcode_detail_id IS NOT NULL AND NEW.taxcode_detail_id IS NULL ) OR
        ( OLD.taxcode_detail_id IS NULL AND NEW.taxcode_detail_id IS NOT NULL AND NEW.manual_taxcode_ind IS NOT NULL )) OR
       (( OLD.jurisdiction_id IS NOT NULL AND OLD.jurisdiction_id <> NEW.jurisdiction_id ) OR
        ( OLD.jurisdiction_id IS NOT NULL AND NEW.jurisdiction_id IS NULL ) OR
        ( OLD.jurisdiction_id IS NULL AND NEW.jurisdiction_id IS NOT NULL AND NEW.manual_jurisdiction_ind IS NOT NULL AND NEW.manual_jurisdiction_ind <> 'AO' )) OR
       (( (OLD.jurisdiction_taxrate_id IS NOT NULL AND OLD.jurisdiction_taxrate_id <> 0) AND OLD.jurisdiction_taxrate_id <> NEW.jurisdiction_taxrate_id ) OR
        ( (OLD.jurisdiction_taxrate_id IS NOT NULL AND OLD.jurisdiction_taxrate_id <> 0) AND (NEW.jurisdiction_taxrate_id IS NULL OR NEW.jurisdiction_taxrate_id = 0) ))))
   AND NOT (( UPPER(OLD.transaction_ind) = 'S' AND UPPER(NEW.transaction_ind) = 'S' ) AND
       (( OLD.taxcode_detail_id IS NOT NULL AND NEW.taxcode_detail_id IS NULL ) OR
        ( OLD.jurisdiction_id IS NOT NULL AND NEW.jurisdiction_id IS NULL ))) THEN
      -- Initialize Transaction Indicator and Suspended Indicator
      NEW.transaction_ind := NULL;
      NEW.suspend_ind := NULL;
      -- Insert New Tax Matrix columns into temporary table -------------------------------------------
      -- 3411 - add taxable amount & tax type used.
      INSERT INTO tb_tmp_transaction_detail (
         transaction_detail_id,
         source_transaction_id,
         process_batch_no,
         gl_extract_batch_no,
         archive_batch_no,
         allocation_matrix_id,
         allocation_subtrans_id,
         entered_date,
         transaction_status,
         gl_date,
         gl_company_nbr,
         gl_company_name,
         gl_division_nbr,
         gl_division_name,
         gl_cc_nbr_dept_id,
         gl_cc_nbr_dept_name,
         gl_local_acct_nbr,
         gl_local_acct_name,
         gl_local_sub_acct_nbr,
         gl_local_sub_acct_name,
         gl_full_acct_nbr,
         gl_full_acct_name,
         gl_line_itm_dist_amt,
         orig_gl_line_itm_dist_amt,
         vendor_nbr,
         vendor_name,
         vendor_address_line_1,
         vendor_address_line_2,
         vendor_address_line_3,
         vendor_address_line_4,
         vendor_address_city,
         vendor_address_county,
         vendor_address_state,
         vendor_address_zip,
         vendor_address_country,
         vendor_type,
         vendor_type_name,
         invoice_nbr,
         invoice_desc,
         invoice_date,
         invoice_freight_amt,
         invoice_discount_amt,
         invoice_tax_amt,
         invoice_total_amt,
         invoice_tax_flg,
         invoice_line_nbr,
         invoice_line_name,
         invoice_line_type,
         invoice_line_type_name,
         invoice_line_amt,
         invoice_line_tax,
         afe_project_nbr,
         afe_project_name,
         afe_category_nbr,
         afe_category_name,
         afe_sub_cat_nbr,
         afe_sub_cat_name,
         afe_use,
         afe_contract_type,
         afe_contract_structure,
         afe_property_cat,
         inventory_nbr,
         inventory_name,
         inventory_class,
         inventory_class_name,
         po_nbr,
         po_name,
         po_date,
         po_line_nbr,
         po_line_name,
         po_line_type,
         po_line_type_name,
         ship_to_location,
         ship_to_location_name,
         ship_to_address_line_1,
         ship_to_address_line_2,
         ship_to_address_line_3,
         ship_to_address_line_4,
         ship_to_address_city,
         ship_to_address_county,
         ship_to_address_state,
         ship_to_address_zip,
         ship_to_address_country,
         wo_nbr,
         wo_name,
         wo_date,
         wo_type,
         wo_type_desc,
         wo_class,
         wo_class_desc,
         wo_entity,
         wo_entity_desc,
         wo_line_nbr,
         wo_line_name,
         wo_line_type,
         wo_line_type_desc,
         wo_shut_down_cd,
         wo_shut_down_cd_desc,
         voucher_id,
         voucher_name,
         voucher_date,
         voucher_line_nbr,
         voucher_line_desc,
         check_nbr,
         check_no,
         check_date,
         check_amt,
         check_desc,
         user_text_01,
         user_text_02,
         user_text_03,
         user_text_04,
         user_text_05,
         user_text_06,
         user_text_07,
         user_text_08,
         user_text_09,
         user_text_10,
         user_text_11,
         user_text_12,
         user_text_13,
         user_text_14,
         user_text_15,
         user_text_16,
         user_text_17,
         user_text_18,
         user_text_19,
         user_text_20,
         user_text_21,
         user_text_22,
         user_text_23,
         user_text_24,
         user_text_25,
         user_text_26,
         user_text_27,
         user_text_28,
         user_text_29,
         user_text_30,
         user_number_01,
         user_number_02,
         user_number_03,
         user_number_04,
         user_number_05,
         user_number_06,
         user_number_07,
         user_number_08,
         user_number_09,
         user_number_10,
         user_date_01,
         user_date_02,
         user_date_03,
         user_date_04,
         user_date_05,
         user_date_06,
         user_date_07,
         user_date_08,
         user_date_09,
         user_date_10,
         comments,
         tb_calc_tax_amt,
         state_use_amount,
         state_use_tier2_amount,
         state_use_tier3_amount,
         county_use_amount,
         county_local_use_amount,
         city_use_amount,
         city_local_use_amount,
         transaction_state_code,
         transaction_ind,
         suspend_ind,
         taxcode_detail_id,
         taxcode_state_code,
         taxcode_type_code,
         taxcode_code,
         cch_taxcat_code,
         cch_group_code,
         cch_item_code,
         manual_taxcode_ind,
         tax_matrix_id,
         location_matrix_id,
         jurisdiction_id,
         jurisdiction_taxrate_id,
         manual_jurisdiction_ind,
         measure_type_code,
         state_use_rate,
         state_use_tier2_rate,
         state_use_tier3_rate,
         state_split_amount,
         state_tier2_min_amount,
         state_tier2_max_amount,
         state_maxtax_amount,
         county_use_rate,
         county_local_use_rate,
         county_split_amount,
         county_maxtax_amount,
         county_single_flag,
         county_default_flag,
         city_use_rate,
         city_local_use_rate,
         city_split_amount,
         city_split_use_rate,
         city_single_flag,
         city_default_flag,
         combined_use_rate,
         load_timestamp,
         gl_extract_updater,
         gl_extract_timestamp,
         gl_extract_flag,
         gl_log_flag,
         gl_extract_amt,
         audit_flag,
         audit_user_id,
         audit_timestamp,
         modify_user_id,
         modify_timestamp,
         update_user_id,
         update_timestamp )
      VALUES (
         NEW.transaction_detail_id,
         NEW.source_transaction_id,
         NEW.process_batch_no,
         NEW.gl_extract_batch_no,
         NEW.archive_batch_no,
         NEW.allocation_matrix_id,
         NEW.allocation_subtrans_id,
         NEW.entered_date,
         NEW.transaction_status,
         NEW.gl_date,
         NEW.gl_company_nbr,
         NEW.gl_company_name,
         NEW.gl_division_nbr,
         NEW.gl_division_name,
         NEW.gl_cc_nbr_dept_id,
         NEW.gl_cc_nbr_dept_name,
         NEW.gl_local_acct_nbr,
         NEW.gl_local_acct_name,
         NEW.gl_local_sub_acct_nbr,
         NEW.gl_local_sub_acct_name,
         NEW.gl_full_acct_nbr,
         NEW.gl_full_acct_name,
         NEW.gl_line_itm_dist_amt,
         NEW.orig_gl_line_itm_dist_amt,
         NEW.vendor_nbr,
         NEW.vendor_name,
         NEW.vendor_address_line_1,
         NEW.vendor_address_line_2,
         NEW.vendor_address_line_3,
         NEW.vendor_address_line_4,
         NEW.vendor_address_city,
         NEW.vendor_address_county,
         NEW.vendor_address_state,
         NEW.vendor_address_zip,
         NEW.vendor_address_country,
         NEW.vendor_type,
         NEW.vendor_type_name,
         NEW.invoice_nbr,
         NEW.invoice_desc,
         NEW.invoice_date,
         NEW.invoice_freight_amt,
         NEW.invoice_discount_amt,
         NEW.invoice_tax_amt,
         NEW.invoice_total_amt,
         NEW.invoice_tax_flg,
         NEW.invoice_line_nbr,
         NEW.invoice_line_name,
         NEW.invoice_line_type,
         NEW.invoice_line_type_name,
         NEW.invoice_line_amt,
         NEW.invoice_line_tax,
         NEW.afe_project_nbr,
         NEW.afe_project_name,
         NEW.afe_category_nbr,
         NEW.afe_category_name,
         NEW.afe_sub_cat_nbr,
         NEW.afe_sub_cat_name,
         NEW.afe_use,
         NEW.afe_contract_type,
         NEW.afe_contract_structure,
         NEW.afe_property_cat,
         NEW.inventory_nbr,
         NEW.inventory_name,
         NEW.inventory_class,
         NEW.inventory_class_name,
         NEW.po_nbr,
         NEW.po_name,
         NEW.po_date,
         NEW.po_line_nbr,
         NEW.po_line_name,
         NEW.po_line_type,
         NEW.po_line_type_name,
         NEW.ship_to_location,
         NEW.ship_to_location_name,
         NEW.ship_to_address_line_1,
         NEW.ship_to_address_line_2,
         NEW.ship_to_address_line_3,
         NEW.ship_to_address_line_4,
         NEW.ship_to_address_city,
         NEW.ship_to_address_county,
         NEW.ship_to_address_state,
         NEW.ship_to_address_zip,
         NEW.ship_to_address_country,
         NEW.wo_nbr,
         NEW.wo_name,
         NEW.wo_date,
         NEW.wo_type,
         NEW.wo_type_desc,
         NEW.wo_class,
         NEW.wo_class_desc,
         NEW.wo_entity,
         NEW.wo_entity_desc,
         NEW.wo_line_nbr,
         NEW.wo_line_name,
         NEW.wo_line_type,
         NEW.wo_line_type_desc,
         NEW.wo_shut_down_cd,
         NEW.wo_shut_down_cd_desc,
         NEW.voucher_id,
         NEW.voucher_name,
         NEW.voucher_date,
         NEW.voucher_line_nbr,
         NEW.voucher_line_desc,
         NEW.check_nbr,
         NEW.check_no,
         NEW.check_date,
         NEW.check_amt,
         NEW.check_desc,
         NEW.user_text_01,
         NEW.user_text_02,
         NEW.user_text_03,
         NEW.user_text_04,
         NEW.user_text_05,
         NEW.user_text_06,
         NEW.user_text_07,
         NEW.user_text_08,
         NEW.user_text_09,
         NEW.user_text_10,
         NEW.user_text_11,
         NEW.user_text_12,
         NEW.user_text_13,
         NEW.user_text_14,
         NEW.user_text_15,
         NEW.user_text_16,
         NEW.user_text_17,
         NEW.user_text_18,
         NEW.user_text_19,
         NEW.user_text_20,
         NEW.user_text_21,
         NEW.user_text_22,
         NEW.user_text_23,
         NEW.user_text_24,
         NEW.user_text_25,
         NEW.user_text_26,
         NEW.user_text_27,
         NEW.user_text_28,
         NEW.user_text_29,
         NEW.user_text_30,
         NEW.user_number_01,
         NEW.user_number_02,
         NEW.user_number_03,
         NEW.user_number_04,
         NEW.user_number_05,
         NEW.user_number_06,
         NEW.user_number_07,
         NEW.user_number_08,
         NEW.user_number_09,
         NEW.user_number_10,
         NEW.user_date_01,
         NEW.user_date_02,
         NEW.user_date_03,
         NEW.user_date_04,
         NEW.user_date_05,
         NEW.user_date_06,
         NEW.user_date_07,
         NEW.user_date_08,
         NEW.user_date_09,
         NEW.user_date_10,
         NEW.comments,
         NEW.tb_calc_tax_amt,
         NEW.state_use_amount,
         NEW.state_use_tier2_amount,
         NEW.state_use_tier3_amount,
         NEW.county_use_amount,
         NEW.county_local_use_amount,
         NEW.city_use_amount,
         NEW.city_local_use_amount,
         NEW.transaction_state_code,
         NEW.transaction_ind,
         NEW.suspend_ind,
         NEW.taxcode_detail_id,
         NEW.taxcode_state_code,
         NEW.taxcode_type_code,
         NEW.taxcode_code,
         NEW.cch_taxcat_code,
         NEW.cch_group_code,
         NEW.cch_item_code,
         NEW.manual_taxcode_ind,
         NEW.tax_matrix_id,
         NEW.location_matrix_id,
         NEW.jurisdiction_id,
         NEW.jurisdiction_taxrate_id,
         NEW.manual_jurisdiction_ind,
         NEW.measure_type_code,
         NEW.state_use_rate,
         NEW.state_use_tier2_rate,
         NEW.state_use_tier3_rate,
         NEW.state_split_amount,
         NEW.state_tier2_min_amount,
         NEW.state_tier2_max_amount,
         NEW.state_maxtax_amount,
         NEW.county_use_rate,
         NEW.county_local_use_rate,
         NEW.county_split_amount,
         NEW.county_maxtax_amount,
         NEW.county_single_flag,
         NEW.county_default_flag,
         NEW.city_use_rate,
         NEW.city_local_use_rate,
         NEW.city_split_amount,
         NEW.city_split_use_rate,
         NEW.city_single_flag,
         NEW.city_default_flag,
         NEW.combined_use_rate,
         NEW.load_timestamp,
         NEW.gl_extract_updater,
         NEW.gl_extract_timestamp,
         NEW.gl_extract_flag,
         NEW.gl_log_flag,
         NEW.gl_extract_amt,
         NEW.audit_flag,
         NEW.audit_user_id,
         NEW.audit_timestamp,
         NEW.modify_user_id,
         NEW.modify_timestamp,
         NEW.update_user_id,
         NEW.update_timestamp );

      -- If the new TaxCode Type is "T"axable then zero Tax Amounts
      IF UPPER(NEW.taxcode_type_code) = 'T' OR UPPER(OLD.taxcode_type_code) = 'T' THEN
         NEW.tb_calc_tax_amt := 0;
-- future?? --         NEW.taxable_amt := 0;
         NEW.state_use_amount := 0;
         NEW.state_use_tier2_amount := 0;
         NEW.state_use_tier3_amount := 0;
         NEW.county_use_amount := 0;
         NEW.county_local_use_amount := 0;
         NEW.city_use_amount := 0;
         NEW.city_local_use_amount := 0;
         NEW.state_use_rate := 0;
         NEW.state_use_tier2_rate := 0;
         NEW.state_use_tier3_rate := 0;
         NEW.state_split_amount := 0;
         NEW.state_tier2_min_amount := 0;
         NEW.state_tier2_max_amount := 0;
         NEW.state_maxtax_amount := 0;
         NEW.county_use_rate := 0;
         NEW.county_local_use_rate := 0;
         NEW.county_split_amount := 0;
         NEW.county_maxtax_amount := 0;
         NEW.county_single_flag := '0';
         NEW.county_default_flag := '0';
         NEW.city_use_rate := 0;
         NEW.city_local_use_rate := 0;
         NEW.city_split_amount := 0;
         NEW.city_split_use_rate := 0;
         NEW.city_single_flag := '0';
         NEW.city_default_flag := '0';
         NEW.combined_use_rate := 0;
         -- Replace original distribution amount if populated -- 3351
         IF NEW.gl_extract_amt IS NOT NULL AND NEW.gl_extract_amt <> 0 THEN
            NEW.gl_line_itm_dist_amt := NEW.gl_extract_amt;
         END IF;
         NEW.gl_extract_amt := 0;
      END IF;

      -- If the new TaxCode Detail ID is null then search tax matrix
      IF NEW.taxcode_detail_id IS NULL THEN
         v_transaction_detail_id := NEW.transaction_detail_id;
         v_transaction_state_code := NEW.transaction_state_code;
         -- ***** Create Dynamic WHERE Clause for Matrix SQLs ***** --
         -- Does Transaction Record have a valid State?
         IF v_transaction_state_code IS NULL THEN
            vn_fetch_rows := 0;
         ELSE
            SELECT count(*)
              INTO STRICT vn_fetch_rows
              FROM tb_taxcode_state
             WHERE taxcode_state_code = v_transaction_state_code;
         END IF;
         IF vn_fetch_rows = 0 THEN
            -- Get State from new Jurisdiction
            v_transaction_state_code := NULL;
            BEGIN
               SELECT state
                 INTO STRICT v_transaction_state_code
                 FROM tb_jurisdiction
                WHERE jurisdiction_id = NEW.jurisdiction_id;
            EXCEPTION
               WHEN OTHERS THEN
                  v_transaction_state_code := NULL;
            END;
            -- Jurisdiction Line Found
            IF v_transaction_state_code IS NOT NULL THEN
               IF NEW.transaction_state_code IS NULL THEN
                  NEW.auto_transaction_state_code := '*NULL';
               ELSE
                  NEW.auto_transaction_state_code := NEW.transaction_state_code;
               END IF;
               NEW.transaction_state_code := v_transaction_state_code;
            ELSE
               NEW.transaction_ind := 'S';
               NEW.suspend_ind := 'L';
            END IF;
         END IF;

         -- Continue if not Suspended
         IF NEW.transaction_ind IS NULL OR NEW.transaction_ind <> 'S' THEN

   -- *******************************************************************************************************************************************
   -- NEW BATCH PROCESS CODE --------------------------------------------------------------------------------------------------------------------
   -- ***** Create Dynamic WHERE Clause for Transaction Detail SQL ***** --
   -- Tax Matrix Drivers
   OPEN tax_driver_cursor;
   LOOP
      FETCH tax_driver_cursor INTO tax_driver;
      
      IF (NOT FOUND) THEN 
	EXIT;
      END IF;
      
      IF tax_driver.range_flag = '1' THEN
         IF tax_driver.to_number_flag = '1' THEN
            IF tax_driver.null_driver_flag = '1' THEN
               -- Range and ToNumber and NULL
               IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                  UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                  vc_tax_matrix_where := vc_tax_matrix_where || 'AND ( ' ||
                  '( tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU = ''*NA'' AND ' ||
                     '( tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NULL AND ( tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*NULL'' )) OR ' ||
                     '( tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND ( tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR CASE isnumber(tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ') WHEN 1 THEN to_char(to_number(tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ' ,''00000000000000000000.00000000000000000000''),''00000000000000000000.00000000000000000000'') ELSE UPPER(tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ') END = CASE isnumber(tb_tax_matrix.' || tax_driver.matrix_column_name || ') WHEN 1 THEN to_char(to_number(tb_tax_matrix.' || tax_driver.matrix_column_name || ' ,''00000000000000000000.00000000000000000000''),''00000000000000000000.00000000000000000000'') ELSE UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') END )) OR ' ||
                  '( tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU <> ''*NA'' AND ' ||
                     '( tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND (CASE isnumber(tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ') WHEN 1 THEN to_char(to_number(tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ' ,''00000000000000000000.00000000000000000000''),''00000000000000000000.00000000000000000000'') ELSE UPPER(tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ') END >= CASE isnumber(tb_tax_matrix.' || tax_driver.matrix_column_name || ') WHEN 1 THEN to_char(to_number(tb_tax_matrix.' || tax_driver.matrix_column_name || ' ,''00000000000000000000.00000000000000000000''),''00000000000000000000.00000000000000000000'') ELSE UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') END AND ' ||
                                                                                            'CASE isnumber(tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ') WHEN 1 THEN to_char(to_number(tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ' ,''00000000000000000000.00000000000000000000''),''00000000000000000000.00000000000000000000'') ELSE UPPER(tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ') END <= CASE isnumber(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU ) WHEN 1 THEN to_char(to_number(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU ,''00000000000000000000.00000000000000000000''),''00000000000000000000.00000000000000000000'') ELSE UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU) END )))) ';
               END IF;
            ELSE
               -- Range and ToNumber and NOT NULL
               IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                  UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                  vc_tax_matrix_where := vc_tax_matrix_where || 'AND ( ' ||
                  '( tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU = ''*NA'' AND (tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR CASE isnumber(tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ') WHEN 1 THEN to_char(to_number(tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ' ,''00000000000000000000.00000000000000000000''),''00000000000000000000.00000000000000000000'') ELSE UPPER(tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ') END = CASE isnumber(tb_tax_matrix.' || tax_driver.matrix_column_name || ') WHEN 1 THEN to_char(to_number(tb_tax_matrix.' || tax_driver.matrix_column_name || ' ,''00000000000000000000.00000000000000000000''),''00000000000000000000.00000000000000000000'') ELSE UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') END)) OR ' ||
                  '( tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU <> ''*NA'' AND (CASE isnumber(tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ') WHEN 1 THEN to_char(to_number(tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ' ,''00000000000000000000.00000000000000000000''),''00000000000000000000.00000000000000000000'') ELSE UPPER(tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ') END >= CASE isnumber(tb_tax_matrix.' || tax_driver.matrix_column_name || ') WHEN 1 THEN to_char(to_number(tb_tax_matrix.' || tax_driver.matrix_column_name || ' ,''00000000000000000000.00000000000000000000''),''00000000000000000000.00000000000000000000'') ELSE UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') END AND ' ||
                                                                                        'CASE isnumber(tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ') WHEN 1 THEN to_char(to_number(tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ' ,''00000000000000000000.00000000000000000000''),''00000000000000000000.00000000000000000000'') ELSE UPPER(tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ') END <= CASE isnumber(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU ) WHEN 1 THEN to_char(to_number(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU ,''00000000000000000000.00000000000000000000''),''00000000000000000000.00000000000000000000'') ELSE UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU) END )) ';
               END IF;
            END IF;
         ELSE
            IF tax_driver.null_driver_flag = '1' THEN
               IF tax_driver.wildcard_flag = '1' THEN
                  -- Range and NOT ToNumber and NULL and Wildcard
                  IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                     UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                     vc_tax_matrix_where := vc_tax_matrix_where || 'AND ( ' ||
                     '( tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU = ''*NA'' AND ' ||
                        '( tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NULL AND (tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*NULL'' )) OR ' ||
                        '( tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND (tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR UPPER(tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ') LIKE UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ')))) OR ' ||
                     '( tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU <> ''*NA'' AND (tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND (UPPER(tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ') >= UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') AND UPPER(tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ') <= UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU) ))) ';
                  END IF;
               ELSE
                  -- Range and NOT ToNumber and NULL and NOT Wildcard
                  IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                     UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                     vc_tax_matrix_where := vc_tax_matrix_where || 'AND ( ' ||
                     '( tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU = ''*NA'' AND ' ||
                        '( tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NULL AND (tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*NULL'' )) OR ' ||
                        '( tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND (tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR UPPER(tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ') = UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ')))) OR ' ||
                     '( tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU <> ''*NA'' AND (tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND (UPPER(tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ') >= UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') AND UPPER(tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ') <= UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU) ))) ';
                  END IF;
               END IF;
            ELSE
               IF tax_driver.wildcard_flag = '1' THEN
                  -- Range and NOT ToNumber and NOT NULL and Wildcard
                  IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                     UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                     vc_tax_matrix_where := vc_tax_matrix_where || 'AND ( ' ||
                     '( tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU = ''*NA'' AND ' ||
                        '(tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR UPPER(tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ') LIKE UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '))) OR ' ||
                     '( tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU <> ''*NA'' AND (UPPER(tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ') >= UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') AND UPPER(tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ') <= UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU) )) ';
                  END IF;
               ELSE
                  -- Range and NOT ToNumber and NOT NULL and NOT Wildcard
                  IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                     UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                     vc_tax_matrix_where := vc_tax_matrix_where || 'AND ( ' ||
                     '( tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU = ''*NA'' AND ' ||
                        '(tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR UPPER(tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ') = UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '))) OR ' ||
                     '( tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU <> ''*NA'' AND (UPPER(tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ') >= UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') AND UPPER(tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ') <= UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU) )) ';
                  END IF;
               END IF;
            END IF;
         END IF;
      ELSE
         IF tax_driver.null_driver_flag = '1' THEN
            IF tax_driver.wildcard_flag = '1' THEN
               -- NOT Range and NULL and Wildcard
               IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                  UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                     vc_tax_matrix_where := vc_tax_matrix_where || 'AND ( ' ||
                     '( tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NULL AND (tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*NULL'' )) OR ' ||
                     '( tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND (tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR UPPER(tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ') LIKE UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ')) ) ';
               END IF;
            ELSE
               -- NOT Range and NULL and NOT Wildcard
               IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                  UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                     vc_tax_matrix_where := vc_tax_matrix_where || 'AND ( ' ||
                     '( tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NULL AND (tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*NULL'' )) OR ' ||
                     '( tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND (tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR UPPER(tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ') = UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ')) ) ';
               END IF;
            END IF;
         ELSE
            IF tax_driver.wildcard_flag = '1' THEN
               -- NOT Range and NOT NULL and Wildcard
               IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                  UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                     vc_tax_matrix_where := vc_tax_matrix_where || 'AND ( ' ||
                     '( tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR UPPER(tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ') LIKE UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') ) ';
               END IF;
            ELSE
               -- NOT Range and NOT NULL and NOT Wildcard
               IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                  UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                  IF tax_driver.trans_dtl_column_name = 'TRANSACTION_STATE_CODE' THEN
                     IF vc_state_driver_flag <> '1' THEN
                        vc_state_driver_flag := '1';
                     END IF;
                     vc_tax_matrix_where := vc_tax_matrix_where || 'AND ( ' ||			
			'( tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR (SELECT UPPER(param_value) FROM tb_transaction_detail_b_u_tmp WHERE param_name = ''v_transaction_state_code'') = UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ')) ';
                  ELSE
                     vc_tax_matrix_where := vc_tax_matrix_where || 'AND ( ' ||
                        '( tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR UPPER(tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ') = UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') ) ';
                  END IF;
               END IF;
            END IF;
         END IF;
      END IF;
      vc_tax_matrix_where := vc_tax_matrix_where || ') ';
   END LOOP;
   CLOSE tax_driver_cursor;
   -- NEW BATCH PROCESS CODE --------------------------------------------------------------------------------------------------------------------
   -- *******************************************************************************************************************************************

            -- If no drivers found raise error, else create location matrix sql statement
            IF vc_tax_matrix_where IS NULL THEN
               NULL;
            ELSE
               vc_tax_matrix_stmt := vc_tax_matrix_select || vc_tax_matrix_where || vc_tax_matrix_orderby;
            END IF;

            -- Search Tax Matrix for matches
            BEGIN
	       -- ****** CREATE Temporary Table to hold "Bind" Values ****** -----------------------------------------
	       BEGIN
		    EXECUTE 'CREATE TEMPORARY TABLE tb_transaction_detail_b_u_tmp (param_name varchar(30), param_value varchar(30))';
	       EXCEPTION 
		     -- Table may already exist if using Connection/Session Pooling, Ignore
		     WHEN OTHERS THEN NULL;
	       END;

	       -- Prep TMP table with values
	       DELETE FROM tb_transaction_detail_b_u_tmp;
	       INSERT INTO tb_transaction_detail_b_u_tmp(param_name, param_value) VALUES ('v_transaction_detail_id',  v_transaction_detail_id);
	       INSERT INTO tb_transaction_detail_b_u_tmp(param_name, param_value) VALUES ('v_transaction_state_code', v_transaction_state_code);

	       IF vc_state_driver_flag = '1' THEN
                   OPEN tax_matrix_cursor
                    FOR EXECUTE vc_tax_matrix_stmt;

                  FETCH tax_matrix_cursor
                   INTO tax_matrix;
               ELSE
                   OPEN tax_matrix_cursor
                    FOR EXECUTE vc_tax_matrix_stmt;

                  FETCH tax_matrix_cursor
                   INTO tax_matrix;
               END IF;
               -- Rows found?
               IF (FOUND) THEN
                  vn_fetch_rows := 1;
               ELSE
                  vn_fetch_rows := 0;
               END IF;
               CLOSE tax_matrix_cursor;
            EXCEPTION
               WHEN OTHERS THEN
                  vn_fetch_rows := 0;
            END;

            -- Tax Matrix Line Found
            IF vn_fetch_rows > 0 THEN
               NEW.tax_matrix_id := tax_matrix.tax_matrix_id;
               -- Determine "Then" or "Else" Results
               IF tax_matrix.relation_sign = 'na' THEN
                  tax_matrix.relation_amount := 0;
                  v_less_hold_code_flag := tax_matrix.then_hold_code_flag;
                  v_less_taxcode_detail_id := tax_matrix.then_taxcode_detail_id;
                  v_less_taxcode_state_code := tax_matrix.then_taxcode_state_code;
                  v_less_taxcode_type_code := tax_matrix.then_taxcode_type_code;
                  v_less_taxcode_code := tax_matrix.then_taxcode_code;
                  v_less_jurisdiction_id := tax_matrix.then_jurisdiction_id;
                  v_less_measure_type_code := tax_matrix.then_measure_type_code;
                  v_less_taxtype_code := tax_matrix.then_taxtype_code;
                  v_less_cch_taxcat_code := tax_matrix.then_cch_taxcat_code;
                  v_less_cch_group_code := tax_matrix.then_cch_group_code;
                  -- v_less_cch_item_code := tax_matrix.then_cch_item_code;
                  v_equal_hold_code_flag := tax_matrix.then_hold_code_flag;
                  v_equal_taxcode_detail_id := tax_matrix.then_taxcode_detail_id;
                  v_equal_taxcode_state_code := tax_matrix.then_taxcode_state_code;
                  v_equal_taxcode_type_code := tax_matrix.then_taxcode_type_code;
                  v_equal_taxcode_code := tax_matrix.then_taxcode_code;
                  v_equal_jurisdiction_id := tax_matrix.then_jurisdiction_id;
                  v_equal_measure_type_code := tax_matrix.then_measure_type_code;
                  v_equal_taxtype_code := tax_matrix.then_taxtype_code;
                  v_equal_cch_taxcat_code := tax_matrix.then_cch_taxcat_code;
                  v_equal_cch_group_code := tax_matrix.then_cch_group_code;
                  -- v_equal_cch_item_code := tax_matrix.then_cch_item_code;
                  v_greater_hold_code_flag := tax_matrix.then_hold_code_flag;
                  v_greater_taxcode_detail_id := tax_matrix.then_taxcode_detail_id;
                  v_greater_taxcode_state_code := tax_matrix.then_taxcode_state_code;
                  v_greater_taxcode_type_code := tax_matrix.then_taxcode_type_code;
                  v_greater_taxcode_code := tax_matrix.then_taxcode_code;
                  v_greater_jurisdiction_id := tax_matrix.then_jurisdiction_id;
                  v_greater_measure_type_code := tax_matrix.then_measure_type_code;
                  v_greater_taxtype_code := tax_matrix.then_taxtype_code;
                  v_greater_cch_taxcat_code := tax_matrix.then_cch_taxcat_code;
                  v_greater_cch_group_code := tax_matrix.then_cch_group_code;
                  -- v_greater_cch_item_code := tax_matrix.then_cch_item_code;
               ELSIF tax_matrix.relation_sign = '=' THEN
                  v_less_hold_code_flag := tax_matrix.else_hold_code_flag;
                  v_less_taxcode_detail_id := tax_matrix.else_taxcode_detail_id;
                  v_less_taxcode_state_code := tax_matrix.else_taxcode_state_code;
                  v_less_taxcode_type_code := tax_matrix.else_taxcode_type_code;
                  v_less_taxcode_code := tax_matrix.else_taxcode_code;
                  v_less_jurisdiction_id := tax_matrix.else_jurisdiction_id;
                  v_less_measure_type_code := tax_matrix.else_measure_type_code;
                  v_less_taxtype_code := tax_matrix.else_taxtype_code;
                  v_less_cch_taxcat_code := tax_matrix.else_cch_taxcat_code;
                  v_less_cch_group_code := tax_matrix.else_cch_group_code;
                  -- v_less_cch_item_code := tax_matrix.else_cch_item_code;
                  v_equal_hold_code_flag := tax_matrix.then_hold_code_flag;
                  v_equal_taxcode_detail_id := tax_matrix.then_taxcode_detail_id;
                  v_equal_taxcode_state_code := tax_matrix.then_taxcode_state_code;
                  v_equal_taxcode_type_code := tax_matrix.then_taxcode_type_code;
                  v_equal_taxcode_code := tax_matrix.then_taxcode_code;
                  v_equal_jurisdiction_id := tax_matrix.then_jurisdiction_id;
                  v_equal_measure_type_code := tax_matrix.then_measure_type_code;
                  v_equal_taxtype_code := tax_matrix.then_taxtype_code;
                  v_equal_cch_taxcat_code := tax_matrix.then_cch_taxcat_code;
                  v_equal_cch_group_code := tax_matrix.then_cch_group_code;
                  -- v_equal_cch_item_code := tax_matrix.then_cch_item_code;
                  v_greater_hold_code_flag := tax_matrix.else_hold_code_flag;
                  v_greater_taxcode_detail_id := tax_matrix.else_taxcode_detail_id;
                  v_greater_taxcode_state_code := tax_matrix.else_taxcode_state_code;
                  v_greater_taxcode_type_code := tax_matrix.else_taxcode_type_code;
                  v_greater_taxcode_code := tax_matrix.else_taxcode_code;
                  v_greater_jurisdiction_id := tax_matrix.else_jurisdiction_id;
                  v_greater_measure_type_code := tax_matrix.else_measure_type_code;
                  v_greater_taxtype_code := tax_matrix.else_taxtype_code;
                  v_greater_cch_taxcat_code := tax_matrix.else_cch_taxcat_code;
                  v_greater_cch_group_code := tax_matrix.else_cch_group_code;
                  -- v_greater_cch_item_code := tax_matrix.else_cch_item_code;
               ELSIF tax_matrix.relation_sign = '<' THEN
                  v_less_hold_code_flag := tax_matrix.then_hold_code_flag;
                  v_less_taxcode_detail_id := tax_matrix.then_taxcode_detail_id;
                  v_less_taxcode_state_code := tax_matrix.then_taxcode_state_code;
                  v_less_taxcode_type_code := tax_matrix.then_taxcode_type_code;
                  v_less_taxcode_code := tax_matrix.then_taxcode_code;
                  v_less_jurisdiction_id := tax_matrix.then_jurisdiction_id;
                  v_less_measure_type_code := tax_matrix.then_measure_type_code;
                  v_less_taxtype_code := tax_matrix.then_taxtype_code;
                  v_less_cch_taxcat_code := tax_matrix.then_cch_taxcat_code;
                  v_less_cch_group_code := tax_matrix.then_cch_group_code;
                  -- v_less_cch_item_code := tax_matrix.then_cch_item_code;
                  v_equal_hold_code_flag := tax_matrix.else_hold_code_flag;
                  v_equal_taxcode_detail_id := tax_matrix.else_taxcode_detail_id;
                  v_equal_taxcode_state_code := tax_matrix.else_taxcode_state_code;
                  v_equal_taxcode_type_code := tax_matrix.else_taxcode_type_code;
                  v_equal_taxcode_code := tax_matrix.else_taxcode_code;
                  v_equal_jurisdiction_id := tax_matrix.else_jurisdiction_id;
                  v_equal_measure_type_code := tax_matrix.else_measure_type_code;
                  v_equal_taxtype_code := tax_matrix.else_taxtype_code;
                  v_equal_cch_taxcat_code := tax_matrix.else_cch_taxcat_code;
                  v_equal_cch_group_code := tax_matrix.else_cch_group_code;
                  -- v_equal_cch_item_code := tax_matrix.else_cch_item_code;
                  v_greater_hold_code_flag := tax_matrix.else_hold_code_flag;
                  v_greater_taxcode_detail_id := tax_matrix.else_taxcode_detail_id;
                  v_greater_taxcode_state_code := tax_matrix.else_taxcode_state_code;
                  v_greater_taxcode_type_code := tax_matrix.else_taxcode_type_code;
                  v_greater_taxcode_code := tax_matrix.else_taxcode_code;
                  v_greater_jurisdiction_id := tax_matrix.else_jurisdiction_id;
                  v_greater_measure_type_code := tax_matrix.else_measure_type_code;
                  v_greater_taxtype_code := tax_matrix.else_taxtype_code;
                  v_greater_cch_taxcat_code := tax_matrix.else_cch_taxcat_code;
                  v_greater_cch_group_code := tax_matrix.else_cch_group_code;
                  -- v_greater_cch_item_code := tax_matrix.else_cch_item_code;
               ELSIF tax_matrix.relation_sign = '<=' THEN
                  v_less_hold_code_flag := tax_matrix.then_hold_code_flag;
                  v_less_taxcode_detail_id := tax_matrix.then_taxcode_detail_id;
                  v_less_taxcode_state_code := tax_matrix.then_taxcode_state_code;
                  v_less_taxcode_type_code := tax_matrix.then_taxcode_type_code;
                  v_less_taxcode_code := tax_matrix.then_taxcode_code;
                  v_less_jurisdiction_id := tax_matrix.then_jurisdiction_id;
                  v_less_measure_type_code := tax_matrix.then_measure_type_code;
                  v_less_taxtype_code := tax_matrix.then_taxtype_code;
                  v_less_cch_taxcat_code := tax_matrix.then_cch_taxcat_code;
                  v_less_cch_group_code := tax_matrix.then_cch_group_code;
                  -- v_less_cch_item_code := tax_matrix.then_cch_item_code;
                  v_equal_hold_code_flag := tax_matrix.then_hold_code_flag;
                  v_equal_taxcode_detail_id := tax_matrix.then_taxcode_detail_id;
                  v_equal_taxcode_state_code := tax_matrix.then_taxcode_state_code;
                  v_equal_taxcode_type_code := tax_matrix.then_taxcode_type_code;
                  v_equal_taxcode_code := tax_matrix.then_taxcode_code;
                  v_equal_jurisdiction_id := tax_matrix.then_jurisdiction_id;
                  v_equal_measure_type_code := tax_matrix.then_measure_type_code;
                  v_equal_taxtype_code := tax_matrix.then_taxtype_code;
                  v_equal_cch_taxcat_code := tax_matrix.then_cch_taxcat_code;
                  v_equal_cch_group_code := tax_matrix.then_cch_group_code;
                  -- v_equal_cch_item_code := tax_matrix.then_cch_item_code;
                  v_greater_hold_code_flag := tax_matrix.else_hold_code_flag;
                  v_greater_taxcode_detail_id := tax_matrix.else_taxcode_detail_id;
                  v_greater_taxcode_state_code := tax_matrix.else_taxcode_state_code;
                  v_greater_taxcode_type_code := tax_matrix.else_taxcode_type_code;
                  v_greater_taxcode_code := tax_matrix.else_taxcode_code;
                  v_greater_jurisdiction_id := tax_matrix.else_jurisdiction_id;
                  v_greater_measure_type_code := tax_matrix.else_measure_type_code;
                  v_greater_taxtype_code := tax_matrix.else_taxtype_code;
                  v_greater_cch_taxcat_code := tax_matrix.else_cch_taxcat_code;
                  v_greater_cch_group_code := tax_matrix.else_cch_group_code;
                  -- v_greater_cch_item_code := tax_matrix.else_cch_item_code;
               ELSIF tax_matrix.relation_sign = '>' THEN
                  v_less_hold_code_flag := tax_matrix.else_hold_code_flag;
                  v_less_taxcode_detail_id := tax_matrix.else_taxcode_detail_id;
                  v_less_taxcode_state_code := tax_matrix.else_taxcode_state_code;
                  v_less_taxcode_type_code := tax_matrix.else_taxcode_type_code;
                  v_less_taxcode_code := tax_matrix.else_taxcode_code;
                  v_less_jurisdiction_id := tax_matrix.else_jurisdiction_id;
                  v_less_measure_type_code := tax_matrix.else_measure_type_code;
                  v_less_taxtype_code := tax_matrix.else_taxtype_code;
                  v_less_cch_taxcat_code := tax_matrix.else_cch_taxcat_code;
                  v_less_cch_group_code := tax_matrix.else_cch_group_code;
                  -- v_less_cch_item_code := tax_matrix.else_cch_item_code;
                  v_equal_hold_code_flag := tax_matrix.else_hold_code_flag;
                  v_equal_taxcode_detail_id := tax_matrix.else_taxcode_detail_id;
                  v_equal_taxcode_state_code := tax_matrix.else_taxcode_state_code;
                  v_equal_taxcode_type_code := tax_matrix.else_taxcode_type_code;
                  v_equal_taxcode_code := tax_matrix.else_taxcode_code;
                  v_equal_jurisdiction_id := tax_matrix.else_jurisdiction_id;
                  v_equal_measure_type_code := tax_matrix.else_measure_type_code;
                  v_equal_taxtype_code := tax_matrix.else_taxtype_code;
                  v_equal_cch_taxcat_code := tax_matrix.else_cch_taxcat_code;
                  v_equal_cch_group_code := tax_matrix.else_cch_group_code;
                  -- v_equal_cch_item_code := tax_matrix.else_cch_item_code;
                  v_greater_hold_code_flag := tax_matrix.then_hold_code_flag;
                  v_greater_taxcode_detail_id := tax_matrix.then_taxcode_detail_id;
                  v_greater_taxcode_state_code := tax_matrix.then_taxcode_state_code;
                  v_greater_taxcode_type_code := tax_matrix.then_taxcode_type_code;
                  v_greater_taxcode_code := tax_matrix.then_taxcode_code;
                  v_greater_jurisdiction_id := tax_matrix.then_jurisdiction_id;
                  v_greater_measure_type_code := tax_matrix.then_measure_type_code;
                  v_greater_taxtype_code := tax_matrix.then_taxtype_code;
                  v_greater_cch_taxcat_code := tax_matrix.then_cch_taxcat_code;
                  v_greater_cch_group_code := tax_matrix.then_cch_group_code;
                  -- v_greater_cch_item_code := tax_matrix.then_cch_item_code;
               ELSIF tax_matrix.relation_sign = '>=' THEN
                  v_less_hold_code_flag := tax_matrix.else_hold_code_flag;
                  v_less_taxcode_detail_id := tax_matrix.else_taxcode_detail_id;
                  v_less_taxcode_state_code := tax_matrix.else_taxcode_state_code;
                  v_less_taxcode_type_code := tax_matrix.else_taxcode_type_code;
                  v_less_taxcode_code := tax_matrix.else_taxcode_code;
                  v_less_jurisdiction_id := tax_matrix.else_jurisdiction_id;
                  v_less_measure_type_code := tax_matrix.else_measure_type_code;
                  v_less_taxtype_code := tax_matrix.else_taxtype_code;
                  v_less_cch_taxcat_code := tax_matrix.else_cch_taxcat_code;
                  v_less_cch_group_code := tax_matrix.else_cch_group_code;
                  -- v_less_cch_item_code := tax_matrix.else_cch_item_code;
                  v_equal_hold_code_flag := tax_matrix.then_hold_code_flag;
                  v_equal_taxcode_detail_id := tax_matrix.then_taxcode_detail_id;
                  v_equal_taxcode_state_code := tax_matrix.then_taxcode_state_code;
                  v_equal_taxcode_type_code := tax_matrix.then_taxcode_type_code;
                  v_equal_taxcode_code := tax_matrix.then_taxcode_code;
                  v_equal_jurisdiction_id := tax_matrix.then_jurisdiction_id;
                  v_equal_measure_type_code := tax_matrix.then_measure_type_code;
                  v_equal_taxtype_code := tax_matrix.then_taxtype_code;
                  v_equal_cch_taxcat_code := tax_matrix.then_cch_taxcat_code;
                  v_equal_cch_group_code := tax_matrix.then_cch_group_code;
                  -- v_equal_cch_item_code := tax_matrix.then_cch_item_code;
                  v_greater_hold_code_flag := tax_matrix.then_hold_code_flag;
                  v_greater_taxcode_detail_id := tax_matrix.then_taxcode_detail_id;
                  v_greater_taxcode_state_code := tax_matrix.then_taxcode_state_code;
                  v_greater_taxcode_type_code := tax_matrix.then_taxcode_type_code;
                  v_greater_taxcode_code := tax_matrix.then_taxcode_code;
                  v_greater_jurisdiction_id := tax_matrix.then_jurisdiction_id;
                  v_greater_measure_type_code := tax_matrix.then_measure_type_code;
                  v_greater_taxtype_code := tax_matrix.then_taxtype_code;
                  v_greater_cch_taxcat_code := tax_matrix.then_cch_taxcat_code;
                  v_greater_cch_group_code := tax_matrix.then_cch_group_code;
                  -- v_greater_cch_item_code := tax_matrix.then_cch_item_code;
               ELSIF tax_matrix.relation_sign = '<>' THEN
                  v_less_hold_code_flag := tax_matrix.then_hold_code_flag;
                  v_less_taxcode_detail_id := tax_matrix.then_taxcode_detail_id;
                  v_less_taxcode_state_code := tax_matrix.then_taxcode_state_code;
                  v_less_taxcode_type_code := tax_matrix.then_taxcode_type_code;
                  v_less_taxcode_code := tax_matrix.then_taxcode_code;
                  v_less_jurisdiction_id := tax_matrix.then_jurisdiction_id;
                  v_less_measure_type_code := tax_matrix.then_measure_type_code;
                  v_less_taxtype_code := tax_matrix.then_taxtype_code;
                  v_less_cch_taxcat_code := tax_matrix.then_cch_taxcat_code;
                  v_less_cch_group_code := tax_matrix.then_cch_group_code;
                  -- v_less_cch_item_code := tax_matrix.then_cch_item_code;
                  v_equal_hold_code_flag := tax_matrix.else_hold_code_flag;
                  v_equal_taxcode_detail_id := tax_matrix.else_taxcode_detail_id;
                  v_equal_taxcode_state_code := tax_matrix.else_taxcode_state_code;
                  v_equal_taxcode_type_code := tax_matrix.else_taxcode_type_code;
                  v_equal_taxcode_code := tax_matrix.else_taxcode_code;
                  v_equal_jurisdiction_id := tax_matrix.else_jurisdiction_id;
                  v_equal_measure_type_code := tax_matrix.else_measure_type_code;
                  v_equal_taxtype_code := tax_matrix.else_taxtype_code;
                  v_equal_cch_taxcat_code := tax_matrix.else_cch_taxcat_code;
                  v_equal_cch_group_code := tax_matrix.else_cch_group_code;
                  -- v_equal_cch_item_code := tax_matrix.else_cch_item_code;
                  v_greater_hold_code_flag := tax_matrix.then_hold_code_flag;
                  v_greater_taxcode_detail_id := tax_matrix.then_taxcode_detail_id;
                  v_greater_taxcode_state_code := tax_matrix.then_taxcode_state_code;
                  v_greater_taxcode_type_code := tax_matrix.then_taxcode_type_code;
                  v_greater_taxcode_code := tax_matrix.then_taxcode_code;
                  v_greater_jurisdiction_id := tax_matrix.then_jurisdiction_id;
                  v_greater_measure_type_code := tax_matrix.then_measure_type_code;
                  v_greater_taxtype_code := tax_matrix.then_taxtype_code;
                  v_greater_cch_taxcat_code := tax_matrix.then_cch_taxcat_code;
                  v_greater_cch_group_code := tax_matrix.then_cch_group_code;
                  -- v_greater_cch_item_code := tax_matrix.then_cch_item_code;
               END IF;

               SELECT CASE SIGN(NEW.gl_line_itm_dist_amt - tax_matrix.relation_amount) WHEN -1 THEN v_less_hold_code_flag WHEN 0 THEN v_equal_hold_code_flag WHEN 1 THEN v_greater_hold_code_flag END,
                      CASE SIGN(NEW.gl_line_itm_dist_amt - tax_matrix.relation_amount) WHEN -1 THEN v_less_taxcode_detail_id WHEN 0 THEN v_equal_taxcode_detail_id WHEN 1 THEN v_greater_taxcode_detail_id END,
                      CASE SIGN(NEW.gl_line_itm_dist_amt - tax_matrix.relation_amount) WHEN -1 THEN v_less_taxcode_state_code WHEN 0 THEN v_equal_taxcode_state_code WHEN 1 THEN v_greater_taxcode_state_code END,
                      CASE SIGN(NEW.gl_line_itm_dist_amt - tax_matrix.relation_amount) WHEN -1 THEN v_less_taxcode_type_code WHEN 0 THEN v_equal_taxcode_type_code WHEN 1 THEN v_greater_taxcode_type_code END,
                      CASE SIGN(NEW.gl_line_itm_dist_amt - tax_matrix.relation_amount) WHEN -1 THEN v_less_taxcode_code WHEN 0 THEN v_equal_taxcode_code WHEN 1 THEN v_greater_taxcode_code END,
                      CASE SIGN(NEW.gl_line_itm_dist_amt - tax_matrix.relation_amount) WHEN -1 THEN v_less_jurisdiction_id WHEN 0 THEN v_equal_jurisdiction_id WHEN 1 THEN v_greater_jurisdiction_id END,
                      CASE SIGN(NEW.gl_line_itm_dist_amt - tax_matrix.relation_amount) WHEN -1 THEN v_less_measure_type_code WHEN 0 THEN v_equal_measure_type_code WHEN 1 THEN v_greater_measure_type_code END,
                      CASE SIGN(NEW.gl_line_itm_dist_amt - tax_matrix.relation_amount) WHEN -1 THEN v_less_taxtype_code WHEN 0 THEN v_equal_taxtype_code WHEN 1 THEN v_greater_taxtype_code END,
                      CASE SIGN(NEW.gl_line_itm_dist_amt - tax_matrix.relation_amount) WHEN -1 THEN v_less_cch_taxcat_code WHEN 0 THEN v_equal_cch_taxcat_code WHEN 1 THEN v_greater_cch_taxcat_code END,
                      CASE SIGN(NEW.gl_line_itm_dist_amt - tax_matrix.relation_amount) WHEN -1 THEN v_less_cch_group_code WHEN 0 THEN v_equal_cch_group_code WHEN 1 THEN v_greater_cch_group_code END
                 INTO v_hold_code_flag, NEW.taxcode_detail_id, NEW.taxcode_state_code, NEW.taxcode_type_code, NEW.taxcode_code, v_override_jurisdiction_id, NEW.measure_type_code, v_taxtype_code, NEW.cch_taxcat_code, NEW.cch_group_code
                 ;

               IF v_override_jurisdiction_id IS NOT NULL AND v_override_jurisdiction_id <> 0 THEN
                  NEW.jurisdiction_id := v_override_jurisdiction_id;
		  NEW.manual_jurisdiction_ind := 'AO';
               END IF;
            ELSE
               -- Tax Matrix Line NOT Found
               NEW.transaction_ind := 'S';
               NEW.suspend_ind := 'T';
            END IF;
         END IF;
      ELSE
         -- Get Tax Type Code
         SELECT taxtype_code
           INTO STRICT v_taxtype_code
           FROM tb_taxcode
          WHERE taxcode_code = NEW.taxcode_code
            AND taxcode_type_code = NEW.taxcode_type_code;
      END IF;

      -- Continue if not Suspended
      IF NEW.transaction_ind IS NULL OR NEW.transaction_ind <> 'S' THEN
         -- ****** DETERMINE TYPE of TAXCODE ****** --
         -- If the new TaxCode Type is "T"axable then recalc Tax Amounts
         IF UPPER(NEW.taxcode_type_code) = 'T' THEN
            -- If Override jurisdiction id is not blank or 0 then use it instead of location matrix
            IF NEW.jurisdiction_id IS NOT NULL AND NEW.jurisdiction_id <> 0 THEN
               -- Only if there is a Location Matrix ID -- 3351 -- mbf01
               IF NEW.location_matrix_id IS NOT NULL AND NEW.location_matrix_id <> 0 THEN
                  -- -- do nothing -- all actions already taken in program!
                  -- NULL;
                  -- Get Override Tax Type Code -- 3351
                  -- Get 5 Tax Calc Flags -- 3411 -- MBF02
                  BEGIN
                     SELECT override_taxtype_code,
                            state_flag, county_flag, county_local_flag, city_flag, city_local_flag
                       INTO STRICT
			    location_matrix.override_taxtype_code,
                            location_matrix.state_flag,
                            location_matrix.county_flag,
                            location_matrix.county_local_flag,
                            location_matrix.city_flag,
                            location_matrix.city_local_flag
                       FROM tb_location_matrix
                      WHERE tb_location_matrix.location_matrix_id = NEW.location_matrix_id;
                  EXCEPTION
                     WHEN NO_DATA_FOUND THEN
                        location_matrix.override_taxtype_code := '*NO';
                        location_matrix.state_flag := '1';
                        location_matrix.county_flag := '1';
                        location_matrix.county_local_flag := '1';
                        location_matrix.city_flag := '1';
                        location_matrix.city_local_flag := '1';		
                  END;
               ELSE
                  location_matrix.override_taxtype_code := '*NO';
                  location_matrix.state_flag := '1';
                  location_matrix.county_flag := '1';
                  location_matrix.county_local_flag := '1';
                  location_matrix.city_flag := '1';
                  location_matrix.city_local_flag := '1';
               END IF;
            ELSE
            -- Search for Location Matrix if not already found
            IF NEW.location_matrix_id IS NULL  AND NEW.manual_jurisdiction_ind IS NULL THEN
               v_transaction_detail_id := NEW.transaction_detail_id;
               -- ***** Create Dynamic WHERE Clause for Matrix SQLs ***** --
               -- Location Matrix Drivers
               OPEN location_driver_cursor;
               LOOP
                  FETCH location_driver_cursor INTO location_driver;
                  
		  IF (NOT FOUND) THEN
			EXIT;
		  END IF;
                  
		  IF location_driver.null_driver_flag = '1' THEN
                     IF location_driver.wildcard_flag = '1' THEN
                        IF UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'CHAR' OR
                           UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'VARC' THEN
                              vc_location_matrix_where := vc_location_matrix_where || 'AND ' ||
                              '((tb_tmp_transaction_detail.' || location_driver.trans_dtl_column_name || ' IS NULL AND (tb_location_matrix.' || location_driver.matrix_column_name || ' = ''*ALL'' OR tb_location_matrix.' || location_driver.matrix_column_name || ' = ''*NULL'' )) OR ' ||
                              '(tb_tmp_transaction_detail.' || location_driver.trans_dtl_column_name || ' IS NOT NULL AND (tb_location_matrix.' || location_driver.matrix_column_name || ' = ''*ALL'' OR UPPER(tb_tmp_transaction_detail.' || location_driver.trans_dtl_column_name || ') LIKE UPPER(tb_location_matrix.' || location_driver.matrix_column_name || ') ))) ';
                        END IF;
                     ELSE
                        IF UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'CHAR' OR
                           UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'VARC' THEN
                              vc_location_matrix_where := vc_location_matrix_where || 'AND ' ||
                              '((tb_tmp_transaction_detail.' || location_driver.trans_dtl_column_name || ' IS NULL AND (tb_location_matrix.' || location_driver.matrix_column_name || ' = ''*ALL'' OR tb_location_matrix.' || location_driver.matrix_column_name || ' = ''*NULL'' )) OR ' ||
                              '(tb_tmp_transaction_detail.' || location_driver.trans_dtl_column_name || ' IS NOT NULL AND (tb_location_matrix.' || location_driver.matrix_column_name || ' = ''*ALL'' OR UPPER(tb_tmp_transaction_detail.' || location_driver.trans_dtl_column_name || ') = UPPER(tb_location_matrix.' || location_driver.matrix_column_name || ') ))) ';
                        END IF;
                     END IF;
                  ELSE
                     IF location_driver.wildcard_flag = '1' THEN
                        IF UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'CHAR' OR
                           UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'VARC' THEN
                              vc_location_matrix_where := vc_location_matrix_where || 'AND ' ||
                              '(tb_location_matrix.' || location_driver.matrix_column_name || ' = ''*ALL'' OR UPPER(tb_tmp_transaction_detail.' || location_driver.trans_dtl_column_name || ') LIKE UPPER(tb_location_matrix.' || location_driver.matrix_column_name || ') ) ';
                        END IF;
                     ELSE
                        IF UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'CHAR' OR
                           UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'VARC' THEN
                              vc_location_matrix_where := vc_location_matrix_where || 'AND ' ||
                              '(tb_location_matrix.' || location_driver.matrix_column_name || ' = ''*ALL'' OR UPPER(tb_tmp_transaction_detail.' || location_driver.trans_dtl_column_name || ') = UPPER(tb_location_matrix.' || location_driver.matrix_column_name || ') ) ';
                        END IF;
                     END IF;
                  END IF;
               END LOOP;
               CLOSE location_driver_cursor;
               -- If no drivers found raise error, else create transaction detail sql statement
               IF vc_location_matrix_where IS NULL THEN
                  NULL;
               ELSE
                  vc_location_matrix_stmt := vc_location_matrix_select || vc_location_matrix_where || vc_location_matrix_orderby;
               END IF;

	       -- ****** CREATE Temporary Table to hold "Bind" Values ****** -----------------------------------------
	       BEGIN
		    EXECUTE 'CREATE TEMPORARY TABLE tb_transaction_detail_b_u_tmp (param_name varchar(30), param_value varchar(30))';
	       EXCEPTION 
		     -- Table may already exist if using Connection/Session Pooling, Ignore
		     WHEN OTHERS THEN NULL;
	       END;

	       -- Prep TMP table with values
	       DELETE FROM tb_transaction_detail_b_u_tmp;
	       INSERT INTO tb_transaction_detail_b_u_tmp(param_name, param_value) VALUES ('v_transaction_detail_id',  v_transaction_detail_id);
	       
	       -- Search Location Matrix for matches
               BEGIN
                   OPEN location_matrix_cursor
                    FOR EXECUTE vc_location_matrix_stmt;

                  FETCH location_matrix_cursor
                   INTO location_matrix;

                  -- Rows found?
                  IF (FOUND) THEN
                     vn_fetch_rows := 1;
                  ELSE
                     vn_fetch_rows := 0;
                  END IF;
                  CLOSE location_matrix_cursor;
               EXCEPTION
                  WHEN OTHERS THEN
                     vn_fetch_rows := 0;
               END;

               -- Location Matrix Line Found
               IF vn_fetch_rows > 0 THEN
                  NEW.location_matrix_id := location_matrix.location_matrix_id;
                  NEW.jurisdiction_id := location_matrix.jurisdiction_id;
               ELSE
                  NEW.transaction_ind := 'S';
                  NEW.suspend_ind := 'L';
               END IF;
            END IF;
            END IF;

            -- Continue if transaction not Suspended
            IF ( NEW.transaction_ind IS NULL OR NEW.transaction_ind <> 'S' ) THEN
               -- Get Jurisdiction TaxRates and calculate Tax Amounts
               EXECUTE sp_getusetax(NEW.jurisdiction_id,
                            NEW.gl_date,
                            NEW.measure_type_code,
                            NEW.gl_line_itm_dist_amt,
                            v_taxtype_code,
                            location_matrix.override_taxtype_code,
			    location_matrix.state_flag,
                            location_matrix.county_flag,
                            location_matrix.county_local_flag,
                            location_matrix.city_flag,
                            location_matrix.city_local_flag,
			    NEW.invoice_freight_amt,
                            NEW.invoice_discount_amt,
                            NEW.transaction_state_code,
-- future?? --                     NEW.import_definition_code,
                            'importdefn',
                            NEW.taxcode_code,
                            NEW.jurisdiction_taxrate_id,
                            NEW.state_use_amount,
                            NEW.state_use_tier2_amount,
                            NEW.state_use_tier3_amount,
                            NEW.county_use_amount,
                            NEW.county_local_use_amount,
                            NEW.city_use_amount,
                            NEW.city_local_use_amount,
                            NEW.state_use_rate,
                            NEW.state_use_tier2_rate,
                            NEW.state_use_tier3_rate,
                            NEW.state_split_amount,
                            NEW.state_tier2_min_amount,
                            NEW.state_tier2_max_amount,
                            NEW.state_maxtax_amount,
                            NEW.county_use_rate,
                            NEW.county_local_use_rate,
                            NEW.county_split_amount,
                            NEW.county_maxtax_amount,
                            NEW.county_single_flag,
                            NEW.county_default_flag,
                            NEW.city_use_rate,
                            NEW.city_local_use_rate,
                            NEW.city_split_amount,
                            NEW.city_split_use_rate,
                            NEW.city_single_flag,
                            NEW.city_default_flag,
                            NEW.combined_use_rate,
                            NEW.tb_calc_tax_amt,
-- future?? --                     NEW.taxable_amt,
-- Targa --                    NEW.user_number_10,
                            vn_taxable_amt,
                            v_taxtype_used );

               -- Suspend if tax rate id is null
               IF NEW.jurisdiction_taxrate_id IS NULL OR NEW.jurisdiction_taxrate_id = 0 THEN
                  NEW.transaction_ind := 'S';
                  NEW.suspend_ind := 'R';
               ELSE
                  -- Check for Taxable Amount -- 3351
                  IF vn_taxable_amt <> NEW.gl_line_itm_dist_amt THEN
                     NEW.gl_extract_amt := NEW.gl_line_itm_dist_amt;
                     NEW.gl_line_itm_dist_amt := vn_taxable_amt;
                  END IF;
               END IF;
            END IF;

         -- Else, If the new TaxCode Type is "E"xempt
         ELSIF NEW.taxcode_type_code = 'E' THEN
            NEW.transaction_ind := 'P';

         -- Else, If the new TaxCode Type is a "Q"uestion
         ELSIF NEW.taxcode_type_code = 'Q' THEN
           NEW.transaction_ind := 'Q';

         -- Else, the new TaxCode Type is Unrecognized - Suspend
         ELSE
            NEW.transaction_ind := 'S';
            NEW.suspend_ind := '?';
         END IF;
      END IF;

      -- Continue if not Suspended
      IF NEW.transaction_ind IS NULL OR NEW.transaction_ind <> 'S' THEN
         NEW.transaction_ind := 'P';
      END IF;
   END IF;

   -- ******  GL logging module  ****** ------------------------------------------------------------
   -- IF the new TaxCode Detail ID, Jurisdiction ID, or Jurisdiction TaxRate ID is different then the old one
   IF ( ( UPPER(OLD.transaction_ind) = 'P' ) AND
        ( ( OLD.taxcode_detail_id <> NEW.taxcode_detail_id OR
            OLD.taxcode_detail_id IS NOT NULL AND NEW.taxcode_detail_id IS NULL ) OR
          ( OLD.jurisdiction_id <> NEW.jurisdiction_id OR
            OLD.jurisdiction_id IS NOT NULL AND NEW.jurisdiction_id IS NULL ) OR
          ( OLD.jurisdiction_taxrate_id <> NEW.jurisdiction_taxrate_id OR
            OLD.jurisdiction_taxrate_id IS NOT NULL AND NEW.jurisdiction_taxrate_id IS NULL ) ) ) OR
      ( UPPER(OLD.transaction_ind) = 'S' AND UPPER(NEW.transaction_ind) = 'P' ) THEN
      -- If this transaction has not been currently marked by GL Extract
      --(It could be marked before but then having the taxcode changed by users)
      IF OLD.gl_extract_flag IS NULL THEN
         -- The GL_LOG_FLAG indicates that this transaction has been flagged for GL Logging.  It means that the previously marked
         -- transaction had its taxcode modified by users.  In this case, log the before and after image to TB_GL_EXPORT_LOG table
         IF  OLD.gl_log_flag IS NOT NULL THEN
            -- 1. Create a before image for this transaction into TO_GL_EXPORT_LOG table with the negative tax amount.
            --    This is to offset the previous tax amount that had been sent into ERP system.
            INSERT INTO tb_gl_export_log (
               transaction_detail_id,
               source_transaction_id,
               process_batch_no,
               gl_extract_batch_no,
               archive_batch_no,
               allocation_matrix_id,
               allocation_subtrans_id,
               entered_date,
               transaction_status,
               gl_date,
               gl_company_nbr,
               gl_company_name,
               gl_division_nbr,
               gl_division_name,
               gl_cc_nbr_dept_id,
               gl_cc_nbr_dept_name,
               gl_local_acct_nbr,
               gl_local_acct_name,
               gl_local_sub_acct_nbr,
               gl_local_sub_acct_name,
               gl_full_acct_nbr,
               gl_full_acct_name,
               gl_line_itm_dist_amt,
               orig_gl_line_itm_dist_amt,
               vendor_nbr,
               vendor_name,
               vendor_address_line_1,
               vendor_address_line_2,
               vendor_address_line_3,
               vendor_address_line_4,
               vendor_address_city,
               vendor_address_county,
               vendor_address_state,
               vendor_address_zip,
               vendor_address_country,
               vendor_type,
               vendor_type_name,
               invoice_nbr,
               invoice_desc,
               invoice_date,
               invoice_freight_amt,
               invoice_discount_amt,
               invoice_tax_amt,
               invoice_total_amt,
               invoice_tax_flg,
               invoice_line_nbr,
               invoice_line_name,
               invoice_line_type,
               invoice_line_type_name,
               invoice_line_amt,
               invoice_line_tax,
               afe_project_nbr,
               afe_project_name,
               afe_category_nbr,
               afe_category_name,
               afe_sub_cat_nbr,
               afe_sub_cat_name,
               afe_use,
               afe_contract_type,
               afe_contract_structure,
               afe_property_cat,
               inventory_nbr,
               inventory_name,
               inventory_class,
               inventory_class_name,
               po_nbr,
               po_name,
               po_date,
               po_line_nbr,
               po_line_name,
               po_line_type,
               po_line_type_name,
               ship_to_location,
               ship_to_location_name,
               ship_to_address_line_1,
               ship_to_address_line_2,
               ship_to_address_line_3,
               ship_to_address_line_4,
               ship_to_address_city,
               ship_to_address_county,
               ship_to_address_state,
               ship_to_address_zip,
               ship_to_address_country,
               wo_nbr,
               wo_name,
               wo_date,
               wo_type,
               wo_type_desc,
               wo_class,
               wo_class_desc,
               wo_entity,
               wo_entity_desc,
               wo_line_nbr,
               wo_line_name,
               wo_line_type,
               wo_line_type_desc,
               wo_shut_down_cd,
               wo_shut_down_cd_desc,
               voucher_id,
               voucher_name,
               voucher_date,
               voucher_line_nbr,
               voucher_line_desc,
               check_nbr,
               check_no,
               check_date,
               check_amt,
               check_desc,
               user_text_01,
               user_text_02,
               user_text_03,
               user_text_04,
               user_text_05,
               user_text_06,
               user_text_07,
               user_text_08,
               user_text_09,
               user_text_10,
               user_text_11,
               user_text_12,
               user_text_13,
               user_text_14,
               user_text_15,
               user_text_16,
               user_text_17,
               user_text_18,
               user_text_19,
               user_text_20,
               user_text_21,
               user_text_22,
               user_text_23,
               user_text_24,
               user_text_25,
               user_text_26,
               user_text_27,
               user_text_28,
               user_text_29,
               user_text_30,
               user_number_01,
               user_number_02,
               user_number_03,
               user_number_04,
               user_number_05,
               user_number_06,
               user_number_07,
               user_number_08,
               user_number_09,
               user_number_10,
               user_date_01,
               user_date_02,
               user_date_03,
               user_date_04,
               user_date_05,
               user_date_06,
               user_date_07,
               user_date_08,
               user_date_09,
               user_date_10,
               comments,
               tb_calc_tax_amt,
               state_use_amount,
               state_use_tier2_amount,
               state_use_tier3_amount,
               county_use_amount,
               county_local_use_amount,
               city_use_amount,
               city_local_use_amount,
               transaction_state_code,
               transaction_ind,
               suspend_ind,
               taxcode_detail_id,
               taxcode_state_code,
               taxcode_type_code,
               taxcode_code,
               cch_taxcat_code,
               cch_group_code,
               cch_item_code,
               manual_taxcode_ind,
               tax_matrix_id,
               location_matrix_id,
               jurisdiction_id,
               jurisdiction_taxrate_id,
               manual_jurisdiction_ind,
               measure_type_code,
               state_use_rate,
               state_use_tier2_rate,
               state_use_tier3_rate,
               state_split_amount,
               state_tier2_min_amount,
               state_tier2_max_amount,
               state_maxtax_amount,
               county_use_rate,
               county_local_use_rate,
               county_split_amount,
               county_maxtax_amount,
               county_single_flag,
               county_default_flag,
               city_use_rate,
               city_local_use_rate,
               city_split_amount,
               city_split_use_rate,
               city_single_flag,
               city_default_flag,
               combined_use_rate,
               load_timestamp,
               gl_extract_flag,
               gl_extract_amt,
               audit_flag,
               audit_user_id,
               audit_timestamp,
               modify_user_id,
               modify_timestamp,
               update_user_id,
               update_timestamp )
            VALUES (
               OLD.transaction_detail_id,
               OLD.source_transaction_id,
               OLD.process_batch_no,
               OLD.gl_extract_batch_no,
               OLD.archive_batch_no,
               OLD.allocation_matrix_id,
               OLD.allocation_subtrans_id,
               OLD.entered_date,
               OLD.transaction_status,
               OLD.gl_date,
               OLD.gl_company_nbr,
               OLD.gl_company_name,
               OLD.gl_division_nbr,
               OLD.gl_division_name,
               OLD.gl_cc_nbr_dept_id,
               OLD.gl_cc_nbr_dept_name,
               OLD.gl_local_acct_nbr,
               OLD.gl_local_acct_name,
               OLD.gl_local_sub_acct_nbr,
               OLD.gl_local_sub_acct_name,
               OLD.gl_full_acct_nbr,
               OLD.gl_full_acct_name,
               OLD.gl_line_itm_dist_amt,
               OLD.orig_gl_line_itm_dist_amt,
               OLD.vendor_nbr,
               OLD.vendor_name,
               OLD.vendor_address_line_1,
               OLD.vendor_address_line_2,
               OLD.vendor_address_line_3,
               OLD.vendor_address_line_4,
               OLD.vendor_address_city,
               OLD.vendor_address_county,
               OLD.vendor_address_state,
               OLD.vendor_address_zip,
               OLD.vendor_address_country,
               OLD.vendor_type,
               OLD.vendor_type_name,
               OLD.invoice_nbr,
               OLD.invoice_desc,
               OLD.invoice_date,
               OLD.invoice_freight_amt,
               OLD.invoice_discount_amt,
               OLD.invoice_tax_amt,
               OLD.invoice_total_amt,
               OLD.invoice_tax_flg,
               OLD.invoice_line_nbr,
               OLD.invoice_line_name,
               OLD.invoice_line_type,
               OLD.invoice_line_type_name,
               OLD.invoice_line_amt,
               OLD.invoice_line_tax,
               OLD.afe_project_nbr,
               OLD.afe_project_name,
               OLD.afe_category_nbr,
               OLD.afe_category_name,
               OLD.afe_sub_cat_nbr,
               OLD.afe_sub_cat_name,
               OLD.afe_use,
               OLD.afe_contract_type,
               OLD.afe_contract_structure,
               OLD.afe_property_cat,
               OLD.inventory_nbr,
               OLD.inventory_name,
               OLD.inventory_class,
               OLD.inventory_class_name,
               OLD.po_nbr,
               OLD.po_name,
               OLD.po_date,
               OLD.po_line_nbr,
               OLD.po_line_name,
               OLD.po_line_type,
               OLD.po_line_type_name,
               OLD.ship_to_location,
               OLD.ship_to_location_name,
               OLD.ship_to_address_line_1,
               OLD.ship_to_address_line_2,
               OLD.ship_to_address_line_3,
               OLD.ship_to_address_line_4,
               OLD.ship_to_address_city,
               OLD.ship_to_address_county,
               OLD.ship_to_address_state,
               OLD.ship_to_address_zip,
               OLD.ship_to_address_country,
               OLD.wo_nbr,
               OLD.wo_name,
               OLD.wo_date,
               OLD.wo_type,
               OLD.wo_type_desc,
               OLD.wo_class,
               OLD.wo_class_desc,
               OLD.wo_entity,
               OLD.wo_entity_desc,
               OLD.wo_line_nbr,
               OLD.wo_line_name,
               OLD.wo_line_type,
               OLD.wo_line_type_desc,
               OLD.wo_shut_down_cd,
               OLD.wo_shut_down_cd_desc,
               OLD.voucher_id,
               OLD.voucher_name,
               OLD.voucher_date,
               OLD.voucher_line_nbr,
               OLD.voucher_line_desc,
               OLD.check_nbr,
               OLD.check_no,
               OLD.check_date,
               OLD.check_amt,
               OLD.check_desc,
               OLD.user_text_01,
               OLD.user_text_02,
               OLD.user_text_03,
               OLD.user_text_04,
               OLD.user_text_05,
               OLD.user_text_06,
               OLD.user_text_07,
               OLD.user_text_08,
               OLD.user_text_09,
               OLD.user_text_10,
               OLD.user_text_11,
               OLD.user_text_12,
               OLD.user_text_13,
               OLD.user_text_14,
               OLD.user_text_15,
               OLD.user_text_16,
               OLD.user_text_17,
               OLD.user_text_18,
               OLD.user_text_19,
               OLD.user_text_20,
               OLD.user_text_21,
               OLD.user_text_22,
               OLD.user_text_23,
               OLD.user_text_24,
               OLD.user_text_25,
               OLD.user_text_26,
               OLD.user_text_27,
               OLD.user_text_28,
               OLD.user_text_29,
               OLD.user_text_30,
               OLD.user_number_01,
               OLD.user_number_02,
               OLD.user_number_03,
               OLD.user_number_04,
               OLD.user_number_05,
               OLD.user_number_06,
               OLD.user_number_07,
               OLD.user_number_08,
               OLD.user_number_09,
               OLD.user_number_10,
               OLD.user_date_01,
               OLD.user_date_02,
               OLD.user_date_03,
               OLD.user_date_04,
               OLD.user_date_05,
               OLD.user_date_06,
               OLD.user_date_07,
               OLD.user_date_08,
               OLD.user_date_09,
               OLD.user_date_10,
               OLD.comments,
               OLD.tb_calc_tax_amt * -1,
               OLD.state_use_amount * -1,
               OLD.state_use_tier2_amount * -1,
               OLD.state_use_tier3_amount * -1,
               OLD.county_use_amount * -1,
               OLD.county_local_use_amount * -1,
               OLD.city_use_amount * -1,
               OLD.city_local_use_amount * -1,
               OLD.transaction_state_code,
               OLD.transaction_ind,
               OLD.suspend_ind,
               OLD.taxcode_detail_id,
               OLD.taxcode_state_code,
               OLD.taxcode_type_code,
               OLD.taxcode_code,
               OLD.cch_taxcat_code,
               OLD.cch_group_code,
               OLD.cch_item_code,
               OLD.manual_taxcode_ind,
               OLD.tax_matrix_id,
               OLD.location_matrix_id,
               OLD.jurisdiction_id,
               OLD.jurisdiction_taxrate_id,
               OLD.manual_jurisdiction_ind,
               OLD.measure_type_code,
               OLD.state_use_rate,
               OLD.state_use_tier2_rate,
               OLD.state_use_tier3_rate,
               OLD.state_split_amount,
               OLD.state_tier2_min_amount,
               OLD.state_tier2_max_amount,
               OLD.state_maxtax_amount,
               OLD.county_use_rate,
               OLD.county_local_use_rate,
               OLD.county_split_amount,
               OLD.county_maxtax_amount,
               OLD.county_single_flag,
               OLD.county_default_flag,
               OLD.city_use_rate,
               OLD.city_local_use_rate,
               OLD.city_split_amount,
               OLD.city_split_use_rate,
               OLD.city_single_flag,
               OLD.city_default_flag,
               OLD.combined_use_rate,
               OLD.load_timestamp,
               OLD.gl_extract_flag,
               OLD.gl_extract_amt,
               OLD.audit_flag,
               OLD.audit_user_id,
               OLD.audit_timestamp,
               OLD.modify_user_id,
               OLD.modify_timestamp,
               OLD.update_user_id,
               v_sysdate );

            -- 2. Create an after image for this transaction into TO_GL_EXPORT_LOG table with the new tax amount.
            --    This if for tracking the newest tax amount.
            INSERT INTO tb_gl_export_log (
               transaction_detail_id,
               source_transaction_id,
               process_batch_no,
               gl_extract_batch_no,
               archive_batch_no,
               allocation_matrix_id,
               allocation_subtrans_id,
               entered_date,
               transaction_status,
               gl_date,
               gl_company_nbr,
               gl_company_name,
               gl_division_nbr,
               gl_division_name,
               gl_cc_nbr_dept_id,
               gl_cc_nbr_dept_name,
               gl_local_acct_nbr,
               gl_local_acct_name,
               gl_local_sub_acct_nbr,
               gl_local_sub_acct_name,
               gl_full_acct_nbr,
               gl_full_acct_name,
               gl_line_itm_dist_amt,
               orig_gl_line_itm_dist_amt,
               vendor_nbr,
               vendor_name,
               vendor_address_line_1,
               vendor_address_line_2,
               vendor_address_line_3,
               vendor_address_line_4,
               vendor_address_city,
               vendor_address_county,
               vendor_address_state,
               vendor_address_zip,
               vendor_address_country,
               vendor_type,
               vendor_type_name,
               invoice_nbr,
               invoice_desc,
               invoice_date,
               invoice_freight_amt,
               invoice_discount_amt,
               invoice_tax_amt,
               invoice_total_amt,
               invoice_tax_flg,
               invoice_line_nbr,
               invoice_line_name,
               invoice_line_type,
               invoice_line_type_name,
               invoice_line_amt,
               invoice_line_tax,
               afe_project_nbr,
               afe_project_name,
               afe_category_nbr,
               afe_category_name,
               afe_sub_cat_nbr,
               afe_sub_cat_name,
               afe_use,
               afe_contract_type,
               afe_contract_structure,
               afe_property_cat,
               inventory_nbr,
               inventory_name,
               inventory_class,
               inventory_class_name,
               po_nbr,
               po_name,
               po_date,
               po_line_nbr,
               po_line_name,
               po_line_type,
               po_line_type_name,
               ship_to_location,
               ship_to_location_name,
               ship_to_address_line_1,
               ship_to_address_line_2,
               ship_to_address_line_3,
               ship_to_address_line_4,
               ship_to_address_city,
               ship_to_address_county,
               ship_to_address_state,
               ship_to_address_zip,
               ship_to_address_country,
               wo_nbr,
               wo_name,
               wo_date,
               wo_type,
               wo_type_desc,
               wo_class,
               wo_class_desc,
               wo_entity,
               wo_entity_desc,
               wo_line_nbr,
               wo_line_name,
               wo_line_type,
               wo_line_type_desc,
               wo_shut_down_cd,
               wo_shut_down_cd_desc,
               voucher_id,
               voucher_name,
               voucher_date,
               voucher_line_nbr,
               voucher_line_desc,
               check_nbr,
               check_no,
               check_date,
               check_amt,
               check_desc,
               user_text_01,
               user_text_02,
               user_text_03,
               user_text_04,
               user_text_05,
               user_text_06,
               user_text_07,
               user_text_08,
               user_text_09,
               user_text_10,
               user_text_11,
               user_text_12,
               user_text_13,
               user_text_14,
               user_text_15,
               user_text_16,
               user_text_17,
               user_text_18,
               user_text_19,
               user_text_20,
               user_text_21,
               user_text_22,
               user_text_23,
               user_text_24,
               user_text_25,
               user_text_26,
               user_text_27,
               user_text_28,
               user_text_29,
               user_text_30,
               user_number_01,
               user_number_02,
               user_number_03,
               user_number_04,
               user_number_05,
               user_number_06,
               user_number_07,
               user_number_08,
               user_number_09,
               user_number_10,
               user_date_01,
               user_date_02,
               user_date_03,
               user_date_04,
               user_date_05,
               user_date_06,
               user_date_07,
               user_date_08,
               user_date_09,
               user_date_10,
               comments,
               tb_calc_tax_amt,
               state_use_amount,
               state_use_tier2_amount,
               state_use_tier3_amount,
               county_use_amount,
               county_local_use_amount,
               city_use_amount,
               city_local_use_amount,
               transaction_state_code,
               transaction_ind,
               suspend_ind,
               taxcode_detail_id,
               taxcode_state_code,
               taxcode_type_code,
               taxcode_code,
               cch_taxcat_code,
               cch_group_code,
               cch_item_code,
               manual_taxcode_ind,
               tax_matrix_id,
               location_matrix_id,
               jurisdiction_id,
               jurisdiction_taxrate_id,
               manual_jurisdiction_ind,
               measure_type_code,
               state_use_rate,
               state_use_tier2_rate,
               state_use_tier3_rate,
               state_split_amount,
               state_tier2_min_amount,
               state_tier2_max_amount,
               state_maxtax_amount,
               county_use_rate,
               county_local_use_rate,
               county_split_amount,
               county_maxtax_amount,
               county_single_flag,
               county_default_flag,
               city_use_rate,
               city_local_use_rate,
               city_split_amount,
               city_split_use_rate,
               city_single_flag,
               city_default_flag,
               combined_use_rate,
               load_timestamp,
               gl_extract_flag,
               gl_extract_amt,
               audit_flag,
               audit_user_id,
               audit_timestamp,
               modify_user_id,
               modify_timestamp,
               update_user_id,
               update_timestamp )
            VALUES (
               NEW.transaction_detail_id,
               NEW.source_transaction_id,
               NEW.process_batch_no,
               NEW.gl_extract_batch_no,
               NEW.archive_batch_no,
               NEW.allocation_matrix_id,
               NEW.allocation_subtrans_id,
               NEW.entered_date,
               NEW.transaction_status,
               NEW.gl_date,
               NEW.gl_company_nbr,
               NEW.gl_company_name,
               NEW.gl_division_nbr,
               NEW.gl_division_name,
               NEW.gl_cc_nbr_dept_id,
               NEW.gl_cc_nbr_dept_name,
               NEW.gl_local_acct_nbr,
               NEW.gl_local_acct_name,
               NEW.gl_local_sub_acct_nbr,
               NEW.gl_local_sub_acct_name,
               NEW.gl_full_acct_nbr,
               NEW.gl_full_acct_name,
               NEW.gl_line_itm_dist_amt,
               NEW.orig_gl_line_itm_dist_amt,
               NEW.vendor_nbr,
               NEW.vendor_name,
               NEW.vendor_address_line_1,
               NEW.vendor_address_line_2,
               NEW.vendor_address_line_3,
               NEW.vendor_address_line_4,
               NEW.vendor_address_city,
               NEW.vendor_address_county,
               NEW.vendor_address_state,
               NEW.vendor_address_zip,
               NEW.vendor_address_country,
               NEW.vendor_type,
               NEW.vendor_type_name,
               NEW.invoice_nbr,
               NEW.invoice_desc,
               NEW.invoice_date,
               NEW.invoice_freight_amt,
               NEW.invoice_discount_amt,
               NEW.invoice_tax_amt,
               NEW.invoice_total_amt,
               NEW.invoice_tax_flg,
               NEW.invoice_line_nbr,
               NEW.invoice_line_name,
               NEW.invoice_line_type,
               NEW.invoice_line_type_name,
               NEW.invoice_line_amt,
               NEW.invoice_line_tax,
               NEW.afe_project_nbr,
               NEW.afe_project_name,
               NEW.afe_category_nbr,
               NEW.afe_category_name,
               NEW.afe_sub_cat_nbr,
               NEW.afe_sub_cat_name,
               NEW.afe_use,
               NEW.afe_contract_type,
               NEW.afe_contract_structure,
               NEW.afe_property_cat,
               NEW.inventory_nbr,
               NEW.inventory_name,
               NEW.inventory_class,
               NEW.inventory_class_name,
               NEW.po_nbr,
               NEW.po_name,
               NEW.po_date,
               NEW.po_line_nbr,
               NEW.po_line_name,
               NEW.po_line_type,
               NEW.po_line_type_name,
               NEW.ship_to_location,
               NEW.ship_to_location_name,
               NEW.ship_to_address_line_1,
               NEW.ship_to_address_line_2,
               NEW.ship_to_address_line_3,
               NEW.ship_to_address_line_4,
               NEW.ship_to_address_city,
               NEW.ship_to_address_county,
               NEW.ship_to_address_state,
               NEW.ship_to_address_zip,
               NEW.ship_to_address_country,
               NEW.wo_nbr,
               NEW.wo_name,
               NEW.wo_date,
               NEW.wo_type,
               NEW.wo_type_desc,
               NEW.wo_class,
               NEW.wo_class_desc,
               NEW.wo_entity,
               NEW.wo_entity_desc,
               NEW.wo_line_nbr,
               NEW.wo_line_name,
               NEW.wo_line_type,
               NEW.wo_line_type_desc,
               NEW.wo_shut_down_cd,
               NEW.wo_shut_down_cd_desc,
               NEW.voucher_id,
               NEW.voucher_name,
               NEW.voucher_date,
               NEW.voucher_line_nbr,
               NEW.voucher_line_desc,
               NEW.check_nbr,
               NEW.check_no,
               NEW.check_date,
               NEW.check_amt,
               NEW.check_desc,
               NEW.user_text_01,
               NEW.user_text_02,
               NEW.user_text_03,
               NEW.user_text_04,
               NEW.user_text_05,
               NEW.user_text_06,
               NEW.user_text_07,
               NEW.user_text_08,
               NEW.user_text_09,
               NEW.user_text_10,
               NEW.user_text_11,
               NEW.user_text_12,
               NEW.user_text_13,
               NEW.user_text_14,
               NEW.user_text_15,
               NEW.user_text_16,
               NEW.user_text_17,
               NEW.user_text_18,
               NEW.user_text_19,
               NEW.user_text_20,
               NEW.user_text_21,
               NEW.user_text_22,
               NEW.user_text_23,
               NEW.user_text_24,
               NEW.user_text_25,
               NEW.user_text_26,
               NEW.user_text_27,
               NEW.user_text_28,
               NEW.user_text_29,
               NEW.user_text_30,
               NEW.user_number_01,
               NEW.user_number_02,
               NEW.user_number_03,
               NEW.user_number_04,
               NEW.user_number_05,
               NEW.user_number_06,
               NEW.user_number_07,
               NEW.user_number_08,
               NEW.user_number_09,
               NEW.user_number_10,
               NEW.user_date_01,
               NEW.user_date_02,
               NEW.user_date_03,
               NEW.user_date_04,
               NEW.user_date_05,
               NEW.user_date_06,
               NEW.user_date_07,
               NEW.user_date_08,
               NEW.user_date_09,
               NEW.user_date_10,
               NEW.comments,
               NEW.tb_calc_tax_amt,
               NEW.state_use_amount,
               NEW.state_use_tier2_amount,
               NEW.state_use_tier3_amount,
               NEW.county_use_amount,
               NEW.county_local_use_amount,
               NEW.city_use_amount,
               NEW.city_local_use_amount,
               NEW.transaction_state_code,
               NEW.transaction_ind,
               NEW.suspend_ind,
               NEW.taxcode_detail_id,
               NEW.taxcode_state_code,
               NEW.taxcode_type_code,
               NEW.taxcode_code,
               NEW.cch_taxcat_code,
               NEW.cch_group_code,
               NEW.cch_item_code,
               NEW.manual_taxcode_ind,
               NEW.tax_matrix_id,
               NEW.location_matrix_id,
               NEW.jurisdiction_id,
               NEW.jurisdiction_taxrate_id,
               NEW.manual_jurisdiction_ind,
               NEW.measure_type_code,
               NEW.state_use_rate,
               NEW.state_use_tier2_rate,
               NEW.state_use_tier3_rate,
               NEW.state_split_amount,
               NEW.state_tier2_min_amount,
               NEW.state_tier2_max_amount,
               NEW.state_maxtax_amount,
               NEW.county_use_rate,
               NEW.county_local_use_rate,
               NEW.county_split_amount,
               NEW.county_maxtax_amount,
               NEW.county_single_flag,
               NEW.county_default_flag,
               NEW.city_use_rate,
               NEW.city_local_use_rate,
               NEW.city_split_amount,
               NEW.city_split_use_rate,
               NEW.city_single_flag,
               NEW.city_default_flag,
               NEW.combined_use_rate,
               NEW.load_timestamp,
               NEW.gl_extract_flag,
               NEW.gl_extract_amt,
               NEW.audit_flag,
               NEW.audit_user_id,
               NEW.audit_timestamp,
               NEW.modify_user_id,
               NEW.modify_timestamp,
               NEW.update_user_id,
               v_sysdate_plus );
         END IF;

      -- If this transaction has been currently marked by GL Extract, and user decides to change the taxcode.
      ELSIF  NEW.GL_EXTRACT_FLAG IS NOT NULL THEN
         -- 1. reset the GL_EXTRACT_FLAG to make it available for next GL Extract
         NEW.gl_extract_flag := NULL;

         -- 2. If the current GL mark was marked first time by the first GL Extract process, then set the GL Log flag
         --    to remember GL Logging.
         IF OLD.gl_log_flag IS NULL THEN
            NEW.GL_LOG_FLAG := 1;
         END IF;

         -- 3. Create a before image for this transaction into TO_GL_EXPORT_LOG table with the negative tax amount.
         --    This is for offset the previous tax amount that had sent into ERP system.
         INSERT INTO tb_gl_export_log (
            transaction_detail_id,
            source_transaction_id,
            process_batch_no,
            gl_extract_batch_no,
            archive_batch_no,
            allocation_matrix_id,
            allocation_subtrans_id,
            entered_date,
            transaction_status,
            gl_date,
            gl_company_nbr,
            gl_company_name,
            gl_division_nbr,
            gl_division_name,
            gl_cc_nbr_dept_id,
            gl_cc_nbr_dept_name,
            gl_local_acct_nbr,
            gl_local_acct_name,
            gl_local_sub_acct_nbr,
            gl_local_sub_acct_name,
            gl_full_acct_nbr,
            gl_full_acct_name,
            gl_line_itm_dist_amt,
            orig_gl_line_itm_dist_amt,
            vendor_nbr,
            vendor_name,
            vendor_address_line_1,
            vendor_address_line_2,
            vendor_address_line_3,
            vendor_address_line_4,
            vendor_address_city,
            vendor_address_county,
            vendor_address_state,
            vendor_address_zip,
            vendor_address_country,
            vendor_type,
            vendor_type_name,
            invoice_nbr,
            invoice_desc,
            invoice_date,
            invoice_freight_amt,
            invoice_discount_amt,
            invoice_tax_amt,
            invoice_total_amt,
            invoice_tax_flg,
            invoice_line_nbr,
            invoice_line_name,
            invoice_line_type,
            invoice_line_type_name,
            invoice_line_amt,
            invoice_line_tax,
            afe_project_nbr,
            afe_project_name,
            afe_category_nbr,
            afe_category_name,
            afe_sub_cat_nbr,
            afe_sub_cat_name,
            afe_use,
            afe_contract_type,
            afe_contract_structure,
            afe_property_cat,
            inventory_nbr,
            inventory_name,
            inventory_class,
            inventory_class_name,
            po_nbr,
            po_name,
            po_date,
            po_line_nbr,
            po_line_name,
            po_line_type,
            po_line_type_name,
            ship_to_location,
            ship_to_location_name,
            ship_to_address_line_1,
            ship_to_address_line_2,
            ship_to_address_line_3,
            ship_to_address_line_4,
            ship_to_address_city,
            ship_to_address_county,
            ship_to_address_state,
            ship_to_address_zip,
            ship_to_address_country,
            wo_nbr,
            wo_name,
            wo_date,
            wo_type,
            wo_type_desc,
            wo_class,
            wo_class_desc,
            wo_entity,
            wo_entity_desc,
            wo_line_nbr,
            wo_line_name,
            wo_line_type,
            wo_line_type_desc,
            wo_shut_down_cd,
            wo_shut_down_cd_desc,
            voucher_id,
            voucher_name,
            voucher_date,
            voucher_line_nbr,
            voucher_line_desc,
            check_nbr,
            check_no,
            check_date,
            check_amt,
            check_desc,
            user_text_01,
            user_text_02,
            user_text_03,
            user_text_04,
            user_text_05,
            user_text_06,
            user_text_07,
            user_text_08,
            user_text_09,
            user_text_10,
            user_text_11,
            user_text_12,
            user_text_13,
            user_text_14,
            user_text_15,
            user_text_16,
            user_text_17,
            user_text_18,
            user_text_19,
            user_text_20,
            user_text_21,
            user_text_22,
            user_text_23,
            user_text_24,
            user_text_25,
            user_text_26,
            user_text_27,
            user_text_28,
            user_text_29,
            user_text_30,
            user_number_01,
            user_number_02,
            user_number_03,
            user_number_04,
            user_number_05,
            user_number_06,
            user_number_07,
            user_number_08,
            user_number_09,
            user_number_10,
            user_date_01,
            user_date_02,
            user_date_03,
            user_date_04,
            user_date_05,
            user_date_06,
            user_date_07,
            user_date_08,
            user_date_09,
            user_date_10,
            comments,
            tb_calc_tax_amt,
            state_use_amount,
            state_use_tier2_amount,
            state_use_tier3_amount,
            county_use_amount,
            county_local_use_amount,
            city_use_amount,
            city_local_use_amount,
            transaction_state_code,
            transaction_ind,
            suspend_ind,
            taxcode_detail_id,
            taxcode_state_code,
            taxcode_type_code,
            taxcode_code,
            cch_taxcat_code,
            cch_group_code,
            cch_item_code,
            manual_taxcode_ind,
            tax_matrix_id,
            location_matrix_id,
            jurisdiction_id,
            jurisdiction_taxrate_id,
            manual_jurisdiction_ind,
            measure_type_code,
            state_use_rate,
            state_use_tier2_rate,
            state_use_tier3_rate,
            state_split_amount,
            state_tier2_min_amount,
            state_tier2_max_amount,
            state_maxtax_amount,
            county_use_rate,
            county_local_use_rate,
            county_split_amount,
            county_maxtax_amount,
            county_single_flag,
            county_default_flag,
            city_use_rate,
            city_local_use_rate,
            city_split_amount,
            city_split_use_rate,
            city_single_flag,
            city_default_flag,
            combined_use_rate,
            load_timestamp,
            gl_extract_flag,
            gl_extract_amt,
            audit_flag,
            audit_user_id,
            audit_timestamp,
            modify_user_id,
            modify_timestamp,
            update_user_id,
            update_timestamp )
         VALUES (
            OLD.transaction_detail_id,
            OLD.source_transaction_id,
            OLD.process_batch_no,
            OLD.gl_extract_batch_no,
            OLD.archive_batch_no,
            OLD.allocation_matrix_id,
            OLD.allocation_subtrans_id,
            OLD.entered_date,
            OLD.transaction_status,
            OLD.gl_date,
            OLD.gl_company_nbr,
            OLD.gl_company_name,
            OLD.gl_division_nbr,
            OLD.gl_division_name,
            OLD.gl_cc_nbr_dept_id,
            OLD.gl_cc_nbr_dept_name,
            OLD.gl_local_acct_nbr,
            OLD.gl_local_acct_name,
            OLD.gl_local_sub_acct_nbr,
            OLD.gl_local_sub_acct_name,
            OLD.gl_full_acct_nbr,
            OLD.gl_full_acct_name,
            OLD.gl_line_itm_dist_amt,
            OLD.orig_gl_line_itm_dist_amt,
            OLD.vendor_nbr,
            OLD.vendor_name,
            OLD.vendor_address_line_1,
            OLD.vendor_address_line_2,
            OLD.vendor_address_line_3,
            OLD.vendor_address_line_4,
            OLD.vendor_address_city,
            OLD.vendor_address_county,
            OLD.vendor_address_state,
            OLD.vendor_address_zip,
            OLD.vendor_address_country,
            OLD.vendor_type,
            OLD.vendor_type_name,
            OLD.invoice_nbr,
            OLD.invoice_desc,
            OLD.invoice_date,
            OLD.invoice_freight_amt,
            OLD.invoice_discount_amt,
            OLD.invoice_tax_amt,
            OLD.invoice_total_amt,
            OLD.invoice_tax_flg,
            OLD.invoice_line_nbr,
            OLD.invoice_line_name,
            OLD.invoice_line_type,
            OLD.invoice_line_type_name,
            OLD.invoice_line_amt,
            OLD.invoice_line_tax,
            OLD.afe_project_nbr,
            OLD.afe_project_name,
            OLD.afe_category_nbr,
            OLD.afe_category_name,
            OLD.afe_sub_cat_nbr,
            OLD.afe_sub_cat_name,
            OLD.afe_use,
            OLD.afe_contract_type,
            OLD.afe_contract_structure,
            OLD.afe_property_cat,
            OLD.inventory_nbr,
            OLD.inventory_name,
            OLD.inventory_class,
            OLD.inventory_class_name,
            OLD.po_nbr,
            OLD.po_name,
            OLD.po_date,
            OLD.po_line_nbr,
            OLD.po_line_name,
            OLD.po_line_type,
            OLD.po_line_type_name,
            OLD.ship_to_location,
            OLD.ship_to_location_name,
            OLD.ship_to_address_line_1,
            OLD.ship_to_address_line_2,
            OLD.ship_to_address_line_3,
            OLD.ship_to_address_line_4,
            OLD.ship_to_address_city,
            OLD.ship_to_address_county,
            OLD.ship_to_address_state,
            OLD.ship_to_address_zip,
            OLD.ship_to_address_country,
            OLD.wo_nbr,
            OLD.wo_name,
            OLD.wo_date,
            OLD.wo_type,
            OLD.wo_type_desc,
            OLD.wo_class,
            OLD.wo_class_desc,
            OLD.wo_entity,
            OLD.wo_entity_desc,
            OLD.wo_line_nbr,
            OLD.wo_line_name,
            OLD.wo_line_type,
            OLD.wo_line_type_desc,
            OLD.wo_shut_down_cd,
            OLD.wo_shut_down_cd_desc,
            OLD.voucher_id,
            OLD.voucher_name,
            OLD.voucher_date,
            OLD.voucher_line_nbr,
            OLD.voucher_line_desc,
            OLD.check_nbr,
            OLD.check_no,
            OLD.check_date,
            OLD.check_amt,
            OLD.check_desc,
            OLD.user_text_01,
            OLD.user_text_02,
            OLD.user_text_03,
            OLD.user_text_04,
            OLD.user_text_05,
            OLD.user_text_06,
            OLD.user_text_07,
            OLD.user_text_08,
            OLD.user_text_09,
            OLD.user_text_10,
            OLD.user_text_11,
            OLD.user_text_12,
            OLD.user_text_13,
            OLD.user_text_14,
            OLD.user_text_15,
            OLD.user_text_16,
            OLD.user_text_17,
            OLD.user_text_18,
            OLD.user_text_19,
            OLD.user_text_20,
            OLD.user_text_21,
            OLD.user_text_22,
            OLD.user_text_23,
            OLD.user_text_24,
            OLD.user_text_25,
            OLD.user_text_26,
            OLD.user_text_27,
            OLD.user_text_28,
            OLD.user_text_29,
            OLD.user_text_30,
            OLD.user_number_01,
            OLD.user_number_02,
            OLD.user_number_03,
            OLD.user_number_04,
            OLD.user_number_05,
            OLD.user_number_06,
            OLD.user_number_07,
            OLD.user_number_08,
            OLD.user_number_09,
            OLD.user_number_10,
            OLD.user_date_01,
            OLD.user_date_02,
            OLD.user_date_03,
            OLD.user_date_04,
            OLD.user_date_05,
            OLD.user_date_06,
            OLD.user_date_07,
            OLD.user_date_08,
            OLD.user_date_09,
            OLD.user_date_10,
            OLD.comments,
            OLD.tb_calc_tax_amt * -1,
            OLD.state_use_amount * -1,
            OLD.state_use_tier2_amount * -1,
            OLD.state_use_tier3_amount * -1,
            OLD.county_use_amount * -1,
            OLD.county_local_use_amount * -1,
            OLD.city_use_amount * -1,
            OLD.city_local_use_amount * -1,
            OLD.transaction_state_code,
            OLD.transaction_ind,
            OLD.suspend_ind,
            OLD.taxcode_detail_id,
            OLD.taxcode_state_code,
            OLD.taxcode_type_code,
            OLD.taxcode_code,
            OLD.cch_taxcat_code,
            OLD.cch_group_code,
            OLD.cch_item_code,
            OLD.manual_taxcode_ind,
            OLD.tax_matrix_id,
            OLD.location_matrix_id,
            OLD.jurisdiction_id,
            OLD.jurisdiction_taxrate_id,
            OLD.manual_jurisdiction_ind,
            OLD.measure_type_code,
            OLD.state_use_rate,
            OLD.state_use_tier2_rate,
            OLD.state_use_tier3_rate,
            OLD.state_split_amount,
            OLD.state_tier2_min_amount,
            OLD.state_tier2_max_amount,
            OLD.state_maxtax_amount,
            OLD.county_use_rate,
            OLD.county_local_use_rate,
            OLD.county_split_amount,
            OLD.county_maxtax_amount,
            OLD.county_single_flag,
            OLD.county_default_flag,
            OLD.city_use_rate,
            OLD.city_local_use_rate,
            OLD.city_split_amount,
            OLD.city_split_use_rate,
            OLD.city_single_flag,
            OLD.city_default_flag,
            OLD.combined_use_rate,
            OLD.load_timestamp,
             NULL,
            OLD.gl_extract_amt,
            OLD.audit_flag,
            OLD.audit_user_id,
            OLD.audit_timestamp,
            OLD.modify_user_id,
            OLD.modify_timestamp,
            OLD.update_user_id,
            v_sysdate );

         -- 4. Create an after image for this transaction into TO_GL_EXPORT_LOG table with the new tax amount.
         --    This is for tracking the newest tax amount.
         INSERT INTO tb_gl_export_log (
            transaction_detail_id,
            source_transaction_id,
            process_batch_no,
            gl_extract_batch_no,
            archive_batch_no,
            allocation_matrix_id,
            allocation_subtrans_id,
            entered_date,
            transaction_status,
            gl_date,
            gl_company_nbr,
            gl_company_name,
            gl_division_nbr,
            gl_division_name,
            gl_cc_nbr_dept_id,
            gl_cc_nbr_dept_name,
            gl_local_acct_nbr,
            gl_local_acct_name,
            gl_local_sub_acct_nbr,
            gl_local_sub_acct_name,
            gl_full_acct_nbr,
            gl_full_acct_name,
            gl_line_itm_dist_amt,
            orig_gl_line_itm_dist_amt,
            vendor_nbr,
            vendor_name,
            vendor_address_line_1,
            vendor_address_line_2,
            vendor_address_line_3,
            vendor_address_line_4,
            vendor_address_city,
            vendor_address_county,
            vendor_address_state,
            vendor_address_zip,
            vendor_address_country,
            vendor_type,
            vendor_type_name,
            invoice_nbr,
            invoice_desc,
            invoice_date,
            invoice_freight_amt,
            invoice_discount_amt,
            invoice_tax_amt,
            invoice_total_amt,
            invoice_tax_flg,
            invoice_line_nbr,
            invoice_line_name,
            invoice_line_type,
            invoice_line_type_name,
            invoice_line_amt,
            invoice_line_tax,
            afe_project_nbr,
            afe_project_name,
            afe_category_nbr,
            afe_category_name,
            afe_sub_cat_nbr,
            afe_sub_cat_name,
            afe_use,
            afe_contract_type,
            afe_contract_structure,
            afe_property_cat,
            inventory_nbr,
            inventory_name,
            inventory_class,
            inventory_class_name,
            po_nbr,
            po_name,
            po_date,
            po_line_nbr,
            po_line_name,
            po_line_type,
            po_line_type_name,
            ship_to_location,
            ship_to_location_name,
            ship_to_address_line_1,
            ship_to_address_line_2,
            ship_to_address_line_3,
            ship_to_address_line_4,
            ship_to_address_city,
            ship_to_address_county,
            ship_to_address_state,
            ship_to_address_zip,
            ship_to_address_country,
            wo_nbr,
            wo_name,
            wo_date,
            wo_type,
            wo_type_desc,
            wo_class,
            wo_class_desc,
            wo_entity,
            wo_entity_desc,
            wo_line_nbr,
            wo_line_name,
            wo_line_type,
            wo_line_type_desc,
            wo_shut_down_cd,
            wo_shut_down_cd_desc,
            voucher_id,
            voucher_name,
            voucher_date,
            voucher_line_nbr,
            voucher_line_desc,
            check_nbr,
            check_no,
            check_date,
            check_amt,
            check_desc,
            user_text_01,
            user_text_02,
            user_text_03,
            user_text_04,
            user_text_05,
            user_text_06,
            user_text_07,
            user_text_08,
            user_text_09,
            user_text_10,
            user_text_11,
            user_text_12,
            user_text_13,
            user_text_14,
            user_text_15,
            user_text_16,
            user_text_17,
            user_text_18,
            user_text_19,
            user_text_20,
            user_text_21,
            user_text_22,
            user_text_23,
            user_text_24,
            user_text_25,
            user_text_26,
            user_text_27,
            user_text_28,
            user_text_29,
            user_text_30,
            user_number_01,
            user_number_02,
            user_number_03,
            user_number_04,
            user_number_05,
            user_number_06,
            user_number_07,
            user_number_08,
            user_number_09,
            user_number_10,
            user_date_01,
            user_date_02,
            user_date_03,
            user_date_04,
            user_date_05,
            user_date_06,
            user_date_07,
            user_date_08,
            user_date_09,
            user_date_10,
            comments,
            tb_calc_tax_amt,
            state_use_amount,
            state_use_tier2_amount,
            state_use_tier3_amount,
            county_use_amount,
            county_local_use_amount,
            city_use_amount,
            city_local_use_amount,
            transaction_state_code,
            transaction_ind,
            suspend_ind,
            taxcode_detail_id,
            taxcode_state_code,
            taxcode_type_code,
            taxcode_code,
            cch_taxcat_code,
            cch_group_code,
            cch_item_code,
            manual_taxcode_ind,
            tax_matrix_id,
            location_matrix_id,
            jurisdiction_id,
            jurisdiction_taxrate_id,
            manual_jurisdiction_ind,
            measure_type_code,
            state_use_rate,
            state_use_tier2_rate,
            state_use_tier3_rate,
            state_split_amount,
            state_tier2_min_amount,
            state_tier2_max_amount,
            state_maxtax_amount,
            county_use_rate,
            county_local_use_rate,
            county_split_amount,
            county_maxtax_amount,
            county_single_flag,
            county_default_flag,
            city_use_rate,
            city_local_use_rate,
            city_split_amount,
            city_split_use_rate,
            city_single_flag,
            city_default_flag,
            combined_use_rate,
            load_timestamp,
            gl_extract_flag,
            gl_extract_amt,
            audit_flag,
            audit_user_id,
            audit_timestamp,
            modify_user_id,
            modify_timestamp,
            update_user_id,
            update_timestamp )
         VALUES (
            NEW.transaction_detail_id,
            NEW.source_transaction_id,
            NEW.process_batch_no,
            NEW.gl_extract_batch_no,
            NEW.archive_batch_no,
            NEW.allocation_matrix_id,
            NEW.allocation_subtrans_id,
            NEW.entered_date,
            NEW.transaction_status,
            NEW.gl_date,
            NEW.gl_company_nbr,
            NEW.gl_company_name,
            NEW.gl_division_nbr,
            NEW.gl_division_name,
            NEW.gl_cc_nbr_dept_id,
            NEW.gl_cc_nbr_dept_name,
            NEW.gl_local_acct_nbr,
            NEW.gl_local_acct_name,
            NEW.gl_local_sub_acct_nbr,
            NEW.gl_local_sub_acct_name,
            NEW.gl_full_acct_nbr,
            NEW.gl_full_acct_name,
            NEW.gl_line_itm_dist_amt,
            NEW.orig_gl_line_itm_dist_amt,
            NEW.vendor_nbr,
            NEW.vendor_name,
            NEW.vendor_address_line_1,
            NEW.vendor_address_line_2,
            NEW.vendor_address_line_3,
            NEW.vendor_address_line_4,
            NEW.vendor_address_city,
            NEW.vendor_address_county,
            NEW.vendor_address_state,
            NEW.vendor_address_zip,
            NEW.vendor_address_country,
            NEW.vendor_type,
            NEW.vendor_type_name,
            NEW.invoice_nbr,
            NEW.invoice_desc,
            NEW.invoice_date,
            NEW.invoice_freight_amt,
            NEW.invoice_discount_amt,
            NEW.invoice_tax_amt,
            NEW.invoice_total_amt,
            NEW.invoice_tax_flg,
            NEW.invoice_line_nbr,
            NEW.invoice_line_name,
            NEW.invoice_line_type,
            NEW.invoice_line_type_name,
            NEW.invoice_line_amt,
            NEW.invoice_line_tax,
            NEW.afe_project_nbr,
            NEW.afe_project_name,
            NEW.afe_category_nbr,
            NEW.afe_category_name,
            NEW.afe_sub_cat_nbr,
            NEW.afe_sub_cat_name,
            NEW.afe_use,
            NEW.afe_contract_type,
            NEW.afe_contract_structure,
            NEW.afe_property_cat,
            NEW.inventory_nbr,
            NEW.inventory_name,
            NEW.inventory_class,
            NEW.inventory_class_name,
            NEW.po_nbr,
            NEW.po_name,
            NEW.po_date,
            NEW.po_line_nbr,
            NEW.po_line_name,
            NEW.po_line_type,
            NEW.po_line_type_name,
            NEW.ship_to_location,
            NEW.ship_to_location_name,
            NEW.ship_to_address_line_1,
            NEW.ship_to_address_line_2,
            NEW.ship_to_address_line_3,
            NEW.ship_to_address_line_4,
            NEW.ship_to_address_city,
            NEW.ship_to_address_county,
            NEW.ship_to_address_state,
            NEW.ship_to_address_zip,
            NEW.ship_to_address_country,
            NEW.wo_nbr,
            NEW.wo_name,
            NEW.wo_date,
            NEW.wo_type,
            NEW.wo_type_desc,
            NEW.wo_class,
            NEW.wo_class_desc,
            NEW.wo_entity,
            NEW.wo_entity_desc,
            NEW.wo_line_nbr,
            NEW.wo_line_name,
            NEW.wo_line_type,
            NEW.wo_line_type_desc,
            NEW.wo_shut_down_cd,
            NEW.wo_shut_down_cd_desc,
            NEW.voucher_id,
            NEW.voucher_name,
            NEW.voucher_date,
            NEW.voucher_line_nbr,
            NEW.voucher_line_desc,
            NEW.check_nbr,
            NEW.check_no,
            NEW.check_date,
            NEW.check_amt,
            NEW.check_desc,
            NEW.user_text_01,
            NEW.user_text_02,
            NEW.user_text_03,
            NEW.user_text_04,
            NEW.user_text_05,
            NEW.user_text_06,
            NEW.user_text_07,
            NEW.user_text_08,
            NEW.user_text_09,
            NEW.user_text_10,
            NEW.user_text_11,
            NEW.user_text_12,
            NEW.user_text_13,
            NEW.user_text_14,
            NEW.user_text_15,
            NEW.user_text_16,
            NEW.user_text_17,
            NEW.user_text_18,
            NEW.user_text_19,
            NEW.user_text_20,
            NEW.user_text_21,
            NEW.user_text_22,
            NEW.user_text_23,
            NEW.user_text_24,
            NEW.user_text_25,
            NEW.user_text_26,
            NEW.user_text_27,
            NEW.user_text_28,
            NEW.user_text_29,
            NEW.user_text_30,
            NEW.user_number_01,
            NEW.user_number_02,
            NEW.user_number_03,
            NEW.user_number_04,
            NEW.user_number_05,
            NEW.user_number_06,
            NEW.user_number_07,
            NEW.user_number_08,
            NEW.user_number_09,
            NEW.user_number_10,
            NEW.user_date_01,
            NEW.user_date_02,
            NEW.user_date_03,
            NEW.user_date_04,
            NEW.user_date_05,
            NEW.user_date_06,
            NEW.user_date_07,
            NEW.user_date_08,
            NEW.user_date_09,
            NEW.user_date_10,
            NEW.comments,
            NEW.tb_calc_tax_amt,
            NEW.state_use_amount,
            NEW.state_use_tier2_amount,
            NEW.state_use_tier3_amount,
            NEW.county_use_amount,
            NEW.county_local_use_amount,
            NEW.city_use_amount,
            NEW.city_local_use_amount,
            NEW.transaction_state_code,
            NEW.transaction_ind,
            NEW.suspend_ind,
            NEW.taxcode_detail_id,
            NEW.taxcode_state_code,
            NEW.taxcode_type_code,
            NEW.taxcode_code,
            NEW.cch_taxcat_code,
            NEW.cch_group_code,
            NEW.cch_item_code,
            NEW.manual_taxcode_ind,
            NEW.tax_matrix_id,
            NEW.location_matrix_id,
            NEW.jurisdiction_id,
            NEW.jurisdiction_taxrate_id,
            NEW.manual_jurisdiction_ind,
            NEW.measure_type_code,
            NEW.state_use_rate,
            NEW.state_use_tier2_rate,
            NEW.state_use_tier3_rate,
            NEW.state_split_amount,
            NEW.state_tier2_min_amount,
            NEW.state_tier2_max_amount,
            NEW.state_maxtax_amount,
            NEW.county_use_rate,
            NEW.county_local_use_rate,
            NEW.county_split_amount,
            NEW.county_maxtax_amount,
            NEW.county_single_flag,
            NEW.county_default_flag,
            NEW.city_use_rate,
            NEW.city_local_use_rate,
            NEW.city_split_amount,
            NEW.city_split_use_rate,
            NEW.city_single_flag,
            NEW.city_default_flag,
            NEW.combined_use_rate,
            NEW.load_timestamp,
            NEW.gl_extract_flag,
            NEW.gl_extract_amt,
            NEW.audit_flag,
            NEW.audit_user_id,
            NEW.audit_timestamp,
            NEW.modify_user_id,
            NEW.modify_timestamp,
            NEW.update_user_id,
            v_sysdate_plus );

         -- 5. Reset GL_EXTRACT_TIMESTAMP to null for next GL Extract process
            NEW.gl_extract_timestamp := NULL;
      END IF;
   END IF;

   RETURN NEW;
END;
$$ LANGUAGE plpgsql;


CREATE TRIGGER tb_transaction_detail_b_u
BEFORE UPDATE ON tb_transaction_detail
FOR EACH ROW
EXECUTE PROCEDURE tb_transaction_detail_b_u();

