/**  Process User Exit Function  ******************************************************************/
CREATE OR REPLACE FUNCTION "F_PROCESS_USEREXIT" (an_batch_id IN NUMERIC)
/**************************************************************************************************/
/* Object Type/Name: Function - f_process_userexit                                                */
/* Author:           Michael B. Fuller                                                            */
/* Date:             05/18/2007                                                                   */
/* Description:      calls custom processing user exit                                            */
/* Arguments:        an_batch_id IN NUMBER (0=All)                                                */
/* Returns:          Number                                                                       */
/**************************************************************************************************/
/* Maintenance History:                                                                           */
/* Scan  Author       Date     Release    Description                                  Ticket #   */
/* ----- ---------- ---------- ---------- -------------------------------------------- ---------- */
/**************************************************************************************************/
RETURNS NUMERIC AS $$
DECLARE
   return_code                     NUMERIC                                            := -1;
   proc_exist                      NUMERIC                                            := 0;

-- ****** Program starts ****** --------------------------------------------------------------------
BEGIN

   -- Determine if stored procedures exists
   SELECT       COUNT(*)
   INTO         STRICT proc_exist
   FROM         information_schema.routines
   WHERE        routine_schema = 'stscorp'
   AND          routine_type = 'FUNCTION'
   AND          routine_name = 'sp_process_userexit';
   
   -- If procedure exists, call it
   IF proc_exist > 0 THEN
      -- return_code returns 0 if success, or else returns SQLCODE
      EXECUTE SP_PROCESS_USEREXIT(an_batch_id, return_code); 
   END IF;

   RETURN return_code;

END;
$$ LANGUAGE plpgsql;
