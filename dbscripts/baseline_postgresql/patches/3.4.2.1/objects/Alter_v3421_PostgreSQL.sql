/**  Alter to v3.4.2.1  **/
/**  Run: create_2_stored_procs, create_3_triggers **/

-- NOTE: The SPOOL command does not exist in psql.  To log the session to file, start psql as follows:
--   CMD> psql.exe -L <fullPathOfLogfile> < ... >

--set define off
--spool c:\alter_v3411_PostgreSQL.log
--set echo on

/**  Update VERSION in option table  **/
UPDATE tb_option
   SET value = '3.4.2.1'
 WHERE option_code = 'VERSION'
   AND option_type_code = 'ADMIN'
   AND user_code = 'ADMIN';

/**************************************************************************************************/
/**  v3411 changes  *******************************************************************************/
/**************************************************************************************************/

/**  Modify Location Matrix table  ****************************************************************/
ALTER TABLE tb_location_matrix DROP CONSTRAINT tb_location_matrix_pkey CASCADE;

DROP FUNCTION tb_location_matrix_a_i()  CASCADE;
DROP FUNCTION tb_location_matrix_b_iu() CASCADE;

DROP INDEX idx_loc_matrix_effective_dt;
DROP INDEX idx_loc_matrix_juris_id;

--DROP PUBLIC SYNONYM tb_location_matrix;

ALTER TABLE tb_location_matrix RENAME TO v337_location_matrix;

CREATE TABLE tb_location_matrix (
   location_matrix_id              numeric           NOT NULL,
   driver_01                       varchar(100),
   driver_01_desc                  varchar(100),
   driver_02                       varchar(100),
   driver_02_desc                  varchar(100),
   driver_03                       varchar(100),
   driver_03_desc                  varchar(100),
   driver_04                       varchar(100),
   driver_04_desc                  varchar(100),
   driver_05                       varchar(100),
   driver_05_desc                  varchar(100),
   driver_06                       varchar(100),
   driver_06_desc                  varchar(100),
   driver_07                       varchar(100),
   driver_07_desc                  varchar(100),
   driver_08                       varchar(100),
   driver_08_desc                  varchar(100),
   driver_09                       varchar(100),
   driver_09_desc                  varchar(100),
   driver_10                       varchar(100),
   driver_10_desc                  varchar(100),
   binary_weight                   numeric,
   significant_digits              varchar(39),
   default_flag                    char(1),
   default_binary_weight           numeric,
   default_significant_digits      varchar(39),
   effective_date                  timestamp(0),
   expiration_date                 timestamp(0),
   jurisdiction_id                 numeric,
   state                           varchar(10),
   override_taxtype_code           varchar(10),
   state_flag                      char(1),
   county_flag                     char(1),
   county_local_flag               char(1),
   city_flag                       char(1),
   city_local_flag                 char(1),
   comments                        varchar(255),
   last_used_timestamp             timestamp(0),
   update_user_id                  varchar(40)     DEFAULT CURRENT_USER,
   update_timestamp                timestamp(0)    DEFAULT CURRENT_TIMESTAMP,
   CONSTRAINT pk_tb_location_matrix PRIMARY KEY ( location_matrix_id ) ) ; 

--CREATE PUBLIC SYNONYM tb_location_matrix FOR STSCORP.tb_location_matrix;

GRANT DELETE, INSERT, SELECT, UPDATE ON tb_location_matrix TO PUBLIC;

CREATE INDEX idx_loc_matrix_effective_dt ON tb_location_matrix(effective_date);
CREATE INDEX idx_loc_matrix_juris_id ON tb_location_matrix(jurisdiction_id);

INSERT INTO tb_location_matrix (
   location_matrix_id,
   driver_01,
   driver_01_desc,
   driver_02,
   driver_02_desc,
   driver_03,
   driver_03_desc,
   driver_04,
   driver_04_desc,
   driver_05,
   driver_05_desc,
   driver_06,
   driver_06_desc,
   driver_07,
   driver_07_desc,
   driver_08,
   driver_08_desc,
   driver_09,
   driver_09_desc,
   driver_10,
   driver_10_desc,
   binary_weight,
   significant_digits,
   default_flag,
   default_binary_weight,
   default_significant_digits,
   effective_date,
   expiration_date,
   jurisdiction_id,
   state,
   override_taxtype_code,
   state_flag,
   county_flag,
   county_local_flag,
   city_flag,
   city_local_flag,
   comments,
   last_used_timestamp,
   update_user_id,
   update_timestamp )
SELECT
   location_matrix_id,
   driver_01,
   driver_01_desc,
   driver_02,
   driver_02_desc,
   driver_03,
   driver_03_desc,
   driver_04,
   driver_04_desc,
   driver_05,
   driver_05_desc,
   driver_06,
   driver_06_desc,
   driver_07,
   driver_07_desc,
   driver_08,
   driver_08_desc,
   driver_09,
   driver_09_desc,
   driver_10,
   driver_10_desc,
   binary_weight,
   significant_digits,
   default_flag,
   default_binary_weight,
   default_significant_digits,
   effective_date,
   expiration_date,
   jurisdiction_id,
   state,
   override_taxtype_code,
   '1',
   '1',
   '1',
   '1',
   '1',
   comments,
   last_used_timestamp,
   update_user_id,
   update_timestamp
FROM v337_location_matrix;


/**************************************************************************************************/
/**  v3421 changes are in stored procs only  ******************************************************/
/**************************************************************************************************/

/**************************************************************************************************/
/**  Foreign Keys/Constraints  ********************************************************************/
/**************************************************************************************************/
ALTER TABLE STSCORP.TB_BATCH_ERROR_LOG drop constraint FK_TB_BATCH_ERROR_LOG;
ALTER TABLE STSCORP.TB_JURISDICTION_TAXRATE drop constraint FK_TB_JURISDICTION_TAXRATE;
ALTER TABLE STSCORP.TB_LOCATION_MATRIX drop constraint FK_TB_LOCATION_MATRIX;
ALTER TABLE STSCORP.TB_TAXCODE_DETAIL drop constraint FK_TB_TAXCODE_DETAIL;
ALTER TABLE STSCORP.TB_TAX_MATRIX drop constraint FK_TB_TAX_MATRIX_1;
ALTER TABLE STSCORP.TB_TAX_MATRIX drop constraint FK_TB_TAX_MATRIX_2;
ALTER TABLE STSCORP.TB_TRANSACTION_DETAIL drop constraint FK_TB_TRANSACTION_DETAIL_1;
ALTER TABLE STSCORP.TB_TRANSACTION_DETAIL drop constraint FK_TB_TRANSACTION_DETAIL_2;
ALTER TABLE STSCORP.TB_TRANSACTION_DETAIL drop constraint FK_TB_TRANSACTION_DETAIL_3;
ALTER TABLE STSCORP.TB_TRANSACTION_DETAIL drop constraint FK_TB_TRANSACTION_DETAIL_4;
ALTER TABLE STSCORP.TB_TRANSACTION_DETAIL drop constraint FK_TB_TRANSACTION_DETAIL_5;
ALTER TABLE STSCORP.TB_TRANSACTION_DETAIL drop constraint FK_TB_TRANSACTION_DETAIL_6;
--ALTER TABLE STSCORP.TB_TRANSACTION_DETAIL_EXCP drop constraint FK_TB_TRANSACTION_DETAIL_EXCP;

UPDATE tb_taxcode_detail SET jurisdiction_id = NULL WHERE jurisdiction_id = 0;
UPDATE tb_tax_matrix SET else_taxcode_detail_id = NULL WHERE else_taxcode_detail_id = 0;
UPDATE tb_transaction_detail SET allocation_matrix_id = NULL WHERE allocation_matrix_id = 0;
UPDATE tb_transaction_detail SET jurisdiction_id = NULL WHERE jurisdiction_id = 0;
UPDATE tb_transaction_detail SET jurisdiction_taxrate_id = NULL WHERE jurisdiction_taxrate_id = 0;
UPDATE tb_transaction_detail SET taxcode_detail_id = NULL WHERE taxcode_detail_id = 0;
UPDATE tb_transaction_detail SET tax_matrix_id = NULL WHERE tax_matrix_id = 0;
UPDATE tb_transaction_detail SET location_matrix_id = NULL WHERE location_matrix_id = 0;

--CREATE TABLE tb_transaction_detail_excp_bu AS SELECT * FROM tb_transaction_detail_excp WHERE transaction_detail_id NOT IN (select transaction_detail_id from tb_transaction_detail);
--DELETE FROM tb_transaction_detail_excp WHERE transaction_detail_id NOT IN (select transaction_detail_id from tb_transaction_detail);

ALTER TABLE STSCORP.TB_BATCH_ERROR_LOG ADD CONSTRAINT FK_TB_BATCH_ERROR_LOG FOREIGN KEY (BATCH_ID) REFERENCES STSCORP.TB_BATCH(BATCH_ID);
ALTER TABLE STSCORP.TB_JURISDICTION_TAXRATE ADD CONSTRAINT FK_TB_JURISDICTION_TAXRATE FOREIGN KEY (JURISDICTION_ID) REFERENCES STSCORP.TB_JURISDICTION(JURISDICTION_ID);
ALTER TABLE STSCORP.TB_LOCATION_MATRIX ADD CONSTRAINT FK_TB_LOCATION_MATRIX FOREIGN KEY (JURISDICTION_ID) REFERENCES STSCORP.TB_JURISDICTION(JURISDICTION_ID);
ALTER TABLE STSCORP.TB_TAXCODE_DETAIL ADD CONSTRAINT FK_TB_TAXCODE_DETAIL FOREIGN KEY (JURISDICTION_ID) REFERENCES STSCORP.TB_JURISDICTION(JURISDICTION_ID);
ALTER TABLE STSCORP.TB_TAX_MATRIX ADD CONSTRAINT FK_TB_TAX_MATRIX_1 FOREIGN KEY (THEN_TAXCODE_DETAIL_ID) REFERENCES STSCORP.TB_TAXCODE_DETAIL(TAXCODE_DETAIL_ID);
ALTER TABLE STSCORP.TB_TAX_MATRIX ADD CONSTRAINT FK_TB_TAX_MATRIX_2 FOREIGN KEY (ELSE_TAXCODE_DETAIL_ID) REFERENCES STSCORP.TB_TAXCODE_DETAIL(TAXCODE_DETAIL_ID);
ALTER TABLE STSCORP.TB_TRANSACTION_DETAIL ADD CONSTRAINT FK_TB_TRANSACTION_DETAIL_1 FOREIGN KEY (ALLOCATION_MATRIX_ID) REFERENCES STSCORP.TB_ALLOCATION_MATRIX(ALLOCATION_MATRIX_ID);
ALTER TABLE STSCORP.TB_TRANSACTION_DETAIL ADD CONSTRAINT FK_TB_TRANSACTION_DETAIL_2 FOREIGN KEY (JURISDICTION_ID) REFERENCES STSCORP.TB_JURISDICTION(JURISDICTION_ID);
ALTER TABLE STSCORP.TB_TRANSACTION_DETAIL ADD CONSTRAINT FK_TB_TRANSACTION_DETAIL_3 FOREIGN KEY (JURISDICTION_TAXRATE_ID) REFERENCES STSCORP.TB_JURISDICTION_TAXRATE(JURISDICTION_TAXRATE_ID);
ALTER TABLE STSCORP.TB_TRANSACTION_DETAIL ADD CONSTRAINT FK_TB_TRANSACTION_DETAIL_4 FOREIGN KEY (TAXCODE_DETAIL_ID) REFERENCES STSCORP.TB_TAXCODE_DETAIL(TAXCODE_DETAIL_ID);
ALTER TABLE STSCORP.TB_TRANSACTION_DETAIL ADD CONSTRAINT FK_TB_TRANSACTION_DETAIL_5 FOREIGN KEY (TAX_MATRIX_ID) REFERENCES STSCORP.TB_TAX_MATRIX(TAX_MATRIX_ID);
ALTER TABLE STSCORP.TB_TRANSACTION_DETAIL ADD CONSTRAINT FK_TB_TRANSACTION_DETAIL_6 FOREIGN KEY (LOCATION_MATRIX_ID) REFERENCES STSCORP.TB_LOCATION_MATRIX(LOCATION_MATRIX_ID);
--ALTER TABLE STSCORP.TB_TRANSACTION_DETAIL_EXCP ADD CONSTRAINT FK_TB_TRANSACTION_DETAIL_EXCP FOREIGN KEY (TRANSACTION_DETAIL_ID) REFERENCES STSCORP.TB_TRANSACTION_DETAIL(TRANSACTION_DETAIL_ID) ON DELETE CASCADE;


/**************************************************************************************************/
/**  Views  ***************************************************************************************/
/**************************************************************************************************/


--COMMIT;

--spool off
--set echo off
