---------------------------------------------------------------------------------------- 
--  Copyright (c) Lincoln Peak Partners (2008) All rights reserved 
    
--  Name         : createFunctions.sql

--  Description  : This file will create functions in the stscorp schema

--  Usage        : => \i createFunctions.sql

--  Notes        Run this script while connected as the stscorp user to the postdb1 DB
---------------------------------------------------------------------------------------- 


\i ./F_GET_KEY.sql
\i ./F_GL_EXTRACT.sql
\i ./F_SET_KEY.sql
\i ./F_SET_ROLE.sql
\i ./F_SP_RUN.sql
\i ./F_TP_EXTRACT.sql
\i ./INSTR.sql
\i ./ISNUMBER.sql
\i ./TRANSCUR.sql
