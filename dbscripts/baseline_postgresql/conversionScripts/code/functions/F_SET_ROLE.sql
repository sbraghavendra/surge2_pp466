CREATE OR REPLACE FUNCTION F_Set_Role(ps_input_string IN VARCHAR) RETURNS VARCHAR AS $$
--AUTHID CURRENT_USER
DECLARE
	current_role VARCHAR(2000) := 'FAIL';
BEGIN
	SELECT CURRENT_ROLE INTO STRICT current_role ;
	RAISE NOTICE 'current role = %', current_role;

	EXECUTE 'SET ROLE stsuser';
	--sys.dbms_session.set_role('STSUSER identified by '||ps_input_string);

	SELECT CURRENT_ROLE INTO STRICT current_role ;

	RETURN current_role;
END;
$$ LANGUAGE plpgsql;
