
CREATE OR REPLACE FUNCTION F_Set_Key (ps_input_string IN VARCHAR) RETURNS NUMERIC AS $$
--AUTHID definer
DECLARE

  vs_input_string 	VARCHAR(2000);
  vs_key_string   	VARCHAR(2000);
  vs_encrypted_string 	VARCHAR(2000);
  vs_temp_string 	VARCHAR(2000);
  vs_decrypted_string 	VARCHAR(2000);
  vs_stored		VARCHAR(100);
  vs_stored_stmt	VARCHAR(100);

  x	NUMERIC;
  i	NUMERIC;
  n1	NUMERIC;
  n2	NUMERIC;
  
  return_code	NUMERIC := -1;
  vi_cursor_id	INTEGER;
  vi_result	NUMERIC;

BEGIN
--  return_code := -1;

  vs_stored := 'STSCORP.TB_OPTION';

  n1 := 33;
  n2 := 126;

  FOR x IN n1 .. n2 LOOP
    IF MOD(x,25) = 0 THEN
       vs_key_string := vs_key_string || CHR(x);
    END IF;
  END LOOP;

  FOR x IN n1 .. n2 LOOP
    IF MOD(x,21) = 0 THEN
       vs_key_string := vs_key_string || CHR(x);
    END IF;
  END LOOP;

  FOR x IN n1 .. n2 LOOP
    IF MOD(x,13) = 0 THEN
       vs_key_string := vs_key_string || CHR(x);
    END IF;
  END LOOP;

  FOR x IN n1 .. n2 LOOP
    IF MOD(x,19) = 0 THEN
       vs_key_string := vs_key_string || CHR(x);
    END IF;
  END LOOP;

  FOR x IN n1 .. n2 LOOP
    IF MOD(x,17) = 0 THEN
       vs_key_string := vs_key_string || CHR(x);
    END IF;
  END LOOP;

  FOR x IN n1 .. n2 LOOP
    IF MOD(x,10) = 0 THEN
       vs_key_string := vs_key_string || CHR(x);
    END IF;
  END LOOP;

  FOR x IN n1 .. n2 LOOP
    IF MOD(x,27) = 0 THEN
       vs_key_string := vs_key_string || CHR(x);
    END IF;
  END LOOP;

  FOR x IN n1 .. n2 LOOP
    IF MOD(x,11) = 0 THEN
       vs_key_string := vs_key_string || CHR(x);
    END IF;
  END LOOP;

  FOR x IN n1 .. n2 LOOP
    IF MOD(x,29) = 0 THEN
       vs_key_string := vs_key_string || CHR(x);
    END IF;
  END LOOP;

  FOR x IN n1 .. n2 LOOP
    IF MOD(x,12) = 0 THEN
       vs_key_string := vs_key_string || CHR(x);
    END IF;
  END LOOP;

  FOR x IN n1 .. n2 LOOP
    IF MOD(x,15) = 0 THEN
       vs_key_string := vs_key_string || CHR(x);
    END IF;
  END LOOP;

  FOR x IN n1 .. n2 LOOP
    IF MOD(x,23) = 0 THEN
       vs_key_string := vs_key_string || CHR(x);
    END IF;
  END LOOP;

  FOR x IN n1 .. n2 LOOP
    IF MOD(x,31) = 0 THEN
       vs_key_string := vs_key_string || CHR(x);
    END IF;
  END LOOP;


  vs_encrypted_string := '';

  SELECT TRUNC(LENGTH(ps_input_string)/8) INTO i;

/*
  --DBMS_OUTPUT.Put_Line('SQLCODE line 113 is ' || sqlcode );

  FOR x IN 1 .. i LOOP

--     DBMS_OUTPUT.Put_Line('SUBSTR(ps_input_string,8*(i-1)+1,8) = ' || SUBSTR(ps_input_string,8*(i-1)+1,8) );

     dbms_obfuscation_toolkit.DESEncrypt(input_string => SUBSTR(ps_input_string,8*(i-1)+1,8), key_string => vs_key_string, encrypted_string => vs_temp_string );

     vs_encrypted_string := vs_encrypted_string||vs_temp_string||' ';

     vs_temp_string := '';

--     DBMS_OUTPUT.Put_Line('vs_encrypted_string = ' || vs_encrypted_string );

  END LOOP;

  DBMS_OUTPUT.Put_Line('SQLCODE line 129 is ' || sqlcode );

  IF MOD(LENGTH(ps_input_string),8) > 0 THEN

	 dbms_obfuscation_toolkit.DESEncrypt(input_string => RPAD(SUBSTR(ps_input_string,(8*i)+1,MOD(LENGTH(ps_input_string),8)),8,'|'), key_string => vs_key_string, encrypted_string => vs_temp_string );

     vs_encrypted_string := vs_encrypted_string||vs_temp_string||' ';

     vs_temp_string := '';

  END IF;
  DBMS_OUTPUT.Put_Line('SQLCODE line 140 is ' || sqlcode );
*/
  vs_stored_stmt := 'comment ';
  vs_stored_stmt := vs_stored_stmt||' on';
  vs_stored_stmt := vs_stored_stmt||' table';
  vs_stored_stmt := vs_stored_stmt||' '||vs_stored||' ';
  vs_stored_stmt := vs_stored_stmt||'i';
  vs_stored_stmt := vs_stored_stmt||'s';
  vs_stored_stmt := vs_stored_stmt||' ';
  --vs_stored_stmt := vs_stored_stmt||''''||vs_encrypted_string||'''';
  vs_stored_stmt := vs_stored_stmt||''''||ps_input_string||'''';

--  DBMS_OUTPUT.Put_Line('vs_stored_stmt = ' || vs_stored_stmt );

--  DBMS_OUTPUT.Put_Line('vs_encrypted_string = ' || vs_encrypted_string );

--  INSERT INTO TB_OPTION VALUES(vs_key_string, vs_encrypted_string);


  EXECUTE vs_stored_stmt;

/*
  DBMS_OUTPUT.Put_Line('SQLCODE line 159 is ' || sqlcode );

  vs_stored_stmt := 'alter role STSUSER identified by '||ps_input_string;
  DBMS_OUTPUT.Put_Line('SQLCODE line 162 is ' || sqlcode );
  DBMS_OUTPUT.Put_Line('vs_stored_stmt at 163 is ' || vs_stored_stmt );

  EXECUTE IMMEDIATE vs_stored_stmt;
  DBMS_OUTPUT.Put_Line('SQLCODE line 166 is ' || sqlcode );
*/

--  select encripted into vs_encrypted_string from test where key = vs_key_string;

--  SELECT comments INTO vs_encrypted_string FROM sys.user_tab_comments WHERE table_name = 'TB_OPTION';

--  DBMS_OUTPUT.Put_Line('encrypted = ' || vs_encrypted_string );

--  dbms_obfuscation_toolkit.DESDecrypt(input_string => vs_encrypted_string, key_string => vs_key_string,decrypted_string => vs_decrypted_string );

--  DBMS_OUTPUT.Put_Line('vs_encrypted_string = ' || vs_decrypted_string );
--  DBMS_OUTPUT.Put_Line('vs_key_string = ' || vs_key_string );


 return_code := 0;

 RETURN return_code;

--return_code := NVL(SQLCODE,0);

/*
EXCEPTION
WHEN OTHERS THEN
    IF (SQLCODE IS NULL) THEN return_code := 0;
        ELSE  return_code := SQLCODE;
    END IF;
   RETURN return_code;
*/

END;
$$ LANGUAGE plpgsql;

