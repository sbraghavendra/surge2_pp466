CREATE OR REPLACE FUNCTION F_SP_RUN (sp_name IN VARCHAR) RETURNS NUMERIC AS $$
DECLARE
   return_code            NUMERIC           := -1;
   proc_exist             NUMERIC           := 1;
   proc_command           VARCHAR(100)    := NULL;
-- Program starts
BEGIN

-- Determine if stored procedures exists
--   SELECT COUNT(*)
--   INTO proc_exist
--   FROM all_objects
--   WHERE owner = 'STSCORP'
--   AND object_type = 'PROCEDURE'
--   AND object_name = sp_name;

   -- If procedure exists, call it
   IF proc_exist > 0 THEN
      proc_command := 'SELECT ' || sp_name;
      EXECUTE proc_command;
   END IF;

   RETURN 0;

END;
$$ LANGUAGE plpgsql;
