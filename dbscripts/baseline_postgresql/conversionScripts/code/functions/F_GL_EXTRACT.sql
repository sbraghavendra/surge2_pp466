CREATE OR REPLACE FUNCTION F_GL_EXTRACT (
	where_clause	IN VARCHAR,
	file_name	IN VARCHAR,
	execution_mode	IN NUMERIC) RETURNS NUMERIC AS $$
DECLARE
   return_code            NUMERIC           := -1;
   proc_exist             NUMERIC           := 0;
   vn_gl_count            NUMERIC           := 0;
-- Program starts
BEGIN

   -- Determine if stored procedures exists
   SELECT	COUNT(*)
   INTO		STRICT proc_exist
   FROM		information_schema.routines
   WHERE	routine_schema = 'stscorp'
   AND		routine_type = 'FUNCTION'
   AND		routine_name = 'sp_gl_extract';

   -- If procedure exists, call it
   IF proc_exist > 0 THEN
      -- return_code returns 0 if success, or else returns SQLCODE
      EXECUTE sp_gl_extract(where_clause, file_name,execution_mode, return_code);
   END IF;

   RETURN return_code;
END;
$$ LANGUAGE plpgsql;
