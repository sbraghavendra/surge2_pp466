CREATE OR REPLACE function isNumber (param1 in varchar) RETURNS NUMERIC AS $$
DECLARE
	v_value numeric;
BEGIN
	v_value := param1;
	RETURN 1;
EXCEPTION
  WHEN OTHERS THEN
	RETURN 0;
END ;
$$ LANGUAGE plpgsql;
