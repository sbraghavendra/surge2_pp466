
CREATE OR REPLACE FUNCTION F_Get_Key() RETURNS VARCHAR AS $$
DECLARE

  vs_input_string 	VARCHAR(2000);
  vs_key_string   	VARCHAR(2000);
  vs_encrypted_string 	VARCHAR(2000);
  vs_decrypted_string 	VARCHAR(2000);
  vs_stored		VARCHAR(100);
  vs_stored_stmt	VARCHAR(100);
  vs_en_string		VARCHAR(100);
  vs_de_string		VARCHAR(100);

  v_oid                 oid;

  x  NUMERIC;
  n1 NUMERIC;
  n2 NUMERIC;
  i  NUMERIC;
  j  NUMERIC;
  k  NUMERIC;

BEGIN
  vs_stored := 'TB_OPTION';

  n1 := 33;
  n2 := 126;

  FOR x IN n1 .. n2 LOOP
    IF MOD(x,25) = 0 THEN
       vs_key_string := vs_key_string || CHR(x);
    END IF;
  END LOOP;

  FOR x IN n1 .. n2 LOOP
    IF MOD(x,21) = 0 THEN
       vs_key_string := vs_key_string || CHR(x);
    END IF;
  END LOOP;

  FOR x IN n1 .. n2 LOOP
    IF MOD(x,13) = 0 THEN
       vs_key_string := vs_key_string || CHR(x);
    END IF;
  END LOOP;

  FOR x IN n1 .. n2 LOOP
    IF MOD(x,19) = 0 THEN
       vs_key_string := vs_key_string || CHR(x);
    END IF;
  END LOOP;

  FOR x IN n1 .. n2 LOOP
    IF MOD(x,17) = 0 THEN
       vs_key_string := vs_key_string || CHR(x);
    END IF;
  END LOOP;

  FOR x IN n1 .. n2 LOOP
    IF MOD(x,10) = 0 THEN
       vs_key_string := vs_key_string || CHR(x);
    END IF;
  END LOOP;

  FOR x IN n1 .. n2 LOOP
    IF MOD(x,27) = 0 THEN
       vs_key_string := vs_key_string || CHR(x);
    END IF;
  END LOOP;

  FOR x IN n1 .. n2 LOOP
    IF MOD(x,11) = 0 THEN
       vs_key_string := vs_key_string || CHR(x);
    END IF;
  END LOOP;

  FOR x IN n1 .. n2 LOOP
    IF MOD(x,29) = 0 THEN
       vs_key_string := vs_key_string || CHR(x);
    END IF;
  END LOOP;

  FOR x IN n1 .. n2 LOOP
    IF MOD(x,12) = 0 THEN
       vs_key_string := vs_key_string || CHR(x);
    END IF;
  END LOOP;

  FOR x IN n1 .. n2 LOOP
    IF MOD(x,15) = 0 THEN
       vs_key_string := vs_key_string || CHR(x);
    END IF;
  END LOOP;

  FOR x IN n1 .. n2 LOOP
    IF MOD(x,23) = 0 THEN
       vs_key_string := vs_key_string || CHR(x);
    END IF;
  END LOOP;

  FOR x IN n1 .. n2 LOOP
    IF MOD(x,31) = 0 THEN
       vs_key_string := vs_key_string || CHR(x);
    END IF;
  END LOOP;

  vs_encrypted_string := NULL;

--  select encripted into vs_encrypted_string from test where key = vs_key_string;

  --SELECT comments INTO vs_encrypted_string FROM sys.user_tab_comments WHERE table_name = 'TB_OPTION';

  SELECT oid INTO v_oid FROM
  (
   SELECT	c.oid,
   n.nspname AS schemaname, 
   c.relname AS tablename
   FROM		pg_class c
   LEFT JOIN	pg_namespace n ON n.oid = c.relnamespace
   WHERE		c.relkind = 'r'::"char"
  ) a
  where tablename  = 'tb_option'
  and   schemaname = 'stscorp';

  select obj_description(v_oid, 'pg_class') INTO vs_de_string;

  /*
  i := 1;
  k := 1;
  j := 0;

  LOOP
    vs_en_string := '';
    i := INSTR(vs_encrypted_string,' ',i,k);
	EXIT WHEN i = 0;
    vs_en_string := SUBSTR(vs_encrypted_string,j+1,i-j-1);
	j := i;
    dbms_obfuscation_toolkit.DESDecrypt(input_string => vs_en_string, key_string => vs_key_string,decrypted_string => vs_decrypted_string );
    vs_de_string := vs_de_string||vs_decrypted_string;
	vs_decrypted_string := '';
    k := k + 1;
  END LOOP;

  vs_de_string := RTRIM(vs_de_string,'|');
  */
  RETURN vs_de_string;

  --COMMIT;

END;
$$ LANGUAGE plpgsql;

