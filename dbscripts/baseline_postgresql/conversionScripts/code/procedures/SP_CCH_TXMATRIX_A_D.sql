CREATE OR REPLACE FUNCTION sp_cch_txmatrix_a_d(av_groupcode IN varchar) RETURNS VOID AS $$
-- ****** Program starts ****** --------------------------------------------------------------------
BEGIN
   DELETE
     FROM tb_cch_txmatrix
    WHERE rectype = 'I'
      AND groupcode = av_groupcode;
END;
$$ LANGUAGE plpgsql;
