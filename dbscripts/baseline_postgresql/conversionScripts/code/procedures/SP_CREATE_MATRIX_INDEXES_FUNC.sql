CREATE OR REPLACE FUNCTION sp_create_matrix_indexes_func() RETURNS VOID AS $$
DECLARE
-- Program defined variables
   vc_statement                    VARCHAR(1000)                            := '';
   vc_index_tablespace             varchar(30)                              := '';
   vi_rows_processed               INTEGER                                  := 0;
   vi_cursor_id                    INTEGER                                  := 0;

   tax_driver                      RECORD;

-- Program starts **********************************************************************************
BEGIN
   -- Get Index Tablespace name from preferences/options table
   BEGIN
      SELECT value
        INTO STRICT vc_index_tablespace
        FROM tb_option
       WHERE option_code = 'INDEX_TABLESPACE'
         AND option_type_code = 'ADMIN'
         AND user_code = 'ADMIN';
   EXCEPTION
      WHEN NO_DATA_FOUND THEN
         vc_index_tablespace := 'indx';
   END;
   
   vc_index_tablespace := TRIM(vc_index_tablespace);
   IF vc_index_tablespace IS NULL OR vc_index_tablespace = '' THEN
      vc_index_tablespace := 'indx';
   END IF;

   -- Create and execute sql to build index for each driver
   FOR tax_driver IN
	SELECT driver.trans_dtl_column_name, driver.matrix_column_name
         FROM TB_DRIVER_NAMES driver
         WHERE driver.driver_names_code = 'T'
         ORDER BY driver.driver_id
   LOOP
      -- Create dynamic SQL
      vc_statement := 'CREATE INDEX idx_tb_tax_matrix_' || tax_driver.matrix_column_name ||
                      '_UP ON tb_tax_matrix (UPPER(' || tax_driver.matrix_column_name ||
                      ')) tablespace ' || vc_index_tablespace;

      -- Execute dynamic SQL
      EXECUTE vc_statement;
   END LOOP;

   COMMIT;
END;
$$ LANGUAGE plpgsql;
