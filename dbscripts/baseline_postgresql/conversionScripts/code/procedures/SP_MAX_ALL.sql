CREATE OR REPLACE FUNCTION sp_max_all() RETURNS VOID AS $$
-- Program starts **********************************************************************************
BEGIN
   EXECUTE sp_max_tb_allocation_matrix_id();
   EXECUTE sp_max_gl_extract_batch_id();
   EXECUTE sp_max_tb_batch_error_log_id();
   EXECUTE sp_max_tb_batch_id();
   EXECUTE sp_max_tb_entity_id();
   EXECUTE sp_max_tb_jurisdiction_id();
   EXECUTE sp_max_tb_juris_taxrate_id();
   EXECUTE sp_max_tb_location_matrix_id();
   EXECUTE sp_max_tb_questans_log_id();
   EXECUTE sp_max_tb_taxcode_detail_id();
   EXECUTE sp_max_tb_tax_matrix_id();
   EXECUTE sp_max_tb_trans_detail_id();
   EXECUTE sp_max_tb_trans_detail_excp_id();
END;
$$ LANGUAGE plpgsql;
