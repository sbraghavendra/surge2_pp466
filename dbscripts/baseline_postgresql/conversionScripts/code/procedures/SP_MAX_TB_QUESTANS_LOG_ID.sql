CREATE OR REPLACE FUNCTION sp_max_tb_questans_log_id() RETURNS VOID AS $$
DECLARE
   v_max_id numeric;
   v_count numeric;
BEGIN
   SELECT MAX( questionanswer_log_id )
   INTO STRICT v_max_id
   FROM tb_questionanswer_log;

   IF v_max_id IS NULL THEN
      v_max_id := 1;
   ELSE
      v_max_id := v_max_id + 1;
   END IF;

   SELECT count(*)
   INTO STRICT v_count
   FROM information_schema.sequences
   WHERE sequence_name = 'sq_tb_questionanswer_log_id'
   AND sequence_schema = 'stscorp';

   IF v_count > 0 THEN
      EXECUTE 'DROP SEQUENCE stscorp.sq_tb_questionanswer_log_id';
   END IF;

   EXECUTE 'CREATE SEQUENCE stscorp.sq_tb_questionanswer_log_id INCREMENT BY 1 START WITH 1 NO MAXVALUE MINVALUE 1 NO CYCLE';

   EXECUTE 'GRANT SELECT ON STSCORP.sq_tb_questionanswer_log_id TO STSUSER';
END;
$$ LANGUAGE plpgsql;
