CREATE OR REPLACE FUNCTION sp_batch_archive(an_batch_id numeric) RETURNS VOID AS $$
DECLARE

-- Update Transaction Detail SQL
   vc_trans_dtl_update             VARCHAR(1000)                                    :=
      'UPDATE tb_transaction_detail ' ||
         'SET archive_batch_no = ' || quote_literal(an_batch_id)|| ' ';

   vc_trans_dtl_where              VARCHAR(2000)                                    :=
      'WHERE archive_batch_no IS NULL AND gl_extract_flag = 1';
   
   vc_trans_dtl_stmt               VARCHAR(3000)                                    := NULL;
   

-- Table defined variables
   v_batch_status_code             tb_batch.batch_status_code%TYPE                   := NULL;
   v_error_sev_code                tb_batch.error_sev_code%TYPE                      := ' ';
   v_severity_level                tb_list_code.severity_level%TYPE                  := ' ';
   v_write_import_line_flag        tb_list_code.write_import_line_flag%TYPE          := '1';
   v_abort_import_flag             tb_list_code.abort_import_flag%TYPE               := '0';
   v_business_unit                 tb_batch.vc01%TYPE                                := NULL;
   v_trans_detail_column_name      tb_batch.vc02%TYPE                                := NULL;
   v_from_timestamp                tb_batch.ts01%TYPE                                := NULL;
   v_to_timestamp                  tb_batch.ts02%TYPE                                := NULL;
   v_sysdate                       tb_batch.actual_start_timestamp%TYPE              := CURRENT_DATE;

-- Program defined variables
   vi_error_count                  INTEGER                                           := 0;
   vi_cursor_id                    INTEGER                                           := 0;
   vi_rows_processed               INTEGER                                           := 0;

-- Program starts ******************************************************************************************
BEGIN
   -- Confirm batch exists and is flagged for processing
   BEGIN
      SELECT batch_status_code
        INTO STRICT v_batch_status_code
        FROM tb_batch
       WHERE batch_id = an_batch_id;
      
      IF v_batch_status_code != 'FA' THEN
         vi_error_count := vi_error_count + 1;
         EXECUTE sp_geterrorcode('A9', an_batch_id, 'A', v_sysdate, v_severity_level, v_write_import_line_flag, v_abort_import_flag);
         IF v_severity_level > v_error_sev_code THEN
            v_error_sev_code := v_severity_level;
         END IF;
      END IF;
   EXCEPTION
      WHEN OTHERS THEN
         vi_error_count := vi_error_count + 1;
         EXECUTE sp_geterrorcode('A8', an_batch_id, 'A', v_sysdate, v_severity_level, v_write_import_line_flag, v_abort_import_flag);
         IF v_severity_level > v_error_sev_code THEN
            v_error_sev_code := v_severity_level;
         END IF;
   END;
   -- Abort Procedure?
   IF v_abort_import_flag = '1' THEN
      v_batch_status_code := 'ABA';
      RAISE EXCEPTION 'Abort Procedure: %', v_batch_status_code;
   END IF;

   -- Get Batch information
   SELECT VC01,
          VC02,
          TS01,
          TS02
     INTO STRICT 
	  v_trans_detail_column_name,
          v_business_unit,
          v_from_timestamp,
          v_to_timestamp
     FROM tb_batch
    WHERE batch_id = an_batch_id;

   -- Update batch as processing
   BEGIN
      UPDATE tb_batch
         SET batch_status_code = 'XA',
             error_sev_code = '',
             actual_start_timestamp = CURRENT_DATE
       WHERE batch_id = an_batch_id;
   EXCEPTION
      WHEN OTHERS THEN
         vi_error_count := vi_error_count + 1;
         EXECUTE sp_geterrorcode('A1', an_batch_id, 'A', v_sysdate, v_severity_level, v_write_import_line_flag, v_abort_import_flag);
         IF v_severity_level > v_error_sev_code THEN
            v_error_sev_code := v_severity_level;
         END IF;
   END;
   -- Abort Procedure?
   IF v_abort_import_flag = '1' THEN
      v_batch_status_code := 'ABA';
      RAISE EXCEPTION 'Abort Procedure: %', v_batch_status_code;
   END IF;

   --COMMIT;

   /**  Create Where Clause and construct Dynamic SQL Statement  **********************************/
   -- Business Unit
   IF v_business_unit IS NOT NULL AND v_trans_detail_column_name IS NOT NULL THEN
      IF vc_trans_dtl_where IS NULL THEN
         vc_trans_dtl_where := 'WHERE ';
      ELSE
         vc_trans_dtl_where := vc_trans_dtl_where || ' AND ';
      END IF;
      vc_trans_dtl_where := vc_trans_dtl_where || v_trans_detail_column_name || ' = ''' || v_business_unit || '''';
   END IF;
   -- From G/L Date
   IF v_from_timestamp IS NOT NULL THEN
      IF vc_trans_dtl_where IS NULL THEN
         vc_trans_dtl_where := 'WHERE ';
      ELSE
         vc_trans_dtl_where := vc_trans_dtl_where || ' AND ';
      END IF;
      vc_trans_dtl_where := vc_trans_dtl_where || 'gl_date >= to_date(''' || to_char(v_from_timestamp,'mm/dd/yyyy') || ''',''mm/dd/yyyy'')';
   END IF;
   -- To G/L Date
   IF v_to_timestamp IS NOT NULL THEN
      IF vc_trans_dtl_where IS NULL THEN
         vc_trans_dtl_where := 'WHERE ';
      ELSE
         vc_trans_dtl_where := vc_trans_dtl_where || ' AND ';
      END IF;
      vc_trans_dtl_where := vc_trans_dtl_where || 'gl_date <= to_date(''' || to_char(v_to_timestamp,'mm/dd/yyyy') || ''',''mm/dd/yyyy'')';
   END IF;
   -- Build Statement
   vc_trans_dtl_stmt := vc_trans_dtl_update || vc_trans_dtl_where;

   /**  Update Archived Rows with Archive Batch No.  **********************************************/
   EXECUTE vc_trans_dtl_stmt;
   --COMMIT;

   /**  Copy Archive Batch from Transaction Detail table to Archive Detail table  *****************/
   INSERT INTO tb_archive_transaction_dtl
      SELECT *
        FROM tb_transaction_detail
       WHERE archive_batch_no = an_batch_id;

   /**  Delete Archive Batch from Transaction Detail table  ***************************************/
   DELETE FROM tb_transaction_detail
    WHERE archive_batch_no = an_batch_id;

   /**  Update Batch file  ************************************************************************/
   UPDATE tb_batch
      SET batch_status_code = 'A',
          total_rows = vi_rows_processed,
          processed_rows = vi_rows_processed,
          actual_end_timestamp = CURRENT_DATE
    WHERE batch_id = an_batch_id;

    --COMMIT;

EXCEPTION
  WHEN OTHERS THEN
    -- If Procedure Aborted...
    IF (v_batch_status_code = 'ABA') THEN
      -- Update batch with error codes
      UPDATE tb_batch
         SET batch_status_code = v_batch_status_code,
             error_sev_code = v_error_sev_code
       WHERE batch_id = an_batch_id;
      --COMMIT;
    ELSE
      -- Update batch with something else
      UPDATE tb_batch
         SET batch_status_code = 'A?',
             error_sev_code = 99
       WHERE batch_id = an_batch_id;
      --COMMIT;
    END IF;
END;
$$ LANGUAGE plpgsql;
