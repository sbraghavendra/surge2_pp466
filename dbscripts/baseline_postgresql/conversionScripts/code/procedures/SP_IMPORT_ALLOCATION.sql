CREATE OR REPLACE FUNCTION sp_import_allocation(an_batch_id numeric) RETURNS VOID AS $$
DECLARE
-- Moved Allocations to the tb_batch_process stored procedure -- 01/25/2007 -- MBF01
-- Corrected Rounded Correction -- 02/09/2007 -- MBF02
-- Add Allocation Rows to Total Imported Rows -- 02/13/2007 -- MBF03

-- Allocation Matrix Selection SQL
   vc_allocation_matrix_select       VARCHAR(4000) :=
      'SELECT tb_allocation_matrix.allocation_matrix_id ' ||
       'FROM tb_allocation_matrix, ' ||
            'tb_bcp_transactions ' ||
      'WHERE ( tb_bcp_transactions.oid = (SELECT CAST (param_value AS OID) FROM sp_import_allocation_tmp WHERE param_name = ''v_oid'') ) AND ' ||
            '( tb_allocation_matrix.effective_date <= tb_bcp_transactions.gl_date ) AND ( tb_allocation_matrix.expiration_date >= tb_bcp_transactions.gl_date ) ';
   vc_allocation_matrix_where        VARCHAR(4000) := '';
   vc_allocation_matrix_orderby      VARCHAR(1000) :=
      'ORDER BY tb_allocation_matrix.binary_weight DESC, tb_allocation_matrix.significant_digits DESC, tb_allocation_matrix.effective_date DESC';
   vc_allocation_matrix_stmt         VARCHAR (9000);
   allocation_matrix_cursor          REFCURSOR;
   allocation_matrix                 RECORD;

-- Table defined variables
   v_value                         tb_option.value%TYPE                              := NULL;
   v_batch_status_code             tb_batch.batch_status_code%TYPE                   := 'P';
   v_error_sev_code                tb_batch.error_sev_code%TYPE                      := ' ';
   v_total_rows                    tb_batch.total_rows%TYPE                          := 0;
   v_severity_level                tb_list_code.severity_level%TYPE                  := ' ';
   v_write_import_line_flag        tb_list_code.write_import_line_flag%TYPE          := '1';
   v_abort_import_flag             tb_list_code.abort_import_flag%TYPE               := '0';
   v_allocation_matrix_id          tb_allocation_matrix.allocation_matrix_id%TYPE    := 0;
   v_transaction_detail_id         tb_bcp_transactions.transaction_detail_id%TYPE    := 0;
   v_sysdate                       tb_bcp_transactions.load_timestamp%TYPE           := CURRENT_TIMESTAMP;
   v_accum_allocation              tb_bcp_transactions.gl_line_itm_dist_amt%TYPE     := 0;
   v_gl_line_itm_dist_amt          tb_bcp_transactions.gl_line_itm_dist_amt%TYPE     := 0;
   v_orig_gl_line_itm_dist_amt     tb_bcp_transactions.gl_line_itm_dist_amt%TYPE     := 0;
   v_transaction_state_code        tb_bcp_transactions.transaction_state_code%TYPE   := NULL;
   v_delta                         tb_bcp_transactions.gl_line_itm_dist_amt%TYPE     := 0;

-- Program defined variables
   v_cnt                           INTEGER					   := 0;
   v_oid                           OID                                             := NULL;
   vn_fetch_rows                   NUMERIC                                         := 0;
   vn_allocation_total_rows        NUMERIC                                         := 0;
   vi_error_count                  INTEGER                                         := 0;

-- Define Tax Driver Names Cursor (tb_driver_names)
   tax_driver_cursor CURSOR 
   FOR
        SELECT driver.trans_dtl_column_name, driver.matrix_column_name, driver.null_driver_flag, driver.wildcard_flag,
               datadefcol.desc_column_name, datadefcol.data_type
          FROM tb_driver_names driver,
               (SELECT *
                  FROM tb_data_def_column
                 WHERE table_name = 'TB_TRANSACTION_DETAIL') datadefcol

         WHERE driver.driver_names_code = 'T' AND
               driver.trans_dtl_column_name = datadefcol.column_name
      ORDER BY driver.driver_id;
   tax_driver                   RECORD;

-- Define Allocation Matrix Detail / Jurisdiction Cursor (tb_allocation_matrix_detail, tb_jurisdiction)
   allocation_detail_cursor CURSOR 
   FOR
        SELECT tb_allocation_matrix_detail.jurisdiction_id, tb_allocation_matrix_detail.allocation_percent,
               tb_jurisdiction.geocode, tb_jurisdiction.state, tb_jurisdiction.county, tb_jurisdiction.city, tb_jurisdiction.zip
          FROM tb_allocation_matrix_detail
	  LEFT OUTER JOIN tb_jurisdiction ON ( tb_allocation_matrix_detail.jurisdiction_id = tb_jurisdiction.jurisdiction_id )
         WHERE tb_allocation_matrix_detail.allocation_matrix_id = v_allocation_matrix_id ;
   allocation_detail                   RECORD;

-- Define BCP Transactions Cursor (tb_bcp_transactions)
   bcp_transactions_cursor CURSOR 
   FOR
      SELECT oid,
             transaction_detail_id,
             source_transaction_id,
             process_batch_no,
             gl_extract_batch_no,
             archive_batch_no,
             allocation_matrix_id,
             allocation_subtrans_id,
             entered_date,
             transaction_status,
             gl_date,
             gl_company_nbr,
             gl_company_name,
             gl_division_nbr,
             gl_division_name,
             gl_cc_nbr_dept_id,
             gl_cc_nbr_dept_name,
             gl_local_acct_nbr,
             gl_local_acct_name,
             gl_local_sub_acct_nbr,
             gl_local_sub_acct_name,
             gl_full_acct_nbr,
             gl_full_acct_name,
             gl_line_itm_dist_amt,
             orig_gl_line_itm_dist_amt,
             vendor_nbr,
             vendor_name,
             vendor_address_line_1,
             vendor_address_line_2,
             vendor_address_line_3,
             vendor_address_line_4,
             vendor_address_city,
             vendor_address_county,
             vendor_address_state,
             vendor_address_zip,
             vendor_address_country,
             vendor_type,
             vendor_type_name,
             invoice_nbr,
             invoice_desc,
             invoice_date,
             invoice_freight_amt,
             invoice_discount_amt,
             invoice_tax_amt,
             invoice_total_amt,
             invoice_tax_flg,
             invoice_line_nbr,
             invoice_line_name,
             invoice_line_type,
             invoice_line_type_name,
             invoice_line_amt,
             invoice_line_tax,
             afe_project_nbr,
             afe_project_name,
             afe_category_nbr,
             afe_category_name,
             afe_sub_cat_nbr,
             afe_sub_cat_name,
             afe_use,
             afe_contract_type,
             afe_contract_structure,
             afe_property_cat,
             inventory_nbr,
             inventory_name,
             inventory_class,
             inventory_class_name,
             po_nbr,
             po_name,
             po_date,
             po_line_nbr,
             po_line_name,
             po_line_type,
             po_line_type_name,
             ship_to_location,
             ship_to_location_name,
             ship_to_address_line_1,
             ship_to_address_line_2,
             ship_to_address_line_3,
             ship_to_address_line_4,
             ship_to_address_city,
             ship_to_address_county,
             ship_to_address_state,
             ship_to_address_zip,
             ship_to_address_country,
             wo_nbr,
             wo_name,
             wo_date,
             wo_type,
             wo_type_desc,
             wo_class,
             wo_class_desc,
             wo_entity,
             wo_entity_desc,
             wo_line_nbr,
             wo_line_name,
             wo_line_type,
             wo_line_type_desc,
             wo_shut_down_cd,
             wo_shut_down_cd_desc,
             voucher_id,
             voucher_name,
             voucher_date,
             voucher_line_nbr,
             voucher_line_desc,
             check_nbr,
             check_no,
             check_date,
             check_amt,
             check_desc,
             user_text_01,
             user_text_02,
             user_text_03,
             user_text_04,
             user_text_05,
             user_text_06,
             user_text_07,
             user_text_08,
             user_text_09,
             user_text_10,
             user_text_11,
             user_text_12,
             user_text_13,
             user_text_14,
             user_text_15,
             user_text_16,
             user_text_17,
             user_text_18,
             user_text_19,
             user_text_20,
             user_text_21,
             user_text_22,
             user_text_23,
             user_text_24,
             user_text_25,
             user_text_26,
             user_text_27,
             user_text_28,
             user_text_29,
             user_text_30,
             user_number_01,
             user_number_02,
             user_number_03,
             user_number_04,
             user_number_05,
             user_number_06,
             user_number_07,
             user_number_08,
             user_number_09,
             user_number_10,
             user_date_01,
             user_date_02,
             user_date_03,
             user_date_04,
             user_date_05,
             user_date_06,
             user_date_07,
             user_date_08,
             user_date_09,
             user_date_10,
             comments,
             tb_calc_tax_amt,
             state_use_amount,
             state_use_tier2_amount,
             state_use_tier3_amount,
             county_use_amount,
             county_local_use_amount,
             city_use_amount,
             city_local_use_amount,
             transaction_state_code,
             auto_transaction_state_code,
             transaction_ind,
             suspend_ind,
             taxcode_detail_id,
             taxcode_state_code,
             taxcode_type_code,
             taxcode_code,
             cch_taxcat_code,
             cch_group_code,
             cch_item_code,
             manual_taxcode_ind,
             tax_matrix_id,
             location_matrix_id,
             jurisdiction_id,
             jurisdiction_taxrate_id,
             manual_jurisdiction_ind,
             measure_type_code,
             state_use_rate,
             state_use_tier2_rate,
             state_use_tier3_rate,
             state_split_amount,
             state_tier2_min_amount,
             state_tier2_max_amount,
             state_maxtax_amount,
             county_use_rate,
             county_local_use_rate,
             county_split_amount,
             county_maxtax_amount,
             county_single_flag,
             county_default_flag,
             city_use_rate,
             city_local_use_rate,
             city_split_amount,
             city_split_use_rate,
             city_single_flag,
             city_default_flag,
             combined_use_rate,
             load_timestamp,
             gl_extract_updater,
             gl_extract_timestamp,
             gl_extract_flag,
             gl_log_flag,
             gl_extract_amt,
             audit_flag,
             audit_user_id,
             audit_timestamp,
             modify_user_id,
             modify_timestamp,
             update_user_id,
             update_timestamp
        FROM tb_bcp_transactions
       WHERE process_batch_no = an_batch_id;
   bcp_transactions             RECORD;

-- ****** Program starts ****** --------------------------------------------------------------------
BEGIN
   -- Confirm allocations are active
   BEGIN
      SELECT value
        INTO STRICT v_value
        FROM tb_option
       WHERE option_code = 'ALLOCATIONSENABLED'
         AND option_type_code = 'ADMIN'
         AND user_code = 'ADMIN';
      
      IF v_value <> '1' THEN
         RAISE EXCEPTION 'Wrong Data';
      END IF;
   EXCEPTION
      WHEN OTHERS THEN
         --insert into tb_debug(row_joe) values('STOP');
         RAISE EXCEPTION 'Abort Procedure';
   END;

   -- Confirm batch exists and is being imported
   -- Confirm batch exists and is flagged for process -- 01/25/2007 -- MBF01
   BEGIN
      SELECT batch_status_code, total_rows
        INTO STRICT v_batch_status_code, v_total_rows
        FROM tb_batch
       WHERE batch_id = an_batch_id;
      
      IF v_batch_status_code != 'FP' THEN
         vi_error_count := vi_error_count + 1;
         EXECUTE sp_geterrorcode('AL2', an_batch_id, 'AL', v_sysdate, v_severity_level, v_write_import_line_flag, v_abort_import_flag);
         IF v_severity_level > v_error_sev_code THEN
            v_error_sev_code := v_severity_level;
         END IF;
      END IF;
   EXCEPTION
      WHEN OTHERS THEN
         vi_error_count := vi_error_count + 1;
         EXECUTE sp_geterrorcode('AL1', an_batch_id, 'AL', v_sysdate, v_severity_level, v_write_import_line_flag, v_abort_import_flag);
         IF v_severity_level > v_error_sev_code THEN
            v_error_sev_code := v_severity_level;
         END IF;
   END;
   -- Abort Procedure?
   IF v_abort_import_flag = '1' THEN
      v_batch_status_code := 'ABAL';
      RAISE EXCEPTION 'Abort Procedure: %', v_batch_status_code;
   END IF;

   -- Update batch
   BEGIN
      UPDATE tb_batch
         SET batch_status_code = 'XAL',
             error_sev_code = '',
             nu04 = v_total_rows
       WHERE batch_id = an_batch_id;
   EXCEPTION
      WHEN OTHERS THEN
         vi_error_count := vi_error_count + 1;
         EXECUTE sp_geterrorcode('AL3', an_batch_id, 'P', v_sysdate, v_severity_level, v_write_import_line_flag, v_abort_import_flag);
         IF v_severity_level > v_error_sev_code THEN
            v_error_sev_code := v_severity_level;
         END IF;
   END;
   -- Abort Procedure?
   IF v_abort_import_flag = '1' THEN
      v_batch_status_code := 'ABAL';
      RAISE EXCEPTION 'Abort Procedure: %', v_batch_status_code;
   END IF;

   -- ***** Create Dynamic WHERE Clause for Matrix SQLs ***** --
   -- Allocation Matrix Drivers
   OPEN tax_driver_cursor;
   LOOP
      FETCH tax_driver_cursor INTO tax_driver;
      
      IF (NOT FOUND) THEN 
	EXIT;
      END IF;

      IF tax_driver.null_driver_flag = '1' THEN
         IF tax_driver.wildcard_flag = '1' THEN
            IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
               UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                  vc_allocation_matrix_where := vc_allocation_matrix_where || 'AND ' ||
                  '((tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ' IS NULL AND (tb_allocation_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR tb_allocation_matrix.' || tax_driver.matrix_column_name || ' = ''*NULL'' )) OR ' ||
                  '(tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND (tb_allocation_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ' LIKE tb_allocation_matrix.' || tax_driver.matrix_column_name || '))) ';
            END IF;
         ELSE
            IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
               UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                  vc_allocation_matrix_where := vc_allocation_matrix_where || 'AND ' ||
                  '((tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ' IS NULL AND (tb_allocation_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR tb_allocation_matrix.' || tax_driver.matrix_column_name || ' = ''*NULL'' )) OR ' ||
                  '(tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND (tb_allocation_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ' = tb_allocation_matrix.' || tax_driver.matrix_column_name || '))) ';
            END IF;
         END IF;
      ELSE
         IF tax_driver.wildcard_flag = '1' THEN
            IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
               UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                  vc_allocation_matrix_where := vc_allocation_matrix_where || 'AND ' ||
                  '(tb_allocation_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ' LIKE tb_allocation_matrix.' || tax_driver.matrix_column_name || ') ';
            END IF;
         ELSE
            IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
               UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                  vc_allocation_matrix_where := vc_allocation_matrix_where || 'AND ' ||
                  '(tb_allocation_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ' = tb_allocation_matrix.' || tax_driver.matrix_column_name || ') ';
            END IF;
         END IF;
      END IF;
   END LOOP;
   CLOSE tax_driver_cursor;

   -- If no drivers found raise error, else create location matrix sql statement
   IF vc_allocation_matrix_where IS NULL THEN
      vi_error_count := vi_error_count + 1;
      EXECUTE sp_geterrorcode('AL3', an_batch_id, 'AL', v_sysdate, v_severity_level, v_write_import_line_flag, v_abort_import_flag);
      IF v_severity_level > v_error_sev_code THEN
         v_error_sev_code := v_severity_level;
      END IF;
   ELSE
      vc_allocation_matrix_stmt := vc_allocation_matrix_select || vc_allocation_matrix_where || vc_allocation_matrix_orderby;
   END IF;
   -- Abort Procedure?
   IF v_abort_import_flag = '1' THEN
      v_batch_status_code := 'ABAL';
      RAISE EXCEPTION 'Abort Procedure: %', v_batch_status_code;
   END IF;

   -- Clear large rollback segment
   --COMMIT;

   -- ****** CREATE Temporary Table to hold "Bind" Values ****** -----------------------------------------
   BEGIN
    EXECUTE 'CREATE TEMPORARY TABLE sp_import_allocation_tmp (param_name varchar(30), param_value varchar(30))';
   EXCEPTION 
     -- Table may already exist if using Connection/Session Pooling, Ignore
     WHEN OTHERS THEN NULL;
   END;
   
   -- ****** Read and Process Transactions ****** -----------------------------------------
   OPEN bcp_transactions_cursor;
   LOOP
      FETCH bcp_transactions_cursor INTO bcp_transactions;
      
      IF (NOT FOUND) THEN 
	EXIT;
      END IF;
      
      v_write_import_line_flag := '1';
      v_oid := bcp_transactions.oid;

      -- Search Allocation Matrix for matches
      BEGIN
	 -- Prep TMP table with values
	 DELETE FROM sp_import_allocation_tmp;
	 INSERT INTO sp_import_allocation_tmp(param_name, param_value) VALUES ('v_oid', v_oid);

	 OPEN allocation_matrix_cursor
           FOR EXECUTE vc_allocation_matrix_stmt;
         
	 FETCH allocation_matrix_cursor
          INTO allocation_matrix;

	 -- Rows found?
         IF (FOUND) THEN
            vn_fetch_rows := 1;
         ELSE
            vn_fetch_rows := 0;
         END IF;
         CLOSE allocation_matrix_cursor;

      EXCEPTION
         WHEN OTHERS THEN
            vn_fetch_rows := 0;
            vi_error_count := vi_error_count + 1;
            EXECUTE sp_geterrorcode('AL4', an_batch_id, 'AL', v_sysdate, v_severity_level, v_write_import_line_flag, v_abort_import_flag);
            IF v_severity_level > v_error_sev_code THEN
               v_error_sev_code := v_severity_level;
            END IF;
      END;
      -- Exit if aborted
      IF v_abort_import_flag = '1' THEN
         v_batch_status_code := 'ABAL';
         RAISE EXCEPTION 'Abort Procedure: %', v_batch_status_code;
      END IF;

      -- Allocation Matrix Line Found
      IF vn_fetch_rows > 0 THEN
         -- Get and create allocations --
         v_transaction_detail_id := bcp_transactions.transaction_detail_id;
         v_allocation_matrix_id := allocation_matrix.allocation_matrix_id;
         v_transaction_state_code := bcp_transactions.transaction_state_code;
         IF v_transaction_state_code IS NULL THEN
            v_transaction_state_code := '*NULL';
         END IF;
         v_orig_gl_line_itm_dist_amt := bcp_transactions.gl_line_itm_dist_amt;
         v_accum_allocation := 0;
         v_delta := 0;

         -- Write lines?
         IF v_write_import_line_flag = '1' THEN
            bcp_transactions.allocation_matrix_id := v_allocation_matrix_id;

            -- Get next allocation sub-trans ID for the new transaction detail records
            SELECT NEXTVAL('sq_tb_allocation_subtrans_id')
              INTO STRICT bcp_transactions.allocation_subtrans_id
              ;

            -- Get number of Allocation Detail lines
            vn_allocation_total_rows := 0;
            SELECT count(*)
              INTO STRICT vn_allocation_total_rows
              FROM tb_allocation_matrix_detail
             WHERE tb_allocation_matrix_detail.allocation_matrix_id = v_allocation_matrix_id;

            IF vn_allocation_total_rows > 0 THEN
               -- Execute Cursor to get allocations
               v_cnt := 0;
	       OPEN allocation_detail_cursor;
               LOOP
		  v_cnt := v_cnt + 1;
		  
		  FETCH allocation_detail_cursor INTO allocation_detail;
                  
                  IF (NOT FOUND) THEN 
                    EXIT;
                  END IF;

                  -- Add allocation rows to total imported rows -- 3361 -- 02/13/2007 -- MBF03
                  v_total_rows := v_total_rows + 1;

                  -- Fill other fields from allocation matrix detail
                  bcp_transactions.auto_transaction_state_code := v_transaction_state_code;
                  bcp_transactions.jurisdiction_id := allocation_detail.jurisdiction_id;
                  bcp_transactions.transaction_state_code := allocation_detail.state;

                  v_gl_line_itm_dist_amt := Round(v_orig_gl_line_itm_dist_amt * allocation_detail.allocation_percent,2);
                  v_accum_allocation := v_accum_allocation + v_gl_line_itm_dist_amt;

                  bcp_transactions.orig_gl_line_itm_dist_amt := v_orig_gl_line_itm_dist_amt;
                  bcp_transactions.gl_line_itm_dist_amt := v_gl_line_itm_dist_amt;

                  -- Check for last allocation and rounding -- 3361 -- 02/09/2007 -- MBF02
                  IF v_cnt = vn_allocation_total_rows THEN
                     -- Check rounding
                     v_delta := v_orig_gl_line_itm_dist_amt - v_accum_allocation;
                     IF v_delta <> 0 THEN
                        bcp_transactions.gl_line_itm_dist_amt := bcp_transactions.gl_line_itm_dist_amt + v_delta;
                     END IF;
                  END IF;

                  -- Insert transaction detail row
                  BEGIN
                     INSERT INTO tb_bcp_transactions (
                        transaction_detail_id,
                        source_transaction_id,
                        process_batch_no,
                        gl_extract_batch_no,
                        archive_batch_no,
                        allocation_matrix_id,
                        allocation_subtrans_id,
                        entered_date,
                        transaction_status,
                        gl_date,
                        gl_company_nbr,
                        gl_company_name,
                        gl_division_nbr,
                        gl_division_name,
                        gl_cc_nbr_dept_id,
                        gl_cc_nbr_dept_name,
                        gl_local_acct_nbr,
                        gl_local_acct_name,
                        gl_local_sub_acct_nbr,
                        gl_local_sub_acct_name,
                        gl_full_acct_nbr,
                        gl_full_acct_name,
                        gl_line_itm_dist_amt,
                        orig_gl_line_itm_dist_amt,
                        vendor_nbr,
                        vendor_name,
                        vendor_address_line_1,
                        vendor_address_line_2,
                        vendor_address_line_3,
                        vendor_address_line_4,
                        vendor_address_city,
                        vendor_address_county,
                        vendor_address_state,
                        vendor_address_zip,
                        vendor_address_country,
                        vendor_type,
                        vendor_type_name,
                        invoice_nbr,
                        invoice_desc,
                        invoice_date,
                        invoice_freight_amt,
                        invoice_discount_amt,
                        invoice_tax_amt,
                        invoice_total_amt,
                        invoice_tax_flg,
                        invoice_line_nbr,
                        invoice_line_name,
                        invoice_line_type,
                        invoice_line_type_name,
                        invoice_line_amt,
                        invoice_line_tax,
                        afe_project_nbr,
                        afe_project_name,
                        afe_category_nbr,
                        afe_category_name,
                        afe_sub_cat_nbr,
                        afe_sub_cat_name,
                        afe_use,
                        afe_contract_type,
                        afe_contract_structure,
                        afe_property_cat,
                        inventory_nbr,
                        inventory_name,
                        inventory_class,
                        inventory_class_name,
                        po_nbr,
                        po_name,
                        po_date,
                        po_line_nbr,
                        po_line_name,
                        po_line_type,
                        po_line_type_name,
                        ship_to_location,
                        ship_to_location_name,
                        ship_to_address_line_1,
                        ship_to_address_line_2,
                        ship_to_address_line_3,
                        ship_to_address_line_4,
                        ship_to_address_city,
                        ship_to_address_county,
                        ship_to_address_state,
                        ship_to_address_zip,
                        ship_to_address_country,
                        wo_nbr,
                        wo_name,
                        wo_date,
                        wo_type,
                        wo_type_desc,
                        wo_class,
                        wo_class_desc,
                        wo_entity,
                        wo_entity_desc,
                        wo_line_nbr,
                        wo_line_name,
                        wo_line_type,
                        wo_line_type_desc,
                        wo_shut_down_cd,
                        wo_shut_down_cd_desc,
                        voucher_id,
                        voucher_name,
                        voucher_date,
                        voucher_line_nbr,
                        voucher_line_desc,
                        check_nbr,
                        check_no,
                        check_date,
                        check_amt,
                        check_desc,
                        user_text_01,
                        user_text_02,
                        user_text_03,
                        user_text_04,
                        user_text_05,
                        user_text_06,
                        user_text_07,
                        user_text_08,
                        user_text_09,
                        user_text_10,
                        user_text_11,
                        user_text_12,
                        user_text_13,
                        user_text_14,
                        user_text_15,
                        user_text_16,
                        user_text_17,
                        user_text_18,
                        user_text_19,
                        user_text_20,
                        user_text_21,
                        user_text_22,
                        user_text_23,
                        user_text_24,
                        user_text_25,
                        user_text_26,
                        user_text_27,
                        user_text_28,
                        user_text_29,
                        user_text_30,
                        user_number_01,
                        user_number_02,
                        user_number_03,
                        user_number_04,
                        user_number_05,
                        user_number_06,
                        user_number_07,
                        user_number_08,
                        user_number_09,
                        user_number_10,
                        user_date_01,
                        user_date_02,
                        user_date_03,
                        user_date_04,
                        user_date_05,
                        user_date_06,
                        user_date_07,
                        user_date_08,
                        user_date_09,
                        user_date_10,
                        comments,
                        tb_calc_tax_amt,
                        state_use_amount,
                        state_use_tier2_amount,
                        state_use_tier3_amount,
                        county_use_amount,
                        county_local_use_amount,
                        city_use_amount,
                        city_local_use_amount,
                        transaction_state_code,
                        auto_transaction_state_code,
                        transaction_ind,
                        suspend_ind,
                        taxcode_detail_id,
                        taxcode_state_code,
                        taxcode_type_code,
                        taxcode_code,
                        cch_taxcat_code,
                        cch_group_code,
                        cch_item_code,
                        manual_taxcode_ind,
                        tax_matrix_id,
                        location_matrix_id,
                        jurisdiction_id,
                        jurisdiction_taxrate_id,
                        manual_jurisdiction_ind,
                        measure_type_code,
                        state_use_rate,
                        state_use_tier2_rate,
                        state_use_tier3_rate,
                        state_split_amount,
                        state_tier2_min_amount,
                        state_tier2_max_amount,
                        state_maxtax_amount,
                        county_use_rate,
                        county_local_use_rate,
                        county_split_amount,
                        county_maxtax_amount,
                        county_single_flag,
                        county_default_flag,
                        city_use_rate,
                        city_local_use_rate,
                        city_split_amount,
                        city_split_use_rate,
                        city_single_flag,
                        city_default_flag,
                        combined_use_rate,
                        load_timestamp,
                        gl_extract_updater,
                        gl_extract_timestamp,
                        gl_extract_flag,
                        gl_log_flag,
                        gl_extract_amt,
                        audit_flag,
                        audit_user_id,
                        audit_timestamp,
                        modify_user_id,
                        modify_timestamp,
                        update_user_id,
                        update_timestamp )
                     VALUES (
                        bcp_transactions.transaction_detail_id,
                        bcp_transactions.source_transaction_id,
                        bcp_transactions.process_batch_no,
                        bcp_transactions.gl_extract_batch_no,
                        bcp_transactions.archive_batch_no,
                        bcp_transactions.allocation_matrix_id,
                        bcp_transactions.allocation_subtrans_id,
                        bcp_transactions.entered_date,
                        bcp_transactions.transaction_status,
                        bcp_transactions.gl_date,
                        bcp_transactions.gl_company_nbr,
                        bcp_transactions.gl_company_name,
                        bcp_transactions.gl_division_nbr,
                        bcp_transactions.gl_division_name,
                        bcp_transactions.gl_cc_nbr_dept_id,
                        bcp_transactions.gl_cc_nbr_dept_name,
                        bcp_transactions.gl_local_acct_nbr,
                        bcp_transactions.gl_local_acct_name,
                        bcp_transactions.gl_local_sub_acct_nbr,
                        bcp_transactions.gl_local_sub_acct_name,
                        bcp_transactions.gl_full_acct_nbr,
                        bcp_transactions.gl_full_acct_name,
                        bcp_transactions.gl_line_itm_dist_amt,
                        bcp_transactions.orig_gl_line_itm_dist_amt,
                        bcp_transactions.vendor_nbr,
                        bcp_transactions.vendor_name,
                        bcp_transactions.vendor_address_line_1,
                        bcp_transactions.vendor_address_line_2,
                        bcp_transactions.vendor_address_line_3,
                        bcp_transactions.vendor_address_line_4,
                        bcp_transactions.vendor_address_city,
                        bcp_transactions.vendor_address_county,
                        bcp_transactions.vendor_address_state,
                        bcp_transactions.vendor_address_zip,
                        bcp_transactions.vendor_address_country,
                        bcp_transactions.vendor_type,
                        bcp_transactions.vendor_type_name,
                        bcp_transactions.invoice_nbr,
                        bcp_transactions.invoice_desc,
                        bcp_transactions.invoice_date,
                        bcp_transactions.invoice_freight_amt,
                        bcp_transactions.invoice_discount_amt,
                        bcp_transactions.invoice_tax_amt,
                        bcp_transactions.invoice_total_amt,
                        bcp_transactions.invoice_tax_flg,
                        bcp_transactions.invoice_line_nbr,
                        bcp_transactions.invoice_line_name,
                        bcp_transactions.invoice_line_type,
                        bcp_transactions.invoice_line_type_name,
                        bcp_transactions.invoice_line_amt,
                        bcp_transactions.invoice_line_tax,
                        bcp_transactions.afe_project_nbr,
                        bcp_transactions.afe_project_name,
                        bcp_transactions.afe_category_nbr,
                        bcp_transactions.afe_category_name,
                        bcp_transactions.afe_sub_cat_nbr,
                        bcp_transactions.afe_sub_cat_name,
                        bcp_transactions.afe_use,
                        bcp_transactions.afe_contract_type,
                        bcp_transactions.afe_contract_structure,
                        bcp_transactions.afe_property_cat,
                        bcp_transactions.inventory_nbr,
                        bcp_transactions.inventory_name,
                        bcp_transactions.inventory_class,
                        bcp_transactions.inventory_class_name,
                        bcp_transactions.po_nbr,
                        bcp_transactions.po_name,
                        bcp_transactions.po_date,
                        bcp_transactions.po_line_nbr,
                        bcp_transactions.po_line_name,
                        bcp_transactions.po_line_type,
                        bcp_transactions.po_line_type_name,
                        bcp_transactions.ship_to_location,
                        bcp_transactions.ship_to_location_name,
                        bcp_transactions.ship_to_address_line_1,
                        bcp_transactions.ship_to_address_line_2,
                        bcp_transactions.ship_to_address_line_3,
                        bcp_transactions.ship_to_address_line_4,
                        bcp_transactions.ship_to_address_city,
                        bcp_transactions.ship_to_address_county,
                        bcp_transactions.ship_to_address_state,
                        bcp_transactions.ship_to_address_zip,
                        bcp_transactions.ship_to_address_country,
                        bcp_transactions.wo_nbr,
                        bcp_transactions.wo_name,
                        bcp_transactions.wo_date,
                        bcp_transactions.wo_type,
                        bcp_transactions.wo_type_desc,
                        bcp_transactions.wo_class,
                        bcp_transactions.wo_class_desc,
                        bcp_transactions.wo_entity,
                        bcp_transactions.wo_entity_desc,
                        bcp_transactions.wo_line_nbr,
                        bcp_transactions.wo_line_name,
                        bcp_transactions.wo_line_type,
                        bcp_transactions.wo_line_type_desc,
                        bcp_transactions.wo_shut_down_cd,
                        bcp_transactions.wo_shut_down_cd_desc,
                        bcp_transactions.voucher_id,
                        bcp_transactions.voucher_name,
                        bcp_transactions.voucher_date,
                        bcp_transactions.voucher_line_nbr,
                        bcp_transactions.voucher_line_desc,
                        bcp_transactions.check_nbr,
                        bcp_transactions.check_no,
                        bcp_transactions.check_date,
                        bcp_transactions.check_amt,
                        bcp_transactions.check_desc,
                        bcp_transactions.user_text_01,
                        bcp_transactions.user_text_02,
                        bcp_transactions.user_text_03,
                        bcp_transactions.user_text_04,
                        bcp_transactions.user_text_05,
                        bcp_transactions.user_text_06,
                        bcp_transactions.user_text_07,
                        bcp_transactions.user_text_08,
                        bcp_transactions.user_text_09,
                        bcp_transactions.user_text_10,
                        bcp_transactions.user_text_11,
                        bcp_transactions.user_text_12,
                        bcp_transactions.user_text_13,
                        bcp_transactions.user_text_14,
                        bcp_transactions.user_text_15,
                        bcp_transactions.user_text_16,
                        bcp_transactions.user_text_17,
                        bcp_transactions.user_text_18,
                        bcp_transactions.user_text_19,
                        bcp_transactions.user_text_20,
                        bcp_transactions.user_text_21,
                        bcp_transactions.user_text_22,
                        bcp_transactions.user_text_23,
                        bcp_transactions.user_text_24,
                        bcp_transactions.user_text_25,
                        bcp_transactions.user_text_26,
                        bcp_transactions.user_text_27,
                        bcp_transactions.user_text_28,
                        bcp_transactions.user_text_29,
                        bcp_transactions.user_text_30,
                        bcp_transactions.user_number_01,
                        bcp_transactions.user_number_02,
                        bcp_transactions.user_number_03,
                        bcp_transactions.user_number_04,
                        bcp_transactions.user_number_05,
                        bcp_transactions.user_number_06,
                        bcp_transactions.user_number_07,
                        bcp_transactions.user_number_08,
                        bcp_transactions.user_number_09,
                        bcp_transactions.user_number_10,
                        bcp_transactions.user_date_01,
                        bcp_transactions.user_date_02,
                        bcp_transactions.user_date_03,
                        bcp_transactions.user_date_04,
                        bcp_transactions.user_date_05,
                        bcp_transactions.user_date_06,
                        bcp_transactions.user_date_07,
                        bcp_transactions.user_date_08,
                        bcp_transactions.user_date_09,
                        bcp_transactions.user_date_10,
                        bcp_transactions.comments,
                        bcp_transactions.tb_calc_tax_amt,
                        bcp_transactions.state_use_amount,
                        bcp_transactions.state_use_tier2_amount,
                        bcp_transactions.state_use_tier3_amount,
                        bcp_transactions.county_use_amount,
                        bcp_transactions.county_local_use_amount,
                        bcp_transactions.city_use_amount,
                        bcp_transactions.city_local_use_amount,
                        bcp_transactions.transaction_state_code,
                        bcp_transactions.auto_transaction_state_code,
                        bcp_transactions.transaction_ind,
                        bcp_transactions.suspend_ind,
                        bcp_transactions.taxcode_detail_id,
                        bcp_transactions.taxcode_state_code,
                        bcp_transactions.taxcode_type_code,
                        bcp_transactions.taxcode_code,
                        bcp_transactions.cch_taxcat_code,
                        bcp_transactions.cch_group_code,
                        bcp_transactions.cch_item_code,
                        bcp_transactions.manual_taxcode_ind,
                        bcp_transactions.tax_matrix_id,
                        bcp_transactions.location_matrix_id,
                        bcp_transactions.jurisdiction_id,
                        bcp_transactions.jurisdiction_taxrate_id,
                        bcp_transactions.manual_jurisdiction_ind,
                        bcp_transactions.measure_type_code,
                        bcp_transactions.state_use_rate,
                        bcp_transactions.state_use_tier2_rate,
                        bcp_transactions.state_use_tier3_rate,
                        bcp_transactions.state_split_amount,
                        bcp_transactions.state_tier2_min_amount,
                        bcp_transactions.state_tier2_max_amount,
                        bcp_transactions.state_maxtax_amount,
                        bcp_transactions.county_use_rate,
                        bcp_transactions.county_local_use_rate,
                        bcp_transactions.county_split_amount,
                        bcp_transactions.county_maxtax_amount,
                        bcp_transactions.county_single_flag,
                        bcp_transactions.county_default_flag,
                        bcp_transactions.city_use_rate,
                        bcp_transactions.city_local_use_rate,
                        bcp_transactions.city_split_amount,
                        bcp_transactions.city_split_use_rate,
                        bcp_transactions.city_single_flag,
                        bcp_transactions.city_default_flag,
                        bcp_transactions.combined_use_rate,
                        bcp_transactions.load_timestamp,
                        bcp_transactions.gl_extract_updater,
                        bcp_transactions.gl_extract_timestamp,
                        bcp_transactions.gl_extract_flag,
                        bcp_transactions.gl_log_flag,
                        bcp_transactions.gl_extract_amt,
                        bcp_transactions.audit_flag,
                        bcp_transactions.audit_user_id,
                        bcp_transactions.audit_timestamp,
                        bcp_transactions.modify_user_id,
                        bcp_transactions.modify_timestamp,
                        bcp_transactions.update_user_id,
                        bcp_transactions.update_timestamp );

                  EXCEPTION
                     WHEN OTHERS THEN
                        vi_error_count := vi_error_count + 1;
                        EXECUTE sp_geterrorcode('AL5', an_batch_id, 'AL', v_sysdate, v_severity_level, v_write_import_line_flag, v_abort_import_flag);
                        IF v_severity_level > v_error_sev_code THEN
                           v_error_sev_code := v_severity_level;
                        END IF;
                  END;
                  -- Exit if aborted
                  IF v_abort_import_flag = '1' THEN
                     v_batch_status_code := 'ABAL';
                     RAISE EXCEPTION 'Abort Procedure: %', v_batch_status_code;
                  END IF;
               END LOOP;
            END IF;
            CLOSE allocation_detail_cursor;

-- MBF02      -- Check rounding
--            v_delta := v_orig_gl_line_itm_dist_amt - v_accum_allocation;
--            IF v_delta <> 0 THEN
--               UPDATE tb_bcp_transactions
--                  SET gl_line_itm_dist_amt = gl_line_itm_dist_amt + v_delta
--                WHERE transaction_detail_id = bcp_transactions.transaction_detail_id;
--            END IF;

            -- Update Allocated Transaction
            v_total_rows := v_total_rows - 1;
            UPDATE tb_bcp_transactions
               SET transaction_detail_id = -1
             WHERE oid = v_oid;
         END IF;
      END IF;
   END LOOP;
   CLOSE bcp_transactions_cursor;
   v_batch_status_code := 'AL';

   -- Update batch with final totals
   UPDATE tb_batch
      SET batch_status_code = v_batch_status_code,
          error_sev_code = v_error_sev_code,
          total_rows = v_total_rows
    WHERE batch_id = an_batch_id;

   --COMMIT;

EXCEPTION
   WHEN OTHERS THEN
      -- Update batch with error codes
      UPDATE tb_batch
         SET batch_status_code = v_batch_status_code,
             error_sev_code = v_error_sev_code
       WHERE batch_id = an_batch_id;
      --COMMIT;
END;
$$ LANGUAGE plpgsql;

