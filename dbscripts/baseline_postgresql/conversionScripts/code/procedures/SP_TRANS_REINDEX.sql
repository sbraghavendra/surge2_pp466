CREATE OR REPLACE FUNCTION sp_trans_reindex(param1 in Varchar) RETURNS VOID AS $$
DECLARE
-- param1 definition:
--  '1' = Build Transaction Detail Indexes only
--  '2' = Build Tax Matrix Indexes only
--  '9' = Build All Indexes

-- Program defined variables
   vc_statement       VARCHAR (250)               := ' ';
   v_number_of_rows   NUMERIC                     := 0;
   v_rows_processed   INTEGER                     := 0;
   v_cursor_id        INTEGER                     := 0;

-- Create Cursor record for Transactions
   trans_record      RECORD;

-- Create Cursor record for Tax Matrix
   matrix_record     RECORD;      

-- Create Cursor record for Both
   both_record       RECORD;

-- Program starts **********************************************************************************
BEGIN
   -- Transation Index Cursor
   IF param1 = '1' THEN
      FOR trans_record IN
	SELECT indexname
         FROM pg_indexes
         WHERE indexname like 'idx_tb_trans_dtl_driver_%'
         AND schemaname = 'stscorp'
      LOOP
         vc_statement := 'DROP INDEX ' || trans_record.index_name;
         EXECUTE vc_statement;
      END LOOP;

      EXECUTE sp_create_trans_indexes;
      EXECUTE sp_create_trans_funct_indexes;
   END IF;

   -- Tax Matrix Index Cursor
   IF param1 = '2' THEN
      FOR matrix_record IN
	SELECT indexname
         FROM pg_indexes
         WHERE indexname like 'idx_tb_tax_matrix_driver_%'
         AND schemaname = 'stscorp'
      LOOP
         vc_statement :='DROP INDEX ' || matrix_record.index_name;
         EXECUTE vc_statement;
      END LOOP;

      EXECUTE sp_create_matrix_indexes;
      EXECUTE sp_create_matrix_indexes_func;
   END IF;

   -- Both Index Cursor
   IF param1 = '9' THEN
      FOR both_record IN
	SELECT indexname
         FROM pg_indexes
         WHERE indexname like 'IDX_TB_TRANS_DTL_DRIVER_%' OR index_name like 'IDX_TB_TAX_MATRIX_DRIVER_%'
         AND schemaname = 'STSCORP'
      LOOP
         vc_statement :='DROP INDEX ' || both_record.index_name;
         EXECUTE vc_statement;
      END LOOP;

      EXECUTE sp_create_trans_indexes;
      EXECUTE sp_create_trans_funct_indexes;
      EXECUTE sp_create_matrix_indexes;
      EXECUTE sp_create_matrix_indexes_func;
   END IF;
END;
$$ LANGUAGE plpgsql;

