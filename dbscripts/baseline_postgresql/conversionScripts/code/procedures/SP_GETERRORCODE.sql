CREATE OR REPLACE FUNCTION sp_geterrorcode(
   av_error_code		in varchar,
   an_batch_id			in numeric,
   av_process_type		in varchar,
   ad_process_timestamp		in date,
   av_severity_level		out varchar,
   av_write_import_line_flag	out varchar,
   av_abort_import_flag		out varchar ) RETURNS RECORD AS $$
-- ****** Program starts ****** --------------------------------------------------------------------
BEGIN
   -- Fetch error condition information
   BEGIN
      SELECT ERRORCODE.severity_level,
             ERRORSEV.write_import_line_flag,
             ERRORSEV.abort_import_flag
        INTO STRICT 
	     av_severity_level,
             av_write_import_line_flag,
             av_abort_import_flag
        FROM ( SELECT *
                 FROM tb_list_code
                WHERE code_type_code = 'ERRORCODE' ) ERRORCODE,
             ( SELECT *
                 FROM tb_list_code
                WHERE code_type_code = 'ERRORSEV' ) ERRORSEV
       WHERE ERRORCODE.severity_level = ERRORSEV.code_code AND
             ERRORCODE.code_code = av_error_code;
      EXCEPTION WHEN OTHERS THEN
         av_severity_level := '90';
         av_write_import_line_flag := '0';
         av_abort_import_flag := '1';
   END;

   -- Write error to log
   INSERT INTO tb_batch_error_log (
      batch_id,
      process_type,
      process_timestamp,
      row_no,
      column_no,
      error_def_code )
   VALUES (
      an_batch_id,
      av_process_type,
      ad_process_timestamp,
      0,
      0,
      av_error_code );
END;
$$ LANGUAGE plpgsql;
