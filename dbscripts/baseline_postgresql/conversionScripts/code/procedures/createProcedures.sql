---------------------------------------------------------------------------------------- 
--  Copyright (c) Lincoln Peak Partners (2008) All rights reserved 
    
--  Name         : createProcedures.sql

--  Description  : This file will create functions (that were formerly procedures in Oracle) in the stscorp schema

--  Usage        : => \i createProcedures.sql

--  Notes        Run this script while connected as the stscorp user to the postdb1 DB
---------------------------------------------------------------------------------------- 

\i ./SP_BATCH.sql
\i ./SP_BATCH_ARCHIVE.sql
\i ./SP_BATCH_ARCHIVE_DELETE.sql
\i ./SP_BATCH_ARCHIVE_RESTORE.sql
\i ./SP_BATCH_DELETE.sql
\i ./SP_BATCH_PROCESS.sql
\i ./SP_BATCH_TAXRATE_UPDATE.sql
\i ./SP_CALCUSETAX.sql
\i ./SP_CCH_TXMATRIX_A_D.sql
\i ./SP_CREATE_MATRIX_INDEXES.sql
\i ./SP_CREATE_MATRIX_INDEXES_FUNC.sql
\i ./SP_CREATE_TRANS_FUNCT_INDEXES.sql
\i ./SP_CREATE_TRANS_INDEXES.sql
\i ./SP_DB_STATISTICS.sql
\i ./SP_DRIVER_REFERENCE_MAINT.sql
\i ./SP_GETERRORCODE.sql
\i ./SP_GETUSETAX.sql
\i ./SP_GL_EXTRACT.sql
\i ./SP_IMPORT_ALLOCATION.sql
\i ./SP_MAX_ALL.sql
\i ./SP_MAX_GL_EXTRACT_BATCH_ID.sql
\i ./SP_MAX_TB_ALLOCATION_MATRIX_ID.sql
\i ./SP_MAX_TB_BATCH_ERROR_LOG_ID.sql
\i ./SP_MAX_TB_BATCH_ID.sql
\i ./SP_MAX_TB_ENTITY_ID.sql
\i ./SP_MAX_TB_GL_EXTRACT_MAP_ID.sql
\i ./SP_MAX_TB_JURISDICTION_ID.sql
\i ./SP_MAX_TB_JURIS_TAXRATE_ID.sql
\i ./SP_MAX_TB_LOCATION_MATRIX_ID.sql
\i ./SP_MAX_TB_QUESTANS_LOG_ID.sql
\i ./SP_MAX_TB_TAXCODE_DETAIL_ID.sql
\i ./SP_MAX_TB_TAX_MATRIX_ID.sql
\i ./SP_MAX_TB_TRANS_DETAIL_EXCP_ID.sql
\i ./SP_MAX_TB_TRANS_DETAIL_ID.sql
\i ./SP_RANDOM_TRANS.sql
\i ./SP_REWEIGHT_MATRIX.sql
\i ./SP_STATISTICS_LINERELOAD.sql
\i ./SP_TRANS_REINDEX.sql

-- NOTE: The file createFunctionSP_DB_STATISTICS_PostgreSQL.sql was created in response to SPR 3464
-- Further confirmation needs to be done on whether this will officially make its way into the schema
