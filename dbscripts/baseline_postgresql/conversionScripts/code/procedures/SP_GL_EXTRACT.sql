CREATE OR REPLACE FUNCTION Sp_Gl_Extract
   ( where_clause IN VARCHAR,  file_name IN VARCHAR, execution_mode IN NUMERIC, return_code IN OUT NUMERIC ) 
  RETURNS NUMERIC AS $$
DECLARE
   --fHandle UTL_FILE.FILE_TYPE;

   pl_line        VARCHAR (1000);
   vc_sql_stmt    VARCHAR (20000);
   vn_total_tax   NUMERIC   := 0.00;
   vn_acc_tax     NUMERIC   := 0;
   vn_counter     INTEGER  := 0;
   vc_COST_CNTR_CDE     VARCHAR(10);
   vn_gl_count    NUMERIC   :=0;
   vd_end_date    TIMESTAMP(0);
   vn_row_count   NUMERIC   :=0;
   v_marking_flag NUMERIC   :=0;

    cur_gl_company_nbr CURSOR FOR SELECT DISTINCT gl_company_nbr  FROM TB_TMP_GL_EXPORT;
    
    cur_gl CURSOR FOR SELECT *  FROM TB_TMP_GL_EXT_CELANESE_PROD_2 ORDER BY filler1;
    gl_rec    RECORD;

    cur_gl_40 CURSOR FOR
           SELECT COMPANY_CODE,posting_key,plant,SUM(amount) as Sums
           FROM TB_TMP_GL_EXT_CELANESE_PROD_2
           WHERE posting_key = '40'
           GROUP BY COMPANY_CODE,posting_key,plant;

    gl_rec_40 RECORD;


    vc_end_date          VARCHAR(20);
    vc_gl_division_nbr	 VARCHAR(10);
    vc_gl_division_nm    VARCHAR(50);
    SYS_DATE	         TIMESTAMP(0);
	vn_gl_company_count  NUMERIC;
    vc_gl_company_nbr    VARCHAR(30);
	vn_gl_sums           NUMERIC  := 0.00;
	vn_gl_sums1          NUMERIC  := 0.00;
	vc_gl_plant          VARCHAR(30);
	vn_gl_batch_id       NUMERIC  := 0;
	vn_WBS_ELEMENT		 VARCHAR(24);
	vn_NETWORK			 VARCHAR(12);
    vc_gl_export_path VARCHAR(128);

-- Program begin
BEGIN
-- 7/20/2004 changes on GL via Lisa's request
-- 1. Replace all hard coded account number 8113401 to gl_local_acct_nbr
-- 2. Make GL one gl_company_nbr at a time

 vc_gl_export_path := 'C:/Clients/Celanese/GL_Export';




   -- Determine if the global temporary table that holds GL_EXTRACT info exists
   SELECT COUNT(*)
     INTO vn_gl_count
     FROM pg_tables
    WHERE schemaname = 'stscorp'
      AND tablename = 'tb_tmp_gl_ext_celanese_prod_2';

   -- If the global temporary table does not exist, create it on the fly
   IF  vn_gl_count = 0 THEN
      EXECUTE 'CREATE GLOBAL TEMPORARY TABLE TB_TMP_GL_EXT_CELANESE_PROD_2 ( '||
         'RECORD_TYPE      CHAR (1), '||
         'POSTING_KEY      CHAR (2), '||
         'GL_ACCOUNT       VARCHAR (10), '||
         'AMOUNT           VARCHAR (16), '||
         'COST_CNTR_CDE    VARCHAR (10), '||
         'ORDR_NBR         VARCHAR (12), '||
         'WBS              VARCHAR (24), '||
         'ALLOCATION       CHAR (18), '||
         'LINE_ITEM_TEXT   VARCHAR (50), '||
         'GL_COMPANY_NBR   VARCHAR (50), '||
         'GL_DIVISION_NBR  VARCHAR (50), '||
	 '	 ACTIVITY         VARCHAR (4)) '||
         'ON COMMIT PRESERVE ROWS ';
       END IF;

--vd_end_date := to_date(substr(where_clause,INSTR(where_clause, 'to_date(' , 1, 1 ) + 9 , 10),'MM/DD/YYYY');
vd_end_date := TO_DATE(SUBSTR(where_clause,1,10),'MM/DD/YYYY');
vc_end_date := TO_CHAR(vd_end_date,'DD-MON-YYYY');
vc_gl_company_nbr := SUBSTR(where_clause,12);

--vc_gl_division_nm := SUBSTR(where_clause,12);
SYS_DATE := CURRENT_TIMESTAMP;

--insert into t1 (d) values(vd_end_date);
--commit;


EXECUTE 'TRUNCATE TABLE TB_TMP_GL_EXT_CELANESE_PROD_2';

----------------- NOT Generic GL Reversal Code.  Condition added  ---------------------------------------
-- gl_company_nbr = vc_gl_company_nbr   AND USER_TEXT_11 <> 'E1' ----------------------------------------

EXECUTE 'CREATE GLOBAL TEMPORARY TABLE TB_TMP_GL_EXPORT
(
  TRANSACTION_DETAIL_ID        NUMERIC,
  SOURCE_TRANSACTION_ID        VARCHAR(100),
  PROCESS_BATCH_NO             NUMERIC,
  GL_EXTRACT_BATCH_NO          NUMERIC,
  ARCHIVE_BATCH_NO             NUMERIC,
  ALLOCATION_MATRIX_ID         NUMERIC,
  ALLOCATION_SUBTRANS_ID       NUMERIC,
  ENTERED_DATE                 TIMESTAMP(0),
  TRANSACTION_STATUS           VARCHAR(10),
  GL_DATE                      TIMESTAMP(0),
  GL_COMPANY_NBR               VARCHAR(100),
  GL_COMPANY_NAME              VARCHAR(100),
  GL_DIVISION_NBR              VARCHAR(100),
  GL_DIVISION_NAME             VARCHAR(100),
  GL_CC_NBR_DEPT_ID            VARCHAR(100),
  GL_CC_NBR_DEPT_NAME          VARCHAR(100),
  GL_LOCAL_ACCT_NBR            VARCHAR(100),
  GL_LOCAL_ACCT_NAME           VARCHAR(100),
  GL_LOCAL_SUB_ACCT_NBR        VARCHAR(100),
  GL_LOCAL_SUB_ACCT_NAME       VARCHAR(100),
  GL_FULL_ACCT_NBR             VARCHAR(100),
  GL_FULL_ACCT_NAME            VARCHAR(100),
  GL_LINE_ITM_DIST_AMT         NUMERIC,
  ORIG_GL_LINE_ITM_DIST_AMT    NUMERIC,
  VENDOR_NBR                   VARCHAR(100),
  VENDOR_NAME                  VARCHAR(100),
  VENDOR_ADDRESS_LINE_1        VARCHAR(100),
  VENDOR_ADDRESS_LINE_2        VARCHAR(100),
  VENDOR_ADDRESS_LINE_3        VARCHAR(100),
  VENDOR_ADDRESS_LINE_4        VARCHAR(100),
  VENDOR_ADDRESS_CITY          VARCHAR(100),
  VENDOR_ADDRESS_COUNTY        VARCHAR(100),
  VENDOR_ADDRESS_STATE         VARCHAR(100),
  VENDOR_ADDRESS_ZIP           VARCHAR(100),
  VENDOR_ADDRESS_COUNTRY       VARCHAR(100),
  VENDOR_TYPE                  VARCHAR(100),
  VENDOR_TYPE_NAME             VARCHAR(100),
  INVOICE_NBR                  VARCHAR(100),
  INVOICE_DESC                 VARCHAR(100),
  INVOICE_DATE                 TIMESTAMP(0),
  INVOICE_FREIGHT_AMT          NUMERIC,
  INVOICE_DISCOUNT_AMT         NUMERIC,
  INVOICE_TAX_AMT              NUMERIC,
  INVOICE_TOTAL_AMT            NUMERIC,
  INVOICE_TAX_FLG              VARCHAR(100),
  INVOICE_LINE_NBR             VARCHAR(100),
  INVOICE_LINE_NAME            VARCHAR(100),
  INVOICE_LINE_TYPE            VARCHAR(100),
  INVOICE_LINE_TYPE_NAME       VARCHAR(100),
  INVOICE_LINE_AMT             NUMERIC,
  INVOICE_LINE_TAX             NUMERIC,
  AFE_PROJECT_NBR              VARCHAR(100),
  AFE_PROJECT_NAME             VARCHAR(100),
  AFE_CATEGORY_NBR             VARCHAR(100),
  AFE_CATEGORY_NAME            VARCHAR(100),
  AFE_SUB_CAT_NBR              VARCHAR(100),
  AFE_SUB_CAT_NAME             VARCHAR(100),
  AFE_USE                      VARCHAR(100),
  AFE_CONTRACT_TYPE            VARCHAR(100),
  AFE_CONTRACT_STRUCTURE       VARCHAR(100),
  AFE_PROPERTY_CAT             VARCHAR(100),
  INVENTORY_NBR                VARCHAR(100),
  INVENTORY_NAME               VARCHAR(100),
  INVENTORY_CLASS              VARCHAR(100),
  INVENTORY_CLASS_NAME         VARCHAR(100),
  PO_NBR                       VARCHAR(100),
  PO_NAME                      VARCHAR(100),
  PO_DATE                      TIMESTAMP(0),
  PO_LINE_NBR                  VARCHAR(100),
  PO_LINE_NAME                 VARCHAR(100),
  PO_LINE_TYPE                 VARCHAR(100),
  PO_LINE_TYPE_NAME            VARCHAR(100),
  SHIP_TO_LOCATION             VARCHAR(100),
  SHIP_TO_LOCATION_NAME        VARCHAR(100),
  SHIP_TO_ADDRESS_LINE_1       VARCHAR(100),
  SHIP_TO_ADDRESS_LINE_2       VARCHAR(100),
  SHIP_TO_ADDRESS_LINE_3       VARCHAR(100),
  SHIP_TO_ADDRESS_LINE_4       VARCHAR(100),
  SHIP_TO_ADDRESS_CITY         VARCHAR(100),
  SHIP_TO_ADDRESS_COUNTY       VARCHAR(100),
  SHIP_TO_ADDRESS_STATE        VARCHAR(100),
  SHIP_TO_ADDRESS_ZIP          VARCHAR(100),
  SHIP_TO_ADDRESS_COUNTRY      VARCHAR(100),
  WO_NBR                       VARCHAR(100),
  WO_NAME                      VARCHAR(100),
  WO_DATE                      TIMESTAMP(0),
  WO_TYPE                      VARCHAR(100),
  WO_TYPE_DESC                 VARCHAR(100),
  WO_CLASS                     VARCHAR(100),
  WO_CLASS_DESC                VARCHAR(100),
  WO_ENTITY                    VARCHAR(100),
  WO_ENTITY_DESC               VARCHAR(100),
  WO_LINE_NBR                  VARCHAR(100),
  WO_LINE_NAME                 VARCHAR(100),
  WO_LINE_TYPE                 VARCHAR(100),
  WO_LINE_TYPE_DESC            VARCHAR(100),
  WO_SHUT_DOWN_CD              VARCHAR(100),
  WO_SHUT_DOWN_CD_DESC         VARCHAR(100),
  VOUCHER_ID                   VARCHAR(100),
  VOUCHER_NAME                 VARCHAR(100),
  VOUCHER_DATE                 TIMESTAMP(0),
  VOUCHER_LINE_NBR             VARCHAR(100),
  VOUCHER_LINE_DESC            VARCHAR(100),
  CHECK_NBR                    VARCHAR(100),
  CHECK_NO                     NUMERIC,
  CHECK_DATE                   TIMESTAMP(0),
  CHECK_AMT                    NUMERIC,
  CHECK_DESC                   VARCHAR(100),
  USER_TEXT_01                 VARCHAR(100),
  USER_TEXT_02                 VARCHAR(100),
  USER_TEXT_03                 VARCHAR(100),
  USER_TEXT_04                 VARCHAR(100),
  USER_TEXT_05                 VARCHAR(100),
  USER_TEXT_06                 VARCHAR(100),
  USER_TEXT_07                 VARCHAR(100),
  USER_TEXT_08                 VARCHAR(100),
  USER_TEXT_09                 VARCHAR(100),
  USER_TEXT_10                 VARCHAR(100),
  USER_TEXT_11                 VARCHAR(100),
  USER_TEXT_12                 VARCHAR(100),
  USER_TEXT_13                 VARCHAR(100),
  USER_TEXT_14                 VARCHAR(100),
  USER_TEXT_15                 VARCHAR(100),
  USER_TEXT_16                 VARCHAR(100),
  USER_TEXT_17                 VARCHAR(100),
  USER_TEXT_18                 VARCHAR(100),
  USER_TEXT_19                 VARCHAR(100),
  USER_TEXT_20                 VARCHAR(100),
  USER_TEXT_21                 VARCHAR(100),
  USER_TEXT_22                 VARCHAR(100),
  USER_TEXT_23                 VARCHAR(100),
  USER_TEXT_24                 VARCHAR(100),
  USER_TEXT_25                 VARCHAR(100),
  USER_TEXT_26                 VARCHAR(100),
  USER_TEXT_27                 VARCHAR(100),
  USER_TEXT_28                 VARCHAR(100),
  USER_TEXT_29                 VARCHAR(100),
  USER_TEXT_30                 VARCHAR(100),
  USER_NUMBER_01               NUMERIC,
  USER_NUMBER_02               NUMERIC,
  USER_NUMBER_03               NUMERIC,
  USER_NUMBER_04               NUMERIC,
  USER_NUMBER_05               NUMERIC,
  USER_NUMBER_06               NUMERIC,
  USER_NUMBER_07               NUMERIC,
  USER_NUMBER_08               NUMERIC,
  USER_NUMBER_09               NUMERIC,
  USER_NUMBER_10               NUMERIC,
  USER_DATE_01                 TIMESTAMP(0),
  USER_DATE_02                 TIMESTAMP(0),
  USER_DATE_03                 TIMESTAMP(0),
  USER_DATE_04                 TIMESTAMP(0),
  USER_DATE_05                 TIMESTAMP(0),
  USER_DATE_06                 TIMESTAMP(0),
  USER_DATE_07                 TIMESTAMP(0),
  USER_DATE_08                 TIMESTAMP(0),
  USER_DATE_09                 TIMESTAMP(0),
  USER_DATE_10                 TIMESTAMP(0),
  COMMENTS                     VARCHAR(4000),
  TB_CALC_TAX_AMT              NUMERIC,
  STATE_USE_AMOUNT             NUMERIC,
  STATE_USE_TIER2_AMOUNT       NUMERIC,
  STATE_USE_TIER3_AMOUNT       NUMERIC,
  COUNTY_USE_AMOUNT            NUMERIC,
  COUNTY_LOCAL_USE_AMOUNT      NUMERIC,
  CITY_USE_AMOUNT              NUMERIC,
  CITY_LOCAL_USE_AMOUNT        NUMERIC,
  TRANSACTION_STATE_CODE       VARCHAR(10),
  AUTO_TRANSACTION_STATE_CODE  VARCHAR(10),
  TRANSACTION_IND              VARCHAR(10),
  SUSPEND_IND                  VARCHAR(10),
  TAXCODE_DETAIL_ID            NUMERIC,
  TAXCODE_STATE_CODE           VARCHAR(10),
  TAXCODE_TYPE_CODE            VARCHAR(10),
  TAXCODE_CODE                 VARCHAR(40),
  CCH_TAXCAT_CODE              VARCHAR(2),
  CCH_GROUP_CODE               VARCHAR(4),
  CCH_ITEM_CODE                VARCHAR(3),
  MANUAL_TAXCODE_IND           VARCHAR(10),
  TAX_MATRIX_ID                NUMERIC,
  LOCATION_MATRIX_ID           NUMERIC,
  JURISDICTION_ID              NUMERIC,
  JURISDICTION_TAXRATE_ID      NUMERIC,
  MANUAL_JURISDICTION_IND      VARCHAR(10),
  MEASURE_TYPE_CODE            VARCHAR(10),
  STATE_USE_RATE               NUMERIC,
  STATE_USE_TIER2_RATE         NUMERIC,
  STATE_USE_TIER3_RATE         NUMERIC,
  STATE_SPLIT_AMOUNT           NUMERIC,
  STATE_TIER2_MIN_AMOUNT       NUMERIC,
  STATE_TIER2_MAX_AMOUNT       NUMERIC,
  STATE_MAXTAX_AMOUNT          NUMERIC,
  COUNTY_USE_RATE              NUMERIC,
  COUNTY_LOCAL_USE_RATE        NUMERIC,
  COUNTY_SPLIT_AMOUNT          NUMERIC,
  COUNTY_MAXTAX_AMOUNT         NUMERIC,
  COUNTY_SINGLE_FLAG           CHAR(1),
  COUNTY_DEFAULT_FLAG          CHAR(1),
  CITY_USE_RATE                NUMERIC,
  CITY_LOCAL_USE_RATE          NUMERIC,
  CITY_SPLIT_AMOUNT            NUMERIC,
  CITY_SPLIT_USE_RATE          NUMERIC,
  CITY_SINGLE_FLAG             CHAR(1),
  CITY_DEFAULT_FLAG            CHAR(1),
  COMBINED_USE_RATE            NUMERIC,
  LOAD_TIMESTAMP               TIMESTAMP(0),
  GL_EXTRACT_UPDATER           VARCHAR(40),
  GL_EXTRACT_TIMESTAMP         TIMESTAMP(0),
  GL_EXTRACT_FLAG              NUMERIC,
  GL_LOG_FLAG                  NUMERIC,
  GL_EXTRACT_AMT               NUMERIC,
  AUDIT_FLAG                   CHAR(1),
  AUDIT_USER_ID                VARCHAR(40),
  AUDIT_TIMESTAMP              TIMESTAMP(0),
  MODIFY_USER_ID               VARCHAR(40),
  MODIFY_TIMESTAMP             TIMESTAMP(0),
  UPDATE_USER_ID               VARCHAR(40),
  UPDATE_TIMESTAMP             TIMESTAMP(0)
)
ON COMMIT PRESERVE ROWS';

EXECUTE 'truncate table TB_TMP_GL_EXPORT';

 INSERT INTO TB_TMP_GL_EXPORT
   (SELECT *
   FROM TB_TRANSACTION_DETAIL T
   WHERE
   (   T.transaction_ind = 'P'
   AND T.taxcode_type_code = 'T'
   AND T.INVOICE_TAX_FLG = 'N'
   AND T.GL_EXTRACT_FLAG IS NULL
   AND T.gl_date <= vd_end_date
   AND gl_company_nbr = vc_gl_company_nbr
   AND (USER_TEXT_11 <> 'E1' OR USER_TEXT_11 IS NULL))
   OR
   (   T.transaction_ind = 'P'
   AND T.taxcode_type_code = 'E'
   AND T.INVOICE_TAX_FLG = 'N'
   AND T.GL_EXTRACT_FLAG IS NULL
   AND T.gl_date <= vd_end_date
   AND T.GL_LOG_FLAG = 1
   AND T.gl_company_nbr = vc_gl_company_nbr
   AND (USER_TEXT_11 <> 'E1' OR USER_TEXT_11 IS NULL)) );

-- debug
--   INSERT INTO DEBUG_TRANS1 SELECT * FROM TB_TMP_GL_EXPORT;
-- debug


   UPDATE TB_GL_EXPORT_LOG
   SET gl_extract_flag = 0
--   WHERE transaction_detail_id IN (SELECT transaction_detail_id FROM TB_TMP_TRANSACTION_DETAIL)
   WHERE transaction_detail_id IN (SELECT transaction_detail_id FROM TB_TMP_GL_EXPORT)
   AND gl_extract_flag IS NULL;

   DELETE FROM TB_TMP_GL_EXPORT
   WHERE transaction_detail_id IN
         (SELECT DISTINCT transaction_detail_id FROM TB_GL_EXPORT_LOG WHERE gl_extract_flag = 0);

-- debug
--   INSERT INTO DEBUG_TRANS2 SELECT * FROM TB_TMP_GL_EXPORT;
-- debug


   INSERT INTO TB_TMP_GL_EXPORT
   (SELECT *
   FROM TB_GL_EXPORT_LOG T
   WHERE T.gl_extract_flag = 0);

-- debug
--   INSERT INTO DEBUG_TRANS3 SELECT * FROM TB_TMP_GL_EXPORT;
-- debug

   --COMMIT;
----------------- Generic GL Reversal Code Stops ---------------------------------------

-- Exclude OTHER gl_company_nbr
--  DELETE TB_TMP_GL_EXPORT
--  WHERE  gl_company_nbr <> vc_gl_company_nbr

-- Exclude   USER_TEXT_11 = 'E1' on 2/4/2004
--  DELETE TB_TMP_GL_EXPORT
--  WHERE USER_TEXT_11 = 'E1';


-- DEBUG
--  INSERT INTO DEBUG_TRANS4 SELECT * FROM TB_TMP_GL_EXPORT;
-- debug

  COMMIT;

  vn_gl_company_count := 1;

--  FOR gl_rec IN cur_gl_company_nbr LOOP

--    vc_gl_company_nbr := gl_rec.gl_company_nbr;

        -- 1. Header
    INSERT INTO TB_TMP_GL_EXT_CELANESE_PROD_2
(Record_ID,company_code,document_date,posting_date,period,YEAR,document_type
,Reference_Code,Currency,Invoice_Description,filler1)
    ( SELECT 'H' as Record_ID,
      vc_gl_company_nbr as company_code,
      TO_CHAR(SYS_DATE,'MM/DD/YYYY') as document_date,
      TO_CHAR(SYS_DATE,'MM/DD/YYYY') as posting_date,
      TO_CHAR(SYS_DATE,'MM') as period,
      TO_CHAR(SYS_DATE,'YYYY') as YEAR,
      'IB' as document_type,
      '' as Reference_Code,
      'USD' as Currency,
      'Burr Wolf I/F '||TO_CHAR(SYS_DATE,'MM/DD/YYYY') as Invoice_Description,
      TO_CHAR(vn_gl_company_count)||'0' as filler1
        );

     -- 2. Detail debit
	 --    Grouping was added on 2/11/2004 due to too many identical records

    INSERT INTO TB_TMP_GL_EXT_CELANESE_PROD_2
(Record_ID,company_code,Posting_key,Plant,Cost_Center,Account_Number,filler1
,tax_code,filler2,filler3,profit_center,Orders,
Allocation,Amount,filler4,WBS_Element,filler5,Network,Activity)
    (SELECT 'D'  as Record_ID,
         vc_gl_company_nbr as company_code,
         '40' as Posting_Key,
         gl_division_nbr as Plant,
         gl_cc_nbr_dept_id as Cost_Center ,
--         '8113401' Account_Number,
--         GL_LOCAL_ACCT_NBR Account_Number,
         CASE GL_LOCAL_ACCT_NBR WHEN 'C43CST' THEN '0008104001' WHEN 'C43COT' THEN '0008104001' ELSE GL_LOCAL_ACCT_NBR END as Account_Number,
         TO_CHAR(vn_gl_company_count)||'1' as filler1,
         '' as taxcode,
         '' as filler2,
         '' as filler3,
         USER_TEXT_07 as profit_center,
         WO_NBR as Orders,
         INVOICE_NBR as Allocation,
         SUM(TB_CALC_TAX_AMT) as Amount,
         '' as filler4,
         AFE_PROJECT_NBR as WBS_Element,  -- put it back on 08/31/2004 for Robert
--         NULL WBS_Element,  --- taking off on 08/31/2004 for Robert
         '' as filler5,
         USER_TEXT_09 as Network,
		 USER_TEXT_18 as Activity
         FROM TB_TMP_GL_EXPORT
         WHERE gl_company_nbr = vc_gl_company_nbr
         AND TB_CALC_TAX_AMT >= 0
		 GROUP BY vc_gl_company_nbr,gl_division_nbr,gl_cc_nbr_dept_id,GL_LOCAL_ACCT_NBR, TO_CHAR(vn_gl_company_count)||'1',USER_TEXT_07,WO_NBR,INVOICE_NBR, AFE_PROJECT_NBR,USER_TEXT_09,USER_TEXT_18);
--COMMIT;
         -- 3. Detail credit
		 --    Grouping was added on 2/11/2004 due to too many identical records

    INSERT INTO TB_TMP_GL_EXT_CELANESE_PROD_2
    (Record_ID,company_code,Posting_key,Plant,Cost_Center,Account_Number,filler1
    ,tax_code,filler2,filler3,profit_center,Orders,
     Allocation,Amount,filler4,WBS_Element,filler5,Network,Activity)
    (SELECT 'D'  as Record_ID,
     vc_gl_company_nbr as company_code,
         '50' as Posting_Key,
         gl_division_nbr as Plant,
         gl_cc_nbr_dept_id as Cost_Center ,
--         '8113401' Account_Number,
--         GL_LOCAL_ACCT_NBR Account_Number,
         CASE GL_LOCAL_ACCT_NBR WHEN 'C43CST' THEN '0008104001' WHEN 'C43COT' THEN '0008104001' ELSE GL_LOCAL_ACCT_NBR END as Account_Number,
         TO_CHAR(vn_gl_company_count)||'2' as filler1,
         '' as taxcode,
         '' as filler2,
         '' as filler3,
         USER_TEXT_07 as profit_center,
         WO_NBR as Orders,
         INVOICE_NBR as Allocation,
         - SUM(TB_CALC_TAX_AMT) as Amount,
         '' as filler4,
         AFE_PROJECT_NBR as WBS_Element,  -- put it back on 08/31/2004 for Robert
--         NULL WBS_Element,  --- taking off on 08/31/2004 for Robert
         '' as filler5,
         USER_TEXT_09 as Network,
 		 USER_TEXT_18 as Activity
         FROM TB_TMP_GL_EXPORT
         WHERE gl_company_nbr = vc_gl_company_nbr
         AND TB_CALC_TAX_AMT < 0
 		 GROUP BY vc_gl_company_nbr,gl_division_nbr,gl_cc_nbr_dept_id,GL_LOCAL_ACCT_NBR, TO_CHAR(vn_gl_company_count)||'1',USER_TEXT_07,WO_NBR,INVOICE_NBR, AFE_PROJECT_NBR,USER_TEXT_09,USER_TEXT_18);

      -- 4. Detail credit offset by gl_division_nbr
     INSERT INTO TB_TMP_GL_EXT_CELANESE_PROD_2
	 (Record_ID,company_code,Posting_key,Plant,Account_Number,filler1,Amount,profit_center)
     (SELECT 'D'  as Record_ID,
      vc_gl_company_nbr as company_code,
          '50' as Posting_Key,
          gl_division_nbr as Plant,
          '0004304001' as Account_Number,
          TO_CHAR(vn_gl_company_count)||'3' as filler1,
      SUM( CASE SIGN(TB_CALC_TAX_AMT) WHEN  1 THEN TB_CALC_TAX_AMT  ELSE 0 END) - 
      SUM( CASE SIGN(TB_CALC_TAX_AMT) WHEN -1 THEN -TB_CALC_TAX_AMT ELSE 0 END) as amount,
	  vc_gl_company_nbr||'-TAX' as profit_center
--      SUM( DECODE(Posting_Key,'40',TB_CALC_TAX_AMT,0)) - SUM(DECODE(Posting_Key,'50',TB_CALC_TAX_AMT,0)) amount
          FROM TB_TMP_GL_EXPORT
          WHERE gl_company_nbr = vc_gl_company_nbr
          AND TB_CALC_TAX_AMT <> 0
          GROUP BY gl_division_nbr);






-- debug

/*     INSERT INTO DEBUG
	 (Record_ID,company_code,Posting_key,Plant,Account_Number,filler1,Amount,profit_center,postive,negtive)
     (SELECT 'B'  Record_ID,
      vc_gl_company_nbr company_code,
          '50' Posting_Key,
          gl_division_nbr Plant,
          '0004304001' Account_Number,
          TO_CHAR(vn_gl_company_count)||'3' filler1,
      SUM( DECODE(SIGN(TB_CALC_TAX_AMT),1,TB_CALC_TAX_AMT,0)) - SUM(DECODE(SIGN(TB_CALC_TAX_AMT),-1,-TB_CALC_TAX_AMT,0)) amount,
	  vc_gl_company_nbr||'-TAX' profit_center,
	  SUM( DECODE(SIGN(TB_CALC_TAX_AMT),1,TB_CALC_TAX_AMT,0)),
	  SUM(DECODE(SIGN(TB_CALC_TAX_AMT),-1,-TB_CALC_TAX_AMT,0))
          FROM TB_TMP_GL_EXPORT
          WHERE gl_company_nbr = vc_gl_company_nbr
          AND TB_CALC_TAX_AMT <> 0
          GROUP BY gl_division_nbr); */
-- debug

      vn_gl_company_count := vn_gl_company_count + 1;


--  END LOOP;

-- 01/18/2007 changing business rule for Judy McFaul (Celanese) via Tim Kirkpatrick
-- 1. Replace all GL_LOCAL_ACCT_NBR with value  '0008117098' where it's '0008117099'
-- 2. GL_LOCAL_ACCT_NBR is mapped to account_number in TB_TMP_GL_EXT_CELANESE_PROD_2

   UPDATE  TB_TMP_GL_EXT_CELANESE_PROD_2
   SET account_number = '0008117098'
   WHERE account_number = '0008117099';


  --COMMIT;

/*  Finding the delta between 40s and 50s (only for those 50 that exist) and insert a offset row for 50s */
      OPEN cur_gl_40;
      LOOP
		  FETCH cur_gl_40 INTO gl_rec_40;
		
		  IF (NOT FOUND) THEN 
		    EXIT;
		  END IF;

                  vn_gl_sums := gl_rec_40.sums;
                  vc_gl_plant := gl_rec_40.plant;
                  vc_gl_company_nbr := gl_rec_40.company_code;

                  SELECT COUNT(*) INTO STRICT vn_row_count
                  FROM TB_TMP_GL_EXT_CELANESE_PROD_2
                  WHERE posting_key = '50'
                  AND account_number <>'0004304001'
                  AND plant = vc_gl_plant
                  AND company_code = vc_gl_company_nbr;

--				 insert into t values(vc_gl_company_nbr,vc_gl_plant,vn_gl_sums,vn_gl_sums1,vn_gl_sums-vn_gl_sums1);

                  IF (vn_row_count IS NOT NULL) AND (vn_row_count > 0)  THEN

             	  	 	 SELECT SUM(amount)
						 INTO STRICT vn_gl_sums1
                 		 FROM TB_TMP_GL_EXT_CELANESE_PROD_2
                 		 WHERE posting_key = '50'
                 		 AND account_number <>'0004304001'
                 		 AND plant = vc_gl_plant
                 		 AND company_code = vc_gl_company_nbr;

             			 UPDATE TB_TMP_GL_EXT_CELANESE_PROD_2
             			 SET amount = ( vn_gl_sums - vn_gl_sums1 )
             			 WHERE company_code = vc_gl_company_nbr
                         AND plant = vc_gl_plant
             			 AND posting_key = '50'
             			 AND account_number = '0004304001';

                  END IF;

      END LOOP;
      CLOSE cur_gl_40;
      

      --COMMIT;

   /* If the offset for 50s has negtive amount, set the amount to possitive and change the posting key to 40 */
   UPDATE  TB_TMP_GL_EXT_CELANESE_PROD_2
   SET amount = -amount, posting_key = '40'
   WHERE SIGN(amount) = -1
   AND posting_key = '50'
   AND account_number = '0004304001';

   /* Below 3 lines is to implement the logic of WBS_Element via Robert's request on 08/31/2004 */
   	UPDATE TB_TMP_GL_EXT_CELANESE_PROD_2
	SET 	WBS_Element = NULL
	WHERE 	Network IS NOT NULL;

   --COMMIT;

   /*Finding the final balance between 40s and 50s (even for the 50s that don't exist) AND UPDATE the offset */
      
      OPEN cur_gl_40;
      LOOP
		  FETCH cur_gl_40 INTO gl_rec_40;
		  	
		  IF (NOT FOUND) THEN 
		    EXIT;
		  END IF;

                  vn_gl_sums := gl_rec_40.sums; /* Sum for 40 */
                  vc_gl_plant := gl_rec_40.plant;
                  vc_gl_company_nbr := gl_rec_40.company_code;

				  /* Get sum for 50 */
                  SELECT SUM(amount) INTO vn_gl_sums1
                  FROM TB_TMP_GL_EXT_CELANESE_PROD_2
                  WHERE posting_key = '50'
                  AND plant = vc_gl_plant
                  AND company_code = vc_gl_company_nbr;

                  /* if there is a difference between sum of 40 and sum of 50, do an adjustment */
				  IF vn_gl_sums1 <> vn_gl_sums THEN

					 UPDATE TB_TMP_GL_EXT_CELANESE_PROD_2
        		  	 SET amount = amount + ( vn_gl_sums - vn_gl_sums1 )
        		  	 WHERE company_code = vc_gl_company_nbr
                  	 AND plant = vc_gl_plant
        		  	 AND posting_key = '50'
        		  	 AND account_number = '0004304001';

				  END IF;

      END LOOP;
      CLOSE cur_gl_40;

      --COMMIT;

 IF (execution_mode = 0) THEN

    UPDATE TB_GL_EXPORT_LOG SET gl_extract_flag = NULL
    WHERE gl_extract_flag = 0;
    -- COMMIT;

 ELSIF (execution_mode = 1 ) THEN

 -- finding the total sum fortax amount by using the posting_key 50


  --fHandle := UTL_FILE.FOPEN(vc_gl_export_path,file_name,'w');

    EXECUTE 'CREATE TEMPORARY SEQUENCE sp_gl_extract_seq';
    EXECUTE 'CREATE TEMPORARY TABLE sp_gl_extract_tmp (order_id integer, content text) ON COMMIT PRESERVE ROWS';

    OPEN cur_gl;
    LOOP

	  FETCH cur_gl INTO gl_rec;

	  IF (NOT FOUND) THEN 
	    EXIT;
	  END IF;


--   	   vn_NETWORK := gl_rec.NETWORK ;
--
-- 	   IF (vn_NETWORK IS NOT NULL) THEN
-- 	           vn_WBS_ELEMENT := '';
-- 	   ELSE
--  		   	   vn_WBS_ELEMENT := gl_rec.WBS_ELEMENT;
-- 	   END IF;

       IF SUBSTR(gl_rec.filler1,2,1) = '0' THEN

           pl_line :=  gl_rec.RECORD_ID||CHR(9)||
                       gl_rec.COMPANY_CODE||CHR(9)||
                       gl_rec.POSTING_DATE||CHR(9)||
                       gl_rec.DOCUMENT_DATE||CHR(9)||
                       gl_rec.PERIOD||CHR(9)||
                       gl_rec.YEAR||CHR(9)||
                       gl_rec.DOCUMENT_TYPE||CHR(9)||
                       gl_rec.REFERENCE_CODE||CHR(9)||
                       gl_rec.CURRENCY||CHR(9)||
                       gl_rec.INVOICE_DESCRIPTION;

           --sys.utl_file.put_line(fHandle, pl_line );

	   INSERT INTO sp_gl_extract_tmp VALUES (NEXTVAL('sp_gl_extract_seq'), pl_line);

           ELSE
           pl_line :=  gl_rec.RECORD_ID||CHR(9)||
                       gl_rec.POSTING_KEY||CHR(9)||
                       gl_rec.PLANT||CHR(9)||
                       gl_rec.COST_CENTER||CHR(9)||
                       gl_rec.ACCOUNT_NUMBER||CHR(9)||
                       ''||CHR(9)||
                       gl_rec.TAX_CODE||CHR(9)||
                       gl_rec.FILLER2||CHR(9)||
                       gl_rec.FILLER3||CHR(9)||
                       gl_rec.PROFIT_CENTER||CHR(9)||
                       gl_rec.ORDERS||CHR(9)||
                       gl_rec.ALLOCATION||CHR(9)||
                       gl_rec.AMOUNT||CHR(9)||
                       gl_rec.FILLER4||CHR(9)||
                       gl_rec.WBS_ELEMENT||CHR(9)||
                       gl_rec.FILLER5||CHR(9)||
                       gl_rec.NETWORK||CHR(9)||
					   gl_rec.ACTIVITY;

          --sys.utl_file.put_line(fHandle, pl_line );

	  INSERT INTO sp_gl_extract_tmp VALUES (NEXTVAL('sp_gl_extract_seq'), pl_line);

       END IF;

     END LOOP;
     CLOSE cur_gl;

     EXECUTE copy_to('sp_gl_extract_tmp', vc_gl_export_path||'/'||file_name);

  --UTL_FILE.FCLOSE(fHandle);

  sys_date := CURRENT_TIMESTAMP;

  -- Select flag value from tb_option table to see if the marking is enabled.
  SELECT value INTO STRICT v_marking_flag
  FROM TB_OPTION
  WHERE option_type_code = 'ADMIN'
  AND user_code = 'ADMIN'
  AND option_code = 'GLEXTRACTMARK';

  -- The following codes mark the GL_EXTRACT_TIMESTAMP with the sysdate
  -- It will activated by the flag value from tb_option table

  -- 1. Mark Gl Extract for TB_TRANSACTION_DETAIL table
  IF v_marking_flag = '1' THEN

     SELECT NEXTVAL('sq_gl_extract_batch_id') INTO STRICT vn_gl_batch_id ;

     UPDATE TB_TMP_GL_EXPORT
     SET gl_extract_timestamp = sys_date,gl_extract_flag = 1,gl_extract_batch_no = vn_gl_batch_id;

     INSERT INTO TB_GL_REPORT_LOG
     SELECT * FROM TB_TMP_GL_EXPORT;
     --COMMIT;
      -- The following codes mark the GL_EXTRACT_TIMESTAMP with the sysdate

     EXECUTE 'Alter table tb_transaction_detail disable trigger all';

     UPDATE TB_TRANSACTION_DETAIL
     SET GL_EXTRACT_TIMESTAMP = sys_date, GL_EXTRACT_UPDATER = CURRENT_USER,GL_EXTRACT_FLAG = 1, gl_extract_batch_no = vn_gl_batch_id
     WHERE transaction_detail_id IN (SELECT DISTINCT transaction_detail_id FROM TB_TMP_GL_EXPORT);
  	 COMMIT;

     EXECUTE 'Alter table tb_transaction_detail enable trigger all';

     -- 2. Mark Gl Extract for TB_GL_EXPORT_LOG table
     UPDATE TB_GL_EXPORT_LOG
     SET GL_EXTRACT_TIMESTAMP = sys_date, GL_EXTRACT_UPDATER = CURRENT_USER,gl_extract_flag = 1,gl_extract_batch_no = vn_gl_batch_id
     WHERE gl_extract_flag = 0;


	   /* GL Extract TB_BATCH Logging Meta Data
    -------------------------------------------------------------------------------------
    | TB_BATCH column name  | VALUES/Meaning											 |
    -------------------------------------------------------------------------------------
     TB_BATCH COLUMN name	  VALUES/Meaning
     batch_type_code 		  'GE'
     entry_timestamp		  GL Creation DATE
     batch_status_code		  'E'
     held_flag				  0
     vc01					  GL extract FILE name AND FILE path
     vc02					  GL extract FILE name
     ts01					  GL extract through DATE
     nu01					  GL extract batch no
    ------------------------------------------------------------------------------------- */


     INSERT INTO TB_BATCH
     (batch_type_code,
      entry_timestamp,
      batch_status_code,
      held_flag,
      vc01,
      vc02,
      ts01,
      nu01)
     VALUES (
        'GE',
         sys_date,
         'E',
	 0,
	 vc_gl_export_path||'/'||file_name,
 	 file_name,
         vd_end_date,
         vn_gl_batch_id
     );

     --COMMIT;


  END IF;

END IF;

  return_code := 0;

/*
EXCEPTION WHEN UTL_FILE.INVALID_PATH THEN
   RAISE_APPLICATION_ERROR(-20100,'Invalid Path');
WHEN UTL_FILE.INVALID_MODE THEN
   RAISE_APPLICATION_ERROR(-20101,'Invalid MODE');
WHEN UTL_FILE.INVALID_OPERATION THEN
   RAISE_APPLICATION_ERROR(-20102,'Invalid Operation');
WHEN UTL_FILE.INVALID_FILEHANDLE THEN
   RAISE_APPLICATION_ERROR(-20103,'Invalid Filehandle');
WHEN UTL_FILE.WRITE_ERROR THEN
   RAISE_APPLICATION_ERROR(-20104,'WRITE Error');
WHEN UTL_FILE.READ_ERROR THEN
   RAISE_APPLICATION_ERROR(-20105,'READ Error');
WHEN UTL_FILE.INTERNAL_ERROR THEN
   RAISE_APPLICATION_ERROR(-20106,'Internal Error');
WHEN OTHERS THEN
   return_code := SQLCODE;
   UTL_FILE.FCLOSE(fHandle);
*/

END;
$$ LANGUAGE plpgsql;

