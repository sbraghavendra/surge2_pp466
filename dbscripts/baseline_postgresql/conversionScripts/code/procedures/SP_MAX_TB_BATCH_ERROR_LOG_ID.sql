CREATE OR REPLACE FUNCTION sp_max_tb_batch_error_log_id() RETURNS VOID AS $$
DECLARE 
   v_max_id numeric;
   v_count numeric;
BEGIN
   SELECT MAX( batch_error_log_id )
   INTO STRICT v_max_id
   FROM tb_batch_error_log;

   IF v_max_id IS NULL THEN
      v_max_id := 1;
   ELSE
      v_max_id := v_max_id + 1;
   END IF;

   SELECT count(*)
   INTO STRICT v_count
   FROM information_schema.sequences
   WHERE sequence_name = 'sq_tb_batch_error_log_id'
   AND sequence_schema = 'stscorp';

   IF v_count > 0 THEN
      EXECUTE 'DROP SEQUENCE stscorp.sq_tb_batch_error_log_id';
   END IF;

   EXECUTE 'CREATE SEQUENCE stscorp.sq_tb_batch_error_log_id INCREMENT BY 1 START WITH 1 NO MAXVALUE MINVALUE 1 NO CYCLE';

   EXECUTE 'GRANT SELECT ON stscorp.sq_tb_batch_error_log_id TO STSUSER';
END;
$$ LANGUAGE plpgsql;
