CREATE OR REPLACE FUNCTION sp_calcusetax(
   an_gl_line_itm_dist_amt in numeric,
   an_invoice_freight_amt in numeric,
   an_invoice_discount_amt in numeric,
   av_transaction_state_code in varchar,
   av_import_definition_code in varchar,
   av_taxcode_code in varchar,
   an_state_rate in numeric,
   an_state_use_tier2_rate in numeric,
   an_state_use_tier3_rate in numeric,
   an_state_split_amount in numeric,
   an_state_tier2_min_amount in numeric,
   an_state_tier2_max_amount in numeric,
   an_state_maxtax_amount in numeric,
   an_county_rate in numeric,
   an_county_local_rate in numeric,
   an_county_split_amount in numeric,
   an_county_maxtax_amount in numeric,
   ac_county_single_flag in char,
   ac_county_default_flag in char,
   an_city_rate in numeric,
   an_city_local_rate in numeric,
   an_city_split_amount in numeric,
   an_city_split_rate in numeric,
   ac_city_single_flag in char,
   ac_city_default_flag in char,
   an_state_amount out numeric,
   an_state_use_tier2_amount out numeric,
   an_state_use_tier3_amount out numeric,
   an_county_amount out numeric,
   an_county_local_amount out numeric,
   an_city_amount out numeric,
   an_city_local_amount out numeric,
   an_tb_calc_tax_amt out numeric,
   an_taxable_amt out numeric ) RETURNS RECORD AS $$
DECLARE
   v_taxable_amt                   tb_transaction_detail.gl_line_itm_dist_amt%TYPE   := 0;
   v_sub_amt                       tb_transaction_detail.gl_line_itm_dist_amt%TYPE   := 0;
   v_pct_dist_amt                  tb_taxcode.pct_dist_amt%TYPE                      := 0.00;
   v_state_use_tier1_rate          tb_jurisdiction_taxrate.state_use_rate%TYPE       := 0;
   v_state_use_tier2_rate          tb_jurisdiction_taxrate.state_use_tier2_rate%TYPE := 0;
   v_state_use_tier3_rate          tb_jurisdiction_taxrate.state_use_tier3_rate%TYPE := 0;
   v_state_tier1_dist_amt          tb_transaction_detail.gl_line_itm_dist_amt%TYPE   := 0;
   v_state_tier2_dist_amt          tb_transaction_detail.gl_line_itm_dist_amt%TYPE   := 0;
   v_state_tier3_dist_amt          tb_transaction_detail.gl_line_itm_dist_amt%TYPE   := 0;
   v_state_tier1_max_amount        tb_jurisdiction_taxrate.state_tier2_min_amount%TYPE := 0;
   v_state_tier2_min_amount        tb_jurisdiction_taxrate.state_tier2_min_amount%TYPE := 0;
   v_state_tier2_max_amount        tb_jurisdiction_taxrate.state_tier2_max_amount%TYPE := 0;
   v_state_tier3_min_amount        tb_jurisdiction_taxrate.state_tier2_max_amount%TYPE := 0;
   v_state_maxtax_amount           tb_jurisdiction_taxrate.state_maxtax_amount%TYPE  := 0;

   -- TEMPORARY
   v_sub_tax_flags varchar(100);
   v_sub_sep_flags varchar(100);
   vc_value varchar(100);

-- ****** Program starts ****** --------------------------------------------------------------------
BEGIN
   

 -- TEMPORARY -- Hard Code Flags
 BEGIN
   SELECT value
     INTO STRICT vc_value
     FROM tb_option
    WHERE UPPER(option_code) = 'PWDENYMSG'
      AND UPPER(option_type_code) = 'SYSTEM'
      AND UPPER(user_code) = 'SYSTEM';
 EXCEPTION
  WHEN OTHERS THEN
   vc_value := 'NoThInG';
 END;
--   IF av_import_definition_code = 'TARGA' THEN
   IF SUBSTR(UPPER(TRIM(vc_value)),1,5) = 'TARGA' THEN
      SELECT CASE av_transaction_state_code WHEN 'LA' THEN 'EE' ELSE 'TE' END
        INTO STRICT v_sub_tax_flags;
      v_sub_sep_flags := 'EI';
   ELSE
      v_sub_tax_flags := 'XX';
      v_sub_sep_flags := 'XX';
   END IF;

   -- Get Percent of Distribution Amount for Tax Basis
   SELECT pct_dist_amt
     INTO STRICT v_pct_dist_amt
     FROM tb_taxcode
    WHERE tb_taxcode.taxcode_code = av_taxcode_code
      AND tb_taxcode.taxcode_type_code = 'T';

   -- Calculate Taxable Amount --
   -- Add/Subtract Freight/Discount
   SELECT an_gl_line_itm_dist_amt +
         (CASE SUBSTR(v_sub_sep_flags,1,1)||SUBSTR(v_sub_tax_flags,1,1)
		WHEN 'IE' THEN
		 COALESCE(an_invoice_freight_amt,0) * -1
		WHEN 'ET' THEN
		 COALESCE(an_invoice_freight_amt,0)
		ELSE 
		 0
	  END
	  ) +
         (CASE SUBSTR(v_sub_sep_flags,2,1)||SUBSTR(v_sub_tax_flags,2,1)
		WHEN 'IE' THEN
		 COALESCE(an_invoice_discount_amt,0) * -1
		WHEN 'ET' THEN
		 COALESCE(an_invoice_discount_amt,0)
		ELSE
		 0
	  END
	 )
    INTO STRICT an_taxable_amt;
   
   -- Apply Percent of Distribution Amount
   IF v_pct_dist_amt IS NOT NULL AND v_pct_dist_amt <> 1.00 AND v_pct_dist_amt <> 0.00 THEN
      an_taxable_amt := an_taxable_amt * v_pct_dist_amt;
   END IF;

   -- Tier 1 Variables --
   v_state_use_tier1_rate := COALESCE(an_state_rate,0);
   v_state_tier1_max_amount := COALESCE(an_state_split_amount,999999999);
   IF v_state_tier1_max_amount = 0 THEN
      v_state_tier1_max_amount := 999999999;
   END IF;
   v_state_tier1_dist_amt := COALESCE(an_taxable_amt,0);
   an_state_amount := 0;

   -- Tier 2 Variables --
   v_state_use_tier2_rate := COALESCE(an_state_use_tier2_rate,0);
   IF v_state_tier1_max_amount < 999999999 THEN
      v_state_tier2_min_amount := v_state_tier1_max_amount;
   ELSE
      v_state_tier2_min_amount := COALESCE(an_state_tier2_min_amount,0);
   END IF;
   v_state_tier2_max_amount := COALESCE(an_state_tier2_max_amount,999999999);
   IF v_state_tier2_max_amount = 0 THEN
      v_state_tier2_max_amount := 999999999;
   END IF;
   IF v_state_tier2_max_amount < 999999999 THEN
      v_state_tier2_max_amount := v_state_tier2_max_amount - v_state_tier2_min_amount;
   END IF;
   v_state_tier2_dist_amt := v_state_tier1_dist_amt - v_state_tier2_min_amount;
   an_state_use_tier2_amount := 0;

   -- Tier 3 Variables --
   v_state_use_tier3_rate := COALESCE(an_state_use_tier3_rate,0);
   v_state_tier3_min_amount := COALESCE(an_state_tier2_max_amount,999999999);
   v_state_tier3_dist_amt := v_state_tier1_dist_amt - v_state_tier3_min_amount;
   an_state_use_tier3_amount := 0;

   v_state_maxtax_amount := COALESCE(an_state_maxtax_amount,999999999);
   IF v_state_maxtax_amount = 0 THEN
      v_state_maxtax_amount := 999999999;
   END IF;
   an_tb_calc_tax_amt := 0;

   -- Calculate State Tiers --
   IF v_state_use_tier1_rate > 0 THEN
      an_state_amount := LEAST(v_state_tier1_dist_amt,v_state_tier1_max_amount) * v_state_use_tier1_rate;
   END IF;
   IF v_state_use_tier2_rate > 0 THEN
      an_state_use_tier2_amount := GREATEST(LEAST(v_state_tier2_dist_amt,v_state_tier2_max_amount) * v_state_use_tier2_rate, 0);
   END IF;
   IF v_state_use_tier3_rate > 0 THEN
      an_state_use_tier3_amount := GREATEST(v_state_tier3_dist_amt * v_state_use_tier3_rate,0);
   END IF;

   -- Determine MAXTAX Amount --
   IF v_state_tier1_max_amount = 999999999 AND v_state_use_tier2_rate = 0 AND v_state_use_tier3_rate = 0 THEN
      an_state_amount := LEAST(an_state_amount, v_state_maxtax_amount);
   END IF;

   -- Calculate County Sales/Use Tax Amount
   IF an_county_split_amount > 0 THEN
      IF an_county_split_amount < an_taxable_amt THEN
         an_county_amount := an_county_split_amount * an_county_rate;
      ELSE
         an_county_amount := an_taxable_amt * an_county_rate;
      END IF;
   ELSE
      an_county_amount := an_taxable_amt * an_county_rate;
   END IF;

   IF an_county_maxtax_amount > 0 THEN
      IF an_county_maxtax_amount < an_county_amount THEN
         an_county_amount := an_county_maxtax_amount;
      END IF;
   END IF;

   -- Calculate County Local Sales/Use Tax Amount
   an_county_local_amount := an_taxable_amt * an_county_local_rate;

   -- Calculate City Sales/Use Tax Amount
   IF an_city_split_amount > 0 THEN
      IF an_city_split_amount < an_taxable_amt THEN
         IF an_city_split_rate > 0 THEN
            an_city_amount :=
               ROUND((an_city_split_amount * an_city_rate) +
               ((an_taxable_amt - an_city_split_amount) * an_city_split_rate),2);
         ELSE
            an_city_amount := an_city_split_amount * an_city_rate;
         END IF;
      ELSE
         an_city_amount := an_taxable_amt * an_city_rate;
      END IF;
   ELSE
      an_city_amount := an_taxable_amt * an_city_rate;
   END IF;

   -- Calculate City Local Sales/Use Tax Amount
   an_city_local_amount := an_taxable_amt * an_city_local_rate;

   -- Calculate Total Tax Amount
   an_tb_calc_tax_amt := an_state_amount +
                         an_state_use_tier2_amount +
                         an_state_use_tier3_amount +
                         an_county_amount +
                         an_county_local_amount +
                         an_city_amount +
                         an_city_local_amount;
END;
$$ LANGUAGE plpgsql;
