CREATE OR REPLACE FUNCTION sp_batch_delete(an_batch_id numeric) RETURNS VOID AS $$
DECLARE

-- Table defined variables
   v_batch_status_code             tb_batch.batch_status_code%TYPE                   := NULL;
   v_abort_import_flag             tb_list_code.abort_import_flag%TYPE               := '0';

-- ****** Program starts ****** --------------------------------------------------------------------
BEGIN
   -- Confirm batch exists and is flagged for processing
   BEGIN
      SELECT batch_status_code
        INTO STRICT v_batch_status_code
        FROM tb_batch
       WHERE batch_id = an_batch_id;
      
      IF v_batch_status_code != 'FD' THEN
         v_abort_import_flag := '1';
      END IF;
   EXCEPTION
      WHEN NO_DATA_FOUND THEN
         v_abort_import_flag := '1';
   END;
   -- Abort Procedure?
   IF v_abort_import_flag = '1' THEN
      RETURN;
   END IF;

   UPDATE tb_batch SET batch_status_code = 'XD' WHERE batch_id = an_batch_id;
   --COMMIT;

   DELETE FROM tb_transaction_detail WHERE process_batch_no = an_batch_id;
   --COMMIT;

   DELETE FROM tb_bcp_transactions WHERE process_batch_no = an_batch_id;
   --COMMIT;

   DELETE FROM tb_bcp_jurisdiction_taxrate WHERE batch_id = an_batch_id;
   --COMMIT;

   UPDATE tb_batch SET batch_status_code = 'D' WHERE batch_id = an_batch_id;
   --COMMIT;
END;
$$ LANGUAGE plpgsql;

