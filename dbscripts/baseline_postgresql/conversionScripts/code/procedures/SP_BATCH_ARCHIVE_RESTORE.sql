CREATE OR REPLACE FUNCTION sp_batch_archive_restore(an_batch_id numeric) RETURNS VOID AS $$
DECLARE
-- Table defined variables
   v_batch_status_code             tb_batch.batch_status_code%TYPE                   := NULL;
   v_error_sev_code                tb_batch.error_sev_code%TYPE                      := ' ';
   v_severity_level                tb_list_code.severity_level%TYPE                  := ' ';
   v_write_import_line_flag        tb_list_code.write_import_line_flag%TYPE          := '1';
   v_abort_import_flag             tb_list_code.abort_import_flag%TYPE               := '0';
   v_sysdate                       tb_batch.actual_start_timestamp%TYPE              := CURRENT_DATE;

-- Program defined variables
   vi_error_count                  INTEGER                                           := 0;
   vi_cursor_id                    INTEGER                                           := 0;
   vi_rows_processed               INTEGER                                           := 0;

-- Program starts ******************************************************************************************
BEGIN
   -- Confirm batch exists and is flagged for processing
   BEGIN
      SELECT batch_status_code
        INTO STRICT v_batch_status_code
        FROM tb_batch
       WHERE batch_id = an_batch_id;
      
      IF v_batch_status_code != 'FAR' THEN
         vi_error_count := vi_error_count + 1;
         EXECUTE sp_geterrorcode('A9', an_batch_id, 'AR', v_sysdate, v_severity_level, v_write_import_line_flag, v_abort_import_flag);
         IF v_severity_level > v_error_sev_code THEN
            v_error_sev_code := v_severity_level;
         END IF;
      END IF;
   EXCEPTION
      WHEN OTHERS THEN
         vi_error_count := vi_error_count + 1;
         EXECUTE sp_geterrorcode('A8', an_batch_id, 'AR', v_sysdate, v_severity_level, v_write_import_line_flag, v_abort_import_flag);
         IF v_severity_level > v_error_sev_code THEN
            v_error_sev_code := v_severity_level;
         END IF;
   END;
   
   -- Abort Procedure?
   IF v_abort_import_flag = '1' THEN
      v_batch_status_code := 'ABAR';
      RAISE EXCEPTION 'Abort Procedure: %', v_batch_status_code;
   END IF;

   -- Update batch as processing
   BEGIN
      UPDATE tb_batch
         SET batch_status_code = 'XAR',
             error_sev_code = '',
             actual_start_timestamp = CURRENT_DATE
       WHERE batch_id = an_batch_id;
   EXCEPTION
      WHEN OTHERS THEN
         vi_error_count := vi_error_count + 1;
         EXECUTE sp_geterrorcode('A1', an_batch_id, 'AR', v_sysdate, v_severity_level, v_write_import_line_flag, v_abort_import_flag);
         IF v_severity_level > v_error_sev_code THEN
            v_error_sev_code := v_severity_level;
         END IF;
   END;
   
   -- Abort Procedure?
   IF v_abort_import_flag = '1' THEN
      v_batch_status_code := 'ABAR';
      RAISE EXCEPTION 'Abort Procedure: %', v_batch_status_code;
   END IF;

   --COMMIT;

   /**  Copy Archive Batch from Archive Trans. Detail table to Restored Trans. Detail table  ******/
   INSERT INTO tb_restore_transaction_dtl
      SELECT *
        FROM tb_archive_transaction_dtl
       WHERE archive_batch_no = an_batch_id;

   /**  Update Batch file  ************************************************************************/
   UPDATE tb_batch
      SET batch_status_code = 'AR',
          actual_end_timestamp = CURRENT_DATE
    WHERE batch_id = an_batch_id;

    --COMMIT;

EXCEPTION
   WHEN OTHERS THEN
      -- Update batch with error codes
      UPDATE tb_batch
         SET batch_status_code = v_batch_status_code,
             error_sev_code = v_error_sev_code
       WHERE batch_id = an_batch_id;
      --COMMIT;
END;
$$ LANGUAGE plpgsql;
