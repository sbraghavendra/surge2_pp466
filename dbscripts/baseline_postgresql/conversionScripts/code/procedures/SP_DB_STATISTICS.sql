CREATE OR REPLACE FUNCTION sp_db_statistics(avc_where in VARCHAR) RETURNS VOID AS $$
DECLARE

-- Statistics Selection SQL
   vc_stat_total                   VARCHAR (1000) :=
      'SELECT /*+ INDEX (tb_transaction_detail IDX_TRANS_DTL_GL_LINE_DIST_AMT) */ count(*), sum(gl_line_itm_dist_amt), sum(tb_calc_tax_amt) ' ||
      'FROM tb_transaction_detail, tb_jurisdiction ';
   
   vc_stat_select                  VARCHAR (1000) :=
      'SELECT /*+ INDEX (tb_transaction_detail IDX_TRANS_DTL_GL_LINE_DIST_AMT) */ count(*), sum(gl_line_itm_dist_amt), sum(tb_calc_tax_amt), max(gl_line_itm_dist_amt), avg(gl_line_itm_dist_amt) ' ||
      'FROM tb_transaction_detail, tb_jurisdiction ';
   
   vc_stat_where_init              VARCHAR (100)  :=
      '( tb_transaction_detail.jurisdiction_id = tb_jurisdiction.jurisdiction_id(+) )';
   
   vc_stat_where                   VARCHAR (4000) := '';
   vc_stat_stmt                    VARCHAR (5000);

-- Driver Reference Insert SQL
   vc_driver_ref_ins_stmt          VARCHAR (3000);
   vn_status                       NUMERIC                                    := NULL;

-- Table defined variables
   v_count_total                   tb_tmp_statistics.count%TYPE              := 0;
   v_sum_total                     tb_tmp_statistics.sum%TYPE                := 0;
   v_sum_tax                       tb_tmp_statistics.sum%TYPE                := 0;
   v_count                         tb_tmp_statistics.count%TYPE              := 0;
   v_count_pct                     tb_tmp_statistics.count_pct%TYPE          := 0;
   v_sum                           tb_tmp_statistics.sum%TYPE                := 0;
   v_sum_pct                       tb_tmp_statistics.sum_pct%TYPE            := 0;
   v_tax_amt                       tb_tmp_statistics.tax_amt%TYPE            := 0;
   v_max                           tb_tmp_statistics.max%TYPE                := 0;
   v_avg                           tb_tmp_statistics.avg%TYPE                := 0;

-- Define Statistics Cursor Record  (tb_db_statistics)
   db_statistics                   RECORD;

-- Program starts **********************************************************************************
BEGIN

   -- Clean up tb_tmp_statistis table
   EXECUTE 'TRUNCATE TABLE tb_tmp_statistics';

   -- Get Totals for Percents-of-totals ------------------------------------------------------------
   -- Initialize fields
   vc_stat_where := '';
   vc_stat_stmt := '';

   -- Check input parameter for where clause
   IF avc_where IS NULL THEN
      vc_stat_where := 'WHERE ' || vc_stat_where_init;
   ELSE
      vc_stat_where := avc_where || ' AND ' || vc_stat_where_init;
   END IF;

   -- Build SQL Statement
   vc_stat_stmt := vc_stat_total || vc_stat_where;

   -- Get Totals
   BEGIN
      EXECUTE vc_stat_stmt INTO STRICT v_count_total, v_sum_total, v_sum_tax;
   EXCEPTION
      WHEN OTHERS THEN
         NULL;
   END;

   -- Read and Process Each Statistics Line  -------------------------------------------------------
   FOR db_statistics IN
	   SELECT row_title, row_where, group_by_on_drilldown
	      FROM tb_db_statistics
	      ORDER BY db_statistics_id
   LOOP
      -- Initialize fields
      vc_stat_where := '';
      vc_stat_stmt := '';

      -- Check input parameter for where clause
      IF avc_where IS NULL THEN
         vc_stat_where := 'WHERE ' || vc_stat_where_init;
      ELSE
         vc_stat_where := avc_where || ' AND ' || vc_stat_where_init;
      END IF;

      -- Check db_statistics table for where clause
      IF db_statistics.row_where IS NOT NULL THEN
         vc_stat_where := vc_stat_where || ' AND ' || db_statistics.row_where;
      END IF;

      -- Build SQL Statement
      vc_stat_stmt := vc_stat_select || vc_stat_where;

      -- Get Transactions
      BEGIN
         EXECUTE vc_stat_stmt INTO STRICT v_count, v_sum, v_tax_amt, v_max, v_avg;
      EXCEPTION
         WHEN OTHERS THEN
            NULL;
      END;

      -- Calculate percents-of-totals
	  IF v_count_total <> 0 THEN
         v_count_pct := (v_count/v_count_total)*100;
      ELSE
         v_count_pct := 0;
      END IF;

      IF v_sum_total <> 0 THEN
         v_sum_pct := (v_sum/v_sum_total)*100;
      ELSE
         v_sum_pct := 0;
      END IF;

      -- Insert tmp statistics detail row
      INSERT INTO tb_tmp_statistics (
         title,
         count,
         count_pct,
         sum,
         sum_pct,
         tax_amt,
         max,
         avg,
         where_clause,
         group_by_on_drilldown )
      VALUES (
         db_statistics.row_title,
         v_count,
         v_count_pct,
         v_sum,
         v_sum_pct,
         v_tax_amt,
         v_max,
         v_avg,
         vc_stat_where,
         db_statistics.group_by_on_drilldown );

   END LOOP;

   --COMMIT;

END;
$$ LANGUAGE plpgsql;
