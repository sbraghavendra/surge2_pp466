CREATE OR REPLACE FUNCTION Sp_Random_Trans (
	start_date IN TIMESTAMP, 
	end_date IN TIMESTAMP, 
	sample_row_count IN NUMERIC, 
	trans1 IN OUT REFCURSOR) RETURNS REFCURSOR AS $$
DECLARE
	vn_total_rows NUMERIC;
	vn_sample_rate NUMERIC;
BEGIN
	SELECT COUNT(*) INTO STRICT vn_total_rows FROM TB_TRANSACTION_DETAIL;

	IF (sample_row_count < vn_total_rows) AND ( vn_total_rows > 0 ) THEN

	   vn_sample_rate:= (sample_row_count/vn_total_rows) * 100;
	
	   -- Drop the sequence in case it already exists
	   BEGIN
	    EXECUTE 'DROP SEQUENCE sp_random_trans_seq';
	   EXCEPTION WHEN OTHERS THEN NULL;
	   END;

	   -- Recreate sequence.  Note this is a temp sequence being used as a workaround.
	   -- The original Oracle version of the code used the sample(n) function which
	   -- there is no direct implementation of in Oracle.
	   BEGIN
	    EXECUTE 'CREATE TEMP SEQUENCE sp_random_trans_seq';
	   EXCEPTION WHEN OTHERS THEN NULL;
	   END;

	   IF vn_sample_rate <= 10 THEN
	      OPEN trans1 FOR SELECT * FROM (SELECT TTD.*, NEXTVAL('sp_random_trans_seq') as rnum FROM TB_TRANSACTION_DETAIL TTD WHERE gl_date >= start_date AND gl_date <= end_date ) s WHERE s.rnum <= sample_row_count;
	   ELSIF (vn_sample_rate <= 20) AND (vn_sample_rate > 10 ) THEN
	      OPEN trans1 FOR SELECT * FROM (SELECT TTD.*, NEXTVAL('sp_random_trans_seq') as rnum FROM TB_TRANSACTION_DETAIL TTD WHERE gl_date >= start_date AND gl_date <= end_date ) s WHERE s.rnum <= sample_row_count;
	   ELSIF (vn_sample_rate <= 30) AND (vn_sample_rate > 20 ) THEN
	      OPEN trans1 FOR SELECT * FROM (SELECT TTD.*, NEXTVAL('sp_random_trans_seq') as rnum FROM TB_TRANSACTION_DETAIL TTD WHERE gl_date >= start_date AND gl_date <= end_date ) s WHERE s.rnum <= sample_row_count;
	   ELSIF (vn_sample_rate <= 40) AND (vn_sample_rate > 30 ) THEN
	      OPEN trans1 FOR SELECT * FROM (SELECT TTD.*, NEXTVAL('sp_random_trans_seq') as rnum FROM TB_TRANSACTION_DETAIL TTD WHERE gl_date >= start_date AND gl_date <= end_date ) s WHERE s.rnum <= sample_row_count;
	   ELSIF (vn_sample_rate <= 50) AND (vn_sample_rate > 40 ) THEN
	      OPEN trans1 FOR SELECT * FROM (SELECT TTD.*, NEXTVAL('sp_random_trans_seq') as rnum FROM TB_TRANSACTION_DETAIL TTD WHERE gl_date >= start_date AND gl_date <= end_date ) s WHERE s.rnum <= sample_row_count;
	   ELSIF (vn_sample_rate <= 60) AND (vn_sample_rate > 50 ) THEN
	      OPEN trans1 FOR SELECT * FROM (SELECT TTD.*, NEXTVAL('sp_random_trans_seq') as rnum FROM TB_TRANSACTION_DETAIL TTD WHERE gl_date >= start_date AND gl_date <= end_date ) s WHERE s.rnum <= sample_row_count;
	   ELSIF (vn_sample_rate <= 70) AND (vn_sample_rate > 60 ) THEN
	      OPEN trans1 FOR SELECT * FROM (SELECT TTD.*, NEXTVAL('sp_random_trans_seq') as rnum FROM TB_TRANSACTION_DETAIL TTD WHERE gl_date >= start_date AND gl_date <= end_date ) s WHERE s.rnum <= sample_row_count;
	   ELSIF (vn_sample_rate <= 80) AND (vn_sample_rate > 70 ) THEN
	      OPEN trans1 FOR SELECT * FROM (SELECT TTD.*, NEXTVAL('sp_random_trans_seq') as rnum FROM TB_TRANSACTION_DETAIL TTD WHERE gl_date >= start_date AND gl_date <= end_date ) s WHERE s.rnum <= sample_row_count;
	   ELSIF (vn_sample_rate <= 100) AND (vn_sample_rate > 80 ) THEN
	      OPEN trans1 FOR SELECT * FROM (SELECT TTD.*, NEXTVAL('sp_random_trans_seq') as rnum FROM TB_TRANSACTION_DETAIL TTD WHERE gl_date >= start_date AND gl_date <= end_date ) s WHERE s.rnum <= sample_row_count;
	   END IF;
	ELSE
	   OPEN trans1 FOR SELECT * FROM (SELECT TTD.*, NEXTVAL('sp_random_trans_seq') as rnum FROM TB_TRANSACTION_DETAIL TTD WHERE gl_date >= start_date AND gl_date <= end_date ) s WHERE s.rnum <= sample_row_count;
	END IF;
END;
$$ LANGUAGE plpgsql;

