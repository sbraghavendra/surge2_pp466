CREATE OR REPLACE FUNCTION sp_batch() RETURNS VOID AS $$
DECLARE
   vi_running                      integer                                           := 0;
   vc_cursor_open                  char(1)                                           := 'N';
   vdt_sysdate                     TIMESTAMP(0)                                      := CURRENT_TIMESTAMP;

batch_cursor CURSOR 
FOR
   SELECT batch_id,
          batch_type_code,
          batch_status_code
    FROM tb_batch
   WHERE ( batch_status_code LIKE 'F%' ) AND
         ( held_flag IS NULL OR held_flag <> '1' ) AND
         ( sched_start_timestamp IS NULL OR sched_start_timestamp <= vdt_sysdate )
ORDER BY sched_start_timestamp, batch_id;

batch_record                       RECORD;

-- ****** Program starts ****** --------------------------------------------------------------------
BEGIN
   SELECT count(*)
     INTO STRICT vi_running
     FROM tb_batch
    WHERE batch_status_code LIKE 'X%';

   IF vi_running = 0 THEN
      LOOP
         IF vc_cursor_open = 'Y' THEN
            CLOSE batch_cursor;
            vc_cursor_open := 'N';
         END IF;

         OPEN batch_cursor;
         vc_cursor_open := 'Y';
         
	 FETCH batch_cursor into batch_record;
         
	 IF (NOT FOUND) THEN
	   EXIT;
	 END IF;

         IF batch_record.batch_status_code = 'FI' THEN
--            sp_batch_import(batch_record.batch_id);
            NULL;
         ELSIF batch_record.batch_status_code = 'FP' THEN
            EXECUTE sp_batch_process(batch_record.batch_id);
         ELSIF batch_record.batch_status_code = 'FD' THEN
            EXECUTE sp_batch_delete(batch_record.batch_id);
         ELSIF batch_record.batch_status_code = 'FA' THEN
            EXECUTE sp_batch_archive(batch_record.batch_id);
         ELSIF batch_record.batch_status_code = 'FAR' THEN
            EXECUTE sp_batch_archive_restore(batch_record.batch_id);
         ELSIF batch_record.batch_status_code = 'FADR' THEN
            EXECUTE sp_batch_archive_delete(batch_record.batch_id);
         ELSIF batch_record.batch_status_code = 'FE' THEN
--            sp_batch_extract(batch_record.batch_id);
            NULL;
         ELSIF batch_record.batch_status_code = 'FTR' THEN
            EXECUTE sp_batch_taxrate_update(batch_record.batch_id);
         ELSE
            UPDATE tb_batch
               SET batch_status_code = 'ERR'
             WHERE batch_id = batch_record.batch_id;
            --COMMIT;
         END IF;
      END LOOP;
      CLOSE batch_cursor;
   END IF;
END;
$$ LANGUAGE plpgsql;

