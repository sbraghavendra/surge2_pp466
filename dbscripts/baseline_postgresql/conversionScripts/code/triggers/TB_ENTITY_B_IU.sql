CREATE OR REPLACE FUNCTION tb_entity_b_iu() RETURNS TRIGGER AS $$
BEGIN
	IF INSERTING THEN
		IF NEW.entity_code = '*ALL' THEN
		 NEW.entity_id := 0;
		ELSE
		 SELECT NEXTVAL('sq_tb_entity_id')
		   INTO STRICT NEW.entity_id;
		END IF;
	END IF;

	NEW.update_user_id	:= CURRENT_USER;
	NEW.update_timestamp	:= CURRENT_DATE;
	
	RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER tb_entity_b_iu
BEFORE INSERT OR UPDATE ON tb_entity
FOR EACH ROW
EXECUTE PROCEDURE tb_entity_b_iu();

