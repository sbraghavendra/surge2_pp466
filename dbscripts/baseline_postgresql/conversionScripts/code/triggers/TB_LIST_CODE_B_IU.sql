CREATE OR REPLACE FUNCTION tb_list_code_b_iu() RETURNS TRIGGER AS $$
BEGIN
	NEW.update_user_id	:= CURRENT_USER;
	NEW.update_timestamp	:= CURRENT_DATE;
	
	RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER tb_list_code_b_iu
BEFORE INSERT OR UPDATE ON tb_list_code
FOR EACH ROW
EXECUTE PROCEDURE tb_list_code_b_iu();

