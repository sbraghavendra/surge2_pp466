CREATE OR REPLACE FUNCTION tb_jurisdiction_b_iu() RETURNS TRIGGER AS $$
BEGIN
	IF INSERTING THEN
		SELECT NEXTVAL('sq_tb_jurisdiction_id')
		INTO STRICT NEW.jurisdiction_id;
	END IF;

	NEW.update_user_id	:= CURRENT_USER;
	NEW.update_timestamp	:= CURRENT_DATE;
	
	RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER tb_jurisdiction_b_iu
BEFORE INSERT OR UPDATE ON tb_jurisdiction
FOR EACH ROW
EXECUTE PROCEDURE tb_jurisdiction_b_iu();

