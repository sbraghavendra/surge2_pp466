CREATE OR REPLACE FUNCTION tb_transaction_detail_b_i() RETURNS TRIGGER AS $$
BEGIN
	NEW.update_user_id	:= CURRENT_USER;
	NEW.update_timestamp	:= CURRENT_DATE;
	
	RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER tb_transaction_detail_b_i
BEFORE INSERT ON tb_transaction_detail
FOR EACH ROW
EXECUTE PROCEDURE tb_transaction_detail_b_i();

