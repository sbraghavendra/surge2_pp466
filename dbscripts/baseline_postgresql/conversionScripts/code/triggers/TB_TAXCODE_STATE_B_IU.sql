CREATE OR REPLACE FUNCTION tb_taxcode_state_b_iu() RETURNS TRIGGER AS $$
BEGIN
	NEW.update_user_id	:= CURRENT_USER;
	NEW.update_timestamp	:= CURRENT_DATE;
	
	RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER tb_taxcode_state_b_iu
BEFORE INSERT OR UPDATE ON tb_taxcode_state
FOR EACH ROW
EXECUTE PROCEDURE tb_taxcode_state_b_iu();

