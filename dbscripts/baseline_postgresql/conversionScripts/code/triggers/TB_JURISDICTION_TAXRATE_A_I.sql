CREATE OR REPLACE FUNCTION tb_jurisdiction_taxrate_a_i() RETURNS TRIGGER AS $$
DECLARE
   -- Table defined variables
   v_sysdate                       tb_transaction_detail.load_timestamp%TYPE         := CURRENT_DATE;
   v_taxtype_code                  tb_taxcode.taxtype_code%TYPE                      := NULL;
   v_override_taxtype_code         tb_taxcode.taxtype_code%TYPE                      := NULL;
   v_taxtype_used                  tb_taxcode.taxtype_code%TYPE                      := NULL;

-- temporary
   vn_taxable_amt                  NUMERIC                                            := 0;

   -- Define Transaction Detail Cursor (tb_transaction_detail)
   transaction_detail           RECORD;
   transaction_detail_cursor	REFCURSOR;

-- ****** Program starts ****** --------------------------------------------------------------------
BEGIN
   -- ****** Read and Process Transactions ****** -----------------------------------------
   OPEN transaction_detail_cursor FOR
	SELECT tb_transaction_detail.*
	FROM tb_transaction_detail
	WHERE ( tb_transaction_detail.jurisdiction_id = NEW.jurisdiction_id ) AND
             ( NEW.effective_date <= tb_transaction_detail.gl_date ) AND ( NEW.expiration_date >= tb_transaction_detail.gl_date ) AND
             ( NEW.measure_type_code = tb_transaction_detail.measure_type_code ) AND
             ( tb_transaction_detail.transaction_ind = 'S' ) AND ( tb_transaction_detail.suspend_ind = 'R' )
	FOR UPDATE;
   LOOP
      FETCH transaction_detail_cursor
       INTO transaction_detail;
      
      IF (NOT FOUND) THEN 
	EXIT;
      END IF;

      -- Get Tax Type Codes
      SELECT taxtype_code
        INTO STRICT v_taxtype_code
        FROM tb_taxcode
       WHERE taxcode_code	= transaction_detail.taxcode_code
         AND taxcode_type_code	= transaction_detail.taxcode_type_code;

      SELECT override_taxtype_code
        INTO STRICT v_override_taxtype_code
        FROM tb_location_matrix
       WHERE tb_location_matrix.location_matrix_id = transaction_detail.location_matrix_id;

      -- Determine Tax Type - Use or Sales
      v_taxtype_used := v_override_taxtype_code;
      IF v_taxtype_used is NULL OR TRIM(v_taxtype_used) = '' OR TRIM(v_taxtype_used) = '*NO' THEN
         v_taxtype_used := SUBSTR(TRIM(v_taxtype_code),1,1);
         IF v_taxtype_used is NULL or Trim(v_taxtype_used) = '' THEN
            v_taxtype_used := 'U';
         END IF;
      END IF;

      -- Populate and/or Clear Transaction Detail working fields
      transaction_detail.state_use_amount := 0;
      transaction_detail.state_use_tier2_amount := 0;
      transaction_detail.state_use_tier3_amount := 0;
      transaction_detail.county_use_amount := 0;
      transaction_detail.county_local_use_amount := 0;
      transaction_detail.city_use_amount := 0;
      transaction_detail.city_local_use_amount := 0;
      transaction_detail.tb_calc_tax_amt := 0;
      transaction_detail.gl_extract_amt := 0;
      transaction_detail.update_user_id := CURRENT_USER;
      transaction_detail.update_timestamp := v_sysdate;

      -- Move Rates to parameters
      transaction_detail.transaction_ind := 'P';
      transaction_detail.suspend_ind := NULL;
      transaction_detail.jurisdiction_id := NEW.jurisdiction_id;
      transaction_detail.jurisdiction_taxrate_id := NEW.jurisdiction_taxrate_id;
      transaction_detail.county_split_amount := NEW.county_split_amount;
      transaction_detail.county_maxtax_amount := NEW.county_maxtax_amount;
      transaction_detail.county_single_flag := NEW.county_single_flag;
      transaction_detail.county_default_flag := NEW.county_default_flag;
      transaction_detail.city_split_amount := NEW.city_split_amount;
      transaction_detail.city_single_flag := NEW.city_single_flag;
      transaction_detail.city_default_flag := NEW.city_default_flag;

      IF v_taxtype_used = 'U' THEN
         -- Use Tax Rates
         transaction_detail.state_use_rate := NEW.state_use_rate;
         transaction_detail.state_use_tier2_rate := NEW.state_use_tier2_rate;
         transaction_detail.state_use_tier3_rate := NEW.state_use_tier3_rate;
         transaction_detail.state_split_amount := NEW.state_split_amount;
         transaction_detail.state_tier2_min_amount := NEW.state_tier2_min_amount;
         transaction_detail.state_tier2_max_amount := NEW.state_tier2_max_amount;
         transaction_detail.state_maxtax_amount := NEW.state_maxtax_amount;
         transaction_detail.county_use_rate := NEW.county_use_rate;
         transaction_detail.county_local_use_rate := NEW.county_local_use_rate;
         transaction_detail.city_use_rate := NEW.city_use_rate;
         transaction_detail.city_local_use_rate := NEW.city_local_use_rate;
         transaction_detail.city_split_use_rate := NEW.city_split_use_rate;
      ELSIF v_taxtype_used = 'S' THEN
         -- Sales Tax Rates
         transaction_detail.state_use_rate := NEW.state_sales_rate;
         transaction_detail.state_use_tier2_rate := 0;
         transaction_detail.state_use_tier3_rate := 0;
         transaction_detail.state_split_amount := 0;
         transaction_detail.state_tier2_min_amount := 0;
         transaction_detail.state_tier2_max_amount := 999999999;
         transaction_detail.state_maxtax_amount := 999999999;
         transaction_detail.county_use_rate := NEW.county_sales_rate;
         transaction_detail.county_local_use_rate := NEW.county_local_sales_rate;
         transaction_detail.city_use_rate := NEW.city_sales_rate;
         transaction_detail.city_local_use_rate := NEW.city_local_sales_rate;
         transaction_detail.city_split_use_rate := NEW.city_split_sales_rate;
      ELSE
         -- Unknown!
         transaction_detail.state_use_rate := 0;
         transaction_detail.state_use_tier2_rate := 0;
         transaction_detail.state_use_tier3_rate := 0;
         transaction_detail.state_split_amount := 0;
         transaction_detail.state_tier2_min_amount := 0;
         transaction_detail.state_tier2_max_amount := 999999999;
         transaction_detail.state_maxtax_amount := 999999999;
         transaction_detail.county_use_rate := 0;
         transaction_detail.county_local_use_rate := 0;
         transaction_detail.city_use_rate := 0;
         transaction_detail.city_local_use_rate := 0;
         transaction_detail.city_split_use_rate := 0;
      END IF;

      -- Get Jurisdiction TaxRates and calculate Tax Amounts
      EXECUTE sp_calcusetax(transaction_detail.gl_line_itm_dist_amt,
                    transaction_detail.invoice_freight_amt,
                    transaction_detail.invoice_discount_amt,
                    transaction_detail.transaction_state_code,
                    'importdefn',
                    transaction_detail.taxcode_code,
                    transaction_detail.state_use_rate,
                    transaction_detail.state_use_tier2_rate,
                    transaction_detail.state_use_tier3_rate,
                    transaction_detail.state_split_amount,
                    transaction_detail.state_tier2_min_amount,
                    transaction_detail.state_tier2_max_amount,
                    transaction_detail.state_maxtax_amount,
                    transaction_detail.county_use_rate,
                    transaction_detail.county_local_use_rate,
                    transaction_detail.county_split_amount,
                    transaction_detail.county_maxtax_amount,
                    transaction_detail.county_single_flag,
                    transaction_detail.county_default_flag,
                    transaction_detail.city_use_rate,
                    transaction_detail.city_local_use_rate,
                    transaction_detail.city_split_amount,
                    transaction_detail.city_split_use_rate,
                    transaction_detail.city_single_flag,
                    transaction_detail.city_default_flag,
                    transaction_detail.state_use_amount,
                    transaction_detail.state_use_tier2_amount,
                    transaction_detail.state_use_tier3_amount,
                    transaction_detail.county_use_amount,
                    transaction_detail.county_local_use_amount,
                    transaction_detail.city_use_amount,
                    transaction_detail.city_local_use_amount,
                    transaction_detail.tb_calc_tax_amt,
                    vn_taxable_amt );

      transaction_detail.combined_use_rate := transaction_detail.state_use_rate +
                                              transaction_detail.county_use_rate +
                                              transaction_detail.county_local_use_rate +
                                              transaction_detail.city_use_rate +
                                              transaction_detail.city_local_use_rate;

      -- Check for Taxable Amount -- 3351
      IF vn_taxable_amt  <> transaction_detail.gl_line_itm_dist_amt THEN
         transaction_detail.gl_extract_amt := transaction_detail.gl_line_itm_dist_amt;
         transaction_detail.gl_line_itm_dist_amt := vn_taxable_amt;
      END IF;

      -- Update transaction detail row
      BEGIN
         -- 3411 - add taxable amount & tax type used.
         UPDATE tb_transaction_detail
            SET gl_line_itm_dist_amt = transaction_detail.gl_line_itm_dist_amt,
                tb_calc_tax_amt = transaction_detail.tb_calc_tax_amt,
                state_use_amount = transaction_detail.state_use_amount,
                state_use_tier2_amount = transaction_detail.state_use_tier2_amount,
                state_use_tier3_amount = transaction_detail.state_use_tier3_amount,
                county_use_amount = transaction_detail.county_use_amount,
                county_local_use_amount = transaction_detail.county_local_use_amount,
                city_use_amount = transaction_detail.city_use_amount,
                city_local_use_amount = transaction_detail.city_local_use_amount,
                transaction_ind = transaction_detail.transaction_ind,
                suspend_ind = transaction_detail.suspend_ind,
                jurisdiction_id = transaction_detail.jurisdiction_id,
                jurisdiction_taxrate_id = transaction_detail.jurisdiction_taxrate_id,
                state_use_rate = transaction_detail.state_use_rate,
                state_use_tier2_rate = transaction_detail.state_use_tier2_rate,
                state_use_tier3_rate = transaction_detail.state_use_tier3_rate,
                state_split_amount = transaction_detail.state_split_amount,
                state_tier2_min_amount = transaction_detail.state_tier2_min_amount,
                state_tier2_max_amount = transaction_detail.state_tier2_max_amount,
                state_maxtax_amount = transaction_detail.state_maxtax_amount,
                county_use_rate = transaction_detail.county_use_rate,
                county_local_use_rate = transaction_detail.county_local_use_rate,
                county_split_amount = transaction_detail.county_split_amount,
                county_maxtax_amount = transaction_detail.county_maxtax_amount,
                county_single_flag = transaction_detail.county_single_flag,
                county_default_flag = transaction_detail.county_default_flag,
                city_use_rate = transaction_detail.city_use_rate,
                city_local_use_rate = transaction_detail.city_local_use_rate,
                city_split_amount = transaction_detail.city_split_amount,
                city_split_use_rate = transaction_detail.city_split_use_rate,
                city_single_flag = transaction_detail.city_single_flag,
                city_default_flag = transaction_detail.city_default_flag,
                combined_use_rate = transaction_detail.combined_use_rate,
                gl_extract_amt = transaction_detail.gl_extract_amt,
                update_user_id = transaction_detail.update_user_id,
                update_timestamp = transaction_detail.update_timestamp
          WHERE transaction_detail_id = transaction_detail.transaction_detail_id;
      END;
   END LOOP;
   CLOSE transaction_detail_cursor;
   
   RETURN NULL;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER tb_jurisdiction_taxrate_a_i
AFTER INSERT ON tb_batch
FOR EACH ROW
EXECUTE PROCEDURE tb_jurisdiction_taxrate_a_i();

