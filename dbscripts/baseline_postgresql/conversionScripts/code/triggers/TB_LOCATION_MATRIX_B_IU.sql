CREATE OR REPLACE FUNCTION tb_location_matrix_b_iu() RETURNS TRIGGER AS $$
DECLARE
-- Table defined variables
   v_binary_weight                 tb_location_matrix.binary_weight%TYPE             := 0;
   v_significant_digits            tb_location_matrix.significant_digits%TYPE        := NULL;

-- ****** Program starts ****** --------------------------------------------------------------------
BEGIN
   IF INSERTING THEN
      SELECT NEXTVAL('sq_tb_location_matrix_id')
       INTO STRICT NEW.location_matrix_id;

      NEW.driver_01 := COALESCE(NEW.driver_01,'*ALL');
      NEW.driver_02 := COALESCE(NEW.driver_02,'*ALL');
      NEW.driver_03 := COALESCE(NEW.driver_03,'*ALL');
      NEW.driver_04 := COALESCE(NEW.driver_04,'*ALL');
      NEW.driver_05 := COALESCE(NEW.driver_05,'*ALL');
      NEW.driver_06 := COALESCE(NEW.driver_06,'*ALL');
      NEW.driver_07 := COALESCE(NEW.driver_07,'*ALL');
      NEW.driver_08 := COALESCE(NEW.driver_08,'*ALL');
      NEW.driver_09 := COALESCE(NEW.driver_09,'*ALL');
      NEW.driver_10 := COALESCE(NEW.driver_10,'*ALL');

      SELECT
         CASE COALESCE(NEW.driver_10,'*ALL') WHEN '*ALL' THEN 0 ELSE POWER(2,0) END +
         CASE COALESCE(NEW.driver_09,'*ALL') WHEN '*ALL' THEN 0 ELSE POWER(2,1) END +
         CASE COALESCE(NEW.driver_08,'*ALL') WHEN '*ALL' THEN 0 ELSE POWER(2,2) END +
         CASE COALESCE(NEW.driver_07,'*ALL') WHEN '*ALL' THEN 0 ELSE POWER(2,3) END +
         CASE COALESCE(NEW.driver_06,'*ALL') WHEN '*ALL' THEN 0 ELSE POWER(2,4) END +
         CASE COALESCE(NEW.driver_05,'*ALL') WHEN '*ALL' THEN 0 ELSE POWER(2,5) END +
         CASE COALESCE(NEW.driver_04,'*ALL') WHEN '*ALL' THEN 0 ELSE POWER(2,6) END +
         CASE COALESCE(NEW.driver_03,'*ALL') WHEN '*ALL' THEN 0 ELSE POWER(2,7) END +
         CASE COALESCE(NEW.driver_02,'*ALL') WHEN '*ALL' THEN 0 ELSE POWER(2,8) END +
         CASE COALESCE(NEW.driver_01,'*ALL') WHEN '*ALL' THEN 0 ELSE POWER(2,9) END
      INTO STRICT v_binary_weight;

      SELECT
         TRIM(
	      CASE INSTR(COALESCE(NEW.driver_01,' '),'%') WHEN 0 THEN CASE COALESCE(NEW.driver_01,'*ALL') WHEN '*ALL' THEN '000.' ELSE '999.' END ELSE TRIM(TO_CHAR(INSTR(COALESCE(NEW.driver_01,' '),'%')-1,'000'))||'.' END ||
	      CASE INSTR(COALESCE(NEW.driver_02,' '),'%') WHEN 0 THEN CASE COALESCE(NEW.driver_02,'*ALL') WHEN '*ALL' THEN '000.' ELSE '999.' END ELSE TRIM(TO_CHAR(INSTR(COALESCE(NEW.driver_02,' '),'%')-1,'000'))||'.' END ||
              CASE INSTR(COALESCE(NEW.driver_03,' '),'%') WHEN 0 THEN CASE COALESCE(NEW.driver_03,'*ALL') WHEN '*ALL' THEN '000.' ELSE '999.' END ELSE TRIM(TO_CHAR(INSTR(COALESCE(NEW.driver_03,' '),'%')-1,'000'))||'.' END ||
              CASE INSTR(COALESCE(NEW.driver_04,' '),'%') WHEN 0 THEN CASE COALESCE(NEW.driver_04,'*ALL') WHEN '*ALL' THEN '000.' ELSE '999.' END ELSE TRIM(TO_CHAR(INSTR(COALESCE(NEW.driver_04,' '),'%')-1,'000'))||'.' END ||
              CASE INSTR(COALESCE(NEW.driver_05,' '),'%') WHEN 0 THEN CASE COALESCE(NEW.driver_05,'*ALL') WHEN '*ALL' THEN '000.' ELSE '999.' END ELSE TRIM(TO_CHAR(INSTR(COALESCE(NEW.driver_05,' '),'%')-1,'000'))||'.' END ||
              CASE INSTR(COALESCE(NEW.driver_06,' '),'%') WHEN 0 THEN CASE COALESCE(NEW.driver_06,'*ALL') WHEN '*ALL' THEN '000.' ELSE '999.' END ELSE TRIM(TO_CHAR(INSTR(COALESCE(NEW.driver_06,' '),'%')-1,'000'))||'.' END ||
              CASE INSTR(COALESCE(NEW.driver_07,' '),'%') WHEN 0 THEN CASE COALESCE(NEW.driver_07,'*ALL') WHEN '*ALL' THEN '000.' ELSE '999.' END ELSE TRIM(TO_CHAR(INSTR(COALESCE(NEW.driver_07,' '),'%')-1,'000'))||'.' END ||
              CASE INSTR(COALESCE(NEW.driver_08,' '),'%') WHEN 0 THEN CASE COALESCE(NEW.driver_08,'*ALL') WHEN '*ALL' THEN '000.' ELSE '999.' END ELSE TRIM(TO_CHAR(INSTR(COALESCE(NEW.driver_08,' '),'%')-1,'000'))||'.' END ||
              CASE INSTR(COALESCE(NEW.driver_09,' '),'%') WHEN 0 THEN CASE COALESCE(NEW.driver_09,'*ALL') WHEN '*ALL' THEN '000.' ELSE '999.' END ELSE TRIM(TO_CHAR(INSTR(COALESCE(NEW.driver_09,' '),'%')-1,'000'))||'.' END ||
	      CASE INSTR(COALESCE(NEW.driver_10,' '),'%') WHEN 0 THEN CASE COALESCE(NEW.driver_10,'*ALL') WHEN '*ALL' THEN '000'  ELSE '999'  END ELSE TRIM(TO_CHAR(INSTR(COALESCE(NEW.driver_10,' '),'%')-1,'000')) END
	      )
      INTO STRICT v_significant_digits;

      NEW.default_flag := NVL(NEW.default_flag,'0');
      IF NEW.default_flag = '1' THEN
         NEW.binary_weight		:= 0;
         NEW.significant_digits		:= NULL;
         NEW.default_binary_weight	:= v_binary_weight;
         NEW.default_significant_digits	:= v_significant_digits;
      ELSE
         NEW.binary_weight		:= v_binary_weight;
         NEW.significant_digits		:= v_significant_digits;
         NEW.default_binary_weight	:= 0;
         NEW.default_significant_digits := NULL;
      END IF;
   END IF;

   NEW.update_user_id	:= CURRENT_USER;
   NEW.update_timestamp	:= CURRENT_DATE;
	
   RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER tb_location_matrix_b_iu
BEFORE INSERT OR UPDATE ON tb_location_matrix
FOR EACH ROW
EXECUTE PROCEDURE tb_location_matrix_b_iu();

