CREATE OR REPLACE FUNCTION tb_batch_error_log_b_i() RETURNS TRIGGER AS $$
BEGIN
	SELECT NEXTVAL('sq_tb_batch_error_log_id')
	INTO STRICT NEW.batch_error_log_id;	

	RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER tb_batch_error_log_b_i
BEFORE INSERT OR UPDATE ON tb_batch_error_log
FOR EACH ROW
EXECUTE PROCEDURE tb_batch_error_log_b_i();

