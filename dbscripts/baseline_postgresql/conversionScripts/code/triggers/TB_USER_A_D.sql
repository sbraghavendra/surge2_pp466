CREATE OR REPLACE FUNCTION tb_user_a_d() RETURNS TRIGGER AS $$
BEGIN
	DELETE
	 FROM tb_option
	 WHERE user_code = OLD.user_code;
	
	DELETE
	 FROM tb_dw_syntax
	 WHERE user_code = OLD.user_code;
	
	RETURN NULL;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER tb_user_a_d
AFTER DELETE ON tb_user
FOR EACH ROW
EXECUTE PROCEDURE tb_user_a_d();

