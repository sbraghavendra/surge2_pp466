CREATE OR REPLACE FUNCTION tb_reference_detail_b_iu() RETURNS TRIGGER AS $$
BEGIN
	NEW.update_user_id	:= CURRENT_USER;
	NEW.update_timestamp	:= CURRENT_DATE;
	
	RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER tb_reference_detail_b_iu
BEFORE INSERT OR UPDATE ON tb_reference_detail
FOR EACH ROW
EXECUTE PROCEDURE tb_reference_detail_b_iu();

