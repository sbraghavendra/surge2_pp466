CREATE OR REPLACE FUNCTION tb_entity_level_b_iu() RETURNS TRIGGER AS $$
BEGIN
	NEW.update_user_id	:= CURRENT_USER;
	NEW.update_timestamp	:= CURRENT_DATE;
	
	RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER tb_entity_level_b_iu
BEFORE INSERT OR UPDATE ON tb_entity_level
FOR EACH ROW
EXECUTE PROCEDURE tb_entity_level_b_iu();

