CREATE OR REPLACE FUNCTION tb_tmp_statistics_b_iu() RETURNS TRIGGER AS $$
BEGIN
	NEW.update_user_id	:= CURRENT_USER;
	NEW.update_timestamp	:= CURRENT_DATE;
	
	RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER tb_tmp_statistics_b_iu
BEFORE INSERT OR UPDATE ON tb_tmp_statistics
FOR EACH ROW
EXECUTE PROCEDURE tb_tmp_statistics_b_iu();

