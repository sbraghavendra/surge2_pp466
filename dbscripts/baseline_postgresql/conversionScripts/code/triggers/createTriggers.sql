---------------------------------------------------------------------------------------- 
--  Copyright (c) Lincoln Peak Partners (2008) All rights reserved 
    
--  Name         : createTriggers.sql

--  Description  : This file will create triggers in the stscorp schema

--  Usage        : => \i createTriggers.sql

--  Notes        Run this script while connected as the stscorp user to the postdb1 DB
---------------------------------------------------------------------------------------- 

\i ./TB_ALLOCATION_MATRIX_B_IU.sql
\i ./TB_ANSWER_B_IU.sql
\i ./TB_BATCH_B_IU.sql
\i ./TB_BATCH_ERROR_LOG_B_I.sql
\i ./TB_BATCH_METADATA_B_IU.sql
\i ./TB_CCH_CODE_B_IU.sql
\i ./TB_CCH_TXMATRIX_B_IU.sql
\i ./TB_DATA_DEF_COLUMN_A_D.sql
\i ./TB_DATA_DEF_COLUMN_B_IU.sql
\i ./TB_DATA_DEF_TABLE_B_IU.sql
\i ./TB_DB_STATISTICS_B_IU.sql
\i ./TB_DRIVER_NAMES_B_IU.sql
\i ./TB_DRIVER_REFERENCE_B_IU.sql
\i ./TB_ENTITY_B_IU.sql
\i ./TB_ENTITY_LEVEL_B_IU.sql
\i ./TB_GL_EXTRACT_MAP_B_IU.sql
\i ./TB_IMPORT_DEFINITION_B_IU.sql
\i ./TB_IMPORT_SPEC_B_IU.sql
\i ./TB_JURISDICTION_B_IU.sql
\i ./TB_JURISDICTION_TAXRATE_A_I.sql
\i ./TB_JURISDICTION_TAXRATE_B_IU.sql
\i ./TB_LIST_CODE_B_IU.sql
\i ./TB_LOCATION_MATRIX_A_I.sql
\i ./TB_LOCATION_MATRIX_B_IU.sql
\i ./TB_MENU_B_IU.sql
\i ./TB_OPTION_B_IU.sql
\i ./TB_QUESTIONANSWER_LOG_B_IU.sql
\i ./TB_QUESTION_DETAIL_B_IU.sql
\i ./TB_REFERENCE_DETAIL_B_IU.sql
\i ./TB_REFERENCE_DOCUMENT_B_IU.sql
\i ./TB_ROLE_B_IU.sql
\i ./TB_ROLE_MENU_B_IU.sql
\i ./TB_SQL_B_IU.sql
\i ./TB_TAXCODE_A_U.sql
\i ./TB_TAXCODE_B_IU.sql
\i ./TB_TAXCODE_DETAIL_B_IU.sql
\i ./TB_TAXCODE_STATE_A_U.sql
\i ./TB_TAXCODE_STATE_B_IU.sql
\i ./TB_TAX_MATRIX_A_I.sql
\i ./TB_TAX_MATRIX_B_IU.sql
\i ./TB_TMP_STATISTICS_B_IU.sql
\i ./TB_TRANSACTION_DETAIL_A_U.sql
\i ./TB_TRANSACTION_DETAIL_B_I.sql
\i ./TB_TRANSACTION_DETAIL_B_U.sql
\i ./TB_TRANS_DETAIL_EXCP_B_IU.sql
\i ./TB_USER_A_D.sql
\i ./TB_USER_B_IU.sql
\i ./TB_USER_ENTITY_B_IU.sql
\i ./TB_USER_MENU_EX_B_IU.sql
