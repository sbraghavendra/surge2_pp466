CREATE OR REPLACE FUNCTION tb_gl_extract_map_b_iu() RETURNS TRIGGER AS $$
BEGIN
	IF INSERTING THEN
	      SELECT NEXTVAL('sq_tb_gl_extract_map_id')
	      INTO STRICT NEW.gl_extract_map_id;
	END IF;
	
	NEW.update_user_id	:= CURRENT_USER;
	NEW.update_timestamp	:= CURRENT_DATE;
	
	RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER tb_gl_extract_map_b_iu
BEFORE INSERT OR UPDATE ON tb_gl_extract_map
FOR EACH ROW
EXECUTE PROCEDURE tb_gl_extract_map_b_iu();

