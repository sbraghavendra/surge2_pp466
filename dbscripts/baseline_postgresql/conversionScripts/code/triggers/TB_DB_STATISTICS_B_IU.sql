CREATE OR REPLACE FUNCTION tb_db_statistics_b_iu() RETURNS TRIGGER AS $$
BEGIN
	NEW.update_user_id	:= CURRENT_USER;
	NEW.update_timestamp	:= CURRENT_DATE;
	
	RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER tb_db_statistics_b_iu
BEFORE INSERT OR UPDATE ON tb_db_statistics
FOR EACH ROW
EXECUTE PROCEDURE tb_db_statistics_b_iu();

