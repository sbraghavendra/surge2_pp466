CREATE OR REPLACE FUNCTION tb_allocation_matrix_b_iu() RETURNS TRIGGER AS $$
BEGIN
	IF INSERTING THEN
	 SELECT NEXTVAL('sq_tb_allocation_matrix_id')
	 INTO STRICT NEW.allocation_matrix_id;
	END IF;

	NEW.update_user_id	:= CURRENT_USER;
	NEW.update_timestamp	:= CURRENT_DATE;
	
	RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER tb_allocation_matrix_b_iu
BEFORE INSERT OR UPDATE ON tb_allocation_matrix
FOR EACH ROW
EXECUTE PROCEDURE tb_allocation_matrix_b_iu();

