CREATE OR REPLACE FUNCTION tb_tax_matrix_b_iu() RETURNS TRIGGER AS $$
DECLARE
-- Table defined variables
   v_binary_weight                 tb_tax_matrix.binary_weight%TYPE                  := 0;
   v_significant_digits            tb_tax_matrix.significant_digits%TYPE             := NULL;

-- ****** Program starts ****** --------------------------------------------------------------------
BEGIN
   IF INSERTING THEN
      SELECT NEXTVAL('sq_tb_tax_matrix_id')
      INTO STRICT NEW.tax_matrix_id;

      NEW.driver_01 := NVL(NEW.driver_01,'*ALL');
      NEW.driver_02 := NVL(NEW.driver_02,'*ALL');
      NEW.driver_03 := NVL(NEW.driver_03,'*ALL');
      NEW.driver_04 := NVL(NEW.driver_04,'*ALL');
      NEW.driver_05 := NVL(NEW.driver_05,'*ALL');
      NEW.driver_06 := NVL(NEW.driver_06,'*ALL');
      NEW.driver_07 := NVL(NEW.driver_07,'*ALL');
      NEW.driver_08 := NVL(NEW.driver_08,'*ALL');
      NEW.driver_09 := NVL(NEW.driver_09,'*ALL');
      NEW.driver_10 := NVL(NEW.driver_10,'*ALL');
      NEW.driver_11 := NVL(NEW.driver_11,'*ALL');
      NEW.driver_12 := NVL(NEW.driver_12,'*ALL');
      NEW.driver_13 := NVL(NEW.driver_13,'*ALL');
      NEW.driver_14 := NVL(NEW.driver_14,'*ALL');
      NEW.driver_15 := NVL(NEW.driver_15,'*ALL');
      NEW.driver_16 := NVL(NEW.driver_16,'*ALL');
      NEW.driver_17 := NVL(NEW.driver_17,'*ALL');
      NEW.driver_18 := NVL(NEW.driver_18,'*ALL');
      NEW.driver_19 := NVL(NEW.driver_19,'*ALL');
      NEW.driver_20 := NVL(NEW.driver_20,'*ALL');
      NEW.driver_21 := NVL(NEW.driver_21,'*ALL');
      NEW.driver_22 := NVL(NEW.driver_22,'*ALL');
      NEW.driver_23 := NVL(NEW.driver_23,'*ALL');
      NEW.driver_24 := NVL(NEW.driver_24,'*ALL');
      NEW.driver_25 := NVL(NEW.driver_25,'*ALL');
      NEW.driver_26 := NVL(NEW.driver_26,'*ALL');
      NEW.driver_27 := NVL(NEW.driver_27,'*ALL');
      NEW.driver_28 := NVL(NEW.driver_28,'*ALL');
      NEW.driver_29 := NVL(NEW.driver_29,'*ALL');
      NEW.driver_30 := NVL(NEW.driver_30,'*ALL');

      SELECT
         CASE COALESCE(NEW.driver_30,'*ALL') WHEN '*ALL' THEN 0 ELSE POWER(2,0) END +
         CASE COALESCE(NEW.driver_29,'*ALL') WHEN '*ALL' THEN 0 ELSE POWER(2,1) END +
         CASE COALESCE(NEW.driver_28,'*ALL') WHEN '*ALL' THEN 0 ELSE POWER(2,2) END +
         CASE COALESCE(NEW.driver_27,'*ALL') WHEN '*ALL' THEN 0 ELSE POWER(2,3) END +
         CASE COALESCE(NEW.driver_26,'*ALL') WHEN '*ALL' THEN 0 ELSE POWER(2,4) END +
         CASE COALESCE(NEW.driver_25,'*ALL') WHEN '*ALL' THEN 0 ELSE POWER(2,5) END +
         CASE COALESCE(NEW.driver_24,'*ALL') WHEN '*ALL' THEN 0 ELSE POWER(2,6) END +
         CASE COALESCE(NEW.driver_23,'*ALL') WHEN '*ALL' THEN 0 ELSE POWER(2,7) END +
         CASE COALESCE(NEW.driver_22,'*ALL') WHEN '*ALL' THEN 0 ELSE POWER(2,8) END +
         CASE COALESCE(NEW.driver_21,'*ALL') WHEN '*ALL' THEN 0 ELSE POWER(2,9) END +
         CASE COALESCE(NEW.driver_20,'*ALL') WHEN '*ALL' THEN 0 ELSE POWER(2,10) END +
         CASE COALESCE(NEW.driver_19,'*ALL') WHEN '*ALL' THEN 0 ELSE POWER(2,11) END +
         CASE COALESCE(NEW.driver_18,'*ALL') WHEN '*ALL' THEN 0 ELSE POWER(2,12) END +
         CASE COALESCE(NEW.driver_17,'*ALL') WHEN '*ALL' THEN 0 ELSE POWER(2,13) END +
         CASE COALESCE(NEW.driver_16,'*ALL') WHEN '*ALL' THEN 0 ELSE POWER(2,14) END +
         CASE COALESCE(NEW.driver_15,'*ALL') WHEN '*ALL' THEN 0 ELSE POWER(2,15) END +
         CASE COALESCE(NEW.driver_14,'*ALL') WHEN '*ALL' THEN 0 ELSE POWER(2,16) END +
         CASE COALESCE(NEW.driver_13,'*ALL') WHEN '*ALL' THEN 0 ELSE POWER(2,17) END +
         CASE COALESCE(NEW.driver_12,'*ALL') WHEN '*ALL' THEN 0 ELSE POWER(2,18) END +
         CASE COALESCE(NEW.driver_11,'*ALL') WHEN '*ALL' THEN 0 ELSE POWER(2,19) END +
         CASE COALESCE(NEW.driver_10,'*ALL') WHEN '*ALL' THEN 0 ELSE POWER(2,20) END +
         CASE COALESCE(NEW.driver_09,'*ALL') WHEN '*ALL' THEN 0 ELSE POWER(2,21) END +
         CASE COALESCE(NEW.driver_08,'*ALL') WHEN '*ALL' THEN 0 ELSE POWER(2,22) END +
         CASE COALESCE(NEW.driver_07,'*ALL') WHEN '*ALL' THEN 0 ELSE POWER(2,23) END +
         CASE COALESCE(NEW.driver_06,'*ALL') WHEN '*ALL' THEN 0 ELSE POWER(2,24) END +
         CASE COALESCE(NEW.driver_05,'*ALL') WHEN '*ALL' THEN 0 ELSE POWER(2,25) END +
         CASE COALESCE(NEW.driver_04,'*ALL') WHEN '*ALL' THEN 0 ELSE POWER(2,26) END +
         CASE COALESCE(NEW.driver_03,'*ALL') WHEN '*ALL' THEN 0 ELSE POWER(2,27) END +
         CASE COALESCE(NEW.driver_02,'*ALL') WHEN '*ALL' THEN 0 ELSE POWER(2,28) END +
         CASE COALESCE(NEW.driver_01,'*ALL') WHEN '*ALL' THEN 0 ELSE POWER(2,29) END +
         CASE COALESCE(NEW.matrix_state_code,'*ALL') WHEN '*ALL' THEN 0 ELSE POWER(2,30) END +
         CASE COALESCE(NEW.driver_global_flag,'0') WHEN '1' THEN POWER(2,31) ELSE 0 END
      INTO STRICT v_binary_weight;

      SELECT
         TRIM(
	      CASE INSTR(COALESCE(NEW.driver_01,' '),'%') WHEN 0 THEN CASE COALESCE(NEW.driver_01,'*ALL') WHEN '*ALL' THEN '000.' ELSE '999.' END ELSE TRIM(TO_CHAR(INSTR(COALESCE(NEW.driver_01,' '),'%')-1,'000'))||'.' END ||
              CASE INSTR(COALESCE(NEW.driver_02,' '),'%') WHEN 0 THEN CASE COALESCE(NEW.driver_02,'*ALL') WHEN '*ALL' THEN '000.' ELSE '999.' END ELSE TRIM(TO_CHAR(INSTR(COALESCE(NEW.driver_02,' '),'%')-1,'000'))||'.' END ||
              CASE INSTR(COALESCE(NEW.driver_03,' '),'%') WHEN 0 THEN CASE COALESCE(NEW.driver_03,'*ALL') WHEN '*ALL' THEN '000.' ELSE '999.' END ELSE TRIM(TO_CHAR(INSTR(COALESCE(NEW.driver_03,' '),'%')-1,'000'))||'.' END ||
              CASE INSTR(COALESCE(NEW.driver_04,' '),'%') WHEN 0 THEN CASE COALESCE(NEW.driver_04,'*ALL') WHEN '*ALL' THEN '000.' ELSE '999.' END ELSE TRIM(TO_CHAR(INSTR(COALESCE(NEW.driver_04,' '),'%')-1,'000'))||'.' END ||
              CASE INSTR(COALESCE(NEW.driver_05,' '),'%') WHEN 0 THEN CASE COALESCE(NEW.driver_05,'*ALL') WHEN '*ALL' THEN '000.' ELSE '999.' END ELSE TRIM(TO_CHAR(INSTR(COALESCE(NEW.driver_05,' '),'%')-1,'000'))||'.' END ||
              CASE INSTR(COALESCE(NEW.driver_06,' '),'%') WHEN 0 THEN CASE COALESCE(NEW.driver_06,'*ALL') WHEN '*ALL' THEN '000.' ELSE '999.' END ELSE TRIM(TO_CHAR(INSTR(COALESCE(NEW.driver_06,' '),'%')-1,'000'))||'.' END ||
              CASE INSTR(COALESCE(NEW.driver_07,' '),'%') WHEN 0 THEN CASE COALESCE(NEW.driver_07,'*ALL') WHEN '*ALL' THEN '000.' ELSE '999.' END ELSE TRIM(TO_CHAR(INSTR(COALESCE(NEW.driver_07,' '),'%')-1,'000'))||'.' END ||
              CASE INSTR(COALESCE(NEW.driver_08,' '),'%') WHEN 0 THEN CASE COALESCE(NEW.driver_08,'*ALL') WHEN '*ALL' THEN '000.' ELSE '999.' END ELSE TRIM(TO_CHAR(INSTR(COALESCE(NEW.driver_08,' '),'%')-1,'000'))||'.' END ||
              CASE INSTR(COALESCE(NEW.driver_09,' '),'%') WHEN 0 THEN CASE COALESCE(NEW.driver_09,'*ALL') WHEN '*ALL' THEN '000.' ELSE '999.' END ELSE TRIM(TO_CHAR(INSTR(COALESCE(NEW.driver_09,' '),'%')-1,'000'))||'.' END ||
              CASE INSTR(COALESCE(NEW.driver_10,' '),'%') WHEN 0 THEN CASE COALESCE(NEW.driver_10,'*ALL') WHEN '*ALL' THEN '000.' ELSE '999.' END ELSE TRIM(TO_CHAR(INSTR(COALESCE(NEW.driver_10,' '),'%')-1,'000'))||'.' END ||
              CASE INSTR(COALESCE(NEW.driver_11,' '),'%') WHEN 0 THEN CASE COALESCE(NEW.driver_11,'*ALL') WHEN '*ALL' THEN '000.' ELSE '999.' END ELSE TRIM(TO_CHAR(INSTR(COALESCE(NEW.driver_11,' '),'%')-1,'000'))||'.' END ||
              CASE INSTR(COALESCE(NEW.driver_12,' '),'%') WHEN 0 THEN CASE COALESCE(NEW.driver_12,'*ALL') WHEN '*ALL' THEN '000.' ELSE '999.' END ELSE TRIM(TO_CHAR(INSTR(COALESCE(NEW.driver_12,' '),'%')-1,'000'))||'.' END ||
              CASE INSTR(COALESCE(NEW.driver_13,' '),'%') WHEN 0 THEN CASE COALESCE(NEW.driver_13,'*ALL') WHEN '*ALL' THEN '000.' ELSE '999.' END ELSE TRIM(TO_CHAR(INSTR(COALESCE(NEW.driver_13,' '),'%')-1,'000'))||'.' END ||
              CASE INSTR(COALESCE(NEW.driver_14,' '),'%') WHEN 0 THEN CASE COALESCE(NEW.driver_14,'*ALL') WHEN '*ALL' THEN '000.' ELSE '999.' END ELSE TRIM(TO_CHAR(INSTR(COALESCE(NEW.driver_14,' '),'%')-1,'000'))||'.' END ||
              CASE INSTR(COALESCE(NEW.driver_15,' '),'%') WHEN 0 THEN CASE COALESCE(NEW.driver_15,'*ALL') WHEN '*ALL' THEN '000.' ELSE '999.' END ELSE TRIM(TO_CHAR(INSTR(COALESCE(NEW.driver_15,' '),'%')-1,'000'))||'.' END ||
              CASE INSTR(COALESCE(NEW.driver_16,' '),'%') WHEN 0 THEN CASE COALESCE(NEW.driver_16,'*ALL') WHEN '*ALL' THEN '000.' ELSE '999.' END ELSE TRIM(TO_CHAR(INSTR(COALESCE(NEW.driver_16,' '),'%')-1,'000'))||'.' END ||
              CASE INSTR(COALESCE(NEW.driver_17,' '),'%') WHEN 0 THEN CASE COALESCE(NEW.driver_17,'*ALL') WHEN '*ALL' THEN '000.' ELSE '999.' END ELSE TRIM(TO_CHAR(INSTR(COALESCE(NEW.driver_17,' '),'%')-1,'000'))||'.' END ||
              CASE INSTR(COALESCE(NEW.driver_18,' '),'%') WHEN 0 THEN CASE COALESCE(NEW.driver_18,'*ALL') WHEN '*ALL' THEN '000.' ELSE '999.' END ELSE TRIM(TO_CHAR(INSTR(COALESCE(NEW.driver_18,' '),'%')-1,'000'))||'.' END ||
              CASE INSTR(COALESCE(NEW.driver_19,' '),'%') WHEN 0 THEN CASE COALESCE(NEW.driver_19,'*ALL') WHEN '*ALL' THEN '000.' ELSE '999.' END ELSE TRIM(TO_CHAR(INSTR(COALESCE(NEW.driver_19,' '),'%')-1,'000'))||'.' END ||
              CASE INSTR(COALESCE(NEW.driver_20,' '),'%') WHEN 0 THEN CASE COALESCE(NEW.driver_20,'*ALL') WHEN '*ALL' THEN '000.' ELSE '999.' END ELSE TRIM(TO_CHAR(INSTR(COALESCE(NEW.driver_20,' '),'%')-1,'000'))||'.' END ||
              CASE INSTR(COALESCE(NEW.driver_21,' '),'%') WHEN 0 THEN CASE COALESCE(NEW.driver_21,'*ALL') WHEN '*ALL' THEN '000.' ELSE '999.' END ELSE TRIM(TO_CHAR(INSTR(COALESCE(NEW.driver_21,' '),'%')-1,'000'))||'.' END ||
              CASE INSTR(COALESCE(NEW.driver_22,' '),'%') WHEN 0 THEN CASE COALESCE(NEW.driver_22,'*ALL') WHEN '*ALL' THEN '000.' ELSE '999.' END ELSE TRIM(TO_CHAR(INSTR(COALESCE(NEW.driver_22,' '),'%')-1,'000'))||'.' END ||
              CASE INSTR(COALESCE(NEW.driver_23,' '),'%') WHEN 0 THEN CASE COALESCE(NEW.driver_23,'*ALL') WHEN '*ALL' THEN '000.' ELSE '999.' END ELSE TRIM(TO_CHAR(INSTR(COALESCE(NEW.driver_23,' '),'%')-1,'000'))||'.' END ||
              CASE INSTR(COALESCE(NEW.driver_24,' '),'%') WHEN 0 THEN CASE COALESCE(NEW.driver_24,'*ALL') WHEN '*ALL' THEN '000.' ELSE '999.' END ELSE TRIM(TO_CHAR(INSTR(COALESCE(NEW.driver_24,' '),'%')-1,'000'))||'.' END ||
              CASE INSTR(COALESCE(NEW.driver_25,' '),'%') WHEN 0 THEN CASE COALESCE(NEW.driver_25,'*ALL') WHEN '*ALL' THEN '000.' ELSE '999.' END ELSE TRIM(TO_CHAR(INSTR(COALESCE(NEW.driver_25,' '),'%')-1,'000'))||'.' END ||
              CASE INSTR(COALESCE(NEW.driver_26,' '),'%') WHEN 0 THEN CASE COALESCE(NEW.driver_26,'*ALL') WHEN '*ALL' THEN '000.' ELSE '999.' END ELSE TRIM(TO_CHAR(INSTR(COALESCE(NEW.driver_26,' '),'%')-1,'000'))||'.' END ||
              CASE INSTR(COALESCE(NEW.driver_27,' '),'%') WHEN 0 THEN CASE COALESCE(NEW.driver_27,'*ALL') WHEN '*ALL' THEN '000.' ELSE '999.' END ELSE TRIM(TO_CHAR(INSTR(COALESCE(NEW.driver_27,' '),'%')-1,'000'))||'.' END ||
              CASE INSTR(COALESCE(NEW.driver_28,' '),'%') WHEN 0 THEN CASE COALESCE(NEW.driver_28,'*ALL') WHEN '*ALL' THEN '000.' ELSE '999.' END ELSE TRIM(TO_CHAR(INSTR(COALESCE(NEW.driver_28,' '),'%')-1,'000'))||'.' END ||
              CASE INSTR(COALESCE(NEW.driver_29,' '),'%') WHEN 0 THEN CASE COALESCE(NEW.driver_29,'*ALL') WHEN '*ALL' THEN '000.' ELSE '999.' END ELSE TRIM(TO_CHAR(INSTR(COALESCE(NEW.driver_29,' '),'%')-1,'000'))||'.' END ||
              CASE INSTR(COALESCE(NEW.driver_30,' '),'%') WHEN 0 THEN CASE COALESCE(NEW.driver_30,'*ALL') WHEN '*ALL' THEN '000'  ELSE '999'   END ELSE TRIM(TO_CHAR(INSTR(COALESCE(NEW.driver_30,' '),'%')-1,'000')) END
	      )
      INTO v_significant_digits
      FROM DUAL;

      NEW.default_flag := NVL(NEW.default_flag,'0');
      IF NEW.default_flag = '1' THEN
         NEW.binary_weight := 0;
         NEW.significant_digits := NULL;
         NEW.default_binary_weight := v_binary_weight;
         NEW.default_significant_digits := v_significant_digits;
      ELSE
         NEW.binary_weight := v_binary_weight;
         NEW.significant_digits := v_significant_digits;
         NEW.default_binary_weight := 0;
         NEW.default_significant_digits := NULL;
      END IF;
   END IF;

   NEW.update_user_id := CURRENT_USER;
   NEW.update_timestamp := CURRENT_DATE;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER tb_tax_matrix_b_iu
BEFORE INSERT OR UPDATE ON tb_tax_matrix
FOR EACH ROW
EXECUTE PROCEDURE tb_tax_matrix_b_iu();
