CREATE OR REPLACE FUNCTION tb_transaction_detail_excp_b_iu() RETURNS TRIGGER AS $$
BEGIN
	IF INSERTING THEN
		SELECT NEXTVAL('sq_tb_trans_detail_excp_id')
		INTO STRICT NEW.transaction_detail_excp_id;
	END IF;
	
	NEW.update_user_id	:= CURRENT_USER;
	NEW.update_timestamp	:= CURRENT_DATE;
	
	RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER tb_transaction_detail_excp_b_iu
BEFORE INSERT OR UPDATE ON tb_transaction_detail_excp
FOR EACH ROW
EXECUTE PROCEDURE tb_transaction_detail_excp_b_iu();

