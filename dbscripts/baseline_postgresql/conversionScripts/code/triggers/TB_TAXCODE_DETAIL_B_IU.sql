CREATE OR REPLACE FUNCTION tb_taxcode_detail_b_iu() RETURNS TRIGGER AS $$
BEGIN
	IF INSERTING THEN
		SELECT NEXTVAL('sq_tb_taxcode_detail_id')
		INTO STRICT NEW.taxcode_detail_id;

		SELECT	a.erp_taxcode,		a.taxtype_code,		a.measure_type_code,	a.pct_dist_amt
		INTO	STRICT
			NEW.erp_taxcode,	NEW.taxtype_code,	NEW.measure_type_code,	NEW.pct_dist_amt
		FROM	tb_taxcode a
		WHERE	a.taxcode_type_code	= NEW.taxcode_type_code
		AND	a.taxcode_code		= NEW.taxcode_code;
	END IF;

	NEW.update_user_id	:= CURRENT_USER;
	NEW.update_timestamp	:= CURRENT_DATE;
	
	RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER tb_taxcode_detail_b_iu
BEFORE INSERT OR UPDATE ON tb_taxcode_detail
FOR EACH ROW
EXECUTE PROCEDURE tb_taxcode_detail_b_iu();

