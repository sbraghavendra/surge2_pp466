CREATE OR REPLACE FUNCTION tb_questionanswer_log_b_iu() RETURNS TRIGGER AS $$
BEGIN
	IF INSERTING THEN
		SELECT NEXTVAL('sq_tb_questionanswer_log_id')
		INTO STRICT NEW.jurisdiction_taxrate_id;
	END IF;

	NEW.update_user_id	:= CURRENT_USER;
	NEW.update_timestamp	:= CURRENT_DATE;
	
	RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER tb_questionanswer_log_b_iu
BEFORE INSERT OR UPDATE ON tb_questionanswer_log
FOR EACH ROW
EXECUTE PROCEDURE tb_questionanswer_log_b_iu();

