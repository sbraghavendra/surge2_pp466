CREATE OR REPLACE FUNCTION tb_taxcode_state_a_u() RETURNS TRIGGER AS $$
BEGIN
	UPDATE	tb_taxcode_detail dtl
	 SET	dtl.active_flag		= NEW.active_flag
	 WHERE	dtl.taxcode_state_code	= NEW.taxcode_state_code;

	RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER tb_taxcode_state_a_u
AFTER UPDATE ON tb_taxcode_state
FOR EACH ROW
EXECUTE PROCEDURE tb_taxcode_state_a_u();

