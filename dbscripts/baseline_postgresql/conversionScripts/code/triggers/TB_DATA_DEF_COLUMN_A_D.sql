CREATE OR REPLACE FUNCTION tb_data_def_column_a_d() RETURNS TRIGGER AS $$
BEGIN
	If LOWER(OLD.table_name) = 'tb_transaction_detail' Then
		DELETE
		 FROM tb_driver_reference
		 WHERE trans_dtl_column_name = OLD.column_name;
	End If;
	
	RETURN NULL;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER tb_data_def_column_a_d
AFTER DELETE ON tb_data_def_column
FOR EACH ROW
EXECUTE PROCEDURE tb_data_def_column_a_d();

