-- "Set scan off" turns off substitution variables. 
Set scan off; 

CREATE OR REPLACE PACKAGE unloader
as
    function run( p_query      in varchar2 default NULL,
                  p_cols       in varchar2 default '*',
                  p_town       in varchar2 default USER,
                  p_tname      in varchar2,
                  p_mode       in varchar2 default 'REPLACE',
                  p_dir        in varchar2,
                  p_filename   in varchar2,
                  p_separator  in varchar2 default ',',
                  p_enclosure  in varchar2 default '#',
                  p_terminator in varchar2 default '|',
                  p_header     in varchar2 default 'NO' )
    return number;
	--
    function run2( p_query      in varchar2 default NULL,
                  p_cols       in varchar2 default '*',
                  p_town       in varchar2 default USER,
                  p_tname      in varchar2,
                  p_mode       in varchar2 default 'REPLACE',
                  p_dir        in varchar2,
                  p_filename   in varchar2,
                  p_separator  in varchar2 default ',',
                  p_enclosure  in varchar2 default '{',
                  p_terminator in varchar2 default '|',
                  p_header     in varchar2 default 'NO' )
    return number;
	--
    function run3( p_query      in varchar2 default NULL,
                  p_cols       in varchar2 default '*',
                  p_town       in varchar2 default USER,
                  p_tname      in varchar2,
                  p_mode       in varchar2 default 'REPLACE',
                  p_dir        in varchar2,
                  p_filename   in varchar2,
                  p_separator  in varchar2 default ',',
                  p_enclosure  in varchar2 default '"',
                  p_terminator in varchar2 default '|',
                  p_header     in varchar2 default 'NO' )
    return number;
	--
    function run4( p_query      in varchar2 default NULL,
                  p_cols       in varchar2 default '*',
                  p_town       in varchar2 default USER,
                  p_tname      in varchar2,
                  p_mode       in varchar2 default 'REPLACE',
                  p_dir        in varchar2,
                  p_filename   in varchar2,
                  p_separator  in varchar2 default ',',
                  p_enclosure  in varchar2 default '"',
                  p_terminator in varchar2 default '|',
                  p_header     in varchar2 default 'NO' )
    return number;
    function run5( p_query      in varchar2 default NULL,
                  p_cols       in varchar2 default '*',
                  p_town       in varchar2 default USER,
                  p_tname      in varchar2,
                  p_mode       in varchar2 default 'REPLACE',
                  p_dir        in varchar2,
                  p_filename   in varchar2,
                  p_separator  in varchar2 default ',',
                  p_enclosure  in varchar2 default '"',
                  p_terminator in varchar2 default '|',
                  p_header     in varchar2 default 'NO' )
    return number;

end;
/

CREATE OR REPLACE PACKAGE BODY unloader
as
  --
  g_theCursor     integer default dbms_sql.open_cursor;
  g_descTbl       dbms_sql.desc_tab;
  g_nl            varchar2(2) default chr(10);
  --
  function to_hex( p_str in varchar2 ) return varchar2
  is
    begin
      return to_char( ascii(p_str), 'fm0x' );
    end;
  --
  function quote(p_str in varchar2, p_enclosure in varchar2)
    return varchar2
  is
  begin
    return replace(p_enclosure || replace( p_str, p_enclosure, p_enclosure||p_enclosure ) || p_enclosure, '##','null');
  end;

  function quote2(p_str in varchar2, p_enclosure in varchar2)
    return varchar2
  is
  begin
    return replace(p_enclosure || replace( p_str, p_enclosure, p_enclosure||p_enclosure ) || p_enclosure, '{{','null');
  end;

  function quote3(p_str in varchar2, p_enclosure in varchar2)
    return varchar2
  is
  begin
    return replace(p_enclosure || replace( p_str, p_enclosure, p_enclosure||p_enclosure ) || p_enclosure, '~~','null');
  end;

  function quote4(p_str in varchar2, p_enclosure in varchar2)
    return varchar2
  is
  begin
    return replace(p_enclosure || replace( p_str, p_enclosure, p_enclosure||p_enclosure ) || p_enclosure, '^^','null');
  end;

  function quote5(p_str in varchar2, p_enclosure in varchar2)
    return varchar2
  is
  begin
    return replace(p_enclosure || replace( p_str, p_enclosure, p_enclosure||p_enclosure ) || p_enclosure, '""','null');
  end;


  --
  --
  function getColName(p_col in varchar2, p_own in varchar2, p_tab in varchar2,
                      debug_opt in varchar2 default 'N')
    return varchar2
  is
    l_col                 VARCHAR2(4000);
  begin
    DECLARE
      var_ddl_stmt          VARCHAR2(4000);    -- DDL statment holder
      cursor_name           INTEGER;           -- Dynamic SQL cursor holder
      var_ret_cd            INTEGER;           -- Dynamic SQL return code
      v_comment             VARCHAR2(100);
    BEGIN
      var_ddl_stmt := 'SELECT comments FROM all_col_comments WHERE table_name = '''||p_tab||
                      ''' AND owner = '''||p_own||''' AND column_name = '''||p_col||'''';
      IF UPPER(debug_opt) = 'Y' THEN
        dbms_output.put_line(var_ddl_stmt);
      END IF;
      cursor_name := DBMS_SQL.OPEN_CURSOR;
      --DDL statements are executed by the parse call, which
      --performs the implied commit
      DBMS_SQL.PARSE(cursor_name, var_ddl_stmt, DBMS_SQL.NATIVE);
      DBMS_SQL.DEFINE_COLUMN(cursor_name, 1, v_comment, 100);
      var_ret_cd := DBMS_SQL.EXECUTE(cursor_name);
      --
      loop
        exit when ( dbms_sql.fetch_rows(cursor_name) <= 0 );
        DBMS_SQL.COLUMN_VALUE(cursor_name, 1, v_comment );
      end loop;
      --
      IF ( v_comment IS NULL ) THEN
        l_col := p_col;
      ELSE
        l_col := v_comment;
      END IF;
      DBMS_SQL.CLOSE_CURSOR(cursor_name);
      --
    EXCEPTION
      WHEN OTHERS THEN
        IF DBMS_SQL.IS_OPEN(cursor_name) THEN
          DBMS_SQL.CLOSE_CURSOR(cursor_name);
        END IF;
        return p_col;
    END;
    --
    return l_col;
  end;

  --
  -- Uses database directory
  --

  function run( p_query      in varchar2 default NULL,
                p_cols       in varchar2 default '*',
                p_town       in varchar2 default USER,
                p_tname      in varchar2,
                p_mode       in varchar2 default 'REPLACE',
                p_dir      in varchar2,
                p_filename   in varchar2,
                p_separator  in varchar2 default ',',
                p_enclosure  in varchar2 default '#',
                p_terminator in varchar2 default '|',
                p_header     in varchar2 default 'NO' ) return number
  is
    l_query         varchar2(4000);
    l_output        utl_file.file_type;
    -- JFRANCO 04.15.08
    --l_columnValue   varchar2(4000);
    l_columnValue   long;
    l_colCnt        number default 0;
    l_separator     varchar2(10) default '';
    l_cnt           number default 0;
    l_line          long;
    l_datefmt       varchar2(255);
    l_descTbl       dbms_sql.desc_tab;
  begin
     --
     -- Set the date format to a big numeric string. Avoids
     -- all NLS issues and saves both the time and date.
     --
    execute immediate
      'alter session set nls_date_format=''MM/DD/YYYY HH24:MI:SS'' ';

      select value
      into l_datefmt
      from nls_session_parameters
      where parameter = 'NLS_DATE_FORMAT';
    --
    -- Set up an exception block so that in the event of any
    -- error, we can at least reset the date format back.
    --
    declare
      invalid_type EXCEPTION;
    begin
      --
      -- Parse and describe the query. We reset the
      -- descTbl to an empty table so .count on it
      -- will be reliable.
      --
      if p_query is NULL then
        l_query := 'select '||p_cols||' from '||p_town||'.'||p_tname;
      else
        l_query := p_query;
      end if;
      --
      --
      -- dbms_output.put_line('Query: '||l_query);
      --
      --
      dbms_sql.parse( g_theCursor, l_query, dbms_sql.native );
      g_descTbl := l_descTbl;
      dbms_sql.describe_columns( g_theCursor, l_colCnt, g_descTbl );
      --
      -- Verify that the table contains supported columns - currently
      -- LOBs are not supported.
      --
      for i in 1 .. g_descTbl.count loop
        IF (g_descTbl(i).col_type = 1) OR (g_descTbl(i).col_type = 2)
            OR (g_descTbl(i).col_type = 12) OR (g_descTbl(i).col_type = 96)
            OR (g_descTbl(i).col_type = 8) OR (g_descTbl(i).col_type = 23) THEN
          NULL;
        ELSE
          RAISE invalid_type;
        END IF;
      end loop;
      --
      -- Bind every single column to a varchar2(4000). We don't care
      -- if we are fetching a number or a date or whatever.
      -- Everything can be a string.
      --
      for i in 1 .. l_colCnt loop
	-- JFRANCO 04.15.08
	--dbms_sql.define_column( g_theCursor, i, l_columnValue, 4000 );
	dbms_sql.define_column( g_theCursor, i, l_columnValue, 30000 );
      end loop;
      --
      -- Run the query - ignore the output of execute. It is only
      -- valid when the DML is an insert/update or delete.
      --
      l_cnt := dbms_sql.execute(g_theCursor);
      --
      -- Open the file to write output to and then write the
      -- delimited data to it.
      --
      l_output := utl_file.fopen( p_dir, p_filename || '.TXT', 'w', 32760 );
      --
      -- Output a column header. This version uses table column comments if they
      -- exist, otherwise it defaults to the actual table column name.
      --
      IF p_header = 'YES' THEN
        l_separator := '';
        l_line := '';
        for i in 1 .. g_descTbl.count
        loop
          l_line := l_line || l_separator ||
                    quote( getColName(g_descTbl(i).col_name, p_town, p_tname), p_enclosure );
          l_separator := p_separator;
        end loop;
        utl_file.put_line( l_output, l_line );
      END IF;
      --
      -- Output data
      --
      loop
        exit when ( dbms_sql.fetch_rows(g_theCursor) <= 0 );
        l_separator := '';
        l_line := null;
        for i in 1 .. l_colCnt loop
          dbms_sql.column_value( g_theCursor, i,
                                 l_columnValue );
          l_line := l_line || l_separator ||
                    quote( l_columnValue, p_enclosure );
          l_separator := p_separator;
        end loop;
        l_line := l_line || p_terminator;
        utl_file.put_line( l_output, l_line );
        l_cnt := l_cnt+1;
      end loop;
      utl_file.fclose( l_output );
      --
      -- Now reset the date format and return the number of rows
      -- written to the output file.
      --
      execute immediate
        'alter session set nls_date_format=''' || l_datefmt || '''';
      --
      return l_cnt;
    exception
      --
      -- In the event of ANY error, reset the data format and
      -- re-raise the error.
      --
      when invalid_type then
        execute immediate
          'alter session set nls_date_format=''' || l_datefmt || '''';
        --
        dbms_output.put_line('Error - Table: '||p_tname||' contains an unsupported column type');
        dbms_output.put_line('Table is being skipped');
        --
        return 0;
      when utl_file.invalid_path then
        execute immediate
          'alter session set nls_date_format=''' || l_datefmt || '''';
        --
        dbms_output.put_line('Invalid path name specified for dat file');
        --
        return 0;
      when others then
        execute immediate
          'alter session set nls_date_format=''' || l_datefmt || '''';
        --
        RAISE;
        --
        return 0;
    end;
  end run;
  --
   function run2( p_query      in varchar2 default NULL,
                p_cols       in varchar2 default '*',
                p_town       in varchar2 default USER,
                p_tname      in varchar2,
                p_mode       in varchar2 default 'REPLACE',
                p_dir      in varchar2,
                p_filename   in varchar2,
                p_separator  in varchar2 default ',',
                p_enclosure  in varchar2 default '{',
                p_terminator in varchar2 default '|',
                p_header     in varchar2 default 'NO' ) return number
  is
    l_query         varchar2(4000);
    l_output        utl_file.file_type;
    -- JFRANCO 04.15.08
    --l_columnValue   varchar2(4000);
    l_columnValue   long;    
    l_colCnt        number default 0;
    l_separator     varchar2(10) default '';
    l_cnt           number default 0;
    l_line          long;
    l_datefmt       varchar2(255);
    l_descTbl       dbms_sql.desc_tab;
  begin
     --
     -- Set the date format to a big numeric string. Avoids
     -- all NLS issues and saves both the time and date.
     --
    execute immediate
      'alter session set nls_date_format=''MM/DD/YYYY HH24:MI:SS'' ';

      select value
      into l_datefmt
      from nls_session_parameters
      where parameter = 'NLS_DATE_FORMAT';
    --
    -- Set up an exception block so that in the event of any
    -- error, we can at least reset the date format back.
    --
    declare
      invalid_type EXCEPTION;
    begin
      --
      -- Parse and describe the query. We reset the
      -- descTbl to an empty table so .count on it
      -- will be reliable.
      --
      if p_query is NULL then
        l_query := 'select '||p_cols||' from '||p_town||'.'||p_tname;
      else
        l_query := p_query;
      end if;
      --
      --
      -- dbms_output.put_line('Query: '||l_query);
      --
      --
      dbms_sql.parse( g_theCursor, l_query, dbms_sql.native );
      g_descTbl := l_descTbl;
      dbms_sql.describe_columns( g_theCursor, l_colCnt, g_descTbl );
      --
      -- Verify that the table contains supported columns - currently
      -- LOBs are not supported.
      --
      for i in 1 .. g_descTbl.count loop
        IF (g_descTbl(i).col_type = 1) OR (g_descTbl(i).col_type = 2)
            OR (g_descTbl(i).col_type = 12) OR (g_descTbl(i).col_type = 96)
            OR (g_descTbl(i).col_type = 8) OR (g_descTbl(i).col_type = 23) THEN
          NULL;
        ELSE
          RAISE invalid_type;
        END IF;
      end loop;
      --
      -- Bind every single column to a varchar2(4000). We don't care
      -- if we are fetching a number or a date or whatever.
      -- Everything can be a string.
      --
      for i in 1 .. l_colCnt loop
	-- JFRANCO 04.15.08
	--dbms_sql.define_column( g_theCursor, i, l_columnValue, 4000 );
	dbms_sql.define_column( g_theCursor, i, l_columnValue, 30000 );	
      end loop;
      --
      -- Run the query - ignore the output of execute. It is only
      -- valid when the DML is an insert/update or delete.
      --
      l_cnt := dbms_sql.execute(g_theCursor);
      --
      -- Open the file to write output to and then write the
      -- delimited data to it.
      --
      l_output := utl_file.fopen( p_dir, p_filename || '.TXT', 'w', 32760 );
      --
      -- Output a column header. This version uses table column comments if they
      -- exist, otherwise it defaults to the actual table column name.
      --
      IF p_header = 'YES' THEN
        l_separator := '';
        l_line := '';
        for i in 1 .. g_descTbl.count
        loop
          l_line := l_line || l_separator ||
                    quote2( getColName(g_descTbl(i).col_name, p_town, p_tname), p_enclosure );
          l_separator := p_separator;
        end loop;
        utl_file.put_line( l_output, l_line );
      END IF;
      --
      -- Output data
      --
      loop
        exit when ( dbms_sql.fetch_rows(g_theCursor) <= 0 );
        l_separator := '';
        l_line := null;
        for i in 1 .. l_colCnt loop
          dbms_sql.column_value( g_theCursor, i,
                                 l_columnValue );
          l_line := l_line || l_separator ||
                    quote2( l_columnValue, p_enclosure );
          l_separator := p_separator;
        end loop;
        l_line := l_line || p_terminator;
        utl_file.put_line( l_output, l_line );
        l_cnt := l_cnt+1;
      end loop;
      utl_file.fclose( l_output );
      --
      -- Now reset the date format and return the number of rows
      -- written to the output file.
      --
      execute immediate
        'alter session set nls_date_format=''' || l_datefmt || '''';
      --
      return l_cnt;
    exception
      --
      -- In the event of ANY error, reset the data format and
      -- re-raise the error.
      --
      when invalid_type then
        execute immediate
          'alter session set nls_date_format=''' || l_datefmt || '''';
        --
        dbms_output.put_line('Error - Table: '||p_tname||' contains an unsupported column type');
        dbms_output.put_line('Table is being skipped');
        --
        return 0;
      when utl_file.invalid_path then
        execute immediate
          'alter session set nls_date_format=''' || l_datefmt || '''';
        --
        dbms_output.put_line('Invalid path name specified for dat file');
        --
        return 0;
      when others then
        execute immediate
          'alter session set nls_date_format=''' || l_datefmt || '''';
        --
        RAISE;
        --
        return 0;
    end;
  end run2;
  --
  function run3( p_query      in varchar2 default NULL,
                p_cols       in varchar2 default '*',
                p_town       in varchar2 default USER,
                p_tname      in varchar2,
                p_mode       in varchar2 default 'REPLACE',
                p_dir      in varchar2,
                p_filename   in varchar2,
                p_separator  in varchar2 default ',',
                p_enclosure  in varchar2 default '"',
                p_terminator in varchar2 default '|',
                p_header     in varchar2 default 'NO' ) return number
  is
    l_query         varchar2(4000);
    l_output        utl_file.file_type;
    -- JFRANCO 04.15.08
    --l_columnValue   varchar2(4000);
    l_columnValue   long;
    l_colCnt        number default 0;
    l_separator     varchar2(10) default '';
    l_cnt           number default 0;
    l_line          long;
    l_datefmt       varchar2(255);
    l_descTbl       dbms_sql.desc_tab;
  begin
     --
     -- Set the date format to a big numeric string. Avoids
     -- all NLS issues and saves both the time and date.
     --
    execute immediate
      'alter session set nls_date_format=''MM/DD/YYYY HH24:MI:SS'' ';

      select value
      into l_datefmt
      from nls_session_parameters
      where parameter = 'NLS_DATE_FORMAT';
    --
    -- Set up an exception block so that in the event of any
    -- error, we can at least reset the date format back.
    --
    declare
      invalid_type EXCEPTION;
    begin
      --
      -- Parse and describe the query. We reset the
      -- descTbl to an empty table so .count on it
      -- will be reliable.
      --
      if p_query is NULL then
        l_query := 'select '||p_cols||' from '||p_town||'.'||p_tname;
      else
        l_query := p_query;
      end if;
      --
      --
      -- dbms_output.put_line('Query: '||l_query);
      --
      --
      dbms_sql.parse( g_theCursor, l_query, dbms_sql.native );
      g_descTbl := l_descTbl;
      dbms_sql.describe_columns( g_theCursor, l_colCnt, g_descTbl );
      --
      -- Verify that the table contains supported columns - currently
      -- LOBs are not supported.
      --
      for i in 1 .. g_descTbl.count loop
        IF (g_descTbl(i).col_type = 1) OR (g_descTbl(i).col_type = 2)
            OR (g_descTbl(i).col_type = 12) OR (g_descTbl(i).col_type = 96)
            OR (g_descTbl(i).col_type = 8) OR (g_descTbl(i).col_type = 23) THEN
          NULL;
        ELSE
          RAISE invalid_type;
        END IF;
      end loop;
      --
      -- Bind every single column to a varchar2(4000). We don't care
      -- if we are fetching a number or a date or whatever.
      -- Everything can be a string.
      --
      for i in 1 .. l_colCnt loop
	-- JFRANCO 04.15.08
	--dbms_sql.define_column( g_theCursor, i, l_columnValue, 4000 );
	dbms_sql.define_column( g_theCursor, i, l_columnValue, 30000 );
      end loop;
      --
      -- Run the query - ignore the output of execute. It is only
      -- valid when the DML is an insert/update or delete.
      --
      l_cnt := dbms_sql.execute(g_theCursor);
      --
      -- Open the file to write output to and then write the
      -- delimited data to it.
      --
      l_output := utl_file.fopen( p_dir, p_filename || '.TXT', 'w', 32760 );
      --
      -- Output a column header. This version uses table column comments if they
      -- exist, otherwise it defaults to the actual table column name.
      --
      IF p_header = 'YES' THEN
        l_separator := '';
        l_line := '';
        for i in 1 .. g_descTbl.count
        loop
          l_line := l_line || l_separator ||
                    quote3( getColName(g_descTbl(i).col_name, p_town, p_tname), p_enclosure );
          l_separator := p_separator;
        end loop;
        utl_file.put_line( l_output, l_line );
      END IF;
      --
      -- Output data
      --
      loop
        exit when ( dbms_sql.fetch_rows(g_theCursor) <= 0 );
        l_separator := '';
        l_line := null;
        for i in 1 .. l_colCnt loop
          dbms_sql.column_value( g_theCursor, i,
                                 l_columnValue );
          l_line := l_line || l_separator ||
                    quote3( l_columnValue, p_enclosure );
          l_separator := p_separator;
        end loop;
        l_line := l_line || p_terminator;
        utl_file.put_line( l_output, l_line );
        l_cnt := l_cnt+1;
      end loop;
      utl_file.fclose( l_output );
      --
      -- Now reset the date format and return the number of rows
      -- written to the output file.
      --
      execute immediate
        'alter session set nls_date_format=''' || l_datefmt || '''';
      --
      return l_cnt;
    exception
      --
      -- In the event of ANY error, reset the data format and
      -- re-raise the error.
      --
      when invalid_type then
        execute immediate
          'alter session set nls_date_format=''' || l_datefmt || '''';
        --
        dbms_output.put_line('Error - Table: '||p_tname||' contains an unsupported column type');
        dbms_output.put_line('Table is being skipped');
        --
        return 0;
      when utl_file.invalid_path then
        execute immediate
          'alter session set nls_date_format=''' || l_datefmt || '''';
        --
        dbms_output.put_line('Invalid path name specified for dat file');
        --
        return 0;
      when others then
        execute immediate
          'alter session set nls_date_format=''' || l_datefmt || '''';
        --
        RAISE;
        --
        return 0;
    end;
  end run3;
  --
  function run4( p_query      in varchar2 default NULL,
                p_cols       in varchar2 default '*',
                p_town       in varchar2 default USER,
                p_tname      in varchar2,
                p_mode       in varchar2 default 'REPLACE',
                p_dir      in varchar2,
                p_filename   in varchar2,
                p_separator  in varchar2 default ',',
                p_enclosure  in varchar2 default '"',
                p_terminator in varchar2 default '|',
                p_header     in varchar2 default 'NO' ) return number
  is
    l_query         varchar2(4000);
    l_output        utl_file.file_type;
    -- JFRANCO 04.15.08
    --l_columnValue   varchar2(4000);
    l_columnValue   long;
    l_colCnt        number default 0;
    l_separator     varchar2(10) default '';
    l_cnt           number default 0;
    l_line          long;
    l_datefmt       varchar2(255);
    l_descTbl       dbms_sql.desc_tab;
  begin
     --
     -- Set the date format to a big numeric string. Avoids
     -- all NLS issues and saves both the time and date.
     --
    execute immediate
      'alter session set nls_date_format=''MM/DD/YYYY HH24:MI:SS'' ';

      select value
      into l_datefmt
      from nls_session_parameters
      where parameter = 'NLS_DATE_FORMAT';
    --
    -- Set up an exception block so that in the event of any
    -- error, we can at least reset the date format back.
    --
    declare
      invalid_type EXCEPTION;
    begin
      --
      -- Parse and describe the query. We reset the
      -- descTbl to an empty table so .count on it
      -- will be reliable.
      --
      if p_query is NULL then
        l_query := 'select '||p_cols||' from '||p_town||'.'||p_tname;
      else
        l_query := p_query;
      end if;
      --
      --
      -- dbms_output.put_line('Query: '||l_query);
      --
      --
      dbms_sql.parse( g_theCursor, l_query, dbms_sql.native );
      g_descTbl := l_descTbl;
      dbms_sql.describe_columns( g_theCursor, l_colCnt, g_descTbl );
      --
      -- Verify that the table contains supported columns - currently
      -- LOBs are not supported.
      --
      for i in 1 .. g_descTbl.count loop
        IF (g_descTbl(i).col_type = 1) OR (g_descTbl(i).col_type = 2)
            OR (g_descTbl(i).col_type = 12) OR (g_descTbl(i).col_type = 96)
            OR (g_descTbl(i).col_type = 8) OR (g_descTbl(i).col_type = 23) THEN
          NULL;
        ELSE
          RAISE invalid_type;
        END IF;
      end loop;
      --
      -- Bind every single column to a varchar2(4000). We don't care
      -- if we are fetching a number or a date or whatever.
      -- Everything can be a string.
      --
      for i in 1 .. l_colCnt loop
	-- JFRANCO 04.15.08
	--dbms_sql.define_column( g_theCursor, i, l_columnValue, 4000 );
	dbms_sql.define_column( g_theCursor, i, l_columnValue, 30000 );
      end loop;
      --
      -- Run the query - ignore the output of execute. It is only
      -- valid when the DML is an insert/update or delete.
      --
      l_cnt := dbms_sql.execute(g_theCursor);
      --
      -- Open the file to write output to and then write the
      -- delimited data to it.
      --
      l_output := utl_file.fopen( p_dir, p_filename || '.TXT', 'w', 32760 );
      --
      -- Output a column header. This version uses table column comments if they
      -- exist, otherwise it defaults to the actual table column name.
      --
      IF p_header = 'YES' THEN
        l_separator := '';
        l_line := '';
        for i in 1 .. g_descTbl.count
        loop
          l_line := l_line || l_separator ||
                    quote4( getColName(g_descTbl(i).col_name, p_town, p_tname), p_enclosure );
          l_separator := p_separator;
        end loop;
        utl_file.put_line( l_output, l_line );
      END IF;
      --
      -- Output data
      --
      loop
        exit when ( dbms_sql.fetch_rows(g_theCursor) <= 0 );
        l_separator := '';
        l_line := null;
        for i in 1 .. l_colCnt loop
          dbms_sql.column_value( g_theCursor, i,
                                 l_columnValue );
          l_line := l_line || l_separator ||
                    quote4( l_columnValue, p_enclosure );
          l_separator := p_separator;
        end loop;
        l_line := l_line || p_terminator;
        utl_file.put_line( l_output, l_line );
        l_cnt := l_cnt+1;
      end loop;
      utl_file.fclose( l_output );
      --
      -- Now reset the date format and return the number of rows
      -- written to the output file.
      --
      execute immediate
        'alter session set nls_date_format=''' || l_datefmt || '''';
      --
      return l_cnt;
    exception
      --
      -- In the event of ANY error, reset the data format and
      -- re-raise the error.
      --
      when invalid_type then
        execute immediate
          'alter session set nls_date_format=''' || l_datefmt || '''';
        --
        dbms_output.put_line('Error - Table: '||p_tname||' contains an unsupported column type');
        dbms_output.put_line('Table is being skipped');
        --
        return 0;
      when utl_file.invalid_path then
        execute immediate
          'alter session set nls_date_format=''' || l_datefmt || '''';
        --
        dbms_output.put_line('Invalid path name specified for dat file');
        --
        return 0;
      when others then
        execute immediate
          'alter session set nls_date_format=''' || l_datefmt || '''';
        --
        RAISE;
        --
        return 0;
    end;
  end run4;
  --
  --
  function run5( p_query      in varchar2 default NULL,
                p_cols       in varchar2 default '*',
                p_town       in varchar2 default USER,
                p_tname      in varchar2,
                p_mode       in varchar2 default 'REPLACE',
                p_dir      in varchar2,
                p_filename   in varchar2,
                p_separator  in varchar2 default ',',
                p_enclosure  in varchar2 default '"',
                p_terminator in varchar2 default '|',
                p_header     in varchar2 default 'NO' ) return number
  is
    l_query         varchar2(4000);
    l_output        utl_file.file_type;
    -- JFRANCO 04.15.08
    --l_columnValue   varchar2(4000);
    l_columnValue   long;
    l_colCnt        number default 0;
    l_separator     varchar2(10) default '';
    l_cnt           number default 0;
    l_line          long;
    l_datefmt       varchar2(255);
    l_descTbl       dbms_sql.desc_tab;
  begin
     --
     -- Set the date format to a big numeric string. Avoids
     -- all NLS issues and saves both the time and date.
     --
    execute immediate
      'alter session set nls_date_format=''MM/DD/YYYY HH24:MI:SS'' ';

      select value
      into l_datefmt
      from nls_session_parameters
      where parameter = 'NLS_DATE_FORMAT';
    --
    -- Set up an exception block so that in the event of any
    -- error, we can at least reset the date format back.
    --
    declare
      invalid_type EXCEPTION;
    begin
      --
      -- Parse and describe the query. We reset the
      -- descTbl to an empty table so .count on it
      -- will be reliable.
      --
      if p_query is NULL then
        l_query := 'select '||p_cols||' from '||p_town||'.'||p_tname;
      else
        l_query := p_query;
      end if;
      --
      --
      -- dbms_output.put_line('Query: '||l_query);
      --
      --
      dbms_sql.parse( g_theCursor, l_query, dbms_sql.native );
      g_descTbl := l_descTbl;
      dbms_sql.describe_columns( g_theCursor, l_colCnt, g_descTbl );
      --
      -- Verify that the table contains supported columns - currently
      -- LOBs are not supported.
      --
      for i in 1 .. g_descTbl.count loop
        IF (g_descTbl(i).col_type = 1) OR (g_descTbl(i).col_type = 2)
            OR (g_descTbl(i).col_type = 12) OR (g_descTbl(i).col_type = 96)
            OR (g_descTbl(i).col_type = 8) OR (g_descTbl(i).col_type = 23) THEN
          NULL;
        ELSE
          RAISE invalid_type;
        END IF;
      end loop;
      --
      -- Bind every single column to a varchar2(4000). We don't care
      -- if we are fetching a number or a date or whatever.
      -- Everything can be a string.
      --
      for i in 1 .. l_colCnt loop
	-- JFRANCO 04.15.08
	--dbms_sql.define_column( g_theCursor, i, l_columnValue, 4000 );
	dbms_sql.define_column( g_theCursor, i, l_columnValue, 30000 );
      end loop;
      --
      -- Run the query - ignore the output of execute. It is only
      -- valid when the DML is an insert/update or delete.
      --
      l_cnt := dbms_sql.execute(g_theCursor);
      --
      -- Open the file to write output to and then write the
      -- delimited data to it.
      --
      l_output := utl_file.fopen( p_dir, p_filename || '.TXT', 'w', 32760 );
      --
      -- Output a column header. This version uses table column comments if they
      -- exist, otherwise it defaults to the actual table column name.
      --
      IF p_header = 'YES' THEN
        l_separator := '';
        l_line := '';
        for i in 1 .. g_descTbl.count
        loop
          l_line := l_line || l_separator ||
                    quote5( getColName(g_descTbl(i).col_name, p_town, p_tname), p_enclosure );
          l_separator := p_separator;
        end loop;
        utl_file.put_line( l_output, l_line );
      END IF;
      --
      -- Output data
      --
      loop
        exit when ( dbms_sql.fetch_rows(g_theCursor) <= 0 );
        l_separator := '';
        l_line := null;
        for i in 1 .. l_colCnt loop
          dbms_sql.column_value( g_theCursor, i,
                                 l_columnValue );
          l_line := l_line || l_separator ||
                    quote5( l_columnValue, p_enclosure );
          l_separator := p_separator;
        end loop;
        l_line := l_line || p_terminator;
        utl_file.put_line( l_output, l_line );
        l_cnt := l_cnt+1;
      end loop;
      utl_file.fclose( l_output );
      --
      -- Now reset the date format and return the number of rows
      -- written to the output file.
      --
      execute immediate
        'alter session set nls_date_format=''' || l_datefmt || '''';
      --
      return l_cnt;
    exception
      --
      -- In the event of ANY error, reset the data format and
      -- re-raise the error.
      --
      when invalid_type then
        execute immediate
          'alter session set nls_date_format=''' || l_datefmt || '''';
        --
        dbms_output.put_line('Error - Table: '||p_tname||' contains an unsupported column type');
        dbms_output.put_line('Table is being skipped');
        --
        return 0;
      when utl_file.invalid_path then
        execute immediate
          'alter session set nls_date_format=''' || l_datefmt || '''';
        --
        dbms_output.put_line('Invalid path name specified for dat file');
        --
        return 0;
      when others then
        execute immediate
          'alter session set nls_date_format=''' || l_datefmt || '''';
        --
        RAISE;
        --
        return 0;
    end;
  end run5;

end unloader;
/
