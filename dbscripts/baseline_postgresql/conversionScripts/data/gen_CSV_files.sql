SET LINESIZE 150;
SET SERVEROUTPUT on SIZE 1000000 FORMAT WRAPPED;
declare      
l_rows    number;
cursor ctab is
select upper(table_name) table_name from dba_tables
where owner='STSCORP' and num_rows > 0 
and table_name not in('TB_IMPORT_DEFINITION','TB_BATCH_ERROR_LOG','TB_GL_EXPORT_LOG','TB_GL_REPORT_LOG','TB_TRANSACTION_DETAIL')
order by num_rows;

cursor ctab2 is
select upper(table_name) table_name from dba_tables
where owner='STSCORP' and num_rows > 0 
and table_name in('TB_IMPORT_DEFINITION','TB_GL_EXPORT_LOG','TB_GL_REPORT_LOG','TB_TRANSACTION_DETAIL')
order by num_rows;

BEGIN
FOR tab_rec in ctab LOOP
          l_rows := unloader.run
                    ( 
		      p_cols       => '*',
                      p_town       => 'STSCORP',
                      p_tname      => tab_rec.table_name,
                      p_dir        => 'UNLOADER',
                      p_filename   => tab_rec.table_name,
                      p_separator  => ',',
                      p_enclosure  => '#',
                      p_terminator => '',
                      p_header     => 'NO' );
        
dbms_output.put_line( tab_rec.table_name ||': '|| to_char(l_rows) ||' rows extracted to CSV file' ); 
END LOOP;
--
FOR tab_rec in ctab2 LOOP
          l_rows := unloader.run2
                    ( 
		      p_cols       => '*',
                      p_town       => 'STSCORP',
                      p_tname      => tab_rec.table_name,
                      p_dir        => 'UNLOADER',
                      p_filename   => tab_rec.table_name,
                      p_separator  => ',',
                      p_enclosure  => '{',
                      p_terminator => '',
                      p_header     => 'NO' );
        
dbms_output.put_line( tab_rec.table_name ||': '|| to_char(l_rows) ||' rows extracted to CSV file' ); 
END LOOP;
END;
/
