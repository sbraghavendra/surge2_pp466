---------------------------------------------------------------------------------------- 
--  Copyright (c) Lincoln Peak Partners (2008) All rights reserved 
    
--  Name         : createSchema.sql

--  Description  : This file will create the postdb1 database

--  Usage        : => \i createSchema.sql

--  Notes        Run this script while connected as the postgres user to the postdb1 DB
---------------------------------------------------------------------------------------- 

-- Create the Schema:
CREATE SCHEMA stscorp
       AUTHORIZATION postgres;

-- Grant privileges to stscorp:
GRANT usage,create ON SCHEMA stscorp TO stscorp;

-- Set default search path 
ALTER ROLE stscorp SET search_path=stscorp;
ALTER ROLE stsuser SET search_path=stscorp;

-- Create the copy_to function as a super user
CREATE OR REPLACE FUNCTION stscorp.copy_to(p_table_name IN VARCHAR, p_file_name IN VARCHAR) RETURNS VOID AS $$ 
BEGIN
	EXECUTE 'COPY (select content from '||p_table_name||' order by order_no) TO '''||p_file_name||''' WITH NULL AS ''null''';
END;
$$ LANGUAGE plpgsql SECURITY definer;

-- Grant privileges on the function to stscorp
GRANT EXECUTE ON FUNCTION stscorp.copy_to(p_table_name IN VARCHAR, p_file_name IN VARCHAR) TO stscorp;
