---------------------------------------------------------------------------------------- 
--  Copyright (c) Lincoln Peak Partners (2008) All rights reserved 
    
--  Name         : createTablespace.sql

--  Description  : This file will create the postdbtbs1 tablespace

--  Usage        : => \i createTablespace.sql

--  Notes        Run this script while connected as the postgres user to the postgres DB
---------------------------------------------------------------------------------------- 

-- Create the tablespace:
CREATE TABLESPACE postdbtbs1 OWNER postgres LOCATION E'C:\\postgresql\\postdb1\\postdbtbs1';
