---------------------------------------------------------------------------------------- 
--  Copyright (c) Lincoln Peak Partners (2008) All rights reserved 
    
--  Name         : createDatabase.sql

--  Description  : This file will create the postdb1 database

--  Usage        : => \i createDatabase.sql

--  Notes        Run this script while connected as the postgres user to the postgres DB
---------------------------------------------------------------------------------------- 

-- Create the database:
CREATE DATABASE postdb1
  WITH ENCODING='UTF8'
       OWNER=postgres
       TABLESPACE=postdbtbs1;
