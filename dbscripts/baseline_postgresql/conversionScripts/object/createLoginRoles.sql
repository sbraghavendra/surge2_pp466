---------------------------------------------------------------------------------------- 
--  Copyright (c) Lincoln Peak Partners (2008) All rights reserved 
    
--  Name         : createLoginRoles.sql

--  Description  : This file will create the login roles: STSCORP,STSUSER

--  Usage        : => \i createLoginRoles.sql

--  Notes        Run this script while connected as the postgres user to the postgres DB
---------------------------------------------------------------------------------------- 

-- Create the Login Role (password stscorp):
CREATE ROLE stscorp LOGIN ENCRYPTED PASSWORD 'md57068d2aa16fe47df83ff8660e17f97a2'
  NOINHERIT
   VALID UNTIL 'infinity';
ALTER ROLE stscorp SET default_tablespace=postdbtbs1;

-- Create the Login Role (password stsuser):
CREATE ROLE stsuser LOGIN ENCRYPTED PASSWORD 'md5c892b55f14d30e1f4d3ebda542d8a772'
  NOINHERIT
   VALID UNTIL 'infinity';
ALTER ROLE stsuser SET default_tablespace=postdbtbs1;

