---------------------------------------------------------------------------------------- 
--  Copyright (c) Lincoln Peak Partners (2008) All rights reserved 
    
--  Name         : createSynonyms.sql

--  Description  : This file will create synonyms in the stscorp schema

--  Usage        : => \i createSynonyms.sql

--  Notes        Run this script while connected as the stscorp user to the postdb1 DB
---------------------------------------------------------------------------------------- 


/*
DROP PUBLIC SYNONYM TB_TMP_GL_EXT_CELANESE_PROD_2;
CREATE PUBLIC SYNONYM TB_TMP_GL_EXT_CELANESE_PROD_2 FOR STSCORP.TB_TMP_GL_EXT_CELANESE_PROD_2;
*/
