-- Function: sp_setallseq()

-- DROP FUNCTION sp_setallseq();

CREATE OR REPLACE FUNCTION sp_setallseq()
  RETURNS void AS
$BODY$
/* ************************************************************************************************/
/* Object Type/Name: Stored Procedure - sp_setallseq                                              */
/* Author:           Chandra M. Indipiginja                                                       */
/* Date:             Aug 12 2009                                                                  */
/* Description:      Reset the sequence value based on the column value                           */
/* Arguments:        None                                                                         */
/* Returns:          None                                                                         */
/* ************************************************************************************************/
/* Maintenance History:                                                                           */
/* Scan  Author       Date     Release    Description                                  Ticket #   */
/* ----- ---------- ---------- ---------- -------------------------------------------- ---------- */
DECLARE
	

		v_SQ_TB_JURISDICTION_TAXRATE_ID    numeric;
		v_SQ_TB_LOCATION_MATRIX_ID    numeric;
		v_SQ_GL_EXTRACT_BATCH_ID    numeric;
		v_SQ_TB_ALLOCATION_MATRIX_ID    numeric;
		v_SQ_TB_ALLOCATION_SUBTRANS_ID    numeric;
		v_SQ_TB_BATCH_ERROR_LOG_ID    numeric;
		v_SQ_TB_BATCH_ID    numeric;
		v_SQ_TB_ENTITY_ID    numeric;
		v_SQ_TB_GL_EXTRACT_MAP_ID    numeric;
		v_SQ_TB_JURISDICTION_ID    numeric;
		v_SQ_TB_PROCESS_COUNT    numeric;
		v_SQ_TB_QUESTIONANSWER_LOG_ID    numeric;
		v_SQ_TB_TAX_MATRIX_ID    numeric;
		v_SQ_TB_TAXCODE_DETAIL_ID    numeric;
		v_SQ_TB_TRANS_DETAIL_EXCP_ID    numeric;
		v_SQ_TB_TRANSACTION_DETAIL_ID    numeric;
		v_SQ_TB_BCP_JURIS_TAXRATE_ID    numeric;
		v_SQ_TB_BCP_TRANSACTIONS    numeric;

BEGIN
                      select coalesce(max(JURISDICTION_TAXRATE_ID),0)+1
                       INTO STRICT v_SQ_TB_JURISDICTION_TAXRATE_ID
                        from TB_JURISDICTION_TAXRATE;
                    execute 'alter sequence SQ_TB_JURISDICTION_TAXRATE_ID restart with '|| v_SQ_TB_JURISDICTION_TAXRATE_ID;


                        select coalesce(max(LOCATION_MATRIX_ID),0)+1
                        INTO STRICT v_SQ_TB_LOCATION_MATRIX_ID
                        from TB_LOCATION_MATRIX;

                    execute 'alter sequence SQ_TB_LOCATION_MATRIX_ID restart with '|| v_SQ_TB_LOCATION_MATRIX_ID;

                       select coalesce(max(GL_EXTRACT_MAP_ID),0)+1
                       INTO STRICT v_SQ_GL_EXTRACT_BATCH_ID
                       from TB_GL_EXTRACT_map;

                       execute 'alter sequence SQ_GL_EXTRACT_BATCH_ID restart with '|| v_SQ_GL_EXTRACT_BATCH_ID;

                        select coalesce(max(ALLOCATION_MATRIX_ID),0) +1
                        INTO STRICT v_SQ_TB_ALLOCATION_MATRIX_ID
                       from TB_ALLOCATION_MATRIX;
                    
                       execute 'alter sequence SQ_TB_ALLOCATION_MATRIX_ID restart with '|| v_SQ_TB_ALLOCATION_MATRIX_ID;
                     
                /*       select max(ALLOCATION_SUBTRANS_ID) 
                    --  INTO STRICT v_SQ_TB_ALLOCATION_SUBTRANS_ID
                       from TB_ALLOCATION_SUBTRANS;
                       execute 'alter sequence SQ_TB_ALLOCATION_SUBTRANS_ID restart with '|| v_SQ_TB_ALLOCATION_SUBTRANS_ID; */
                    

                      select coalesce(max(BATCH_ERROR_LOG_ID),0)+1
                       INTO STRICT v_SQ_TB_BATCH_ERROR_LOG_ID
                       from TB_BATCH_ERROR_LOG;
                       execute 'alter sequence SQ_TB_BATCH_ERROR_LOG_ID restart with '|| v_SQ_TB_BATCH_ERROR_LOG_ID;


                        select coalesce(max(BATCH_ID),0)+1
                        INTO STRICT v_SQ_TB_BATCH_ID
                        from TB_BATCH;
                    execute 'alter sequence SQ_TB_BATCH_ID restart with '|| v_SQ_TB_BATCH_ID;

                     select coalesce(max(ENTITY_ID),0)+1
                         INTO STRICT v_SQ_TB_ENTITY_ID
                         from TB_ENTITY;
                          
                    execute 'alter sequence SQ_TB_ENTITY_ID restart with '|| v_SQ_TB_ENTITY_ID;

                     select coalesce(max(GL_EXTRACT_MAP_ID),0) +1
                         INTO STRICT v_SQ_TB_GL_EXTRACT_MAP_ID
                         from TB_GL_EXTRACT_MAP;
                    
                    execute 'alter sequence SQ_TB_GL_EXTRACT_MAP_ID restart with '|| v_SQ_TB_GL_EXTRACT_MAP_ID;

                         select coalesce(max(JURISDICTION_ID),0)+1 
                         INTO STRICT v_SQ_TB_JURISDICTION_ID
                         from TB_JURISDICTION;
                    
                    execute 'alter sequence SQ_TB_JURISDICTION_ID restart with '|| v_SQ_TB_JURISDICTION_ID;
              
                      select coalesce(max(TRANSACTION_DETAIL_EXCP_ID),0)+1 
                       INTO STRICT v_SQ_TB_TRANS_DETAIL_EXCP_ID
                       from TB_TRANSACTION_DETAIL_EXCP;
                    execute 'alter sequence SQ_TB_TRANS_DETAIL_EXCP_ID restart with '|| v_SQ_TB_TRANS_DETAIL_EXCP_ID;
            
                    /* select max(PROCESS_COUNT) 
                         INTO STRICT v_SQ_TB_PROCESS_COUNT
                         from TB_PROCESS_COUNT;
                         
                    execute 'alter sequence SQ_TB_PROCESS_COUNT restart with '|| v_SQ_TB_PROCESS_COUNT;  

                         select max(QUESTIONANSWER_LOG_ID) 
                         INTO STRICT v_SQ_TB_QUESTIONANSWER_LOG_ID
                         from TB_QUESTIONANSWER_LOG;
                    execute 'alter sequence SQ_TB_QUESTIONANSWER_LOG_ID restart with '|| v_SQ_TB_QUESTIONANSWER_LOG_ID;  */

                    select coalesce(max(TAX_MATRIX_ID),0)+1 
                         INTO STRICT v_SQ_TB_TAX_MATRIX_ID
                         from TB_TAX_MATRIX;
                    execute 'alter sequence SQ_TB_TAX_MATRIX_ID restart with '|| v_SQ_TB_TAX_MATRIX_ID;

                         select coalesce(max(TAXCODE_DETAIL_ID),0)+1 
                        INTO STRICT v_SQ_TB_TAXCODE_DETAIL_ID
                         from TB_TAXCODE_DETAIL;
                    execute 'alter sequence SQ_TB_TAXCODE_DETAIL_ID restart with '|| v_SQ_TB_TAXCODE_DETAIL_ID;  

                       select coalesce(max(TRANSACTION_DETAIL_EXCP_ID),0)+1 
                       INTO STRICT v_SQ_TB_TRANS_DETAIL_EXCP_ID
                       from TB_TRANSACTION_DETAIL_EXCP;
                    execute 'alter sequence SQ_TB_TRANS_DETAIL_EXCP_ID restart with '|| v_SQ_TB_TRANS_DETAIL_EXCP_ID;


                       select coalesce(max(TRANSACTION_DETAIL_ID),0)+1 
                       INTO STRICT v_SQ_TB_TRANSACTION_DETAIL_ID
                       from TB_TRANSACTION_DETAIL;    
                    execute 'alter sequence SQ_TB_TRANSACTION_DETAIL_ID restart with '|| v_SQ_TB_TRANSACTION_DETAIL_ID;

                       select coalesce(max(BCP_JURISDICTION_TAXRATE_ID),0)+1 
                       INTO STRICT v_SQ_TB_BCP_JURIS_TAXRATE_ID
                       from TB_BCP_JURISDICTION_TAXRATE;
                    execute 'alter sequence SQ_TB_BCP_JURIS_TAXRATE_ID restart with '|| v_SQ_TB_BCP_JURIS_TAXRATE_ID; 


                      select coalesce(max(BCP_TRANSACTION_ID),0)+1
                      INTO STRICT v_SQ_TB_BCP_TRANSACTIONS
                       from TB_BCP_TRANSACTIONS;
                   
                    execute 'alter sequence SQ_TB_BCP_TRANSACTIONS restart with '|| v_SQ_TB_BCP_TRANSACTIONS;  
                    
          

                  
	
EXCEPTION
  WHEN OTHERS THEN
	RAISE WARNING 'EXCEPTION ';
END ;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
ALTER FUNCTION sp_setallseq() OWNER TO stscorp;
