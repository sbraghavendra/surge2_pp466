-- Function: sp_create_trans_indexes()

-- DROP FUNCTION sp_create_trans_indexes();

CREATE OR REPLACE FUNCTION sp_create_trans_indexes()
  RETURNS void AS
$BODY$
DECLARE
-- Program defined variables
   vc_statement                    varchar(1000)                            := '';
   vc_index_tablespace             varchar(30)                              := '';
   vi_rows_processed               integer                                  := 0;
   vi_cursor_id                    integer                                  := 0;
 
   tax_driver                      RECORD;

-- Program starts **********************************************************************************
BEGIN
   -- Get Index Tablespace name from preferences/options table
   BEGIN
      SELECT value
        INTO STRICT vc_index_tablespace
        FROM tb_option
       WHERE option_code = 'INDEX_TABLESPACE'
         AND option_type_code = 'ADMIN'
         AND user_code = 'ADMIN';
   EXCEPTION
      WHEN NO_DATA_FOUND THEN
         vc_index_tablespace := 'postdbtbs1';
   END;
   
   vc_index_tablespace := TRIM(vc_index_tablespace);
   IF vc_index_tablespace IS NULL OR vc_index_tablespace = '' THEN
      vc_index_tablespace := 'postdbtbs1';
   END IF;

   -- Create and execute sql to build index for each driver
   FOR tax_driver IN
      SELECT driver.trans_dtl_column_name, driver.matrix_column_name
        FROM tb_driver_names driver
        WHERE driver.driver_names_code = 'T'
        ORDER BY driver.driver_id
   LOOP
      -- Create dynamic SQL
      vc_statement := 'CREATE INDEX idx_tb_trans_dtl_' || tax_driver.matrix_column_name ||
                      ' ON tb_transaction_detail (' || tax_driver.trans_dtl_column_name ||
                      ')';

      -- Execute dynamic SQL
      EXECUTE vc_statement;
   END LOOP;

   --CM COMMIT;
END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
ALTER FUNCTION sp_create_trans_indexes() OWNER TO stscorp;
