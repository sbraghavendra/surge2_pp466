-- Function: sp_trans_reindex(character varying)

-- DROP FUNCTION sp_trans_reindex(character varying);

CREATE OR REPLACE FUNCTION sp_trans_reindex(param1 character varying)
  RETURNS void AS
$BODY$
DECLARE
-- param1 definition:
--  '1' = Build Transaction Detail Indexes only
--  '2' = Build Tax Matrix Indexes only
--  '9' = Build All Indexes

-- Program defined variables
   vc_statement       VARCHAR (250)               := ' ';
   v_number_of_rows   NUMERIC                     := 0;
   v_rows_processed   INTEGER                     := 0;
   v_cursor_id        INTEGER                     := 0;

-- Create Cursor record for Transactions
   trans_record      RECORD;

-- Create Cursor record for Tax Matrix
   matrix_record     RECORD;      

-- Create Cursor record for Both
   both_record       RECORD;

-- Program starts **********************************************************************************
BEGIN
   -- Transation Index Cursor
   IF param1 = '1' THEN
      FOR trans_record IN
	SELECT indexname
         FROM pg_indexes
         WHERE indexname like 'idx_tb_trans_dtl_driver_%'
         AND schemaname = 'stscorp'
      LOOP
         vc_statement := 'DROP INDEX ' || trans_record.indexname;

          BEGIN
		     EXECUTE vc_statement;
		     EXCEPTION 
		       WHEN OTHERS THEN 
		     RAISE NOTICE 'vc_statement others(%)', vc_statement;
	  END;

        
     RAISE NOTICE 'vc_statement(%)', vc_statement;

      END LOOP;
      BEGIN
        PERFORM sp_create_trans_indexes();
        PERFORM sp_create_trans_funct_indexes();
      END;
   END IF;

   -- Tax Matrix Index Cursor
   IF param1 = '2' THEN
      FOR matrix_record IN
	SELECT indexname
         FROM pg_indexes
         WHERE indexname like 'idx_tb_tax_matrix_driver_%'
         AND schemaname = 'stscorp'
      LOOP
         vc_statement :='DROP INDEX ' || matrix_record.indexname;

         EXECUTE vc_statement;
      END LOOP;
      BEGIN
        PERFORM sp_create_matrix_indexes();
        PERFORM sp_create_matrix_indexes_func();
      END;
   END IF;

   -- Both Index Cursor
   IF param1 = '9' THEN
      FOR both_record IN
	SELECT indexname
         FROM pg_indexes
         WHERE indexname like 'idx_tb_trans_dtl_driver_%' OR indexname like 'idx_tb_tax_matrix_driver_%'
         AND schemaname = 'stscorp'
      LOOP
         vc_statement :='DROP INDEX ' || both_record.indexname;
         EXECUTE vc_statement;
      END LOOP;
      BEGIN
         PERFORM sp_create_trans_indexes();
         PERFORM sp_create_trans_funct_indexes();
         PERFORM sp_create_matrix_indexes();
         PERFORM sp_create_matrix_indexes_func();
      END;
   END IF;
END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
ALTER FUNCTION sp_trans_reindex(character varying) OWNER TO stscorp;
