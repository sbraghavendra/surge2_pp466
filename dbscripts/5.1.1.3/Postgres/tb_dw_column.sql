-- Table: tb_dw_column

-- DROP TABLE tb_dw_column;

CREATE TABLE tb_dw_column
(
  window_name character varying(40),
  datawindow_name character varying(40),
  user_code character varying(40) NOT NULL,
  column_order numeric NOT NULL,
  column_name character varying(40),
  column_width numeric,
  column_x numeric,
  all_columns character(1)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE tb_dw_column OWNER TO stscorp;