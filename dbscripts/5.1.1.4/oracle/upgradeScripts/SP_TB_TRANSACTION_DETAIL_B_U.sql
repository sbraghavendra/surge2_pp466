CREATE OR REPLACE PROCEDURE Sp_Tb_Transaction_Detail_B_U(
	p_old_tb_transaction_detail IN     TB_TRANSACTION_DETAIL%ROWTYPE,
	p_new_tb_transaction_detail IN OUT TB_TRANSACTION_DETAIL%ROWTYPE
) AS
/* ************************************************************************************************/
/* Object Type/Name: Trigger/Before Update - tb_transaction_detail_b_u                            */
/* Author:           Michael B. Fuller                                                            */
/* Date:             a long time ago                                                              */
/* Description:      A transaction has been changed                                               */
/* Arguments:        None                                                                         */
/* Returns:          None                                                                         */
/* ************************************************************************************************/
/* Maintenance History:                                                                           */
/* Scan  Author       Date     Release    Description                                  Ticket #   */
/* ----- ---------- ---------- ---------- -------------------------------------------- ---------- */
/*       M. Fuller  06/02/2006 3.3.5.0    Fix processing an allocated line - Temporary 871        */
/*       M. Fuller  01/04/2007 3.3.5.1    Fix processing an allocated line - Permenant 871        */
/* MBF01 M. Fuller  02/09/2007 3.3.5.1    Correct allocated lines with no locn matrix id          */
/* MBF02 M. Fuller  08/13/2007 3.4.1.1    Add 5 Calc Tax Flags                         883        */
/* MBF03 M. Fuller  03/06/2008 3.4.2.1    Correct If/Then/Else's for "AO"              913        */
/* JFF   J. Franco  12/17/2008 x.x.x.x    Convert trigger to stored procedure          xxx        */
/* ************************************************************************************************/

-- Define Cursor Type
   TYPE cursor_type IS REF CURSOR;

-- Location Matrix Selection SQL      // MBF02
   vc_location_matrix_select       VARCHAR2(1000) :=
      'SELECT tb_location_matrix.location_matrix_id, ' ||
             'tb_location_matrix.jurisdiction_id, ' ||
             'tb_location_matrix.override_taxtype_code, ' ||
             'tb_location_matrix.state_flag,' ||
             'tb_location_matrix.county_flag,' ||
             'tb_location_matrix.county_local_flag,' ||
             'tb_location_matrix.city_flag,' ||
             'tb_location_matrix.city_local_flag,' ||
             'tb_jurisdiction.state, ' ||
             'tb_jurisdiction.county, ' ||
             'tb_jurisdiction.city, ' ||
             'tb_jurisdiction.zip, ' ||
             'tb_jurisdiction.in_out ' ||
        'FROM tb_location_matrix, ' ||
             'tb_jurisdiction, ' ||
             'tb_tmp_transaction_detail ' ||
      'WHERE ( tb_tmp_transaction_detail.transaction_detail_id = :v_transaction_detail_id ) AND ' ||
            '( tb_location_matrix.default_flag = ''0'' AND tb_location_matrix.binary_weight > 0 ) AND ' ||
            '( tb_location_matrix.effective_date <= tb_tmp_transaction_detail.gl_date ) AND ( tb_location_matrix.expiration_date >= tb_tmp_transaction_detail.gl_date ) AND ' ||
            '( tb_location_matrix.jurisdiction_id = tb_jurisdiction.jurisdiction_id ) ';
   vc_location_matrix_where        VARCHAR2(3000) := '';
   vc_location_matrix_orderby      VARCHAR2(1000) :=
      'ORDER BY tb_location_matrix.binary_weight DESC, tb_location_matrix.significant_digits DESC, tb_location_matrix.effective_date DESC';
   vc_location_matrix_stmt         VARCHAR2 (5000);
   location_matrix_cursor          cursor_type;

-- Location Matrix Record TYPE
   /*TYPE location_matrix_record is RECORD (
      location_matrix_id           tb_location_matrix.location_matrix_id%TYPE,
      jurisdiction_id              tb_location_matrix.jurisdiction_id%TYPE,
      override_taxtype_code        tb_location_matrix.override_taxtype_code%TYPE,
      state_flag                   tb_location_matrix.state_flag%TYPE,
      county_flag                  tb_location_matrix.county_flag%TYPE,
      county_local_flag            tb_location_matrix.county_local_flag%TYPE,
      city_flag                    tb_location_matrix.city_flag%TYPE,
      city_local_flag              tb_location_matrix.city_local_flag%TYPE,
      state                        tb_jurisdiction.state%TYPE,
      county                       tb_jurisdiction.county%TYPE,
      city                         tb_jurisdiction.city%TYPE,
      zip                          tb_jurisdiction.zip%TYPE,
      in_out                       tb_jurisdiction.in_out%TYPE );*/
   location_matrix                 Matrix_Record_Pkg.location_matrix_record;

-- Tax Matrix Selection SQL
   vc_tax_matrix_select            VARCHAR2(2000) :=
      'SELECT tb_tax_matrix.tax_matrix_id, tb_tax_matrix.relation_sign, tb_tax_matrix.relation_amount, ' ||
             'tb_tax_matrix.then_hold_code_flag, tb_tax_matrix.else_hold_code_flag, ' ||
             'tb_tax_matrix.then_taxcode_detail_id, tb_tax_matrix.else_taxcode_detail_id, ' ||
             'tb_tax_matrix.then_cch_taxcat_code, tb_tax_matrix.then_cch_group_code, tb_tax_matrix.then_cch_item_code, ' ||
             'tb_tax_matrix.else_cch_taxcat_code, tb_tax_matrix.else_cch_group_code, tb_tax_matrix.else_cch_item_code, ' ||
             'thentaxcddtl.taxcode_state_code, thentaxcddtl.taxcode_type_code, thentaxcddtl.taxcode_code, thentaxcddtl.jurisdiction_id, thentaxcddtl.measure_type_code, thentaxcddtl.taxtype_code, ' ||
             'elsetaxcddtl.taxcode_state_code, elsetaxcddtl.taxcode_type_code, elsetaxcddtl.taxcode_code, elsetaxcddtl.jurisdiction_id, elsetaxcddtl.measure_type_code, elsetaxcddtl.taxtype_code ' ||
       'FROM tb_tax_matrix, ' ||
            'tb_taxcode_detail thentaxcddtl, ' ||
            'tb_taxcode_detail elsetaxcddtl, ' ||
            'tb_tmp_transaction_detail ' ||
      'WHERE ( tb_tmp_transaction_detail.transaction_detail_id = :v_transaction_detail_id ) AND ' ||
            '( tb_tax_matrix.default_flag = ''0'' AND tb_tax_matrix.binary_weight > 0 ) AND ' ||
            '(( tb_tax_matrix.matrix_state_code = ''*ALL'' ) OR ( tb_tax_matrix.matrix_state_code = :v_transaction_state_code )) AND ' ||
            '( tb_tax_matrix.effective_date <= tb_tmp_transaction_detail.gl_date ) AND ( tb_tax_matrix.expiration_date >= tb_tmp_transaction_detail.gl_date ) AND ' ||
            '( tb_tax_matrix.then_taxcode_detail_id = thentaxcddtl.taxcode_detail_id (+)) AND ' ||
            '( tb_tax_matrix.else_taxcode_detail_id = elsetaxcddtl.taxcode_detail_id (+)) ';
   vc_tax_matrix_where             VARCHAR2(6000) := '';
   vc_tax_matrix_orderby           VARCHAR2(1000) :=
      'ORDER BY tb_tax_matrix.binary_weight DESC, tb_tax_matrix.significant_digits DESC, tb_tax_matrix.effective_date DESC';
   vc_tax_matrix_stmt              VARCHAR2 (9000);
   tax_matrix_cursor               cursor_type;

-- Tax Matrix Record TYPE
   /*TYPE tax_matrix_record is RECORD (
      tax_matrix_id                tb_tax_matrix.tax_matrix_id%TYPE,
      relation_sign                tb_tax_matrix.relation_sign%TYPE,
      relation_amount              tb_tax_matrix.relation_amount%TYPE,
      then_hold_code_flag          tb_tax_matrix.then_hold_code_flag%TYPE,
      else_hold_code_flag          tb_tax_matrix.else_hold_code_flag%TYPE,
      then_taxcode_detail_id       tb_bcp_transactions.taxcode_detail_id%TYPE,
      else_taxcode_detail_id       tb_bcp_transactions.taxcode_detail_id%TYPE,
      then_cch_taxcat_code         tb_tax_matrix.then_cch_taxcat_code%TYPE,
      then_cch_group_code          tb_tax_matrix.then_cch_group_code%TYPE,
      then_cch_item_code           tb_tax_matrix.then_cch_item_code%TYPE,
      else_cch_taxcat_code         tb_tax_matrix.else_cch_taxcat_code%TYPE,
      else_cch_group_code          tb_tax_matrix.else_cch_group_code%TYPE,
      else_cch_item_code           tb_tax_matrix.else_cch_item_code%TYPE,
      then_taxcode_state_code      tb_bcp_transactions.taxcode_state_code%TYPE,
      then_taxcode_type_code       tb_bcp_transactions.taxcode_type_code%TYPE,
      then_taxcode_code            tb_bcp_transactions.taxcode_code%TYPE,
      then_jurisdiction_id         tb_bcp_transactions.jurisdiction_id%TYPE,
      then_measure_type_code       tb_bcp_transactions.measure_type_code%TYPE,
      then_taxtype_code            tb_taxcode.taxtype_code%TYPE,
      else_taxcode_state_code      tb_bcp_transactions.taxcode_state_code%TYPE,
      else_taxcode_type_code       tb_bcp_transactions.taxcode_type_code%TYPE,
      else_taxcode_code            tb_bcp_transactions.taxcode_code%TYPE,
      else_jurisdiction_id         tb_bcp_transactions.jurisdiction_id%TYPE,
      else_measure_type_code       tb_bcp_transactions.measure_type_code%TYPE,
      else_taxtype_code            tb_taxcode.taxtype_code%TYPE );*/
   tax_matrix                      Matrix_Record_Pkg.tax_matrix_record;

-- Table defined variables
   v_hold_code_flag                TB_TAX_MATRIX.then_hold_code_flag%TYPE            := NULL;
   v_less_hold_code_flag           TB_TAX_MATRIX.then_hold_code_flag%TYPE            := NULL;
   v_less_taxcode_detail_id        TB_TAX_MATRIX.then_taxcode_detail_id%TYPE         := NULL;
   v_less_cch_taxcat_code          TB_TAX_MATRIX.then_cch_taxcat_code%TYPE           := NULL;
   v_less_cch_group_code           TB_TAX_MATRIX.then_cch_group_code%TYPE            := NULL;
   v_equal_hold_code_flag          TB_TAX_MATRIX.then_hold_code_flag%TYPE            := NULL;
   v_equal_taxcode_detail_id       TB_TAX_MATRIX.then_taxcode_detail_id%TYPE         := NULL;
   v_equal_cch_taxcat_code         TB_TAX_MATRIX.then_cch_taxcat_code%TYPE           := NULL;
   v_equal_cch_group_code          TB_TAX_MATRIX.then_cch_group_code%TYPE            := NULL;
   v_greater_hold_code_flag        TB_TAX_MATRIX.then_hold_code_flag%TYPE            := NULL;
   v_greater_taxcode_detail_id     TB_TAX_MATRIX.then_taxcode_detail_id%TYPE         := NULL;
   v_greater_cch_taxcat_code       TB_TAX_MATRIX.then_cch_taxcat_code%TYPE           := NULL;
   v_greater_cch_group_code        TB_TAX_MATRIX.then_cch_group_code%TYPE            := NULL;
   v_jurisdiction_id               TB_JURISDICTION.jurisdiction_id%TYPE              := NULL;
   v_less_taxcode_state_code       TB_TAXCODE_DETAIL.taxcode_state_code%TYPE         := NULL;
   v_less_taxcode_type_code        TB_TAXCODE_DETAIL.taxcode_type_code%TYPE          := NULL;
   v_less_taxcode_code             TB_TAXCODE_DETAIL.taxcode_code%TYPE               := NULL;
   v_less_jurisdiction_id          TB_TAXCODE_DETAIL.jurisdiction_id%TYPE            := NULL;
   v_less_measure_type_code        TB_TAXCODE_DETAIL.measure_type_code%TYPE          := NULL;
   v_less_taxtype_code             TB_TAXCODE_DETAIL.taxtype_code%TYPE               := NULL;
   v_equal_taxcode_state_code      TB_TAXCODE_DETAIL.taxcode_state_code%TYPE         := NULL;
   v_equal_taxcode_type_code       TB_TAXCODE_DETAIL.taxcode_type_code%TYPE          := NULL;
   v_equal_taxcode_code            TB_TAXCODE_DETAIL.taxcode_code%TYPE               := NULL;
   v_equal_jurisdiction_id         TB_TAXCODE_DETAIL.jurisdiction_id%TYPE            := NULL;
   v_equal_measure_type_code       TB_TAXCODE_DETAIL.measure_type_code%TYPE          := NULL;
   v_equal_taxtype_code            TB_TAXCODE_DETAIL.taxtype_code%TYPE               := NULL;
   v_greater_taxcode_state_code    TB_TAXCODE_DETAIL.taxcode_state_code%TYPE         := NULL;
   v_greater_taxcode_type_code     TB_TAXCODE_DETAIL.taxcode_type_code%TYPE          := NULL;
   v_greater_taxcode_code          TB_TAXCODE_DETAIL.taxcode_code%TYPE               := NULL;
   v_greater_jurisdiction_id       TB_TAXCODE_DETAIL.jurisdiction_id%TYPE            := NULL;
   v_greater_measure_type_code     TB_TAXCODE_DETAIL.measure_type_code%TYPE          := NULL;
   v_greater_taxtype_code          TB_TAXCODE_DETAIL.taxtype_code%TYPE               := NULL;
   v_transaction_detail_id         TB_TRANSACTION_DETAIL.transaction_detail_id%TYPE  := NULL;
   v_transaction_state_code        TB_TRANSACTION_DETAIL.transaction_state_code%TYPE := NULL;
   v_override_jurisdiction_id      TB_TRANSACTION_DETAIL.jurisdiction_id%TYPE        := NULL;
   v_sysdate                       TB_TRANSACTION_DETAIL.load_timestamp%TYPE         := SYS_EXTRACT_UTC(SYSTIMESTAMP);
   v_sysdate_plus                  TB_TRANSACTION_DETAIL.load_timestamp%TYPE         := v_sysdate + (1/86400);
   v_taxtype_code                  TB_TAXCODE.taxtype_code%TYPE                      := NULL;
   v_taxtype_used                  TB_TAXCODE.taxtype_code%TYPE                      := NULL;

-- Program defined variables
   vn_fetch_rows                   NUMBER                                            := 0;
   vc_state_driver_flag            CHAR(1)                                           := '0';
-- temporary
   vn_taxable_amt                  NUMBER                                            := 0;

-- Define Location Driver Names Cursor (tb_driver_names)
   /*CURSOR location_driver_cursor
   IS
        SELECT driver.trans_dtl_column_name, driver.matrix_column_name, driver.null_driver_flag, driver.wildcard_flag,
               datadefcol.desc_column_name, datadefcol.data_type
          FROM tb_driver_names driver,
               (SELECT *
                  FROM tb_data_def_column
                 WHERE table_name = 'TB_TRANSACTION_DETAIL') datadefcol
         WHERE driver.driver_names_code = 'L' AND
               driver.trans_dtl_column_name = datadefcol.column_name
      ORDER BY driver.driver_id;
      location_driver              location_driver_cursor%ROWTYPE;*/

-- Define Tax Driver Names Cursor (tb_driver_names)
   /*CURSOR tax_driver_cursor
   IS
        SELECT driver.trans_dtl_column_name, driver.matrix_column_name, driver.null_driver_flag, driver.wildcard_flag, driver.range_flag, to_number_flag,
               datadefcol.desc_column_name, datadefcol.data_type
          FROM tb_driver_names driver,
               (SELECT *
                  FROM tb_data_def_column
                 WHERE table_name = 'TB_TRANSACTION_DETAIL') datadefcol
         WHERE driver.driver_names_code = 'T' AND
               driver.trans_dtl_column_name = datadefcol.column_name
      ORDER BY driver.driver_id;
      tax_driver                   tax_driver_cursor%ROWTYPE;*/

-- Define Exceptions
   e_badread                       EXCEPTION;

-- ****** Program starts ****** --------------------------------------------------------------------
BEGIN
   p_new_tb_transaction_detail.update_user_id   := USER;
   p_new_tb_transaction_detail.update_timestamp := v_sysdate;

   -- If was processed and now processed OR
   --    was tax suspended and now processed and now manual taxcode indicator not null OR
   --    was locn suspended and now processed and now manual jurisdiction indicator not null THEN
--   IF ( UPPER(p_old_tb_transaction_detail.transaction_ind) = 'P' AND UPPER(p_new_tb_transaction_detail.transaction_ind) = 'P' ) OR
--      ( UPPER(p_old_tb_transaction_detail.transaction_ind) = 'S' AND UPPER(p_old_tb_transaction_detail.suspend_ind) = 'T' AND p_new_tb_transaction_detail.manual_taxcode_ind IS NOT NULL ) OR
--      ( UPPER(p_old_tb_transaction_detail.transaction_ind) = 'S' AND UPPER(p_old_tb_transaction_detail.suspend_ind) = 'L' AND p_new_tb_transaction_detail.manual_jurisdiction_ind IS NOT NULL ) THEN
   IF (( NOT ( UPPER(p_old_tb_transaction_detail.transaction_ind) = 'P' AND UPPER(p_new_tb_transaction_detail.transaction_ind) = 'S' )) AND
      ((( p_old_tb_transaction_detail.taxcode_detail_id IS NOT NULL AND p_old_tb_transaction_detail.taxcode_detail_id <> p_new_tb_transaction_detail.taxcode_detail_id ) OR
        ( p_old_tb_transaction_detail.taxcode_detail_id IS NOT NULL AND p_new_tb_transaction_detail.taxcode_detail_id IS NULL ) OR
        ( p_old_tb_transaction_detail.taxcode_detail_id IS NULL AND p_new_tb_transaction_detail.taxcode_detail_id IS NOT NULL AND p_new_tb_transaction_detail.manual_taxcode_ind IS NOT NULL )) OR
       (( p_old_tb_transaction_detail.jurisdiction_id IS NOT NULL AND p_old_tb_transaction_detail.jurisdiction_id <> p_new_tb_transaction_detail.jurisdiction_id ) OR
        ( p_old_tb_transaction_detail.jurisdiction_id IS NOT NULL AND p_new_tb_transaction_detail.jurisdiction_id IS NULL ) OR
        ( p_old_tb_transaction_detail.jurisdiction_id IS NULL AND p_new_tb_transaction_detail.jurisdiction_id IS NOT NULL AND p_new_tb_transaction_detail.manual_jurisdiction_ind IS NOT NULL AND p_new_tb_transaction_detail.manual_jurisdiction_ind <> 'AO' )) OR
       (( (p_old_tb_transaction_detail.jurisdiction_taxrate_id IS NOT NULL AND p_old_tb_transaction_detail.jurisdiction_taxrate_id <> 0) AND p_old_tb_transaction_detail.jurisdiction_taxrate_id <> p_new_tb_transaction_detail.jurisdiction_taxrate_id ) OR
        ( (p_old_tb_transaction_detail.jurisdiction_taxrate_id IS NOT NULL AND p_old_tb_transaction_detail.jurisdiction_taxrate_id <> 0) AND (p_new_tb_transaction_detail.jurisdiction_taxrate_id IS NULL OR p_new_tb_transaction_detail.jurisdiction_taxrate_id = 0) ))))
   AND NOT (( UPPER(p_old_tb_transaction_detail.transaction_ind) = 'S' AND UPPER(p_new_tb_transaction_detail.transaction_ind) = 'S' ) AND
       (( p_old_tb_transaction_detail.taxcode_detail_id IS NOT NULL AND p_new_tb_transaction_detail.taxcode_detail_id IS NULL ) OR
        ( p_old_tb_transaction_detail.jurisdiction_id IS NOT NULL AND p_new_tb_transaction_detail.jurisdiction_id IS NULL ))) THEN
      -- Initialize Transaction Indicator and Suspended Indicator
      p_new_tb_transaction_detail.transaction_ind := NULL;
      p_new_tb_transaction_detail.suspend_ind := NULL;
      -- Insert New Tax Matrix columns into temporary table -------------------------------------------
      -- 3411 - add taxable amount and tax type used.
      INSERT INTO TB_TMP_TRANSACTION_DETAIL (
         transaction_detail_id,
         source_transaction_id,
         process_batch_no,
         gl_extract_batch_no,
         archive_batch_no,
         allocation_matrix_id,
         allocation_subtrans_id,
         entered_date,
         transaction_status,
         gl_date,
         gl_company_nbr,
         gl_company_name,
         gl_division_nbr,
         gl_division_name,
         gl_cc_nbr_dept_id,
         gl_cc_nbr_dept_name,
         gl_local_acct_nbr,
         gl_local_acct_name,
         gl_local_sub_acct_nbr,
         gl_local_sub_acct_name,
         gl_full_acct_nbr,
         gl_full_acct_name,
         gl_line_itm_dist_amt,
         orig_gl_line_itm_dist_amt,
         vendor_nbr,
         vendor_name,
         vendor_address_line_1,
         vendor_address_line_2,
         vendor_address_line_3,
         vendor_address_line_4,
         vendor_address_city,
         vendor_address_county,
         vendor_address_state,
         vendor_address_zip,
         vendor_address_country,
         vendor_type,
         vendor_type_name,
         invoice_nbr,
         invoice_desc,
         invoice_date,
         invoice_freight_amt,
         invoice_discount_amt,
         invoice_tax_amt,
         invoice_total_amt,
         invoice_tax_flg,
         invoice_line_nbr,
         invoice_line_name,
         invoice_line_type,
         invoice_line_type_name,
         invoice_line_amt,
         invoice_line_tax,
         afe_project_nbr,
         afe_project_name,
         afe_category_nbr,
         afe_category_name,
         afe_sub_cat_nbr,
         afe_sub_cat_name,
         afe_use,
         afe_contract_type,
         afe_contract_structure,
         afe_property_cat,
         inventory_nbr,
         inventory_name,
         inventory_class,
         inventory_class_name,
         po_nbr,
         po_name,
         po_date,
         po_line_nbr,
         po_line_name,
         po_line_type,
         po_line_type_name,
         ship_to_location,
         ship_to_location_name,
         ship_to_address_line_1,
         ship_to_address_line_2,
         ship_to_address_line_3,
         ship_to_address_line_4,
         ship_to_address_city,
         ship_to_address_county,
         ship_to_address_state,
         ship_to_address_zip,
         ship_to_address_country,
         wo_nbr,
         wo_name,
         wo_date,
         wo_type,
         wo_type_desc,
         wo_class,
         wo_class_desc,
         wo_entity,
         wo_entity_desc,
         wo_line_nbr,
         wo_line_name,
         wo_line_type,
         wo_line_type_desc,
         wo_shut_down_cd,
         wo_shut_down_cd_desc,
         voucher_id,
         voucher_name,
         voucher_date,
         voucher_line_nbr,
         voucher_line_desc,
         check_nbr,
         check_no,
         check_date,
         check_amt,
         check_desc,
         user_text_01,
         user_text_02,
         user_text_03,
         user_text_04,
         user_text_05,
         user_text_06,
         user_text_07,
         user_text_08,
         user_text_09,
         user_text_10,
         user_text_11,
         user_text_12,
         user_text_13,
         user_text_14,
         user_text_15,
         user_text_16,
         user_text_17,
         user_text_18,
         user_text_19,
         user_text_20,
         user_text_21,
         user_text_22,
         user_text_23,
         user_text_24,
         user_text_25,
         user_text_26,
         user_text_27,
         user_text_28,
         user_text_29,
         user_text_30,
         user_number_01,
         user_number_02,
         user_number_03,
         user_number_04,
         user_number_05,
         user_number_06,
         user_number_07,
         user_number_08,
         user_number_09,
         user_number_10,
         user_date_01,
         user_date_02,
         user_date_03,
         user_date_04,
         user_date_05,
         user_date_06,
         user_date_07,
         user_date_08,
         user_date_09,
         user_date_10,
         comments,
         tb_calc_tax_amt,
         state_use_amount,
         state_use_tier2_amount,
         state_use_tier3_amount,
         county_use_amount,
         county_local_use_amount,
         city_use_amount,
         city_local_use_amount,
         transaction_state_code,
         transaction_ind,
         suspend_ind,
         taxcode_detail_id,
         taxcode_state_code,
         taxcode_type_code,
         taxcode_code,
         cch_taxcat_code,
         cch_group_code,
         cch_item_code,
         manual_taxcode_ind,
         tax_matrix_id,
         location_matrix_id,
         jurisdiction_id,
         jurisdiction_taxrate_id,
         manual_jurisdiction_ind,
         measure_type_code,
         state_use_rate,
         state_use_tier2_rate,
         state_use_tier3_rate,
         state_split_amount,
         state_tier2_min_amount,
         state_tier2_max_amount,
         state_maxtax_amount,
         county_use_rate,
         county_local_use_rate,
         county_split_amount,
         county_maxtax_amount,
         county_single_flag,
         county_default_flag,
         city_use_rate,
         city_local_use_rate,
         city_split_amount,
         city_split_use_rate,
         city_single_flag,
         city_default_flag,
         combined_use_rate,
         load_timestamp,
         gl_extract_updater,
         gl_extract_timestamp,
         gl_extract_flag,
         gl_log_flag,
         gl_extract_amt,
         audit_flag,
         audit_user_id,
         audit_timestamp,
         modify_user_id,
         modify_timestamp,
         update_user_id,
         update_timestamp )
      VALUES (
         p_new_tb_transaction_detail.transaction_detail_id,
         p_new_tb_transaction_detail.source_transaction_id,
         p_new_tb_transaction_detail.process_batch_no,
         p_new_tb_transaction_detail.gl_extract_batch_no,
         p_new_tb_transaction_detail.archive_batch_no,
         p_new_tb_transaction_detail.allocation_matrix_id,
         p_new_tb_transaction_detail.allocation_subtrans_id,
         p_new_tb_transaction_detail.entered_date,
         p_new_tb_transaction_detail.transaction_status,
         p_new_tb_transaction_detail.gl_date,
         p_new_tb_transaction_detail.gl_company_nbr,
         p_new_tb_transaction_detail.gl_company_name,
         p_new_tb_transaction_detail.gl_division_nbr,
         p_new_tb_transaction_detail.gl_division_name,
         p_new_tb_transaction_detail.gl_cc_nbr_dept_id,
         p_new_tb_transaction_detail.gl_cc_nbr_dept_name,
         p_new_tb_transaction_detail.gl_local_acct_nbr,
         p_new_tb_transaction_detail.gl_local_acct_name,
         p_new_tb_transaction_detail.gl_local_sub_acct_nbr,
         p_new_tb_transaction_detail.gl_local_sub_acct_name,
         p_new_tb_transaction_detail.gl_full_acct_nbr,
         p_new_tb_transaction_detail.gl_full_acct_name,
         p_new_tb_transaction_detail.gl_line_itm_dist_amt,
         p_new_tb_transaction_detail.orig_gl_line_itm_dist_amt,
         p_new_tb_transaction_detail.vendor_nbr,
         p_new_tb_transaction_detail.vendor_name,
         p_new_tb_transaction_detail.vendor_address_line_1,
         p_new_tb_transaction_detail.vendor_address_line_2,
         p_new_tb_transaction_detail.vendor_address_line_3,
         p_new_tb_transaction_detail.vendor_address_line_4,
         p_new_tb_transaction_detail.vendor_address_city,
         p_new_tb_transaction_detail.vendor_address_county,
         p_new_tb_transaction_detail.vendor_address_state,
         p_new_tb_transaction_detail.vendor_address_zip,
         p_new_tb_transaction_detail.vendor_address_country,
         p_new_tb_transaction_detail.vendor_type,
         p_new_tb_transaction_detail.vendor_type_name,
         p_new_tb_transaction_detail.invoice_nbr,
         p_new_tb_transaction_detail.invoice_desc,
         p_new_tb_transaction_detail.invoice_date,
         p_new_tb_transaction_detail.invoice_freight_amt,
         p_new_tb_transaction_detail.invoice_discount_amt,
         p_new_tb_transaction_detail.invoice_tax_amt,
         p_new_tb_transaction_detail.invoice_total_amt,
         p_new_tb_transaction_detail.invoice_tax_flg,
         p_new_tb_transaction_detail.invoice_line_nbr,
         p_new_tb_transaction_detail.invoice_line_name,
         p_new_tb_transaction_detail.invoice_line_type,
         p_new_tb_transaction_detail.invoice_line_type_name,
         p_new_tb_transaction_detail.invoice_line_amt,
         p_new_tb_transaction_detail.invoice_line_tax,
         p_new_tb_transaction_detail.afe_project_nbr,
         p_new_tb_transaction_detail.afe_project_name,
         p_new_tb_transaction_detail.afe_category_nbr,
         p_new_tb_transaction_detail.afe_category_name,
         p_new_tb_transaction_detail.afe_sub_cat_nbr,
         p_new_tb_transaction_detail.afe_sub_cat_name,
         p_new_tb_transaction_detail.afe_use,
         p_new_tb_transaction_detail.afe_contract_type,
         p_new_tb_transaction_detail.afe_contract_structure,
         p_new_tb_transaction_detail.afe_property_cat,
         p_new_tb_transaction_detail.inventory_nbr,
         p_new_tb_transaction_detail.inventory_name,
         p_new_tb_transaction_detail.inventory_class,
         p_new_tb_transaction_detail.inventory_class_name,
         p_new_tb_transaction_detail.po_nbr,
         p_new_tb_transaction_detail.po_name,
         p_new_tb_transaction_detail.po_date,
         p_new_tb_transaction_detail.po_line_nbr,
         p_new_tb_transaction_detail.po_line_name,
         p_new_tb_transaction_detail.po_line_type,
         p_new_tb_transaction_detail.po_line_type_name,
         p_new_tb_transaction_detail.ship_to_location,
         p_new_tb_transaction_detail.ship_to_location_name,
         p_new_tb_transaction_detail.ship_to_address_line_1,
         p_new_tb_transaction_detail.ship_to_address_line_2,
         p_new_tb_transaction_detail.ship_to_address_line_3,
         p_new_tb_transaction_detail.ship_to_address_line_4,
         p_new_tb_transaction_detail.ship_to_address_city,
         p_new_tb_transaction_detail.ship_to_address_county,
         p_new_tb_transaction_detail.ship_to_address_state,
         p_new_tb_transaction_detail.ship_to_address_zip,
         p_new_tb_transaction_detail.ship_to_address_country,
         p_new_tb_transaction_detail.wo_nbr,
         p_new_tb_transaction_detail.wo_name,
         p_new_tb_transaction_detail.wo_date,
         p_new_tb_transaction_detail.wo_type,
         p_new_tb_transaction_detail.wo_type_desc,
         p_new_tb_transaction_detail.wo_class,
         p_new_tb_transaction_detail.wo_class_desc,
         p_new_tb_transaction_detail.wo_entity,
         p_new_tb_transaction_detail.wo_entity_desc,
         p_new_tb_transaction_detail.wo_line_nbr,
         p_new_tb_transaction_detail.wo_line_name,
         p_new_tb_transaction_detail.wo_line_type,
         p_new_tb_transaction_detail.wo_line_type_desc,
         p_new_tb_transaction_detail.wo_shut_down_cd,
         p_new_tb_transaction_detail.wo_shut_down_cd_desc,
         p_new_tb_transaction_detail.voucher_id,
         p_new_tb_transaction_detail.voucher_name,
         p_new_tb_transaction_detail.voucher_date,
         p_new_tb_transaction_detail.voucher_line_nbr,
         p_new_tb_transaction_detail.voucher_line_desc,
         p_new_tb_transaction_detail.check_nbr,
         p_new_tb_transaction_detail.check_no,
         p_new_tb_transaction_detail.check_date,
         p_new_tb_transaction_detail.check_amt,
         p_new_tb_transaction_detail.check_desc,
         p_new_tb_transaction_detail.user_text_01,
         p_new_tb_transaction_detail.user_text_02,
         p_new_tb_transaction_detail.user_text_03,
         p_new_tb_transaction_detail.user_text_04,
         p_new_tb_transaction_detail.user_text_05,
         p_new_tb_transaction_detail.user_text_06,
         p_new_tb_transaction_detail.user_text_07,
         p_new_tb_transaction_detail.user_text_08,
         p_new_tb_transaction_detail.user_text_09,
         p_new_tb_transaction_detail.user_text_10,
         p_new_tb_transaction_detail.user_text_11,
         p_new_tb_transaction_detail.user_text_12,
         p_new_tb_transaction_detail.user_text_13,
         p_new_tb_transaction_detail.user_text_14,
         p_new_tb_transaction_detail.user_text_15,
         p_new_tb_transaction_detail.user_text_16,
         p_new_tb_transaction_detail.user_text_17,
         p_new_tb_transaction_detail.user_text_18,
         p_new_tb_transaction_detail.user_text_19,
         p_new_tb_transaction_detail.user_text_20,
         p_new_tb_transaction_detail.user_text_21,
         p_new_tb_transaction_detail.user_text_22,
         p_new_tb_transaction_detail.user_text_23,
         p_new_tb_transaction_detail.user_text_24,
         p_new_tb_transaction_detail.user_text_25,
         p_new_tb_transaction_detail.user_text_26,
         p_new_tb_transaction_detail.user_text_27,
         p_new_tb_transaction_detail.user_text_28,
         p_new_tb_transaction_detail.user_text_29,
         p_new_tb_transaction_detail.user_text_30,
         p_new_tb_transaction_detail.user_number_01,
         p_new_tb_transaction_detail.user_number_02,
         p_new_tb_transaction_detail.user_number_03,
         p_new_tb_transaction_detail.user_number_04,
         p_new_tb_transaction_detail.user_number_05,
         p_new_tb_transaction_detail.user_number_06,
         p_new_tb_transaction_detail.user_number_07,
         p_new_tb_transaction_detail.user_number_08,
         p_new_tb_transaction_detail.user_number_09,
         p_new_tb_transaction_detail.user_number_10,
         p_new_tb_transaction_detail.user_date_01,
         p_new_tb_transaction_detail.user_date_02,
         p_new_tb_transaction_detail.user_date_03,
         p_new_tb_transaction_detail.user_date_04,
         p_new_tb_transaction_detail.user_date_05,
         p_new_tb_transaction_detail.user_date_06,
         p_new_tb_transaction_detail.user_date_07,
         p_new_tb_transaction_detail.user_date_08,
         p_new_tb_transaction_detail.user_date_09,
         p_new_tb_transaction_detail.user_date_10,
         p_new_tb_transaction_detail.comments,
         p_new_tb_transaction_detail.tb_calc_tax_amt,
         p_new_tb_transaction_detail.state_use_amount,
         p_new_tb_transaction_detail.state_use_tier2_amount,
         p_new_tb_transaction_detail.state_use_tier3_amount,
         p_new_tb_transaction_detail.county_use_amount,
         p_new_tb_transaction_detail.county_local_use_amount,
         p_new_tb_transaction_detail.city_use_amount,
         p_new_tb_transaction_detail.city_local_use_amount,
         p_new_tb_transaction_detail.transaction_state_code,
         p_new_tb_transaction_detail.transaction_ind,
         p_new_tb_transaction_detail.suspend_ind,
         p_new_tb_transaction_detail.taxcode_detail_id,
         p_new_tb_transaction_detail.taxcode_state_code,
         p_new_tb_transaction_detail.taxcode_type_code,
         p_new_tb_transaction_detail.taxcode_code,
         p_new_tb_transaction_detail.cch_taxcat_code,
         p_new_tb_transaction_detail.cch_group_code,
         p_new_tb_transaction_detail.cch_item_code,
         p_new_tb_transaction_detail.manual_taxcode_ind,
         p_new_tb_transaction_detail.tax_matrix_id,
         p_new_tb_transaction_detail.location_matrix_id,
         p_new_tb_transaction_detail.jurisdiction_id,
         p_new_tb_transaction_detail.jurisdiction_taxrate_id,
         p_new_tb_transaction_detail.manual_jurisdiction_ind,
         p_new_tb_transaction_detail.measure_type_code,
         p_new_tb_transaction_detail.state_use_rate,
         p_new_tb_transaction_detail.state_use_tier2_rate,
         p_new_tb_transaction_detail.state_use_tier3_rate,
         p_new_tb_transaction_detail.state_split_amount,
         p_new_tb_transaction_detail.state_tier2_min_amount,
         p_new_tb_transaction_detail.state_tier2_max_amount,
         p_new_tb_transaction_detail.state_maxtax_amount,
         p_new_tb_transaction_detail.county_use_rate,
         p_new_tb_transaction_detail.county_local_use_rate,
         p_new_tb_transaction_detail.county_split_amount,
         p_new_tb_transaction_detail.county_maxtax_amount,
         p_new_tb_transaction_detail.county_single_flag,
         p_new_tb_transaction_detail.county_default_flag,
         p_new_tb_transaction_detail.city_use_rate,
         p_new_tb_transaction_detail.city_local_use_rate,
         p_new_tb_transaction_detail.city_split_amount,
         p_new_tb_transaction_detail.city_split_use_rate,
         p_new_tb_transaction_detail.city_single_flag,
         p_new_tb_transaction_detail.city_default_flag,
         p_new_tb_transaction_detail.combined_use_rate,
         p_new_tb_transaction_detail.load_timestamp,
         p_new_tb_transaction_detail.gl_extract_updater,
         p_new_tb_transaction_detail.gl_extract_timestamp,
         p_new_tb_transaction_detail.gl_extract_flag,
         p_new_tb_transaction_detail.gl_log_flag,
         p_new_tb_transaction_detail.gl_extract_amt,
         p_new_tb_transaction_detail.audit_flag,
         p_new_tb_transaction_detail.audit_user_id,
         p_new_tb_transaction_detail.audit_timestamp,
         p_new_tb_transaction_detail.modify_user_id,
         p_new_tb_transaction_detail.modify_timestamp,
         p_new_tb_transaction_detail.update_user_id,
         p_new_tb_transaction_detail.update_timestamp );

      -- If the new TaxCode Type is "T"axable then zero Tax Amounts
      IF UPPER(p_new_tb_transaction_detail.taxcode_type_code) = 'T' OR UPPER(p_old_tb_transaction_detail.taxcode_type_code) = 'T' THEN
         p_new_tb_transaction_detail.tb_calc_tax_amt := 0;
-- future?? --         p_new_tb_transaction_detail.taxable_amt := 0;
         p_new_tb_transaction_detail.state_use_amount := 0;
         p_new_tb_transaction_detail.state_use_tier2_amount := 0;
         p_new_tb_transaction_detail.state_use_tier3_amount := 0;
         p_new_tb_transaction_detail.county_use_amount := 0;
         p_new_tb_transaction_detail.county_local_use_amount := 0;
         p_new_tb_transaction_detail.city_use_amount := 0;
         p_new_tb_transaction_detail.city_local_use_amount := 0;
         p_new_tb_transaction_detail.state_use_rate := 0;
         p_new_tb_transaction_detail.state_use_tier2_rate := 0;
         p_new_tb_transaction_detail.state_use_tier3_rate := 0;
         p_new_tb_transaction_detail.state_split_amount := 0;
         p_new_tb_transaction_detail.state_tier2_min_amount := 0;
         p_new_tb_transaction_detail.state_tier2_max_amount := 0;
         p_new_tb_transaction_detail.state_maxtax_amount := 0;
         p_new_tb_transaction_detail.county_use_rate := 0;
         p_new_tb_transaction_detail.county_local_use_rate := 0;
         p_new_tb_transaction_detail.county_split_amount := 0;
         p_new_tb_transaction_detail.county_maxtax_amount := 0;
         p_new_tb_transaction_detail.county_single_flag := '0';
         p_new_tb_transaction_detail.county_default_flag := '0';
         p_new_tb_transaction_detail.city_use_rate := 0;
         p_new_tb_transaction_detail.city_local_use_rate := 0;
         p_new_tb_transaction_detail.city_split_amount := 0;
         p_new_tb_transaction_detail.city_split_use_rate := 0;
         p_new_tb_transaction_detail.city_single_flag := '0';
         p_new_tb_transaction_detail.city_default_flag := '0';
         p_new_tb_transaction_detail.combined_use_rate := 0;
         -- Replace original distribution amount if populated -- 3351
         IF p_new_tb_transaction_detail.gl_extract_amt IS NOT NULL AND p_new_tb_transaction_detail.gl_extract_amt <> 0 THEN
            p_new_tb_transaction_detail.gl_line_itm_dist_amt := p_new_tb_transaction_detail.gl_extract_amt;
         END IF;
         p_new_tb_transaction_detail.gl_extract_amt := 0;
      END IF;

      -- If the new TaxCode Detail ID is null then search tax matrix
      IF p_new_tb_transaction_detail.taxcode_detail_id IS NULL THEN
         v_transaction_detail_id := p_new_tb_transaction_detail.transaction_detail_id;
         v_transaction_state_code := p_new_tb_transaction_detail.transaction_state_code;
         -- ***** Create Dynamic WHERE Clause for Matrix SQLs ***** --
         -- Does Transaction Record have a valid State?
         IF v_transaction_state_code IS NULL THEN
            vn_fetch_rows := 0;
         ELSE
            SELECT COUNT(*)
              INTO vn_fetch_rows
              FROM TB_TAXCODE_STATE
             WHERE taxcode_state_code = v_transaction_state_code;
         END IF;
         IF vn_fetch_rows = 0 THEN
            -- Get State from new Jurisdiction
            v_transaction_state_code := NULL;
            BEGIN
               SELECT state
                 INTO v_transaction_state_code
                 FROM TB_JURISDICTION
                WHERE jurisdiction_id = p_new_tb_transaction_detail.jurisdiction_id;
               IF SQLCODE != 0 THEN
                  RAISE e_badread;
               END IF;
            EXCEPTION
               WHEN e_badread THEN
                  v_transaction_state_code := NULL;
               WHEN NO_DATA_FOUND THEN
                  v_transaction_state_code := NULL;
            END;
            -- Jurisdiction Line Found
            IF v_transaction_state_code IS NOT NULL THEN
               IF p_new_tb_transaction_detail.transaction_state_code IS NULL THEN
                  p_new_tb_transaction_detail.auto_transaction_state_code := '*NULL';
               ELSE
                  p_new_tb_transaction_detail.auto_transaction_state_code := p_new_tb_transaction_detail.transaction_state_code;
               END IF;
               p_new_tb_transaction_detail.transaction_state_code := v_transaction_state_code;
            ELSE
               p_new_tb_transaction_detail.transaction_ind := 'S';
               p_new_tb_transaction_detail.suspend_ind := 'L';
            END IF;
         END IF;

         -- Continue if not Suspended
         IF p_new_tb_transaction_detail.transaction_ind IS NULL OR p_new_tb_transaction_detail.transaction_ind <> 'S' THEN

   -- *******************************************************************************************************************************************
   -- NEW BATCH PROCESS CODE --------------------------------------------------------------------------------------------------------------------
   -- ***** Create Dynamic WHERE Clause for Transaction Detail SQL ***** --
   -- Tax Matrix Drivers
   /*OPEN tax_driver_cursor;
   LOOP
      FETCH tax_driver_cursor INTO tax_driver;
      EXIT WHEN tax_driver_cursor%NOTFOUND;
      IF tax_driver.range_flag = '1' THEN
         IF tax_driver.to_number_flag = '1' THEN
            IF tax_driver.null_driver_flag = '1' THEN
               -- Range and ToNumber and NULL
               IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                  UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                  vc_tax_matrix_where := vc_tax_matrix_where || 'AND ( ' ||
                  '( tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU = ''*NA'' AND ' ||
                     '( tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NULL AND ( tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*NULL'' )) OR ' ||
                     '( tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND ( tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR DECODE(isnumber(tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || '),1,to_char(tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ',''00000000000000000000.00000000000000000000''),UPPER(tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ')) = DECODE(isnumber(tb_tax_matrix.' || tax_driver.matrix_column_name || '),1,to_char(tb_tax_matrix.' || tax_driver.matrix_column_name || ',''00000000000000000000.00000000000000000000''),UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') ))) OR ' ||
                  '( tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU <> ''*NA'' AND ' ||
                     '( tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND (DECODE(isnumber(tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || '),1,to_char(tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ',''00000000000000000000.00000000000000000000''),UPPER(tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ')) >= DECODE(isnumber(tb_tax_matrix.' || tax_driver.matrix_column_name || '),1,to_char(tb_tax_matrix.' || tax_driver.matrix_column_name || ',''00000000000000000000.00000000000000000000''),UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') ) AND ' ||
                                                                                            'DECODE(isnumber(tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || '),1,to_char(tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ',''00000000000000000000.00000000000000000000''),UPPER(tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ')) <= DECODE(isnumber(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU ),1,to_char(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU,''00000000000000000000.00000000000000000000''),UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU)) )))) ';
               END IF;
            ELSE
               -- Range and ToNumber and NOT NULL
               IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                  UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                  vc_tax_matrix_where := vc_tax_matrix_where || 'AND ( ' ||
                  '( tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU = ''*NA'' AND (tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR DECODE(isnumber(tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || '),1,to_char(tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ',''00000000000000000000.00000000000000000000''),UPPER(tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ')) = DECODE(isnumber(tb_tax_matrix.' || tax_driver.matrix_column_name || '),1,to_char(tb_tax_matrix.' || tax_driver.matrix_column_name || ',''00000000000000000000.00000000000000000000''),UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ')))) OR ' ||
                  '( tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU <> ''*NA'' AND (DECODE(isnumber(tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || '),1,to_char(tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ',''00000000000000000000.00000000000000000000''),UPPER(tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ')) >= DECODE(isnumber(tb_tax_matrix.' || tax_driver.matrix_column_name || '),1,to_char(tb_tax_matrix.' || tax_driver.matrix_column_name || ',''00000000000000000000.00000000000000000000''),UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ')) AND ' ||
                                                                                        'DECODE(isnumber(tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || '),1,to_char(tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ',''00000000000000000000.00000000000000000000''),UPPER(tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ')) <= DECODE(isnumber(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU ),1,to_char(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU,''00000000000000000000.00000000000000000000''),UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU)) )) ';
               END IF;
            END IF;
         ELSE
            IF tax_driver.null_driver_flag = '1' THEN
               IF tax_driver.wildcard_flag = '1' THEN
                  -- Range and NOT ToNumber and NULL and Wildcard
                  IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                     UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                     vc_tax_matrix_where := vc_tax_matrix_where || 'AND ( ' ||
                     '( tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU = ''*NA'' AND ' ||
                        '( tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NULL AND (tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*NULL'' )) OR ' ||
                        '( tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND (tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR UPPER(tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ') LIKE UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ')))) OR ' ||
                     '( tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU <> ''*NA'' AND (tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND (UPPER(tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ') >= UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') AND UPPER(tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ') <= UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU) ))) ';
                  END IF;
               ELSE
                  -- Range and NOT ToNumber and NULL and NOT Wildcard
                  IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                     UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                     vc_tax_matrix_where := vc_tax_matrix_where || 'AND ( ' ||
                     '( tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU = ''*NA'' AND ' ||
                        '( tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NULL AND (tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*NULL'' )) OR ' ||
                        '( tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND (tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR UPPER(tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ') = UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ')))) OR ' ||
                     '( tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU <> ''*NA'' AND (tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND (UPPER(tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ') >= UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') AND UPPER(tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ') <= UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU) ))) ';
                  END IF;
               END IF;
            ELSE
               IF tax_driver.wildcard_flag = '1' THEN
                  -- Range and NOT ToNumber and NOT NULL and Wildcard
                  IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                     UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                     vc_tax_matrix_where := vc_tax_matrix_where || 'AND ( ' ||
                     '( tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU = ''*NA'' AND ' ||
                        '(tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR UPPER(tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ') LIKE UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '))) OR ' ||
                     '( tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU <> ''*NA'' AND (UPPER(tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ') >= UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') AND UPPER(tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ') <= UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU) )) ';
                  END IF;
               ELSE
                  -- Range and NOT ToNumber and NOT NULL and NOT Wildcard
                  IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                     UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                     vc_tax_matrix_where := vc_tax_matrix_where || 'AND ( ' ||
                     '( tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU = ''*NA'' AND ' ||
                        '(tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR UPPER(tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ') = UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '))) OR ' ||
                     '( tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU <> ''*NA'' AND (UPPER(tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ') >= UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') AND UPPER(tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ') <= UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU) )) ';
                  END IF;
               END IF;
            END IF;
         END IF;
      ELSE
         IF tax_driver.null_driver_flag = '1' THEN
            IF tax_driver.wildcard_flag = '1' THEN
               -- NOT Range and NULL and Wildcard
               IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                  UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                     vc_tax_matrix_where := vc_tax_matrix_where || 'AND ( ' ||
                     '( tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NULL AND (tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*NULL'' )) OR ' ||
                     '( tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND (tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR UPPER(tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ') LIKE UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ')) ) ';
               END IF;
            ELSE
               -- NOT Range and NULL and NOT Wildcard
               IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                  UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                     vc_tax_matrix_where := vc_tax_matrix_where || 'AND ( ' ||
                     '( tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NULL AND (tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*NULL'' )) OR ' ||
                     '( tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND (tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR UPPER(tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ') = UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ')) ) ';
               END IF;
            END IF;
         ELSE
            IF tax_driver.wildcard_flag = '1' THEN
               -- NOT Range and NOT NULL and Wildcard
               IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                  UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                     vc_tax_matrix_where := vc_tax_matrix_where || 'AND ( ' ||
                     '( tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR UPPER(tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ') LIKE UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') ) ';
               END IF;
            ELSE
               -- NOT Range and NOT NULL and NOT Wildcard
               IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                  UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                  IF tax_driver.trans_dtl_column_name = 'TRANSACTION_STATE_CODE' THEN
                     IF vc_state_driver_flag <> '1' THEN
                        vc_state_driver_flag := '1';
                     END IF;
                     vc_tax_matrix_where := vc_tax_matrix_where || 'AND ( ' ||
                        '( tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR UPPER(:v_transaction_state_code) = UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ')) ';
                  ELSE
                     vc_tax_matrix_where := vc_tax_matrix_where || 'AND ( ' ||
                        '( tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR UPPER(tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ') = UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') ) ';
                  END IF;
               END IF;
            END IF;
         END IF;
      END IF;
      vc_tax_matrix_where := vc_tax_matrix_where || ') ';
   END LOOP;
   CLOSE tax_driver_cursor;*/

   -- New call to sp_gen_tax_driver for MidTier Cleanup
   Sp_Gen_Tax_Driver (
	p_generate_driver_reference	=> 'N',
	p_an_batch_id			=> NULL,
	p_transaction_table_name	=> 'tb_tmp_transaction_detail',
	p_tax_table_name		=> 'tb_tax_matrix',
	p_vc_state_driver_flag		=> vc_state_driver_flag,
	p_vc_tax_matrix_where		=> vc_tax_matrix_where);

   -- NEW BATCH PROCESS CODE --------------------------------------------------------------------------------------------------------------------
   -- *******************************************************************************************************************************************

            -- If no drivers found raise error, else create location matrix sql statement
            IF vc_tax_matrix_where IS NULL THEN
               NULL;
            ELSE
               vc_tax_matrix_stmt := vc_tax_matrix_select || vc_tax_matrix_where || vc_tax_matrix_orderby;
            END IF;

            -- Search Tax Matrix for matches
            BEGIN
               IF vc_state_driver_flag = '1' THEN
                   OPEN tax_matrix_cursor
                    FOR vc_tax_matrix_stmt
                  USING v_transaction_detail_id, v_transaction_state_code, v_transaction_state_code;
                  FETCH tax_matrix_cursor
                   INTO tax_matrix;
               ELSE
                   OPEN tax_matrix_cursor
                    FOR vc_tax_matrix_stmt
                  USING v_transaction_detail_id, v_transaction_state_code;
                  FETCH tax_matrix_cursor
                   INTO tax_matrix;
               END IF;
               -- Rows found?
               IF tax_matrix_cursor%FOUND THEN
                  vn_fetch_rows := tax_matrix_cursor%ROWCOUNT;
               ELSE
                  vn_fetch_rows := 0;
               END IF;
               CLOSE tax_matrix_cursor;
               IF SQLCODE != 0 THEN
                  RAISE e_badread;
               END IF;
            EXCEPTION
               WHEN e_badread THEN
                  vn_fetch_rows := 0;
            END;

            -- Tax Matrix Line Found
            IF vn_fetch_rows > 0 THEN
               p_new_tb_transaction_detail.tax_matrix_id := tax_matrix.tax_matrix_id;
               -- Determine "Then" or "Else" Results
               IF tax_matrix.relation_sign = 'na' THEN
                  tax_matrix.relation_amount := 0;
                  v_less_hold_code_flag := tax_matrix.then_hold_code_flag;
                  v_less_taxcode_detail_id := tax_matrix.then_taxcode_detail_id;
                  v_less_taxcode_state_code := tax_matrix.then_taxcode_state_code;
                  v_less_taxcode_type_code := tax_matrix.then_taxcode_type_code;
                  v_less_taxcode_code := tax_matrix.then_taxcode_code;
                  v_less_jurisdiction_id := tax_matrix.then_jurisdiction_id;
                  v_less_measure_type_code := tax_matrix.then_measure_type_code;
                  v_less_taxtype_code := tax_matrix.then_taxtype_code;
                  v_less_cch_taxcat_code := tax_matrix.then_cch_taxcat_code;
                  v_less_cch_group_code := tax_matrix.then_cch_group_code;
                  -- v_less_cch_item_code := tax_matrix.then_cch_item_code;
                  v_equal_hold_code_flag := tax_matrix.then_hold_code_flag;
                  v_equal_taxcode_detail_id := tax_matrix.then_taxcode_detail_id;
                  v_equal_taxcode_state_code := tax_matrix.then_taxcode_state_code;
                  v_equal_taxcode_type_code := tax_matrix.then_taxcode_type_code;
                  v_equal_taxcode_code := tax_matrix.then_taxcode_code;
                  v_equal_jurisdiction_id := tax_matrix.then_jurisdiction_id;
                  v_equal_measure_type_code := tax_matrix.then_measure_type_code;
                  v_equal_taxtype_code := tax_matrix.then_taxtype_code;
                  v_equal_cch_taxcat_code := tax_matrix.then_cch_taxcat_code;
                  v_equal_cch_group_code := tax_matrix.then_cch_group_code;
                  -- v_equal_cch_item_code := tax_matrix.then_cch_item_code;
                  v_greater_hold_code_flag := tax_matrix.then_hold_code_flag;
                  v_greater_taxcode_detail_id := tax_matrix.then_taxcode_detail_id;
                  v_greater_taxcode_state_code := tax_matrix.then_taxcode_state_code;
                  v_greater_taxcode_type_code := tax_matrix.then_taxcode_type_code;
                  v_greater_taxcode_code := tax_matrix.then_taxcode_code;
                  v_greater_jurisdiction_id := tax_matrix.then_jurisdiction_id;
                  v_greater_measure_type_code := tax_matrix.then_measure_type_code;
                  v_greater_taxtype_code := tax_matrix.then_taxtype_code;
                  v_greater_cch_taxcat_code := tax_matrix.then_cch_taxcat_code;
                  v_greater_cch_group_code := tax_matrix.then_cch_group_code;
                  -- v_greater_cch_item_code := tax_matrix.then_cch_item_code;
               ELSIF tax_matrix.relation_sign = '=' THEN
                  v_less_hold_code_flag := tax_matrix.else_hold_code_flag;
                  v_less_taxcode_detail_id := tax_matrix.else_taxcode_detail_id;
                  v_less_taxcode_state_code := tax_matrix.else_taxcode_state_code;
                  v_less_taxcode_type_code := tax_matrix.else_taxcode_type_code;
                  v_less_taxcode_code := tax_matrix.else_taxcode_code;
                  v_less_jurisdiction_id := tax_matrix.else_jurisdiction_id;
                  v_less_measure_type_code := tax_matrix.else_measure_type_code;
                  v_less_taxtype_code := tax_matrix.else_taxtype_code;
                  v_less_cch_taxcat_code := tax_matrix.else_cch_taxcat_code;
                  v_less_cch_group_code := tax_matrix.else_cch_group_code;
                  -- v_less_cch_item_code := tax_matrix.else_cch_item_code;
                  v_equal_hold_code_flag := tax_matrix.then_hold_code_flag;
                  v_equal_taxcode_detail_id := tax_matrix.then_taxcode_detail_id;
                  v_equal_taxcode_state_code := tax_matrix.then_taxcode_state_code;
                  v_equal_taxcode_type_code := tax_matrix.then_taxcode_type_code;
                  v_equal_taxcode_code := tax_matrix.then_taxcode_code;
                  v_equal_jurisdiction_id := tax_matrix.then_jurisdiction_id;
                  v_equal_measure_type_code := tax_matrix.then_measure_type_code;
                  v_equal_taxtype_code := tax_matrix.then_taxtype_code;
                  v_equal_cch_taxcat_code := tax_matrix.then_cch_taxcat_code;
                  v_equal_cch_group_code := tax_matrix.then_cch_group_code;
                  -- v_equal_cch_item_code := tax_matrix.then_cch_item_code;
                  v_greater_hold_code_flag := tax_matrix.else_hold_code_flag;
                  v_greater_taxcode_detail_id := tax_matrix.else_taxcode_detail_id;
                  v_greater_taxcode_state_code := tax_matrix.else_taxcode_state_code;
                  v_greater_taxcode_type_code := tax_matrix.else_taxcode_type_code;
                  v_greater_taxcode_code := tax_matrix.else_taxcode_code;
                  v_greater_jurisdiction_id := tax_matrix.else_jurisdiction_id;
                  v_greater_measure_type_code := tax_matrix.else_measure_type_code;
                  v_greater_taxtype_code := tax_matrix.else_taxtype_code;
                  v_greater_cch_taxcat_code := tax_matrix.else_cch_taxcat_code;
                  v_greater_cch_group_code := tax_matrix.else_cch_group_code;
                  -- v_greater_cch_item_code := tax_matrix.else_cch_item_code;
               ELSIF tax_matrix.relation_sign = '<' THEN
                  v_less_hold_code_flag := tax_matrix.then_hold_code_flag;
                  v_less_taxcode_detail_id := tax_matrix.then_taxcode_detail_id;
                  v_less_taxcode_state_code := tax_matrix.then_taxcode_state_code;
                  v_less_taxcode_type_code := tax_matrix.then_taxcode_type_code;
                  v_less_taxcode_code := tax_matrix.then_taxcode_code;
                  v_less_jurisdiction_id := tax_matrix.then_jurisdiction_id;
                  v_less_measure_type_code := tax_matrix.then_measure_type_code;
                  v_less_taxtype_code := tax_matrix.then_taxtype_code;
                  v_less_cch_taxcat_code := tax_matrix.then_cch_taxcat_code;
                  v_less_cch_group_code := tax_matrix.then_cch_group_code;
                  -- v_less_cch_item_code := tax_matrix.then_cch_item_code;
                  v_equal_hold_code_flag := tax_matrix.else_hold_code_flag;
                  v_equal_taxcode_detail_id := tax_matrix.else_taxcode_detail_id;
                  v_equal_taxcode_state_code := tax_matrix.else_taxcode_state_code;
                  v_equal_taxcode_type_code := tax_matrix.else_taxcode_type_code;
                  v_equal_taxcode_code := tax_matrix.else_taxcode_code;
                  v_equal_jurisdiction_id := tax_matrix.else_jurisdiction_id;
                  v_equal_measure_type_code := tax_matrix.else_measure_type_code;
                  v_equal_taxtype_code := tax_matrix.else_taxtype_code;
                  v_equal_cch_taxcat_code := tax_matrix.else_cch_taxcat_code;
                  v_equal_cch_group_code := tax_matrix.else_cch_group_code;
                  -- v_equal_cch_item_code := tax_matrix.else_cch_item_code;
                  v_greater_hold_code_flag := tax_matrix.else_hold_code_flag;
                  v_greater_taxcode_detail_id := tax_matrix.else_taxcode_detail_id;
                  v_greater_taxcode_state_code := tax_matrix.else_taxcode_state_code;
                  v_greater_taxcode_type_code := tax_matrix.else_taxcode_type_code;
                  v_greater_taxcode_code := tax_matrix.else_taxcode_code;
                  v_greater_jurisdiction_id := tax_matrix.else_jurisdiction_id;
                  v_greater_measure_type_code := tax_matrix.else_measure_type_code;
                  v_greater_taxtype_code := tax_matrix.else_taxtype_code;
                  v_greater_cch_taxcat_code := tax_matrix.else_cch_taxcat_code;
                  v_greater_cch_group_code := tax_matrix.else_cch_group_code;
                  -- v_greater_cch_item_code := tax_matrix.else_cch_item_code;
               ELSIF tax_matrix.relation_sign = '<=' THEN
                  v_less_hold_code_flag := tax_matrix.then_hold_code_flag;
                  v_less_taxcode_detail_id := tax_matrix.then_taxcode_detail_id;
                  v_less_taxcode_state_code := tax_matrix.then_taxcode_state_code;
                  v_less_taxcode_type_code := tax_matrix.then_taxcode_type_code;
                  v_less_taxcode_code := tax_matrix.then_taxcode_code;
                  v_less_jurisdiction_id := tax_matrix.then_jurisdiction_id;
                  v_less_measure_type_code := tax_matrix.then_measure_type_code;
                  v_less_taxtype_code := tax_matrix.then_taxtype_code;
                  v_less_cch_taxcat_code := tax_matrix.then_cch_taxcat_code;
                  v_less_cch_group_code := tax_matrix.then_cch_group_code;
                  -- v_less_cch_item_code := tax_matrix.then_cch_item_code;
                  v_equal_hold_code_flag := tax_matrix.then_hold_code_flag;
                  v_equal_taxcode_detail_id := tax_matrix.then_taxcode_detail_id;
                  v_equal_taxcode_state_code := tax_matrix.then_taxcode_state_code;
                  v_equal_taxcode_type_code := tax_matrix.then_taxcode_type_code;
                  v_equal_taxcode_code := tax_matrix.then_taxcode_code;
                  v_equal_jurisdiction_id := tax_matrix.then_jurisdiction_id;
                  v_equal_measure_type_code := tax_matrix.then_measure_type_code;
                  v_equal_taxtype_code := tax_matrix.then_taxtype_code;
                  v_equal_cch_taxcat_code := tax_matrix.then_cch_taxcat_code;
                  v_equal_cch_group_code := tax_matrix.then_cch_group_code;
                  -- v_equal_cch_item_code := tax_matrix.then_cch_item_code;
                  v_greater_hold_code_flag := tax_matrix.else_hold_code_flag;
                  v_greater_taxcode_detail_id := tax_matrix.else_taxcode_detail_id;
                  v_greater_taxcode_state_code := tax_matrix.else_taxcode_state_code;
                  v_greater_taxcode_type_code := tax_matrix.else_taxcode_type_code;
                  v_greater_taxcode_code := tax_matrix.else_taxcode_code;
                  v_greater_jurisdiction_id := tax_matrix.else_jurisdiction_id;
                  v_greater_measure_type_code := tax_matrix.else_measure_type_code;
                  v_greater_taxtype_code := tax_matrix.else_taxtype_code;
                  v_greater_cch_taxcat_code := tax_matrix.else_cch_taxcat_code;
                  v_greater_cch_group_code := tax_matrix.else_cch_group_code;
                  -- v_greater_cch_item_code := tax_matrix.else_cch_item_code;
               ELSIF tax_matrix.relation_sign = '>' THEN
                  v_less_hold_code_flag := tax_matrix.else_hold_code_flag;
                  v_less_taxcode_detail_id := tax_matrix.else_taxcode_detail_id;
                  v_less_taxcode_state_code := tax_matrix.else_taxcode_state_code;
                  v_less_taxcode_type_code := tax_matrix.else_taxcode_type_code;
                  v_less_taxcode_code := tax_matrix.else_taxcode_code;
                  v_less_jurisdiction_id := tax_matrix.else_jurisdiction_id;
                  v_less_measure_type_code := tax_matrix.else_measure_type_code;
                  v_less_taxtype_code := tax_matrix.else_taxtype_code;
                  v_less_cch_taxcat_code := tax_matrix.else_cch_taxcat_code;
                  v_less_cch_group_code := tax_matrix.else_cch_group_code;
                  -- v_less_cch_item_code := tax_matrix.else_cch_item_code;
                  v_equal_hold_code_flag := tax_matrix.else_hold_code_flag;
                  v_equal_taxcode_detail_id := tax_matrix.else_taxcode_detail_id;
                  v_equal_taxcode_state_code := tax_matrix.else_taxcode_state_code;
                  v_equal_taxcode_type_code := tax_matrix.else_taxcode_type_code;
                  v_equal_taxcode_code := tax_matrix.else_taxcode_code;
                  v_equal_jurisdiction_id := tax_matrix.else_jurisdiction_id;
                  v_equal_measure_type_code := tax_matrix.else_measure_type_code;
                  v_equal_taxtype_code := tax_matrix.else_taxtype_code;
                  v_equal_cch_taxcat_code := tax_matrix.else_cch_taxcat_code;
                  v_equal_cch_group_code := tax_matrix.else_cch_group_code;
                  -- v_equal_cch_item_code := tax_matrix.else_cch_item_code;
                  v_greater_hold_code_flag := tax_matrix.then_hold_code_flag;
                  v_greater_taxcode_detail_id := tax_matrix.then_taxcode_detail_id;
                  v_greater_taxcode_state_code := tax_matrix.then_taxcode_state_code;
                  v_greater_taxcode_type_code := tax_matrix.then_taxcode_type_code;
                  v_greater_taxcode_code := tax_matrix.then_taxcode_code;
                  v_greater_jurisdiction_id := tax_matrix.then_jurisdiction_id;
                  v_greater_measure_type_code := tax_matrix.then_measure_type_code;
                  v_greater_taxtype_code := tax_matrix.then_taxtype_code;
                  v_greater_cch_taxcat_code := tax_matrix.then_cch_taxcat_code;
                  v_greater_cch_group_code := tax_matrix.then_cch_group_code;
                  -- v_greater_cch_item_code := tax_matrix.then_cch_item_code;
               ELSIF tax_matrix.relation_sign = '>=' THEN
                  v_less_hold_code_flag := tax_matrix.else_hold_code_flag;
                  v_less_taxcode_detail_id := tax_matrix.else_taxcode_detail_id;
                  v_less_taxcode_state_code := tax_matrix.else_taxcode_state_code;
                  v_less_taxcode_type_code := tax_matrix.else_taxcode_type_code;
                  v_less_taxcode_code := tax_matrix.else_taxcode_code;
                  v_less_jurisdiction_id := tax_matrix.else_jurisdiction_id;
                  v_less_measure_type_code := tax_matrix.else_measure_type_code;
                  v_less_taxtype_code := tax_matrix.else_taxtype_code;
                  v_less_cch_taxcat_code := tax_matrix.else_cch_taxcat_code;
                  v_less_cch_group_code := tax_matrix.else_cch_group_code;
                  -- v_less_cch_item_code := tax_matrix.else_cch_item_code;
                  v_equal_hold_code_flag := tax_matrix.then_hold_code_flag;
                  v_equal_taxcode_detail_id := tax_matrix.then_taxcode_detail_id;
                  v_equal_taxcode_state_code := tax_matrix.then_taxcode_state_code;
                  v_equal_taxcode_type_code := tax_matrix.then_taxcode_type_code;
                  v_equal_taxcode_code := tax_matrix.then_taxcode_code;
                  v_equal_jurisdiction_id := tax_matrix.then_jurisdiction_id;
                  v_equal_measure_type_code := tax_matrix.then_measure_type_code;
                  v_equal_taxtype_code := tax_matrix.then_taxtype_code;
                  v_equal_cch_taxcat_code := tax_matrix.then_cch_taxcat_code;
                  v_equal_cch_group_code := tax_matrix.then_cch_group_code;
                  -- v_equal_cch_item_code := tax_matrix.then_cch_item_code;
                  v_greater_hold_code_flag := tax_matrix.then_hold_code_flag;
                  v_greater_taxcode_detail_id := tax_matrix.then_taxcode_detail_id;
                  v_greater_taxcode_state_code := tax_matrix.then_taxcode_state_code;
                  v_greater_taxcode_type_code := tax_matrix.then_taxcode_type_code;
                  v_greater_taxcode_code := tax_matrix.then_taxcode_code;
                  v_greater_jurisdiction_id := tax_matrix.then_jurisdiction_id;
                  v_greater_measure_type_code := tax_matrix.then_measure_type_code;
                  v_greater_taxtype_code := tax_matrix.then_taxtype_code;
                  v_greater_cch_taxcat_code := tax_matrix.then_cch_taxcat_code;
                  v_greater_cch_group_code := tax_matrix.then_cch_group_code;
                  -- v_greater_cch_item_code := tax_matrix.then_cch_item_code;
               ELSIF tax_matrix.relation_sign = '<>' THEN
                  v_less_hold_code_flag := tax_matrix.then_hold_code_flag;
                  v_less_taxcode_detail_id := tax_matrix.then_taxcode_detail_id;
                  v_less_taxcode_state_code := tax_matrix.then_taxcode_state_code;
                  v_less_taxcode_type_code := tax_matrix.then_taxcode_type_code;
                  v_less_taxcode_code := tax_matrix.then_taxcode_code;
                  v_less_jurisdiction_id := tax_matrix.then_jurisdiction_id;
                  v_less_measure_type_code := tax_matrix.then_measure_type_code;
                  v_less_taxtype_code := tax_matrix.then_taxtype_code;
                  v_less_cch_taxcat_code := tax_matrix.then_cch_taxcat_code;
                  v_less_cch_group_code := tax_matrix.then_cch_group_code;
                  -- v_less_cch_item_code := tax_matrix.then_cch_item_code;
                  v_equal_hold_code_flag := tax_matrix.else_hold_code_flag;
                  v_equal_taxcode_detail_id := tax_matrix.else_taxcode_detail_id;
                  v_equal_taxcode_state_code := tax_matrix.else_taxcode_state_code;
                  v_equal_taxcode_type_code := tax_matrix.else_taxcode_type_code;
                  v_equal_taxcode_code := tax_matrix.else_taxcode_code;
                  v_equal_jurisdiction_id := tax_matrix.else_jurisdiction_id;
                  v_equal_measure_type_code := tax_matrix.else_measure_type_code;
                  v_equal_taxtype_code := tax_matrix.else_taxtype_code;
                  v_equal_cch_taxcat_code := tax_matrix.else_cch_taxcat_code;
                  v_equal_cch_group_code := tax_matrix.else_cch_group_code;
                  -- v_equal_cch_item_code := tax_matrix.else_cch_item_code;
                  v_greater_hold_code_flag := tax_matrix.then_hold_code_flag;
                  v_greater_taxcode_detail_id := tax_matrix.then_taxcode_detail_id;
                  v_greater_taxcode_state_code := tax_matrix.then_taxcode_state_code;
                  v_greater_taxcode_type_code := tax_matrix.then_taxcode_type_code;
                  v_greater_taxcode_code := tax_matrix.then_taxcode_code;
                  v_greater_jurisdiction_id := tax_matrix.then_jurisdiction_id;
                  v_greater_measure_type_code := tax_matrix.then_measure_type_code;
                  v_greater_taxtype_code := tax_matrix.then_taxtype_code;
                  v_greater_cch_taxcat_code := tax_matrix.then_cch_taxcat_code;
                  v_greater_cch_group_code := tax_matrix.then_cch_group_code;
                  -- v_greater_cch_item_code := tax_matrix.then_cch_item_code;
               END IF;

               SELECT DECODE(SIGN(p_new_tb_transaction_detail.gl_line_itm_dist_amt - tax_matrix.relation_amount), -1, v_less_hold_code_flag, 0, v_equal_hold_code_flag, 1, v_greater_hold_code_flag),
                      DECODE(SIGN(p_new_tb_transaction_detail.gl_line_itm_dist_amt - tax_matrix.relation_amount), -1, v_less_taxcode_detail_id, 0, v_equal_taxcode_detail_id, 1, v_greater_taxcode_detail_id),
                      DECODE(SIGN(p_new_tb_transaction_detail.gl_line_itm_dist_amt - tax_matrix.relation_amount), -1, v_less_taxcode_state_code, 0, v_equal_taxcode_state_code, 1, v_greater_taxcode_state_code),
                      DECODE(SIGN(p_new_tb_transaction_detail.gl_line_itm_dist_amt - tax_matrix.relation_amount), -1, v_less_taxcode_type_code, 0, v_equal_taxcode_type_code, 1, v_greater_taxcode_type_code),
                      DECODE(SIGN(p_new_tb_transaction_detail.gl_line_itm_dist_amt - tax_matrix.relation_amount), -1, v_less_taxcode_code, 0, v_equal_taxcode_code, 1, v_greater_taxcode_code),
                      DECODE(SIGN(p_new_tb_transaction_detail.gl_line_itm_dist_amt - tax_matrix.relation_amount), -1, v_less_jurisdiction_id, 0, v_equal_jurisdiction_id, 1, v_greater_jurisdiction_id),
                      DECODE(SIGN(p_new_tb_transaction_detail.gl_line_itm_dist_amt - tax_matrix.relation_amount), -1, v_less_measure_type_code, 0, v_equal_measure_type_code, 1, v_greater_measure_type_code),
                      DECODE(SIGN(p_new_tb_transaction_detail.gl_line_itm_dist_amt - tax_matrix.relation_amount), -1, v_less_taxtype_code, 0, v_equal_taxtype_code, 1, v_greater_taxtype_code),
                      DECODE(SIGN(p_new_tb_transaction_detail.gl_line_itm_dist_amt - tax_matrix.relation_amount), -1, v_less_cch_taxcat_code, 0, v_equal_cch_taxcat_code, 1, v_greater_cch_taxcat_code),
                      DECODE(SIGN(p_new_tb_transaction_detail.gl_line_itm_dist_amt - tax_matrix.relation_amount), -1, v_less_cch_group_code, 0, v_equal_cch_group_code, 1, v_greater_cch_group_code)
                 INTO v_hold_code_flag, p_new_tb_transaction_detail.taxcode_detail_id, p_new_tb_transaction_detail.taxcode_state_code, p_new_tb_transaction_detail.taxcode_type_code, p_new_tb_transaction_detail.taxcode_code, v_override_jurisdiction_id, p_new_tb_transaction_detail.measure_type_code, v_taxtype_code, p_new_tb_transaction_detail.cch_taxcat_code, p_new_tb_transaction_detail.cch_group_code
                 FROM DUAL;

               IF v_override_jurisdiction_id IS NOT NULL AND v_override_jurisdiction_id <> 0 THEN
                  p_new_tb_transaction_detail.jurisdiction_id := v_override_jurisdiction_id;
                  p_new_tb_transaction_detail.manual_jurisdiction_ind := 'AO';
               END IF;
            ELSE
               -- Tax Matrix Line NOT Found
               p_new_tb_transaction_detail.transaction_ind := 'S';
               p_new_tb_transaction_detail.suspend_ind := 'T';
            END IF;
         END IF;
      ELSE
         -- Get Tax Type Code
         BEGIN
           SELECT taxtype_code
             INTO v_taxtype_code
             FROM tb_taxcode
            WHERE taxcode_code = p_new_tb_transaction_detail.taxcode_code
              AND taxcode_type_code = p_new_tb_transaction_detail.taxcode_type_code;
          EXCEPTION
            WHEN OTHERS THEN
              v_taxtype_code := 'U';
         END;
      END IF;

      -- Continue if not Suspended
      IF p_new_tb_transaction_detail.transaction_ind IS NULL OR p_new_tb_transaction_detail.transaction_ind <> 'S' THEN
         -- ****** DETERMINE TYPE of TAXCODE ****** --
         -- If the new TaxCode Type is "T"axable then recalc Tax Amounts
         IF UPPER(p_new_tb_transaction_detail.taxcode_type_code) = 'T' THEN
            -- If Override jurisdiction id is not blank or 0 then use it instead of location matrix
            IF p_new_tb_transaction_detail.jurisdiction_id IS NOT NULL AND p_new_tb_transaction_detail.jurisdiction_id <> 0 THEN
               -- Only if there is a Location Matrix ID -- 3351 -- mbf01
               IF p_new_tb_transaction_detail.location_matrix_id IS NOT NULL AND p_new_tb_transaction_detail.location_matrix_id <> 0 THEN
                  -- Get Override Tax Type Code -- 3351
                  -- Get 5 Tax Calc Flags -- 3411 -- MBF02
                  BEGIN
                     SELECT override_taxtype_code,
                            state_flag, county_flag, county_local_flag, city_flag, city_local_flag
                       INTO location_matrix.override_taxtype_code,
                            location_matrix.state_flag,
                            location_matrix.county_flag,
                            location_matrix.county_local_flag,
                            location_matrix.city_flag,
                            location_matrix.city_local_flag
                       FROM TB_LOCATION_MATRIX
                      WHERE TB_LOCATION_MATRIX.location_matrix_id = p_new_tb_transaction_detail.location_matrix_id;
                  EXCEPTION
                     WHEN NO_DATA_FOUND THEN
                        location_matrix.override_taxtype_code := '*NO';
                        location_matrix.state_flag := '1';
                        location_matrix.county_flag := '1';
                        location_matrix.county_local_flag := '1';
                        location_matrix.city_flag := '1';
                        location_matrix.city_local_flag := '1';
                  END;
               ELSE
                  location_matrix.override_taxtype_code := '*NO';
                  location_matrix.state_flag := '1';
                  location_matrix.county_flag := '1';
                  location_matrix.county_local_flag := '1';
                  location_matrix.city_flag := '1';
                  location_matrix.city_local_flag := '1';
               END IF;
            ELSE
            -- Search for Location Matrix if not already found
            IF p_new_tb_transaction_detail.location_matrix_id IS NULL  AND p_new_tb_transaction_detail.manual_jurisdiction_ind IS NULL THEN
               v_transaction_detail_id := p_new_tb_transaction_detail.transaction_detail_id;
               -- ***** Create Dynamic WHERE Clause for Matrix SQLs ***** --
               -- Location Matrix Drivers
               /*OPEN location_driver_cursor;
               LOOP
                  FETCH location_driver_cursor INTO location_driver;
                  EXIT WHEN location_driver_cursor%NOTFOUND;
                  IF location_driver.null_driver_flag = '1' THEN
                     IF location_driver.wildcard_flag = '1' THEN
                        IF UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'CHAR' OR
                           UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'VARC' THEN
                              vc_location_matrix_where := vc_location_matrix_where || 'AND ' ||
                              '((tb_tmp_transaction_detail.' || location_driver.trans_dtl_column_name || ' IS NULL AND (tb_location_matrix.' || location_driver.matrix_column_name || ' = ''*ALL'' OR tb_location_matrix.' || location_driver.matrix_column_name || ' = ''*NULL'' )) OR ' ||
                              '(tb_tmp_transaction_detail.' || location_driver.trans_dtl_column_name || ' IS NOT NULL AND (tb_location_matrix.' || location_driver.matrix_column_name || ' = ''*ALL'' OR UPPER(tb_tmp_transaction_detail.' || location_driver.trans_dtl_column_name || ') LIKE UPPER(tb_location_matrix.' || location_driver.matrix_column_name || ') ))) ';
                        END IF;
                     ELSE
                        IF UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'CHAR' OR
                           UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'VARC' THEN
                              vc_location_matrix_where := vc_location_matrix_where || 'AND ' ||
                              '((tb_tmp_transaction_detail.' || location_driver.trans_dtl_column_name || ' IS NULL AND (tb_location_matrix.' || location_driver.matrix_column_name || ' = ''*ALL'' OR tb_location_matrix.' || location_driver.matrix_column_name || ' = ''*NULL'' )) OR ' ||
                              '(tb_tmp_transaction_detail.' || location_driver.trans_dtl_column_name || ' IS NOT NULL AND (tb_location_matrix.' || location_driver.matrix_column_name || ' = ''*ALL'' OR UPPER(tb_tmp_transaction_detail.' || location_driver.trans_dtl_column_name || ') = UPPER(tb_location_matrix.' || location_driver.matrix_column_name || ') ))) ';
                        END IF;
                     END IF;
                  ELSE
                     IF location_driver.wildcard_flag = '1' THEN
                        IF UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'CHAR' OR
                           UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'VARC' THEN
                              vc_location_matrix_where := vc_location_matrix_where || 'AND ' ||
                              '(tb_location_matrix.' || location_driver.matrix_column_name || ' = ''*ALL'' OR UPPER(tb_tmp_transaction_detail.' || location_driver.trans_dtl_column_name || ') LIKE UPPER(tb_location_matrix.' || location_driver.matrix_column_name || ') ) ';
                        END IF;
                     ELSE
                        IF UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'CHAR' OR
                           UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'VARC' THEN
                              vc_location_matrix_where := vc_location_matrix_where || 'AND ' ||
                              '(tb_location_matrix.' || location_driver.matrix_column_name || ' = ''*ALL'' OR UPPER(tb_tmp_transaction_detail.' || location_driver.trans_dtl_column_name || ') = UPPER(tb_location_matrix.' || location_driver.matrix_column_name || ') ) ';
                        END IF;
                     END IF;
                  END IF;
               END LOOP;
               CLOSE location_driver_cursor;*/

	       -- New call to sp_gen_location_driver for MidTier Cleanup
	       Sp_Gen_Location_Driver (
			p_generate_driver_reference	=> 'N',
			p_an_batch_id			=> NULL,
			p_transaction_table_name	=> 'tb_tmp_transaction_detail',
			p_location_table_name		=> 'tb_location_matrix',
			p_vc_location_matrix_where	=> vc_location_matrix_where);

               -- If no drivers found raise error, else create transaction detail sql statement
               IF vc_location_matrix_where IS NULL THEN
                  NULL;
               ELSE
                  vc_location_matrix_stmt := vc_location_matrix_select || vc_location_matrix_where || vc_location_matrix_orderby;
               END IF;

               -- Search Location Matrix for matches
               BEGIN
                   OPEN location_matrix_cursor
                    FOR vc_location_matrix_stmt
                  USING v_transaction_detail_id;
                  FETCH location_matrix_cursor
                   INTO location_matrix;
                  -- Rows found?
                  IF location_matrix_cursor%FOUND THEN
                     vn_fetch_rows := location_matrix_cursor%ROWCOUNT;
                  ELSE
                     vn_fetch_rows := 0;
                  END IF;
                  CLOSE location_matrix_cursor;
                  IF SQLCODE != 0 THEN
                    RAISE e_badread;
                  END IF;
               EXCEPTION
                  WHEN e_badread THEN
                     vn_fetch_rows := 0;
               END;

               -- Location Matrix Line Found
               IF vn_fetch_rows > 0 THEN
                  p_new_tb_transaction_detail.location_matrix_id := location_matrix.location_matrix_id;
                  p_new_tb_transaction_detail.jurisdiction_id := location_matrix.jurisdiction_id;
               ELSE
                  p_new_tb_transaction_detail.transaction_ind := 'S';
                  p_new_tb_transaction_detail.suspend_ind := 'L';
               END IF;
            END IF;
            END IF;

            -- Continue if transaction not Suspended
            IF ( p_new_tb_transaction_detail.transaction_ind IS NULL OR p_new_tb_transaction_detail.transaction_ind <> 'S' ) THEN
               -- Get Jurisdiction TaxRates and calculate Tax Amounts
               Sp_Getusetax(p_new_tb_transaction_detail.jurisdiction_id,
                            p_new_tb_transaction_detail.gl_date,
                            p_new_tb_transaction_detail.measure_type_code,
                            p_new_tb_transaction_detail.gl_line_itm_dist_amt,
                            v_taxtype_code,
                            location_matrix.override_taxtype_code,
                            location_matrix.state_flag,
                            location_matrix.county_flag,
                            location_matrix.county_local_flag,
                            location_matrix.city_flag,
                            location_matrix.city_local_flag,
                            p_new_tb_transaction_detail.invoice_freight_amt,
                            p_new_tb_transaction_detail.invoice_discount_amt,
                            p_new_tb_transaction_detail.transaction_state_code,
-- future?? --                     p_new_tb_transaction_detail.import_definition_code,
                            'importdefn',
                            p_new_tb_transaction_detail.taxcode_code,
                            p_new_tb_transaction_detail.jurisdiction_taxrate_id,
                            p_new_tb_transaction_detail.state_use_amount,
                            p_new_tb_transaction_detail.state_use_tier2_amount,
                            p_new_tb_transaction_detail.state_use_tier3_amount,
                            p_new_tb_transaction_detail.county_use_amount,
                            p_new_tb_transaction_detail.county_local_use_amount,
                            p_new_tb_transaction_detail.city_use_amount,
                            p_new_tb_transaction_detail.city_local_use_amount,
                            p_new_tb_transaction_detail.state_use_rate,
                            p_new_tb_transaction_detail.state_use_tier2_rate,
                            p_new_tb_transaction_detail.state_use_tier3_rate,
                            p_new_tb_transaction_detail.state_split_amount,
                            p_new_tb_transaction_detail.state_tier2_min_amount,
                            p_new_tb_transaction_detail.state_tier2_max_amount,
                            p_new_tb_transaction_detail.state_maxtax_amount,
                            p_new_tb_transaction_detail.county_use_rate,
                            p_new_tb_transaction_detail.county_local_use_rate,
                            p_new_tb_transaction_detail.county_split_amount,
                            p_new_tb_transaction_detail.county_maxtax_amount,
                            p_new_tb_transaction_detail.county_single_flag,
                            p_new_tb_transaction_detail.county_default_flag,
                            p_new_tb_transaction_detail.city_use_rate,
                            p_new_tb_transaction_detail.city_local_use_rate,
                            p_new_tb_transaction_detail.city_split_amount,
                            p_new_tb_transaction_detail.city_split_use_rate,
                            p_new_tb_transaction_detail.city_single_flag,
                            p_new_tb_transaction_detail.city_default_flag,
                            p_new_tb_transaction_detail.combined_use_rate,
                            p_new_tb_transaction_detail.tb_calc_tax_amt,
-- future?? --                     p_new_tb_transaction_detail.taxable_amt,
-- Targa --                    p_new_tb_transaction_detail.user_number_10,
                            vn_taxable_amt,
                            v_taxtype_used );

               -- Suspend if tax rate id is null
               IF p_new_tb_transaction_detail.jurisdiction_taxrate_id IS NULL OR p_new_tb_transaction_detail.jurisdiction_taxrate_id = 0 THEN
                  p_new_tb_transaction_detail.transaction_ind := 'S';
                  p_new_tb_transaction_detail.suspend_ind := 'R';
               ELSE
                  -- Check for Taxable Amount -- 3351
                  IF vn_taxable_amt <> p_new_tb_transaction_detail.gl_line_itm_dist_amt THEN
                     p_new_tb_transaction_detail.gl_extract_amt := p_new_tb_transaction_detail.gl_line_itm_dist_amt;
                     p_new_tb_transaction_detail.gl_line_itm_dist_amt := vn_taxable_amt;
                  END IF;
               END IF;
            END IF;

         -- Else, If the new TaxCode Type is "E"xempt
         ELSIF p_new_tb_transaction_detail.taxcode_type_code = 'E' THEN
            p_new_tb_transaction_detail.transaction_ind := 'P';

         -- Else, If the new TaxCode Type is a "Q"uestion
         ELSIF p_new_tb_transaction_detail.taxcode_type_code = 'Q' THEN
           p_new_tb_transaction_detail.transaction_ind := 'Q';

         -- Else, the new TaxCode Type is Unrecognized - Suspend
         ELSE
            p_new_tb_transaction_detail.transaction_ind := 'S';
            p_new_tb_transaction_detail.suspend_ind := '?';
         END IF;
      END IF;

      -- Continue if not Suspended
      IF p_new_tb_transaction_detail.transaction_ind IS NULL OR p_new_tb_transaction_detail.transaction_ind <> 'S' THEN
         p_new_tb_transaction_detail.transaction_ind := 'P';
      END IF;
   END IF;

   -- ******  GL logging module  ****** ------------------------------------------------------------
   Sp_Gl_Logging (p_old_tb_transaction_detail, p_new_tb_transaction_detail, v_sysdate, v_sysdate_plus);

END;
/

