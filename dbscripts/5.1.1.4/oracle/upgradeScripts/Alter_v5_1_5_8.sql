set define off
spool c:\alter_v5_1_5_8.log
set echo on


ALTER TABLE STSCORP.TB_REFERENCE_DOCUMENT
  MODIFY (DOCUMENT_NAME  VARCHAR2(255) );

COMMIT;


spool off