ALTER TABLE STSCORP.TB_BCP_JURISDICTION_TAXRATE MODIFY (GEOCODE  VARCHAR2(12) );
ALTER TABLE STSCORP.TB_JURISDICTION 
  MODIFY (GEOCODE  VARCHAR2(12) )
  MODIFY (CLIENT_GEOCODE  VARCHAR2(12) )
  MODIFY (COMP_GEOCODE  VARCHAR2(12) );
ALTER TABLE STSCORP.TB_TAXCODE_STATE
  MODIFY (GEOCODE VARCHAR2(12) );
