CREATE OR REPLACE PROCEDURE "STSCORP"."SP_REALTIME_PROCESS_WRAPPER" (
    ac_calc_flag 				 IN CHAR,
    ac_write_flag 				 IN CHAR,
    ac_error_code 				 OUT VARCHAR2,
	transaction_detail_id        IN OUT NUMBER,
    source_transaction_id        IN OUT VARCHAR2,
    process_batch_no             IN OUT NUMBER,
    gl_extract_batch_no          IN OUT NUMBER,
    archive_batch_no             IN OUT NUMBER,
    allocation_matrix_id         IN OUT NUMBER,
    allocation_subtrans_id       IN OUT NUMBER,
    entered_date                 IN OUT DATE,
    transaction_status           IN OUT VARCHAR2,
    gl_date                      IN OUT DATE,
    gl_company_nbr               IN OUT VARCHAR2,
    gl_company_name              IN OUT VARCHAR2,
    gl_division_nbr              IN OUT VARCHAR2,
    gl_division_name             IN OUT VARCHAR2,
    gl_cc_nbr_dept_id            IN OUT VARCHAR2,
    gl_cc_nbr_dept_name          IN OUT VARCHAR2,
    gl_local_acct_nbr            IN OUT VARCHAR2,
    gl_local_acct_name           IN OUT VARCHAR2,
    gl_local_sub_acct_nbr        IN OUT VARCHAR2,
    gl_local_sub_acct_name       IN OUT VARCHAR2,
    gl_full_acct_nbr             IN OUT VARCHAR2,
    gl_full_acct_name            IN OUT VARCHAR2,
    gl_line_itm_dist_amt         IN OUT NUMBER,
    orig_gl_line_itm_dist_amt    IN OUT NUMBER,
    vendor_nbr                   IN OUT VARCHAR2,
    vendor_name                  IN OUT VARCHAR2,
    vendor_address_line_1        IN OUT VARCHAR2,
    vendor_address_line_2        IN OUT VARCHAR2,
    vendor_address_line_3        IN OUT VARCHAR2,
    vendor_address_line_4        IN OUT VARCHAR2,
    vendor_address_city          IN OUT VARCHAR2,
    vendor_address_county        IN OUT VARCHAR2,
    vendor_address_state         IN OUT VARCHAR2,
    vendor_address_zip           IN OUT VARCHAR2,
    vendor_address_country       IN OUT VARCHAR2,
    vendor_type                  IN OUT VARCHAR2,
    vendor_type_name             IN OUT VARCHAR2,
    invoice_nbr                  IN OUT VARCHAR2,
    invoice_desc                 IN OUT VARCHAR2,
    invoice_date                 IN OUT DATE,
    invoice_freight_amt          IN OUT NUMBER,
    invoice_discount_amt         IN OUT NUMBER,
    invoice_tax_amt              IN OUT NUMBER,
    invoice_total_amt            IN OUT NUMBER,
    invoice_tax_flg              IN OUT VARCHAR2,
    invoice_line_nbr             IN OUT VARCHAR2,
    invoice_line_name            IN OUT VARCHAR2,
    invoice_line_type            IN OUT VARCHAR2,
    invoice_line_type_name       IN OUT VARCHAR2,
    invoice_line_amt             IN OUT NUMBER,
    invoice_line_tax             IN OUT NUMBER,
    afe_project_nbr              IN OUT VARCHAR2,
    afe_project_name             IN OUT VARCHAR2,
    afe_category_nbr             IN OUT VARCHAR2,
    afe_category_name            IN OUT VARCHAR2,
    afe_sub_cat_nbr              IN OUT VARCHAR2,
    afe_sub_cat_name             IN OUT VARCHAR2,
    afe_use                      IN OUT VARCHAR2,
    afe_contract_type            IN OUT VARCHAR2,
    afe_contract_structure       IN OUT VARCHAR2,
    afe_property_cat             IN OUT VARCHAR2,
    inventory_nbr                IN OUT VARCHAR2,
    inventory_name               IN OUT VARCHAR2,
    inventory_class              IN OUT VARCHAR2,
    inventory_class_name         IN OUT VARCHAR2,
    po_nbr                       IN OUT VARCHAR2,
    po_name                      IN OUT VARCHAR2,
    po_date                      IN OUT DATE,
    po_line_nbr                  IN OUT VARCHAR2,
    po_line_name                 IN OUT VARCHAR2,
    po_line_type                 IN OUT VARCHAR2,
    po_line_type_name            IN OUT VARCHAR2,
    ship_to_location             IN OUT VARCHAR2,
    ship_to_location_name        IN OUT VARCHAR2,
    ship_to_address_line_1       IN OUT VARCHAR2,
    ship_to_address_line_2       IN OUT VARCHAR2,
    ship_to_address_line_3       IN OUT VARCHAR2,
    ship_to_address_line_4       IN OUT VARCHAR2,
    ship_to_address_city         IN OUT VARCHAR2,
    ship_to_address_county       IN OUT VARCHAR2,
    ship_to_address_state        IN OUT VARCHAR2,
    ship_to_address_zip          IN OUT VARCHAR2,
    ship_to_address_country      IN OUT VARCHAR2,
    wo_nbr                       IN OUT VARCHAR2,
    wo_name                      IN OUT VARCHAR2,
    wo_date                      IN OUT DATE,
    wo_type                      IN OUT VARCHAR2,
    wo_type_desc                 IN OUT VARCHAR2,
    wo_class                     IN OUT VARCHAR2,
    wo_class_desc                IN OUT VARCHAR2,
    wo_entity                    IN OUT VARCHAR2,
    wo_entity_desc               IN OUT VARCHAR2,
    wo_line_nbr                  IN OUT VARCHAR2,
    wo_line_name                 IN OUT VARCHAR2,
    wo_line_type                 IN OUT VARCHAR2,
    wo_line_type_desc            IN OUT VARCHAR2,
    wo_shut_down_cd              IN OUT VARCHAR2,
    wo_shut_down_cd_desc         IN OUT VARCHAR2,
    voucher_id                   IN OUT VARCHAR2,
    voucher_name                 IN OUT VARCHAR2,
    voucher_date                 IN OUT DATE,
    voucher_line_nbr             IN OUT VARCHAR2,
    voucher_line_desc            IN OUT VARCHAR2,
    check_nbr                    IN OUT VARCHAR2,
    check_no                     IN OUT NUMBER,
    check_date                   IN OUT DATE,
    check_amt                    IN OUT NUMBER,
    check_desc                   IN OUT VARCHAR2,
    user_text_01                 IN OUT VARCHAR2,
    user_text_02                 IN OUT VARCHAR2,
    user_text_03                 IN OUT VARCHAR2,
    user_text_04                 IN OUT VARCHAR2,
    user_text_05                 IN OUT VARCHAR2,
    user_text_06                 IN OUT VARCHAR2,
    user_text_07                 IN OUT VARCHAR2,
    user_text_08                 IN OUT VARCHAR2,
    user_text_09                 IN OUT VARCHAR2,
    user_text_10                 IN OUT VARCHAR2,
    user_text_11                 IN OUT VARCHAR2,
    user_text_12                 IN OUT VARCHAR2,
    user_text_13                 IN OUT VARCHAR2,
    user_text_14                 IN OUT VARCHAR2,
    user_text_15                 IN OUT VARCHAR2,
    user_text_16                 IN OUT VARCHAR2,
    user_text_17                 IN OUT VARCHAR2,
    user_text_18                 IN OUT VARCHAR2,
    user_text_19                 IN OUT VARCHAR2,
    user_text_20                 IN OUT VARCHAR2,
    user_text_21                 IN OUT VARCHAR2,
    user_text_22                 IN OUT VARCHAR2,
    user_text_23                 IN OUT VARCHAR2,
    user_text_24                 IN OUT VARCHAR2,
    user_text_25                 IN OUT VARCHAR2,
    user_text_26                 IN OUT VARCHAR2,
    user_text_27                 IN OUT VARCHAR2,
    user_text_28                 IN OUT VARCHAR2,
    user_text_29                 IN OUT VARCHAR2,
    user_text_30                 IN OUT VARCHAR2,
    user_number_01               IN OUT NUMBER,
    user_number_02               IN OUT NUMBER,
    user_number_03               IN OUT NUMBER,
    user_number_04               IN OUT NUMBER,
    user_number_05               IN OUT NUMBER,
    user_number_06               IN OUT NUMBER,
    user_number_07               IN OUT NUMBER,
    user_number_08               IN OUT NUMBER,
    user_number_09               IN OUT NUMBER,
    user_number_10               IN OUT NUMBER,
    user_date_01                 IN OUT DATE,
    user_date_02                 IN OUT DATE,
    user_date_03                 IN OUT DATE,
    user_date_04                 IN OUT DATE,
    user_date_05                 IN OUT DATE,
    user_date_06                 IN OUT DATE,
    user_date_07                 IN OUT DATE,
    user_date_08                 IN OUT DATE,
    user_date_09                 IN OUT DATE,
    user_date_10                 IN OUT DATE,
    comments                     IN OUT VARCHAR2,
    tb_calc_tax_amt              IN OUT NUMBER,
    state_use_amount             IN OUT NUMBER,
    state_use_tier2_amount       IN OUT NUMBER,
    state_use_tier3_amount       IN OUT NUMBER,
    county_use_amount            IN OUT NUMBER,
    county_local_use_amount      IN OUT NUMBER,
    city_use_amount              IN OUT NUMBER,
    city_local_use_amount        IN OUT NUMBER,
    transaction_state_code       IN OUT VARCHAR2,
    auto_transaction_state_code  IN OUT VARCHAR2,
    transaction_ind              IN OUT VARCHAR2,
    suspend_ind                  IN OUT VARCHAR2,
    taxcode_detail_id            IN OUT NUMBER,
    taxcode_state_code           IN OUT VARCHAR2,
    taxcode_type_code            IN OUT VARCHAR2,
    taxcode_code                 IN OUT VARCHAR2,
    cch_taxcat_code              IN OUT VARCHAR2,
    cch_group_code               IN OUT VARCHAR2,
    cch_item_code                IN OUT VARCHAR2,
    manual_taxcode_ind           IN OUT VARCHAR2,
    tax_matrix_id                IN OUT NUMBER,
    location_matrix_id           IN OUT NUMBER,
    jurisdiction_id              IN OUT NUMBER,
    jurisdiction_taxrate_id      IN OUT NUMBER,
    manual_jurisdiction_ind      IN OUT VARCHAR2,
    measure_type_code            IN OUT VARCHAR2,
    state_use_rate               IN OUT NUMBER,
    state_use_tier2_rate         IN OUT NUMBER,
    state_use_tier3_rate         IN OUT NUMBER,
    state_split_amount           IN OUT NUMBER,
    state_tier2_min_amount       IN OUT NUMBER,
    state_tier2_max_amount       IN OUT NUMBER,
    state_maxtax_amount          IN OUT NUMBER,
    county_use_rate              IN OUT NUMBER,
    county_local_use_rate        IN OUT NUMBER,
    county_split_amount          IN OUT NUMBER,
    county_maxtax_amount         IN OUT NUMBER,
    county_single_flag           IN OUT CHAR,
    county_default_flag          IN OUT CHAR,
    city_use_rate                IN OUT NUMBER,
    city_local_use_rate          IN OUT NUMBER,
    city_split_amount            IN OUT NUMBER,
    city_split_use_rate          IN OUT NUMBER,
    city_single_flag             IN OUT CHAR,
    city_default_flag            IN OUT CHAR,
    combined_use_rate            IN OUT NUMBER,
    load_timestamp               IN OUT DATE,
    gl_extract_updater           IN OUT VARCHAR2,
    gl_extract_timestamp         IN OUT DATE,
    gl_extract_flag              IN OUT NUMBER,
    gl_log_flag                  IN OUT NUMBER,
    gl_extract_amt               IN OUT NUMBER,
    audit_flag                   IN OUT CHAR,
    audit_user_id                IN OUT VARCHAR2,
    audit_timestamp              IN OUT DATE,
    modify_user_id               IN OUT VARCHAR2,
    modify_timestamp             IN OUT DATE,
    update_user_id               IN OUT VARCHAR2,
    update_timestamp             IN OUT DATE
)
IS
    trans   tb_transaction_detail%rowtype;
BEGIN
    trans.transaction_detail_id := transaction_detail_id;
    trans.source_transaction_id := source_transaction_id;
    trans.process_batch_no := process_batch_no;
    trans.gl_extract_batch_no := gl_extract_batch_no;
    trans.archive_batch_no := archive_batch_no;
    trans.allocation_matrix_id := allocation_matrix_id;
    trans.allocation_subtrans_id := allocation_subtrans_id;
    trans.entered_date := entered_date;
    trans.transaction_status := transaction_status;
    trans.gl_date := gl_date;
    trans.gl_company_nbr := gl_company_nbr;
    trans.gl_company_name := gl_company_name;
    trans.gl_division_nbr := gl_division_nbr;
    trans.gl_division_name := gl_division_name;
    trans.gl_cc_nbr_dept_id := gl_cc_nbr_dept_id;
    trans.gl_cc_nbr_dept_name := gl_cc_nbr_dept_name;
    trans.gl_local_acct_nbr := gl_local_acct_nbr;
    trans.gl_local_acct_name := gl_local_acct_name;
    trans.gl_local_sub_acct_nbr := gl_local_sub_acct_nbr;
    trans.gl_local_sub_acct_name := gl_local_sub_acct_name;
    trans.gl_full_acct_nbr := gl_full_acct_nbr;
    trans.gl_full_acct_name := gl_full_acct_name;
    trans.gl_line_itm_dist_amt := gl_line_itm_dist_amt;
    trans.orig_gl_line_itm_dist_amt := orig_gl_line_itm_dist_amt;
    trans.vendor_nbr := vendor_nbr;
    trans.vendor_name := vendor_name;
    trans.vendor_address_line_1 := vendor_address_line_1;
    trans.vendor_address_line_2 := vendor_address_line_2;
    trans.vendor_address_line_3 := vendor_address_line_3;
    trans.vendor_address_line_4 := vendor_address_line_4;
    trans.vendor_address_city := vendor_address_city;
    trans.vendor_address_county := vendor_address_county;
    trans.vendor_address_state := vendor_address_state;
    trans.vendor_address_zip := vendor_address_zip;
    trans.vendor_address_country := vendor_address_country;
    trans.vendor_type := vendor_type;
    trans.vendor_type_name := vendor_type_name;
    trans.invoice_nbr := invoice_nbr;
    trans.invoice_desc := invoice_desc;
    trans.invoice_date := invoice_date;
    trans.invoice_freight_amt := invoice_freight_amt;
    trans.invoice_discount_amt := invoice_discount_amt;
    trans.invoice_tax_amt := invoice_tax_amt;
    trans.invoice_total_amt := invoice_total_amt;
    trans.invoice_tax_flg := invoice_tax_flg;
    trans.invoice_line_nbr := invoice_line_nbr;
    trans.invoice_line_name := invoice_line_name;
    trans.invoice_line_type := invoice_line_type;
    trans.invoice_line_type_name := invoice_line_type_name;
    trans.invoice_line_amt := invoice_line_amt;
    trans.invoice_line_tax := invoice_line_tax;
    trans.afe_project_nbr := afe_project_nbr;
    trans.afe_project_name := afe_project_name;
    trans.afe_category_nbr := afe_category_nbr;
    trans.afe_category_name := afe_category_name;
    trans.afe_sub_cat_nbr := afe_sub_cat_nbr;
    trans.afe_sub_cat_name := afe_sub_cat_name;
    trans.afe_use := afe_use;
    trans.afe_contract_type := afe_contract_type;
    trans.afe_contract_structure := afe_contract_structure;
    trans.afe_property_cat := afe_property_cat;
    trans.inventory_nbr := inventory_nbr;
    trans.inventory_name := inventory_name;
    trans.inventory_class := inventory_class;
    trans.inventory_class_name := inventory_class_name;
    trans.po_nbr := po_nbr;
    trans.po_name := po_name;
    trans.po_date := po_date;
    trans.po_line_nbr := po_line_nbr;
    trans.po_line_name := po_line_name;
    trans.po_line_type := po_line_type;
    trans.po_line_type_name := po_line_type_name;
    trans.ship_to_location := ship_to_location;
    trans.ship_to_location_name := ship_to_location_name;
    trans.ship_to_address_line_1 := ship_to_address_line_1;
    trans.ship_to_address_line_2 := ship_to_address_line_2;
    trans.ship_to_address_line_3 := ship_to_address_line_3;
    trans.ship_to_address_line_4 := ship_to_address_line_4;
    trans.ship_to_address_city := ship_to_address_city;
    trans.ship_to_address_county := ship_to_address_county;
    trans.ship_to_address_state := ship_to_address_state;
    trans.ship_to_address_zip := ship_to_address_zip;
    trans.ship_to_address_country := ship_to_address_country;
    trans.wo_nbr := wo_nbr;
    trans.wo_name := wo_name;
    trans.wo_date := wo_date;
    trans.wo_type := wo_type;
    trans.wo_type_desc := wo_type_desc;
    trans.wo_class := wo_class;
    trans.wo_class_desc := wo_class_desc;
    trans.wo_entity := wo_entity;
    trans.wo_entity_desc := wo_entity_desc;
    trans.wo_line_nbr := wo_line_nbr;
    trans.wo_line_name := wo_line_name;
    trans.wo_line_type := wo_line_type;
    trans.wo_line_type_desc := wo_line_type_desc;
    trans.wo_shut_down_cd := wo_shut_down_cd;
    trans.wo_shut_down_cd_desc := wo_shut_down_cd_desc;
    trans.voucher_id := voucher_id;
    trans.voucher_name := voucher_name;
    trans.voucher_date := voucher_date;
    trans.voucher_line_nbr := voucher_line_nbr;
    trans.voucher_line_desc := voucher_line_desc;
    trans.check_nbr := check_nbr;
    trans.check_no := check_no;
    trans.check_date := check_date;
    trans.check_amt := check_amt;
    trans.check_desc := check_desc;
    trans.user_text_01 := user_text_01;
    trans.user_text_02 := user_text_02;
    trans.user_text_03 := user_text_03;
    trans.user_text_04 := user_text_04;
    trans.user_text_05 := user_text_05;
    trans.user_text_06 := user_text_06;
    trans.user_text_07 := user_text_07;
    trans.user_text_08 := user_text_08;
    trans.user_text_09 := user_text_09;
    trans.user_text_10 := user_text_10;
    trans.user_text_11 := user_text_11;
    trans.user_text_12 := user_text_12;
    trans.user_text_13 := user_text_13;
    trans.user_text_14 := user_text_14;
    trans.user_text_15 := user_text_15;
    trans.user_text_16 := user_text_16;
    trans.user_text_17 := user_text_17;
    trans.user_text_18 := user_text_18;
    trans.user_text_19 := user_text_19;
    trans.user_text_20 := user_text_20;
    trans.user_text_21 := user_text_21;
    trans.user_text_22 := user_text_22;
    trans.user_text_23 := user_text_23;
    trans.user_text_24 := user_text_24;
    trans.user_text_25 := user_text_25;
    trans.user_text_26 := user_text_26;
    trans.user_text_27 := user_text_27;
    trans.user_text_28 := user_text_28;
    trans.user_text_29 := user_text_29;
    trans.user_text_30 := user_text_30;
    trans.user_number_01 := user_number_01;
    trans.user_number_02 := user_number_02;
    trans.user_number_03 := user_number_03;
    trans.user_number_04 := user_number_04;
    trans.user_number_05 := user_number_05;
    trans.user_number_06 := user_number_06;
    trans.user_number_07 := user_number_07;
    trans.user_number_08 := user_number_08;
    trans.user_number_09 := user_number_09;
    trans.user_number_10 := user_number_10;
    trans.user_date_01 := user_date_01;
    trans.user_date_02 := user_date_02;
    trans.user_date_03 := user_date_03;
    trans.user_date_04 := user_date_04;
    trans.user_date_05 := user_date_05;
    trans.user_date_06 := user_date_06;
    trans.user_date_07 := user_date_07;
    trans.user_date_08 := user_date_08;
    trans.user_date_09 := user_date_09;
    trans.user_date_10 := user_date_10;
    trans.comments := comments;
    trans.tb_calc_tax_amt := tb_calc_tax_amt;
    trans.state_use_amount := state_use_amount;
    trans.state_use_tier2_amount := state_use_tier2_amount;
    trans.state_use_tier3_amount := state_use_tier3_amount;
    trans.county_use_amount := county_use_amount;
    trans.county_local_use_amount := county_local_use_amount;
    trans.city_use_amount := city_use_amount;
    trans.city_local_use_amount := city_local_use_amount;
    trans.transaction_state_code := transaction_state_code;
    trans.auto_transaction_state_code := auto_transaction_state_code;
    trans.transaction_ind := transaction_ind;
    trans.suspend_ind := suspend_ind;
    trans.taxcode_detail_id := taxcode_detail_id;
    trans.taxcode_state_code := taxcode_state_code;
    trans.taxcode_type_code := taxcode_type_code;
    trans.taxcode_code := taxcode_code;
    trans.cch_taxcat_code := cch_taxcat_code;
    trans.cch_group_code := cch_group_code;
    trans.cch_item_code := cch_item_code;
    trans.manual_taxcode_ind := manual_taxcode_ind;
    trans.tax_matrix_id := tax_matrix_id;
    trans.location_matrix_id := location_matrix_id;
    trans.jurisdiction_id := jurisdiction_id;
    trans.jurisdiction_taxrate_id := jurisdiction_taxrate_id;
    trans.manual_jurisdiction_ind := manual_jurisdiction_ind;
    trans.measure_type_code := measure_type_code;
    trans.state_use_rate := state_use_rate;
    trans.state_use_tier2_rate := state_use_tier2_rate;
    trans.state_use_tier3_rate := state_use_tier3_rate;
    trans.state_split_amount := state_split_amount;
    trans.state_tier2_min_amount := state_tier2_min_amount;
    trans.state_tier2_max_amount := state_tier2_max_amount;
    trans.state_maxtax_amount := state_maxtax_amount;
    trans.county_use_rate := county_use_rate;
    trans.county_local_use_rate := county_local_use_rate;
    trans.county_split_amount := county_split_amount;
    trans.county_maxtax_amount := county_maxtax_amount;
    trans.county_single_flag := county_single_flag;
    trans.county_default_flag := county_default_flag;
    trans.city_use_rate := city_use_rate;
    trans.city_local_use_rate := city_local_use_rate;
    trans.city_split_amount := city_split_amount;
    trans.city_split_use_rate := city_split_use_rate;
    trans.city_single_flag := city_single_flag;
    trans.city_default_flag := city_default_flag;
    trans.combined_use_rate := combined_use_rate;
    trans.load_timestamp := load_timestamp;
    trans.gl_extract_updater := gl_extract_updater;
    trans.gl_extract_timestamp := gl_extract_timestamp;
    trans.gl_extract_flag := gl_extract_flag;
    trans.gl_log_flag := gl_log_flag;
    trans.gl_extract_amt := gl_extract_amt;
    trans.audit_flag := audit_flag;
    trans.audit_user_id := audit_user_id;
    trans.audit_timestamp := audit_timestamp;
    trans.modify_user_id := modify_user_id;
    trans.modify_timestamp := modify_timestamp;
    trans.update_user_id := update_user_id;
    trans.update_timestamp := update_timestamp;

    sp_realtime_process(trans, ac_calc_flag, ac_write_flag, ac_error_code);

    transaction_detail_id := trans.transaction_detail_id;
    source_transaction_id := trans.source_transaction_id;
    process_batch_no := trans.process_batch_no;
    gl_extract_batch_no := trans.gl_extract_batch_no;
    archive_batch_no := trans.archive_batch_no;
    allocation_matrix_id := trans.allocation_matrix_id;
    allocation_subtrans_id := trans.allocation_subtrans_id;
    entered_date := trans.entered_date;
    transaction_status := trans.transaction_status;
    gl_date := trans.gl_date;
    gl_company_nbr := trans.gl_company_nbr;
    gl_company_name := trans.gl_company_name;
    gl_division_nbr := trans.gl_division_nbr;
    gl_division_name := trans.gl_division_name;
    gl_cc_nbr_dept_id := trans.gl_cc_nbr_dept_id;
    gl_cc_nbr_dept_name := trans.gl_cc_nbr_dept_name;
    gl_local_acct_nbr := trans.gl_local_acct_nbr;
    gl_local_acct_name := trans.gl_local_acct_name;
    gl_local_sub_acct_nbr := trans.gl_local_sub_acct_nbr;
    gl_local_sub_acct_name := trans.gl_local_sub_acct_name;
    gl_full_acct_nbr := trans.gl_full_acct_nbr;
    gl_full_acct_name := trans.gl_full_acct_name;
    gl_line_itm_dist_amt := trans.gl_line_itm_dist_amt;
    orig_gl_line_itm_dist_amt := trans.orig_gl_line_itm_dist_amt;
    vendor_nbr := trans.vendor_nbr;
    vendor_name := trans.vendor_name;
    vendor_address_line_1 := trans.vendor_address_line_1;
    vendor_address_line_2 := trans.vendor_address_line_2;
    vendor_address_line_3 := trans.vendor_address_line_3;
    vendor_address_line_4 := trans.vendor_address_line_4;
    vendor_address_city := trans.vendor_address_city;
    vendor_address_county := trans.vendor_address_county;
    vendor_address_state := trans.vendor_address_state;
    vendor_address_zip := trans.vendor_address_zip;
    vendor_address_country := trans.vendor_address_country;
    vendor_type := trans.vendor_type;
    vendor_type_name := trans.vendor_type_name;
    invoice_nbr := trans.invoice_nbr;
    invoice_desc := trans.invoice_desc;
    invoice_date := trans.invoice_date;
    invoice_freight_amt := trans.invoice_freight_amt;
    invoice_discount_amt := trans.invoice_discount_amt;
    invoice_tax_amt := trans.invoice_tax_amt;
    invoice_total_amt := trans.invoice_total_amt;
    invoice_tax_flg := trans.invoice_tax_flg;
    invoice_line_nbr := trans.invoice_line_nbr;
    invoice_line_name := trans.invoice_line_name;
    invoice_line_type := trans.invoice_line_type;
    invoice_line_type_name := trans.invoice_line_type_name;
    invoice_line_amt := trans.invoice_line_amt;
    invoice_line_tax := trans.invoice_line_tax;
    afe_project_nbr := trans.afe_project_nbr;
    afe_project_name := trans.afe_project_name;
    afe_category_nbr := trans.afe_category_nbr;
    afe_category_name := trans.afe_category_name;
    afe_sub_cat_nbr := trans.afe_sub_cat_nbr;
    afe_sub_cat_name := trans.afe_sub_cat_name;
    afe_use := trans.afe_use;
    afe_contract_type := trans.afe_contract_type;
    afe_contract_structure := trans.afe_contract_structure;
    afe_property_cat := trans.afe_property_cat;
    inventory_nbr := trans.inventory_nbr;
    inventory_name := trans.inventory_name;
    inventory_class := trans.inventory_class;
    inventory_class_name := trans.inventory_class_name;
    po_nbr := trans.po_nbr;
    po_name := trans.po_name;
    po_date := trans.po_date;
    po_line_nbr := trans.po_line_nbr;
    po_line_name := trans.po_line_name;
    po_line_type := trans.po_line_type;
    po_line_type_name := trans.po_line_type_name;
    ship_to_location := trans.ship_to_location;
    ship_to_location_name := trans.ship_to_location_name;
    ship_to_address_line_1 := trans.ship_to_address_line_1;
    ship_to_address_line_2 := trans.ship_to_address_line_2;
    ship_to_address_line_3 := trans.ship_to_address_line_3;
    ship_to_address_line_4 := trans.ship_to_address_line_4;
    ship_to_address_city := trans.ship_to_address_city;
    ship_to_address_county := trans.ship_to_address_county;
    ship_to_address_state := trans.ship_to_address_state;
    ship_to_address_zip := trans.ship_to_address_zip;
    ship_to_address_country := trans.ship_to_address_country;
    wo_nbr := trans.wo_nbr;
    wo_name := trans.wo_name;
    wo_date := trans.wo_date;
    wo_type := trans.wo_type;
    wo_type_desc := trans.wo_type_desc;
    wo_class := trans.wo_class;
    wo_class_desc := trans.wo_class_desc;
    wo_entity := trans.wo_entity;
    wo_entity_desc := trans.wo_entity_desc;
    wo_line_nbr := trans.wo_line_nbr;
    wo_line_name := trans.wo_line_name;
    wo_line_type := trans.wo_line_type;
    wo_line_type_desc := trans.wo_line_type_desc;
    wo_shut_down_cd := trans.wo_shut_down_cd;
    wo_shut_down_cd_desc := trans.wo_shut_down_cd_desc;
    voucher_id := trans.voucher_id;
    voucher_name := trans.voucher_name;
    voucher_date := trans.voucher_date;
    voucher_line_nbr := trans.voucher_line_nbr;
    voucher_line_desc := trans.voucher_line_desc;
    check_nbr := trans.check_nbr;
    check_no := trans.check_no;
    check_date := trans.check_date;
    check_amt := trans.check_amt;
    check_desc := trans.check_desc;
    user_text_01 := trans.user_text_01;
    user_text_02 := trans.user_text_02;
    user_text_03 := trans.user_text_03;
    user_text_04 := trans.user_text_04;
    user_text_05 := trans.user_text_05;
    user_text_06 := trans.user_text_06;
    user_text_07 := trans.user_text_07;
    user_text_08 := trans.user_text_08;
    user_text_09 := trans.user_text_09;
    user_text_10 := trans.user_text_10;
    user_text_11 := trans.user_text_11;
    user_text_12 := trans.user_text_12;
    user_text_13 := trans.user_text_13;
    user_text_14 := trans.user_text_14;
    user_text_15 := trans.user_text_15;
    user_text_16 := trans.user_text_16;
    user_text_17 := trans.user_text_17;
    user_text_18 := trans.user_text_18;
    user_text_19 := trans.user_text_19;
    user_text_20 := trans.user_text_20;
    user_text_21 := trans.user_text_21;
    user_text_22 := trans.user_text_22;
    user_text_23 := trans.user_text_23;
    user_text_24 := trans.user_text_24;
    user_text_25 := trans.user_text_25;
    user_text_26 := trans.user_text_26;
    user_text_27 := trans.user_text_27;
    user_text_28 := trans.user_text_28;
    user_text_29 := trans.user_text_29;
    user_text_30 := trans.user_text_30;
    user_number_01 := trans.user_number_01;
    user_number_02 := trans.user_number_02;
    user_number_03 := trans.user_number_03;
    user_number_04 := trans.user_number_04;
    user_number_05 := trans.user_number_05;
    user_number_06 := trans.user_number_06;
    user_number_07 := trans.user_number_07;
    user_number_08 := trans.user_number_08;
    user_number_09 := trans.user_number_09;
    user_number_10 := trans.user_number_10;
    user_date_01 := trans.user_date_01;
    user_date_02 := trans.user_date_02;
    user_date_03 := trans.user_date_03;
    user_date_04 := trans.user_date_04;
    user_date_05 := trans.user_date_05;
    user_date_06 := trans.user_date_06;
    user_date_07 := trans.user_date_07;
    user_date_08 := trans.user_date_08;
    user_date_09 := trans.user_date_09;
    user_date_10 := trans.user_date_10;
    comments := trans.comments;
    tb_calc_tax_amt := trans.tb_calc_tax_amt;
    state_use_amount := trans.state_use_amount;
    state_use_tier2_amount := trans.state_use_tier2_amount;
    state_use_tier3_amount := trans.state_use_tier3_amount;
    county_use_amount := trans.county_use_amount;
    county_local_use_amount := trans.county_local_use_amount;
    city_use_amount := trans.city_use_amount;
    city_local_use_amount := trans.city_local_use_amount;
    transaction_state_code := trans.transaction_state_code;
    auto_transaction_state_code := trans.auto_transaction_state_code;
    transaction_ind := trans.transaction_ind;
    suspend_ind := trans.suspend_ind;
    taxcode_detail_id := trans.taxcode_detail_id;
    taxcode_state_code := trans.taxcode_state_code;
    taxcode_type_code := trans.taxcode_type_code;
    taxcode_code := trans.taxcode_code;
    cch_taxcat_code := trans.cch_taxcat_code;
    cch_group_code := trans.cch_group_code;
    cch_item_code := trans.cch_item_code;
    manual_taxcode_ind := trans.manual_taxcode_ind;
    tax_matrix_id := trans.tax_matrix_id;
    location_matrix_id := trans.location_matrix_id;
    jurisdiction_id := trans.jurisdiction_id;
    jurisdiction_taxrate_id := trans.jurisdiction_taxrate_id;
    manual_jurisdiction_ind := trans.manual_jurisdiction_ind;
    measure_type_code := trans.measure_type_code;
    state_use_rate := trans.state_use_rate;
    state_use_tier2_rate := trans.state_use_tier2_rate;
    state_use_tier3_rate := trans.state_use_tier3_rate;
    state_split_amount := trans.state_split_amount;
    state_tier2_min_amount := trans.state_tier2_min_amount;
    state_tier2_max_amount := trans.state_tier2_max_amount;
    state_maxtax_amount := trans.state_maxtax_amount;
    county_use_rate := trans.county_use_rate;
    county_local_use_rate := trans.county_local_use_rate;
    county_split_amount := trans.county_split_amount;
    county_maxtax_amount := trans.county_maxtax_amount;
    county_single_flag := trans.county_single_flag;
    county_default_flag := trans.county_default_flag;
    city_use_rate := trans.city_use_rate;
    city_local_use_rate := trans.city_local_use_rate;
    city_split_amount := trans.city_split_amount;
    city_split_use_rate := trans.city_split_use_rate;
    city_single_flag := trans.city_single_flag;
    city_default_flag := trans.city_default_flag;
    combined_use_rate := trans.combined_use_rate;
    load_timestamp := trans.load_timestamp;
    gl_extract_updater := trans.gl_extract_updater;
    gl_extract_timestamp := trans.gl_extract_timestamp;
    gl_extract_flag := trans.gl_extract_flag;
    gl_log_flag := trans.gl_log_flag;
    gl_extract_amt := trans.gl_extract_amt;
    audit_flag := trans.audit_flag;
    audit_user_id := trans.audit_user_id;
    audit_timestamp := trans.audit_timestamp;
    modify_user_id := trans.modify_user_id;
    modify_timestamp := trans.modify_timestamp;
    update_user_id := trans.update_user_id;
    update_timestamp := trans.update_timestamp;
END;