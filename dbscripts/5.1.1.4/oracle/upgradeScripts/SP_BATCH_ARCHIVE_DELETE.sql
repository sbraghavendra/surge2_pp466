CREATE OR REPLACE PROCEDURE Sp_Batch_Archive_Delete(an_batch_id NUMBER)
IS
-- Table defined variables
   v_batch_status_code             TB_BATCH.batch_status_code%TYPE                   := NULL;
   v_error_sev_code                TB_BATCH.error_sev_code%TYPE                      := ' ';
   v_severity_level                TB_LIST_CODE.severity_level%TYPE                  := ' ';
   v_write_import_line_flag        TB_LIST_CODE.write_import_line_flag%TYPE          := '1';
   v_abort_import_flag             TB_LIST_CODE.abort_import_flag%TYPE               := '0';
   v_sysdate                       TB_BATCH.actual_start_timestamp%TYPE              := SYS_EXTRACT_UTC(SYSTIMESTAMP);

-- Program defined variables
   vi_error_count                  INTEGER                                           := 0;
   vi_cursor_id                    INTEGER                                           := 0;
   vi_rows_processed               INTEGER                                           := 0;

-- Define Exceptions
   e_badread                       EXCEPTION;
   e_badupdate                     EXCEPTION;
   e_badwrite                      EXCEPTION;
   e_wrongdata                     EXCEPTION;
   e_abort                         EXCEPTION;

-- Program starts ******************************************************************************************
BEGIN
   -- Confirm batch exists and is flagged for processing
   BEGIN
      SELECT batch_status_code
        INTO v_batch_status_code
        FROM TB_BATCH
       WHERE batch_id = an_batch_id;
      IF SQLCODE != 0 THEN
         RAISE e_badread;
      ELSIF v_batch_status_code != 'FADR' THEN
         RAISE e_wrongdata;
      END IF;
   EXCEPTION
      WHEN NO_DATA_FOUND OR e_badread THEN
         vi_error_count := vi_error_count + 1;
         Sp_Geterrorcode('A8', an_batch_id, 'ADR', v_sysdate, v_severity_level, v_write_import_line_flag, v_abort_import_flag);
         IF v_severity_level > v_error_sev_code THEN
            v_error_sev_code := v_severity_level;
         END IF;
      WHEN e_wrongdata THEN
         vi_error_count := vi_error_count + 1;
         Sp_Geterrorcode('A9', an_batch_id, 'ADR', v_sysdate, v_severity_level, v_write_import_line_flag, v_abort_import_flag);
         IF v_severity_level > v_error_sev_code THEN
            v_error_sev_code := v_severity_level;
         END IF;
   END;
   -- Abort Procedure?
   IF v_abort_import_flag = '1' THEN
      v_batch_status_code := 'ABADR';
      RAISE e_abort;
   END IF;

   -- Update batch as processing
   BEGIN
      UPDATE TB_BATCH
         SET batch_status_code = 'XADR',
             error_sev_code = '',
             actual_start_timestamp = SYS_EXTRACT_UTC(SYSTIMESTAMP)
       WHERE batch_id = an_batch_id;
      IF SQLCODE != 0 THEN
         RAISE e_badupdate;
      END IF;
   EXCEPTION
      WHEN e_badupdate THEN
         vi_error_count := vi_error_count + 1;
         Sp_Geterrorcode('A1', an_batch_id, 'ADR', v_sysdate, v_severity_level, v_write_import_line_flag, v_abort_import_flag);
         IF v_severity_level > v_error_sev_code THEN
            v_error_sev_code := v_severity_level;
         END IF;
   END;
   -- Abort Procedure?
   IF v_abort_import_flag = '1' THEN
      v_batch_status_code := 'ABADR';
      RAISE e_abort;
   END IF;

   COMMIT;

   /**  Delete Archive Batch from Restore Transaction Detail table  *******************************/
   DELETE FROM TB_RESTORE_TRANSACTION_DTL
    WHERE archive_batch_no = an_batch_id;

   /**  Update Batch file  ************************************************************************/
   UPDATE TB_BATCH
      SET batch_status_code = 'A',
          actual_end_timestamp = SYS_EXTRACT_UTC(SYSTIMESTAMP)
    WHERE batch_id = an_batch_id;

    COMMIT;

EXCEPTION
   WHEN e_abort THEN
      -- Update batch with error codes
      UPDATE TB_BATCH
         SET batch_status_code = v_batch_status_code,
             error_sev_code = v_error_sev_code
       WHERE batch_id = an_batch_id;
      COMMIT;
END;
/

