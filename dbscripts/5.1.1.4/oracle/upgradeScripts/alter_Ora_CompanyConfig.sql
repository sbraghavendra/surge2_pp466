/* ***  Alter for Company Config  *** */

set define off
SPOOL alter_CompanyConfig.log

-- Add columns to existing tables
-- tb_entity
ALTER TABLE stscorp.tb_entity
   ADD (fein                       NUMBER)
   ADD (description                VARCHAR2(255))
   ADD (jurisdiction_id            NUMBER)
   ADD (geocode                    VARCHAR2(12))
   ADD (address_line_1             VARCHAR2(50))
   ADD (address_line_2             VARCHAR2(50))
   ADD (city                       VARCHAR2(50))
   ADD (county                     VARCHAR2(50))
   ADD (state                      VARCHAR2(10))
   ADD (zip                        VARCHAR2(5))
   ADD (zipplus4                   VARCHAR2(4))
   ADD (country                    VARCHAR2(10))
   ADD (lock_flag                  CHAR(1))
   ADD (active_flag                CHAR(1));

-- tb_jurisdiction
ALTER TABLE stscorp.tb_jurisdiction
   ADD (reporting_city             VARCHAR2(50))
   ADD (reporting_city_fips        VARCHAR2(5))
   ADD (stj1_name                  VARCHAR2(50))
   ADD (stj2_name                  VARCHAR2(50))
   ADD (stj3_name                  VARCHAR2(50))
   ADD (stj4_name                  VARCHAR2(50))
   ADD (stj5_name                  VARCHAR2(50))
   ADD (effective_date             DATE)
   ADD (expiration_date            DATE)
   ADD (modified_flag              CHAR(1))
   ADD (country_nexusind_code      VARCHAR2(10))
   ADD (state_nexusind_code        VARCHAR2(10))
   ADD (county_nexusind_code       VARCHAR2(10))
   ADD (city_nexusind_code         VARCHAR2(10))
   ADD (stj1_nexusind_code         VARCHAR2(10))
   ADD (stj2_nexusind_code         VARCHAR2(10))
   ADD (stj3_nexusind_code         VARCHAR2(10))
   ADD (stj4_nexusind_code         VARCHAR2(10))
   ADD (stj5_nexusind_code         VARCHAR2(10));

-- tb_jurisdiction_taxrate
ALTER TABLE stscorp.tb_jurisdiction_taxrate
   ADD (stj1_use_rate              NUMBER)
   ADD (stj1_sales_rate            NUMBER)
   ADD (stj2_use_rate              NUMBER)
   ADD (stj2_sales_rate            NUMBER)
   ADD (stj3_use_rate              NUMBER)
   ADD (stj3_sales_rate            NUMBER)
   ADD (stj4_use_rate              NUMBER)
   ADD (stj4_sales_rate            NUMBER)
   ADD (stj5_use_rate              NUMBER)
   ADD (stj5_sales_rate            NUMBER)
   ADD (combined_use_rate          NUMBER)
   ADD (combined_sales_rate        NUMBER);

-- tb_location_matrix
ALTER TABLE stscorp.tb_location_matrix
   ADD (entity_id                  NUMBER)
   ADD (stj1_flag                  NUMBER)
   ADD (stj2_flag                  NUMBER)
   ADD (stj3_flag                  NUMBER)
   ADD (stj4_flag                  NUMBER)
   ADD (stj5_flag                  NUMBER);

-- tb_tmp_location_matrix
ALTER TABLE stscorp.tb_tmp_location_matrix
   ADD (entity_id                  NUMBER);

-- tb_user
ALTER TABLE stscorp.tb_user
   ADD (add_nexus_flag             CHAR(1));

-- tb_transaction_detail, tb_tmp_transaction_detail, tb_gl_export_log, tb_gl_report_log, tb_bcp_transactions
ALTER TABLE stscorp.tb_transaction_detail
   ADD (entity_code                VARCHAR2(40))
   ADD (stj1_rate                  NUMBER)
   ADD (stj2_rate                  NUMBER)
   ADD (stj3_rate                  NUMBER)
   ADD (stj4_rate                  NUMBER)
   ADD (stj5_rate                  NUMBER)
   ADD (stj1_amount                NUMBER)
   ADD (stj2_amount                NUMBER)
   ADD (stj3_amount                NUMBER)
   ADD (stj4_amount                NUMBER)
   ADD (stj5_amount                NUMBER)
   ADD (product_code               VARCHAR2(100));

ALTER TABLE tb_tmp_transaction_detail
   ADD (entity_code                VARCHAR2(40))
   ADD (stj1_rate                  NUMBER)
   ADD (stj2_rate                  NUMBER)
   ADD (stj3_rate                  NUMBER)
   ADD (stj4_rate                  NUMBER)
   ADD (stj5_rate                  NUMBER)
   ADD (stj1_amount                NUMBER)
   ADD (stj2_amount                NUMBER)
   ADD (stj3_amount                NUMBER)
   ADD (stj4_amount                NUMBER)
   ADD (stj5_amount                NUMBER)
   ADD (product_code               VARCHAR2(100));

ALTER TABLE tb_gl_export_log
   ADD (entity_code                VARCHAR2(40))
   ADD (stj1_rate                  NUMBER)
   ADD (stj2_rate                  NUMBER)
   ADD (stj3_rate                  NUMBER)
   ADD (stj4_rate                  NUMBER)
   ADD (stj5_rate                  NUMBER)
   ADD (stj1_amount                NUMBER)
   ADD (stj2_amount                NUMBER)
   ADD (stj3_amount                NUMBER)
   ADD (stj4_amount                NUMBER)
   ADD (stj5_amount                NUMBER)
   ADD (product_code               VARCHAR2(100));

ALTER TABLE tb_gl_report_log
   ADD (entity_code                VARCHAR2(40))
   ADD (stj1_rate                  NUMBER)
   ADD (stj2_rate                  NUMBER)
   ADD (stj3_rate                  NUMBER)
   ADD (stj4_rate                  NUMBER)
   ADD (stj5_rate                  NUMBER)
   ADD (stj1_amount                NUMBER)
   ADD (stj2_amount                NUMBER)
   ADD (stj3_amount                NUMBER)
   ADD (stj4_amount                NUMBER)
   ADD (stj5_amount                NUMBER)
   ADD (product_code               VARCHAR2(100));

ALTER TABLE tb_bcp_transactions
   ADD (entity_code                VARCHAR2(40))
   ADD (stj1_rate                  NUMBER)
   ADD (stj2_rate                  NUMBER)
   ADD (stj3_rate                  NUMBER)
   ADD (stj4_rate                  NUMBER)
   ADD (stj5_rate                  NUMBER)
   ADD (stj1_amount                NUMBER)
   ADD (stj2_amount                NUMBER)
   ADD (stj3_amount                NUMBER)
   ADD (stj4_amount                NUMBER)
   ADD (stj5_amount                NUMBER)
   ADD (product_code               VARCHAR2(100));

-- Create new tables
-- tb_entity_default
CREATE TABLE stscorp.tb_entity_default (
   entity_default_id               NUMBER                  NOT NULL,
   entity_id                       NUMBER,
   default_code                    VARCHAR2(10),
   default_value                   VARCHAR2(100),
   update_user_id                  VARCHAR2(40)            DEFAULT USER,
   update_timestamp                DATE                    DEFAULT SYS_EXTRACT_UTC(SYSTIMESTAMP),
   CONSTRAINT pk_tb_entity_default PRIMARY KEY (entity_default_id));
CREATE SEQUENCE stscorp.sq_tb_entity_default_id MINVALUE 1 MAXVALUE 1000000000000000000000000000 INCREMENT BY 1 START WITH 120 NOCACHE NOORDER NOCYCLE ;

ALTER TABLE stscorp.tb_entity_default
   ADD CONSTRAINT fk_tb_entity_default_1
   FOREIGN KEY (entity_id) 
   REFERENCES stscorp.tb_entity (entity_id) ON DELETE CASCADE;

-- tb_entity_locn_set
CREATE TABLE stscorp.tb_entity_locn_set (
   entity_locn_set_id              NUMBER                  NOT NULL,
   entity_id                       NUMBER,
   locn_set_code                   VARCHAR2(40),
   name                            VARCHAR2(50),
   active_flag                     CHAR(1),
   update_user_id                  VARCHAR2(40)            DEFAULT USER,
   update_timestamp                DATE                    DEFAULT SYS_EXTRACT_UTC(SYSTIMESTAMP),
   CONSTRAINT pk_tb_entity_locn_set PRIMARY KEY (entity_locn_set_id));
CREATE SEQUENCE stscorp.sq_tb_entity_locn_set_id MINVALUE 1 MAXVALUE 1000000000000000000000000000 INCREMENT BY 1 START WITH 120 NOCACHE NOORDER NOCYCLE ;

ALTER TABLE stscorp.tb_entity_locn_set
   ADD CONSTRAINT fk_tb_entity_locn_set_1
   FOREIGN KEY (entity_id) 
   REFERENCES stscorp.tb_entity (entity_id) ON DELETE CASCADE;

-- tb_entity_locn_set_dtl
CREATE TABLE stscorp.tb_entity_locn_set_dtl (
   entity_locn_set_dtl_id          NUMBER                  NOT NULL,
   entity_locn_set_id              NUMBER,
   description                     VARCHAR2(100),
   sf_entity_id                    NUMBER,
   poo_entity_id                   NUMBER,
   poa_entity_id                   NUMBER,
   effective_date                  DATE,
   update_user_id                  VARCHAR2(40)            DEFAULT USER,
   update_timestamp                DATE                    DEFAULT SYS_EXTRACT_UTC(SYSTIMESTAMP),
   CONSTRAINT pk_tb_entity_locn_set_dtl PRIMARY KEY (entity_locn_set_dtl_id));
CREATE SEQUENCE stscorp.sq_tb_entity_locn_set_dtl_id MINVALUE 1 MAXVALUE 1000000000000000000000000000 INCREMENT BY 1 START WITH 120 NOCACHE NOORDER NOCYCLE ;

ALTER TABLE stscorp.tb_entity_locn_set_dtl
   ADD CONSTRAINT fk_tb_entity_locn_set_dtl_1
   FOREIGN KEY (entity_locn_set_id) 
   REFERENCES stscorp.tb_entity_locn_set (entity_locn_set_id) ON DELETE CASCADE;

-- tb_nexus_def
CREATE TABLE stscorp.tb_nexus_def (
   nexus_def_id                    NUMBER                  NOT NULL,
   entity_id                       NUMBER,
   nexus_country_code              VARCHAR2(10),
   nexus_state_code                VARCHAR2(10),
   nexus_county                    VARCHAR2(50),
   nexus_city                      VARCHAR2(50),
   nexus_stj                       VARCHAR2(50),
   nexus_flag                      CHAR(1),
   auto_user_id                    VARCHAR2(40),
   update_user_id                  VARCHAR2(40)            DEFAULT USER,
   update_timestamp                DATE                    DEFAULT SYS_EXTRACT_UTC(SYSTIMESTAMP),
   CONSTRAINT pk_tb_nexus_def PRIMARY KEY (nexus_def_id));
CREATE SEQUENCE stscorp.sq_tb_nexus_def_id MINVALUE 1 MAXVALUE 1000000000000000000000000000 INCREMENT BY 1 START WITH 120 NOCACHE NOORDER NOCYCLE ;

ALTER TABLE stscorp.tb_nexus_def
   ADD CONSTRAINT fk_tb_nexus_def_1
   FOREIGN KEY (entity_id) 
   REFERENCES stscorp.tb_entity (entity_id) ON DELETE CASCADE;

-- tb_nexus_def_detail
CREATE TABLE stscorp.tb_nexus_def_detail (
   nexus_def_detail_id             NUMBER                  NOT NULL,
   nexus_def_id                    NUMBER,
   effective_date                  DATE,
   expiration_date                 DATE,
   nexus_type_code                 VARCHAR2(10),
   active_flag                     CHAR(1),
   auto_user_id                    VARCHAR2(40),
   update_user_id                  VARCHAR2(40)            DEFAULT USER,
   update_timestamp                DATE                    DEFAULT SYS_EXTRACT_UTC(SYSTIMESTAMP),
   CONSTRAINT pk_tb_nexus_def_detail PRIMARY KEY (nexus_def_detail_id));
CREATE SEQUENCE stscorp.sq_tb_nexus_def_detail_id MINVALUE 1 MAXVALUE 1000000000000000000000000000 INCREMENT BY 1 START WITH 120 NOCACHE NOORDER NOCYCLE ;

ALTER TABLE stscorp.tb_nexus_def_detail
   ADD CONSTRAINT fk_tb_nexus_def_detail_1
   FOREIGN KEY (nexus_def_id) 
   REFERENCES stscorp.tb_nexus_def (nexus_def_id) ON DELETE CASCADE;

-- tb_nexus_def_log
CREATE TABLE stscorp.tb_nexus_def_log (
   nexus_def_log_id                NUMBER                  NOT NULL,
   source_code                     VARCHAR2(10),
   source_id                       NUMBER,
   entity_id                       NUMBER,
   nexus_def_detail_id             NUMBER,
   nexus_country_code              VARCHAR2(10),
   nexus_state_code                VARCHAR2(10),
   nexus_county                    VARCHAR2(50),
   nexus_city                      VARCHAR2(50),
   nexus_stj                       VARCHAR2(50),
   comments                        VARCHAR(255),
   auto_user_id                    VARCHAR2(40),
   update_user_id                  VARCHAR2(40)            DEFAULT USER,
   update_timestamp                DATE                    DEFAULT SYS_EXTRACT_UTC(SYSTIMESTAMP),
   CONSTRAINT pk_tb_nexus_def_log PRIMARY KEY (nexus_def_log_id));
CREATE SEQUENCE stscorp.sq_tb_nexus_def_log_id MINVALUE 1 MAXVALUE 1000000000000000000000000000 INCREMENT BY 1 START WITH 120 NOCACHE NOORDER NOCYCLE ;

ALTER TABLE stscorp.tb_nexus_def_log
   ADD CONSTRAINT fk_tb_nexus_def_log_1
   FOREIGN KEY (entity_id) 
   REFERENCES stscorp.tb_entity (entity_id) ON DELETE CASCADE;

-- tb_registration
CREATE TABLE stscorp.tb_registration (
   registration_id                 NUMBER                  NOT NULL,
   entity_id                       NUMBER,
   reg_country_code                VARCHAR2(10),
   reg_state_code                  VARCHAR2(10),
   reg_county                      VARCHAR2(50),
   reg_city                        VARCHAR2(50),
   reg_stj                         VARCHAR2(50),
   update_user_id                  VARCHAR2(40)            DEFAULT USER,
   update_timestamp                DATE                    DEFAULT SYS_EXTRACT_UTC(SYSTIMESTAMP),
   CONSTRAINT pk_tb_registration PRIMARY KEY (registration_id));
CREATE SEQUENCE stscorp.sq_tb_registration_id MINVALUE 1 MAXVALUE 1000000000000000000000000000 INCREMENT BY 1 START WITH 120 NOCACHE NOORDER NOCYCLE ;

ALTER TABLE stscorp.tb_registration
   ADD CONSTRAINT fk_tb_registration_1
   FOREIGN KEY (entity_id)
   REFERENCES stscorp.tb_entity (entity_id) ON DELETE CASCADE;

-- tb_registration_detail
CREATE TABLE tb_registration_detail (
   registration_detail_id          NUMBER                  NOT NULL,
   registration_id                 NUMBER,
   effective_date                  DATE,
   expiration_date                 DATE,
   reg_type_code                   VARCHAR2(10),
   reg_no                          VARCHAR2(100),
   active_flag                     CHAR(1),
   update_user_id                  VARCHAR2(40)            DEFAULT USER,
   update_timestamp                DATE                    DEFAULT SYS_EXTRACT_UTC(SYSTIMESTAMP),
   CONSTRAINT pk_tb_registration_detail PRIMARY KEY (registration_detail_id));
CREATE SEQUENCE stscorp.sq_tb_registration_detail_id MINVALUE 1 MAXVALUE 1000000000000000000000000000 INCREMENT BY 1 START WITH 120 NOCACHE NOORDER NOCYCLE ;

ALTER TABLE stscorp.tb_registration_detail
   ADD CONSTRAINT fk_tb_registration_detail_1
   FOREIGN KEY (registration_id)
   REFERENCES stscorp.tb_registration (registration_id) ON DELETE CASCADE;

-- tb_product_map
CREATE TABLE stscorp.tb_product_map (
   product_map_id                  NUMBER                  NOT NULL,
   entity_id                       NUMBER                  CONSTRAINT fk_tb_product_map_1 REFERENCES tb_entity(entity_id) ON DELETE CASCADE,
   product_code                    VARCHAR2(100),
   product_code_thru               VARCHAR2(100),
   taxcode_code                    VARCHAR2(40)            CONSTRAINT fk_tb_product_map_2 REFERENCES tb_taxcode(taxcode_code) ON DELETE CASCADE,
   sort_no                         NUMBER,
   update_user_id                  VARCHAR2(40)            DEFAULT USER,
   update_timestamp                DATE                    DEFAULT SYS_EXTRACT_UTC(SYSTIMESTAMP),
   CONSTRAINT pk_tb_product_map PRIMARY KEY (product_map_id));
CREATE SEQUENCE stscorp.sq_tb_product_map_id MINVALUE 1 MAXVALUE 1000000000000000000000000000 INCREMENT BY 1 START WITH 120 NOCACHE NOORDER NOCYCLE ;

-- Update table data
-- tb_batch_metadata ??
--INSERT INTO tb_batch_metadata (batch_type_code, vc01_desc, vc02_desc, nu01_desc, nu02_desc, ts01_desc, update_user_id, update_timestamp)
--VALUES ('TCR', 'Rules Update File Name', 'File Type', 'Update Release', 'Special Rate Total', 'Update Date', USER, SYS_EXTRACT_UTC(SYSTIMESTAMP));

-- tb_list_code
INSERT INTO tb_list_code (code_type_code, code_code, description, message, explanation, severity_level, write_import_line_flag, abort_import_flag, update_user_id, update_timestamp)
VALUES ('*DEF', 'UNLOCKCOPY', 'Unlock Copy Options', NULL, NULL, NULL, NULL, NULL, USER, SYS_EXTRACT_UTC(SYSTIMESTAMP)); 
INSERT INTO tb_list_code (code_type_code, code_code, description, message, explanation, severity_level, write_import_line_flag, abort_import_flag, update_user_id, update_timestamp)
VALUES ('UNLOCKCOPY', '1', 'Defaults', NULL, NULL, NULL, NULL, NULL, USER, SYS_EXTRACT_UTC(SYSTIMESTAMP)); 
INSERT INTO tb_list_code (code_type_code, code_code, description, message, explanation, severity_level, write_import_line_flag, abort_import_flag, update_user_id, update_timestamp)
VALUES ('UNLOCKCOPY', '2', 'Nexus Definitions', NULL, NULL, NULL, NULL, NULL, USER, SYS_EXTRACT_UTC(SYSTIMESTAMP)); 
INSERT INTO tb_list_code (code_type_code, code_code, description, message, explanation, severity_level, write_import_line_flag, abort_import_flag, update_user_id, update_timestamp)
VALUES ('UNLOCKCOPY', '3', 'Registrations', NULL, NULL, NULL, NULL, NULL, USER, SYS_EXTRACT_UTC(SYSTIMESTAMP)); 
INSERT INTO tb_list_code (code_type_code, code_code, description, message, explanation, severity_level, write_import_line_flag, abort_import_flag, update_user_id, update_timestamp)
VALUES ('UNLOCKCOPY', '4', 'Direct Pay Permits', NULL, NULL, NULL, NULL, NULL, USER, SYS_EXTRACT_UTC(SYSTIMESTAMP)); 
INSERT INTO tb_list_code (code_type_code, code_code, description, message, explanation, severity_level, write_import_line_flag, abort_import_flag, update_user_id, update_timestamp)
VALUES ('UNLOCKCOPY', '5', 'Product Mappings', NULL, NULL, NULL, NULL, NULL, USER, SYS_EXTRACT_UTC(SYSTIMESTAMP));

INSERT INTO tb_list_code (code_type_code, code_code, description, message, explanation, severity_level, write_import_line_flag, abort_import_flag, update_user_id, update_timestamp)
VALUES ('*DEF', 'DEFCODES', 'Default Codes', NULL, NULL, NULL, NULL, NULL, USER, SYS_EXTRACT_UTC(SYSTIMESTAMP)); 
INSERT INTO tb_list_code (code_type_code, code_code, description, message, explanation, severity_level, write_import_line_flag, abort_import_flag, update_user_id, update_timestamp)
VALUES ('DEFCODES', 'MOD', 'Method of Delivery', NULL, NULL, NULL, NULL, NULL, USER, SYS_EXTRACT_UTC(SYSTIMESTAMP)); 
INSERT INTO tb_list_code (code_type_code, code_code, description, message, explanation, severity_level, write_import_line_flag, abort_import_flag, update_user_id, update_timestamp)
VALUES ('DEFCODES', 'RATETYPE', 'Product Type', NULL, NULL, NULL, NULL, NULL, USER, SYS_EXTRACT_UTC(SYSTIMESTAMP)); 
INSERT INTO tb_list_code (code_type_code, code_code, description, message, explanation, severity_level, write_import_line_flag, abort_import_flag, update_user_id, update_timestamp)
VALUES ('DEFCODES', 'GEORECON', 'Geocode Reconciliation', NULL, NULL, NULL, NULL, NULL, USER, SYS_EXTRACT_UTC(SYSTIMESTAMP)); 
INSERT INTO tb_list_code (code_type_code, code_code, description, message, explanation, severity_level, write_import_line_flag, abort_import_flag, update_user_id, update_timestamp)
VALUES ('DEFCODES', 'TRANSTYPE', 'Transaction Type', NULL, NULL, NULL, NULL, NULL, USER, SYS_EXTRACT_UTC(SYSTIMESTAMP)); 

INSERT INTO tb_list_code (code_type_code, code_code, description, message, explanation, severity_level, write_import_line_flag, abort_import_flag, update_user_id, update_timestamp)
VALUES ('*DEF', 'MOD', 'Method of Delivery', NULL, NULL, NULL, NULL, NULL, USER, SYS_EXTRACT_UTC(SYSTIMESTAMP)); 
INSERT INTO tb_list_code (code_type_code, code_code, description, message, explanation, severity_level, write_import_line_flag, abort_import_flag, update_user_id, update_timestamp)
VALUES ('MOD', '1', 'Common Carrier', NULL, NULL, NULL, NULL, NULL, USER, SYS_EXTRACT_UTC(SYSTIMESTAMP)); 
INSERT INTO tb_list_code (code_type_code, code_code, description, message, explanation, severity_level, write_import_line_flag, abort_import_flag, update_user_id, update_timestamp)
VALUES ('MOD', '2', 'Contract Carrier', NULL, NULL, NULL, NULL, NULL, USER, SYS_EXTRACT_UTC(SYSTIMESTAMP)); 
INSERT INTO tb_list_code (code_type_code, code_code, description, message, explanation, severity_level, write_import_line_flag, abort_import_flag, update_user_id, update_timestamp)
VALUES ('MOD', '3', 'Purchaser Vehicle', NULL, NULL, NULL, NULL, NULL, USER, SYS_EXTRACT_UTC(SYSTIMESTAMP)); 
INSERT INTO tb_list_code (code_type_code, code_code, description, message, explanation, severity_level, write_import_line_flag, abort_import_flag, update_user_id, update_timestamp)
VALUES ('MOD', '4', 'Seller Vehicle', NULL, NULL, NULL, NULL, NULL, USER, SYS_EXTRACT_UTC(SYSTIMESTAMP)); 
INSERT INTO tb_list_code (code_type_code, code_code, description, message, explanation, severity_level, write_import_line_flag, abort_import_flag, update_user_id, update_timestamp)
VALUES ('MOD', '5', 'Tangible Medium', NULL, NULL, NULL, NULL, NULL, USER, SYS_EXTRACT_UTC(SYSTIMESTAMP)); 
INSERT INTO tb_list_code (code_type_code, code_code, description, message, explanation, severity_level, write_import_line_flag, abort_import_flag, update_user_id, update_timestamp)
VALUES ('MOD', '6', 'Load & Leave', NULL, NULL, NULL, NULL, NULL, USER, SYS_EXTRACT_UTC(SYSTIMESTAMP)); 
INSERT INTO tb_list_code (code_type_code, code_code, description, message, explanation, severity_level, write_import_line_flag, abort_import_flag, update_user_id, update_timestamp)
VALUES ('MOD', '7', 'Electronic Download - Remote Access', NULL, NULL, NULL, NULL, NULL, USER, SYS_EXTRACT_UTC(SYSTIMESTAMP)); 
INSERT INTO tb_list_code (code_type_code, code_code, description, message, explanation, severity_level, write_import_line_flag, abort_import_flag, update_user_id, update_timestamp)
VALUES ('MOD', '8', 'Electronic Download', NULL, NULL, NULL, NULL, NULL, USER, SYS_EXTRACT_UTC(SYSTIMESTAMP)); 

INSERT INTO tb_list_code (code_type_code, code_code, description, message, explanation, severity_level, write_import_line_flag, abort_import_flag, update_user_id, update_timestamp)
VALUES ('*DEF', 'GEORECON', 'Geocode Reconciliation', NULL, NULL, NULL, NULL, NULL, USER, SYS_EXTRACT_UTC(SYSTIMESTAMP)); 
INSERT INTO tb_list_code (code_type_code, code_code, description, message, explanation, severity_level, write_import_line_flag, abort_import_flag, update_user_id, update_timestamp)
VALUES ('GEORECON', '1', 'Highest Rate', NULL, NULL, NULL, NULL, NULL, USER, SYS_EXTRACT_UTC(SYSTIMESTAMP)); 
INSERT INTO tb_list_code (code_type_code, code_code, description, message, explanation, severity_level, write_import_line_flag, abort_import_flag, update_user_id, update_timestamp)
VALUES ('GEORECON', '2', 'Lowest Rate', NULL, NULL, NULL, NULL, NULL, USER, SYS_EXTRACT_UTC(SYSTIMESTAMP)); 
INSERT INTO tb_list_code (code_type_code, code_code, description, message, explanation, severity_level, write_import_line_flag, abort_import_flag, update_user_id, update_timestamp)
VALUES ('GEORECON', '3', 'Highest Jurisdictional Level', NULL, NULL, NULL, NULL, NULL, USER, SYS_EXTRACT_UTC(SYSTIMESTAMP)); 
INSERT INTO tb_list_code (code_type_code, code_code, description, message, explanation, severity_level, write_import_line_flag, abort_import_flag, update_user_id, update_timestamp)
VALUES ('GEORECON', '4', 'Lowest Jurisdictional Level', NULL, NULL, NULL, NULL, NULL, USER, SYS_EXTRACT_UTC(SYSTIMESTAMP)); 
INSERT INTO tb_list_code (code_type_code, code_code, description, message, explanation, severity_level, write_import_line_flag, abort_import_flag, update_user_id, update_timestamp)
VALUES ('GEORECON', '5', 'Highest Rate/Highest Jurisdictional Level', NULL, NULL, NULL, NULL, NULL, USER, SYS_EXTRACT_UTC(SYSTIMESTAMP)); 
INSERT INTO tb_list_code (code_type_code, code_code, description, message, explanation, severity_level, write_import_line_flag, abort_import_flag, update_user_id, update_timestamp)
VALUES ('GEORECON', '6', 'Highest Rate/Lowest Jurisdictional Level', NULL, NULL, NULL, NULL, NULL, USER, SYS_EXTRACT_UTC(SYSTIMESTAMP)); 
INSERT INTO tb_list_code (code_type_code, code_code, description, message, explanation, severity_level, write_import_line_flag, abort_import_flag, update_user_id, update_timestamp)
VALUES ('GEORECON', '7', 'Lowest Rate/Highest Jurisdictional Level', NULL, NULL, NULL, NULL, NULL, USER, SYS_EXTRACT_UTC(SYSTIMESTAMP)); 
INSERT INTO tb_list_code (code_type_code, code_code, description, message, explanation, severity_level, write_import_line_flag, abort_import_flag, update_user_id, update_timestamp)
VALUES ('GEORECON', '8', 'Lowest Rate/Lowest Jurisdictional Level', NULL, NULL, NULL, NULL, NULL, USER, SYS_EXTRACT_UTC(SYSTIMESTAMP)); 
INSERT INTO tb_list_code (code_type_code, code_code, description, message, explanation, severity_level, write_import_line_flag, abort_import_flag, update_user_id, update_timestamp)
VALUES ('GEORECON', '9', 'Highest Jurisdictional Level/Highest Rate', NULL, NULL, NULL, NULL, NULL, USER, SYS_EXTRACT_UTC(SYSTIMESTAMP)); 
INSERT INTO tb_list_code (code_type_code, code_code, description, message, explanation, severity_level, write_import_line_flag, abort_import_flag, update_user_id, update_timestamp)
VALUES ('GEORECON', '10', 'Highest Jurisdictional Level/Lowest Rate', NULL, NULL, NULL, NULL, NULL, USER, SYS_EXTRACT_UTC(SYSTIMESTAMP)); 
INSERT INTO tb_list_code (code_type_code, code_code, description, message, explanation, severity_level, write_import_line_flag, abort_import_flag, update_user_id, update_timestamp)
VALUES ('GEORECON', '11', 'Lowest Jurisdictional Level/Highest Rate', NULL, NULL, NULL, NULL, NULL, USER, SYS_EXTRACT_UTC(SYSTIMESTAMP)); 
INSERT INTO tb_list_code (code_type_code, code_code, description, message, explanation, severity_level, write_import_line_flag, abort_import_flag, update_user_id, update_timestamp)
VALUES ('GEORECON', '12', 'Lowest Jurisdictional Level/Lowest Rate', NULL, NULL, NULL, NULL, NULL, USER, SYS_EXTRACT_UTC(SYSTIMESTAMP)); 

INSERT INTO tb_list_code (code_type_code, code_code, description, message, explanation, severity_level, write_import_line_flag, abort_import_flag, update_user_id, update_timestamp)
VALUES ('*DEF', 'NEXUSIND', 'Nexus Indicators', NULL, NULL, NULL, NULL, NULL, USER, SYS_EXTRACT_UTC(SYSTIMESTAMP)); 
INSERT INTO tb_list_code (code_type_code, code_code, description, message, explanation, severity_level, write_import_line_flag, abort_import_flag, update_user_id, update_timestamp)
VALUES ('NEXUSIND', 'S', 'State', NULL, NULL, NULL, NULL, NULL, USER, SYS_EXTRACT_UTC(SYSTIMESTAMP)); 
INSERT INTO tb_list_code (code_type_code, code_code, description, message, explanation, severity_level, write_import_line_flag, abort_import_flag, update_user_id, update_timestamp)
VALUES ('NEXUSIND', 'I', 'Independant', NULL, NULL, NULL, NULL, NULL, USER, SYS_EXTRACT_UTC(SYSTIMESTAMP)); 
INSERT INTO tb_list_code (code_type_code, code_code, description, message, explanation, severity_level, write_import_line_flag, abort_import_flag, update_user_id, update_timestamp)
VALUES ('NEXUSIND', 'N', 'No Tax', NULL, NULL, NULL, NULL, NULL, USER, SYS_EXTRACT_UTC(SYSTIMESTAMP)); 

INSERT INTO tb_list_code (code_type_code, code_code, description, message, explanation, severity_level, write_import_line_flag, abort_import_flag, update_user_id, update_timestamp)
VALUES ('*DEF', 'NEXUSTYPE', 'Nexus Types', NULL, NULL, NULL, NULL, NULL, USER, SYS_EXTRACT_UTC(SYSTIMESTAMP)); 
INSERT INTO tb_list_code (code_type_code, code_code, description, message, explanation, severity_level, write_import_line_flag, abort_import_flag, update_user_id, update_timestamp)
VALUES ('NEXUSTYPE', 'E', 'Established', NULL, NULL, NULL, NULL, NULL, USER, SYS_EXTRACT_UTC(SYSTIMESTAMP)); 
INSERT INTO tb_list_code (code_type_code, code_code, description, message, explanation, severity_level, write_import_line_flag, abort_import_flag, update_user_id, update_timestamp)
VALUES ('NEXUSTYPE', 'V', 'Voluntary', NULL, NULL, NULL, NULL, NULL, USER, SYS_EXTRACT_UTC(SYSTIMESTAMP)); 

INSERT INTO tb_list_code (code_type_code, code_code, description, message, explanation, severity_level, write_import_line_flag, abort_import_flag, update_user_id, update_timestamp)
VALUES ('*DEF', 'AUTONEXSRC', 'Auto Create Nexus Definition Source', NULL, NULL, NULL, NULL, NULL, USER, SYS_EXTRACT_UTC(SYSTIMESTAMP)); 
INSERT INTO tb_list_code (code_type_code, code_code, description, message, explanation, severity_level, write_import_line_flag, abort_import_flag, update_user_id, update_timestamp)
VALUES ('AUTONEXSRC', 'EN', 'Entity', NULL, NULL, NULL, NULL, NULL, USER, SYS_EXTRACT_UTC(SYSTIMESTAMP)); 
INSERT INTO tb_list_code (code_type_code, code_code, description, message, explanation, severity_level, write_import_line_flag, abort_import_flag, update_user_id, update_timestamp)
VALUES ('AUTONEXSRC', 'LM', 'Location Matrix', NULL, NULL, NULL, NULL, NULL, USER, SYS_EXTRACT_UTC(SYSTIMESTAMP)); 

INSERT INTO tb_list_code (code_type_code, code_code, description, message, explanation, severity_level, write_import_line_flag, abort_import_flag, update_user_id, update_timestamp)
VALUES ('*DEF', 'REGTYPE', 'Registration Types', NULL, NULL, NULL, NULL, NULL, USER, SYS_EXTRACT_UTC(SYSTIMESTAMP)); 
INSERT INTO tb_list_code (code_type_code, code_code, description, message, explanation, severity_level, write_import_line_flag, abort_import_flag, update_user_id, update_timestamp)
VALUES ('REGTYPE', '0', 'Generic', NULL, NULL, NULL, NULL, NULL, USER, SYS_EXTRACT_UTC(SYSTIMESTAMP)); 
INSERT INTO tb_list_code (code_type_code, code_code, description, message, explanation, severity_level, write_import_line_flag, abort_import_flag, update_user_id, update_timestamp)
VALUES ('REGTYPE', '1', 'Sales Tax', NULL, NULL, NULL, NULL, NULL, USER, SYS_EXTRACT_UTC(SYSTIMESTAMP)); 
INSERT INTO tb_list_code (code_type_code, code_code, description, message, explanation, severity_level, write_import_line_flag, abort_import_flag, update_user_id, update_timestamp)
VALUES ('REGTYPE', '2', 'Vendor''s Use Tax', NULL, NULL, NULL, NULL, NULL, USER, SYS_EXTRACT_UTC(SYSTIMESTAMP)); 
INSERT INTO tb_list_code (code_type_code, code_code, description, message, explanation, severity_level, write_import_line_flag, abort_import_flag, update_user_id, update_timestamp)
VALUES ('REGTYPE', '3', 'Consumer''s Use Tax', NULL, NULL, NULL, NULL, NULL, USER, SYS_EXTRACT_UTC(SYSTIMESTAMP)); 


-- tb_menu
INSERT INTO tb_menu(menu_code, main_menu_code, menu_sequence, option_name, command_type, command_line, key_values, panel_class_name, update_user_id, update_timestamp)
VALUES('m_entities', 'm_maintenance', 10, 'Entities', 'OPEN', '', '', '', USER, SYS_EXTRACT_UTC(SYSTIMESTAMP));

UPDATE tb_menu
   SET option_name='Roles & Users',
       update_user_id = USER,
       update_timestamp = SYS_EXTRACT_UTC(SYSTIMESTAMP)
 WHERE menu_code='m_securitymodules';


-- SP_TB_LOCATION_MTRX_MASTR_LIST 
create or replace
FUNCTION SP_TB_LOCATION_MTRX_MASTR_LIST (
p_where_clause_token IN VARCHAR2,
p_order_by_token IN VARCHAR2,
p_start_index IN INTEGER,
p_max_rows IN INTEGER
) RETURN SYS_REFCURSOR
AS
l_cursor sys_refcursor;
BEGIN
OPEN l_cursor FOR
'SELECT * FROM ( SELECT B.*, rownum rnum
FROM (
SELECT DISTINCT
DECODE(TB_LOCATION_MATRIX.ENTITY_ID,NULL,-1,TB_LOCATION_MATRIX.ENTITY_ID), TB_LOCATION_MATRIX.DRIVER_01, TB_LOCATION_MATRIX.DRIVER_02, TB_LOCATION_MATRIX.DRIVER_03,TB_LOCATION_MATRIX.DRIVER_04, TB_LOCATION_MATRIX.DRIVER_05, TB_LOCATION_MATRIX.DRIVER_06,TB_LOCATION_MATRIX.DRIVER_07, TB_LOCATION_MATRIX.DRIVER_08, TB_LOCATION_MATRIX.DRIVER_09, TB_LOCATION_MATRIX.DRIVER_10, TB_LOCATION_MATRIX.BINARY_WEIGHT,TB_LOCATION_MATRIX.DEFAULT_FLAG, TB_LOCATION_MATRIX.DEFAULT_BINARY_WEIGHT
FROM TB_LOCATION_MATRIX left outer join TB_JURISDICTION
on (TB_LOCATION_MATRIX.JURISDICTION_ID = TB_JURISDICTION.JURISDICTION_ID)
WHERE 1=1 '||p_where_clause_token||' '||p_order_by_token||' ) B WHERE rownum <= '|| (p_start_index + p_max_rows) ||' ) WHERE rnum > '||p_start_index; 
RETURN l_cursor; 
END;


COMMIT;

SHOW ERRORS
SPOOL OFF 
