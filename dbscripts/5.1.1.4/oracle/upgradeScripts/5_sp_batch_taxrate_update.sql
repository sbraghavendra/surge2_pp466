CREATE OR REPLACE PROCEDURE Sp_Batch_Taxrate_Update(an_batch_id NUMBER)
/* ************************************************************************************************/
/* Object Type/Name: Stored Procedure - sp_batch_taxrate_update                                   */
/* Author:           Michael B. Fuller                                                            */
/* Date:             a long time ago                                                              */
/* Description:      process a batch of tax rate updates                                          */
/* Arguments:        an_batch_id number                                                           */
/* Returns:          None                                                                         */
/* ************************************************************************************************/
/* Maintenance History:                                                                           */
/* Scan  Author       Date     Release    Description                                  Ticket #   */
/* ----- ---------- ---------- ---------- -------------------------------------------- ---------- */
/* MBF01 M. Fuller  01/07/2010 5.1.1.1    Add code to get next sequences               4597       */
/* MBF02 M. Fuller  09/10/2010 5.1.3.4    Initialize fields not in bcp to zero.        73         */
/* MBF03 M. Fuller  04/29/2011            Include Country fields                       1346       */
/* MBF04 M. Fuller  05/05/2011            Replace ROWNUM with row count                           */
/* ************************************************************************************************/
IS
-- Table defined variables
   v_batch_status_code             tb_batch.batch_status_code%TYPE                   := NULL;
   v_error_sev_code                tb_batch.error_sev_code%TYPE                      := ' ';
   v_starting_sequence             tb_batch.start_row%TYPE                           := 0;
   v_process_count                 tb_batch.start_row%TYPE                           := 0;
   v_processed_rows                tb_batch.start_row%TYPE                           := 0;
   v_batch_error_log_id            tb_batch_error_log.batch_error_log_id%TYPE        := 0;
   v_severity_level                tb_list_code.severity_level%TYPE                  := ' ';
   v_write_import_line_flag        tb_list_code.write_import_line_flag%TYPE          := '1';
   v_abort_import_flag             tb_list_code.abort_import_flag%TYPE               := '0';
   v_jurisdiction_id               tb_jurisdiction.jurisdiction_id%TYPE              := NULL;
   v_jur_custom_flag               tb_jurisdiction.custom_flag%TYPE                  := NULL;
   v_jurisdiction_taxrate_id       tb_jurisdiction_taxrate.jurisdiction_taxrate_id%TYPE := NULL;
   v_rate_custom_flag              tb_jurisdiction_taxrate.custom_flag%TYPE          := NULL;
   v_rate_modified_flag            tb_jurisdiction_taxrate.modified_flag%TYPE        := NULL;
   v_sysdate                       tb_bcp_transactions.load_timestamp%TYPE           := SYS_EXTRACT_UTC(SYSTIMESTAMP);

-- TEMPORARY --
   v_state_use_tier2_rate          tb_jurisdiction_taxrate.state_use_tier2_rate%TYPE   := 0;
   v_state_use_tier3_rate          tb_jurisdiction_taxrate.state_use_tier3_rate%TYPE   := 0;
   v_state_split_amount            tb_jurisdiction_taxrate.state_split_amount%TYPE     := 0;
   v_state_tier2_min_amount        tb_jurisdiction_taxrate.state_tier2_min_amount%TYPE := 0;
   v_state_tier2_max_amount        tb_jurisdiction_taxrate.state_tier2_max_amount%TYPE := 0;
   v_state_maxtax_amount           tb_jurisdiction_taxrate.state_maxtax_amount%TYPE    := 0;
-- TEMPORARY --

-- Program defined variables
   vc_taxrate_update_type          VARCHAR(10)                                       := NULL;
   vi_release_number               INTEGER                                           := 0;
   vi_sessions                     INTEGER                                           := 0;
   vd_update_date                  DATE                                              := NULL;
   vi_cursor_id                    INTEGER                                           := 0;
   vi_error_count                  INTEGER                                           := 0;

-- Define BCP Jurisdiction TaxRate Updates Cursor (tb_bcp_jurisdiction_taxrate)
   CURSOR bcp_juris_taxrate_cursor
   IS
      SELECT
         batch_id,
         geocode,
         zip,
         state,
         county,
         city,
         zipplus4,
         in_out,
         state_sales_rate,
         state_use_rate,
         county_sales_rate,
         county_use_rate,
         county_local_sales_rate,
         county_local_use_rate,
         county_split_amount,
         county_maxtax_amount,
         county_single_flag,
         county_default_flag,
         city_sales_rate,
         city_use_rate,
         city_local_sales_rate,
         city_local_use_rate,
         city_split_amount,
         city_split_sales_rate,
         city_split_use_rate,
         city_single_flag,
         city_default_flag,
         effective_date,
         country,
         country_sales_rate,
         country_use_rate
    FROM tb_bcp_jurisdiction_taxrate
   WHERE batch_id = an_batch_id;
   bcp                             bcp_juris_taxrate_cursor%ROWTYPE;

-- Define Exceptions
   e_badread                       EXCEPTION;
   e_badupdate                     EXCEPTION;
   e_badwrite                      EXCEPTION;
   e_wrongdata                     EXCEPTION;
   e_abort                         EXCEPTION;
   e_halt                          EXCEPTION;

-- ****** Program starts ****** --------------------------------------------------------------------
BEGIN
   -- Confirm batch exists and is flagged for processing
   BEGIN
      SELECT batch_status_code
        INTO v_batch_status_code
        FROM tb_batch
       WHERE batch_id = an_batch_id;
      IF SQLCODE != 0 THEN
         RAISE e_badread;
      ELSIF v_batch_status_code != 'FTR' THEN
         RAISE e_wrongdata;
      END IF;
   EXCEPTION
      WHEN NO_DATA_FOUND OR e_badread THEN
         vi_error_count := vi_error_count + 1;
         Sp_Geterrorcode('TR8', an_batch_id, 'TR', v_sysdate, v_severity_level, v_write_import_line_flag, v_abort_import_flag);
         IF v_severity_level > v_error_sev_code THEN
            v_error_sev_code := v_severity_level;
         END IF;
      WHEN e_wrongdata THEN
         vi_error_count := vi_error_count + 1;
         Sp_Geterrorcode('TR9', an_batch_id, 'TR', v_sysdate, v_severity_level, v_write_import_line_flag, v_abort_import_flag);
         IF v_severity_level > v_error_sev_code THEN
            v_error_sev_code := v_severity_level;
         END IF;
   END;
   -- Abort Procedure?
   IF v_abort_import_flag = '1' THEN
      v_batch_status_code := 'ABTR';
      RAISE e_abort;
   END IF;

   -- Check for Users in STS??
   /* SELECT count(*)
     INTO vi_sessions
     FROM v$session
    WHERE audsid <> 0
      AND audsid NOT IN
         (SELECT sys_context('USERENV','SESSIONID') FROM DUAL);
   IF vi_sessions > 0 Then
      RAISE e_halt;
   END IF; */

   -- Get starting sequence no.
   BEGIN
      SELECT last_number
        INTO v_starting_sequence
        FROM user_sequences
       WHERE sequence_name = 'SQ_TB_PROCESS_COUNT';
   EXCEPTION
      WHEN OTHERS THEN
         NULL;
   END;

   -- Get batch information
   SELECT UPPER(VC02),
          NU01,
          TS01
     INTO vc_taxrate_update_type,
          vi_release_number,
          vd_update_date
     FROM tb_batch
    WHERE batch_id = an_batch_id;

   -- Update batch as processing
   BEGIN
      UPDATE tb_batch
         SET batch_status_code = 'XTR',
             error_sev_code = '',
             start_row = v_starting_sequence,
             actual_start_timestamp = v_sysdate
       WHERE batch_id = an_batch_id;
      IF SQLCODE != 0 THEN
         RAISE e_badupdate;
      END IF;
   EXCEPTION
      WHEN e_badupdate THEN
         vi_error_count := vi_error_count + 1;
         Sp_Geterrorcode('TR1', an_batch_id, 'TR', v_sysdate, v_severity_level, v_write_import_line_flag, v_abort_import_flag);
         IF v_severity_level > v_error_sev_code THEN
            v_error_sev_code := v_severity_level;
         END IF;
   END;
   -- Abort Procedure?
   IF v_abort_import_flag = '1' THEN
      v_batch_status_code := 'ABTR';
      RAISE e_abort;
   END IF;

   COMMIT;

   -- ****** Read and Process Transactions ****** -----------------------------------------
   OPEN bcp_juris_taxrate_cursor;
   LOOP
      FETCH bcp_juris_taxrate_cursor INTO bcp;
      EXIT WHEN bcp_juris_taxrate_cursor%NOTFOUND;

      -- Increment Process Count
      SELECT sq_tb_process_count.NEXTVAL
        INTO v_process_count
        FROM DUAL;
      v_processed_rows := v_processed_rows + 1;

      -- Find jurisdiction
      BEGIN
         SELECT tb_jurisdiction.jurisdiction_id,
                tb_jurisdiction.custom_flag
           INTO v_jurisdiction_id,
                v_jur_custom_flag
           FROM tb_jurisdiction
          WHERE ( tb_jurisdiction.geocode = bcp.geocode )
            AND ( tb_jurisdiction.state = bcp.state )
            AND ( tb_jurisdiction.county = bcp.county )
            AND ( tb_jurisdiction.city = bcp.city )
            AND ( tb_jurisdiction.zip = bcp.zip )
            AND ( tb_jurisdiction.country = bcp.country );
      EXCEPTION
         WHEN OTHERS THEN
            v_jurisdiction_id := 0;
            v_jur_custom_flag := '0';
      END;

      -- If it does not exist, add it
      IF v_jurisdiction_id IS NULL OR v_jurisdiction_id = 0 THEN
         -- Insert new jurisdiction
         SELECT sq_tb_jurisdiction_id.NEXTVAL
           INTO v_jurisdiction_id
           FROM DUAL;

         INSERT INTO tb_jurisdiction(
            jurisdiction_id,
            geocode,
            state,
            county,
            city,
            zip,
            zipplus4,
            in_out,
            custom_flag,
            description,
            client_geocode,
            comp_geocode,
            country )
         VALUES(
            v_jurisdiction_id,
            bcp.geocode,
            bcp.state,
            bcp.county,
            bcp.city,
            bcp.zip,
            bcp.zipplus4,
            bcp.in_out,
            '0',
            'Second Decimal Supplied Jurisdiction/Tax Rate',
            NULL,
            NULL,
            bcp.country );
         -- Get last inserted jurisdiction id
         --SELECT MAX(jurisdiction_id)
         --  INTO v_jurisdiction_id
         --  FROM TB_JURISDICTION;
      ELSE
         -- If it does exist and is custom then write errog log warning
         IF v_jur_custom_flag = '1' THEN
            v_error_sev_code := '10';
            SELECT sq_tb_batch_error_log_id.NEXTVAL
              INTO v_batch_error_log_id
              FROM DUAL;

            INSERT INTO tb_batch_error_log (
               batch_error_log_id,
               batch_id,
               process_type,
               process_timestamp,
               row_no,
               column_no,
               error_def_code,
               import_header_column,
               import_column_value,
               trans_dtl_column_name,
               trans_dtl_datatype,
               import_row )
            VALUES (
               v_batch_error_log_id,
               bcp.batch_id,
               'TR',
               v_sysdate,
               v_processed_rows,
               0,
               'TR1',
               'GeoCode|State|County|City|Zip|Country',
               bcp.geocode || '|' || bcp.state || '|' || bcp.county || '|' || bcp.city || '|' || bcp.zip || '|' || bcp.country,
               'n/a',
               'n/a',
               'n/a' );
         END IF;
      END IF;

      -- Find jurisdiction taxrate
      BEGIN
         SELECT tb_jurisdiction_taxrate.jurisdiction_taxrate_id,
                tb_jurisdiction_taxrate.custom_flag,
                tb_jurisdiction_taxrate.modified_flag
           INTO v_jurisdiction_taxrate_id,
                v_rate_custom_flag,
                v_rate_modified_flag
           FROM tb_jurisdiction_taxrate
          WHERE ( tb_jurisdiction_taxrate.jurisdiction_id = v_jurisdiction_id )
            AND ( tb_jurisdiction_taxrate.effective_date = bcp.effective_date )
            AND ( tb_jurisdiction_taxrate.measure_type_code = '0' );
      EXCEPTION
         WHEN OTHERS THEN
            v_jurisdiction_taxrate_id := 0;
            v_rate_custom_flag := '0';
            v_rate_modified_flag := '0';
      END;

      -- If it does not exist, add it
      IF v_jurisdiction_taxrate_id IS NULL OR v_jurisdiction_taxrate_id = 0 THEN
         SELECT sq_tb_jurisdiction_taxrate_id.NEXTVAL
           INTO v_jurisdiction_taxrate_id
           FROM DUAL;

      -- TEMPORARY --
         -- Initialize Tier Rates
         v_state_use_tier2_rate := 0;
         v_state_use_tier3_rate := 0;
         v_state_split_amount := 0;
         v_state_tier2_min_amount := 0;
         v_state_tier2_max_amount := 0;
         v_state_maxtax_amount := 0;
         -- Check for US/TN rates
         IF bcp.country = 'US' AND bcp.state = 'TN' AND bcp.county_split_amount = 1600 THEN
            v_state_use_tier2_rate := 0.0275;
            v_state_split_amount := 999999999;
            v_state_tier2_min_amount := 1600;
            v_state_tier2_max_amount := 3200;
         END IF;
      -- TEMPORARY --

         INSERT INTO tb_jurisdiction_taxrate (
            jurisdiction_taxrate_id,
            jurisdiction_id,
            measure_type_code,
            effective_date,
            expiration_date,
            custom_flag,
            modified_flag,
            state_sales_rate,
            state_use_rate,
            state_use_tier2_rate,
            state_use_tier3_rate,
            state_split_amount,
            state_tier2_min_amount,
            state_tier2_max_amount,
            state_maxtax_amount,
            county_sales_rate,
            county_use_rate,
            county_local_sales_rate,
            county_local_use_rate,
            county_split_amount,
            county_maxtax_amount,
            county_single_flag,
            county_default_flag,
            city_sales_rate,
            city_use_rate,
            city_local_sales_rate,
            city_local_use_rate,
            city_split_amount,
            city_split_sales_rate,
            city_split_use_rate,
            city_single_flag,
            city_default_flag,
            country_sales_rate,
            country_use_rate )
         VALUES (
            v_jurisdiction_taxrate_id,
            v_jurisdiction_id,
            '0',
            bcp.effective_date,
            TO_DATE('12/31/9999', 'mm/dd/yyyy'),
            '0',
            '0',
            bcp.state_sales_rate,
            bcp.state_use_rate,
            v_state_use_tier2_rate,
            v_state_use_tier3_rate,
            v_state_split_amount,
            v_state_tier2_min_amount,
            v_state_tier2_max_amount,
            v_state_maxtax_amount,
            bcp.county_sales_rate,
            bcp.county_use_rate,
            bcp.county_local_sales_rate,
            bcp.county_local_use_rate,
            bcp.county_split_amount,
            bcp.county_maxtax_amount,
            bcp.county_single_flag,
            bcp.county_default_flag,
            bcp.city_sales_rate,
            bcp.city_use_rate,
            bcp.city_local_sales_rate,
            bcp.city_local_use_rate,
            bcp.city_split_amount,
            bcp.city_split_sales_rate,
            bcp.city_split_use_rate,
            bcp.city_single_flag,
            bcp.city_default_flag,
            bcp.country_sales_rate,
            bcp.country_use_rate );
      ELSE
         -- If it does exist and jurisdiction is not custom but rate is custom (modified or not) then write errog log warning
         IF (( v_jur_custom_flag IS NULL ) OR ( v_jur_custom_flag = '0' )) AND
            ( v_rate_custom_flag = '1' ) THEN
            v_error_sev_code := '10';
            SELECT sq_tb_batch_error_log_id.NEXTVAL
              INTO v_batch_error_log_id
              FROM DUAL;

            INSERT INTO tb_batch_error_log (
               batch_error_log_id,
               batch_id,
               process_type,
               process_timestamp,
               row_no,
               column_no,
               error_def_code,
               import_header_column,
               import_column_value,
               trans_dtl_column_name,
               trans_dtl_datatype,
               import_row )
            VALUES (
               v_batch_error_log_id,
               bcp.batch_id,
               'TR',
               v_sysdate,
               v_processed_rows,
               0,
               'TR2',
               'GeoCode|State|County|City|Zip|Country|Eff. Date',
               bcp.geocode || '|' || bcp.state || '|' || bcp.county || '|' || bcp.city || '|' || bcp.zip || '|' || bcp.country || '|' || TO_CHAR(bcp.effective_date, 'mm/dd/yyyy'),
               'n/a',
               'n/a',
               'n/a' );
         ELSE
            -- If it does exist and jurisdiction is not custom and rate is not custom but rate is modified then write error log warning
            IF (( v_jur_custom_flag IS NULL ) OR ( v_jur_custom_flag = '0' )) AND
               (( v_rate_custom_flag IS NULL ) OR ( v_rate_custom_flag = '0' )) AND
               ( v_rate_modified_flag = '1' ) THEN
               v_error_sev_code := '10';
               SELECT sq_tb_batch_error_log_id.NEXTVAL
                 INTO v_batch_error_log_id
                 FROM DUAL;

               INSERT INTO tb_batch_error_log (
                  batch_error_log_id,
                  batch_id,
                  process_type,
                  process_timestamp,
                  row_no,
                  column_no,
                  error_def_code,
                  import_header_column,
                  import_column_value,
                  trans_dtl_column_name,
                  trans_dtl_datatype,
                  import_row )
               VALUES (
                  v_batch_error_log_id,
                  bcp.batch_id,
                  'TR',
                  v_sysdate,
                  v_processed_rows,
                  0,
                  'TR3',
                  'GeoCode|State|County|City|Zip|Country|Eff. Date',
                  bcp.geocode || '|' || bcp.state || '|' || bcp.county || '|' || bcp.city || '|' || bcp.zip || '|' || bcp.country || '|' || TO_CHAR(bcp.effective_date, 'mm/dd/yyyy'),
                  'n/a',
                  'n/a',
                  'n/a' );
            END IF;
         END IF;
         -- Update TaxRate
         UPDATE tb_jurisdiction_taxrate SET
            tb_jurisdiction_taxrate.modified_flag = 0,
            tb_jurisdiction_taxrate.state_sales_rate = bcp.state_sales_rate,
            tb_jurisdiction_taxrate.state_use_rate =  bcp.state_use_rate,
            tb_jurisdiction_taxrate.county_sales_rate = bcp.county_sales_rate,
            tb_jurisdiction_taxrate.county_use_rate = bcp.county_use_rate,
            tb_jurisdiction_taxrate.county_local_sales_rate = bcp.county_local_sales_rate,
            tb_jurisdiction_taxrate.county_local_use_rate = bcp.county_local_use_rate,
            tb_jurisdiction_taxrate.county_split_amount = bcp.county_split_amount,
            tb_jurisdiction_taxrate.county_maxtax_amount = bcp.county_maxtax_amount,
            tb_jurisdiction_taxrate.county_single_flag = bcp.county_single_flag,
            tb_jurisdiction_taxrate.county_default_flag = bcp.county_default_flag,
            tb_jurisdiction_taxrate.city_sales_rate = bcp.city_sales_rate,
            tb_jurisdiction_taxrate.city_use_rate = bcp.city_use_rate,
            tb_jurisdiction_taxrate.city_local_sales_rate = bcp.city_local_sales_rate,
            tb_jurisdiction_taxrate.city_local_use_rate = bcp.city_local_use_rate,
            tb_jurisdiction_taxrate.city_split_amount = bcp.city_split_amount,
            tb_jurisdiction_taxrate.city_split_sales_rate = bcp.city_split_sales_rate,
            tb_jurisdiction_taxrate.city_split_use_rate = bcp.city_split_use_rate,
            tb_jurisdiction_taxrate.city_single_flag = bcp.city_single_flag,
            tb_jurisdiction_taxrate.city_default_flag = bcp.city_default_flag,
            tb_jurisdiction_taxrate.country_sales_rate = bcp.country_sales_rate,
            tb_jurisdiction_taxrate.country_use_rate = bcp.country_use_rate
         WHERE tb_jurisdiction_taxrate.jurisdiction_taxrate_id = v_jurisdiction_taxrate_id;
      END IF;
   END LOOP;
   CLOSE bcp_juris_taxrate_cursor;

   -- Remove Batch from BCP Transactions Table
   DELETE
     FROM tb_bcp_jurisdiction_taxrate
    WHERE batch_id = an_batch_id;

   --  Update Options table with last month/year updated
   UPDATE tb_option
      SET value = TO_CHAR(vd_update_date, 'yyyy_mm')
    WHERE option_code = 'LASTTAXRATEDATEUPDATE' AND
          option_type_code = 'SYSTEM' AND
          user_code = 'SYSTEM';

   -- Update Options table with last release number
   UPDATE tb_option
      SET value = vi_release_number
    WHERE option_code = 'LASTTAXRATERELUPDATE' AND
          option_type_code = 'SYSTEM' AND
          user_code = 'SYSTEM';

   -- Update batch with final totals
   UPDATE tb_batch
      SET batch_status_code = 'TR',
          error_sev_code = v_error_sev_code,
          processed_rows = v_processed_rows,
          actual_end_timestamp = SYS_EXTRACT_UTC(SYSTIMESTAMP)
    WHERE batch_id = an_batch_id;

   -- commit all updates
   COMMIT;

EXCEPTION
   WHEN e_abort THEN
      -- Update batch with error codes
      UPDATE tb_batch
         SET batch_status_code = v_batch_status_code,
             error_sev_code = v_error_sev_code,
             actual_end_timestamp = SYS_EXTRACT_UTC(SYSTIMESTAMP)
       WHERE batch_id = an_batch_id;
      COMMIT;
   WHEN e_halt THEN
      NULL;
END;
/
