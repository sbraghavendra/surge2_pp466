create or replace
PROCEDURE sp_tb_transaction_detail_b_u (
   p_transaction_detail_id     IN     tb_transaction_detail.transaction_detail_id%TYPE)
/* ************************************************************************************************/
/* Object Type/Name: Stored Procedure (Before Update) - tb_transaction_detail_b_u                 */
/* Author:           Michael B. Fuller                                                            */
/* Date:             a long time ago                                                              */
/* Description:      A transaction has been changed                                               */
/* Arguments:        p_transaction_detail_id(tb_transaction_detail.transaction_detail_id%TYPE)    */
/* Returns:          None                                                                         */
/* ************************************************************************************************/
/* Maintenance History:                                                                           */
/* Scan  Author       Date     Release    Description                                  Ticket #   */
/* ----- ---------- ---------- ---------- -------------------------------------------- ---------- */
/*       M. Fuller  06/02/2006 3.3.5.0    Fix processing an allocated line - Temporary 871        */
/*       M. Fuller  01/04/2007 3.3.5.1    Fix processing an allocated line - Permenant 871        */
/* MBF01 M. Fuller  02/09/2007 3.3.5.1    Correct allocated lines with no locn matrix id          */
/* MBF02 M. Fuller  08/13/2007 3.4.1.1    Add 5 Calc Tax Flags                         883        */
/* MBF03 M. Fuller  03/06/2008 3.4.2.1    Correct If/Then/Else's for "AO"              913        */
/* JFF   J. Franco  12/17/2008 x.x.x.x    Convert trigger to stored procedure          xxx        */
/* MBF04 M. Fuller  08/30/2011            Add TaxCode Rules changes                    17         */
/* ************************************************************************************************/
AS
-- Define Cursor Type
   TYPE cursor_type IS REF CURSOR;

-- Defire Row Type for tb_transaction_detail record
   l_tb_old_transaction            tb_transaction_detail%ROWTYPE;

-- Defire Row Type for tb_tmp_transaction_detail record
   l_tb_new_transaction            tb_tmp_transaction_detail%ROWTYPE;

-- Location Matrix Selection SQL      // MBF02
   vc_location_matrix_select       VARCHAR2(2000) :=
      'SELECT tb_location_matrix.location_matrix_id, ' ||
             'tb_location_matrix.jurisdiction_id, ' ||
             'tb_location_matrix.override_taxtype_code, ' ||
             'tb_jurisdiction.country, ' ||
             'tb_jurisdiction.state, ' ||
             'tb_jurisdiction.county, ' ||
             'tb_jurisdiction.city, ' ||
             'tb_jurisdiction.zip, ' ||
             'tb_jurisdiction.in_out ' ||
        'FROM tb_location_matrix, ' ||
             'tb_jurisdiction, ' ||
             'tb_tmp_transaction_detail ' ||
      'WHERE ( tb_tmp_transaction_detail.transaction_detail_id = :transaction_detail_id ) AND ' ||
            '( tb_location_matrix.default_flag = ''0'' AND tb_location_matrix.binary_weight > 0 ) AND ' ||
            '( tb_location_matrix.effective_date <= tb_tmp_transaction_detail.gl_date ) AND ( tb_location_matrix.expiration_date >= tb_tmp_transaction_detail.gl_date ) AND ' ||
            '( tb_location_matrix.jurisdiction_id = tb_jurisdiction.jurisdiction_id ) ';
   vc_location_matrix_where        VARCHAR2(28000) := '';
   vc_location_matrix_orderby      VARCHAR2(1000) :=
      'ORDER BY tb_location_matrix.binary_weight DESC, tb_location_matrix.significant_digits DESC, tb_location_matrix.effective_date DESC';
   vc_location_matrix_stmt         VARCHAR2 (31000);
   location_matrix_cursor          cursor_type;

-- Location Matrix Record TYPE
   TYPE location_matrix_record is RECORD (
      location_matrix_id           tb_location_matrix.location_matrix_id%TYPE,
      jurisdiction_id              tb_location_matrix.jurisdiction_id%TYPE,
      override_taxtype_code        tb_location_matrix.override_taxtype_code%TYPE,
      country                      tb_jurisdiction.country%TYPE,
      state                        tb_jurisdiction.state%TYPE,
      county                       tb_jurisdiction.county%TYPE,
      city                         tb_jurisdiction.city%TYPE,
      zip                          tb_jurisdiction.zip%TYPE,
      in_out                       tb_jurisdiction.in_out%TYPE );
   location_matrix                 location_matrix_record;

-- Tax Matrix Selection SQL
   vc_tax_matrix_select            VARCHAR2(2000) :=
      'SELECT tb_tax_matrix.tax_matrix_id, tb_tax_matrix.relation_sign, tb_tax_matrix.relation_amount, ' ||
             'tb_tax_matrix.then_hold_code_flag, tb_tax_matrix.else_hold_code_flag, ' ||
             'tb_tax_matrix.then_taxcode_code, tb_tax_matrix.else_taxcode_code ' ||
       'FROM tb_tax_matrix, ' ||
            'tb_tmp_transaction_detail ' ||
      'WHERE ( tb_tmp_transaction_detail.transaction_detail_id = :transaction_detail_id ) AND ' ||
            '( tb_tax_matrix.default_flag = ''0'' AND tb_tax_matrix.binary_weight > 0 ) AND ' ||
            '( tb_tax_matrix.effective_date <= tb_tmp_transaction_detail.gl_date ) AND ( tb_tax_matrix.expiration_date >= tb_tmp_transaction_detail.gl_date ) ';
   vc_tax_matrix_where             VARCHAR2(28000) := '';
   vc_tax_matrix_orderby           VARCHAR2(1000) :=
      'ORDER BY tb_tax_matrix.binary_weight DESC, tb_tax_matrix.significant_digits DESC, tb_tax_matrix.effective_date DESC';
   vc_tax_matrix_stmt              VARCHAR2 (31000);
   tax_matrix_cursor               cursor_type;

-- Tax Matrix Record TYPE
   TYPE tax_matrix_record is RECORD (
      tax_matrix_id                tb_tax_matrix.tax_matrix_id%TYPE,
      relation_sign                tb_tax_matrix.relation_sign%TYPE,
      relation_amount              tb_tax_matrix.relation_amount%TYPE,
      then_hold_code_flag          tb_tax_matrix.then_hold_code_flag%TYPE,
      else_hold_code_flag          tb_tax_matrix.else_hold_code_flag%TYPE,
      then_taxcode_code            tb_tax_matrix.then_taxcode_code%TYPE,
      else_taxcode_code            tb_tax_matrix.else_taxcode_code%TYPE);
   tax_matrix                      tax_matrix_record;

-- TacCode Detail Selection SQL
   vc_taxcode_detail_stmt           VARCHAR2(31000) :=
      'SELECT taxcode_detail_id, taxcode_type_code, override_taxtype_code, ratetype_code, taxable_threshold_amt, ' ||
             'tax_limitation_amt, cap_amt, base_change_pct, special_rate ' ||
        'FROM tb_taxcode_detail ' ||
       'WHERE tb_taxcode_detail.active_flag = ''1'' ' ||
         'AND tb_taxcode_detail.taxcode_code = :taxcode_code ' ||
         'AND tb_taxcode_detail.taxcode_country_code = :transaction_country_code ' ||
         'AND tb_taxcode_detail.taxcode_state_code = :transaction_state_code ' ||
         'AND tb_taxcode_detail.taxcode_county = :taxcode_county ' ||
         'AND tb_taxcode_detail.taxcode_city = :taxcode_city ' ||
         'AND tb_taxcode_detail.effective_date <= :gl_date ' ||
         'AND tb_taxcode_detail.expiration_date >= :gl_date ' ||
      'ORDER BY tb_taxcode_detail.effective_date DESC';
   taxcode_detail_cursor           cursor_type;

-- TaxCode Detail Record TYPE
   TYPE taxcode_detail_record is RECORD (
      taxcode_detail_id            tb_taxcode_detail.taxcode_detail_id%TYPE,
      taxcode_type_code            tb_taxcode_detail.taxcode_type_code%TYPE,
      override_taxtype_code        tb_taxcode_detail.override_taxtype_code%TYPE,
      ratetype_code                tb_taxcode_detail.ratetype_code%TYPE,
      taxable_threshold_amt        tb_taxcode_detail.taxable_threshold_amt%TYPE,
      tax_limitation_amt           tb_taxcode_detail.tax_limitation_amt%TYPE,
      cap_amt                      tb_taxcode_detail.cap_amt%TYPE,
      base_change_pct              tb_taxcode_detail.base_change_pct%TYPE,
      special_rate                 tb_taxcode_detail.special_rate%TYPE);
   taxcode_detail                  taxcode_detail_record;

-- Table defined variables
   v_transaction_county            tb_taxcode_detail.taxcode_county%TYPE             := NULL;
   v_transaction_city              tb_taxcode_detail.taxcode_city%TYPE               := NULL;
   v_less_code                     tb_taxcode_detail.taxcode_type_code%TYPE          := NULL;
   v_equal_code                    tb_taxcode_detail.taxcode_type_code%TYPE          := NULL;
   v_greater_code                  tb_taxcode_detail.taxcode_type_code%TYPE          := NULL;
   v_relation_code                 tb_taxcode_detail.taxcode_type_code%TYPE          := NULL;
   v_hold_code_flag                tb_tax_matrix.then_hold_code_flag%TYPE            := NULL;
   v_taxcode_code                  tb_taxcode_detail.taxcode_code%TYPE               := NULL;
   v_state_override_taxtype_code   tb_taxcode_detail.override_taxtype_code%TYPE      := NULL;
   v_state_ratetype_code           tb_taxcode_detail.ratetype_code%TYPE              := NULL;
   v_state_taxable_threshold_amt   tb_taxcode_detail.taxable_threshold_amt%TYPE      := 0;
   v_county_taxable_threshold_amt  tb_taxcode_detail.taxable_threshold_amt%TYPE      := 0;
   v_city_taxable_threshold_amt    tb_taxcode_detail.taxable_threshold_amt%TYPE      := 0;
   v_state_tax_limitation_amt      tb_taxcode_detail.tax_limitation_amt%TYPE         := 0;
   v_county_tax_limitation_amt     tb_taxcode_detail.tax_limitation_amt%TYPE         := 0;
   v_city_tax_limitation_amt       tb_taxcode_detail.tax_limitation_amt%TYPE         := 0;
   v_state_cap_amt                 tb_taxcode_detail.cap_amt%TYPE                    := 0;
   v_county_cap_amt                tb_taxcode_detail.cap_amt%TYPE                    := 0;
   v_city_cap_amt                  tb_taxcode_detail.cap_amt%TYPE                    := 0;
   v_state_base_change_pct         tb_taxcode_detail.base_change_pct%TYPE            := 0;
   v_county_base_change_pct        tb_taxcode_detail.base_change_pct%TYPE            := 0;
   v_city_base_change_pct          tb_taxcode_detail.base_change_pct%TYPE            := 0;
   v_state_special_rate            tb_taxcode_detail.special_rate%TYPE               := 0;
   v_county_special_rate           tb_taxcode_detail.special_rate%TYPE               := 0;
   v_city_special_rate             tb_taxcode_detail.special_rate%TYPE               := 0;
   --v_override_jurisdiction_id      tb_taxcode_detail.jurisdiction_id%TYPE            := 0;
   v_sysdate                       tb_transaction_detail.load_timestamp%TYPE         := SYS_EXTRACT_UTC(SYSTIMESTAMP);
   v_sysdate_plus                  tb_transaction_detail.load_timestamp%TYPE         := v_sysdate + (1/86400);

-- Program defined variables
   vn_fetch_rows                   NUMBER                                            := 0;
   vn_state_rows                   NUMBER                                            := 0;
   vn_county_rows                  NUMBER                                            := 0;
   vn_city_rows                    NUMBER                                            := 0;
   vc_state_driver_flag            CHAR(1)                                           := '0';
   vc_country_flag                 CHAR(1)                                           := '0';
   vc_state_flag                   CHAR(1)                                           := '0';
   vc_county_flag                  CHAR(1)                                           := '0';
   vc_county_local_flag            CHAR(1)                                           := '0';
   vc_city_flag                    CHAR(1)                                           := '0';
   vc_city_local_flag              CHAR(1)                                           := '0';

-- Define Exceptions
   e_badread                       EXCEPTION;

-- ****** Program starts ****** --------------------------------------------------------------------
BEGIN
   -- Populate row type with old transaction from tb_transaction_detail
   SELECT tb_transaction_detail.*
     INTO l_tb_old_transaction
     FROM tb_transaction_detail
    WHERE transaction_detail_id = p_transaction_detail_id;

   -- Populate row type with new transaction from tb_tmp_transaction_detail
   SELECT tb_tmp_transaction_detail.*
     INTO l_tb_new_transaction
     FROM tb_tmp_transaction_detail
    WHERE transaction_detail_id = p_transaction_detail_id;

   -- Don't process Suspended Transactions
   IF NOT (l_tb_old_transaction.transaction_ind = 'P' AND l_tb_new_transaction.transaction_ind = 'S') THEN
      -- Initialize fields
      l_tb_new_transaction.transaction_ind := NULL;
      l_tb_new_transaction.suspend_ind := NULL;
      l_tb_new_transaction.tb_calc_tax_amt := 0;
      l_tb_new_transaction.country_use_amount := 0;
      l_tb_new_transaction.state_taxable_amt := 0;
      l_tb_new_transaction.state_use_amount := 0;
      l_tb_new_transaction.state_use_tier2_amount := 0;
      l_tb_new_transaction.state_use_tier3_amount := 0;
      l_tb_new_transaction.county_taxable_amt := 0;
      l_tb_new_transaction.county_use_amount := 0;
      l_tb_new_transaction.county_local_use_amount := 0;
      l_tb_new_transaction.city_taxable_amt := 0;
      l_tb_new_transaction.city_use_amount := 0;
      l_tb_new_transaction.city_local_use_amount := 0;
      l_tb_new_transaction.country_use_rate := 0;
      l_tb_new_transaction.state_use_rate := 0;
      l_tb_new_transaction.state_use_tier2_rate := 0;
      l_tb_new_transaction.state_use_tier3_rate := 0;
      l_tb_new_transaction.state_split_amount := 0;
      l_tb_new_transaction.state_tier2_min_amount := 0;
      l_tb_new_transaction.state_tier2_max_amount := 0;
      l_tb_new_transaction.state_maxtax_amount := 0;
      l_tb_new_transaction.county_use_rate := 0;
      l_tb_new_transaction.county_local_use_rate := 0;
      l_tb_new_transaction.county_split_amount := 0;
      l_tb_new_transaction.county_maxtax_amount := 0;
      l_tb_new_transaction.county_single_flag := NULL;
      l_tb_new_transaction.county_default_flag := NULL;
      l_tb_new_transaction.city_use_rate := 0;
      l_tb_new_transaction.city_local_use_rate := 0;
      l_tb_new_transaction.city_split_amount := 0;
      l_tb_new_transaction.city_split_use_rate := 0;
      l_tb_new_transaction.city_single_flag := NULL;
      l_tb_new_transaction.city_default_flag := NULL;
      l_tb_new_transaction.combined_use_rate := 0;
      l_tb_new_transaction.taxtype_used_code := NULL;
      l_tb_new_transaction.update_user_id   := USER;
      l_tb_new_transaction.update_timestamp := v_sysdate;

      /* -- Search Location Matrix for matches -- */
      -- If jurisdiction_id is null or zero then search for location matrix
      IF l_tb_new_transaction.jurisdiction_id IS NULL OR l_tb_new_transaction.jurisdiction_id = 0 THEN
         -- New call to sp_gen_location_driver for MidTier Cleanup
         sp_gen_location_driver (
            p_generate_driver_reference   => 'N',
            p_an_batch_id                 => NULL,
            p_transaction_table_name      => 'tb_tmp_transaction_detail',
            p_location_table_name         => 'tb_location_matrix',
            p_vc_location_matrix_where    => vc_location_matrix_where);
         -- If no drivers found raise error, else create transaction detail sql statement
         IF vc_location_matrix_where IS NULL THEN
            NULL;
         ELSE
            vc_location_matrix_stmt := vc_location_matrix_select || vc_location_matrix_where || vc_location_matrix_orderby;
         END IF;
         -- Get Location Matrix line
         BEGIN
             OPEN location_matrix_cursor
              FOR vc_location_matrix_stmt
            USING p_transaction_detail_id;
            FETCH location_matrix_cursor
             INTO location_matrix;
            -- Rows found?
            IF location_matrix_cursor%FOUND THEN
               vn_fetch_rows := location_matrix_cursor%ROWCOUNT;
            ELSE
               vn_fetch_rows := 0;
            END IF;
            CLOSE location_matrix_cursor;
            IF SQLCODE != 0 THEN
               vn_fetch_rows := 0;
            END IF;
         END;
         -- Location Matrix line found
         IF vn_fetch_rows > 0 THEN
            l_tb_new_transaction.location_matrix_id := location_matrix.location_matrix_id;
            l_tb_new_transaction.jurisdiction_id := location_matrix.jurisdiction_id;
            l_tb_new_transaction.transaction_country_code := location_matrix.country;
            l_tb_new_transaction.transaction_state_code := location_matrix.state;
            v_transaction_county := location_matrix.county;
            v_transaction_city := location_matrix.city;
         ELSE
            -- Location Matrix line NOT found
            --l_tb_new_transaction.transaction_state_code := '*DEF';
            l_tb_new_transaction.transaction_ind := 'S';
            l_tb_new_transaction.suspend_ind := 'L';
         END IF;
      ELSE
         -- Transaction already has a jurisdiction_id - get jurisdiction info
         BEGIN
            SELECT county, city
              INTO v_transaction_county,
                   v_transaction_city
              FROM tb_jurisdiction
             WHERE tb_jurisdiction.jurisdiction_id = l_tb_new_transaction.jurisdiction_id;
         END;
         -- default other location matrix values
         location_matrix.override_taxtype_code := '*NO';
      END IF;

      -- Continue if not Suspended
      IF l_tb_new_transaction.transaction_ind IS NULL OR l_tb_new_transaction.transaction_ind <> 'S' THEN
         -- Search for Tax Matrix if taxcode_code and state_taxcode_detail_id are null or 0
         IF l_tb_new_transaction.taxcode_code IS NULL AND
           (l_tb_new_transaction.state_taxcode_detail_id IS NULL OR l_tb_new_transaction.state_taxcode_detail_id = 0) THEN
            vn_fetch_rows := 0;
            /* -- Search Tax Matrix for matches -- */
            -- New call to sp_gen_tax_driver for MidTier Cleanup
            sp_gen_tax_driver (
               p_generate_driver_reference   => 'N',
               p_an_batch_id                 => NULL,
               p_transaction_table_name      => 'tb_tmp_transaction_detail',
               p_tax_table_name              => 'tb_tax_matrix',
               p_vc_state_driver_flag        => vc_state_driver_flag,
               p_vc_tax_matrix_where         => vc_tax_matrix_where);
            -- If no drivers found raise error, else create location matrix sql statement
            IF vc_tax_matrix_where IS NULL THEN
               NULL;
            ELSE
               vc_tax_matrix_stmt := vc_tax_matrix_select || vc_tax_matrix_where || vc_tax_matrix_orderby;
            END IF;
            -- Get Tax Matrix line
            BEGIN
               IF vc_state_driver_flag = '1' THEN
                   OPEN tax_matrix_cursor
                    FOR vc_tax_matrix_stmt
                  USING p_transaction_detail_id, l_tb_new_transaction.transaction_state_code;
                  FETCH tax_matrix_cursor
                   INTO tax_matrix;
               ELSE
                   OPEN tax_matrix_cursor
                    FOR vc_tax_matrix_stmt
                  USING p_transaction_detail_id;
                  FETCH tax_matrix_cursor
                   INTO tax_matrix;
               END IF;
               -- Rows found?
               IF tax_matrix_cursor%FOUND THEN
                  vn_fetch_rows := tax_matrix_cursor%ROWCOUNT;
               ELSE
                  vn_fetch_rows := 0;
               END IF;
               CLOSE tax_matrix_cursor;
               IF SQLCODE != 0 THEN
                  vn_fetch_rows := 0;
               END IF;
            END;
            -- Tax Matrix line found
            IF vn_fetch_rows > 0 THEN
               l_tb_new_transaction.tax_matrix_id := tax_matrix.tax_matrix_id;
               -- Determine "Then" or "Else" Results
               SELECT DECODE(TRIM(tax_matrix.relation_sign),'na','then','=','else','<','then','<=','then','>','else','>=','else','<>','then','error') INTO v_less_code FROM DUAL;
               SELECT DECODE(TRIM(tax_matrix.relation_sign),'na','then','=','then','<','else','<=','then','>','else','>=','then','<>','else','error') INTO v_equal_code FROM DUAL;
               SELECT DECODE(TRIM(tax_matrix.relation_sign),'na','then','=','else','<','else','<=','else','>','then','>=','then','<>','then','error') INTO v_greater_code FROM DUAL;
               SELECT DECODE(SIGN(l_tb_new_transaction.gl_line_itm_dist_amt - NVL(tax_matrix.relation_amount,0)), -1, v_less_code, 0, v_equal_code, 1, v_greater_code, 'error') INTO v_relation_code FROM DUAL;
               IF v_relation_code = 'then' THEN
                  v_hold_code_flag := tax_matrix.then_hold_code_flag;
                  l_tb_new_transaction.taxcode_code := tax_matrix.then_taxcode_code;
                  --v_override_jurisdiction_id := tax_matrix.then_jurisdiction_id;
               ELSIF v_relation_code = 'else' THEN
                  v_hold_code_flag := tax_matrix.else_hold_code_flag;
                  l_tb_new_transaction.taxcode_code := tax_matrix.else_taxcode_code;
                  --v_override_jurisdiction_id := tax_matrix.else_jurisdiction_id;
               ELSE
                  null;
               END IF;
            END IF;
         END IF;

         -- If TaxCode is not Null then get all info
         IF l_tb_new_transaction.taxcode_code IS NOT NULL THEN
            -- ****** Get TaxCode Details ****** --
            l_tb_new_transaction.state_taxcode_type_code := NULL;
            l_tb_new_transaction.county_taxcode_type_code := NULL;
            l_tb_new_transaction.city_taxcode_type_code := NULL;
            -- Get Country/State/all County/all City for defaults
            vn_state_rows := 0;
            BEGIN
                OPEN taxcode_detail_cursor
                 FOR vc_taxcode_detail_stmt
               USING l_tb_new_transaction.taxcode_code, l_tb_new_transaction.transaction_country_code, l_tb_new_transaction.transaction_state_code, '*ALL', '*ALL', l_tb_new_transaction.gl_date, l_tb_new_transaction.gl_date;
               FETCH taxcode_detail_cursor
                INTO taxcode_detail;
               -- Rows found?
               IF taxcode_detail_cursor%FOUND THEN
                  vn_state_rows := taxcode_detail_cursor%ROWCOUNT;
               ELSE
                  vn_state_rows := 0;
               END IF;
               CLOSE taxcode_detail_cursor;
               IF SQLCODE != 0 THEN
                  vn_state_rows := 0;
               END IF;
            END;
            IF vn_state_rows > 0 THEN
               l_tb_new_transaction.state_taxcode_detail_id := taxcode_detail.taxcode_detail_id;
               l_tb_new_transaction.state_taxcode_type_code := taxcode_detail.taxcode_type_code;
               v_state_override_taxtype_code := taxcode_detail.override_taxtype_code;
               v_state_ratetype_code := taxcode_detail.ratetype_code;
               v_state_taxable_threshold_amt := taxcode_detail.taxable_threshold_amt;
               v_state_tax_limitation_amt := taxcode_detail.tax_limitation_amt;
               v_state_cap_amt := taxcode_detail.cap_amt;
               v_state_base_change_pct := taxcode_detail.base_change_pct;
               v_state_special_rate := taxcode_detail.special_rate;
               l_tb_new_transaction.county_taxcode_type_code := taxcode_detail.taxcode_type_code;
               v_county_taxable_threshold_amt := taxcode_detail.taxable_threshold_amt;
               v_county_tax_limitation_amt := taxcode_detail.tax_limitation_amt;
               v_county_cap_amt := taxcode_detail.cap_amt;
               v_county_base_change_pct := taxcode_detail.base_change_pct;
               v_county_special_rate := taxcode_detail.special_rate;
               l_tb_new_transaction.city_taxcode_type_code := taxcode_detail.taxcode_type_code;
               v_city_taxable_threshold_amt := taxcode_detail.taxable_threshold_amt;
               v_city_tax_limitation_amt := taxcode_detail.tax_limitation_amt;
               v_city_cap_amt := taxcode_detail.cap_amt;
               v_city_base_change_pct := taxcode_detail.base_change_pct;
               v_city_special_rate := taxcode_detail.special_rate;
               -- Get Country/State/County/all City for County
               vn_county_rows := 0;
               BEGIN
                   OPEN taxcode_detail_cursor
                    FOR vc_taxcode_detail_stmt
                  USING l_tb_new_transaction.taxcode_code, l_tb_new_transaction.transaction_country_code, l_tb_new_transaction.transaction_state_code, v_transaction_county, '*ALL', l_tb_new_transaction.gl_date, l_tb_new_transaction.gl_date;
                  FETCH taxcode_detail_cursor
                   INTO taxcode_detail;
                  -- Rows found?
                  IF taxcode_detail_cursor%FOUND THEN
                     vn_county_rows := taxcode_detail_cursor%ROWCOUNT;
                  ELSE
                     vn_county_rows := 0;
                  END IF;
                  CLOSE taxcode_detail_cursor;
                  IF SQLCODE != 0 THEN
                     vn_county_rows := 0;
                  END IF;
               END;
               IF vn_county_rows > 0 THEN
                  l_tb_new_transaction.county_taxcode_detail_id := taxcode_detail.taxcode_detail_id;
                  l_tb_new_transaction.county_taxcode_type_code := taxcode_detail.taxcode_type_code;
                  v_county_taxable_threshold_amt := taxcode_detail.taxable_threshold_amt;
                  v_county_tax_limitation_amt := taxcode_detail.tax_limitation_amt;
                  v_county_cap_amt := taxcode_detail.cap_amt;
                  v_county_base_change_pct := taxcode_detail.base_change_pct;
                  v_county_special_rate := taxcode_detail.special_rate;
               END IF;
               -- Get Country/State/all County/City for City
               vn_city_rows := 0;
               BEGIN
                   OPEN taxcode_detail_cursor
                    FOR vc_taxcode_detail_stmt
                  USING l_tb_new_transaction.taxcode_code, l_tb_new_transaction.transaction_country_code, l_tb_new_transaction.transaction_state_code, '*ALL', v_transaction_city, l_tb_new_transaction.gl_date, l_tb_new_transaction.gl_date;
                  FETCH taxcode_detail_cursor
                   INTO taxcode_detail;
                  -- Rows found?
                  IF taxcode_detail_cursor%FOUND THEN
                     vn_city_rows := taxcode_detail_cursor%ROWCOUNT;
                  ELSE
                     vn_city_rows := 0;
                  END IF;
                  CLOSE taxcode_detail_cursor;
                  IF SQLCODE != 0 THEN
                     vn_city_rows := 0;
                  END IF;
               END;
               IF vn_city_rows > 0 THEN
                  l_tb_new_transaction.city_taxcode_detail_id := taxcode_detail.taxcode_detail_id;
                  l_tb_new_transaction.city_taxcode_type_code := taxcode_detail.taxcode_type_code;
                  v_city_taxable_threshold_amt := taxcode_detail.taxable_threshold_amt;
                  v_city_tax_limitation_amt := taxcode_detail.tax_limitation_amt;
                  v_city_cap_amt := taxcode_detail.cap_amt;
                  v_city_base_change_pct := taxcode_detail.base_change_pct;
                  v_city_special_rate := taxcode_detail.special_rate;
               END IF;

               -- ****** DETERMINE TYPE of TAXCODE ****** --
               -- The Tax Code is Taxable
               IF l_tb_new_transaction.state_taxcode_type_code = 'T' OR l_tb_new_transaction.county_taxcode_type_code = 'T' OR l_tb_new_transaction.city_taxcode_type_code = 'T' THEN
                  -- Determine Tax Calc Flags
                  vc_country_flag := '1';
                  vc_state_flag := '1';
                  IF l_tb_new_transaction.state_taxcode_type_code = 'E' THEN
                     vc_state_flag := '0';
                  END IF;
                  vc_county_flag := '1';
                  vc_county_local_flag := '1';
                  IF l_tb_new_transaction.county_taxcode_type_code = 'E' THEN
                     vc_county_flag := '0';
                     vc_county_local_flag := '1';
                  END IF;
                  vc_city_flag := '1';
                  vc_city_local_flag := '1';
                  IF l_tb_new_transaction.city_taxcode_type_code = 'E' THEN
                     vc_city_flag := '0';
                     vc_city_local_flag := '1';
                  END IF;

                  -- Get Jurisdiction TaxRates and calculate Tax Amounts
                  sp_getusetax(l_tb_new_transaction.jurisdiction_id,
                               l_tb_new_transaction.gl_date,
                               l_tb_new_transaction.gl_line_itm_dist_amt,
                               'U',   -- Situs Tax Type code from Location Matrix when implemented.
                               vc_country_flag,   -- Situs Country Nexusflag when implemented
                               vc_state_flag,   -- Situs State Nexus flag when implemented
                               vc_county_flag,   -- Situs County Nexus flag when implemented
                               vc_county_local_flag,   -- Situs County Local nexus flag when implemented
                               vc_city_flag,   -- Situs City flag when implemented
                               vc_city_local_flag,   -- Situs City Local flag when implemented
                               v_state_ratetype_code,
                               v_state_override_taxtype_code,
                               v_state_taxable_threshold_amt,
                               v_state_tax_limitation_amt,
                               v_state_cap_amt,
                               v_state_base_change_pct,
                               v_state_special_rate,
                               v_county_taxable_threshold_amt,
                               v_county_tax_limitation_amt,
                               v_county_cap_amt,
                               v_county_base_change_pct,
                               v_county_special_rate,
                               v_city_taxable_threshold_amt,
                               v_city_tax_limitation_amt,
                               v_city_cap_amt,
                               v_city_base_change_pct,
                               v_city_special_rate,

                               l_tb_new_transaction.jurisdiction_taxrate_id,
                               l_tb_new_transaction.taxtype_used_code,
                               l_tb_new_transaction.country_use_amount,
                               l_tb_new_transaction.state_taxable_amt,
                               l_tb_new_transaction.state_use_amount,
                               l_tb_new_transaction.state_use_tier2_amount,
                               l_tb_new_transaction.state_use_tier3_amount,
                               l_tb_new_transaction.county_taxable_amt,
                               l_tb_new_transaction.county_use_amount,
                               l_tb_new_transaction.county_local_use_amount,
                               l_tb_new_transaction.city_taxable_amt,
                               l_tb_new_transaction.city_use_amount,
                               l_tb_new_transaction.city_local_use_amount,
                               l_tb_new_transaction.country_use_rate,
                               l_tb_new_transaction.state_use_rate,
                               l_tb_new_transaction.state_use_tier2_rate,
                               l_tb_new_transaction.state_use_tier3_rate,
                               l_tb_new_transaction.state_split_amount,
                               l_tb_new_transaction.state_tier2_min_amount,
                               l_tb_new_transaction.state_tier2_max_amount,
                               l_tb_new_transaction.state_maxtax_amount,
                               l_tb_new_transaction.county_use_rate,
                               l_tb_new_transaction.county_local_use_rate,
                               l_tb_new_transaction.county_split_amount,
                               l_tb_new_transaction.county_maxtax_amount,
                               l_tb_new_transaction.county_single_flag,
                               l_tb_new_transaction.county_default_flag,
                               l_tb_new_transaction.city_use_rate,
                               l_tb_new_transaction.city_local_use_rate,
                               l_tb_new_transaction.city_split_amount,
                               l_tb_new_transaction.city_split_use_rate,
                               l_tb_new_transaction.city_single_flag,
                               l_tb_new_transaction.city_default_flag,
                               l_tb_new_transaction.combined_use_rate,
                               l_tb_new_transaction.tb_calc_tax_amt);
                  IF l_tb_new_transaction.jurisdiction_taxrate_id IS NULL OR l_tb_new_transaction.jurisdiction_taxrate_id = 0 THEN
                     -- Jurisdiciton Tax Rate NOT Found
                     l_tb_new_transaction.transaction_ind := 'S';
                     l_tb_new_transaction.suspend_ind := 'R';
                  END IF;

               -- All TaxCodes are Exempt
               ELSIF l_tb_new_transaction.state_taxcode_type_code = 'E' AND l_tb_new_transaction.county_taxcode_type_code = 'E' AND l_tb_new_transaction.city_taxcode_type_code = 'E' THEN
                  l_tb_new_transaction.transaction_ind := 'P';

               -- The TaxCode is Unrecognized - Suspend
               ELSE
                  l_tb_new_transaction.transaction_ind := 'S';
                  l_tb_new_transaction.suspend_ind := '?';
               END IF;
            ELSE   -- If TaxCode Detail found
               -- Suspend for TaxCode Detail
               l_tb_new_transaction.transaction_ind := 'S';
               l_tb_new_transaction.suspend_ind := 'D';
            END IF;   -- If TaxCode Detail found
         END IF;
      END IF;

      -- Continue if not Suspended
      IF l_tb_new_transaction.transaction_ind IS NULL OR l_tb_new_transaction.transaction_ind <> 'S' THEN
         l_tb_new_transaction.transaction_ind := 'P';
      END IF;
   END IF;

   -- ******  GL logging module  ****** ------------------------------------------------------------
   sp_gl_logging (l_tb_old_transaction, l_tb_new_transaction, v_sysdate, v_sysdate_plus);

   -- Update transaction detail row
   BEGIN
      UPDATE tb_transaction_detail
         SET tb_calc_tax_amt = l_tb_new_transaction.tb_calc_tax_amt,
             country_use_amount = l_tb_new_transaction.country_use_amount,
             state_use_amount = l_tb_new_transaction.state_use_amount,
             state_use_tier2_amount = l_tb_new_transaction.state_use_tier2_amount,
             state_use_tier3_amount = l_tb_new_transaction.state_use_tier3_amount,
             county_use_amount = l_tb_new_transaction.county_use_amount,
             county_local_use_amount = l_tb_new_transaction.county_local_use_amount,
             city_use_amount = l_tb_new_transaction.city_use_amount,
             city_local_use_amount = l_tb_new_transaction.city_local_use_amount,
             transaction_ind = l_tb_new_transaction.transaction_ind,
             suspend_ind = l_tb_new_transaction.suspend_ind,
             taxcode_code = l_tb_new_transaction.taxcode_code,
             manual_taxcode_ind = l_tb_new_transaction.manual_taxcode_ind,
             tax_matrix_id = l_tb_new_transaction.tax_matrix_id,
             location_matrix_id = l_tb_new_transaction.location_matrix_id,
             jurisdiction_id = l_tb_new_transaction.jurisdiction_id,
             jurisdiction_taxrate_id = l_tb_new_transaction.jurisdiction_taxrate_id,
             manual_jurisdiction_ind = l_tb_new_transaction.manual_jurisdiction_ind,
             measure_type_code = v_state_ratetype_code,
             country_use_rate = l_tb_new_transaction.country_use_rate,
             state_use_rate = l_tb_new_transaction.state_use_rate,
             state_use_tier2_rate = l_tb_new_transaction.state_use_tier2_rate,
             state_use_tier3_rate = l_tb_new_transaction.state_use_tier3_rate,
             state_split_amount = l_tb_new_transaction.state_split_amount,
             state_tier2_min_amount = l_tb_new_transaction.state_tier2_min_amount,
             state_tier2_max_amount = l_tb_new_transaction.state_tier2_max_amount,
             state_maxtax_amount = l_tb_new_transaction.state_maxtax_amount,
             county_use_rate = l_tb_new_transaction.county_use_rate,
             county_local_use_rate = l_tb_new_transaction.county_local_use_rate,
             county_split_amount = l_tb_new_transaction.county_split_amount,
             county_maxtax_amount = l_tb_new_transaction.county_maxtax_amount,
             county_single_flag = l_tb_new_transaction.county_single_flag,
             county_default_flag = l_tb_new_transaction.county_default_flag,
             city_use_rate = l_tb_new_transaction.city_use_rate,
             city_local_use_rate = l_tb_new_transaction.city_local_use_rate,
             city_split_amount = l_tb_new_transaction.city_split_amount,
             city_split_use_rate = l_tb_new_transaction.city_split_use_rate,
             city_single_flag = l_tb_new_transaction.city_single_flag,
             city_default_flag = l_tb_new_transaction.city_default_flag,
             combined_use_rate = l_tb_new_transaction.combined_use_rate,
             state_taxcode_detail_id = l_tb_new_transaction.state_taxcode_detail_id,
             county_taxcode_detail_id = l_tb_new_transaction.county_taxcode_detail_id,
             city_taxcode_detail_id = l_tb_new_transaction.city_taxcode_detail_id,
             taxtype_used_code = l_tb_new_transaction.taxtype_used_code,
             state_taxable_amt = l_tb_new_transaction.state_taxable_amt,
             county_taxable_amt = l_tb_new_transaction.county_taxable_amt,
             city_taxable_amt = l_tb_new_transaction.city_taxable_amt,
             state_taxcode_type_code = l_tb_new_transaction.state_taxcode_type_code,
             county_taxcode_type_code = l_tb_new_transaction.county_taxcode_type_code,
             city_taxcode_type_code = l_tb_new_transaction.city_taxcode_type_code,
             gl_extract_updater = l_tb_new_transaction.gl_extract_updater,
             gl_extract_timestamp = l_tb_new_transaction.gl_extract_timestamp,
             gl_extract_flag = l_tb_new_transaction.gl_extract_flag,
             gl_log_flag = l_tb_new_transaction.gl_log_flag,
             update_user_id = l_tb_new_transaction.update_user_id,
             update_timestamp = l_tb_new_transaction.update_timestamp
       WHERE transaction_detail_id = p_transaction_detail_id;
   END;
END;
