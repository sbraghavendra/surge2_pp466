------------------------------------------------------
-- Upgrade Scripts for build 949, or build after 01/06/2010 --
------------------------------------------------------

SP_BATCH.sql
SP_BATCH_ARCHIVE.sql
SP_BATCH_ARCHIVE_DELETE.sql
SP_BATCH_ARCHIVE_RESTORE.sql
SP_BATCH_PROCESS.sql
SP_BATCH_TAXRATE_UPDATE.sql
SP_IMPORT_ALLOCATION.sql
SP_TB_JURISDICTION_TAXRATE_A_I.sql
SP_TB_LOCATION_MATRIX_A_I.sql
SP_TB_TAX_MATRIX_A_I.sql
SP_TB_TRANSACTION_DETAIL_A_U.sql
SP_TB_TRANSACTION_DETAIL_B_U.sql
DataUpgrade.sql



------------------------------------------------------
-- NOTE: These scripts are used to fix following issues --
-- 0004634: The time is off everywhere. -- 
-- 0004853: Add Time Zone options to Preferences -- 
------------------------------------------------------

Steps for upgrading DB and Tomcat:
 
1) Execute these scripts. The SYSDATE will be replaced with SYS_EXTRACT_UTC(SYSTIMESTAMP) in the following Stored Procedures and Sp_Batch_Process in line 995 will be changed to: bcp_transactions.gl_date := SYSDATE; 

Sp_Batch
Sp_Batch_Archive
Sp_Batch_Archive_Delete
Sp_Batch_Archive_Restore
Sp_Batch_Process 
Sp_Batch_Taxrate_Update 
Sp_Import_Allocation
Sp_Tb_Jurisdiction_Taxrate_A_I
Sp_Tb_Location_Matrix_A_I
Sp_Tb_Tax_Matrix_A_I
Sp_Tb_Transaction_Detail_A_U
Sp_Tb_Transaction_Detail_B_U


2) Execute the script to set/change timezone codes/ids in TB_LIST_CODE and TB_OPTION
DataUpgrade.sql




3) set Tomcat timezone for Windows or Unix. 

On Windows: add the "-Duser.timezone=UTC" to the Java options list.

On Linux/Unix: add the "-Duser.timezone=UTC" parameter to the JAVA_OPTS environment variable in your {TOMCAT_HOME}/bin/startup.sh. 
If you don't have a line in your startup.sh for setting the JAVA_OPTS, then you can add this to the start of the file (after the shell-specification in the first line of the shell-script):

export JAVA_OPTS="-Duser.timezone=UTC"


