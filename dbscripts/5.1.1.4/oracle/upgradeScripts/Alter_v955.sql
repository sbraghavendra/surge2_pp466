set define off
spool c:\alter_v955.log
set echo on

--For build 955 --

CREATE OR REPLACE FUNCTION F_Calc_Taxes(
   an_jurisdiction_id IN NUMBER,
   ad_gl_date IN DATE,
   av_measure_type_code IN VARCHAR2,
   an_gl_line_itm_dist_amt IN NUMBER,
   av_taxtype_code IN VARCHAR2,
   av_override_taxtype_code IN VARCHAR2,
   ac_state_flag IN CHAR,
   ac_county_flag IN CHAR,
   ac_county_local_flag IN CHAR,
   ac_city_flag IN CHAR,
   ac_city_local_flag IN CHAR,
   an_invoice_freight_amt IN NUMBER,
   an_invoice_discount_amt IN NUMBER,
   av_transaction_state_code IN VARCHAR2,
   av_import_definition_code IN VARCHAR2,
   av_taxcode_code IN VARCHAR2
)
RETURN VARCHAR2  
IS

-- Program defined variables
   vn_fetch_rows NUMBER := 0;

-- Define Jurisdiction Tax Rates Cursor (tb_jurisdiction_taxrate)
   CURSOR jurisdiction_taxrate_cursor
   IS
        SELECT /*+ INDEX (TB_JURISDICTION_TAXRATE)*/
               jurisdiction_taxrate_id, state_sales_rate, state_use_rate, state_use_tier2_rate, state_use_tier3_rate,
               state_split_amount, state_tier2_min_amount, state_tier2_max_amount, state_maxtax_amount,
               county_sales_rate, county_use_rate, county_local_sales_rate, county_local_use_rate,
               county_split_amount, county_maxtax_amount, county_single_flag, county_default_flag,
               city_sales_rate, city_use_rate, city_local_sales_rate, city_local_use_rate,
               city_split_amount, city_split_sales_rate, city_split_use_rate, city_single_flag, city_default_flag
          FROM TB_JURISDICTION_TAXRATE
         WHERE jurisdiction_id = an_jurisdiction_id AND
               effective_date <= ad_gl_date AND
               expiration_date >= ad_gl_date AND
               measure_type_code = av_measure_type_code
      ORDER BY effective_date DESC;
      jurisdiction_taxrate         jurisdiction_taxrate_cursor%ROWTYPE;

-- Define Exceptions
   e_badread                       EXCEPTION;

   an_jurisdiction_taxrate_id NUMBER;
   an_state_amount NUMBER;
   an_state_use_tier2_amount NUMBER;
   an_state_use_tier3_amount NUMBER;
   an_county_amount NUMBER;
   an_county_local_amount NUMBER;
   an_city_amount NUMBER;
   an_city_local_amount NUMBER;
   an_state_rate NUMBER;
   an_state_use_tier2_rate NUMBER;
   an_state_use_tier3_rate NUMBER;
   an_state_split_amount NUMBER;
   an_state_tier2_min_amount NUMBER;
   an_state_tier2_max_amount NUMBER;
   an_state_maxtax_amount NUMBER;
   an_county_rate NUMBER;
   an_county_local_rate NUMBER;
   an_county_split_amount NUMBER;
   an_county_maxtax_amount NUMBER;
   ac_county_single_flag CHAR;
   ac_county_default_flag CHAR;
   an_city_rate NUMBER;
   an_city_local_rate NUMBER;
   an_city_split_amount NUMBER;
   an_city_split_rate NUMBER;
   ac_city_single_flag CHAR;
   ac_city_default_flag CHAR;
   an_combined_rate NUMBER;
   an_tb_calc_tax_amt NUMBER;
   an_taxable_amt NUMBER;
   av_taxtype_used VARCHAR2(10);
   av_return_vals VARCHAR2(4000) := '';
   
BEGIN
  BEGIN
   -- Search for Jurisdiction Tax Rates
   OPEN jurisdiction_taxrate_cursor;
   FETCH jurisdiction_taxrate_cursor
    INTO jurisdiction_taxrate;
   -- Rows found?
   IF jurisdiction_taxrate_cursor%FOUND THEN
      vn_fetch_rows := jurisdiction_taxrate_cursor%ROWCOUNT;
   ELSE
      vn_fetch_rows := 0;
   END IF;
   CLOSE jurisdiction_taxrate_cursor;
   IF SQLCODE != 0 THEN
      RAISE e_badread;
   END IF;

   IF vn_fetch_rows > 0 THEN
      -- Determine Tax Type - Use or Sales
      av_taxtype_used := av_override_taxtype_code;
      IF av_taxtype_used IS NULL OR TRIM(av_taxtype_used) = '' OR TRIM(av_taxtype_used) = '*NO' THEN
         av_taxtype_used := SUBSTR(TRIM(av_taxtype_code),1,1);
         IF av_taxtype_used IS NULL OR Trim(av_taxtype_used) = '' THEN
            av_taxtype_used := 'U';
         END IF;
      END IF;

      -- Move Rates to parameters
      an_jurisdiction_taxrate_id := jurisdiction_taxrate.jurisdiction_taxrate_id;
      an_county_split_amount := jurisdiction_taxrate.county_split_amount;
      an_county_maxtax_amount := jurisdiction_taxrate.county_maxtax_amount;
      ac_county_single_flag := jurisdiction_taxrate.county_single_flag;
      ac_county_default_flag := jurisdiction_taxrate.county_default_flag;
      an_city_split_amount := jurisdiction_taxrate.city_split_amount;
      ac_city_single_flag := jurisdiction_taxrate.city_single_flag;
      ac_city_default_flag := jurisdiction_taxrate.city_default_flag;

      IF av_taxtype_used = 'U' THEN
         -- Use Tax Rates
         an_state_rate := jurisdiction_taxrate.state_use_rate;
         an_state_use_tier2_rate := jurisdiction_taxrate.state_use_tier2_rate;
         an_state_use_tier3_rate := jurisdiction_taxrate.state_use_tier3_rate;
         an_state_split_amount := jurisdiction_taxrate.state_split_amount;
         an_state_tier2_min_amount := jurisdiction_taxrate.state_tier2_min_amount;
         an_state_tier2_max_amount := jurisdiction_taxrate.state_tier2_max_amount;
         an_state_maxtax_amount := jurisdiction_taxrate.state_maxtax_amount;
         an_county_rate := jurisdiction_taxrate.county_use_rate;
         an_county_local_rate := jurisdiction_taxrate.county_local_use_rate;
         an_city_rate := jurisdiction_taxrate.city_use_rate;
         an_city_local_rate := jurisdiction_taxrate.city_local_use_rate;
         an_city_split_rate := jurisdiction_taxrate.city_split_use_rate;
      ELSIF av_taxtype_used = 'S' THEN
         -- Sales Tax Rates
         an_state_rate := jurisdiction_taxrate.state_sales_rate;
         an_state_use_tier2_rate := 0;
         an_state_use_tier3_rate := 0;
         an_state_split_amount := 0;
         an_state_tier2_min_amount := 0;
         an_state_tier2_max_amount := 999999999;
         an_state_maxtax_amount := 999999999;
         an_county_rate := jurisdiction_taxrate.county_sales_rate;
         an_county_local_rate := jurisdiction_taxrate.county_local_sales_rate;
         an_city_rate := jurisdiction_taxrate.city_sales_rate;
         an_city_local_rate := jurisdiction_taxrate.city_local_sales_rate;
         an_city_split_rate := jurisdiction_taxrate.city_split_sales_rate;
      ELSE
         -- Unknown!
         an_state_rate := 0;
         an_state_use_tier2_rate := 0;
         an_state_use_tier3_rate := 0;
         an_state_split_amount := 0;
         an_state_tier2_min_amount := 0;
         an_state_tier2_max_amount := 999999999;
         an_state_maxtax_amount := 999999999;
         an_county_rate := 0;
         an_county_local_rate := 0;
         an_city_rate := 0;
         an_city_local_rate := 0;
         an_city_split_rate := 0;
      END IF;

      -- Calculate Sales/Use Tax Amounts
      Sp_Calcusetax(an_gl_line_itm_dist_amt,
                    an_invoice_freight_amt,
                    an_invoice_discount_amt,
                    av_transaction_state_code,
                    av_import_definition_code,
                    av_taxcode_code,
                    ac_state_flag,
                    ac_county_flag,
                    ac_county_local_flag,
                    ac_city_flag,
                    ac_city_local_flag,
                    an_state_rate,
                    an_state_use_tier2_rate,
                    an_state_use_tier3_rate,
                    an_state_split_amount,
                    an_state_tier2_min_amount,
                    an_state_tier2_max_amount,
                    an_state_maxtax_amount,
                    an_county_rate,
                    an_county_local_rate,
                    an_county_split_amount,
                    an_county_maxtax_amount,
                    ac_county_single_flag,
                    ac_county_default_flag,
                    an_city_rate,
                    an_city_local_rate,
                    an_city_split_amount,
                    an_city_split_rate,
                    ac_city_single_flag,
                    ac_city_default_flag,
                    an_state_amount,
                    an_state_use_tier2_amount,
                    an_state_use_tier3_amount,
                    an_county_amount,
                    an_county_local_amount,
                    an_city_amount,
                    an_city_local_amount,
                    an_tb_calc_tax_amt,
                    an_taxable_amt );

      -- Calculate Combined Sales/Use Rate
      an_combined_rate := an_state_rate +
                          an_county_rate +
                          an_county_local_rate +
                          an_city_rate +
                          an_city_local_rate;
   ELSE
      an_jurisdiction_taxrate_id := 0;
   END IF;

   EXCEPTION
      WHEN e_badread THEN
         an_jurisdiction_taxrate_id := 0;
      WHEN OTHERS THEN
         an_jurisdiction_taxrate_id := 0;
  END;
   av_return_vals := TO_CHAR(an_jurisdiction_taxrate_id) || ',' ||
                    TO_CHAR(an_state_amount) || ',' ||
                    TO_CHAR(an_state_use_tier2_amount) || ',' ||
                    TO_CHAR(an_state_use_tier3_amount) || ',' ||
                    TO_CHAR(an_county_amount) || ',' ||
                    TO_CHAR(an_county_local_amount) || ',' ||
                    TO_CHAR(an_city_amount) || ',' ||
                    TO_CHAR(an_city_local_amount) || ',' ||
                    TO_CHAR(an_state_rate) || ',' ||
                    TO_CHAR(an_state_use_tier2_rate) || ',' ||
                    TO_CHAR(an_state_use_tier3_rate) || ',' ||
                    TO_CHAR(an_state_split_amount) || ',' ||
                    TO_CHAR(an_state_tier2_min_amount) || ',' ||
                    TO_CHAR(an_state_tier2_max_amount) || ',' ||
                    TO_CHAR(an_state_maxtax_amount) || ',' ||
                    TO_CHAR(an_county_rate) || ',' ||
                    TO_CHAR(an_county_local_rate) || ',' ||
                    TO_CHAR(an_county_split_amount) || ',' ||
                    TO_CHAR(an_county_maxtax_amount) || ',' ||
                    TO_CHAR(ac_county_single_flag) || ',' ||
                    TO_CHAR(ac_county_default_flag) || ',' ||
                    TO_CHAR(an_city_rate) || ',' ||
                    TO_CHAR(an_city_local_rate) || ',' ||
                    TO_CHAR(an_city_split_amount) || ',' ||
                    TO_CHAR(an_city_split_rate) || ',' ||
                    TO_CHAR(ac_city_single_flag) || ',' ||
                    TO_CHAR(ac_city_default_flag) || ',' ||
                    TO_CHAR(an_combined_rate) || ',' ||
                    TO_CHAR(an_tb_calc_tax_amt) || ',' ||
                    TO_CHAR(an_taxable_amt) || ',' ||
                    TO_CHAR(av_taxtype_used);
   RETURN av_return_vals;  
END F_Calc_Taxes;
/


CREATE OR REPLACE function f_get_location_matrix (
    p_transaction_detail_id IN tb_transaction_detail.transaction_detail_id%TYPE
)
return varchar2 is
   vn_location_matrix_id NUMBER := 0;
   vc_location_matrix_select       VARCHAR2(1000) :=
      'SELECT tb_location_matrix.location_matrix_id, ' ||
             'tb_location_matrix.jurisdiction_id, ' ||
             'tb_location_matrix.override_taxtype_code, ' ||
             'tb_location_matrix.state_flag,' ||
             'tb_location_matrix.county_flag,' ||
             'tb_location_matrix.county_local_flag,' ||
             'tb_location_matrix.city_flag,' ||
             'tb_location_matrix.city_local_flag,' ||
             'tb_jurisdiction.state, ' ||
             'tb_jurisdiction.county, ' ||
             'tb_jurisdiction.city, ' ||
             'tb_jurisdiction.zip, ' ||
             'tb_jurisdiction.in_out ' ||
        'FROM tb_location_matrix, ' ||
             'tb_jurisdiction, ' ||
             'tb_tmp_transaction_detail_b_u ' ||
      'WHERE ( tb_tmp_transaction_detail_b_u.transaction_detail_id = :v_transaction_detail_id ) AND ' ||
            '( tb_location_matrix.default_flag = ''0'' AND tb_location_matrix.binary_weight > 0 ) AND ' ||
            '( tb_location_matrix.effective_date <= tb_tmp_transaction_detail_b_u.gl_date ) AND ( tb_location_matrix.expiration_date >= tb_tmp_transaction_detail_b_u.gl_date ) AND ' ||
            '( tb_location_matrix.jurisdiction_id = tb_jurisdiction.jurisdiction_id ) ';
   vc_location_matrix_where        VARCHAR2(3000) := '';
   vc_location_matrix_orderby      VARCHAR2(1000) :=
      'ORDER BY tb_location_matrix.binary_weight DESC, tb_location_matrix.significant_digits DESC, tb_location_matrix.effective_date DESC';
   vc_location_matrix_stmt         VARCHAR2 (5000);
   p_new_tb_transaction_detail     tb_tmp_transaction_detail_b_u%ROWTYPE;
begin
  SELECT * INTO p_new_tb_transaction_detail FROM tb_tmp_transaction_detail_b_u
    WHERE transaction_detail_id = p_transaction_detail_id;
  sp_gen_location_driver (
      p_generate_driver_reference    => 'N',
      p_an_batch_id            => NULL,
      p_transaction_table_name    => 'tb_tmp_transaction_detail_b_u',
      p_location_table_name        => 'tb_location_matrix',
      p_vc_location_matrix_where    => vc_location_matrix_where);

  -- If no drivers found raise error, else create transaction detail sql statement
  IF vc_location_matrix_where IS NULL THEN
    vc_location_matrix_stmt := '';
  ELSE
    vc_location_matrix_stmt := vc_location_matrix_select || vc_location_matrix_where || vc_location_matrix_orderby;
  END IF;
  return vc_location_matrix_stmt;
end f_get_location_matrix;
/


CREATE OR REPLACE function get_tax_matrix (
    p_transaction_detail_id IN tb_transaction_detail.transaction_detail_id%TYPE
)
return varchar2
is 
   vc_tax_matrix_where             VARCHAR2(4000) := '';
   tmp                             VARCHAR2(4000) := '';
   vc_state_driver_flag            VARCHAR(10) := '0';
begin
    sp_gen_tax_driver (p_generate_driver_reference    => 'N',
      p_an_batch_id            => NULL,
      p_transaction_table_name    => 'trn',
      p_tax_table_name        => 'mat',
      p_vc_state_driver_flag        => vc_state_driver_flag,
      p_vc_tax_matrix_where        => vc_tax_matrix_where);   

  IF vc_tax_matrix_where IS NULL THEN
    vc_tax_matrix_where := '';
  END IF;

  return vc_tax_matrix_where;
end get_tax_matrix;
/




COMMIT;

spool off

