create or replace
PROCEDURE sp_batch
/* ************************************************************************************************/
/* Object Type/Name: SProc - sp_batch                                                             */
/* Author:           Michael B. Fuller                                                            */
/* Date:             a long time ago                                                              */
/* Description:      Main proc to call batch procs                                                */
/* Arguments:        None                                                                         */
/* Returns:          None                                                                         */
/* ************************************************************************************************/
/* Maintenance History:                                                                           */
/* Scan  Author       Date     Release    Description                                  Ticket #   */
/* ----- ---------- ---------- ---------- -------------------------------------------- ---------- */
/* MBF01 M. Fuller  05/05/2006            Comment out Archive Procs                               */
/* MBF02 M. Fuller  08/12/2011            Add TaxCode Rules Updates                    17         */
/* ************************************************************************************************/
IS
   vi_running                      INTEGER                                           := 0;
   vc_cursor_open                  CHAR(1)                                           := 'N';
   vdt_sysdate                     DATE                                              := SYS_EXTRACT_UTC(SYSTIMESTAMP);

CURSOR batch_cursor
IS
   SELECT batch_id,
          batch_type_code,
          batch_status_code
    FROM tb_batch
   WHERE ( batch_status_code LIKE 'F%' ) AND
         ( held_flag IS NULL OR held_flag <> '1' ) AND
         ( sched_start_timestamp IS NULL OR sched_start_timestamp <= vdt_sysdate )
ORDER BY sched_start_timestamp, batch_id;
batch_record                       batch_cursor%ROWTYPE;

-- ****** Program starts ****** --------------------------------------------------------------------
BEGIN
   SELECT COUNT(*)
     INTO vi_running
     FROM tb_batch
    WHERE batch_status_code LIKE 'X%';

   IF vi_running = 0 THEN
      LOOP
         IF vc_cursor_open = 'Y' THEN
            CLOSE batch_cursor;
            vc_cursor_open := 'N';
         END IF;

         OPEN batch_cursor;
         vc_cursor_open := 'Y';
         FETCH batch_cursor INTO batch_record;
         EXIT WHEN batch_cursor%NOTFOUND;

         IF batch_record.batch_status_code = 'FI' THEN
            --sp_batch_import(batch_record.batch_id);
            NULL;
         ELSIF batch_record.batch_status_code = 'FP' THEN        -- Flagged for Process
            sp_batch_process(batch_record.batch_id);
         ELSIF batch_record.batch_status_code = 'FD' THEN        -- Flagged for Deleted
            sp_batch_delete(batch_record.batch_id);
         ELSIF batch_record.batch_status_code = 'FA' THEN        -- Flagged for Archive
            null;
            --sp_batch_archive(batch_record.batch_id);
         ELSIF batch_record.batch_status_code = 'FAR' THEN       -- Flagged for Archive Restore
            null;
            --sp_batch_archive_restore(batch_record.batch_id);
         ELSIF batch_record.batch_status_code = 'FADR' THEN      -- Flagged for Archive Delete
            null;
            --sp_batch_archive_delete(batch_record.batch_id);
         ELSIF batch_record.batch_status_code = 'FE' THEN        -- Flaged for Extract
            null;
            --sp_batch_extract(batch_record.batch_id);
            NULL;
         ELSIF batch_record.batch_status_code = 'FTR' THEN       -- Flagged for Tax Rate Update
            sp_batch_taxrate_update(batch_record.batch_id);
         ELSIF batch_record.batch_status_code = 'FTCR' THEN      -- Flagged for TaxCode Rules Update
            sp_batch_taxcode_rules_update(batch_record.batch_id);
         ELSE                                                    -- No recognizable status
            UPDATE tb_batch
               SET batch_status_code = 'ERR'
             WHERE batch_id = batch_record.batch_id;
            COMMIT;
         END IF;
      END LOOP;
      CLOSE batch_cursor;
   END IF;
END;
