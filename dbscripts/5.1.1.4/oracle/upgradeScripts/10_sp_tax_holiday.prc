create or replace
PROCEDURE sp_tax_holiday
/* ************************************************************************************************/
/* Object Type/Name: Stored Procedure - sp_tax_holiday                                            */
/* Author:           Michael B. Fuller                                                            */
/* Date:             a long time ago                                                              */
/* Description:      create tax holiday tax matrix lines                                          */
/* Arguments:        an_batch_id number                                                           */
/* Returns:          None                                                                         */
/* ************************************************************************************************/
/* Maintenance History:                                                                           */
/* Scan  Author       Date     Release    Description                                  Ticket #   */
/* ----- ---------- ---------- ---------- -------------------------------------------- ---------- */
/* ************************************************************************************************/
IS
-- Define Cursor Type
   TYPE cursor_type is REF CURSOR;

-- Table defined variables
   v_taxcode_detail_id             tb_taxcode_detail.taxcode_detail_id%TYPE          := NULL;
   v_sysdate                       tb_taxcode_detail.update_timestamp%TYPE           := SYS_EXTRACT_UTC(SYSTIMESTAMP);

-- Program defined variables
   vn_return                       NUMBER                                            := 0;

-- Define Tax Holiday Cursor
   CURSOR tax_holiday_cursor
   IS
      SELECT tb_tax_holiday.tax_holiday_code,
             tb_tax_holiday.taxcode_country_code,
             tb_tax_holiday.taxcode_state_code,
             tb_tax_holiday.effective_date,
             tb_tax_holiday.expiration_date,
             tb_tax_holiday.taxcode_type_code,
             tb_tax_holiday.override_taxtype_code,
             tb_tax_holiday.ratetype_code,
             tb_tax_holiday.taxable_threshold_amt,
             tb_tax_holiday.tax_limitation_amt,
             tb_tax_holiday.cap_amt,
             tb_tax_holiday.base_change_pct,
             tb_tax_holiday.special_rate,
             tb_tax_holiday_detail.taxcode_code,
             tb_tax_holiday_detail.taxcode_county,
             tb_tax_holiday_detail.taxcode_city
        FROM tb_tax_holiday,
             tb_tax_holiday_detail
       WHERE tb_tax_holiday.execute_flag = '0'
         AND tb_tax_holiday.tax_holiday_code = tb_tax_holiday_detail.tax_holiday_code;
   tax_holiday                     tax_holiday_cursor%ROWTYPE;

-- Define Exceptions
   e_badread                       exception;
   e_badupdate                     exception;
   e_badwrite                      exception;
   e_wrongdata                     exception;
   e_abort                         exception;
   e_kill                          exception;

-- ****** Program starts ****** --------------------------------------------------------------------
BEGIN
   -- ****** Read and Process Tax Holidays ****** -------------------------------------------
   OPEN tax_holiday_cursor;
   LOOP
      FETCH tax_holiday_cursor INTO tax_holiday;
      EXIT WHEN tax_holiday_cursor%NOTFOUND;

      -- Create tax matrix line for holiday
      SELECT sq_tb_taxcode_detail_id.NEXTVAL
        INTO v_taxcode_detail_id
        FROM DUAL;

      BEGIN
         INSERT INTO tb_taxcode_detail (
            taxcode_detail_id,
            taxcode_code,
            taxcode_country_code,
            taxcode_state_code,
            taxcode_county,
            taxcode_city,
            effective_date,
            expiration_date,
            taxcode_type_code,
            override_taxtype_code,
            ratetype_code,
            taxable_threshold_amt,
            tax_limitation_amt,
            cap_amt,
            base_change_pct,
            special_rate,
            active_flag,
            custom_flag,
            modified_flag,
            update_user_id,
            update_timestamp)
         VALUES (
            v_taxcode_detail_id,
            tax_holiday.taxcode_code,
            tax_holiday.taxcode_country_code,
            tax_holiday.taxcode_state_code,
            tax_holiday.taxcode_county,
            tax_holiday.taxcode_city,
            tax_holiday.effective_date,
            tax_holiday.expiration_date,
            tax_holiday.taxcode_type_code,
            tax_holiday.override_taxtype_code,
            tax_holiday.ratetype_code,
            tax_holiday.taxable_threshold_amt,
            tax_holiday.tax_limitation_amt,
            tax_holiday.cap_amt,
            tax_holiday.base_change_pct,
            tax_holiday.special_rate,
            '1',   -- set active_flag to "on"
            '0',   -- set custom_flag to "off"
            '0',   -- set modified_flag to "off"
            USER,
            v_sysdate);
      END;

      UPDATE tb_tax_holiday
         SET tb_tax_holiday.execute_flag = '1'
       WHERE tb_tax_holiday.tax_holiday_code = tax_holiday.tax_holiday_code;
   END LOOP;
END;