CREATE OR REPLACE PROCEDURE Sp_Batch
/* ************************************************************************************************/
/* Object Type/Name: SP - sp_batch                                                                */
/* Author:           Michael B. Fuller                                                            */
/* Date:             a long time ago                                                              */
/* Description:      Main proc to call batch procs                                                */
/* Arguments:        None                                                                         */
/* Returns:          None                                                                         */
/* ************************************************************************************************/
/* Maintenance History:                                                                           */
/* Scan  Author       Date     Release    Description                                  Ticket #   */
/* ----- ---------- ---------- ---------- -------------------------------------------- ---------- */
/* MBF01 M. Fuller  05/05/2006            Comment out Archive Procs                               */
/* ************************************************************************************************/
IS
   vi_running                      INTEGER                                           := 0;
   vi_batches                      INTEGER                                           := 0;
   vc_cursor_open                  CHAR(1)                                           := 'N';
   vdt_sysdate                     DATE                                              := SYS_EXTRACT_UTC(SYSTIMESTAMP);

CURSOR batch_cursor
IS
   SELECT batch_id,
          batch_type_code,
          batch_status_code
    FROM TB_BATCH
   WHERE ( batch_status_code LIKE 'F%' ) AND
         ( held_flag IS NULL OR held_flag <> '1' ) AND
         ( sched_start_timestamp IS NULL OR sched_start_timestamp <= vdt_sysdate )
ORDER BY sched_start_timestamp, batch_id;
batch_record                       batch_cursor%ROWTYPE;

-- ****** Program starts ****** --------------------------------------------------------------------
BEGIN
   SELECT COUNT(*)
     INTO vi_running
     FROM TB_BATCH
    WHERE batch_status_code LIKE 'X%';

   IF vi_running = 0 THEN
      LOOP
         IF vc_cursor_open = 'Y' THEN
            CLOSE batch_cursor;
            vc_cursor_open := 'N';
         END IF;

         OPEN batch_cursor;
         vc_cursor_open := 'Y';
         FETCH batch_cursor INTO batch_record;
         EXIT WHEN batch_cursor%NOTFOUND;

         IF batch_record.batch_status_code = 'FI' THEN
            --sp_batch_import(batch_record.batch_id);
            NULL;
         ELSIF batch_record.batch_status_code = 'FP' THEN
            Sp_Batch_Process(batch_record.batch_id);
         ELSIF batch_record.batch_status_code = 'FD' THEN
            Sp_Batch_Delete(batch_record.batch_id);
         ELSIF batch_record.batch_status_code = 'FA' THEN
            null;
            --Sp_Batch_Archive(batch_record.batch_id);
         ELSIF batch_record.batch_status_code = 'FAR' THEN
            null;
            --Sp_Batch_Archive_Restore(batch_record.batch_id);
         ELSIF batch_record.batch_status_code = 'FADR' THEN
            null;
            --Sp_Batch_Archive_Delete(batch_record.batch_id);
         ELSIF batch_record.batch_status_code = 'FE' THEN
            null;
            --sp_batch_extract(batch_record.batch_id);
            NULL;
         ELSIF batch_record.batch_status_code = 'FTR' THEN
            Sp_Batch_Taxrate_Update(batch_record.batch_id);
         ELSE
            UPDATE TB_BATCH
               SET batch_status_code = 'ERR'
             WHERE batch_id = batch_record.batch_id;
            COMMIT;
         END IF;
      END LOOP;
      CLOSE batch_cursor;
   END IF;
END;
/
