CREATE OR REPLACE PROCEDURE Sp_Batch_Taxrate_Update(an_batch_id NUMBER)
/* ************************************************************************************************/
/* Object Type/Name: Stored Procedure - sp_batch_taxrate_update                                   */
/* Author:           Michael B. Fuller                                                            */
/* Date:             a long time ago                                                              */
/* Description:      process a batch of tax rate updates                                          */
/* Arguments:        an_batch_id number                                                           */
/* Returns:          None                                                                         */
/* ************************************************************************************************/
/* Maintenance History:                                                                           */
/* Scan  Author       Date     Release    Description                                  Ticket #   */
/* ----- ---------- ---------- ---------- -------------------------------------------- ---------- */
/* MBF01 M. Fuller  01/07/2010 5.1.1.1    Add code to get next sequences               4597       */
/* ************************************************************************************************/
IS
-- Table defined variables
   v_batch_status_code             TB_BATCH.batch_status_code%TYPE                   := NULL;
   v_error_sev_code                TB_BATCH.error_sev_code%TYPE                      := ' ';
   v_starting_sequence             TB_BATCH.start_row%TYPE                           := 0;
   v_process_count                 TB_BATCH.start_row%TYPE                           := 0;
   v_processed_rows                TB_BATCH.start_row%TYPE                           := 0;
   v_batch_error_log_id            TB_BATCH_ERROR_LOG.batch_error_log_id%TYPE        := 0;
   v_severity_level                TB_LIST_CODE.severity_level%TYPE                  := ' ';
   v_write_import_line_flag        TB_LIST_CODE.write_import_line_flag%TYPE          := '1';
   v_abort_import_flag             TB_LIST_CODE.abort_import_flag%TYPE               := '0';
   v_jurisdiction_id               TB_JURISDICTION.jurisdiction_id%TYPE              := NULL;
   v_jur_custom_flag               TB_JURISDICTION.custom_flag%TYPE                  := NULL;
   v_jurisdiction_taxrate_id       TB_JURISDICTION_TAXRATE.jurisdiction_taxrate_id%TYPE := NULL;
   v_rate_custom_flag              TB_JURISDICTION_TAXRATE.custom_flag%TYPE          := NULL;
   v_rate_modified_flag            TB_JURISDICTION_TAXRATE.modified_flag%TYPE        := NULL;
   v_sysdate                       TB_BCP_TRANSACTIONS.load_timestamp%TYPE           := SYS_EXTRACT_UTC(SYSTIMESTAMP);

-- Program defined variables
   vc_taxrate_update_type          VARCHAR(10)                                       := NULL;
   vi_release_number               INTEGER                                           := 0;
   vi_sessions                     INTEGER                                           := 0;
   vd_update_date                  DATE                                              := NULL;
   vi_cursor_id                    INTEGER                                           := 0;
   vi_error_count                  INTEGER                                           := 0;

-- Define BCP Jurisdiction TaxRate Updates Cursor (tb_bcp_jurisdiction_taxrate)
   CURSOR bcp_juris_taxrate_cursor
   IS
      SELECT
         batch_id,
         geocode,
         zip,
         state,
         county,
         city,
         zipplus4,
         in_out,
         state_sales_rate,
         state_use_rate,
         county_sales_rate,
         county_use_rate,
         county_local_sales_rate,
         county_local_use_rate,
         county_split_amount,
         county_maxtax_amount,
         county_single_flag,
         county_default_flag,
         city_sales_rate,
         city_use_rate,
         city_local_sales_rate,
         city_local_use_rate,
         city_split_amount,
         city_split_sales_rate,
         city_split_use_rate,
         city_single_flag,
         city_default_flag,
         effective_date
    FROM TB_BCP_JURISDICTION_TAXRATE
   WHERE batch_id = an_batch_id;
   bcp                             bcp_juris_taxrate_cursor%ROWTYPE;

-- Define Exceptions
   e_badread                       EXCEPTION;
   e_badupdate                     EXCEPTION;
   e_badwrite                      EXCEPTION;
   e_wrongdata                     EXCEPTION;
   e_abort                         EXCEPTION;
   e_halt                          EXCEPTION;

-- ****** Program starts ****** --------------------------------------------------------------------
BEGIN
   -- Confirm batch exists and is flagged for processing
   BEGIN
      SELECT batch_status_code
        INTO v_batch_status_code
        FROM TB_BATCH
       WHERE batch_id = an_batch_id;
      IF SQLCODE != 0 THEN
         RAISE e_badread;
      ELSIF v_batch_status_code != 'FTR' THEN
         RAISE e_wrongdata;
      END IF;
   EXCEPTION
      WHEN NO_DATA_FOUND OR e_badread THEN
         vi_error_count := vi_error_count + 1;
         Sp_Geterrorcode('TR8', an_batch_id, 'TR', v_sysdate, v_severity_level, v_write_import_line_flag, v_abort_import_flag);
         IF v_severity_level > v_error_sev_code THEN
            v_error_sev_code := v_severity_level;
         END IF;
      WHEN e_wrongdata THEN
         vi_error_count := vi_error_count + 1;
         Sp_Geterrorcode('TR9', an_batch_id, 'TR', v_sysdate, v_severity_level, v_write_import_line_flag, v_abort_import_flag);
         IF v_severity_level > v_error_sev_code THEN
            v_error_sev_code := v_severity_level;
         END IF;
   END;
   -- Abort Procedure?
   IF v_abort_import_flag = '1' THEN
      v_batch_status_code := 'ABTR';
      RAISE e_abort;
   END IF;

   -- Check for Users in STS??
   -- Take this out for now -- 3.3.4.1
   /* SELECT count(*)
     INTO vi_sessions
     FROM v$session
    WHERE audsid <> 0
      AND audsid NOT IN
         (SELECT sys_context('USERENV','SESSIONID') FROM DUAL);
   IF vi_sessions > 0 Then
      RAISE e_halt;
   END IF; */

   -- Disable triggers
   EXECUTE IMMEDIATE 'ALTER TRIGGER tb_jurisdiction_taxrate_a_i DISABLE';

   -- Get starting sequence no.
   BEGIN
      SELECT last_number
        INTO v_starting_sequence
        FROM user_sequences
       WHERE sequence_name = 'SQ_TB_PROCESS_COUNT';
   EXCEPTION
      WHEN OTHERS THEN
         NULL;
   END;

   -- Get batch information
   SELECT UPPER(VC02),
          NU01,
          TS01
     INTO vc_taxrate_update_type,
          vi_release_number,
          vd_update_date
     FROM TB_BATCH
    WHERE batch_id = an_batch_id;

   -- Update batch as processing
   BEGIN
      UPDATE TB_BATCH
         SET batch_status_code = 'XTR',
             error_sev_code = '',
             start_row = v_starting_sequence,
             actual_start_timestamp = v_sysdate
       WHERE batch_id = an_batch_id;
      IF SQLCODE != 0 THEN
         RAISE e_badupdate;
      END IF;
   EXCEPTION
      WHEN e_badupdate THEN
         vi_error_count := vi_error_count + 1;
         Sp_Geterrorcode('TR1', an_batch_id, 'TR', v_sysdate, v_severity_level, v_write_import_line_flag, v_abort_import_flag);
         IF v_severity_level > v_error_sev_code THEN
            v_error_sev_code := v_severity_level;
         END IF;
   END;
   -- Abort Procedure?
   IF v_abort_import_flag = '1' THEN
      v_batch_status_code := 'ABTR';
      RAISE e_abort;
   END IF;

   COMMIT;

   -- ****** Read and Process Transactions ****** -----------------------------------------
   OPEN bcp_juris_taxrate_cursor;
   LOOP
      FETCH bcp_juris_taxrate_cursor INTO bcp;
      EXIT WHEN bcp_juris_taxrate_cursor%NOTFOUND;

      -- Increment Process Count
      SELECT sq_tb_process_count.NEXTVAL
        INTO v_process_count
        FROM DUAL;
      v_processed_rows := v_processed_rows + 1;

      -- Find jurisdiction
      BEGIN
         SELECT jur.jurisdiction_id,
                jur.custom_flag
           INTO v_jurisdiction_id,
                v_jur_custom_flag
           FROM TB_JURISDICTION jur
          WHERE ( jur.geocode = bcp.geocode ) AND
                ( jur.state = bcp.state ) AND
                ( jur.county = bcp.county ) AND
                ( jur.city = bcp.city ) AND
                ( jur.zip = bcp.zip );
      EXCEPTION
         WHEN OTHERS THEN
            v_jurisdiction_id := 0;
            v_jur_custom_flag := '0';
      END;

      -- If it does not exist, add it
      IF v_jurisdiction_id IS NULL OR v_jurisdiction_id = 0 THEN
         -- Insert new jurisdiction
         SELECT sq_tb_jurisdiction_id.NEXTVAL
           INTO v_jurisdiction_id
           FROM DUAL;

         INSERT INTO TB_JURISDICTION(
            jurisdiction_id,
            geocode,
            state,
            county,
            city,
            zip,
            zipplus4,
            in_out,
            custom_flag,
            description )
         VALUES(
            v_jurisdiction_id,
            bcp.geocode,
            bcp.state,
            bcp.county,
            bcp.city,
            bcp.zip,
            bcp.zipplus4,
            bcp.in_out,
            '0',
            'CCH Supplied Jurisdiction/Tax Rate' );
         -- Get last inserted jurisdiction id
         --SELECT MAX(jurisdiction_id)
         --  INTO v_jurisdiction_id
         --  FROM TB_JURISDICTION;
      ELSE
         -- If it does exist and is custom then write errog log warning
         IF v_jur_custom_flag = '1' THEN
            v_error_sev_code := '10';
            SELECT sq_tb_batch_error_log_id.NEXTVAL
              INTO v_batch_error_log_id
              FROM DUAL;

            INSERT INTO TB_BATCH_ERROR_LOG (
               batch_error_log_id,
               batch_id,
               process_type,
               process_timestamp,
               row_no,
               column_no,
               error_def_code,
               import_header_column,
               import_column_value,
               trans_dtl_column_name,
               trans_dtl_datatype,
               import_row )
            VALUES (
               v_batch_error_log_id,
               bcp.batch_id,
               'TR',
               v_sysdate,
               ROWNUM,
               0,
               'TR1',
               'GeoCode|State|County|City|Zip',
               bcp.geocode || '|' || bcp.state || '|' || bcp.county || '|' || bcp.city || '|' || bcp.zip,
               'n/a',
               'n/a',
               'n/a' );
         END IF;
      END IF;

      -- Find jurisdiction taxrate
      BEGIN
         SELECT rate.jurisdiction_taxrate_id,
                rate.custom_flag,
                rate.modified_flag
           INTO v_jurisdiction_taxrate_id,
                v_rate_custom_flag,
                v_rate_modified_flag
           FROM TB_JURISDICTION_TAXRATE rate
          WHERE ( rate.jurisdiction_id = v_jurisdiction_id ) AND
                ( rate.effective_date = bcp.effective_date ) AND
                ( rate.measure_type_code = '0' );
      EXCEPTION
         WHEN OTHERS THEN
            v_jurisdiction_taxrate_id := 0;
            v_rate_custom_flag := '0';
            v_rate_modified_flag := '0';
      END;

      -- If it does not exist, add it
      IF v_jurisdiction_taxrate_id IS NULL OR v_jurisdiction_taxrate_id = 0 THEN
         SELECT sq_tb_jurisdiction_taxrate_id.NEXTVAL
           INTO v_jurisdiction_taxrate_id
           FROM DUAL;

         INSERT INTO TB_JURISDICTION_TAXRATE(
            jurisdiction_taxrate_id,
            jurisdiction_id,
            measure_type_code,
            effective_date,
            expiration_date,
            custom_flag,
            modified_flag,
            state_sales_rate,
            state_use_rate,
            county_sales_rate,
            county_use_rate,
            county_local_sales_rate,
            county_local_use_rate,
            county_split_amount,
            county_maxtax_amount,
            county_single_flag,
            county_default_flag,
            city_sales_rate,
            city_use_rate,
            city_local_sales_rate,
            city_local_use_rate,
            city_split_amount,
            city_split_sales_rate,
            city_split_use_rate,
            city_single_flag,
            city_default_flag )
         VALUES(
            v_jurisdiction_taxrate_id,
            v_jurisdiction_id,
            '0',
            bcp.effective_date,
            TO_DATE('12/31/9999', 'mm/dd/yyyy'),
            '0',
            '0',
            bcp.state_sales_rate,
            bcp.state_use_rate,
            bcp.county_sales_rate,
            bcp.county_use_rate,
            bcp.county_local_sales_rate,
            bcp.county_local_use_rate,
            bcp.county_split_amount,
            bcp.county_maxtax_amount,
            bcp.county_single_flag,
            bcp.county_default_flag,
            bcp.city_sales_rate,
            bcp.city_use_rate,
            bcp.city_local_sales_rate,
            bcp.city_local_use_rate,
            bcp.city_split_amount,
            bcp.city_split_sales_rate,
            bcp.city_split_use_rate,
            bcp.city_single_flag,
            bcp.city_default_flag );
      ELSE
         -- If it does exist and jurisdiction is not custom but rate is custom (modified or not) then write errog log warning
         IF (( v_jur_custom_flag IS NULL ) OR ( v_jur_custom_flag = '0' )) AND
            ( v_rate_custom_flag = '1' ) THEN
            v_error_sev_code := '10';
            SELECT sq_tb_batch_error_log_id.NEXTVAL
              INTO v_batch_error_log_id
              FROM DUAL;

            INSERT INTO TB_BATCH_ERROR_LOG (
               batch_error_log_id,
               batch_id,
               process_type,
               process_timestamp,
               row_no,
               column_no,
               error_def_code,
               import_header_column,
               import_column_value,
               trans_dtl_column_name,
               trans_dtl_datatype,
               import_row )
            VALUES (
               v_batch_error_log_id,
               bcp.batch_id,
               'TR',
               v_sysdate,
               ROWNUM,
               0,
               'TR2',
               'GeoCode|State|County|City|Zip|Effective Date',
               bcp.geocode || '|' || bcp.state || '|' || bcp.county || '|' || bcp.city || '|' || bcp.zip || '|' || TO_CHAR(bcp.effective_date, 'mm/dd/yyyy'),
               'n/a',
               'n/a',
               'n/a' );
         ELSE
            -- If it does exist and jurisdiction is not custom and rate is not custom but rate is modified then write error log warning
            IF (( v_jur_custom_flag IS NULL ) OR ( v_jur_custom_flag = '0' )) AND
               (( v_rate_custom_flag IS NULL ) OR ( v_rate_custom_flag = '0' )) AND
               ( v_rate_modified_flag = '1' ) THEN
               v_error_sev_code := '10';
               SELECT sq_tb_batch_error_log_id.NEXTVAL
                 INTO v_batch_error_log_id
                 FROM DUAL;

               INSERT INTO TB_BATCH_ERROR_LOG (
                  batch_error_log_id,
                  batch_id,
                  process_type,
                  process_timestamp,
                  row_no,
                  column_no,
                  error_def_code,
                  import_header_column,
                  import_column_value,
                  trans_dtl_column_name,
                  trans_dtl_datatype,
                  import_row )
               VALUES (
                  v_batch_error_log_id,
                  bcp.batch_id,
                  'TR',
                  v_sysdate,
                  ROWNUM,
                  0,
                  'TR3',
                  'GeoCode|State|County|City|Zip|Effective Date',
                  bcp.geocode || '|' || bcp.state || '|' || bcp.county || '|' || bcp.city || '|' || bcp.zip || '|' || TO_CHAR(bcp.effective_date, 'mm/dd/yyyy'),
                  'n/a',
                  'n/a',
                  'n/a' );
            END IF;
         END IF;
         -- Update TaxRate
         UPDATE TB_JURISDICTION_TAXRATE rate SET
            rate.modified_flag = 0,
            rate.state_sales_rate = bcp.state_sales_rate,
            rate.state_use_rate =  bcp.state_use_rate,
            rate.county_sales_rate = bcp.county_sales_rate,
            rate.county_use_rate = bcp.county_use_rate,
            rate.county_local_sales_rate = bcp.county_local_sales_rate,
            rate.county_local_use_rate = bcp.county_local_use_rate,
            rate.county_split_amount = bcp.county_split_amount,
            rate.county_maxtax_amount = bcp.county_maxtax_amount,
            rate.county_single_flag = bcp.county_single_flag,
            rate.county_default_flag = bcp.county_default_flag,
            rate.city_sales_rate = bcp.city_sales_rate,
            rate.city_use_rate = bcp.city_use_rate,
            rate.city_local_sales_rate = bcp.city_local_sales_rate,
            rate.city_local_use_rate = bcp.city_local_use_rate,
            rate.city_split_amount = bcp.city_split_amount,
            rate.city_split_sales_rate = bcp.city_split_sales_rate,
            rate.city_split_use_rate = bcp.city_split_use_rate,
            rate.city_single_flag = bcp.city_single_flag,
            rate.city_default_flag = bcp.city_default_flag
         WHERE rate.jurisdiction_taxrate_id = v_jurisdiction_taxrate_id;
      END IF;
   END LOOP;
   CLOSE bcp_juris_taxrate_cursor;

   -- Remove Batch from BCP Transactions Table
   DELETE
     FROM TB_BCP_JURISDICTION_TAXRATE
    WHERE batch_id = an_batch_id;

   --  Update Options table with last month/year updated
   UPDATE TB_OPTION
      SET value = TO_CHAR(vd_update_date, 'yyyy_mm')
    WHERE option_code = 'LASTTAXRATEDATE' || vc_taxrate_update_type AND
          option_type_code = 'SYSTEM' AND
          user_code = 'SYSTEM';

   -- Update Options table with last release number
   UPDATE TB_OPTION
      SET value = vi_release_number
    WHERE option_code = 'LASTTAXRATEREL' || vc_taxrate_update_type AND
          option_type_code = 'SYSTEM' AND
          user_code = 'SYSTEM';

   -- Update batch with final totals
   UPDATE TB_BATCH
      SET batch_status_code = 'TR',
          error_sev_code = v_error_sev_code,
          processed_rows = v_processed_rows,
          actual_end_timestamp = SYS_EXTRACT_UTC(SYSTIMESTAMP)
    WHERE batch_id = an_batch_id;

   -- Update CCH Code table
   DELETE FROM TB_CCH_CODE WHERE custom_flag <> '1';

   /*INSERT INTO tb_cch_code (
      filename,
      fieldname,
      fieldpos,
      fieldlen,
      fieldno,
      code,
      description,
      custom_flag,
      modified_flag )
   SELECT
      filename,
      fieldname,
      fieldpos,
      fieldlen,
      fieldno,
      code,
      description,
      '0',
      '0'
   FROM tb_bcp_cch_code
   WHERE batch_id = an_batch_id;*/

   /*DELETE
     FROM tb_bcp_cch_code
    WHERE batch_id = an_batch_id;*/

   -- Update CCH TxMatrix table
   DELETE FROM TB_CCH_TXMATRIX WHERE custom_flag <> '1';

   /*INSERT INTO tb_cch_txmatrix (
      city,
      county,
      state,
      local,
      geocode,
      groupcode,
      groupdesc,
      item,
      itemdesc,
      provider,
      customer,
      taxtypet,
      taxcatt,
      taxable,
      taxtype,
      taxcat,
      effdate,
      rectype,
      custom_flag,
      modified_flag )
   SELECT
      city,
      county,
      state,
      local,
      geocode,
      groupcode,
      groupdesc,
      item,
      itemdesc,
      provider,
      customer,
      taxtypet,
      taxcatt,
      taxable,
      taxtype,
      taxcat,
      to_date(effdate,'yyyymmdd'),
      rectype,
     '0',
     '0'
   FROM tb_bcp_cch_txmatrix
   WHERE batch_id = an_batch_id;*/

   /*DELETE
     FROM tb_bcp_cch_txmatrix
    WHERE batch_id = an_batch_id;*/

   -- commit all updates
   COMMIT;

   -- Enable triggers
   EXECUTE IMMEDIATE 'ALTER TRIGGER tb_jurisdiction_taxrate_a_i ENABLE';

EXCEPTION
   WHEN e_abort THEN
      -- Update batch with error codes
      UPDATE TB_BATCH
         SET batch_status_code = v_batch_status_code,
             error_sev_code = v_error_sev_code,
             actual_end_timestamp = SYS_EXTRACT_UTC(SYSTIMESTAMP)
       WHERE batch_id = an_batch_id;
      COMMIT;
      -- Enable triggers
      EXECUTE IMMEDIATE 'ALTER TRIGGER tb_jurisdiction_taxrate_a_i ENABLE';
   WHEN e_halt THEN
      NULL;
END;
/
