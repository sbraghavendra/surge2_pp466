create or replace
PROCEDURE sp_calcusetax(
/* ************************************************************************************************/
/* Object Type/Name: Stored Procedure - sp_calcusetax                                             */
/* Author:           Michael B. Fuller                                                            */
/* Date:             a long time ago                                                              */
/* Description:      calculate sales or use taxes                                                 */
/* Arguments:        many - see "IN"s below                                                       */
/* Returns:          many = see "OUT"s below                                                      */
/* ************************************************************************************************/
/* Maintenance History:                                                                           */
/* Scan  Author       Date     Release    Description                                  Ticket #   */
/* ----- ---------- ---------- ---------- -------------------------------------------- ---------- */
/*       M. Fuller  06/02/2006 3.3.5.0    Fix processing an allocated line - Temporary 871        */
/*       M. Fuller  01/04/2007 3.3.5.1    Fix processing an allocated line - Permenant 871        */
/* MBF01 M. Fuller  04/12/2007 3.3.7.x    Change check for 'TARGA' to be in the                   */
/*                                        tb_options table so it does not have to be              */
/*                                        hard-coded.                                             */
/* MBF02 M. Fuller  10/12/2007 3.3.8.x    Correct tax calcs for negative dist. amt.s   904        */
/* MBF03 M. Fuller  08/13/2007 3.4.1.1    Add checkboxes to toggle calc of 5 rates     883        */
/* MBF04 M. Fuller  05/04/2011            Add Country for Federal Tax Level            24         */
/* MBF05 M. Fuller  08/12/2011            Add TaxCode Rules changes                    17         */
/* ************************************************************************************************/
   an_gl_line_itm_dist_amt IN NUMBER,
   av_taxtype_code IN VARCHAR2,
   ac_country_flag IN CHAR,
   ac_state_flag IN CHAR,
   ac_county_flag IN CHAR,
   ac_county_local_flag IN CHAR,
   ac_city_flag IN CHAR,
   ac_city_local_flag IN CHAR,
   an_state_taxable_threshold_amt IN NUMBER,
   an_state_tax_limitation_amt IN NUMBER,
   an_state_cap_amt IN NUMBER,
   an_state_base_change_pct IN NUMBER,
   an_state_special_rate IN NUMBER,
   an_cnty_taxable_threshold_amt IN NUMBER,
   an_cnty_tax_limitation_amt IN NUMBER,
   an_cnty_cap_amt IN NUMBER,
   an_cnty_base_change_pct IN NUMBER,
   an_cnty_special_rate IN NUMBER,
   an_city_taxable_threshold_amt IN NUMBER,
   an_city_tax_limitation_amt IN NUMBER,
   an_city_cap_amt IN NUMBER,
   an_city_base_change_pct IN NUMBER,
   an_city_special_rate IN NUMBER,

   an_country_rate IN OUT NUMBER,
   an_state_rate IN OUT NUMBER,
   an_state_use_tier2_rate IN OUT NUMBER,
   an_state_use_tier3_rate IN OUT NUMBER,
   an_state_split_amount IN OUT NUMBER,
   an_state_tier2_min_amount IN OUT NUMBER,
   an_state_tier2_max_amount IN OUT NUMBER,
   an_state_maxtax_amount IN OUT NUMBER,
   an_county_rate IN OUT NUMBER,
   an_county_local_rate IN OUT NUMBER,
   an_county_split_amount IN OUT NUMBER,
   an_county_maxtax_amount IN OUT NUMBER,
   ac_county_single_flag IN OUT CHAR,
   ac_county_default_flag IN OUT CHAR,
   an_city_rate IN OUT NUMBER,
   an_city_local_rate IN OUT NUMBER,
   an_city_split_amount IN OUT NUMBER,
   an_city_split_rate IN OUT NUMBER,
   ac_city_single_flag IN OUT CHAR,
   ac_city_default_flag IN OUT CHAR,

   an_country_amount OUT NUMBER,
   an_state_taxable_amt OUT NUMBER,
   an_state_amount OUT NUMBER,
   an_state_use_tier2_amount OUT NUMBER,
   an_state_use_tier3_amount OUT NUMBER,
   an_county_taxable_amt OUT NUMBER,
   an_county_amount OUT NUMBER,
   an_county_local_amount OUT NUMBER,
   an_city_taxable_amt OUT NUMBER,
   an_city_amount OUT NUMBER,
   an_city_local_amount OUT NUMBER,
   an_tb_calc_tax_amt OUT NUMBER)
IS
   v_state_use_tier1_rate          tb_jurisdiction_taxrate.state_use_rate%TYPE       := 0;
   v_state_use_tier2_rate          tb_jurisdiction_taxrate.state_use_tier2_rate%TYPE := 0;
   v_state_use_tier3_rate          tb_jurisdiction_taxrate.state_use_tier3_rate%TYPE := 0;
   v_state_tier1_dist_amt          tb_transaction_detail.gl_line_itm_dist_amt%TYPE   := 0;
   v_state_tier2_dist_amt          tb_transaction_detail.gl_line_itm_dist_amt%TYPE   := 0;
   v_state_tier3_dist_amt          tb_transaction_detail.gl_line_itm_dist_amt%TYPE   := 0;
   v_state_tier1_max_amount        tb_jurisdiction_taxrate.state_tier2_min_amount%TYPE := 0;
   v_state_tier2_min_amount        tb_jurisdiction_taxrate.state_tier2_min_amount%TYPE := 0;
   v_state_tier2_max_amount        tb_jurisdiction_taxrate.state_tier2_max_amount%TYPE := 0;
   v_state_tier3_min_amount        tb_jurisdiction_taxrate.state_tier2_max_amount%TYPE := 0;
   v_state_maxtax_amount           tb_jurisdiction_taxrate.state_maxtax_amount%TYPE  := 0;

   -- TEMPORARY
   --v_sub_tax_flags varchar(100);
   --v_sub_sep_flags varchar(100);
   --vc_value varchar(100);

-- ****** Program starts ****** --------------------------------------------------------------------
BEGIN
   -- TEMPORARY -- Hard Code Flags
/*   BEGIN
      SELECT value
        INTO vc_value
        FROM tb_option
       WHERE UPPER(option_code) = 'PWDENYMSG'
         AND UPPER(option_type_code) = 'SYSTEM'
         AND UPPER(user_code) = 'SYSTEM';
   EXCEPTION
     WHEN OTHERS THEN
        vc_value := 'NoThInG';
   END;
--   IF av_import_definition_code = 'TARGA' THEN
   IF SUBSTR(UPPER(TRIM(vc_value)),1,5) = 'TARGA' THEN
      SELECT DECODE(av_transaction_state_code,'LA','EE','TE')
        INTO v_sub_tax_flags
        FROM DUAL;
      v_sub_sep_flags := 'EI';
   ELSE
      v_sub_tax_flags := 'XX';
      v_sub_sep_flags := 'XX';
   END IF;

   -- Add/Subtract Freight/Discount
   SELECT an_gl_line_itm_dist_amt +
         (DECODE(SUBSTR(v_sub_sep_flags,1,1)||SUBSTR(v_sub_tax_flags,1,1),'IE',NVL(an_invoice_freight_amt,0) * -1,'ET',NVL(an_invoice_freight_amt,0),0)) +
         (DECODE(SUBSTR(v_sub_sep_flags,2,1)||SUBSTR(v_sub_tax_flags,2,1),'IE',NVL(an_invoice_discount_amt,0) * -1,'ET',NVL(an_invoice_discount_amt,0),0))
     INTO an_taxable_amt
     FROM DUAL; *** MBF05 ***/

   /*  Determine Taxable Amount for each level  *******************************/
   -- Base Change Percent - State - from TaxCode
   an_state_taxable_amt := an_gl_line_itm_dist_amt;
   IF an_state_base_change_pct IS NOT NULL AND an_state_base_change_pct <> 1.00 AND an_state_base_change_pct <> 0.00 THEN
      an_state_taxable_amt := an_state_taxable_amt * an_state_base_change_pct;
   END IF;
   -- Base Change Percent - County - from TaxCode
   an_county_taxable_amt := an_gl_line_itm_dist_amt;
   IF an_cnty_base_change_pct IS NOT NULL AND an_cnty_base_change_pct <> 1.00 AND an_cnty_base_change_pct <> 0.00 THEN
      an_county_taxable_amt := an_county_taxable_amt * an_cnty_base_change_pct;
   END IF;
   -- Base Change Percent - City - from TaxCode
   an_city_taxable_amt := an_gl_line_itm_dist_amt;
   IF an_city_base_change_pct IS NOT NULL AND an_city_base_change_pct <> 1.00 AND an_city_base_change_pct <> 0.00 THEN
      an_city_taxable_amt := an_city_taxable_amt * an_city_base_change_pct;
   END IF;

   -- Cap - State - from TaxCode
   IF an_state_cap_amt IS NOT NULL AND an_state_cap_amt > 0 THEN
      IF an_state_cap_amt < ABS(an_state_taxable_amt) THEN
         an_state_taxable_amt := an_state_taxable_amt - (an_state_cap_amt * SIGN(an_state_taxable_amt));
      END IF;
   END IF;
   -- Cap - County - from TaxCode
   IF an_cnty_cap_amt IS NOT NULL AND an_cnty_cap_amt > 0 THEN
      IF an_cnty_cap_amt < ABS(an_county_taxable_amt) THEN
         an_county_taxable_amt := an_county_taxable_amt - (an_cnty_cap_amt * SIGN(an_county_taxable_amt));
      END IF;
   END IF;
   -- Cap - City - from TaxCode
   IF an_city_cap_amt IS NOT NULL AND an_city_cap_amt > 0 THEN
      IF an_city_cap_amt < ABS(an_city_taxable_amt) THEN
         an_city_taxable_amt := an_city_taxable_amt - (an_city_cap_amt * SIGN(an_city_taxable_amt));
      END IF;
   END IF;

   /*  Determine State Tiers  *************************************************/
   -- Special rate - State - from TaxCode - overrides tiers
   IF an_state_special_rate IS NOT NULL AND an_state_special_rate > 0.00 THEN
      v_state_use_tier1_rate := an_state_special_rate;
      v_state_use_tier2_rate := 0.00;
      v_state_use_tier3_rate := 0.00;
      v_state_tier1_max_amount := 999999999;
      v_state_tier1_dist_amt := NVL(an_state_taxable_amt,0);
   ELSE
      -- Tier 1 Variables --
      v_state_use_tier1_rate := NVL(an_state_rate,0);
      v_state_tier1_max_amount := NVL(an_state_split_amount,999999999);
      IF v_state_tier1_max_amount = 0 THEN
         v_state_tier1_max_amount := 999999999;
      END IF;
      v_state_tier1_dist_amt := NVL(an_state_taxable_amt,0);

      -- Tier 2 Variables --
      v_state_use_tier2_rate := NVL(an_state_use_tier2_rate,0);
      IF v_state_tier1_max_amount < 999999999 THEN
         v_state_tier2_min_amount := v_state_tier1_max_amount;
      ELSE
         v_state_tier2_min_amount := NVL(an_state_tier2_min_amount,0);
      END IF;
      v_state_tier2_max_amount := NVL(an_state_tier2_max_amount,999999999);
      IF v_state_tier2_max_amount = 0 THEN
         v_state_tier2_max_amount := 999999999;
      END IF;
      IF v_state_tier2_max_amount < 999999999 THEN
         v_state_tier2_max_amount := v_state_tier2_max_amount - v_state_tier2_min_amount;
      END IF;
      v_state_tier2_dist_amt := GREATEST((ABS(v_state_tier1_dist_amt) - v_state_tier2_min_amount),0) * SIGN(v_state_tier1_dist_amt);

      -- Tier 3 Variables --
      v_state_use_tier3_rate := NVL(an_state_use_tier3_rate,0);
      v_state_tier3_min_amount := NVL(an_state_tier2_max_amount,999999999);
      v_state_tier3_dist_amt := GREATEST((ABS(v_state_tier1_dist_amt) - v_state_tier3_min_amount),0) * SIGN(v_state_tier1_dist_amt);
   END IF;

   -- MaxTax Amount --
   v_state_maxtax_amount := NVL(an_state_maxtax_amount,999999999);
   IF v_state_maxtax_amount = 0 THEN
      v_state_maxtax_amount := 999999999;
   END IF;

   /*  Initialize Variables  **************************************************/
   an_country_amount := 0;
   an_state_amount := 0;
   an_state_use_tier2_amount := 0;
   an_state_use_tier3_amount := 0;
   an_county_amount := 0;
   an_county_local_amount := 0;
   an_city_amount := 0;
   an_city_local_amount := 0;
   an_tb_calc_tax_amt := 0;

   /*  Calculate Country (Federal) Taxes  *************************************/    -- MBF04
   IF ac_country_flag = '1' THEN
      -- Calculate Country Sales/Use Tax Amount
      an_country_amount := an_gl_line_itm_dist_amt * an_country_rate;
   END IF;

   /*  Calculate State Taxes  *************************************************/
   IF ac_state_flag = '1' THEN
      -- Tier 1
      IF v_state_use_tier1_rate > 0 THEN
         IF v_state_tier1_max_amount < ABS(v_state_tier1_dist_amt) THEN
            an_state_amount := (v_state_tier1_max_amount * v_state_use_tier1_rate) * SIGN(v_state_tier1_dist_amt);
         ELSE
            an_state_amount := v_state_tier1_dist_amt * v_state_use_tier1_rate;
         END IF;
      END IF;
      -- Tier 2
      IF v_state_use_tier2_rate > 0 THEN
         IF v_state_tier2_max_amount < ABS(v_state_tier2_dist_amt) THEN
            an_state_use_tier2_amount := (v_state_tier2_max_amount * v_state_use_tier2_rate) * SIGN(v_state_tier2_dist_amt);
         ELSE
            an_state_use_tier2_amount := v_state_tier2_dist_amt * v_state_use_tier2_rate;
         END IF;
      END IF;
      -- Tier 3
      IF v_state_use_tier3_rate > 0 THEN
         an_state_use_tier3_amount := v_state_tier3_dist_amt * v_state_use_tier3_rate;
      END IF;

-- ***>>>  Should these be mutually exclusive??????

      -- Taxable Amount Threshold
      IF an_state_taxable_threshold_amt IS NOT NULL AND an_state_taxable_threshold_amt > 0 THEN
         IF ABS(an_state_taxable_amt) <= an_state_taxable_threshold_amt THEN
            an_state_amount := 0;
            an_state_use_tier2_amount := 0;
            an_state_use_tier3_amount := 0;
         END IF;
      END IF;

-- ***>>> should tax limitation amount only apply to single tier1 rates, like MAXTAX does?????

      -- Tax Amount Limitation - from TaxCode - overrides MAXTAX
      IF an_state_tax_limitation_amt IS NOT NULL AND an_state_tax_limitation_amt > 0 THEN
         IF an_state_tax_limitation_amt < ABS(an_state_amount) THEN
            an_state_amount := an_state_tax_limitation_amt * SIGN(an_state_amount);
         END IF;
      ELSE
         -- MAXTAX Amount - from Tax Rate
         IF v_state_tier1_max_amount = 999999999 AND v_state_use_tier2_rate = 0 AND v_state_use_tier3_rate = 0 THEN
            IF v_state_maxtax_amount > 0 THEN
               IF v_state_maxtax_amount < ABS(an_state_amount) THEN
                  an_state_amount := v_state_maxtax_amount * SIGN(an_state_amount);
               END IF;
            END IF;
         END IF;
      END IF;
   END IF;
-- INSERT INTO tb_debug(row_joe) VAUES('State Tier 1 = ' || to_char(an_state_use_amount));
-- INSERT INTO tb_debug(row_joe) VAUES('State Tier 2 = ' || to_char(an_state_use_tier2_amount));
-- INSERT INTO tb_debug(row_joe) VAUES('State Tier 3 = ' || to_char(an_state_use_tier3_amount));

   /*  Calculate County Taxes  ************************************************/
   IF ac_county_flag = '1' THEN
      -- Special Rate - County - from TaxCode
      IF an_cnty_special_rate IS NOT NULL AND an_cnty_special_rate > 0.00 THEN
         an_county_rate := an_cnty_special_rate;
      END IF;
      -- Calculate County Sales/Use Tax Amount
      IF an_county_split_amount > 0 THEN
         IF an_county_split_amount < ABS(an_county_taxable_amt) THEN
            an_county_amount := (an_county_split_amount * an_county_rate) * SIGN(an_county_taxable_amt);
         ELSE
            an_county_amount := an_county_taxable_amt * an_county_rate;
         END IF;
      ELSE
         an_county_amount := an_county_taxable_amt * an_county_rate;
      END IF;

      -- Taxable Amount Threshold
      IF an_cnty_taxable_threshold_amt IS NOT NULL AND an_cnty_taxable_threshold_amt > 0 THEN
         IF ABS(an_county_taxable_amt) <= an_cnty_taxable_threshold_amt THEN
            an_county_amount := 0;
         END IF;
      END IF;

      -- Tax Amount Limitation - from TaxCode - overrides MAXTAX
      IF an_cnty_tax_limitation_amt IS NOT NULL AND an_cnty_tax_limitation_amt > 0 THEN
         IF an_cnty_tax_limitation_amt < ABS(an_county_amount) THEN
            an_county_amount := an_cnty_tax_limitation_amt * SIGN(an_county_amount);
         END IF;
      ELSE
         -- Determine MAXTAX Amount --
         IF an_county_maxtax_amount > 0 THEN
            IF an_county_maxtax_amount < ABS(an_county_amount) THEN
               an_county_amount := an_county_maxtax_amount * SIGN(an_county_amount);
            END IF;
         END IF;
      END IF;
   END IF;

   /*  Calculate County Local Taxes  ******************************************/
   IF ac_county_local_flag = '1' THEN
      -- Calculate County Local Sales/Use Tax Amount
      an_county_local_amount := an_county_taxable_amt * an_county_local_rate;
   END IF;

   /*  Calculate City Taxes  **************************************************/
   IF ac_city_flag = '1' THEN
      -- special rate overrides tiers
      IF an_city_special_rate IS NOT NULL AND an_city_special_rate > 0.00 THEN
         an_city_rate := an_city_special_rate;
         an_city_split_amount := 0;
         an_city_split_rate := 0.00;
      END IF;
      -- Calculate City Sales/Use Tax Amount
      IF an_city_split_amount > 0 THEN
         IF an_city_split_amount < ABS(an_city_taxable_amt) THEN
            IF an_city_split_rate > 0 THEN
               an_city_amount :=
                  ROUND((an_city_split_amount * an_city_rate) +
                     ((ABS(an_city_taxable_amt) - an_city_split_amount) * an_city_split_rate),2) * SIGN(an_city_taxable_amt);
            ELSE
               an_city_amount := (an_city_split_amount * an_city_rate) * SIGN(an_city_taxable_amt);
            END IF;
         ELSE
            an_city_amount := an_city_taxable_amt * an_city_rate;
         END IF;
      ELSE
         an_city_amount := an_city_taxable_amt * an_city_rate;
      END IF;
   END IF;

   /*  Calculate City Local Taxes  ********************************************/
   IF ac_city_local_flag = '1' THEN
      -- Calculate City Local Sales/Use Tax Amount
      an_city_local_amount := an_city_taxable_amt * an_city_local_rate;
   END IF;

   /*  Calculate Total Taxes  *************************************************/
   -- Calculate Total Tax Amount
   an_tb_calc_tax_amt := an_country_amount +
                         an_state_amount +
                         an_state_use_tier2_amount +
                         an_state_use_tier3_amount +
                         an_county_amount +
                         an_county_local_amount +
                         an_city_amount +
                         an_city_local_amount;
END;
