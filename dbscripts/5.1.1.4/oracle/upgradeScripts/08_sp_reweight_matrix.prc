create or replace
PROCEDURE sp_reweight_matrix
/* ************************************************************************************************/
/* Object Type/Name: Stored Procedure - sp_reweight_matrix                                        */
/* Author:           Michael B. Fuller                                                            */
/* Date:             a long time ago                                                              */
/* Description:      reweight all weighted matrices                                               */
/* Arguments:        None                                                                         */
/* Returns:          None                                                                         */
/* ************************************************************************************************/
/* Maintenance History:                                                                           */
/* Scan  Author       Date     Release    Description                                  Ticket #   */
/* ----- ---------- ---------- ---------- -------------------------------------------- ---------- */
/* MBF01 M. Fuller  08/22/2011            Add Tax & Jurisdiction Allocation Matrices              */
/* ************************************************************************************************/
IS
-- ****** Program starts ****** --------------------------------------------------------------------
BEGIN
-- Update binary weight for normal tax matrix lines
UPDATE tb_tax_matrix
   SET binary_weight =
         DECODE(NVL(driver_30,'*ALL'),'*ALL',0,power(2,0)) +
         DECODE(NVL(driver_29,'*ALL'),'*ALL',0,power(2,1)) +
         DECODE(NVL(driver_28,'*ALL'),'*ALL',0,power(2,2)) +
         DECODE(NVL(driver_27,'*ALL'),'*ALL',0,power(2,3)) +
         DECODE(NVL(driver_26,'*ALL'),'*ALL',0,power(2,4)) +
         DECODE(NVL(driver_25,'*ALL'),'*ALL',0,power(2,5)) +
         DECODE(NVL(driver_24,'*ALL'),'*ALL',0,power(2,6)) +
         DECODE(NVL(driver_23,'*ALL'),'*ALL',0,power(2,7)) +
         DECODE(NVL(driver_22,'*ALL'),'*ALL',0,power(2,8)) +
         DECODE(NVL(driver_21,'*ALL'),'*ALL',0,power(2,9)) +
         DECODE(NVL(driver_20,'*ALL'),'*ALL',0,power(2,10)) +
         DECODE(NVL(driver_19,'*ALL'),'*ALL',0,power(2,11)) +
         DECODE(NVL(driver_18,'*ALL'),'*ALL',0,power(2,12)) +
         DECODE(NVL(driver_17,'*ALL'),'*ALL',0,power(2,13)) +
         DECODE(NVL(driver_16,'*ALL'),'*ALL',0,power(2,14)) +
         DECODE(NVL(driver_15,'*ALL'),'*ALL',0,power(2,15)) +
         DECODE(NVL(driver_14,'*ALL'),'*ALL',0,power(2,16)) +
         DECODE(NVL(driver_13,'*ALL'),'*ALL',0,power(2,17)) +
         DECODE(NVL(driver_12,'*ALL'),'*ALL',0,power(2,18)) +
         DECODE(NVL(driver_11,'*ALL'),'*ALL',0,power(2,19)) +
         DECODE(NVL(driver_10,'*ALL'),'*ALL',0,power(2,20)) +
         DECODE(NVL(driver_09,'*ALL'),'*ALL',0,power(2,21)) +
         DECODE(NVL(driver_08,'*ALL'),'*ALL',0,power(2,22)) +
         DECODE(NVL(driver_07,'*ALL'),'*ALL',0,power(2,23)) +
         DECODE(NVL(driver_06,'*ALL'),'*ALL',0,power(2,24)) +
         DECODE(NVL(driver_05,'*ALL'),'*ALL',0,power(2,25)) +
         DECODE(NVL(driver_04,'*ALL'),'*ALL',0,power(2,26)) +
         DECODE(NVL(driver_03,'*ALL'),'*ALL',0,power(2,27)) +
         DECODE(NVL(driver_02,'*ALL'),'*ALL',0,power(2,28)) +
         DECODE(NVL(driver_01,'*ALL'),'*ALL',0,power(2,29)) +
         DECODE(driver_global_flag,'1',power(2,30),0),
      default_binary_weight = 0
 WHERE default_flag is NULL OR default_flag <> '1';

-- Update significant digits for normal tax matrix lines
UPDATE tb_tax_matrix
   SET significant_digits =
      TRIM(DECODE(INSTR(NVL(driver_01,' '),'%'),0,DECODE(NVL(driver_01,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(driver_01,' '),'%')-1,'000'))||'.') ||
           DECODE(INSTR(NVL(driver_02,' '),'%'),0,DECODE(NVL(driver_02,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(driver_02,' '),'%')-1,'000'))||'.') ||
           DECODE(INSTR(NVL(driver_03,' '),'%'),0,DECODE(NVL(driver_03,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(driver_03,' '),'%')-1,'000'))||'.') ||
           DECODE(INSTR(NVL(driver_04,' '),'%'),0,DECODE(NVL(driver_04,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(driver_04,' '),'%')-1,'000'))||'.') ||
           DECODE(INSTR(NVL(driver_05,' '),'%'),0,DECODE(NVL(driver_05,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(driver_05,' '),'%')-1,'000'))||'.') ||
           DECODE(INSTR(NVL(driver_06,' '),'%'),0,DECODE(NVL(driver_06,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(driver_06,' '),'%')-1,'000'))||'.') ||
           DECODE(INSTR(NVL(driver_07,' '),'%'),0,DECODE(NVL(driver_07,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(driver_07,' '),'%')-1,'000'))||'.') ||
           DECODE(INSTR(NVL(driver_08,' '),'%'),0,DECODE(NVL(driver_08,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(driver_08,' '),'%')-1,'000'))||'.') ||
           DECODE(INSTR(NVL(driver_09,' '),'%'),0,DECODE(NVL(driver_09,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(driver_09,' '),'%')-1,'000'))||'.') ||
           DECODE(INSTR(NVL(driver_10,' '),'%'),0,DECODE(NVL(driver_10,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(driver_10,' '),'%')-1,'000'))||'.') ||
           DECODE(INSTR(NVL(driver_11,' '),'%'),0,DECODE(NVL(driver_11,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(driver_11,' '),'%')-1,'000'))||'.') ||
           DECODE(INSTR(NVL(driver_12,' '),'%'),0,DECODE(NVL(driver_12,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(driver_12,' '),'%')-1,'000'))||'.') ||
           DECODE(INSTR(NVL(driver_13,' '),'%'),0,DECODE(NVL(driver_13,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(driver_13,' '),'%')-1,'000'))||'.') ||
           DECODE(INSTR(NVL(driver_14,' '),'%'),0,DECODE(NVL(driver_14,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(driver_14,' '),'%')-1,'000'))||'.') ||
           DECODE(INSTR(NVL(driver_15,' '),'%'),0,DECODE(NVL(driver_15,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(driver_15,' '),'%')-1,'000'))||'.') ||
           DECODE(INSTR(NVL(driver_16,' '),'%'),0,DECODE(NVL(driver_16,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(driver_16,' '),'%')-1,'000'))||'.') ||
           DECODE(INSTR(NVL(driver_17,' '),'%'),0,DECODE(NVL(driver_17,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(driver_17,' '),'%')-1,'000'))||'.') ||
           DECODE(INSTR(NVL(driver_18,' '),'%'),0,DECODE(NVL(driver_18,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(driver_18,' '),'%')-1,'000'))||'.') ||
           DECODE(INSTR(NVL(driver_19,' '),'%'),0,DECODE(NVL(driver_19,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(driver_19,' '),'%')-1,'000'))||'.') ||
           DECODE(INSTR(NVL(driver_20,' '),'%'),0,DECODE(NVL(driver_20,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(driver_20,' '),'%')-1,'000'))||'.') ||
           DECODE(INSTR(NVL(driver_21,' '),'%'),0,DECODE(NVL(driver_21,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(driver_21,' '),'%')-1,'000'))||'.') ||
           DECODE(INSTR(NVL(driver_22,' '),'%'),0,DECODE(NVL(driver_22,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(driver_22,' '),'%')-1,'000'))||'.') ||
           DECODE(INSTR(NVL(driver_23,' '),'%'),0,DECODE(NVL(driver_23,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(driver_23,' '),'%')-1,'000'))||'.') ||
           DECODE(INSTR(NVL(driver_24,' '),'%'),0,DECODE(NVL(driver_24,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(driver_24,' '),'%')-1,'000'))||'.') ||
           DECODE(INSTR(NVL(driver_25,' '),'%'),0,DECODE(NVL(driver_25,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(driver_25,' '),'%')-1,'000'))||'.') ||
           DECODE(INSTR(NVL(driver_26,' '),'%'),0,DECODE(NVL(driver_26,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(driver_26,' '),'%')-1,'000'))||'.') ||
           DECODE(INSTR(NVL(driver_27,' '),'%'),0,DECODE(NVL(driver_27,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(driver_27,' '),'%')-1,'000'))||'.') ||
           DECODE(INSTR(NVL(driver_28,' '),'%'),0,DECODE(NVL(driver_28,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(driver_28,' '),'%')-1,'000'))||'.') ||
           DECODE(INSTR(NVL(driver_29,' '),'%'),0,DECODE(NVL(driver_29,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(driver_29,' '),'%')-1,'000'))||'.') ||
           DECODE(INSTR(NVL(driver_30,' '),'%'),0,DECODE(NVL(driver_30,'*ALL'),'*ALL','000','999'),TRIM(TO_CHAR(INSTR(NVL(driver_30,' '),'%')-1,'000')))),
       default_significant_digits = NULL
 WHERE default_flag is NULL OR default_flag <> '1';

-- Update binary weight for default tax matrix lines
UPDATE tb_tax_matrix
   SET default_binary_weight =
         DECODE(NVL(driver_30,'*ALL'),'*ALL',0,power(2,0)) +
         DECODE(NVL(driver_29,'*ALL'),'*ALL',0,power(2,1)) +
         DECODE(NVL(driver_28,'*ALL'),'*ALL',0,power(2,2)) +
         DECODE(NVL(driver_27,'*ALL'),'*ALL',0,power(2,3)) +
         DECODE(NVL(driver_26,'*ALL'),'*ALL',0,power(2,4)) +
         DECODE(NVL(driver_25,'*ALL'),'*ALL',0,power(2,5)) +
         DECODE(NVL(driver_24,'*ALL'),'*ALL',0,power(2,6)) +
         DECODE(NVL(driver_23,'*ALL'),'*ALL',0,power(2,7)) +
         DECODE(NVL(driver_22,'*ALL'),'*ALL',0,power(2,8)) +
         DECODE(NVL(driver_21,'*ALL'),'*ALL',0,power(2,9)) +
         DECODE(NVL(driver_20,'*ALL'),'*ALL',0,power(2,10)) +
         DECODE(NVL(driver_19,'*ALL'),'*ALL',0,power(2,11)) +
         DECODE(NVL(driver_18,'*ALL'),'*ALL',0,power(2,12)) +
         DECODE(NVL(driver_17,'*ALL'),'*ALL',0,power(2,13)) +
         DECODE(NVL(driver_16,'*ALL'),'*ALL',0,power(2,14)) +
         DECODE(NVL(driver_15,'*ALL'),'*ALL',0,power(2,15)) +
         DECODE(NVL(driver_14,'*ALL'),'*ALL',0,power(2,16)) +
         DECODE(NVL(driver_13,'*ALL'),'*ALL',0,power(2,17)) +
         DECODE(NVL(driver_12,'*ALL'),'*ALL',0,power(2,18)) +
         DECODE(NVL(driver_11,'*ALL'),'*ALL',0,power(2,19)) +
         DECODE(NVL(driver_10,'*ALL'),'*ALL',0,power(2,20)) +
         DECODE(NVL(driver_09,'*ALL'),'*ALL',0,power(2,21)) +
         DECODE(NVL(driver_08,'*ALL'),'*ALL',0,power(2,22)) +
         DECODE(NVL(driver_07,'*ALL'),'*ALL',0,power(2,23)) +
         DECODE(NVL(driver_06,'*ALL'),'*ALL',0,power(2,24)) +
         DECODE(NVL(driver_05,'*ALL'),'*ALL',0,power(2,25)) +
         DECODE(NVL(driver_04,'*ALL'),'*ALL',0,power(2,26)) +
         DECODE(NVL(driver_03,'*ALL'),'*ALL',0,power(2,27)) +
         DECODE(NVL(driver_02,'*ALL'),'*ALL',0,power(2,28)) +
         DECODE(NVL(driver_01,'*ALL'),'*ALL',0,power(2,29)) +
         DECODE(driver_global_flag,'1',power(2,30),0),
      binary_weight = 0
 WHERE default_flag = '1';

-- Update significant digits for default tax matrix lines
UPDATE tb_tax_matrix
   SET default_significant_digits =
      TRIM(DECODE(INSTR(NVL(driver_01,' '),'%'),0,DECODE(NVL(driver_01,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(driver_01,' '),'%')-1,'000'))||'.') ||
           DECODE(INSTR(NVL(driver_02,' '),'%'),0,DECODE(NVL(driver_02,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(driver_02,' '),'%')-1,'000'))||'.') ||
           DECODE(INSTR(NVL(driver_03,' '),'%'),0,DECODE(NVL(driver_03,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(driver_03,' '),'%')-1,'000'))||'.') ||
           DECODE(INSTR(NVL(driver_04,' '),'%'),0,DECODE(NVL(driver_04,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(driver_04,' '),'%')-1,'000'))||'.') ||
           DECODE(INSTR(NVL(driver_05,' '),'%'),0,DECODE(NVL(driver_05,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(driver_05,' '),'%')-1,'000'))||'.') ||
           DECODE(INSTR(NVL(driver_06,' '),'%'),0,DECODE(NVL(driver_06,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(driver_06,' '),'%')-1,'000'))||'.') ||
           DECODE(INSTR(NVL(driver_07,' '),'%'),0,DECODE(NVL(driver_07,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(driver_07,' '),'%')-1,'000'))||'.') ||
           DECODE(INSTR(NVL(driver_08,' '),'%'),0,DECODE(NVL(driver_08,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(driver_08,' '),'%')-1,'000'))||'.') ||
           DECODE(INSTR(NVL(driver_09,' '),'%'),0,DECODE(NVL(driver_09,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(driver_09,' '),'%')-1,'000'))||'.') ||
           DECODE(INSTR(NVL(driver_10,' '),'%'),0,DECODE(NVL(driver_10,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(driver_10,' '),'%')-1,'000'))||'.') ||
           DECODE(INSTR(NVL(driver_11,' '),'%'),0,DECODE(NVL(driver_11,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(driver_11,' '),'%')-1,'000'))||'.') ||
           DECODE(INSTR(NVL(driver_12,' '),'%'),0,DECODE(NVL(driver_12,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(driver_12,' '),'%')-1,'000'))||'.') ||
           DECODE(INSTR(NVL(driver_13,' '),'%'),0,DECODE(NVL(driver_13,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(driver_13,' '),'%')-1,'000'))||'.') ||
           DECODE(INSTR(NVL(driver_14,' '),'%'),0,DECODE(NVL(driver_14,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(driver_14,' '),'%')-1,'000'))||'.') ||
           DECODE(INSTR(NVL(driver_15,' '),'%'),0,DECODE(NVL(driver_15,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(driver_15,' '),'%')-1,'000'))||'.') ||
           DECODE(INSTR(NVL(driver_16,' '),'%'),0,DECODE(NVL(driver_16,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(driver_16,' '),'%')-1,'000'))||'.') ||
           DECODE(INSTR(NVL(driver_17,' '),'%'),0,DECODE(NVL(driver_17,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(driver_17,' '),'%')-1,'000'))||'.') ||
           DECODE(INSTR(NVL(driver_18,' '),'%'),0,DECODE(NVL(driver_18,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(driver_18,' '),'%')-1,'000'))||'.') ||
           DECODE(INSTR(NVL(driver_19,' '),'%'),0,DECODE(NVL(driver_19,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(driver_19,' '),'%')-1,'000'))||'.') ||
           DECODE(INSTR(NVL(driver_20,' '),'%'),0,DECODE(NVL(driver_20,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(driver_20,' '),'%')-1,'000'))||'.') ||
           DECODE(INSTR(NVL(driver_21,' '),'%'),0,DECODE(NVL(driver_21,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(driver_21,' '),'%')-1,'000'))||'.') ||
           DECODE(INSTR(NVL(driver_22,' '),'%'),0,DECODE(NVL(driver_22,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(driver_22,' '),'%')-1,'000'))||'.') ||
           DECODE(INSTR(NVL(driver_23,' '),'%'),0,DECODE(NVL(driver_23,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(driver_23,' '),'%')-1,'000'))||'.') ||
           DECODE(INSTR(NVL(driver_24,' '),'%'),0,DECODE(NVL(driver_24,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(driver_24,' '),'%')-1,'000'))||'.') ||
           DECODE(INSTR(NVL(driver_25,' '),'%'),0,DECODE(NVL(driver_25,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(driver_25,' '),'%')-1,'000'))||'.') ||
           DECODE(INSTR(NVL(driver_26,' '),'%'),0,DECODE(NVL(driver_26,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(driver_26,' '),'%')-1,'000'))||'.') ||
           DECODE(INSTR(NVL(driver_27,' '),'%'),0,DECODE(NVL(driver_27,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(driver_27,' '),'%')-1,'000'))||'.') ||
           DECODE(INSTR(NVL(driver_28,' '),'%'),0,DECODE(NVL(driver_28,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(driver_28,' '),'%')-1,'000'))||'.') ||
           DECODE(INSTR(NVL(driver_29,' '),'%'),0,DECODE(NVL(driver_29,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(driver_29,' '),'%')-1,'000'))||'.') ||
           DECODE(INSTR(NVL(driver_30,' '),'%'),0,DECODE(NVL(driver_30,'*ALL'),'*ALL','000','999'),TRIM(TO_CHAR(INSTR(NVL(driver_30,' '),'%')-1,'000')))),
      significant_digits = NULL
 WHERE default_flag = '1';

-- Update binary weight for normal location matrix lines
UPDATE tb_location_matrix
   SET binary_weight =
      DECODE(NVL(driver_10,'*ALL'),'*ALL',0,power(2,0)) +
      DECODE(NVL(driver_09,'*ALL'),'*ALL',0,power(2,1)) +
      DECODE(NVL(driver_08,'*ALL'),'*ALL',0,power(2,2)) +
      DECODE(NVL(driver_07,'*ALL'),'*ALL',0,power(2,3)) +
      DECODE(NVL(driver_06,'*ALL'),'*ALL',0,power(2,4)) +
      DECODE(NVL(driver_05,'*ALL'),'*ALL',0,power(2,5)) +
      DECODE(NVL(driver_04,'*ALL'),'*ALL',0,power(2,6)) +
      DECODE(NVL(driver_03,'*ALL'),'*ALL',0,power(2,7)) +
      DECODE(NVL(driver_02,'*ALL'),'*ALL',0,power(2,8)) +
      DECODE(NVL(driver_01,'*ALL'),'*ALL',0,power(2,9)),
   default_binary_weight = 0
 WHERE default_flag is NULL OR default_flag <> '1';

-- Update significant digits for normal location matrix lines
UPDATE tb_location_matrix
   Set significant_digits =
      TRIM(DECODE(INSTR(NVL(driver_01,' '),'%'),0,DECODE(NVL(driver_01,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(driver_01,' '),'%')-1,'000'))||'.') ||
           DECODE(INSTR(NVL(driver_02,' '),'%'),0,DECODE(NVL(driver_02,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(driver_02,' '),'%')-1,'000'))||'.') ||
           DECODE(INSTR(NVL(driver_03,' '),'%'),0,DECODE(NVL(driver_03,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(driver_03,' '),'%')-1,'000'))||'.') ||
           DECODE(INSTR(NVL(driver_04,' '),'%'),0,DECODE(NVL(driver_04,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(driver_04,' '),'%')-1,'000'))||'.') ||
           DECODE(INSTR(NVL(driver_05,' '),'%'),0,DECODE(NVL(driver_05,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(driver_05,' '),'%')-1,'000'))||'.') ||
           DECODE(INSTR(NVL(driver_06,' '),'%'),0,DECODE(NVL(driver_06,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(driver_06,' '),'%')-1,'000'))||'.') ||
           DECODE(INSTR(NVL(driver_07,' '),'%'),0,DECODE(NVL(driver_07,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(driver_07,' '),'%')-1,'000'))||'.') ||
           DECODE(INSTR(NVL(driver_08,' '),'%'),0,DECODE(NVL(driver_08,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(driver_08,' '),'%')-1,'000'))||'.') ||
           DECODE(INSTR(NVL(driver_09,' '),'%'),0,DECODE(NVL(driver_09,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(driver_09,' '),'%')-1,'000'))||'.') ||
           DECODE(INSTR(NVL(driver_10,' '),'%'),0,DECODE(NVL(driver_10,'*ALL'),'*ALL','000','999'),TRIM(TO_CHAR(INSTR(NVL(driver_10,' '),'%')-1,'000')))),
       default_significant_digits = NULL
 WHERE default_flag is NULL OR default_flag <> '1';

-- Update binary weight for default tax matrix lines
UPDATE tb_location_matrix
   SET default_binary_weight =
      DECODE(NVL(driver_10,'*ALL'),'*ALL',0,power(2,0)) +
      DECODE(NVL(driver_09,'*ALL'),'*ALL',0,power(2,1)) +
      DECODE(NVL(driver_08,'*ALL'),'*ALL',0,power(2,2)) +
      DECODE(NVL(driver_07,'*ALL'),'*ALL',0,power(2,3)) +
      DECODE(NVL(driver_06,'*ALL'),'*ALL',0,power(2,4)) +
      DECODE(NVL(driver_05,'*ALL'),'*ALL',0,power(2,5)) +
      DECODE(NVL(driver_04,'*ALL'),'*ALL',0,power(2,6)) +
      DECODE(NVL(driver_03,'*ALL'),'*ALL',0,power(2,7)) +
      DECODE(NVL(driver_02,'*ALL'),'*ALL',0,power(2,8)) +
      DECODE(NVL(driver_01,'*ALL'),'*ALL',0,power(2,9)),
   binary_weight = 0
 WHERE default_flag = '1';

-- Update significant digits for default tax matrix lines
UPDATE tb_location_matrix
   Set default_significant_digits =
      TRIM(DECODE(INSTR(NVL(driver_01,' '),'%'),0,DECODE(NVL(driver_01,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(driver_01,' '),'%')-1,'000'))||'.') ||
           DECODE(INSTR(NVL(driver_02,' '),'%'),0,DECODE(NVL(driver_02,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(driver_02,' '),'%')-1,'000'))||'.') ||
           DECODE(INSTR(NVL(driver_03,' '),'%'),0,DECODE(NVL(driver_03,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(driver_03,' '),'%')-1,'000'))||'.') ||
           DECODE(INSTR(NVL(driver_04,' '),'%'),0,DECODE(NVL(driver_04,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(driver_04,' '),'%')-1,'000'))||'.') ||
           DECODE(INSTR(NVL(driver_05,' '),'%'),0,DECODE(NVL(driver_05,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(driver_05,' '),'%')-1,'000'))||'.') ||
           DECODE(INSTR(NVL(driver_06,' '),'%'),0,DECODE(NVL(driver_06,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(driver_06,' '),'%')-1,'000'))||'.') ||
           DECODE(INSTR(NVL(driver_07,' '),'%'),0,DECODE(NVL(driver_07,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(driver_07,' '),'%')-1,'000'))||'.') ||
           DECODE(INSTR(NVL(driver_08,' '),'%'),0,DECODE(NVL(driver_08,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(driver_08,' '),'%')-1,'000'))||'.') ||
           DECODE(INSTR(NVL(driver_09,' '),'%'),0,DECODE(NVL(driver_09,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(driver_09,' '),'%')-1,'000'))||'.') ||
           DECODE(INSTR(NVL(driver_10,' '),'%'),0,DECODE(NVL(driver_10,'*ALL'),'*ALL','000','999'),TRIM(TO_CHAR(INSTR(NVL(driver_10,' '),'%')-1,'000')))),
       significant_digits = NULL
 WHERE default_flag = '1';

-- Update binary weight for jurisdiction allocation lines
UPDATE tb_allocation_matrix
   SET binary_weight =
         DECODE(NVL(driver_30,'*ALL'),'*ALL',0,power(2,0)) +
         DECODE(NVL(driver_29,'*ALL'),'*ALL',0,power(2,1)) +
         DECODE(NVL(driver_28,'*ALL'),'*ALL',0,power(2,2)) +
         DECODE(NVL(driver_27,'*ALL'),'*ALL',0,power(2,3)) +
         DECODE(NVL(driver_26,'*ALL'),'*ALL',0,power(2,4)) +
         DECODE(NVL(driver_25,'*ALL'),'*ALL',0,power(2,5)) +
         DECODE(NVL(driver_24,'*ALL'),'*ALL',0,power(2,6)) +
         DECODE(NVL(driver_23,'*ALL'),'*ALL',0,power(2,7)) +
         DECODE(NVL(driver_22,'*ALL'),'*ALL',0,power(2,8)) +
         DECODE(NVL(driver_21,'*ALL'),'*ALL',0,power(2,9)) +
         DECODE(NVL(driver_20,'*ALL'),'*ALL',0,power(2,10)) +
         DECODE(NVL(driver_19,'*ALL'),'*ALL',0,power(2,11)) +
         DECODE(NVL(driver_18,'*ALL'),'*ALL',0,power(2,12)) +
         DECODE(NVL(driver_17,'*ALL'),'*ALL',0,power(2,13)) +
         DECODE(NVL(driver_16,'*ALL'),'*ALL',0,power(2,14)) +
         DECODE(NVL(driver_15,'*ALL'),'*ALL',0,power(2,15)) +
         DECODE(NVL(driver_14,'*ALL'),'*ALL',0,power(2,16)) +
         DECODE(NVL(driver_13,'*ALL'),'*ALL',0,power(2,17)) +
         DECODE(NVL(driver_12,'*ALL'),'*ALL',0,power(2,18)) +
         DECODE(NVL(driver_11,'*ALL'),'*ALL',0,power(2,19)) +
         DECODE(NVL(driver_10,'*ALL'),'*ALL',0,power(2,20)) +
         DECODE(NVL(driver_09,'*ALL'),'*ALL',0,power(2,21)) +
         DECODE(NVL(driver_08,'*ALL'),'*ALL',0,power(2,22)) +
         DECODE(NVL(driver_07,'*ALL'),'*ALL',0,power(2,23)) +
         DECODE(NVL(driver_06,'*ALL'),'*ALL',0,power(2,24)) +
         DECODE(NVL(driver_05,'*ALL'),'*ALL',0,power(2,25)) +
         DECODE(NVL(driver_04,'*ALL'),'*ALL',0,power(2,26)) +
         DECODE(NVL(driver_03,'*ALL'),'*ALL',0,power(2,27)) +
         DECODE(NVL(driver_02,'*ALL'),'*ALL',0,power(2,28)) +
         DECODE(NVL(driver_01,'*ALL'),'*ALL',0,power(2,29));
 
-- Update significant digits for jurisdiction allocation lines
UPDATE tb_allocation_matrix
   SET significant_digits =
      TRIM(DECODE(INSTR(NVL(driver_01,' '),'%'),0,DECODE(NVL(driver_01,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(driver_01,' '),'%')-1,'000'))||'.') ||
           DECODE(INSTR(NVL(driver_02,' '),'%'),0,DECODE(NVL(driver_02,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(driver_02,' '),'%')-1,'000'))||'.') ||
           DECODE(INSTR(NVL(driver_03,' '),'%'),0,DECODE(NVL(driver_03,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(driver_03,' '),'%')-1,'000'))||'.') ||
           DECODE(INSTR(NVL(driver_04,' '),'%'),0,DECODE(NVL(driver_04,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(driver_04,' '),'%')-1,'000'))||'.') ||
           DECODE(INSTR(NVL(driver_05,' '),'%'),0,DECODE(NVL(driver_05,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(driver_05,' '),'%')-1,'000'))||'.') ||
           DECODE(INSTR(NVL(driver_06,' '),'%'),0,DECODE(NVL(driver_06,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(driver_06,' '),'%')-1,'000'))||'.') ||
           DECODE(INSTR(NVL(driver_07,' '),'%'),0,DECODE(NVL(driver_07,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(driver_07,' '),'%')-1,'000'))||'.') ||
           DECODE(INSTR(NVL(driver_08,' '),'%'),0,DECODE(NVL(driver_08,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(driver_08,' '),'%')-1,'000'))||'.') ||
           DECODE(INSTR(NVL(driver_09,' '),'%'),0,DECODE(NVL(driver_09,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(driver_09,' '),'%')-1,'000'))||'.') ||
           DECODE(INSTR(NVL(driver_10,' '),'%'),0,DECODE(NVL(driver_10,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(driver_10,' '),'%')-1,'000'))||'.') ||
           DECODE(INSTR(NVL(driver_11,' '),'%'),0,DECODE(NVL(driver_11,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(driver_11,' '),'%')-1,'000'))||'.') ||
           DECODE(INSTR(NVL(driver_12,' '),'%'),0,DECODE(NVL(driver_12,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(driver_12,' '),'%')-1,'000'))||'.') ||
           DECODE(INSTR(NVL(driver_13,' '),'%'),0,DECODE(NVL(driver_13,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(driver_13,' '),'%')-1,'000'))||'.') ||
           DECODE(INSTR(NVL(driver_14,' '),'%'),0,DECODE(NVL(driver_14,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(driver_14,' '),'%')-1,'000'))||'.') ||
           DECODE(INSTR(NVL(driver_15,' '),'%'),0,DECODE(NVL(driver_15,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(driver_15,' '),'%')-1,'000'))||'.') ||
           DECODE(INSTR(NVL(driver_16,' '),'%'),0,DECODE(NVL(driver_16,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(driver_16,' '),'%')-1,'000'))||'.') ||
           DECODE(INSTR(NVL(driver_17,' '),'%'),0,DECODE(NVL(driver_17,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(driver_17,' '),'%')-1,'000'))||'.') ||
           DECODE(INSTR(NVL(driver_18,' '),'%'),0,DECODE(NVL(driver_18,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(driver_18,' '),'%')-1,'000'))||'.') ||
           DECODE(INSTR(NVL(driver_19,' '),'%'),0,DECODE(NVL(driver_19,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(driver_19,' '),'%')-1,'000'))||'.') ||
           DECODE(INSTR(NVL(driver_20,' '),'%'),0,DECODE(NVL(driver_20,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(driver_20,' '),'%')-1,'000'))||'.') ||
           DECODE(INSTR(NVL(driver_21,' '),'%'),0,DECODE(NVL(driver_21,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(driver_21,' '),'%')-1,'000'))||'.') ||
           DECODE(INSTR(NVL(driver_22,' '),'%'),0,DECODE(NVL(driver_22,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(driver_22,' '),'%')-1,'000'))||'.') ||
           DECODE(INSTR(NVL(driver_23,' '),'%'),0,DECODE(NVL(driver_23,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(driver_23,' '),'%')-1,'000'))||'.') ||
           DECODE(INSTR(NVL(driver_24,' '),'%'),0,DECODE(NVL(driver_24,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(driver_24,' '),'%')-1,'000'))||'.') ||
           DECODE(INSTR(NVL(driver_25,' '),'%'),0,DECODE(NVL(driver_25,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(driver_25,' '),'%')-1,'000'))||'.') ||
           DECODE(INSTR(NVL(driver_26,' '),'%'),0,DECODE(NVL(driver_26,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(driver_26,' '),'%')-1,'000'))||'.') ||
           DECODE(INSTR(NVL(driver_27,' '),'%'),0,DECODE(NVL(driver_27,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(driver_27,' '),'%')-1,'000'))||'.') ||
           DECODE(INSTR(NVL(driver_28,' '),'%'),0,DECODE(NVL(driver_28,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(driver_28,' '),'%')-1,'000'))||'.') ||
           DECODE(INSTR(NVL(driver_29,' '),'%'),0,DECODE(NVL(driver_29,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(driver_29,' '),'%')-1,'000'))||'.') ||
           DECODE(INSTR(NVL(driver_30,' '),'%'),0,DECODE(NVL(driver_30,'*ALL'),'*ALL','000','999'),TRIM(TO_CHAR(INSTR(NVL(driver_30,' '),'%')-1,'000'))));

-- Update binary weight for tax allocation lines
UPDATE tb_tax_alloc_matrix
   SET binary_weight =
         DECODE(NVL(driver_30,'*ALL'),'*ALL',0,power(2,0)) +
         DECODE(NVL(driver_29,'*ALL'),'*ALL',0,power(2,1)) +
         DECODE(NVL(driver_28,'*ALL'),'*ALL',0,power(2,2)) +
         DECODE(NVL(driver_27,'*ALL'),'*ALL',0,power(2,3)) +
         DECODE(NVL(driver_26,'*ALL'),'*ALL',0,power(2,4)) +
         DECODE(NVL(driver_25,'*ALL'),'*ALL',0,power(2,5)) +
         DECODE(NVL(driver_24,'*ALL'),'*ALL',0,power(2,6)) +
         DECODE(NVL(driver_23,'*ALL'),'*ALL',0,power(2,7)) +
         DECODE(NVL(driver_22,'*ALL'),'*ALL',0,power(2,8)) +
         DECODE(NVL(driver_21,'*ALL'),'*ALL',0,power(2,9)) +
         DECODE(NVL(driver_20,'*ALL'),'*ALL',0,power(2,10)) +
         DECODE(NVL(driver_19,'*ALL'),'*ALL',0,power(2,11)) +
         DECODE(NVL(driver_18,'*ALL'),'*ALL',0,power(2,12)) +
         DECODE(NVL(driver_17,'*ALL'),'*ALL',0,power(2,13)) +
         DECODE(NVL(driver_16,'*ALL'),'*ALL',0,power(2,14)) +
         DECODE(NVL(driver_15,'*ALL'),'*ALL',0,power(2,15)) +
         DECODE(NVL(driver_14,'*ALL'),'*ALL',0,power(2,16)) +
         DECODE(NVL(driver_13,'*ALL'),'*ALL',0,power(2,17)) +
         DECODE(NVL(driver_12,'*ALL'),'*ALL',0,power(2,18)) +
         DECODE(NVL(driver_11,'*ALL'),'*ALL',0,power(2,19)) +
         DECODE(NVL(driver_10,'*ALL'),'*ALL',0,power(2,20)) +
         DECODE(NVL(driver_09,'*ALL'),'*ALL',0,power(2,21)) +
         DECODE(NVL(driver_08,'*ALL'),'*ALL',0,power(2,22)) +
         DECODE(NVL(driver_07,'*ALL'),'*ALL',0,power(2,23)) +
         DECODE(NVL(driver_06,'*ALL'),'*ALL',0,power(2,24)) +
         DECODE(NVL(driver_05,'*ALL'),'*ALL',0,power(2,25)) +
         DECODE(NVL(driver_04,'*ALL'),'*ALL',0,power(2,26)) +
         DECODE(NVL(driver_03,'*ALL'),'*ALL',0,power(2,27)) +
         DECODE(NVL(driver_02,'*ALL'),'*ALL',0,power(2,28)) +
         DECODE(NVL(driver_01,'*ALL'),'*ALL',0,power(2,29));
 
-- Update significant digits for tax allocation lines
UPDATE tb_tax_alloc_matrix
   SET significant_digits =
      TRIM(DECODE(INSTR(NVL(driver_01,' '),'%'),0,DECODE(NVL(driver_01,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(driver_01,' '),'%')-1,'000'))||'.') ||
           DECODE(INSTR(NVL(driver_02,' '),'%'),0,DECODE(NVL(driver_02,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(driver_02,' '),'%')-1,'000'))||'.') ||
           DECODE(INSTR(NVL(driver_03,' '),'%'),0,DECODE(NVL(driver_03,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(driver_03,' '),'%')-1,'000'))||'.') ||
           DECODE(INSTR(NVL(driver_04,' '),'%'),0,DECODE(NVL(driver_04,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(driver_04,' '),'%')-1,'000'))||'.') ||
           DECODE(INSTR(NVL(driver_05,' '),'%'),0,DECODE(NVL(driver_05,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(driver_05,' '),'%')-1,'000'))||'.') ||
           DECODE(INSTR(NVL(driver_06,' '),'%'),0,DECODE(NVL(driver_06,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(driver_06,' '),'%')-1,'000'))||'.') ||
           DECODE(INSTR(NVL(driver_07,' '),'%'),0,DECODE(NVL(driver_07,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(driver_07,' '),'%')-1,'000'))||'.') ||
           DECODE(INSTR(NVL(driver_08,' '),'%'),0,DECODE(NVL(driver_08,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(driver_08,' '),'%')-1,'000'))||'.') ||
           DECODE(INSTR(NVL(driver_09,' '),'%'),0,DECODE(NVL(driver_09,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(driver_09,' '),'%')-1,'000'))||'.') ||
           DECODE(INSTR(NVL(driver_10,' '),'%'),0,DECODE(NVL(driver_10,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(driver_10,' '),'%')-1,'000'))||'.') ||
           DECODE(INSTR(NVL(driver_11,' '),'%'),0,DECODE(NVL(driver_11,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(driver_11,' '),'%')-1,'000'))||'.') ||
           DECODE(INSTR(NVL(driver_12,' '),'%'),0,DECODE(NVL(driver_12,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(driver_12,' '),'%')-1,'000'))||'.') ||
           DECODE(INSTR(NVL(driver_13,' '),'%'),0,DECODE(NVL(driver_13,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(driver_13,' '),'%')-1,'000'))||'.') ||
           DECODE(INSTR(NVL(driver_14,' '),'%'),0,DECODE(NVL(driver_14,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(driver_14,' '),'%')-1,'000'))||'.') ||
           DECODE(INSTR(NVL(driver_15,' '),'%'),0,DECODE(NVL(driver_15,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(driver_15,' '),'%')-1,'000'))||'.') ||
           DECODE(INSTR(NVL(driver_16,' '),'%'),0,DECODE(NVL(driver_16,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(driver_16,' '),'%')-1,'000'))||'.') ||
           DECODE(INSTR(NVL(driver_17,' '),'%'),0,DECODE(NVL(driver_17,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(driver_17,' '),'%')-1,'000'))||'.') ||
           DECODE(INSTR(NVL(driver_18,' '),'%'),0,DECODE(NVL(driver_18,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(driver_18,' '),'%')-1,'000'))||'.') ||
           DECODE(INSTR(NVL(driver_19,' '),'%'),0,DECODE(NVL(driver_19,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(driver_19,' '),'%')-1,'000'))||'.') ||
           DECODE(INSTR(NVL(driver_20,' '),'%'),0,DECODE(NVL(driver_20,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(driver_20,' '),'%')-1,'000'))||'.') ||
           DECODE(INSTR(NVL(driver_21,' '),'%'),0,DECODE(NVL(driver_21,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(driver_21,' '),'%')-1,'000'))||'.') ||
           DECODE(INSTR(NVL(driver_22,' '),'%'),0,DECODE(NVL(driver_22,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(driver_22,' '),'%')-1,'000'))||'.') ||
           DECODE(INSTR(NVL(driver_23,' '),'%'),0,DECODE(NVL(driver_23,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(driver_23,' '),'%')-1,'000'))||'.') ||
           DECODE(INSTR(NVL(driver_24,' '),'%'),0,DECODE(NVL(driver_24,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(driver_24,' '),'%')-1,'000'))||'.') ||
           DECODE(INSTR(NVL(driver_25,' '),'%'),0,DECODE(NVL(driver_25,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(driver_25,' '),'%')-1,'000'))||'.') ||
           DECODE(INSTR(NVL(driver_26,' '),'%'),0,DECODE(NVL(driver_26,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(driver_26,' '),'%')-1,'000'))||'.') ||
           DECODE(INSTR(NVL(driver_27,' '),'%'),0,DECODE(NVL(driver_27,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(driver_27,' '),'%')-1,'000'))||'.') ||
           DECODE(INSTR(NVL(driver_28,' '),'%'),0,DECODE(NVL(driver_28,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(driver_28,' '),'%')-1,'000'))||'.') ||
           DECODE(INSTR(NVL(driver_29,' '),'%'),0,DECODE(NVL(driver_29,'*ALL'),'*ALL','000.','999.'),TRIM(TO_CHAR(INSTR(NVL(driver_29,' '),'%')-1,'000'))||'.') ||
           DECODE(INSTR(NVL(driver_30,' '),'%'),0,DECODE(NVL(driver_30,'*ALL'),'*ALL','000','999'),TRIM(TO_CHAR(INSTR(NVL(driver_30,' '),'%')-1,'000'))));
END;

 
 
 
 