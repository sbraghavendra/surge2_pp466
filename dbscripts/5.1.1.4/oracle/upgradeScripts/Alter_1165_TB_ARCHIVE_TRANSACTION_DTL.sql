ALTER TABLE STSCORP.TB_ARCHIVE_TRANSACTION_DTL
   ADD ( split_subtrans_id NUMBER )
   ADD (multi_trans_code   varchar2(10))
   ADD (tax_alloc_matrix_id NUMBER);
