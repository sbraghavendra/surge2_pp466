create or replace
PROCEDURE sp_tb_taxcode_detail_a_i (
   p_taxcode_detail_id         IN     tb_taxcode_detail.taxcode_detail_id%TYPE)
/* ************************************************************************************************/
/* Object Type/Name: Stored Procedure (After Insert) - sp_tb_taxcode_detail_a_i                   */
/* Author:           Michael B. Fuller                                                            */
/* Date:             09/21/2011                                                                   */
/* Description:      A TaxCode Detail line has been added                                         */
/* Arguments:        p_taxcode_detail_id(tb_taxcode_detail.taxcode_detail_id%TYPE)                */
/* Returns:          None                                                                         */
/* ************************************************************************************************/
/* Maintenance History:                                                                           */
/* Scan  Author       Date     Release    Description                                  Ticket #   */
/* ----- ---------- ---------- ---------- -------------------------------------------- ---------- */
/* ************************************************************************************************/
AS
-- Defire Row Type for tb_taxcode_detail record
   l_tb_taxcode_detail             tb_taxcode_detail%ROWTYPE;

-- Define Cursor Type
   TYPE cursor_type IS REF CURSOR;

-- Transaction Detail Selection SQL
   vc_transaction_detail_select    VARCHAR2(2000) :=
      'SELECT tb_transaction_detail.* ' ||
      'FROM tb_tmp_taxcode_detail, tb_transaction_detail, tb_jurisdiction ' ||
      'WHERE tb_tmp_taxcode_detail.taxcode_detail_id = ' || p_taxcode_detail_id || ' ' ||
      'AND tb_tmp_taxcode_detail.taxcode_code = tb_transaction_detail.taxcode_code ' ||
      'AND tb_transaction_detail.jurisdiction_id = tb_jurisdiction.jurisdiction_id ' ||
      'AND tb_tmp_taxcode_detail.effective_date <= tb_transaction_detail.gl_date AND tb_tmp_taxcode_detail.expiration_date >= tb_transaction_detail.gl_date ' ||
      'AND tb_transaction_detail.transaction_ind = ''S'' AND tb_transaction_detail.suspend_ind = ''D'' ' ||
      'AND tb_jurisdiction.country = tb_tmp_taxcode_detail.taxcode_country_code ' ||
      'AND tb_jurisdiction.state = tb_tmp_taxcode_detail.taxcode_state_code ';
   vc_transaction_detail_where     VARCHAR2(28000) := '';
   vc_transaction_detail_update    VARCHAR2(100) := 'FOR UPDATE';
   vc_transaction_detail_stmt      VARCHAR2(30100);
   transaction_detail_cursor       cursor_type;
   transaction_detail              tb_transaction_detail%ROWTYPE;

-- TaxCode Detail Selection SQL
   vc_taxcode_detail_stmt           VARCHAR2(31000) :=
      'SELECT taxcode_detail_id, taxcode_type_code, override_taxtype_code, ratetype_code, taxable_threshold_amt, ' ||
             'tax_limitation_amt, cap_amt, base_change_pct, special_rate ' ||
        'FROM tb_taxcode_detail ' ||
       'WHERE tb_taxcode_detail.active_flag = ''1'' ' ||
         'AND tb_taxcode_detail.taxcode_code = :taxcode_code ' ||
         'AND tb_taxcode_detail.taxcode_country_code = :country ' ||
         'AND tb_taxcode_detail.taxcode_state_code = :state ' ||
         'AND tb_taxcode_detail.taxcode_county = :county ' ||
         'AND tb_taxcode_detail.taxcode_city = :city ' ||
         'AND tb_taxcode_detail.effective_date <= :gl_date ' ||
         'AND tb_taxcode_detail.expiration_date >= :gl_date ' ||
      'ORDER BY tb_taxcode_detail.effective_date DESC';
   taxcode_detail_cursor           cursor_type;

-- TaxCode Detail Record TYPE
   TYPE taxcode_detail_record is RECORD (
      taxcode_detail_id            tb_taxcode_detail.taxcode_detail_id%TYPE,
      taxcode_type_code            tb_taxcode_detail.taxcode_type_code%TYPE,
      override_taxtype_code        tb_taxcode_detail.override_taxtype_code%TYPE,
      ratetype_code                tb_taxcode_detail.ratetype_code%TYPE,
      taxable_threshold_amt        tb_taxcode_detail.taxable_threshold_amt%TYPE,
      tax_limitation_amt           tb_taxcode_detail.tax_limitation_amt%TYPE,
      cap_amt                      tb_taxcode_detail.cap_amt%TYPE,
      base_change_pct              tb_taxcode_detail.base_change_pct%TYPE,
      special_rate                 tb_taxcode_detail.special_rate%TYPE);
   taxcode_detail                  taxcode_detail_record;

-- Table defined variables
   v_transaction_country           tb_taxcode_detail.taxcode_country_code%TYPE       := NULL;
   v_transaction_state             tb_taxcode_detail.taxcode_state_code%TYPE         := NULL;
   v_transaction_county            tb_taxcode_detail.taxcode_county%TYPE             := NULL;
   v_transaction_city              tb_taxcode_detail.taxcode_city%TYPE               := NULL;
   v_less_code                     tb_taxcode_detail.taxcode_type_code%TYPE          := NULL;
   v_equal_code                    tb_taxcode_detail.taxcode_type_code%TYPE          := NULL;
   v_greater_code                  tb_taxcode_detail.taxcode_type_code%TYPE          := NULL;
   v_relation_code                 tb_taxcode_detail.taxcode_type_code%TYPE          := NULL;
   v_relation_amount               TB_TAX_MATRIX.relation_amount%TYPE                := NULL;
   v_hold_code_flag                tb_tax_matrix.then_hold_code_flag%TYPE            := NULL;
   v_state_override_taxtype_code   tb_taxcode_detail.override_taxtype_code%TYPE      := NULL;
   v_state_ratetype_code           tb_taxcode_detail.ratetype_code%TYPE              := NULL;
   v_state_taxable_threshold_amt   tb_taxcode_detail.taxable_threshold_amt%TYPE      := 0;
   v_county_taxable_threshold_amt  tb_taxcode_detail.taxable_threshold_amt%TYPE      := 0;
   v_city_taxable_threshold_amt    tb_taxcode_detail.taxable_threshold_amt%TYPE      := 0;
   v_state_tax_limitation_amt      tb_taxcode_detail.tax_limitation_amt%TYPE         := 0;
   v_county_tax_limitation_amt     tb_taxcode_detail.tax_limitation_amt%TYPE         := 0;
   v_city_tax_limitation_amt       tb_taxcode_detail.tax_limitation_amt%TYPE         := 0;
   v_state_cap_amt                 tb_taxcode_detail.cap_amt%TYPE                    := 0;
   v_county_cap_amt                tb_taxcode_detail.cap_amt%TYPE                    := 0;
   v_city_cap_amt                  tb_taxcode_detail.cap_amt%TYPE                    := 0;
   v_state_base_change_pct         tb_taxcode_detail.base_change_pct%TYPE            := 0;
   v_county_base_change_pct        tb_taxcode_detail.base_change_pct%TYPE            := 0;
   v_city_base_change_pct          tb_taxcode_detail.base_change_pct%TYPE            := 0;
   v_state_special_rate            tb_taxcode_detail.special_rate%TYPE               := 0;
   v_county_special_rate           tb_taxcode_detail.special_rate%TYPE               := 0;
   v_city_special_rate             tb_taxcode_detail.special_rate%TYPE               := 0;
   --v_override_jurisdiction_id      tb_taxcode_detail.jurisdiction_id%TYPE            := 0;
   v_transaction_detail_id         TB_TRANSACTION_DETAIL.transaction_detail_id%TYPE  := NULL;
   v_sysdate                       TB_TRANSACTION_DETAIL.load_timestamp%TYPE         := SYS_EXTRACT_UTC(SYSTIMESTAMP);

-- Program defined variables
   vc_state_driver_flag            CHAR(1)                                           := NULL;
   vn_state_rows                   NUMBER                                            := 0;
   vn_county_rows                  NUMBER                                            := 0;
   vn_city_rows                    NUMBER                                            := 0;
   vc_country_flag                 CHAR(1)                                           := '0';
   vc_state_flag                   CHAR(1)                                           := '0';
   vc_county_flag                  CHAR(1)                                           := '0';
   vc_county_local_flag            CHAR(1)                                           := '0';
   vc_city_flag                    CHAR(1)                                           := '0';
   vc_city_local_flag              CHAR(1)                                           := '0';

-- Define Exceptions
   e_badread                       EXCEPTION;
   e_badupdate                     EXCEPTION;

-- ****** Program starts ****** --------------------------------------------------------------------
BEGIN
   -- Populate row type with TaxCode Detail record
   SELECT tb_taxcode_detail.*
     INTO l_tb_taxcode_detail
     FROM tb_taxcode_detail
    WHERE taxcode_detail_id = p_taxcode_detail_id;

   -- Add County or City to SQL if needed
   IF l_tb_taxcode_detail.taxcode_county <> '*ALL' THEN
      vc_transaction_detail_where := 'AND tb_jurisdiction.county = tb_tmp_taxcode_detail.taxcode_county ';
   ELSIF l_tb_taxcode_detail.taxcode_city <> '*ALL' THEN
      vc_transaction_detail_where := 'AND tb_jurisdiction.city = tb_tmp_taxcode_detail.taxcode_city ';
   END IF;
   vc_transaction_detail_stmt := vc_transaction_detail_select || vc_transaction_detail_where || vc_transaction_detail_update;

   -- Insert New TaxCode Detail columns into temporary table -------------------------------------------
   INSERT INTO tb_tmp_taxcode_detail (
      taxcode_detail_id,
      taxcode_code,
      taxcode_country_code,
      taxcode_state_code,
      taxcode_county,
      taxcode_city,
      effective_date,
      expiration_date,
      taxcode_type_code,
      override_taxtype_code,
      ratetype_code,
      taxable_threshold_amt,
      tax_limitation_amt,
      cap_amt,
      base_change_pct,
      special_rate,
      active_flag,
      custom_flag,
      modified_flag,
      jurisdiction_id,
      update_user_id,
      update_timestamp)
   VALUES (
      l_tb_taxcode_detail.taxcode_detail_id,
      l_tb_taxcode_detail.taxcode_code,
      l_tb_taxcode_detail.taxcode_country_code,
      l_tb_taxcode_detail.taxcode_state_code,
      l_tb_taxcode_detail.taxcode_county,
      l_tb_taxcode_detail.taxcode_city,
      l_tb_taxcode_detail.effective_date,
      l_tb_taxcode_detail.expiration_date,
      l_tb_taxcode_detail.taxcode_type_code,
      l_tb_taxcode_detail.override_taxtype_code,
      l_tb_taxcode_detail.ratetype_code,
      l_tb_taxcode_detail.taxable_threshold_amt,
      l_tb_taxcode_detail.tax_limitation_amt,
      l_tb_taxcode_detail.cap_amt,
      l_tb_taxcode_detail.base_change_pct,
      l_tb_taxcode_detail.special_rate,
      l_tb_taxcode_detail.active_flag,
      l_tb_taxcode_detail.custom_flag,
      l_tb_taxcode_detail.modified_flag,
      l_tb_taxcode_detail.jurisdiction_id,
      l_tb_taxcode_detail.update_user_id,
      l_tb_taxcode_detail.update_timestamp);

   -- ****** Read and Process Transactions ****** -----------------------------------------
   OPEN transaction_detail_cursor
    FOR vc_transaction_detail_stmt;
   LOOP
      FETCH transaction_detail_cursor
       INTO transaction_detail;
       EXIT WHEN transaction_detail_cursor%NOTFOUND;

      -- Populate and/or Clear Transaction Detail working fields
      transaction_detail.transaction_ind := NULL;
      transaction_detail.suspend_ind := NULL;
      transaction_detail.update_user_id := USER;
      transaction_detail.update_timestamp := v_sysdate;

      -- Get jurisdiction info
      BEGIN
         SELECT country, state, county, city
           INTO v_transaction_country,
                v_transaction_state,
                v_transaction_county,
                v_transaction_city
           FROM tb_jurisdiction
          WHERE tb_jurisdiction.jurisdiction_id = transaction_detail.jurisdiction_id;
      END;

      -- ****** Get TaxCode Details ****** --
      transaction_detail.state_taxcode_type_code := NULL;
      transaction_detail.county_taxcode_type_code := NULL;
      transaction_detail.city_taxcode_type_code := NULL;
      -- Get Country/State/all County/all City for defaults
      vn_state_rows := 0;
      BEGIN
          OPEN taxcode_detail_cursor
           FOR vc_taxcode_detail_stmt
         USING transaction_detail.taxcode_code, v_transaction_country, v_transaction_state, '*ALL', '*ALL', transaction_detail.gl_date, transaction_detail.gl_date;
         FETCH taxcode_detail_cursor
          INTO taxcode_detail;
         -- Rows found?
         IF taxcode_detail_cursor%FOUND THEN
            vn_state_rows := taxcode_detail_cursor%ROWCOUNT;
         ELSE
            vn_state_rows := 0;
         END IF;
         CLOSE taxcode_detail_cursor;
         IF SQLCODE != 0 THEN
            vn_state_rows := 0;
         END IF;
      END;
      IF vn_state_rows > 0 THEN
         transaction_detail.state_taxcode_detail_id := taxcode_detail.taxcode_detail_id;
         transaction_detail.state_taxcode_type_code := taxcode_detail.taxcode_type_code;
         v_state_override_taxtype_code := taxcode_detail.override_taxtype_code;
         v_state_ratetype_code := taxcode_detail.ratetype_code;
         v_state_taxable_threshold_amt := taxcode_detail.taxable_threshold_amt;
         v_state_tax_limitation_amt := taxcode_detail.tax_limitation_amt;
         v_state_cap_amt := taxcode_detail.cap_amt;
         v_state_base_change_pct := taxcode_detail.base_change_pct;
         v_state_special_rate := taxcode_detail.special_rate;
         transaction_detail.county_taxcode_type_code := taxcode_detail.taxcode_type_code;
         v_county_taxable_threshold_amt := taxcode_detail.taxable_threshold_amt;
         v_county_tax_limitation_amt := taxcode_detail.tax_limitation_amt;
         v_county_cap_amt := taxcode_detail.cap_amt;
         v_county_base_change_pct := taxcode_detail.base_change_pct;
         v_county_special_rate := taxcode_detail.special_rate;
         transaction_detail.city_taxcode_type_code := taxcode_detail.taxcode_type_code;
         v_city_taxable_threshold_amt := taxcode_detail.taxable_threshold_amt;
         v_city_tax_limitation_amt := taxcode_detail.tax_limitation_amt;
         v_city_cap_amt := taxcode_detail.cap_amt;
         v_city_base_change_pct := taxcode_detail.base_change_pct;
         v_city_special_rate := taxcode_detail.special_rate;
         -- Get Country/State/County/all City for County
         vn_county_rows := 0;
         BEGIN
             OPEN taxcode_detail_cursor
              FOR vc_taxcode_detail_stmt
            USING transaction_detail.taxcode_code, v_transaction_country, v_transaction_state, v_transaction_county, '*ALL', transaction_detail.gl_date, transaction_detail.gl_date;
            FETCH taxcode_detail_cursor
             INTO taxcode_detail;
            -- Rows found?
            IF taxcode_detail_cursor%FOUND THEN
               vn_county_rows := taxcode_detail_cursor%ROWCOUNT;
            ELSE
               vn_county_rows := 0;
            END IF;
            CLOSE taxcode_detail_cursor;
            IF SQLCODE != 0 THEN
               vn_county_rows := 0;
            END IF;
         END;
         IF vn_county_rows > 0 THEN
            transaction_detail.county_taxcode_detail_id := taxcode_detail.taxcode_detail_id;
            transaction_detail.county_taxcode_type_code := taxcode_detail.taxcode_type_code;
            v_county_taxable_threshold_amt := taxcode_detail.taxable_threshold_amt;
            v_county_tax_limitation_amt := taxcode_detail.tax_limitation_amt;
            v_county_cap_amt := taxcode_detail.cap_amt;
            v_county_base_change_pct := taxcode_detail.base_change_pct;
            v_county_special_rate := taxcode_detail.special_rate;
         END IF;
         -- Get Country/State/all County/City for City
         vn_city_rows := 0;
         BEGIN
             OPEN taxcode_detail_cursor
              FOR vc_taxcode_detail_stmt
            USING transaction_detail.taxcode_code, v_transaction_country, v_transaction_state, '*ALL', v_transaction_city, transaction_detail.gl_date, transaction_detail.gl_date;
            FETCH taxcode_detail_cursor
             INTO taxcode_detail;
            -- Rows found?
            IF taxcode_detail_cursor%FOUND THEN
               vn_city_rows := taxcode_detail_cursor%ROWCOUNT;
            ELSE
               vn_city_rows := 0;
            END IF;
            CLOSE taxcode_detail_cursor;
            IF SQLCODE != 0 THEN
               vn_city_rows := 0;
            END IF;
         END;
         IF vn_city_rows > 0 THEN
            transaction_detail.city_taxcode_detail_id := taxcode_detail.taxcode_detail_id;
            transaction_detail.city_taxcode_type_code := taxcode_detail.taxcode_type_code;
            v_city_taxable_threshold_amt := taxcode_detail.taxable_threshold_amt;
            v_city_tax_limitation_amt := taxcode_detail.tax_limitation_amt;
            v_city_cap_amt := taxcode_detail.cap_amt;
            v_city_base_change_pct := taxcode_detail.base_change_pct;
            v_city_special_rate := taxcode_detail.special_rate;
         END IF;
      ELSE
         -- suspend for TaxCode Detail
         transaction_detail.transaction_ind := 'S';
         transaction_detail.suspend_ind := 'D';
      END IF;

      --- Continue if not suspended
      IF transaction_detail.transaction_ind IS NULL OR transaction_detail.transaction_ind <> 'S' THEN
         -- ****** DETERMINE TYPE of TAXCODE ****** --
         -- The TaxCode is Taxable
         IF transaction_detail.state_taxcode_type_code = 'T' OR transaction_detail.county_taxcode_type_code = 'T' OR transaction_detail.city_taxcode_type_code = 'T' THEN
            -- Determine Tax Calc Flags
            vc_country_flag := '1';
            vc_state_flag := '1';
            IF transaction_detail.state_taxcode_type_code = 'E' THEN
               vc_state_flag := '0';
            END IF;
            vc_county_flag := '1';
            vc_county_local_flag := '1';
            IF transaction_detail.county_taxcode_type_code = 'E' THEN
               vc_county_flag := '0';
               vc_county_local_flag := '1';
            END IF;
            vc_city_flag := '1';
            vc_city_local_flag := '1';
            IF transaction_detail.city_taxcode_type_code = 'E' THEN
               vc_city_flag := '0';
               vc_city_local_flag := '1';
            END IF;
            -- Get Jurisdiction TaxRates and calculate Tax Amounts
            sp_getusetax(transaction_detail.jurisdiction_id,
                         transaction_detail.gl_date,
                         transaction_detail.gl_line_itm_dist_amt,
                         'U',   -- Situs Tax Type code from Location Matrix when implemented.
                         vc_country_flag,   -- Situs Country Nexusflag when implemented
                         vc_state_flag,   -- Situs State Nexus flag when implemented
                         vc_county_flag,   -- Situs County Nexus flag when implemented
                         vc_county_local_flag,   -- Situs County Local nexus flag when implemented
                         vc_city_flag,   -- Situs City flag when implemented
                         vc_city_local_flag,   -- Situs City Local flag when implemented
                         v_state_ratetype_code,
                         v_state_override_taxtype_code,
                         v_state_taxable_threshold_amt,
                         v_state_tax_limitation_amt,
                         v_state_cap_amt,
                         v_state_base_change_pct,
                         v_state_special_rate,
                         v_county_taxable_threshold_amt,
                         v_county_tax_limitation_amt,
                         v_county_cap_amt,
                         v_county_base_change_pct,
                         v_county_special_rate,
                         v_city_taxable_threshold_amt,
                         v_city_tax_limitation_amt,
                         v_city_cap_amt,
                         v_city_base_change_pct,
                         v_city_special_rate,

                         transaction_detail.jurisdiction_taxrate_id,
                         transaction_detail.taxtype_used_code,
                         transaction_detail.country_use_amount,
                         transaction_detail.state_taxable_amt,
                         transaction_detail.state_use_amount,
                         transaction_detail.state_use_tier2_amount,
                         transaction_detail.state_use_tier3_amount,
                         transaction_detail.county_taxable_amt,
                         transaction_detail.county_use_amount,
                         transaction_detail.county_local_use_amount,
                         transaction_detail.city_taxable_amt,
                         transaction_detail.city_use_amount,
                         transaction_detail.city_local_use_amount,
                         transaction_detail.country_use_rate,
                         transaction_detail.state_use_rate,
                         transaction_detail.state_use_tier2_rate,
                         transaction_detail.state_use_tier3_rate,
                         transaction_detail.state_split_amount,
                         transaction_detail.state_tier2_min_amount,
                         transaction_detail.state_tier2_max_amount,
                         transaction_detail.state_maxtax_amount,
                         transaction_detail.county_use_rate,
                         transaction_detail.county_local_use_rate,
                         transaction_detail.county_split_amount,
                         transaction_detail.county_maxtax_amount,
                         transaction_detail.county_single_flag,
                         transaction_detail.county_default_flag,
                         transaction_detail.city_use_rate,
                         transaction_detail.city_local_use_rate,
                         transaction_detail.city_split_amount,
                         transaction_detail.city_split_use_rate,
                         transaction_detail.city_single_flag,
                         transaction_detail.city_default_flag,
                         transaction_detail.combined_use_rate,
                         transaction_detail.tb_calc_tax_amt);

            -- Suspend if tax rate id is null
            IF transaction_detail.jurisdiction_taxrate_id IS NULL OR transaction_detail.jurisdiction_taxrate_id = 0 THEN
               transaction_detail.transaction_ind := 'S';
               transaction_detail.suspend_ind := 'R';
            --ELSE
               -- Check for Taxable Amount -- 3351
               --IF vn_taxable_amt <> transaction_detail.gl_line_itm_dist_amt THEN
               --   transaction_detail.gl_extract_amt := transaction_detail.gl_line_itm_dist_amt;
               --   transaction_detail.gl_line_itm_dist_amt := vn_taxable_amt;
               --END IF;
            END IF;

         -- All TaxCodes are Exempt
         ELSIF transaction_detail.state_taxcode_type_code = 'E' AND transaction_detail.county_taxcode_type_code = 'E' AND transaction_detail.city_taxcode_type_code = 'E' THEN
            transaction_detail.transaction_ind := 'P';

         -- The Tax Code is Unrecognized - Suspend
         ELSE
            transaction_detail.transaction_ind := 'S';
            transaction_detail.suspend_ind := '?';
         END IF;

         -- Continue if not Suspended
         IF transaction_detail.transaction_ind IS NULL OR transaction_detail.transaction_ind <> 'S' THEN
            -- Check for matrix "H"old
            IF ( v_hold_code_flag = '1' ) AND
               ( transaction_detail.state_taxcode_type_code = 'T' OR transaction_detail.state_taxcode_type_code = 'E') THEN
                  transaction_detail.transaction_ind := 'H';
            ELSE
                transaction_detail.transaction_ind := 'P';
            END IF;
         END IF;
      END IF;

      -- Update transaction detail row
      BEGIN
         UPDATE tb_transaction_detail
            SET tb_calc_tax_amt = transaction_detail.tb_calc_tax_amt,
                country_use_amount = transaction_detail.country_use_amount,
                state_use_amount = transaction_detail.state_use_amount,
                state_use_tier2_amount = transaction_detail.state_use_tier2_amount,
                state_use_tier3_amount = transaction_detail.state_use_tier3_amount,
                county_use_amount = transaction_detail.county_use_amount,
                county_local_use_amount = transaction_detail.county_local_use_amount,
                city_use_amount = transaction_detail.city_use_amount,
                city_local_use_amount = transaction_detail.city_local_use_amount,
                transaction_ind = transaction_detail.transaction_ind,
                suspend_ind = transaction_detail.suspend_ind,
                taxcode_code = transaction_detail.taxcode_code,
                tax_matrix_id = transaction_detail.tax_matrix_id,
                jurisdiction_taxrate_id = transaction_detail.jurisdiction_taxrate_id,
                measure_type_code = v_state_ratetype_code,
                country_use_rate = transaction_detail.country_use_rate,
                state_use_rate = transaction_detail.state_use_rate,
                state_use_tier2_rate = transaction_detail.state_use_tier2_rate,
                state_use_tier3_rate = transaction_detail.state_use_tier3_rate,
                state_split_amount = transaction_detail.state_split_amount,
                state_tier2_min_amount = transaction_detail.state_tier2_min_amount,
                state_tier2_max_amount = transaction_detail.state_tier2_max_amount,
                state_maxtax_amount = transaction_detail.state_maxtax_amount,
                county_use_rate = transaction_detail.county_use_rate,
                county_local_use_rate = transaction_detail.county_local_use_rate,
                county_split_amount = transaction_detail.county_split_amount,
                county_maxtax_amount = transaction_detail.county_maxtax_amount,
                county_single_flag = transaction_detail.county_single_flag,
                county_default_flag = transaction_detail.county_default_flag,
                city_use_rate = transaction_detail.city_use_rate,
                city_local_use_rate = transaction_detail.city_local_use_rate,
                city_split_amount = transaction_detail.city_split_amount,
                city_split_use_rate = transaction_detail.city_split_use_rate,
                city_single_flag = transaction_detail.city_single_flag,
                city_default_flag = transaction_detail.city_default_flag,
                combined_use_rate = transaction_detail.combined_use_rate,
                state_taxcode_detail_id = transaction_detail.state_taxcode_detail_id,
                county_taxcode_detail_id = transaction_detail.county_taxcode_detail_id,
                city_taxcode_detail_id = transaction_detail.city_taxcode_detail_id,
                taxtype_used_code = transaction_detail.taxtype_used_code,
                state_taxable_amt = transaction_detail.state_taxable_amt,
                county_taxable_amt = transaction_detail.county_taxable_amt,
                city_taxable_amt = transaction_detail.city_taxable_amt,
                state_taxcode_type_code = transaction_detail.state_taxcode_type_code,
                county_taxcode_type_code = transaction_detail.county_taxcode_type_code,
                city_taxcode_type_code = transaction_detail.city_taxcode_type_code,
                update_user_id = transaction_detail.update_user_id,
                update_timestamp = transaction_detail.update_timestamp
          WHERE transaction_detail_id = transaction_detail.transaction_detail_id;
      END;
   END LOOP;
   CLOSE transaction_detail_cursor;
   COMMIT;
--EXCEPTION
--  WHEN OTHERS THEN
--vc_test1 := substr(vc_transaction_detail_stmt,1,4000);
--vc_test2 := substr(vc_transaction_detail_stmt,4001,4000);
--insert into tb_debug(row_joe, row_bob) values(vc_test1, vc_test2);
END;
