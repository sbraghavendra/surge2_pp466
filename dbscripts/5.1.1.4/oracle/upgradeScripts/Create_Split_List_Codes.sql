--Create Allocation/Split List Codes
INSERT INTO tb_list_code(code_type_code,code_code,description,message,explanation,severity_level,write_import_line_flag,abort_import_flag,update_user_id,update_timestamp)
VALUES('*DEF','MULTITRANS','Multi-Transaction Code','0','0','0','0','0',USER,SYSDATE);
INSERT INTO tb_list_code(code_type_code,code_code,description,message,explanation,severity_level,write_import_line_flag,abort_import_flag,update_user_id,update_timestamp)
VALUES('MULTITRANS','FS','Flagged for Future Split',NULL,NULL,NULL,NULL,NULL,USER,SYSDATE);
INSERT INTO tb_list_code(code_type_code,code_code,description,message,explanation,severity_level,write_import_line_flag,abort_import_flag,update_user_id,update_timestamp)
VALUES('MULTITRANS','OS','Original Split Transaction',NULL,NULL,NULL,NULL,NULL,USER,SYSDATE);
INSERT INTO tb_list_code(code_type_code,code_code,description,message,explanation,severity_level,write_import_line_flag,abort_import_flag,update_user_id,update_timestamp)
VALUES('MULTITRANS','S','Split Transaction',NULL,NULL,NULL,NULL,NULL,USER,SYSDATE);
INSERT INTO tb_list_code(code_type_code,code_code,description,message,explanation,severity_level,write_import_line_flag,abort_import_flag,update_user_id,update_timestamp)
VALUES('MULTITRANS','OA','Original Allocation',NULL,NULL,NULL,NULL,NULL,USER,SYSDATE);
INSERT INTO tb_list_code(code_type_code,code_code,description,message,explanation,severity_level,write_import_line_flag,abort_import_flag,update_user_id,update_timestamp)
VALUES('MULTITRANS','A','Allocation',NULL,NULL,NULL,NULL,NULL,USER,SYSDATE);

