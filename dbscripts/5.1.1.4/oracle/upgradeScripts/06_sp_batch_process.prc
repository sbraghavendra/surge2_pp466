create or replace
PROCEDURE sp_batch_process(an_batch_id number)
-- location_matrix flags vs. rates found  ????????? don't use them
-- what about override taxtype code in location matrix ????
-- Leaving all old fields in tb_transaction_detail for historic/audit purposes. Only CCH fields being removed. Adding new fields for new purposes.


/* ************************************************************************************************/
/* Object Type/Name: Stored Procedure - sp_batch_process                                          */
/* Author:           Michael B. Fuller                                                            */
/* Date:             a long time ago                                                              */
/* Description:      process a batch of transactions                                              */
/* Arguments:        an_batch_id number                                                           */
/* Returns:          None                                                                         */
/* ************************************************************************************************/
/* Maintenance History:                                                                           */
/* Scan  Author       Date     Release    Description                                  Ticket #   */
/* ----- ---------- ---------- ---------- -------------------------------------------- ---------- */
/*       M. Fuller  06/02/2006 3.3.5.0    Fix processing an allocated line - Temporary 871        */
/*       M. Fuller  01/04/2007 3.3.5.1    Fix processing an allocated line - Permenant 871        */
/* MBF01 M. Fuller                        Add Kill Process Flag                                   */
/* MBF02 M. Fuller  01/25/2007 3.3.5.1    Add Allocations                                         */
/* MBF03 M. Fuller  05/18/2007 3.3.7a.x   Add User Exit to end of procedure            888        */
/* MBF04 M. Fuller  08/13/2007 3.4.1.1    Add 5 Calc Tax Flags                         883        */
/* MBF05 M. Fuller  11/26/2008 3.4.4.x    Correct taxable txns with 0 calced tax amt   924        */
/* MBF06 M. Fuller  04/18/2011            Add Taxability Allocations                   1179       */
/* MBF07 M. Fuller  05/04/2011            Add Country for Federal Tax Level            1346       */
/* SD06  Srini D    08/08/2011 3.4.4.x    Correct UTC timestamp                                   */
/* MBF08 M. Fuller  08/12/2011            Add TaxCode Rules changes                    17         */
/* ************************************************************************************************/
IS
-- Define Cursor Type
   TYPE cursor_type is REF CURSOR;

-- Location Matrix Selection SQL           // MBF04
   vc_location_matrix_select       VARCHAR2(2000) :=
      'SELECT tb_location_matrix.location_matrix_id, ' ||
             'tb_location_matrix.jurisdiction_id, ' ||
             'tb_location_matrix.override_taxtype_code, ' ||
             'tb_jurisdiction.country, ' ||
             'tb_jurisdiction.state, ' ||
             'tb_jurisdiction.county, ' ||
             'tb_jurisdiction.city, ' ||
             'tb_jurisdiction.zip, ' ||
             'tb_jurisdiction.in_out ' ||
        'FROM tb_location_matrix, ' ||
             'tb_jurisdiction, ' ||
             'tb_bcp_transactions ' ||
      'WHERE ( tb_bcp_transactions.rowid = :v_rowid ) AND ' ||
            '( tb_location_matrix.default_flag = ''0'' AND tb_location_matrix.binary_weight > 0 ) AND ' ||
            '( tb_location_matrix.effective_date <= tb_bcp_transactions.gl_date ) AND ( tb_location_matrix.expiration_date >= tb_bcp_transactions.gl_date ) AND ' ||
            '( tb_location_matrix.jurisdiction_id = tb_jurisdiction.jurisdiction_id ) ';
   vc_location_matrix_where        VARCHAR2(28000) := '';
   vc_location_matrix_orderby      VARCHAR2(1000) :=
      'ORDER BY tb_location_matrix.binary_weight DESC, tb_location_matrix.significant_digits DESC, tb_location_matrix.effective_date DESC';
   vc_location_matrix_stmt         VARCHAR2 (31000);
   location_matrix_cursor          cursor_type;

-- Location Matrix Record TYPE
   TYPE location_matrix_record is RECORD (
      location_matrix_id           tb_location_matrix.location_matrix_id%TYPE,
      jurisdiction_id              tb_location_matrix.jurisdiction_id%TYPE,
      override_taxtype_code        tb_location_matrix.override_taxtype_code%TYPE,
      country                      tb_jurisdiction.country%TYPE,
      state                        tb_jurisdiction.state%TYPE,
      county                       tb_jurisdiction.county%TYPE,
      city                         tb_jurisdiction.city%TYPE,
      zip                          tb_jurisdiction.zip%TYPE,
      in_out                       tb_jurisdiction.in_out%TYPE );
   location_matrix                 location_matrix_record;

-- Tax Matrix Selection SQL
   vc_tax_matrix_select            VARCHAR2(2000) :=
      'SELECT tb_tax_matrix.tax_matrix_id, tb_tax_matrix.relation_sign, tb_tax_matrix.relation_amount, ' ||
             'tb_tax_matrix.then_hold_code_flag, tb_tax_matrix.else_hold_code_flag, ' ||
             'tb_tax_matrix.then_taxcode_code, tb_tax_matrix.else_taxcode_code ' ||
       'FROM tb_tax_matrix, ' ||
            'tb_bcp_transactions ' ||
      'WHERE ( tb_bcp_transactions.rowid = :v_rowid ) AND ' ||
            '( tb_tax_matrix.default_flag = ''0'' AND tb_tax_matrix.binary_weight > 0 ) AND ' ||
            '( tb_tax_matrix.effective_date <= tb_bcp_transactions.gl_date ) AND ( tb_tax_matrix.expiration_date >= tb_bcp_transactions.gl_date )';
   vc_tax_matrix_where             VARCHAR2(28000) := '';
   vc_tax_matrix_orderby           VARCHAR2(1000) :=
      'ORDER BY tb_tax_matrix.binary_weight DESC, tb_tax_matrix.significant_digits DESC, tb_tax_matrix.effective_date DESC';
   vc_tax_matrix_stmt              VARCHAR2 (31000);
   tax_matrix_cursor               cursor_type;

-- Tax Matrix Record TYPE
   TYPE tax_matrix_record is RECORD (
      tax_matrix_id                tb_tax_matrix.tax_matrix_id%TYPE,
      relation_sign                tb_tax_matrix.relation_sign%TYPE,
      relation_amount              tb_tax_matrix.relation_amount%TYPE,
      then_hold_code_flag          tb_tax_matrix.then_hold_code_flag%TYPE,
      else_hold_code_flag          tb_tax_matrix.else_hold_code_flag%TYPE,
      then_taxcode_code            tb_tax_matrix.then_taxcode_code%TYPE,
      else_taxcode_code            tb_tax_matrix.else_taxcode_code%TYPE);
   tax_matrix                      tax_matrix_record;

-- TacCode Detail Selection SQL
   vc_taxcode_detail_stmt           VARCHAR2(31000) :=
      'SELECT taxcode_detail_id, taxcode_type_code, override_taxtype_code, ratetype_code, taxable_threshold_amt, ' ||
             'tax_limitation_amt, cap_amt, base_change_pct, special_rate ' ||
        'FROM tb_taxcode_detail ' ||
       'WHERE tb_taxcode_detail.active_flag = ''1'' ' ||
         'AND tb_taxcode_detail.taxcode_code = :taxcode_code ' ||
         'AND tb_taxcode_detail.taxcode_country_code = :transaction_country_code ' ||
         'AND tb_taxcode_detail.taxcode_state_code = :transaction_state_code ' ||
         'AND tb_taxcode_detail.taxcode_county = :taxcode_county ' ||
         'AND tb_taxcode_detail.taxcode_city = :taxcode_city ' ||
         'AND tb_taxcode_detail.effective_date <= :gl_date ' ||
         'AND tb_taxcode_detail.expiration_date >= :gl_date ' ||
      'ORDER BY tb_taxcode_detail.effective_date DESC';
   taxcode_detail_cursor           cursor_type;

-- TaxCode Detail Record TYPE
   TYPE taxcode_detail_record is RECORD (
      taxcode_detail_id            tb_taxcode_detail.taxcode_detail_id%TYPE,
      taxcode_type_code            tb_taxcode_detail.taxcode_type_code%TYPE,
      override_taxtype_code        tb_taxcode_detail.override_taxtype_code%TYPE,
      ratetype_code                tb_taxcode_detail.ratetype_code%TYPE,
      taxable_threshold_amt        tb_taxcode_detail.taxable_threshold_amt%TYPE,
      tax_limitation_amt           tb_taxcode_detail.tax_limitation_amt%TYPE,
      cap_amt                      tb_taxcode_detail.cap_amt%TYPE,
      base_change_pct              tb_taxcode_detail.base_change_pct%TYPE,
      special_rate                 tb_taxcode_detail.special_rate%TYPE);
   taxcode_detail                  taxcode_detail_record;

-- Driver Reference Insert SQL
   vc_driver_ref_insert_stmt       VARCHAR2(30000);

-- Table defined variables
   v_batch_status_code             tb_batch.batch_status_code%TYPE                   := 'P';
   v_starting_sequence             tb_batch.start_row%TYPE                           := 0;
   v_processed_rows                tb_batch.processed_rows%TYPE                      := 0;
   v_error_sev_code                tb_batch.error_sev_code%TYPE                      := ' ';
   v_severity_level                tb_list_code.severity_level%TYPE                  := ' ';
   v_write_import_line_flag        tb_list_code.write_import_line_flag%TYPE          := '1';
   v_abort_import_flag             tb_list_code.abort_import_flag%TYPE               := '0';
   v_allocation_flag               tb_option.value%TYPE                              := NULL;
   v_delete_zero_amounts           tb_option.value%TYPE                              := NULL;
   v_global_hold_flag              tb_option.value%TYPE                              := NULL;
   v_global_hold_amt               tb_option.value%TYPE                              := NULL;
   v_autoproc_flag                 tb_option.value%TYPE                              := NULL;
   v_autoproc_amount               tb_option.value%TYPE                              := NULL;
   v_processuserexit_flag          tb_option.value%TYPE                              := NULL;
   v_transaction_county            tb_taxcode_detail.taxcode_county%TYPE             := NULL;
   v_transaction_city              tb_taxcode_detail.taxcode_city%TYPE               := NULL;
   v_less_code                     tb_taxcode_detail.taxcode_type_code%TYPE          := NULL;
   v_equal_code                    tb_taxcode_detail.taxcode_type_code%TYPE          := NULL;
   v_greater_code                  tb_taxcode_detail.taxcode_type_code%TYPE          := NULL;
   v_relation_code                 tb_taxcode_detail.taxcode_type_code%TYPE          := NULL;
   v_hold_code_flag                tb_tax_matrix.then_hold_code_flag%TYPE            := NULL;
   v_state_override_taxtype_code   tb_taxcode_detail.override_taxtype_code%TYPE      := NULL;
   v_state_ratetype_code           tb_taxcode_detail.ratetype_code%TYPE              := NULL;
   v_state_taxable_threshold_amt   tb_taxcode_detail.taxable_threshold_amt%TYPE      := 0;
   v_county_taxable_threshold_amt  tb_taxcode_detail.taxable_threshold_amt%TYPE      := 0;
   v_city_taxable_threshold_amt    tb_taxcode_detail.taxable_threshold_amt%TYPE      := 0;
   v_state_tax_limitation_amt      tb_taxcode_detail.tax_limitation_amt%TYPE         := 0;
   v_county_tax_limitation_amt     tb_taxcode_detail.tax_limitation_amt%TYPE         := 0;
   v_city_tax_limitation_amt       tb_taxcode_detail.tax_limitation_amt%TYPE         := 0;
   v_state_cap_amt                 tb_taxcode_detail.cap_amt%TYPE                    := 0;
   v_county_cap_amt                tb_taxcode_detail.cap_amt%TYPE                    := 0;
   v_city_cap_amt                  tb_taxcode_detail.cap_amt%TYPE                    := 0;
   v_state_base_change_pct         tb_taxcode_detail.base_change_pct%TYPE            := 0;
   v_county_base_change_pct        tb_taxcode_detail.base_change_pct%TYPE            := 0;
   v_city_base_change_pct          tb_taxcode_detail.base_change_pct%TYPE            := 0;
   v_state_special_rate            tb_taxcode_detail.special_rate%TYPE               := 0;
   v_county_special_rate           tb_taxcode_detail.special_rate%TYPE               := 0;
   v_city_special_rate             tb_taxcode_detail.special_rate%TYPE               := 0;
   --v_override_jurisdiction_id      tb_taxcode_detail.jurisdiction_id%TYPE            := 0;
   v_sysdate                       tb_bcp_transactions.load_timestamp%TYPE           := SYS_EXTRACT_UTC(SYSTIMESTAMP);
   v_transaction_amt               tb_bcp_transactions.gl_line_itm_dist_amt%TYPE     := 0;
   v_kill_proc_flag                tb_option.value%TYPE                              := NULL;
   v_kill_proc_count               tb_option.value%TYPE                              := NULL;

-- Program defined variables
   v_rowid                         ROWID                                             := NULL;
   vi_cursor_id                    INTEGER                                           := 0;
   vi_rows_processed               INTEGER                                           := 0;
   vn_hold_trans_amount            NUMBER                                            := 0;
   vn_autoproc_amount              NUMBER                                            := 0;
   vn_fetch_rows                   NUMBER                                            := 0;
   vn_state_rows                   NUMBER                                            := 0;
   vn_county_rows                  NUMBER                                            := 0;
   vn_city_rows                    NUMBER                                            := 0;
   vi_error_count                  INTEGER                                           := 0;
   vn_actual_rowno                 NUMBER                                            := 0;
   vc_driver_ref_ins_stmt          VARCHAR2(5000)                                    := NULL;
   vc_state_driver_flag            CHAR(1)                                           := '0';
   vn_kill_proc_count              NUMBER                                            := 0;
   vn_kill_proc_counter            NUMBER                                            := 0;
   vc_batch_status_code            VARCHAR2(10)                                      := NULL;
   vn_return                       NUMBER                                            := 0;
   vc_country_flag                 CHAR(1)                                           := '0';
   vc_state_flag                   CHAR(1)                                           := '0';
   vc_county_flag                  CHAR(1)                                           := '0';
   vc_county_local_flag            CHAR(1)                                           := '0';
   vc_city_flag                    CHAR(1)                                           := '0';
   vc_city_local_flag              CHAR(1)                                           := '0';

-- Define Location Driver Names Cursor (tb_driver_names)
   CURSOR location_driver_cursor
   IS
        SELECT tb_driver_names.trans_dtl_column_name, tb_driver_names.matrix_column_name, tb_driver_names.null_driver_flag, tb_driver_names.wildcard_flag,
               datadefcol.desc_column_name, datadefcol.data_type
          FROM tb_driver_names,
               (SELECT *
                  FROM tb_data_def_column
                 WHERE table_name = 'TB_TRANSACTION_DETAIL') datadefcol
         WHERE tb_driver_names.driver_names_code = 'L' AND
               tb_driver_names.trans_dtl_column_name = datadefcol.column_name
      ORDER BY tb_driver_names.driver_id;
      location_driver              location_driver_cursor%ROWTYPE;

-- Define Tax Driver Names Cursor (tb_driver_names)
   CURSOR tax_driver_cursor
   IS
        SELECT tb_driver_names.trans_dtl_column_name, tb_driver_names.matrix_column_name, tb_driver_names.null_driver_flag, tb_driver_names.wildcard_flag, tb_driver_names.range_flag, tb_driver_names.to_number_flag,
               datadefcol.desc_column_name, datadefcol.data_type
          FROM tb_driver_names,
               (SELECT *
                  FROM tb_data_def_column
                 WHERE table_name = 'TB_TRANSACTION_DETAIL') datadefcol
         WHERE tb_driver_names.driver_names_code = 'T' AND
               tb_driver_names.trans_dtl_column_name = datadefcol.column_name
      ORDER BY tb_driver_names.driver_id;
      tax_driver                   tax_driver_cursor%ROWTYPE;

-- Define BCP Transactions Cursor (tb_bcp_transactions)
   -- 3411 - add taxable amount and tax type used.
   CURSOR bcp_transactions_cursor
   IS
      SELECT ROWID,
             transaction_detail_id,
             source_transaction_id,
             process_batch_no,
             gl_extract_batch_no,
             archive_batch_no,
             allocation_matrix_id,
             allocation_subtrans_id,
             entered_date,
             transaction_status,
             gl_date,
             gl_company_nbr,
             gl_company_name,
             gl_division_nbr,
             gl_division_name,
             gl_cc_nbr_dept_id,
             gl_cc_nbr_dept_name,
             gl_local_acct_nbr,
             gl_local_acct_name,
             gl_local_sub_acct_nbr,
             gl_local_sub_acct_name,
             gl_full_acct_nbr,
             gl_full_acct_name,
             gl_line_itm_dist_amt,
             orig_gl_line_itm_dist_amt,
             vendor_nbr,
             vendor_name,
             vendor_address_line_1,
             vendor_address_line_2,
             vendor_address_line_3,
             vendor_address_line_4,
             vendor_address_city,
             vendor_address_county,
             vendor_address_state,
             vendor_address_zip,
             vendor_address_country,
             vendor_type,
             vendor_type_name,
             invoice_nbr,
             invoice_desc,
             invoice_date,
             invoice_freight_amt,
             invoice_discount_amt,
             invoice_tax_amt,
             invoice_total_amt,
             invoice_tax_flg,
             invoice_line_nbr,
             invoice_line_name,
             invoice_line_type,
             invoice_line_type_name,
             invoice_line_amt,
             invoice_line_tax,
             afe_project_nbr,
             afe_project_name,
             afe_category_nbr,
             afe_category_name,
             afe_sub_cat_nbr,
             afe_sub_cat_name,
             afe_use,
             afe_contract_type,
             afe_contract_structure,
             afe_property_cat,
             inventory_nbr,
             inventory_name,
             inventory_class,
             inventory_class_name,
             po_nbr,
             po_name,
             po_date,
             po_line_nbr,
             po_line_name,
             po_line_type,
             po_line_type_name,
             ship_to_location,
             ship_to_location_name,
             ship_to_address_line_1,
             ship_to_address_line_2,
             ship_to_address_line_3,
             ship_to_address_line_4,
             ship_to_address_city,
             ship_to_address_county,
             ship_to_address_state,
             ship_to_address_zip,
             ship_to_address_country,
             wo_nbr,
             wo_name,
             wo_date,
             wo_type,
             wo_type_desc,
             wo_class,
             wo_class_desc,
             wo_entity,
             wo_entity_desc,
             wo_line_nbr,
             wo_line_name,
             wo_line_type,
             wo_line_type_desc,
             wo_shut_down_cd,
             wo_shut_down_cd_desc,
             voucher_id,
             voucher_name,
             voucher_date,
             voucher_line_nbr,
             voucher_line_desc,
             check_nbr,
             check_no,
             check_date,
             check_amt,
             check_desc,
             user_text_01,
             user_text_02,
             user_text_03,
             user_text_04,
             user_text_05,
             user_text_06,
             user_text_07,
             user_text_08,
             user_text_09,
             user_text_10,
             user_text_11,
             user_text_12,
             user_text_13,
             user_text_14,
             user_text_15,
             user_text_16,
             user_text_17,
             user_text_18,
             user_text_19,
             user_text_20,
             user_text_21,
             user_text_22,
             user_text_23,
             user_text_24,
             user_text_25,
             user_text_26,
             user_text_27,
             user_text_28,
             user_text_29,
             user_text_30,
             user_number_01,
             user_number_02,
             user_number_03,
             user_number_04,
             user_number_05,
             user_number_06,
             user_number_07,
             user_number_08,
             user_number_09,
             user_number_10,
             user_date_01,
             user_date_02,
             user_date_03,
             user_date_04,
             user_date_05,
             user_date_06,
             user_date_07,
             user_date_08,
             user_date_09,
             user_date_10,
             comments,
             tb_calc_tax_amt,
             state_use_amount,
             state_use_tier2_amount,
             state_use_tier3_amount,
             county_use_amount,
             county_local_use_amount,
             city_use_amount,
             city_local_use_amount,
             transaction_state_code,
             auto_transaction_state_code,
             transaction_ind,
             suspend_ind,
             taxcode_code,
             manual_taxcode_ind,
             tax_matrix_id,
             location_matrix_id,
             jurisdiction_id,
             jurisdiction_taxrate_id,
             manual_jurisdiction_ind,
             state_use_rate,
             state_use_tier2_rate,
             state_use_tier3_rate,
             state_split_amount,
             state_tier2_min_amount,
             state_tier2_max_amount,
             state_maxtax_amount,
             county_use_rate,
             county_local_use_rate,
             county_split_amount,
             county_maxtax_amount,
             county_single_flag,
             county_default_flag,
             city_use_rate,
             city_local_use_rate,
             city_split_amount,
             city_split_use_rate,
             city_single_flag,
             city_default_flag,
             combined_use_rate,
             load_timestamp,
             gl_extract_updater,
             gl_extract_timestamp,
             gl_extract_flag,
             gl_log_flag,
             gl_extract_amt,
             audit_flag,
             audit_user_id,
             audit_timestamp,
             modify_user_id,
             modify_timestamp,
             update_user_id,
             update_timestamp,
             bcp_transaction_id,
             country_use_amount,
             transaction_country_code,
             auto_transaction_country_code,
             country_use_rate,
             split_subtrans_id,
             multi_trans_code,
             tax_alloc_matrix_id,
             state_taxcode_detail_id,
             county_taxcode_detail_id,
             city_taxcode_detail_id,
             taxtype_used_code,
             state_taxable_amt,
             county_taxable_amt,
             city_taxable_amt,
             state_taxcode_type_code,
             county_taxcode_type_code,
             city_taxcode_type_code
        FROM tb_bcp_transactions
       WHERE process_batch_no = an_batch_id;
         -- MBF06 -- removed -- AND (( transaction_detail_id is NULL ) OR ( transaction_detail_id <> -1 ));
      bcp_transactions             bcp_transactions_cursor%ROWTYPE;

-- Define Exceptions
   e_badread                       exception;
   e_badupdate                     exception;
   e_badwrite                      exception;
   e_wrongdata                     exception;
   e_abort                         exception;
   e_kill                          exception;

-- ****** Program starts ****** --------------------------------------------------------------------
BEGIN
   -- Confirm batch exists and is flagged for processing
   BEGIN
      SELECT batch_status_code
        INTO v_batch_status_code
        FROM tb_batch
       WHERE batch_id = an_batch_id;
      IF SQLCODE != 0 THEN
         RAISE e_badread;
      ELSIF v_batch_status_code != 'FP' THEN
         RAISE e_wrongdata;
      END IF;
   EXCEPTION
      WHEN NO_DATA_FOUND OR e_badread THEN
         vi_error_count := vi_error_count + 1;
         sp_geterrorcode('P8', an_batch_id, 'P', v_sysdate, v_severity_level, v_write_import_line_flag, v_abort_import_flag);
         IF v_severity_level > v_error_sev_code THEN
            v_error_sev_code := v_severity_level;
         END IF;
      WHEN e_wrongdata THEN
         vi_error_count := vi_error_count + 1;
         sp_geterrorcode('P9', an_batch_id, 'P', v_sysdate, v_severity_level, v_write_import_line_flag, v_abort_import_flag);
         IF v_severity_level > v_error_sev_code THEN
            v_error_sev_code := v_severity_level;
         END IF;
   END;
   -- Abort Procedure?
   IF v_abort_import_flag = '1' THEN
      v_batch_status_code := 'ABP';
      RAISE e_abort;
   END IF;

   /* -- RUN Taxability Allocations? -- MBF06 */
   -- Determine Taxability Allocation Flag
   v_allocation_flag := '0';
   BEGIN
      SELECT LTRIM(RTRIM(value))
        INTO v_allocation_flag
        FROM tb_option
       WHERE option_code = 'TAXALLOCENABLED'
         AND option_type_code = 'ADMIN'
         AND user_code = 'ADMIN';
   EXCEPTION
      WHEN OTHERS THEN
         v_allocation_flag := '0';
   END;
   IF v_allocation_flag IS NULL THEN
      v_allocation_flag := '0';
   END IF;
   -- Perform Allocations
   IF v_allocation_flag = '1' THEN
      sp_batch_tax_allocation(an_batch_id);
   END IF;

   /* -- RUN Jurisdiction Allocations? -- MBF02 */
   -- Determine Allocation Flag
   BEGIN
      SELECT LTRIM(RTRIM(value))
        INTO v_allocation_flag
        FROM tb_option
       WHERE option_code = 'ALLOCATIONSENABLED'
         AND option_type_code = 'ADMIN'
         AND user_code = 'ADMIN';
   EXCEPTION
      WHEN OTHERS THEN
         v_allocation_flag := '0';
   END;
   IF v_allocation_flag IS NULL THEN
      v_allocation_flag := '0';
   END IF;
   -- Perform Allocations
   IF v_allocation_flag = '1' THEN
      sp_batch_juris_allocation(an_batch_id);
   END IF;
		
   -- Get starting sequence no.
   BEGIN
      SELECT last_number
        INTO v_starting_sequence
        FROM user_sequences
       WHERE sequence_name = 'SQ_TB_TRANSACTION_DETAIL_ID';
   EXCEPTION
      WHEN OTHERS THEN
         NULL;
   END;

   -- Update batch
   BEGIN
      UPDATE tb_batch
         SET batch_status_code = 'XP',
             error_sev_code = '',
             start_row = v_starting_sequence,
             --actual_start_timestamp = SYSDATE  /**SD06**/
             actual_start_timestamp = SYS_EXTRACT_UTC(SYSTIMESTAMP) /**SD06**/
       WHERE batch_id = an_batch_id;
      IF SQLCODE != 0 THEN
         RAISE e_badupdate;
      END IF;
   EXCEPTION
      WHEN e_badupdate THEN
         vi_error_count := vi_error_count + 1;
         sp_geterrorcode('P1', an_batch_id, 'P', v_sysdate, v_severity_level, v_write_import_line_flag, v_abort_import_flag);
         IF v_severity_level > v_error_sev_code THEN
            v_error_sev_code := v_severity_level;
         END IF;
   END;
   -- Abort Procedure?
   IF v_abort_import_flag = '1' THEN
      v_batch_status_code := 'ABP';
      RAISE e_abort;
   END IF;

   -- Determine Global Delete Zero Amounts Flag
   BEGIN
      SELECT LTRIM(RTRIM(value))
        INTO v_delete_zero_amounts
        FROM tb_option
       WHERE option_code = 'DELETE0AMTFLAG'
         AND option_type_code = 'SYSTEM'
         AND user_code = 'SYSTEM';
   EXCEPTION
      WHEN OTHERS THEN
         v_delete_zero_amounts := '0';
   END;
   IF v_delete_zero_amounts IS NULL THEN
      v_delete_zero_amounts := '0';
   END IF;

   -- Determine Global Hold Transactions Flag and Amount
   BEGIN
      SELECT LTRIM(RTRIM(value))
        INTO v_global_hold_flag
        FROM tb_option
       WHERE option_code = 'HOLDTRANSFLAG'
         AND option_type_code = 'SYSTEM'
         AND user_code = 'SYSTEM';
   EXCEPTION
      WHEN OTHERS THEN
         v_global_hold_flag := '0';
   END;
   IF v_global_hold_flag IS NULL THEN
      v_global_hold_flag := '0';
   END IF;
   IF v_global_hold_flag = '1' THEN
      BEGIN
         SELECT LTRIM(RTRIM(value))
           INTO v_global_hold_amt
           FROM tb_option
          WHERE option_code = 'HOLDTRANSAMT'
            AND option_type_code = 'SYSTEM'
            AND user_code = 'SYSTEM';
      EXCEPTION
         WHEN OTHERS THEN
            v_global_hold_amt := '0';
      END;
      IF v_global_hold_amt IS NULL THEN
         v_global_hold_amt := '0';
      END IF;
      vn_hold_trans_amount := TO_NUMBER(v_global_hold_amt);
   END IF;

   -- Determine "auto-process" Flag, Amount, and TaxCode
   BEGIN
      SELECT LTRIM(RTRIM(value))
        INTO v_autoproc_flag
        FROM tb_option
       WHERE option_code = 'AUTOPROCFLAG'
         AND option_type_code = 'SYSTEM'
         AND user_code = 'SYSTEM';
   EXCEPTION
      WHEN OTHERS THEN
         v_autoproc_flag := '0';
   END;
   IF v_autoproc_flag IS NULL THEN
      v_autoproc_flag := '0';
   END IF;
   IF v_autoproc_flag = '1' THEN
      BEGIN
         SELECT LTRIM(RTRIM(value))
           INTO v_autoproc_amount
           FROM tb_option
          WHERE option_code = 'AUTOPROCAMT'
            AND option_type_code = 'SYSTEM'
            AND user_code = 'SYSTEM';
      EXCEPTION
         WHEN OTHERS THEN
            v_autoproc_amount := '0';
      END;
      IF v_autoproc_amount IS NULL THEN
         v_autoproc_amount := '0';
      END IF;
      vn_autoproc_amount := TO_NUMBER(v_autoproc_amount);
   END IF;

   -- Determine Kill Process Flag/Count -- MBF01 -- begin
   BEGIN
      SELECT LTRIM(RTRIM(value))
        INTO v_kill_proc_flag
        FROM tb_option
       WHERE option_code = 'KILLPROCFLAG'
         AND option_type_code = 'SYSTEM'
         AND user_code = 'SYSTEM';
   EXCEPTION
      WHEN OTHERS THEN
         v_kill_proc_flag := '0';
   END;
   IF v_kill_proc_flag IS NULL THEN
      v_kill_proc_flag := '0';
   END IF;
   IF v_kill_proc_flag = '1' THEN
      BEGIN
         SELECT LTRIM(RTRIM(value))
           INTO v_kill_proc_count
           FROM tb_option
          WHERE option_code = 'KILLPROCCOUNT'
            AND option_type_code = 'SYSTEM'
            AND user_code = 'SYSTEM';
      EXCEPTION
         WHEN OTHERS THEN
            v_kill_proc_count := '5000';
      END;
      IF v_kill_proc_count IS NULL THEN
         v_kill_proc_count := '5000';
      END IF;
      vn_kill_proc_count := TO_NUMBER(v_kill_proc_count);
   END IF;
   -- Determine Kill Process Flag/Count -- MBF01 -- end

   -- ***** Create Dynamic WHERE Clause for Matrix SQLs ***** --
   -- Location Matrix Drivers
   OPEN location_driver_cursor;
   LOOP
      FETCH location_driver_cursor INTO location_driver;
      EXIT WHEN location_driver_cursor%NOTFOUND;
      IF location_driver.null_driver_flag = '1' THEN
         IF location_driver.wildcard_flag = '1' THEN
            IF UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'CHAR' OR
               UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'VARC' THEN
                  vc_location_matrix_where := vc_location_matrix_where || 'AND ' ||
                  '((tb_bcp_transactions.' || location_driver.trans_dtl_column_name || ' IS NULL AND (tb_location_matrix.' || location_driver.matrix_column_name || ' = ''*ALL'' OR tb_location_matrix.' || location_driver.matrix_column_name || ' = ''*NULL'' )) OR ' ||
                  '(tb_bcp_transactions.' || location_driver.trans_dtl_column_name || ' IS NOT NULL AND (tb_location_matrix.' || location_driver.matrix_column_name || ' = ''*ALL'' OR tb_bcp_transactions.' || location_driver.trans_dtl_column_name || ' LIKE tb_location_matrix.' || location_driver.matrix_column_name || '))) ';
            END IF;
         ELSE
            IF UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'CHAR' OR
               UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'VARC' THEN
                  vc_location_matrix_where := vc_location_matrix_where || 'AND ' ||
                  '((tb_bcp_transactions.' || location_driver.trans_dtl_column_name || ' IS NULL AND (tb_location_matrix.' || location_driver.matrix_column_name || ' = ''*ALL'' OR tb_location_matrix.' || location_driver.matrix_column_name || ' = ''*NULL'' )) OR ' ||
                  '(tb_bcp_transactions.' || location_driver.trans_dtl_column_name || ' IS NOT NULL AND (tb_location_matrix.' || location_driver.matrix_column_name || ' = ''*ALL'' OR tb_bcp_transactions.' || location_driver.trans_dtl_column_name || ' = tb_location_matrix.' || location_driver.matrix_column_name || '))) ';
            END IF;
         END IF;
      ELSE
         IF location_driver.wildcard_flag = '1' THEN
            IF UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'CHAR' OR
               UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'VARC' THEN
                  vc_location_matrix_where := vc_location_matrix_where || 'AND ' ||
                  '(tb_location_matrix.' || location_driver.matrix_column_name || ' = ''*ALL'' OR tb_bcp_transactions.' || location_driver.trans_dtl_column_name || ' LIKE tb_location_matrix.' || location_driver.matrix_column_name || ') ';
            END IF;
         ELSE
            IF UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'CHAR' OR
               UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'VARC' THEN
                  vc_location_matrix_where := vc_location_matrix_where || 'AND ' ||
                  '(tb_location_matrix.' || location_driver.matrix_column_name || ' = ''*ALL'' OR tb_bcp_transactions.' || location_driver.trans_dtl_column_name || ' = tb_location_matrix.' || location_driver.matrix_column_name || ') ';
            END IF;
         END IF;
      END IF;
      -- Insert Driver Reference Values for this driver
      -- Create Insert SQL
      vc_driver_ref_ins_stmt :=
         'INSERT INTO tb_driver_reference (trans_dtl_column_name, driver_value, driver_description) ' ||
            'SELECT ''' || Upper(location_driver.trans_dtl_column_name) ||
                    ''', ' || location_driver.trans_dtl_column_name || ', ';
      IF location_driver.desc_column_name IS NOT NULL THEN
         vc_driver_ref_ins_stmt := vc_driver_ref_ins_stmt || 'min(' || location_driver.desc_column_name || ') ';
      ELSE
         vc_driver_ref_ins_stmt := vc_driver_ref_ins_stmt || 'NULL ';
      END IF;
      vc_driver_ref_ins_stmt := vc_driver_ref_ins_stmt ||
              'FROM tb_bcp_transactions ' ||
             'WHERE process_batch_no = ' || TO_CHAR(an_batch_id) || ' AND ' ||
                    location_driver.trans_dtl_column_name || ' IS NOT NULL AND ' ||
                    location_driver.trans_dtl_column_name || ' NOT IN ' ||
               '(SELECT driver_value ' ||
                  'FROM tb_driver_reference ' ||
                 'WHERE trans_dtl_column_name = ''' || Upper(location_driver.trans_dtl_column_name) || ''') ' ||
              'GROUP BY ''' || Upper(location_driver.trans_dtl_column_name) ||
                        ''', ' || location_driver.trans_dtl_column_name;
      -- Execute Insert SQL
      vi_cursor_id := DBMS_SQL.open_cursor;
      DBMS_SQL.parse (vi_cursor_id, vc_driver_ref_ins_stmt, DBMS_SQL.v7);
      vi_rows_processed := DBMS_SQL.execute (vi_cursor_id);
      DBMS_SQL.close_cursor (vi_cursor_id);
   END LOOP;
   CLOSE location_driver_cursor;
   -- If no drivers found raise error, else create location matrix sql statement
   IF vc_location_matrix_where IS NULL THEN
      vi_error_count := vi_error_count + 1;
      sp_geterrorcode('P2', an_batch_id, 'P', v_sysdate, v_severity_level, v_write_import_line_flag, v_abort_import_flag);
      IF v_severity_level > v_error_sev_code THEN
         v_error_sev_code := v_severity_level;
      END IF;
   ELSE
      vc_location_matrix_stmt := vc_location_matrix_select || vc_location_matrix_where || vc_location_matrix_orderby;
   END IF;
   -- Abort Procedure?
   IF v_abort_import_flag = '1' THEN
      v_batch_status_code := 'ABP';
      RAISE e_abort;
   END IF;

   -- Tax Matrix Drivers
   OPEN tax_driver_cursor;
   LOOP
      FETCH tax_driver_cursor INTO tax_driver;
      EXIT WHEN tax_driver_cursor%NOTFOUND;
      IF tax_driver.range_flag = '1' THEN
         IF tax_driver.to_number_flag = '1' THEN
            IF tax_driver.null_driver_flag = '1' THEN
               -- Range and ToNumber and NULL
               IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                  UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                  vc_tax_matrix_where := vc_tax_matrix_where || 'AND ( ' ||
                  '( UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU) = ''*NA'' AND ' ||
                     '( tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ' IS NULL AND ( UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') = ''*ALL'' OR UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') = ''*NULL'' )) OR ' ||
                     '( tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND ( UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') = ''*ALL'' OR DECODE(isnumber(tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || '),1,to_char(tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ',''00000000000000000000.00000000000000000000''),UPPER(tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ')) = DECODE(isnumber(UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ')),1,to_char(UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '),''00000000000000000000.00000000000000000000''),UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') ))) OR ' ||
                  '( UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU) <> ''*NA'' AND ' ||
                     '( tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND (DECODE(isnumber(tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || '),1,to_char(tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ',''00000000000000000000.00000000000000000000''),UPPER(tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ')) >= DECODE(isnumber(UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ')),1,to_char(UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '),''00000000000000000000.00000000000000000000''),UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') ) AND ' ||
                                                                                                       'DECODE(isnumber(tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || '),1,to_char(tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ',''00000000000000000000.00000000000000000000''),UPPER(tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ')) <= DECODE(isnumber(UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU)),1,to_char(UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU),''00000000000000000000.00000000000000000000''),UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU) ))))) ';
               END IF;
            ELSE
               -- Range and ToNumber and NOT NULL
               IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                  UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                  vc_tax_matrix_where := vc_tax_matrix_where || 'AND ( ' ||
                  '( UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU) = ''*NA'' AND (UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') = ''*ALL'' OR DECODE(isnumber(tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || '),1,to_char(tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ',''00000000000000000000.00000000000000000000''),UPPER(tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ')) = DECODE(isnumber(UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ')),1,to_char(UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '),''00000000000000000000.00000000000000000000''),UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ')))) OR ' ||
                  '( UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU) <> ''*NA'' AND (DECODE(isnumber(tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || '),1,to_char(tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ',''00000000000000000000.00000000000000000000''),UPPER(tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ')) >= DECODE(isnumber(UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ')),1,to_char(UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '),''00000000000000000000.00000000000000000000''),UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ')) AND ' ||
                                                                                        'DECODE(isnumber(tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || '),1,to_char(tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ',''00000000000000000000.00000000000000000000''),UPPER(tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ')) <= DECODE(isnumber(UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU )),1,to_char(UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU),''00000000000000000000.00000000000000000000''),UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU) ))) ';
               END IF;
            END IF;
         ELSE
            IF tax_driver.null_driver_flag = '1' THEN
               IF tax_driver.wildcard_flag = '1' THEN
                  -- Range and NOT ToNumber and NULL and Wildcard
                  IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                     UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                     vc_tax_matrix_where := vc_tax_matrix_where || 'AND ( ' ||
                     '( UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU) = ''*NA'' AND ' ||
                        '( tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ' IS NULL AND (UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') = ''*ALL'' OR UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') = ''*NULL'' )) OR ' ||
                        '( tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND (UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') = ''*ALL'' OR UPPER(tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ') LIKE UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ')))) OR ' ||
                     '( UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU) <> ''*NA'' AND (tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND (UPPER(tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ') >= UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') AND UPPER(tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ') <= UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU) ))) ';
                  END IF;
               ELSE
                  -- Range and NOT ToNumber and NULL and NOT Wildcard
                  IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                     UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                     vc_tax_matrix_where := vc_tax_matrix_where || 'AND ( ' ||
                     '( UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU) = ''*NA'' AND ' ||
                        '( tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ' IS NULL AND (UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') = ''*ALL'' OR UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') = ''*NULL'' )) OR ' ||
                        '( tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND (UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') = ''*ALL'' OR UPPER(tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ') = UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ')))) OR ' ||
                     '( UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU) <> ''*NA'' AND (tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND (UPPER(tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ') >= UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') AND UPPER(tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ') <= UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU) ))) ';
                  END IF;
               END IF;
            ELSE
               IF tax_driver.wildcard_flag = '1' THEN
                  -- Range and NOT ToNumber and NOT NULL and Wildcard
                  IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                     UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                     vc_tax_matrix_where := vc_tax_matrix_where || 'AND ( ' ||
                     '( UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU) = ''*NA'' AND ' ||
                        '(UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') = ''*ALL'' OR UPPER(tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ') LIKE UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '))) OR ' ||
                     '( UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU) <> ''*NA'' AND (UPPER(tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ') >= UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') AND UPPER(tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ') <= UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU) )) ';
                  END IF;
               ELSE
                  -- Range and NOT ToNumber and NOT NULL and NOT Wildcard
                  IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                     UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                     vc_tax_matrix_where := vc_tax_matrix_where || 'AND ( ' ||
                     '( UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU) = ''*NA'' AND ' ||
                        '(UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') = ''*ALL'' OR UPPER(tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ') = UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '))) OR ' ||
                     '( UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU) <> ''*NA'' AND (UPPER(tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ') >= UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') AND UPPER(tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ') <= UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU) )) ';
                  END IF;
               END IF;
            END IF;
         END IF;
      ELSE
         IF tax_driver.null_driver_flag = '1' THEN
            IF tax_driver.wildcard_flag = '1' THEN
               -- NOT Range and NULL and Wildcard
               IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                  UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                     vc_tax_matrix_where := vc_tax_matrix_where || 'AND ( ' ||
                     '(( tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ' IS NULL AND (UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') = ''*ALL'' OR UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') = ''*NULL'' )) OR ' ||
                     '( tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND (UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') = ''*ALL'' OR UPPER(tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ') LIKE UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ')))) ';
               END IF;
            ELSE
               -- NOT Range and NULL and NOT Wildcard
               IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                  UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                     vc_tax_matrix_where := vc_tax_matrix_where || 'AND ( ' ||
                     '(( tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ' IS NULL AND (UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') = ''*ALL'' OR UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') = ''*NULL'' )) OR ' ||
                     '( tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND (UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') = ''*ALL'' OR UPPER(tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ') = UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ')))) ';
               END IF;
            END IF;
         ELSE
            IF tax_driver.wildcard_flag = '1' THEN
               -- NOT Range and NOT NULL and Wildcard
               IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                  UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                     vc_tax_matrix_where := vc_tax_matrix_where || 'AND ( ' ||
                     '( UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') = ''*ALL'' OR UPPER(tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ') LIKE UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ')) ';
               END IF;
            ELSE
               -- NOT Range and NOT NULL and NOT Wildcard
               IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                  UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                  IF tax_driver.trans_dtl_column_name = 'TRANSACTION_STATE_CODE' THEN
                     IF vc_state_driver_flag <> '1' THEN
                        vc_state_driver_flag := '1';
                     END IF;
                     vc_tax_matrix_where := vc_tax_matrix_where || 'AND ( ' ||
                     '( UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') = ''*ALL'' OR UPPER(:transaction_state_code) = UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ')) ';
                  ELSE
                     vc_tax_matrix_where := vc_tax_matrix_where || 'AND ( ' ||
                     '( UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') = ''*ALL'' OR UPPER(tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ') = UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ')) ';
                  END IF;
               END IF;
            END IF;
         END IF;
      END IF;
      vc_tax_matrix_where := vc_tax_matrix_where || ') ';
      -- Insert Driver Reference Values for this driver
      -- Create Insert SQL
      vc_driver_ref_ins_stmt :=
         'INSERT INTO tb_driver_reference (trans_dtl_column_name, driver_value, driver_description) ' ||
            'SELECT ''' || Upper(tax_driver.trans_dtl_column_name) ||
                    ''', ' || tax_driver.trans_dtl_column_name || ', ';
      IF tax_driver.desc_column_name IS NOT NULL THEN
         vc_driver_ref_ins_stmt := vc_driver_ref_ins_stmt || 'min(' || tax_driver.desc_column_name || ') ';
      ELSE
         vc_driver_ref_ins_stmt := vc_driver_ref_ins_stmt || 'NULL ';
      END IF;
      vc_driver_ref_ins_stmt := vc_driver_ref_ins_stmt ||
              'FROM tb_bcp_transactions ' ||
             'WHERE process_batch_no = ' || TO_CHAR(an_batch_id) || ' AND ' ||
                    tax_driver.trans_dtl_column_name || ' IS NOT NULL AND ' ||
                    tax_driver.trans_dtl_column_name || ' NOT IN ' ||
               '(SELECT driver_value ' ||
                  'FROM tb_driver_reference ' ||
                 'WHERE trans_dtl_column_name = ''' || Upper(tax_driver.trans_dtl_column_name) || ''') ' ||
              'GROUP BY ''' || Upper(tax_driver.trans_dtl_column_name) ||
                        ''', ' || tax_driver.trans_dtl_column_name;
      -- Execute Insert SQL
      vi_cursor_id := DBMS_SQL.open_cursor;
      DBMS_SQL.parse (vi_cursor_id, vc_driver_ref_ins_stmt, DBMS_SQL.v7);
      vi_rows_processed := DBMS_SQL.execute (vi_cursor_id);
      DBMS_SQL.close_cursor (vi_cursor_id);
   END LOOP;
   CLOSE tax_driver_cursor;
   -- If no drivers found raise error, else create location matrix sql statement
   IF vc_tax_matrix_where IS NULL THEN
      vi_error_count := vi_error_count + 1;
      sp_geterrorcode('P3', an_batch_id, 'P', v_sysdate, v_severity_level, v_write_import_line_flag, v_abort_import_flag);
      IF v_severity_level > v_error_sev_code THEN
         v_error_sev_code := v_severity_level;
      END IF;
   ELSE
      vc_tax_matrix_stmt := vc_tax_matrix_select || vc_tax_matrix_where || vc_tax_matrix_orderby;
   END IF;
   -- Abort Procedure?
   IF v_abort_import_flag = '1' THEN
      v_batch_status_code := 'ABP';
      RAISE e_abort;
   END IF;

   -- Set large rollback segment
   COMMIT;

   -- ****** Read and Process Transactions ****** -----------------------------------------
   OPEN bcp_transactions_cursor;
   LOOP
      FETCH bcp_transactions_cursor INTO bcp_transactions;
      EXIT WHEN bcp_transactions_cursor%NOTFOUND;
      vn_actual_rowno := vn_actual_rowno + 1;
      v_write_import_line_flag := '1';

      -- Skip zero amounts?
      IF (v_delete_zero_amounts = '1' AND bcp_transactions.gl_line_itm_dist_amt <> 0) OR
         (v_delete_zero_amounts <> '1') THEN
         -- Skip "Original" transactions
         IF bcp_transactions.multi_trans_code LIKE 'O%' THEN    -- MBF06
            bcp_transactions.transaction_ind := 'O';
            bcp_transactions.suspend_ind := '';
         -- Hold "Future Split" transactions
         ELSIF bcp_transactions.multi_trans_code = 'FS' THEN    -- MBF06
            bcp_transactions.transaction_ind := 'H';
            bcp_transactions.suspend_ind :='';
         -- Process all other transactions
         ELSE                                                  -- MBF06
            v_rowid := bcp_transactions.rowid;
            v_processed_rows := v_processed_rows + 1;
            v_transaction_amt := v_transaction_amt + bcp_transactions.gl_line_itm_dist_amt;

            -- Perform some error checking and manually set the default value
            IF bcp_transactions.gl_company_nbr IS NULL THEN
               bcp_transactions.gl_company_nbr := ' ';
            END IF;
            IF bcp_transactions.gl_date IS NULL THEN
               bcp_transactions.gl_date := v_sysdate;
            END IF;

            -- Populate and/or Clear working fields
            bcp_transactions.process_batch_no := an_batch_id;
            bcp_transactions.transaction_ind := NULL;
            bcp_transactions.suspend_ind := NULL;
            bcp_transactions.state_taxcode_detail_id := NULL;
            bcp_transactions.county_taxcode_detail_id := NULL;
            bcp_transactions.city_taxcode_detail_id := NULL;
            bcp_transactions.tax_matrix_id := NULL;
            bcp_transactions.location_matrix_id := NULL;
-- Do not clear! Allocations may have put this in. ---> bcp_transactions.jurisdiction_id := NULL;
            bcp_transactions.jurisdiction_taxrate_id := NULL;
            bcp_transactions.taxtype_used_code := NULL;                           -- MBF08
            bcp_transactions.state_taxable_amt := NULL;                           -- MBF08
            bcp_transactions.county_taxable_amt := NULL;                          -- MBF08
            bcp_transactions.city_taxable_amt := NULL;                            -- MBF08
            --bcp_transactions.measure_type_code := NULL;                                                                  -- MBF08
            bcp_transactions.gl_extract_updater := NULL;
            bcp_transactions.gl_extract_timestamp := NULL;
            bcp_transactions.gl_extract_flag := NULL;
            bcp_transactions.gl_log_flag := NULL;
            bcp_transactions.gl_extract_amt := NULL;
            --bcp_transactions.load_timestamp := v_sysdate;                                                                   -- MBF08
            bcp_transactions.load_timestamp := SYS_EXTRACT_UTC(SYSTIMESTAMP);     -- MBF08
            bcp_transactions.update_user_id := USER;
            --bcp_transactions.update_timestamp := v_sysdate;                                                               -- MBF08
            bcp_transactions.update_timestamp := SYS_EXTRACT_UTC(SYSTIMESTAMP);   -- MBF08
            v_transaction_county := NULL;
            v_transaction_city := NULL;

            -- Hold Original Country & State
            IF bcp_transactions.transaction_country_code IS NULL THEN
               bcp_transactions.auto_transaction_country_code := '*NULL';
            ELSE
               bcp_transactions.auto_transaction_country_code := bcp_transactions.transaction_country_code;
            END IF;
            IF bcp_transactions.transaction_state_code IS NULL THEN
               bcp_transactions.auto_transaction_state_code := '*NULL';
            ELSE
               bcp_transactions.auto_transaction_state_code := bcp_transactions.transaction_state_code;
            END IF;

            /* -- Search Location Matrix for matches -- */
            -- (if transaction does not have a jurisdiction_id)
            IF bcp_transactions.jurisdiction_id IS NULL OR bcp_transactions.jurisdiction_id = 0 THEN
               BEGIN
                   OPEN location_matrix_cursor
                    FOR vc_location_matrix_stmt
                  USING v_rowid;
                  FETCH location_matrix_cursor
                   INTO location_matrix;
                  -- Rows found?
                  IF location_matrix_cursor%FOUND THEN
                     vn_fetch_rows := location_matrix_cursor%ROWCOUNT;
                  ELSE
                     vn_fetch_rows := 0;
                  END IF;
                  CLOSE location_matrix_cursor;
                  IF SQLCODE != 0 THEN
                     RAISE e_badread;
                  END IF;
               EXCEPTION
                  WHEN e_badread THEN
                     vn_fetch_rows := 0;
                     vi_error_count := vi_error_count + 1;
                        sp_geterrorcode('P4', an_batch_id, 'P', v_sysdate, v_severity_level, v_write_import_line_flag, v_abort_import_flag);
                     IF v_severity_level > v_error_sev_code THEN
                        v_error_sev_code := v_severity_level;
                     END IF;
               END;
               -- Exit if aborted
               IF v_abort_import_flag = '1' THEN
                  v_batch_status_code := 'ABP';
                  RAISE e_abort;
               END IF;

               -- Location Matrix line found
               IF vn_fetch_rows > 0 THEN
                  bcp_transactions.location_matrix_id := location_matrix.location_matrix_id;
                  bcp_transactions.jurisdiction_id := location_matrix.jurisdiction_id;
                  bcp_transactions.transaction_country_code := location_matrix.country;
                  bcp_transactions.transaction_state_code := location_matrix.state;
                  v_transaction_county := location_matrix.county;
                  v_transaction_city := location_matrix.city;
               ELSE
                  -- Location Matrix line NOT found
                  --bcp_transactions.transaction_state_code := '*DEF';
                  bcp_transactions.transaction_ind := 'S';
                  bcp_transactions.suspend_ind := 'L';
               END IF;
            ELSE
               -- Transaction already had a jurisdiction-id - get jurisdiction info
               BEGIN
                  SELECT country, state, county, city
                    INTO bcp_transactions.transaction_country_code,
                         bcp_transactions.transaction_state_code,
                         v_transaction_county,
                         v_transaction_city
                    FROM tb_jurisdiction
                   WHERE tb_jurisdiction.jurisdiction_id = bcp_transactions.jurisdiction_id;
               END;
               -- default other location matrix values
               location_matrix.override_taxtype_code := '*NO';
            END IF;

            -- Continue if not Suspended
            IF bcp_transactions.transaction_ind IS NULL OR bcp_transactions.transaction_ind <> 'S' THEN
               /* -- Search Tax Matrix for matches -- */
               BEGIN
                  IF vc_state_driver_flag = '1' THEN
                      OPEN tax_matrix_cursor
                       FOR vc_tax_matrix_stmt
                     USING v_rowid, bcp_transactions.transaction_state_code;
                     FETCH tax_matrix_cursor
                      INTO tax_matrix;
                  ELSE
                      OPEN tax_matrix_cursor
                       FOR vc_tax_matrix_stmt
                     USING v_rowid;
                     FETCH tax_matrix_cursor
                      INTO tax_matrix;
                  END IF;
                  -- Rows found?
                  IF tax_matrix_cursor%FOUND THEN
                     vn_fetch_rows := tax_matrix_cursor%ROWCOUNT;
                  ELSE
                     vn_fetch_rows := 0;
                  END IF;
                  CLOSE tax_matrix_cursor;
                  IF SQLCODE != 0 THEN
                     RAISE e_badread;
                  END IF;
               EXCEPTION
                  WHEN e_badread THEN
                     vn_fetch_rows := 0;
                     vi_error_count := vi_error_count + 1;
                     sp_geterrorcode('P5', an_batch_id, 'P', v_sysdate, v_severity_level, v_write_import_line_flag, v_abort_import_flag);
                     IF v_severity_level > v_error_sev_code THEN
                        v_error_sev_code := v_severity_level;
                      END IF;
               END;
               -- Exit if aborted
               IF v_abort_import_flag = '1' THEN
                  v_batch_status_code := 'ABP';
                  RAISE e_abort;
               END IF;

               -- Tax Matrix line found
               IF vn_fetch_rows > 0 THEN
                  bcp_transactions.tax_matrix_id := tax_matrix.tax_matrix_id;
                  -- Determine "Then" or "Else" Results
                  SELECT DECODE(TRIM(tax_matrix.relation_sign),'na','then','=','else','<','then','<=','then','>','else','>=','else','<>','then','error') INTO v_less_code FROM DUAL;
                  SELECT DECODE(TRIM(tax_matrix.relation_sign),'na','then','=','then','<','else','<=','then','>','else','>=','then','<>','else','error') INTO v_equal_code FROM DUAL;
                  SELECT DECODE(TRIM(tax_matrix.relation_sign),'na','then','=','else','<','else','<=','else','>','then','>=','then','<>','then','error') INTO v_greater_code FROM DUAL;
                  SELECT DECODE(SIGN(bcp_transactions.gl_line_itm_dist_amt - NVL(tax_matrix.relation_amount,0)), -1, v_less_code, 0, v_equal_code, 1, v_greater_code, 'error') INTO v_relation_code FROM DUAL;
                  IF v_relation_code = 'then' THEN
                     v_hold_code_flag := tax_matrix.then_hold_code_flag;
                     bcp_transactions.taxcode_code := tax_matrix.then_taxcode_code;
                     --v_override_jurisdiction_id := tax_matrix.then_jurisdiction_id;
                  ELSIF v_relation_code = 'else' THEN
                     v_hold_code_flag := tax_matrix.else_hold_code_flag;
                     bcp_transactions.taxcode_code := tax_matrix.else_taxcode_code;
                     --v_override_jurisdiction_id := tax_matrix.else_jurisdiction_id;
                  ELSE
                     bcp_transactions.taxcode_code := 'ThenElseEr';
                  END IF;

                  -- ****** Get TaxCode Details ****** --
                  bcp_transactions.state_taxcode_type_code := NULL;
                  bcp_transactions.county_taxcode_type_code := NULL;
                  bcp_transactions.city_taxcode_type_code := NULL;
						-- Get Country/State/all County/all City for defaults
                  vn_state_rows := 0;
                  BEGIN
                      OPEN taxcode_detail_cursor
                       FOR vc_taxcode_detail_stmt
                     USING bcp_transactions.taxcode_code, bcp_transactions.transaction_country_code, bcp_transactions.transaction_state_code, '*ALL', '*ALL', bcp_transactions.gl_date, bcp_transactions.gl_date;
                     FETCH taxcode_detail_cursor
                      INTO taxcode_detail;
                     -- Rows found?
                     IF taxcode_detail_cursor%FOUND THEN
                        vn_state_rows := taxcode_detail_cursor%ROWCOUNT;
                     ELSE
                        vn_state_rows := 0;
                     END IF;
                     CLOSE taxcode_detail_cursor;
                     IF SQLCODE != 0 THEN
                        RAISE e_badread;
                     END IF;
                  EXCEPTION
                     WHEN e_badread THEN
                        vn_fetch_rows := 0;
                        vi_error_count := vi_error_count + 1;
                        sp_geterrorcode('P99999', an_batch_id, 'P', v_sysdate, v_severity_level, v_write_import_line_flag, v_abort_import_flag);
                        IF v_severity_level > v_error_sev_code THEN
                           v_error_sev_code := v_severity_level;
                         END IF;
                  END;
                  IF vn_state_rows > 0 THEN
                     bcp_transactions.state_taxcode_detail_id := taxcode_detail.taxcode_detail_id;
                     bcp_transactions.state_taxcode_type_code := taxcode_detail.taxcode_type_code;
                     v_state_override_taxtype_code := taxcode_detail.override_taxtype_code;
                     v_state_ratetype_code := taxcode_detail.ratetype_code;
                     v_state_taxable_threshold_amt := taxcode_detail.taxable_threshold_amt;
                     v_state_tax_limitation_amt := taxcode_detail.tax_limitation_amt;
                     v_state_cap_amt := taxcode_detail.cap_amt;
                     v_state_base_change_pct := taxcode_detail.base_change_pct;
                     v_state_special_rate := taxcode_detail.special_rate;
                     bcp_transactions.county_taxcode_type_code := taxcode_detail.taxcode_type_code;
                     v_county_taxable_threshold_amt := taxcode_detail.taxable_threshold_amt;
                     v_county_tax_limitation_amt := taxcode_detail.tax_limitation_amt;
                     v_county_cap_amt := taxcode_detail.cap_amt;
                     v_county_base_change_pct := taxcode_detail.base_change_pct;
                     v_county_special_rate := taxcode_detail.special_rate;
                     bcp_transactions.city_taxcode_type_code := taxcode_detail.taxcode_type_code;
                     v_city_taxable_threshold_amt := taxcode_detail.taxable_threshold_amt;
                     v_city_tax_limitation_amt := taxcode_detail.tax_limitation_amt;
                     v_city_cap_amt := taxcode_detail.cap_amt;
                     v_city_base_change_pct := taxcode_detail.base_change_pct;
                     v_city_special_rate := taxcode_detail.special_rate;
						   -- Get Country/State/County/all City for County
                     vn_county_rows := 0;
                     BEGIN
                         OPEN taxcode_detail_cursor
                          FOR vc_taxcode_detail_stmt
                        USING bcp_transactions.taxcode_code, bcp_transactions.transaction_country_code, bcp_transactions.transaction_state_code, v_transaction_county, '*ALL', bcp_transactions.gl_date, bcp_transactions.gl_date;
                        FETCH taxcode_detail_cursor
                         INTO taxcode_detail;
                        -- Rows found?
                        IF taxcode_detail_cursor%FOUND THEN
                           vn_county_rows := taxcode_detail_cursor%ROWCOUNT;
                        ELSE
                           vn_county_rows := 0;
                        END IF;
                        CLOSE taxcode_detail_cursor;
                        IF SQLCODE != 0 THEN
                           RAISE e_badread;
                        END IF;
                     EXCEPTION
                        WHEN e_badread THEN
                           vn_fetch_rows := 0;
                           vi_error_count := vi_error_count + 1;
                           sp_geterrorcode('P99999', an_batch_id, 'P', v_sysdate, v_severity_level, v_write_import_line_flag, v_abort_import_flag);
                           IF v_severity_level > v_error_sev_code THEN
                              v_error_sev_code := v_severity_level;
                            END IF;
                     END;
                     IF vn_county_rows > 0 THEN
                        bcp_transactions.county_taxcode_detail_id := taxcode_detail.taxcode_detail_id;
                        bcp_transactions.county_taxcode_type_code := taxcode_detail.taxcode_type_code;
                        v_county_taxable_threshold_amt := taxcode_detail.taxable_threshold_amt;
                        v_county_tax_limitation_amt := taxcode_detail.tax_limitation_amt;
                        v_county_cap_amt := taxcode_detail.cap_amt;
                        v_county_base_change_pct := taxcode_detail.base_change_pct;
                        v_county_special_rate := taxcode_detail.special_rate;
                     END IF;
						   -- Get Country/State/all County/City for City
                     vn_city_rows := 0;
                     BEGIN
                         OPEN taxcode_detail_cursor
                          FOR vc_taxcode_detail_stmt
                        USING bcp_transactions.taxcode_code, bcp_transactions.transaction_country_code, bcp_transactions.transaction_state_code, '*ALL', v_transaction_city, bcp_transactions.gl_date, bcp_transactions.gl_date;
                        FETCH taxcode_detail_cursor
                         INTO taxcode_detail;
                        -- Rows found?
                        IF taxcode_detail_cursor%FOUND THEN
                           vn_city_rows := taxcode_detail_cursor%ROWCOUNT;
                        ELSE
                           vn_city_rows := 0;
                        END IF;
                        CLOSE taxcode_detail_cursor;
                        IF SQLCODE != 0 THEN
                           RAISE e_badread;
                        END IF;
                     EXCEPTION
                        WHEN e_badread THEN
                           vn_fetch_rows := 0;
                           vi_error_count := vi_error_count + 1;
                           sp_geterrorcode('P99999', an_batch_id, 'P', v_sysdate, v_severity_level, v_write_import_line_flag, v_abort_import_flag);
                           IF v_severity_level > v_error_sev_code THEN
                              v_error_sev_code := v_severity_level;
                            END IF;
                     END;
                     IF vn_city_rows > 0 THEN
                        bcp_transactions.city_taxcode_detail_id := taxcode_detail.taxcode_detail_id;
                        bcp_transactions.city_taxcode_type_code := taxcode_detail.taxcode_type_code;
                        v_city_taxable_threshold_amt := taxcode_detail.taxable_threshold_amt;
                        v_city_tax_limitation_amt := taxcode_detail.tax_limitation_amt;
                        v_city_cap_amt := taxcode_detail.cap_amt;
                        v_city_base_change_pct := taxcode_detail.base_change_pct;
                        v_city_special_rate := taxcode_detail.special_rate;
                     END IF;
                  ELSE   -- If TaxCode Detail found
                     -- suspend for TaxCode Detail
                     bcp_transactions.transaction_ind := 'S';
                     bcp_transactions.suspend_ind := 'D';
                  END IF;   -- If TaxCode Detail found

--TaxCode wins on special rates, but use tax rate if no taxcode info

                  -- Continue if not suspended
                  IF bcp_transactions.transaction_ind IS NULL OR bcp_transactions.transaction_ind <> 'S' THEN
                     -- ****** DETERMINE TYPE of TAXCODE ****** --
                     -- The Tax Code is Taxable
                     IF bcp_transactions.state_taxcode_type_code = 'T' OR bcp_transactions.county_taxcode_type_code = 'T' OR bcp_transactions.city_taxcode_type_code = 'T' THEN
                        -- Determine Tax Calc Flags
                        vc_country_flag := '1';
                        vc_state_flag := '1';
                        IF bcp_transactions.state_taxcode_type_code = 'E' THEN
                           vc_state_flag := '0';
                        END IF;
                        vc_county_flag := '1';
                        vc_county_local_flag := '1';
                        IF bcp_transactions.county_taxcode_type_code = 'E' THEN
                           vc_county_flag := '0';
                           vc_county_local_flag := '1';
                        END IF;
                        vc_city_flag := '1';
                        vc_city_local_flag := '1';
                        IF bcp_transactions.city_taxcode_type_code = 'E' THEN
                           vc_city_flag := '0';
                           vc_city_local_flag := '1';
                        END IF;
                        -- Get Jurisdiction TaxRates and calculate Tax Amounts
                        sp_getusetax(bcp_transactions.jurisdiction_id,
                                     bcp_transactions.gl_date,
                                     bcp_transactions.gl_line_itm_dist_amt,
                                     'U',   -- Situs Tax Type code from Location Matrix when implemented.
                                     vc_country_flag,   -- Situs Country Nexusflag when implemented
                                     vc_state_flag,   -- Situs State Nexus flag when implemented
                                     vc_county_flag,   -- Situs County Nexus flag when implemented
                                     vc_county_local_flag,   -- Situs County Local nexus flag when implemented
                                     vc_city_flag,   -- Situs City flag when implemented
                                     vc_city_local_flag,   -- Situs City Local flag when implemented
                                     v_state_ratetype_code,
                                     v_state_override_taxtype_code,
                                     v_state_taxable_threshold_amt,
                                     v_state_tax_limitation_amt,
                                     v_state_cap_amt,
                                     v_state_base_change_pct,
                                     v_state_special_rate,
                                     v_county_taxable_threshold_amt,
                                     v_county_tax_limitation_amt,
                                     v_county_cap_amt,
                                     v_county_base_change_pct,
                                     v_county_special_rate,
                                     v_city_taxable_threshold_amt,
                                     v_city_tax_limitation_amt,
                                     v_city_cap_amt,
                                     v_city_base_change_pct,
                                     v_city_special_rate,

                                     bcp_transactions.jurisdiction_taxrate_id,
                                     bcp_transactions.taxtype_used_code,
                                     bcp_transactions.country_use_amount,
                                     bcp_transactions.state_taxable_amt,
                                     bcp_transactions.state_use_amount,
                                     bcp_transactions.state_use_tier2_amount,
                                     bcp_transactions.state_use_tier3_amount,
                                     bcp_transactions.county_taxable_amt,
                                     bcp_transactions.county_use_amount,
                                     bcp_transactions.county_local_use_amount,
                                     bcp_transactions.city_taxable_amt,
                                     bcp_transactions.city_use_amount,
                                     bcp_transactions.city_local_use_amount,
                                     bcp_transactions.country_use_rate,
                                     bcp_transactions.state_use_rate,
                                     bcp_transactions.state_use_tier2_rate,
                                     bcp_transactions.state_use_tier3_rate,
                                     bcp_transactions.state_split_amount,
                                     bcp_transactions.state_tier2_min_amount,
                                     bcp_transactions.state_tier2_max_amount,
                                     bcp_transactions.state_maxtax_amount,
                                     bcp_transactions.county_use_rate,
                                     bcp_transactions.county_local_use_rate,
                                     bcp_transactions.county_split_amount,
                                     bcp_transactions.county_maxtax_amount,
                                     bcp_transactions.county_single_flag,
                                     bcp_transactions.county_default_flag,
                                     bcp_transactions.city_use_rate,
                                     bcp_transactions.city_local_use_rate,
                                     bcp_transactions.city_split_amount,
                                     bcp_transactions.city_split_use_rate,
                                     bcp_transactions.city_single_flag,
                                     bcp_transactions.city_default_flag,
                                     bcp_transactions.combined_use_rate,
                                     bcp_transactions.tb_calc_tax_amt);
                        vn_fetch_rows := 1;
                        IF bcp_transactions.jurisdiction_taxrate_id = 0 THEN
                           vn_fetch_rows := 0;
                        END IF;
                        IF bcp_transactions.jurisdiction_taxrate_id IS NULL THEN
                           vn_fetch_rows := 0;
                           vi_error_count := vi_error_count + 1;
                           sp_geterrorcode('P6', an_batch_id, 'P', v_sysdate, v_severity_level, v_write_import_line_flag, v_abort_import_flag);
                           IF v_severity_level > v_error_sev_code THEN
                              v_error_sev_code := v_severity_level;
                           END IF;
                        END IF;
                        -- Exit if aborted
                        IF v_abort_import_flag = '1' THEN
                           v_batch_status_code := 'ABP';
                           RAISE e_abort;
                        END IF;

                        -- Jurisdiction Tax Rate FOUND -- 3351 --
                        IF vn_fetch_rows > 0 THEN
                           NULL;
                        ELSE
                           -- Jurisdiciton Tax Rate NOT Found
                           bcp_transactions.transaction_ind := 'S';
                           bcp_transactions.suspend_ind := 'R';
                        END IF;

                     -- All TaxCodes are Exempt
                     ELSIF bcp_transactions.state_taxcode_type_code = 'E' AND bcp_transactions.county_taxcode_type_code = 'E' AND bcp_transactions.city_taxcode_type_code = 'E' THEN
                        bcp_transactions.transaction_ind := 'P';

                     -- The TaxCode is Unrecognized - Suspend
                     ELSE
                        bcp_transactions.transaction_ind := 'S';
                        bcp_transactions.suspend_ind := '?';
                     END IF;

                     -- Continue if not Suspended
                     IF bcp_transactions.transaction_ind IS NULL OR bcp_transactions.transaction_ind <> 'S' THEN
                        -- Check for matrix "H"old AND and global "H"old ELSE "P"rocessed
                        IF ( v_hold_code_flag = '1' ) THEN
                           bcp_transactions.transaction_ind := 'H';
                        ELSIF ( v_global_hold_flag = '1' )
                          AND ( ABS(bcp_transactions.gl_line_itm_dist_amt) >= v_global_hold_amt ) THEN
                                 bcp_transactions.transaction_ind := 'H';
                        ELSE
                           bcp_transactions.transaction_ind := 'P';
                        END IF;
                     END IF;
                  END IF;   -- if not suspended
               -- Tax Matrix Line NOT Found
               ELSE   -- if Tax Matrix line found
                  --IF bcp_transactions.transaction_state_code = '*DEF' THEN
                  --   bcp_transactions.transaction_ind := 'S';
                  --   bcp_transactions.suspend_ind := 'L';
                  --ELSE
                     bcp_transactions.transaction_ind := 'S';
                     bcp_transactions.suspend_ind := 'T';
                  --END IF;
               END IF;   -- if Tax Matrix line found
            END IF;   -- if not suspended
         END IF;   -- if multi_trans_code LIKE 'O%' THEN

         -- Write line?
         IF v_write_import_line_flag = '1' THEN
            -- Check for Auto-Process Material Limit on Suspended Lines
            IF v_autoproc_flag = '1' THEN
               IF bcp_transactions.transaction_ind = 'S' AND bcp_transactions.suspend_ind = 'T' THEN
                  IF ABS(bcp_transactions.gl_line_itm_dist_amt) <= vn_autoproc_amount THEN
                     bcp_transactions.transaction_ind := 'P';
                     bcp_transactions.suspend_ind := '';
                     bcp_transactions.manual_taxcode_ind := 'AE';
                  END IF;
               END IF;
            END IF;

            -- Get next transaction ID for the new transaction detail record
            SELECT sq_tb_transaction_detail_id.NEXTVAL
              INTO bcp_transactions.transaction_detail_id
              FROM DUAL;

            -- Insert transaction detail row
            BEGIN
               INSERT INTO tb_transaction_detail (
                  transaction_detail_id,
                  source_transaction_id,
                  process_batch_no,
                  gl_extract_batch_no,
                  archive_batch_no,
                  allocation_matrix_id,
                  allocation_subtrans_id,
                  entered_date,
                  transaction_status,
                  gl_date,
                  gl_company_nbr,
                  gl_company_name,
                  gl_division_nbr,
                  gl_division_name,
                  gl_cc_nbr_dept_id,
                  gl_cc_nbr_dept_name,
                  gl_local_acct_nbr,
                  gl_local_acct_name,
                  gl_local_sub_acct_nbr,
                  gl_local_sub_acct_name,
                  gl_full_acct_nbr,
                  gl_full_acct_name,
                  gl_line_itm_dist_amt,
                  orig_gl_line_itm_dist_amt,
                  vendor_nbr,
                  vendor_name,
                  vendor_address_line_1,
                  vendor_address_line_2,
                  vendor_address_line_3,
                  vendor_address_line_4,
                  vendor_address_city,
                  vendor_address_county,
                  vendor_address_state,
                  vendor_address_zip,
                  vendor_address_country,
                  vendor_type,
                  vendor_type_name,
                  invoice_nbr,
                  invoice_desc,
                  invoice_date,
                  invoice_freight_amt,
                  invoice_discount_amt,
                  invoice_tax_amt,
                  invoice_total_amt,
                  invoice_tax_flg,
                  invoice_line_nbr,
                  invoice_line_name,
                  invoice_line_type,
                  invoice_line_type_name,
                  invoice_line_amt,
                  invoice_line_tax,
                  afe_project_nbr,
                  afe_project_name,
                  afe_category_nbr,
                  afe_category_name,
                  afe_sub_cat_nbr,
                  afe_sub_cat_name,
                  afe_use,
                  afe_contract_type,
                  afe_contract_structure,
                  afe_property_cat,
                  inventory_nbr,
                  inventory_name,
                  inventory_class,
                  inventory_class_name,
                  po_nbr,
                  po_name,
                  po_date,
                  po_line_nbr,
                  po_line_name,
                  po_line_type,
                  po_line_type_name,
                  ship_to_location,
                  ship_to_location_name,
                  ship_to_address_line_1,
                  ship_to_address_line_2,
                  ship_to_address_line_3,
                  ship_to_address_line_4,
                  ship_to_address_city,
                  ship_to_address_county,
                  ship_to_address_state,
                  ship_to_address_zip,
                  ship_to_address_country,
                  wo_nbr,
                  wo_name,
                  wo_date,
                  wo_type,
                  wo_type_desc,
                  wo_class,
                  wo_class_desc,
                  wo_entity,
                  wo_entity_desc,
                  wo_line_nbr,
                  wo_line_name,
                  wo_line_type,
                  wo_line_type_desc,
                  wo_shut_down_cd,
                  wo_shut_down_cd_desc,
                  voucher_id,
                  voucher_name,
                  voucher_date,
                  voucher_line_nbr,
                  voucher_line_desc,
                  check_nbr,
                  check_no,
                  check_date,
                  check_amt,
                  check_desc,
                  user_text_01,
                  user_text_02,
                  user_text_03,
                  user_text_04,
                  user_text_05,
                  user_text_06,
                  user_text_07,
                  user_text_08,
                  user_text_09,
                  user_text_10,
                  user_text_11,
                  user_text_12,
                  user_text_13,
                  user_text_14,
                  user_text_15,
                  user_text_16,
                  user_text_17,
                  user_text_18,
                  user_text_19,
                  user_text_20,
                  user_text_21,
                  user_text_22,
                  user_text_23,
                  user_text_24,
                  user_text_25,
                  user_text_26,
                  user_text_27,
                  user_text_28,
                  user_text_29,
                  user_text_30,
                  user_number_01,
                  user_number_02,
                  user_number_03,
                  user_number_04,
                  user_number_05,
                  user_number_06,
                  user_number_07,
                  user_number_08,
                  user_number_09,
                  user_number_10,
                  user_date_01,
                  user_date_02,
                  user_date_03,
                  user_date_04,
                  user_date_05,
                  user_date_06,
                  user_date_07,
                  user_date_08,
                  user_date_09,
                  user_date_10,
                  comments,
                  tb_calc_tax_amt,
                  state_use_amount,
                  state_use_tier2_amount,
                  state_use_tier3_amount,
                  county_use_amount,
                  county_local_use_amount,
                  city_use_amount,
                  city_local_use_amount,
                  transaction_state_code,
                  auto_transaction_state_code,
                  transaction_ind,
                  suspend_ind,
                  taxcode_detail_id,          -- Deprecated -- MBF08
                  taxcode_state_code,         -- Deprecated -- MBF08
                  taxcode_type_code,          -- Deprecated -- MBF08
                  taxcode_code,
                  manual_taxcode_ind,
                  tax_matrix_id,
                  location_matrix_id,
                  jurisdiction_id,
                  jurisdiction_taxrate_id,
                  manual_jurisdiction_ind,
                  measure_type_code,          -- Depreacated -- MBF08
                  state_use_rate,
                  state_use_tier2_rate,
                  state_use_tier3_rate,
                  state_split_amount,
                  state_tier2_min_amount,
                  state_tier2_max_amount,
                  state_maxtax_amount,
                  county_use_rate,
                  county_local_use_rate,
                  county_split_amount,
                  county_maxtax_amount,
                  county_single_flag,
                  county_default_flag,
                  city_use_rate,
                  city_local_use_rate,
                  city_split_amount,
                  city_split_use_rate,
                  city_single_flag,
                  city_default_flag,
                  combined_use_rate,
                  load_timestamp,
                  gl_extract_updater,
                  gl_extract_timestamp,
                  gl_extract_flag,
                  gl_log_flag,
                  gl_extract_amt,
                  audit_flag,
                  audit_user_id,
                  audit_timestamp,
                  modify_user_id,
                  modify_timestamp,
                  update_user_id,
                  update_timestamp,
                  country_use_amount,
                  transaction_country_code,
                  auto_transaction_country_code,
                  taxcode_country_code,
                  country_use_rate,
                  split_subtrans_id,
                  multi_trans_code,
                  tax_alloc_matrix_id,
                  state_taxcode_detail_id,
                  county_taxcode_detail_id,
                  city_taxcode_detail_id,
                  taxtype_used_code,
                  state_taxable_amt,
                  county_taxable_amt,
                  city_taxable_amt,
                  state_taxcode_type_code,
                  county_taxcode_type_code,
                  city_taxcode_type_code)
               VALUES (
                  bcp_transactions.transaction_detail_id,
                  bcp_transactions.source_transaction_id,
                  bcp_transactions.process_batch_no,
                  bcp_transactions.gl_extract_batch_no,
                  bcp_transactions.archive_batch_no,
                  bcp_transactions.allocation_matrix_id,
                  bcp_transactions.allocation_subtrans_id,
                  bcp_transactions.entered_date,
                  bcp_transactions.transaction_status,
                  bcp_transactions.gl_date,
                  bcp_transactions.gl_company_nbr,
                  bcp_transactions.gl_company_name,
                  bcp_transactions.gl_division_nbr,
                  bcp_transactions.gl_division_name,
                  bcp_transactions.gl_cc_nbr_dept_id,
                  bcp_transactions.gl_cc_nbr_dept_name,
                  bcp_transactions.gl_local_acct_nbr,
                  bcp_transactions.gl_local_acct_name,
                  bcp_transactions.gl_local_sub_acct_nbr,
                  bcp_transactions.gl_local_sub_acct_name,
                  bcp_transactions.gl_full_acct_nbr,
                  bcp_transactions.gl_full_acct_name,
                  bcp_transactions.gl_line_itm_dist_amt,
                  bcp_transactions.orig_gl_line_itm_dist_amt,
                  bcp_transactions.vendor_nbr,
                  bcp_transactions.vendor_name,
                  bcp_transactions.vendor_address_line_1,
                  bcp_transactions.vendor_address_line_2,
                  bcp_transactions.vendor_address_line_3,
                  bcp_transactions.vendor_address_line_4,
                  bcp_transactions.vendor_address_city,
                  bcp_transactions.vendor_address_county,
                  bcp_transactions.vendor_address_state,
                  bcp_transactions.vendor_address_zip,
                  bcp_transactions.vendor_address_country,
                  bcp_transactions.vendor_type,
                  bcp_transactions.vendor_type_name,
                  bcp_transactions.invoice_nbr,
                  bcp_transactions.invoice_desc,
                  bcp_transactions.invoice_date,
                  bcp_transactions.invoice_freight_amt,
                  bcp_transactions.invoice_discount_amt,
                  bcp_transactions.invoice_tax_amt,
                  bcp_transactions.invoice_total_amt,
                  bcp_transactions.invoice_tax_flg,
                  bcp_transactions.invoice_line_nbr,
                  bcp_transactions.invoice_line_name,
                  bcp_transactions.invoice_line_type,
                  bcp_transactions.invoice_line_type_name,
                  bcp_transactions.invoice_line_amt,
                  bcp_transactions.invoice_line_tax,
                  bcp_transactions.afe_project_nbr,
                  bcp_transactions.afe_project_name,
                  bcp_transactions.afe_category_nbr,
                  bcp_transactions.afe_category_name,
                  bcp_transactions.afe_sub_cat_nbr,
                  bcp_transactions.afe_sub_cat_name,
                  bcp_transactions.afe_use,
                  bcp_transactions.afe_contract_type,
                  bcp_transactions.afe_contract_structure,
                  bcp_transactions.afe_property_cat,
                  bcp_transactions.inventory_nbr,
                  bcp_transactions.inventory_name,
                  bcp_transactions.inventory_class,
                  bcp_transactions.inventory_class_name,
                  bcp_transactions.po_nbr,
                  bcp_transactions.po_name,
                  bcp_transactions.po_date,
                  bcp_transactions.po_line_nbr,
                  bcp_transactions.po_line_name,
                  bcp_transactions.po_line_type,
                  bcp_transactions.po_line_type_name,
                  bcp_transactions.ship_to_location,
                  bcp_transactions.ship_to_location_name,
                  bcp_transactions.ship_to_address_line_1,
                  bcp_transactions.ship_to_address_line_2,
                  bcp_transactions.ship_to_address_line_3,
                  bcp_transactions.ship_to_address_line_4,
                  bcp_transactions.ship_to_address_city,
                  bcp_transactions.ship_to_address_county,
                  bcp_transactions.ship_to_address_state,
                  bcp_transactions.ship_to_address_zip,
                  bcp_transactions.ship_to_address_country,
                  bcp_transactions.wo_nbr,
                  bcp_transactions.wo_name,
                  bcp_transactions.wo_date,
                  bcp_transactions.wo_type,
                  bcp_transactions.wo_type_desc,
                  bcp_transactions.wo_class,
                  bcp_transactions.wo_class_desc,
                  bcp_transactions.wo_entity,
                  bcp_transactions.wo_entity_desc,
                  bcp_transactions.wo_line_nbr,
                  bcp_transactions.wo_line_name,
                  bcp_transactions.wo_line_type,
                  bcp_transactions.wo_line_type_desc,
                  bcp_transactions.wo_shut_down_cd,
                  bcp_transactions.wo_shut_down_cd_desc,
                  bcp_transactions.voucher_id,
                  bcp_transactions.voucher_name,
                  bcp_transactions.voucher_date,
                  bcp_transactions.voucher_line_nbr,
                  bcp_transactions.voucher_line_desc,
                  bcp_transactions.check_nbr,
                  bcp_transactions.check_no,
                  bcp_transactions.check_date,
                  bcp_transactions.check_amt,
                  bcp_transactions.check_desc,
                  bcp_transactions.user_text_01,
                  bcp_transactions.user_text_02,
                  bcp_transactions.user_text_03,
                  bcp_transactions.user_text_04,
                  bcp_transactions.user_text_05,
                  bcp_transactions.user_text_06,
                  bcp_transactions.user_text_07,
                  bcp_transactions.user_text_08,
                  bcp_transactions.user_text_09,
                  bcp_transactions.user_text_10,
                  bcp_transactions.user_text_11,
                  bcp_transactions.user_text_12,
                  bcp_transactions.user_text_13,
                  bcp_transactions.user_text_14,
                  bcp_transactions.user_text_15,
                  bcp_transactions.user_text_16,
                  bcp_transactions.user_text_17,
                  bcp_transactions.user_text_18,
                  bcp_transactions.user_text_19,
                  bcp_transactions.user_text_20,
                  bcp_transactions.user_text_21,
                  bcp_transactions.user_text_22,
                  bcp_transactions.user_text_23,
                  bcp_transactions.user_text_24,
                  bcp_transactions.user_text_25,
                  bcp_transactions.user_text_26,
                  bcp_transactions.user_text_27,
                  bcp_transactions.user_text_28,
                  bcp_transactions.user_text_29,
                  bcp_transactions.user_text_30,
                  bcp_transactions.user_number_01,
                  bcp_transactions.user_number_02,
                  bcp_transactions.user_number_03,
                  bcp_transactions.user_number_04,
                  bcp_transactions.user_number_05,
                  bcp_transactions.user_number_06,
                  bcp_transactions.user_number_07,
                  bcp_transactions.user_number_08,
                  bcp_transactions.user_number_09,
                  bcp_transactions.user_number_10,
                  bcp_transactions.user_date_01,
                  bcp_transactions.user_date_02,
                  bcp_transactions.user_date_03,
                  bcp_transactions.user_date_04,
                  bcp_transactions.user_date_05,
                  bcp_transactions.user_date_06,
                  bcp_transactions.user_date_07,
                  bcp_transactions.user_date_08,
                  bcp_transactions.user_date_09,
                  bcp_transactions.user_date_10,
                  bcp_transactions.comments,
                  bcp_transactions.tb_calc_tax_amt,
                  bcp_transactions.state_use_amount,
                  bcp_transactions.state_use_tier2_amount,
                  bcp_transactions.state_use_tier3_amount,
                  bcp_transactions.county_use_amount,
                  bcp_transactions.county_local_use_amount,
                  bcp_transactions.city_use_amount,
                  bcp_transactions.city_local_use_amount,
                  bcp_transactions.transaction_state_code,
                  bcp_transactions.auto_transaction_state_code,
                  bcp_transactions.transaction_ind,
                  bcp_transactions.suspend_ind,
                  NULL,   -- taxcode_detail_id -- Deprecated
                  NULL,   -- taxcode_state_code -- Deprecated
                  NULL,   -- taxcode_type_code -- Deprecated
                  bcp_transactions.taxcode_code,
                  bcp_transactions.manual_taxcode_ind,
                  bcp_transactions.tax_matrix_id,
                  bcp_transactions.location_matrix_id,
                  bcp_transactions.jurisdiction_id,
                  bcp_transactions.jurisdiction_taxrate_id,
                  bcp_transactions.manual_jurisdiction_ind,
                  v_state_ratetype_code,
                  bcp_transactions.state_use_rate,
                  bcp_transactions.state_use_tier2_rate,
                  bcp_transactions.state_use_tier3_rate,
                  bcp_transactions.state_split_amount,
                  bcp_transactions.state_tier2_min_amount,
                  bcp_transactions.state_tier2_max_amount,
                  bcp_transactions.state_maxtax_amount,
                  bcp_transactions.county_use_rate,
                  bcp_transactions.county_local_use_rate,
                  bcp_transactions.county_split_amount,
                  bcp_transactions.county_maxtax_amount,
                  bcp_transactions.county_single_flag,
                  bcp_transactions.county_default_flag,
                  bcp_transactions.city_use_rate,
                  bcp_transactions.city_local_use_rate,
                  bcp_transactions.city_split_amount,
                  bcp_transactions.city_split_use_rate,
                  bcp_transactions.city_single_flag,
                  bcp_transactions.city_default_flag,
                  bcp_transactions.combined_use_rate,
                  bcp_transactions.load_timestamp,
                  bcp_transactions.gl_extract_updater,
                  bcp_transactions.gl_extract_timestamp,
                  bcp_transactions.gl_extract_flag,
                  bcp_transactions.gl_log_flag,
                  bcp_transactions.gl_extract_amt,
                  bcp_transactions.audit_flag,
                  bcp_transactions.audit_user_id,
                  bcp_transactions.audit_timestamp,
                  bcp_transactions.modify_user_id,
                  bcp_transactions.modify_timestamp,
                  bcp_transactions.update_user_id,
                  bcp_transactions.update_timestamp,
                  bcp_transactions.country_use_amount,
                  bcp_transactions.transaction_country_code,
                  bcp_transactions.auto_transaction_country_code,
                  NULL,   -- taxcode_country_code -- Deprecated
                  bcp_transactions.country_use_rate,
                  bcp_transactions.split_subtrans_id,
                  bcp_transactions.multi_trans_code,
                  bcp_transactions.tax_alloc_matrix_id,
                  bcp_transactions.state_taxcode_detail_id,
                  bcp_transactions.county_taxcode_detail_id,
                  bcp_transactions.city_taxcode_detail_id,
                  bcp_transactions.taxtype_used_code,
                  bcp_transactions.state_taxable_amt,
                  bcp_transactions.county_taxable_amt,
                  bcp_transactions.city_taxable_amt,
                  bcp_transactions.state_taxcode_type_code,
                  bcp_transactions.county_taxcode_type_code,
                  bcp_transactions.city_taxcode_type_code);
               -- Error Checking
               IF SQLCODE != 0 THEN
                  RAISE e_badwrite;
               END IF;
            EXCEPTION
               WHEN e_badwrite THEN
                  vi_error_count := vi_error_count + 1;
                  sp_geterrorcode('P7', an_batch_id, 'P', v_sysdate, v_severity_level, v_write_import_line_flag, v_abort_import_flag);
                  IF v_severity_level > v_error_sev_code THEN
                     v_error_sev_code := v_severity_level;
                  END IF;
            END;
            -- Exit if aborted
            IF v_abort_import_flag = '1' THEN
               v_batch_status_code := 'ABP';
               RAISE e_abort;
            END IF;
         END IF;   -- if Write Import line flag is on
      END IF;   -- skip zero amounts

      -- Check Kill Process Flag -- MBF01 -- begin
      IF v_kill_proc_flag = '1' THEN
         vn_kill_proc_counter := vn_kill_proc_counter + 1;
         IF vn_kill_proc_counter >= vn_kill_proc_count THEN
            SELECT batch_status_code
              INTO vc_batch_status_code
              FROM tb_batch
             WHERE batch_id = an_batch_id;
            IF vc_batch_status_code = 'KILLP' THEN
               v_batch_status_code := 'ABP';
               RAISE e_kill;
            END IF;
            vn_kill_proc_counter := 0;
         END IF;
      END IF;
      -- Check Kill Process Flag -- MBF01 -- end

   END LOOP;
   CLOSE bcp_transactions_cursor;
   v_batch_status_code := 'P';

   --- Remove Batch from BCP Transactions Table
/*   DELETE
     FROM tb_bcp_transactions
    WHERE process_batch_no = an_batch_id;  */

   -- MBF03 -- Run User Exit?
   /*  BEGIN
      SELECT LTRIM(RTRIM(value))
        INTO v_processuserexit_flag
        FROM tb_option
       WHERE option_code = 'PROCESSUSEREXIT' AND
             option_type_code = 'ADMIN' AND
             user_code = 'ADMIN';
   EXCEPTION
      WHEN OTHERS THEN
         v_processuserexit_flag := '0';
   END;
   IF v_processuserexit_flag IS NULL THEN
      v_processuserexit_flag := '0';
   END IF;
   -- MBF03 - Run User Exit if selected
   IF v_processuserexit_flag = '1' THEN
      vn_return := f_process_userexit(an_batch_id);
      IF vn_return < 0 THEN
         vi_error_count := vi_error_count + 1;
         sp_geterrorcode('UE4', an_batch_id, 'UE', v_sysdate, v_severity_level, v_write_import_line_flag, v_abort_import_flag);
         IF v_severity_level > v_error_sev_code THEN
            v_error_sev_code := v_severity_level;
         END IF;
      END IF;
   END IF;   */
   -- MBF03 - User Exit

   -- Update batch with final totals
   UPDATE tb_batch
      SET batch_status_code = v_batch_status_code,
          error_sev_code = v_error_sev_code,
          processed_rows = v_processed_rows,
          --actual_end_timestamp = SYSDATE,  /**SD06**/
          actual_end_timestamp = SYS_EXTRACT_UTC(SYSTIMESTAMP), /**SD06**/
          nu02 = vn_actual_rowno,
          nu03 = v_transaction_amt
    WHERE batch_id = an_batch_id;

    COMMIT;

EXCEPTION
   WHEN e_abort THEN
      Rollback;
      -- Update batch with error codes
      UPDATE tb_batch
         SET batch_status_code = v_batch_status_code,
             error_sev_code = v_error_sev_code
       WHERE batch_id = an_batch_id;
      COMMIT;

   -- Check Kill Process Flag -- MBF01 -- begin
   WHEN e_kill THEN
      Rollback;
      -- Write error to errorlog
      sp_geterrorcode('P66', an_batch_id, 'P', v_sysdate, v_severity_level, v_write_import_line_flag, v_abort_import_flag);
      IF v_severity_level > v_error_sev_code THEN
         v_error_sev_code := v_severity_level;
      END IF;
      -- Update batch with error codes
      UPDATE tb_batch
         SET batch_status_code = v_batch_status_code,
             error_sev_code = v_error_sev_code
       WHERE batch_id = an_batch_id;
      COMMIT;
   -- Check Kill Process Flag -- MBF01 -- end
END;