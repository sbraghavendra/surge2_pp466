--liquibase formatted sql
--changeset mfuller:1

/* ***  Alter v5.1.1.1 to v5.2.1.1  *** */

SPOOL alter_taxcodes.log

/* TaxCode tables */
-- Drop constraints
ALTER TABLE tb_gl_extract_map DROP CONSTRAINT tb_gl_extract_map_fk01;
ALTER TABLE tb_gl_extract_map DROP CONSTRAINT tb_gl_extract_map_fk02;
ALTER TABLE tb_jurisdiction DROP CONSTRAINT tb_jurisdiction_fk01;
ALTER TABLE tb_taxcode_state DROP PRIMARY KEY CASCADE;
ALTER TABLE tb_taxcode DROP PRIMARY KEY CASCADE;
ALTER TABLE tb_taxcode_detail DROP PRIMARY KEY CASCADE;
ALTER TABLE tb_taxcode_detail DROP CONSTRAINT tb_taxcode_detail_fk01;
ALTER TABLE tb_taxcode_detail DROP CONSTRAINT tb_taxcode_detail_fk02;
ALTER TABLE tb_taxcode_detail DROP CONSTRAINT tb_taxcode_detail_fk03;
ALTER TABLE tb_tax_matrix DROP CONSTRAINT tb_tax_matrix_fk01;
ALTER TABLE tb_tax_matrix DROP CONSTRAINT tb_tax_matrix_fk02;
ALTER TABLE tb_tax_matrix DROP CONSTRAINT tb_tax_matrix_fk03;

DROP INDEX pk_tb_taxcode_state;
DROP INDEX pk_tb_taxcode;
DROP INDEX pk_tb_taxcode_detail;


-- tb_taxcode_state
RENAME tb_taxcode_state TO v511_taxcode_state;
CREATE TABLE tb_taxcode_state (
   taxcode_country_code            VARCHAR2(10)            NOT NULL,
   taxcode_state_code              VARCHAR2(10)            NOT NULL,
   geocode                         VARCHAR2(12),
   name                            VARCHAR2(50),
   local_taxability_code           VARCHAR2(10),
   active_flag                     CHAR(1),
   custom_flag                     CHAR(1),
   modified_flag                   CHAR(1),
   update_user_id                  VARCHAR2(40)            DEFAULT USER,
   update_timestamp                DATE                    DEFAULT SYSDATE,
   CONSTRAINT pk_tb_taxcode_state PRIMARY KEY (taxcode_country_code, taxcode_state_code));

INSERT INTO tb_taxcode_state (
   taxcode_country_code,
   taxcode_state_code,
   geocode,
   name,
   local_taxability_code,
   active_flag,
   custom_flag,
   modified_flag,
   update_user_id,
   update_timestamp)
SELECT
   DECODE(upper(country),'USA','US','CANADA','CA',substr(country,1,10)),
   taxcode_state_code,
   geocode,
   name,
   'S',
   active_flag,
   '0',
   '0',
   update_user_id,
   update_timestamp
  FROM v511_taxcode_state;


-- tb_taxcode
RENAME tb_taxcode TO v511_taxcode;
CREATE TABLE tb_taxcode (
   taxcode_code                    VARCHAR2(40)            NOT NULL,
   description                     VARCHAR2(120),
   comments                        VARCHAR2(255),
   erp_taxcode                     VARCHAR2(40),
   active_flag                     CHAR(1),
   custom_flag                     CHAR(1),
   modified_flag                   CHAR(1),
   update_user_id                  VARCHAR2(40)            DEFAULT USER,
   update_timestamp                DATE                    DEFAULT SYSDATE,
   CONSTRAINT pk_tb_taxcode PRIMARY KEY (taxcode_code));

INSERT INTO tb_taxcode (
   taxcode_code,
   description,
   comments,
   erp_taxcode,
   active_flag,
   custom_flag,
   modified_flag,
   update_user_id,
   update_timestamp)
SELECT
   DECODE((SELECT count(*) FROM v511_taxcode B WHERE B.taxcode_code=A.taxcode_code),1,taxcode_code,taxcode_type_code || substr(taxcode_code,1,39)),
   description,
   comments,
   erp_taxcode,
   '1',
   '0',
   '0',
   update_user_id,
   update_timestamp
  FROM v511_taxcode A;

 
-- tb_taxcode_detail
RENAME tb_taxcode_detail TO v511_taxcode_detail;
CREATE TABLE tb_taxcode_detail (
   taxcode_detail_id               NUMBER                  NOT NULL,
   taxcode_code                    VARCHAR2(40),
   taxcode_country_code            VARCHAR2(10),
   taxcode_state_code              VARCHAR2(10),
   taxcode_county                  VARCHAR2(50),
   taxcode_city                    VARCHAR2(50),
   effective_date                  DATE,
   expiration_date                 DATE,
   taxcode_type_code               VARCHAR2(10),
   override_taxtype_code           VARCHAR2(10),
   ratetype_code                   VARCHAR2(10),
   taxable_threshold_amt           NUMBER,
   tax_limitation_amt              NUMBER,
   cap_amt                         NUMBER,
   base_change_pct                 NUMBER,
   special_rate                    NUMBER,
   active_flag                     CHAR(1),
   custom_flag                     CHAR(1),
   modified_flag                   CHAR(1),
   jurisdiction_id                 NUMBER,
   update_user_id                  VARCHAR2(40)            DEFAULT USER,
   update_timestamp                DATE                    DEFAULT SYSDATE,
   CONSTRAINT pk_tb_taxcode_detail PRIMARY KEY (taxcode_detail_id));

INSERT INTO tb_taxcode_detail (
   taxcode_detail_id,
   taxcode_code,
   taxcode_country_code,
   taxcode_state_code,
   taxcode_county,
   taxcode_city,
   effective_date,
   expiration_date,
   taxcode_type_code,
   override_taxtype_code,
   ratetype_code,
   taxable_threshold_amt,
   tax_limitation_amt,
   cap_amt,
   base_change_pct,
   special_rate,
   active_flag,
   custom_flag,
   modified_flag,
   jurisdiction_id,
   update_user_id,
   update_timestamp)
SELECT
   taxcode_detail_id,
   DECODE((SELECT count(*) FROM v511_taxcode B WHERE B.taxcode_code=A.taxcode_code),1,taxcode_code,taxcode_type_code || substr(taxcode_code,1,39)) taxcode_code,
   (SELECT taxcode_country_code FROM tb_taxcode_state B WHERE A.taxcode_state_code=B.taxcode_state_code) taxcode_country_code,
   --DECODE(taxcode_state_code,'CN','CA','US') taxcode_country_code,
   taxcode_state_code,
   ' ' taxcode_county,
   ' ' taxcode_city,
   to_date('01/01/1970','mm/dd/yyyy') effective_date,
   to_date('12/31/9999','mm/dd/yyyy') expiration_date,
   taxcode_type_code,
   DECODE(taxcode_type_code,'T',taxtype_code,NULL) override_taxtype_code,
   DECODE(taxcode_type_code,'T',measure_type_code,NULL) ratetype_code,
   DECODE(taxcode_type_code,'T',0,NULL) taxable_threshold_amt,
   DECODE(taxcode_type_code,'T',0,NULL) tax_limitation_amt,
   DECODE(taxcode_type_code,'T',0,NULL) cap_amt,
   DECODE(taxcode_type_code,'T',pct_dist_amt,NULL) base_change_pct,
   DECODE(taxcode_type_code,'T',0,NULL) special_rate,
   '1' active_flag,
   '0' custom_flag,
   '0' modified_flag,
   jurisdiction_id,
   update_user_id,
   update_timestamp
  FROM v511_taxcode_detail A;


CREATE TABLE kill_me AS
   SELECT taxcode_detail_id, min(gl_date) effective_date
     FROM tb_transaction_detail
    WHERE taxcode_detail_id IS NOT NULL
      AND taxcode_detail_id>0
 GROUP BY taxcode_detail_id;

UPDATE tb_taxcode_detail
   SET effective_date = NVL((SELECT effective_date FROM kill_me where tb_taxcode_detail.taxcode_detail_id=kill_me.taxcode_detail_id),to_date('01/01/1970','mm/dd/yyyy'));

DROP TABLE kill_me;

-- Build constraints
-- ALTER TABLE tb_gl_extract_map
--  ADD (CONSTRAINT tb_gl_extract_map_fk01 FOREIGN KEY(taxcode_state_code) REFERENCES tb_taxcode_state(taxcode_state_code),
--       CONSTRAINT tb_gl_extract_map_fk02 FOREIGN KEY(taxcode_code) REFERENCES tb_taxcode(taxcode_code));

ALTER TABLE tb_jurisdiction
  ADD (CONSTRAINT tb_jurisdiction_fk01 FOREIGN KEY(country, state) REFERENCES tb_taxcode_state(taxcode_country_code, taxcode_state_code));

ALTER TABLE tb_taxcode_detail
  ADD (CONSTRAINT tb_taxcode_detail_fk01 FOREIGN KEY(taxcode_country_code, taxcode_state_code) REFERENCES tb_taxcode_state(taxcode_country_code, taxcode_state_code),
       CONSTRAINT tb_taxcode_detail_fk02 FOREIGN KEY(taxcode_code) REFERENCES tb_taxcode(taxcode_code),
       CONSTRAINT tb_taxcode_detail_fk03 FOREIGN KEY(jurisdiction_id) REFERENCES tb_jurisdiction(jurisdiction_id));
	  
--ALTER TABLE tb_tax_matrix
--  ADD (CONSTRAINT tb_tax_matrix_fk01 FOREIGN KEY(matrix_country_code, matrix_state_code) REFERENCES tb_taxcode_state(taxcode_country_code, taxcode_state_code),
--       CONSTRAINT tb_tax_matrix_fk02 FOREIGN KEY(then_taxcode_detail_id) REFERENCES tb_taxcode_detail(taxcode_detail_id),
--       CONSTRAINT tb_tax_matrix_fk03 FOREIGN KEY(else_taxcode_detail_id) REFERENCES tb_taxcode_detail(taxcode_detail_id));

-- tb_bcp_taxcode_rules
CREATE TABLE tb_bcp_taxcode_rules (
   bcp_taxcode_rule_id             NUMBER                  NOT NULL,
   batch_id                        NUMBER,
   taxcode_country_code            VARCHAR2(10),
   taxcode_state_code              VARCHAR2(10),
   taxcode_county                  VARCHAR2(50),
   taxcode_city                    VARCHAR2(50),
   taxcode_code                    VARCHAR2(40),
   description                     VARCHAR2(120),
   comments                        VARCHAR2(255),
   effective_date                  DATE,
   expiration_date                 DATE,
   taxcode_type_code               VARCHAR2(10),
   override_taxtype_code           VARCHAR2(10),
   ratetype_code                   VARCHAR2(10),
   taxable_threshold_amt           NUMBER,
   tax_limitation_amt              NUMBER,
   cap_amt                         NUMBER,
   base_change_pct                 NUMBER,
   special_rate                    NUMBER,
   update_code                     VARCHAR2(40),
   CONSTRAINT pk_tb_bcp_taxcode_rules PRIMARY KEY (bcp_taxcode_rule_id));

CREATE SEQUENCE stscorp.sq_tb_bcp_taxcode_rule_id INCREMENT BY 1 START WITH 1 MAXVALUE 1.0E27 MINVALUE 1 NOCYCLE NOCACHE NOORDER;

/* Jurisdiction table */
-- tb_jurisdiction
ALTER TABLE tb_jurisdiction
  ADD (country VARCHAR2(50));

UPDATE tb_jurisdiction
   SET country=(SELECT taxcode_country_code FROM tb_taxcode_state WHERE taxcode_state_code=state);

/* List table */
INSERT INTO TB_LIST_CODE ( CODE_TYPE_CODE, CODE_CODE, DESCRIPTION, MESSAGE, EXPLANATION,
SEVERITY_LEVEL, WRITE_IMPORT_LINE_FLAG, ABORT_IMPORT_FLAG, UPDATE_USER_ID,
UPDATE_TIMESTAMP ) VALUES ( 
'*DEF', 'RATETYPE', 'Rate Types', NULL, NULL, NULL, NULL, NULL, 'STSCORP', SYSDATE); 
INSERT INTO TB_LIST_CODE ( CODE_TYPE_CODE, CODE_CODE, DESCRIPTION, MESSAGE, EXPLANATION,
SEVERITY_LEVEL, WRITE_IMPORT_LINE_FLAG, ABORT_IMPORT_FLAG, UPDATE_USER_ID,
UPDATE_TIMESTAMP ) VALUES ( 
'*DEF', 'LOCALTAX', 'Local Taxability Codes', NULL, NULL, NULL, NULL, NULL, 'STSCORP', SYSDATE); 
INSERT INTO TB_LIST_CODE ( CODE_TYPE_CODE, CODE_CODE, DESCRIPTION, MESSAGE, EXPLANATION,
SEVERITY_LEVEL, WRITE_IMPORT_LINE_FLAG, ABORT_IMPORT_FLAG, UPDATE_USER_ID,
UPDATE_TIMESTAMP ) VALUES ( 
'BATCHTYPE', 'TCR', 'TaxCode Rules', '0', '0', '0', '0', '0', 'STSCORP', SYSDATE); 
INSERT INTO TB_LIST_CODE ( CODE_TYPE_CODE, CODE_CODE, DESCRIPTION, MESSAGE, EXPLANATION,
SEVERITY_LEVEL, WRITE_IMPORT_LINE_FLAG, ABORT_IMPORT_FLAG, UPDATE_USER_ID,
UPDATE_TIMESTAMP ) VALUES ( 
'RATETYPE', '0', 'Tangible Personal Property', NULL, NULL, NULL, NULL, NULL, 'STSCORP', SYSDATE); 
INSERT INTO TB_LIST_CODE ( CODE_TYPE_CODE, CODE_CODE, DESCRIPTION, MESSAGE, EXPLANATION,
SEVERITY_LEVEL, WRITE_IMPORT_LINE_FLAG, ABORT_IMPORT_FLAG, UPDATE_USER_ID,
UPDATE_TIMESTAMP ) VALUES ( 
'RATETYPE', '1', 'Services', NULL, NULL, NULL, NULL, NULL, 'STSCORP', SYSDATE); 
INSERT INTO TB_LIST_CODE ( CODE_TYPE_CODE, CODE_CODE, DESCRIPTION, MESSAGE, EXPLANATION,
SEVERITY_LEVEL, WRITE_IMPORT_LINE_FLAG, ABORT_IMPORT_FLAG, UPDATE_USER_ID,
UPDATE_TIMESTAMP ) VALUES ( 
'LOCALTAX', 'S', 'State Only', NULL, NULL, NULL, NULL, NULL, 'STSCORP', SYSDATE); 
INSERT INTO TB_LIST_CODE ( CODE_TYPE_CODE, CODE_CODE, DESCRIPTION, MESSAGE, EXPLANATION,
SEVERITY_LEVEL, WRITE_IMPORT_LINE_FLAG, ABORT_IMPORT_FLAG, UPDATE_USER_ID,
UPDATE_TIMESTAMP ) VALUES ( 
'LOCALTAX', 'L', 'State & Local', NULL, NULL, NULL, NULL, NULL, 'STSCORP', SYSDATE); 
 
commit;

SHOW ERRORS
SPOOL OFF