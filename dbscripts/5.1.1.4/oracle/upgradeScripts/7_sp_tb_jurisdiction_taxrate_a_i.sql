CREATE OR REPLACE PROCEDURE Sp_Tb_Jurisdiction_Taxrate_A_I(p_jurisdiction_taxrate_id IN NUMBER)
/* ************************************************************************************************/
/* Object Type/Name: SP Trigger/After Insert - sp_tb_jurisdiction_taxrate                         */
/* Author:           Michael B. Fuller                                                            */
/* Date:             a long time ago                                                              */
/* Description:      A tax rate was added                                                         */
/* Arguments:        p_jurisdiction_taxrate_id(number)                                            */
/* Returns:          None                                                                         */
/* ************************************************************************************************/
/* Maintenance History:                                                                           */
/* Scan  Author       Date     Release    Description                                  Ticket #   */
/* ----- ---------- ---------- ---------- -------------------------------------------- ---------- */
/* MBF01 M. Fuller  05/06/2011            Include Country fields                       1346       */
/* ************************************************************************************************/
AS
   -- Table defined variables
   v_sysdate                       TB_TRANSACTION_DETAIL.load_timestamp%TYPE         := SYS_EXTRACT_UTC(SYSTIMESTAMP);
   v_taxtype_code                  TB_TAXCODE.taxtype_code%TYPE                      := NULL;
   v_override_taxtype_code         TB_TAXCODE.taxtype_code%TYPE                      := NULL;
   v_taxtype_used                  TB_TAXCODE.taxtype_code%TYPE                      := NULL;
   v_country_flag                  TB_LOCATION_MATRIX.country_flag%TYPE              := '0';
   v_state_flag                    TB_LOCATION_MATRIX.state_flag%TYPE                := '0';
   v_county_flag                   TB_LOCATION_MATRIX.county_flag%TYPE               := '0';
   v_county_local_flag             TB_LOCATION_MATRIX.county_local_flag%TYPE         := '0';
   v_city_flag                     TB_LOCATION_MATRIX.city_flag%TYPE                 := '0';
   v_city_local_flag               TB_LOCATION_MATRIX.city_local_flag%TYPE           := '0';

   -- temporary
   vn_taxable_amt                  NUMBER                                            := 0;

   l_tb_jurisdiction_taxrate_type TB_JURISDICTION_TAXRATE%ROWTYPE;

   -- Define Exceptions
   --e_badupdate                     exception;

-- ****** Program starts ****** --------------------------------------------------------------------
BEGIN

   SELECT *
     INTO l_tb_jurisdiction_taxrate_type
     FROM TB_JURISDICTION_TAXRATE
    WHERE jurisdiction_taxrate_id = p_jurisdiction_taxrate_id;

   DECLARE
	   -- Define Transaction Detail Cursor (tb_transaction_detail)
	   CURSOR transaction_detail_cursor
	   IS
         SELECT TB_TRANSACTION_DETAIL.*
           FROM TB_TRANSACTION_DETAIL
          WHERE ( TB_TRANSACTION_DETAIL.jurisdiction_id = l_tb_jurisdiction_taxrate_type.jurisdiction_id ) AND
                ( l_tb_jurisdiction_taxrate_type.effective_date <= TB_TRANSACTION_DETAIL.gl_date ) AND ( l_tb_jurisdiction_taxrate_type.expiration_date >= TB_TRANSACTION_DETAIL.gl_date ) AND
                ( l_tb_jurisdiction_taxrate_type.measure_type_code = TB_TRANSACTION_DETAIL.measure_type_code ) AND
                ( TB_TRANSACTION_DETAIL.transaction_ind = 'S' ) AND ( TB_TRANSACTION_DETAIL.suspend_ind = 'R' )
         FOR UPDATE;
	      transaction_detail           transaction_detail_cursor%ROWTYPE;
   BEGIN
	   -- ****** Read and Process Transactions ****** -----------------------------------------
	   OPEN transaction_detail_cursor;
	   LOOP
	      FETCH transaction_detail_cursor
	       INTO transaction_detail;
	       EXIT WHEN transaction_detail_cursor%NOTFOUND;

	      -- Get Tax Type Codes
         SELECT taxtype_code
           INTO v_taxtype_code
           FROM TB_TAXCODE
          WHERE taxcode_code = transaction_detail.taxcode_code
            AND taxcode_type_code = transaction_detail.taxcode_type_code;

         -- Get Location Matrix Codes if NOT applied or overriden          -- MBF01
         IF (transaction_detail.location_matrix_id IS NOT NULL AND transaction_detail.location_matrix_id <> 0) AND
            (transaction_detail.manual_jurisdiction_ind IS NULL) THEN
            -- Get Override Tax Type Code -- 3351
            -- Get 5 Tax Calc Flags -- 3411 -- MBF02
            SELECT override_taxtype_code,
                   country_flag, state_flag, county_flag, county_local_flag, city_flag, city_local_flag
              INTO v_override_taxtype_code,
                   v_country_flag, v_state_flag, v_county_flag, v_county_local_flag, v_city_flag, v_city_local_flag
              FROM TB_LOCATION_MATRIX
             WHERE TB_LOCATION_MATRIX.location_matrix_id = transaction_detail.location_matrix_id;
         ELSE
            v_override_taxtype_code := '*NO';
            v_country_flag := '1';                         -- MBF01
            v_state_flag := '1';                           -- MBF01
            v_county_flag := '1';                          -- MBF01
            v_county_local_flag := '1';                    -- MBF01
            v_city_flag := '1';                            -- MBF01
            v_city_local_flag := '1';                      -- MBF01
         END IF;

         -- Determine Tax Type - Use or Sales
         v_taxtype_used := v_override_taxtype_code;
         IF v_taxtype_used IS NULL OR TRIM(v_taxtype_used) = '' OR TRIM(v_taxtype_used) = '*NO' THEN
            v_taxtype_used := SUBSTR(TRIM(v_taxtype_code),1,1);
            IF v_taxtype_used IS NULL OR Trim(v_taxtype_used) = '' THEN
               v_taxtype_used := 'U';
            END IF;
         END IF;

         -- Populate and/or Clear Transaction Detail working fields
         transaction_detail.country_use_amount := 0;
         transaction_detail.state_use_amount := 0;
         transaction_detail.state_use_tier2_amount := 0;
         transaction_detail.state_use_tier3_amount := 0;
         transaction_detail.county_use_amount := 0;
         transaction_detail.county_local_use_amount := 0;
         transaction_detail.city_use_amount := 0;
         transaction_detail.city_local_use_amount := 0;
         transaction_detail.tb_calc_tax_amt := 0;
         transaction_detail.gl_extract_amt := 0;
         -- future? --      transaction_detail.taxable_amt := 0;
         -- future? --      transactoin_detail.taxtype_used := v_taxtype_used;
         transaction_detail.update_user_id := USER;
         transaction_detail.update_timestamp := v_sysdate;

         -- Move Rates to parameters
         transaction_detail.transaction_ind := 'P';
         transaction_detail.suspend_ind := NULL;
         transaction_detail.jurisdiction_id := l_tb_jurisdiction_taxrate_type.jurisdiction_id;
         transaction_detail.jurisdiction_taxrate_id := l_tb_jurisdiction_taxrate_type.jurisdiction_taxrate_id;
         transaction_detail.county_split_amount := l_tb_jurisdiction_taxrate_type.county_split_amount;
         transaction_detail.county_maxtax_amount := l_tb_jurisdiction_taxrate_type.county_maxtax_amount;
         transaction_detail.county_single_flag := l_tb_jurisdiction_taxrate_type.county_single_flag;
         transaction_detail.county_default_flag := l_tb_jurisdiction_taxrate_type.county_default_flag;
         transaction_detail.city_split_amount := l_tb_jurisdiction_taxrate_type.city_split_amount;
         transaction_detail.city_single_flag := l_tb_jurisdiction_taxrate_type.city_single_flag;
         transaction_detail.city_default_flag := l_tb_jurisdiction_taxrate_type.city_default_flag;

         IF v_taxtype_used = 'U' THEN
            -- Use Tax Rates
            transaction_detail.country_use_rate := l_tb_jurisdiction_taxrate_type.country_use_rate;
            transaction_detail.state_use_rate := l_tb_jurisdiction_taxrate_type.state_use_rate;
            transaction_detail.state_use_tier2_rate := l_tb_jurisdiction_taxrate_type.state_use_tier2_rate;
            transaction_detail.state_use_tier3_rate := l_tb_jurisdiction_taxrate_type.state_use_tier3_rate;
            transaction_detail.state_split_amount := l_tb_jurisdiction_taxrate_type.state_split_amount;
            transaction_detail.state_tier2_min_amount := l_tb_jurisdiction_taxrate_type.state_tier2_min_amount;
            transaction_detail.state_tier2_max_amount := l_tb_jurisdiction_taxrate_type.state_tier2_max_amount;
            transaction_detail.state_maxtax_amount := l_tb_jurisdiction_taxrate_type.state_maxtax_amount;
            transaction_detail.county_use_rate := l_tb_jurisdiction_taxrate_type.county_use_rate;
            transaction_detail.county_local_use_rate := l_tb_jurisdiction_taxrate_type.county_local_use_rate;
            transaction_detail.city_use_rate := l_tb_jurisdiction_taxrate_type.city_use_rate;
            transaction_detail.city_local_use_rate := l_tb_jurisdiction_taxrate_type.city_local_use_rate;
            transaction_detail.city_split_use_rate := l_tb_jurisdiction_taxrate_type.city_split_use_rate;
         ELSIF v_taxtype_used = 'S' THEN
            -- Sales Tax Rates
            transaction_detail.country_use_rate := l_tb_jurisdiction_taxrate_type.country_sales_rate;
            transaction_detail.state_use_rate := l_tb_jurisdiction_taxrate_type.state_sales_rate;
            transaction_detail.state_use_tier2_rate := 0;
            transaction_detail.state_use_tier3_rate := 0;
            transaction_detail.state_split_amount := 0;
            transaction_detail.state_tier2_min_amount := 0;
            transaction_detail.state_tier2_max_amount := 999999999;
            transaction_detail.state_maxtax_amount := 999999999;
            transaction_detail.county_use_rate := l_tb_jurisdiction_taxrate_type.county_sales_rate;
            transaction_detail.county_local_use_rate := l_tb_jurisdiction_taxrate_type.county_local_sales_rate;
            transaction_detail.city_use_rate := l_tb_jurisdiction_taxrate_type.city_sales_rate;
            transaction_detail.city_local_use_rate := l_tb_jurisdiction_taxrate_type.city_local_sales_rate;
            transaction_detail.city_split_use_rate := l_tb_jurisdiction_taxrate_type.city_split_sales_rate;
         ELSE
            -- Unknown!
            transaction_detail.country_use_rate := 0;
            transaction_detail.state_use_rate := 0;
            transaction_detail.state_use_tier2_rate := 0;
            transaction_detail.state_use_tier3_rate := 0;
            transaction_detail.state_split_amount := 0;
            transaction_detail.state_tier2_min_amount := 0;
            transaction_detail.state_tier2_max_amount := 999999999;
            transaction_detail.state_maxtax_amount := 999999999;
            transaction_detail.county_use_rate := 0;
            transaction_detail.county_local_use_rate := 0;
            transaction_detail.city_use_rate := 0;
            transaction_detail.city_local_use_rate := 0;
            transaction_detail.city_split_use_rate := 0;
	      END IF;

         -- Get Jurisdiction TaxRates and calculate Tax Amounts
         Sp_Calcusetax(transaction_detail.gl_line_itm_dist_amt,
                       transaction_detail.invoice_freight_amt,
                       transaction_detail.invoice_discount_amt,
                       transaction_detail.transaction_country_code,
                       transaction_detail.transaction_state_code,
-- future? --                           transaction_detail.import_definition_code,
                       'importdefn',
                       transaction_detail.taxcode_code,
                       v_country_flag,
                       v_state_flag,
                       v_county_flag,
                       v_county_local_flag,
                       v_city_flag,
                       v_city_local_flag,
                       transaction_detail.country_use_rate,
                       transaction_detail.state_use_rate,
                       transaction_detail.state_use_tier2_rate,
                       transaction_detail.state_use_tier3_rate,
                       transaction_detail.state_split_amount,
                       transaction_detail.state_tier2_min_amount,
                       transaction_detail.state_tier2_max_amount,
                       transaction_detail.state_maxtax_amount,
                       transaction_detail.county_use_rate,
                       transaction_detail.county_local_use_rate,
                       transaction_detail.county_split_amount,
                       transaction_detail.county_maxtax_amount,
                       transaction_detail.county_single_flag,
                       transaction_detail.county_default_flag,
                       transaction_detail.city_use_rate,
                       transaction_detail.city_local_use_rate,
                       transaction_detail.city_split_amount,
                       transaction_detail.city_split_use_rate,
                       transaction_detail.city_single_flag,
                       transaction_detail.city_default_flag,
                       transaction_detail.country_use_amount,
                       transaction_detail.state_use_amount,
                       transaction_detail.state_use_tier2_amount,
                       transaction_detail.state_use_tier3_amount,
                       transaction_detail.county_use_amount,
                       transaction_detail.county_local_use_amount,
                       transaction_detail.city_use_amount,
                       transaction_detail.city_local_use_amount,
                       transaction_detail.tb_calc_tax_amt,
-- future? --                           transaction_detail.taxable_amt);
-- Targa? --                            transaction_detail.user_number_10 );
                       vn_taxable_amt );

         transaction_detail.combined_use_rate := transaction_detail.country_use_rate +
                                                 transaction_detail.state_use_rate +
                                                 transaction_detail.county_use_rate +
                                                 transaction_detail.county_local_use_rate +
                                                 transaction_detail.city_use_rate +
                                                 transaction_detail.city_local_use_rate;

         -- Check for Taxable Amount -- 3351
         IF vn_taxable_amt  <> transaction_detail.gl_line_itm_dist_amt THEN
            transaction_detail.gl_extract_amt := transaction_detail.gl_line_itm_dist_amt;
            transaction_detail.gl_line_itm_dist_amt := vn_taxable_amt;
         END IF;

         -- ----->  l_tb_jurisdiction_taxrate_type.effective_date := v_sysdate;
         -- Update transaction detail row
         BEGIN
            -- 3411 - add taxable amount and tax type used.
            UPDATE TB_TRANSACTION_DETAIL
               SET gl_line_itm_dist_amt = transaction_detail.gl_line_itm_dist_amt,
                   tb_calc_tax_amt = transaction_detail.tb_calc_tax_amt,
                   state_use_amount = transaction_detail.state_use_amount,
                   state_use_tier2_amount = transaction_detail.state_use_tier2_amount,
                   state_use_tier3_amount = transaction_detail.state_use_tier3_amount,
                   county_use_amount = transaction_detail.county_use_amount,
                   county_local_use_amount = transaction_detail.county_local_use_amount,
                   city_use_amount = transaction_detail.city_use_amount,
                   city_local_use_amount = transaction_detail.city_local_use_amount,
                   transaction_ind = transaction_detail.transaction_ind,
                   suspend_ind = transaction_detail.suspend_ind,
                   jurisdiction_id = transaction_detail.jurisdiction_id,
                   jurisdiction_taxrate_id = transaction_detail.jurisdiction_taxrate_id,
                   state_use_rate = transaction_detail.state_use_rate,
                   state_use_tier2_rate = transaction_detail.state_use_tier2_rate,
                   state_use_tier3_rate = transaction_detail.state_use_tier3_rate,
                   state_split_amount = transaction_detail.state_split_amount,
                   state_tier2_min_amount = transaction_detail.state_tier2_min_amount,
                   state_tier2_max_amount = transaction_detail.state_tier2_max_amount,
                   state_maxtax_amount = transaction_detail.state_maxtax_amount,
                   county_use_rate = transaction_detail.county_use_rate,
                   county_local_use_rate = transaction_detail.county_local_use_rate,
                   county_split_amount = transaction_detail.county_split_amount,
                   county_maxtax_amount = transaction_detail.county_maxtax_amount,
                   county_single_flag = transaction_detail.county_single_flag,
                   county_default_flag = transaction_detail.county_default_flag,
                   city_use_rate = transaction_detail.city_use_rate,
                   city_local_use_rate = transaction_detail.city_local_use_rate,
                   city_split_amount = transaction_detail.city_split_amount,
                   city_split_use_rate = transaction_detail.city_split_use_rate,
                   city_single_flag = transaction_detail.city_single_flag,
                   city_default_flag = transaction_detail.city_default_flag,
                   combined_use_rate = transaction_detail.combined_use_rate,
                   gl_extract_amt = transaction_detail.gl_extract_amt,
                   update_user_id = transaction_detail.update_user_id,
                   update_timestamp = transaction_detail.update_timestamp
             WHERE transaction_detail_id = transaction_detail.transaction_detail_id;
            -- Error Checking
            --         IF SQLCODE != 0 THEN
            --            RAISE e_badupdate;
            --         END IF;
            --      EXCEPTION
            --         WHEN e_badupdate THEN
            --            NULL;
         END;

         -- Set effective date value to earliest transaction gl_date
         -- ----->  IF transaction_detail.gl_date <= l_tb_jurisdiction_taxrate_type.effective_date THEN
         -- ----->     l_tb_jurisdiction_taxrate_type.effective_date := transaction_detail.gl_date;
         -- ----->    END IF;
      END LOOP;
      CLOSE transaction_detail_cursor;

      UPDATE tb_jurisdiction_taxrate set effective_date = l_tb_jurisdiction_taxrate_type.effective_date
       WHERE jurisdiction_taxrate_id = p_jurisdiction_taxrate_id;
   END;

--EXCEPTION
--   WHEN OTHERS THEN
--      NULL;
END;
/
