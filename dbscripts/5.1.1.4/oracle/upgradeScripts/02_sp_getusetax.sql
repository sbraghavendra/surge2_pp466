CREATE OR REPLACE PROCEDURE sp_getusetax(
/* ************************************************************************************************/
/* Object Type/Name: Stored Procedure - sp_getusetax                                              */
/* Author:           Michael B. Fuller                                                            */
/* Date:             a long time ago                                                              */
/* Description:      get use (or sales by override) tax rates and call calc routine               */
/* Arguments:        many - see "IN"s below                                                       */
/* Returns:          many = see "OUT"s below                                                      */
/* ************************************************************************************************/
/* Maintenance History:                                                                           */
/* Scan  Author       Date     Release    Description                                  Ticket #   */
/* ----- ---------- ---------- ---------- -------------------------------------------- ---------- */
/*       M. Fuller  06/02/2006 3.3.5.0    Fix processing an allocated line - Temporary 871        */
/*       M. Fuller  01/04/2007 3.3.5.1    Fix processing an allocated line - Permenant 871        */
/* MBF01 M. Fuller  08/13/2007 3.4.1.1    Add checkboxes to toggle calc of 5 rates     883        */
/* MBF02 M. Fuller  05/04/2011            Add Country for Federal Tax Level            24         */
/* MBF03 M. Fuller  08/12/2011            Add TaxCode Rules changes                    17         */
/* ************************************************************************************************/
   an_jurisdiction_id IN NUMBER,
   ad_gl_date IN DATE,
   an_gl_line_itm_dist_amt IN NUMBER,
   av_situs_taxtype_code IN VARCHAR2,
   ac_country_flag IN CHAR,
   ac_state_flag IN CHAR,
   ac_county_flag IN CHAR,
   ac_county_local_flag IN CHAR,
   ac_city_flag IN CHAR,
   ac_city_local_flag IN CHAR,
   av_state_ratetype_code IN VARCHAR2,
   av_state_override_taxtype_code IN VARCHAR2,
   an_state_taxable_threshold_amt IN NUMBER,
   an_state_tax_limitation_amt IN NUMBER,
   an_state_cap_amt IN NUMBER,
   an_state_base_change_pct IN NUMBER,
   an_state_special_rate IN NUMBER,
   an_cnty_taxable_threshold_amt IN NUMBER,
   an_cnty_tax_limitation_amt IN NUMBER,
   an_cnty_cap_amt IN NUMBER,
   an_cnty_base_change_pct IN NUMBER,
   an_cnty_special_rate IN NUMBER,
   an_city_taxable_threshold_amt IN NUMBER,
   an_city_tax_limitation_amt IN NUMBER,
   an_city_cap_amt IN NUMBER,
   an_city_base_change_pct IN NUMBER,
   an_city_special_rate IN NUMBER,

   an_jurisdiction_taxrate_id OUT NUMBER,
   av_taxtype_used OUT VARCHAR2,
   an_country_amount OUT NUMBER,
   an_state_taxable_amt OUT NUMBER,
   an_state_amount OUT NUMBER,
   an_state_use_tier2_amount OUT NUMBER,
   an_state_use_tier3_amount OUT NUMBER,
   an_county_taxable_amt OUT NUMBER,
   an_county_amount OUT NUMBER,
   an_county_local_amount OUT NUMBER,
   an_city_taxable_amt OUT NUMBER,
   an_city_amount OUT NUMBER,
   an_city_local_amount OUT NUMBER,
   an_country_rate OUT NUMBER,
   an_state_rate OUT NUMBER,
   an_state_use_tier2_rate OUT NUMBER,
   an_state_use_tier3_rate OUT NUMBER,
   an_state_split_amount OUT NUMBER,
   an_state_tier2_min_amount OUT NUMBER,
   an_state_tier2_max_amount OUT NUMBER,
   an_state_maxtax_amount OUT NUMBER,
   an_county_rate OUT NUMBER,
   an_county_local_rate OUT NUMBER,
   an_county_split_amount OUT NUMBER,
   an_county_maxtax_amount OUT NUMBER,
   ac_county_single_flag OUT CHAR,
   ac_county_default_flag OUT CHAR,
   an_city_rate OUT NUMBER,
   an_city_local_rate OUT NUMBER,
   an_city_split_amount OUT NUMBER,
   an_city_split_rate OUT NUMBER,
   ac_city_single_flag OUT CHAR,
   ac_city_default_flag OUT CHAR,
   an_combined_rate OUT NUMBER,
   an_tb_calc_tax_amt OUT NUMBER)
IS
-- Program defined variables
   vn_fetch_rows                   NUMBER                                            := 0;

-- Define Jurisdiction Tax Rates Cursor (tb_jurisdiction_taxrate)
   CURSOR jurisdiction_taxrate_cursor
   IS
        SELECT jurisdiction_taxrate_id, country_sales_rate, country_use_rate, state_sales_rate, state_use_rate, state_use_tier2_rate, state_use_tier3_rate,
               state_split_amount, state_tier2_min_amount, state_tier2_max_amount, state_maxtax_amount,
               county_sales_rate, county_use_rate, county_local_sales_rate, county_local_use_rate,
               county_split_amount, county_maxtax_amount, county_single_flag, county_default_flag,
               city_sales_rate, city_use_rate, city_local_sales_rate, city_local_use_rate,
               city_split_amount, city_split_sales_rate, city_split_use_rate, city_single_flag, city_default_flag, measure_type_code
          FROM tb_jurisdiction_taxrate
         WHERE jurisdiction_id = an_jurisdiction_id
           AND effective_date <= ad_gl_date
           AND expiration_date >= ad_gl_date
           AND (measure_type_code = av_state_ratetype_code
            OR  measure_type_code = '0')
      ORDER BY measure_type_code DESC, effective_date DESC;
      jurisdiction_taxrate         jurisdiction_taxrate_cursor%ROWTYPE;

-- Define Exceptions
   e_badread                       exception;

-- ****** Program starts ****** --------------------------------------------------------------------
BEGIN
   -- Search for Jurisdiction Tax Rates
    OPEN jurisdiction_taxrate_cursor;
   FETCH jurisdiction_taxrate_cursor
    INTO jurisdiction_taxrate;
   -- Rows found?
   IF jurisdiction_taxrate_cursor%FOUND THEN
      -- Only use 0-TPP rates if State Rate Type = 0 or 1.
      IF jurisdiction_taxrate.measure_type_code = '0' AND
         (av_state_ratetype_code <> '0' AND av_state_ratetype_code <> '1') THEN
            vn_fetch_rows := 0;
      ELSE
         vn_fetch_rows := jurisdiction_taxrate_cursor%ROWCOUNT;
      END IF;
   ELSE
      vn_fetch_rows := 0;
   END IF;
   CLOSE jurisdiction_taxrate_cursor;
   IF SQLCODE != 0 THEN
      RAISE e_badread;
   END IF;
   -- Tax Rate found
   IF vn_fetch_rows > 0 THEN
      -- Determine Tax Type - Use or Sales - Determined by Situs or overriden by State
      av_taxtype_used := av_state_override_taxtype_code;
      -- *** THIS WILL BE IMPLEMENTED WHEN SITUSING RETURNS A TAX TYPE CODE *** --
      --IF av_taxtype_used IS NULL OR TRIM(av_taxtype_used) = '' OR TRIM(av_taxtype_used) = '*NO' THEN
      --   av_taxtype_used := SUBSTR(TRIM(av_situs_taxtype_code),1,1);
      --   IF av_taxtype_used IS NULL OR Trim(av_taxtype_used) = '' THEN
      --      av_taxtype_used := 'U';
      --   END IF;
      --END IF;
      --IF av_taxtype_used IS NULL OR (Trim(av_taxtype_used) <> 'U' AND Trim(av_taxtype_used) <> 'S') THEN
      --   av_taxtype_used := 'U';
      --END IF;

      -- Move Rates to parameters
      an_jurisdiction_taxrate_id := jurisdiction_taxrate.jurisdiction_taxrate_id;
      an_county_split_amount := jurisdiction_taxrate.county_split_amount;
      an_county_maxtax_amount := jurisdiction_taxrate.county_maxtax_amount;
      ac_county_single_flag := jurisdiction_taxrate.county_single_flag;
      ac_county_default_flag := jurisdiction_taxrate.county_default_flag;
      an_city_split_amount := jurisdiction_taxrate.city_split_amount;
      ac_city_single_flag := jurisdiction_taxrate.city_single_flag;
      ac_city_default_flag := jurisdiction_taxrate.city_default_flag;

      IF av_taxtype_used = 'U' THEN
         -- Use Tax Rates
         an_country_rate := NVL(jurisdiction_taxrate.country_use_rate,0);
         an_state_rate := NVL(jurisdiction_taxrate.state_use_rate,0);
         an_state_use_tier2_rate := NVL(jurisdiction_taxrate.state_use_tier2_rate,0);
         an_state_use_tier3_rate := NVL(jurisdiction_taxrate.state_use_tier3_rate,0);
         an_state_split_amount := NVL(jurisdiction_taxrate.state_split_amount,0);
         an_state_tier2_min_amount := NVL(jurisdiction_taxrate.state_tier2_min_amount,0);
         an_state_tier2_max_amount := NVL(jurisdiction_taxrate.state_tier2_max_amount,0);
         an_state_maxtax_amount := NVL(jurisdiction_taxrate.state_maxtax_amount,0);
         an_county_rate := NVL(jurisdiction_taxrate.county_use_rate,0);
         an_county_local_rate := NVL(jurisdiction_taxrate.county_local_use_rate,0);
         an_city_rate := NVL(jurisdiction_taxrate.city_use_rate,0);
         an_city_local_rate := NVL(jurisdiction_taxrate.city_local_use_rate,0);
         an_city_split_rate := NVL(jurisdiction_taxrate.city_split_use_rate,0);
      ELSIF av_taxtype_used = 'S' THEN
         -- Sales Tax Rates
         an_country_rate := NVL(jurisdiction_taxrate.country_sales_rate,0);
         an_state_rate := NVL(jurisdiction_taxrate.state_sales_rate,0);
         an_state_use_tier2_rate := 0;
         an_state_use_tier3_rate := 0;
         an_state_split_amount := 0;
         an_state_tier2_min_amount := 0;
         an_state_tier2_max_amount := 999999999;
         an_state_maxtax_amount := 999999999;
         an_county_rate := NVL(jurisdiction_taxrate.county_sales_rate,0);
         an_county_local_rate := NVL(jurisdiction_taxrate.county_local_sales_rate,0);
         an_city_rate := NVL(jurisdiction_taxrate.city_sales_rate,0);
         an_city_local_rate := NVL(jurisdiction_taxrate.city_local_sales_rate,0);
         an_city_split_rate := NVL(jurisdiction_taxrate.city_split_sales_rate,0);
      ELSE
         -- Unknown!
         an_country_rate := 0;
         an_state_rate := 0;
         an_state_use_tier2_rate := 0;
         an_state_use_tier3_rate := 0;
         an_state_split_amount := 0;
         an_state_tier2_min_amount := 0;
         an_state_tier2_max_amount := 999999999;
         an_state_maxtax_amount := 999999999;
         an_county_rate := 0;
         an_county_local_rate := 0;
         an_city_rate := 0;
         an_city_local_rate := 0;
         an_city_split_rate := 0;
      END IF;

      -- Calculate Sales/Use Tax Amounts
      sp_calcusetax(an_gl_line_itm_dist_amt,
                    av_taxtype_used,
                    ac_country_flag,
                    ac_state_flag,
                    ac_county_flag,
                    ac_county_local_flag,
                    ac_city_flag,
                    ac_city_local_flag,
                    an_state_taxable_threshold_amt,
                    an_state_tax_limitation_amt,
                    an_state_cap_amt,
                    an_state_base_change_pct,
                    an_state_special_rate,
                    an_cnty_taxable_threshold_amt,
                    an_cnty_tax_limitation_amt,
                    an_cnty_cap_amt,
                    an_cnty_base_change_pct,
                    an_cnty_special_rate,
                    an_city_taxable_threshold_amt,
                    an_city_tax_limitation_amt,
                    an_city_cap_amt,
                    an_city_base_change_pct,
                    an_city_special_rate,

                    an_country_rate,
                    an_state_rate,
                    an_state_use_tier2_rate,
                    an_state_use_tier3_rate,
                    an_state_split_amount,
                    an_state_tier2_min_amount,
                    an_state_tier2_max_amount,
                    an_state_maxtax_amount,
                    an_county_rate,
                    an_county_local_rate,
                    an_county_split_amount,
                    an_county_maxtax_amount,
                    ac_county_single_flag,
                    ac_county_default_flag,
                    an_city_rate,
                    an_city_local_rate,
                    an_city_split_amount,
                    an_city_split_rate,
                    ac_city_single_flag,
                    ac_city_default_flag,

                    an_country_amount,
                    an_state_taxable_amt,
                    an_state_amount,
                    an_state_use_tier2_amount,
                    an_state_use_tier3_amount,
                    an_county_taxable_amt,
                    an_county_amount,
                    an_county_local_amount,
                    an_city_taxable_amt,
                    an_city_amount,
                    an_city_local_amount,
                    an_tb_calc_tax_amt);

      -- Calculate Combined Sales/Use Rate
      an_combined_rate := an_country_rate +
                          an_state_rate +
                          an_county_rate +
                          an_county_local_rate +
                          an_city_rate +
                          an_city_local_rate;
   ELSE
      -- Tax Rates not found
      an_jurisdiction_taxrate_id := 0;
   END IF;

   EXCEPTION
      WHEN e_badread THEN
         an_jurisdiction_taxrate_id := NULL;
      WHEN OTHERS THEN
         an_jurisdiction_taxrate_id := NULL;
END;
