--liquibase formatted sql
--changeset jjacob:2 stripComments:false splitStatements:false runOnChange:true

CREATE OR REPLACE FUNCTION "STSCORP"."SP_TAXCODE_DETAIL_JURIS_LIST"(
taxcode_code IN VARCHAR2,
country_code IN VARCHAR2,
state_code IN VARCHAR2,
county IN VARCHAR2,
city IN VARCHAR2,
show_local_taxability IN CHAR,
active_flag IN CHAR,
find_all_defined IN CHAR)
RETURN sys_refcursor
AS
 l_cursor sys_refcursor;
 l_sql VARCHAR2(4000);
BEGIN
  IF find_all_defined IS NOT NULL AND find_all_defined = '1' THEN
      l_sql := '
  SELECT distinct tcd.taxcode_country_code         country,
       lc.description                   country_name,
       tcd.taxcode_state_code           taxcode_state_code,
       tcs.name                         taxcode_state_name,
       DECODE(tcd.taxcode_county,''*ALL'',DECODE(tcd.taxcode_city,''*ALL'',tcs.local_taxability_code,NULL),NULL)        local_taxability_code,
	   1								HAVE_RULES_FLAG,
       tcd.taxcode_county               taxcode_county,
       tcd.taxcode_city                 taxcode_city
  FROM tb_taxcode_detail tcd
     LEFT JOIN tb_list_code lc 
            ON lc.code_type_code = ''COUNTRY'' 
           AND lc.code_code = tcd.taxcode_country_code
     LEFT JOIN tb_taxcode_state tcs
            ON tcs.taxcode_country_code = tcd.taxcode_country_code
           AND tcs.taxcode_state_code = tcd.taxcode_state_code
  WHERE tcd.taxcode_code = ''' || taxcode_code || '''';
	
	  IF active_flag IS NOT NULL THEN
	    l_sql := l_sql || ' AND tcd.ACTIVE_FLAG = ''' || active_flag || '''';
	  END IF;
  ELSIF show_local_taxability IS NULL AND county IS NULL AND city IS NULL THEN
	  l_sql := 'SELECT
	   TCS.taxcode_country_code COUNTRY,
	   LC.DESCRIPTION COUNTRY_NAME,
	   TCS.taxcode_state_code TAXCODE_STATE_CODE,
	   TCS.NAME TAXCODE_STATE_NAME,
	   TCS.local_taxability_code LOCAL_TAXABILITY_CODE,
	   (SELECT count(*)
	    FROM tb_taxcode_detail TCD
	    WHERE TCD.taxcode_country_code=TCS.taxcode_country_code
	    AND TCD.taxcode_state_code=TCS.taxcode_state_code
        AND TCD.taxcode_code = ''' || taxcode_code || ''') HAVE_RULES_FLAG,
	    ''*ALL'' TAXCODE_COUNTY,
	    ''*ALL'' TAXCODE_CITY
	FROM tb_taxcode_state TCS
	left join tb_list_code LC 
	ON LC.code_type_code = ''COUNTRY''
	AND LC.code_code = TCS.taxcode_country_code
	WHERE 1=1';
	
	  IF active_flag IS NOT NULL THEN
	    l_sql := l_sql || ' AND ACTIVE_FLAG = ''' || active_flag || '''';
	  END IF;
	
	  IF COUNTRY_CODE IS NOT NULL THEN
	    l_sql := l_sql || ' AND TCS.taxcode_country_code = ''' || COUNTRY_CODE || '''';
	  END IF;
	
	  IF STATE_CODE IS NOT NULL THEN
	    l_sql := l_sql || ' AND TCS.taxcode_state_code = ''' || STATE_CODE || '''';
	  END IF;
  ELSE
    l_sql := '';
    IF show_local_taxability IS NOT NULL OR COUNTY IS NOT NULL OR CITY IS NOT NULL THEN
   	  l_sql := l_sql || 'SELECT DISTINCT
  J.country COUNTRY,
  LC.DESCRIPTION COUNTRY_NAME,
   J.state TAXCODE_STATE_CODE,
  TCS.NAME TAXCODE_STATE_NAME,
  '''' LOCAL_TAXABILITY_CODE,
   J.county TAXCODE_COUNTY,
   ''*ALL'' TAXCODE_CITY,
   (SELECT count(*)
    FROM tb_taxcode_detail TCD
    WHERE TCD.taxcode_country_code=J.country
      AND TCD.taxcode_state_code=J.state
      AND TCD.taxcode_county=J.county
      AND TCD.taxcode_code = ''' || taxcode_code || ''') HAVE_RULES_FLAG
FROM (tb_jurisdiction J
left join tb_list_code LC 
	ON LC.code_type_code = ''COUNTRY''
	AND LC.code_code = J.country)
left join TB_TAXCODE_STATE TCS 
	ON TCS.taxcode_country_code = J.COUNTRY
	AND TCS.TAXCODE_STATE_CODE = J.STATE
    WHERE J.country = ''' || COUNTRY_CODE || ''' AND J.state = ''' || STATE_CODE || '''';
    
    IF COUNTY IS NOT NULL THEN
      l_sql := l_sql || ' AND J.county = ''' || COUNTY || '''';
    END IF;
    
    IF CITY IS NOT NULL THEN
      l_sql := l_sql || ' AND J.city = ''' || CITY || '''';
    END IF;
    
    END IF;
    IF show_local_taxability IS NOT NULL OR COUNTY IS NOT NULL OR CITY IS NOT NULL THEN
        l_sql := l_sql || ' UNION ALL ';
    END IF;
    
    IF show_local_taxability IS NOT NULL OR CITY IS NOT NULL OR COUNTY IS NOT NULL THEN
    l_sql := l_sql ||
'SELECT DISTINCT
  J.country COUNTRY,
  LC.DESCRIPTION COUNTRY_NAME,
   J.state TAXCODE_STATE_CODE,
  TCS.NAME TAXCODE_STATE_NAME,
  '''' LOCAL_TAXABILITY_CODE,
   ''*ALL'' TAXCODE_COUNTY,
   J.city TAXCODE_CITY,
   (SELECT count(*)
    FROM tb_taxcode_detail TCD
    WHERE TCD.taxcode_country_code=J.country
      AND TCD.taxcode_state_code=J.state
      AND TCD.taxcode_city=J.city
      AND TCD.taxcode_code = ''' || taxcode_code || ''') HAVE_RULES_FLAG
FROM (tb_jurisdiction J
left join tb_list_code LC 
	ON LC.code_type_code = ''COUNTRY''
	AND LC.code_code = J.country)
left join TB_TAXCODE_STATE TCS 
	ON TCS.taxcode_country_code = J.COUNTRY
	AND TCS.TAXCODE_STATE_CODE = J.STATE
WHERE J.country = ''' || COUNTRY_CODE || ''' AND J.state = ''' || STATE_CODE || '''';

	IF COUNTY IS NOT NULL THEN
      l_sql := l_sql || ' AND J.county = ''' || COUNTY || '''';
    END IF;
    
    IF CITY IS NOT NULL THEN
      l_sql := l_sql || ' AND J.city = ''' || CITY || '''';
    END IF;
    
    END IF;
  END IF;

  IF show_local_taxability IS NULL AND CITY IS NOT NULL THEN
  	l_sql := l_sql || ' order by COUNTRY, TAXCODE_STATE_CODE, TAXCODE_COUNTY, TAXCODE_CITY';
  ELSE
  	l_sql := l_sql || ' order by COUNTRY, TAXCODE_STATE_CODE, TAXCODE_CITY, TAXCODE_COUNTY';
  END IF;

  OPEN l_cursor FOR l_sql;

 RETURN l_cursor;
END;