create or replace
PROCEDURE sp_tb_jurisdiction_taxrate_a_i (
   p_jurisdiction_taxrate_id   IN     tb_jurisdiction_taxrate.jurisdiction_taxrate_id%TYPE)
/* ************************************************************************************************/
/* Object Type/Name: Stored Procedure (After Insert) - sp_tb_jurisdiction_taxrate_a_i             */
/* Author:           Michael B. Fuller                                                            */
/* Date:             a long time ago                                                              */
/* Description:      A Jurisdiction Tax Rate has been added                                       */
/* Arguments:        p_jurisdiction_taxratea_id(NUMBER)                                           */
/* Returns:          None                                                                         */
/* ************************************************************************************************/
/* Maintenance History:                                                                           */
/* Scan  Author       Date     Release    Description                                  Ticket #   */
/* ----- ---------- ---------- ---------- -------------------------------------------- ---------- */
/* MBF01 M. Fuller  08/30/2011            Add TaxCode Rules changes                    17         */
/* ************************************************************************************************/
AS
   -- Define Row Type for tb_location_matrix record
   l_tb_jurisdiction_taxrate       tb_jurisdiction_taxrate%ROWTYPE;

   -- Table defined variables
   v_state_taxcode_type_code       tb_taxcode_detail.taxcode_type_code%TYPE          := NULL;
   v_county_taxcode_type_code      tb_taxcode_detail.taxcode_type_code%TYPE          := NULL;
   v_city_taxcode_type_code        tb_taxcode_detail.taxcode_type_code%TYPE          := NULL;
   v_state_override_taxtype_code   tb_taxcode_detail.override_taxtype_code%TYPE      := NULL;
   v_state_ratetype_code           tb_taxcode_detail.ratetype_code%TYPE              := NULL;
   v_state_taxable_threshold_amt   tb_taxcode_detail.taxable_threshold_amt%TYPE      := 0;
   v_county_taxable_threshold_amt  tb_taxcode_detail.taxable_threshold_amt%TYPE      := 0;
   v_city_taxable_threshold_amt    tb_taxcode_detail.taxable_threshold_amt%TYPE      := 0;
   v_state_tax_limitation_amt      tb_taxcode_detail.tax_limitation_amt%TYPE         := 0;
   v_county_tax_limitation_amt     tb_taxcode_detail.tax_limitation_amt%TYPE         := 0;
   v_city_tax_limitation_amt       tb_taxcode_detail.tax_limitation_amt%TYPE         := 0;
   v_state_cap_amt                 tb_taxcode_detail.cap_amt%TYPE                    := 0;
   v_county_cap_amt                tb_taxcode_detail.cap_amt%TYPE                    := 0;
   v_city_cap_amt                  tb_taxcode_detail.cap_amt%TYPE                    := 0;
   v_state_base_change_pct         tb_taxcode_detail.base_change_pct%TYPE            := 0;
   v_county_base_change_pct        tb_taxcode_detail.base_change_pct%TYPE            := 0;
   v_city_base_change_pct          tb_taxcode_detail.base_change_pct%TYPE            := 0;
   v_state_special_rate            tb_taxcode_detail.special_rate%TYPE               := 0;
   v_county_special_rate           tb_taxcode_detail.special_rate%TYPE               := 0;
   v_city_special_rate             tb_taxcode_detail.special_rate%TYPE               := 0;
   v_sysdate                       TB_TRANSACTION_DETAIL.load_timestamp%TYPE         := SYS_EXTRACT_UTC(SYSTIMESTAMP);

-- Program defined variables
   vc_country_flag                 CHAR(1)                                           := '0';
   vc_state_flag                   CHAR(1)                                           := '0';
   vc_county_flag                  CHAR(1)                                           := '0';
   vc_county_local_flag            CHAR(1)                                           := '0';
   vc_city_flag                    CHAR(1)                                           := '0';
   vc_city_local_flag              CHAR(1)                                           := '0';

   -- Define Transaction Detail Cursor (tb_transaction_detail)
   CURSOR transaction_detail_cursor
   IS
      SELECT tb_transaction_detail.*
        FROM tb_transaction_detail,
             tb_taxcode_detail
       WHERE tb_transaction_detail.state_taxcode_detail_id = tb_taxcode_detail.taxcode_detail_id
         AND tb_transaction_detail.jurisdiction_id = l_tb_jurisdiction_taxrate.jurisdiction_id
         AND l_tb_jurisdiction_taxrate.effective_date <= tb_transaction_detail.gl_date
         AND l_tb_jurisdiction_taxrate.expiration_date >= tb_transaction_detail.gl_date
         AND l_tb_jurisdiction_taxrate.measure_type_code = tb_taxcode_detail.ratetype_code
         AND tb_transaction_detail.transaction_ind = 'S' AND tb_transaction_detail.suspend_ind = 'R'
      FOR UPDATE;
      transaction_detail           transaction_detail_cursor%ROWTYPE;

   -- Define Exceptions
   --e_badupdate                     exception;

-- ****** Program starts ****** --------------------------------------------------------------------
BEGIN
   -- Populate row type with jurisdiction taxrate record
   SELECT *
     INTO l_tb_jurisdiction_taxrate
     FROM tb_jurisdiction_taxrate
    WHERE jurisdiction_taxrate_id = p_jurisdiction_taxrate_id;

   -- ****** Read and Process Transactions ****** -----------------------------------------
   OPEN transaction_detail_cursor;
   LOOP
      FETCH transaction_detail_cursor
       INTO transaction_detail;
      EXIT WHEN transaction_detail_cursor%NOTFOUND;

      -- Populate and/or Clear Transaction Detail working fields
      transaction_detail.transaction_ind := 'P';
      transaction_detail.suspend_ind := NULL;
      transaction_detail.country_use_amount := 0;
      transaction_detail.state_use_amount := 0;
      transaction_detail.state_use_tier2_amount := 0;
      transaction_detail.state_use_tier3_amount := 0;
      transaction_detail.county_use_amount := 0;
      transaction_detail.county_local_use_amount := 0;
      transaction_detail.city_use_amount := 0;
      transaction_detail.city_local_use_amount := 0;
      transaction_detail.tb_calc_tax_amt := 0;
      transaction_detail.update_user_id := USER;
      transaction_detail.update_timestamp := v_sysdate;

      v_state_taxcode_type_code := NULL;
      v_state_override_taxtype_code := NULL;
      v_state_ratetype_code := NULL;
      v_state_taxable_threshold_amt := 0;
      v_state_tax_limitation_amt := 0;
      v_state_cap_amt := 0;
      v_state_base_change_pct := 0;
      v_state_special_rate := 0;
      v_county_taxcode_type_code := NULL;
      v_county_taxable_threshold_amt := 0;
      v_county_tax_limitation_amt := 0;
      v_county_cap_amt := 0;
      v_county_base_change_pct := 0;
      v_county_special_rate := 0;
      v_city_taxcode_type_code := NULL;
      v_city_taxable_threshold_amt := 0;
      v_city_tax_limitation_amt := 0;
      v_city_cap_amt := 0;
      v_city_base_change_pct := 0;
      v_city_special_rate := 0;

      IF transaction_detail.state_taxcode_type_code = 'E' THEN
         v_state_taxcode_type_code := 'E';
      ELSE
         -- Get Country/State/County/City values
         SELECT taxcode_type_code,
                override_taxtype_code,
                ratetype_code,
                taxable_threshold_amt,
                tax_limitation_amt,
                cap_amt,
                base_change_pct,
                special_rate
           INTO v_state_taxcode_type_code,
                v_state_override_taxtype_code,
                v_state_ratetype_code,
                v_state_taxable_threshold_amt,
                v_state_tax_limitation_amt,
                v_state_cap_amt,
                v_state_base_change_pct,
                v_state_special_rate
           FROM tb_taxcode_detail
          WHERE tb_taxcode_detail.taxcode_detail_id = transaction_detail.state_taxcode_detail_id;
         -- Put State values in County & City as default
         v_county_taxcode_type_code := v_state_taxcode_type_code;
         v_county_taxable_threshold_amt := v_state_taxable_threshold_amt;
         v_county_tax_limitation_amt := v_state_tax_limitation_amt;
         v_county_cap_amt := v_state_cap_amt;
         v_county_base_change_pct := v_state_base_change_pct;
         v_county_special_rate := v_state_special_rate;
         v_city_taxcode_type_code := v_state_taxcode_type_code;
         v_city_taxable_threshold_amt := v_state_taxable_threshold_amt;
         v_city_tax_limitation_amt := v_state_tax_limitation_amt;
         v_city_cap_amt := v_state_cap_amt;
         v_city_base_change_pct := v_state_base_change_pct;
         v_city_special_rate := v_state_special_rate;
      END IF;

      -- Get Country/State/County/all City for County if used
      IF transaction_detail.county_taxcode_type_code = 'E' THEN
         v_county_taxcode_type_code := 'E';
		ELSE
         IF transaction_detail.county_taxcode_detail_id IS NOT NULL AND transaction_detail.county_taxcode_detail_id <> 0 THEN
            SELECT taxcode_type_code,
                   taxable_threshold_amt,
                   tax_limitation_amt,
                   cap_amt,
                   base_change_pct,
                   special_rate
              INTO v_county_taxcode_type_code,
                   v_county_taxable_threshold_amt,
                   v_county_tax_limitation_amt,
                   v_county_cap_amt,
                   v_county_base_change_pct,
                   v_county_special_rate
              FROM tb_taxcode_detail
             WHERE tb_taxcode_detail.taxcode_detail_id = transaction_detail.county_taxcode_detail_id;
         END IF;
      END IF;

      -- Get Country/State/all County/City for City if used
      IF transaction_detail.city_taxcode_type_code = 'E' THEN
         v_city_taxcode_type_code := 'E';
		ELSE
         IF transaction_detail.city_taxcode_detail_id IS NOT NULL AND transaction_detail.city_taxcode_detail_id <> 0 THEN
            SELECT taxcode_type_code,
                   taxable_threshold_amt,
                   tax_limitation_amt,
                   cap_amt,
                   base_change_pct,
                   special_rate
              INTO v_city_taxcode_type_code,
                   v_city_taxable_threshold_amt,
                   v_city_tax_limitation_amt,
                   v_city_cap_amt,
                   v_city_base_change_pct,
                   v_city_special_rate
              FROM tb_taxcode_detail
             WHERE tb_taxcode_detail.taxcode_detail_id = transaction_detail.county_taxcode_detail_id;
         END IF;
      END IF;

      -- Determine Tax Calc Flags
      vc_country_flag := '1';
      vc_state_flag := '1';
      IF v_state_taxcode_type_code = 'E' THEN
         vc_state_flag := '0';
      END IF;
      vc_county_flag := '1';
      vc_county_local_flag := '1';
      IF v_county_taxcode_type_code = 'E' THEN
         vc_county_flag := '0';
         vc_county_local_flag := '1';
      END IF;
      vc_city_flag := '1';
      vc_city_local_flag := '1';
      IF v_city_taxcode_type_code = 'E' THEN
         vc_city_flag := '0';
         vc_city_local_flag := '1';
      END IF;

      -- Move Rates to parameters
      transaction_detail.jurisdiction_taxrate_id := l_tb_jurisdiction_taxrate.jurisdiction_taxrate_id;
      transaction_detail.county_split_amount := l_tb_jurisdiction_taxrate.county_split_amount;
      transaction_detail.county_maxtax_amount := l_tb_jurisdiction_taxrate.county_maxtax_amount;
      transaction_detail.county_single_flag := l_tb_jurisdiction_taxrate.county_single_flag;
      transaction_detail.county_default_flag := l_tb_jurisdiction_taxrate.county_default_flag;
      transaction_detail.city_split_amount := l_tb_jurisdiction_taxrate.city_split_amount;
      transaction_detail.city_single_flag := l_tb_jurisdiction_taxrate.city_single_flag;
      transaction_detail.city_default_flag := l_tb_jurisdiction_taxrate.city_default_flag;

      IF transaction_detail.taxtype_used_code = 'U' THEN
         -- Use Tax Rates
         transaction_detail.country_use_rate := NVL(l_tb_jurisdiction_taxrate.country_use_rate,0);
         transaction_detail.state_use_rate := NVL(l_tb_jurisdiction_taxrate.state_use_rate,0);
         transaction_detail.state_use_tier2_rate := NVL(l_tb_jurisdiction_taxrate.state_use_tier2_rate,0);
         transaction_detail.state_use_tier3_rate := NVL(l_tb_jurisdiction_taxrate.state_use_tier3_rate,0);
         transaction_detail.state_split_amount := NVL(l_tb_jurisdiction_taxrate.state_split_amount,0);
         transaction_detail.state_tier2_min_amount := NVL(l_tb_jurisdiction_taxrate.state_tier2_min_amount,0);
         transaction_detail.state_tier2_max_amount := NVL(l_tb_jurisdiction_taxrate.state_tier2_max_amount,0);
         transaction_detail.state_maxtax_amount := NVL(l_tb_jurisdiction_taxrate.state_maxtax_amount,0);
         transaction_detail.county_use_rate := NVL(l_tb_jurisdiction_taxrate.county_use_rate,0);
         transaction_detail.county_local_use_rate := NVL(l_tb_jurisdiction_taxrate.county_local_use_rate,0);
         transaction_detail.city_use_rate := NVL(l_tb_jurisdiction_taxrate.city_use_rate,0);
         transaction_detail.city_local_use_rate := NVL(l_tb_jurisdiction_taxrate.city_local_use_rate,0);
         transaction_detail.city_split_use_rate := NVL(l_tb_jurisdiction_taxrate.city_split_use_rate,0);
      ELSIF transaction_detail.taxtype_used_code = 'S' THEN
         -- Sales Tax Rates
         transaction_detail.country_use_rate := NVL(l_tb_jurisdiction_taxrate.country_sales_rate,0);
         transaction_detail.state_use_rate := NVL(l_tb_jurisdiction_taxrate.state_sales_rate,0);
         transaction_detail.state_use_tier2_rate := 0;
         transaction_detail.state_use_tier3_rate := 0;
         transaction_detail.state_split_amount := 0;
         transaction_detail.state_tier2_min_amount := 0;
         transaction_detail.state_tier2_max_amount := 999999999;
         transaction_detail.state_maxtax_amount := 999999999;
         transaction_detail.county_use_rate := NVL(l_tb_jurisdiction_taxrate.county_sales_rate,0);
         transaction_detail.county_local_use_rate := NVL(l_tb_jurisdiction_taxrate.county_local_sales_rate,0);
         transaction_detail.city_use_rate := NVL(l_tb_jurisdiction_taxrate.city_sales_rate,0);
         transaction_detail.city_local_use_rate := NVL(l_tb_jurisdiction_taxrate.city_local_sales_rate,0);
         transaction_detail.city_split_use_rate := NVL(l_tb_jurisdiction_taxrate.city_split_sales_rate,0);
      ELSE
         -- Unknown!
         transaction_detail.country_use_rate := 0;
         transaction_detail.state_use_rate := 0;
         transaction_detail.state_use_tier2_rate := 0;
         transaction_detail.state_use_tier3_rate := 0;
         transaction_detail.state_split_amount := 0;
         transaction_detail.state_tier2_min_amount := 0;
         transaction_detail.state_tier2_max_amount := 999999999;
         transaction_detail.state_maxtax_amount := 999999999;
         transaction_detail.county_use_rate := 0;
         transaction_detail.county_local_use_rate := 0;
         transaction_detail.city_use_rate := 0;
         transaction_detail.city_local_use_rate := 0;
         transaction_detail.city_split_use_rate := 0;
      END IF;

      -- Calculate Sales/Use Tax Amounts
      sp_calcusetax(transaction_detail.gl_line_itm_dist_amt,
                    transaction_detail.taxtype_used_code,
                    vc_country_flag,   -- Situs Country Nexusflag when implemented
                    vc_state_flag,   -- Situs State Nexus flag when implemented
                    vc_county_flag,   -- Situs County Nexus flag when implemented
                    vc_county_local_flag,   -- Situs County Local nexus flag when implemented
                    vc_city_flag,   -- Situs City flag when implemented
                    vc_city_local_flag,   -- Situs City Local flag when implemented
                    v_state_taxable_threshold_amt,
                    v_state_tax_limitation_amt,
                    v_state_cap_amt,
                    v_state_base_change_pct,
                    v_state_special_rate,
                    v_county_taxable_threshold_amt,
                    v_county_tax_limitation_amt,
                    v_county_cap_amt,
                    v_county_base_change_pct,
                    v_county_special_rate,
                    v_city_taxable_threshold_amt,
                    v_city_tax_limitation_amt,
                    v_city_cap_amt,
                    v_city_base_change_pct,
                    v_city_special_rate,

                    transaction_detail.country_use_rate,
                    transaction_detail.state_use_rate,
                    transaction_detail.state_use_tier2_rate,
                    transaction_detail.state_use_tier3_rate,
                    transaction_detail.state_split_amount,
                    transaction_detail.state_tier2_min_amount,
                    transaction_detail.state_tier2_max_amount,
                    transaction_detail.state_maxtax_amount,
                    transaction_detail.county_use_rate,
                    transaction_detail.county_local_use_rate,
                    transaction_detail.county_split_amount,
                    transaction_detail.county_maxtax_amount,
                    transaction_detail.county_single_flag,
                    transaction_detail.county_default_flag,
                    transaction_detail.city_use_rate,
                    transaction_detail.city_local_use_rate,
                    transaction_detail.city_split_amount,
                    transaction_detail.city_split_use_rate,
                    transaction_detail.city_single_flag,
                    transaction_detail.city_default_flag,

                    transaction_detail.country_use_amount,
                    transaction_detail.state_taxable_amt,
                    transaction_detail.state_use_amount,
                    transaction_detail.state_use_tier2_amount,
                    transaction_detail.state_use_tier3_amount,
                    transaction_detail.county_taxable_amt,
                    transaction_detail.county_use_amount,
                    transaction_detail.county_local_use_amount,
                    transaction_detail.city_taxable_amt,
                    transaction_detail.city_use_amount,
                    transaction_detail.city_local_use_amount,
                    transaction_detail.tb_calc_tax_amt);

      -- Calculate Combined Sales/Use Rate
      transaction_detail.combined_use_rate := transaction_detail.country_use_rate +
                          transaction_detail.state_use_rate +
                          transaction_detail.county_use_rate +
                          transaction_detail.county_local_use_rate +
                          transaction_detail.city_use_rate +
                          transaction_detail.city_local_use_rate;

      -- Update transaction detail row
      BEGIN
         UPDATE tb_transaction_detail
           SET tb_calc_tax_amt = transaction_detail.tb_calc_tax_amt,
               state_use_amount = transaction_detail.state_use_amount,
               state_use_tier2_amount = transaction_detail.state_use_tier2_amount,
               state_use_tier3_amount = transaction_detail.state_use_tier3_amount,
               county_use_amount = transaction_detail.county_use_amount,
               county_local_use_amount = transaction_detail.county_local_use_amount,
               city_use_amount = transaction_detail.city_use_amount,
               city_local_use_amount = transaction_detail.city_local_use_amount,
               transaction_ind = transaction_detail.transaction_ind,
               suspend_ind = transaction_detail.suspend_ind,
               jurisdiction_taxrate_id = transaction_detail.jurisdiction_taxrate_id,
               measure_type_code = v_state_ratetype_code,
               state_use_rate = transaction_detail.state_use_rate,
               state_use_tier2_rate = transaction_detail.state_use_tier2_rate,
               state_use_tier3_rate = transaction_detail.state_use_tier3_rate,
               state_split_amount = transaction_detail.state_split_amount,
               state_tier2_min_amount = transaction_detail.state_tier2_min_amount,
               state_tier2_max_amount = transaction_detail.state_tier2_max_amount,
               state_maxtax_amount = transaction_detail.state_maxtax_amount,
               county_use_rate = transaction_detail.county_use_rate,
               county_local_use_rate = transaction_detail.county_local_use_rate,
               county_split_amount = transaction_detail.county_split_amount,
               county_maxtax_amount = transaction_detail.county_maxtax_amount,
               county_single_flag = transaction_detail.county_single_flag,
               county_default_flag = transaction_detail.county_default_flag,
               city_use_rate = transaction_detail.city_use_rate,
               city_local_use_rate = transaction_detail.city_local_use_rate,
               city_split_amount = transaction_detail.city_split_amount,
               city_split_use_rate = transaction_detail.city_split_use_rate,
               city_single_flag = transaction_detail.city_single_flag,
               city_default_flag = transaction_detail.city_default_flag,
               combined_use_rate = transaction_detail.combined_use_rate,
               update_user_id = transaction_detail.update_user_id,
               update_timestamp = transaction_detail.update_timestamp
         WHERE transaction_detail_id = transaction_detail.transaction_detail_id;
      END;
   END LOOP;
   CLOSE transaction_detail_cursor;

--EXCEPTION
--   WHEN OTHERS THEN
--      NULL;
END;

 
 
 
 
 