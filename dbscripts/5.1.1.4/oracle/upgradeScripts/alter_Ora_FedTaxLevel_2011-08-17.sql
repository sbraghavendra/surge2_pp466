-- Alter Federal Tax Level changes for Oracle
-- 04-06-2011
-- MBF -- 08/17/2011

/* ** Tables ** */
-- tb_taxcode_state
ALTER TABLE tb_gl_extract_map DROP CONSTRAINT tb_gl_extract_map_fk01;
ALTER TABLE tb_jurisdiction DROP CONSTRAINT tb_jurisdiction_fk01;
ALTER TABLE tb_taxcode_detail DROP CONSTRAINT tb_taxcode_dtl_fk01;
ALTER TABLE tb_tax_matrix DROP CONSTRAINT tb_tax_matrix_fk01;
ALTER TABLE tb_transaction_detail DROP CONSTRAINT tb_transaction_detail_fk03; 
ALTER TABLE tb_taxcode_state DROP CONSTRAINT pk_tb_taxcode_state;
DROP INDEX pk_tb_taxcode_state;
ALTER TABLE TB_TAXCODE_STATE MODIFY COUNTRY NOT NULL;
ALTER TABLE tb_taxcode_state ADD CONSTRAINT pk_tb_taxcode_state PRIMARY KEY (country, taxcode_state_code) ENABLE VALIDATE;
UPDATE tb_taxcode_state SET country='US' WHERE upper(country) LIKE 'US%';
UPDATE tb_taxcode_state SET country='CA' WHERE upper(country) LIKE 'CA%';
UPDATE tb_taxcode_state SET country=substr(country,1,10) WHERE upper(country) NOT IN ('US', 'CA');

-- tb_bcp_jurisdiction_taxrate
ALTER TABLE tb_bcp_jurisdiction_taxrate ADD country VARCHAR2(10);
ALTER TABLE tb_bcp_jurisdiction_taxrate ADD country_sales_rate NUMBER;
ALTER TABLE tb_bcp_jurisdiction_taxrate ADD country_use_rate NUMBER;
UPDATE tb_bcp_jurisdiction_taxrate A SET country=(SELECT country FROM tb_taxcode_state B WHERE A.state=B.taxcode_state_code);

-- tb_jurisdiction
ALTER TABLE tb_jurisdiction ADD country VARCHAR2(10);
ALTER TABLE tb_jurisdiction ADD (
   CONSTRAINT tb_jurisdiction_fk01 FOREIGN KEY (country, state)
   REFERENCES tb_taxcode_state (country, taxcode_state_code));
UPDATE tb_jurisdiction A SET country=(SELECT country FROM tb_taxcode_state B WHERE A.state=B.taxcode_state_code);

-- tb_jurisdiction_taxrate
ALTER TABLE tb_jurisdiction_taxrate ADD country_sales_rate NUMBER;
ALTER TABLE tb_jurisdiction_taxrate ADD country_use_rate NUMBER;

-- tb_taxcode_detail
ALTER TABLE tb_taxcode_detail ADD taxcode_country_code VARCHAR2(10);
ALTER TABLE tb_taxcode_detail ADD (
   CONSTRAINT tb_taxcode_dtl_fk01 FOREIGN KEY (taxcode_country_code, taxcode_state_code)
   REFERENCES tb_taxcode_state (country, taxcode_state_code));
UPDATE tb_taxcode_detail A SET taxcode_country_code=(SELECT country FROM tb_taxcode_state B WHERE A.taxcode_state_code=B.taxcode_state_code);

-- tb_tax_matrix
ALTER TABLE tb_tax_matrix ADD matrix_country_code VARCHAR2(10);
ALTER TABLE tb_tax_matrix ADD (
   CONSTRAINT tb_tax_matrix_fk01 FOREIGN KEY (matrix_country_code, matrix_state_code)
   REFERENCES tb_taxcode_state (country, taxcode_state_code));
UPDATE tb_tax_matrix A SET matrix_country_code=(SELECT country FROM tb_taxcode_state B WHERE A.matrix_state_code=B.taxcode_state_code);

-- tb_tmp_tax_matrix
ALTER TABLE tb_tmp_tax_matrix ADD matrix_country_code VARCHAR2(10);

-- tb_location_matrix
ALTER TABLE tb_location_matrix ADD country VARCHAR2(10);
ALTER TABLE tb_location_matrix ADD country_flag CHAR(1 BYTE);
UPDATE tb_location_matrix A SET country=(SELECT country FROM tb_taxcode_state B WHERE A.state=B.taxcode_state_code);

-- tb_tmp_location_matrix (no java object)
ALTER TABLE tb_tmp_location_matrix ADD override_taxtype_code VARCHAR2(10 BYTE);
ALTER TABLE tb_tmp_location_matrix ADD state_flag CHAR(1 BYTE);
ALTER TABLE tb_tmp_location_matrix ADD county_flag CHAR(1 BYTE);
ALTER TABLE tb_tmp_location_matrix ADD county_local_flag CHAR(1 BYTE);
ALTER TABLE tb_tmp_location_matrix ADD city_flag CHAR(1 BYTE);
ALTER TABLE tb_tmp_location_matrix ADD city_local_flag CHAR(1 BYTE);
ALTER TABLE tb_tmp_location_matrix ADD country VARCHAR2(10);
ALTER TABLE tb_tmp_location_matrix ADD country_flag CHAR(1 BYTE);

-- tb_gl_extract_map
ALTER TABLE tb_gl_extract_map ADD taxcode_country_code VARCHAR2(10);
ALTER TABLE tb_gl_extract_map ADD (
   CONSTRAINT tb_gl_extract_map_fk01 FOREIGN KEY (taxcode_country_code, taxcode_state_code)
   REFERENCES tb_taxcode_state (country, taxcode_state_code));
UPDATE tb_gl_extract_map A SET taxcode_country_code=(SELECT country FROM tb_taxcode_state B WHERE A.taxcode_state_code=B.taxcode_state_code);

-- tb_bcp_transactions
ALTER TABLE tb_bcp_transactions  ADD country_use_amount NUMBER;
ALTER TABLE tb_bcp_transactions  ADD transaction_country_code VARCHAR2(10);
ALTER TABLE tb_bcp_transactions  ADD auto_transaction_country_code  VARCHAR2(10);
ALTER TABLE tb_bcp_transactions  ADD taxcode_country_code VARCHAR2(10);
ALTER TABLE tb_bcp_transactions  ADD country_use_rate NUMBER;

-- tb_transaction_detail
ALTER TABLE tb_transaction_detail ADD country_use_amount NUMBER;
ALTER TABLE tb_transaction_detail ADD transaction_country_code VARCHAR2(10);
ALTER TABLE tb_transaction_detail ADD auto_transaction_country_code VARCHAR2(10);
ALTER TABLE tb_transaction_detail ADD taxcode_country_code VARCHAR2(10);
ALTER TABLE tb_transaction_detail ADD country_use_rate NUMBER;
UPDATE tb_transaction_detail A SET transaction_country_code=(SELECT country FROM tb_taxcode_state B WHERE A.transaction_state_code=B.taxcode_state_code);
UPDATE tb_transaction_detail A SET taxcode_country_code=(SELECT country FROM tb_taxcode_state B WHERE A.taxcode_state_code=B.taxcode_state_code);

INSERT INTO TB_DATA_DEF_COLUMN (TABLE_NAME, COLUMN_NAME, DATA_TYPE, DATA_LENGTH, DESCRIPTION, ABBR_DESC,  MAP_FLAG) 
VALUES ('TB_TRANSACTION_DETAIL', 'COUNTRY_USE_AMOUNT', 'NUMBER', 22, 'Country Use Amount', 'Country Use Amt.', '1');

INSERT INTO TB_DATA_DEF_COLUMN (TABLE_NAME, COLUMN_NAME, DATA_TYPE, DATA_LENGTH, DESCRIPTION, ABBR_DESC,  MAP_FLAG) 
VALUES ('TB_TRANSACTION_DETAIL', 'COUNTRY_USE_RATE', 'NUMBER', 22, 'Country Use Rate', 'Country Use Rate', '1');

INSERT INTO TB_DATA_DEF_COLUMN (TABLE_NAME, COLUMN_NAME, DATA_TYPE, DATA_LENGTH, DESCRIPTION, ABBR_DESC,  MAP_FLAG) 
VALUES ('TB_TRANSACTION_DETAIL', 'TRANSACTION_COUNTRY_CODE', 'VARCHAR2', 10, 'Transaction Country Code', 'Trans. Country', '1');

INSERT INTO TB_DATA_DEF_COLUMN (TABLE_NAME, COLUMN_NAME, DATA_TYPE, DATA_LENGTH, DESCRIPTION, ABBR_DESC,  MAP_FLAG) 
VALUES ('TB_TRANSACTION_DETAIL', 'AUTO_TRANSACTION_COUNTRY_CODE', 'VARCHAR2', 10, 'Auto Transaction Country code', 'Auto Trans. Country', '1');

INSERT INTO TB_DATA_DEF_COLUMN (TABLE_NAME, COLUMN_NAME, DATA_TYPE, DATA_LENGTH, DESCRIPTION, ABBR_DESC,  MAP_FLAG) 
VALUES ('TB_TRANSACTION_DETAIL', 'TAXCODE_COUNTRY_CODE', 'VARCHAR2', 10, 'TaxCode Country code', 'TaxCode Country', '1');

-- tb_gl_export_log
ALTER TABLE tb_gl_export_log ADD country_use_amount NUMBER;
ALTER TABLE tb_gl_export_log ADD transaction_country_code VARCHAR2(10);
ALTER TABLE tb_gl_export_log ADD auto_transaction_country_code VARCHAR2(10);
ALTER TABLE tb_gl_export_log ADD taxcode_country_code VARCHAR2(10);
ALTER TABLE tb_gl_export_log ADD country_use_rate NUMBER;

-- tb_gl_report_log (no java object)
ALTER TABLE tb_gl_report_log ADD country_use_amount NUMBER;
ALTER TABLE tb_gl_report_log ADD transaction_country_code VARCHAR2(10);
ALTER TABLE tb_gl_report_log ADD auto_transaction_country_code VARCHAR2(10);
ALTER TABLE tb_gl_report_log ADD taxcode_country_code VARCHAR2(10);
ALTER TABLE tb_gl_report_log ADD country_use_rate NUMBER;

-- tb_tmp_gl_export (no java object)
ALTER TABLE tb_tmp_gl_export ADD country_use_amount NUMBER;
ALTER TABLE tb_tmp_gl_export ADD transaction_country_code VARCHAR2(10);
ALTER TABLE tb_tmp_gl_export ADD auto_transaction_country_code VARCHAR2(10);
ALTER TABLE tb_tmp_gl_export ADD taxcode_country_code VARCHAR2(10);
ALTER TABLE tb_tmp_gl_export ADD country_use_rate NUMBER;

-- tb_tmp_transaction_detail
ALTER TABLE tb_tmp_transaction_detail ADD country_use_amount NUMBER;
ALTER TABLE tb_tmp_transaction_detail ADD transaction_country_code VARCHAR2(10);
ALTER TABLE tb_tmp_transaction_detail ADD auto_transaction_country_code VARCHAR2(10);
ALTER TABLE tb_tmp_transaction_detail ADD taxcode_country_code VARCHAR2(10);
ALTER TABLE tb_tmp_transaction_detail ADD country_use_rate NUMBER;

-- tb_tmp_transaction_detail_b_u
ALTER TABLE tb_tmp_transaction_detail_b_u ADD country_use_amount NUMBER;
ALTER TABLE tb_tmp_transaction_detail_b_u ADD transaction_country_code VARCHAR2(10);
ALTER TABLE tb_tmp_transaction_detail_b_u ADD auto_transaction_country_code VARCHAR2(10);
ALTER TABLE tb_tmp_transaction_detail_b_u ADD taxcode_country_code VARCHAR2(10);
ALTER TABLE tb_tmp_transaction_detail_b_u ADD country_use_rate NUMBER;

-- tb_list_code
INSERT INTO tb_list_code (code_type_code, code_code, description, write_import_line_flag, update_user_id, update_timestamp) VALUES ('*DEF', 'COUNTRY', 'Valid Countries', '1', USER, SYS_EXTRACT_UTC(SYSTIMESTAMP));
INSERT INTO tb_list_code (code_type_code, code_code, description, write_import_line_flag, update_user_id, update_timestamp) VALUES ('COUNTRY', 'US', 'USA', '1', USER, SYS_EXTRACT_UTC(SYSTIMESTAMP));
INSERT INTO tb_list_code (code_type_code, code_code, description, write_import_line_flag, update_user_id, update_timestamp) VALUES ('COUNTRY', 'CA', 'Canada', '0', USER, SYS_EXTRACT_UTC(SYSTIMESTAMP));

-- tb_option
INSERT INTO tb_option (option_code, option_type_code, user_code, description, value, update_user_id, update_timestamp) VALUES ('DEFAULTCOUNTRY', 'USER', 'STSCORP', 'Default Country Code', 'US', USER, SYS_EXTRACT_UTC(SYSTIMESTAMP));

/* ** Functions ** */
-- change Sp_Tb_Tax_Matrix_Master_List
CREATE OR REPLACE FUNCTION Sp_Tb_Tax_Matrix_Master_List (
	p_where_clause_token	IN VARCHAR2,
	p_order_by_token	IN VARCHAR2,
	p_start_index		IN INTEGER,
	p_max_rows		IN INTEGER
) RETURN sys_refcursor
AS
	l_cursor	sys_refcursor;
BEGIN
	OPEN l_cursor FOR
	'SELECT * FROM ( SELECT B.*, rownum rnum
	  FROM (
	  SELECT DISTINCT
		T.DEFAULT_FLAG,T.MATRIX_COUNTRY_CODE,T.MATRIX_STATE_CODE,T.DRIVER_GLOBAL_FLAG,
		T.DRIVER_01,T.DRIVER_02,T.DRIVER_03,T.DRIVER_04,T.DRIVER_05,T.DRIVER_06,
		T.DRIVER_07,T.DRIVER_08,T.DRIVER_09,T.DRIVER_10,T.DRIVER_11,T.DRIVER_12,
		T.DRIVER_13,T.DRIVER_14,T.DRIVER_15,T.DRIVER_16,T.DRIVER_17,T.DRIVER_18,
		T.DRIVER_19,T.DRIVER_20,T.DRIVER_21,T.DRIVER_22,T.DRIVER_23,T.DRIVER_24,
		T.DRIVER_25,T.DRIVER_26,T.DRIVER_27,T.DRIVER_28,T.DRIVER_29,T.DRIVER_30,
		T.BINARY_WEIGHT,T.DEFAULT_BINARY_WEIGHT, T.DRIVER_01_THRU,T.DRIVER_02_THRU,T.DRIVER_03_THRU,T.DRIVER_04_THRU,T.DRIVER_05_THRU,
		T.DRIVER_06_THRU,T.DRIVER_07_THRU,T.DRIVER_08_THRU,T.DRIVER_09_THRU,T.DRIVER_10_THRU,
		T.DRIVER_11_THRU,T.DRIVER_12_THRU,T.DRIVER_13_THRU,T.DRIVER_14_THRU,T.DRIVER_15_THRU,
		T.DRIVER_16_THRU,T.DRIVER_17_THRU,T.DRIVER_18_THRU,T.DRIVER_19_THRU,T.DRIVER_20_THRU,
		T.DRIVER_21_THRU,T.DRIVER_22_THRU,T.DRIVER_23_THRU,T.DRIVER_24_THRU,T.DRIVER_25_THRU,
		T.DRIVER_26_THRU,T.DRIVER_27_THRU,T.DRIVER_28_THRU,T.DRIVER_29_THRU,T.DRIVER_30_THRU
	FROM	TB_TAX_MATRIX T
	left outer join TB_TAXCODE_DETAIL TB_TAXCODE_DETAIL_A ON (T.THEN_TAXCODE_DETAIL_ID = TB_TAXCODE_DETAIL_A.TAXCODE_DETAIL_ID)
	left outer join TB_TAXCODE_DETAIL TB_TAXCODE_DETAIL_B ON (T.ELSE_TAXCODE_DETAIL_ID = TB_TAXCODE_DETAIL_B.TAXCODE_DETAIL_ID)
	WHERE 1=1 '
	||p_where_clause_token||' '
	||p_order_by_token    ||' ) B WHERE rownum <= '|| (p_start_index + p_max_rows) ||' ) WHERE rnum > '||p_start_index;

	RETURN l_cursor;
END;
/

-- change 
CREATE OR REPLACE FUNCTION Sp_Tb_Tax_Matrix_Master_Count (
	p_where_clause_token	IN VARCHAR2
) RETURN sys_refcursor
AS
	l_cursor sys_refcursor;
BEGIN

	OPEN l_cursor FOR
	'SELECT COUNT(*) FROM ( SELECT DISTINCT
	     T.DEFAULT_FLAG,T.MATRIX_COUNTRY_CODE,T.MATRIX_STATE_CODE,T.DRIVER_GLOBAL_FLAG,T.DRIVER_01,T.DRIVER_02,T.DRIVER_03,T.DRIVER_04,T.DRIVER_05,T.DRIVER_06,
	     T.DRIVER_07,T.DRIVER_08,T.DRIVER_09,T.DRIVER_10,T.DRIVER_11,T.DRIVER_12,T.DRIVER_13,T.DRIVER_14,T.DRIVER_15,T.DRIVER_16,T.DRIVER_17,T.DRIVER_18,
	     T.DRIVER_19,T.DRIVER_20,T.DRIVER_21,T.DRIVER_22,T.DRIVER_23,T.DRIVER_24,T.DRIVER_25,T.DRIVER_26,T.DRIVER_27,T.DRIVER_28,T.DRIVER_29,T.DRIVER_30,
	     T.BINARY_WEIGHT,T.DEFAULT_BINARY_WEIGHT,T.DRIVER_01_THRU,T.DRIVER_02_THRU,T.DRIVER_03_THRU,T.DRIVER_04_THRU,T.DRIVER_05_THRU,
	     T.DRIVER_06_THRU,T.DRIVER_07_THRU,T.DRIVER_08_THRU,T.DRIVER_09_THRU,T.DRIVER_10_THRU,
	     T.DRIVER_11_THRU,T.DRIVER_12_THRU,T.DRIVER_13_THRU,T.DRIVER_14_THRU,T.DRIVER_15_THRU,
	     T.DRIVER_16_THRU,T.DRIVER_17_THRU,T.DRIVER_18_THRU,T.DRIVER_19_THRU,T.DRIVER_20_THRU,
	     T.DRIVER_21_THRU,T.DRIVER_22_THRU,T.DRIVER_23_THRU,T.DRIVER_24_THRU,T.DRIVER_25_THRU,
	     T.DRIVER_26_THRU,T.DRIVER_27_THRU,T.DRIVER_28_THRU,T.DRIVER_29_THRU,T.DRIVER_30_THRU
	FROM TB_TAX_MATRIX T
	 left outer join TB_TAXCODE_DETAIL TB_TAXCODE_DETAIL_A ON (T.THEN_TAXCODE_DETAIL_ID = TB_TAXCODE_DETAIL_A.TAXCODE_DETAIL_ID)
	 left outer join TB_TAXCODE_DETAIL TB_TAXCODE_DETAIL_B ON (T.ELSE_TAXCODE_DETAIL_ID = TB_TAXCODE_DETAIL_B.TAXCODE_DETAIL_ID)
	WHERE 1=1 '|| p_where_clause_token ||' ) B ';

	RETURN l_cursor;

END;
/

-- change
CREATE OR REPLACE FUNCTION Sp_Taxcode_Detail_List (
 p_where_clause_token IN VARCHAR2
) RETURN sys_refcursor
AS
 l_cursor sys_refcursor;
BEGIN
 OPEN l_cursor FOR
 'SELECT TB_TAXCODE_DETAIL.TAXCODE_CODE, TB_TAXCODE_DETAIL.TAXCODE_STATE_CODE,
  TB_TAXCODE_STATE.NAME, TB_TAXCODE_DETAIL.TAXCODE_TYPE_CODE,
  TB_LIST_CODE.DESCRIPTION AS TAXCODE_TYPE_DESCRIPTION,
  TB_TAXCODE.DESCRIPTION AS TAXCODE_DESCRIPTION, TB_TAXCODE.COMMENTS,
  TB_TAXCODE.TAXTYPE_CODE, TB_TAXCODE.ERP_TAXCODE,
  TB_LIST_CODE1.DESCRIPTION AS TAX_TYPE_DESCRIPTION,
  TB_TAXCODE_DETAIL.TAXCODE_DETAIL_ID,
  TB_TAXCODE_DETAIL.JURISDICTION_ID, TB_JURISDICTION.GEOCODE,
  TB_JURISDICTION.CITY, TB_JURISDICTION.COUNTY, TB_JURISDICTION.STATE,
  TB_JURISDICTION.ZIP,TB_TAXCODE.MEASURE_TYPE_CODE,TB_TAXCODE.MEASURE_TYPE_CODE, 
  TB_TAXCODE_DETAIL.TAXCODE_COUNTRY_CODE
 FROM ((((TB_TAXCODE_DETAIL INNER JOIN TB_TAXCODE_STATE ON TB_TAXCODE_DETAIL.TAXCODE_STATE_CODE = TB_TAXCODE_STATE.TAXCODE_STATE_CODE AND TB_TAXCODE_DETAIL.TAXCODE_COUNTRY_CODE = TB_TAXCODE_STATE.COUNTRY)
 INNER JOIN TB_TAXCODE ON TB_TAXCODE_DETAIL.TAXCODE_CODE = TB_TAXCODE.TAXCODE_CODE AND TB_TAXCODE_DETAIL.TAXCODE_TYPE_CODE = TB_TAXCODE.TAXCODE_TYPE_CODE)
 INNER JOIN TB_LIST_CODE ON TB_TAXCODE_DETAIL.TAXCODE_TYPE_CODE = TB_LIST_CODE.CODE_CODE)
 LEFT JOIN  TB_LIST_CODE TB_LIST_CODE1 ON TB_LIST_CODE1.CODE_TYPE_CODE = ''TAXTYPE'' AND TB_LIST_CODE1.CODE_CODE = TB_TAXCODE.TAXTYPE_CODE)
 LEFT JOIN  TB_JURISDICTION ON TB_JURISDICTION.JURISDICTION_ID = TB_TAXCODE_DETAIL.JURISDICTION_ID
 WHERE      TB_TAXCODE_DETAIL.ACTIVE_FLAG = ''1''
 AND        TB_LIST_CODE.CODE_TYPE_CODE = ''TCTYPE'' '||
 p_where_clause_token
 ;

 RETURN l_cursor;
END;
/


COMMIT;

