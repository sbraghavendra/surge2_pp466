CREATE OR REPLACE PROCEDURE sp_transaction_process(ac_error_code out varchar2 )
/* ************************************************************************************************/
/* Object Type/Name: Stored Procedure - sp_transaction_process                                    */
/* Author:           Michael B. Fuller                                                            */
/* Date:             04/20/2011                                                                   */
/* Description:      Process unprocessed transactions                                             */
/* Arguments:        None                                                                         */
/* Returns:          None                                                                         */
/* ************************************************************************************************/
/* Maintenance History:                                                                           */
/* Scan  Author       Date     Release    Description                                  Ticket #   */
/* ----- ---------- ---------- ---------- -------------------------------------------- ---------- */
/*       M. Fuller  04/20/2011            Copied from tb_batch_process                 1179       */
/* MBF01 M. Fuller  05/23/2011            Add Country for Federal Tax Level            1346       */
/* ************************************************************************************************/
IS
-- Define Cursor Type
   TYPE cursor_type is REF CURSOR;

-- Location Matrix Selection SQL
   vc_location_matrix_select       VARCHAR2(2000) :=
      'SELECT tb_location_matrix.location_matrix_id, ' ||
             'tb_location_matrix.jurisdiction_id, ' ||
             'tb_location_matrix.override_taxtype_code, ' ||
             'tb_location_matrix.country_flag,' ||
             'tb_location_matrix.state_flag,' ||
             'tb_location_matrix.county_flag,' ||
             'tb_location_matrix.county_local_flag,' ||
             'tb_location_matrix.city_flag,' ||
             'tb_location_matrix.city_local_flag,' ||
             'tb_jurisdiction.country, ' ||
             'tb_jurisdiction.state, ' ||
             'tb_jurisdiction.county, ' ||
             'tb_jurisdiction.city, ' ||
             'tb_jurisdiction.zip, ' ||
             'tb_jurisdiction.in_out ' ||
        'FROM tb_location_matrix, ' ||
             'tb_jurisdiction, ' ||
             'tb_transaction_detail ' ||
      'WHERE ( tb_transaction_detail.rowid = :v_rowid ) AND ' ||
            '( tb_location_matrix.default_flag = ''0'' AND tb_location_matrix.binary_weight > 0 ) AND ' ||
            '( tb_location_matrix.effective_date <= tb_transaction_detail.gl_date ) AND ( tb_location_matrix.expiration_date >= tb_transaction_detail.gl_date ) AND ' ||
            '( tb_location_matrix.jurisdiction_id = tb_jurisdiction.jurisdiction_id ) ';
   vc_location_matrix_where        VARCHAR2(28000) := '';
   vc_location_matrix_orderby      VARCHAR2(1000) :=
      'ORDER BY tb_location_matrix.binary_weight DESC, tb_location_matrix.significant_digits DESC, tb_location_matrix.effective_date DESC';
   vc_location_matrix_stmt         VARCHAR2 (31000);
   location_matrix_cursor          cursor_type;

-- Location Matrix Record TYPE
   TYPE location_matrix_record is RECORD (
      location_matrix_id           tb_location_matrix.location_matrix_id%TYPE,
      jurisdiction_id              tb_location_matrix.jurisdiction_id%TYPE,
      override_taxtype_code        tb_location_matrix.override_taxtype_code%TYPE,
      country_flag                 tb_location_matrix.country_flag%TYPE,
      state_flag                   tb_location_matrix.state_flag%TYPE,
      county_flag                  tb_location_matrix.county_flag%TYPE,
      county_local_flag            tb_location_matrix.county_local_flag%TYPE,
      city_flag                    tb_location_matrix.city_flag%TYPE,
      city_local_flag              tb_location_matrix.city_local_flag%TYPE,
      country                      tb_jurisdiction.country%TYPE,
      state                        tb_jurisdiction.state%TYPE,
      county                       tb_jurisdiction.county%TYPE,
      city                         tb_jurisdiction.city%TYPE,
      zip                          tb_jurisdiction.zip%TYPE,
      in_out                       tb_jurisdiction.in_out%TYPE );
   location_matrix                 location_matrix_record;

-- Tax Matrix Selection SQL
   vc_tax_matrix_select            VARCHAR2(2000) :=
      'SELECT tb_tax_matrix.tax_matrix_id, tb_tax_matrix.relation_sign, tb_tax_matrix.relation_amount, ' ||
             'tb_tax_matrix.then_hold_code_flag, tb_tax_matrix.else_hold_code_flag, ' ||
             'tb_tax_matrix.then_taxcode_detail_id, tb_tax_matrix.else_taxcode_detail_id, ' ||
             'tb_tax_matrix.then_cch_taxcat_code, tb_tax_matrix.then_cch_group_code, tb_tax_matrix.then_cch_item_code, ' ||
             'tb_tax_matrix.else_cch_taxcat_code, tb_tax_matrix.else_cch_group_code, tb_tax_matrix.else_cch_item_code, ' ||
             'thentaxcddtl.taxcode_country_code, thentaxcddtl.taxcode_state_code, thentaxcddtl.taxcode_type_code, thentaxcddtl.taxcode_code, thentaxcddtl.jurisdiction_id, thentaxcddtl.measure_type_code, thentaxcddtl.taxtype_code, ' ||
             'elsetaxcddtl.taxcode_country_code, elsetaxcddtl.taxcode_state_code, elsetaxcddtl.taxcode_type_code, elsetaxcddtl.taxcode_code, elsetaxcddtl.jurisdiction_id, elsetaxcddtl.measure_type_code, elsetaxcddtl.taxtype_code ' ||
       'FROM tb_tax_matrix, ' ||
            'tb_taxcode_detail thentaxcddtl, ' ||
            'tb_taxcode_detail elsetaxcddtl, ' ||
            'tb_transaction_detail ' ||
      'WHERE ( tb_transaction_detail.rowid = :v_rowid ) AND ' ||
            '( tb_tax_matrix.default_flag = ''0'' AND tb_tax_matrix.binary_weight > 0 ) AND ' ||
            '(( tb_tax_matrix.matrix_country_code = ''*ALL'' ) OR ( tb_tax_matrix.matrix_country_code = :v_transaction_country_code )) AND ' ||
            '(( tb_tax_matrix.matrix_state_code = ''*ALL'' ) OR ( tb_tax_matrix.matrix_state_code = :v_transaction_state_code )) AND ' ||
            '( tb_tax_matrix.effective_date <= tb_transaction_detail.gl_date ) AND ( tb_tax_matrix.expiration_date >= tb_transaction_detail.gl_date ) AND ' ||
            '( tb_tax_matrix.then_taxcode_detail_id = thentaxcddtl.taxcode_detail_id (+)) AND ' ||
            '( tb_tax_matrix.else_taxcode_detail_id = elsetaxcddtl.taxcode_detail_id (+)) ';
   vc_tax_matrix_where             VARCHAR2(28000) := '';
   vc_tax_matrix_orderby           VARCHAR2(1000) :=
      'ORDER BY tb_tax_matrix.binary_weight DESC, tb_tax_matrix.significant_digits DESC, tb_tax_matrix.effective_date DESC';
   vc_tax_matrix_stmt              VARCHAR2 (31000);
   tax_matrix_cursor               cursor_type;

-- Tax Matrix Record TYPE
   TYPE tax_matrix_record is RECORD (
      tax_matrix_id                tb_tax_matrix.tax_matrix_id%TYPE,
      relation_sign                tb_tax_matrix.relation_sign%TYPE,
      relation_amount              tb_tax_matrix.relation_amount%TYPE,
      then_hold_code_flag          tb_tax_matrix.then_hold_code_flag%TYPE,
      else_hold_code_flag          tb_tax_matrix.else_hold_code_flag%TYPE,
      then_taxcode_detail_id       tb_transaction_detail.taxcode_detail_id%TYPE,
      else_taxcode_detail_id       tb_transaction_detail.taxcode_detail_id%TYPE,
      then_cch_taxcat_code         tb_tax_matrix.then_cch_taxcat_code%TYPE,
      then_cch_group_code          tb_tax_matrix.then_cch_group_code%TYPE,
      then_cch_item_code           tb_tax_matrix.then_cch_item_code%TYPE,
      else_cch_taxcat_code         tb_tax_matrix.else_cch_taxcat_code%TYPE,
      else_cch_group_code          tb_tax_matrix.else_cch_group_code%TYPE,
      else_cch_item_code           tb_tax_matrix.else_cch_item_code%TYPE,
      then_taxcode_country_code    tb_transaction_detail.taxcode_country_code%TYPE,
      then_taxcode_state_code      tb_transaction_detail.taxcode_state_code%TYPE,
      then_taxcode_type_code       tb_transaction_detail.taxcode_type_code%TYPE,
      then_taxcode_code            tb_transaction_detail.taxcode_code%TYPE,
      then_jurisdiction_id         tb_transaction_detail.jurisdiction_id%TYPE,
      then_measure_type_code       tb_transaction_detail.measure_type_code%TYPE,
      then_taxtype_code            tb_taxcode.taxtype_code%TYPE,
      else_taxcode_country_code    tb_transaction_detail.taxcode_country_code%TYPE,
      else_taxcode_state_code      tb_transaction_detail.taxcode_state_code%TYPE,
      else_taxcode_type_code       tb_transaction_detail.taxcode_type_code%TYPE,
      else_taxcode_code            tb_transaction_detail.taxcode_code%TYPE,
      else_jurisdiction_id         tb_transaction_detail.jurisdiction_id%TYPE,
      else_measure_type_code       tb_transaction_detail.measure_type_code%TYPE,
      else_taxtype_code            tb_taxcode.taxtype_code%TYPE );
   tax_matrix                      tax_matrix_record;

-- Table defined variables
   v_batch_status_code             tb_batch.batch_status_code%TYPE                   := 'P';
   v_starting_sequence             tb_batch.start_row%TYPE                           := 0;
   v_processed_rows                tb_batch.processed_rows%TYPE                      := 0;
   v_error_sev_code                tb_batch.error_sev_code%TYPE                      := ' ';
   v_severity_level                tb_list_code.severity_level%TYPE                  := ' ';
   v_write_import_line_flag        tb_list_code.write_import_line_flag%TYPE          := '1';
   v_abort_import_flag             tb_list_code.abort_import_flag%TYPE               := '0';
   v_location_abort_import_flag    tb_list_code.abort_import_flag%TYPE               := '0';
   v_tax_abort_import_flag         tb_list_code.abort_import_flag%TYPE               := '0';
   v_allocation_flag               tb_option.value%TYPE                              := NULL;
   v_delete_zero_amounts           tb_option.value%TYPE                              := NULL;
   v_hold_trans_flag               tb_option.value%TYPE                              := NULL;
   v_hold_trans_amount             tb_option.value%TYPE                              := NULL;
   v_autoproc_flag                 tb_option.value%TYPE                              := NULL;
   v_autoproc_amount               tb_option.value%TYPE                              := NULL;
   v_autoproc_id                   tb_option.value%TYPE                              := NULL;
   v_processuserexit_flag          tb_option.value%TYPE                              := NULL;
   v_autoproc_country_code         tb_taxcode_detail.taxcode_country_code%TYPE       := NULL;
   v_autoproc_state_code           tb_taxcode_detail.taxcode_state_code%TYPE         := NULL;
   v_autoproc_type_code            tb_taxcode_detail.taxcode_type_code%TYPE          := NULL;
   v_autoproc_code                 tb_taxcode_detail.taxcode_code%TYPE               := NULL;
   v_hold_code_flag                tb_tax_matrix.then_hold_code_flag%TYPE            := NULL;
   v_less_hold_code_flag           tb_tax_matrix.then_hold_code_flag%TYPE            := NULL;
   v_less_taxcode_detail_id        tb_tax_matrix.then_taxcode_detail_id%TYPE         := NULL;
   v_less_cch_taxcat_code          tb_tax_matrix.then_cch_taxcat_code%TYPE           := NULL;
   v_less_cch_group_code           tb_tax_matrix.then_cch_group_code%TYPE            := NULL;
   v_equal_hold_code_flag          tb_tax_matrix.then_hold_code_flag%TYPE            := NULL;
   v_equal_taxcode_detail_id       tb_tax_matrix.then_taxcode_detail_id%TYPE         := NULL;
   v_equal_cch_taxcat_code         tb_tax_matrix.then_cch_taxcat_code%TYPE           := NULL;
   v_equal_cch_group_code          tb_tax_matrix.then_cch_group_code%TYPE            := NULL;
   v_greater_hold_code_flag        tb_tax_matrix.then_hold_code_flag%TYPE            := NULL;
   v_greater_taxcode_detail_id     tb_tax_matrix.then_taxcode_detail_id%TYPE         := NULL;
   v_greater_cch_taxcat_code       tb_tax_matrix.then_cch_taxcat_code%TYPE           := NULL;
   v_greater_cch_group_code        tb_tax_matrix.then_cch_group_code%TYPE            := NULL;
   v_jurisdiction_id               tb_jurisdiction.jurisdiction_id%TYPE              := NULL;
   v_less_taxcode_country_code     tb_taxcode_detail.taxcode_country_code%TYPE       := NULL;
   v_less_taxcode_state_code       tb_taxcode_detail.taxcode_state_code%TYPE         := NULL;
   v_less_taxcode_type_code        tb_taxcode_detail.taxcode_type_code%TYPE          := NULL;
   v_less_taxcode_code             tb_taxcode_detail.taxcode_code%TYPE               := NULL;
   v_less_jurisdiction_id          tb_taxcode_detail.jurisdiction_id%TYPE            := NULL;
   v_less_measure_type_code        tb_taxcode_detail.measure_type_code%TYPE          := NULL;
   v_less_taxtype_code             tb_taxcode_detail.taxtype_code%TYPE               := NULL;
   v_equal_taxcode_country_code    tb_taxcode_detail.taxcode_country_code%TYPE       := NULL;
   v_equal_taxcode_state_code      tb_taxcode_detail.taxcode_state_code%TYPE         := NULL;
   v_equal_taxcode_type_code       tb_taxcode_detail.taxcode_type_code%TYPE          := NULL;
   v_equal_taxcode_code            tb_taxcode_detail.taxcode_code%TYPE               := NULL;
   v_equal_jurisdiction_id         tb_taxcode_detail.jurisdiction_id%TYPE            := NULL;
   v_equal_measure_type_code       tb_taxcode_detail.measure_type_code%TYPE          := NULL;
   v_equal_taxtype_code            tb_taxcode_detail.taxtype_code%TYPE               := NULL;
   v_greater_taxcode_country_code  tb_taxcode_detail.taxcode_country_code%TYPE       := NULL;
   v_greater_taxcode_state_code    tb_taxcode_detail.taxcode_state_code%TYPE         := NULL;
   v_greater_taxcode_type_code     tb_taxcode_detail.taxcode_type_code%TYPE          := NULL;
   v_greater_taxcode_code          tb_taxcode_detail.taxcode_code%TYPE               := NULL;
   v_greater_jurisdiction_id       tb_taxcode_detail.jurisdiction_id%TYPE            := NULL;
   v_greater_measure_type_code     tb_taxcode_detail.measure_type_code%TYPE          := NULL;
   v_greater_taxtype_code          tb_taxcode_detail.taxtype_code%TYPE               := NULL;
   v_gl_date                       tb_transaction_detail.gl_date%TYPE                  := NULL;
   v_transaction_country_code      tb_bcp_transactions.transaction_country_code%TYPE := NULL;
   v_transaction_state_code        tb_transaction_detail.transaction_state_code%TYPE   := NULL;
   v_override_jurisdiction_id      tb_transaction_detail.jurisdiction_id%TYPE          := NULL;
   v_sysdate                       tb_transaction_detail.load_timestamp%TYPE           := SYSDATE;
   v_transaction_amt               tb_transaction_detail.gl_line_itm_dist_amt%TYPE     := 0;
   v_kill_proc_flag                tb_option.value%TYPE                              := NULL;
   v_kill_proc_count               tb_option.value%TYPE                              := NULL;
   v_taxtype_code                  tb_taxcode.taxtype_code%TYPE                      := NULL;
   v_taxtype_used                  tb_taxcode.taxtype_code%TYPE                      := NULL;

-- Program defined variables
   v_rowid                         ROWID                                             := NULL;
   vc_error_code                   VARCHAR2(10)                                      := NULL;
   vi_cursor_id                    INTEGER                                           := 0;
   vi_rows_processed               INTEGER                                           := 0;
   vn_hold_trans_amount            NUMBER                                            := 0;
   vn_autoproc_amount              NUMBER                                            := 0;
   vn_autoproc_id                  NUMBER                                            := 0;
   vn_fetch_rows                   NUMBER                                            := 0;
   vn_country_rows                 NUMBER                                            := 0;
   vn_state_rows                   NUMBER                                            := 0;
   vi_error_count                  INTEGER                                           := 0;
   vc_driver_ref_ins_stmt          VARCHAR2(5000)                                    := NULL;
   vc_state_driver_flag            CHAR(1)                                           := '0';
   vn_kill_proc_count              NUMBER                                            := 0;
   vn_kill_proc_counter            NUMBER                                            := 0;
   vc_batch_status_code            VARCHAR2(10)                                      := NULL;
   vn_trans_created                NUMBER                                            := 0;
   vn_return                       NUMBER                                            := 0;
-- temporary
   vn_taxable_amt                  NUMBER                                            := 0;

-- Define Location Driver Names Cursor (tb_driver_names)
   CURSOR location_driver_cursor
   IS
        SELECT tb_driver_names.trans_dtl_column_name, tb_driver_names.matrix_column_name, tb_driver_names.null_driver_flag, tb_driver_names.wildcard_flag,
               datadefcol.desc_column_name, datadefcol.data_type
          FROM tb_driver_names,
               (SELECT *
                  FROM tb_data_def_column
                 WHERE table_name = 'TB_TRANSACTION_DETAIL') datadefcol
         WHERE tb_driver_names.driver_names_code = 'L' AND
               tb_driver_names.trans_dtl_column_name = datadefcol.column_name
      ORDER BY tb_driver_names.driver_id;
      location_driver              location_driver_cursor%ROWTYPE;

-- Define Tax Driver Names Cursor (tb_driver_names)
   CURSOR tax_driver_cursor
   IS
        SELECT tb_driver_names.trans_dtl_column_name, tb_driver_names.matrix_column_name, tb_driver_names.null_driver_flag, tb_driver_names.wildcard_flag, tb_driver_names.range_flag, tb_driver_names.to_number_flag,
               datadefcol.desc_column_name, datadefcol.data_type
          FROM tb_driver_names,
               (SELECT *
                  FROM tb_data_def_column
                 WHERE table_name = 'TB_TRANSACTION_DETAIL') datadefcol
         WHERE tb_driver_names.driver_names_code = 'T' AND
               tb_driver_names.trans_dtl_column_name = datadefcol.column_name
      ORDER BY tb_driver_names.driver_id;
      tax_driver                   tax_driver_cursor%ROWTYPE;

-- Define Transaction Detail Cursor (tb_transaction_detail)
   -- 3411 - add taxable amount and tax type used.
   CURSOR transaction_detail_cursor
   IS
      SELECT ROWID,
             transaction_detail_id,
             source_transaction_id,
             process_batch_no,
             gl_extract_batch_no,
             archive_batch_no,
             allocation_matrix_id,
             allocation_subtrans_id,
             entered_date,
             transaction_status,
             gl_date,
             gl_company_nbr,
             gl_company_name,
             gl_division_nbr,
             gl_division_name,
             gl_cc_nbr_dept_id,
             gl_cc_nbr_dept_name,
             gl_local_acct_nbr,
             gl_local_acct_name,
             gl_local_sub_acct_nbr,
             gl_local_sub_acct_name,
             gl_full_acct_nbr,
             gl_full_acct_name,
             gl_line_itm_dist_amt,
             orig_gl_line_itm_dist_amt,
             vendor_nbr,
             vendor_name,
             vendor_address_line_1,
             vendor_address_line_2,
             vendor_address_line_3,
             vendor_address_line_4,
             vendor_address_city,
             vendor_address_county,
             vendor_address_state,
             vendor_address_zip,
             vendor_address_country,
             vendor_type,
             vendor_type_name,
             invoice_nbr,
             invoice_desc,
             invoice_date,
             invoice_freight_amt,
             invoice_discount_amt,
             invoice_tax_amt,
             invoice_total_amt,
             invoice_tax_flg,
             invoice_line_nbr,
             invoice_line_name,
             invoice_line_type,
             invoice_line_type_name,
             invoice_line_amt,
             invoice_line_tax,
             afe_project_nbr,
             afe_project_name,
             afe_category_nbr,
             afe_category_name,
             afe_sub_cat_nbr,
             afe_sub_cat_name,
             afe_use,
             afe_contract_type,
             afe_contract_structure,
             afe_property_cat,
             inventory_nbr,
             inventory_name,
             inventory_class,
             inventory_class_name,
             po_nbr,
             po_name,
             po_date,
             po_line_nbr,
             po_line_name,
             po_line_type,
             po_line_type_name,
             ship_to_location,
             ship_to_location_name,
             ship_to_address_line_1,
             ship_to_address_line_2,
             ship_to_address_line_3,
             ship_to_address_line_4,
             ship_to_address_city,
             ship_to_address_county,
             ship_to_address_state,
             ship_to_address_zip,
             ship_to_address_country,
             wo_nbr,
             wo_name,
             wo_date,
             wo_type,
             wo_type_desc,
             wo_class,
             wo_class_desc,
             wo_entity,
             wo_entity_desc,
             wo_line_nbr,
             wo_line_name,
             wo_line_type,
             wo_line_type_desc,
             wo_shut_down_cd,
             wo_shut_down_cd_desc,
             voucher_id,
             voucher_name,
             voucher_date,
             voucher_line_nbr,
             voucher_line_desc,
             check_nbr,
             check_no,
             check_date,
             check_amt,
             check_desc,
             user_text_01,
             user_text_02,
             user_text_03,
             user_text_04,
             user_text_05,
             user_text_06,
             user_text_07,
             user_text_08,
             user_text_09,
             user_text_10,
             user_text_11,
             user_text_12,
             user_text_13,
             user_text_14,
             user_text_15,
             user_text_16,
             user_text_17,
             user_text_18,
             user_text_19,
             user_text_20,
             user_text_21,
             user_text_22,
             user_text_23,
             user_text_24,
             user_text_25,
             user_text_26,
             user_text_27,
             user_text_28,
             user_text_29,
             user_text_30,
             user_number_01,
             user_number_02,
             user_number_03,
             user_number_04,
             user_number_05,
             user_number_06,
             user_number_07,
             user_number_08,
             user_number_09,
             user_number_10,
             user_date_01,
             user_date_02,
             user_date_03,
             user_date_04,
             user_date_05,
             user_date_06,
             user_date_07,
             user_date_08,
             user_date_09,
             user_date_10,
             comments,
             tb_calc_tax_amt,
             state_use_amount,
             state_use_tier2_amount,
             state_use_tier3_amount,
             county_use_amount,
             county_local_use_amount,
             city_use_amount,
             city_local_use_amount,
             transaction_state_code,
             auto_transaction_state_code,
             transaction_ind,
             suspend_ind,
             taxcode_detail_id,
             taxcode_state_code,
             taxcode_type_code,
             taxcode_code,
             cch_taxcat_code,
             cch_group_code,
             cch_item_code,
             manual_taxcode_ind,
             tax_matrix_id,
             location_matrix_id,
             jurisdiction_id,
             jurisdiction_taxrate_id,
             manual_jurisdiction_ind,
             measure_type_code,
             state_use_rate,
             state_use_tier2_rate,
             state_use_tier3_rate,
             state_split_amount,
             state_tier2_min_amount,
             state_tier2_max_amount,
             state_maxtax_amount,
             county_use_rate,
             county_local_use_rate,
             county_split_amount,
             county_maxtax_amount,
             county_single_flag,
             county_default_flag,
             city_use_rate,
             city_local_use_rate,
             city_split_amount,
             city_split_use_rate,
             city_single_flag,
             city_default_flag,
             combined_use_rate,
             load_timestamp,
             gl_extract_updater,
             gl_extract_timestamp,
             gl_extract_flag,
             gl_log_flag,
             gl_extract_amt,
             audit_flag,
             audit_user_id,
             audit_timestamp,
             modify_user_id,
             modify_timestamp,
             update_user_id,
             update_timestamp,
             country_use_amount,
             transaction_country_code,
             auto_transaction_country_code,
             taxcode_country_code,
             country_use_rate,
             split_subtrans_id,
             multi_trans_code,
             tax_alloc_matrix_id
        FROM tb_transaction_detail
       WHERE transaction_ind IS NULL;
      transaction_detail             transaction_detail_cursor%ROWTYPE;

-- Define Exceptions
   e_badread                       exception;
   e_badupdate                     exception;
   e_badwrite                      exception;
   e_wrongdata                     exception;
   e_abort                         exception;
   e_kill                          exception;

-- ****** Program starts ****** --------------------------------------------------------------------
BEGIN
   ac_error_code := 'OK!';
insert into tb_debug(row_joe) values('entering tb_transaction_process');
   -- RUN Jurisdiction Allocations?
   -- Determine Allocation Flag
   BEGIN
      SELECT LTRIM(RTRIM(value))
        INTO v_allocation_flag
        FROM tb_option
       WHERE option_code = 'ALLOCATIONSENABLED' AND
             option_type_code = 'ADMIN' AND
             user_code = 'ADMIN';
   EXCEPTION
      WHEN OTHERS THEN
         v_allocation_flag := '0';
   END;
   IF v_allocation_flag IS NULL THEN
      v_allocation_flag := '0';
   END IF;
   -- Perform Allocations
   IF v_allocation_flag = '1' THEN
      sp_transaction_juris_alloc(vc_error_code);
      IF vc_error_code IS NOT NULL THEN
         ac_error_code := vc_error_code;
         RAISE e_abort;
      END IF;
   END IF;

   -- Get starting sequence no.
   BEGIN
      SELECT last_number
        INTO v_starting_sequence
        FROM user_sequences
       WHERE sequence_name = 'SQ_TB_TRANSACTION_DETAIL_ID';
   EXCEPTION
      WHEN OTHERS THEN
         NULL;
   END;

   -- Determine Global Delete Zero Amounts Flag
   BEGIN
      SELECT LTRIM(RTRIM(value))
        INTO v_delete_zero_amounts
        FROM tb_option
       WHERE option_code = 'DELETE0AMTFLAG' AND
             option_type_code = 'SYSTEM' AND
             user_code = 'SYSTEM';
   EXCEPTION
      WHEN OTHERS THEN
         v_delete_zero_amounts := '0';
   END;
   IF v_delete_zero_amounts IS NULL THEN
      v_delete_zero_amounts := '0';
   END IF;

   -- Determine Global Hold Transactions Flag and Amount
   BEGIN
      SELECT LTRIM(RTRIM(value))
        INTO v_hold_trans_flag
        FROM tb_option
       WHERE option_code = 'HOLDTRANSFLAG' AND
             option_type_code = 'SYSTEM' AND
             user_code = 'SYSTEM';
   EXCEPTION
      WHEN OTHERS THEN
         v_hold_trans_flag := '0';
   END;
   IF v_hold_trans_flag IS NULL THEN
      v_hold_trans_flag := '0';
   END IF;
   IF v_hold_trans_flag = '1' THEN
      BEGIN
         SELECT LTRIM(RTRIM(value))
           INTO v_hold_trans_amount
           FROM tb_option
          WHERE option_code = 'HOLDTRANSAMT' AND
                option_type_code = 'SYSTEM' AND
                user_code = 'SYSTEM';
      EXCEPTION
         WHEN OTHERS THEN
            v_hold_trans_amount := '0';
      END;
      IF v_hold_trans_amount IS NULL THEN
         v_hold_trans_amount := '0';
      END IF;
      vn_hold_trans_amount := TO_NUMBER(v_hold_trans_amount);
   END IF;

   -- Determine "auto-process" Flag, Amount, and TaxCode Detail ID
   BEGIN
      SELECT LTRIM(RTRIM(value))
        INTO v_autoproc_flag
        FROM tb_option
       WHERE option_code = 'AUTOPROCFLAG' AND
             option_type_code = 'SYSTEM' AND
             user_code = 'SYSTEM';
   EXCEPTION
      WHEN OTHERS THEN
         v_hold_trans_flag := '0';
   END;
   IF v_autoproc_flag IS NULL THEN
      v_autoproc_flag := '0';
   END IF;

   IF v_autoproc_flag = '1' THEN
      BEGIN
         SELECT LTRIM(RTRIM(value))
           INTO v_autoproc_amount
           FROM tb_option
          WHERE option_code = 'AUTOPROCAMT' AND
                option_type_code = 'SYSTEM' AND
                user_code = 'SYSTEM';
      EXCEPTION
         WHEN OTHERS THEN
            v_autoproc_amount := '0';
      END;
      IF v_autoproc_amount IS NULL THEN
         v_autoproc_amount := '0';
      END IF;
      vn_autoproc_amount := TO_NUMBER(v_autoproc_amount);

      BEGIN
         SELECT LTRIM(RTRIM(value))
           INTO v_autoproc_id
           FROM tb_option
          WHERE option_code = 'AUTOPROCID' AND
                option_type_code = 'SYSTEM' AND
                user_code = 'SYSTEM';
      EXCEPTION
         WHEN OTHERS THEN
            v_autoproc_id := '0';
      END;
      IF v_autoproc_id IS NULL THEN
         v_autoproc_id := '0';
      END IF;
      vn_autoproc_id := TO_NUMBER(v_autoproc_id);

      BEGIN
         SELECT taxcode_country_code, taxcode_state_code, taxcode_type_code, taxcode_code
           INTO v_autoproc_country_code, v_autoproc_state_code, v_autoproc_type_code, v_autoproc_code
           FROM tb_taxcode_detail
          WHERE taxcode_detail_id = vn_autoproc_id;
      EXCEPTION
         WHEN OTHERS THEN
            v_autoproc_flag := '0';
      END;
      IF v_autoproc_country_code IS NULL OR v_autoproc_state_code IS NULL OR v_autoproc_type_code IS NULL OR v_autoproc_code IS NULL THEN
         v_autoproc_flag := '0';
      END IF;
   END IF;

   -- ***** Create Dynamic WHERE Clause for Matrix SQLs ***** --
   -- Location Matrix Drivers
   OPEN location_driver_cursor;
   LOOP
      FETCH location_driver_cursor INTO location_driver;
      EXIT WHEN location_driver_cursor%NOTFOUND;
      IF location_driver.null_driver_flag = '1' THEN
         IF location_driver.wildcard_flag = '1' THEN
            IF UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'CHAR' OR
               UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'VARC' THEN
                  vc_location_matrix_where := vc_location_matrix_where || 'AND ' ||
                  '((tb_transaction_detail.' || location_driver.trans_dtl_column_name || ' IS NULL AND (tb_location_matrix.' || location_driver.matrix_column_name || ' = ''*ALL'' OR tb_location_matrix.' || location_driver.matrix_column_name || ' = ''*NULL'' )) OR ' ||
                  '(tb_transaction_detail.' || location_driver.trans_dtl_column_name || ' IS NOT NULL AND (tb_location_matrix.' || location_driver.matrix_column_name || ' = ''*ALL'' OR tb_transaction_detail.' || location_driver.trans_dtl_column_name || ' LIKE tb_location_matrix.' || location_driver.matrix_column_name || '))) ';
            END IF;
         ELSE
            IF UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'CHAR' OR
               UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'VARC' THEN
                  vc_location_matrix_where := vc_location_matrix_where || 'AND ' ||
                  '((tb_transaction_detail.' || location_driver.trans_dtl_column_name || ' IS NULL AND (tb_location_matrix.' || location_driver.matrix_column_name || ' = ''*ALL'' OR tb_location_matrix.' || location_driver.matrix_column_name || ' = ''*NULL'' )) OR ' ||
                  '(tb_transaction_detail.' || location_driver.trans_dtl_column_name || ' IS NOT NULL AND (tb_location_matrix.' || location_driver.matrix_column_name || ' = ''*ALL'' OR tb_transaction_detail.' || location_driver.trans_dtl_column_name || ' = tb_location_matrix.' || location_driver.matrix_column_name || '))) ';
            END IF;
         END IF;
      ELSE
         IF location_driver.wildcard_flag = '1' THEN
            IF UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'CHAR' OR
               UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'VARC' THEN
                  vc_location_matrix_where := vc_location_matrix_where || 'AND ' ||
                  '(tb_location_matrix.' || location_driver.matrix_column_name || ' = ''*ALL'' OR tb_transaction_detail.' || location_driver.trans_dtl_column_name || ' LIKE tb_location_matrix.' || location_driver.matrix_column_name || ') ';
            END IF;
         ELSE
            IF UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'CHAR' OR
               UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'VARC' THEN
                  vc_location_matrix_where := vc_location_matrix_where || 'AND ' ||
                  '(tb_location_matrix.' || location_driver.matrix_column_name || ' = ''*ALL'' OR tb_transaction_detail.' || location_driver.trans_dtl_column_name || ' = tb_location_matrix.' || location_driver.matrix_column_name || ') ';
            END IF;
         END IF;
      END IF;
   END LOOP;
   CLOSE location_driver_cursor;
   -- If no drivers found raise error, else create location matrix sql statement
   IF vc_location_matrix_where IS NULL THEN
      ac_error_code := 'P2';
      RAISE e_abort;
   ELSE
      vc_location_matrix_stmt := vc_location_matrix_select || vc_location_matrix_where || vc_location_matrix_orderby;
   END IF;

   -- Tax Matrix Drivers
   OPEN tax_driver_cursor;
   LOOP
      FETCH tax_driver_cursor INTO tax_driver;
      EXIT WHEN tax_driver_cursor%NOTFOUND;
      IF tax_driver.range_flag = '1' THEN
         IF tax_driver.to_number_flag = '1' THEN
            IF tax_driver.null_driver_flag = '1' THEN
               -- Range and ToNumber and NULL
               IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                  UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                  vc_tax_matrix_where := vc_tax_matrix_where || 'AND ( ' ||
                  '( UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU) = ''*NA'' AND ' ||
                     '( tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NULL AND ( UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') = ''*ALL'' OR UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') = ''*NULL'' )) OR ' ||
                     '( tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND ( UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') = ''*ALL'' OR DECODE(isnumber(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || '),1,to_char(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ',''00000000000000000000.00000000000000000000''),UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ')) = DECODE(isnumber(UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ')),1,to_char(UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '),''00000000000000000000.00000000000000000000''),UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') ))) OR ' ||
                  '( UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU) <> ''*NA'' AND ' ||
                     '( tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND (DECODE(isnumber(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || '),1,to_char(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ',''00000000000000000000.00000000000000000000''),UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ')) >= DECODE(isnumber(UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ')),1,to_char(UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '),''00000000000000000000.00000000000000000000''),UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') ) AND ' ||
                                                                                                       'DECODE(isnumber(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || '),1,to_char(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ',''00000000000000000000.00000000000000000000''),UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ')) <= DECODE(isnumber(UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU)),1,to_char(UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU),''00000000000000000000.00000000000000000000''),UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU) ))))) ';
               END IF;
            ELSE
               -- Range and ToNumber and NOT NULL
               IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                  UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                  vc_tax_matrix_where := vc_tax_matrix_where || 'AND ( ' ||
                  '( UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU) = ''*NA'' AND (UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') = ''*ALL'' OR DECODE(isnumber(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || '),1,to_char(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ',''00000000000000000000.00000000000000000000''),UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ')) = DECODE(isnumber(UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ')),1,to_char(UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '),''00000000000000000000.00000000000000000000''),UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ')))) OR ' ||
                  '( UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU) <> ''*NA'' AND (DECODE(isnumber(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || '),1,to_char(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ',''00000000000000000000.00000000000000000000''),UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ')) >= DECODE(isnumber(UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ')),1,to_char(UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '),''00000000000000000000.00000000000000000000''),UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ')) AND ' ||
                                                                                        'DECODE(isnumber(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || '),1,to_char(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ',''00000000000000000000.00000000000000000000''),UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ')) <= DECODE(isnumber(UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU )),1,to_char(UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU),''00000000000000000000.00000000000000000000''),UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU) ))) ';
               END IF;
            END IF;
         ELSE
            IF tax_driver.null_driver_flag = '1' THEN
               IF tax_driver.wildcard_flag = '1' THEN
                  -- Range and NOT ToNumber and NULL and Wildcard
                  IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                     UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                     vc_tax_matrix_where := vc_tax_matrix_where || 'AND ( ' ||
                     '( UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU) = ''*NA'' AND ' ||
                        '( tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NULL AND (UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') = ''*ALL'' OR UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') = ''*NULL'' )) OR ' ||
                        '( tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND (UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') = ''*ALL'' OR UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') LIKE UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ')))) OR ' ||
                     '( UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU) <> ''*NA'' AND (tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND (UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') >= UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') AND UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') <= UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU) ))) ';
                  END IF;
               ELSE
                  -- Range and NOT ToNumber and NULL and NOT Wildcard
                  IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                     UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                     vc_tax_matrix_where := vc_tax_matrix_where || 'AND ( ' ||
                     '( UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU) = ''*NA'' AND ' ||
                        '( tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NULL AND (UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') = ''*ALL'' OR UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') = ''*NULL'' )) OR ' ||
                        '( tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND (UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') = ''*ALL'' OR UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') = UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ')))) OR ' ||
                     '( UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU) <> ''*NA'' AND (tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND (UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') >= UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') AND UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') <= UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU) ))) ';
                  END IF;
               END IF;
            ELSE
               IF tax_driver.wildcard_flag = '1' THEN
                  -- Range and NOT ToNumber and NOT NULL and Wildcard
                  IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                     UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                     vc_tax_matrix_where := vc_tax_matrix_where || 'AND ( ' ||
                     '( UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU) = ''*NA'' AND ' ||
                        '(UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') = ''*ALL'' OR UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') LIKE UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '))) OR ' ||
                     '( UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU) <> ''*NA'' AND (UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') >= UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') AND UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') <= UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU) )) ';
                  END IF;
               ELSE
                  -- Range and NOT ToNumber and NOT NULL and NOT Wildcard
                  IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                     UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                     vc_tax_matrix_where := vc_tax_matrix_where || 'AND ( ' ||
                     '( UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU) = ''*NA'' AND ' ||
                        '(UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') = ''*ALL'' OR UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') = UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '))) OR ' ||
                     '( UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU) <> ''*NA'' AND (UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') >= UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') AND UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') <= UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU) )) ';
                  END IF;
               END IF;
            END IF;
         END IF;
      ELSE
         IF tax_driver.null_driver_flag = '1' THEN
            IF tax_driver.wildcard_flag = '1' THEN
               -- NOT Range and NULL and Wildcard
               IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                  UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                     vc_tax_matrix_where := vc_tax_matrix_where || 'AND ( ' ||
                     '(( tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NULL AND (UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') = ''*ALL'' OR UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') = ''*NULL'' )) OR ' ||
                     '( tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND (UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') = ''*ALL'' OR UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') LIKE UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ')))) ';
               END IF;
            ELSE
               -- NOT Range and NULL and NOT Wildcard
               IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                  UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                     vc_tax_matrix_where := vc_tax_matrix_where || 'AND ( ' ||
                     '(( tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NULL AND (UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') = ''*ALL'' OR UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') = ''*NULL'' )) OR ' ||
                     '( tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND (UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') = ''*ALL'' OR UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') = UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ')))) ';
               END IF;
            END IF;
         ELSE
            IF tax_driver.wildcard_flag = '1' THEN
               -- NOT Range and NOT NULL and Wildcard
               IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                  UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                     vc_tax_matrix_where := vc_tax_matrix_where || 'AND ( ' ||
                     '( UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') = ''*ALL'' OR UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') LIKE UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ')) ';
               END IF;
            ELSE
               -- NOT Range and NOT NULL and NOT Wildcard
               IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                  UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                  IF tax_driver.trans_dtl_column_name = 'TRANSACTION_STATE_CODE' THEN
                     IF vc_state_driver_flag <> '1' THEN
                        vc_state_driver_flag := '1';
                     END IF;
                     vc_tax_matrix_where := vc_tax_matrix_where || 'AND ( ' ||
                     '( UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') = ''*ALL'' OR UPPER(:v_transaction_state_code) = UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ')) ';
                  ELSE
                     vc_tax_matrix_where := vc_tax_matrix_where || 'AND ( ' ||
                     '( UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') = ''*ALL'' OR UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') = UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ')) ';
                  END IF;
               END IF;
            END IF;
         END IF;
      END IF;
      vc_tax_matrix_where := vc_tax_matrix_where || ') ';
   END LOOP;
   CLOSE tax_driver_cursor;
   -- If no drivers found raise error, else create location matrix sql statement
   IF vc_tax_matrix_where IS NULL THEN
      ac_error_code := 'P3';
      RAISE e_abort;
   ELSE
      vc_tax_matrix_stmt := vc_tax_matrix_select || vc_tax_matrix_where || vc_tax_matrix_orderby;
   END IF;

   -- Set large rollback segment
   COMMIT;

   -- ****** Read and Process Transactions ****** -----------------------------------------
   OPEN transaction_detail_cursor;
   LOOP
      FETCH transaction_detail_cursor INTO transaction_detail;
      EXIT WHEN transaction_detail_cursor%NOTFOUND;
      v_rowid := transaction_detail.rowid;
      v_write_import_line_flag := '1';

      -- Skip zero amounts?
      IF (v_delete_zero_amounts = '1' AND transaction_detail.gl_line_itm_dist_amt <> 0) OR
         (v_delete_zero_amounts <> '1') THEN

         IF transaction_detail.multi_trans_code LIKE 'O%' THEN
            transaction_detail.transaction_ind := 'O';
            transaction_detail.suspend_ind := '';
         ELSIF transaction_detail.multi_trans_code = 'FS' THEN
            transaction_detail.transaction_ind := 'H';
            transaction_detail.suspend_ind :='';
         ELSE
            v_processed_rows := v_processed_rows + 1;
            v_transaction_amt := v_transaction_amt + transaction_detail.gl_line_itm_dist_amt;

            -- Perform some error checking and manually set the default value
            IF transaction_detail.gl_company_nbr IS NULL THEN
               transaction_detail.gl_company_nbr := ' ';
            END IF;
            IF transaction_detail.gl_date IS NULL THEN
               transaction_detail.gl_date := v_sysdate;
            END IF;

            -- Populate and/or Clear working fields
            transaction_detail.transaction_ind := NULL;
            transaction_detail.suspend_ind := NULL;
            transaction_detail.taxcode_detail_id := NULL;
            transaction_detail.taxcode_country_code := NULL;
            transaction_detail.taxcode_state_code := NULL;
            transaction_detail.taxcode_type_code := NULL;
            transaction_detail.taxcode_code := NULL;
            transaction_detail.tax_matrix_id := NULL;
            transaction_detail.location_matrix_id := NULL;
-- Do not clear! Allocations may have put this in. ---> transaction_detail.jurisdiction_id := NULL;
            transaction_detail.jurisdiction_taxrate_id := NULL;
            transaction_detail.measure_type_code := NULL;
            transaction_detail.gl_extract_updater := NULL;
            transaction_detail.gl_extract_timestamp := NULL;
            transaction_detail.gl_extract_flag := NULL;
            transaction_detail.gl_log_flag := NULL;
            transaction_detail.gl_extract_amt := NULL;

            -- Does Transaction Record have a valid Country                                  -- MBF01
            vn_country_rows := 0;
            IF transaction_detail.transaction_country_code IS NULL THEN
               vn_country_rows := 0;
            ELSE
               SELECT count(*)
                 INTO vn_country_rows
                 FROM tb_taxcode_state
                WHERE country = transaction_detail.transaction_country_code;
            END IF;
            -- Does Transaction Record have a valid Country/State?                        -- MBF01
            vn_state_rows := 0;
            IF transaction_detail.transaction_state_code IS NULL OR transaction_detail.transaction_state_code IS NULL THEN
               vn_state_rows := 0;
            ELSE
               SELECT count(*)
                 INTO vn_state_rows
                 FROM tb_taxcode_state
                WHERE country = transaction_detail.transaction_country_code
                  AND taxcode_state_code = transaction_detail.transaction_state_code;
            END IF;
            IF vn_state_rows = 0 THEN
               -- Search Location Matrix for matches
               BEGIN
                   OPEN location_matrix_cursor
                    FOR vc_location_matrix_stmt
                  USING v_rowid;
                  FETCH location_matrix_cursor
                   INTO location_matrix;
                  -- Rows found?
                  IF location_matrix_cursor%FOUND THEN
                     vn_fetch_rows := location_matrix_cursor%ROWCOUNT;
                  ELSE
                     vn_fetch_rows := 0;
                  END IF;
                  CLOSE location_matrix_cursor;
                  IF SQLCODE != 0 THEN
                     RAISE e_badread;
                  END IF;
               EXCEPTION
                  WHEN e_badread THEN
                     ac_error_code := 'P4';
                     RAISE e_abort;
               END;
               -- Location Matrix Line Found
               IF vn_fetch_rows > 0 THEN
                  IF transaction_detail.transaction_country_code IS NULL THEN
                     transaction_detail.auto_transaction_country_code := '*NULL';
                  ELSE
                     transaction_detail.auto_transaction_country_code := transaction_detail.transaction_country_code;
                  END IF;
                  IF transaction_detail.transaction_state_code IS NULL THEN
                     transaction_detail.auto_transaction_state_code := '*NULL';
                  ELSE
                     transaction_detail.auto_transaction_state_code := transaction_detail.transaction_state_code;
                  END IF;
                  transaction_detail.transaction_country_code := location_matrix.country;
                  transaction_detail.transaction_state_code := location_matrix.state;
                  transaction_detail.location_matrix_id := location_matrix.location_matrix_id;
                  transaction_detail.jurisdiction_id := location_matrix.jurisdiction_id;
               ELSE
                  -- added--> the following stuff:         added country -- MBF01
                  IF vn_country_rows = 0 THEN
                     transaction_detail.transaction_ind := 'S';
                     transaction_detail.suspend_ind := 'L';
                  ELSE
                     IF transaction_detail.transaction_state_code IS NULL THEN
                        transaction_detail.auto_transaction_state_code := '*NULL';
                     ELSE
                        transaction_detail.auto_transaction_state_code := transaction_detail.transaction_state_code;
                     END IF;
                     transaction_detail.transaction_state_code := '*DEF';
                     -- removed--> transaction_detail.transaction_ind := 'S';
                     -- removed--> transaction_detail.suspend_ind := 'L';
                  END IF;
               END IF;
            END IF;

            -- Continue if not Suspended
            IF transaction_detail.transaction_ind IS NULL OR transaction_detail.transaction_ind <> 'S' THEN
               v_transaction_country_code := transaction_detail.transaction_country_code;
               v_transaction_state_code := transaction_detail.transaction_state_code;
               -- Search Tax Matrix for matches
               BEGIN
                  IF vc_state_driver_flag = '1' THEN
                      OPEN tax_matrix_cursor
                       FOR vc_tax_matrix_stmt
                     USING v_rowid, v_transaction_country_code, v_transaction_state_code, v_transaction_state_code;
                     FETCH tax_matrix_cursor
                      INTO tax_matrix;
                  ELSE
                      OPEN tax_matrix_cursor
                       FOR vc_tax_matrix_stmt
                     USING v_rowid, v_transaction_country_code, v_transaction_state_code;
                     FETCH tax_matrix_cursor
                      INTO tax_matrix;
                  END IF;
                  -- Rows found?
                  IF tax_matrix_cursor%FOUND THEN
                     vn_fetch_rows := tax_matrix_cursor%ROWCOUNT;
                  ELSE
                     vn_fetch_rows := 0;
                  END IF;
                  CLOSE tax_matrix_cursor;
                  IF SQLCODE != 0 THEN
                     RAISE e_badread;
                  END IF;
               EXCEPTION
                  WHEN e_badread THEN
                     ac_error_code := 'P5';
                     RAISE e_abort;
               END;

               -- Tax Matrix Line Found
               IF vn_fetch_rows > 0 THEN
                  transaction_detail.tax_matrix_id := tax_matrix.tax_matrix_id;
                  -- Determine "Then" or "Else" Results
                  IF tax_matrix.relation_sign = 'na' THEN
                     tax_matrix.relation_amount := 0;
                     v_less_hold_code_flag := tax_matrix.then_hold_code_flag;
                     v_less_taxcode_detail_id := tax_matrix.then_taxcode_detail_id;
                     v_less_taxcode_country_code := tax_matrix.then_taxcode_country_code;
                     v_less_taxcode_state_code := tax_matrix.then_taxcode_state_code;
                     v_less_taxcode_type_code := tax_matrix.then_taxcode_type_code;
                     v_less_taxcode_code := tax_matrix.then_taxcode_code;
                     v_less_jurisdiction_id := tax_matrix.then_jurisdiction_id;
                     v_less_measure_type_code := tax_matrix.then_measure_type_code;
                     v_less_taxtype_code := tax_matrix.then_taxtype_code;
                     v_less_cch_taxcat_code := tax_matrix.then_cch_taxcat_code;
                     v_less_cch_group_code := tax_matrix.then_cch_group_code;
                     -- v_less_cch_item_code := tax_matrix.then_cch_item_code;
                     v_equal_hold_code_flag := tax_matrix.then_hold_code_flag;
                     v_equal_taxcode_detail_id := tax_matrix.then_taxcode_detail_id;
                     v_equal_taxcode_country_code := tax_matrix.then_taxcode_country_code;
                     v_equal_taxcode_state_code := tax_matrix.then_taxcode_state_code;
                     v_equal_taxcode_type_code := tax_matrix.then_taxcode_type_code;
                     v_equal_taxcode_code := tax_matrix.then_taxcode_code;
                     v_equal_jurisdiction_id := tax_matrix.then_jurisdiction_id;
                     v_equal_measure_type_code := tax_matrix.then_measure_type_code;
                     v_equal_taxtype_code := tax_matrix.then_taxtype_code;
                     v_equal_cch_taxcat_code := tax_matrix.then_cch_taxcat_code;
                     v_equal_cch_group_code := tax_matrix.then_cch_group_code;
                     -- v_equal_cch_item_code := tax_matrix.then_cch_item_code;
                     v_greater_hold_code_flag := tax_matrix.then_hold_code_flag;
                     v_greater_taxcode_detail_id := tax_matrix.then_taxcode_detail_id;
                     v_greater_taxcode_country_code := tax_matrix.then_taxcode_country_code;
                     v_greater_taxcode_state_code := tax_matrix.then_taxcode_state_code;
                     v_greater_taxcode_type_code := tax_matrix.then_taxcode_type_code;
                     v_greater_taxcode_code := tax_matrix.then_taxcode_code;
                     v_greater_jurisdiction_id := tax_matrix.then_jurisdiction_id;
                     v_greater_measure_type_code := tax_matrix.then_measure_type_code;
                     v_greater_taxtype_code := tax_matrix.then_taxtype_code;
                     v_greater_cch_taxcat_code := tax_matrix.then_cch_taxcat_code;
                     v_greater_cch_group_code := tax_matrix.then_cch_group_code;
                     -- v_greater_cch_item_code := tax_matrix.then_cch_item_code;
                  ELSIF tax_matrix.relation_sign = '=' THEN
                     v_less_hold_code_flag := tax_matrix.else_hold_code_flag;
                     v_less_taxcode_detail_id := tax_matrix.else_taxcode_detail_id;
                     v_less_taxcode_country_code := tax_matrix.else_taxcode_country_code;
                     v_less_taxcode_state_code := tax_matrix.else_taxcode_state_code;
                     v_less_taxcode_type_code := tax_matrix.else_taxcode_type_code;
                     v_less_taxcode_code := tax_matrix.else_taxcode_code;
                     v_less_jurisdiction_id := tax_matrix.else_jurisdiction_id;
                     v_less_measure_type_code := tax_matrix.else_measure_type_code;
                     v_less_taxtype_code := tax_matrix.else_taxtype_code;
                     v_less_cch_taxcat_code := tax_matrix.else_cch_taxcat_code;
                     v_less_cch_group_code := tax_matrix.else_cch_group_code;
                     -- v_less_cch_item_code := tax_matrix.else_cch_item_code;
                     v_equal_hold_code_flag := tax_matrix.then_hold_code_flag;
                     v_equal_taxcode_detail_id := tax_matrix.then_taxcode_detail_id;
                     v_equal_taxcode_country_code := tax_matrix.then_taxcode_country_code;
                     v_equal_taxcode_state_code := tax_matrix.then_taxcode_state_code;
                     v_equal_taxcode_type_code := tax_matrix.then_taxcode_type_code;
                     v_equal_taxcode_code := tax_matrix.then_taxcode_code;
                     v_equal_jurisdiction_id := tax_matrix.then_jurisdiction_id;
                     v_equal_measure_type_code := tax_matrix.then_measure_type_code;
                     v_equal_taxtype_code := tax_matrix.then_taxtype_code;
                     v_equal_cch_taxcat_code := tax_matrix.then_cch_taxcat_code;
                     v_equal_cch_group_code := tax_matrix.then_cch_group_code;
                     -- v_equal_cch_item_code := tax_matrix.then_cch_item_code;
                     v_greater_hold_code_flag := tax_matrix.else_hold_code_flag;
                     v_greater_taxcode_detail_id := tax_matrix.else_taxcode_detail_id;
                     v_greater_taxcode_country_code := tax_matrix.else_taxcode_country_code;
                     v_greater_taxcode_state_code := tax_matrix.else_taxcode_state_code;
                     v_greater_taxcode_type_code := tax_matrix.else_taxcode_type_code;
                     v_greater_taxcode_code := tax_matrix.else_taxcode_code;
                     v_greater_jurisdiction_id := tax_matrix.else_jurisdiction_id;
                     v_greater_measure_type_code := tax_matrix.else_measure_type_code;
                     v_greater_taxtype_code := tax_matrix.else_taxtype_code;
                     v_greater_cch_taxcat_code := tax_matrix.else_cch_taxcat_code;
                     v_greater_cch_group_code := tax_matrix.else_cch_group_code;
                     -- v_greater_cch_item_code := tax_matrix.else_cch_item_code;
                  ELSIF tax_matrix.relation_sign = '<' THEN
                     v_less_hold_code_flag := tax_matrix.then_hold_code_flag;
                     v_less_taxcode_detail_id := tax_matrix.then_taxcode_detail_id;
                     v_less_taxcode_country_code := tax_matrix.then_taxcode_country_code;
                     v_less_taxcode_state_code := tax_matrix.then_taxcode_state_code;
                     v_less_taxcode_type_code := tax_matrix.then_taxcode_type_code;
                     v_less_taxcode_code := tax_matrix.then_taxcode_code;
                     v_less_jurisdiction_id := tax_matrix.then_jurisdiction_id;
                     v_less_measure_type_code := tax_matrix.then_measure_type_code;
                     v_less_taxtype_code := tax_matrix.then_taxtype_code;
                     v_less_cch_taxcat_code := tax_matrix.then_cch_taxcat_code;
                     v_less_cch_group_code := tax_matrix.then_cch_group_code;
                     -- v_less_cch_item_code := tax_matrix.then_cch_item_code;
                     v_equal_hold_code_flag := tax_matrix.else_hold_code_flag;
                     v_equal_taxcode_detail_id := tax_matrix.else_taxcode_detail_id;
                     v_equal_taxcode_country_code := tax_matrix.else_taxcode_country_code;
                     v_equal_taxcode_state_code := tax_matrix.else_taxcode_state_code;
                     v_equal_taxcode_type_code := tax_matrix.else_taxcode_type_code;
                     v_equal_taxcode_code := tax_matrix.else_taxcode_code;
                     v_equal_jurisdiction_id := tax_matrix.else_jurisdiction_id;
                     v_equal_measure_type_code := tax_matrix.else_measure_type_code;
                     v_equal_taxtype_code := tax_matrix.else_taxtype_code;
                     v_equal_cch_taxcat_code := tax_matrix.else_cch_taxcat_code;
                     v_equal_cch_group_code := tax_matrix.else_cch_group_code;
                     -- v_equal_cch_item_code := tax_matrix.else_cch_item_code;
                     v_greater_hold_code_flag := tax_matrix.else_hold_code_flag;
                     v_greater_taxcode_detail_id := tax_matrix.else_taxcode_detail_id;
                     v_greater_taxcode_country_code := tax_matrix.else_taxcode_country_code;
                     v_greater_taxcode_state_code := tax_matrix.else_taxcode_state_code;
                     v_greater_taxcode_type_code := tax_matrix.else_taxcode_type_code;
                     v_greater_taxcode_code := tax_matrix.else_taxcode_code;
                     v_greater_jurisdiction_id := tax_matrix.else_jurisdiction_id;
                     v_greater_measure_type_code := tax_matrix.else_measure_type_code;
                     v_greater_taxtype_code := tax_matrix.else_taxtype_code;
                     v_greater_cch_taxcat_code := tax_matrix.else_cch_taxcat_code;
                     v_greater_cch_group_code := tax_matrix.else_cch_group_code;
                     -- v_greater_cch_item_code := tax_matrix.else_cch_item_code;
                  ELSIF tax_matrix.relation_sign = '<=' THEN
                     v_less_hold_code_flag := tax_matrix.then_hold_code_flag;
                     v_less_taxcode_detail_id := tax_matrix.then_taxcode_detail_id;
                     v_less_taxcode_country_code := tax_matrix.then_taxcode_country_code;
                     v_less_taxcode_state_code := tax_matrix.then_taxcode_state_code;
                     v_less_taxcode_type_code := tax_matrix.then_taxcode_type_code;
                     v_less_taxcode_code := tax_matrix.then_taxcode_code;
                     v_less_jurisdiction_id := tax_matrix.then_jurisdiction_id;
                     v_less_measure_type_code := tax_matrix.then_measure_type_code;
                     v_less_taxtype_code := tax_matrix.then_taxtype_code;
                     v_less_cch_taxcat_code := tax_matrix.then_cch_taxcat_code;
                     v_less_cch_group_code := tax_matrix.then_cch_group_code;
                     -- v_less_cch_item_code := tax_matrix.then_cch_item_code;
                     v_equal_hold_code_flag := tax_matrix.then_hold_code_flag;
                     v_equal_taxcode_detail_id := tax_matrix.then_taxcode_detail_id;
                     v_equal_taxcode_country_code := tax_matrix.then_taxcode_country_code;
                     v_equal_taxcode_state_code := tax_matrix.then_taxcode_state_code;
                     v_equal_taxcode_type_code := tax_matrix.then_taxcode_type_code;
                     v_equal_taxcode_code := tax_matrix.then_taxcode_code;
                     v_equal_jurisdiction_id := tax_matrix.then_jurisdiction_id;
                     v_equal_measure_type_code := tax_matrix.then_measure_type_code;
                     v_equal_taxtype_code := tax_matrix.then_taxtype_code;
                     v_equal_cch_taxcat_code := tax_matrix.then_cch_taxcat_code;
                     v_equal_cch_group_code := tax_matrix.then_cch_group_code;
                     -- v_equal_cch_item_code := tax_matrix.then_cch_item_code;
                     v_greater_hold_code_flag := tax_matrix.else_hold_code_flag;
                     v_greater_taxcode_detail_id := tax_matrix.else_taxcode_detail_id;
                     v_greater_taxcode_country_code := tax_matrix.else_taxcode_country_code;
                     v_greater_taxcode_state_code := tax_matrix.else_taxcode_state_code;
                     v_greater_taxcode_type_code := tax_matrix.else_taxcode_type_code;
                     v_greater_taxcode_code := tax_matrix.else_taxcode_code;
                     v_greater_jurisdiction_id := tax_matrix.else_jurisdiction_id;
                     v_greater_measure_type_code := tax_matrix.else_measure_type_code;
                     v_greater_taxtype_code := tax_matrix.else_taxtype_code;
                     v_greater_cch_taxcat_code := tax_matrix.else_cch_taxcat_code;
                     v_greater_cch_group_code := tax_matrix.else_cch_group_code;
                     -- v_greater_cch_item_code := tax_matrix.else_cch_item_code;
                  ELSIF tax_matrix.relation_sign = '>' THEN
                     v_less_hold_code_flag := tax_matrix.else_hold_code_flag;
                     v_less_taxcode_detail_id := tax_matrix.else_taxcode_detail_id;
                     v_less_taxcode_country_code := tax_matrix.else_taxcode_country_code;
                     v_less_taxcode_state_code := tax_matrix.else_taxcode_state_code;
                     v_less_taxcode_type_code := tax_matrix.else_taxcode_type_code;
                     v_less_taxcode_code := tax_matrix.else_taxcode_code;
                     v_less_jurisdiction_id := tax_matrix.else_jurisdiction_id;
                     v_less_measure_type_code := tax_matrix.else_measure_type_code;
                     v_less_taxtype_code := tax_matrix.else_taxtype_code;
                     v_less_cch_taxcat_code := tax_matrix.else_cch_taxcat_code;
                     v_less_cch_group_code := tax_matrix.else_cch_group_code;
                     -- v_less_cch_item_code := tax_matrix.else_cch_item_code;
                     v_equal_hold_code_flag := tax_matrix.else_hold_code_flag;
                     v_equal_taxcode_detail_id := tax_matrix.else_taxcode_detail_id;
                     v_equal_taxcode_country_code := tax_matrix.else_taxcode_country_code;
                     v_equal_taxcode_state_code := tax_matrix.else_taxcode_state_code;
                     v_equal_taxcode_type_code := tax_matrix.else_taxcode_type_code;
                     v_equal_taxcode_code := tax_matrix.else_taxcode_code;
                     v_equal_jurisdiction_id := tax_matrix.else_jurisdiction_id;
                     v_equal_measure_type_code := tax_matrix.else_measure_type_code;
                     v_equal_taxtype_code := tax_matrix.else_taxtype_code;
                     v_equal_cch_taxcat_code := tax_matrix.else_cch_taxcat_code;
                     v_equal_cch_group_code := tax_matrix.else_cch_group_code;
                     -- v_equal_cch_item_code := tax_matrix.else_cch_item_code;
                     v_greater_hold_code_flag := tax_matrix.then_hold_code_flag;
                     v_greater_taxcode_detail_id := tax_matrix.then_taxcode_detail_id;
                     v_greater_taxcode_country_code := tax_matrix.then_taxcode_country_code;
                     v_greater_taxcode_state_code := tax_matrix.then_taxcode_state_code;
                     v_greater_taxcode_type_code := tax_matrix.then_taxcode_type_code;
                     v_greater_taxcode_code := tax_matrix.then_taxcode_code;
                     v_greater_jurisdiction_id := tax_matrix.then_jurisdiction_id;
                     v_greater_measure_type_code := tax_matrix.then_measure_type_code;
                     v_greater_taxtype_code := tax_matrix.then_taxtype_code;
                     v_greater_cch_taxcat_code := tax_matrix.then_cch_taxcat_code;
                     v_greater_cch_group_code := tax_matrix.then_cch_group_code;
                     -- v_greater_cch_item_code := tax_matrix.then_cch_item_code;
                  ELSIF tax_matrix.relation_sign = '>=' THEN
                     v_less_hold_code_flag := tax_matrix.else_hold_code_flag;
                     v_less_taxcode_detail_id := tax_matrix.else_taxcode_detail_id;
                     v_less_taxcode_country_code := tax_matrix.else_taxcode_country_code;
                     v_less_taxcode_state_code := tax_matrix.else_taxcode_state_code;
                     v_less_taxcode_type_code := tax_matrix.else_taxcode_type_code;
                     v_less_taxcode_code := tax_matrix.else_taxcode_code;
                     v_less_jurisdiction_id := tax_matrix.else_jurisdiction_id;
                     v_less_measure_type_code := tax_matrix.else_measure_type_code;
                     v_less_taxtype_code := tax_matrix.else_taxtype_code;
                     v_less_cch_taxcat_code := tax_matrix.else_cch_taxcat_code;
                     v_less_cch_group_code := tax_matrix.else_cch_group_code;
                     -- v_less_cch_item_code := tax_matrix.else_cch_item_code;
                     v_equal_hold_code_flag := tax_matrix.then_hold_code_flag;
                     v_equal_taxcode_detail_id := tax_matrix.then_taxcode_detail_id;
                     v_equal_taxcode_country_code := tax_matrix.then_taxcode_country_code;
                     v_equal_taxcode_state_code := tax_matrix.then_taxcode_state_code;
                     v_equal_taxcode_type_code := tax_matrix.then_taxcode_type_code;
                     v_equal_taxcode_code := tax_matrix.then_taxcode_code;
                     v_equal_jurisdiction_id := tax_matrix.then_jurisdiction_id;
                     v_equal_measure_type_code := tax_matrix.then_measure_type_code;
                     v_equal_taxtype_code := tax_matrix.then_taxtype_code;
                     v_equal_cch_taxcat_code := tax_matrix.then_cch_taxcat_code;
                     v_equal_cch_group_code := tax_matrix.then_cch_group_code;
                     -- v_equal_cch_item_code := tax_matrix.then_cch_item_code;
                     v_greater_hold_code_flag := tax_matrix.then_hold_code_flag;
                     v_greater_taxcode_detail_id := tax_matrix.then_taxcode_detail_id;
                     v_greater_taxcode_country_code := tax_matrix.then_taxcode_country_code;
                     v_greater_taxcode_state_code := tax_matrix.then_taxcode_state_code;
                     v_greater_taxcode_type_code := tax_matrix.then_taxcode_type_code;
                     v_greater_taxcode_code := tax_matrix.then_taxcode_code;
                     v_greater_jurisdiction_id := tax_matrix.then_jurisdiction_id;
                     v_greater_measure_type_code := tax_matrix.then_measure_type_code;
                     v_greater_taxtype_code := tax_matrix.then_taxtype_code;
                     v_greater_cch_taxcat_code := tax_matrix.then_cch_taxcat_code;
                     v_greater_cch_group_code := tax_matrix.then_cch_group_code;
                     -- v_greater_cch_item_code := tax_matrix.then_cch_item_code;
                  ELSIF tax_matrix.relation_sign = '<>' THEN
                     v_less_hold_code_flag := tax_matrix.then_hold_code_flag;
                     v_less_taxcode_detail_id := tax_matrix.then_taxcode_detail_id;
                     v_less_taxcode_country_code := tax_matrix.then_taxcode_country_code;
                     v_less_taxcode_state_code := tax_matrix.then_taxcode_state_code;
                     v_less_taxcode_type_code := tax_matrix.then_taxcode_type_code;
                     v_less_taxcode_code := tax_matrix.then_taxcode_code;
                     v_less_jurisdiction_id := tax_matrix.then_jurisdiction_id;
                     v_less_measure_type_code := tax_matrix.then_measure_type_code;
                     v_less_taxtype_code := tax_matrix.then_taxtype_code;
                     v_less_cch_taxcat_code := tax_matrix.then_cch_taxcat_code;
                     v_less_cch_group_code := tax_matrix.then_cch_group_code;
                     -- v_less_cch_item_code := tax_matrix.then_cch_item_code;
                     v_equal_hold_code_flag := tax_matrix.else_hold_code_flag;
                     v_equal_taxcode_detail_id := tax_matrix.else_taxcode_detail_id;
                     v_equal_taxcode_country_code := tax_matrix.else_taxcode_country_code;
                     v_equal_taxcode_state_code := tax_matrix.else_taxcode_state_code;
                     v_equal_taxcode_type_code := tax_matrix.else_taxcode_type_code;
                     v_equal_taxcode_code := tax_matrix.else_taxcode_code;
                     v_equal_jurisdiction_id := tax_matrix.else_jurisdiction_id;
                     v_equal_measure_type_code := tax_matrix.else_measure_type_code;
                     v_equal_taxtype_code := tax_matrix.else_taxtype_code;
                     v_equal_cch_taxcat_code := tax_matrix.else_cch_taxcat_code;
                     v_equal_cch_group_code := tax_matrix.else_cch_group_code;
                     -- v_equal_cch_item_code := tax_matrix.else_cch_item_code;
                     v_greater_hold_code_flag := tax_matrix.then_hold_code_flag;
                     v_greater_taxcode_detail_id := tax_matrix.then_taxcode_detail_id;
                     v_greater_taxcode_country_code := tax_matrix.then_taxcode_country_code;
                     v_greater_taxcode_state_code := tax_matrix.then_taxcode_state_code;
                     v_greater_taxcode_type_code := tax_matrix.then_taxcode_type_code;
                     v_greater_taxcode_code := tax_matrix.then_taxcode_code;
                     v_greater_jurisdiction_id := tax_matrix.then_jurisdiction_id;
                     v_greater_measure_type_code := tax_matrix.then_measure_type_code;
                     v_greater_taxtype_code := tax_matrix.then_taxtype_code;
                     v_greater_cch_taxcat_code := tax_matrix.then_cch_taxcat_code;
                     v_greater_cch_group_code := tax_matrix.then_cch_group_code;
                     -- v_greater_cch_item_code := tax_matrix.then_cch_item_code;
                  END IF;

                  SELECT DECODE(SIGN(transaction_detail.gl_line_itm_dist_amt - tax_matrix.relation_amount), -1, v_less_hold_code_flag, 0, v_equal_hold_code_flag, 1, v_greater_hold_code_flag),
                         DECODE(SIGN(transaction_detail.gl_line_itm_dist_amt - tax_matrix.relation_amount), -1, v_less_taxcode_detail_id, 0, v_equal_taxcode_detail_id, 1, v_greater_taxcode_detail_id),
                         DECODE(SIGN(transaction_detail.gl_line_itm_dist_amt - tax_matrix.relation_amount), -1, v_less_taxcode_country_code, 0, v_equal_taxcode_country_code, 1, v_greater_taxcode_country_code),
                         DECODE(SIGN(transaction_detail.gl_line_itm_dist_amt - tax_matrix.relation_amount), -1, v_less_taxcode_state_code, 0, v_equal_taxcode_state_code, 1, v_greater_taxcode_state_code),
                         DECODE(SIGN(transaction_detail.gl_line_itm_dist_amt - tax_matrix.relation_amount), -1, v_less_taxcode_type_code, 0, v_equal_taxcode_type_code, 1, v_greater_taxcode_type_code),
                         DECODE(SIGN(transaction_detail.gl_line_itm_dist_amt - tax_matrix.relation_amount), -1, v_less_taxcode_code, 0, v_equal_taxcode_code, 1, v_greater_taxcode_code),
                         DECODE(SIGN(transaction_detail.gl_line_itm_dist_amt - tax_matrix.relation_amount), -1, v_less_jurisdiction_id, 0, v_equal_jurisdiction_id, 1, v_greater_jurisdiction_id),
                         DECODE(SIGN(transaction_detail.gl_line_itm_dist_amt - tax_matrix.relation_amount), -1, v_less_measure_type_code, 0, v_equal_measure_type_code, 1, v_greater_measure_type_code),
                         DECODE(SIGN(transaction_detail.gl_line_itm_dist_amt - tax_matrix.relation_amount), -1, v_less_taxtype_code, 0, v_equal_taxtype_code, 1, v_greater_taxtype_code),
                         DECODE(SIGN(transaction_detail.gl_line_itm_dist_amt - tax_matrix.relation_amount), -1, v_less_cch_taxcat_code, 0, v_equal_cch_taxcat_code, 1, v_greater_cch_taxcat_code),
                         DECODE(SIGN(transaction_detail.gl_line_itm_dist_amt - tax_matrix.relation_amount), -1, v_less_cch_group_code, 0, v_equal_cch_group_code, 1, v_greater_cch_group_code)
                    INTO v_hold_code_flag, transaction_detail.taxcode_detail_id, transaction_detail.taxcode_country_code, transaction_detail.taxcode_state_code, transaction_detail.taxcode_type_code, transaction_detail.taxcode_code, v_override_jurisdiction_id, transaction_detail.measure_type_code, v_taxtype_code, transaction_detail.cch_taxcat_code, transaction_detail.cch_group_code
                    FROM DUAL;

                  -- ****** DETERMINE TYPE of TAXCODE ****** --
                  -- The Tax Code is Taxable
                  IF transaction_detail.taxcode_type_code = 'T' THEN
                     -- If Override jurisdiction id is not blank or 0 then use it instead of location matrix
                     IF v_override_jurisdiction_id IS NOT NULL AND v_override_jurisdiction_id <> 0 THEN
                        transaction_detail.jurisdiction_id := v_override_jurisdiction_id;
                        transaction_detail.manual_jurisdiction_ind := 'AO';
                     ELSE
                        -- Search for Location Matrix if not already found
                        IF (transaction_detail.location_matrix_id IS NULL OR transaction_detail.location_matrix_id = 0) THEN
                           -- and no jurisdiction        MBF05
                           IF (transaction_detail.jurisdiction_id IS NULL OR transaction_detail.jurisdiction_id = 0) THEN
                              -- Search Location Matrix for matches
                              BEGIN
                                  OPEN location_matrix_cursor
                                   FOR vc_location_matrix_stmt
                                 USING v_rowid;
                                 FETCH location_matrix_cursor
                                  INTO location_matrix;
                                 -- Rows found?
                                 IF location_matrix_cursor%FOUND THEN
                                    vn_fetch_rows := location_matrix_cursor%ROWCOUNT;
                                 ELSE
                                    vn_fetch_rows := 0;
                                 END IF;
                                 CLOSE location_matrix_cursor;
                                 IF SQLCODE != 0 THEN
                                    RAISE e_badread;
                                 END IF;
                              EXCEPTION
                                 WHEN e_badread THEN
                                    ac_error_code := 'P4';
                                    RAISE e_abort;
                              END;

                              -- Location Matrix Line Found
                              IF vn_fetch_rows > 0 THEN
                                 transaction_detail.location_matrix_id := location_matrix.location_matrix_id;
                                 transaction_detail.jurisdiction_id := location_matrix.jurisdiction_id;
                              ELSE
                                 transaction_detail.transaction_ind := 'S';
                                 transaction_detail.suspend_ind := 'L';
                              END IF;
                           ELSE
                              -- if there is a jurisdiction without a location matrix, then default tax flags        MBF05
                              location_matrix.override_taxtype_code := '*NO';
                              location_matrix.country_flag := '1';
                              location_matrix.state_flag := '1';
                              location_matrix.county_flag := '1';
                              location_matrix.county_local_flag := '1';
                              location_matrix.city_flag := '1';
                              location_matrix.city_local_flag := '1';
                           END IF;
                        END IF;
                     END IF;

                     -- Continue if not Suspended
                     IF transaction_detail.transaction_ind IS NULL OR transaction_detail.transaction_ind <> 'S' THEN
                        -- Get Jurisdiction TaxRates and calculate Tax Amounts
                        sp_getusetax(transaction_detail.jurisdiction_id,
                                     transaction_detail.gl_date,
                                     transaction_detail.measure_type_code,
                                     transaction_detail.gl_line_itm_dist_amt,
                                     v_taxtype_code,
                                     location_matrix.override_taxtype_code,
                                     location_matrix.country_flag,
                                     location_matrix.state_flag,
                                     location_matrix.county_flag,
                                     location_matrix.county_local_flag,
                                     location_matrix.city_flag,
                                     location_matrix.city_local_flag,
                                     transaction_detail.invoice_freight_amt,
                                     transaction_detail.invoice_discount_amt,
                                     transaction_detail.transaction_country_code,
                                     transaction_detail.transaction_state_code,
-- future?? --                          transaction_detail.import_definition_code,
                                     'importdefn',
                                     transaction_detail.taxcode_code,
                                     transaction_detail.jurisdiction_taxrate_id,
                                     transaction_detail.country_use_amount,
                                     transaction_detail.state_use_amount,
                                     transaction_detail.state_use_tier2_amount,
                                     transaction_detail.state_use_tier3_amount,
                                     transaction_detail.county_use_amount,
                                     transaction_detail.county_local_use_amount,
                                     transaction_detail.city_use_amount,
                                     transaction_detail.city_local_use_amount,
                                     transaction_detail.country_use_rate,
                                     transaction_detail.state_use_rate,
                                     transaction_detail.state_use_tier2_rate,
                                     transaction_detail.state_use_tier3_rate,
                                     transaction_detail.state_split_amount,
                                     transaction_detail.state_tier2_min_amount,
                                     transaction_detail.state_tier2_max_amount,
                                     transaction_detail.state_maxtax_amount,
                                     transaction_detail.county_use_rate,
                                     transaction_detail.county_local_use_rate,
                                     transaction_detail.county_split_amount,
                                     transaction_detail.county_maxtax_amount,
                                     transaction_detail.county_single_flag,
                                     transaction_detail.county_default_flag,
                                     transaction_detail.city_use_rate,
                                     transaction_detail.city_local_use_rate,
                                     transaction_detail.city_split_amount,
                                     transaction_detail.city_split_use_rate,
                                     transaction_detail.city_single_flag,
                                     transaction_detail.city_default_flag,
                                     transaction_detail.combined_use_rate,
                                     transaction_detail.tb_calc_tax_amt,
-- future?? --                          transaction_detail.taxable_amt,
-- Targa --                         transaction_detail.user_number_10,
                                     vn_taxable_amt,
                                     v_taxtype_used );
                        IF transaction_detail.jurisdiction_taxrate_id = 0 THEN
                            vn_fetch_rows := 0;
                        END IF;
                        IF transaction_detail.jurisdiction_taxrate_id IS NULL THEN
                           ac_error_code := 'P6';
                           RAISE e_abort;
                        END IF;

                        -- Jurisdiction Tax Rate FOUND
                        IF vn_fetch_rows > 0 THEN
                           IF vn_taxable_amt <> transaction_detail.gl_line_itm_dist_amt THEN
                              transaction_detail.gl_extract_amt := transaction_detail.gl_line_itm_dist_amt;
                              transaction_detail.gl_line_itm_dist_amt := vn_taxable_amt;
                           END IF;
                        -- Jurisdiciton Tax Rate NOT Found
                        ELSE
                        -- IF vn_fetch_rows = 0 THEN
                           transaction_detail.transaction_ind := 'S';
                           transaction_detail.suspend_ind := 'R';
                        END IF;
                     END IF;

                  -- The Tax Code is Exempt
                  ELSIF transaction_detail.taxcode_type_code = 'E' THEN
                     transaction_detail.transaction_ind := 'P';

                  -- The Tax Code is a Question
                  ELSIF transaction_detail.taxcode_type_code = 'Q' THEN
                    transaction_detail.transaction_ind := 'Q';

                  -- The Tax Code is Unrecognized - Suspend
                  ELSE
                     transaction_detail.transaction_ind := 'S';
                     transaction_detail.suspend_ind := '?';
                  END IF;

                  -- Continue if not Suspended
                  IF transaction_detail.transaction_ind IS NULL OR transaction_detail.transaction_ind <> 'S' THEN
                     -- Check for matrix "H"old AND and global "H"old ELSE "P"rocessed
                     IF ( v_hold_code_flag = '1' ) AND
                        ( transaction_detail.taxcode_type_code = 'T' OR transaction_detail.taxcode_type_code = 'E') THEN
                           transaction_detail.transaction_ind := 'H';
                     ELSIF ( v_hold_trans_flag = '1' ) AND
                           ( transaction_detail.taxcode_type_code = 'T' OR transaction_detail.taxcode_type_code = 'E') AND
                           ( ABS(transaction_detail.gl_line_itm_dist_amt) >= v_hold_trans_amount ) THEN
                              transaction_detail.transaction_ind := 'H';
                     ELSE
                        transaction_detail.transaction_ind := 'P';
                     END IF;
                  END IF;

               -- Tax Matrix Line NOT Found
               ELSE
                  IF transaction_detail.transaction_state_code = '*DEF' THEN
                     transaction_detail.transaction_ind := 'S';
                     transaction_detail.suspend_ind := 'L';
                     IF transaction_detail.auto_transaction_state_code = '*NULL' THEN
                        transaction_detail.transaction_state_code := NULL;
                     ELSE
                        transaction_detail.transaction_state_code := transaction_detail.auto_transaction_state_code;
                     END IF;
                     transaction_detail.auto_transaction_state_code := NULL;
                  ELSE
                     transaction_detail.transaction_ind := 'S';
                     transaction_detail.suspend_ind := 'T';
                  END IF;
               END IF;
            END IF;
         END IF;   -- IF multi_trans_code LIKE 'O%' THEN

         -- Write line?
         IF v_write_import_line_flag = '1' THEN
            transaction_detail.update_user_id := USER;
            transaction_detail.update_timestamp := SYSDATE;

            -- Check for Auto-Process Material Limit on Suspended Lines
            IF v_autoproc_flag = '1' THEN
               IF transaction_detail.transaction_ind = 'S' AND transaction_detail.suspend_ind = 'T' THEN
                  IF ABS(transaction_detail.gl_line_itm_dist_amt) <= vn_autoproc_amount THEN
                     transaction_detail.transaction_ind := 'P';
                     transaction_detail.suspend_ind := '';
                     transaction_detail.manual_taxcode_ind := 'AP';
                     transaction_detail.taxcode_detail_id := vn_autoproc_id;
                     transaction_detail.taxcode_country_code := v_autoproc_country_code;
                     transaction_detail.taxcode_state_code := v_autoproc_state_code;
                     transaction_detail.taxcode_type_code := v_autoproc_type_code;
                     transaction_detail.taxcode_code := v_autoproc_code;
                  END IF;
               END IF;
            END IF;

            -- Insert transaction detail row
            BEGIN
               -- 3411 - add taxable amount and tax type used.
               UPDATE tb_transaction_detail
                  SET gl_line_itm_dist_amt = transaction_detail.gl_line_itm_dist_amt,
                      orig_gl_line_itm_dist_amt = transaction_detail.orig_gl_line_itm_dist_amt,
                      tb_calc_tax_amt = transaction_detail.tb_calc_tax_amt,
                      country_use_amount = transaction_detail.country_use_amount,
                      state_use_amount = transaction_detail.state_use_amount,
                      state_use_tier2_amount = transaction_detail.state_use_tier2_amount,
                      state_use_tier3_amount = transaction_detail.state_use_tier3_amount,
                      county_use_amount = transaction_detail.county_use_amount,
                      county_local_use_amount = transaction_detail.county_local_use_amount,
                      city_use_amount = transaction_detail.city_use_amount,
                      city_local_use_amount = transaction_detail.city_local_use_amount,
                      transaction_country_code = transaction_detail.transaction_country_code,
                      transaction_state_code = transaction_detail.transaction_state_code,
                      auto_transaction_country_code = transaction_detail.auto_transaction_country_code,
                      auto_transaction_state_code = transaction_detail.auto_transaction_state_code,
                      transaction_ind = transaction_detail.transaction_ind,
                      suspend_ind = transaction_detail.suspend_ind,
                      taxcode_detail_id = transaction_detail.taxcode_detail_id,
                      taxcode_country_code = transaction_detail.taxcode_country_code,
                      taxcode_state_code = transaction_detail.taxcode_state_code,
                      taxcode_type_code = transaction_detail.taxcode_type_code,
                      taxcode_code = transaction_detail.taxcode_code,
                      cch_taxcat_code = transaction_detail.cch_taxcat_code,
                      cch_group_code = transaction_detail.cch_group_code,
                      cch_item_code = transaction_detail.cch_item_code,
                      manual_taxcode_ind = transaction_detail.manual_taxcode_ind,
                      tax_matrix_id = transaction_detail.tax_matrix_id,
                      location_matrix_id = transaction_detail.location_matrix_id,
                      jurisdiction_id = transaction_detail.jurisdiction_id,
                      jurisdiction_taxrate_id = transaction_detail.jurisdiction_taxrate_id,
                      manual_jurisdiction_ind = transaction_detail.manual_jurisdiction_ind,
                      measure_type_code = transaction_detail.measure_type_code,
                      country_use_rate = transaction_detail.country_use_rate,
                      state_use_rate = transaction_detail.state_use_rate,
                      state_use_tier2_rate = transaction_detail.state_use_tier2_rate,
                      state_use_tier3_rate = transaction_detail.state_use_tier3_rate,
                      state_split_amount = transaction_detail.state_split_amount,
                      state_tier2_min_amount = transaction_detail.state_tier2_min_amount,
                      state_tier2_max_amount = transaction_detail.state_tier2_max_amount,
                      state_maxtax_amount = transaction_detail.state_maxtax_amount,
                      county_use_rate = transaction_detail.county_use_rate,
                      county_local_use_rate = transaction_detail.county_local_use_rate,
                      county_split_amount = transaction_detail.county_split_amount,
                      county_maxtax_amount = transaction_detail.county_maxtax_amount,
                      county_single_flag = transaction_detail.county_single_flag,
                      county_default_flag = transaction_detail.county_default_flag,
                      city_use_rate = transaction_detail.city_use_rate,
                      city_local_use_rate = transaction_detail.city_local_use_rate,
                      city_split_amount = transaction_detail.city_split_amount,
                      city_split_use_rate = transaction_detail.city_split_use_rate,
                      city_single_flag = transaction_detail.city_single_flag,
                      city_default_flag = transaction_detail.city_default_flag,
                      combined_use_rate = transaction_detail.combined_use_rate,
                      gl_extract_amt = transaction_detail.gl_extract_amt,
                      modify_user_id = transaction_detail.modify_user_id,
                      modify_timestamp = transaction_detail.modify_timestamp,
                      update_user_id = transaction_detail.update_user_id,
                      update_timestamp = transaction_detail.update_timestamp
                WHERE tb_transaction_detail.rowid = v_rowid;
               -- Error Checking
               IF SQLCODE != 0 THEN
                  RAISE e_badwrite;
               END IF;
            EXCEPTION
               WHEN e_badwrite THEN
                  ac_error_code := 'P7';
                  RAISE e_abort;
            END;
         END IF;
      END IF;
   END LOOP;
   CLOSE transaction_detail_cursor;
insert into tb_debug(row_joe) values('leaving tb_transaction_process, error code = ' || ac_error_code);
   --Run User Exit?
/*   BEGIN
      SELECT LTRIM(RTRIM(value))
        INTO v_processuserexit_flag
        FROM tb_option
       WHERE option_code = 'PROCESSUSEREXIT' AND
             option_type_code = 'ADMIN' AND
             user_code = 'ADMIN';
   EXCEPTION
      WHEN OTHERS THEN
         v_processuserexit_flag := '0';
   END;
   IF v_processuserexit_flag IS NULL THEN
      v_processuserexit_flag := '0';
   END IF;
   --  Run User Exit if selected
   IF v_processuserexit_flag = '1' THEN
      vn_return := f_process_userexit(an_batch_id);
      IF vn_return < 0 THEN
         ac_error_code := 'UE4';
      END IF;
   END IF;    */
   COMMIT;

EXCEPTION
   WHEN e_abort THEN
      ROLLBACK;
END;
/
