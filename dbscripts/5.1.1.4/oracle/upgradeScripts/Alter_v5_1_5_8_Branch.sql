-- TB_LIST_CODE
INSERT INTO TB_LIST_CODE (CODE_TYPE_CODE, CODE_CODE, DESCRIPTION, MESSAGE) VALUES ('*DEF', 'COUNTRY', 'Country', '') ;
INSERT INTO TB_LIST_CODE (CODE_TYPE_CODE, CODE_CODE, DESCRIPTION, MESSAGE) VALUES ('COUNTRY', 'US', 'United States', '');
INSERT INTO TB_LIST_CODE (CODE_TYPE_CODE, CODE_CODE, DESCRIPTION, MESSAGE) VALUES ('COUNTRY', 'CA', 'Canada', '');

-- TB_BCP_JURISDICTION_TAXRATE
ALTER TABLE TB_BCP_JURISDICTION_TAXRATE ADD COUNTRY VARCHAR2(10);
ALTER TABLE TB_BCP_JURISDICTION_TAXRATE ADD COUNTRY_SALES_RATE NUMBER;
ALTER TABLE TB_BCP_JURISDICTION_TAXRATE ADD COUNTRY_USE_RATE NUMBER;
UPDATE TB_BCP_JURISDICTION_TAXRATE SET COUNTRY='US';

-- TB_JURISDICTION
ALTER TABLE TB_JURISDICTION ADD COUNTRY  VARCHAR2(10);
UPDATE TB_JURISDICTION SET COUNTRY='US';

-- TB_JURISDICTION_TAXRATE
ALTER TABLE TB_JURISDICTION_TAXRATE ADD COUNTRY_SALES_RATE  NUMBER;
ALTER TABLE TB_JURISDICTION_TAXRATE ADD COUNTRY_USE_RATE  NUMBER;

-- TB_TAXCODE_STATE
ALTER TABLE TB_TAXCODE_STATE MODIFY COUNTRY NOT NULL;
UPDATE TB_TAXCODE_STATE SET COUNTRY='US'
-- ALTER TABLE TB_TAXCODE_STATE ADD CONSTRAINT PK_TB_TAXCODE_STATE PRIMARY KEY (TAXCODE_COUNTRY_CODE, TAXCODE_STATE_CODE) ENABLE VALIDATE;
-- ALTER TABLE TB_TAXCODE_STATE ADD CONSTRAINT PK_TB_TAXCODE_STATE PRIMARY KEY (COUNTRY, TAXCODE_STATE_CODE) ENABLE VALIDATE;

-- TB_TAXCODE_DETAIL
ALTER TABLE TB_TAXCODE_DETAIL ADD TAXCODE_COUNTRY_CODE VARCHAR2(10);
UPDATE TB_TAXCODE_DETAIL SET TAXCODE_COUNTRY_CODE='US';

-- TB_TAX_MATRIX
ALTER TABLE TB_TAX_MATRIX ADD MATRIX_COUNTRY_CODE VARCHAR2(10);
UPDATE TB_TAX_MATRIX SET MATRIX_COUNTRY_CODE='US';

-- TB_TMP_TAX_MATRIX, no Java object
ALTER TABLE TB_TMP_TAX_MATRIX  ADD MATRIX_COUNTRY_CODE VARCHAR2(10);

-- TB_LOCATION_MATRIX
ALTER TABLE TB_LOCATION_MATRIX ADD COUNTRY VARCHAR2(10);
ALTER TABLE TB_LOCATION_MATRIX ADD COUNTRY_FLAG CHAR(1 BYTE);
UPDATE TB_LOCATION_MATRIX SET COUNTRY='US';

-- TB_TMP_LOCATION_MATRIX, no Java object
ALTER TABLE TB_TMP_LOCATION_MATRIX ADD COUNTRY VARCHAR2(10);
ALTER TABLE TB_TMP_LOCATION_MATRIX ADD COUNTRY_FLAG CHAR(1 BYTE);

-- TB_GL_EXTRACT_MAP
ALTER TABLE TB_GL_EXTRACT_MAP ADD TAXCODE_COUNTRY_CODE VARCHAR2(10);
UPDATE TB_GL_EXTRACT_MAP SET TAXCODE_COUNTRY_CODE='US';

-- TB_BCP_TRANSACTIONS
ALTER TABLE TB_BCP_TRANSACTIONS  ADD COUNTRY_USE_AMOUNT  NUMBER;
ALTER TABLE TB_BCP_TRANSACTIONS  ADD TRANSACTION_COUNTRY_CODE VARCHAR2(10);
ALTER TABLE TB_BCP_TRANSACTIONS  ADD AUTO_TRANSACTION_COUNTRY_CODE  VARCHAR2(10);
ALTER TABLE TB_BCP_TRANSACTIONS  ADD TAXCODE_COUNTRY_CODE VARCHAR2(10);
ALTER TABLE TB_BCP_TRANSACTIONS  ADD COUNTRY_USE_RATE NUMBER;

-- TB_TRANSACTION_DETAIL
ALTER TABLE TB_TRANSACTION_DETAIL ADD COUNTRY_USE_AMOUNT  NUMBER;
ALTER TABLE TB_TRANSACTION_DETAIL ADD TRANSACTION_COUNTRY_CODE VARCHAR2(10);
UPDATE TB_TRANSACTION_DETAIL SET TRANSACTION_COUNTRY_CODE='US';
ALTER TABLE TB_TRANSACTION_DETAIL ADD AUTO_TRANSACTION_COUNTRY_CODE  VARCHAR2(10);
ALTER TABLE TB_TRANSACTION_DETAIL ADD TAXCODE_COUNTRY_CODE VARCHAR2(10);
UPDATE TB_TRANSACTION_DETAIL SET TAXCODE_COUNTRY_CODE='US';
ALTER TABLE TB_TRANSACTION_DETAIL ADD COUNTRY_USE_RATE NUMBER;

INSERT INTO TB_DATA_DEF_COLUMN (TABLE_NAME, COLUMN_NAME, DATA_TYPE, DATA_LENGTH, DESCRIPTION, ABBR_DESC,  MAP_FLAG) 
VALUES ('TB_TRANSACTION_DETAIL', 'COUNTRY_USE_AMOUNT', 'NUMBER', 22, 'Country Use Amount', 'Country Use Amt.', '1');

INSERT INTO TB_DATA_DEF_COLUMN (TABLE_NAME, COLUMN_NAME, DATA_TYPE, DATA_LENGTH, DESCRIPTION, ABBR_DESC,  MAP_FLAG) 
VALUES ('TB_TRANSACTION_DETAIL', 'COUNTRY_USE_RATE', 'NUMBER', 22, 'Country Use Rate', 'Country Use Rate', '1');

INSERT INTO TB_DATA_DEF_COLUMN (TABLE_NAME, COLUMN_NAME, DATA_TYPE, DATA_LENGTH, DESCRIPTION, ABBR_DESC,  MAP_FLAG) 
VALUES ('TB_TRANSACTION_DETAIL', 'TRANSACTION_COUNTRY_CODE', 'VARCHAR2', 10, 'Transaction Country Code', 'Trans. Country', '1');

INSERT INTO TB_DATA_DEF_COLUMN (TABLE_NAME, COLUMN_NAME, DATA_TYPE, DATA_LENGTH, DESCRIPTION, ABBR_DESC,  MAP_FLAG) 
VALUES ('TB_TRANSACTION_DETAIL', 'AUTO_TRANSACTION_COUNTRY_CODE', 'VARCHAR2', 10, 'Auto Transaction Country code', 'Auto Trans. Country', '1');

INSERT INTO TB_DATA_DEF_COLUMN (TABLE_NAME, COLUMN_NAME, DATA_TYPE, DATA_LENGTH, DESCRIPTION, ABBR_DESC,  MAP_FLAG) 
VALUES ('TB_TRANSACTION_DETAIL', 'TAXCODE_COUNTRY_CODE', 'VARCHAR2', 10, 'TaxCode Country code', 'TaxCode Country', '1');

-- TB_GL_EXPORT_LOG
ALTER TABLE TB_GL_EXPORT_LOG ADD COUNTRY_USE_AMOUNT  NUMBER;
ALTER TABLE TB_GL_EXPORT_LOG ADD TRANSACTION_COUNTRY_CODE VARCHAR2(10);
ALTER TABLE TB_GL_EXPORT_LOG ADD AUTO_TRANSACTION_COUNTRY_CODE  VARCHAR2(10);
ALTER TABLE TB_GL_EXPORT_LOG ADD TAXCODE_COUNTRY_CODE VARCHAR2(10);
ALTER TABLE TB_GL_EXPORT_LOG ADD COUNTRY_USE_RATE NUMBER;

-- TB_GL_REPORT_LOG, no Java object
ALTER TABLE TB_GL_REPORT_LOG ADD COUNTRY_USE_AMOUNT  NUMBER;
ALTER TABLE TB_GL_REPORT_LOG ADD TRANSACTION_COUNTRY_CODE VARCHAR2(10);
ALTER TABLE TB_GL_REPORT_LOG ADD AUTO_TRANSACTION_COUNTRY_CODE  VARCHAR2(10);
ALTER TABLE TB_GL_REPORT_LOG ADD TAXCODE_COUNTRY_CODE VARCHAR2(10);
ALTER TABLE TB_GL_REPORT_LOG ADD COUNTRY_USE_RATE NUMBER;

-- TB_TMP_GL_EXPORT, no Java object
ALTER TABLE TB_TMP_GL_EXPORT ADD COUNTRY_USE_AMOUNT  NUMBER;
ALTER TABLE TB_TMP_GL_EXPORT ADD TRANSACTION_COUNTRY_CODE VARCHAR2(10);
ALTER TABLE TB_TMP_GL_EXPORT ADD AUTO_TRANSACTION_COUNTRY_CODE  VARCHAR2(10);
ALTER TABLE TB_TMP_GL_EXPORT ADD TAXCODE_COUNTRY_CODE VARCHAR2(10);
ALTER TABLE TB_TMP_GL_EXPORT ADD COUNTRY_USE_RATE NUMBER;

-- TB_TMP_TRANSACTION_DETAIL
ALTER TABLE TB_TMP_TRANSACTION_DETAIL ADD COUNTRY_USE_AMOUNT  NUMBER;
ALTER TABLE TB_TMP_TRANSACTION_DETAIL ADD TRANSACTION_COUNTRY_CODE VARCHAR2(10);
ALTER TABLE TB_TMP_TRANSACTION_DETAIL ADD AUTO_TRANSACTION_COUNTRY_CODE  VARCHAR2(10);
ALTER TABLE TB_TMP_TRANSACTION_DETAIL ADD TAXCODE_COUNTRY_CODE VARCHAR2(10);
ALTER TABLE TB_TMP_TRANSACTION_DETAIL ADD COUNTRY_USE_RATE NUMBER;

-- TB_TMP_TRANSACTION_DETAIL_B_U
ALTER TABLE TB_TMP_TRANSACTION_DETAIL_B_U ADD COUNTRY_USE_AMOUNT  NUMBER;
ALTER TABLE TB_TMP_TRANSACTION_DETAIL_B_U ADD TRANSACTION_COUNTRY_CODE VARCHAR2(10);
ALTER TABLE TB_TMP_TRANSACTION_DETAIL_B_U ADD AUTO_TRANSACTION_COUNTRY_CODE  VARCHAR2(10);
ALTER TABLE TB_TMP_TRANSACTION_DETAIL_B_U ADD TAXCODE_COUNTRY_CODE VARCHAR2(10);
ALTER TABLE TB_TMP_TRANSACTION_DETAIL_B_U ADD COUNTRY_USE_RATE NUMBER;


ALTER TABLE TB_GL_EXTRACT_MAP DROP CONSTRAINT TB_GL_EXTRACT_MAP_FK01;
ALTER TABLE TB_JURISDICTION DROP CONSTRAINT TB_JURISDICTION_FK01;
ALTER TABLE TB_TAX_MATRIX DROP CONSTRAINT TB_TAX_MATRIX_FK01;
ALTER TABLE TB_TAXCODE_DETAIL DROP CONSTRAINT TB_TAXCODE_DTL_FK01; 
ALTER TABLE TB_TRANSACTION_DETAIL DROP CONSTRAINT TB_TRANSACTION_DETAIL_FK03; 
ALTER TABLE TB_TAXCODE_STATE DROP CONSTRAINT PK_TB_TAXCODE_STATE;
DROP INDEX PK_TB_TAXCODE_STATE;

ALTER TABLE TB_TAXCODE_STATE ADD CONSTRAINT PK_TB_TAXCODE_STATE PRIMARY KEY (TAXCODE_STATE_CODE,COUNTRY) ENABLE VALIDATE;

ALTER TABLE TB_GL_EXTRACT_MAP ADD (

  CONSTRAINT TB_GL_EXTRACT_MAP_FK01 FOREIGN KEY (TAXCODE_STATE_CODE,TAXCODE_COUNTRY_CODE)

    REFERENCES TB_TAXCODE_STATE (TAXCODE_STATE_CODE,COUNTRY));

ALTER TABLE TB_JURISDICTION ADD (

  CONSTRAINT TB_JURISDICTION_FK01 FOREIGN KEY (STATE,COUNTRY)

    REFERENCES TB_TAXCODE_STATE (TAXCODE_STATE_CODE,COUNTRY));

ALTER TABLE TB_TAXCODE_DETAIL ADD (

  CONSTRAINT TB_TAXCODE_DTL_FK01 FOREIGN KEY (TAXCODE_STATE_CODE,TAXCODE_COUNTRY_CODE)

    REFERENCES TB_TAXCODE_STATE (TAXCODE_STATE_CODE,COUNTRY));

ALTER TABLE TB_TAX_MATRIX ADD (

  CONSTRAINT TB_TAX_MATRIX_FK01 FOREIGN KEY (MATRIX_STATE_CODE,MATRIX_COUNTRY_CODE)

    REFERENCES TB_TAXCODE_STATE (TAXCODE_STATE_CODE,COUNTRY));


-- change Sp_Tb_Tax_Matrix_Master_List
CREATE OR REPLACE FUNCTION Sp_Tb_Tax_Matrix_Master_List (
	p_where_clause_token	IN VARCHAR2,
	p_order_by_token	IN VARCHAR2,
	p_start_index		IN INTEGER,
	p_max_rows		IN INTEGER
) RETURN sys_refcursor
AS
	l_cursor	sys_refcursor;
BEGIN
	OPEN l_cursor FOR
	'SELECT * FROM ( SELECT B.*, rownum rnum
	  FROM (
	  SELECT DISTINCT
		T.DEFAULT_FLAG,T.MATRIX_COUNTRY_CODE,T.MATRIX_STATE_CODE,T.DRIVER_GLOBAL_FLAG,
		T.DRIVER_01,T.DRIVER_02,T.DRIVER_03,T.DRIVER_04,T.DRIVER_05,T.DRIVER_06,
		T.DRIVER_07,T.DRIVER_08,T.DRIVER_09,T.DRIVER_10,T.DRIVER_11,T.DRIVER_12,
		T.DRIVER_13,T.DRIVER_14,T.DRIVER_15,T.DRIVER_16,T.DRIVER_17,T.DRIVER_18,
		T.DRIVER_19,T.DRIVER_20,T.DRIVER_21,T.DRIVER_22,T.DRIVER_23,T.DRIVER_24,
		T.DRIVER_25,T.DRIVER_26,T.DRIVER_27,T.DRIVER_28,T.DRIVER_29,T.DRIVER_30,
		T.BINARY_WEIGHT,T.DEFAULT_BINARY_WEIGHT, T.DRIVER_01_THRU,T.DRIVER_02_THRU,T.DRIVER_03_THRU,T.DRIVER_04_THRU,T.DRIVER_05_THRU,
		T.DRIVER_06_THRU,T.DRIVER_07_THRU,T.DRIVER_08_THRU,T.DRIVER_09_THRU,T.DRIVER_10_THRU,
		T.DRIVER_11_THRU,T.DRIVER_12_THRU,T.DRIVER_13_THRU,T.DRIVER_14_THRU,T.DRIVER_15_THRU,
		T.DRIVER_16_THRU,T.DRIVER_17_THRU,T.DRIVER_18_THRU,T.DRIVER_19_THRU,T.DRIVER_20_THRU,
		T.DRIVER_21_THRU,T.DRIVER_22_THRU,T.DRIVER_23_THRU,T.DRIVER_24_THRU,T.DRIVER_25_THRU,
		T.DRIVER_26_THRU,T.DRIVER_27_THRU,T.DRIVER_28_THRU,T.DRIVER_29_THRU,T.DRIVER_30_THRU
	FROM	TB_TAX_MATRIX T
	left outer join TB_TAXCODE_DETAIL TB_TAXCODE_DETAIL_A ON (T.THEN_TAXCODE_DETAIL_ID = TB_TAXCODE_DETAIL_A.TAXCODE_DETAIL_ID)
	left outer join TB_TAXCODE_DETAIL TB_TAXCODE_DETAIL_B ON (T.ELSE_TAXCODE_DETAIL_ID = TB_TAXCODE_DETAIL_B.TAXCODE_DETAIL_ID)
	WHERE 1=1 '
	||p_where_clause_token||' '
	||p_order_by_token    ||' ) B WHERE rownum <= '|| (p_start_index + p_max_rows) ||' ) WHERE rnum > '||p_start_index;

	RETURN l_cursor;
END;
/

-- change 
CREATE OR REPLACE FUNCTION Sp_Tb_Tax_Matrix_Master_Count (
	p_where_clause_token	IN VARCHAR2
) RETURN sys_refcursor
AS
	l_cursor sys_refcursor;
BEGIN

	OPEN l_cursor FOR
	'SELECT COUNT(*) FROM ( SELECT DISTINCT
	     T.DEFAULT_FLAG,T.MATRIX_COUNTRY_CODE,T.MATRIX_STATE_CODE,T.DRIVER_GLOBAL_FLAG,T.DRIVER_01,T.DRIVER_02,T.DRIVER_03,T.DRIVER_04,T.DRIVER_05,T.DRIVER_06,
	     T.DRIVER_07,T.DRIVER_08,T.DRIVER_09,T.DRIVER_10,T.DRIVER_11,T.DRIVER_12,T.DRIVER_13,T.DRIVER_14,T.DRIVER_15,T.DRIVER_16,T.DRIVER_17,T.DRIVER_18,
	     T.DRIVER_19,T.DRIVER_20,T.DRIVER_21,T.DRIVER_22,T.DRIVER_23,T.DRIVER_24,T.DRIVER_25,T.DRIVER_26,T.DRIVER_27,T.DRIVER_28,T.DRIVER_29,T.DRIVER_30,
	     T.BINARY_WEIGHT,T.DEFAULT_BINARY_WEIGHT,T.DRIVER_01_THRU,T.DRIVER_02_THRU,T.DRIVER_03_THRU,T.DRIVER_04_THRU,T.DRIVER_05_THRU,
	     T.DRIVER_06_THRU,T.DRIVER_07_THRU,T.DRIVER_08_THRU,T.DRIVER_09_THRU,T.DRIVER_10_THRU,
	     T.DRIVER_11_THRU,T.DRIVER_12_THRU,T.DRIVER_13_THRU,T.DRIVER_14_THRU,T.DRIVER_15_THRU,
	     T.DRIVER_16_THRU,T.DRIVER_17_THRU,T.DRIVER_18_THRU,T.DRIVER_19_THRU,T.DRIVER_20_THRU,
	     T.DRIVER_21_THRU,T.DRIVER_22_THRU,T.DRIVER_23_THRU,T.DRIVER_24_THRU,T.DRIVER_25_THRU,
	     T.DRIVER_26_THRU,T.DRIVER_27_THRU,T.DRIVER_28_THRU,T.DRIVER_29_THRU,T.DRIVER_30_THRU
	FROM TB_TAX_MATRIX T
	 left outer join TB_TAXCODE_DETAIL TB_TAXCODE_DETAIL_A ON (T.THEN_TAXCODE_DETAIL_ID = TB_TAXCODE_DETAIL_A.TAXCODE_DETAIL_ID)
	 left outer join TB_TAXCODE_DETAIL TB_TAXCODE_DETAIL_B ON (T.ELSE_TAXCODE_DETAIL_ID = TB_TAXCODE_DETAIL_B.TAXCODE_DETAIL_ID)
	WHERE 1=1 '|| p_where_clause_token ||' ) B ';

	RETURN l_cursor;

END;
/

-- change
CREATE OR REPLACE FUNCTION Sp_Taxcode_Detail_List (
 p_where_clause_token IN VARCHAR2
) RETURN sys_refcursor
AS
 l_cursor sys_refcursor;
BEGIN
 OPEN l_cursor FOR
 'SELECT TB_TAXCODE_DETAIL.TAXCODE_CODE, TB_TAXCODE_DETAIL.TAXCODE_STATE_CODE,
  TB_TAXCODE_STATE.NAME, TB_TAXCODE_DETAIL.TAXCODE_TYPE_CODE,
  TB_LIST_CODE.DESCRIPTION AS TAXCODE_TYPE_DESCRIPTION,
  TB_TAXCODE.DESCRIPTION AS TAXCODE_DESCRIPTION, TB_TAXCODE.COMMENTS,
  TB_TAXCODE.TAXTYPE_CODE, TB_TAXCODE.ERP_TAXCODE,
  TB_LIST_CODE1.DESCRIPTION AS TAX_TYPE_DESCRIPTION,
  TB_TAXCODE_DETAIL.TAXCODE_DETAIL_ID,
  TB_TAXCODE_DETAIL.JURISDICTION_ID, TB_JURISDICTION.GEOCODE,
  TB_JURISDICTION.CITY, TB_JURISDICTION.COUNTY, TB_JURISDICTION.STATE,
  TB_JURISDICTION.ZIP,TB_TAXCODE.MEASURE_TYPE_CODE,TB_TAXCODE.MEASURE_TYPE_CODE, 
  TB_TAXCODE_DETAIL.TAXCODE_COUNTRY_CODE
 FROM ((((TB_TAXCODE_DETAIL INNER JOIN TB_TAXCODE_STATE ON TB_TAXCODE_DETAIL.TAXCODE_STATE_CODE = TB_TAXCODE_STATE.TAXCODE_STATE_CODE AND TB_TAXCODE_DETAIL.TAXCODE_COUNTRY_CODE = TB_TAXCODE_STATE.COUNTRY)
 INNER JOIN TB_TAXCODE ON TB_TAXCODE_DETAIL.TAXCODE_CODE = TB_TAXCODE.TAXCODE_CODE AND TB_TAXCODE_DETAIL.TAXCODE_TYPE_CODE = TB_TAXCODE.TAXCODE_TYPE_CODE)
 INNER JOIN TB_LIST_CODE ON TB_TAXCODE_DETAIL.TAXCODE_TYPE_CODE = TB_LIST_CODE.CODE_CODE)
 LEFT JOIN  TB_LIST_CODE TB_LIST_CODE1 ON TB_LIST_CODE1.CODE_TYPE_CODE = ''TAXTYPE'' AND TB_LIST_CODE1.CODE_CODE = TB_TAXCODE.TAXTYPE_CODE)
 LEFT JOIN  TB_JURISDICTION ON TB_JURISDICTION.JURISDICTION_ID = TB_TAXCODE_DETAIL.JURISDICTION_ID
 WHERE      TB_TAXCODE_DETAIL.ACTIVE_FLAG = ''1''
 AND        TB_LIST_CODE.CODE_TYPE_CODE = ''TCTYPE'' '||
 p_where_clause_token
 ;

 RETURN l_cursor;
END;
/

-- tb_tmp_location_matrix on 05/05/2011
ALTER TABLE tb_tmp_location_matrix ADD override_taxtype_code VARCHAR2(10 BYTE);
ALTER TABLE tb_tmp_location_matrix ADD state_flag CHAR(1 BYTE);
-- ALTER TABLE tb_tmp_location_matrix ADD county_flag CHAR(1 BYTE);
ALTER TABLE tb_tmp_location_matrix ADD county_local_flag CHAR(1 BYTE);
ALTER TABLE tb_tmp_location_matrix ADD city_flag CHAR(1 BYTE);
ALTER TABLE tb_tmp_location_matrix ADD city_local_flag CHAR(1 BYTE);


COMMIT;