create or replace
PROCEDURE sp_batch_taxcode_rules_update(an_batch_id number)
/* ************************************************************************************************/
/* Object Type/Name: SProc - sp_batch_taxcode_rules_update                                        */
/* Author:           Michael B. Fuller                                                            */
/* Date:             08/22/211                                                                    */
/* Description:      Apply a batch of TaxCode Rules Updates                                       */
/* Arguments:        an_batch_id(number)                                                          */
/* Returns:          None                                                                         */
/* ************************************************************************************************/
/* Maintenance History:                                                                           */
/* Scan  Author       Date     Release    Description                                  Ticket #   */
/* ----- ---------- ---------- ---------- -------------------------------------------- ---------- */
/* ************************************************************************************************/
IS
-- Table defined variables
   v_batch_status_code             tb_batch.batch_status_code%TYPE                   := NULL;
   v_error_sev_code                tb_batch.error_sev_code%TYPE                      := ' ';
   v_starting_sequence             tb_batch.start_row%TYPE                           := 0;
   v_process_count                 tb_batch.start_row%TYPE                           := 0;
   v_processed_rows                tb_batch.start_row%TYPE                           := 0;
   v_batch_error_log_id            tb_batch_error_log.batch_error_log_id%TYPE        := 0;
   v_severity_level                tb_list_code.severity_level%TYPE                  := ' ';
   v_write_import_line_flag        tb_list_code.write_import_line_flag%TYPE          := '1';
   v_abort_import_flag             tb_list_code.abort_import_flag%TYPE               := '0';
   v_custom_flag                   tb_taxcode.custom_flag%TYPE                       := NULL;
   v_modified_flag                 tb_taxcode.modified_flag%TYPE                     := NULL;
   v_taxcode_detail_id             tb_taxcode_detail.taxcode_detail_id%TYPE          := NULL;
   v_sysdate                       tb_bcp_transactions.load_timestamp%TYPE           := SYS_EXTRACT_UTC(SYSTIMESTAMP);

-- Program defined variables
   vc_taxcode_rule_update_type     VARCHAR(10)                                       := NULL;
   vi_release_number               INTEGER                                           := 0;
   vd_update_date                  DATE                                              := NULL;
   vi_cursor_id                    INTEGER                                           := 0;
   vi_error_count                  INTEGER                                           := 0;
   vi_count                        INTEGER                                           := 0;

-- Define BCP Jurisdiction TaxRate Updates Cursor (tb_bcp_jurisdiction_taxrate)
   CURSOR bcp_taxcode_rules_cursor
   IS
      SELECT
         bcp_taxcode_rule_id,
         batch_id,
         taxcode_country_code,
         lc_description,
         taxcode_state_code,
         ts_geocode,
         ts_name,
         ts_local_taxability_code,
         ts_active_flag,
         taxcode_code,
         tc_description,
         tc_comments,
         tc_active_flag,
         td_taxcode_county,
         td_taxcode_city,
         td_effective_date,
         td_expiration_date,
         td_taxcode_type_code,
         td_override_taxtype_code,
         td_ratetype_code,
         td_taxable_threshold_amt,
         td_tax_limitation_amt,
         td_cap_amt,
         td_base_change_pct,
         td_special_rate,
         td_active_flag,
         update_code
    FROM tb_bcp_taxcode_rules
   WHERE batch_id = an_batch_id;
   bcp                             bcp_taxcode_rules_cursor%ROWTYPE;

-- Define Exceptions
   e_badread                       EXCEPTION;
   e_badupdate                     EXCEPTION;
   e_badwrite                      EXCEPTION;
   e_wrongdata                     EXCEPTION;
   e_abort                         EXCEPTION;
   e_halt                          EXCEPTION;

-- ****** Program starts ****** --------------------------------------------------------------------
BEGIN
   -- Confirm batch exists and is flagged for processing
   BEGIN
      SELECT batch_status_code
        INTO v_batch_status_code
        FROM tb_batch
       WHERE batch_id = an_batch_id;
      IF SQLCODE != 0 THEN
         RAISE e_badread;
      ELSIF v_batch_status_code != 'FTCR' THEN
         RAISE e_wrongdata;
      END IF;
   EXCEPTION
      WHEN NO_DATA_FOUND OR e_badread THEN
         vi_error_count := vi_error_count + 1;
         sp_geterrorcode('TCR8', an_batch_id, 'TCR', v_sysdate, v_severity_level, v_write_import_line_flag, v_abort_import_flag);
         IF v_severity_level > v_error_sev_code THEN
            v_error_sev_code := v_severity_level;
         END IF;
      WHEN e_wrongdata THEN
         vi_error_count := vi_error_count + 1;
         sp_geterrorcode('TCR9', an_batch_id, 'TCR', v_sysdate, v_severity_level, v_write_import_line_flag, v_abort_import_flag);
         IF v_severity_level > v_error_sev_code THEN
            v_error_sev_code := v_severity_level;
         END IF;
   END;
   -- Abort Procedure?
   IF v_abort_import_flag = '1' THEN
      v_batch_status_code := 'ABTCR';
      RAISE e_abort;
   END IF;

   -- Get starting sequence no.
   BEGIN
      SELECT last_number
        INTO v_starting_sequence
        FROM user_sequences
       WHERE sequence_name = 'SQ_TB_PROCESS_COUNT';
   EXCEPTION
      WHEN OTHERS THEN
         NULL;
   END;

   -- Get batch information
   SELECT UPPER(VC02),
          NU01,
          TS01
     INTO vc_taxcode_rule_update_type,
          vi_release_number,
          vd_update_date
     FROM tb_batch
    WHERE batch_id = an_batch_id;

   -- Update batch as processing
   BEGIN
      UPDATE tb_batch
         SET batch_status_code = 'XTCR',
             error_sev_code = '',
             start_row = v_starting_sequence,
             actual_start_timestamp = v_sysdate
       WHERE batch_id = an_batch_id;
      IF SQLCODE != 0 THEN
         RAISE e_badupdate;
      END IF;
   EXCEPTION
      WHEN e_badupdate THEN
         vi_error_count := vi_error_count + 1;
         sp_geterrorcode('TCR7', an_batch_id, 'TCR', v_sysdate, v_severity_level, v_write_import_line_flag, v_abort_import_flag);
         IF v_severity_level > v_error_sev_code THEN
            v_error_sev_code := v_severity_level;
         END IF;
   END;
   -- Abort Procedure?
   IF v_abort_import_flag = '1' THEN
      v_batch_status_code := 'ABTCR';
      RAISE e_abort;
   END IF;

   COMMIT;

   -- ****** Read and Process Transactions ****** -----------------------------------------
   OPEN bcp_taxcode_rules_cursor;
   LOOP
      FETCH bcp_taxcode_rules_cursor INTO bcp;
      EXIT WHEN bcp_taxcode_rules_cursor%NOTFOUND;

      -- Increment Process Count
      SELECT sq_tb_process_count.NEXTVAL
        INTO v_process_count
        FROM DUAL;
      v_processed_rows := v_processed_rows + 1;

      -- Error Corrections
      IF bcp.td_taxcode_county IS NULL OR Trim(bcp.td_taxcode_county) = '' THEN
         bcp.td_taxcode_county := '*ALL';
      END IF;
      IF bcp.td_taxcode_city IS NULL OR Trim(bcp.td_taxcode_city) = '' THEN
         bcp.td_taxcode_city := '*ALL';
      END IF;

/* *** Update Code = 'CS' - Add/Update County & State *** */
      IF bcp.update_code='CS' THEN
         -- Find Country
         BEGIN
            SELECT count(*)
              INTO vi_count
              FROM tb_list_code
             WHERE code_type_code = 'COUNTRY'
               AND code_code = bcp.taxcode_country_code;
         EXCEPTION
            WHEN OTHERS THEN
               vi_count := 0;
         END;
         -- If it does not exist, add it
         IF vi_count IS NULL OR vi_count = 0 THEN
            INSERT INTO tb_list_code(
               code_type_code,
               code_code,
               description,
               update_user_id,
               update_timestamp)
            VALUES(
               'COUNTRY',
               bcp.taxcode_country_code,
               bcp.lc_description,
               USER,
               v_sysdate);
         ELSE
            -- Update existing Country
            UPDATE tb_list_code SET
               tb_list_code.description = bcp.lc_description,
               tb_list_code.update_user_id = USER,
               tb_list_code.update_timestamp = v_sysdate
             WHERE tb_list_code.code_type_code = 'COUNTRY'
               AND tb_list_code.code_code = bcp.taxcode_country_code;
         END IF;

         -- Find TaxCode State
         vi_count := 1;
         BEGIN
            SELECT tb_taxcode_state.custom_flag,
                   tb_taxcode_state.modified_flag
              INTO v_custom_flag,
                   v_modified_flag
              FROM tb_taxcode_state
             WHERE tb_taxcode_state.taxcode_country_code = bcp.taxcode_country_code
               AND tb_taxcode_state.taxcode_state_code = bcp.taxcode_state_code;
         EXCEPTION
            WHEN OTHERS THEN
               vi_count := 0;
         END;
         -- If it does not exist, add it
         IF vi_count IS NULL OR vi_count = 0 THEN
            INSERT INTO tb_taxcode_state(
               taxcode_country_code,
               taxcode_state_code,
               geocode,
               name,
               local_taxability_code,
               active_flag,
               custom_flag,
               modified_flag,
               update_user_id,
               update_timestamp)
            VALUES(
               bcp.taxcode_country_code,
               bcp.taxcode_state_code,
               bcp.ts_geocode,
               bcp.ts_name,
               bcp.ts_local_taxability_code,
               bcp.ts_active_flag,
               '0',   -- custom flag set to "off"
               '0',   -- modified flag set to "off"
               USER,
               v_sysdate);
         ELSE
            -- If it does exist and is custom then write errog log warning
            IF v_custom_flag = '1' THEN
               v_error_sev_code := '10';
               SELECT sq_tb_batch_error_log_id.NEXTVAL
                 INTO v_batch_error_log_id
                 FROM DUAL;
               INSERT INTO tb_batch_error_log (
                  batch_error_log_id,
                  batch_id,
                  process_type,
                  process_timestamp,
                  row_no,
                  column_no,
                  error_def_code,
                  import_header_column,
                  import_column_value,
                  trans_dtl_column_name,
                  trans_dtl_datatype,
                  import_row)
               VALUES (
                  v_batch_error_log_id,
                  bcp.batch_id,
                  'TCR',
                  v_sysdate,
                  v_processed_rows,
                  0,
                  'TCR5',
                  'Country|State',
                  bcp.taxcode_country_code || '|' || bcp.taxcode_state_code,
                  'n/a',
                  'n/a',
                  'n/a');
            -- Else, If it does exist and is not custom and is modified then write errog log warning
            ELSIF v_custom_flag = '0' AND v_modified_flag = '1' THEN
               v_error_sev_code := '10';
               SELECT sq_tb_batch_error_log_id.NEXTVAL
                 INTO v_batch_error_log_id
                 FROM DUAL;
               INSERT INTO tb_batch_error_log (
                  batch_error_log_id,
                  batch_id,
                  process_type,
                  process_timestamp,
                  row_no,
                  column_no,
                  error_def_code,
                  import_header_column,
                  import_column_value,
                  trans_dtl_column_name,
                  trans_dtl_datatype,
                  import_row)
               VALUES (
                  v_batch_error_log_id,
                  bcp.batch_id,
                  'TCR',
                  v_sysdate,
                  v_processed_rows,
                  0,
                  'TCR6',
                  'Country|State',
                  bcp.taxcode_country_code || '|' || bcp.taxcode_state_code,
                  'n/a',
                  'n/a',
                  'n/a');
            END IF;
            -- Update existing TaxCode State
            UPDATE tb_taxcode_state SET
               tb_taxcode_state.geocode = bcp.ts_geocode,
               tb_taxcode_state.name = bcp.ts_name,
               tb_taxcode_state.local_taxability_code = bcp.ts_local_taxability_code,
               tb_taxcode_state.active_flag = bcp.ts_active_flag,
               tb_taxcode_state.custom_flag = '0',
               tb_taxcode_state.modified_flag = '0',
               tb_taxcode_state.update_user_id = USER,
               tb_taxcode_state.update_timestamp = v_sysdate
             WHERE tb_taxcode_state.taxcode_country_code = bcp.taxcode_country_code
               AND tb_taxcode_state.taxcode_state_code = bcp.taxcode_state_code;
         END IF;

/* *** Update Code = 'UC' and 'UD' - Add/Update TaxCode and Detail *** */
      ELSIF bcp.update_code='UC' OR bcp.update_code='UD' THEN
         -- Update TaxCode for code = UC and UD
         -- Find TaxCode
         vi_count := 1;
         BEGIN
            SELECT tb_taxcode.custom_flag,
                   tb_taxcode.modified_flag
              INTO v_custom_flag,
                   v_modified_flag
              FROM tb_taxcode
             WHERE tb_taxcode.taxcode_code = bcp.taxcode_code;
         EXCEPTION
            WHEN OTHERS THEN
               vi_count := 0;
         END;
         -- If it does not exist, add it
         IF vi_count IS NULL OR vi_count = 0 THEN
            INSERT INTO tb_taxcode(
               taxcode_code,
               description,
               comments,
               active_flag,
               custom_flag,
               modified_flag,
               update_user_id,
               update_timestamp)
            VALUES(
               bcp.taxcode_code,
               bcp.tc_description,
               bcp.tc_comments,
               bcp.tc_active_flag,
               '0',   -- custom flag set to "off"
               '0',   -- modified flag set to "off"
               USER,
               v_sysdate);
         ELSE
            -- If it does exist and is custom then write errog log warning
            IF v_custom_flag = '1' THEN
               v_error_sev_code := '10';
               SELECT sq_tb_batch_error_log_id.NEXTVAL
                 INTO v_batch_error_log_id
                 FROM DUAL;
               INSERT INTO tb_batch_error_log (
                  batch_error_log_id,
                  batch_id,
                  process_type,
                  process_timestamp,
                  row_no,
                  column_no,
                  error_def_code,
                  import_header_column,
                  import_column_value,
                  trans_dtl_column_name,
                  trans_dtl_datatype,
                  import_row)
               VALUES (
                  v_batch_error_log_id,
                  bcp.batch_id,
                  'TCR',
                  v_sysdate,
                  v_processed_rows,
                  0,
                  'TCR1',
                  'Taxcode Code',
                  bcp.taxcode_code,
                  'n/a',
                  'n/a',
                  'n/a');
            -- Else, If it does exist and is not custom and is modified then write errog log warning
            ELSIF (v_custom_flag IS NULL OR v_custom_flag = '0') AND v_modified_flag = '1' THEN
               v_error_sev_code := '10';
               SELECT sq_tb_batch_error_log_id.NEXTVAL
                 INTO v_batch_error_log_id
                 FROM DUAL;
               INSERT INTO tb_batch_error_log (
                  batch_error_log_id,
                  batch_id,
                  process_type,
                  process_timestamp,
                  row_no,
                  column_no,
                  error_def_code,
                  import_header_column,
                  import_column_value,
                  trans_dtl_column_name,
                  trans_dtl_datatype,
                  import_row)
               VALUES (
                  v_batch_error_log_id,
                  bcp.batch_id,
                  'TCR',
                  v_sysdate,
                  v_processed_rows,
                  0,
                  'TCR2',
                  'Taxcode Code',
                  bcp.taxcode_code,
                  'n/a',
                  'n/a',
                  'n/a');
            END IF;
            -- Update existing TaxCode
            UPDATE tb_taxcode SET
               tb_taxcode.description = bcp.tc_description,
               tb_taxcode.comments = bcp.tc_comments,
               tb_taxcode.active_flag = bcp.tc_active_flag,
               tb_taxcode.custom_flag = '0',
               tb_taxcode.modified_flag = '0'
             WHERE tb_taxcode.taxcode_code = bcp.taxcode_code;
         END IF;

         IF bcp.update_code='UD' THEN
            -- Update TaxCode Detail for code = UD
            -- Find TaxCode Detail
            vi_count := 1;
            BEGIN
               SELECT tb_taxcode_detail.taxcode_detail_id,
                      tb_taxcode_detail.custom_flag,
                      tb_taxcode_detail.modified_flag
                 INTO v_taxcode_detail_id,
                      v_custom_flag,
                      v_modified_flag
                 FROM tb_taxcode_detail
                WHERE tb_taxcode_detail.taxcode_code = bcp.taxcode_code
                  AND tb_taxcode_detail.taxcode_country_code = bcp.taxcode_country_code
                  AND tb_taxcode_detail.taxcode_state_code = bcp.taxcode_state_code
                  AND tb_taxcode_detail.taxcode_county = bcp.td_taxcode_county
                  AND tb_taxcode_detail.taxcode_city = bcp.td_taxcode_city
                  AND tb_taxcode_detail.effective_date = bcp.td_effective_date;
            EXCEPTION
               WHEN OTHERS THEN
                  vi_count := 0;
            END;
            -- If it does not exist, add it
            IF vi_count IS NULL OR vi_count = 0 THEN
               SELECT sq_tb_taxcode_detail_id.NEXTVAL
                 INTO v_taxcode_detail_id
                 FROM DUAL;
               INSERT INTO tb_taxcode_detail(
                  taxcode_detail_id,
                  taxcode_code,
                  taxcode_country_code,
                  taxcode_state_code,
                  taxcode_county,
                  taxcode_city,
                  effective_date,
                  expiration_date,
                  taxcode_type_code,
                  override_taxtype_code,
                  ratetype_code,
                  taxable_threshold_amt,
                  tax_limitation_amt,
                  cap_amt,
                  base_change_pct,
                  special_rate,
                  active_flag,
                  custom_flag,
                  modified_flag,
                  update_user_id,
                  update_timestamp)
               VALUES(
                  v_taxcode_detail_id,
                  bcp.taxcode_code,
                  bcp.taxcode_country_code,
                  bcp.taxcode_state_code,
                  bcp.td_taxcode_county,
                  bcp.td_taxcode_city,
                  bcp.td_effective_date,
                  bcp.td_expiration_date,
                  bcp.td_taxcode_type_code,
                  bcp.td_override_taxtype_code,
                  bcp.td_ratetype_code,
                  bcp.td_taxable_threshold_amt,
                  bcp.td_tax_limitation_amt,
                  bcp.td_cap_amt,
                  bcp.td_base_change_pct,
                  bcp.td_special_rate,
                  bcp.td_active_flag,
                  '0',   -- custom flag set to "off"
                  '0',   -- modified flag set to "off"
                  USER,
                  v_sysdate);
            ELSE
               -- If it does exist and is custom then write errog log warning
               IF v_custom_flag = '1' THEN
                  v_error_sev_code := '10';
                  SELECT sq_tb_batch_error_log_id.NEXTVAL
                    INTO v_batch_error_log_id
                    FROM DUAL;
                  INSERT INTO tb_batch_error_log (
                     batch_error_log_id,
                     batch_id,
                     process_type,
                     process_timestamp,
                     row_no,
                     column_no,
                     error_def_code,
                     import_header_column,
                     import_column_value,
                     trans_dtl_column_name,
                     trans_dtl_datatype,
                     import_row)
                  VALUES (
                     v_batch_error_log_id,
                     bcp.batch_id,
                     'TCR',
                     v_sysdate,
                     v_processed_rows,
                     0,
                     'TCR3',
                     'TaxCode Code|Country|State|County|City|Eff.Date',
                     bcp.taxcode_code || '|' || bcp.taxcode_country_code || '|' || bcp.taxcode_state_code || '|' || bcp.td_taxcode_county || '|' || bcp.td_taxcode_city || '|' || bcp.td_effective_date,
                     'n/a',
                     'n/a',
                     'n/a');
               -- Else, If it does exist and is not custom and is modified then write errog log warning
               ELSIF (v_custom_flag IS NULL OR v_custom_flag = '0') AND v_modified_flag = '1' THEN
                  v_error_sev_code := '10';
                  SELECT sq_tb_batch_error_log_id.NEXTVAL
                    INTO v_batch_error_log_id
                    FROM DUAL;
                  INSERT INTO tb_batch_error_log (
                     batch_error_log_id,
                     batch_id,
                     process_type,
                     process_timestamp,
                     row_no,
                     column_no,
                     error_def_code,
                     import_header_column,
                     import_column_value,
                     trans_dtl_column_name,
                     trans_dtl_datatype,
                     import_row)
                  VALUES (
                     v_batch_error_log_id,
                     bcp.batch_id,
                     'TCR',
                     v_sysdate,
                     v_processed_rows,
                     0,
                     'TCR4',
                     'TaxCode Code|Country|State|County|City|Eff.Date',
                     bcp.taxcode_code || '|' || bcp.taxcode_country_code || '|' || bcp.taxcode_state_code || '|' || bcp.td_taxcode_county || '|' || bcp.td_taxcode_city || '|' || bcp.td_effective_date,
                     'n/a',
                     'n/a',
                     'n/a');
               END IF;
               -- Update existing TaxCode Detail
               UPDATE tb_taxcode_detail SET
                  tb_taxcode_detail.expiration_date = bcp.td_expiration_date,
                  tb_taxcode_detail.taxcode_type_code = bcp.td_taxcode_type_code,
                  tb_taxcode_detail.override_taxtype_code = bcp.td_override_taxtype_code,
                  tb_taxcode_detail.ratetype_code = bcp.td_ratetype_code,
                  tb_taxcode_detail.taxable_threshold_amt = bcp.td_taxable_threshold_amt,
                  tb_taxcode_detail.tax_limitation_amt = bcp.td_tax_limitation_amt,
                  tb_taxcode_detail.cap_amt = bcp.td_cap_amt,
                  tb_taxcode_detail.base_change_pct = bcp.td_base_change_pct,
                  tb_taxcode_detail.special_rate = bcp.td_special_rate,
                  tb_taxcode_detail.active_flag = bcp.td_active_flag,
                  tb_taxcode_detail.custom_flag = '0',
                  tb_taxcode_detail.modified_flag = '0',
                  tb_taxcode_detail.update_user_id = USER,
                  tb_taxcode_detail.update_timestamp = v_sysdate
                WHERE tb_taxcode_detail.taxcode_detail_id = v_taxcode_detail_id;
            END IF;
         END IF;

/* *** Update Code = 'DC' - Delete TaxCode *** */
      ELSIF bcp.update_code='DC' THEN
         -- Find TaxCode
         vi_count := 1;
         BEGIN
            SELECT count(*)
              INTO vi_count
              FROM tb_taxcode
             WHERE tb_taxcode.taxcode_code = bcp.taxcode_code;
         EXCEPTION
            WHEN OTHERS THEN
               vi_count := 0;
         END;
         -- If it does exist, delete it
         IF vi_count > 0 THEN
            -- check if taxcode is used in tax matrix
            vi_count := 0;
            BEGIN
               SELECT count(*)
                 INTO vi_count
                 FROM tb_tax_matrix
                WHERE tb_tax_matrix.then_taxcode_code = bcp.taxcode_code
                   OR tb_tax_matrix.else_taxcode_code = bcp.taxcode_code;
            EXCEPTION
               WHEN OTHERS THEN
                  vi_count := 0;
            END;
            -- if not used, continue to delete
            IF vi_count IS NULL OR vi_count = 0 THEN
               DELETE FROM tb_taxcode_detail
                     WHERE tb_taxcode_detail.taxcode_code = bcp.taxcode_code;
               DELETE FROM tb_taxcode
                     WHERE tb_taxcode.taxcode_code = bcp.taxcode_code;
            ELSE
               -- else, write warning to error log & mark as inactive
               v_error_sev_code := '10';
               SELECT sq_tb_batch_error_log_id.NEXTVAL
                 INTO v_batch_error_log_id
                 FROM DUAL;
               INSERT INTO tb_batch_error_log (
                  batch_error_log_id,
                  batch_id,
                  process_type,
                  process_timestamp,
                  row_no,
                  column_no,
                  error_def_code,
                  import_header_column,
                  import_column_value,
                  trans_dtl_column_name,
                  trans_dtl_datatype,
                  import_row)
               VALUES (
                  v_batch_error_log_id,
                  bcp.batch_id,
                  'TCR',
                  v_sysdate,
                  v_processed_rows,
                  0,
                  'TCR10',
                  'TaxCode Code',
                  bcp.taxcode_code,
                  'n/a',
                  'n/a',
                  'n/a');
               UPDATE tb_taxcode_detail
                  SET tb_taxcode_detail.active_flag = '0'
                WHERE tb_taxcode_detail.taxcode_code = bcp.taxcode_code;
               UPDATE tb_taxcode
                  SET tb_taxcode.active_flag = '0'
                WHERE tb_taxcode.taxcode_code = bcp.taxcode_code;
            END IF;
         ELSE
            -- else, if it doesn't exist write error log warning
            v_error_sev_code := '10';
            SELECT sq_tb_batch_error_log_id.NEXTVAL
              INTO v_batch_error_log_id
              FROM DUAL;
            INSERT INTO tb_batch_error_log (
               batch_error_log_id,
               batch_id,
               process_type,
               process_timestamp,
               row_no,
               column_no,
               error_def_code,
               import_header_column,
               import_column_value,
               trans_dtl_column_name,
               trans_dtl_datatype,
               import_row )
            VALUES (
               v_batch_error_log_id,
               bcp.batch_id,
               'TCR',
               v_sysdate,
               v_processed_rows,
               0,
               'TCR11',
               'TaxCode Code',
               bcp.taxcode_code,
               'n/a',
               'n/a',
               'n/a');
         END IF;

/* *** Update Code = 'DD' - Delete TaxCode Detail *** */
      ELSIF bcp.update_code='DD' THEN
         -- Find TaxCode Detail
         vi_count := 1;
         BEGIN
            SELECT taxcode_detail_id
              INTO v_taxcode_detail_id
              FROM tb_taxcode_detail
             WHERE tb_taxcode_detail.taxcode_code = bcp.taxcode_code
               AND tb_taxcode_detail.taxcode_country_code = bcp.taxcode_country_code
               AND tb_taxcode_detail.taxcode_state_code = bcp.taxcode_state_code
               AND tb_taxcode_detail.taxcode_county = bcp.td_taxcode_county
               AND tb_taxcode_detail.taxcode_city = bcp.td_taxcode_city
               AND tb_taxcode_detail.effective_date = bcp.td_effective_date;
         EXCEPTION
            WHEN OTHERS THEN
               vi_count := 0;
         END;
         -- If it does exist, delete it
         IF vi_count > 0 THEN
            -- check if taxcode detail is used in transaction detail
            BEGIN
               SELECT count(*)
                 INTO vi_count
                 FROM tb_transaction_detail
                WHERE tb_transaction_detail.state_taxcode_detail_id = v_taxcode_detail_id
                   OR tb_transaction_detail.county_taxcode_detail_id = v_taxcode_detail_id
                   OR tb_transaction_detail.city_taxcode_detail_id = v_taxcode_detail_id;
            EXCEPTION
               WHEN OTHERS THEN
                  vi_count := 0;
            END;
            -- if not used, continue to delete
            IF vi_count IS NULL OR vi_count = 0 THEN
               DELETE FROM tb_taxcode_detail
                     WHERE tb_taxcode_detail.taxcode_detail_id = v_taxcode_detail_id;
            ELSE
               -- else, write warning to error log & mark as inactive
               v_error_sev_code := '10';
               SELECT sq_tb_batch_error_log_id.NEXTVAL
                 INTO v_batch_error_log_id
                 FROM DUAL;
               INSERT INTO tb_batch_error_log (
                  batch_error_log_id,
                  batch_id,
                  process_type,
                  process_timestamp,
                  row_no,
                  column_no,
                  error_def_code,
                  import_header_column,
                  import_column_value,
                  trans_dtl_column_name,
                  trans_dtl_datatype,
                  import_row)
               VALUES (
                  v_batch_error_log_id,
                  bcp.batch_id,
                  'TCR',
                  v_sysdate,
                  v_processed_rows,
                  0,
                  'TCR12',
                  'TaxCode Code|Country|State|County|City|Eff.Date',
                  bcp.taxcode_code || '|' || bcp.taxcode_country_code || '|' || bcp.taxcode_state_code || '|' || bcp.td_taxcode_county || '|' || bcp.td_taxcode_city || '|' || bcp.td_effective_date,
                  'n/a',
                  'n/a',
                  'n/a');
               UPDATE tb_taxcode_detail
                  SET tb_taxcode_detail.active_flag = '0'
                WHERE tb_taxcode_detail.taxcode_detail_id = v_taxcode_detail_id;
            END IF;
         ELSE
            -- else, if it doesn't exist write error log warning
            v_error_sev_code := '10';
            SELECT sq_tb_batch_error_log_id.NEXTVAL
              INTO v_batch_error_log_id
              FROM DUAL;
            INSERT INTO tb_batch_error_log (
               batch_error_log_id,
               batch_id,
               process_type,
               process_timestamp,
               row_no,
               column_no,
               error_def_code,
               import_header_column,
               import_column_value,
               trans_dtl_column_name,
               trans_dtl_datatype,
               import_row )
            VALUES (
               v_batch_error_log_id,
               bcp.batch_id,
               'TCR',
               v_sysdate,
               v_processed_rows,
               0,
               'TCR13',
               'TaxCode Code|Country|State|County|City|Eff.Date',
               bcp.taxcode_code || '|' || bcp.taxcode_country_code || '|' || bcp.taxcode_state_code || '|' || bcp.td_taxcode_county || '|' || bcp.td_taxcode_city || '|' || bcp.td_effective_date,
               'n/a',
               'n/a',
               'n/a');
         END IF;

/* *** Update Code not recognized *** */
      ELSE
         v_error_sev_code := '10';
         SELECT sq_tb_batch_error_log_id.NEXTVAL
           INTO v_batch_error_log_id
           FROM DUAL;
         INSERT INTO tb_batch_error_log (
            batch_error_log_id,
            batch_id,
            process_type,
            process_timestamp,
            row_no,
            column_no,
            error_def_code,
            import_header_column,
            import_column_value,
            trans_dtl_column_name,
            trans_dtl_datatype,
            import_row )
         VALUES (
            v_batch_error_log_id,
            bcp.batch_id,
            'TCR',
            v_sysdate,
            v_processed_rows,
            0,
            'TCR22',
            'Update Code',
            bcp.update_code,
            'n/a',
            'n/a',
            'n/a');
      END IF;
   END LOOP;
   CLOSE bcp_taxcode_rules_cursor;

   -- Remove Batch from BCP table
/*   DELETE
     FROM tb_bcp_taxcode_rules
    WHERE batch_id = an_batch_id;   */

   --  Update Options table with last month/year updated
   UPDATE tb_option
      SET value = TO_CHAR(vd_update_date, 'yyyy_mm')
    WHERE option_code = 'LASTTAXCODERULEDATE' AND
          option_type_code = 'SYSTEM' AND
          user_code = 'SYSTEM';

   -- Update Options table with last release number
   UPDATE tb_option
      SET value = vi_release_number
    WHERE option_code = 'LASTTAXCODERULEREL' AND
          option_type_code = 'SYSTEM' AND
          user_code = 'SYSTEM';

   -- Update batch with final totals
   UPDATE tb_batch
      SET batch_status_code = 'TCR',
          error_sev_code = v_error_sev_code,
          processed_rows = v_processed_rows,
          actual_end_timestamp = SYS_EXTRACT_UTC(SYSTIMESTAMP)
    WHERE batch_id = an_batch_id;

   -- commit all updates
   COMMIT;

EXCEPTION
   WHEN e_abort THEN
      -- Update batch with error codes
      UPDATE tb_batch
         SET batch_status_code = v_batch_status_code,
             error_sev_code = v_error_sev_code,
             actual_end_timestamp = SYS_EXTRACT_UTC(SYSTIMESTAMP)
       WHERE batch_id = an_batch_id;
      COMMIT;
   WHEN e_halt THEN
      NULL;
END;
