CREATE OR REPLACE function get_tax_matrix (
    p_transaction_detail_id IN tb_transaction_detail.transaction_detail_id%TYPE
)
return varchar2
is 
   vc_tax_matrix_where             VARCHAR2(4000) := '';
   tmp                             VARCHAR2(4000) := '';
   vc_state_driver_flag            VARCHAR(10) := '0';
begin
    sp_gen_tax_driver (p_generate_driver_reference    => 'N',
      p_an_batch_id            => NULL,
      p_transaction_table_name    => 'trn',
      p_tax_table_name        => 'mat',
      p_vc_state_driver_flag        => vc_state_driver_flag,
      p_vc_tax_matrix_where        => vc_tax_matrix_where);   

  IF vc_tax_matrix_where IS NULL THEN
    vc_tax_matrix_where := '';
  END IF;

  return vc_tax_matrix_where;
end get_tax_matrix;
/

