=======================================================================
     			PinPoint v5.1.5.8
=======================================================================

The following issues have been resolved in this build:

0001316: Crash when adding reference document if file name is too long
0001102: Deleting a State checked on a TaxCode crashes.
0000069: Modify Rates and PinPoint to handle new measure types

------------------------------------------------------
-- Upgrade Scripts for v5.1.5.8
------------------------------------------------------
Alter_v5_1_5_8.sql
ALTER_Sp_Batch_Taxrate_Update_1389.sql
alter_tb_bcp_jursidiction_taxrate_1389.sql
alter_tb_jursidiction_1389.sql