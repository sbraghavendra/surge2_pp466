create or replace
PROCEDURE sp_tb_location_matrix_a_i(
   p_location_matrix_id        IN     tb_location_matrix.location_matrix_id%TYPE)
/* ************************************************************************************************/
/* Object Type/Name: Stored Procedure (After Insert) - sp_tb_location_matrix_a_i                  */
/* Author:           Michael B. Fuller                                                            */
/* Date:             a long time ago                                                              */
/* Description:      A Location Matrix line has been added                                        */
/* Arguments:        p_location_matrix_id(tb_location_matrix.location_matrix_id%TYPE)             */
/* Returns:          None                                                                         */
/* ************************************************************************************************/
/* Maintenance History:                                                                           */
/* Scan  Author       Date     Release    Description                                  Ticket #   */
/* ----- ---------- ---------- ---------- -------------------------------------------- ---------- */
/*       M. Fuller  06/02/2006 3.3.5.0    Fix processing an allocated line - Temporary 871        */
/*       M. Fuller  01/04/2007 3.3.5.1    Fix processing an allocated line - Permenant 871        */
/* MBF01 M. Fuller  08/14/2007 3.4.1.1    Add 5 Calc Tax Flags                         883        */
/*       J. Franco  12/16/2008 x.x.x.x    Convert Trigger to SP                        xxx        */
/*       J. Marks   01/18/2010 x.x.x.x    Set effective date to earliest gl date       3921       */
/* MBF02 M. Fuller  01/29/2010 5.1.1.1    Remove effective date code                   3921       */
/* MBF03 M. Fuller  08/30/2011            Add TaxCode Rules changes                    17         */
/* ************************************************************************************************/
AS
-- Define Row Type for tb_location_matrix record
   l_tb_location_matrix            tb_location_matrix%ROWTYPE;

-- Define Cursor Type
   TYPE cursor_type IS REF CURSOR;

-- Transaction Detail Selection SQL
   vc_transaction_detail_select    VARCHAR2(2000) :=
      'SELECT tb_transaction_detail.* ' ||
        'FROM tb_tmp_location_matrix, ' ||
             'tb_transaction_detail '   ||
       'WHERE ( tb_tmp_location_matrix.location_matrix_id = ' || p_location_matrix_id || ') AND ' ||
             '( tb_tmp_location_matrix.effective_date <= tb_transaction_detail.gl_date ) AND ( tb_tmp_location_matrix.expiration_date >= tb_transaction_detail.gl_date ) AND ' ||
             '( tb_transaction_detail.transaction_ind = ''S'' ) AND ( tb_transaction_detail.suspend_ind = ''L'' ) ';
   vc_transaction_detail_where     VARCHAR2(28000) := '';
   vc_transaction_detail_update    VARCHAR2(100) := 'FOR UPDATE';
   vc_transaction_detail_stmt      VARCHAR2(30100);
   transaction_detail_cursor       cursor_type;
   transaction_detail              tb_transaction_detail%ROWTYPE;

-- Tax Matrix Selection SQL
   vc_tax_matrix_select            VARCHAR2(2000) :=
      'SELECT tb_tax_matrix.tax_matrix_id, tb_tax_matrix.relation_sign, tb_tax_matrix.relation_amount, ' ||
             'tb_tax_matrix.then_hold_code_flag, tb_tax_matrix.else_hold_code_flag, ' ||
             'tb_tax_matrix.then_taxcode_code, tb_tax_matrix.else_taxcode_code ' ||
       'FROM tb_tax_matrix, ' ||
            'tb_transaction_detail ' ||
      'WHERE ( tb_transaction_detail.transaction_detail_id = :transaction_detail_id ) AND ' ||
            '( tb_tax_matrix.default_flag = ''0'' AND tb_tax_matrix.binary_weight > 0 ) AND ' ||
            '( tb_tax_matrix.effective_date <= tb_transaction_detail.gl_date ) AND ( tb_tax_matrix.expiration_date >= tb_transaction_detail.gl_date )';
   vc_tax_matrix_where             VARCHAR2(28000) := '';
   vc_tax_matrix_orderby           VARCHAR2(1000) :=
      'ORDER BY tb_tax_matrix.binary_weight DESC, tb_tax_matrix.significant_digits DESC, tb_tax_matrix.effective_date DESC';
   vc_tax_matrix_stmt              VARCHAR2 (31000);
   tax_matrix_cursor               cursor_type;

-- Tax Matrix Record TYPE
   TYPE tax_matrix_record is RECORD (
      tax_matrix_id                tb_tax_matrix.tax_matrix_id%TYPE,
      relation_sign                tb_tax_matrix.relation_sign%TYPE,
      relation_amount              tb_tax_matrix.relation_amount%TYPE,
      then_hold_code_flag          tb_tax_matrix.then_hold_code_flag%TYPE,
      else_hold_code_flag          tb_tax_matrix.else_hold_code_flag%TYPE,
      then_taxcode_code            tb_tax_matrix.then_taxcode_code%TYPE,
      else_taxcode_code            tb_tax_matrix.else_taxcode_code%TYPE);
   tax_matrix                      tax_matrix_record;
   --tax_matrix                      Matrix_Record_Pkg.tax_matrix_record;

-- TaxCode Detail Selection SQL
   vc_taxcode_detail_stmt           VARCHAR2(31000) :=
      'SELECT taxcode_detail_id, taxcode_type_code, override_taxtype_code, ratetype_code, taxable_threshold_amt, ' ||
             'tax_limitation_amt, cap_amt, base_change_pct, special_rate ' ||
        'FROM tb_taxcode_detail ' ||
       'WHERE tb_taxcode_detail.active_flag = ''1'' ' ||
         'AND tb_taxcode_detail.taxcode_code = :taxcode_code ' ||
         'AND tb_taxcode_detail.taxcode_country_code = :transaction_country_code ' ||
         'AND tb_taxcode_detail.taxcode_state_code = :transaction_state_code ' ||
         'AND tb_taxcode_detail.taxcode_county = :taxcode_county ' ||
         'AND tb_taxcode_detail.taxcode_city = :taxcode_city ' ||
         'AND tb_taxcode_detail.effective_date <= :gl_date ' ||
         'AND tb_taxcode_detail.expiration_date >= :gl_date ' ||
      'ORDER BY tb_taxcode_detail.effective_date DESC';
   taxcode_detail_cursor           cursor_type;

-- TaxCode Detail Record TYPE
   TYPE taxcode_detail_record is RECORD (
      taxcode_detail_id            tb_taxcode_detail.taxcode_detail_id%TYPE,
      taxcode_type_code            tb_taxcode_detail.taxcode_type_code%TYPE,
      override_taxtype_code        tb_taxcode_detail.override_taxtype_code%TYPE,
      ratetype_code                tb_taxcode_detail.ratetype_code%TYPE,
      taxable_threshold_amt        tb_taxcode_detail.taxable_threshold_amt%TYPE,
      tax_limitation_amt           tb_taxcode_detail.tax_limitation_amt%TYPE,
      cap_amt                      tb_taxcode_detail.cap_amt%TYPE,
      base_change_pct              tb_taxcode_detail.base_change_pct%TYPE,
      special_rate                 tb_taxcode_detail.special_rate%TYPE);
   taxcode_detail                  taxcode_detail_record;

-- Table defined variables
   v_transaction_county            tb_taxcode_detail.taxcode_county%TYPE             := NULL;
   v_transaction_city              tb_taxcode_detail.taxcode_city%TYPE               := NULL;
   v_less_code                     tb_taxcode_detail.taxcode_type_code%TYPE          := NULL;
   v_equal_code                    tb_taxcode_detail.taxcode_type_code%TYPE          := NULL;
   v_greater_code                  tb_taxcode_detail.taxcode_type_code%TYPE          := NULL;
   v_relation_code                 tb_taxcode_detail.taxcode_type_code%TYPE          := NULL;
   v_relation_amount               TB_TAX_MATRIX.relation_amount%TYPE                := NULL;
   v_hold_code_flag                tb_tax_matrix.then_hold_code_flag%TYPE            := NULL;
   v_state_override_taxtype_code   tb_taxcode_detail.override_taxtype_code%TYPE      := NULL;
   v_state_ratetype_code           tb_taxcode_detail.ratetype_code%TYPE              := NULL;
   v_state_taxable_threshold_amt   tb_taxcode_detail.taxable_threshold_amt%TYPE      := 0;
   v_county_taxable_threshold_amt  tb_taxcode_detail.taxable_threshold_amt%TYPE      := 0;
   v_city_taxable_threshold_amt    tb_taxcode_detail.taxable_threshold_amt%TYPE      := 0;
   v_state_tax_limitation_amt      tb_taxcode_detail.tax_limitation_amt%TYPE         := 0;
   v_county_tax_limitation_amt     tb_taxcode_detail.tax_limitation_amt%TYPE         := 0;
   v_city_tax_limitation_amt       tb_taxcode_detail.tax_limitation_amt%TYPE         := 0;
   v_state_cap_amt                 tb_taxcode_detail.cap_amt%TYPE                    := 0;
   v_county_cap_amt                tb_taxcode_detail.cap_amt%TYPE                    := 0;
   v_city_cap_amt                  tb_taxcode_detail.cap_amt%TYPE                    := 0;
   v_state_base_change_pct         tb_taxcode_detail.base_change_pct%TYPE            := 0;
   v_county_base_change_pct        tb_taxcode_detail.base_change_pct%TYPE            := 0;
   v_city_base_change_pct          tb_taxcode_detail.base_change_pct%TYPE            := 0;
   v_state_special_rate            tb_taxcode_detail.special_rate%TYPE               := 0;
   v_county_special_rate           tb_taxcode_detail.special_rate%TYPE               := 0;
   v_city_special_rate             tb_taxcode_detail.special_rate%TYPE               := 0;
   --v_override_jurisdiction_id      tb_taxcode_detail.jurisdiction_id%TYPE            := 0;
   v_sysdate                       TB_TRANSACTION_DETAIL.load_timestamp%TYPE         := SYS_EXTRACT_UTC(SYSTIMESTAMP);

-- Program defined variables
   vc_state_driver_flag            CHAR(1)                                           := NULL;
   vn_fetch_rows                   NUMBER                                            := 0;
   vn_state_rows                   NUMBER                                            := 0;
   vn_county_rows                  NUMBER                                            := 0;
   vn_city_rows                    NUMBER                                            := 0;
   vc_country_flag                 CHAR(1)                                           := '0';
   vc_state_flag                   CHAR(1)                                           := '0';
   vc_county_flag                  CHAR(1)                                           := '0';
   vc_county_local_flag            CHAR(1)                                           := '0';
   vc_city_flag                    CHAR(1)                                           := '0';
   vc_city_local_flag              CHAR(1)                                           := '0';

-- Define Exceptions
   e_badread                       EXCEPTION;
   e_badupdate                     EXCEPTION;

-- ****** Program starts ****** --------------------------------------------------------------------
BEGIN
   -- Populate row type with location matrix record
   SELECT tb_location_matrix.*
     INTO l_tb_location_matrix
     FROM tb_location_matrix
    WHERE location_matrix_id = p_location_matrix_id;

   -- Only process non-default location matrix lines
   IF l_tb_location_matrix.default_flag <> '1' AND l_tb_location_matrix.binary_weight > 0 THEN
      -- New call to sp_gen_location_driver for MidTier Cleanup
      sp_gen_location_driver (
         p_generate_driver_reference	=> 'N',
         p_an_batch_id			=> NULL,
         p_transaction_table_name	=> 'tb_transaction_detail',
         p_location_table_name		=> 'tb_tmp_location_matrix',
         p_vc_location_matrix_where	=> vc_transaction_detail_where);

      -- If no drivers found raise error, else create transaction detail sql statement
      IF vc_transaction_detail_where IS NULL THEN
         NULL;
      ELSE
         vc_transaction_detail_stmt := vc_transaction_detail_select || vc_transaction_detail_where || vc_transaction_detail_update;
      END IF;

      -- New call to sp_gen_tax_driver for MidTier Cleanup
      sp_gen_tax_driver (
         p_generate_driver_reference	=> 'N',
         p_an_batch_id			=> NULL,
         p_transaction_table_name	=> 'tb_transaction_detail',
         p_tax_table_name		=> 'tb_tax_matrix',
         p_vc_state_driver_flag		=> vc_state_driver_flag,
         p_vc_tax_matrix_where		=> vc_tax_matrix_where);

      -- If no drivers found raise error, else create location matrix sql statement
      IF vc_tax_matrix_where IS NULL THEN
         NULL;
      ELSE
         vc_tax_matrix_stmt := vc_tax_matrix_select || vc_tax_matrix_where || vc_tax_matrix_orderby;
      END IF;

      -- Insert New Location Matrix columns into temporary table --------------------------------------
      INSERT INTO tb_tmp_location_matrix (
         location_matrix_id,
         driver_01, driver_01_desc,
         driver_02, driver_02_desc,
         driver_03, driver_03_desc,
         driver_04, driver_04_desc,
         driver_05, driver_05_desc,
         driver_06, driver_06_desc,
         driver_07, driver_07_desc,
         driver_08, driver_08_desc,
         driver_09, driver_09_desc,
         driver_10, driver_10_desc,
         binary_weight, significant_digits,
         default_flag, default_binary_weight, default_significant_digits,
         effective_date, expiration_date,
         jurisdiction_id, state, override_taxtype_code,
         state_flag, county_flag, county_local_flag, city_flag, city_local_flag,
         comments, last_used_timestamp,
         update_user_id, update_timestamp,
         country, country_flag)
      VALUES (
         l_tb_location_matrix.location_matrix_id,
         l_tb_location_matrix.driver_01, l_tb_location_matrix.driver_01_desc,
         l_tb_location_matrix.driver_02, l_tb_location_matrix.driver_02_desc,
         l_tb_location_matrix.driver_03, l_tb_location_matrix.driver_03_desc,
         l_tb_location_matrix.driver_04, l_tb_location_matrix.driver_04_desc,
         l_tb_location_matrix.driver_05, l_tb_location_matrix.driver_05_desc,
         l_tb_location_matrix.driver_06, l_tb_location_matrix.driver_06_desc,
         l_tb_location_matrix.driver_07, l_tb_location_matrix.driver_07_desc,
         l_tb_location_matrix.driver_08, l_tb_location_matrix.driver_08_desc,
         l_tb_location_matrix.driver_09, l_tb_location_matrix.driver_09_desc,
         l_tb_location_matrix.driver_10, l_tb_location_matrix.driver_10_desc,
         l_tb_location_matrix.binary_weight, l_tb_location_matrix.significant_digits,
         l_tb_location_matrix.default_flag, l_tb_location_matrix.default_binary_weight, l_tb_location_matrix.default_significant_digits,
         l_tb_location_matrix.effective_date, l_tb_location_matrix.expiration_date,
         l_tb_location_matrix.jurisdiction_id, l_tb_location_matrix.state, l_tb_location_matrix.override_taxtype_code,
         l_tb_location_matrix.state_flag, l_tb_location_matrix.county_flag, l_tb_location_matrix.county_local_flag, l_tb_location_matrix.city_flag, l_tb_location_matrix.city_local_flag,
         l_tb_location_matrix.comments, l_tb_location_matrix.last_used_timestamp,
         l_tb_location_matrix.update_user_id, l_tb_location_matrix.update_timestamp,
         l_tb_location_matrix.country, l_tb_location_matrix.country_flag);

      -- ****** Read and Process Transactions ****** -----------------------------------------
      OPEN transaction_detail_cursor
       FOR vc_transaction_detail_stmt;
      LOOP
         FETCH transaction_detail_cursor
          INTO transaction_detail;
          EXIT WHEN transaction_detail_cursor%NOTFOUND;

         -- Populate and/or Transaction Detail Clear working fields
         transaction_detail.transaction_ind := NULL;
         transaction_detail.suspend_ind := NULL;
         transaction_detail.location_matrix_id := l_tb_location_matrix.location_matrix_id;
         transaction_detail.jurisdiction_id := l_tb_location_matrix.jurisdiction_id;
         transaction_detail.update_user_id := USER;
         transaction_detail.update_timestamp := v_sysdate;

         -- Get jurisdiction info
         BEGIN
            SELECT country, state, county, city
              INTO transaction_detail.transaction_country_code,
                   transaction_detail.transaction_state_code,
                   v_transaction_county,
                   v_transaction_city
              FROM tb_jurisdiction
             WHERE tb_jurisdiction.jurisdiction_id = l_tb_location_matrix.jurisdiction_id;
         END;

         -- Search Tax Matrix for matches
         BEGIN
            IF vc_state_driver_flag = '1' THEN
                OPEN tax_matrix_cursor
                 FOR vc_tax_matrix_stmt
               USING transaction_detail.transaction_detail_id, transaction_detail.transaction_state_code;
               FETCH tax_matrix_cursor
                INTO tax_matrix;
            ELSE
                OPEN tax_matrix_cursor
                 FOR vc_tax_matrix_stmt
               USING transaction_detail.transaction_detail_id;
               FETCH tax_matrix_cursor
                INTO tax_matrix;
            END IF;
            -- Rows found?
            IF tax_matrix_cursor%FOUND THEN
               vn_fetch_rows := tax_matrix_cursor%ROWCOUNT;
            ELSE
               vn_fetch_rows := 0;
            END IF;
            CLOSE tax_matrix_cursor;
            IF SQLCODE != 0 THEN
               vn_fetch_rows := 0;
            END IF;
         END;

         -- Tax Matrix Line Found
         IF vn_fetch_rows > 0 THEN
            transaction_detail.tax_matrix_id := tax_matrix.tax_matrix_id;
            -- Determine "Then" or "Else" Results
            v_relation_amount := tax_matrix.relation_amount;
            IF v_relation_amount IS NULL THEN
               v_relation_amount := 0;
            END IF;
            SELECT DECODE(TRIM(tax_matrix.relation_sign),'na','then','=','else','<','then','<=','then','>','else','>=','else','<>','then','error') INTO v_less_code FROM DUAL;
            SELECT DECODE(TRIM(tax_matrix.relation_sign),'na','then','=','then','<','else','<=','then','>','else','>=','then','<>','else','error') INTO v_equal_code FROM DUAL;
            SELECT DECODE(TRIM(tax_matrix.relation_sign),'na','then','=','else','<','else','<=','else','>','then','>=','then','<>','then','error') INTO v_greater_code FROM DUAL;
            SELECT DECODE(SIGN(transaction_detail.gl_line_itm_dist_amt - NVL(v_relation_amount,0)), -1, v_less_code, 0, v_equal_code, 1, v_greater_code, 'error') INTO v_relation_code FROM DUAL;
            IF v_relation_code = 'then' THEN
               v_hold_code_flag := tax_matrix.then_hold_code_flag;
               transaction_detail.taxcode_code := tax_matrix.then_taxcode_code;
               --v_override_jurisdiction_id := v_then_jurisdiction_id;
            ELSIF v_relation_code = 'else' THEN
               v_hold_code_flag := tax_matrix.else_hold_code_flag;
               transaction_detail.taxcode_code := tax_matrix.else_taxcode_code;
               --v_override_jurisdiction_id := v_else_jurisdiction_id;
            ELSE
               null;
            END IF;

            -- ****** Get TaxCode Details ****** --
            transaction_detail.state_taxcode_type_code := NULL;
            transaction_detail.county_taxcode_type_code := NULL;
            transaction_detail.city_taxcode_type_code := NULL;
            -- Get Country/State/all County/all City for defaults
            vn_state_rows := 0;
            BEGIN
                OPEN taxcode_detail_cursor
                 FOR vc_taxcode_detail_stmt
               USING transaction_detail.taxcode_code, transaction_detail.transaction_country_code, transaction_detail.transaction_state_code, '*ALL', '*ALL', transaction_detail.gl_date, transaction_detail.gl_date;
               FETCH taxcode_detail_cursor
                INTO taxcode_detail;
               -- Rows found?
               IF taxcode_detail_cursor%FOUND THEN
                  vn_state_rows := taxcode_detail_cursor%ROWCOUNT;
               ELSE
                  vn_state_rows := 0;
               END IF;
               CLOSE taxcode_detail_cursor;
               IF SQLCODE != 0 THEN
                  vn_state_rows := 0;
               END IF;
            END;
            IF vn_state_rows > 0 THEN
               transaction_detail.state_taxcode_detail_id := taxcode_detail.taxcode_detail_id;
               transaction_detail.state_taxcode_type_code := taxcode_detail.taxcode_type_code;
               v_state_override_taxtype_code := taxcode_detail.override_taxtype_code;
               v_state_ratetype_code := taxcode_detail.ratetype_code;
               v_state_taxable_threshold_amt := taxcode_detail.taxable_threshold_amt;
               v_state_tax_limitation_amt := taxcode_detail.tax_limitation_amt;
               v_state_cap_amt := taxcode_detail.cap_amt;
               v_state_base_change_pct := taxcode_detail.base_change_pct;
               v_state_special_rate := taxcode_detail.special_rate;
               transaction_detail.county_taxcode_type_code := taxcode_detail.taxcode_type_code;
               v_county_taxable_threshold_amt := taxcode_detail.taxable_threshold_amt;
               v_county_tax_limitation_amt := taxcode_detail.tax_limitation_amt;
               v_county_cap_amt := taxcode_detail.cap_amt;
               v_county_base_change_pct := taxcode_detail.base_change_pct;
               v_county_special_rate := taxcode_detail.special_rate;
               transaction_detail.city_taxcode_type_code := taxcode_detail.taxcode_type_code;
               v_city_taxable_threshold_amt := taxcode_detail.taxable_threshold_amt;
               v_city_tax_limitation_amt := taxcode_detail.tax_limitation_amt;
               v_city_cap_amt := taxcode_detail.cap_amt;
               v_city_base_change_pct := taxcode_detail.base_change_pct;
               v_city_special_rate := taxcode_detail.special_rate;
               -- Get Country/State/County/all City for County
               vn_county_rows := 0;
               BEGIN
                   OPEN taxcode_detail_cursor
                    FOR vc_taxcode_detail_stmt
                  USING transaction_detail.taxcode_code, transaction_detail.transaction_country_code, transaction_detail.transaction_state_code, v_transaction_county, '*ALL', transaction_detail.gl_date, transaction_detail.gl_date;
                  FETCH taxcode_detail_cursor
                   INTO taxcode_detail;
                  -- Rows found?
                  IF taxcode_detail_cursor%FOUND THEN
                     vn_county_rows := taxcode_detail_cursor%ROWCOUNT;
                  ELSE
                     vn_county_rows := 0;
                  END IF;
                  CLOSE taxcode_detail_cursor;
                  IF SQLCODE != 0 THEN
                     vn_county_rows := 0;
                  END IF;
               END;
               IF vn_county_rows > 0 THEN
                  transaction_detail.county_taxcode_detail_id := taxcode_detail.taxcode_detail_id;
                  transaction_detail.county_taxcode_type_code := taxcode_detail.taxcode_type_code;
                  v_county_taxable_threshold_amt := taxcode_detail.taxable_threshold_amt;
                  v_county_tax_limitation_amt := taxcode_detail.tax_limitation_amt;
                  v_county_cap_amt := taxcode_detail.cap_amt;
                  v_county_base_change_pct := taxcode_detail.base_change_pct;
                  v_county_special_rate := taxcode_detail.special_rate;
               END IF;
               -- Get Country/State/all County/City for City
               vn_city_rows := 0;
               BEGIN
                   OPEN taxcode_detail_cursor
                    FOR vc_taxcode_detail_stmt
                  USING transaction_detail.taxcode_code, transaction_detail.transaction_country_code, transaction_detail.transaction_state_code, '*ALL', v_transaction_city, transaction_detail.gl_date, transaction_detail.gl_date;
                  FETCH taxcode_detail_cursor
                   INTO taxcode_detail;
                  -- Rows found?
                  IF taxcode_detail_cursor%FOUND THEN
                     vn_city_rows := taxcode_detail_cursor%ROWCOUNT;
                  ELSE
                     vn_city_rows := 0;
                  END IF;
                  CLOSE taxcode_detail_cursor;
                  IF SQLCODE != 0 THEN
                     vn_city_rows := 0;
                  END IF;
               END;
               IF vn_city_rows > 0 THEN
                  transaction_detail.city_taxcode_detail_id := taxcode_detail.taxcode_detail_id;
                  transaction_detail.city_taxcode_type_code := taxcode_detail.taxcode_type_code;
                  v_city_taxable_threshold_amt := taxcode_detail.taxable_threshold_amt;
                  v_city_tax_limitation_amt := taxcode_detail.tax_limitation_amt;
                  v_city_cap_amt := taxcode_detail.cap_amt;
                  v_city_base_change_pct := taxcode_detail.base_change_pct;
                  v_city_special_rate := taxcode_detail.special_rate;
               END IF;
            ELSE   -- If TaxCode Detail found
               -- suspend for TaxCode Detail
               transaction_detail.transaction_ind := 'S';
               transaction_detail.suspend_ind := 'D';
            END IF;   -- If TaxCode Detail found

            -- Continue if not suspended
            IF transaction_detail.transaction_ind IS NULL OR transaction_detail.transaction_ind <> 'S' THEN
               -- ****** DETERMINE TYPE of TAXCODE ****** --
               -- The TaxCode is Taxable
               IF transaction_detail.state_taxcode_type_code = 'T' OR transaction_detail.county_taxcode_type_code = 'T' OR transaction_detail.city_taxcode_type_code = 'T' THEN
                  -- Determine Tax Calc Flags
                  vc_country_flag := '1';
                  vc_state_flag := '1';
                  IF transaction_detail.state_taxcode_type_code = 'E' THEN
                     vc_state_flag := '0';
                  END IF;
                  vc_county_flag := '1';
                  vc_county_local_flag := '1';
                  IF transaction_detail.county_taxcode_type_code = 'E' THEN
                     vc_county_flag := '0';
                     vc_county_local_flag := '1';
                  END IF;
                  vc_city_flag := '1';
                  vc_city_local_flag := '1';
                  IF transaction_detail.city_taxcode_type_code = 'E' THEN
                     vc_city_flag := '0';
                     vc_city_local_flag := '1';
                  END IF;

                  -- Get Jurisdiction TaxRates and calculate Tax Amounts
                  sp_getusetax(transaction_detail.jurisdiction_id,
                               transaction_detail.gl_date,
                               transaction_detail.gl_line_itm_dist_amt,
                               'U',   -- Situs Tax Type code from Location Matrix when implemented.
                               vc_country_flag,   -- Situs Country Nexusflag when implemented
                               vc_state_flag,   -- Situs State Nexus flag when implemented
                               vc_county_flag,   -- Situs County Nexus flag when implemented
                               vc_county_local_flag,   -- Situs County Local nexus flag when implemented
                               vc_city_flag,   -- Situs City flag when implemented
                               vc_city_local_flag,   -- Situs City Local flag when implemented
                               v_state_ratetype_code,
                               v_state_override_taxtype_code,
                               v_state_taxable_threshold_amt,
                               v_state_tax_limitation_amt,
                               v_state_cap_amt,
                               v_state_base_change_pct,
                               v_state_special_rate,
                               v_county_taxable_threshold_amt,
                               v_county_tax_limitation_amt,
                               v_county_cap_amt,
                               v_county_base_change_pct,
                               v_county_special_rate,
                               v_city_taxable_threshold_amt,
                               v_city_tax_limitation_amt,
                               v_city_cap_amt,
                               v_city_base_change_pct,
                               v_city_special_rate,

                               transaction_detail.jurisdiction_taxrate_id,
                               transaction_detail.taxtype_used_code,
                               transaction_detail.country_use_amount,
                               transaction_detail.state_taxable_amt,
                               transaction_detail.state_use_amount,
                               transaction_detail.state_use_tier2_amount,
                               transaction_detail.state_use_tier3_amount,
                               transaction_detail.county_taxable_amt,
                               transaction_detail.county_use_amount,
                               transaction_detail.county_local_use_amount,
                               transaction_detail.city_taxable_amt,
                               transaction_detail.city_use_amount,
                               transaction_detail.city_local_use_amount,
                               transaction_detail.country_use_rate,
                               transaction_detail.state_use_rate,
                               transaction_detail.state_use_tier2_rate,
                               transaction_detail.state_use_tier3_rate,
                               transaction_detail.state_split_amount,
                               transaction_detail.state_tier2_min_amount,
                               transaction_detail.state_tier2_max_amount,
                               transaction_detail.state_maxtax_amount,
                               transaction_detail.county_use_rate,
                               transaction_detail.county_local_use_rate,
                               transaction_detail.county_split_amount,
                               transaction_detail.county_maxtax_amount,
                               transaction_detail.county_single_flag,
                               transaction_detail.county_default_flag,
                               transaction_detail.city_use_rate,
                               transaction_detail.city_local_use_rate,
                               transaction_detail.city_split_amount,
                               transaction_detail.city_split_use_rate,
                               transaction_detail.city_single_flag,
                               transaction_detail.city_default_flag,
                               transaction_detail.combined_use_rate,
                               transaction_detail.tb_calc_tax_amt);
                  -- Suspend if tax rate id is null
                  IF transaction_detail.jurisdiction_taxrate_id IS NULL OR transaction_detail.jurisdiction_taxrate_id = 0 THEN
                     transaction_detail.transaction_ind := 'S';
                     transaction_detail.suspend_ind := 'R';
                  --ELSE
                     -- Check for Taxable Amount -- 3351
                     --IF vn_taxable_amt <> transaction_detail.gl_line_itm_dist_amt THEN
                     --   transaction_detail.gl_extract_amt := transaction_detail.gl_line_itm_dist_amt;
                     --   transaction_detail.gl_line_itm_dist_amt := vn_taxable_amt;
                     --END IF;
                  END IF;

               -- All TaxCodes are Exempt
               ELSIF transaction_detail.state_taxcode_type_code = 'E' AND transaction_detail.county_taxcode_type_code = 'E' AND transaction_detail.city_taxcode_type_code = 'E' THEN
                  transaction_detail.transaction_ind := 'P';

               -- The Tax Code is Unrecognized - Suspend
               ELSE
                  transaction_detail.transaction_ind := 'S';
                  transaction_detail.suspend_ind := '?';
               END IF;
            END IF;   -- if not suspended
         ELSE   -- If Tax Matrix line found
            -- Tax Matrix Line NOT Found
            transaction_detail.transaction_ind := 'S';
            transaction_detail.suspend_ind := 'T';
         END IF;   -- If Tax Matrix line found

         -- Continue if not Suspended
         IF transaction_detail.transaction_ind IS NULL OR transaction_detail.transaction_ind <> 'S' THEN
            -- Check for matrix "H"old
            IF ( v_hold_code_flag = '1' ) AND
               ( transaction_detail.state_taxcode_type_code = 'T' OR transaction_detail.state_taxcode_type_code = 'E') THEN
                  transaction_detail.transaction_ind := 'H';
            ELSE
                transaction_detail.transaction_ind := 'P';
            END IF;
         END IF;

         -- Update transaction detail row
         BEGIN
            UPDATE tb_transaction_detail
               SET tb_calc_tax_amt = transaction_detail.tb_calc_tax_amt,
                   country_use_amount = transaction_detail.country_use_amount,
                   state_use_amount = transaction_detail.state_use_amount,
                   state_use_tier2_amount = transaction_detail.state_use_tier2_amount,
                   state_use_tier3_amount = transaction_detail.state_use_tier3_amount,
                   county_use_amount = transaction_detail.county_use_amount,
                   county_local_use_amount = transaction_detail.county_local_use_amount,
                   city_use_amount = transaction_detail.city_use_amount,
                   city_local_use_amount = transaction_detail.city_local_use_amount,
                   transaction_country_code = transaction_detail.transaction_country_code,
                   transaction_state_code = transaction_detail.transaction_state_code,
                   transaction_ind = transaction_detail.transaction_ind,
                   suspend_ind = transaction_detail.suspend_ind,
                   taxcode_code = transaction_detail.taxcode_code,
                   tax_matrix_id = transaction_detail.tax_matrix_id,
                   location_matrix_id = transaction_detail.location_matrix_id,
                   jurisdiction_id = transaction_detail.jurisdiction_id,
                   jurisdiction_taxrate_id = transaction_detail.jurisdiction_taxrate_id,
                   measure_type_code = v_state_ratetype_code,
                   state_use_rate = transaction_detail.state_use_rate,
                   state_use_tier2_rate = transaction_detail.state_use_tier2_rate,
                   state_use_tier3_rate = transaction_detail.state_use_tier3_rate,
                   state_split_amount = transaction_detail.state_split_amount,
                   state_tier2_min_amount = transaction_detail.state_tier2_min_amount,
                   state_tier2_max_amount = transaction_detail.state_tier2_max_amount,
                   state_maxtax_amount = transaction_detail.state_maxtax_amount,
                   county_use_rate = transaction_detail.county_use_rate,
                   county_local_use_rate = transaction_detail.county_local_use_rate,
                   county_split_amount = transaction_detail.county_split_amount,
                   county_maxtax_amount = transaction_detail.county_maxtax_amount,
                   county_single_flag = transaction_detail.county_single_flag,
                   county_default_flag = transaction_detail.county_default_flag,
                   city_use_rate = transaction_detail.city_use_rate,
                   city_local_use_rate = transaction_detail.city_local_use_rate,
                   city_split_amount = transaction_detail.city_split_amount,
                   city_split_use_rate = transaction_detail.city_split_use_rate,
                   city_single_flag = transaction_detail.city_single_flag,
                   city_default_flag = transaction_detail.city_default_flag,
                   combined_use_rate = transaction_detail.combined_use_rate,
                   state_taxcode_detail_id = transaction_detail.state_taxcode_detail_id,
                   county_taxcode_detail_id = transaction_detail.county_taxcode_detail_id,
                   city_taxcode_detail_id = transaction_detail.city_taxcode_detail_id,
                   taxtype_used_code = transaction_detail.taxtype_used_code,
                   state_taxable_amt = transaction_detail.state_taxable_amt,
                   county_taxable_amt = transaction_detail.county_taxable_amt,
                   city_taxable_amt = transaction_detail.city_taxable_amt,
                   state_taxcode_type_code = transaction_detail.state_taxcode_type_code,
                   county_taxcode_type_code = transaction_detail.county_taxcode_type_code,
                   city_taxcode_type_code = transaction_detail.city_taxcode_type_code,
                   update_user_id = transaction_detail.update_user_id,
                   update_timestamp = transaction_detail.update_timestamp
             WHERE transaction_detail_id = transaction_detail.transaction_detail_id;
         END;
      END LOOP;
      CLOSE transaction_detail_cursor;
      COMMIT;
   END IF;   -- if not default matrix line
--EXCEPTION
--   WHEN OTHERS THEN
--vc_test1 := substr(vc_transaction_detail_stmt,1,4000);
--vc_test2 := substr(vc_transaction_detail_stmt,4001,4000);
--insert into tb_debug(row_joe, row_bob) values(vc_test1, vc_test2);
END;
