set define off
spool c:\alter_v5_1_1_3.log
set echo on


-- 0005427: Import headers not uppercase --

INSERT INTO TB_LIST_CODE (CODE_TYPE_CODE, CODE_CODE, DESCRIPTION, MESSAGE, SEVERITY_LEVEL) VALUES ('ERRORCODE', 'IP9', 'Import file columns not mapped in the Definition.', '', 20);


COMMIT;

spool off