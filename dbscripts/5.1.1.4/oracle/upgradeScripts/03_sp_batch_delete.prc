create or replace
PROCEDURE sp_batch_delete(an_batch_id number)
/* ************************************************************************************************/
/* Object Type/Name: SProc - sp_batch_delete                                                      */
/* Author:           Michael B. Fuller                                                            */
/* Date:             a long time ago                                                              */
/* Description:      Delete a batch                                                               */
/* Arguments:        an_batch_id(number)                                                          */
/* Returns:          None                                                                         */
/* ************************************************************************************************/
/* Maintenance History:                                                                           */
/* Scan  Author       Date     Release    Description                                  Ticket #   */
/* ----- ---------- ---------- ---------- -------------------------------------------- ---------- */
/* MBF01 M. Fuller  08/12/2011            Add TaxCode Rules Updates                    17         */
/* ************************************************************************************************/
IS

-- Table defined variables
   v_batch_status_code             tb_batch.batch_status_code%TYPE                   := NULL;
   v_abort_import_flag             tb_list_code.abort_import_flag%TYPE               := '0';

-- Define Exceptions
   e_badread                       exception;
   e_wrongdata                     exception;
   e_abort                         exception;

-- ****** Program starts ****** --------------------------------------------------------------------
BEGIN
   -- Confirm batch exists and is flagged for processing
   BEGIN
      SELECT batch_status_code
        INTO v_batch_status_code
        FROM tb_batch
       WHERE batch_id = an_batch_id;
      IF SQLCODE != 0 THEN
         RAISE e_badread;
      ELSIF v_batch_status_code != 'FD' THEN
         RAISE e_wrongdata;
      END IF;
   EXCEPTION
      WHEN NO_DATA_FOUND OR e_badread THEN
         v_abort_import_flag := '1';
      WHEN e_wrongdata THEN
         v_abort_import_flag := '1';
   END;
   -- Abort Procedure?
   IF v_abort_import_flag = '1' THEN
      RAISE e_abort;
   END IF;

   UPDATE tb_batch
      SET batch_status_code = 'XD',
          actual_start_timestamp = SYS_EXTRACT_UTC(SYSTIMESTAMP)
    WHERE batch_id = an_batch_id;
   COMMIT;

   DELETE FROM tb_transaction_detail WHERE process_batch_no = an_batch_id;
   COMMIT;

   DELETE FROM tb_bcp_transactions WHERE process_batch_no = an_batch_id;
   COMMIT;

   DELETE FROM tb_bcp_jurisdiction_taxrate WHERE batch_id = an_batch_id;
   COMMIT;

   DELETE FROM tb_bcp_taxcode_rules WHERE batch_id = an_batch_id;
   COMMIT;

   UPDATE tb_batch
      SET batch_status_code = 'D',
          actual_end_timestamp = SYS_EXTRACT_UTC(SYSTIMESTAMP)
      WHERE batch_id = an_batch_id;
   COMMIT;
EXCEPTION
   WHEN e_abort THEN
      NULL;
END;
