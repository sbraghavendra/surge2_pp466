/*  Taxability Allocations  ***********************************************************************/
CREATE OR REPLACE PROCEDURE sp_batch_tax_allocation(an_batch_id number)
/* ************************************************************************************************/
/* Object Type/Name: Stored Procedure - sp_batch_tax_allocation                                   */
/* Author:           Michael B. Fuller                                                            */
/* Date:             04/11/2011                                                                   */
/* Description:      Process Taxability Allocations                                               */
/* Arguments:        an_batch_id (number)                                                         */
/* Returns:          n/a                                                                          */
/* ************************************************************************************************/
/* Maintenance History:                                                                           */
/* Scan  Author       Date     Release    Description                                  Ticket #   */
/* ----- ---------- ---------- ---------- -------------------------------------------- ---------- */
/*       M. Fuller  04/11/2011            Created Stored Procedure                     1179       */
/* MBF01 M. Fuller  05/23/2011            Add Country for Federal Tax Level            1346       */
/* ************************************************************************************************/
IS
-- Define Cursor Type
   TYPE cursor_type is REF CURSOR;

-- Define Drivers Types
	TYPE trans_dtl_column_name_tab IS VARRAY(30) OF VARCHAR2(40);
	trans_dtl_column_names          trans_dtl_column_name_tab;
   TYPE driver_value_tab IS VARRAY(30) OF VARCHAR2(100);
   driver_values                   driver_value_tab;

-- Taxability Allocation Matrix Selection SQL
   vc_tax_alloc_matrix_select      VARCHAR2(2000) :=
      'SELECT tb_tax_alloc_matrix.tax_alloc_matrix_id, tb_tax_alloc_matrix.future_split ' ||
       'FROM tb_tax_alloc_matrix, tb_bcp_transactions ' ||
      'WHERE ( tb_bcp_transactions.rowid = :v_rowid ) AND ' ||
            '( tb_tax_alloc_matrix.effective_date <= tb_bcp_transactions.gl_date ) AND ( tb_tax_alloc_matrix.expiration_date >= tb_bcp_transactions.gl_date ) ';
   vc_tax_alloc_matrix_where       VARCHAR2(28000) := '';
   vc_tax_alloc_matrix_orderby     VARCHAR2(1000) :=
      'ORDER BY tb_tax_alloc_matrix.binary_weight DESC, tb_tax_alloc_matrix.significant_digits DESC, tb_tax_alloc_matrix.effective_date DESC';
   vc_tax_alloc_matrix_stmt        VARCHAR2 (31000);
   tax_alloc_matrix_cursor         cursor_type;

-- Taxability Allocation Matrix Record TYPE   
   TYPE tax_alloc_matrix_record is RECORD (
      tax_alloc_matrix_id         tb_tax_alloc_matrix.tax_alloc_matrix_id%TYPE,
      future_split                tb_tax_alloc_matrix.future_split%TYPE );
   tax_alloc_matrix               tax_alloc_matrix_record;

-- Table defined variables
   v_value                         tb_option.value%TYPE                              := NULL;
   v_batch_status_code             tb_batch.batch_status_code%TYPE                   := 'P';
   v_error_sev_code                tb_batch.error_sev_code%TYPE                      := ' ';
   v_total_rows                    tb_batch.total_rows%TYPE                          := 0;
   v_severity_level                tb_list_code.severity_level%TYPE                  := ' ';
   v_write_import_line_flag        tb_list_code.write_import_line_flag%TYPE          := '1';
   v_abort_import_flag             tb_list_code.abort_import_flag%TYPE               := '0';
   v_tax_alloc_matrix_id           tb_tax_alloc_matrix.tax_alloc_matrix_id%TYPE      := 0;
   v_tax_alloc_matrix_dtl_id       tb_tax_alloc_matrix_detail.tax_alloc_matrix_dtl_id%TYPE := 0;
   v_global_or_line_dtl_id         tb_tax_alloc_matrix_detail.tax_alloc_matrix_dtl_id%TYPE := 0;
   v_transaction_detail_id         tb_bcp_transactions.transaction_detail_id%TYPE    := 0;
   v_sysdate                       tb_bcp_transactions.load_timestamp%TYPE           := SYSDATE;
   v_transaction_country_code      tb_bcp_transactions.transaction_country_code%TYPE := NULL;
   v_transaction_state_code        tb_bcp_transactions.transaction_state_code%TYPE   := NULL;
   v_tax_gl_line_itm_dist_amt      tb_bcp_transactions.gl_line_itm_dist_amt%TYPE     := 0;
   v_tax_orig_gl_line_itm          tb_bcp_transactions.gl_line_itm_dist_amt%TYPE     := 0;
   v_tax_accum_alloc               tb_bcp_transactions.gl_line_itm_dist_amt%TYPE     := 0;
   v_tax_delta                     tb_bcp_transactions.gl_line_itm_dist_amt%TYPE     := 0;
   v_jur_transaction_country_code  tb_bcp_transactions.transaction_country_code%TYPE := NULL;
   v_jur_auto_trans_country_code   tb_bcp_transactions.auto_transaction_country_code%TYPE := NULL;
   v_jur_transaction_state_code    tb_bcp_transactions.transaction_state_code%TYPE   := NULL;
   v_jur_auto_trans_state_code     tb_bcp_transactions.auto_transaction_state_code%TYPE := NULL;
   v_jur_jurisdiction_id           tb_bcp_transactions.jurisdiction_id%TYPE          := 0;
   v_jur_gl_line_itm_dist_amt      tb_bcp_transactions.gl_line_itm_dist_amt%TYPE     := 0;
   v_jur_orig_gl_line_itm          tb_bcp_transactions.gl_line_itm_dist_amt%TYPE     := 0;
   v_jur_accum_alloc               tb_bcp_transactions.gl_line_itm_dist_amt%TYPE     := 0;
   v_jur_delta                     tb_bcp_transactions.gl_line_itm_dist_amt%TYPE     := 0;
   v_allocation_matrix_id          tb_bcp_transactions.allocation_matrix_id%TYPE     := NULL;
   v_allocation_subtrans_id        tb_bcp_transactions.allocation_subtrans_id%TYPE   := NULL;
   v_split_subtrans_id             tb_bcp_transactions.split_subtrans_id%TYPE        := NULL;
   v_tax_multi_trans_code          tb_bcp_transactions.multi_trans_code%TYPE         := NULL;
   v_bcp_transaction_id            tb_bcp_transactions.bcp_transaction_id%TYPE       := 0;

-- Program defined variables
   v_rowid                         ROWID                                             := NULL;
   vi_cursor_id                    INTEGER                                           := 0;
   vi_rows_processed               INTEGER                                           := 0;
   vn_fetch_rows                   NUMBER                                            := 0;
   vn_tax_alloc_total_rows         NUMBER                                            := 0;
	vn_jur_alloc_total_rows         NUMBER                                            := 0;
   vi_error_count                  INTEGER                                           := 0;
   vc_jur_alloc_flag               CHAR(1)                                           := 'X';
   vc_state_driver_flag            CHAR(1)                                           := '0';
   vn_driver_index                 INTEGER                                           := 0;
   vc_fieldname                    VARCHAR2(40)                                      := NULL;
   vc_fieldvalue                   VARCHAR2(100)                                     := NULL;
   vc_driver_sql                   VARCHAR2(10000)                                   := '';

-- Define Tax Driver Names Cursor (tb_driver_names)
   CURSOR tax_driver_cursor
   IS
        SELECT driver.trans_dtl_column_name, driver.matrix_column_name, driver.null_driver_flag, driver.wildcard_flag,
               datadefcol.desc_column_name, datadefcol.data_type
          FROM tb_driver_names driver,
               (SELECT *
                  FROM tb_data_def_column
                 WHERE table_name = 'TB_TRANSACTION_DETAIL') datadefcol

         WHERE driver.driver_names_code = 'T' AND
               driver.trans_dtl_column_name = datadefcol.column_name
      ORDER BY driver.driver_id;
      tax_driver                   tax_driver_cursor%ROWTYPE;

-- Define Taxability Allocation Matrix Detail / Jurisdiction Cursor (tb_tax_alloc_matrix_detail, tb_jurisdiction)
   CURSOR tax_alloc_detail_cursor
   IS
        SELECT tax_alloc_matrix_id, tax_alloc_matrix_dtl_id,
               tb_tax_alloc_matrix_detail.driver_01, tb_tax_alloc_matrix_detail.driver_02, tb_tax_alloc_matrix_detail.driver_03,
		         tb_tax_alloc_matrix_detail.driver_04, tb_tax_alloc_matrix_detail.driver_05, tb_tax_alloc_matrix_detail.driver_06,
					tb_tax_alloc_matrix_detail.driver_07, tb_tax_alloc_matrix_detail.driver_08, tb_tax_alloc_matrix_detail.driver_09,
					tb_tax_alloc_matrix_detail.driver_10, tb_tax_alloc_matrix_detail.driver_11, tb_tax_alloc_matrix_detail.driver_12,
		         tb_tax_alloc_matrix_detail.driver_13, tb_tax_alloc_matrix_detail.driver_14, tb_tax_alloc_matrix_detail.driver_15,
					tb_tax_alloc_matrix_detail.driver_16, tb_tax_alloc_matrix_detail.driver_17, tb_tax_alloc_matrix_detail.driver_18,
					tb_tax_alloc_matrix_detail.driver_19, tb_tax_alloc_matrix_detail.driver_20, tb_tax_alloc_matrix_detail.driver_21,
		         tb_tax_alloc_matrix_detail.driver_22, tb_tax_alloc_matrix_detail.driver_23, tb_tax_alloc_matrix_detail.driver_24,
					tb_tax_alloc_matrix_detail.driver_25, tb_tax_alloc_matrix_detail.driver_26, tb_tax_alloc_matrix_detail.driver_27,
					tb_tax_alloc_matrix_detail.driver_28, tb_tax_alloc_matrix_detail.driver_29, tb_tax_alloc_matrix_detail.driver_30,
               tb_tax_alloc_matrix_detail.allocation_percent
          FROM tb_tax_alloc_matrix_detail
         WHERE tb_tax_alloc_matrix_detail.tax_alloc_matrix_id = v_tax_alloc_matrix_id;
      tax_alloc_detail             tax_alloc_detail_cursor%ROWTYPE;

-- Define Taxability Jurisdiction Allocation Matrix Detail / Jurisdiction Cursor (tb_tax_alloc_matrix_detail, tb_jurisdiction)
   CURSOR jur_alloc_detail_cursor
   IS
        SELECT tb_jur_alloc_matrix_detail.jurisdiction_id, tb_jur_alloc_matrix_detail.allocation_percent,
               tb_jurisdiction.geocode, tb_jurisdiction.state, tb_jurisdiction.county, tb_jurisdiction.city, tb_jurisdiction.zip, tb_jurisdiction.country
          FROM tb_jur_alloc_matrix_detail, tb_jurisdiction
         WHERE ( tb_jur_alloc_matrix_detail.jurisdiction_id = tb_jurisdiction.jurisdiction_id (+))
           AND ( tb_jur_alloc_matrix_detail.tax_alloc_matrix_id = v_tax_alloc_matrix_id
           AND   tb_jur_alloc_matrix_detail.tax_alloc_matrix_dtl_id = v_global_or_line_dtl_id );
      jur_alloc_detail             jur_alloc_detail_cursor%ROWTYPE;

-- Define BCP Transactions Cursor (tb_bcp_transactions)
   CURSOR bcp_transactions_cursor
   IS
      SELECT ROWID,
             transaction_detail_id,
             source_transaction_id,
             process_batch_no,
             gl_extract_batch_no,
             archive_batch_no,
             allocation_matrix_id,
             allocation_subtrans_id,
             entered_date,
             transaction_status,
             gl_date,
             gl_company_nbr,
             gl_company_name,
             gl_division_nbr,
             gl_division_name,
             gl_cc_nbr_dept_id,
             gl_cc_nbr_dept_name,
             gl_local_acct_nbr,
             gl_local_acct_name,
             gl_local_sub_acct_nbr,
             gl_local_sub_acct_name,
             gl_full_acct_nbr,
             gl_full_acct_name,
             gl_line_itm_dist_amt,
             orig_gl_line_itm_dist_amt,
             vendor_nbr,
             vendor_name,
             vendor_address_line_1,
             vendor_address_line_2,
             vendor_address_line_3,
             vendor_address_line_4,
             vendor_address_city,
             vendor_address_county,
             vendor_address_state,
             vendor_address_zip,
             vendor_address_country,
             vendor_type,
             vendor_type_name,
             invoice_nbr,
             invoice_desc,
             invoice_date,
             invoice_freight_amt,
             invoice_discount_amt,
             invoice_tax_amt,
             invoice_total_amt,
             invoice_tax_flg,
             invoice_line_nbr,
             invoice_line_name,
             invoice_line_type,
             invoice_line_type_name,
             invoice_line_amt,
             invoice_line_tax,
             afe_project_nbr,
             afe_project_name,
             afe_category_nbr,
             afe_category_name,
             afe_sub_cat_nbr,
             afe_sub_cat_name,
             afe_use,
             afe_contract_type,
             afe_contract_structure,
             afe_property_cat,
             inventory_nbr,
             inventory_name,
             inventory_class,
             inventory_class_name,
             po_nbr,
             po_name,
             po_date,
             po_line_nbr,
             po_line_name,
             po_line_type,
             po_line_type_name,
             ship_to_location,
             ship_to_location_name,
             ship_to_address_line_1,
             ship_to_address_line_2,
             ship_to_address_line_3,
             ship_to_address_line_4,
             ship_to_address_city,
             ship_to_address_county,
             ship_to_address_state,
             ship_to_address_zip,
             ship_to_address_country,
             wo_nbr,
             wo_name,
             wo_date,
             wo_type,
             wo_type_desc,
             wo_class,
             wo_class_desc,
             wo_entity,
             wo_entity_desc,
             wo_line_nbr,
             wo_line_name,
             wo_line_type,
             wo_line_type_desc,
             wo_shut_down_cd,
             wo_shut_down_cd_desc,
             voucher_id,
             voucher_name,
             voucher_date,
             voucher_line_nbr,
             voucher_line_desc,
             check_nbr,
             check_no,
             check_date,
             check_amt,
             check_desc,
             user_text_01,
             user_text_02,
             user_text_03,
             user_text_04,
             user_text_05,
             user_text_06,
             user_text_07,
             user_text_08,
             user_text_09,
             user_text_10,
             user_text_11,
             user_text_12,
             user_text_13,
             user_text_14,
             user_text_15,
             user_text_16,
             user_text_17,
             user_text_18,
             user_text_19,
             user_text_20,
             user_text_21,
             user_text_22,
             user_text_23,
             user_text_24,
             user_text_25,
             user_text_26,
             user_text_27,
             user_text_28,
             user_text_29,
             user_text_30,
             user_number_01,
             user_number_02,
             user_number_03,
             user_number_04,
             user_number_05,
             user_number_06,
             user_number_07,
             user_number_08,
             user_number_09,
             user_number_10,
             user_date_01,
             user_date_02,
             user_date_03,
             user_date_04,
             user_date_05,
             user_date_06,
             user_date_07,
             user_date_08,
             user_date_09,
             user_date_10,
             comments,
             tb_calc_tax_amt,
             state_use_amount,
             state_use_tier2_amount,
             state_use_tier3_amount,
             county_use_amount,
             county_local_use_amount,
             city_use_amount,
             city_local_use_amount,
             transaction_state_code,
             auto_transaction_state_code,
             transaction_ind,
             suspend_ind,
             taxcode_detail_id,
             taxcode_state_code,
             taxcode_type_code,
             taxcode_code,
             cch_taxcat_code,
             cch_group_code,
             cch_item_code,
             manual_taxcode_ind,
             tax_matrix_id,
             location_matrix_id,
             jurisdiction_id,
             jurisdiction_taxrate_id,
             manual_jurisdiction_ind,
             measure_type_code,
             state_use_rate,
             state_use_tier2_rate,
             state_use_tier3_rate,
             state_split_amount,
             state_tier2_min_amount,
             state_tier2_max_amount,
             state_maxtax_amount,
             county_use_rate,
             county_local_use_rate,
             county_split_amount,
             county_maxtax_amount,
             county_single_flag,
             county_default_flag,
             city_use_rate,
             city_local_use_rate,
             city_split_amount,
             city_split_use_rate,
             city_single_flag,
             city_default_flag,
             combined_use_rate,
             load_timestamp,
             gl_extract_updater,
             gl_extract_timestamp,
             gl_extract_flag,
             gl_log_flag,
             gl_extract_amt,
             audit_flag,
             audit_user_id,
             audit_timestamp,
             modify_user_id,
             modify_timestamp,
             update_user_id,
             update_timestamp,
             bcp_transaction_id,
             country_use_amount,
             transaction_country_code,
             auto_transaction_country_code,
             taxcode_country_code,
             country_use_rate,
             split_subtrans_id,
             multi_trans_code,
             tax_alloc_matrix_id
        FROM tb_bcp_transactions
       WHERE process_batch_no = an_batch_id
         AND (gl_line_itm_dist_amt IS NOT NULL AND gl_line_itm_dist_amt <> 0)
         AND split_subtrans_id IS NULL;
      bcp_transactions             bcp_transactions_cursor%ROWTYPE;

-- Define Exceptions
   e_badread                       exception;
   e_badupdate                     exception;
   e_badwrite                      exception;
   e_wrongdata                     exception;
   e_abort                         exception;

-- ****** Program starts ****** --------------------------------------------------------------------
BEGIN
   -- Confirm Taxability Allocations are active
   BEGIN
      SELECT value
        INTO v_value
        FROM tb_option
       WHERE option_code = 'TAXALLOCENABLED'
         AND option_type_code = 'ADMIN'
         AND user_code = 'ADMIN';
      IF SQLCODE != 0 THEN
         RAISE e_badread;
      ELSIF v_value <> '1' THEN
         RAISE e_wrongdata;
      END IF;
   EXCEPTION
      WHEN NO_DATA_FOUND OR e_badread OR e_wrongdata THEN
         --insert into tb_debug(row_joe) values('STOP');
         RAISE e_abort;
   END;

   -- Confirm batch exists and is flagged for process
   BEGIN
      SELECT batch_status_code, total_rows
        INTO v_batch_status_code, v_total_rows
        FROM tb_batch
       WHERE batch_id = an_batch_id;
      IF SQLCODE != 0 THEN
         RAISE e_badread;
      ELSIF v_batch_status_code != 'FP' THEN
         RAISE e_wrongdata;
      END IF;
   EXCEPTION
      WHEN NO_DATA_FOUND OR e_badread THEN
         vi_error_count := vi_error_count + 1;
         sp_geterrorcode('TAL1', an_batch_id, 'TAL', v_sysdate, v_severity_level, v_write_import_line_flag, v_abort_import_flag);
         IF v_severity_level > v_error_sev_code THEN
            v_error_sev_code := v_severity_level;
         END IF;
      WHEN e_wrongdata THEN
         vi_error_count := vi_error_count + 1;
         sp_geterrorcode('TAL2', an_batch_id, 'TAL', v_sysdate, v_severity_level, v_write_import_line_flag, v_abort_import_flag);
         IF v_severity_level > v_error_sev_code THEN
            v_error_sev_code := v_severity_level;
         END IF;
   END;
   -- Abort Procedure?
   IF v_abort_import_flag = '1' THEN
      v_batch_status_code := 'ABTAL';
      RAISE e_abort;
   END IF;

   -- Update batch
   BEGIN
      UPDATE tb_batch
         SET batch_status_code = 'XTAL',
             error_sev_code = '',
             nu04 = v_total_rows
       WHERE batch_id = an_batch_id;
      IF SQLCODE != 0 THEN
         RAISE e_badupdate;
      END IF;
   EXCEPTION
      WHEN e_badupdate THEN
         vi_error_count := vi_error_count + 1;
         sp_geterrorcode('TAL3', an_batch_id, 'P', v_sysdate, v_severity_level, v_write_import_line_flag, v_abort_import_flag);
         IF v_severity_level > v_error_sev_code THEN
            v_error_sev_code := v_severity_level;
         END IF;
   END;
   -- Abort Procedure?
   IF v_abort_import_flag = '1' THEN
      v_batch_status_code := 'ABTAL';
      RAISE e_abort;
   END IF;

   -- ***** Create Dynamic WHERE Clause for Matrix SQLs ***** --
   -- Initialize arrays
   trans_dtl_column_names := trans_dtl_column_name_tab();
   driver_values := driver_value_tab();
   -- Tax Allocation Matrix Drivers
   driver_values.EXTEND(30);
   OPEN tax_driver_cursor;
   LOOP
      FETCH tax_driver_cursor INTO tax_driver;
      EXIT WHEN tax_driver_cursor%NOTFOUND;
      IF tax_driver.null_driver_flag = '1' THEN
         IF tax_driver.wildcard_flag = '1' THEN
            -- NULL and Wildcard
            IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
               UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                  vc_tax_alloc_matrix_where := vc_tax_alloc_matrix_where || 'AND ' ||
                  '(( tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ' IS NULL AND (UPPER(tb_tax_alloc_matrix.' || tax_driver.matrix_column_name || ') = ''*ALL'' OR UPPER(tb_tax_alloc_matrix.' || tax_driver.matrix_column_name || ') = ''*NULL'' )) OR ' ||
                  '( tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND (UPPER(tb_tax_alloc_matrix.' || tax_driver.matrix_column_name || ') = ''*ALL'' OR UPPER(tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ') LIKE UPPER(tb_tax_alloc_matrix.' || tax_driver.matrix_column_name || ')))) ';
            END IF;
         ELSE
            -- NULL and NOT Wildcard
            IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
               UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                  vc_tax_alloc_matrix_where := vc_tax_alloc_matrix_where || 'AND ' ||
                  '(( tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ' IS NULL AND (UPPER(tb_tax_alloc_matrix.' || tax_driver.matrix_column_name || ') = ''*ALL'' OR UPPER(tb_tax_alloc_matrix.' || tax_driver.matrix_column_name || ') = ''*NULL'' )) OR ' ||
                  '( tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND (UPPER(tb_tax_alloc_matrix.' || tax_driver.matrix_column_name || ') = ''*ALL'' OR UPPER(tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ') = UPPER(tb_tax_alloc_matrix.' || tax_driver.matrix_column_name || ')))) ';
            END IF;
         END IF;
      ELSE
         IF tax_driver.wildcard_flag = '1' THEN
            -- NOT NULL and Wildcard
            IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
               UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                  vc_tax_alloc_matrix_where := vc_tax_alloc_matrix_where || 'AND ' ||
                  '( UPPER(tb_tax_alloc_matrix.' || tax_driver.matrix_column_name || ') = ''*ALL'' OR UPPER(tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ') LIKE UPPER(tb_tax_alloc_matrix.' || tax_driver.matrix_column_name || ')) ';
            END IF;
         ELSE
            -- NOT NULL and NOT Wildcard
            IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
               UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                  IF tax_driver.trans_dtl_column_name = 'TRANSACTION_STATE_CODE' THEN
                     -- Driver is "transaction_state_code"
                     IF vc_state_driver_flag <> '1' THEN
                        vc_state_driver_flag := '1';
                     END IF;
                     vc_tax_alloc_matrix_where := vc_tax_alloc_matrix_where || 'AND ' ||
                     '( UPPER(tb_tax_alloc_matrix.' || tax_driver.matrix_column_name || ') = ''*ALL'' OR UPPER(:v_transaction_state_code) = UPPER(tb_tax_alloc_matrix.' || tax_driver.matrix_column_name || ')) ';
                  ELSE
                     -- Driver is not "transaction_state_code"
                     vc_tax_alloc_matrix_where := vc_tax_alloc_matrix_where || 'AND ' ||
                     '( UPPER(tb_tax_alloc_matrix.' || tax_driver.matrix_column_name || ') = ''*ALL'' OR UPPER(tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ') = UPPER(tb_tax_alloc_matrix.' || tax_driver.matrix_column_name || ')) ';
                  END IF;
            END IF;
         END IF;
      END IF;

     -- Build Transaction Column Name Array
      vn_driver_index := vn_driver_index + 1;
      trans_dtl_column_names.EXTEND(1);
      trans_dtl_column_names(vn_driver_index) := tax_driver.trans_dtl_column_name;
   END LOOP;
   CLOSE tax_driver_cursor;

   -- If no drivers found raise error, else create Tax Allocation matrix sql statement
   IF vc_tax_alloc_matrix_where IS NULL THEN
      vi_error_count := vi_error_count + 1;
      sp_geterrorcode('TAL4', an_batch_id, 'TAL', v_sysdate, v_severity_level, v_write_import_line_flag, v_abort_import_flag);
      IF v_severity_level > v_error_sev_code THEN
         v_error_sev_code := v_severity_level;
      END IF;
   ELSE
      vc_tax_alloc_matrix_stmt := vc_tax_alloc_matrix_select || vc_tax_alloc_matrix_where || vc_tax_alloc_matrix_orderby;
   END IF;
   -- Abort Procedure?
   IF v_abort_import_flag = '1' THEN
      v_batch_status_code := 'ABTAL';
      RAISE e_abort;
   END IF;

   -- Clear large rollback segment
   COMMIT;

   -- ****** Read and Process Transactions ****** --------------------------------------------------
   OPEN bcp_transactions_cursor;
   LOOP
      FETCH bcp_transactions_cursor INTO bcp_transactions;
      EXIT WHEN bcp_transactions_cursor%NOTFOUND;
      v_rowid := bcp_transactions.rowid;

      -- Search Tax Allocation Matrix for matches
      BEGIN
          OPEN tax_alloc_matrix_cursor
           FOR vc_tax_alloc_matrix_stmt
         USING v_rowid;
         FETCH tax_alloc_matrix_cursor
          INTO tax_alloc_matrix;
         -- Rows found?
         IF tax_alloc_matrix_cursor%FOUND THEN
            vn_fetch_rows := tax_alloc_matrix_cursor%ROWCOUNT;
         ELSE
            vn_fetch_rows := 0;
         END IF;
         CLOSE tax_alloc_matrix_cursor;
         IF SQLCODE != 0 THEN
            RAISE e_badread;
         END IF;
      EXCEPTION
         WHEN e_badread THEN
            vn_fetch_rows := 0;
            vi_error_count := vi_error_count + 1;
            sp_geterrorcode('TAL5', an_batch_id, 'TAL', v_sysdate, v_severity_level, v_write_import_line_flag, v_abort_import_flag);
            IF v_severity_level > v_error_sev_code THEN
               v_error_sev_code := v_severity_level;
             END IF;
      END;
      -- Exit if aborted
      IF v_abort_import_flag = '1' THEN
         v_batch_status_code := 'ABTAL';
         RAISE e_abort;
      END IF;

      -- Tax Allocation Matrix Line Found
      IF vn_fetch_rows > 0 THEN
         v_tax_alloc_matrix_id := tax_alloc_matrix.tax_alloc_matrix_id;
         IF tax_alloc_matrix.future_split = '1' THEN
            -- Update Future Split Transaction
            UPDATE tb_bcp_transactions
               SET multi_trans_code = 'FS',
                   tax_alloc_matrix_id = v_tax_alloc_matrix_id
             WHERE rowid = v_rowid;
         ELSE
            -- Get number of Tax Allocation Detail lines
            vn_tax_alloc_total_rows := 0;
            SELECT count(*)
              INTO vn_tax_alloc_total_rows
              FROM tb_tax_alloc_matrix_detail
             WHERE tb_tax_alloc_matrix_detail.tax_alloc_matrix_id = v_tax_alloc_matrix_id;
            IF vn_tax_alloc_total_rows > 0 THEN
               -- Get and create Tax Allocations --
               v_transaction_detail_id := bcp_transactions.transaction_detail_id;
               v_transaction_country_code := bcp_transactions.transaction_country_code;
               v_transaction_state_code := bcp_transactions.transaction_state_code;
               IF v_transaction_country_code IS NULL THEN
                  v_transaction_country_code := '*NULL';
               END IF;
               IF v_transaction_state_code IS NULL THEN
                  v_transaction_state_code := '*NULL';
               END IF;

               -- Working fields
               v_tax_multi_trans_code := 'S';
               v_tax_orig_gl_line_itm := bcp_transactions.gl_line_itm_dist_amt;
               v_tax_accum_alloc := 0;
               v_tax_delta := 0;

               -- Get next Split Allocation sub-trans ID for the new transaction detail records
               SELECT sq_tb_split_subtrans_id.NEXTVAL
                 INTO v_split_subtrans_id
                 FROM DUAL;

               -- Check for Jurisdiction Allocation Matrix for the whole Allocation set
               vc_jur_alloc_flag := 'X';
               vn_jur_alloc_total_rows := 0;
               SELECT count(*)
                 INTO vn_jur_alloc_total_rows
                 FROM tb_jur_alloc_matrix_detail
                WHERE tb_jur_alloc_matrix_detail.tax_alloc_matrix_id = v_tax_alloc_matrix_id
                  AND tb_jur_alloc_matrix_detail.tax_alloc_matrix_dtl_id = 0;
               IF vn_jur_alloc_total_rows > 0 THEN
                  vc_jur_alloc_flag := 'G';
                  v_global_or_line_dtl_id := 0;
               END IF;

               -- Execute Cursor to get Tax Allocation Details
               OPEN tax_alloc_detail_cursor;
               LOOP
                  FETCH tax_alloc_detail_cursor INTO tax_alloc_detail;
                  EXIT WHEN tax_alloc_detail_cursor%NOTFOUND;

                  -- Add Tax Allocation Detail rows to total imported rows
                  driver_values(1) := tax_alloc_detail.driver_01;
                  driver_values(2) := tax_alloc_detail.driver_02;
                  driver_values(3) := tax_alloc_detail.driver_03;
                  driver_values(4) := tax_alloc_detail.driver_04;
                  driver_values(5) := tax_alloc_detail.driver_05;
                  driver_values(6) := tax_alloc_detail.driver_06;
                  driver_values(7) := tax_alloc_detail.driver_07;
                  driver_values(8) := tax_alloc_detail.driver_08;
                  driver_values(9) := tax_alloc_detail.driver_09;
                  driver_values(10) := tax_alloc_detail.driver_10;
                  driver_values(11) := tax_alloc_detail.driver_11;
                  driver_values(12) := tax_alloc_detail.driver_12;
                  driver_values(13) := tax_alloc_detail.driver_13;
                  driver_values(14) := tax_alloc_detail.driver_14;
                  driver_values(15) := tax_alloc_detail.driver_15;
                  driver_values(16) := tax_alloc_detail.driver_16;
                  driver_values(17) := tax_alloc_detail.driver_17;
                  driver_values(18) := tax_alloc_detail.driver_18;
                  driver_values(19) := tax_alloc_detail.driver_19;
                  driver_values(20) := tax_alloc_detail.driver_20;
                  driver_values(21) := tax_alloc_detail.driver_21;
                  driver_values(22) := tax_alloc_detail.driver_22;
                  driver_values(23) := tax_alloc_detail.driver_23;
                  driver_values(24) := tax_alloc_detail.driver_24;
                  driver_values(25) := tax_alloc_detail.driver_25;
                  driver_values(26) := tax_alloc_detail.driver_26;
                  driver_values(27) := tax_alloc_detail.driver_27;
                  driver_values(28) := tax_alloc_detail.driver_28;
                  driver_values(29) := tax_alloc_detail.driver_29;
                  driver_values(30) := tax_alloc_detail.driver_30;
		   			v_allocation_subtrans_id := NULL;
                  v_tax_alloc_matrix_dtl_id := tax_alloc_detail.tax_alloc_matrix_dtl_id;
                  v_total_rows := v_total_rows + 1;

                  -- If global jurisdiction flag not on, check for allocation line jurisdictions
                  IF vc_jur_alloc_flag <> 'G' THEN
                     vn_jur_alloc_total_rows := 0;
                     SELECT count(*)
                       INTO vn_jur_alloc_total_rows
                       FROM tb_jur_alloc_matrix_detail
				          WHERE tb_jur_alloc_matrix_detail.tax_alloc_matrix_id = v_tax_alloc_matrix_id
                        AND tb_jur_alloc_matrix_detail.tax_alloc_matrix_dtl_id = v_tax_alloc_matrix_dtl_id;
                     IF vn_jur_alloc_total_rows > 0 THEN
                        vc_jur_alloc_flag := 'L';
                        v_global_or_line_dtl_id := v_tax_alloc_matrix_dtl_id;
                     END IF;
                  END IF;

                  -- Fill other fields from Tax Allocation Matrix Detail
                  v_tax_gl_line_itm_dist_amt := Round(v_tax_orig_gl_line_itm * tax_alloc_detail.allocation_percent,2);
                  v_tax_accum_alloc := v_tax_accum_alloc + v_tax_gl_line_itm_dist_amt;

                  -- Check for last Tax Allocation and rounding
                  IF tax_alloc_detail_cursor%ROWCOUNT = vn_tax_alloc_total_rows THEN
                     -- Check rounding
                     v_tax_delta := v_tax_orig_gl_line_itm - v_tax_accum_alloc;
                     IF v_tax_delta <> 0 THEN
                        v_tax_gl_line_itm_dist_amt := v_tax_gl_line_itm_dist_amt + v_tax_delta;
                     END IF;
                  END IF;

                  -- Search Jurisdiction Allocation Matrix lines
                  IF vc_jur_alloc_flag = 'G' OR vc_jur_alloc_flag = 'L' THEN
                     v_tax_multi_trans_code := 'OA';
                     v_jur_auto_trans_state_code := v_transaction_state_code;
                     v_jur_orig_gl_line_itm := v_tax_gl_line_itm_dist_amt;
                     v_jur_accum_alloc := 0;
                     v_jur_delta := 0;

                     -- Get next Split Allocation sub-trans ID for the new transaction detail records
                     SELECT sq_tb_allocation_subtrans_id.NEXTVAL
                       INTO v_allocation_subtrans_id
                       FROM DUAL;

                     -- Open and Fetch Jurisdiction Allocations
                     OPEN jur_alloc_detail_cursor;
                     LOOP
                        FETCH jur_alloc_detail_cursor INTO jur_alloc_detail;
                        EXIT WHEN jur_alloc_detail_cursor%NOTFOUND;

                        -- Add Tax Jurisdiction Allocation rows to total imported rows
                        v_total_rows := v_total_rows + 1;
                        v_jur_jurisdiction_id := jur_alloc_detail.jurisdiction_id;
                        v_jur_transaction_country_code := jur_alloc_detail.country;
                        v_jur_transaction_state_code := jur_alloc_detail.state;

                        -- Fill other fields from allocation matrix detail
                        v_jur_gl_line_itm_dist_amt := Round(v_jur_orig_gl_line_itm * jur_alloc_detail.allocation_percent,2);
                        v_jur_accum_alloc := v_jur_accum_alloc + v_jur_gl_line_itm_dist_amt;

                        -- Check for last allocation and rounding
                        IF jur_alloc_detail_cursor%ROWCOUNT = vn_jur_alloc_total_rows THEN
                           -- Check rounding
                           v_jur_delta := v_jur_orig_gl_line_itm - v_jur_accum_alloc;
                           IF v_jur_delta <> 0 THEN
                              v_jur_gl_line_itm_dist_amt := v_jur_gl_line_itm_dist_amt + v_jur_delta;
                           END IF;
                        END IF;

                        -- Get next sequence
                        SELECT sq_tb_bcp_transactions.NEXTVAL
                          INTO v_bcp_transaction_id
                          FROM DUAL;

                        -- Insert Jurisdiction Allocation transaction detail rows
                        BEGIN
							      INSERT INTO tb_bcp_transactions (
                              transaction_detail_id,
                              source_transaction_id,
                              process_batch_no,
                              gl_extract_batch_no,
                              archive_batch_no,
                              allocation_matrix_id,
                              allocation_subtrans_id,
                              entered_date,
                              transaction_status,
                              gl_date,
                              gl_company_nbr,
                              gl_company_name,
                              gl_division_nbr,
                              gl_division_name,
                              gl_cc_nbr_dept_id,
                              gl_cc_nbr_dept_name,
                              gl_local_acct_nbr,
                              gl_local_acct_name,
                              gl_local_sub_acct_nbr,
                              gl_local_sub_acct_name,
                              gl_full_acct_nbr,
                              gl_full_acct_name,
                              gl_line_itm_dist_amt,
                              orig_gl_line_itm_dist_amt,
                              vendor_nbr,
                              vendor_name,
                              vendor_address_line_1,
                              vendor_address_line_2,
                              vendor_address_line_3,
                              vendor_address_line_4,
                              vendor_address_city,
                              vendor_address_county,
                              vendor_address_state,
                              vendor_address_zip,
                              vendor_address_country,
                              vendor_type,
                              vendor_type_name,
                              invoice_nbr,
                              invoice_desc,
                              invoice_date,
                              invoice_freight_amt,
                              invoice_discount_amt,
                              invoice_tax_amt,
                              invoice_total_amt,
                              invoice_tax_flg,
                              invoice_line_nbr,
                              invoice_line_name,
                              invoice_line_type,
                              invoice_line_type_name,
                              invoice_line_amt,
                              invoice_line_tax,
                              afe_project_nbr,
                              afe_project_name,
                              afe_category_nbr,
                              afe_category_name,
                              afe_sub_cat_nbr,
                              afe_sub_cat_name,
                              afe_use,
                              afe_contract_type,
                              afe_contract_structure,
                              afe_property_cat,
                              inventory_nbr,
                              inventory_name,
                              inventory_class,
                              inventory_class_name,
                              po_nbr,
                              po_name,
                              po_date,
                              po_line_nbr,
                              po_line_name,
                              po_line_type,
                              po_line_type_name,
                              ship_to_location,
                              ship_to_location_name,
                              ship_to_address_line_1,
                              ship_to_address_line_2,
                              ship_to_address_line_3,
                              ship_to_address_line_4,
                              ship_to_address_city,
                              ship_to_address_county,
                              ship_to_address_state,
                              ship_to_address_zip,
                              ship_to_address_country,
                              wo_nbr,
                              wo_name,
                              wo_date,
                              wo_type,
                              wo_type_desc,
                              wo_class,
                              wo_class_desc,
                              wo_entity,
                              wo_entity_desc,
                              wo_line_nbr,
                              wo_line_name,
                              wo_line_type,
                              wo_line_type_desc,
                              wo_shut_down_cd,
                              wo_shut_down_cd_desc,
                              voucher_id,
                              voucher_name,
                              voucher_date,
                              voucher_line_nbr,
                              voucher_line_desc,
                              check_nbr,
                              check_no,
                              check_date,
                              check_amt,
                              check_desc,
                              user_text_01,
                              user_text_02,
                              user_text_03,
                              user_text_04,
                              user_text_05,
                              user_text_06,
                              user_text_07,
                              user_text_08,
                              user_text_09,
                              user_text_10,
                              user_text_11,
                              user_text_12,
                              user_text_13,
                              user_text_14,
                              user_text_15,
                              user_text_16,
                              user_text_17,
                              user_text_18,
                              user_text_19,
                              user_text_20,
                              user_text_21,
                              user_text_22,
                              user_text_23,
                              user_text_24,
                              user_text_25,
                              user_text_26,
                              user_text_27,
                              user_text_28,
                              user_text_29,
                              user_text_30,
                              user_number_01,
                              user_number_02,
                              user_number_03,
                              user_number_04,
                              user_number_05,
                              user_number_06,
                              user_number_07,
                              user_number_08,
                              user_number_09,
                              user_number_10,
                              user_date_01,
                              user_date_02,
                              user_date_03,
                              user_date_04,
                              user_date_05,
                              user_date_06,
                              user_date_07,
                              user_date_08,
                              user_date_09,
                              user_date_10,
                              comments,
                              tb_calc_tax_amt,
                              state_use_amount,
                              state_use_tier2_amount,
                              state_use_tier3_amount,
                              county_use_amount,
                              county_local_use_amount,
                              city_use_amount,
                              city_local_use_amount,
                              transaction_state_code,
                              auto_transaction_state_code,
                              transaction_ind,
                              suspend_ind,
                              taxcode_detail_id,
                              taxcode_state_code,
                              taxcode_type_code,
                              taxcode_code,
                              cch_taxcat_code,
                              cch_group_code,
                              cch_item_code,
                              manual_taxcode_ind,
                              tax_matrix_id,
                              location_matrix_id,
                              jurisdiction_id,
                              jurisdiction_taxrate_id,
                              manual_jurisdiction_ind,
                              measure_type_code,
                              state_use_rate,
                              state_use_tier2_rate,
                              state_use_tier3_rate,
                              state_split_amount,
                              state_tier2_min_amount,
                              state_tier2_max_amount,
                              state_maxtax_amount,
                              county_use_rate,
                              county_local_use_rate,
                              county_split_amount,
                              county_maxtax_amount,
                              county_single_flag,
                              county_default_flag,
                              city_use_rate,
                              city_local_use_rate,
                              city_split_amount,
                              city_split_use_rate,
                              city_single_flag,
                              city_default_flag,
                              combined_use_rate,
                              load_timestamp,
                              gl_extract_updater,
                              gl_extract_timestamp,
                              gl_extract_flag,
                              gl_log_flag,
                              gl_extract_amt,
                              audit_flag,
                              audit_user_id,
                              audit_timestamp,
                              modify_user_id,
                              modify_timestamp,
                              update_user_id,
                              update_timestamp,
                              bcp_transaction_id,
                              country_use_amount,
                              transaction_country_code,
                              auto_transaction_country_code,
                              taxcode_country_code,
                              country_use_rate,
                              split_subtrans_id,
                              multi_trans_code,
                              tax_alloc_matrix_id )
                           VALUES (
                              v_transaction_detail_id,
                              bcp_transactions.source_transaction_id,
                              bcp_transactions.process_batch_no,
                              bcp_transactions.gl_extract_batch_no,
                              bcp_transactions.archive_batch_no,
                              -1,
                              v_allocation_subtrans_id,
                              bcp_transactions.entered_date,
                              bcp_transactions.transaction_status,
                              bcp_transactions.gl_date,
                              bcp_transactions.gl_company_nbr,
                              bcp_transactions.gl_company_name,
                              bcp_transactions.gl_division_nbr,
                              bcp_transactions.gl_division_name,
                              bcp_transactions.gl_cc_nbr_dept_id,
                              bcp_transactions.gl_cc_nbr_dept_name,
                              bcp_transactions.gl_local_acct_nbr,
                              bcp_transactions.gl_local_acct_name,
                              bcp_transactions.gl_local_sub_acct_nbr,
                              bcp_transactions.gl_local_sub_acct_name,
                              bcp_transactions.gl_full_acct_nbr,
                              bcp_transactions.gl_full_acct_name,
                              v_jur_gl_line_itm_dist_amt,
                              v_jur_orig_gl_line_itm,
                              bcp_transactions.vendor_nbr,
                              bcp_transactions.vendor_name,
                              bcp_transactions.vendor_address_line_1,
                              bcp_transactions.vendor_address_line_2,
                              bcp_transactions.vendor_address_line_3,
                              bcp_transactions.vendor_address_line_4,
                              bcp_transactions.vendor_address_city,
                              bcp_transactions.vendor_address_county,
                              bcp_transactions.vendor_address_state,
                              bcp_transactions.vendor_address_zip,
                              bcp_transactions.vendor_address_country,
                              bcp_transactions.vendor_type,
                              bcp_transactions.vendor_type_name,
                              bcp_transactions.invoice_nbr,
                              bcp_transactions.invoice_desc,
                              bcp_transactions.invoice_date,
                              bcp_transactions.invoice_freight_amt,
                              bcp_transactions.invoice_discount_amt,
                              bcp_transactions.invoice_tax_amt,
                              bcp_transactions.invoice_total_amt,
                              bcp_transactions.invoice_tax_flg,
                              bcp_transactions.invoice_line_nbr,
                              bcp_transactions.invoice_line_name,
                              bcp_transactions.invoice_line_type,
                              bcp_transactions.invoice_line_type_name,
                              bcp_transactions.invoice_line_amt,
                              bcp_transactions.invoice_line_tax,
                              bcp_transactions.afe_project_nbr,
                              bcp_transactions.afe_project_name,
                              bcp_transactions.afe_category_nbr,
                              bcp_transactions.afe_category_name,
                              bcp_transactions.afe_sub_cat_nbr,
                              bcp_transactions.afe_sub_cat_name,
                              bcp_transactions.afe_use,
                              bcp_transactions.afe_contract_type,
                              bcp_transactions.afe_contract_structure,
                              bcp_transactions.afe_property_cat,
                              bcp_transactions.inventory_nbr,
                              bcp_transactions.inventory_name,
                              bcp_transactions.inventory_class,
                              bcp_transactions.inventory_class_name,
                              bcp_transactions.po_nbr,
                              bcp_transactions.po_name,
                              bcp_transactions.po_date,
                              bcp_transactions.po_line_nbr,
                              bcp_transactions.po_line_name,
                              bcp_transactions.po_line_type,
                              bcp_transactions.po_line_type_name,
                              bcp_transactions.ship_to_location,
                              bcp_transactions.ship_to_location_name,
                              bcp_transactions.ship_to_address_line_1,
                              bcp_transactions.ship_to_address_line_2,
                              bcp_transactions.ship_to_address_line_3,
                              bcp_transactions.ship_to_address_line_4,
                              bcp_transactions.ship_to_address_city,
                              bcp_transactions.ship_to_address_county,
                              bcp_transactions.ship_to_address_state,
                              bcp_transactions.ship_to_address_zip,
                              bcp_transactions.ship_to_address_country,
                              bcp_transactions.wo_nbr,
                              bcp_transactions.wo_name,
                              bcp_transactions.wo_date,
                              bcp_transactions.wo_type,
                              bcp_transactions.wo_type_desc,
                              bcp_transactions.wo_class,
                              bcp_transactions.wo_class_desc,
                              bcp_transactions.wo_entity,
                              bcp_transactions.wo_entity_desc,
                              bcp_transactions.wo_line_nbr,
                              bcp_transactions.wo_line_name,
                              bcp_transactions.wo_line_type,
                              bcp_transactions.wo_line_type_desc,
                              bcp_transactions.wo_shut_down_cd,
                              bcp_transactions.wo_shut_down_cd_desc,
                              bcp_transactions.voucher_id,
                              bcp_transactions.voucher_name,
                              bcp_transactions.voucher_date,
                              bcp_transactions.voucher_line_nbr,
                              bcp_transactions.voucher_line_desc,
                              bcp_transactions.check_nbr,
                              bcp_transactions.check_no,
                              bcp_transactions.check_date,
                              bcp_transactions.check_amt,
                              bcp_transactions.check_desc,
                              bcp_transactions.user_text_01,
                              bcp_transactions.user_text_02,
                              bcp_transactions.user_text_03,
                              bcp_transactions.user_text_04,
                              bcp_transactions.user_text_05,
                              bcp_transactions.user_text_06,
                              bcp_transactions.user_text_07,
                              bcp_transactions.user_text_08,
                              bcp_transactions.user_text_09,
                              bcp_transactions.user_text_10,
                              bcp_transactions.user_text_11,
                              bcp_transactions.user_text_12,
                              bcp_transactions.user_text_13,
                              bcp_transactions.user_text_14,
                              bcp_transactions.user_text_15,
                              bcp_transactions.user_text_16,
                              bcp_transactions.user_text_17,
                              bcp_transactions.user_text_18,
                              bcp_transactions.user_text_19,
                              bcp_transactions.user_text_20,
                              bcp_transactions.user_text_21,
                              bcp_transactions.user_text_22,
                              bcp_transactions.user_text_23,
                              bcp_transactions.user_text_24,
                              bcp_transactions.user_text_25,
                              bcp_transactions.user_text_26,
                              bcp_transactions.user_text_27,
                              bcp_transactions.user_text_28,
                              bcp_transactions.user_text_29,
                              bcp_transactions.user_text_30,
                              bcp_transactions.user_number_01,
                              bcp_transactions.user_number_02,
                              bcp_transactions.user_number_03,
                              bcp_transactions.user_number_04,
                              bcp_transactions.user_number_05,
                              bcp_transactions.user_number_06,
                              bcp_transactions.user_number_07,
                              bcp_transactions.user_number_08,
                              bcp_transactions.user_number_09,
                              bcp_transactions.user_number_10,
                              bcp_transactions.user_date_01,
                              bcp_transactions.user_date_02,
                              bcp_transactions.user_date_03,
                              bcp_transactions.user_date_04,
                              bcp_transactions.user_date_05,
                              bcp_transactions.user_date_06,
                              bcp_transactions.user_date_07,
                              bcp_transactions.user_date_08,
                              bcp_transactions.user_date_09,
                              bcp_transactions.user_date_10,
                              bcp_transactions.comments,
                              bcp_transactions.tb_calc_tax_amt,
                              bcp_transactions.state_use_amount,
                              bcp_transactions.state_use_tier2_amount,
                              bcp_transactions.state_use_tier3_amount,
                              bcp_transactions.county_use_amount,
                              bcp_transactions.county_local_use_amount,
                              bcp_transactions.city_use_amount,
                              bcp_transactions.city_local_use_amount,
                              v_jur_transaction_state_code,
                              v_jur_auto_trans_state_code,
                              bcp_transactions.transaction_ind,
                              bcp_transactions.suspend_ind,
                              bcp_transactions.taxcode_detail_id,
                              bcp_transactions.taxcode_state_code,
                              bcp_transactions.taxcode_type_code,
                              bcp_transactions.taxcode_code,
                              bcp_transactions.cch_taxcat_code,
                              bcp_transactions.cch_group_code,
                              bcp_transactions.cch_item_code,
                              bcp_transactions.manual_taxcode_ind,
                              bcp_transactions.tax_matrix_id,
                              bcp_transactions.location_matrix_id,
                              v_jur_jurisdiction_id,
                              bcp_transactions.jurisdiction_taxrate_id,
                              bcp_transactions.manual_jurisdiction_ind,
                              bcp_transactions.measure_type_code,
                              bcp_transactions.state_use_rate,
                              bcp_transactions.state_use_tier2_rate,
                              bcp_transactions.state_use_tier3_rate,
                              bcp_transactions.state_split_amount,
                              bcp_transactions.state_tier2_min_amount,
                              bcp_transactions.state_tier2_max_amount,
                              bcp_transactions.state_maxtax_amount,
                              bcp_transactions.county_use_rate,
                              bcp_transactions.county_local_use_rate,
                              bcp_transactions.county_split_amount,
                              bcp_transactions.county_maxtax_amount,
                              bcp_transactions.county_single_flag,
                              bcp_transactions.county_default_flag,
                              bcp_transactions.city_use_rate,
                              bcp_transactions.city_local_use_rate,
                              bcp_transactions.city_split_amount,
                              bcp_transactions.city_split_use_rate,
                              bcp_transactions.city_single_flag,
                              bcp_transactions.city_default_flag,
                              bcp_transactions.combined_use_rate,
                              bcp_transactions.load_timestamp,
                              bcp_transactions.gl_extract_updater,
                              bcp_transactions.gl_extract_timestamp,
                              bcp_transactions.gl_extract_flag,
                              bcp_transactions.gl_log_flag,
                              bcp_transactions.gl_extract_amt,
                              bcp_transactions.audit_flag,
                              bcp_transactions.audit_user_id,
                              bcp_transactions.audit_timestamp,
                              bcp_transactions.modify_user_id,
                              bcp_transactions.modify_timestamp,
                              bcp_transactions.update_user_id,
                              bcp_transactions.update_timestamp,
                              v_bcp_transaction_id,
                              bcp_transactions.country_use_amount,
                              bcp_transactions.transaction_country_code,
                              bcp_transactions.auto_transaction_country_code,
                              bcp_transactions.taxcode_country_code,
                              bcp_transactions.country_use_rate,
                              v_split_subtrans_id,
                              'A',
                              v_tax_alloc_matrix_id );
                           -- Error Checking
                           IF SQLCODE != 0 THEN
                              RAISE e_badwrite;
                           END IF;
                        EXCEPTION
                           WHEN e_badwrite THEN
                              vi_error_count := vi_error_count + 1;
                              sp_geterrorcode('AL6a', an_batch_id, 'TAL', v_sysdate, v_severity_level, v_write_import_line_flag, v_abort_import_flag);
                              IF v_severity_level > v_error_sev_code THEN
                                 v_error_sev_code := v_severity_level;
                              END IF;
                        END;
                        -- Exit if aborted
                        IF v_abort_import_flag = '1' THEN
                           v_batch_status_code := 'ABTAL';
                           RAISE e_abort;
                        END IF;

                        -- Update columns
                        FOR idx IN 1..vn_driver_index LOOP
                           IF driver_values(idx) <> '*ALL' THEN
                              vc_fieldname := trans_dtl_column_names(idx);
                              vc_fieldvalue := driver_values(idx);
                              vc_driver_sql := 'UPDATE tb_bcp_transactions SET ' || vc_fieldname || ' = ''' || vc_fieldvalue || ''' WHERE bcp_transaction_id=' || v_bcp_transaction_id;
                              --bind--vc_driver_sql := 'UPDATE tb_bcp_transactions SET :fieldname = :fieldvalue WHERE bcp_transaction_id = :bcp_transaction_id';
                              EXECUTE IMMEDIATE (vc_driver_sql);
                              --bind--EXECUTE IMMEDIATE (vc_driver_sql) USING vc_fieldname, vc_fieldvalue, v_bcp_transaction_id;
                           END IF;
                        END LOOP;
                     END LOOP;
                     CLOSE jur_alloc_detail_cursor;
                  END IF;

                  -- Get next sequence
                  SELECT sq_tb_bcp_transactions.NEXTVAL
                    INTO v_bcp_transaction_id
                    FROM DUAL;

                  -- Insert transaction detail row
                  BEGIN
                     INSERT INTO tb_bcp_transactions (
                        transaction_detail_id,
                        source_transaction_id,
                        process_batch_no,
                        gl_extract_batch_no,
                        archive_batch_no,
                        allocation_matrix_id,
                        allocation_subtrans_id,
                        entered_date,
                        transaction_status,
                        gl_date,
                        gl_company_nbr,
                        gl_company_name,
                        gl_division_nbr,
                        gl_division_name,
                        gl_cc_nbr_dept_id,
                        gl_cc_nbr_dept_name,
                        gl_local_acct_nbr,
                        gl_local_acct_name,
                        gl_local_sub_acct_nbr,
                        gl_local_sub_acct_name,
                        gl_full_acct_nbr,
                        gl_full_acct_name,
                        gl_line_itm_dist_amt,
                        orig_gl_line_itm_dist_amt,
                        vendor_nbr,
                        vendor_name,
                        vendor_address_line_1,
                        vendor_address_line_2,
                        vendor_address_line_3,
                        vendor_address_line_4,
                        vendor_address_city,
                        vendor_address_county,
                        vendor_address_state,
                        vendor_address_zip,
                        vendor_address_country,
                        vendor_type,
                        vendor_type_name,
                        invoice_nbr,
                        invoice_desc,
                        invoice_date,
                        invoice_freight_amt,
                        invoice_discount_amt,
                        invoice_tax_amt,
                        invoice_total_amt,
                        invoice_tax_flg,
                        invoice_line_nbr,
                        invoice_line_name,
                        invoice_line_type,
                        invoice_line_type_name,
                        invoice_line_amt,
                        invoice_line_tax,
                        afe_project_nbr,
                        afe_project_name,
                        afe_category_nbr,
                        afe_category_name,
                        afe_sub_cat_nbr,
                        afe_sub_cat_name,
                        afe_use,
                        afe_contract_type,
                        afe_contract_structure,
                        afe_property_cat,
                        inventory_nbr,
                        inventory_name,
                        inventory_class,
                        inventory_class_name,
                        po_nbr,
                        po_name,
                        po_date,
                        po_line_nbr,
                        po_line_name,
                        po_line_type,
                        po_line_type_name,
                        ship_to_location,
                        ship_to_location_name,
                        ship_to_address_line_1,
                        ship_to_address_line_2,
                        ship_to_address_line_3,
                        ship_to_address_line_4,
                        ship_to_address_city,
                        ship_to_address_county,
                        ship_to_address_state,
                        ship_to_address_zip,
                        ship_to_address_country,
                        wo_nbr,
                        wo_name,
                        wo_date,
                        wo_type,
                        wo_type_desc,
                        wo_class,
                        wo_class_desc,
                        wo_entity,
                        wo_entity_desc,
                        wo_line_nbr,
                        wo_line_name,
                        wo_line_type,
                        wo_line_type_desc,
                        wo_shut_down_cd,
                        wo_shut_down_cd_desc,
                        voucher_id,
                        voucher_name,
                        voucher_date,
                        voucher_line_nbr,
                        voucher_line_desc,
                        check_nbr,
                        check_no,
                        check_date,
                        check_amt,
                        check_desc,
                        user_text_01,
                        user_text_02,
                        user_text_03,
                        user_text_04,
                        user_text_05,
                        user_text_06,
                        user_text_07,
                        user_text_08,
                        user_text_09,
                        user_text_10,
                        user_text_11,
                        user_text_12,
                        user_text_13,
                        user_text_14,
                        user_text_15,
                        user_text_16,
                        user_text_17,
                        user_text_18,
                        user_text_19,
                        user_text_20,
                        user_text_21,
                        user_text_22,
                        user_text_23,
                        user_text_24,
                        user_text_25,
                        user_text_26,
                        user_text_27,
                        user_text_28,
                        user_text_29,
                        user_text_30,
                        user_number_01,
                        user_number_02,
                        user_number_03,
                        user_number_04,
                        user_number_05,
                        user_number_06,
                        user_number_07,
                        user_number_08,
                        user_number_09,
                        user_number_10,
                        user_date_01,
                        user_date_02,
                        user_date_03,
                        user_date_04,
                        user_date_05,
                        user_date_06,
                        user_date_07,
                        user_date_08,
                        user_date_09,
                        user_date_10,
                        comments,
                        tb_calc_tax_amt,
                        state_use_amount,
                        state_use_tier2_amount,
                        state_use_tier3_amount,
                        county_use_amount,
                        county_local_use_amount,
                        city_use_amount,
                        city_local_use_amount,
                        transaction_state_code,
                        auto_transaction_state_code,
                        transaction_ind,
                        suspend_ind,
                        taxcode_detail_id,
                        taxcode_state_code,
                        taxcode_type_code,
                        taxcode_code,
                        cch_taxcat_code,
                        cch_group_code,
                        cch_item_code,
                        manual_taxcode_ind,
                        tax_matrix_id,
                        location_matrix_id,
                        jurisdiction_id,
                        jurisdiction_taxrate_id,
                        manual_jurisdiction_ind,
                        measure_type_code,
                        state_use_rate,
                        state_use_tier2_rate,
                        state_use_tier3_rate,
                        state_split_amount,
                        state_tier2_min_amount,
                        state_tier2_max_amount,
                        state_maxtax_amount,
                        county_use_rate,
                        county_local_use_rate,
                        county_split_amount,
                        county_maxtax_amount,
                        county_single_flag,
                        county_default_flag,
                        city_use_rate,
                        city_local_use_rate,
                        city_split_amount,
                        city_split_use_rate,
                        city_single_flag,
                        city_default_flag,
                        combined_use_rate,
                        load_timestamp,
                        gl_extract_updater,
                        gl_extract_timestamp,
                        gl_extract_flag,
                        gl_log_flag,
                        gl_extract_amt,
                        audit_flag,
                        audit_user_id,
                        audit_timestamp,
                        modify_user_id,
                        modify_timestamp,
                        update_user_id,
                        update_timestamp,
                        bcp_transaction_id,
                        country_use_amount,
                        transaction_country_code,
                        auto_transaction_country_code,
                        taxcode_country_code,
                        country_use_rate,
                        split_subtrans_id,
                        multi_trans_code,
                        tax_alloc_matrix_id )
                     VALUES (
                        v_transaction_detail_id,
                        bcp_transactions.source_transaction_id,
                        bcp_transactions.process_batch_no,
                        bcp_transactions.gl_extract_batch_no,
                        bcp_transactions.archive_batch_no,
                        bcp_transactions.allocation_matrix_id,
                        v_allocation_subtrans_id,
                        bcp_transactions.entered_date,
                        bcp_transactions.transaction_status,
                        bcp_transactions.gl_date,
                        bcp_transactions.gl_company_nbr,
                        bcp_transactions.gl_company_name,
                        bcp_transactions.gl_division_nbr,
                        bcp_transactions.gl_division_name,
                        bcp_transactions.gl_cc_nbr_dept_id,
                        bcp_transactions.gl_cc_nbr_dept_name,
                        bcp_transactions.gl_local_acct_nbr,
                        bcp_transactions.gl_local_acct_name,
                        bcp_transactions.gl_local_sub_acct_nbr,
                        bcp_transactions.gl_local_sub_acct_name,
                        bcp_transactions.gl_full_acct_nbr,
                        bcp_transactions.gl_full_acct_name,
                        v_tax_gl_line_itm_dist_amt,
                        v_tax_orig_gl_line_itm,
                        bcp_transactions.vendor_nbr,
                        bcp_transactions.vendor_name,
                        bcp_transactions.vendor_address_line_1,
                        bcp_transactions.vendor_address_line_2,
                        bcp_transactions.vendor_address_line_3,
                        bcp_transactions.vendor_address_line_4,
                        bcp_transactions.vendor_address_city,
                        bcp_transactions.vendor_address_county,
                        bcp_transactions.vendor_address_state,
                        bcp_transactions.vendor_address_zip,
                        bcp_transactions.vendor_address_country,
                        bcp_transactions.vendor_type,
                        bcp_transactions.vendor_type_name,
                        bcp_transactions.invoice_nbr,
                        bcp_transactions.invoice_desc,
                        bcp_transactions.invoice_date,
                        bcp_transactions.invoice_freight_amt,
                        bcp_transactions.invoice_discount_amt,
                        bcp_transactions.invoice_tax_amt,
                        bcp_transactions.invoice_total_amt,
                        bcp_transactions.invoice_tax_flg,
                        bcp_transactions.invoice_line_nbr,
                        bcp_transactions.invoice_line_name,
                        bcp_transactions.invoice_line_type,
                        bcp_transactions.invoice_line_type_name,
                        bcp_transactions.invoice_line_amt,
                        bcp_transactions.invoice_line_tax,
                        bcp_transactions.afe_project_nbr,
                        bcp_transactions.afe_project_name,
                        bcp_transactions.afe_category_nbr,
                        bcp_transactions.afe_category_name,
                        bcp_transactions.afe_sub_cat_nbr,
                        bcp_transactions.afe_sub_cat_name,
                        bcp_transactions.afe_use,
                        bcp_transactions.afe_contract_type,
                        bcp_transactions.afe_contract_structure,
                        bcp_transactions.afe_property_cat,
                        bcp_transactions.inventory_nbr,
                        bcp_transactions.inventory_name,
                        bcp_transactions.inventory_class,
                        bcp_transactions.inventory_class_name,
                        bcp_transactions.po_nbr,
                        bcp_transactions.po_name,
                        bcp_transactions.po_date,
                        bcp_transactions.po_line_nbr,
                        bcp_transactions.po_line_name,
                        bcp_transactions.po_line_type,
                        bcp_transactions.po_line_type_name,
                        bcp_transactions.ship_to_location,
                        bcp_transactions.ship_to_location_name,
                        bcp_transactions.ship_to_address_line_1,
                        bcp_transactions.ship_to_address_line_2,
                        bcp_transactions.ship_to_address_line_3,
                        bcp_transactions.ship_to_address_line_4,
                        bcp_transactions.ship_to_address_city,
                        bcp_transactions.ship_to_address_county,
                        bcp_transactions.ship_to_address_state,
                        bcp_transactions.ship_to_address_zip,
                        bcp_transactions.ship_to_address_country,
                        bcp_transactions.wo_nbr,
                        bcp_transactions.wo_name,
                        bcp_transactions.wo_date,
                        bcp_transactions.wo_type,
                        bcp_transactions.wo_type_desc,
                        bcp_transactions.wo_class,
                        bcp_transactions.wo_class_desc,
                        bcp_transactions.wo_entity,
                        bcp_transactions.wo_entity_desc,
                        bcp_transactions.wo_line_nbr,
                        bcp_transactions.wo_line_name,
                        bcp_transactions.wo_line_type,
                        bcp_transactions.wo_line_type_desc,
                        bcp_transactions.wo_shut_down_cd,
                        bcp_transactions.wo_shut_down_cd_desc,
                        bcp_transactions.voucher_id,
                        bcp_transactions.voucher_name,
                        bcp_transactions.voucher_date,
                        bcp_transactions.voucher_line_nbr,
                        bcp_transactions.voucher_line_desc,
                        bcp_transactions.check_nbr,
                        bcp_transactions.check_no,
                        bcp_transactions.check_date,
                        bcp_transactions.check_amt,
                        bcp_transactions.check_desc,
                        bcp_transactions.user_text_01,
                        bcp_transactions.user_text_02,
                        bcp_transactions.user_text_03,
                        bcp_transactions.user_text_04,
                        bcp_transactions.user_text_05,
                        bcp_transactions.user_text_06,
                        bcp_transactions.user_text_07,
                        bcp_transactions.user_text_08,
                        bcp_transactions.user_text_09,
                        bcp_transactions.user_text_10,
                        bcp_transactions.user_text_11,
                        bcp_transactions.user_text_12,
                        bcp_transactions.user_text_13,
                        bcp_transactions.user_text_14,
                        bcp_transactions.user_text_15,
                        bcp_transactions.user_text_16,
                        bcp_transactions.user_text_17,
                        bcp_transactions.user_text_18,
                        bcp_transactions.user_text_19,
                        bcp_transactions.user_text_20,
                        bcp_transactions.user_text_21,
                        bcp_transactions.user_text_22,
                        bcp_transactions.user_text_23,
                        bcp_transactions.user_text_24,
                        bcp_transactions.user_text_25,
                        bcp_transactions.user_text_26,
                        bcp_transactions.user_text_27,
                        bcp_transactions.user_text_28,
                        bcp_transactions.user_text_29,
                        bcp_transactions.user_text_30,
                        bcp_transactions.user_number_01,
                        bcp_transactions.user_number_02,
                        bcp_transactions.user_number_03,
                        bcp_transactions.user_number_04,
                        bcp_transactions.user_number_05,
                        bcp_transactions.user_number_06,
                        bcp_transactions.user_number_07,
                        bcp_transactions.user_number_08,
                        bcp_transactions.user_number_09,
                        bcp_transactions.user_number_10,
                        bcp_transactions.user_date_01,
                        bcp_transactions.user_date_02,
                        bcp_transactions.user_date_03,
                        bcp_transactions.user_date_04,
                        bcp_transactions.user_date_05,
                        bcp_transactions.user_date_06,
                        bcp_transactions.user_date_07,
                        bcp_transactions.user_date_08,
                        bcp_transactions.user_date_09,
                        bcp_transactions.user_date_10,
                        bcp_transactions.comments,
                        bcp_transactions.tb_calc_tax_amt,
                        bcp_transactions.state_use_amount,
                        bcp_transactions.state_use_tier2_amount,
                        bcp_transactions.state_use_tier3_amount,
                        bcp_transactions.county_use_amount,
                        bcp_transactions.county_local_use_amount,
                        bcp_transactions.city_use_amount,
                        bcp_transactions.city_local_use_amount,
                        bcp_transactions.transaction_state_code,
                        bcp_transactions.auto_transaction_state_code,
                        bcp_transactions.transaction_ind,
                        bcp_transactions.suspend_ind,
                        bcp_transactions.taxcode_detail_id,
                        bcp_transactions.taxcode_state_code,
                        bcp_transactions.taxcode_type_code,
                        bcp_transactions.taxcode_code,
                        bcp_transactions.cch_taxcat_code,
                        bcp_transactions.cch_group_code,
                        bcp_transactions.cch_item_code,
                        bcp_transactions.manual_taxcode_ind,
                        bcp_transactions.tax_matrix_id,
                        bcp_transactions.location_matrix_id,
                        bcp_transactions.jurisdiction_id,
                        bcp_transactions.jurisdiction_taxrate_id,
                        bcp_transactions.manual_jurisdiction_ind,
                        bcp_transactions.measure_type_code,
                        bcp_transactions.state_use_rate,
                        bcp_transactions.state_use_tier2_rate,
                        bcp_transactions.state_use_tier3_rate,
                        bcp_transactions.state_split_amount,
                        bcp_transactions.state_tier2_min_amount,
                        bcp_transactions.state_tier2_max_amount,
                        bcp_transactions.state_maxtax_amount,
                        bcp_transactions.county_use_rate,
                        bcp_transactions.county_local_use_rate,
                        bcp_transactions.county_split_amount,
                        bcp_transactions.county_maxtax_amount,
                        bcp_transactions.county_single_flag,
                        bcp_transactions.county_default_flag,
                        bcp_transactions.city_use_rate,
                        bcp_transactions.city_local_use_rate,
                        bcp_transactions.city_split_amount,
                        bcp_transactions.city_split_use_rate,
                        bcp_transactions.city_single_flag,
                        bcp_transactions.city_default_flag,
                        bcp_transactions.combined_use_rate,
                        bcp_transactions.load_timestamp,
                        bcp_transactions.gl_extract_updater,
                        bcp_transactions.gl_extract_timestamp,
                        bcp_transactions.gl_extract_flag,
                        bcp_transactions.gl_log_flag,
                        bcp_transactions.gl_extract_amt,
                        bcp_transactions.audit_flag,
                        bcp_transactions.audit_user_id,
                        bcp_transactions.audit_timestamp,
                        bcp_transactions.modify_user_id,
                        bcp_transactions.modify_timestamp,
                        bcp_transactions.update_user_id,
                        bcp_transactions.update_timestamp,
                        v_bcp_transaction_id,
                        bcp_transactions.country_use_amount,
                        bcp_transactions.transaction_country_code,
                        bcp_transactions.auto_transaction_country_code,
                        bcp_transactions.taxcode_country_code,
                        bcp_transactions.country_use_rate,
                        v_split_subtrans_id,
                        v_tax_multi_trans_code,
                        v_tax_alloc_matrix_id );
                     -- Error Checking
                     IF SQLCODE != 0 THEN
                        RAISE e_badwrite;
                     END IF;
                  EXCEPTION
                     WHEN e_badwrite THEN
                        vi_error_count := vi_error_count + 1;
                        sp_geterrorcode('TAL6b', an_batch_id, 'TAL', v_sysdate, v_severity_level, v_write_import_line_flag, v_abort_import_flag);
                        IF v_severity_level > v_error_sev_code THEN
                           v_error_sev_code := v_severity_level;
                        END IF;
                  END;
                  -- Exit if aborted
                  IF v_abort_import_flag = '1' THEN
                     v_batch_status_code := 'ABTAL';
                     RAISE e_abort;
                  END IF;

                  -- Update columns
                  FOR idx IN 1..vn_driver_index LOOP
                     IF driver_values(idx) <> '*ALL' THEN
                        vc_fieldname := trans_dtl_column_names(idx);
                        vc_fieldvalue := driver_values(idx);
					         vc_driver_sql := 'UPDATE tb_bcp_transactions SET ' || vc_fieldname || ' = ''' || vc_fieldvalue || ''' WHERE bcp_transaction_id=' || v_bcp_transaction_id;
					         --bind--vc_driver_sql := 'UPDATE tb_bcp_transactions SET :fieldname = :fieldvalue WHERE bcp_transaction_id = :bcp_transaction_id';
                        EXECUTE IMMEDIATE (vc_driver_sql);
                        --bind--EXECUTE IMMEDIATE (vc_driver_sql) USING vc_fieldname, vc_fieldvalue, v_bcp_transaction_id;
                     END IF;
                  END LOOP;
               END LOOP;
               CLOSE tax_alloc_detail_cursor;
            END IF;

            -- Update Split Transaction
            UPDATE tb_bcp_transactions
               SET multi_trans_code = 'OS',
                   split_subtrans_id = v_split_subtrans_id
             WHERE rowid = v_rowid;
         END IF;
      END IF;
   END LOOP;
   CLOSE bcp_transactions_cursor;

   v_batch_status_code := 'FP';
   -- Update batch with final totals
   UPDATE tb_batch
      SET batch_status_code = v_batch_status_code,
          error_sev_code = v_error_sev_code,
          total_rows = v_total_rows
    WHERE batch_id = an_batch_id;

   COMMIT;

EXCEPTION
   WHEN e_abort THEN
      -- Update batch with error codes
      UPDATE tb_batch
         SET batch_status_code = v_batch_status_code,
             error_sev_code = v_error_sev_code
       WHERE batch_id = an_batch_id;
      COMMIT;
END;
/
