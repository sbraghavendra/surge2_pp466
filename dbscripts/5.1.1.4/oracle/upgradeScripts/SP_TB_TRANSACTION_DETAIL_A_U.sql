CREATE OR REPLACE PROCEDURE Sp_Tb_Transaction_Detail_A_U
		(
		 p_transaction_detail_id IN TB_TRANSACTION_DETAIL_EXCP.transaction_detail_id%TYPE,
		 p_old_taxcode_detail_id IN TB_TRANSACTION_DETAIL_EXCP.old_taxcode_detail_id%TYPE,
		 p_old_taxcode_state_code IN TB_TRANSACTION_DETAIL_EXCP.old_taxcode_state_code%TYPE,
		 p_old_taxcode_type_code IN TB_TRANSACTION_DETAIL_EXCP.old_taxcode_type_code%TYPE,
		 p_old_taxcode_code IN TB_TRANSACTION_DETAIL_EXCP.old_taxcode_code%TYPE,
		 p_old_jurisdiction_id IN TB_TRANSACTION_DETAIL_EXCP.old_jurisdiction_id%TYPE,
		 p_old_jurisdiction_taxrate_id IN TB_TRANSACTION_DETAIL_EXCP.old_jurisdiction_taxrate_id%TYPE,
		 p_old_cch_taxcat_code IN TB_TRANSACTION_DETAIL_EXCP.old_cch_taxcat_code%TYPE,
		 p_old_cch_group_code IN TB_TRANSACTION_DETAIL_EXCP.old_cch_group_code%TYPE,
		 p_old_cch_item_code IN TB_TRANSACTION_DETAIL_EXCP.old_cch_item_code%TYPE,
		 p_new_taxcode_detail_id IN TB_TRANSACTION_DETAIL_EXCP.new_taxcode_detail_id%TYPE,
		 p_new_taxcode_state_code IN TB_TRANSACTION_DETAIL_EXCP.new_taxcode_state_code%TYPE,
		 p_new_taxcode_type_code IN TB_TRANSACTION_DETAIL_EXCP.new_taxcode_type_code%TYPE,
		 p_new_taxcode_code IN TB_TRANSACTION_DETAIL_EXCP.new_taxcode_code%TYPE,
		 p_new_jurisdiction_id IN TB_TRANSACTION_DETAIL_EXCP.new_jurisdiction_id%TYPE,
		 p_new_jurisdiction_taxrate_id IN TB_TRANSACTION_DETAIL_EXCP.new_jurisdiction_taxrate_id%TYPE,
		 p_new_cch_taxcat_code IN TB_TRANSACTION_DETAIL_EXCP.new_cch_taxcat_code%TYPE,
		 p_new_cch_group_code IN TB_TRANSACTION_DETAIL_EXCP.new_cch_group_code%TYPE,
		 p_new_cch_item_code IN TB_TRANSACTION_DETAIL_EXCP.new_cch_item_code%TYPE
		)

	AS
	BEGIN
	   IF (( p_old_taxcode_detail_id IS NOT NULL ) AND p_old_taxcode_detail_id <> p_new_taxcode_detail_id ) OR
	      (( p_old_jurisdiction_id IS NOT NULL ) AND p_old_jurisdiction_id <> p_new_jurisdiction_id ) OR
	      (( p_old_jurisdiction_taxrate_id IS NOT NULL ) AND p_old_jurisdiction_taxrate_id <> p_new_jurisdiction_taxrate_id ) THEN

	      INSERT INTO TB_TRANSACTION_DETAIL_EXCP (
		 transaction_detail_excp_id,
		 transaction_detail_id,
		 old_taxcode_detail_id,
		 old_taxcode_state_code,
		 old_taxcode_type_code,
		 old_taxcode_code,
		 old_jurisdiction_id,
		 old_jurisdiction_taxrate_id,
		 old_cch_taxcat_code,
		 old_cch_group_code,
		 old_cch_item_code,
		 new_taxcode_detail_id,
		 new_taxcode_state_code,
		 new_taxcode_type_code,
		 new_taxcode_code,
		 new_jurisdiction_id,
		 new_jurisdiction_taxrate_id,
		 new_cch_taxcat_code,
		 new_cch_group_code,
		 new_cch_item_code,
		 update_user_id,
		 update_timestamp )
	      VALUES (
		 SQ_TB_TRANS_DETAIL_EXCP_ID.NEXTVAL,
		 p_transaction_detail_id,
		 p_old_taxcode_detail_id,
		 p_old_taxcode_state_code,
		 p_old_taxcode_type_code,
		 p_old_taxcode_code,
		 p_old_jurisdiction_id,
		 p_old_jurisdiction_taxrate_id,
		 p_old_cch_taxcat_code,
		 p_old_cch_group_code,
		 p_old_cch_item_code,
		 p_new_taxcode_detail_id,
		 p_new_taxcode_state_code,
		 p_new_taxcode_type_code,
		 p_new_taxcode_code,
		 p_new_jurisdiction_id,
		 p_new_jurisdiction_taxrate_id,
		 p_new_cch_taxcat_code,
		 p_new_cch_group_code,
		 p_new_cch_item_code,
		 USER,
		 SYS_EXTRACT_UTC(SYSTIMESTAMP) );
	   END IF;
	END;
/

