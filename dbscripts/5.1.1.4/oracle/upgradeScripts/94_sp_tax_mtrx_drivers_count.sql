CREATE OR REPLACE PROCEDURE "STSCORP"."SP_TAX_MTRX_DRIVERS_COUNT" (
 p_where_clause_token IN VARCHAR2
)
AS
BEGIN
  EXECUTE IMMEDIATE
  'INSERT INTO tb_tmp_tax_mtrx_drivers_count
  SELECT * FROM
  (
  SELECT COUNT (DECODE (driver_01, ''*ALL'', NULL, driver_01)) AS driver_01,
         COUNT (DECODE (driver_02, ''*ALL'', NULL, driver_02)) AS driver_02,
         COUNT (DECODE (driver_03, ''*ALL'', NULL, driver_03)) AS driver_03,
         COUNT (DECODE (driver_04, ''*ALL'', NULL, driver_04)) AS driver_04,
         COUNT (DECODE (driver_05, ''*ALL'', NULL, driver_05)) AS driver_05,
         COUNT (DECODE (driver_06, ''*ALL'', NULL, driver_06)) AS driver_06,
         COUNT (DECODE (driver_07, ''*ALL'', NULL, driver_07)) AS driver_07,
         COUNT (DECODE (driver_08, ''*ALL'', NULL, driver_08)) AS driver_08,
         COUNT (DECODE (driver_09, ''*ALL'', NULL, driver_09)) AS driver_09,
         COUNT (DECODE (driver_10, ''*ALL'', NULL, driver_10)) AS driver_10,
         COUNT (DECODE (driver_11, ''*ALL'', NULL, driver_11)) AS driver_11,
         COUNT (DECODE (driver_12, ''*ALL'', NULL, driver_12)) AS driver_12,
         COUNT (DECODE (driver_13, ''*ALL'', NULL, driver_13)) AS driver_13,
         COUNT (DECODE (driver_14, ''*ALL'', NULL, driver_14)) AS driver_14,
         COUNT (DECODE (driver_15, ''*ALL'', NULL, driver_15)) AS driver_15,
         COUNT (DECODE (driver_16, ''*ALL'', NULL, driver_16)) AS driver_16,
         COUNT (DECODE (driver_17, ''*ALL'', NULL, driver_17)) AS driver_17,
         COUNT (DECODE (driver_18, ''*ALL'', NULL, driver_18)) AS driver_18,
         COUNT (DECODE (driver_19, ''*ALL'', NULL, driver_19)) AS driver_19,
         COUNT (DECODE (driver_20, ''*ALL'', NULL, driver_20)) AS driver_20,
         COUNT (DECODE (driver_21, ''*ALL'', NULL, driver_21)) AS driver_21,
         COUNT (DECODE (driver_22, ''*ALL'', NULL, driver_22)) AS driver_22,
         COUNT (DECODE (driver_23, ''*ALL'', NULL, driver_23)) AS driver_23,
         COUNT (DECODE (driver_24, ''*ALL'', NULL, driver_24)) AS driver_24,
         COUNT (DECODE (driver_25, ''*ALL'', NULL, driver_25)) AS driver_25,
         COUNT (DECODE (driver_26, ''*ALL'', NULL, driver_26)) AS driver_26,
         COUNT (DECODE (driver_27, ''*ALL'', NULL, driver_27)) AS driver_27,
         COUNT (DECODE (driver_28, ''*ALL'', NULL, driver_28)) AS driver_28,
         COUNT (DECODE (driver_29, ''*ALL'', NULL, driver_29)) AS driver_29,
         COUNT (DECODE (driver_30, ''*ALL'', NULL, driver_30)) AS driver_30
    FROM "TB_TAX_MATRIX"
    WHERE ( "TB_TAX_MATRIX"."EFFECTIVE_DATE" <= sysdate )
     AND ( "TB_TAX_MATRIX"."EXPIRATION_DATE" >= sysdate ) '||p_where_clause_token||'
  ) WHERE 1=1 ';
END;
