set define off
spool c:\alter_v956.log
set echo on

--For build 956 --

CREATE OR REPLACE function get_tax_matrix (
    p_transaction_detail_id IN tb_transaction_detail.transaction_detail_id%TYPE
)
return varchar2
is
   vc_tax_matrix_where             VARCHAR2(30000) := '';
   tmp                             VARCHAR2(30000) := '';
   vc_state_driver_flag            VARCHAR(10) := '0';
begin
    sp_gen_tax_driver (p_generate_driver_reference    => 'N',
      p_an_batch_id            => NULL,
      p_transaction_table_name    => 'trn',
      p_tax_table_name        => 'mat',
      p_vc_state_driver_flag        => vc_state_driver_flag,
      p_vc_tax_matrix_where        => vc_tax_matrix_where);

  IF vc_tax_matrix_where IS NULL THEN
    vc_tax_matrix_where := '';
  END IF;

  return vc_tax_matrix_where;
end get_tax_matrix;
/


CREATE OR REPLACE function f_get_location_matrix (
    p_transaction_detail_id IN tb_transaction_detail.transaction_detail_id%TYPE
)
return varchar2 is
   vn_location_matrix_id NUMBER := 0;
   vc_location_matrix_select       VARCHAR2(2000) :=
      'SELECT tb_location_matrix.location_matrix_id, ' ||
             'tb_location_matrix.jurisdiction_id, ' ||
             'tb_location_matrix.override_taxtype_code, ' ||
             'tb_location_matrix.state_flag,' ||
             'tb_location_matrix.county_flag,' ||
             'tb_location_matrix.county_local_flag,' ||
             'tb_location_matrix.city_flag,' ||
             'tb_location_matrix.city_local_flag,' ||
             'tb_jurisdiction.state, ' ||
             'tb_jurisdiction.county, ' ||
             'tb_jurisdiction.city, ' ||
             'tb_jurisdiction.zip, ' ||
             'tb_jurisdiction.in_out ' ||
        'FROM tb_location_matrix, ' ||
             'tb_jurisdiction, ' ||
             'tb_tmp_transaction_detail_b_u ' ||
      'WHERE ( tb_tmp_transaction_detail_b_u.transaction_detail_id = :v_transaction_detail_id ) AND ' ||
            '( tb_location_matrix.default_flag = ''0'' AND tb_location_matrix.binary_weight > 0 ) AND ' ||
            '( tb_location_matrix.effective_date <= tb_tmp_transaction_detail_b_u.gl_date ) AND ( tb_location_matrix.expiration_date >= tb_tmp_transaction_detail_b_u.gl_date ) AND ' ||
            '( tb_location_matrix.jurisdiction_id = tb_jurisdiction.jurisdiction_id ) ';
   vc_location_matrix_where        VARCHAR2(28000) := '';
   vc_location_matrix_orderby      VARCHAR2(1000) :=
      'ORDER BY tb_location_matrix.binary_weight DESC, tb_location_matrix.significant_digits DESC, tb_location_matrix.effective_date DESC';
   vc_location_matrix_stmt         VARCHAR2 (31000);
   p_new_tb_transaction_detail     tb_tmp_transaction_detail_b_u%ROWTYPE;
begin
  SELECT * INTO p_new_tb_transaction_detail FROM tb_tmp_transaction_detail_b_u
    WHERE transaction_detail_id = p_transaction_detail_id;
  sp_gen_location_driver (
      p_generate_driver_reference    => 'N',
      p_an_batch_id            => NULL,
      p_transaction_table_name    => 'tb_tmp_transaction_detail_b_u',
      p_location_table_name        => 'tb_location_matrix',
      p_vc_location_matrix_where    => vc_location_matrix_where);

  -- If no drivers found raise error, else create transaction detail sql statement
  IF vc_location_matrix_where IS NULL THEN
    vc_location_matrix_stmt := '';
  ELSE
    vc_location_matrix_stmt := vc_location_matrix_select || vc_location_matrix_where || vc_location_matrix_orderby;
  END IF;
  return vc_location_matrix_stmt;
end f_get_location_matrix;
/


CREATE OR REPLACE procedure sp_get_tax_matrix (
    tax_matrix_cursor OUT SYS_REFCURSOR,
    transaction_detail_id IN tb_transaction_detail.transaction_detail_id%TYPE
)
as
-- Tax Matrix Selection SQL
   vc_tax_matrix_select            VARCHAR2(2000) :=
      'SELECT tb_tax_matrix.tax_matrix_id, tb_tax_matrix.relation_sign, tb_tax_matrix.relation_amount, ' ||
             'tb_tax_matrix.then_hold_code_flag, tb_tax_matrix.else_hold_code_flag, ' ||
             'tb_tax_matrix.then_taxcode_detail_id, tb_tax_matrix.else_taxcode_detail_id, ' ||
             'tb_tax_matrix.then_cch_taxcat_code, tb_tax_matrix.then_cch_group_code, tb_tax_matrix.then_cch_item_code, ' ||
             'tb_tax_matrix.else_cch_taxcat_code, tb_tax_matrix.else_cch_group_code, tb_tax_matrix.else_cch_item_code, ' ||
             'thentaxcddtl.taxcode_state_code, thentaxcddtl.taxcode_type_code, thentaxcddtl.taxcode_code, thentaxcddtl.jurisdiction_id, thentaxcddtl.measure_type_code, thentaxcddtl.taxtype_code, ' ||
             'elsetaxcddtl.taxcode_state_code, elsetaxcddtl.taxcode_type_code, elsetaxcddtl.taxcode_code, elsetaxcddtl.jurisdiction_id, elsetaxcddtl.measure_type_code, elsetaxcddtl.taxtype_code ' ||
       'FROM tb_tax_matrix, ' ||
            'tb_taxcode_detail thentaxcddtl, ' ||
            'tb_taxcode_detail elsetaxcddtl, ' ||
            'tb_tmp_transaction_detail ' ||
      'WHERE ( tb_tmp_transaction_detail.transaction_detail_id = :v_transaction_detail_id ) AND ' ||
            '( tb_tax_matrix.default_flag = ''0'' AND tb_tax_matrix.binary_weight > 0 ) AND ' ||
            '(( tb_tax_matrix.matrix_state_code = ''*ALL'' ) OR ( tb_tax_matrix.matrix_state_code = :v_transaction_state_code )) AND ' ||
            '( tb_tax_matrix.effective_date <= tb_tmp_transaction_detail.gl_date ) AND ( tb_tax_matrix.expiration_date >= tb_tmp_transaction_detail.gl_date ) AND ' ||
            '( tb_tax_matrix.then_taxcode_detail_id = thentaxcddtl.taxcode_detail_id (+)) AND ' ||
            '( tb_tax_matrix.else_taxcode_detail_id = elsetaxcddtl.taxcode_detail_id (+)) ';
   vc_tax_matrix_where             VARCHAR2(28000) := '';
   vc_tax_matrix_orderby           VARCHAR2(1000) :=
      'ORDER BY tb_tax_matrix.binary_weight DESC, tb_tax_matrix.significant_digits DESC, tb_tax_matrix.effective_date DESC';
   vc_tax_matrix_stmt              VARCHAR2 (31000);
--   tax_matrix_cursor               sys_refcursor;
   tax_matrix                      matrix_record_pkg.tax_matrix_record;
   p_new_tb_transaction_detail     tb_tmp_transaction_detail%ROWTYPE;
   v_transaction_detail_id         tb_transaction_detail.transaction_detail_id%TYPE  := NULL;
   v_transaction_state_code        tb_transaction_detail.transaction_state_code%TYPE := NULL;

-- Program defined variables
   vn_fetch_rows                   NUMBER                                            := 0;
   vc_state_driver_flag            CHAR(1)                                           := '0';
   vn_taxable_amt                  NUMBER                                            := 0;
   e_badread                       exception;
BEGIN
  SELECT * INTO p_new_tb_transaction_detail FROM tb_tmp_transaction_detail_b_u
    WHERE transaction_detail_id = transaction_detail_id;
         v_transaction_detail_id := p_new_tb_transaction_detail.transaction_detail_id;
         v_transaction_state_code := p_new_tb_transaction_detail.transaction_state_code;
         IF v_transaction_state_code IS NULL THEN
            vn_fetch_rows := 0;
         ELSE
            SELECT count(*)
              INTO vn_fetch_rows
              FROM tb_taxcode_state
             WHERE taxcode_state_code = v_transaction_state_code;
         END IF;
         IF vn_fetch_rows = 0 THEN
            v_transaction_state_code := NULL;
            BEGIN
               SELECT state
                 INTO v_transaction_state_code
                 FROM tb_jurisdiction
                WHERE jurisdiction_id = p_new_tb_transaction_detail.jurisdiction_id;
               IF SQLCODE != 0 THEN
                  RAISE e_badread;
               END IF;
            EXCEPTION
               WHEN e_badread THEN
                  v_transaction_state_code := NULL;
               WHEN NO_DATA_FOUND THEN
                  v_transaction_state_code := NULL;
            END;
            IF v_transaction_state_code IS NOT NULL THEN
               IF p_new_tb_transaction_detail.transaction_state_code IS NULL THEN
                  p_new_tb_transaction_detail.auto_transaction_state_code := '*NULL';
               ELSE
                  p_new_tb_transaction_detail.auto_transaction_state_code := p_new_tb_transaction_detail.transaction_state_code;
               END IF;
               p_new_tb_transaction_detail.transaction_state_code := v_transaction_state_code;
            ELSE
               p_new_tb_transaction_detail.transaction_ind := 'S';
               p_new_tb_transaction_detail.suspend_ind := 'L';
            END IF;
         END IF;
         IF p_new_tb_transaction_detail.transaction_ind IS NULL OR p_new_tb_transaction_detail.transaction_ind <> 'S' THEN
           sp_gen_tax_driver (p_generate_driver_reference    => 'N',
             p_an_batch_id            => NULL,
             p_transaction_table_name    => 'tb_tmp_transaction_detail',
             p_tax_table_name        => 'tb_tax_matrix',
             p_vc_state_driver_flag        => vc_state_driver_flag,
             p_vc_tax_matrix_where        => vc_tax_matrix_where);   

            IF vc_tax_matrix_where IS NULL THEN
               NULL;
            ELSE
               vc_tax_matrix_stmt := vc_tax_matrix_select || vc_tax_matrix_where || vc_tax_matrix_orderby;
            END IF;

            BEGIN
               IF vc_state_driver_flag = '1' THEN
                   OPEN tax_matrix_cursor
                    FOR vc_tax_matrix_stmt
                  USING v_transaction_detail_id, v_transaction_state_code, v_transaction_state_code;
                  FETCH tax_matrix_cursor
                   INTO tax_matrix;
               ELSE
                   OPEN tax_matrix_cursor
                    FOR vc_tax_matrix_stmt
                  USING v_transaction_detail_id, v_transaction_state_code;
                  FETCH tax_matrix_cursor
                   INTO tax_matrix;
               END IF;
               -- Rows found?
               IF tax_matrix_cursor%FOUND THEN
                  vn_fetch_rows := tax_matrix_cursor%ROWCOUNT;
               ELSE
                  vn_fetch_rows := 0;
               END IF;
               CLOSE tax_matrix_cursor;
               IF SQLCODE != 0 THEN
                  RAISE e_badread;
               END IF;
            EXCEPTION
               WHEN e_badread THEN
                  vn_fetch_rows := 0;
            END;
      ELSE
        OPEN tax_matrix_cursor
          FOR 'select * from tb_tax_matrix where tax_matrix_id < 1';
          FETCH tax_matrix_cursor
          INTO tax_matrix;
      END IF;
end sp_get_tax_matrix;
/


CREATE OR REPLACE procedure sp_get_location_matrix(
    res OUT SYS_REFCURSOR,
    transaction_detail_id IN tb_transaction_detail.transaction_detail_id%TYPE
)
as 
   vc_location_matrix_select       VARCHAR2(2000) :=
      'SELECT tb_location_matrix.location_matrix_id, ' ||
             'tb_location_matrix.jurisdiction_id, ' ||
             'tb_location_matrix.override_taxtype_code, ' ||
             'tb_location_matrix.state_flag,' ||
             'tb_location_matrix.county_flag,' ||
             'tb_location_matrix.county_local_flag,' ||
             'tb_location_matrix.city_flag,' ||
             'tb_location_matrix.city_local_flag,' ||
             'tb_jurisdiction.state, ' ||
             'tb_jurisdiction.county, ' ||
             'tb_jurisdiction.city, ' ||
             'tb_jurisdiction.zip, ' ||
             'tb_jurisdiction.in_out ' ||
        'FROM tb_location_matrix, ' ||
             'tb_jurisdiction, ' ||
             'tb_tmp_transaction_detail ' ||
      'WHERE ( tb_tmp_transaction_detail.transaction_detail_id = :v_transaction_detail_id ) AND ' ||
            '( tb_location_matrix.default_flag = ''0'' AND tb_location_matrix.binary_weight > 0 ) AND ' ||
            '( tb_location_matrix.effective_date <= tb_tmp_transaction_detail.gl_date ) AND ( tb_location_matrix.expiration_date >= tb_tmp_transaction_detail.gl_date ) AND ' ||
            '( tb_location_matrix.jurisdiction_id = tb_jurisdiction.jurisdiction_id ) ';
   vc_location_matrix_where        VARCHAR2(28000) := '';
   vc_location_matrix_orderby      VARCHAR2(1000) :=
      'ORDER BY tb_location_matrix.binary_weight DESC, tb_location_matrix.significant_digits DESC, tb_location_matrix.effective_date DESC';
   vc_location_matrix_stmt         VARCHAR2 (31000);
   location_matrix_cursor          sys_refcursor;
   location_matrix                 matrix_record_pkg.location_matrix_record;
   p_new_tb_transaction_detail     tb_tmp_transaction_detail%ROWTYPE;
   v_transaction_detail_id         tb_transaction_detail.transaction_detail_id%TYPE  := NULL;
   vn_fetch_rows                   NUMBER                                            := 0;
   e_badread                       exception;
begin
   SELECT * INTO p_new_tb_transaction_detail FROM tb_tmp_transaction_detail
     WHERE transaction_detail_id = transaction_detail_id;
   IF p_new_tb_transaction_detail.transaction_ind IS NULL OR p_new_tb_transaction_detail.transaction_ind <> 'S' THEN
     IF UPPER(p_new_tb_transaction_detail.taxcode_type_code) = 'T' THEN
        IF p_new_tb_transaction_detail.jurisdiction_id IS NULL OR p_new_tb_transaction_detail.jurisdiction_id = 0 THEN
           IF p_new_tb_transaction_detail.location_matrix_id IS NULL AND p_new_tb_transaction_detail.manual_jurisdiction_ind IS NULL THEN
               v_transaction_detail_id := p_new_tb_transaction_detail.transaction_detail_id;
               sp_gen_location_driver (
                 p_generate_driver_reference    => 'N',
                 p_an_batch_id            => NULL,
                 p_transaction_table_name    => 'tb_tmp_transaction_detail',
                 p_location_table_name        => 'tb_location_matrix',
                 p_vc_location_matrix_where    => vc_location_matrix_where);

               -- If no drivers found raise error, else create transaction detail sql statement
               IF vc_location_matrix_where IS NULL THEN
                  NULL;
               ELSE
                  vc_location_matrix_stmt := vc_location_matrix_select || vc_location_matrix_where || vc_location_matrix_orderby;
               END IF;

               -- Search Location Matrix for matches
               BEGIN
                   OPEN location_matrix_cursor
                    FOR vc_location_matrix_stmt
                  USING v_transaction_detail_id;
                  FETCH location_matrix_cursor
                   INTO location_matrix;
                  -- Rows found?
                  IF location_matrix_cursor%FOUND THEN
                     vn_fetch_rows := location_matrix_cursor%ROWCOUNT;
                  ELSE
                     vn_fetch_rows := 0;
                  END IF;
                  CLOSE location_matrix_cursor;
                  IF SQLCODE != 0 THEN
                    RAISE e_badread;
                  END IF;
               EXCEPTION
                  WHEN e_badread THEN
                     vn_fetch_rows := 0;
               END;

               -- Location Matrix Line Found
               IF vn_fetch_rows > 0 THEN
                  p_new_tb_transaction_detail.location_matrix_id := location_matrix.location_matrix_id;
                  p_new_tb_transaction_detail.jurisdiction_id := location_matrix.jurisdiction_id;

                  update tb_tmp_transaction_detail set
                     taxcode_detail_id = p_new_tb_transaction_detail.taxcode_detail_id,
                     taxcode_state_code = p_new_tb_transaction_detail.taxcode_state_code,
                     taxcode_type_code = p_new_tb_transaction_detail.taxcode_type_code,
                     taxcode_code = p_new_tb_transaction_detail.taxcode_code,
                     measure_type_code = p_new_tb_transaction_detail.measure_type_code,
                     cch_taxcat_code = p_new_tb_transaction_detail.cch_taxcat_code,
                     cch_group_code = p_new_tb_transaction_detail.cch_group_code
                     where transaction_detail_id = p_new_tb_transaction_detail.transaction_detail_id;
               ELSE
                  p_new_tb_transaction_detail.transaction_detail_id := 0;
               END IF;
            END IF;
        END IF;
     END IF;
   END IF;
   OPEN res
     FOR 'select * from tb_tmp_transaction_detail where transaction_detail_id = ' ||
       to_char(p_new_tb_transaction_detail.transaction_detail_id);
end sp_get_location_matrix;
/





COMMIT;

spool off
