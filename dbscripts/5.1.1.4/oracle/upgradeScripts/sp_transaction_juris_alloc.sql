/*  Jurisdiction Allocations  *********************************************************************/
CREATE OR REPLACE PROCEDURE sp_transaction_juris_alloc(ac_error_code out varchar2 )
/* ************************************************************************************************/
/* Object Type/Name: Stored Procedure - sp_transaction_juris_alloc                                */
/* Author:           Michael B. Fuller                                                            */
/* Date:             04/20/2011                                                                   */
/* Description:      Perfom Jurisdiction Allocations on Transactions                              */
/* Arguments:        None                                                                         */
/* Returns:          ac_error_code(varchar2)                                                      */
/* ************************************************************************************************/
/* Maintenance History:                                                                           */
/* Scan  Author       Date     Release    Description                                  Ticket #   */
/* ----- ---------- ---------- ---------- -------------------------------------------- ---------- */
/*       M. Fuller  04/20/2011            Copied from tb_batch_juris_allocation        1179       */
/* ************************************************************************************************/
IS
-- Define Cursor Type
   TYPE cursor_type is REF CURSOR;

-- Allocation Matrix Selection SQL
   vc_allocation_matrix_select       VARCHAR2(4000) :=
      'SELECT tb_allocation_matrix.allocation_matrix_id ' ||
       'FROM tb_allocation_matrix, ' ||
            'tb_transaction_detail ' ||
      'WHERE ( tb_transaction_detail.rowid = :v_rowid ) AND ' ||
            '( tb_allocation_matrix.effective_date <= tb_transaction_detail.gl_date ) AND ( tb_allocation_matrix.expiration_date >= tb_transaction_detail.gl_date ) ';
   vc_allocation_matrix_where        VARCHAR2(4000) := '';
   vc_allocation_matrix_orderby      VARCHAR2(1000) :=
      'ORDER BY tb_allocation_matrix.binary_weight DESC, tb_allocation_matrix.significant_digits DESC, tb_allocation_matrix.effective_date DESC';
   vc_allocation_matrix_stmt         VARCHAR2 (9000);
   allocation_matrix_cursor          cursor_type;

-- Location Matrix Record TYPE   
   TYPE allocation_matrix_record is RECORD (
      allocation_matrix_id         tb_allocation_matrix.allocation_matrix_id%TYPE );
   allocation_matrix               allocation_matrix_record;

-- Table defined variables
   v_value                         tb_option.value%TYPE                              := NULL;
   v_batch_status_code             tb_batch.batch_status_code%TYPE                   := 'P';
   v_starting_sequence             tb_batch.start_row%TYPE                           := 0;
   v_error_sev_code                tb_batch.error_sev_code%TYPE                      := ' ';
   v_total_rows                    tb_batch.total_rows%TYPE                          := 0;
   v_severity_level                tb_list_code.severity_level%TYPE                  := ' ';
   v_write_import_line_flag        tb_list_code.write_import_line_flag%TYPE          := '1';
   v_abort_import_flag             tb_list_code.abort_import_flag%TYPE               := '0';
   v_allocation_matrix_id          tb_allocation_matrix.allocation_matrix_id%TYPE    := 0;
   v_transaction_detail_id         tb_transaction_detail.transaction_detail_id%TYPE    := 0;
   v_sysdate                       tb_transaction_detail.load_timestamp%TYPE           := SYSDATE;
   v_accum_allocation              tb_transaction_detail.gl_line_itm_dist_amt%TYPE     := 0;
   v_gl_line_itm_dist_amt          tb_transaction_detail.gl_line_itm_dist_amt%TYPE     := 0;
   v_orig_gl_line_itm_dist_amt     tb_transaction_detail.gl_line_itm_dist_amt%TYPE     := 0;
   v_transaction_state_code        tb_transaction_detail.transaction_state_code%TYPE   := NULL;
   v_delta                         tb_transaction_detail.gl_line_itm_dist_amt%TYPE     := 0;
   v_allocation_subtrans_id        tb_transaction_detail.allocation_subtrans_id%TYPE   := NULL;

-- Program defined variables
   v_rowid                         ROWID                                             := NULL;
   vn_fetch_rows                   NUMBER                                            := 0;
   vn_allocation_total_rows        NUMBER                                            := 0;
   vc_state_driver_flag            CHAR(1)                                           := '0';

-- Define Tax Driver Names Cursor (tb_driver_names)
   CURSOR tax_driver_cursor
   IS
        SELECT driver.trans_dtl_column_name, driver.matrix_column_name, driver.null_driver_flag, driver.wildcard_flag,
               datadefcol.desc_column_name, datadefcol.data_type
          FROM tb_driver_names driver,
               (SELECT *
                  FROM tb_data_def_column
                 WHERE table_name = 'TB_TRANSACTION_DETAIL') datadefcol

         WHERE driver.driver_names_code = 'T' AND
               driver.trans_dtl_column_name = datadefcol.column_name
      ORDER BY driver.driver_id;
      tax_driver                   tax_driver_cursor%ROWTYPE;

-- Define Allocation Matrix Detail / Jurisdiction Cursor (tb_allocation_matrix_detail, tb_jurisdiction)
   CURSOR allocation_detail_cursor
   IS
        SELECT tb_allocation_matrix_detail.jurisdiction_id, tb_allocation_matrix_detail.allocation_percent,
               tb_jurisdiction.geocode, tb_jurisdiction.state, tb_jurisdiction.county, tb_jurisdiction.city, tb_jurisdiction.zip
          FROM tb_allocation_matrix_detail,
	       tb_jurisdiction
         WHERE ( tb_allocation_matrix_detail.jurisdiction_id = tb_jurisdiction.jurisdiction_id (+))
           AND ( tb_allocation_matrix_detail.allocation_matrix_id = v_allocation_matrix_id );
      allocation_detail                   allocation_detail_cursor%ROWTYPE;

-- Define BCP Transactions Cursor (tb_transaction_detail)
   CURSOR transaction_detail_cursor
   IS
      SELECT rowid,
             transaction_detail_id,
             source_transaction_id,
             process_batch_no,
             gl_extract_batch_no,
             archive_batch_no,
             allocation_matrix_id,
             allocation_subtrans_id,
             entered_date,
             transaction_status,
             gl_date,
             gl_company_nbr,
             gl_company_name,
             gl_division_nbr,
             gl_division_name,
             gl_cc_nbr_dept_id,
             gl_cc_nbr_dept_name,
             gl_local_acct_nbr,
             gl_local_acct_name,
             gl_local_sub_acct_nbr,
             gl_local_sub_acct_name,
             gl_full_acct_nbr,
             gl_full_acct_name,
             gl_line_itm_dist_amt,
             orig_gl_line_itm_dist_amt,
             vendor_nbr,
             vendor_name,
             vendor_address_line_1,
             vendor_address_line_2,
             vendor_address_line_3,
             vendor_address_line_4,
             vendor_address_city,
             vendor_address_county,
             vendor_address_state,
             vendor_address_zip,
             vendor_address_country,
             vendor_type,
             vendor_type_name,
             invoice_nbr,
             invoice_desc,
             invoice_date,
             invoice_freight_amt,
             invoice_discount_amt,
             invoice_tax_amt,
             invoice_total_amt,
             invoice_tax_flg,
             invoice_line_nbr,
             invoice_line_name,
             invoice_line_type,
             invoice_line_type_name,
             invoice_line_amt,
             invoice_line_tax,
             afe_project_nbr,
             afe_project_name,
             afe_category_nbr,
             afe_category_name,
             afe_sub_cat_nbr,
             afe_sub_cat_name,
             afe_use,
             afe_contract_type,
             afe_contract_structure,
             afe_property_cat,
             inventory_nbr,
             inventory_name,
             inventory_class,
             inventory_class_name,
             po_nbr,
             po_name,
             po_date,
             po_line_nbr,
             po_line_name,
             po_line_type,
             po_line_type_name,
             ship_to_location,
             ship_to_location_name,
             ship_to_address_line_1,
             ship_to_address_line_2,
             ship_to_address_line_3,
             ship_to_address_line_4,
             ship_to_address_city,
             ship_to_address_county,
             ship_to_address_state,
             ship_to_address_zip,
             ship_to_address_country,
             wo_nbr,
             wo_name,
             wo_date,
             wo_type,
             wo_type_desc,
             wo_class,
             wo_class_desc,
             wo_entity,
             wo_entity_desc,
             wo_line_nbr,
             wo_line_name,
             wo_line_type,
             wo_line_type_desc,
             wo_shut_down_cd,
             wo_shut_down_cd_desc,
             voucher_id,
             voucher_name,
             voucher_date,
             voucher_line_nbr,
             voucher_line_desc,
             check_nbr,
             check_no,
             check_date,
             check_amt,
             check_desc,
             user_text_01,
             user_text_02,
             user_text_03,
             user_text_04,
             user_text_05,
             user_text_06,
             user_text_07,
             user_text_08,
             user_text_09,
             user_text_10,
             user_text_11,
             user_text_12,
             user_text_13,
             user_text_14,
             user_text_15,
             user_text_16,
             user_text_17,
             user_text_18,
             user_text_19,
             user_text_20,
             user_text_21,
             user_text_22,
             user_text_23,
             user_text_24,
             user_text_25,
             user_text_26,
             user_text_27,
             user_text_28,
             user_text_29,
             user_text_30,
             user_number_01,
             user_number_02,
             user_number_03,
             user_number_04,
             user_number_05,
             user_number_06,
             user_number_07,
             user_number_08,
             user_number_09,
             user_number_10,
             user_date_01,
             user_date_02,
             user_date_03,
             user_date_04,
             user_date_05,
             user_date_06,
             user_date_07,
             user_date_08,
             user_date_09,
             user_date_10,
             comments,
             tb_calc_tax_amt,
             state_use_amount,
             state_use_tier2_amount,
             state_use_tier3_amount,
             county_use_amount,
             county_local_use_amount,
             city_use_amount,
             city_local_use_amount,
             transaction_state_code,
             auto_transaction_state_code,
             transaction_ind,
             suspend_ind,
             taxcode_detail_id,
             taxcode_state_code,
             taxcode_type_code,
             taxcode_code,
             cch_taxcat_code,
             cch_group_code,
             cch_item_code,
             manual_taxcode_ind,
             tax_matrix_id,
             location_matrix_id,
             jurisdiction_id,
             jurisdiction_taxrate_id,
             manual_jurisdiction_ind,
             measure_type_code,
             state_use_rate,
             state_use_tier2_rate,
             state_use_tier3_rate,
             state_split_amount,
             state_tier2_min_amount,
             state_tier2_max_amount,
             state_maxtax_amount,
             county_use_rate,
             county_local_use_rate,
             county_split_amount,
             county_maxtax_amount,
             county_single_flag,
             county_default_flag,
             city_use_rate,
             city_local_use_rate,
             city_split_amount,
             city_split_use_rate,
             city_single_flag,
             city_default_flag,
             combined_use_rate,
             load_timestamp,
             gl_extract_updater,
             gl_extract_timestamp,
             gl_extract_flag,
             gl_log_flag,
             gl_extract_amt,
             audit_flag,
             audit_user_id,
             audit_timestamp,
             modify_user_id,
             modify_timestamp,
             update_user_id,
             update_timestamp,
             split_subtrans_id,
             multi_trans_code,
             tax_alloc_matrix_id
        FROM tb_transaction_detail
       WHERE transaction_ind IS NULL
         AND (gl_line_itm_dist_amt IS NOT NULL AND gl_line_itm_dist_amt <> 0)
         AND allocation_subtrans_id IS NULL;
      transaction_detail             transaction_detail_cursor%ROWTYPE;

-- Define Exceptions
   e_badread                       exception;
   e_badupdate                     exception;
   e_badwrite                      exception;
   e_wrongdata                     exception;
   e_abort                         exception;

-- ****** Program starts ****** --------------------------------------------------------------------
BEGIN
   -- Confirm allocations are active
   BEGIN
      SELECT value
        INTO v_value
        FROM tb_option
       WHERE option_code = 'ALLOCATIONSENABLED'
         AND option_type_code = 'ADMIN'
         AND user_code = 'ADMIN';
      IF SQLCODE != 0 THEN
         RAISE e_badread;
      ELSIF v_value <> '1' THEN
         RAISE e_wrongdata;
      END IF;
   EXCEPTION
      WHEN NO_DATA_FOUND OR e_badread OR e_wrongdata THEN
         ac_error_code := 'STOP';
         RAISE e_abort;
   END;

   -- ***** Create Dynamic WHERE Clause for Matrix SQLs ***** --
   -- Allocation Matrix Drivers
   OPEN tax_driver_cursor;
   LOOP
      FETCH tax_driver_cursor INTO tax_driver;
      EXIT WHEN tax_driver_cursor%NOTFOUND;
      IF tax_driver.null_driver_flag = '1' THEN
         IF tax_driver.wildcard_flag = '1' THEN
            -- NULL and Wildcard
            IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
               UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                  vc_allocation_matrix_where := vc_allocation_matrix_where || 'AND ' ||
                  '(( tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NULL AND (UPPER(tb_allocation_matrix.' || tax_driver.matrix_column_name || ') = ''*ALL'' OR UPPER(tb_allocation_matrix.' || tax_driver.matrix_column_name || ') = ''*NULL'' )) OR ' ||
                  '( tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND (UPPER(tb_allocation_matrix.' || tax_driver.matrix_column_name || ') = ''*ALL'' OR UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') LIKE UPPER(tb_allocation_matrix.' || tax_driver.matrix_column_name || ')))) ';
            END IF;
         ELSE
            -- NULL and NOT Wildcard
            IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
               UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                  vc_allocation_matrix_where := vc_allocation_matrix_where || 'AND ' ||
                  '(( tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NULL AND (UPPER(tb_allocation_matrix.' || tax_driver.matrix_column_name || ') = ''*ALL'' OR UPPER(tb_allocation_matrix.' || tax_driver.matrix_column_name || ') = ''*NULL'' )) OR ' ||
                  '( tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND (UPPER(tb_allocation_matrix.' || tax_driver.matrix_column_name || ') = ''*ALL'' OR UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') = UPPER(tb_allocation_matrix.' || tax_driver.matrix_column_name || ')))) ';
            END IF;
         END IF;
      ELSE
         IF tax_driver.wildcard_flag = '1' THEN
            -- NOT NULL and Wildcard
            IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
               UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                  vc_allocation_matrix_where := vc_allocation_matrix_where || 'AND ' ||
                  '( UPPER(tb_allocation_matrix.' || tax_driver.matrix_column_name || ') = ''*ALL'' OR UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') LIKE UPPER(tb_allocation_matrix.' || tax_driver.matrix_column_name || ')) ';
            END IF;
         ELSE
            -- NOT NULL and NOT Wildcard
            IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
               UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                  IF tax_driver.trans_dtl_column_name = 'TRANSACTION_STATE_CODE' THEN
                     -- Driver is "transaction_state_code"
                     IF vc_state_driver_flag <> '1' THEN
                        vc_state_driver_flag := '1';
                     END IF;
                     vc_allocation_matrix_where := vc_allocation_matrix_where || 'AND ' ||
                     '( UPPER(tb_allocation_matrix.' || tax_driver.matrix_column_name || ') = ''*ALL'' OR UPPER(:v_transaction_state_code) = UPPER(tb_allocation_matrix.' || tax_driver.matrix_column_name || ')) ';
                  ELSE
                     -- Driver is not "transaction_state_code"
                     vc_allocation_matrix_where := vc_allocation_matrix_where || 'AND ' ||
                     '( UPPER(tb_allocation_matrix.' || tax_driver.matrix_column_name || ') = ''*ALL'' OR UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') = UPPER(tb_allocation_matrix.' || tax_driver.matrix_column_name || ')) ';
                  END IF;
            END IF;
         END IF;
      END IF;
   END LOOP;
   CLOSE tax_driver_cursor;
   -- If no drivers found raise error, else create location matrix sql statement
   IF vc_allocation_matrix_where IS NULL THEN
      ac_error_code := 'AL6';
      RAISE e_abort;
   ELSE
      vc_allocation_matrix_stmt := vc_allocation_matrix_select || vc_allocation_matrix_where || vc_allocation_matrix_orderby;
   END IF;

   -- Clear large rollback segment
   COMMIT;

   -- ****** Read and Process Transactions ****** -----------------------------------------
   OPEN transaction_detail_cursor;
   LOOP
      FETCH transaction_detail_cursor INTO transaction_detail;
      EXIT WHEN transaction_detail_cursor%NOTFOUND;
      v_rowid := transaction_detail.rowid;

      -- Search Allocation Matrix for matches
      BEGIN
          OPEN allocation_matrix_cursor
           FOR vc_allocation_matrix_stmt
         USING v_rowid;
         FETCH allocation_matrix_cursor
          INTO allocation_matrix;
         -- Rows found?
         IF allocation_matrix_cursor%FOUND THEN
            vn_fetch_rows := allocation_matrix_cursor%ROWCOUNT;
         ELSE
            vn_fetch_rows := 0;
         END IF;
         CLOSE allocation_matrix_cursor;
         IF SQLCODE != 0 THEN
            RAISE e_badread;
         END IF;
      EXCEPTION
         WHEN e_badread THEN
            ac_error_code := 'AL4';
            RAISE e_abort;
      END;

      -- Allocation Matrix Line Found
      IF vn_fetch_rows > 0 THEN
         v_allocation_matrix_id := allocation_matrix.allocation_matrix_id;
         -- Get number of Allocation Detail lines
         vn_allocation_total_rows := 0;
         SELECT count(*)
           INTO vn_allocation_total_rows
           FROM tb_allocation_matrix_detail
          WHERE tb_allocation_matrix_detail.allocation_matrix_id = v_allocation_matrix_id;
         IF vn_allocation_total_rows > 0 THEN
            -- Get and create Allocations --
            v_transaction_detail_id := transaction_detail.transaction_detail_id;
            v_transaction_state_code := transaction_detail.transaction_state_code;
            IF v_transaction_state_code IS NULL THEN
               v_transaction_state_code := '*NULL';
            END IF;
            transaction_detail.allocation_matrix_id := v_allocation_matrix_id;
            transaction_detail.auto_transaction_state_code := v_transaction_state_code;

            -- Working Fields
            v_orig_gl_line_itm_dist_amt := transaction_detail.gl_line_itm_dist_amt;
            v_accum_allocation := 0;
            v_delta := 0;

            -- Get next Allocation sub-trans ID for the new transaction detail records
            SELECT sq_tb_allocation_subtrans_id.NEXTVAL
              INTO v_allocation_subtrans_id
              FROM DUAL;

            -- Execute Cursor to get allocations
            OPEN allocation_detail_cursor;
            LOOP
               FETCH allocation_detail_cursor INTO allocation_detail;
               EXIT WHEN allocation_detail_cursor%NOTFOUND;

               -- Add allocation rows to total imported rows -- MBF03
               v_total_rows := v_total_rows + 1;

               -- Fill other fields from allocation matrix detail
               transaction_detail.jurisdiction_id := allocation_detail.jurisdiction_id;
               transaction_detail.transaction_state_code := allocation_detail.state;

               v_gl_line_itm_dist_amt := Round(v_orig_gl_line_itm_dist_amt * allocation_detail.allocation_percent,2);
               v_accum_allocation := v_accum_allocation + v_gl_line_itm_dist_amt;
               transaction_detail.orig_gl_line_itm_dist_amt := v_orig_gl_line_itm_dist_amt;
               transaction_detail.gl_line_itm_dist_amt := v_gl_line_itm_dist_amt;

               -- Check for last allocation and rounding
               IF allocation_detail_cursor%ROWCOUNT = vn_allocation_total_rows THEN
                  -- Check rounding
                  v_delta := v_orig_gl_line_itm_dist_amt - v_accum_allocation;
                  IF v_delta <> 0 THEN
                     transaction_detail.gl_line_itm_dist_amt := transaction_detail.gl_line_itm_dist_amt + v_delta;
                  END IF;
               END IF;

               -- Get next sequence
               SELECT sq_tb_transaction_detail_id.NEXTVAL
                 INTO transaction_detail.transaction_detail_id
                 FROM DUAL;

               -- Insert transaction detail row
               BEGIN
                  INSERT INTO tb_transaction_detail (
                     transaction_detail_id,
                     source_transaction_id,
                     process_batch_no,
                     gl_extract_batch_no,
                     archive_batch_no,
                     allocation_matrix_id,
                     allocation_subtrans_id,
                     entered_date,
                     transaction_status,
                     gl_date,
                     gl_company_nbr,
                     gl_company_name,
                     gl_division_nbr,
                     gl_division_name,
                     gl_cc_nbr_dept_id,
                     gl_cc_nbr_dept_name,
                     gl_local_acct_nbr,
                     gl_local_acct_name,
                     gl_local_sub_acct_nbr,
                     gl_local_sub_acct_name,
                     gl_full_acct_nbr,
                     gl_full_acct_name,
                     gl_line_itm_dist_amt,
                     orig_gl_line_itm_dist_amt,
                     vendor_nbr,
                     vendor_name,
                     vendor_address_line_1,
                     vendor_address_line_2,
                     vendor_address_line_3,
                     vendor_address_line_4,
                     vendor_address_city,
                     vendor_address_county,
                     vendor_address_state,
                     vendor_address_zip,
                     vendor_address_country,
                     vendor_type,
                     vendor_type_name,
                     invoice_nbr,
                     invoice_desc,
                     invoice_date,
                     invoice_freight_amt,
                     invoice_discount_amt,
                     invoice_tax_amt,
                     invoice_total_amt,
                     invoice_tax_flg,
                     invoice_line_nbr,
                     invoice_line_name,
                     invoice_line_type,
                     invoice_line_type_name,
                     invoice_line_amt,
                     invoice_line_tax,
                     afe_project_nbr,
                     afe_project_name,
                     afe_category_nbr,
                     afe_category_name,
                     afe_sub_cat_nbr,
                     afe_sub_cat_name,
                     afe_use,
                     afe_contract_type,
                     afe_contract_structure,
                     afe_property_cat,
                     inventory_nbr,
                     inventory_name,
                     inventory_class,
                     inventory_class_name,
                     po_nbr,
                     po_name,
                     po_date,
                     po_line_nbr,
                     po_line_name,
                     po_line_type,
                     po_line_type_name,
                     ship_to_location,
                     ship_to_location_name,
                     ship_to_address_line_1,
                     ship_to_address_line_2,
                     ship_to_address_line_3,
                     ship_to_address_line_4,
                     ship_to_address_city,
                     ship_to_address_county,
                     ship_to_address_state,
                     ship_to_address_zip,
                     ship_to_address_country,
                     wo_nbr,
                     wo_name,
                     wo_date,
                     wo_type,
                     wo_type_desc,
                     wo_class,
                     wo_class_desc,
                     wo_entity,
                     wo_entity_desc,
                     wo_line_nbr,
                     wo_line_name,
                     wo_line_type,
                     wo_line_type_desc,
                     wo_shut_down_cd,
                     wo_shut_down_cd_desc,
                     voucher_id,
                     voucher_name,
                     voucher_date,
                     voucher_line_nbr,
                     voucher_line_desc,
                     check_nbr,
                     check_no,
                     check_date,
                     check_amt,
                     check_desc,
                     user_text_01,
                     user_text_02,
                     user_text_03,
                     user_text_04,
                     user_text_05,
                     user_text_06,
                     user_text_07,
                     user_text_08,
                     user_text_09,
                     user_text_10,
                     user_text_11,
                     user_text_12,
                     user_text_13,
                     user_text_14,
                     user_text_15,
                     user_text_16,
                     user_text_17,
                     user_text_18,
                     user_text_19,
                     user_text_20,
                     user_text_21,
                     user_text_22,
                     user_text_23,
                     user_text_24,
                     user_text_25,
                     user_text_26,
                     user_text_27,
                     user_text_28,
                     user_text_29,
                     user_text_30,
                     user_number_01,
                     user_number_02,
                     user_number_03,
                     user_number_04,
                     user_number_05,
                     user_number_06,
                     user_number_07,
                     user_number_08,
                     user_number_09,
                     user_number_10,
                     user_date_01,
                     user_date_02,
                     user_date_03,
                     user_date_04,
                     user_date_05,
                     user_date_06,
                     user_date_07,
                     user_date_08,
                     user_date_09,
                     user_date_10,
                     comments,
                     tb_calc_tax_amt,
                     state_use_amount,
                     state_use_tier2_amount,
                     state_use_tier3_amount,
                     county_use_amount,
                     county_local_use_amount,
                     city_use_amount,
                     city_local_use_amount,
                     transaction_state_code,
                     auto_transaction_state_code,
                     transaction_ind,
                     suspend_ind,
                     taxcode_detail_id,
                     taxcode_state_code,
                     taxcode_type_code,
                     taxcode_code,
                     cch_taxcat_code,
                     cch_group_code,
                     cch_item_code,
                     manual_taxcode_ind,
                     tax_matrix_id,
                     location_matrix_id,
                     jurisdiction_id,
                     jurisdiction_taxrate_id,
                     manual_jurisdiction_ind,
                     measure_type_code,
                     state_use_rate,
                     state_use_tier2_rate,
                     state_use_tier3_rate,
                     state_split_amount,
                     state_tier2_min_amount,
                     state_tier2_max_amount,
                     state_maxtax_amount,
                     county_use_rate,
                     county_local_use_rate,
                     county_split_amount,
                     county_maxtax_amount,
                     county_single_flag,
                     county_default_flag,
                     city_use_rate,
                     city_local_use_rate,
                     city_split_amount,
                     city_split_use_rate,
                     city_single_flag,
                     city_default_flag,
                     combined_use_rate,
                     load_timestamp,
                     gl_extract_updater,
                     gl_extract_timestamp,
                     gl_extract_flag,
                     gl_log_flag,
                     gl_extract_amt,
                     audit_flag,
                     audit_user_id,
                     audit_timestamp,
                     modify_user_id,
                     modify_timestamp,
                     update_user_id,
                     update_timestamp,
                     split_subtrans_id,
                     multi_trans_code,
                     tax_alloc_matrix_id )
                  VALUES (
                     transaction_detail.transaction_detail_id,
                     transaction_detail.source_transaction_id,
                     transaction_detail.process_batch_no,
                     transaction_detail.gl_extract_batch_no,
                     transaction_detail.archive_batch_no,
                     transaction_detail.allocation_matrix_id,
                     v_allocation_subtrans_id,
                     transaction_detail.entered_date,
                     transaction_detail.transaction_status,
                     transaction_detail.gl_date,
                     transaction_detail.gl_company_nbr,
                     transaction_detail.gl_company_name,
                     transaction_detail.gl_division_nbr,
                     transaction_detail.gl_division_name,
                     transaction_detail.gl_cc_nbr_dept_id,
                     transaction_detail.gl_cc_nbr_dept_name,
                     transaction_detail.gl_local_acct_nbr,
                     transaction_detail.gl_local_acct_name,
                     transaction_detail.gl_local_sub_acct_nbr,
                     transaction_detail.gl_local_sub_acct_name,
                     transaction_detail.gl_full_acct_nbr,
                     transaction_detail.gl_full_acct_name,
                     transaction_detail.gl_line_itm_dist_amt,
                     transaction_detail.orig_gl_line_itm_dist_amt,
                     transaction_detail.vendor_nbr,
                     transaction_detail.vendor_name,
                     transaction_detail.vendor_address_line_1,
                     transaction_detail.vendor_address_line_2,
                     transaction_detail.vendor_address_line_3,
                     transaction_detail.vendor_address_line_4,
                     transaction_detail.vendor_address_city,
                     transaction_detail.vendor_address_county,
                     transaction_detail.vendor_address_state,
                     transaction_detail.vendor_address_zip,
                     transaction_detail.vendor_address_country,
                     transaction_detail.vendor_type,
                     transaction_detail.vendor_type_name,
                     transaction_detail.invoice_nbr,
                     transaction_detail.invoice_desc,
                     transaction_detail.invoice_date,
                     transaction_detail.invoice_freight_amt,
                     transaction_detail.invoice_discount_amt,
                     transaction_detail.invoice_tax_amt,
                     transaction_detail.invoice_total_amt,
                     transaction_detail.invoice_tax_flg,
                     transaction_detail.invoice_line_nbr,
                     transaction_detail.invoice_line_name,
                     transaction_detail.invoice_line_type,
                     transaction_detail.invoice_line_type_name,
                     transaction_detail.invoice_line_amt,
                     transaction_detail.invoice_line_tax,
                     transaction_detail.afe_project_nbr,
                     transaction_detail.afe_project_name,
                     transaction_detail.afe_category_nbr,
                     transaction_detail.afe_category_name,
                     transaction_detail.afe_sub_cat_nbr,
                     transaction_detail.afe_sub_cat_name,
                     transaction_detail.afe_use,
                     transaction_detail.afe_contract_type,
                     transaction_detail.afe_contract_structure,
                     transaction_detail.afe_property_cat,
                     transaction_detail.inventory_nbr,
                     transaction_detail.inventory_name,
                     transaction_detail.inventory_class,
                     transaction_detail.inventory_class_name,
                     transaction_detail.po_nbr,
                     transaction_detail.po_name,
                     transaction_detail.po_date,
                     transaction_detail.po_line_nbr,
                     transaction_detail.po_line_name,
                     transaction_detail.po_line_type,
                     transaction_detail.po_line_type_name,
                     transaction_detail.ship_to_location,
                     transaction_detail.ship_to_location_name,
                     transaction_detail.ship_to_address_line_1,
                     transaction_detail.ship_to_address_line_2,
                     transaction_detail.ship_to_address_line_3,
                     transaction_detail.ship_to_address_line_4,
                     transaction_detail.ship_to_address_city,
                     transaction_detail.ship_to_address_county,
                     transaction_detail.ship_to_address_state,
                     transaction_detail.ship_to_address_zip,
                     transaction_detail.ship_to_address_country,
                     transaction_detail.wo_nbr,
                     transaction_detail.wo_name,
                     transaction_detail.wo_date,
                     transaction_detail.wo_type,
                     transaction_detail.wo_type_desc,
                     transaction_detail.wo_class,
                     transaction_detail.wo_class_desc,
                     transaction_detail.wo_entity,
                     transaction_detail.wo_entity_desc,
                     transaction_detail.wo_line_nbr,
                     transaction_detail.wo_line_name,
                     transaction_detail.wo_line_type,
                     transaction_detail.wo_line_type_desc,
                     transaction_detail.wo_shut_down_cd,
                     transaction_detail.wo_shut_down_cd_desc,
                     transaction_detail.voucher_id,
                     transaction_detail.voucher_name,
                     transaction_detail.voucher_date,
                     transaction_detail.voucher_line_nbr,
                     transaction_detail.voucher_line_desc,
                     transaction_detail.check_nbr,
                     transaction_detail.check_no,
                     transaction_detail.check_date,
                     transaction_detail.check_amt,
                     transaction_detail.check_desc,
                     transaction_detail.user_text_01,
                     transaction_detail.user_text_02,
                     transaction_detail.user_text_03,
                     transaction_detail.user_text_04,
                     transaction_detail.user_text_05,
                     transaction_detail.user_text_06,
                     transaction_detail.user_text_07,
                     transaction_detail.user_text_08,
                     transaction_detail.user_text_09,
                     transaction_detail.user_text_10,
                     transaction_detail.user_text_11,
                     transaction_detail.user_text_12,
                     transaction_detail.user_text_13,
                     transaction_detail.user_text_14,
                     transaction_detail.user_text_15,
                     transaction_detail.user_text_16,
                     transaction_detail.user_text_17,
                     transaction_detail.user_text_18,
                     transaction_detail.user_text_19,
                     transaction_detail.user_text_20,
                     transaction_detail.user_text_21,
                     transaction_detail.user_text_22,
                     transaction_detail.user_text_23,
                     transaction_detail.user_text_24,
                     transaction_detail.user_text_25,
                     transaction_detail.user_text_26,
                     transaction_detail.user_text_27,
                     transaction_detail.user_text_28,
                     transaction_detail.user_text_29,
                     transaction_detail.user_text_30,
                     transaction_detail.user_number_01,
                     transaction_detail.user_number_02,
                     transaction_detail.user_number_03,
                     transaction_detail.user_number_04,
                     transaction_detail.user_number_05,
                     transaction_detail.user_number_06,
                     transaction_detail.user_number_07,
                     transaction_detail.user_number_08,
                     transaction_detail.user_number_09,
                     transaction_detail.user_number_10,
                     transaction_detail.user_date_01,
                     transaction_detail.user_date_02,
                     transaction_detail.user_date_03,
                     transaction_detail.user_date_04,
                     transaction_detail.user_date_05,
                     transaction_detail.user_date_06,
                     transaction_detail.user_date_07,
                     transaction_detail.user_date_08,
                     transaction_detail.user_date_09,
                     transaction_detail.user_date_10,
                     transaction_detail.comments,
                     transaction_detail.tb_calc_tax_amt,
                     transaction_detail.state_use_amount,
                     transaction_detail.state_use_tier2_amount,
                     transaction_detail.state_use_tier3_amount,
                     transaction_detail.county_use_amount,
                     transaction_detail.county_local_use_amount,
                     transaction_detail.city_use_amount,
                     transaction_detail.city_local_use_amount,
                     transaction_detail.transaction_state_code,
                     transaction_detail.auto_transaction_state_code,
                     transaction_detail.transaction_ind,
                     transaction_detail.suspend_ind,
                     transaction_detail.taxcode_detail_id,
                     transaction_detail.taxcode_state_code,
                     transaction_detail.taxcode_type_code,
                     transaction_detail.taxcode_code,
                     transaction_detail.cch_taxcat_code,
                     transaction_detail.cch_group_code,
                     transaction_detail.cch_item_code,
                     transaction_detail.manual_taxcode_ind,
                     transaction_detail.tax_matrix_id,
                     transaction_detail.location_matrix_id,
                     transaction_detail.jurisdiction_id,
                     transaction_detail.jurisdiction_taxrate_id,
                     transaction_detail.manual_jurisdiction_ind,
                     transaction_detail.measure_type_code,
                     transaction_detail.state_use_rate,
                     transaction_detail.state_use_tier2_rate,
                     transaction_detail.state_use_tier3_rate,
                     transaction_detail.state_split_amount,
                     transaction_detail.state_tier2_min_amount,
                     transaction_detail.state_tier2_max_amount,
                     transaction_detail.state_maxtax_amount,
                     transaction_detail.county_use_rate,
                     transaction_detail.county_local_use_rate,
                     transaction_detail.county_split_amount,
                     transaction_detail.county_maxtax_amount,
                     transaction_detail.county_single_flag,
                     transaction_detail.county_default_flag,
                     transaction_detail.city_use_rate,
                     transaction_detail.city_local_use_rate,
                     transaction_detail.city_split_amount,
                     transaction_detail.city_split_use_rate,
                     transaction_detail.city_single_flag,
                     transaction_detail.city_default_flag,
                     transaction_detail.combined_use_rate,
                     transaction_detail.load_timestamp,
                     transaction_detail.gl_extract_updater,
                     transaction_detail.gl_extract_timestamp,
                     transaction_detail.gl_extract_flag,
                     transaction_detail.gl_log_flag,
                     transaction_detail.gl_extract_amt,
                     transaction_detail.audit_flag,
                     transaction_detail.audit_user_id,
                     transaction_detail.audit_timestamp,
                     transaction_detail.modify_user_id,
                     transaction_detail.modify_timestamp,
                     transaction_detail.update_user_id,
                     transaction_detail.update_timestamp,
                     transaction_detail.split_subtrans_id,
                     'A',
                     transaction_detail.tax_alloc_matrix_id );
                  -- Error Checking
                  IF SQLCODE != 0 THEN
                     RAISE e_badwrite;
                  END IF;
               EXCEPTION
                  WHEN e_badwrite THEN
                     ac_error_code := 'AL5';
                     RAISE e_abort;
               END;
            END LOOP;
            CLOSE allocation_detail_cursor;
         END IF;

         -- Update Allocated Transaction
         UPDATE tb_transaction_detail
            SET multi_trans_code = 'OA',
                allocation_subtrans_id = v_allocation_subtrans_id
          WHERE rowid = v_rowid;
      END IF;
   END LOOP;
   CLOSE transaction_detail_cursor;
   COMMIT;

EXCEPTION
   WHEN e_abort THEN
      ROLLBACK;
END;
/
