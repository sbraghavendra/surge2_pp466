create or replace
PROCEDURE sp_tb_user_a_d (
   p_old_user_code             IN     tb_user.user_code%TYPE)
/* ************************************************************************************************/
/* Object Type/Name: Stored Procedure (after delete) - sp_tb_user_a_d                             */
/* Author:           Michael B. Fuller                                                            */
/* Date:             a long time ago                                                              */
/* Description:      A User has been deleted                                                      */
/* Arguments:        p_old_user_codee(tb_user.user_code%TYPE)                                     */
/* Returns:          None                                                                         */
/* ************************************************************************************************/
/* Maintenance History:                                                                           */
/* Scan  Author       Date     Release    Description                                  Ticket #   */
/* ----- ---------- ---------- ---------- -------------------------------------------- ---------- */
/* ************************************************************************************************/
AS
BEGIN
   -- Delete User Options
   DELETE
     FROM tb_option
    WHERE user_code = p_old_user_code;
   -- Delete saved column order
   DELETE
   FROM tb_dw_column
   WHERE user_code = p_old_user_code;
END;
