-- Alter Tax Allocations for Oracle
-- MBF -- 08/17/2011

ALTER TABLE STSCORP.tb_bcp_transactions
   ADD (split_subtrans_id NUMBER)
   ADD (multi_trans_code VARCHAR2(10)
   ADD (tax_alloc_matrix_id NUMBER));

ALTER TABLE STSCORP.tb_driver_names
   ADD (split_driver_flag CHAR(1) NULL);

ALTER TABLE STSCORP.tb_transaction_detail
   ADD (split_subtrans_id NUMBER)
   ADD (multi_trans_code VARCHAR2(10))
   ADD (tax_alloc_matrix_id NUMBER);

CREATE SEQUENCE STSCORP.sq_tb_split_subtrans_id
   INCREMENT BY 1 START WITH 1
	NOMAXVALUE NOMINVALUE NOCYCLE NOORDER;

CREATE SEQUENCE STSCORP.sq_tb_tax_alloc_matrix_dtl_id
	INCREMENT BY 1 START WITH 1
   NOMAXVALUE NOMINVALUE NOCYCLE NOORDER;

CREATE SEQUENCE STSCORP.sq_tb_tax_alloc_matrix_id
   INCREMENT BY 1 START WITH 1
   NOMAXVALUE NOMINVALUE NOCYCLE NOORDER;

CREATE TABLE STSCORP.tb_tax_alloc_matrix (
	TAX_ALLOC_MATRIX_ID       	NUMBER,
	DRIVER_01                 	VARCHAR2(100) ,
	DRIVER_02                 	VARCHAR2(100) ,
	DRIVER_03                 	VARCHAR2(100) ,
	DRIVER_04                 	VARCHAR2(100) ,
	DRIVER_05                 	VARCHAR2(100) ,
	DRIVER_06                 	VARCHAR2(100) ,
	DRIVER_07                 	VARCHAR2(100) ,
	DRIVER_08                 	VARCHAR2(100) ,
	DRIVER_09                 	VARCHAR2(100) ,
	DRIVER_10                 	VARCHAR2(100) ,
	DRIVER_11                 	VARCHAR2(100) ,
	DRIVER_12                 	VARCHAR2(100) ,
	DRIVER_13                 	VARCHAR2(100) ,
	DRIVER_14                 	VARCHAR2(100) ,
	DRIVER_15                 	VARCHAR2(100) ,
	DRIVER_16                 	VARCHAR2(100) ,
	DRIVER_17                 	VARCHAR2(100) ,
	DRIVER_18                 	VARCHAR2(100) ,
	DRIVER_19                 	VARCHAR2(100) ,
	DRIVER_20                 	VARCHAR2(100) ,
	DRIVER_21                 	VARCHAR2(100) ,
	DRIVER_22                 	VARCHAR2(100) ,
	DRIVER_23                 	VARCHAR2(100) ,
	DRIVER_24                 	VARCHAR2(100) ,
	DRIVER_25                 	VARCHAR2(100) ,
	DRIVER_26                 	VARCHAR2(100) ,
	DRIVER_27                 	VARCHAR2(100) ,
	DRIVER_28                 	VARCHAR2(100) ,
	DRIVER_29                 	VARCHAR2(100) ,
	DRIVER_30                 	VARCHAR2(100) ,
	BINARY_WEIGHT             	NUMBER,
	SIGNIFICANT_DIGITS        	VARCHAR2(119),
	EFFECTIVE_DATE            	DATE,
	EXPIRATION_DATE            	DATE,
	FUTURE_SPLIT                CHAR(1),
	COMMENTS                  	VARCHAR2(255),
	UPDATE_USER_ID            	VARCHAR2(40),
	UPDATE_TIMESTAMP          	DATE ,
	CONSTRAINT PK_TAX_ALLOC_MATRIX_ID PRIMARY KEY(TAX_ALLOC_MATRIX_ID)
	NOT DEFERRABLE
	 DISABLE NOVALIDATE
);

CREATE TABLE STSCORP.TB_TAX_ALLOC_MATRIX_DETAIL  ( 
	TAX_ALLOC_MATRIX_ID       	NUMBER,
	TAX_ALLOC_MATRIX_DTL_ID       	NUMBER,
	DRIVER_01                 	VARCHAR2(100) ,
	DRIVER_02                 	VARCHAR2(100) ,
	DRIVER_03                 	VARCHAR2(100) ,
	DRIVER_04                 	VARCHAR2(100) ,
	DRIVER_05                 	VARCHAR2(100) ,
	DRIVER_06                 	VARCHAR2(100) ,
	DRIVER_07                 	VARCHAR2(100) ,
	DRIVER_08                 	VARCHAR2(100) ,
	DRIVER_09                 	VARCHAR2(100) ,
	DRIVER_10                 	VARCHAR2(100) ,
	DRIVER_11                 	VARCHAR2(100) ,
	DRIVER_12                 	VARCHAR2(100) ,
	DRIVER_13                 	VARCHAR2(100) ,
	DRIVER_14                 	VARCHAR2(100) ,
	DRIVER_15                 	VARCHAR2(100) ,
	DRIVER_16                 	VARCHAR2(100) ,
	DRIVER_17                 	VARCHAR2(100) ,
	DRIVER_18                 	VARCHAR2(100) ,
	DRIVER_19                 	VARCHAR2(100) ,
	DRIVER_20                 	VARCHAR2(100) ,
	DRIVER_21                 	VARCHAR2(100) ,
	DRIVER_22                 	VARCHAR2(100) ,
	DRIVER_23                 	VARCHAR2(100) ,
	DRIVER_24                 	VARCHAR2(100) ,
	DRIVER_25                 	VARCHAR2(100) ,
	DRIVER_26                 	VARCHAR2(100) ,
	DRIVER_27                 	VARCHAR2(100) ,
	DRIVER_28                 	VARCHAR2(100) ,
	DRIVER_29                 	VARCHAR2(100) ,
	DRIVER_30                 	VARCHAR2(100) ,
	COMMENTS                  	VARCHAR2(255),
	ALLOCATION_PERCENT             	NUMBER,
	CONSTRAINT PK_TAX_ALLOC_MATRIX_DTL_ID PRIMARY KEY(TAX_ALLOC_MATRIX_DTL_ID)
	NOT DEFERRABLE
	 DISABLE NOVALIDATE
);

ALTER TABLE STSCORP.TB_TAX_ALLOC_MATRIX_DETAIL
	ADD ( CONSTRAINT TB_TAX_ALLOC_MATRIX_DTL_FK01
	FOREIGN KEY(TAX_ALLOC_MATRIX_ID)
	REFERENCES STSCORP.TB_TAX_ALLOC_MATRIX(TAX_ALLOC_MATRIX_ID)
	NOT DEFERRABLE INITIALLY IMMEDIATE DISABLE NOVALIDATE );

CREATE TABLE STSCORP.tb_jur_alloc_matrix_detail   ( 
	TAX_ALLOC_MATRIX_ID       	NUMBER,
	TAX_ALLOC_MATRIX_DTL_ID       	NUMBER,
	JURISDICTION_ID			NUMBER,	
	ALLOCATION_PERCENT             	NUMBER,
	CONSTRAINT PK_jur_alloc_matrix_detail_ID PRIMARY KEY(TAX_ALLOC_MATRIX_ID,TAX_ALLOC_MATRIX_DTL_ID,JURISDICTION_ID)
	NOT DEFERRABLE
	 DISABLE NOVALIDATE
);

ALTER TABLE STSCORP.tb_jur_alloc_matrix_detail
	ADD ( CONSTRAINT tb_jur_alloc_matrix_dtl_FK01
	FOREIGN KEY(TAX_ALLOC_MATRIX_ID)
	REFERENCES STSCORP.TB_TAX_ALLOC_MATRIX(TAX_ALLOC_MATRIX_ID)
	NOT DEFERRABLE INITIALLY IMMEDIATE DISABLE NOVALIDATE );

ALTER TABLE STSCORP.tb_jur_alloc_matrix_detail
	ADD ( CONSTRAINT tb_jur_alloc_matrix_dtl_FK02
	FOREIGN KEY(JURISDICTION_ID)
	REFERENCES STSCORP.TB_JURISDICTION(JURISDICTION_ID)
	NOT DEFERRABLE INITIALLY IMMEDIATE DISABLE NOVALIDATE );

--Create Allocation/Split List Codes
INSERT INTO tb_list_code(code_type_code,code_code,description,message,explanation,severity_level,write_import_line_flag,abort_import_flag,update_user_id,update_timestamp)
VALUES('*DEF','MULTITRANS','Multi-Transaction Code','0','0','0','0','0',USER,SYSDATE);
INSERT INTO tb_list_code(code_type_code,code_code,description,message,explanation,severity_level,write_import_line_flag,abort_import_flag,update_user_id,update_timestamp)
VALUES('MULTITRANS','FS','Flagged for Future Split',NULL,NULL,NULL,NULL,NULL,USER,SYSDATE);
INSERT INTO tb_list_code(code_type_code,code_code,description,message,explanation,severity_level,write_import_line_flag,abort_import_flag,update_user_id,update_timestamp)
VALUES('MULTITRANS','OS','Original Split Transaction',NULL,NULL,NULL,NULL,NULL,USER,SYSDATE);
INSERT INTO tb_list_code(code_type_code,code_code,description,message,explanation,severity_level,write_import_line_flag,abort_import_flag,update_user_id,update_timestamp)
VALUES('MULTITRANS','S','Split Transaction',NULL,NULL,NULL,NULL,NULL,USER,SYSDATE);
INSERT INTO tb_list_code(code_type_code,code_code,description,message,explanation,severity_level,write_import_line_flag,abort_import_flag,update_user_id,update_timestamp)
VALUES('MULTITRANS','OA','Original Allocation',NULL,NULL,NULL,NULL,NULL,USER,SYSDATE);
INSERT INTO tb_list_code(code_type_code,code_code,description,message,explanation,severity_level,write_import_line_flag,abort_import_flag,update_user_id,update_timestamp)
VALUES('MULTITRANS','A','Allocation',NULL,NULL,NULL,NULL,NULL,USER,SYSDATE);
