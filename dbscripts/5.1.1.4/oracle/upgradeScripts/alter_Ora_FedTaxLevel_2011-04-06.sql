-- Alter Federal Tax Level changes for Oracle
-- 04-06-2011

-- tb_bcp_jurisdiction_taxrate
ALTER TABLE tb_bcp_jurisdiction_taxrate ADD country VARCHAR2(10);
ALTER TABLE tb_bcp_jurisdiction_taxrate ADD country_sales_rate NUMBER;
ALTER TABLE tb_bcp_jurisdiction_taxrate ADD country_use_rate NUMBER;

-- tb_jurisdiction
ALTER TABLE tb_jurisdiction ADD country VARCHAR2(10);

-- tb_jurisdiction_taxrate
ALTER TABLE tb_jurisdiction_taxrate ADD country_sales_rate NUMBER;
ALTER TABLE tb_jurisdiction_taxrate ADD country_use_rate NUMBER;

-- tb_taxcode_state
ALTER TABLE tb_taxcode_state ADD CONSTRAINT pk_tb_taxcode_state PRIMARY KEY (country, taxcode_state_code) ENABLE VALIDATE;

-- tb_taxcode_detail
ALTER TABLE tb_taxcode_detail ADD taxcode_country_code VARCHAR2(10);

-- tb_tax_matrix
ALTER TABLE tb_tax_matrix ADD matrix_country_code VARCHAR2(10);

-- tb_tmp_tax_matrix
ALTER TABLE tb_tmp_tax_matrix ADD matrix_country_code VARCHAR2(10);

-- tb_location_matrix
ALTER TABLE tb_location_matrix ADD country VARCHAR2(10);
ALTER TABLE tb_location_matrix ADD country_flag CHAR(1 BYTE);

-- tb_tmp_location_matrix
ALTER TABLE tb_tmp_location_matrix ADD override_taxtype_code VARCHAR2(10 BYTE);
ALTER TABLE tb_tmp_location_matrix ADD state_flag CHAR(1 BYTE);
ALTER TABLE tb_tmp_location_matrix ADD county_flag CHAR(1 BYTE);
ALTER TABLE tb_tmp_location_matrix ADD county_local_flag CHAR(1 BYTE);
ALTER TABLE tb_tmp_location_matrix ADD city_flag CHAR(1 BYTE);
ALTER TABLE tb_tmp_location_matrix ADD city_local_flag CHAR(1 BYTE);
ALTER TABLE tb_tmp_location_matrix ADD country VARCHAR2(10);
ALTER TABLE tb_tmp_location_matrix ADD country_flag CHAR(1 BYTE);

-- tb_gl_extract_map
ALTER TABLE tb_gl_extract_map ADD taxcode_country_code VARCHAR2(10);

-- tb_bcp_transactions
ALTER TABLE tb_bcp_transactions  ADD country_use_amount NUMBER;
ALTER TABLE tb_bcp_transactions  ADD transaction_country_code VARCHAR2(10);
ALTER TABLE tb_bcp_transactions  ADD auto_transaction_country_code  VARCHAR2(10);
ALTER TABLE tb_bcp_transactions  ADD taxcode_country_code VARCHAR2(10);
ALTER TABLE tb_bcp_transactions  ADD country_use_rate NUMBER;

-- tb_transaction_detail
ALTER TABLE tb_transaction_detail ADD country_use_amount NUMBER;
ALTER TABLE tb_transaction_detail ADD transaction_country_code VARCHAR2(10);
ALTER TABLE tb_transaction_detail ADD auto_transaction_country_code VARCHAR2(10);
ALTER TABLE tb_transaction_detail ADD taxcode_country_code VARCHAR2(10);
ALTER TABLE tb_transaction_detail ADD country_use_rate NUMBER;

-- tb_gl_export_log
ALTER TABLE tb_gl_export_log ADD country_use_amount NUMBER;
ALTER TABLE tb_gl_export_log ADD transaction_country_code VARCHAR2(10);
ALTER TABLE tb_gl_export_log ADD auto_transaction_country_code VARCHAR2(10);
ALTER TABLE tb_gl_export_log ADD taxcode_country_code VARCHAR2(10);
ALTER TABLE tb_gl_export_log ADD country_use_rate NUMBER;

-- tb_gl_report_log
ALTER TABLE tb_gl_report_log ADD country_use_amount NUMBER;
ALTER TABLE tb_gl_report_log ADD transaction_country_code VARCHAR2(10);
ALTER TABLE tb_gl_report_log ADD auto_transaction_country_code VARCHAR2(10);
ALTER TABLE tb_gl_report_log ADD taxcode_country_code VARCHAR2(10);
ALTER TABLE tb_gl_report_log ADD country_use_rate NUMBER;

-- tb_tmp_gl_export
ALTER TABLE tb_tmp_gl_export ADD country_use_amount NUMBER;
ALTER TABLE tb_tmp_gl_export ADD transaction_country_code VARCHAR2(10);
ALTER TABLE tb_tmp_gl_export ADD auto_transaction_country_code VARCHAR2(10);
ALTER TABLE tb_tmp_gl_export ADD taxcode_country_code VARCHAR2(10);
ALTER TABLE tb_tmp_gl_export ADD country_use_rate NUMBER;

-- tb_tmp_transaction_detail
ALTER TABLE tb_tmp_transaction_detail ADD country_use_amount NUMBER;
ALTER TABLE tb_tmp_transaction_detail ADD transaction_country_code VARCHAR2(10);
ALTER TABLE tb_tmp_transaction_detail ADD auto_transaction_country_code VARCHAR2(10);
ALTER TABLE tb_tmp_transaction_detail ADD taxcode_country_code VARCHAR2(10);
ALTER TABLE tb_tmp_transaction_detail ADD country_use_rate NUMBER;

-- tb_tmp_transaction_detail_b_u
ALTER TABLE tb_tmp_transaction_detail_b_u ADD country_use_amount NUMBER;
ALTER TABLE tb_tmp_transaction_detail_b_u ADD transaction_country_code VARCHAR2(10);
ALTER TABLE tb_tmp_transaction_detail_b_u ADD auto_transaction_country_code VARCHAR2(10);
ALTER TABLE tb_tmp_transaction_detail_b_u ADD taxcode_country_code VARCHAR2(10);
ALTER TABLE tb_tmp_transaction_detail_b_u ADD country_use_rate NUMBER;

-- tb_list_code
INSERT INTO tb_list_code (code_type_code, code_code, description, write_import_line_flag, update_user_id, update_timestamp) VALUES ('*DEF', 'COUNTRY', 'Valid Countries', '1', USER, SYSTIME);
INSERT INTO tb_list_code (code_type_code, code_code, description, write_import_line_flag, update_user_id, update_timestamp) VALUES ('COUNTRY', 'US', 'USA', '1', USER, SYSTIME);
INSERT INTO tb_list_code (code_type_code, code_code, description, write_import_line_flag, update_user_id, update_timestamp) VALUES ('COUNTRY', 'CA', 'Canada', '0', USER, SYSTIME);

-- tb_option
INSERT INTO tb_option (option_code, option_type_code, user_code, description, value, update_user_id, update_timestamp) VALUES ('DEFAULTCOUNTRY', 'USER', 'STSCORP', 'Default Country Code', 'US', USER, SYSTIME)

COMMIT;

