CREATE OR REPLACE PROCEDURE Sp_Batch_Archive(an_batch_id NUMBER)
IS
-- Define Cursor Type
   TYPE cursor_type IS REF CURSOR;

-- Update Transaction Detail SQL
   vc_trans_dtl_update             VARCHAR2(1000)                                    :=
      'UPDATE tb_transaction_detail ' ||
         'SET archive_batch_no = :an_batch_id ';
   vc_trans_dtl_where              VARCHAR2(2000)                                    :=
      'WHERE archive_batch_no IS NULL AND gl_extract_flag = 1';
   vc_trans_dtl_stmt               VARCHAR2(3000)                                    := NULL;
   trans_dtl_cursor                cursor_type;

-- Table defined variables
   v_batch_status_code             TB_BATCH.batch_status_code%TYPE                   := NULL;
   v_error_sev_code                TB_BATCH.error_sev_code%TYPE                      := ' ';
   v_severity_level                TB_LIST_CODE.severity_level%TYPE                  := ' ';
   v_write_import_line_flag        TB_LIST_CODE.write_import_line_flag%TYPE          := '1';
   v_abort_import_flag             TB_LIST_CODE.abort_import_flag%TYPE               := '0';
   v_business_unit                 TB_BATCH.vc01%TYPE                                := NULL;
   v_trans_detail_column_name      TB_BATCH.vc02%TYPE                                := NULL;
   v_from_timestamp                TB_BATCH.ts01%TYPE                                := NULL;
   v_to_timestamp                  TB_BATCH.ts02%TYPE                                := NULL;
   v_sysdate                       TB_BATCH.actual_start_timestamp%TYPE              := SYS_EXTRACT_UTC(SYSTIMESTAMP);

-- Program defined variables
   vi_error_count                  INTEGER                                           := 0;
   vi_cursor_id                    INTEGER                                           := 0;
   vi_rows_processed               INTEGER                                           := 0;

-- Define Exceptions
   e_badread                       EXCEPTION;
   e_badupdate                     EXCEPTION;
   e_badwrite                      EXCEPTION;
   e_wrongdata                     EXCEPTION;
   e_abort                         EXCEPTION;

-- Program starts ******************************************************************************************
BEGIN
   -- Confirm batch exists and is flagged for processing
   BEGIN
      SELECT batch_status_code
        INTO v_batch_status_code
        FROM TB_BATCH
       WHERE batch_id = an_batch_id;
      IF SQLCODE != 0 THEN
         RAISE e_badread;
      ELSIF v_batch_status_code != 'FA' THEN
         RAISE e_wrongdata;
      END IF;
   EXCEPTION
      WHEN NO_DATA_FOUND OR e_badread THEN
         vi_error_count := vi_error_count + 1;
         Sp_Geterrorcode('A8', an_batch_id, 'A', v_sysdate, v_severity_level, v_write_import_line_flag, v_abort_import_flag);
         IF v_severity_level > v_error_sev_code THEN
            v_error_sev_code := v_severity_level;
         END IF;
      WHEN e_wrongdata THEN
         vi_error_count := vi_error_count + 1;
         Sp_Geterrorcode('A9', an_batch_id, 'A', v_sysdate, v_severity_level, v_write_import_line_flag, v_abort_import_flag);
         IF v_severity_level > v_error_sev_code THEN
            v_error_sev_code := v_severity_level;
         END IF;
   END;
   -- Abort Procedure?
   IF v_abort_import_flag = '1' THEN
      v_batch_status_code := 'ABA';
      RAISE e_abort;
   END IF;

   -- Get Batch information
   SELECT VC01,
          VC02,
          TS01,
          TS02
     INTO v_trans_detail_column_name,
          v_business_unit,
          v_from_timestamp,
          v_to_timestamp
     FROM TB_BATCH
    WHERE batch_id = an_batch_id;

   -- Update batch as processing
   BEGIN
      UPDATE TB_BATCH
         SET batch_status_code = 'XA',
             error_sev_code = '',
             actual_start_timestamp = SYS_EXTRACT_UTC(SYSTIMESTAMP)
       WHERE batch_id = an_batch_id;
      IF SQLCODE != 0 THEN
         RAISE e_badupdate;
      END IF;
   EXCEPTION
      WHEN e_badupdate THEN
         vi_error_count := vi_error_count + 1;
         Sp_Geterrorcode('A1', an_batch_id, 'A', v_sysdate, v_severity_level, v_write_import_line_flag, v_abort_import_flag);
         IF v_severity_level > v_error_sev_code THEN
            v_error_sev_code := v_severity_level;
         END IF;
   END;
   -- Abort Procedure?
   IF v_abort_import_flag = '1' THEN
      v_batch_status_code := 'ABA';
      RAISE e_abort;
   END IF;

   COMMIT;

   /**  Create Where Clause and construct Dynamic SQL Statement  **********************************/
   -- Business Unit
   IF v_business_unit IS NOT NULL AND v_trans_detail_column_name IS NOT NULL THEN
      IF vc_trans_dtl_where IS NULL THEN
         vc_trans_dtl_where := 'WHERE ';
      ELSE
         vc_trans_dtl_where := vc_trans_dtl_where || ' AND ';
      END IF;
      vc_trans_dtl_where := vc_trans_dtl_where || v_trans_detail_column_name || ' = ''' || v_business_unit || '''';
   END IF;
   -- From G/L Date
   IF v_from_timestamp IS NOT NULL THEN
      IF vc_trans_dtl_where IS NULL THEN
         vc_trans_dtl_where := 'WHERE ';
      ELSE
         vc_trans_dtl_where := vc_trans_dtl_where || ' AND ';
      END IF;
      vc_trans_dtl_where := vc_trans_dtl_where || 'gl_date >= to_date(''' || TO_CHAR(v_from_timestamp,'mm/dd/yyyy') || ''',''mm/dd/yyyy'')';
   END IF;
   -- To G/L Date
   IF v_to_timestamp IS NOT NULL THEN
      IF vc_trans_dtl_where IS NULL THEN
         vc_trans_dtl_where := 'WHERE ';
      ELSE
         vc_trans_dtl_where := vc_trans_dtl_where || ' AND ';
      END IF;
      vc_trans_dtl_where := vc_trans_dtl_where || 'gl_date <= to_date(''' || TO_CHAR(v_to_timestamp,'mm/dd/yyyy') || ''',''mm/dd/yyyy'')';
   END IF;
   -- Build Statement
   vc_trans_dtl_stmt := vc_trans_dtl_update || vc_trans_dtl_where;

--insert into tb_debug(row_joe) values(vc_trans_dtl_stmt);
--commit;

   /**  Update Archived Rows with Archive Batch No.  **********************************************/
   vi_cursor_id := DBMS_SQL.open_cursor;
   DBMS_SQL.parse (vi_cursor_id, vc_trans_dtl_stmt, DBMS_SQL.v7);
   DBMS_SQL.bind_variable (vi_cursor_id, ':an_batch_id', an_batch_id);
   vi_rows_processed := DBMS_SQL.EXECUTE (vi_cursor_id);
   DBMS_SQL.close_cursor (vi_cursor_id);
   COMMIT;

   /**  Copy Archive Batch from Transaction Detail table to Archive Detail table  *****************/
   INSERT INTO TB_ARCHIVE_TRANSACTION_DTL
      SELECT *
        FROM TB_TRANSACTION_DETAIL
       WHERE archive_batch_no = an_batch_id;

   /**  Delete Archive Batch from Transaction Detail table  ***************************************/
   DELETE FROM TB_TRANSACTION_DETAIL
    WHERE archive_batch_no = an_batch_id;

   /**  Update Batch file  ************************************************************************/
   UPDATE TB_BATCH
      SET batch_status_code = 'A',
          total_rows = vi_rows_processed,
          processed_rows = vi_rows_processed,
          actual_end_timestamp = SYS_EXTRACT_UTC(SYSTIMESTAMP)
    WHERE batch_id = an_batch_id;

    COMMIT;

EXCEPTION
   WHEN e_abort THEN
      -- Update batch with error codes
      UPDATE TB_BATCH
         SET batch_status_code = v_batch_status_code,
             error_sev_code = v_error_sev_code
       WHERE batch_id = an_batch_id;
      COMMIT;
   WHEN OTHERS THEN
      -- Update batch with something else
      UPDATE TB_BATCH
         SET batch_status_code = 'A?',
             error_sev_code = 99
       WHERE batch_id = an_batch_id;
      COMMIT;
END;
/

