CREATE SEQUENCE SQ_TB_BCP_JURIS_TAXRATE_ID
  START WITH 1
  MAXVALUE 1000000000000000000000000000
  MINVALUE 1
  NOCYCLE
  CACHE 1000
  NOORDER;

CREATE SEQUENCE SQ_TB_BCP_TRANSACTIONS
  START WITH 1
  MAXVALUE 1000000000000000000000000000
  MINVALUE 1
  NOCYCLE
  CACHE 1000
  NOORDER;


ALTER TABLE TB_ALLOCATION_MATRIX
MODIFY(EFFECTIVE_DATE  DEFAULT SYSDATE);
ALTER TABLE TB_ALLOCATION_MATRIX
MODIFY(EXPIRATION_DATE  DEFAULT TO_DATE('12/31/9999','mm/dd/yyyy'));
ALTER TABLE TB_BATCH NOCACHE;
ALTER TABLE TB_BATCH_ERROR_LOG
MODIFY(TRANS_DTL_COLUMN_NAME VARCHAR2(40 BYTE));
ALTER TABLE TB_BATCH_ERROR_LOG
MODIFY(TRANS_DTL_DATATYPE VARCHAR2(10 BYTE));
ALTER TABLE TB_BCP_JURISDICTION_TAXRATE ADD (BCP_JURISDICTION_TAXRATE_ID  NUMBER);
CREATE TABLE TB_BCP_JURI_TAXRATE_TEXT
(
  BATCH_ID  NUMBER,
  LINE      NUMBER,
  TEXT      VARCHAR2(4000 BYTE)
)
LOGGING 
NOCACHE
NOPARALLEL;
ALTER TABLE TB_BCP_TRANSACTIONS ADD (BCP_TRANSACTION_ID  NUMBER);
ALTER TABLE TB_CCH_TXMATRIX NOCACHE;
ALTER TABLE TB_DATA_DEF_COLUMN ADD (MINIMUM_VIEW_FLAG  CHAR(1 BYTE));
ALTER TABLE TB_DATA_DEF_COLUMN NOCACHE;
ALTER TABLE TB_DEBUG
MODIFY(ROW_JOE VARCHAR2(4000 BYTE));
--ALTER TABLE TB_DRIVER_NAMES DROP COLUMN VENDOR_FLAG;
--ALTER TABLE TB_DRIVER_NAMES DROP COLUMN TAX_PAID_TO_VENDOR_FLAG;
--ALTER TABLE TB_DRIVER_NAMES DROP COLUMN ESTIMATOR_DRIVER_FLAG;
--ALTER TABLE TB_DRIVER_NAMES DROP COLUMN ESTIMATOR_HEADER_INDEX;
ALTER TABLE TB_DW_COLUMN ADD (COLUMN_ORDER  NUMBER);
ALTER TABLE TB_DW_COLUMN ADD (ALL_COLUMNS  CHAR(1 BYTE));
--ALTER TABLE TB_DW_COLUMN DROP COLUMN COLUMN_ID;
ALTER TABLE TB_IMPORT_DEFINITION
MODIFY(SAMPLE_FILE_NAME VARCHAR2(4000 BYTE));
--ALTER TABLE TB_IMPORT_DEFINITION DROP COLUMN IMPORT_LOADER;
--ALTER TABLE TB_IMPORT_DEFINITION DROP COLUMN FLAT_FIXED_RECORD_LENGTH;
--ALTER TABLE TB_IMPORT_DEFINITION DROP COLUMN DB_TABLE_NAME;
--ALTER TABLE TB_IMPORT_DEFINITION DROP COLUMN XML_DOM_NAME;
--ALTER TABLE TB_IMPORT_DEFINITION DROP COLUMN STORED_PROC_NAME;
ALTER TABLE TB_IMPORT_DEFINITION CACHE;
--ALTER TABLE TB_IMPORT_DEFINITION_DETAIL DROP COLUMN IMPORT_COLUMN_START;
--ALTER TABLE TB_IMPORT_DEFINITION_DETAIL DROP COLUMN IMPORT_COLUMN_LENGTH;
--ALTER TABLE TB_IMPORT_DEFINITION_DETAIL DROP COLUMN IMPORT_COLUMN_DECIMALS;
ALTER TABLE TB_IMPORT_DEFINITION_DETAIL CACHE;
--ALTER TABLE TB_IMPORT_SPEC DROP COLUMN SUBMIT_JOB_FLAG;
ALTER TABLE TB_JURISDICTION_TAXRATE
MODIFY(EFFECTIVE_DATE  DEFAULT SYSDATE);
ALTER TABLE TB_LIST_CODE CACHE;
ALTER TABLE TB_LOCATION_MATRIX
MODIFY(EFFECTIVE_DATE  DEFAULT SYSDATE);
ALTER TABLE TB_LOCATION_MATRIX
MODIFY(EXPIRATION_DATE  DEFAULT TO_DATE('12/31/9999','mm/dd/yyyy'));
CREATE TABLE TB_PAGE_ATTRIBUTES
(
  USER_ID           VARCHAR2(40 BYTE),
  PAGE              VARCHAR2(300 BYTE),
  XML               CLOB,
  UPDATE_USER_ID    VARCHAR2(40 BYTE)           DEFAULT USER,
  UPDATE_TIMESTAMP  DATE                        DEFAULT SYSDATE
)
NOLOGGING 
CACHE
NOPARALLEL;
ALTER TABLE TB_TAXCODE_STATE NOCACHE;
ALTER TABLE TB_TAX_MATRIX
MODIFY(EFFECTIVE_DATE  DEFAULT SYSDATE);
ALTER TABLE TB_TAX_MATRIX
MODIFY(EXPIRATION_DATE  DEFAULT TO_DATE('12/31/9999','mm/dd/yyyy'));
DROP TABLE TB_TMP_COMPUTE_DISTAMT_1;
CREATE GLOBAL TEMPORARY TABLE TB_TMP_COMPUTE_DISTAMT_1
(
  COMPUTE_COUNT       NUMBER,
  COMPUTE_DISTAMT     NUMBER,
  COMPUTE_TAXAMT      NUMBER,
  COMPUTE_EFFTAXRATE  NUMBER,
  COMPUTE_MAXDISTAMT  NUMBER,
  COMPUTE_AVGDISTAMT  NUMBER
)
ON COMMIT PRESERVE ROWS;
DROP TABLE TB_TMP_COMPUTE_DISTAMT_2;
CREATE GLOBAL TEMPORARY TABLE TB_TMP_COMPUTE_DISTAMT_2
(
  COMPUTE_COUNT       NUMBER,
  COMPUTE_DISTAMT     NUMBER,
  COMPUTE_TAXAMT      NUMBER,
  COMPUTE_EFFTAXRATE  NUMBER,
  COMPUTE_MAXDISTAMT  NUMBER,
  COMPUTE_AVGDISTAMT  NUMBER
)
ON COMMIT PRESERVE ROWS;
DROP TABLE TB_TMP_LOC_MTRX_DRIVERS_COUNT;
CREATE GLOBAL TEMPORARY TABLE TB_TMP_LOC_MTRX_DRIVERS_COUNT
(
  DRIVER_01  NUMBER,
  DRIVER_02  NUMBER,
  DRIVER_03  NUMBER,
  DRIVER_04  NUMBER,
  DRIVER_05  NUMBER,
  DRIVER_06  NUMBER,
  DRIVER_07  NUMBER,
  DRIVER_08  NUMBER,
  DRIVER_09  NUMBER,
  DRIVER_10  NUMBER
)
ON COMMIT PRESERVE ROWS;
ALTER TABLE TB_TMP_STATISTICS ADD (UPDATE_USER_ID  VARCHAR2(40 BYTE));
ALTER TABLE TB_TMP_STATISTICS ADD (UPDATE_TIMESTAMP  DATE);
DROp TABLE TB_TMP_TAX_MTRX_DRIVERS_COUNT;
CREATE GLOBAL TEMPORARY TABLE TB_TMP_TAX_MTRX_DRIVERS_COUNT
(
  DRIVER_01  NUMBER,
  DRIVER_02  NUMBER,
  DRIVER_03  NUMBER,
  DRIVER_04  NUMBER,
  DRIVER_05  NUMBER,
  DRIVER_06  NUMBER,
  DRIVER_07  NUMBER,
  DRIVER_08  NUMBER,
  DRIVER_09  NUMBER,
  DRIVER_10  NUMBER,
  DRIVER_11  NUMBER,
  DRIVER_12  NUMBER,
  DRIVER_13  NUMBER,
  DRIVER_14  NUMBER,
  DRIVER_15  NUMBER,
  DRIVER_16  NUMBER,
  DRIVER_17  NUMBER,
  DRIVER_18  NUMBER,
  DRIVER_19  NUMBER,
  DRIVER_20  NUMBER,
  DRIVER_21  NUMBER,
  DRIVER_22  NUMBER,
  DRIVER_23  NUMBER,
  DRIVER_24  NUMBER,
  DRIVER_25  NUMBER,
  DRIVER_26  NUMBER,
  DRIVER_27  NUMBER,
  DRIVER_28  NUMBER,
  DRIVER_29  NUMBER,
  DRIVER_30  NUMBER
)
ON COMMIT PRESERVE ROWS;
DROp TABLE TB_TMP_TRANSACTION_DETAIL_B_U;
CREATE GLOBAL TEMPORARY TABLE TB_TMP_TRANSACTION_DETAIL_B_U
(
  TRANSACTION_DETAIL_ID        NUMBER,
  SOURCE_TRANSACTION_ID        VARCHAR2(100 BYTE),
  PROCESS_BATCH_NO             NUMBER,
  GL_EXTRACT_BATCH_NO          NUMBER,
  ARCHIVE_BATCH_NO             NUMBER,
  ALLOCATION_MATRIX_ID         NUMBER,
  ALLOCATION_SUBTRANS_ID       NUMBER,
  ENTERED_DATE                 DATE,
  TRANSACTION_STATUS           VARCHAR2(10 BYTE),
  GL_DATE                      DATE,
  GL_COMPANY_NBR               VARCHAR2(100 BYTE),
  GL_COMPANY_NAME              VARCHAR2(100 BYTE),
  GL_DIVISION_NBR              VARCHAR2(100 BYTE),
  GL_DIVISION_NAME             VARCHAR2(100 BYTE),
  GL_CC_NBR_DEPT_ID            VARCHAR2(100 BYTE),
  GL_CC_NBR_DEPT_NAME          VARCHAR2(100 BYTE),
  GL_LOCAL_ACCT_NBR            VARCHAR2(100 BYTE),
  GL_LOCAL_ACCT_NAME           VARCHAR2(100 BYTE),
  GL_LOCAL_SUB_ACCT_NBR        VARCHAR2(100 BYTE),
  GL_LOCAL_SUB_ACCT_NAME       VARCHAR2(100 BYTE),
  GL_FULL_ACCT_NBR             VARCHAR2(100 BYTE),
  GL_FULL_ACCT_NAME            VARCHAR2(100 BYTE),
  GL_LINE_ITM_DIST_AMT         NUMBER,
  ORIG_GL_LINE_ITM_DIST_AMT    NUMBER,
  VENDOR_NBR                   VARCHAR2(100 BYTE),
  VENDOR_NAME                  VARCHAR2(100 BYTE),
  VENDOR_ADDRESS_LINE_1        VARCHAR2(100 BYTE),
  VENDOR_ADDRESS_LINE_2        VARCHAR2(100 BYTE),
  VENDOR_ADDRESS_LINE_3        VARCHAR2(100 BYTE),
  VENDOR_ADDRESS_LINE_4        VARCHAR2(100 BYTE),
  VENDOR_ADDRESS_CITY          VARCHAR2(100 BYTE),
  VENDOR_ADDRESS_COUNTY        VARCHAR2(100 BYTE),
  VENDOR_ADDRESS_STATE         VARCHAR2(100 BYTE),
  VENDOR_ADDRESS_ZIP           VARCHAR2(100 BYTE),
  VENDOR_ADDRESS_COUNTRY       VARCHAR2(100 BYTE),
  VENDOR_TYPE                  VARCHAR2(100 BYTE),
  VENDOR_TYPE_NAME             VARCHAR2(100 BYTE),
  INVOICE_NBR                  VARCHAR2(100 BYTE),
  INVOICE_DESC                 VARCHAR2(100 BYTE),
  INVOICE_DATE                 DATE,
  INVOICE_FREIGHT_AMT          NUMBER,
  INVOICE_DISCOUNT_AMT         NUMBER,
  INVOICE_TAX_AMT              NUMBER,
  INVOICE_TOTAL_AMT            NUMBER,
  INVOICE_TAX_FLG              VARCHAR2(100 BYTE),
  INVOICE_LINE_NBR             VARCHAR2(100 BYTE),
  INVOICE_LINE_NAME            VARCHAR2(100 BYTE),
  INVOICE_LINE_TYPE            VARCHAR2(100 BYTE),
  INVOICE_LINE_TYPE_NAME       VARCHAR2(100 BYTE),
  INVOICE_LINE_AMT             NUMBER,
  INVOICE_LINE_TAX             NUMBER,
  AFE_PROJECT_NBR              VARCHAR2(100 BYTE),
  AFE_PROJECT_NAME             VARCHAR2(100 BYTE),
  AFE_CATEGORY_NBR             VARCHAR2(100 BYTE),
  AFE_CATEGORY_NAME            VARCHAR2(100 BYTE),
  AFE_SUB_CAT_NBR              VARCHAR2(100 BYTE),
  AFE_SUB_CAT_NAME             VARCHAR2(100 BYTE),
  AFE_USE                      VARCHAR2(100 BYTE),
  AFE_CONTRACT_TYPE            VARCHAR2(100 BYTE),
  AFE_CONTRACT_STRUCTURE       VARCHAR2(100 BYTE),
  AFE_PROPERTY_CAT             VARCHAR2(100 BYTE),
  INVENTORY_NBR                VARCHAR2(100 BYTE),
  INVENTORY_NAME               VARCHAR2(100 BYTE),
  INVENTORY_CLASS              VARCHAR2(100 BYTE),
  INVENTORY_CLASS_NAME         VARCHAR2(100 BYTE),
  PO_NBR                       VARCHAR2(100 BYTE),
  PO_NAME                      VARCHAR2(100 BYTE),
  PO_DATE                      DATE,
  PO_LINE_NBR                  VARCHAR2(100 BYTE),
  PO_LINE_NAME                 VARCHAR2(100 BYTE),
  PO_LINE_TYPE                 VARCHAR2(100 BYTE),
  PO_LINE_TYPE_NAME            VARCHAR2(100 BYTE),
  SHIP_TO_LOCATION             VARCHAR2(100 BYTE),
  SHIP_TO_LOCATION_NAME        VARCHAR2(100 BYTE),
  SHIP_TO_ADDRESS_LINE_1       VARCHAR2(100 BYTE),
  SHIP_TO_ADDRESS_LINE_2       VARCHAR2(100 BYTE),
  SHIP_TO_ADDRESS_LINE_3       VARCHAR2(100 BYTE),
  SHIP_TO_ADDRESS_LINE_4       VARCHAR2(100 BYTE),
  SHIP_TO_ADDRESS_CITY         VARCHAR2(100 BYTE),
  SHIP_TO_ADDRESS_COUNTY       VARCHAR2(100 BYTE),
  SHIP_TO_ADDRESS_STATE        VARCHAR2(100 BYTE),
  SHIP_TO_ADDRESS_ZIP          VARCHAR2(100 BYTE),
  SHIP_TO_ADDRESS_COUNTRY      VARCHAR2(100 BYTE),
  WO_NBR                       VARCHAR2(100 BYTE),
  WO_NAME                      VARCHAR2(100 BYTE),
  WO_DATE                      DATE,
  WO_TYPE                      VARCHAR2(100 BYTE),
  WO_TYPE_DESC                 VARCHAR2(100 BYTE),
  WO_CLASS                     VARCHAR2(100 BYTE),
  WO_CLASS_DESC                VARCHAR2(100 BYTE),
  WO_ENTITY                    VARCHAR2(100 BYTE),
  WO_ENTITY_DESC               VARCHAR2(100 BYTE),
  WO_LINE_NBR                  VARCHAR2(100 BYTE),
  WO_LINE_NAME                 VARCHAR2(100 BYTE),
  WO_LINE_TYPE                 VARCHAR2(100 BYTE),
  WO_LINE_TYPE_DESC            VARCHAR2(100 BYTE),
  WO_SHUT_DOWN_CD              VARCHAR2(100 BYTE),
  WO_SHUT_DOWN_CD_DESC         VARCHAR2(100 BYTE),
  VOUCHER_ID                   VARCHAR2(100 BYTE),
  VOUCHER_NAME                 VARCHAR2(100 BYTE),
  VOUCHER_DATE                 DATE,
  VOUCHER_LINE_NBR             VARCHAR2(100 BYTE),
  VOUCHER_LINE_DESC            VARCHAR2(100 BYTE),
  CHECK_NBR                    VARCHAR2(100 BYTE),
  CHECK_NO                     NUMBER,
  CHECK_DATE                   DATE,
  CHECK_AMT                    NUMBER,
  CHECK_DESC                   VARCHAR2(100 BYTE),
  USER_TEXT_01                 VARCHAR2(100 BYTE),
  USER_TEXT_02                 VARCHAR2(100 BYTE),
  USER_TEXT_03                 VARCHAR2(100 BYTE),
  USER_TEXT_04                 VARCHAR2(100 BYTE),
  USER_TEXT_05                 VARCHAR2(100 BYTE),
  USER_TEXT_06                 VARCHAR2(100 BYTE),
  USER_TEXT_07                 VARCHAR2(100 BYTE),
  USER_TEXT_08                 VARCHAR2(100 BYTE),
  USER_TEXT_09                 VARCHAR2(100 BYTE),
  USER_TEXT_10                 VARCHAR2(100 BYTE),
  USER_TEXT_11                 VARCHAR2(100 BYTE),
  USER_TEXT_12                 VARCHAR2(100 BYTE),
  USER_TEXT_13                 VARCHAR2(100 BYTE),
  USER_TEXT_14                 VARCHAR2(100 BYTE),
  USER_TEXT_15                 VARCHAR2(100 BYTE),
  USER_TEXT_16                 VARCHAR2(100 BYTE),
  USER_TEXT_17                 VARCHAR2(100 BYTE),
  USER_TEXT_18                 VARCHAR2(100 BYTE),
  USER_TEXT_19                 VARCHAR2(100 BYTE),
  USER_TEXT_20                 VARCHAR2(100 BYTE),
  USER_TEXT_21                 VARCHAR2(100 BYTE),
  USER_TEXT_22                 VARCHAR2(100 BYTE),
  USER_TEXT_23                 VARCHAR2(100 BYTE),
  USER_TEXT_24                 VARCHAR2(100 BYTE),
  USER_TEXT_25                 VARCHAR2(100 BYTE),
  USER_TEXT_26                 VARCHAR2(100 BYTE),
  USER_TEXT_27                 VARCHAR2(100 BYTE),
  USER_TEXT_28                 VARCHAR2(100 BYTE),
  USER_TEXT_29                 VARCHAR2(100 BYTE),
  USER_TEXT_30                 VARCHAR2(100 BYTE),
  USER_NUMBER_01               NUMBER,
  USER_NUMBER_02               NUMBER,
  USER_NUMBER_03               NUMBER,
  USER_NUMBER_04               NUMBER,
  USER_NUMBER_05               NUMBER,
  USER_NUMBER_06               NUMBER,
  USER_NUMBER_07               NUMBER,
  USER_NUMBER_08               NUMBER,
  USER_NUMBER_09               NUMBER,
  USER_NUMBER_10               NUMBER,
  USER_DATE_01                 DATE,
  USER_DATE_02                 DATE,
  USER_DATE_03                 DATE,
  USER_DATE_04                 DATE,
  USER_DATE_05                 DATE,
  USER_DATE_06                 DATE,
  USER_DATE_07                 DATE,
  USER_DATE_08                 DATE,
  USER_DATE_09                 DATE,
  USER_DATE_10                 DATE,
  COMMENTS                     VARCHAR2(4000 BYTE),
  TB_CALC_TAX_AMT              NUMBER,
  STATE_USE_AMOUNT             NUMBER,
  STATE_USE_TIER2_AMOUNT       NUMBER,
  STATE_USE_TIER3_AMOUNT       NUMBER,
  COUNTY_USE_AMOUNT            NUMBER,
  COUNTY_LOCAL_USE_AMOUNT      NUMBER,
  CITY_USE_AMOUNT              NUMBER,
  CITY_LOCAL_USE_AMOUNT        NUMBER,
  TRANSACTION_STATE_CODE       VARCHAR2(10 BYTE),
  AUTO_TRANSACTION_STATE_CODE  VARCHAR2(10 BYTE),
  TRANSACTION_IND              VARCHAR2(10 BYTE),
  SUSPEND_IND                  VARCHAR2(10 BYTE),
  TAXCODE_DETAIL_ID            NUMBER,
  TAXCODE_STATE_CODE           VARCHAR2(10 BYTE),
  TAXCODE_TYPE_CODE            VARCHAR2(10 BYTE),
  TAXCODE_CODE                 VARCHAR2(40 BYTE),
  CCH_TAXCAT_CODE              VARCHAR2(2 BYTE),
  CCH_GROUP_CODE               VARCHAR2(4 BYTE),
  CCH_ITEM_CODE                VARCHAR2(3 BYTE),
  MANUAL_TAXCODE_IND           VARCHAR2(10 BYTE),
  TAX_MATRIX_ID                NUMBER,
  LOCATION_MATRIX_ID           NUMBER,
  JURISDICTION_ID              NUMBER,
  JURISDICTION_TAXRATE_ID      NUMBER,
  MANUAL_JURISDICTION_IND      VARCHAR2(10 BYTE),
  MEASURE_TYPE_CODE            VARCHAR2(10 BYTE),
  STATE_USE_RATE               NUMBER,
  STATE_USE_TIER2_RATE         NUMBER,
  STATE_USE_TIER3_RATE         NUMBER,
  STATE_SPLIT_AMOUNT           NUMBER,
  STATE_TIER2_MIN_AMOUNT       NUMBER,
  STATE_TIER2_MAX_AMOUNT       NUMBER,
  STATE_MAXTAX_AMOUNT          NUMBER,
  COUNTY_USE_RATE              NUMBER,
  COUNTY_LOCAL_USE_RATE        NUMBER,
  COUNTY_SPLIT_AMOUNT          NUMBER,
  COUNTY_MAXTAX_AMOUNT         NUMBER,
  COUNTY_SINGLE_FLAG           CHAR(1 BYTE),
  COUNTY_DEFAULT_FLAG          CHAR(1 BYTE),
  CITY_USE_RATE                NUMBER,
  CITY_LOCAL_USE_RATE          NUMBER,
  CITY_SPLIT_AMOUNT            NUMBER,
  CITY_SPLIT_USE_RATE          NUMBER,
  CITY_SINGLE_FLAG             CHAR(1 BYTE),
  CITY_DEFAULT_FLAG            CHAR(1 BYTE),
  COMBINED_USE_RATE            NUMBER,
  LOAD_TIMESTAMP               DATE,
  GL_EXTRACT_UPDATER           VARCHAR2(40 BYTE),
  GL_EXTRACT_TIMESTAMP         DATE,
  GL_EXTRACT_FLAG              NUMBER,
  GL_LOG_FLAG                  NUMBER,
  GL_EXTRACT_AMT               NUMBER,
  AUDIT_FLAG                   CHAR(1 BYTE),
  AUDIT_USER_ID                VARCHAR2(40 BYTE),
  AUDIT_TIMESTAMP              DATE,
  MODIFY_USER_ID               VARCHAR2(40 BYTE),
  MODIFY_TIMESTAMP             DATE,
  UPDATE_USER_ID               VARCHAR2(40 BYTE),
  UPDATE_TIMESTAMP             DATE
)
ON COMMIT PRESERVE ROWS;
ALTER TABLE TB_USER_ENTITY NOCACHE;
CREATE INDEX IDX_GL_EXPORT_LOG_TRANS_ID ON TB_GL_EXPORT_LOG
(TRANSACTION_DETAIL_ID)
NOLOGGING
NOPARALLEL;
CREATE INDEX IDX_GL_REPORT_LOG_EX_TIME ON TB_GL_REPORT_LOG
(GL_EXTRACT_TIMESTAMP)
NOLOGGING
NOPARALLEL;
CREATE INDEX IDX_GL_REPORT_LOG_JURIS_ID ON TB_GL_REPORT_LOG
(JURISDICTION_ID)
NOLOGGING
NOPARALLEL;
CREATE INDEX IDX_PROC_TRANS_MATRIX_LOOKUP ON TB_TAX_MATRIX
(MATRIX_STATE_CODE, EFFECTIVE_DATE, EXPIRATION_DATE, THEN_TAXCODE_DETAIL_ID, ELSE_TAXCODE_DETAIL_ID, 
DRIVER_01, DRIVER_02, DRIVER_03, DRIVER_04, DRIVER_05, 
DRIVER_06, DRIVER_07, DRIVER_08, DRIVER_09, DRIVER_10, 
DRIVER_11, DRIVER_12, DRIVER_13, DRIVER_14, DRIVER_15, 
DRIVER_16, DRIVER_17, DRIVER_18, DRIVER_19, DRIVER_20, 
DRIVER_21, DRIVER_22)
LOGGING
NOPARALLEL;
CREATE INDEX IDX_PRO_TRAN_LOC_MATRIX_LOOKUP ON TB_LOCATION_MATRIX
(EFFECTIVE_DATE, EXPIRATION_DATE, JURISDICTION_ID, DRIVER_01, DRIVER_02, 
DRIVER_03, DRIVER_04)
LOGGING
NOPARALLEL;
CREATE INDEX IDX_TB_JUR_TAXRATE_EFF_DT ON TB_JURISDICTION_TAXRATE
(JURISDICTION_TAXRATE_ID, EFFECTIVE_DATE)
NOLOGGING
NOPARALLEL;
DROP INDEX IDX_TB_TRANS_DTL_DRIVER_01;
CREATE INDEX IDX_TB_TRANS_DTL_DRIVER_01 ON TB_TRANSACTION_DETAIL
(VENDOR_TYPE)
NOLOGGING
NOPARALLEL;
DROP INDEX IDX_TB_TRANS_DTL_DRIVER_01_UP;
CREATE INDEX IDX_TB_TRANS_DTL_DRIVER_01_UP ON TB_TRANSACTION_DETAIL
(UPPER("VENDOR_TYPE"))
NOLOGGING
NOPARALLEL;
DROP INDEX IDX_TB_TRANS_DTL_DRIVER_02;
CREATE INDEX IDX_TB_TRANS_DTL_DRIVER_02 ON TB_TRANSACTION_DETAIL
(GL_COMPANY_NBR)
NOLOGGING
NOPARALLEL;
DROP INDEX IDX_TB_TRANS_DTL_DRIVER_02_UP;
CREATE INDEX IDX_TB_TRANS_DTL_DRIVER_02_UP ON TB_TRANSACTION_DETAIL
(UPPER("GL_COMPANY_NBR"))
NOLOGGING
NOPARALLEL;
DROP INDEX IDX_TB_TRANS_DTL_DRIVER_03;
CREATE INDEX IDX_TB_TRANS_DTL_DRIVER_03 ON TB_TRANSACTION_DETAIL
(GL_DIVISION_NBR)
NOLOGGING
NOPARALLEL;
DROP INDEX IDX_TB_TRANS_DTL_DRIVER_03_UP;
CREATE INDEX IDX_TB_TRANS_DTL_DRIVER_03_UP ON TB_TRANSACTION_DETAIL
(UPPER("GL_DIVISION_NBR"))
NOLOGGING
NOPARALLEL;
DROP INDEX IDX_TB_TRANS_DTL_DRIVER_04;
CREATE INDEX IDX_TB_TRANS_DTL_DRIVER_04 ON TB_TRANSACTION_DETAIL
(VENDOR_NBR)
NOLOGGING
NOPARALLEL;
DROP INDEX IDX_TB_TRANS_DTL_DRIVER_04_UP;
CREATE INDEX IDX_TB_TRANS_DTL_DRIVER_04_UP ON TB_TRANSACTION_DETAIL
(UPPER("VENDOR_NBR"))
NOLOGGING
NOPARALLEL;
DROP INDEX IDX_TB_TRANS_DTL_DRIVER_05;
CREATE INDEX IDX_TB_TRANS_DTL_DRIVER_05 ON TB_TRANSACTION_DETAIL
(GL_LOCAL_ACCT_NBR)
NOLOGGING
NOPARALLEL;
DROP INDEX IDX_TB_TRANS_DTL_DRIVER_05_UP;
CREATE INDEX IDX_TB_TRANS_DTL_DRIVER_05_UP ON TB_TRANSACTION_DETAIL
(UPPER("GL_LOCAL_ACCT_NBR"))
NOLOGGING
NOPARALLEL;
DROP INDEX IDX_TB_TRANS_DTL_DRIVER_06;
CREATE INDEX IDX_TB_TRANS_DTL_DRIVER_06 ON TB_TRANSACTION_DETAIL
(GL_CC_NBR_DEPT_ID)
NOLOGGING
NOPARALLEL;
DROP INDEX IDX_TB_TRANS_DTL_DRIVER_06_UP;
CREATE INDEX IDX_TB_TRANS_DTL_DRIVER_06_UP ON TB_TRANSACTION_DETAIL
(UPPER("GL_CC_NBR_DEPT_ID"))
NOLOGGING
NOPARALLEL;
DROP INDEX IDX_TB_TRANS_DTL_DRIVER_07;
CREATE INDEX IDX_TB_TRANS_DTL_DRIVER_07 ON TB_TRANSACTION_DETAIL
(AFE_PROJECT_NBR)
NOLOGGING
NOPARALLEL;
DROP INDEX IDX_TB_TRANS_DTL_DRIVER_07_UP;
CREATE INDEX IDX_TB_TRANS_DTL_DRIVER_07_UP ON TB_TRANSACTION_DETAIL
(UPPER("AFE_PROJECT_NBR"))
NOLOGGING
NOPARALLEL;
DROP INDEX IDX_TB_TRANS_DTL_DRIVER_08;
CREATE INDEX IDX_TB_TRANS_DTL_DRIVER_08 ON TB_TRANSACTION_DETAIL
(WO_TYPE)
NOLOGGING
NOPARALLEL;
DROP INDEX IDX_TB_TRANS_DTL_DRIVER_08_UP;
CREATE INDEX IDX_TB_TRANS_DTL_DRIVER_08_UP ON TB_TRANSACTION_DETAIL
(UPPER("WO_TYPE"))
NOLOGGING
NOPARALLEL;
DROP INDEX IDX_TB_TRANS_DTL_DRIVER_09;
CREATE INDEX IDX_TB_TRANS_DTL_DRIVER_09 ON TB_TRANSACTION_DETAIL
(SHIP_TO_LOCATION)
NOLOGGING
NOPARALLEL;
DROP INDEX IDX_TB_TRANS_DTL_DRIVER_09_UP;
CREATE INDEX IDX_TB_TRANS_DTL_DRIVER_09_UP ON TB_TRANSACTION_DETAIL
(UPPER("SHIP_TO_LOCATION"))
NOLOGGING
NOPARALLEL;
DROP INDEX IDX_TB_TRANS_DTL_DRIVER_10;
CREATE INDEX IDX_TB_TRANS_DTL_DRIVER_10 ON TB_TRANSACTION_DETAIL
(INVENTORY_NBR)
NOLOGGING
NOPARALLEL;
DROP INDEX IDX_TB_TRANS_DTL_DRIVER_10_UP;
CREATE INDEX IDX_TB_TRANS_DTL_DRIVER_10_UP ON TB_TRANSACTION_DETAIL
(UPPER("INVENTORY_NBR"))
NOLOGGING
NOPARALLEL;
DROP INDEX IDX_TB_TRANS_DTL_DRIVER_11;
CREATE INDEX IDX_TB_TRANS_DTL_DRIVER_11 ON TB_TRANSACTION_DETAIL
(WO_NBR)
NOLOGGING
NOPARALLEL;
DROP INDEX IDX_TB_TRANS_DTL_DRIVER_11_UP;
CREATE INDEX IDX_TB_TRANS_DTL_DRIVER_11_UP ON TB_TRANSACTION_DETAIL
(UPPER("WO_NBR"))
NOLOGGING
NOPARALLEL;
DROP INDEX IDX_TB_TRANS_DTL_DRIVER_12;
CREATE INDEX IDX_TB_TRANS_DTL_DRIVER_12 ON TB_TRANSACTION_DETAIL
(PO_NBR)
NOLOGGING
NOPARALLEL;
DROP INDEX IDX_TB_TRANS_DTL_DRIVER_12_UP;
CREATE INDEX IDX_TB_TRANS_DTL_DRIVER_12_UP ON TB_TRANSACTION_DETAIL
(UPPER("PO_NBR"))
NOLOGGING
NOPARALLEL;
CREATE INDEX IDX_TB_TRANS_MATRIX_LOOKUP ON TB_TRANSACTION_DETAIL
(GL_DATE, TRANSACTION_IND, SUSPEND_IND)
NOLOGGING
NOPARALLEL;
CREATE INDEX IDX_TRANS_DETAIL_PROCESS_BATCH ON TB_TRANSACTION_DETAIL
(PROCESS_BATCH_NO, TRANSACTION_DETAIL_ID)
NOLOGGING
NOPARALLEL;
CREATE INDEX IDX_TRANS_DET_JURISDICTION_ID ON TB_TRANSACTION_DETAIL
(JURISDICTION_ID)
NOLOGGING
NOPARALLEL;
CREATE UNIQUE INDEX PK_TB_BCP_JURISDICTION_TAXRATE ON TB_BCP_JURISDICTION_TAXRATE
(BCP_JURISDICTION_TAXRATE_ID)
LOGGING
NOPARALLEL;
CREATE UNIQUE INDEX PK_TB_BCP_JURI_TAXRATE_TEXT ON TB_BCP_JURI_TAXRATE_TEXT
(BATCH_ID, LINE)
LOGGING
NOPARALLEL;
CREATE UNIQUE INDEX PK_TB_BCP_TRANSACTIONS ON TB_BCP_TRANSACTIONS
(BCP_TRANSACTION_ID)
LOGGING
NOPARALLEL;
CREATE UNIQUE INDEX PK_TB_CCH_TXMATRIX ON TB_CCH_TXMATRIX
(GROUPCODE, RECTYPE, ITEM)
LOGGING
NOPARALLEL;
DROP INDEX PK_TB_IMPORT_DEFINITION_DETAIL;
CREATE UNIQUE INDEX PK_TB_IMPORT_DEFINITION_DETAIL ON TB_IMPORT_DEFINITION_DETAIL
(IMPORT_DEFINITION_CODE, TRANS_DTL_COLUMN_NAME)
LOGGING
NOPARALLEL;

CREATE OR REPLACE PROCEDURE sp_compute_distamt_1(
	p_group_by		  IN VARCHAR2,
	p_where_clause_token	  IN VARCHAR2
)
AS
BEGIN
  EXECUTE IMMEDIATE '
          INSERT INTO tb_tmp_compute_distamt_1(COMPUTE_COUNT,COMPUTE_DISTAMT,COMPUTE_TAXAMT,COMPUTE_EFFTAXRATE,COMPUTE_MAXDISTAMT,COMPUTE_AVGDISTAMT) 
	  SELECT COMPUTE_COUNT,COMPUTE_DISTAMT,COMPUTE_TAXAMT,COMPUTE_EFFTAXRATE,COMPUTE_MAXDISTAMT,COMPUTE_AVGDISTAMT FROM (
	  SELECT '||p_group_by||',
		COUNT (*) AS COMPUTE_COUNT, 
		SUM (GL_LINE_ITM_DIST_AMT) AS COMPUTE_DISTAMT, 
		SUM (TB_CALC_TAX_AMT) AS COMPUTE_TAXAMT, 
		CASE 
			SUM(GL_LINE_ITM_DIST_AMT) 
			WHEN 0 
			THEN 0
			ELSE (SUM (TB_CALC_TAX_AMT) / SUM (GL_LINE_ITM_DIST_AMT)) 
		END AS  COMPUTE_EFFTAXRATE, 
		MAX (GL_LINE_ITM_DIST_AMT) AS COMPUTE_MAXDISTAMT,
		AVG (GL_LINE_ITM_DIST_AMT) AS COMPUTE_AVGDISTAMT      
	 FROM TB_TRANSACTION_DETAIL 
	 LEFT JOIN TB_JURISDICTION ON (TB_TRANSACTION_DETAIL.jurisdiction_id = TB_JURISDICTION.jurisdiction_id)
	 GROUP BY '||p_group_by||' ) WHERE 1=1 '||p_where_clause_token;
END;
/

SHOW ERRORS;

CREATE OR REPLACE PROCEDURE sp_compute_distamt_2 (
           p_group_by             IN VARCHAR2,
	   p_where_clause_token	  IN VARCHAR2
)
AS
BEGIN

  EXECUTE IMMEDIATE '
          INSERT INTO tb_tmp_compute_distamt_2(COMPUTE_COUNT,COMPUTE_DISTAMT,COMPUTE_TAXAMT,COMPUTE_EFFTAXRATE,COMPUTE_MAXDISTAMT,COMPUTE_AVGDISTAMT) 
	  SELECT COMPUTE_COUNT,COMPUTE_DISTAMT,COMPUTE_TAXAMT,COMPUTE_EFFTAXRATE,COMPUTE_MAXDISTAMT,COMPUTE_AVGDISTAMT FROM (
	  SELECT COUNT (*) AS COMPUTE_COUNT, 
		 SUM (GL_LINE_ITM_DIST_AMT) AS COMPUTE_DISTAMT, 
		 SUM (TB_CALC_TAX_AMT) AS COMPUTE_TAXAMT, 
		 CASE SUM(GL_LINE_ITM_DIST_AMT) WHEN 0 THEN 0 ELSE (SUM (TB_CALC_TAX_AMT) / SUM (GL_LINE_ITM_DIST_AMT)) END AS COMPUTE_EFFTAXRATE, 
		 MAX (GL_LINE_ITM_DIST_AMT) AS COMPUTE_MAXDISTAMT, 
		 AVG (GL_LINE_ITM_DIST_AMT) AS COMPUTE_AVGDISTAMT, 
		'||p_group_by||' 
	  FROM	  TB_TRANSACTION_DETAIL 
	  LEFT JOIN TB_JURISDICTION ON (TB_TRANSACTION_DETAIL.jurisdiction_id = TB_JURISDICTION.jurisdiction_id)
	  GROUP BY '||p_group_by||' ) WHERE 1=1 '||p_where_clause_token;

END;
/

SHOW ERRORS;

CREATE OR REPLACE PROCEDURE sp_db_statistics(avc_where in VARCHAR2)
/* ************************************************************************************************/
/* Object Type/Name: Stored Procedure - sp_batch_process                                          */
/* Author:           Michael B. Fuller                                                            */
/* Date:             a long time ago                                                              */
/* Description:      process a batch of transactions                                              */
/* Arguments:        an_batch_id number                                                           */
/* Returns:          None                                                                         */
/* ************************************************************************************************/
/* Maintenance History:                                                                           */
/* Scan  Author       Date     Release    Description                                  Ticket #   */
/* ----- ---------- ---------- ---------- -------------------------------------------- ---------- */
/*       M. Fuller  06/02/2006 3.3.5.0    Fix processing an allocated line - Temporary 871        */
/*       M. Fuller  01/04/2007 3.3.5.1    Fix processing an allocated line - Permenant 871        */
/* MBF01 M. Fuller                        Add Kill Process Flag                                   */
/* MBF02 M. Fuller  01/25/2007 3.3.5.1    Add Allocations                                         */
/* MBF03 M. Fuller  05/18/2007 3.3.7a.x   Add User Exit to end of procedure            888        */
/* MBF04 M. Fuller  08/13/2007 3.4.1.1    Add 5 Calc Tax Flags                         883        */
/* MBF05 M. Fuller  10/10/2008 5.1.1.1    Change SQL to ANSI standard                  3464       */
/* ************************************************************************************************/
IS
-- Define Cursor Type
   TYPE cur_type is REF CURSOR;

-- Statistics Selection SQL
-- MBF -- Removed hints -- 5.1.1.1
   vc_stat_total                   VARCHAR2 (1000) :=
      'SELECT count(*), sum(gl_line_itm_dist_amt), sum(tb_calc_tax_amt) ' ||
      'FROM tb_transaction_detail LEFT JOIN tb_jurisdiction ON (tb_transaction_detail.jurisdiction_id = tb_jurisdiction.jurisdiction_id) ';
   vc_stat_select                  VARCHAR2 (1000) :=
      'SELECT count(*), sum(gl_line_itm_dist_amt), sum(tb_calc_tax_amt), max(gl_line_itm_dist_amt), avg(gl_line_itm_dist_amt) ' ||
      'FROM tb_transaction_detail LEFT JOIN tb_jurisdiction ON (tb_transaction_detail.jurisdiction_id = tb_jurisdiction.jurisdiction_id) ';
   vc_stat_where                   VARCHAR2 (4000) := '';
   vc_stat_stmt                    VARCHAR2 (5000);
   stat_cursor                     cur_type;

-- Driver Reference Insert SQL
   vc_driver_ref_ins_stmt          VARCHAR2 (3000);
   vn_status                       NUMBER                                    := NULL;

-- Table defined variables
   v_count_total                   tb_tmp_statistics.count%TYPE              := 0;
   v_sum_total                     tb_tmp_statistics.sum%TYPE                := 0;
   v_sum_tax                       tb_tmp_statistics.sum%TYPE                := 0;
   v_count                         tb_tmp_statistics.count%TYPE              := 0;
   v_count_pct                     tb_tmp_statistics.count_pct%TYPE          := 0;
   v_sum                           tb_tmp_statistics.sum%TYPE                := 0;
   v_sum_pct                       tb_tmp_statistics.sum_pct%TYPE            := 0;
   v_tax_amt                       tb_tmp_statistics.tax_amt%TYPE            := 0;
   v_max                           tb_tmp_statistics.max%TYPE                := 0;
   v_avg                           tb_tmp_statistics.avg%TYPE                := 0;

-- Define Statistics Cursor  (tb_db_statistics)
   CURSOR db_statistics_cursor
   IS
      SELECT row_title, row_where, group_by_on_drilldown
      FROM tb_db_statistics
      ORDER BY db_statistics_id;
      db_statistics                 db_statistics_cursor%ROWTYPE;


-- Program starts **********************************************************************************
BEGIN

   -- Clean up tb_tmp_statistis table
   EXECUTE IMMEDIATE 'TRUNCATE TABLE tb_tmp_statistics';

   -- Get Totals for Percents-of-totals ------------------------------------------------------------
   -- Initialize fields
   vc_stat_where := '';
   vc_stat_stmt := '';

   -- Check input parameter for where clause and build SQL statement
   IF avc_where IS NULL THEN
      vc_stat_stmt := vc_stat_total;
   ELSE
	   vc_stat_stmt := vc_stat_total || avc_where;
   END IF;

	-- Debug Statements
   --insert into tb_debug(row_joe) values(vc_stat_stmt);
   --commit;

	-- Get Totals
   BEGIN
      OPEN stat_cursor
       FOR vc_stat_stmt;
      FETCH stat_cursor
       INTO v_count_total, v_sum_total, v_sum_tax;
      IF stat_cursor%FOUND THEN
         vn_status := stat_cursor%ROWCOUNT;
      ELSE
         vn_status := 0;
      END IF;
      CLOSE stat_cursor;
   EXCEPTION
      WHEN OTHERS THEN
         vn_status := 0;
   END;

   -- Read and Process Each Statistics Line  -------------------------------------------------------
   OPEN db_statistics_cursor;
   LOOP
      FETCH db_statistics_cursor INTO db_statistics;
      EXIT WHEN db_statistics_cursor%NOTFOUND;

      -- Initialize fields
      vc_stat_where := '';
      vc_stat_stmt := '';

      -- Check db_statistics table and input parameter and create where clause
      IF db_statistics.row_where IS NULL THEN
         IF avc_where IS NULL THEN
            vc_stat_where := NULL;
         ELSE
            vc_stat_where := avc_where;
         END IF;
      ELSE
         IF avc_where IS NULL THEN
            vc_stat_where := ' WHERE ' || db_statistics.row_where;
         ELSE
            vc_stat_where := avc_where || ' AND ' || db_statistics.row_where;
         END IF;
      END IF;

		-- Build SQL statement
		IF vc_stat_where IS NULL OR vc_stat_where = '' THEN
		   vc_stat_stmt := vc_stat_select;
		ELSE
		   vc_stat_stmt := vc_stat_select || vc_stat_where;
		END IF;

      -- Debug Statements
		--insert into tb_debug(row_joe) values(vc_stat_where);
      --insert into tb_debug(row_joe) values(vc_stat_stmt);
      --commit;

      -- Get Transactions
      BEGIN
         OPEN stat_cursor
          FOR vc_stat_stmt;
         FETCH stat_cursor
          INTO v_count, v_sum, v_tax_amt, v_max, v_avg;
         IF stat_cursor%FOUND THEN
            vn_status := stat_cursor%ROWCOUNT;
         ELSE
            vn_status := 0;
         END IF;
         CLOSE stat_cursor;
      EXCEPTION
         WHEN OTHERS THEN
            vn_status := 0;
      END;

      -- Calculate percents-of-totals
	  IF v_count_total <> 0 THEN
         v_count_pct := (v_count/v_count_total)*100;
      ELSE
         v_count_pct := 0;
      END IF;

      IF v_sum_total <> 0 THEN
         v_sum_pct := (v_sum/v_sum_total)*100;
      ELSE
         v_sum_pct := 0;
      END IF;

      -- Insert tmp statistics detail row
      INSERT INTO tb_tmp_statistics (
         title,
         count,
         count_pct,
         sum,
         sum_pct,
         tax_amt,
         max,
         avg,
         where_clause,
         group_by_on_drilldown )
      VALUES (
         db_statistics.row_title,
         v_count,
         v_count_pct,
         v_sum,
         v_sum_pct,
         v_tax_amt,
         v_max,
         v_avg,
         vc_stat_where,
         db_statistics.group_by_on_drilldown );

      -- Error checking
--    IF SQLCODE != 0 THEN
--       vc_status_message := 'ERROR Inserting into Transaction Detail: ' || TO_CHAR (SQLCODE);
--       INSERT INTO tb_stscdu_messages (run_date, message)
--           VALUES (v_load_timestamp, vc_status_message);
--           vi_error_count := vi_error_count + 1;
--    END IF;

   END LOOP;

   CLOSE db_statistics_cursor;

   COMMIT;

END;
/

SHOW ERRORS;

CREATE OR REPLACE PROCEDURE sp_gen_location_driver (
	p_generate_driver_reference	IN	VARCHAR2,
	p_an_batch_id			IN	NUMBER,
	p_transaction_table_name	IN	VARCHAR2,
	p_location_table_name		IN	VARCHAR2,
	p_vc_location_matrix_where	IN OUT	VARCHAR2
)
AS
	-- Define Location Driver Names Cursor (tb_driver_names)
	CURSOR location_driver_cursor
	   IS
		SELECT tb_driver_names.trans_dtl_column_name, tb_driver_names.matrix_column_name, tb_driver_names.null_driver_flag, tb_driver_names.wildcard_flag,
		       datadefcol.desc_column_name, datadefcol.data_type
		  FROM tb_driver_names,
		       (SELECT *
			  FROM tb_data_def_column
			 WHERE table_name = 'TB_TRANSACTION_DETAIL') datadefcol
		 WHERE tb_driver_names.driver_names_code = 'L' AND
		       tb_driver_names.trans_dtl_column_name = datadefcol.column_name
	      ORDER BY tb_driver_names.driver_id;

	location_driver		location_driver_cursor%ROWTYPE;

	vi_rows_processed	INTEGER		:= 0;

	vc_driver_ref_ins_stmt          VARCHAR2(5000)              := NULL;
	vi_cursor_id                    INTEGER                     := 0;
BEGIN

   -- ***** Create Dynamic WHERE Clause for Matrix SQLs ***** --
   -- Location Matrix Drivers
   OPEN location_driver_cursor;
   LOOP

                 FETCH location_driver_cursor INTO location_driver;
                  EXIT WHEN location_driver_cursor%NOTFOUND;
                  IF location_driver.null_driver_flag = '1' THEN
                     IF location_driver.wildcard_flag = '1' THEN
                        IF UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'CHAR' OR
                           UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'VARC' THEN
                              p_vc_location_matrix_where := p_vc_location_matrix_where || 'AND ' ||
                              '(('|| p_transaction_table_name ||'.' || location_driver.trans_dtl_column_name || ' IS NULL AND ('|| p_location_table_name ||'.' || location_driver.matrix_column_name || ' = ''*ALL'' OR '|| p_location_table_name ||'.' || location_driver.matrix_column_name || ' = ''*NULL'' )) OR ' ||
                              '('|| p_transaction_table_name ||'.' || location_driver.trans_dtl_column_name || ' IS NOT NULL AND ('|| p_location_table_name ||'.' || location_driver.matrix_column_name || ' = ''*ALL'' OR UPPER('|| p_transaction_table_name ||'.' || location_driver.trans_dtl_column_name || ') LIKE UPPER('|| p_location_table_name ||'.' || location_driver.matrix_column_name || ') ))) ';
                        END IF;
                     ELSE
                        IF UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'CHAR' OR
                           UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'VARC' THEN
                              p_vc_location_matrix_where := p_vc_location_matrix_where || 'AND ' ||
                              '(('|| p_transaction_table_name ||'.' || location_driver.trans_dtl_column_name || ' IS NULL AND ('|| p_location_table_name ||'.' || location_driver.matrix_column_name || ' = ''*ALL'' OR '|| p_location_table_name ||'.' || location_driver.matrix_column_name || ' = ''*NULL'' )) OR ' ||
                              '('|| p_transaction_table_name ||'.' || location_driver.trans_dtl_column_name || ' IS NOT NULL AND ('|| p_location_table_name ||'.' || location_driver.matrix_column_name || ' = ''*ALL'' OR UPPER('|| p_transaction_table_name ||'.' || location_driver.trans_dtl_column_name || ') = UPPER('|| p_location_table_name ||'.' || location_driver.matrix_column_name || ') ))) ';
                        END IF;
                     END IF;
                  ELSE
                     IF location_driver.wildcard_flag = '1' THEN
                        IF UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'CHAR' OR
                           UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'VARC' THEN
                              p_vc_location_matrix_where := p_vc_location_matrix_where || 'AND ' ||
                              '('|| p_location_table_name ||'.' || location_driver.matrix_column_name || ' = ''*ALL'' OR UPPER('|| p_transaction_table_name ||'.' || location_driver.trans_dtl_column_name || ') LIKE UPPER('|| p_location_table_name ||'.' || location_driver.matrix_column_name || ') ) ';
                        END IF;
                     ELSE
                        IF UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'CHAR' OR
                           UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'VARC' THEN
                              p_vc_location_matrix_where := p_vc_location_matrix_where || 'AND ' ||
                              '('|| p_location_table_name ||'.' || location_driver.matrix_column_name || ' = ''*ALL'' OR UPPER('|| p_transaction_table_name ||'.' || location_driver.trans_dtl_column_name || ') = UPPER('|| p_location_table_name ||'.' || location_driver.matrix_column_name || ') ) ';
                        END IF;
                     END IF;
                  END IF;

		-- Check to confirm whether we're running driver reference portion.
		-- NOTE: This is done ONLY for Batch Processing.
		IF (p_generate_driver_reference = 'Y') THEN
		      -- Insert Driver Reference Values for this driver
		      -- Create Insert SQL
		      vc_driver_ref_ins_stmt :=
			 'INSERT INTO tb_driver_reference (trans_dtl_column_name, driver_value, driver_description) ' ||
			    'SELECT ''' || Upper(location_driver.trans_dtl_column_name) ||
				    ''', ' || location_driver.trans_dtl_column_name || ', ';
		      IF location_driver.desc_column_name IS NOT NULL THEN
			 vc_driver_ref_ins_stmt := vc_driver_ref_ins_stmt || 'min(' || location_driver.desc_column_name || ') ';
		      ELSE
			 vc_driver_ref_ins_stmt := vc_driver_ref_ins_stmt || 'NULL ';
		      END IF;
		      vc_driver_ref_ins_stmt := vc_driver_ref_ins_stmt ||
			      'FROM tb_bcp_transactions ' ||
			     'WHERE process_batch_no = ' || TO_CHAR(p_an_batch_id) || ' AND ' ||
				    location_driver.trans_dtl_column_name || ' IS NOT NULL AND ' ||
				    location_driver.trans_dtl_column_name || ' NOT IN ' ||
			       '(SELECT driver_value ' ||
				  'FROM tb_driver_reference ' ||
				 'WHERE trans_dtl_column_name = ''' || Upper(location_driver.trans_dtl_column_name) || ''') ' ||
			      'GROUP BY ''' || Upper(location_driver.trans_dtl_column_name) ||
					''', ' || location_driver.trans_dtl_column_name;
		      -- Execute Insert SQL
		      vi_cursor_id := DBMS_SQL.open_cursor;
		      DBMS_SQL.parse (vi_cursor_id, vc_driver_ref_ins_stmt, DBMS_SQL.v7);
		      vi_rows_processed := DBMS_SQL.execute (vi_cursor_id);
		      DBMS_SQL.close_cursor (vi_cursor_id);
		END IF;

   END LOOP;
   CLOSE location_driver_cursor;

END;
/

SHOW ERRORS;


CREATE OR REPLACE PROCEDURE sp_gen_location_driver (
	p_generate_driver_reference	IN	VARCHAR2,
	p_an_batch_id			IN	NUMBER,
	p_transaction_table_name	IN	VARCHAR2,
	p_location_table_name		IN	VARCHAR2,
	p_vc_location_matrix_where	IN OUT	VARCHAR2
)
AS
	-- Define Location Driver Names Cursor (tb_driver_names)
	CURSOR location_driver_cursor
	   IS
		SELECT tb_driver_names.trans_dtl_column_name, tb_driver_names.matrix_column_name, tb_driver_names.null_driver_flag, tb_driver_names.wildcard_flag,
		       datadefcol.desc_column_name, datadefcol.data_type
		  FROM tb_driver_names,
		       (SELECT *
			  FROM tb_data_def_column
			 WHERE table_name = 'TB_TRANSACTION_DETAIL') datadefcol
		 WHERE tb_driver_names.driver_names_code = 'L' AND
		       tb_driver_names.trans_dtl_column_name = datadefcol.column_name
	      ORDER BY tb_driver_names.driver_id;

	location_driver		location_driver_cursor%ROWTYPE;

	vi_rows_processed	INTEGER		:= 0;

	vc_driver_ref_ins_stmt          VARCHAR2(5000)              := NULL;
	vi_cursor_id                    INTEGER                     := 0;
BEGIN

   -- ***** Create Dynamic WHERE Clause for Matrix SQLs ***** --
   -- Location Matrix Drivers
   OPEN location_driver_cursor;
   LOOP

                 FETCH location_driver_cursor INTO location_driver;
                  EXIT WHEN location_driver_cursor%NOTFOUND;
                  IF location_driver.null_driver_flag = '1' THEN
                     IF location_driver.wildcard_flag = '1' THEN
                        IF UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'CHAR' OR
                           UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'VARC' THEN
                              p_vc_location_matrix_where := p_vc_location_matrix_where || 'AND ' ||
                              '(('|| p_transaction_table_name ||'.' || location_driver.trans_dtl_column_name || ' IS NULL AND ('|| p_location_table_name ||'.' || location_driver.matrix_column_name || ' = ''*ALL'' OR '|| p_location_table_name ||'.' || location_driver.matrix_column_name || ' = ''*NULL'' )) OR ' ||
                              '('|| p_transaction_table_name ||'.' || location_driver.trans_dtl_column_name || ' IS NOT NULL AND ('|| p_location_table_name ||'.' || location_driver.matrix_column_name || ' = ''*ALL'' OR UPPER('|| p_transaction_table_name ||'.' || location_driver.trans_dtl_column_name || ') LIKE UPPER('|| p_location_table_name ||'.' || location_driver.matrix_column_name || ') ))) ';
                        END IF;
                     ELSE
                        IF UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'CHAR' OR
                           UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'VARC' THEN
                              p_vc_location_matrix_where := p_vc_location_matrix_where || 'AND ' ||
                              '(('|| p_transaction_table_name ||'.' || location_driver.trans_dtl_column_name || ' IS NULL AND ('|| p_location_table_name ||'.' || location_driver.matrix_column_name || ' = ''*ALL'' OR '|| p_location_table_name ||'.' || location_driver.matrix_column_name || ' = ''*NULL'' )) OR ' ||
                              '('|| p_transaction_table_name ||'.' || location_driver.trans_dtl_column_name || ' IS NOT NULL AND ('|| p_location_table_name ||'.' || location_driver.matrix_column_name || ' = ''*ALL'' OR UPPER('|| p_transaction_table_name ||'.' || location_driver.trans_dtl_column_name || ') = UPPER('|| p_location_table_name ||'.' || location_driver.matrix_column_name || ') ))) ';
                        END IF;
                     END IF;
                  ELSE
                     IF location_driver.wildcard_flag = '1' THEN
                        IF UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'CHAR' OR
                           UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'VARC' THEN
                              p_vc_location_matrix_where := p_vc_location_matrix_where || 'AND ' ||
                              '('|| p_location_table_name ||'.' || location_driver.matrix_column_name || ' = ''*ALL'' OR UPPER('|| p_transaction_table_name ||'.' || location_driver.trans_dtl_column_name || ') LIKE UPPER('|| p_location_table_name ||'.' || location_driver.matrix_column_name || ') ) ';
                        END IF;
                     ELSE
                        IF UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'CHAR' OR
                           UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'VARC' THEN
                              p_vc_location_matrix_where := p_vc_location_matrix_where || 'AND ' ||
                              '('|| p_location_table_name ||'.' || location_driver.matrix_column_name || ' = ''*ALL'' OR UPPER('|| p_transaction_table_name ||'.' || location_driver.trans_dtl_column_name || ') = UPPER('|| p_location_table_name ||'.' || location_driver.matrix_column_name || ') ) ';
                        END IF;
                     END IF;
                  END IF;

		-- Check to confirm whether we're running driver reference portion.
		-- NOTE: This is done ONLY for Batch Processing.
		IF (p_generate_driver_reference = 'Y') THEN
		      -- Insert Driver Reference Values for this driver
		      -- Create Insert SQL
		      vc_driver_ref_ins_stmt :=
			 'INSERT INTO tb_driver_reference (trans_dtl_column_name, driver_value, driver_description) ' ||
			    'SELECT ''' || Upper(location_driver.trans_dtl_column_name) ||
				    ''', ' || location_driver.trans_dtl_column_name || ', ';
		      IF location_driver.desc_column_name IS NOT NULL THEN
			 vc_driver_ref_ins_stmt := vc_driver_ref_ins_stmt || 'min(' || location_driver.desc_column_name || ') ';
		      ELSE
			 vc_driver_ref_ins_stmt := vc_driver_ref_ins_stmt || 'NULL ';
		      END IF;
		      vc_driver_ref_ins_stmt := vc_driver_ref_ins_stmt ||
			      'FROM tb_bcp_transactions ' ||
			     'WHERE process_batch_no = ' || TO_CHAR(p_an_batch_id) || ' AND ' ||
				    location_driver.trans_dtl_column_name || ' IS NOT NULL AND ' ||
				    location_driver.trans_dtl_column_name || ' NOT IN ' ||
			       '(SELECT driver_value ' ||
				  'FROM tb_driver_reference ' ||
				 'WHERE trans_dtl_column_name = ''' || Upper(location_driver.trans_dtl_column_name) || ''') ' ||
			      'GROUP BY ''' || Upper(location_driver.trans_dtl_column_name) ||
					''', ' || location_driver.trans_dtl_column_name;
		      -- Execute Insert SQL
		      vi_cursor_id := DBMS_SQL.open_cursor;
		      DBMS_SQL.parse (vi_cursor_id, vc_driver_ref_ins_stmt, DBMS_SQL.v7);
		      vi_rows_processed := DBMS_SQL.execute (vi_cursor_id);
		      DBMS_SQL.close_cursor (vi_cursor_id);
		END IF;

   END LOOP;
   CLOSE location_driver_cursor;

END;
/

SHOW ERRORS;

CREATE OR REPLACE PROCEDURE sp_gen_tax_driver (
	p_generate_driver_reference	IN	VARCHAR2,
	p_an_batch_id			IN	NUMBER,
	p_transaction_table_name	IN	VARCHAR2,
	p_tax_table_name		IN	VARCHAR2,
	p_vc_state_driver_flag		IN OUT  CHAR,
	p_vc_tax_matrix_where		IN OUT	VARCHAR2
)
AS
-- Define Tax Driver Names Cursor (tb_driver_names)
   CURSOR tax_driver_cursor
   IS
        SELECT tb_driver_names.trans_dtl_column_name, tb_driver_names.matrix_column_name, tb_driver_names.null_driver_flag, tb_driver_names.wildcard_flag, tb_driver_names.range_flag, tb_driver_names.to_number_flag,
               datadefcol.desc_column_name, datadefcol.data_type
          FROM tb_driver_names,
               (SELECT *
                  FROM tb_data_def_column
                 WHERE table_name = 'TB_TRANSACTION_DETAIL') datadefcol
         WHERE tb_driver_names.driver_names_code = 'T' AND
               tb_driver_names.trans_dtl_column_name = datadefcol.column_name
      ORDER BY tb_driver_names.driver_id;
      tax_driver                   tax_driver_cursor%ROWTYPE;

   vi_rows_processed INTEGER := 0;

   vc_driver_ref_ins_stmt          VARCHAR2(5000)              := NULL;
   vi_cursor_id                    INTEGER                     := 0;

BEGIN

   -- ***** Create Dynamic WHERE Clause for Matrix SQLs ***** --
   -- Tax Matrix Drivers
   OPEN tax_driver_cursor;
   LOOP
      FETCH tax_driver_cursor INTO tax_driver;
      EXIT WHEN tax_driver_cursor%NOTFOUND;
      IF tax_driver.range_flag = '1' THEN
         IF tax_driver.to_number_flag = '1' THEN
            IF tax_driver.null_driver_flag = '1' THEN
               -- Range and ToNumber and NULL
               IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                  UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                  p_vc_tax_matrix_where := p_vc_tax_matrix_where || 'AND ( ' ||
                  '( UPPER('|| p_tax_table_name ||'.' || tax_driver.matrix_column_name || '_THRU) = ''*NA'' AND ' ||
                     '( '|| p_transaction_table_name ||'.' || tax_driver.trans_dtl_column_name || ' IS NULL AND ( UPPER('|| p_tax_table_name ||'.' || tax_driver.matrix_column_name || ') = ''*ALL'' OR UPPER('|| p_tax_table_name ||'.' || tax_driver.matrix_column_name || ') = ''*NULL'' )) OR ' ||
                     '( '|| p_transaction_table_name ||'.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND ( UPPER('|| p_tax_table_name ||'.' || tax_driver.matrix_column_name || ') = ''*ALL'' OR DECODE(isnumber('|| p_transaction_table_name ||'.' || tax_driver.trans_dtl_column_name || '),1,to_char('|| p_transaction_table_name ||'.' || tax_driver.trans_dtl_column_name || ',''00000000000000000000.00000000000000000000''),UPPER('|| p_transaction_table_name ||'.' || tax_driver.trans_dtl_column_name || ')) = DECODE(isnumber(UPPER('|| p_tax_table_name ||'.' || tax_driver.matrix_column_name || ')),1,to_char(UPPER('|| p_tax_table_name ||'.' || tax_driver.matrix_column_name || '),''00000000000000000000.00000000000000000000''),UPPER('|| p_tax_table_name ||'.' || tax_driver.matrix_column_name || ') ))) OR ' ||
                  '( UPPER('|| p_tax_table_name ||'.' || tax_driver.matrix_column_name || '_THRU) <> ''*NA'' AND ' ||
                     '( '|| p_transaction_table_name ||'.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND (DECODE(isnumber('|| p_transaction_table_name ||'.' || tax_driver.trans_dtl_column_name || '),1,to_char('|| p_transaction_table_name ||'.' || tax_driver.trans_dtl_column_name || ',''00000000000000000000.00000000000000000000''),UPPER('|| p_transaction_table_name ||'.' || tax_driver.trans_dtl_column_name || ')) >= DECODE(isnumber(UPPER('|| p_tax_table_name ||'.' || tax_driver.matrix_column_name || ')),1,to_char(UPPER('|| p_tax_table_name ||'.' || tax_driver.matrix_column_name || '),''00000000000000000000.00000000000000000000''),UPPER('|| p_tax_table_name ||'.' || tax_driver.matrix_column_name || ') ) AND ' ||
                                                                                                       'DECODE(isnumber('|| p_transaction_table_name ||'.' || tax_driver.trans_dtl_column_name || '),1,to_char('|| p_transaction_table_name ||'.' || tax_driver.trans_dtl_column_name || ',''00000000000000000000.00000000000000000000''),UPPER('|| p_transaction_table_name ||'.' || tax_driver.trans_dtl_column_name || ')) <= DECODE(isnumber(UPPER('|| p_tax_table_name ||'.' || tax_driver.matrix_column_name || '_THRU)),1,to_char(UPPER('|| p_tax_table_name ||'.' || tax_driver.matrix_column_name || '_THRU),''00000000000000000000.00000000000000000000''),UPPER('|| p_tax_table_name ||'.' || tax_driver.matrix_column_name || '_THRU) ))))) ';
               END IF;
            ELSE
               -- Range and ToNumber and NOT NULL
               IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                  UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                  p_vc_tax_matrix_where := p_vc_tax_matrix_where || 'AND ( ' ||
                  '( UPPER('|| p_tax_table_name ||'.' || tax_driver.matrix_column_name || '_THRU) = ''*NA'' AND (UPPER('|| p_tax_table_name ||'.' || tax_driver.matrix_column_name || ') = ''*ALL'' OR DECODE(isnumber('|| p_transaction_table_name ||'.' || tax_driver.trans_dtl_column_name || '),1,to_char('|| p_transaction_table_name ||'.' || tax_driver.trans_dtl_column_name || ',''00000000000000000000.00000000000000000000''),UPPER('|| p_transaction_table_name ||'.' || tax_driver.trans_dtl_column_name || ')) = DECODE(isnumber(UPPER('|| p_tax_table_name ||'.' || tax_driver.matrix_column_name || ')),1,to_char(UPPER('|| p_tax_table_name ||'.' || tax_driver.matrix_column_name || '),''00000000000000000000.00000000000000000000''),UPPER('|| p_tax_table_name ||'.' || tax_driver.matrix_column_name || ')))) OR ' ||
                  '( UPPER('|| p_tax_table_name ||'.' || tax_driver.matrix_column_name || '_THRU) <> ''*NA'' AND (DECODE(isnumber('|| p_transaction_table_name ||'.' || tax_driver.trans_dtl_column_name || '),1,to_char('|| p_transaction_table_name ||'.' || tax_driver.trans_dtl_column_name || ',''00000000000000000000.00000000000000000000''),UPPER('|| p_transaction_table_name ||'.' || tax_driver.trans_dtl_column_name || ')) >= DECODE(isnumber(UPPER('|| p_tax_table_name ||'.' || tax_driver.matrix_column_name || ')),1,to_char(UPPER('|| p_tax_table_name ||'.' || tax_driver.matrix_column_name || '),''00000000000000000000.00000000000000000000''),UPPER('|| p_tax_table_name ||'.' || tax_driver.matrix_column_name || ')) AND ' ||
                                                                                        'DECODE(isnumber('|| p_transaction_table_name ||'.' || tax_driver.trans_dtl_column_name || '),1,to_char('|| p_transaction_table_name ||'.' || tax_driver.trans_dtl_column_name || ',''00000000000000000000.00000000000000000000''),UPPER('|| p_transaction_table_name ||'.' || tax_driver.trans_dtl_column_name || ')) <= DECODE(isnumber(UPPER('|| p_tax_table_name ||'.' || tax_driver.matrix_column_name || '_THRU )),1,to_char(UPPER('|| p_tax_table_name ||'.' || tax_driver.matrix_column_name || '_THRU),''00000000000000000000.00000000000000000000''),UPPER('|| p_tax_table_name ||'.' || tax_driver.matrix_column_name || '_THRU) ))) ';
               END IF;
            END IF;
         ELSE
            IF tax_driver.null_driver_flag = '1' THEN
               IF tax_driver.wildcard_flag = '1' THEN
                  -- Range and NOT ToNumber and NULL and Wildcard
                  IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                     UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                     p_vc_tax_matrix_where := p_vc_tax_matrix_where || 'AND ( ' ||
                     '( UPPER('|| p_tax_table_name ||'.' || tax_driver.matrix_column_name || '_THRU) = ''*NA'' AND ' ||
                        '( '|| p_transaction_table_name ||'.' || tax_driver.trans_dtl_column_name || ' IS NULL AND (UPPER('|| p_tax_table_name ||'.' || tax_driver.matrix_column_name || ') = ''*ALL'' OR UPPER('|| p_tax_table_name ||'.' || tax_driver.matrix_column_name || ') = ''*NULL'' )) OR ' ||
                        '( '|| p_transaction_table_name ||'.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND (UPPER('|| p_tax_table_name ||'.' || tax_driver.matrix_column_name || ') = ''*ALL'' OR UPPER('|| p_transaction_table_name ||'.' || tax_driver.trans_dtl_column_name || ') LIKE UPPER('|| p_tax_table_name ||'.' || tax_driver.matrix_column_name || ')))) OR ' ||
                     '( UPPER('|| p_tax_table_name ||'.' || tax_driver.matrix_column_name || '_THRU) <> ''*NA'' AND ('|| p_transaction_table_name ||'.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND (UPPER('|| p_transaction_table_name ||'.' || tax_driver.trans_dtl_column_name || ') >= UPPER('|| p_tax_table_name ||'.' || tax_driver.matrix_column_name || ') AND UPPER('|| p_transaction_table_name ||'.' || tax_driver.trans_dtl_column_name || ') <= UPPER('|| p_tax_table_name ||'.' || tax_driver.matrix_column_name || '_THRU) ))) ';
                  END IF;
               ELSE
                  -- Range and NOT ToNumber and NULL and NOT Wildcard
                  IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                     UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                     p_vc_tax_matrix_where := p_vc_tax_matrix_where || 'AND ( ' ||
                     '( UPPER('|| p_tax_table_name ||'.' || tax_driver.matrix_column_name || '_THRU) = ''*NA'' AND ' ||
                        '( '|| p_transaction_table_name ||'.' || tax_driver.trans_dtl_column_name || ' IS NULL AND (UPPER('|| p_tax_table_name ||'.' || tax_driver.matrix_column_name || ') = ''*ALL'' OR UPPER('|| p_tax_table_name ||'.' || tax_driver.matrix_column_name || ') = ''*NULL'' )) OR ' ||
                        '( '|| p_transaction_table_name ||'.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND (UPPER('|| p_tax_table_name ||'.' || tax_driver.matrix_column_name || ') = ''*ALL'' OR UPPER('|| p_transaction_table_name ||'.' || tax_driver.trans_dtl_column_name || ') = UPPER('|| p_tax_table_name ||'.' || tax_driver.matrix_column_name || ')))) OR ' ||
                     '( UPPER('|| p_tax_table_name ||'.' || tax_driver.matrix_column_name || '_THRU) <> ''*NA'' AND ('|| p_transaction_table_name ||'.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND (UPPER('|| p_transaction_table_name ||'.' || tax_driver.trans_dtl_column_name || ') >= UPPER('|| p_tax_table_name ||'.' || tax_driver.matrix_column_name || ') AND UPPER('|| p_transaction_table_name ||'.' || tax_driver.trans_dtl_column_name || ') <= UPPER('|| p_tax_table_name ||'.' || tax_driver.matrix_column_name || '_THRU) ))) ';
                  END IF;
               END IF;
            ELSE
               IF tax_driver.wildcard_flag = '1' THEN
                  -- Range and NOT ToNumber and NOT NULL and Wildcard
                  IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                     UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                     p_vc_tax_matrix_where := p_vc_tax_matrix_where || 'AND ( ' ||
                     '( UPPER('|| p_tax_table_name ||'.' || tax_driver.matrix_column_name || '_THRU) = ''*NA'' AND ' ||
                        '(UPPER('|| p_tax_table_name ||'.' || tax_driver.matrix_column_name || ') = ''*ALL'' OR UPPER('|| p_transaction_table_name ||'.' || tax_driver.trans_dtl_column_name || ') LIKE UPPER('|| p_tax_table_name ||'.' || tax_driver.matrix_column_name || '))) OR ' ||
                     '( UPPER('|| p_tax_table_name ||'.' || tax_driver.matrix_column_name || '_THRU) <> ''*NA'' AND (UPPER('|| p_transaction_table_name ||'.' || tax_driver.trans_dtl_column_name || ') >= UPPER('|| p_tax_table_name ||'.' || tax_driver.matrix_column_name || ') AND UPPER('|| p_transaction_table_name ||'.' || tax_driver.trans_dtl_column_name || ') <= UPPER('|| p_tax_table_name ||'.' || tax_driver.matrix_column_name || '_THRU) )) ';
                  END IF;
               ELSE
                  -- Range and NOT ToNumber and NOT NULL and NOT Wildcard
                  IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                     UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                     p_vc_tax_matrix_where := p_vc_tax_matrix_where || 'AND ( ' ||
                     '( UPPER('|| p_tax_table_name ||'.' || tax_driver.matrix_column_name || '_THRU) = ''*NA'' AND ' ||
                        '(UPPER('|| p_tax_table_name ||'.' || tax_driver.matrix_column_name || ') = ''*ALL'' OR UPPER('|| p_transaction_table_name ||'.' || tax_driver.trans_dtl_column_name || ') = UPPER('|| p_tax_table_name ||'.' || tax_driver.matrix_column_name || '))) OR ' ||
                     '( UPPER('|| p_tax_table_name ||'.' || tax_driver.matrix_column_name || '_THRU) <> ''*NA'' AND (UPPER('|| p_transaction_table_name ||'.' || tax_driver.trans_dtl_column_name || ') >= UPPER('|| p_tax_table_name ||'.' || tax_driver.matrix_column_name || ') AND UPPER('|| p_transaction_table_name ||'.' || tax_driver.trans_dtl_column_name || ') <= UPPER('|| p_tax_table_name ||'.' || tax_driver.matrix_column_name || '_THRU) )) ';
                  END IF;
               END IF;
            END IF;
         END IF;
      ELSE
         IF tax_driver.null_driver_flag = '1' THEN
            IF tax_driver.wildcard_flag = '1' THEN
               -- NOT Range and NULL and Wildcard
               IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                  UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                     p_vc_tax_matrix_where := p_vc_tax_matrix_where || 'AND ( ' ||
                     '(( '|| p_transaction_table_name ||'.' || tax_driver.trans_dtl_column_name || ' IS NULL AND (UPPER('|| p_tax_table_name ||'.' || tax_driver.matrix_column_name || ') = ''*ALL'' OR UPPER('|| p_tax_table_name ||'.' || tax_driver.matrix_column_name || ') = ''*NULL'' )) OR ' ||
                     '( '|| p_transaction_table_name ||'.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND (UPPER('|| p_tax_table_name ||'.' || tax_driver.matrix_column_name || ') = ''*ALL'' OR UPPER('|| p_transaction_table_name ||'.' || tax_driver.trans_dtl_column_name || ') LIKE UPPER('|| p_tax_table_name ||'.' || tax_driver.matrix_column_name || ')))) ';
               END IF;
            ELSE
               -- NOT Range and NULL and NOT Wildcard
               IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                  UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                     p_vc_tax_matrix_where := p_vc_tax_matrix_where || 'AND ( ' ||
                     '(( '|| p_transaction_table_name ||'.' || tax_driver.trans_dtl_column_name || ' IS NULL AND (UPPER('|| p_tax_table_name ||'.' || tax_driver.matrix_column_name || ') = ''*ALL'' OR UPPER('|| p_tax_table_name ||'.' || tax_driver.matrix_column_name || ') = ''*NULL'' )) OR ' ||
                     '( '|| p_transaction_table_name ||'.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND (UPPER('|| p_tax_table_name ||'.' || tax_driver.matrix_column_name || ') = ''*ALL'' OR UPPER('|| p_transaction_table_name ||'.' || tax_driver.trans_dtl_column_name || ') = UPPER('|| p_tax_table_name ||'.' || tax_driver.matrix_column_name || ')))) ';
               END IF;
            END IF;
         ELSE
            IF tax_driver.wildcard_flag = '1' THEN
               -- NOT Range and NOT NULL and Wildcard
               IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                  UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                     p_vc_tax_matrix_where := p_vc_tax_matrix_where || 'AND ( ' ||
                     '( UPPER('|| p_tax_table_name ||'.' || tax_driver.matrix_column_name || ') = ''*ALL'' OR UPPER('|| p_transaction_table_name ||'.' || tax_driver.trans_dtl_column_name || ') LIKE UPPER('|| p_tax_table_name ||'.' || tax_driver.matrix_column_name || ')) ';
               END IF;
            ELSE
               -- NOT Range and NOT NULL and NOT Wildcard
               IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                  UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                  IF tax_driver.trans_dtl_column_name = 'TRANSACTION_STATE_CODE' THEN
                     IF p_vc_state_driver_flag <> '1' THEN
                        p_vc_state_driver_flag := '1';
                     END IF;
                     p_vc_tax_matrix_where := p_vc_tax_matrix_where || 'AND ( ' ||
                     '( UPPER('|| p_tax_table_name ||'.' || tax_driver.matrix_column_name || ') = ''*ALL'' OR UPPER(:v_transaction_state_code) = UPPER('|| p_tax_table_name ||'.' || tax_driver.matrix_column_name || ')) ';
                  ELSE
                     p_vc_tax_matrix_where := p_vc_tax_matrix_where || 'AND ( ' ||
                     '( UPPER('|| p_tax_table_name ||'.' || tax_driver.matrix_column_name || ') = ''*ALL'' OR UPPER('|| p_transaction_table_name ||'.' || tax_driver.trans_dtl_column_name || ') = UPPER('|| p_tax_table_name ||'.' || tax_driver.matrix_column_name || ')) ';
                  END IF;
               END IF;
            END IF;
         END IF;
      END IF;
      p_vc_tax_matrix_where := p_vc_tax_matrix_where || ') ';

      -- Check to confirm whether we're running driver reference portion.
      -- NOTE: This is done ONLY for Batch Processing.
      IF (p_generate_driver_reference = 'Y') THEN

	      -- Insert Driver Reference Values for this driver
	      -- Create Insert SQL
	      vc_driver_ref_ins_stmt :=
		 'INSERT INTO tb_driver_reference (trans_dtl_column_name, driver_value, driver_description) ' ||
		    'SELECT ''' || Upper(tax_driver.trans_dtl_column_name) ||
			    ''', ' || tax_driver.trans_dtl_column_name || ', ';
	      IF tax_driver.desc_column_name IS NOT NULL THEN
		 vc_driver_ref_ins_stmt := vc_driver_ref_ins_stmt || 'min(' || tax_driver.desc_column_name || ') ';
	      ELSE
		 vc_driver_ref_ins_stmt := vc_driver_ref_ins_stmt || 'NULL ';
	      END IF;
	      vc_driver_ref_ins_stmt := vc_driver_ref_ins_stmt ||
		      'FROM tb_bcp_transactions ' ||
		     'WHERE process_batch_no = ' || TO_CHAR(p_an_batch_id) || ' AND ' ||
			    tax_driver.trans_dtl_column_name || ' IS NOT NULL AND ' ||
			    tax_driver.trans_dtl_column_name || ' NOT IN ' ||
		       '(SELECT driver_value ' ||
			  'FROM tb_driver_reference ' ||
			 'WHERE trans_dtl_column_name = ''' || Upper(tax_driver.trans_dtl_column_name) || ''') ' ||
		      'GROUP BY ''' || Upper(tax_driver.trans_dtl_column_name) ||
				''', ' || tax_driver.trans_dtl_column_name;
	      -- Execute Insert SQL
	      vi_cursor_id := DBMS_SQL.open_cursor;
	      DBMS_SQL.parse (vi_cursor_id, vc_driver_ref_ins_stmt, DBMS_SQL.v7);
	      vi_rows_processed := DBMS_SQL.execute (vi_cursor_id);
	      DBMS_SQL.close_cursor (vi_cursor_id);
      END IF;

   END LOOP;
   CLOSE tax_driver_cursor;

END;
/

SHOW ERRORS;
CREATE OR REPLACE PROCEDURE sp_gl_logging (
	p_old_tb_transaction_detail IN     tb_transaction_detail%ROWTYPE,
	p_new_tb_transaction_detail IN OUT tb_transaction_detail%ROWTYPE,
	p_v_sysdate                 IN     tb_transaction_detail.load_timestamp%TYPE,
	p_v_sysdate_plus            IN     tb_transaction_detail.load_timestamp%TYPE
)
AS
BEGIN

   -- ******  GL logging module  ****** ------------------------------------------------------------
   -- IF the new TaxCode Detail ID, Jurisdiction ID, or Jurisdiction TaxRate ID is different then the old one
   IF ( ( UPPER(p_old_tb_transaction_detail.transaction_ind) = 'P' ) AND
        ( ( p_old_tb_transaction_detail.taxcode_detail_id <> p_new_tb_transaction_detail.taxcode_detail_id OR
            p_old_tb_transaction_detail.taxcode_detail_id IS NOT NULL AND p_new_tb_transaction_detail.taxcode_detail_id IS NULL ) OR
          ( p_old_tb_transaction_detail.jurisdiction_id <> p_new_tb_transaction_detail.jurisdiction_id OR
            p_old_tb_transaction_detail.jurisdiction_id IS NOT NULL AND p_new_tb_transaction_detail.jurisdiction_id IS NULL ) OR
          ( p_old_tb_transaction_detail.jurisdiction_taxrate_id <> p_new_tb_transaction_detail.jurisdiction_taxrate_id OR
            p_old_tb_transaction_detail.jurisdiction_taxrate_id IS NOT NULL AND p_new_tb_transaction_detail.jurisdiction_taxrate_id IS NULL ) ) ) OR
      ( UPPER(p_old_tb_transaction_detail.transaction_ind) = 'S' AND UPPER(p_new_tb_transaction_detail.transaction_ind) = 'P' ) THEN
      -- If this transaction has not been currently marked by GL Extract
      --(It could be marked before but then having the taxcode changed by users)
      IF p_old_tb_transaction_detail.gl_extract_flag IS NULL THEN
         -- The GL_LOG_FLAG indicates that this transaction has been flagged for GL Logging.  It means that the previously marked
         -- transaction had its taxcode modified by users.  In this case, log the before and after image to TB_GL_EXPORT_LOG table
         IF  p_old_tb_transaction_detail.gl_log_flag IS NOT NULL THEN
            -- 1. Create a before image for this transaction into TO_GL_EXPORT_LOG table with the negative tax amount.
            --    This is to offset the previous tax amount that had been sent into ERP system.
            INSERT INTO tb_gl_export_log (
               transaction_detail_id,
               source_transaction_id,
               process_batch_no,
               gl_extract_batch_no,
               archive_batch_no,
               allocation_matrix_id,
               allocation_subtrans_id,
               entered_date,
               transaction_status,
               gl_date,
               gl_company_nbr,
               gl_company_name,
               gl_division_nbr,
               gl_division_name,
               gl_cc_nbr_dept_id,
               gl_cc_nbr_dept_name,
               gl_local_acct_nbr,
               gl_local_acct_name,
               gl_local_sub_acct_nbr,
               gl_local_sub_acct_name,
               gl_full_acct_nbr,
               gl_full_acct_name,
               gl_line_itm_dist_amt,
               orig_gl_line_itm_dist_amt,
               vendor_nbr,
               vendor_name,
               vendor_address_line_1,
               vendor_address_line_2,
               vendor_address_line_3,
               vendor_address_line_4,
               vendor_address_city,
               vendor_address_county,
               vendor_address_state,
               vendor_address_zip,
               vendor_address_country,
               vendor_type,
               vendor_type_name,
               invoice_nbr,
               invoice_desc,
               invoice_date,
               invoice_freight_amt,
               invoice_discount_amt,
               invoice_tax_amt,
               invoice_total_amt,
               invoice_tax_flg,
               invoice_line_nbr,
               invoice_line_name,
               invoice_line_type,
               invoice_line_type_name,
               invoice_line_amt,
               invoice_line_tax,
               afe_project_nbr,
               afe_project_name,
               afe_category_nbr,
               afe_category_name,
               afe_sub_cat_nbr,
               afe_sub_cat_name,
               afe_use,
               afe_contract_type,
               afe_contract_structure,
               afe_property_cat,
               inventory_nbr,
               inventory_name,
               inventory_class,
               inventory_class_name,
               po_nbr,
               po_name,
               po_date,
               po_line_nbr,
               po_line_name,
               po_line_type,
               po_line_type_name,
               ship_to_location,
               ship_to_location_name,
               ship_to_address_line_1,
               ship_to_address_line_2,
               ship_to_address_line_3,
               ship_to_address_line_4,
               ship_to_address_city,
               ship_to_address_county,
               ship_to_address_state,
               ship_to_address_zip,
               ship_to_address_country,
               wo_nbr,
               wo_name,
               wo_date,
               wo_type,
               wo_type_desc,
               wo_class,
               wo_class_desc,
               wo_entity,
               wo_entity_desc,
               wo_line_nbr,
               wo_line_name,
               wo_line_type,
               wo_line_type_desc,
               wo_shut_down_cd,
               wo_shut_down_cd_desc,
               voucher_id,
               voucher_name,
               voucher_date,
               voucher_line_nbr,
               voucher_line_desc,
               check_nbr,
               check_no,
               check_date,
               check_amt,
               check_desc,
               user_text_01,
               user_text_02,
               user_text_03,
               user_text_04,
               user_text_05,
               user_text_06,
               user_text_07,
               user_text_08,
               user_text_09,
               user_text_10,
               user_text_11,
               user_text_12,
               user_text_13,
               user_text_14,
               user_text_15,
               user_text_16,
               user_text_17,
               user_text_18,
               user_text_19,
               user_text_20,
               user_text_21,
               user_text_22,
               user_text_23,
               user_text_24,
               user_text_25,
               user_text_26,
               user_text_27,
               user_text_28,
               user_text_29,
               user_text_30,
               user_number_01,
               user_number_02,
               user_number_03,
               user_number_04,
               user_number_05,
               user_number_06,
               user_number_07,
               user_number_08,
               user_number_09,
               user_number_10,
               user_date_01,
               user_date_02,
               user_date_03,
               user_date_04,
               user_date_05,
               user_date_06,
               user_date_07,
               user_date_08,
               user_date_09,
               user_date_10,
               comments,
               tb_calc_tax_amt,
               state_use_amount,
               state_use_tier2_amount,
               state_use_tier3_amount,
               county_use_amount,
               county_local_use_amount,
               city_use_amount,
               city_local_use_amount,
               transaction_state_code,
               transaction_ind,
               suspend_ind,
               taxcode_detail_id,
               taxcode_state_code,
               taxcode_type_code,
               taxcode_code,
               cch_taxcat_code,
               cch_group_code,
               cch_item_code,
               manual_taxcode_ind,
               tax_matrix_id,
               location_matrix_id,
               jurisdiction_id,
               jurisdiction_taxrate_id,
               manual_jurisdiction_ind,
               measure_type_code,
               state_use_rate,
               state_use_tier2_rate,
               state_use_tier3_rate,
               state_split_amount,
               state_tier2_min_amount,
               state_tier2_max_amount,
               state_maxtax_amount,
               county_use_rate,
               county_local_use_rate,
               county_split_amount,
               county_maxtax_amount,
               county_single_flag,
               county_default_flag,
               city_use_rate,
               city_local_use_rate,
               city_split_amount,
               city_split_use_rate,
               city_single_flag,
               city_default_flag,
               combined_use_rate,
               load_timestamp,
               gl_extract_flag,
               gl_extract_amt,
               audit_flag,
               audit_user_id,
               audit_timestamp,
               modify_user_id,
               modify_timestamp,
               update_user_id,
               update_timestamp )
            VALUES (
               p_old_tb_transaction_detail.transaction_detail_id,
               p_old_tb_transaction_detail.source_transaction_id,
               p_old_tb_transaction_detail.process_batch_no,
               p_old_tb_transaction_detail.gl_extract_batch_no,
               p_old_tb_transaction_detail.archive_batch_no,
               p_old_tb_transaction_detail.allocation_matrix_id,
               p_old_tb_transaction_detail.allocation_subtrans_id,
               p_old_tb_transaction_detail.entered_date,
               p_old_tb_transaction_detail.transaction_status,
               p_old_tb_transaction_detail.gl_date,
               p_old_tb_transaction_detail.gl_company_nbr,
               p_old_tb_transaction_detail.gl_company_name,
               p_old_tb_transaction_detail.gl_division_nbr,
               p_old_tb_transaction_detail.gl_division_name,
               p_old_tb_transaction_detail.gl_cc_nbr_dept_id,
               p_old_tb_transaction_detail.gl_cc_nbr_dept_name,
               p_old_tb_transaction_detail.gl_local_acct_nbr,
               p_old_tb_transaction_detail.gl_local_acct_name,
               p_old_tb_transaction_detail.gl_local_sub_acct_nbr,
               p_old_tb_transaction_detail.gl_local_sub_acct_name,
               p_old_tb_transaction_detail.gl_full_acct_nbr,
               p_old_tb_transaction_detail.gl_full_acct_name,
               p_old_tb_transaction_detail.gl_line_itm_dist_amt,
               p_old_tb_transaction_detail.orig_gl_line_itm_dist_amt,
               p_old_tb_transaction_detail.vendor_nbr,
               p_old_tb_transaction_detail.vendor_name,
               p_old_tb_transaction_detail.vendor_address_line_1,
               p_old_tb_transaction_detail.vendor_address_line_2,
               p_old_tb_transaction_detail.vendor_address_line_3,
               p_old_tb_transaction_detail.vendor_address_line_4,
               p_old_tb_transaction_detail.vendor_address_city,
               p_old_tb_transaction_detail.vendor_address_county,
               p_old_tb_transaction_detail.vendor_address_state,
               p_old_tb_transaction_detail.vendor_address_zip,
               p_old_tb_transaction_detail.vendor_address_country,
               p_old_tb_transaction_detail.vendor_type,
               p_old_tb_transaction_detail.vendor_type_name,
               p_old_tb_transaction_detail.invoice_nbr,
               p_old_tb_transaction_detail.invoice_desc,
               p_old_tb_transaction_detail.invoice_date,
               p_old_tb_transaction_detail.invoice_freight_amt,
               p_old_tb_transaction_detail.invoice_discount_amt,
               p_old_tb_transaction_detail.invoice_tax_amt,
               p_old_tb_transaction_detail.invoice_total_amt,
               p_old_tb_transaction_detail.invoice_tax_flg,
               p_old_tb_transaction_detail.invoice_line_nbr,
               p_old_tb_transaction_detail.invoice_line_name,
               p_old_tb_transaction_detail.invoice_line_type,
               p_old_tb_transaction_detail.invoice_line_type_name,
               p_old_tb_transaction_detail.invoice_line_amt,
               p_old_tb_transaction_detail.invoice_line_tax,
               p_old_tb_transaction_detail.afe_project_nbr,
               p_old_tb_transaction_detail.afe_project_name,
               p_old_tb_transaction_detail.afe_category_nbr,
               p_old_tb_transaction_detail.afe_category_name,
               p_old_tb_transaction_detail.afe_sub_cat_nbr,
               p_old_tb_transaction_detail.afe_sub_cat_name,
               p_old_tb_transaction_detail.afe_use,
               p_old_tb_transaction_detail.afe_contract_type,
               p_old_tb_transaction_detail.afe_contract_structure,
               p_old_tb_transaction_detail.afe_property_cat,
               p_old_tb_transaction_detail.inventory_nbr,
               p_old_tb_transaction_detail.inventory_name,
               p_old_tb_transaction_detail.inventory_class,
               p_old_tb_transaction_detail.inventory_class_name,
               p_old_tb_transaction_detail.po_nbr,
               p_old_tb_transaction_detail.po_name,
               p_old_tb_transaction_detail.po_date,
               p_old_tb_transaction_detail.po_line_nbr,
               p_old_tb_transaction_detail.po_line_name,
               p_old_tb_transaction_detail.po_line_type,
               p_old_tb_transaction_detail.po_line_type_name,
               p_old_tb_transaction_detail.ship_to_location,
               p_old_tb_transaction_detail.ship_to_location_name,
               p_old_tb_transaction_detail.ship_to_address_line_1,
               p_old_tb_transaction_detail.ship_to_address_line_2,
               p_old_tb_transaction_detail.ship_to_address_line_3,
               p_old_tb_transaction_detail.ship_to_address_line_4,
               p_old_tb_transaction_detail.ship_to_address_city,
               p_old_tb_transaction_detail.ship_to_address_county,
               p_old_tb_transaction_detail.ship_to_address_state,
               p_old_tb_transaction_detail.ship_to_address_zip,
               p_old_tb_transaction_detail.ship_to_address_country,
               p_old_tb_transaction_detail.wo_nbr,
               p_old_tb_transaction_detail.wo_name,
               p_old_tb_transaction_detail.wo_date,
               p_old_tb_transaction_detail.wo_type,
               p_old_tb_transaction_detail.wo_type_desc,
               p_old_tb_transaction_detail.wo_class,
               p_old_tb_transaction_detail.wo_class_desc,
               p_old_tb_transaction_detail.wo_entity,
               p_old_tb_transaction_detail.wo_entity_desc,
               p_old_tb_transaction_detail.wo_line_nbr,
               p_old_tb_transaction_detail.wo_line_name,
               p_old_tb_transaction_detail.wo_line_type,
               p_old_tb_transaction_detail.wo_line_type_desc,
               p_old_tb_transaction_detail.wo_shut_down_cd,
               p_old_tb_transaction_detail.wo_shut_down_cd_desc,
               p_old_tb_transaction_detail.voucher_id,
               p_old_tb_transaction_detail.voucher_name,
               p_old_tb_transaction_detail.voucher_date,
               p_old_tb_transaction_detail.voucher_line_nbr,
               p_old_tb_transaction_detail.voucher_line_desc,
               p_old_tb_transaction_detail.check_nbr,
               p_old_tb_transaction_detail.check_no,
               p_old_tb_transaction_detail.check_date,
               p_old_tb_transaction_detail.check_amt,
               p_old_tb_transaction_detail.check_desc,
               p_old_tb_transaction_detail.user_text_01,
               p_old_tb_transaction_detail.user_text_02,
               p_old_tb_transaction_detail.user_text_03,
               p_old_tb_transaction_detail.user_text_04,
               p_old_tb_transaction_detail.user_text_05,
               p_old_tb_transaction_detail.user_text_06,
               p_old_tb_transaction_detail.user_text_07,
               p_old_tb_transaction_detail.user_text_08,
               p_old_tb_transaction_detail.user_text_09,
               p_old_tb_transaction_detail.user_text_10,
               p_old_tb_transaction_detail.user_text_11,
               p_old_tb_transaction_detail.user_text_12,
               p_old_tb_transaction_detail.user_text_13,
               p_old_tb_transaction_detail.user_text_14,
               p_old_tb_transaction_detail.user_text_15,
               p_old_tb_transaction_detail.user_text_16,
               p_old_tb_transaction_detail.user_text_17,
               p_old_tb_transaction_detail.user_text_18,
               p_old_tb_transaction_detail.user_text_19,
               p_old_tb_transaction_detail.user_text_20,
               p_old_tb_transaction_detail.user_text_21,
               p_old_tb_transaction_detail.user_text_22,
               p_old_tb_transaction_detail.user_text_23,
               p_old_tb_transaction_detail.user_text_24,
               p_old_tb_transaction_detail.user_text_25,
               p_old_tb_transaction_detail.user_text_26,
               p_old_tb_transaction_detail.user_text_27,
               p_old_tb_transaction_detail.user_text_28,
               p_old_tb_transaction_detail.user_text_29,
               p_old_tb_transaction_detail.user_text_30,
               p_old_tb_transaction_detail.user_number_01,
               p_old_tb_transaction_detail.user_number_02,
               p_old_tb_transaction_detail.user_number_03,
               p_old_tb_transaction_detail.user_number_04,
               p_old_tb_transaction_detail.user_number_05,
               p_old_tb_transaction_detail.user_number_06,
               p_old_tb_transaction_detail.user_number_07,
               p_old_tb_transaction_detail.user_number_08,
               p_old_tb_transaction_detail.user_number_09,
               p_old_tb_transaction_detail.user_number_10,
               p_old_tb_transaction_detail.user_date_01,
               p_old_tb_transaction_detail.user_date_02,
               p_old_tb_transaction_detail.user_date_03,
               p_old_tb_transaction_detail.user_date_04,
               p_old_tb_transaction_detail.user_date_05,
               p_old_tb_transaction_detail.user_date_06,
               p_old_tb_transaction_detail.user_date_07,
               p_old_tb_transaction_detail.user_date_08,
               p_old_tb_transaction_detail.user_date_09,
               p_old_tb_transaction_detail.user_date_10,
               p_old_tb_transaction_detail.comments,
               p_old_tb_transaction_detail.tb_calc_tax_amt * -1,
               p_old_tb_transaction_detail.state_use_amount * -1,
               p_old_tb_transaction_detail.state_use_tier2_amount * -1,
               p_old_tb_transaction_detail.state_use_tier3_amount * -1,
               p_old_tb_transaction_detail.county_use_amount * -1,
               p_old_tb_transaction_detail.county_local_use_amount * -1,
               p_old_tb_transaction_detail.city_use_amount * -1,
               p_old_tb_transaction_detail.city_local_use_amount * -1,
               p_old_tb_transaction_detail.transaction_state_code,
               p_old_tb_transaction_detail.transaction_ind,
               p_old_tb_transaction_detail.suspend_ind,
               p_old_tb_transaction_detail.taxcode_detail_id,
               p_old_tb_transaction_detail.taxcode_state_code,
               p_old_tb_transaction_detail.taxcode_type_code,
               p_old_tb_transaction_detail.taxcode_code,
               p_old_tb_transaction_detail.cch_taxcat_code,
               p_old_tb_transaction_detail.cch_group_code,
               p_old_tb_transaction_detail.cch_item_code,
               p_old_tb_transaction_detail.manual_taxcode_ind,
               p_old_tb_transaction_detail.tax_matrix_id,
               p_old_tb_transaction_detail.location_matrix_id,
               p_old_tb_transaction_detail.jurisdiction_id,
               p_old_tb_transaction_detail.jurisdiction_taxrate_id,
               p_old_tb_transaction_detail.manual_jurisdiction_ind,
               p_old_tb_transaction_detail.measure_type_code,
               p_old_tb_transaction_detail.state_use_rate,
               p_old_tb_transaction_detail.state_use_tier2_rate,
               p_old_tb_transaction_detail.state_use_tier3_rate,
               p_old_tb_transaction_detail.state_split_amount,
               p_old_tb_transaction_detail.state_tier2_min_amount,
               p_old_tb_transaction_detail.state_tier2_max_amount,
               p_old_tb_transaction_detail.state_maxtax_amount,
               p_old_tb_transaction_detail.county_use_rate,
               p_old_tb_transaction_detail.county_local_use_rate,
               p_old_tb_transaction_detail.county_split_amount,
               p_old_tb_transaction_detail.county_maxtax_amount,
               p_old_tb_transaction_detail.county_single_flag,
               p_old_tb_transaction_detail.county_default_flag,
               p_old_tb_transaction_detail.city_use_rate,
               p_old_tb_transaction_detail.city_local_use_rate,
               p_old_tb_transaction_detail.city_split_amount,
               p_old_tb_transaction_detail.city_split_use_rate,
               p_old_tb_transaction_detail.city_single_flag,
               p_old_tb_transaction_detail.city_default_flag,
               p_old_tb_transaction_detail.combined_use_rate,
               p_old_tb_transaction_detail.load_timestamp,
               p_old_tb_transaction_detail.gl_extract_flag,
               p_old_tb_transaction_detail.gl_extract_amt,
               p_old_tb_transaction_detail.audit_flag,
               p_old_tb_transaction_detail.audit_user_id,
               p_old_tb_transaction_detail.audit_timestamp,
               p_old_tb_transaction_detail.modify_user_id,
               p_old_tb_transaction_detail.modify_timestamp,
               p_old_tb_transaction_detail.update_user_id,
               p_v_sysdate );

            -- 2. Create an after image for this transaction into TO_GL_EXPORT_LOG table with the new tax amount.
            --    This if for tracking the newest tax amount.
            INSERT INTO tb_gl_export_log (
               transaction_detail_id,
               source_transaction_id,
               process_batch_no,
               gl_extract_batch_no,
               archive_batch_no,
               allocation_matrix_id,
               allocation_subtrans_id,
               entered_date,
               transaction_status,
               gl_date,
               gl_company_nbr,
               gl_company_name,
               gl_division_nbr,
               gl_division_name,
               gl_cc_nbr_dept_id,
               gl_cc_nbr_dept_name,
               gl_local_acct_nbr,
               gl_local_acct_name,
               gl_local_sub_acct_nbr,
               gl_local_sub_acct_name,
               gl_full_acct_nbr,
               gl_full_acct_name,
               gl_line_itm_dist_amt,
               orig_gl_line_itm_dist_amt,
               vendor_nbr,
               vendor_name,
               vendor_address_line_1,
               vendor_address_line_2,
               vendor_address_line_3,
               vendor_address_line_4,
               vendor_address_city,
               vendor_address_county,
               vendor_address_state,
               vendor_address_zip,
               vendor_address_country,
               vendor_type,
               vendor_type_name,
               invoice_nbr,
               invoice_desc,
               invoice_date,
               invoice_freight_amt,
               invoice_discount_amt,
               invoice_tax_amt,
               invoice_total_amt,
               invoice_tax_flg,
               invoice_line_nbr,
               invoice_line_name,
               invoice_line_type,
               invoice_line_type_name,
               invoice_line_amt,
               invoice_line_tax,
               afe_project_nbr,
               afe_project_name,
               afe_category_nbr,
               afe_category_name,
               afe_sub_cat_nbr,
               afe_sub_cat_name,
               afe_use,
               afe_contract_type,
               afe_contract_structure,
               afe_property_cat,
               inventory_nbr,
               inventory_name,
               inventory_class,
               inventory_class_name,
               po_nbr,
               po_name,
               po_date,
               po_line_nbr,
               po_line_name,
               po_line_type,
               po_line_type_name,
               ship_to_location,
               ship_to_location_name,
               ship_to_address_line_1,
               ship_to_address_line_2,
               ship_to_address_line_3,
               ship_to_address_line_4,
               ship_to_address_city,
               ship_to_address_county,
               ship_to_address_state,
               ship_to_address_zip,
               ship_to_address_country,
               wo_nbr,
               wo_name,
               wo_date,
               wo_type,
               wo_type_desc,
               wo_class,
               wo_class_desc,
               wo_entity,
               wo_entity_desc,
               wo_line_nbr,
               wo_line_name,
               wo_line_type,
               wo_line_type_desc,
               wo_shut_down_cd,
               wo_shut_down_cd_desc,
               voucher_id,
               voucher_name,
               voucher_date,
               voucher_line_nbr,
               voucher_line_desc,
               check_nbr,
               check_no,
               check_date,
               check_amt,
               check_desc,
               user_text_01,
               user_text_02,
               user_text_03,
               user_text_04,
               user_text_05,
               user_text_06,
               user_text_07,
               user_text_08,
               user_text_09,
               user_text_10,
               user_text_11,
               user_text_12,
               user_text_13,
               user_text_14,
               user_text_15,
               user_text_16,
               user_text_17,
               user_text_18,
               user_text_19,
               user_text_20,
               user_text_21,
               user_text_22,
               user_text_23,
               user_text_24,
               user_text_25,
               user_text_26,
               user_text_27,
               user_text_28,
               user_text_29,
               user_text_30,
               user_number_01,
               user_number_02,
               user_number_03,
               user_number_04,
               user_number_05,
               user_number_06,
               user_number_07,
               user_number_08,
               user_number_09,
               user_number_10,
               user_date_01,
               user_date_02,
               user_date_03,
               user_date_04,
               user_date_05,
               user_date_06,
               user_date_07,
               user_date_08,
               user_date_09,
               user_date_10,
               comments,
               tb_calc_tax_amt,
               state_use_amount,
               state_use_tier2_amount,
               state_use_tier3_amount,
               county_use_amount,
               county_local_use_amount,
               city_use_amount,
               city_local_use_amount,
               transaction_state_code,
               transaction_ind,
               suspend_ind,
               taxcode_detail_id,
               taxcode_state_code,
               taxcode_type_code,
               taxcode_code,
               cch_taxcat_code,
               cch_group_code,
               cch_item_code,
               manual_taxcode_ind,
               tax_matrix_id,
               location_matrix_id,
               jurisdiction_id,
               jurisdiction_taxrate_id,
               manual_jurisdiction_ind,
               measure_type_code,
               state_use_rate,
               state_use_tier2_rate,
               state_use_tier3_rate,
               state_split_amount,
               state_tier2_min_amount,
               state_tier2_max_amount,
               state_maxtax_amount,
               county_use_rate,
               county_local_use_rate,
               county_split_amount,
               county_maxtax_amount,
               county_single_flag,
               county_default_flag,
               city_use_rate,
               city_local_use_rate,
               city_split_amount,
               city_split_use_rate,
               city_single_flag,
               city_default_flag,
               combined_use_rate,
               load_timestamp,
               gl_extract_flag,
               gl_extract_amt,
               audit_flag,
               audit_user_id,
               audit_timestamp,
               modify_user_id,
               modify_timestamp,
               update_user_id,
               update_timestamp )
            VALUES (
               p_new_tb_transaction_detail.transaction_detail_id,
               p_new_tb_transaction_detail.source_transaction_id,
               p_new_tb_transaction_detail.process_batch_no,
               p_new_tb_transaction_detail.gl_extract_batch_no,
               p_new_tb_transaction_detail.archive_batch_no,
               p_new_tb_transaction_detail.allocation_matrix_id,
               p_new_tb_transaction_detail.allocation_subtrans_id,
               p_new_tb_transaction_detail.entered_date,
               p_new_tb_transaction_detail.transaction_status,
               p_new_tb_transaction_detail.gl_date,
               p_new_tb_transaction_detail.gl_company_nbr,
               p_new_tb_transaction_detail.gl_company_name,
               p_new_tb_transaction_detail.gl_division_nbr,
               p_new_tb_transaction_detail.gl_division_name,
               p_new_tb_transaction_detail.gl_cc_nbr_dept_id,
               p_new_tb_transaction_detail.gl_cc_nbr_dept_name,
               p_new_tb_transaction_detail.gl_local_acct_nbr,
               p_new_tb_transaction_detail.gl_local_acct_name,
               p_new_tb_transaction_detail.gl_local_sub_acct_nbr,
               p_new_tb_transaction_detail.gl_local_sub_acct_name,
               p_new_tb_transaction_detail.gl_full_acct_nbr,
               p_new_tb_transaction_detail.gl_full_acct_name,
               p_new_tb_transaction_detail.gl_line_itm_dist_amt,
               p_new_tb_transaction_detail.orig_gl_line_itm_dist_amt,
               p_new_tb_transaction_detail.vendor_nbr,
               p_new_tb_transaction_detail.vendor_name,
               p_new_tb_transaction_detail.vendor_address_line_1,
               p_new_tb_transaction_detail.vendor_address_line_2,
               p_new_tb_transaction_detail.vendor_address_line_3,
               p_new_tb_transaction_detail.vendor_address_line_4,
               p_new_tb_transaction_detail.vendor_address_city,
               p_new_tb_transaction_detail.vendor_address_county,
               p_new_tb_transaction_detail.vendor_address_state,
               p_new_tb_transaction_detail.vendor_address_zip,
               p_new_tb_transaction_detail.vendor_address_country,
               p_new_tb_transaction_detail.vendor_type,
               p_new_tb_transaction_detail.vendor_type_name,
               p_new_tb_transaction_detail.invoice_nbr,
               p_new_tb_transaction_detail.invoice_desc,
               p_new_tb_transaction_detail.invoice_date,
               p_new_tb_transaction_detail.invoice_freight_amt,
               p_new_tb_transaction_detail.invoice_discount_amt,
               p_new_tb_transaction_detail.invoice_tax_amt,
               p_new_tb_transaction_detail.invoice_total_amt,
               p_new_tb_transaction_detail.invoice_tax_flg,
               p_new_tb_transaction_detail.invoice_line_nbr,
               p_new_tb_transaction_detail.invoice_line_name,
               p_new_tb_transaction_detail.invoice_line_type,
               p_new_tb_transaction_detail.invoice_line_type_name,
               p_new_tb_transaction_detail.invoice_line_amt,
               p_new_tb_transaction_detail.invoice_line_tax,
               p_new_tb_transaction_detail.afe_project_nbr,
               p_new_tb_transaction_detail.afe_project_name,
               p_new_tb_transaction_detail.afe_category_nbr,
               p_new_tb_transaction_detail.afe_category_name,
               p_new_tb_transaction_detail.afe_sub_cat_nbr,
               p_new_tb_transaction_detail.afe_sub_cat_name,
               p_new_tb_transaction_detail.afe_use,
               p_new_tb_transaction_detail.afe_contract_type,
               p_new_tb_transaction_detail.afe_contract_structure,
               p_new_tb_transaction_detail.afe_property_cat,
               p_new_tb_transaction_detail.inventory_nbr,
               p_new_tb_transaction_detail.inventory_name,
               p_new_tb_transaction_detail.inventory_class,
               p_new_tb_transaction_detail.inventory_class_name,
               p_new_tb_transaction_detail.po_nbr,
               p_new_tb_transaction_detail.po_name,
               p_new_tb_transaction_detail.po_date,
               p_new_tb_transaction_detail.po_line_nbr,
               p_new_tb_transaction_detail.po_line_name,
               p_new_tb_transaction_detail.po_line_type,
               p_new_tb_transaction_detail.po_line_type_name,
               p_new_tb_transaction_detail.ship_to_location,
               p_new_tb_transaction_detail.ship_to_location_name,
               p_new_tb_transaction_detail.ship_to_address_line_1,
               p_new_tb_transaction_detail.ship_to_address_line_2,
               p_new_tb_transaction_detail.ship_to_address_line_3,
               p_new_tb_transaction_detail.ship_to_address_line_4,
               p_new_tb_transaction_detail.ship_to_address_city,
               p_new_tb_transaction_detail.ship_to_address_county,
               p_new_tb_transaction_detail.ship_to_address_state,
               p_new_tb_transaction_detail.ship_to_address_zip,
               p_new_tb_transaction_detail.ship_to_address_country,
               p_new_tb_transaction_detail.wo_nbr,
               p_new_tb_transaction_detail.wo_name,
               p_new_tb_transaction_detail.wo_date,
               p_new_tb_transaction_detail.wo_type,
               p_new_tb_transaction_detail.wo_type_desc,
               p_new_tb_transaction_detail.wo_class,
               p_new_tb_transaction_detail.wo_class_desc,
               p_new_tb_transaction_detail.wo_entity,
               p_new_tb_transaction_detail.wo_entity_desc,
               p_new_tb_transaction_detail.wo_line_nbr,
               p_new_tb_transaction_detail.wo_line_name,
               p_new_tb_transaction_detail.wo_line_type,
               p_new_tb_transaction_detail.wo_line_type_desc,
               p_new_tb_transaction_detail.wo_shut_down_cd,
               p_new_tb_transaction_detail.wo_shut_down_cd_desc,
               p_new_tb_transaction_detail.voucher_id,
               p_new_tb_transaction_detail.voucher_name,
               p_new_tb_transaction_detail.voucher_date,
               p_new_tb_transaction_detail.voucher_line_nbr,
               p_new_tb_transaction_detail.voucher_line_desc,
               p_new_tb_transaction_detail.check_nbr,
               p_new_tb_transaction_detail.check_no,
               p_new_tb_transaction_detail.check_date,
               p_new_tb_transaction_detail.check_amt,
               p_new_tb_transaction_detail.check_desc,
               p_new_tb_transaction_detail.user_text_01,
               p_new_tb_transaction_detail.user_text_02,
               p_new_tb_transaction_detail.user_text_03,
               p_new_tb_transaction_detail.user_text_04,
               p_new_tb_transaction_detail.user_text_05,
               p_new_tb_transaction_detail.user_text_06,
               p_new_tb_transaction_detail.user_text_07,
               p_new_tb_transaction_detail.user_text_08,
               p_new_tb_transaction_detail.user_text_09,
               p_new_tb_transaction_detail.user_text_10,
               p_new_tb_transaction_detail.user_text_11,
               p_new_tb_transaction_detail.user_text_12,
               p_new_tb_transaction_detail.user_text_13,
               p_new_tb_transaction_detail.user_text_14,
               p_new_tb_transaction_detail.user_text_15,
               p_new_tb_transaction_detail.user_text_16,
               p_new_tb_transaction_detail.user_text_17,
               p_new_tb_transaction_detail.user_text_18,
               p_new_tb_transaction_detail.user_text_19,
               p_new_tb_transaction_detail.user_text_20,
               p_new_tb_transaction_detail.user_text_21,
               p_new_tb_transaction_detail.user_text_22,
               p_new_tb_transaction_detail.user_text_23,
               p_new_tb_transaction_detail.user_text_24,
               p_new_tb_transaction_detail.user_text_25,
               p_new_tb_transaction_detail.user_text_26,
               p_new_tb_transaction_detail.user_text_27,
               p_new_tb_transaction_detail.user_text_28,
               p_new_tb_transaction_detail.user_text_29,
               p_new_tb_transaction_detail.user_text_30,
               p_new_tb_transaction_detail.user_number_01,
               p_new_tb_transaction_detail.user_number_02,
               p_new_tb_transaction_detail.user_number_03,
               p_new_tb_transaction_detail.user_number_04,
               p_new_tb_transaction_detail.user_number_05,
               p_new_tb_transaction_detail.user_number_06,
               p_new_tb_transaction_detail.user_number_07,
               p_new_tb_transaction_detail.user_number_08,
               p_new_tb_transaction_detail.user_number_09,
               p_new_tb_transaction_detail.user_number_10,
               p_new_tb_transaction_detail.user_date_01,
               p_new_tb_transaction_detail.user_date_02,
               p_new_tb_transaction_detail.user_date_03,
               p_new_tb_transaction_detail.user_date_04,
               p_new_tb_transaction_detail.user_date_05,
               p_new_tb_transaction_detail.user_date_06,
               p_new_tb_transaction_detail.user_date_07,
               p_new_tb_transaction_detail.user_date_08,
               p_new_tb_transaction_detail.user_date_09,
               p_new_tb_transaction_detail.user_date_10,
               p_new_tb_transaction_detail.comments,
               p_new_tb_transaction_detail.tb_calc_tax_amt,
               p_new_tb_transaction_detail.state_use_amount,
               p_new_tb_transaction_detail.state_use_tier2_amount,
               p_new_tb_transaction_detail.state_use_tier3_amount,
               p_new_tb_transaction_detail.county_use_amount,
               p_new_tb_transaction_detail.county_local_use_amount,
               p_new_tb_transaction_detail.city_use_amount,
               p_new_tb_transaction_detail.city_local_use_amount,
               p_new_tb_transaction_detail.transaction_state_code,
               p_new_tb_transaction_detail.transaction_ind,
               p_new_tb_transaction_detail.suspend_ind,
               p_new_tb_transaction_detail.taxcode_detail_id,
               p_new_tb_transaction_detail.taxcode_state_code,
               p_new_tb_transaction_detail.taxcode_type_code,
               p_new_tb_transaction_detail.taxcode_code,
               p_new_tb_transaction_detail.cch_taxcat_code,
               p_new_tb_transaction_detail.cch_group_code,
               p_new_tb_transaction_detail.cch_item_code,
               p_new_tb_transaction_detail.manual_taxcode_ind,
               p_new_tb_transaction_detail.tax_matrix_id,
               p_new_tb_transaction_detail.location_matrix_id,
               p_new_tb_transaction_detail.jurisdiction_id,
               p_new_tb_transaction_detail.jurisdiction_taxrate_id,
               p_new_tb_transaction_detail.manual_jurisdiction_ind,
               p_new_tb_transaction_detail.measure_type_code,
               p_new_tb_transaction_detail.state_use_rate,
               p_new_tb_transaction_detail.state_use_tier2_rate,
               p_new_tb_transaction_detail.state_use_tier3_rate,
               p_new_tb_transaction_detail.state_split_amount,
               p_new_tb_transaction_detail.state_tier2_min_amount,
               p_new_tb_transaction_detail.state_tier2_max_amount,
               p_new_tb_transaction_detail.state_maxtax_amount,
               p_new_tb_transaction_detail.county_use_rate,
               p_new_tb_transaction_detail.county_local_use_rate,
               p_new_tb_transaction_detail.county_split_amount,
               p_new_tb_transaction_detail.county_maxtax_amount,
               p_new_tb_transaction_detail.county_single_flag,
               p_new_tb_transaction_detail.county_default_flag,
               p_new_tb_transaction_detail.city_use_rate,
               p_new_tb_transaction_detail.city_local_use_rate,
               p_new_tb_transaction_detail.city_split_amount,
               p_new_tb_transaction_detail.city_split_use_rate,
               p_new_tb_transaction_detail.city_single_flag,
               p_new_tb_transaction_detail.city_default_flag,
               p_new_tb_transaction_detail.combined_use_rate,
               p_new_tb_transaction_detail.load_timestamp,
               p_new_tb_transaction_detail.gl_extract_flag,
               p_new_tb_transaction_detail.gl_extract_amt,
               p_new_tb_transaction_detail.audit_flag,
               p_new_tb_transaction_detail.audit_user_id,
               p_new_tb_transaction_detail.audit_timestamp,
               p_new_tb_transaction_detail.modify_user_id,
               p_new_tb_transaction_detail.modify_timestamp,
               p_new_tb_transaction_detail.update_user_id,
               p_v_sysdate_plus );
         END IF;

      -- If this transaction has been currently marked by GL Extract, and user decides to change the taxcode.
      ELSIF  p_new_tb_transaction_detail.GL_EXTRACT_FLAG IS NOT NULL THEN
         -- 1. reset the GL_EXTRACT_FLAG to make it available for next GL Extract
         p_new_tb_transaction_detail.gl_extract_flag := NULL;

         -- 2. If the current GL mark was marked first time by the first GL Extract process, then set the GL Log flag
         --    to remember GL Logging.
         IF p_old_tb_transaction_detail.gl_log_flag IS NULL THEN
            p_new_tb_transaction_detail.GL_LOG_FLAG := 1;
         END IF;

         -- 3. Create a before image for this transaction into TO_GL_EXPORT_LOG table with the negative tax amount.
         --    This is for offset the previous tax amount that had sent into ERP system.
         INSERT INTO tb_gl_export_log (
            transaction_detail_id,
            source_transaction_id,
            process_batch_no,
            gl_extract_batch_no,
            archive_batch_no,
            allocation_matrix_id,
            allocation_subtrans_id,
            entered_date,
            transaction_status,
            gl_date,
            gl_company_nbr,
            gl_company_name,
            gl_division_nbr,
            gl_division_name,
            gl_cc_nbr_dept_id,
            gl_cc_nbr_dept_name,
            gl_local_acct_nbr,
            gl_local_acct_name,
            gl_local_sub_acct_nbr,
            gl_local_sub_acct_name,
            gl_full_acct_nbr,
            gl_full_acct_name,
            gl_line_itm_dist_amt,
            orig_gl_line_itm_dist_amt,
            vendor_nbr,
            vendor_name,
            vendor_address_line_1,
            vendor_address_line_2,
            vendor_address_line_3,
            vendor_address_line_4,
            vendor_address_city,
            vendor_address_county,
            vendor_address_state,
            vendor_address_zip,
            vendor_address_country,
            vendor_type,
            vendor_type_name,
            invoice_nbr,
            invoice_desc,
            invoice_date,
            invoice_freight_amt,
            invoice_discount_amt,
            invoice_tax_amt,
            invoice_total_amt,
            invoice_tax_flg,
            invoice_line_nbr,
            invoice_line_name,
            invoice_line_type,
            invoice_line_type_name,
            invoice_line_amt,
            invoice_line_tax,
            afe_project_nbr,
            afe_project_name,
            afe_category_nbr,
            afe_category_name,
            afe_sub_cat_nbr,
            afe_sub_cat_name,
            afe_use,
            afe_contract_type,
            afe_contract_structure,
            afe_property_cat,
            inventory_nbr,
            inventory_name,
            inventory_class,
            inventory_class_name,
            po_nbr,
            po_name,
            po_date,
            po_line_nbr,
            po_line_name,
            po_line_type,
            po_line_type_name,
            ship_to_location,
            ship_to_location_name,
            ship_to_address_line_1,
            ship_to_address_line_2,
            ship_to_address_line_3,
            ship_to_address_line_4,
            ship_to_address_city,
            ship_to_address_county,
            ship_to_address_state,
            ship_to_address_zip,
            ship_to_address_country,
            wo_nbr,
            wo_name,
            wo_date,
            wo_type,
            wo_type_desc,
            wo_class,
            wo_class_desc,
            wo_entity,
            wo_entity_desc,
            wo_line_nbr,
            wo_line_name,
            wo_line_type,
            wo_line_type_desc,
            wo_shut_down_cd,
            wo_shut_down_cd_desc,
            voucher_id,
            voucher_name,
            voucher_date,
            voucher_line_nbr,
            voucher_line_desc,
            check_nbr,
            check_no,
            check_date,
            check_amt,
            check_desc,
            user_text_01,
            user_text_02,
            user_text_03,
            user_text_04,
            user_text_05,
            user_text_06,
            user_text_07,
            user_text_08,
            user_text_09,
            user_text_10,
            user_text_11,
            user_text_12,
            user_text_13,
            user_text_14,
            user_text_15,
            user_text_16,
            user_text_17,
            user_text_18,
            user_text_19,
            user_text_20,
            user_text_21,
            user_text_22,
            user_text_23,
            user_text_24,
            user_text_25,
            user_text_26,
            user_text_27,
            user_text_28,
            user_text_29,
            user_text_30,
            user_number_01,
            user_number_02,
            user_number_03,
            user_number_04,
            user_number_05,
            user_number_06,
            user_number_07,
            user_number_08,
            user_number_09,
            user_number_10,
            user_date_01,
            user_date_02,
            user_date_03,
            user_date_04,
            user_date_05,
            user_date_06,
            user_date_07,
            user_date_08,
            user_date_09,
            user_date_10,
            comments,
            tb_calc_tax_amt,
            state_use_amount,
            state_use_tier2_amount,
            state_use_tier3_amount,
            county_use_amount,
            county_local_use_amount,
            city_use_amount,
            city_local_use_amount,
            transaction_state_code,
            transaction_ind,
            suspend_ind,
            taxcode_detail_id,
            taxcode_state_code,
            taxcode_type_code,
            taxcode_code,
            cch_taxcat_code,
            cch_group_code,
            cch_item_code,
            manual_taxcode_ind,
            tax_matrix_id,
            location_matrix_id,
            jurisdiction_id,
            jurisdiction_taxrate_id,
            manual_jurisdiction_ind,
            measure_type_code,
            state_use_rate,
            state_use_tier2_rate,
            state_use_tier3_rate,
            state_split_amount,
            state_tier2_min_amount,
            state_tier2_max_amount,
            state_maxtax_amount,
            county_use_rate,
            county_local_use_rate,
            county_split_amount,
            county_maxtax_amount,
            county_single_flag,
            county_default_flag,
            city_use_rate,
            city_local_use_rate,
            city_split_amount,
            city_split_use_rate,
            city_single_flag,
            city_default_flag,
            combined_use_rate,
            load_timestamp,
            gl_extract_flag,
            gl_extract_amt,
            audit_flag,
            audit_user_id,
            audit_timestamp,
            modify_user_id,
            modify_timestamp,
            update_user_id,
            update_timestamp )
         VALUES (
            p_old_tb_transaction_detail.transaction_detail_id,
            p_old_tb_transaction_detail.source_transaction_id,
            p_old_tb_transaction_detail.process_batch_no,
            p_old_tb_transaction_detail.gl_extract_batch_no,
            p_old_tb_transaction_detail.archive_batch_no,
            p_old_tb_transaction_detail.allocation_matrix_id,
            p_old_tb_transaction_detail.allocation_subtrans_id,
            p_old_tb_transaction_detail.entered_date,
            p_old_tb_transaction_detail.transaction_status,
            p_old_tb_transaction_detail.gl_date,
            p_old_tb_transaction_detail.gl_company_nbr,
            p_old_tb_transaction_detail.gl_company_name,
            p_old_tb_transaction_detail.gl_division_nbr,
            p_old_tb_transaction_detail.gl_division_name,
            p_old_tb_transaction_detail.gl_cc_nbr_dept_id,
            p_old_tb_transaction_detail.gl_cc_nbr_dept_name,
            p_old_tb_transaction_detail.gl_local_acct_nbr,
            p_old_tb_transaction_detail.gl_local_acct_name,
            p_old_tb_transaction_detail.gl_local_sub_acct_nbr,
            p_old_tb_transaction_detail.gl_local_sub_acct_name,
            p_old_tb_transaction_detail.gl_full_acct_nbr,
            p_old_tb_transaction_detail.gl_full_acct_name,
            p_old_tb_transaction_detail.gl_line_itm_dist_amt,
            p_old_tb_transaction_detail.orig_gl_line_itm_dist_amt,
            p_old_tb_transaction_detail.vendor_nbr,
            p_old_tb_transaction_detail.vendor_name,
            p_old_tb_transaction_detail.vendor_address_line_1,
            p_old_tb_transaction_detail.vendor_address_line_2,
            p_old_tb_transaction_detail.vendor_address_line_3,
            p_old_tb_transaction_detail.vendor_address_line_4,
            p_old_tb_transaction_detail.vendor_address_city,
            p_old_tb_transaction_detail.vendor_address_county,
            p_old_tb_transaction_detail.vendor_address_state,
            p_old_tb_transaction_detail.vendor_address_zip,
            p_old_tb_transaction_detail.vendor_address_country,
            p_old_tb_transaction_detail.vendor_type,
            p_old_tb_transaction_detail.vendor_type_name,
            p_old_tb_transaction_detail.invoice_nbr,
            p_old_tb_transaction_detail.invoice_desc,
            p_old_tb_transaction_detail.invoice_date,
            p_old_tb_transaction_detail.invoice_freight_amt,
            p_old_tb_transaction_detail.invoice_discount_amt,
            p_old_tb_transaction_detail.invoice_tax_amt,
            p_old_tb_transaction_detail.invoice_total_amt,
            p_old_tb_transaction_detail.invoice_tax_flg,
            p_old_tb_transaction_detail.invoice_line_nbr,
            p_old_tb_transaction_detail.invoice_line_name,
            p_old_tb_transaction_detail.invoice_line_type,
            p_old_tb_transaction_detail.invoice_line_type_name,
            p_old_tb_transaction_detail.invoice_line_amt,
            p_old_tb_transaction_detail.invoice_line_tax,
            p_old_tb_transaction_detail.afe_project_nbr,
            p_old_tb_transaction_detail.afe_project_name,
            p_old_tb_transaction_detail.afe_category_nbr,
            p_old_tb_transaction_detail.afe_category_name,
            p_old_tb_transaction_detail.afe_sub_cat_nbr,
            p_old_tb_transaction_detail.afe_sub_cat_name,
            p_old_tb_transaction_detail.afe_use,
            p_old_tb_transaction_detail.afe_contract_type,
            p_old_tb_transaction_detail.afe_contract_structure,
            p_old_tb_transaction_detail.afe_property_cat,
            p_old_tb_transaction_detail.inventory_nbr,
            p_old_tb_transaction_detail.inventory_name,
            p_old_tb_transaction_detail.inventory_class,
            p_old_tb_transaction_detail.inventory_class_name,
            p_old_tb_transaction_detail.po_nbr,
            p_old_tb_transaction_detail.po_name,
            p_old_tb_transaction_detail.po_date,
            p_old_tb_transaction_detail.po_line_nbr,
            p_old_tb_transaction_detail.po_line_name,
            p_old_tb_transaction_detail.po_line_type,
            p_old_tb_transaction_detail.po_line_type_name,
            p_old_tb_transaction_detail.ship_to_location,
            p_old_tb_transaction_detail.ship_to_location_name,
            p_old_tb_transaction_detail.ship_to_address_line_1,
            p_old_tb_transaction_detail.ship_to_address_line_2,
            p_old_tb_transaction_detail.ship_to_address_line_3,
            p_old_tb_transaction_detail.ship_to_address_line_4,
            p_old_tb_transaction_detail.ship_to_address_city,
            p_old_tb_transaction_detail.ship_to_address_county,
            p_old_tb_transaction_detail.ship_to_address_state,
            p_old_tb_transaction_detail.ship_to_address_zip,
            p_old_tb_transaction_detail.ship_to_address_country,
            p_old_tb_transaction_detail.wo_nbr,
            p_old_tb_transaction_detail.wo_name,
            p_old_tb_transaction_detail.wo_date,
            p_old_tb_transaction_detail.wo_type,
            p_old_tb_transaction_detail.wo_type_desc,
            p_old_tb_transaction_detail.wo_class,
            p_old_tb_transaction_detail.wo_class_desc,
            p_old_tb_transaction_detail.wo_entity,
            p_old_tb_transaction_detail.wo_entity_desc,
            p_old_tb_transaction_detail.wo_line_nbr,
            p_old_tb_transaction_detail.wo_line_name,
            p_old_tb_transaction_detail.wo_line_type,
            p_old_tb_transaction_detail.wo_line_type_desc,
            p_old_tb_transaction_detail.wo_shut_down_cd,
            p_old_tb_transaction_detail.wo_shut_down_cd_desc,
            p_old_tb_transaction_detail.voucher_id,
            p_old_tb_transaction_detail.voucher_name,
            p_old_tb_transaction_detail.voucher_date,
            p_old_tb_transaction_detail.voucher_line_nbr,
            p_old_tb_transaction_detail.voucher_line_desc,
            p_old_tb_transaction_detail.check_nbr,
            p_old_tb_transaction_detail.check_no,
            p_old_tb_transaction_detail.check_date,
            p_old_tb_transaction_detail.check_amt,
            p_old_tb_transaction_detail.check_desc,
            p_old_tb_transaction_detail.user_text_01,
            p_old_tb_transaction_detail.user_text_02,
            p_old_tb_transaction_detail.user_text_03,
            p_old_tb_transaction_detail.user_text_04,
            p_old_tb_transaction_detail.user_text_05,
            p_old_tb_transaction_detail.user_text_06,
            p_old_tb_transaction_detail.user_text_07,
            p_old_tb_transaction_detail.user_text_08,
            p_old_tb_transaction_detail.user_text_09,
            p_old_tb_transaction_detail.user_text_10,
            p_old_tb_transaction_detail.user_text_11,
            p_old_tb_transaction_detail.user_text_12,
            p_old_tb_transaction_detail.user_text_13,
            p_old_tb_transaction_detail.user_text_14,
            p_old_tb_transaction_detail.user_text_15,
            p_old_tb_transaction_detail.user_text_16,
            p_old_tb_transaction_detail.user_text_17,
            p_old_tb_transaction_detail.user_text_18,
            p_old_tb_transaction_detail.user_text_19,
            p_old_tb_transaction_detail.user_text_20,
            p_old_tb_transaction_detail.user_text_21,
            p_old_tb_transaction_detail.user_text_22,
            p_old_tb_transaction_detail.user_text_23,
            p_old_tb_transaction_detail.user_text_24,
            p_old_tb_transaction_detail.user_text_25,
            p_old_tb_transaction_detail.user_text_26,
            p_old_tb_transaction_detail.user_text_27,
            p_old_tb_transaction_detail.user_text_28,
            p_old_tb_transaction_detail.user_text_29,
            p_old_tb_transaction_detail.user_text_30,
            p_old_tb_transaction_detail.user_number_01,
            p_old_tb_transaction_detail.user_number_02,
            p_old_tb_transaction_detail.user_number_03,
            p_old_tb_transaction_detail.user_number_04,
            p_old_tb_transaction_detail.user_number_05,
            p_old_tb_transaction_detail.user_number_06,
            p_old_tb_transaction_detail.user_number_07,
            p_old_tb_transaction_detail.user_number_08,
            p_old_tb_transaction_detail.user_number_09,
            p_old_tb_transaction_detail.user_number_10,
            p_old_tb_transaction_detail.user_date_01,
            p_old_tb_transaction_detail.user_date_02,
            p_old_tb_transaction_detail.user_date_03,
            p_old_tb_transaction_detail.user_date_04,
            p_old_tb_transaction_detail.user_date_05,
            p_old_tb_transaction_detail.user_date_06,
            p_old_tb_transaction_detail.user_date_07,
            p_old_tb_transaction_detail.user_date_08,
            p_old_tb_transaction_detail.user_date_09,
            p_old_tb_transaction_detail.user_date_10,
            p_old_tb_transaction_detail.comments,
            p_old_tb_transaction_detail.tb_calc_tax_amt * -1,
            p_old_tb_transaction_detail.state_use_amount * -1,
            p_old_tb_transaction_detail.state_use_tier2_amount * -1,
            p_old_tb_transaction_detail.state_use_tier3_amount * -1,
            p_old_tb_transaction_detail.county_use_amount * -1,
            p_old_tb_transaction_detail.county_local_use_amount * -1,
            p_old_tb_transaction_detail.city_use_amount * -1,
            p_old_tb_transaction_detail.city_local_use_amount * -1,
            p_old_tb_transaction_detail.transaction_state_code,
            p_old_tb_transaction_detail.transaction_ind,
            p_old_tb_transaction_detail.suspend_ind,
            p_old_tb_transaction_detail.taxcode_detail_id,
            p_old_tb_transaction_detail.taxcode_state_code,
            p_old_tb_transaction_detail.taxcode_type_code,
            p_old_tb_transaction_detail.taxcode_code,
            p_old_tb_transaction_detail.cch_taxcat_code,
            p_old_tb_transaction_detail.cch_group_code,
            p_old_tb_transaction_detail.cch_item_code,
            p_old_tb_transaction_detail.manual_taxcode_ind,
            p_old_tb_transaction_detail.tax_matrix_id,
            p_old_tb_transaction_detail.location_matrix_id,
            p_old_tb_transaction_detail.jurisdiction_id,
            p_old_tb_transaction_detail.jurisdiction_taxrate_id,
            p_old_tb_transaction_detail.manual_jurisdiction_ind,
            p_old_tb_transaction_detail.measure_type_code,
            p_old_tb_transaction_detail.state_use_rate,
            p_old_tb_transaction_detail.state_use_tier2_rate,
            p_old_tb_transaction_detail.state_use_tier3_rate,
            p_old_tb_transaction_detail.state_split_amount,
            p_old_tb_transaction_detail.state_tier2_min_amount,
            p_old_tb_transaction_detail.state_tier2_max_amount,
            p_old_tb_transaction_detail.state_maxtax_amount,
            p_old_tb_transaction_detail.county_use_rate,
            p_old_tb_transaction_detail.county_local_use_rate,
            p_old_tb_transaction_detail.county_split_amount,
            p_old_tb_transaction_detail.county_maxtax_amount,
            p_old_tb_transaction_detail.county_single_flag,
            p_old_tb_transaction_detail.county_default_flag,
            p_old_tb_transaction_detail.city_use_rate,
            p_old_tb_transaction_detail.city_local_use_rate,
            p_old_tb_transaction_detail.city_split_amount,
            p_old_tb_transaction_detail.city_split_use_rate,
            p_old_tb_transaction_detail.city_single_flag,
            p_old_tb_transaction_detail.city_default_flag,
            p_old_tb_transaction_detail.combined_use_rate,
            p_old_tb_transaction_detail.load_timestamp,
             NULL,
            p_old_tb_transaction_detail.gl_extract_amt,
            p_old_tb_transaction_detail.audit_flag,
            p_old_tb_transaction_detail.audit_user_id,
            p_old_tb_transaction_detail.audit_timestamp,
            p_old_tb_transaction_detail.modify_user_id,
            p_old_tb_transaction_detail.modify_timestamp,
            p_old_tb_transaction_detail.update_user_id,
            p_v_sysdate );

         -- 4. Create an after image for this transaction into TO_GL_EXPORT_LOG table with the new tax amount.
         --    This is for tracking the newest tax amount.
         INSERT INTO tb_gl_export_log (
            transaction_detail_id,
            source_transaction_id,
            process_batch_no,
            gl_extract_batch_no,
            archive_batch_no,
            allocation_matrix_id,
            allocation_subtrans_id,
            entered_date,
            transaction_status,
            gl_date,
            gl_company_nbr,
            gl_company_name,
            gl_division_nbr,
            gl_division_name,
            gl_cc_nbr_dept_id,
            gl_cc_nbr_dept_name,
            gl_local_acct_nbr,
            gl_local_acct_name,
            gl_local_sub_acct_nbr,
            gl_local_sub_acct_name,
            gl_full_acct_nbr,
            gl_full_acct_name,
            gl_line_itm_dist_amt,
            orig_gl_line_itm_dist_amt,
            vendor_nbr,
            vendor_name,
            vendor_address_line_1,
            vendor_address_line_2,
            vendor_address_line_3,
            vendor_address_line_4,
            vendor_address_city,
            vendor_address_county,
            vendor_address_state,
            vendor_address_zip,
            vendor_address_country,
            vendor_type,
            vendor_type_name,
            invoice_nbr,
            invoice_desc,
            invoice_date,
            invoice_freight_amt,
            invoice_discount_amt,
            invoice_tax_amt,
            invoice_total_amt,
            invoice_tax_flg,
            invoice_line_nbr,
            invoice_line_name,
            invoice_line_type,
            invoice_line_type_name,
            invoice_line_amt,
            invoice_line_tax,
            afe_project_nbr,
            afe_project_name,
            afe_category_nbr,
            afe_category_name,
            afe_sub_cat_nbr,
            afe_sub_cat_name,
            afe_use,
            afe_contract_type,
            afe_contract_structure,
            afe_property_cat,
            inventory_nbr,
            inventory_name,
            inventory_class,
            inventory_class_name,
            po_nbr,
            po_name,
            po_date,
            po_line_nbr,
            po_line_name,
            po_line_type,
            po_line_type_name,
            ship_to_location,
            ship_to_location_name,
            ship_to_address_line_1,
            ship_to_address_line_2,
            ship_to_address_line_3,
            ship_to_address_line_4,
            ship_to_address_city,
            ship_to_address_county,
            ship_to_address_state,
            ship_to_address_zip,
            ship_to_address_country,
            wo_nbr,
            wo_name,
            wo_date,
            wo_type,
            wo_type_desc,
            wo_class,
            wo_class_desc,
            wo_entity,
            wo_entity_desc,
            wo_line_nbr,
            wo_line_name,
            wo_line_type,
            wo_line_type_desc,
            wo_shut_down_cd,
            wo_shut_down_cd_desc,
            voucher_id,
            voucher_name,
            voucher_date,
            voucher_line_nbr,
            voucher_line_desc,
            check_nbr,
            check_no,
            check_date,
            check_amt,
            check_desc,
            user_text_01,
            user_text_02,
            user_text_03,
            user_text_04,
            user_text_05,
            user_text_06,
            user_text_07,
            user_text_08,
            user_text_09,
            user_text_10,
            user_text_11,
            user_text_12,
            user_text_13,
            user_text_14,
            user_text_15,
            user_text_16,
            user_text_17,
            user_text_18,
            user_text_19,
            user_text_20,
            user_text_21,
            user_text_22,
            user_text_23,
            user_text_24,
            user_text_25,
            user_text_26,
            user_text_27,
            user_text_28,
            user_text_29,
            user_text_30,
            user_number_01,
            user_number_02,
            user_number_03,
            user_number_04,
            user_number_05,
            user_number_06,
            user_number_07,
            user_number_08,
            user_number_09,
            user_number_10,
            user_date_01,
            user_date_02,
            user_date_03,
            user_date_04,
            user_date_05,
            user_date_06,
            user_date_07,
            user_date_08,
            user_date_09,
            user_date_10,
            comments,
            tb_calc_tax_amt,
            state_use_amount,
            state_use_tier2_amount,
            state_use_tier3_amount,
            county_use_amount,
            county_local_use_amount,
            city_use_amount,
            city_local_use_amount,
            transaction_state_code,
            transaction_ind,
            suspend_ind,
            taxcode_detail_id,
            taxcode_state_code,
            taxcode_type_code,
            taxcode_code,
            cch_taxcat_code,
            cch_group_code,
            cch_item_code,
            manual_taxcode_ind,
            tax_matrix_id,
            location_matrix_id,
            jurisdiction_id,
            jurisdiction_taxrate_id,
            manual_jurisdiction_ind,
            measure_type_code,
            state_use_rate,
            state_use_tier2_rate,
            state_use_tier3_rate,
            state_split_amount,
            state_tier2_min_amount,
            state_tier2_max_amount,
            state_maxtax_amount,
            county_use_rate,
            county_local_use_rate,
            county_split_amount,
            county_maxtax_amount,
            county_single_flag,
            county_default_flag,
            city_use_rate,
            city_local_use_rate,
            city_split_amount,
            city_split_use_rate,
            city_single_flag,
            city_default_flag,
            combined_use_rate,
            load_timestamp,
            gl_extract_flag,
            gl_extract_amt,
            audit_flag,
            audit_user_id,
            audit_timestamp,
            modify_user_id,
            modify_timestamp,
            update_user_id,
            update_timestamp )
         VALUES (
            p_new_tb_transaction_detail.transaction_detail_id,
            p_new_tb_transaction_detail.source_transaction_id,
            p_new_tb_transaction_detail.process_batch_no,
            p_new_tb_transaction_detail.gl_extract_batch_no,
            p_new_tb_transaction_detail.archive_batch_no,
            p_new_tb_transaction_detail.allocation_matrix_id,
            p_new_tb_transaction_detail.allocation_subtrans_id,
            p_new_tb_transaction_detail.entered_date,
            p_new_tb_transaction_detail.transaction_status,
            p_new_tb_transaction_detail.gl_date,
            p_new_tb_transaction_detail.gl_company_nbr,
            p_new_tb_transaction_detail.gl_company_name,
            p_new_tb_transaction_detail.gl_division_nbr,
            p_new_tb_transaction_detail.gl_division_name,
            p_new_tb_transaction_detail.gl_cc_nbr_dept_id,
            p_new_tb_transaction_detail.gl_cc_nbr_dept_name,
            p_new_tb_transaction_detail.gl_local_acct_nbr,
            p_new_tb_transaction_detail.gl_local_acct_name,
            p_new_tb_transaction_detail.gl_local_sub_acct_nbr,
            p_new_tb_transaction_detail.gl_local_sub_acct_name,
            p_new_tb_transaction_detail.gl_full_acct_nbr,
            p_new_tb_transaction_detail.gl_full_acct_name,
            p_new_tb_transaction_detail.gl_line_itm_dist_amt,
            p_new_tb_transaction_detail.orig_gl_line_itm_dist_amt,
            p_new_tb_transaction_detail.vendor_nbr,
            p_new_tb_transaction_detail.vendor_name,
            p_new_tb_transaction_detail.vendor_address_line_1,
            p_new_tb_transaction_detail.vendor_address_line_2,
            p_new_tb_transaction_detail.vendor_address_line_3,
            p_new_tb_transaction_detail.vendor_address_line_4,
            p_new_tb_transaction_detail.vendor_address_city,
            p_new_tb_transaction_detail.vendor_address_county,
            p_new_tb_transaction_detail.vendor_address_state,
            p_new_tb_transaction_detail.vendor_address_zip,
            p_new_tb_transaction_detail.vendor_address_country,
            p_new_tb_transaction_detail.vendor_type,
            p_new_tb_transaction_detail.vendor_type_name,
            p_new_tb_transaction_detail.invoice_nbr,
            p_new_tb_transaction_detail.invoice_desc,
            p_new_tb_transaction_detail.invoice_date,
            p_new_tb_transaction_detail.invoice_freight_amt,
            p_new_tb_transaction_detail.invoice_discount_amt,
            p_new_tb_transaction_detail.invoice_tax_amt,
            p_new_tb_transaction_detail.invoice_total_amt,
            p_new_tb_transaction_detail.invoice_tax_flg,
            p_new_tb_transaction_detail.invoice_line_nbr,
            p_new_tb_transaction_detail.invoice_line_name,
            p_new_tb_transaction_detail.invoice_line_type,
            p_new_tb_transaction_detail.invoice_line_type_name,
            p_new_tb_transaction_detail.invoice_line_amt,
            p_new_tb_transaction_detail.invoice_line_tax,
            p_new_tb_transaction_detail.afe_project_nbr,
            p_new_tb_transaction_detail.afe_project_name,
            p_new_tb_transaction_detail.afe_category_nbr,
            p_new_tb_transaction_detail.afe_category_name,
            p_new_tb_transaction_detail.afe_sub_cat_nbr,
            p_new_tb_transaction_detail.afe_sub_cat_name,
            p_new_tb_transaction_detail.afe_use,
            p_new_tb_transaction_detail.afe_contract_type,
            p_new_tb_transaction_detail.afe_contract_structure,
            p_new_tb_transaction_detail.afe_property_cat,
            p_new_tb_transaction_detail.inventory_nbr,
            p_new_tb_transaction_detail.inventory_name,
            p_new_tb_transaction_detail.inventory_class,
            p_new_tb_transaction_detail.inventory_class_name,
            p_new_tb_transaction_detail.po_nbr,
            p_new_tb_transaction_detail.po_name,
            p_new_tb_transaction_detail.po_date,
            p_new_tb_transaction_detail.po_line_nbr,
            p_new_tb_transaction_detail.po_line_name,
            p_new_tb_transaction_detail.po_line_type,
            p_new_tb_transaction_detail.po_line_type_name,
            p_new_tb_transaction_detail.ship_to_location,
            p_new_tb_transaction_detail.ship_to_location_name,
            p_new_tb_transaction_detail.ship_to_address_line_1,
            p_new_tb_transaction_detail.ship_to_address_line_2,
            p_new_tb_transaction_detail.ship_to_address_line_3,
            p_new_tb_transaction_detail.ship_to_address_line_4,
            p_new_tb_transaction_detail.ship_to_address_city,
            p_new_tb_transaction_detail.ship_to_address_county,
            p_new_tb_transaction_detail.ship_to_address_state,
            p_new_tb_transaction_detail.ship_to_address_zip,
            p_new_tb_transaction_detail.ship_to_address_country,
            p_new_tb_transaction_detail.wo_nbr,
            p_new_tb_transaction_detail.wo_name,
            p_new_tb_transaction_detail.wo_date,
            p_new_tb_transaction_detail.wo_type,
            p_new_tb_transaction_detail.wo_type_desc,
            p_new_tb_transaction_detail.wo_class,
            p_new_tb_transaction_detail.wo_class_desc,
            p_new_tb_transaction_detail.wo_entity,
            p_new_tb_transaction_detail.wo_entity_desc,
            p_new_tb_transaction_detail.wo_line_nbr,
            p_new_tb_transaction_detail.wo_line_name,
            p_new_tb_transaction_detail.wo_line_type,
            p_new_tb_transaction_detail.wo_line_type_desc,
            p_new_tb_transaction_detail.wo_shut_down_cd,
            p_new_tb_transaction_detail.wo_shut_down_cd_desc,
            p_new_tb_transaction_detail.voucher_id,
            p_new_tb_transaction_detail.voucher_name,
            p_new_tb_transaction_detail.voucher_date,
            p_new_tb_transaction_detail.voucher_line_nbr,
            p_new_tb_transaction_detail.voucher_line_desc,
            p_new_tb_transaction_detail.check_nbr,
            p_new_tb_transaction_detail.check_no,
            p_new_tb_transaction_detail.check_date,
            p_new_tb_transaction_detail.check_amt,
            p_new_tb_transaction_detail.check_desc,
            p_new_tb_transaction_detail.user_text_01,
            p_new_tb_transaction_detail.user_text_02,
            p_new_tb_transaction_detail.user_text_03,
            p_new_tb_transaction_detail.user_text_04,
            p_new_tb_transaction_detail.user_text_05,
            p_new_tb_transaction_detail.user_text_06,
            p_new_tb_transaction_detail.user_text_07,
            p_new_tb_transaction_detail.user_text_08,
            p_new_tb_transaction_detail.user_text_09,
            p_new_tb_transaction_detail.user_text_10,
            p_new_tb_transaction_detail.user_text_11,
            p_new_tb_transaction_detail.user_text_12,
            p_new_tb_transaction_detail.user_text_13,
            p_new_tb_transaction_detail.user_text_14,
            p_new_tb_transaction_detail.user_text_15,
            p_new_tb_transaction_detail.user_text_16,
            p_new_tb_transaction_detail.user_text_17,
            p_new_tb_transaction_detail.user_text_18,
            p_new_tb_transaction_detail.user_text_19,
            p_new_tb_transaction_detail.user_text_20,
            p_new_tb_transaction_detail.user_text_21,
            p_new_tb_transaction_detail.user_text_22,
            p_new_tb_transaction_detail.user_text_23,
            p_new_tb_transaction_detail.user_text_24,
            p_new_tb_transaction_detail.user_text_25,
            p_new_tb_transaction_detail.user_text_26,
            p_new_tb_transaction_detail.user_text_27,
            p_new_tb_transaction_detail.user_text_28,
            p_new_tb_transaction_detail.user_text_29,
            p_new_tb_transaction_detail.user_text_30,
            p_new_tb_transaction_detail.user_number_01,
            p_new_tb_transaction_detail.user_number_02,
            p_new_tb_transaction_detail.user_number_03,
            p_new_tb_transaction_detail.user_number_04,
            p_new_tb_transaction_detail.user_number_05,
            p_new_tb_transaction_detail.user_number_06,
            p_new_tb_transaction_detail.user_number_07,
            p_new_tb_transaction_detail.user_number_08,
            p_new_tb_transaction_detail.user_number_09,
            p_new_tb_transaction_detail.user_number_10,
            p_new_tb_transaction_detail.user_date_01,
            p_new_tb_transaction_detail.user_date_02,
            p_new_tb_transaction_detail.user_date_03,
            p_new_tb_transaction_detail.user_date_04,
            p_new_tb_transaction_detail.user_date_05,
            p_new_tb_transaction_detail.user_date_06,
            p_new_tb_transaction_detail.user_date_07,
            p_new_tb_transaction_detail.user_date_08,
            p_new_tb_transaction_detail.user_date_09,
            p_new_tb_transaction_detail.user_date_10,
            p_new_tb_transaction_detail.comments,
            p_new_tb_transaction_detail.tb_calc_tax_amt,
            p_new_tb_transaction_detail.state_use_amount,
            p_new_tb_transaction_detail.state_use_tier2_amount,
            p_new_tb_transaction_detail.state_use_tier3_amount,
            p_new_tb_transaction_detail.county_use_amount,
            p_new_tb_transaction_detail.county_local_use_amount,
            p_new_tb_transaction_detail.city_use_amount,
            p_new_tb_transaction_detail.city_local_use_amount,
            p_new_tb_transaction_detail.transaction_state_code,
            p_new_tb_transaction_detail.transaction_ind,
            p_new_tb_transaction_detail.suspend_ind,
            p_new_tb_transaction_detail.taxcode_detail_id,
            p_new_tb_transaction_detail.taxcode_state_code,
            p_new_tb_transaction_detail.taxcode_type_code,
            p_new_tb_transaction_detail.taxcode_code,
            p_new_tb_transaction_detail.cch_taxcat_code,
            p_new_tb_transaction_detail.cch_group_code,
            p_new_tb_transaction_detail.cch_item_code,
            p_new_tb_transaction_detail.manual_taxcode_ind,
            p_new_tb_transaction_detail.tax_matrix_id,
            p_new_tb_transaction_detail.location_matrix_id,
            p_new_tb_transaction_detail.jurisdiction_id,
            p_new_tb_transaction_detail.jurisdiction_taxrate_id,
            p_new_tb_transaction_detail.manual_jurisdiction_ind,
            p_new_tb_transaction_detail.measure_type_code,
            p_new_tb_transaction_detail.state_use_rate,
            p_new_tb_transaction_detail.state_use_tier2_rate,
            p_new_tb_transaction_detail.state_use_tier3_rate,
            p_new_tb_transaction_detail.state_split_amount,
            p_new_tb_transaction_detail.state_tier2_min_amount,
            p_new_tb_transaction_detail.state_tier2_max_amount,
            p_new_tb_transaction_detail.state_maxtax_amount,
            p_new_tb_transaction_detail.county_use_rate,
            p_new_tb_transaction_detail.county_local_use_rate,
            p_new_tb_transaction_detail.county_split_amount,
            p_new_tb_transaction_detail.county_maxtax_amount,
            p_new_tb_transaction_detail.county_single_flag,
            p_new_tb_transaction_detail.county_default_flag,
            p_new_tb_transaction_detail.city_use_rate,
            p_new_tb_transaction_detail.city_local_use_rate,
            p_new_tb_transaction_detail.city_split_amount,
            p_new_tb_transaction_detail.city_split_use_rate,
            p_new_tb_transaction_detail.city_single_flag,
            p_new_tb_transaction_detail.city_default_flag,
            p_new_tb_transaction_detail.combined_use_rate,
            p_new_tb_transaction_detail.load_timestamp,
            p_new_tb_transaction_detail.gl_extract_flag,
            p_new_tb_transaction_detail.gl_extract_amt,
            p_new_tb_transaction_detail.audit_flag,
            p_new_tb_transaction_detail.audit_user_id,
            p_new_tb_transaction_detail.audit_timestamp,
            p_new_tb_transaction_detail.modify_user_id,
            p_new_tb_transaction_detail.modify_timestamp,
            p_new_tb_transaction_detail.update_user_id,
            p_v_sysdate_plus );

         -- 5. Reset GL_EXTRACT_TIMESTAMP to null for next GL Extract process
            p_new_tb_transaction_detail.gl_extract_timestamp := NULL;
      END IF;
   END IF;

END;
/

SHOW ERRORS;

CREATE OR REPLACE PROCEDURE sp_load_tb_tmp_trns_detail_b_u(p_transaction_detail_id IN tb_transaction_detail.transaction_detail_id%TYPE)
AS
BEGIN
	DELETE tb_tmp_transaction_detail_b_u;

	INSERT INTO tb_tmp_transaction_detail_b_u
	 SELECT * from tb_transaction_detail WHERE transaction_detail_id = p_transaction_detail_id;
END;
/

SHOW ERRORS;

CREATE OR REPLACE PROCEDURE sp_loc_mtrx_drivers_count (
	p_where_clause_token	IN VARCHAR2
)
AS
BEGIN
	EXECUTE IMMEDIATE 
	'INSERT INTO tb_tmp_loc_mtrx_drivers_count
	SELECT * FROM (
		SELECT COUNT (DECODE (driver_01, ''*ALL'', NULL, driver_01)) AS driver_01,
		       COUNT (DECODE (driver_02, ''*ALL'', NULL, driver_02)) AS driver_02,
		       COUNT (DECODE (driver_03, ''*ALL'', NULL, driver_03)) AS driver_03,
		       COUNT (DECODE (driver_04, ''*ALL'', NULL, driver_04)) AS driver_04,
		       COUNT (DECODE (driver_05, ''*ALL'', NULL, driver_05)) AS driver_05,
		       COUNT (DECODE (driver_06, ''*ALL'', NULL, driver_06)) AS driver_06,
		       COUNT (DECODE (driver_07, ''*ALL'', NULL, driver_07)) AS driver_07,
		       COUNT (DECODE (driver_08, ''*ALL'', NULL, driver_08)) AS driver_08,
		       COUNT (DECODE (driver_09, ''*ALL'', NULL, driver_09)) AS driver_09,
		       COUNT (DECODE (driver_10, ''*ALL'', NULL, driver_10)) AS driver_10
		  FROM TB_LOCATION_MATRIX, TB_JURISDICTION
		 WHERE (TB_LOCATION_MATRIX.JURISDICTION_ID = TB_JURISDICTION.JURISDICTION_ID(+))
		   AND ( "TB_LOCATION_MATRIX"."EFFECTIVE_DATE"  <= sysdate )
		   AND ( "TB_LOCATION_MATRIX"."EXPIRATION_DATE" >= sysdate ) '||p_where_clause_token||' 
	)WHERE 1=1 '; 
END;
/

SHOW ERRORS;

CREATE OR REPLACE PROCEDURE sp_max_tb_bcp_juris_taxrate_id
IS
   v_max_id number;
   v_count number;
BEGIN
   SELECT MAX( bcp_jurisdiction_taxrate_id )
   INTO v_max_id
   FROM tb_bcp_jurisdiction_taxrate;

   IF v_max_id IS NULL THEN
      v_max_id := 1;
   ELSE
      v_max_id := v_max_id + 1;
   END IF;

   SELECT count(*)
   INTO v_count
   FROM all_sequences
   WHERE sequence_name = 'SQ_TB_BCP_JURIS_TAXRATE_ID'
   AND sequence_owner = 'STSCORP';

   IF v_count > 0 THEN
      EXECUTE IMMEDIATE 'DROP SEQUENCE STSCORP.sq_tb_bcp_juris_taxrate_id';
   END IF;

   EXECUTE IMMEDIATE 'CREATE SEQUENCE STSCORP.sq_tb_bcp_juris_taxrate_id INCREMENT BY 1 START WITH ' ||
   v_max_id || ' MAXVALUE 1.0E27 MINVALUE 1 NOCYCLE NOCACHE NOORDER';

   EXECUTE IMMEDIATE 'GRANT SELECT ON STSCORP.sq_tb_bcp_juris_taxrate_id TO STSUSER';
END;
/

SHOW ERRORS;

CREATE OR REPLACE PROCEDURE Sp_Random_Trans (start_date IN DATE, end_date IN DATE, sample_row_count IN NUMBER, trans1 IN OUT Sts.trans_cur)
IS
vn_total_rows NUMBER;
vn_sample_rate NUMBER;

BEGIN

SELECT COUNT(*) INTO vn_total_rows FROM TB_TRANSACTION_DETAIL;

IF (sample_row_count < vn_total_rows) AND ( vn_total_rows > 0 ) THEN

   vn_sample_rate:= (sample_row_count/vn_total_rows) * 100;

   IF vn_sample_rate <= 10 THEN
      OPEN trans1 FOR SELECT * FROM (SELECT * FROM TB_TRANSACTION_DETAIL sample(20) WHERE gl_date >= start_date AND gl_date <= end_date ) WHERE ROWNUM <= sample_row_count;
   ELSIF (vn_sample_rate <= 20) AND (vn_sample_rate > 10 ) THEN
      OPEN trans1 FOR SELECT * FROM (SELECT * FROM TB_TRANSACTION_DETAIL sample(40) WHERE gl_date >= start_date AND gl_date <= end_date ) WHERE ROWNUM <= sample_row_count;
   ELSIF (vn_sample_rate <= 30) AND (vn_sample_rate > 20 ) THEN
      OPEN trans1 FOR SELECT * FROM (SELECT * FROM TB_TRANSACTION_DETAIL sample(60) WHERE gl_date >= start_date AND gl_date <= end_date ) WHERE ROWNUM <= sample_row_count;
   ELSIF (vn_sample_rate <= 40) AND (vn_sample_rate > 30 ) THEN
      OPEN trans1 FOR SELECT * FROM (SELECT * FROM TB_TRANSACTION_DETAIL sample(80) WHERE gl_date >= start_date AND gl_date <= end_date ) WHERE ROWNUM <= sample_row_count;
   ELSIF (vn_sample_rate <= 50) AND (vn_sample_rate > 40 ) THEN
      OPEN trans1 FOR SELECT * FROM (SELECT * FROM TB_TRANSACTION_DETAIL sample(80) WHERE gl_date >= start_date AND gl_date <= end_date ) WHERE ROWNUM <= sample_row_count;
   ELSIF (vn_sample_rate <= 60) AND (vn_sample_rate > 50 ) THEN
      OPEN trans1 FOR SELECT * FROM (SELECT * FROM TB_TRANSACTION_DETAIL sample(80) WHERE gl_date >= start_date AND gl_date <= end_date ) WHERE ROWNUM <= sample_row_count;
   ELSIF (vn_sample_rate <= 70) AND (vn_sample_rate > 60 ) THEN
      OPEN trans1 FOR SELECT * FROM (SELECT * FROM TB_TRANSACTION_DETAIL sample(80) WHERE gl_date >= start_date AND gl_date <= end_date ) WHERE ROWNUM <= sample_row_count;
   ELSIF (vn_sample_rate <= 80) AND (vn_sample_rate > 70 ) THEN
      OPEN trans1 FOR SELECT * FROM (SELECT * FROM TB_TRANSACTION_DETAIL sample(90) WHERE gl_date >= start_date AND gl_date <= end_date ) WHERE ROWNUM <= sample_row_count;
   ELSIF (vn_sample_rate <= 100) AND (vn_sample_rate > 80 ) THEN
      OPEN trans1 FOR SELECT * FROM (SELECT * FROM TB_TRANSACTION_DETAIL sample(99) WHERE gl_date >= start_date AND gl_date <= end_date ) WHERE ROWNUM <= sample_row_count;
   END IF;

ELSE

   OPEN trans1 FOR SELECT * FROM (SELECT * FROM TB_TRANSACTION_DETAIL sample(99) WHERE gl_date >= start_date AND gl_date <= end_date ) WHERE ROWNUM <= sample_row_count;

END IF;

END;
/

SHOW ERRORS;

CREATE OR REPLACE PROCEDURE SP_TAX_MTRX_DRIVERS_COUNT (  
	p_where_clause_token IN VARCHAR2
)
AS
BEGIN
	 EXECUTE IMMEDIATE 
	 'INSERT INTO tb_tmp_tax_mtrx_drivers_count
	 SELECT * FROM
	 (
		SELECT COUNT (DECODE (driver_01, ''*ALL'', NULL, driver_01)) AS driver_01,
		       COUNT (DECODE (driver_02, ''*ALL'', NULL, driver_02)) AS driver_02,
		       COUNT (DECODE (driver_03, ''*ALL'', NULL, driver_03)) AS driver_03,
		       COUNT (DECODE (driver_04, ''*ALL'', NULL, driver_04)) AS driver_04,
		       COUNT (DECODE (driver_05, ''*ALL'', NULL, driver_05)) AS driver_05,
		       COUNT (DECODE (driver_06, ''*ALL'', NULL, driver_06)) AS driver_06,
		       COUNT (DECODE (driver_07, ''*ALL'', NULL, driver_07)) AS driver_07,
		       COUNT (DECODE (driver_08, ''*ALL'', NULL, driver_08)) AS driver_08,
		       COUNT (DECODE (driver_09, ''*ALL'', NULL, driver_09)) AS driver_09,
		       COUNT (DECODE (driver_10, ''*ALL'', NULL, driver_10)) AS driver_10,
		       COUNT (DECODE (driver_11, ''*ALL'', NULL, driver_11)) AS driver_11,
		       COUNT (DECODE (driver_12, ''*ALL'', NULL, driver_12)) AS driver_12,
		       COUNT (DECODE (driver_13, ''*ALL'', NULL, driver_13)) AS driver_13,
		       COUNT (DECODE (driver_14, ''*ALL'', NULL, driver_14)) AS driver_14,
		       COUNT (DECODE (driver_15, ''*ALL'', NULL, driver_15)) AS driver_15,
		       COUNT (DECODE (driver_16, ''*ALL'', NULL, driver_16)) AS driver_16,
		       COUNT (DECODE (driver_17, ''*ALL'', NULL, driver_17)) AS driver_17,
		       COUNT (DECODE (driver_18, ''*ALL'', NULL, driver_18)) AS driver_18,
		       COUNT (DECODE (driver_19, ''*ALL'', NULL, driver_19)) AS driver_19,
		       COUNT (DECODE (driver_20, ''*ALL'', NULL, driver_20)) AS driver_20,
		       COUNT (DECODE (driver_21, ''*ALL'', NULL, driver_21)) AS driver_21,
		       COUNT (DECODE (driver_22, ''*ALL'', NULL, driver_22)) AS driver_22,
		       COUNT (DECODE (driver_23, ''*ALL'', NULL, driver_23)) AS driver_23,
		       COUNT (DECODE (driver_24, ''*ALL'', NULL, driver_24)) AS driver_24,
		       COUNT (DECODE (driver_25, ''*ALL'', NULL, driver_25)) AS driver_25,
		       COUNT (DECODE (driver_26, ''*ALL'', NULL, driver_26)) AS driver_26,
		       COUNT (DECODE (driver_27, ''*ALL'', NULL, driver_27)) AS driver_27,
		       COUNT (DECODE (driver_28, ''*ALL'', NULL, driver_28)) AS driver_28,
		       COUNT (DECODE (driver_29, ''*ALL'', NULL, driver_29)) AS driver_29,
		       COUNT (DECODE (driver_30, ''*ALL'', NULL, driver_30)) AS driver_30
		  FROM "TB_TAX_MATRIX",
		       "TB_TAXCODE_DETAIL" "TB_TAXCODE_DETAIL_A",
		       "TB_TAXCODE_DETAIL" "TB_TAXCODE_DETAIL_B"
		 WHERE (TB_TAX_MATRIX.then_taxcode_detail_id = tb_taxcode_detail_a.taxcode_detail_id(+))
		   AND (TB_TAX_MATRIX.else_taxcode_detail_id = tb_taxcode_detail_b.taxcode_detail_id(+))
		   AND ( "TB_TAX_MATRIX"."EFFECTIVE_DATE" <= sysdate )
		   AND ( "TB_TAX_MATRIX"."EXPIRATION_DATE" >= sysdate ) '||p_where_clause_token||'
	 ) WHERE 1=1 ';
END;
/

SHOW ERRORS;

CREATE OR REPLACE PROCEDURE Sp_Tax_Partners_Extract
     (gl_batch IN NUMBER,
      file_name IN VARCHAR2,
      execution_mode IN NUMBER,
      return_code IN OUT NUMBER )
IS
   fHandle UTL_FILE.FILE_TYPE;
   sql_stmt VARCHAR2(20000) := '';
   pl_line VARCHAR2 (1000);
   vc_total_offset VARCHAR2(17);
   vc_total_row VARCHAR2(7);
   vc_total_credit VARCHAR2(17);
   vc_total_debit VARCHAR2(17);
   end_date DATE;
   vd_end_date DATE;
   cur_counter NUMBER := 100;
   vn_row_count  NUMBER :=0;
   sys_date  DATE;
   vn_gl_batch_id NUMBER := 0;
   vc_gl_file   VARCHAR2(128);
   vd_gl_through_date DATE;
   vc_gl_updater VARCHAR2(128);
   vc_category  VARCHAR2(100);
   vc_groups  VARCHAR2(100);
   vn_file_size NUMBER :=0;
   vc_tp_export_path VARCHAR2(128);

--CURSOR cur_tp_returns IS SELECT *  FROM TB_TMP_TP_RETURNS;
total_records NUMBER :=0;
Total_tax     NUMBER :=0.00;

BEGIN

-- end_date := TO_DATE(where_clause,'mm/dd/yyyy');
 vd_end_date := SYSDATE;
 vc_tp_export_path := 'C:\Clients\Aetna\Tax_Partner\';
 /* Truncate the gl_interface table for fresh data */
--  EXECUTE IMMEDIATE 'truncate table TB_TMP_TP_RETURNS';
-- 
--  INSERT INTO TB_TMP_TP_RETURNS
--  SELECT 1 Sequence_no,
--    	   R.gl_company_nbr EntityID,
--   	   DECODE(H.NEW_GEO_CODE,NULL,J.geocode,H.NEW_GEO_CODE) Geocode,
--   	   NVL(R.TAXCODE_STATE_CODE,'')||NVL(R.TAXCODE_TYPE_CODE,'')||NVL(R.TAXCODE_CODE,'') TAXCODE,
--   	   R.gl_line_itm_dist_amt gl_line_itm_dist_amt,
-- 	   R.TB_CALC_TAX_AMT        TB_CALC_TAX_AMT,
--   	   R.combined_use_rate,
--   	   R.state_use_rate state_use_rate,
--   	   R.county_use_rate ,
--   	   R.county_local_use_rate,
--   	   R.city_use_rate,
--   	   R.city_local_use_rate,
--   	   'Consumer Use',
-- 	   H.CATEGORY,
-- 	   H.GROUPS,
-- 	   R.user_text_12
--  FROM TB_GL_REPORT_LOG R, TB_JURISDICTION J, TB_TAXCODE_DETAIL C, CCH_CUSTOM_CODE H
--  WHERE R.GL_EXTRACT_BATCH_NO = gl_batch
--  AND R.TAXCODE_STATE_CODE (+) = C.TAXCODE_STATE_CODE
--  AND R.TAXCODE_TYPE_CODE (+) = C.TAXCODE_TYPE_CODE
--  AND R.TAXCODE_CODE (+) = C.TAXCODE_CODE
--  AND R.JURISDICTION_ID = J.JURISDICTION_ID
--  AND R.TAXCODE_STATE_CODE = H.TAXCODE_STATE_CODE (+)
--  AND R.TAXCODE_TYPE_CODE = H.TAXCODE_TYPE_CODE (+)
--  AND R.TAXCODE_CODE = H.TAXCODE_CODE (+)
--  AND R.TAXCODE_TYPE_CODE = 'T';
-- 
--  SELECT CATEGORY,GROUPS
--  INTO vc_category,vc_groups
--  FROM CCH_CUSTOM_CODE H
--  WHERE H.TAXCODE_CODE = 'GENERAL';
-- 
--  UPDATE TB_TMP_TP_RETURNS
--  SET TAX_CATEGORY = vc_category,TAX_GROUP = vc_groups
--  WHERE taxcode NOT IN
--       (SELECT taxcode_code FROM CCH_CUSTOM_CODE);
-- 
-- -- UPDATE  TB_TMP_TP_RETURNS
-- -- SET tax_type = ''
-- -- WHERE SUBSTR(geocode,1,1) = '0' ;
-- 
-- 
--  COMMIT;
-- 
-- IF execution_mode = 1 THEN
-- 
--    SELECT vc01,ts01, update_user_id
--    INTO vc_gl_file, vd_gl_through_date, vc_gl_updater
--    FROM TB_BATCH
--    WHERE batch_type_code = 'GE'
--    AND nu01 = gl_batch;
-- 
-- /*  SELECT COUNT(*), ROUND(SUM(R.TB_CALC_TAX_AMT),2)
--   INTO total_records,Total_tax
--   FROM TB_GL_REPORT_LOG R, TB_TAX_CODE C
--   WHERE R.GL_EXTRACT_BATCH_NO = gl_batch
--   AND SUBSTR(R.TAX_QUES_cd,3,1) = 'T'
--   AND R.TAX_QUES_CD = C.taxcode_key; */
-- 
-- 
--   fHandle := UTL_FILE.FOPEN('C:\Clients\Aetna\Tax_Partner\',file_name,'w');
-- --  pl_line := '1AUSHC-MF-Sts-TAXACCRUAL  01'||TO_CHAR(end_date,'mmddyyyy')||TO_CHAR(SYSDATE,'mmddyyyy')||'01USE TAX ACCRUAL          '||RPAD(' ',218,' ');
--   pl_line := 'SEQUENCE_NO|ENTITYID|GEOCODE|TAXCODE|GL_LINE_ITEM_DIST_AMT|TAX_AMT|COMBINED_USE_RATE|STATE_USE_RATE|COUNTY_USE_RATE|COUNTY_LOCAL_USE_RATE|CITY_USE_RATE|CITY_LOCAL_USE_RATE|TAX_TYPE|TAX_CATEGORY|TAX_GROUP|VAT_CODE';
-- --  pl_line := '000001|200312|'||TO_CHAR(total_records)||'|'||TO_CHAR(Total_tax);
--   SELECT VSIZE(pl_line) INTO vn_file_size FROM dual;
--   sys.utl_file.put_line(fHandle, pl_line);
-- 
--   FOR tp_rec IN cur_tp_returns LOOP
-- 
--     pl_line :=
--             tp_rec.SEQUENCE_NO||'|'||
--             tp_rec.ENTITYID||'|'||
--             tp_rec.GEOCODE||'|'||
--             tp_rec.TAXCODE||'|'||
--             tp_rec.GL_LINE_ITM_DIST_AMT||'|'||
-- 			tp_rec.TB_CALC_TAX_AMT||'|'||
--             tp_rec.COMBINED_USE_RATE||'|'||
--             tp_rec.STATE_USE_RATE||'|'||
--             tp_rec.COUNTY_USE_RATE||'|'||
--             tp_rec.COUNTY_LOCAL_USE_RATE||'|'||
--             tp_rec.CITY_USE_RATE||'|'||
--             tp_rec.CITY_LOCAL_USE_RATE||'|'||
-- 			tp_rec.TAX_TYPE||'|'||
-- 			tp_rec.TAX_Category||'|'||
-- 			tp_rec.TAX_GROUP||'|'||
-- 			tp_rec.VAT_CODE;
-- 
--         SELECT VSIZE(pl_line)+vn_file_size INTO vn_file_size FROM dual;
--         sys.utl_file.put_line(fHandle, pl_line );
-- 
--   END LOOP;
-- 
-- --  pl_line := '9AUSHC-MF-Sts-TAXACCRUAL  01'||vc_total_row||vc_total_debit||vc_total_credit||RPAD(' ',231,' ');
-- 
--   UTL_FILE.FCLOSE(fHandle);
-- 
-- /* Tax Partner TB_BATCH Logging Meta Data
--   -------------------------------------------------------------------------------------
--   TB_BATCH column name | VALUES/Meaning
--   -------------------------------------------------------------------------------------
--   batch_type_code		TE'
--   entry_timestamp		TP extract FILE creation datetime
--   batch_status_code		'E'
--   held_flag				0
--   vc01					TP extract FILE name AND FILE path
--   vc02					TP extract FILE name
--   vc03					Corresponding GL FILE name AND FILE path
--   ts02					Corresponding GL through DATE
--   vc04					Corresponding GL updater
--   nu01					Corresponding GL batch no
--   nu02					Total bytes FOR TP extract FILE
--   ------------------------------------------------------------------------------------- */
-- 
-- 
--   INSERT INTO TB_BATCH
--      (batch_type_code,
--       entry_timestamp,
--       batch_status_code,
--       held_flag,
-- 	  vc01,
--       vc02,
--       vc03,
--       ts02,
--       vc04,
-- 	  nu01,
--       nu02)
--     SELECT
--      'TE',
--      vd_end_date,
--      'E',
--      0,
--      vc_tp_export_path||file_name,
--      file_name,
--      vc_gl_file,
--      vd_gl_through_date,
--      vc_gl_updater,
--      gl_batch,
--      vn_file_size
--      FROM dual;
-- 
-- 	 COMMIT;
-- 
-- END IF;
-- 
--     return_code := 0;
-- 
-- EXCEPTION WHEN UTL_FILE.INVALID_PATH THEN
--    RAISE_APPLICATION_ERROR(-20100,'Invalid Path');
-- WHEN UTL_FILE.INVALID_MODE THEN
--    RAISE_APPLICATION_ERROR(-20101,'Invalid MODE');
-- WHEN UTL_FILE.INVALID_OPERATION THEN
--    RAISE_APPLICATION_ERROR(-20102,'Invalid Operation');
-- WHEN UTL_FILE.INVALID_FILEHANDLE THEN
--    RAISE_APPLICATION_ERROR(-20103,'Invalid Filehandle');
-- WHEN UTL_FILE.WRITE_ERROR THEN
--    RAISE_APPLICATION_ERROR(-20104,'WRITE Error');
-- WHEN UTL_FILE.READ_ERROR THEN
--    RAISE_APPLICATION_ERROR(-20105,'Read Error');
-- WHEN UTL_FILE.INTERNAL_ERROR THEN
--    RAISE_APPLICATION_ERROR(-20106,'Internal Error');
-- WHEN OTHERS THEN
--    return_code := SQLCODE;
--    UTL_FILE.FCLOSE(fHandle);
END;
/

SHOW ERRORS;

CREATE OR REPLACE PROCEDURE sp_tb_data_def_column_a_d (p_old_table_name IN VARCHAR2, p_old_column_name IN VARCHAR2)
AS
BEGIN
   If Lower(p_old_table_name) = 'tb_transaction_detail' Then
      DELETE
	FROM tb_driver_reference
       WHERE trans_dtl_column_name = p_old_column_name;
   End If;
END;
/

SHOW ERRORS;

CREATE OR REPLACE PROCEDURE sp_tb_jurisdiction_taxrate_a_i(
p_jurisdiction_taxrate_id IN number
--p_tb_jurisdiction_taxrate_type IN tb_jurisdiction_taxrate%ROWTYPE
)
AS
   -- Table defined variables
   v_sysdate                       tb_transaction_detail.load_timestamp%TYPE         := SYSDATE;
   v_taxtype_code                  tb_taxcode.taxtype_code%TYPE                      := NULL;
   v_override_taxtype_code         tb_taxcode.taxtype_code%TYPE                      := NULL;
   v_taxtype_used                  tb_taxcode.taxtype_code%TYPE                      := NULL;
   v_state_flag                    tb_location_matrix.state_flag%TYPE                := '0';
   v_county_flag                   tb_location_matrix.county_flag%TYPE               := '0';
   v_county_local_flag             tb_location_matrix.county_local_flag%TYPE         := '0';
   v_city_flag                     tb_location_matrix.city_flag%TYPE                 := '0';
   v_city_local_flag               tb_location_matrix.city_local_flag%TYPE           := '0';

   -- temporary
   vn_taxable_amt                  NUMBER                                            := 0;

   l_tb_jurisdiction_taxrate_type tb_jurisdiction_taxrate%ROWTYPE;

   -- Define Exceptions
   --e_badupdate                     exception;

-- ****** Program starts ****** --------------------------------------------------------------------
BEGIN

   SELECT	*
		INTO l_tb_jurisdiction_taxrate_type
   FROM		tb_jurisdiction_taxrate
   WHERE	jurisdiction_taxrate_id = p_jurisdiction_taxrate_id;

   DECLARE
	   -- Define Transaction Detail Cursor (tb_transaction_detail)
	   CURSOR transaction_detail_cursor
	   IS
	      SELECT tb_transaction_detail.*
		FROM tb_transaction_detail
	       WHERE ( tb_transaction_detail.jurisdiction_id = l_tb_jurisdiction_taxrate_type.jurisdiction_id ) AND
		     ( l_tb_jurisdiction_taxrate_type.effective_date <= tb_transaction_detail.gl_date ) AND ( l_tb_jurisdiction_taxrate_type.expiration_date >= tb_transaction_detail.gl_date ) AND
		     ( l_tb_jurisdiction_taxrate_type.measure_type_code = tb_transaction_detail.measure_type_code ) AND
		     ( tb_transaction_detail.transaction_ind = 'S' ) AND ( tb_transaction_detail.suspend_ind = 'R' )
	      FOR UPDATE;
	      transaction_detail           transaction_detail_cursor%ROWTYPE;
   BEGIN
	   -- ****** Read and Process Transactions ****** -----------------------------------------
	   OPEN transaction_detail_cursor;
	   LOOP
	      FETCH transaction_detail_cursor
	       INTO transaction_detail;
	       EXIT WHEN transaction_detail_cursor%NOTFOUND;

	      -- Get Tax Type Codes
	      SELECT taxtype_code
		INTO v_taxtype_code
		FROM tb_taxcode
	       WHERE taxcode_code = transaction_detail.taxcode_code
		 AND taxcode_type_code = transaction_detail.taxcode_type_code;

	      -- Get Location Matrix Codes if NOT applied or overriden          -- MBF01
	      IF (transaction_detail.location_matrix_id IS NOT NULL AND transaction_detail.location_matrix_id <> 0) AND
		 (transaction_detail.manual_jurisdiction_ind IS NULL) THEN
		 -- Get Override Tax Type Code -- 3351
		 -- Get 5 Tax Calc Flags -- 3411 -- MBF02
		 SELECT override_taxtype_code,
			state_flag, county_flag, county_local_flag, city_flag, city_local_flag
		   INTO v_override_taxtype_code,
			v_state_flag,
			v_county_flag,
			v_county_local_flag,
			v_city_flag,
			v_city_local_flag
		   FROM tb_location_matrix
		  WHERE tb_location_matrix.location_matrix_id = transaction_detail.location_matrix_id;
	      ELSE
		 v_override_taxtype_code := '*NO';
		 v_state_flag := '1';                           -- MBF01
		 v_county_flag := '1';                          -- MBF01
		 v_county_local_flag := '1';                    -- MBF01
		 v_city_flag := '1';                            -- MBF01
		 v_city_local_flag := '1';                      -- MBF01
	      END IF;

	      -- Determine Tax Type - Use or Sales
	      v_taxtype_used := v_override_taxtype_code;
	      IF v_taxtype_used is NULL OR TRIM(v_taxtype_used) = '' OR TRIM(v_taxtype_used) = '*NO' THEN
		 v_taxtype_used := SUBSTR(TRIM(v_taxtype_code),1,1);
		 IF v_taxtype_used is NULL or Trim(v_taxtype_used) = '' THEN
		    v_taxtype_used := 'U';
		 END IF;
	      END IF;

	      -- Populate and/or Clear Transaction Detail working fields
	      transaction_detail.state_use_amount := 0;
	      transaction_detail.state_use_tier2_amount := 0;
	      transaction_detail.state_use_tier3_amount := 0;
	      transaction_detail.county_use_amount := 0;
	      transaction_detail.county_local_use_amount := 0;
	      transaction_detail.city_use_amount := 0;
	      transaction_detail.city_local_use_amount := 0;
	      transaction_detail.tb_calc_tax_amt := 0;
	      transaction_detail.gl_extract_amt := 0;
	-- future? --      transaction_detail.taxable_amt := 0;
	-- future? --      transactoin_detail.taxtype_used := v_taxtype_used;
	      transaction_detail.update_user_id := USER;
	      transaction_detail.update_timestamp := v_sysdate;

	      -- Move Rates to parameters
	      transaction_detail.transaction_ind := 'P';
	      transaction_detail.suspend_ind := NULL;
	      transaction_detail.jurisdiction_id := l_tb_jurisdiction_taxrate_type.jurisdiction_id;
	      transaction_detail.jurisdiction_taxrate_id := l_tb_jurisdiction_taxrate_type.jurisdiction_taxrate_id;
	      transaction_detail.county_split_amount := l_tb_jurisdiction_taxrate_type.county_split_amount;
	      transaction_detail.county_maxtax_amount := l_tb_jurisdiction_taxrate_type.county_maxtax_amount;
	      transaction_detail.county_single_flag := l_tb_jurisdiction_taxrate_type.county_single_flag;
	      transaction_detail.county_default_flag := l_tb_jurisdiction_taxrate_type.county_default_flag;
	      transaction_detail.city_split_amount := l_tb_jurisdiction_taxrate_type.city_split_amount;
	      transaction_detail.city_single_flag := l_tb_jurisdiction_taxrate_type.city_single_flag;
	      transaction_detail.city_default_flag := l_tb_jurisdiction_taxrate_type.city_default_flag;

	      IF v_taxtype_used = 'U' THEN
		 -- Use Tax Rates
		 transaction_detail.state_use_rate := l_tb_jurisdiction_taxrate_type.state_use_rate;
		 transaction_detail.state_use_tier2_rate := l_tb_jurisdiction_taxrate_type.state_use_tier2_rate;
		 transaction_detail.state_use_tier3_rate := l_tb_jurisdiction_taxrate_type.state_use_tier3_rate;
		 transaction_detail.state_split_amount := l_tb_jurisdiction_taxrate_type.state_split_amount;
		 transaction_detail.state_tier2_min_amount := l_tb_jurisdiction_taxrate_type.state_tier2_min_amount;
		 transaction_detail.state_tier2_max_amount := l_tb_jurisdiction_taxrate_type.state_tier2_max_amount;
		 transaction_detail.state_maxtax_amount := l_tb_jurisdiction_taxrate_type.state_maxtax_amount;
		 transaction_detail.county_use_rate := l_tb_jurisdiction_taxrate_type.county_use_rate;
		 transaction_detail.county_local_use_rate := l_tb_jurisdiction_taxrate_type.county_local_use_rate;
		 transaction_detail.city_use_rate := l_tb_jurisdiction_taxrate_type.city_use_rate;
		 transaction_detail.city_local_use_rate := l_tb_jurisdiction_taxrate_type.city_local_use_rate;
		 transaction_detail.city_split_use_rate := l_tb_jurisdiction_taxrate_type.city_split_use_rate;
	      ELSIF v_taxtype_used = 'S' THEN
		 -- Sales Tax Rates
		 transaction_detail.state_use_rate := l_tb_jurisdiction_taxrate_type.state_sales_rate;
		 transaction_detail.state_use_tier2_rate := 0;
		 transaction_detail.state_use_tier3_rate := 0;
		 transaction_detail.state_split_amount := 0;
		 transaction_detail.state_tier2_min_amount := 0;
		 transaction_detail.state_tier2_max_amount := 999999999;
		 transaction_detail.state_maxtax_amount := 999999999;
		 transaction_detail.county_use_rate := l_tb_jurisdiction_taxrate_type.county_sales_rate;
		 transaction_detail.county_local_use_rate := l_tb_jurisdiction_taxrate_type.county_local_sales_rate;
		 transaction_detail.city_use_rate := l_tb_jurisdiction_taxrate_type.city_sales_rate;
		 transaction_detail.city_local_use_rate := l_tb_jurisdiction_taxrate_type.city_local_sales_rate;
		 transaction_detail.city_split_use_rate := l_tb_jurisdiction_taxrate_type.city_split_sales_rate;
	      ELSE
		 -- Unknown!
		 transaction_detail.state_use_rate := 0;
		 transaction_detail.state_use_tier2_rate := 0;
		 transaction_detail.state_use_tier3_rate := 0;
		 transaction_detail.state_split_amount := 0;
		 transaction_detail.state_tier2_min_amount := 0;
		 transaction_detail.state_tier2_max_amount := 999999999;
		 transaction_detail.state_maxtax_amount := 999999999;
		 transaction_detail.county_use_rate := 0;
		 transaction_detail.county_local_use_rate := 0;
		 transaction_detail.city_use_rate := 0;
		 transaction_detail.city_local_use_rate := 0;
		 transaction_detail.city_split_use_rate := 0;
	      END IF;

	      -- Get Jurisdiction TaxRates and calculate Tax Amounts
	      sp_calcusetax(transaction_detail.gl_line_itm_dist_amt,
			    transaction_detail.invoice_freight_amt,
			    transaction_detail.invoice_discount_amt,
			    transaction_detail.transaction_state_code,
	-- future? --             transaction_detail.import_definition_code,
			    'importdefn',
			    transaction_detail.taxcode_code,
			    v_state_flag,
			    v_county_flag,
			    v_county_local_flag,
			    v_city_flag,
			    v_city_local_flag,
			    transaction_detail.state_use_rate,
			    transaction_detail.state_use_tier2_rate,
			    transaction_detail.state_use_tier3_rate,
			    transaction_detail.state_split_amount,
			    transaction_detail.state_tier2_min_amount,
			    transaction_detail.state_tier2_max_amount,
			    transaction_detail.state_maxtax_amount,
			    transaction_detail.county_use_rate,
			    transaction_detail.county_local_use_rate,
			    transaction_detail.county_split_amount,
			    transaction_detail.county_maxtax_amount,
			    transaction_detail.county_single_flag,
			    transaction_detail.county_default_flag,
			    transaction_detail.city_use_rate,
			    transaction_detail.city_local_use_rate,
			    transaction_detail.city_split_amount,
			    transaction_detail.city_split_use_rate,
			    transaction_detail.city_single_flag,
			    transaction_detail.city_default_flag,
			    transaction_detail.state_use_amount,
			    transaction_detail.state_use_tier2_amount,
			    transaction_detail.state_use_tier3_amount,
			    transaction_detail.county_use_amount,
			    transaction_detail.county_local_use_amount,
			    transaction_detail.city_use_amount,
			    transaction_detail.city_local_use_amount,
			    transaction_detail.tb_calc_tax_amt,
	-- future? --             transaction_detail.taxable_amt);
	-- Targa? --            transaction_detail.user_number_10 );
			    vn_taxable_amt );

	      transaction_detail.combined_use_rate := transaction_detail.state_use_rate +
						      transaction_detail.county_use_rate +
						      transaction_detail.county_local_use_rate +
						      transaction_detail.city_use_rate +
						      transaction_detail.city_local_use_rate;

	      -- Check for Taxable Amount -- 3351
	      IF vn_taxable_amt  <> transaction_detail.gl_line_itm_dist_amt THEN
		 transaction_detail.gl_extract_amt := transaction_detail.gl_line_itm_dist_amt;
		 transaction_detail.gl_line_itm_dist_amt := vn_taxable_amt;
	      END IF;

	      -- Update transaction detail row
	      BEGIN
		 -- 3411 - add taxable amount and tax type used.
		 UPDATE tb_transaction_detail
		    SET gl_line_itm_dist_amt = transaction_detail.gl_line_itm_dist_amt,
			tb_calc_tax_amt = transaction_detail.tb_calc_tax_amt,
			state_use_amount = transaction_detail.state_use_amount,
			state_use_tier2_amount = transaction_detail.state_use_tier2_amount,
			state_use_tier3_amount = transaction_detail.state_use_tier3_amount,
			county_use_amount = transaction_detail.county_use_amount,
			county_local_use_amount = transaction_detail.county_local_use_amount,
			city_use_amount = transaction_detail.city_use_amount,
			city_local_use_amount = transaction_detail.city_local_use_amount,
			transaction_ind = transaction_detail.transaction_ind,
			suspend_ind = transaction_detail.suspend_ind,
			jurisdiction_id = transaction_detail.jurisdiction_id,
			jurisdiction_taxrate_id = transaction_detail.jurisdiction_taxrate_id,
			state_use_rate = transaction_detail.state_use_rate,
			state_use_tier2_rate = transaction_detail.state_use_tier2_rate,
			state_use_tier3_rate = transaction_detail.state_use_tier3_rate,
			state_split_amount = transaction_detail.state_split_amount,
			state_tier2_min_amount = transaction_detail.state_tier2_min_amount,
			state_tier2_max_amount = transaction_detail.state_tier2_max_amount,
			state_maxtax_amount = transaction_detail.state_maxtax_amount,
			county_use_rate = transaction_detail.county_use_rate,
			county_local_use_rate = transaction_detail.county_local_use_rate,
			county_split_amount = transaction_detail.county_split_amount,
			county_maxtax_amount = transaction_detail.county_maxtax_amount,
			county_single_flag = transaction_detail.county_single_flag,
			county_default_flag = transaction_detail.county_default_flag,
			city_use_rate = transaction_detail.city_use_rate,
			city_local_use_rate = transaction_detail.city_local_use_rate,
			city_split_amount = transaction_detail.city_split_amount,
			city_split_use_rate = transaction_detail.city_split_use_rate,
			city_single_flag = transaction_detail.city_single_flag,
			city_default_flag = transaction_detail.city_default_flag,
			combined_use_rate = transaction_detail.combined_use_rate,
			gl_extract_amt = transaction_detail.gl_extract_amt,
			update_user_id = transaction_detail.update_user_id,
			update_timestamp = transaction_detail.update_timestamp
		  WHERE transaction_detail_id = transaction_detail.transaction_detail_id;
		 -- Error Checking
	--         IF SQLCODE != 0 THEN
	--            RAISE e_badupdate;
	--         END IF;
	--      EXCEPTION
	--         WHEN e_badupdate THEN
	--            NULL;
	      END;
	   END LOOP;
	   CLOSE transaction_detail_cursor;
END;

--EXCEPTION
--   WHEN OTHERS THEN
--      NULL;
END;
/

SHOW ERRORS;
CREATE OR REPLACE PROCEDURE sp_tb_location_matrix_a_i(p_location_matrix_id IN tb_location_matrix.location_matrix_id%TYPE)
AS
/* ************************************************************************************************/
/* Object Type/Name: SP Trigger/After Insert - sp_tb_location_matrix_a_i                          */
/* Author:           Michael B. Fuller                                                            */
/* Date:             a long time ago                                                              */
/* Description:      A Location Matrix line has been added                                        */
/* Arguments:        None                                                                         */
/* Returns:          None                                                                         */
/* ************************************************************************************************/
/* Maintenance History:                                                                           */
/* Scan  Author       Date     Release    Description                                  Ticket #   */
/* ----- ---------- ---------- ---------- -------------------------------------------- ---------- */
/*       M. Fuller  06/02/2006 3.3.5.0    Fix processing an allocated line - Temporary 871        */
/*       M. Fuller  01/04/2007 3.3.5.1    Fix processing an allocated line - Permenant 871        */
/* MBF01 M. Fuller  08/14/2007 3.4.1.1    Add 5 Calc Tax Flags                         883        */
/* MBF01 J. Franco  12/16/2008 x.x.x.x    Convert Trigger to SP                        xxx        */
/* ************************************************************************************************/

-- Define Row Type
   l_tb_location_matrix tb_location_matrix%ROWTYPE;

-- Define Cursor Type
   TYPE cursor_type is REF CURSOR;

-- Transaction Detail Selection SQL
   vc_transaction_detail_select    VARCHAR2(2000) :=
      'SELECT tb_transaction_detail.* ' ||
        'FROM tb_tmp_location_matrix, ' ||
             'tb_transaction_detail '   ||
       'WHERE ( tb_tmp_location_matrix.location_matrix_id = ' || p_location_matrix_id || ') AND ' ||
             '( tb_tmp_location_matrix.effective_date <= tb_transaction_detail.gl_date ) AND ( tb_tmp_location_matrix.expiration_date >= tb_transaction_detail.gl_date ) AND ' ||
             '( tb_transaction_detail.transaction_ind = ''S'' ) AND ( tb_transaction_detail.suspend_ind = ''L'' ) ';
   vc_transaction_detail_where     VARCHAR2(5000) := '';
   vc_transaction_detail_update    VARCHAR2(100) := 'FOR UPDATE';
   vc_transaction_detail_stmt      VARCHAR2(7100);
   transaction_detail_cursor       cursor_type;
   transaction_detail              tb_transaction_detail%ROWTYPE;

-- Tax Matrix Selection SQL
   vc_tax_matrix_select            VARCHAR2(2000) :=
      'SELECT tb_tax_matrix.tax_matrix_id, tb_tax_matrix.relation_sign, tb_tax_matrix.relation_amount, ' ||
             'tb_tax_matrix.then_hold_code_flag, tb_tax_matrix.else_hold_code_flag, ' ||
             'tb_tax_matrix.then_taxcode_detail_id, tb_tax_matrix.else_taxcode_detail_id, ' ||
             'tb_tax_matrix.then_cch_taxcat_code, tb_tax_matrix.then_cch_group_code, tb_tax_matrix.then_cch_item_code, ' ||
             'tb_tax_matrix.else_cch_taxcat_code, tb_tax_matrix.else_cch_group_code, tb_tax_matrix.else_cch_item_code, ' ||
             'thentaxcddtl.taxcode_state_code, thentaxcddtl.taxcode_type_code, thentaxcddtl.taxcode_code, thentaxcddtl.jurisdiction_id, thentaxcddtl.measure_type_code, thentaxcddtl.taxtype_code, ' ||
             'elsetaxcddtl.taxcode_state_code, elsetaxcddtl.taxcode_type_code, elsetaxcddtl.taxcode_code, elsetaxcddtl.jurisdiction_id, elsetaxcddtl.measure_type_code, elsetaxcddtl.taxtype_code ' ||
       'FROM tb_tax_matrix, ' ||
            'tb_taxcode_detail thentaxcddtl, ' ||
            'tb_taxcode_detail elsetaxcddtl, ' ||
            'tb_transaction_detail ' ||
      'WHERE ( tb_transaction_detail.transaction_detail_id = :v_transaction_detail_id ) AND ' ||
            '( tb_tax_matrix.default_flag = ''0'' AND tb_tax_matrix.binary_weight > 0 ) AND ' ||
            '(( tb_tax_matrix.matrix_state_code = ''*ALL'' ) OR ( tb_tax_matrix.matrix_state_code = :v_transaction_state_code )) AND ' ||
            '( tb_tax_matrix.effective_date <= tb_transaction_detail.gl_date ) AND ( tb_tax_matrix.expiration_date >= tb_transaction_detail.gl_date ) AND ' ||
            '( tb_tax_matrix.then_taxcode_detail_id = thentaxcddtl.taxcode_detail_id (+)) AND ' ||
            '( tb_tax_matrix.else_taxcode_detail_id = elsetaxcddtl.taxcode_detail_id (+)) ';
   vc_tax_matrix_where             VARCHAR2(6000) := '';
   vc_tax_matrix_orderby           VARCHAR2(1000) :=
      'ORDER BY tb_tax_matrix.binary_weight DESC, tb_tax_matrix.significant_digits DESC, tb_tax_matrix.effective_date DESC';
   vc_tax_matrix_stmt              VARCHAR2 (9000);
   tax_matrix_cursor               cursor_type;

-- Tax Matrix Record TYPE
   /*TYPE tax_matrix_record is RECORD (
      tax_matrix_id                tb_tax_matrix.tax_matrix_id%TYPE,
      relation_sign                tb_tax_matrix.relation_sign%TYPE,
      relation_amount              tb_tax_matrix.relation_amount%TYPE,
      then_hold_code_flag          tb_tax_matrix.then_hold_code_flag%TYPE,
      else_hold_code_flag          tb_tax_matrix.else_hold_code_flag%TYPE,
      then_taxcode_detail_id       tb_bcp_transactions.taxcode_detail_id%TYPE,
      else_taxcode_detail_id       tb_bcp_transactions.taxcode_detail_id%TYPE,
      then_cch_taxcat_code         tb_tax_matrix.then_cch_taxcat_code%TYPE,
      then_cch_group_code          tb_tax_matrix.then_cch_group_code%TYPE,
      then_cch_item_code           tb_tax_matrix.then_cch_item_code%TYPE,
      else_cch_taxcat_code         tb_tax_matrix.else_cch_taxcat_code%TYPE,
      else_cch_group_code          tb_tax_matrix.else_cch_group_code%TYPE,
      else_cch_item_code           tb_tax_matrix.else_cch_item_code%TYPE,
      then_taxcode_state_code      tb_bcp_transactions.taxcode_state_code%TYPE,
      then_taxcode_type_code       tb_bcp_transactions.taxcode_type_code%TYPE,
      then_taxcode_code            tb_bcp_transactions.taxcode_code%TYPE,
      then_jurisdiction_id         tb_bcp_transactions.jurisdiction_id%TYPE,
      then_measure_type_code       tb_bcp_transactions.measure_type_code%TYPE,
      then_taxtype_code            tb_taxcode.taxtype_code%TYPE,
      else_taxcode_state_code      tb_bcp_transactions.taxcode_state_code%TYPE,
      else_taxcode_type_code       tb_bcp_transactions.taxcode_type_code%TYPE,
      else_taxcode_code            tb_bcp_transactions.taxcode_code%TYPE,
      else_jurisdiction_id         tb_bcp_transactions.jurisdiction_id%TYPE,
      else_measure_type_code       tb_bcp_transactions.measure_type_code%TYPE,
      else_taxtype_code            tb_taxcode.taxtype_code%TYPE );*/
   tax_matrix                      matrix_record_pkg.tax_matrix_record;

-- Table defined variables
   v_hold_code_flag                tb_tax_matrix.then_hold_code_flag%TYPE            := NULL;
   v_less_hold_code_flag           tb_tax_matrix.then_hold_code_flag%TYPE            := NULL;
   v_less_taxcode_detail_id        tb_tax_matrix.then_taxcode_detail_id%TYPE         := NULL;
   v_less_cch_taxcat_code          tb_tax_matrix.then_cch_taxcat_code%TYPE           := NULL;
   v_less_cch_group_code           tb_tax_matrix.then_cch_group_code%TYPE            := NULL;
   v_equal_hold_code_flag          tb_tax_matrix.then_hold_code_flag%TYPE            := NULL;
   v_equal_taxcode_detail_id       tb_tax_matrix.then_taxcode_detail_id%TYPE         := NULL;
   v_equal_cch_taxcat_code         tb_tax_matrix.then_cch_taxcat_code%TYPE           := NULL;
   v_equal_cch_group_code          tb_tax_matrix.then_cch_group_code%TYPE            := NULL;
   v_greater_hold_code_flag        tb_tax_matrix.then_hold_code_flag%TYPE            := NULL;
   v_greater_taxcode_detail_id     tb_tax_matrix.then_taxcode_detail_id%TYPE         := NULL;
   v_greater_cch_taxcat_code       tb_tax_matrix.then_cch_taxcat_code%TYPE           := NULL;
   v_greater_cch_group_code        tb_tax_matrix.then_cch_group_code%TYPE            := NULL;
   v_jurisdiction_id               tb_jurisdiction.jurisdiction_id%TYPE              := NULL;
   v_less_taxcode_state_code       tb_taxcode_detail.taxcode_state_code%TYPE         := NULL;
   v_less_taxcode_type_code        tb_taxcode_detail.taxcode_type_code%TYPE          := NULL;
   v_less_taxcode_code             tb_taxcode_detail.taxcode_code%TYPE               := NULL;
   v_less_jurisdiction_id          tb_taxcode_detail.jurisdiction_id%TYPE            := NULL;
   v_less_measure_type_code        tb_taxcode_detail.measure_type_code%TYPE          := NULL;
   v_less_taxtype_code             tb_taxcode_detail.taxtype_code%TYPE               := NULL;
   v_equal_taxcode_state_code      tb_taxcode_detail.taxcode_state_code%TYPE         := NULL;
   v_equal_taxcode_type_code       tb_taxcode_detail.taxcode_type_code%TYPE          := NULL;
   v_equal_taxcode_code            tb_taxcode_detail.taxcode_code%TYPE               := NULL;
   v_equal_jurisdiction_id         tb_taxcode_detail.jurisdiction_id%TYPE            := NULL;
   v_equal_measure_type_code       tb_taxcode_detail.measure_type_code%TYPE          := NULL;
   v_equal_taxtype_code            tb_taxcode_detail.taxtype_code%TYPE               := NULL;
   v_greater_taxcode_state_code    tb_taxcode_detail.taxcode_state_code%TYPE         := NULL;
   v_greater_taxcode_type_code     tb_taxcode_detail.taxcode_type_code%TYPE          := NULL;
   v_greater_taxcode_code          tb_taxcode_detail.taxcode_code%TYPE               := NULL;
   v_greater_jurisdiction_id       tb_taxcode_detail.jurisdiction_id%TYPE            := NULL;
   v_greater_measure_type_code     tb_taxcode_detail.measure_type_code%TYPE          := NULL;
   v_greater_taxtype_code          tb_taxcode_detail.taxtype_code%TYPE               := NULL;
   v_transaction_detail_id         tb_transaction_detail.transaction_detail_id%TYPE  := NULL;
   v_transaction_state_code        tb_transaction_detail.transaction_state_code%TYPE := NULL;
   v_override_jurisdiction_id      tb_transaction_detail.jurisdiction_id%TYPE        := NULL;
   v_sysdate                       tb_transaction_detail.load_timestamp%TYPE         := SYSDATE;
   v_taxtype_code                  tb_taxcode.taxtype_code%TYPE                      := NULL;
   v_taxtype_used                  tb_taxcode.taxtype_code%TYPE                      := NULL;

-- Program defined variables
   vn_hold_trans_amount            NUMBER                                            := 0;
   vn_fetch_rows                   NUMBER                                            := 0;
   vi_error_count                  INTEGER                                           := 0;
   vn_actual_rowno                 NUMBER                                            := 0;
   vc_state_driver_flag            CHAR(1)                                           := '0';
-- temporary
   vn_taxable_amt                  NUMBER                                            := 0;

-- Define Location Driver Names Cursor (tb_driver_names)
   /*CURSOR location_driver_cursor
   IS
        SELECT driver.trans_dtl_column_name, driver.matrix_column_name, driver.null_driver_flag, driver.wildcard_flag,
               datadefcol.desc_column_name, datadefcol.data_type
          FROM tb_driver_names driver,
               (SELECT *
                  FROM tb_data_def_column
                 WHERE table_name = 'TB_TRANSACTION_DETAIL') datadefcol
         WHERE driver.driver_names_code = 'L' AND
               driver.trans_dtl_column_name = datadefcol.column_name
      ORDER BY driver.driver_id;
      location_driver              location_driver_cursor%ROWTYPE;*/

-- Define Tax Driver Names Cursor (tb_driver_names)
   /*CURSOR tax_driver_cursor
   IS
        SELECT driver.trans_dtl_column_name, driver.matrix_column_name, driver.null_driver_flag, driver.wildcard_flag, driver.range_flag, to_number_flag,
               datadefcol.desc_column_name, datadefcol.data_type
          FROM tb_driver_names driver,
               (SELECT *
                  FROM tb_data_def_column
                 WHERE table_name = 'TB_TRANSACTION_DETAIL') datadefcol
         WHERE driver.driver_names_code = 'T' AND
               driver.trans_dtl_column_name = datadefcol.column_name
      ORDER BY driver.driver_id;
      tax_driver                   tax_driver_cursor%ROWTYPE;*/

-- Define Exceptions
   e_badread                       exception;
   e_badupdate                     exception;

-- ****** Program starts ****** --------------------------------------------------------------------
BEGIN

-- Populate row type with location matrix record
SELECT tb_location_matrix.*
 INTO  l_tb_location_matrix
 FROM  tb_location_matrix
 WHERE location_matrix_id = p_location_matrix_id;

IF l_tb_location_matrix.default_flag <> '1' AND l_tb_location_matrix.binary_weight > 0 THEN

   -- ***** Create Dynamic WHERE Clause for Transaction Detail SQL ***** --
   -- Location Matrix Drivers
   /*OPEN location_driver_cursor;
   LOOP
      FETCH location_driver_cursor INTO location_driver;
      EXIT WHEN location_driver_cursor%NOTFOUND;
      IF location_driver.null_driver_flag = '1' THEN
         IF location_driver.wildcard_flag = '1' THEN
            IF UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'CHAR' OR
               UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'VARC' THEN
                  vc_transaction_detail_where := vc_transaction_detail_where || 'AND ' ||
                  '((tb_transaction_detail.' || location_driver.trans_dtl_column_name || ' IS NULL AND ( tb_tmp_location_matrix.' || location_driver.matrix_column_name || ' = ''*ALL'' OR tb_tmp_location_matrix.' || location_driver.matrix_column_name || ' = ''*NULL'' )) OR ' ||
                  '(tb_transaction_detail.' || location_driver.trans_dtl_column_name || ' IS NOT NULL AND ( tb_tmp_location_matrix.' || location_driver.matrix_column_name || ' = ''*ALL'' OR UPPER(tb_transaction_detail.' || location_driver.trans_dtl_column_name || ') LIKE UPPER(tb_tmp_location_matrix.' || location_driver.matrix_column_name || ') ))) ';
            END IF;
         ELSE
            IF UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'CHAR' OR
               UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'VARC' THEN
                  vc_transaction_detail_where := vc_transaction_detail_where || 'AND ' ||
                  '((tb_transaction_detail.' || location_driver.trans_dtl_column_name || ' IS NULL AND ( tb_tmp_location_matrix.' || location_driver.matrix_column_name || ' = ''*ALL'' OR tb_tmp_location_matrix.' || location_driver.matrix_column_name || ' = ''*NULL'' )) OR ' ||
                  '(tb_transaction_detail.' || location_driver.trans_dtl_column_name || ' IS NOT NULL AND ( tb_tmp_location_matrix.' || location_driver.matrix_column_name || ' = ''*ALL'' OR UPPER(tb_transaction_detail.' || location_driver.trans_dtl_column_name || ') = UPPER(tb_tmp_location_matrix.' || location_driver.matrix_column_name || ') ))) ';
            END IF;
         END IF;
      ELSE
         IF location_driver.wildcard_flag = '1' THEN
            IF UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'CHAR' OR
               UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'VARC' THEN
                  vc_transaction_detail_where := vc_transaction_detail_where || 'AND ' ||
                  '( tb_tmp_location_matrix.' || location_driver.matrix_column_name || ' = ''*ALL'' OR UPPER(tb_transaction_detail.' || location_driver.trans_dtl_column_name || ') LIKE UPPER(tb_tmp_location_matrix.' || location_driver.matrix_column_name || ') ) ';
            END IF;
         ELSE
            IF UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'CHAR' OR
               UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'VARC' THEN
                  vc_transaction_detail_where := vc_transaction_detail_where || 'AND ' ||
                  '( tb_tmp_location_matrix.' || location_driver.matrix_column_name || ' = ''*ALL'' OR UPPER(tb_transaction_detail.' || location_driver.trans_dtl_column_name || ') = UPPER(tb_tmp_location_matrix.' || location_driver.matrix_column_name || ') ) ';
            END IF;
         END IF;
      END IF;
   END LOOP;
   CLOSE location_driver_cursor;*/

   -- New call to sp_gen_location_driver for MidTier Cleanup
   sp_gen_location_driver (
	p_generate_driver_reference	=> 'N',
	p_an_batch_id			=> NULL,
	p_transaction_table_name	=> 'tb_transaction_detail',
	p_location_table_name		=> 'tb_tmp_location_matrix',
	p_vc_location_matrix_where	=> vc_transaction_detail_where);

   -- If no drivers found raise error, else create transaction detail sql statement
   IF vc_transaction_detail_where IS NULL THEN
      NULL;
   ELSE
      vc_transaction_detail_stmt := vc_transaction_detail_select || vc_transaction_detail_where || vc_transaction_detail_update;
   END IF;

   -- *******************************************************************************************************************************************
   -- NEW BATCH PROCESS CODE --------------------------------------------------------------------------------------------------------------------
   -- ***** Create Dynamic WHERE Clause for Transaction Detail SQL ***** --
   -- Tax Matrix Drivers
   /*OPEN tax_driver_cursor;
   LOOP
      FETCH tax_driver_cursor INTO tax_driver;
      EXIT WHEN tax_driver_cursor%NOTFOUND;
      IF tax_driver.range_flag = '1' THEN
         IF tax_driver.to_number_flag = '1' THEN
            IF tax_driver.null_driver_flag = '1' THEN
               -- Range and ToNumber and NULL
               IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                  UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                  vc_tax_matrix_where := vc_tax_matrix_where || 'AND ( ' ||
                  '( tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU = ''*NA'' AND ' ||
                     '( tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NULL AND ( tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*NULL'' )) OR ' ||
                     '( tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND ( tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR DECODE(isnumber(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || '),1,to_char(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ',''00000000000000000000.00000000000000000000''),UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ')) = DECODE(isnumber(tb_tax_matrix.' || tax_driver.matrix_column_name || '),1,to_char(tb_tax_matrix.' || tax_driver.matrix_column_name || ',''00000000000000000000.00000000000000000000''),UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') ))) OR ' ||
                  '( tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU <> ''*NA'' AND ' ||
                     '( tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND (DECODE(isnumber(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || '),1,to_char(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ',''00000000000000000000.00000000000000000000''),UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ')) >= DECODE(isnumber(tb_tax_matrix.' || tax_driver.matrix_column_name || '),1,to_char(tb_tax_matrix.' || tax_driver.matrix_column_name || ',''00000000000000000000.00000000000000000000''),UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') ) AND ' ||
                                                                                            'DECODE(isnumber(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || '),1,to_char(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ',''00000000000000000000.00000000000000000000''),UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ')) <= DECODE(isnumber(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU ),1,to_char(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU,''00000000000000000000.00000000000000000000''),UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU)) )))) ';
               END IF;
            ELSE
               -- Range and ToNumber and NOT NULL
               IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                  UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                  vc_tax_matrix_where := vc_tax_matrix_where || 'AND ( ' ||
                  '( tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU = ''*NA'' AND (tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR DECODE(isnumber(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || '),1,to_char(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ',''00000000000000000000.00000000000000000000''),UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ')) = DECODE(isnumber(tb_tax_matrix.' || tax_driver.matrix_column_name || '),1,to_char(tb_tax_matrix.' || tax_driver.matrix_column_name || ',''00000000000000000000.00000000000000000000''),UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ')))) OR ' ||
                  '( tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU <> ''*NA'' AND (DECODE(isnumber(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || '),1,to_char(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ',''00000000000000000000.00000000000000000000''),UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ')) >= DECODE(isnumber(tb_tax_matrix.' || tax_driver.matrix_column_name || '),1,to_char(tb_tax_matrix.' || tax_driver.matrix_column_name || ',''00000000000000000000.00000000000000000000''),UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ')) AND ' ||
                                                                                        'DECODE(isnumber(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || '),1,to_char(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ',''00000000000000000000.00000000000000000000''),UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ')) <= DECODE(isnumber(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU ),1,to_char(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU,''00000000000000000000.00000000000000000000''),UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU)) )) ';
               END IF;
            END IF;
         ELSE
            IF tax_driver.null_driver_flag = '1' THEN
               IF tax_driver.wildcard_flag = '1' THEN
                  -- Range and NOT ToNumber and NULL and Wildcard
                  IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                     UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                     vc_tax_matrix_where := vc_tax_matrix_where || 'AND ( ' ||
                     '( tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU = ''*NA'' AND ' ||
                        '( tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NULL AND (tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*NULL'' )) OR ' ||
                        '( tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND (tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') LIKE UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ')))) OR ' ||
                     '( tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU <> ''*NA'' AND (tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND (UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') >= UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') AND UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') <= UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU) ))) ';
                  END IF;
               ELSE
                  -- Range and NOT ToNumber and NULL and NOT Wildcard
                  IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                     UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                     vc_tax_matrix_where := vc_tax_matrix_where || 'AND ( ' ||
                     '( tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU = ''*NA'' AND ' ||
                        '( tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NULL AND (tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*NULL'' )) OR ' ||
                        '( tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND (tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') = UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ')))) OR ' ||
                     '( tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU <> ''*NA'' AND (tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND (UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') >= UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') AND UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') <= UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU) ))) ';
                  END IF;
               END IF;
            ELSE
               IF tax_driver.wildcard_flag = '1' THEN
                  -- Range and NOT ToNumber and NOT NULL and Wildcard
                  IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                     UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                     vc_tax_matrix_where := vc_tax_matrix_where || 'AND ( ' ||
                     '( tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU = ''*NA'' AND ' ||
                        '(tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') LIKE UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '))) OR ' ||
                     '( tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU <> ''*NA'' AND (UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') >= UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') AND UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') <= UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU) )) ';
                  END IF;
               ELSE
                  -- Range and NOT ToNumber and NOT NULL and NOT Wildcard
                  IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                     UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                     vc_tax_matrix_where := vc_tax_matrix_where || 'AND ( ' ||
                     '( tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU = ''*NA'' AND ' ||
                        '(tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') = UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '))) OR ' ||
                     '( tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU <> ''*NA'' AND (UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') >= UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') AND UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') <= UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU) )) ';
                  END IF;
               END IF;
            END IF;
         END IF;
      ELSE
         IF tax_driver.null_driver_flag = '1' THEN
            IF tax_driver.wildcard_flag = '1' THEN
               -- NOT Range and NULL and Wildcard
               IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                  UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                     vc_tax_matrix_where := vc_tax_matrix_where || 'AND ( ' ||
                     '( tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NULL AND (tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*NULL'' )) OR ' ||
                     '( tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND (tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') LIKE UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ')) ) ';
               END IF;
            ELSE
               -- NOT Range and NULL and NOT Wildcard
               IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                  UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                     vc_tax_matrix_where := vc_tax_matrix_where || 'AND ( ' ||
                     '( tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NULL AND (tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*NULL'' )) OR ' ||
                     '( tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND (tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') = UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ')) ) ';
               END IF;
            END IF;
         ELSE
            IF tax_driver.wildcard_flag = '1' THEN
               -- NOT Range and NOT NULL and Wildcard
               IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                  UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                     vc_tax_matrix_where := vc_tax_matrix_where || 'AND ( ' ||
                     '( tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') LIKE UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') ) ';
               END IF;
            ELSE
               -- NOT Range and NOT NULL and NOT Wildcard
               IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                  UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                  IF tax_driver.trans_dtl_column_name = 'TRANSACTION_STATE_CODE' THEN
                     IF vc_state_driver_flag <> '1' THEN
                        vc_state_driver_flag := '1';
                     END IF;
                     vc_tax_matrix_where := vc_tax_matrix_where || 'AND ( ' ||
                        '( tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR UPPER(:v_transaction_state_code) = UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ')) ';
                  ELSE
                     vc_tax_matrix_where := vc_tax_matrix_where || 'AND ( ' ||
                        '( tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') = UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') ) ';
                  END IF;
               END IF;
            END IF;
         END IF;
      END IF;
      vc_tax_matrix_where := vc_tax_matrix_where || ') ';
   END LOOP;
   CLOSE tax_driver_cursor;*/

   -- New call to sp_gen_tax_driver for MidTier Cleanup
   sp_gen_tax_driver (
	p_generate_driver_reference	=> 'N',
	p_an_batch_id			=> NULL,
	p_transaction_table_name	=> 'tb_transaction_detail',
	p_tax_table_name		=> 'tb_tax_matrix',
	p_vc_state_driver_flag		=> vc_state_driver_flag,
	p_vc_tax_matrix_where		=> vc_tax_matrix_where);

   -- NEW BATCH PROCESS CODE --------------------------------------------------------------------------------------------------------------------
   -- *******************************************************************************************************************************************

   -- If no drivers found raise error, else create location matrix sql statement
   IF vc_tax_matrix_where IS NULL THEN
      NULL;
   ELSE
      vc_tax_matrix_stmt := vc_tax_matrix_select || vc_tax_matrix_where || vc_tax_matrix_orderby;
   END IF;

   -- Insert New Location Matrix columns into temporary table --------------------------------------
   INSERT INTO tb_tmp_location_matrix (
      location_matrix_id,
      driver_01, driver_01_desc,
      driver_02, driver_02_desc,
      driver_03, driver_03_desc,
      driver_04, driver_04_desc,
      driver_05, driver_05_desc,
      driver_06, driver_06_desc,
      driver_07, driver_07_desc,
      driver_08, driver_08_desc,
      driver_09, driver_09_desc,
      driver_10, driver_10_desc,
      binary_weight, significant_digits,
      default_flag, default_binary_weight, default_significant_digits,
      effective_date, expiration_date,
      jurisdiction_id, state, comments,
      last_used_timestamp, update_user_id, update_timestamp )
   VALUES (
      l_tb_location_matrix.location_matrix_id,
      l_tb_location_matrix.driver_01, l_tb_location_matrix.driver_01_desc,
      l_tb_location_matrix.driver_02, l_tb_location_matrix.driver_02_desc,
      l_tb_location_matrix.driver_03, l_tb_location_matrix.driver_03_desc,
      l_tb_location_matrix.driver_04, l_tb_location_matrix.driver_04_desc,
      l_tb_location_matrix.driver_05, l_tb_location_matrix.driver_05_desc,
      l_tb_location_matrix.driver_06, l_tb_location_matrix.driver_06_desc,
      l_tb_location_matrix.driver_07, l_tb_location_matrix.driver_07_desc,
      l_tb_location_matrix.driver_08, l_tb_location_matrix.driver_08_desc,
      l_tb_location_matrix.driver_09, l_tb_location_matrix.driver_09_desc,
      l_tb_location_matrix.driver_10, l_tb_location_matrix.driver_10_desc,
      l_tb_location_matrix.binary_weight, l_tb_location_matrix.significant_digits,
      l_tb_location_matrix.default_flag, l_tb_location_matrix.default_binary_weight, l_tb_location_matrix.default_significant_digits,
      l_tb_location_matrix.effective_date, l_tb_location_matrix.expiration_date,
      l_tb_location_matrix.jurisdiction_id, l_tb_location_matrix.state, l_tb_location_matrix.comments,
      l_tb_location_matrix.last_used_timestamp, l_tb_location_matrix.update_user_id, l_tb_location_matrix.update_timestamp );

   -- ****** Read and Process Transactions ****** -----------------------------------------
   OPEN transaction_detail_cursor
    FOR vc_transaction_detail_stmt;
   LOOP
      FETCH transaction_detail_cursor
       INTO transaction_detail;
       EXIT WHEN transaction_detail_cursor%NOTFOUND;

      -- Populate and/or Transaction Detail Clear working fields
      transaction_detail.transaction_ind := NULL;
      transaction_detail.suspend_ind := NULL;
      transaction_detail.location_matrix_id := l_tb_location_matrix.location_matrix_id;
      transaction_detail.jurisdiction_id := l_tb_location_matrix.jurisdiction_id;
      transaction_detail.update_user_id := USER;
      transaction_detail.update_timestamp := v_sysdate;

      -- Continue if no Matrix ID
      IF transaction_detail.tax_matrix_id IS NULL OR transaction_detail.tax_matrix_id = 0 THEN
         v_transaction_detail_id := transaction_detail.transaction_detail_id;
         v_transaction_state_code := transaction_detail.transaction_state_code;

         -- Does Transaction Record have a valid State?
         IF v_transaction_state_code IS NULL THEN
            vn_fetch_rows := 0;
         ELSE
            SELECT count(*)
              INTO vn_fetch_rows
              FROM tb_taxcode_state
             WHERE taxcode_state_code = v_transaction_state_code;
         END IF;
         IF vn_fetch_rows = 0 THEN
            -- Get State from new Jurisdiction
            v_transaction_state_code := NULL;
            BEGIN
               SELECT state
                 INTO v_transaction_state_code
                 FROM tb_jurisdiction
                WHERE jurisdiction_id = l_tb_location_matrix.jurisdiction_id;
               IF SQLCODE != 0 THEN
                  RAISE e_badread;
               END IF;
            EXCEPTION
               WHEN e_badread THEN
                  v_transaction_state_code := NULL;
               WHEN NO_DATA_FOUND THEN
                  v_transaction_state_code := NULL;
            END;
            -- Jurisdiction Line Found
            IF v_transaction_state_code IS NOT NULL THEN
               IF transaction_detail.transaction_state_code IS NULL THEN
                  transaction_detail.auto_transaction_state_code := '*NULL';
               ELSE
                  transaction_detail.auto_transaction_state_code := transaction_detail.transaction_state_code;
               END IF;
               transaction_detail.transaction_state_code := v_transaction_state_code;
            ELSE
               transaction_detail.transaction_ind := 'S';
               transaction_detail.suspend_ind := 'L';
            END IF;
         END IF;

         -- Continue if not Suspended
         IF transaction_detail.transaction_ind IS NULL OR transaction_detail.transaction_ind <> 'S' THEN
            -- Search Tax Matrix for matches
            BEGIN
               IF vc_state_driver_flag = '1' THEN
                   OPEN tax_matrix_cursor
                    FOR vc_tax_matrix_stmt
                  USING v_transaction_detail_id, v_transaction_state_code, v_transaction_state_code;
                  FETCH tax_matrix_cursor
                   INTO tax_matrix;
               ELSE
                   OPEN tax_matrix_cursor
                    FOR vc_tax_matrix_stmt
                  USING v_transaction_detail_id, v_transaction_state_code;
                  FETCH tax_matrix_cursor
                   INTO tax_matrix;
               END IF;

               -- Rows found?
               IF tax_matrix_cursor%FOUND THEN
                  vn_fetch_rows := tax_matrix_cursor%ROWCOUNT;
               ELSE
                  vn_fetch_rows := 0;
               END IF;
               CLOSE tax_matrix_cursor;
               IF SQLCODE != 0 THEN
                  RAISE e_badread;
               END IF;
            EXCEPTION
               WHEN e_badread THEN
                  vn_fetch_rows := 0;
            END;

            -- Tax Matrix Line Found
            IF vn_fetch_rows > 0 THEN
               transaction_detail.tax_matrix_id := tax_matrix.tax_matrix_id;
               -- Determine "Then" or "Else" Results
               IF tax_matrix.relation_sign = 'na' THEN
                  tax_matrix.relation_amount := 0;
                  v_less_hold_code_flag := tax_matrix.then_hold_code_flag;
                  v_less_taxcode_detail_id := tax_matrix.then_taxcode_detail_id;
                  v_less_taxcode_state_code := tax_matrix.then_taxcode_state_code;
                  v_less_taxcode_type_code := tax_matrix.then_taxcode_type_code;
                  v_less_taxcode_code := tax_matrix.then_taxcode_code;
                  v_less_jurisdiction_id := tax_matrix.then_jurisdiction_id;
                  v_less_measure_type_code := tax_matrix.then_measure_type_code;
                  v_less_taxtype_code := tax_matrix.then_taxtype_code;
                  v_less_cch_taxcat_code := tax_matrix.then_cch_taxcat_code;
                  v_less_cch_group_code := tax_matrix.then_cch_group_code;
                  -- v_less_cch_item_code := tax_matrix.then_cch_item_code;
                  v_equal_hold_code_flag := tax_matrix.then_hold_code_flag;
                  v_equal_taxcode_detail_id := tax_matrix.then_taxcode_detail_id;
                  v_equal_taxcode_state_code := tax_matrix.then_taxcode_state_code;
                  v_equal_taxcode_type_code := tax_matrix.then_taxcode_type_code;
                  v_equal_taxcode_code := tax_matrix.then_taxcode_code;
                  v_equal_jurisdiction_id := tax_matrix.then_jurisdiction_id;
                  v_equal_measure_type_code := tax_matrix.then_measure_type_code;
                  v_equal_taxtype_code := tax_matrix.then_taxtype_code;
                  v_equal_cch_taxcat_code := tax_matrix.then_cch_taxcat_code;
                  v_equal_cch_group_code := tax_matrix.then_cch_group_code;
                  -- v_equal_cch_item_code := tax_matrix.then_cch_item_code;
                  v_greater_hold_code_flag := tax_matrix.then_hold_code_flag;
                  v_greater_taxcode_detail_id := tax_matrix.then_taxcode_detail_id;
                  v_greater_taxcode_state_code := tax_matrix.then_taxcode_state_code;
                  v_greater_taxcode_type_code := tax_matrix.then_taxcode_type_code;
                  v_greater_taxcode_code := tax_matrix.then_taxcode_code;
                  v_greater_jurisdiction_id := tax_matrix.then_jurisdiction_id;
                  v_greater_measure_type_code := tax_matrix.then_measure_type_code;
                  v_greater_taxtype_code := tax_matrix.then_taxtype_code;
                  v_greater_cch_taxcat_code := tax_matrix.then_cch_taxcat_code;
                  v_greater_cch_group_code := tax_matrix.then_cch_group_code;
                  -- v_greater_cch_item_code := tax_matrix.then_cch_item_code;
              ELSIF tax_matrix.relation_sign = '=' THEN
                  v_less_hold_code_flag := tax_matrix.else_hold_code_flag;
                  v_less_taxcode_detail_id := tax_matrix.else_taxcode_detail_id;
                  v_less_taxcode_state_code := tax_matrix.else_taxcode_state_code;
                  v_less_taxcode_type_code := tax_matrix.else_taxcode_type_code;
                  v_less_taxcode_code := tax_matrix.else_taxcode_code;
                  v_less_jurisdiction_id := tax_matrix.else_jurisdiction_id;
                  v_less_measure_type_code := tax_matrix.else_measure_type_code;
                  v_less_taxtype_code := tax_matrix.else_taxtype_code;
                  v_less_cch_taxcat_code := tax_matrix.else_cch_taxcat_code;
                  v_less_cch_group_code := tax_matrix.else_cch_group_code;
                  -- v_less_cch_item_code := tax_matrix.else_cch_item_code;
                  v_equal_hold_code_flag := tax_matrix.then_hold_code_flag;
                  v_equal_taxcode_detail_id := tax_matrix.then_taxcode_detail_id;
                  v_equal_taxcode_state_code := tax_matrix.then_taxcode_state_code;
                  v_equal_taxcode_type_code := tax_matrix.then_taxcode_type_code;
                  v_equal_taxcode_code := tax_matrix.then_taxcode_code;
                  v_equal_jurisdiction_id := tax_matrix.then_jurisdiction_id;
                  v_equal_measure_type_code := tax_matrix.then_measure_type_code;
                  v_equal_taxtype_code := tax_matrix.then_taxtype_code;
                  v_equal_cch_taxcat_code := tax_matrix.then_cch_taxcat_code;
                  v_equal_cch_group_code := tax_matrix.then_cch_group_code;
                  -- v_equal_cch_item_code := tax_matrix.then_cch_item_code;
                  v_greater_hold_code_flag := tax_matrix.else_hold_code_flag;
                  v_greater_taxcode_detail_id := tax_matrix.else_taxcode_detail_id;
                  v_greater_taxcode_state_code := tax_matrix.else_taxcode_state_code;
                  v_greater_taxcode_type_code := tax_matrix.else_taxcode_type_code;
                  v_greater_taxcode_code := tax_matrix.else_taxcode_code;
                  v_greater_jurisdiction_id := tax_matrix.else_jurisdiction_id;
                  v_greater_measure_type_code := tax_matrix.else_measure_type_code;
                  v_greater_taxtype_code := tax_matrix.else_taxtype_code;
                  v_greater_cch_taxcat_code := tax_matrix.else_cch_taxcat_code;
                  v_greater_cch_group_code := tax_matrix.else_cch_group_code;
                  -- v_greater_cch_item_code := tax_matrix.else_cch_item_code;
               ELSIF tax_matrix.relation_sign = '<' THEN
                  v_less_hold_code_flag := tax_matrix.then_hold_code_flag;
                  v_less_taxcode_detail_id := tax_matrix.then_taxcode_detail_id;
                  v_less_taxcode_state_code := tax_matrix.then_taxcode_state_code;
                  v_less_taxcode_type_code := tax_matrix.then_taxcode_type_code;
                  v_less_taxcode_code := tax_matrix.then_taxcode_code;
                  v_less_jurisdiction_id := tax_matrix.then_jurisdiction_id;
                  v_less_measure_type_code := tax_matrix.then_measure_type_code;
                  v_less_taxtype_code := tax_matrix.then_taxtype_code;
                  v_less_cch_taxcat_code := tax_matrix.then_cch_taxcat_code;
                  v_less_cch_group_code := tax_matrix.then_cch_group_code;
                  -- v_less_cch_item_code := tax_matrix.then_cch_item_code;
                  v_equal_hold_code_flag := tax_matrix.else_hold_code_flag;
                  v_equal_taxcode_detail_id := tax_matrix.else_taxcode_detail_id;
                  v_equal_taxcode_state_code := tax_matrix.else_taxcode_state_code;
                  v_equal_taxcode_type_code := tax_matrix.else_taxcode_type_code;
                  v_equal_taxcode_code := tax_matrix.else_taxcode_code;
                  v_equal_jurisdiction_id := tax_matrix.else_jurisdiction_id;
                  v_equal_measure_type_code := tax_matrix.else_measure_type_code;
                  v_equal_taxtype_code := tax_matrix.else_taxtype_code;
                  v_equal_cch_taxcat_code := tax_matrix.else_cch_taxcat_code;
                  v_equal_cch_group_code := tax_matrix.else_cch_group_code;
                  -- v_equal_cch_item_code := tax_matrix.else_cch_item_code;
                  v_greater_hold_code_flag := tax_matrix.else_hold_code_flag;
                  v_greater_taxcode_detail_id := tax_matrix.else_taxcode_detail_id;
                  v_greater_taxcode_state_code := tax_matrix.else_taxcode_state_code;
                  v_greater_taxcode_type_code := tax_matrix.else_taxcode_type_code;
                  v_greater_taxcode_code := tax_matrix.else_taxcode_code;
                  v_greater_jurisdiction_id := tax_matrix.else_jurisdiction_id;
                  v_greater_measure_type_code := tax_matrix.else_measure_type_code;
                  v_greater_taxtype_code := tax_matrix.else_taxtype_code;
                  v_greater_cch_taxcat_code := tax_matrix.else_cch_taxcat_code;
                  v_greater_cch_group_code := tax_matrix.else_cch_group_code;
                  -- v_greater_cch_item_code := tax_matrix.else_cch_item_code;
               ELSIF tax_matrix.relation_sign = '<=' THEN
                  v_less_hold_code_flag := tax_matrix.then_hold_code_flag;
                  v_less_taxcode_detail_id := tax_matrix.then_taxcode_detail_id;
                  v_less_taxcode_state_code := tax_matrix.then_taxcode_state_code;
                  v_less_taxcode_type_code := tax_matrix.then_taxcode_type_code;
                  v_less_taxcode_code := tax_matrix.then_taxcode_code;
                  v_less_jurisdiction_id := tax_matrix.then_jurisdiction_id;
                  v_less_measure_type_code := tax_matrix.then_measure_type_code;
                  v_less_taxtype_code := tax_matrix.then_taxtype_code;
                  v_less_cch_taxcat_code := tax_matrix.then_cch_taxcat_code;
                  v_less_cch_group_code := tax_matrix.then_cch_group_code;
                  -- v_less_cch_item_code := tax_matrix.then_cch_item_code;
                  v_equal_hold_code_flag := tax_matrix.then_hold_code_flag;
                  v_equal_taxcode_detail_id := tax_matrix.then_taxcode_detail_id;
                  v_equal_taxcode_state_code := tax_matrix.then_taxcode_state_code;
                  v_equal_taxcode_type_code := tax_matrix.then_taxcode_type_code;
                  v_equal_taxcode_code := tax_matrix.then_taxcode_code;
                  v_equal_jurisdiction_id := tax_matrix.then_jurisdiction_id;
                  v_equal_measure_type_code := tax_matrix.then_measure_type_code;
                  v_equal_taxtype_code := tax_matrix.then_taxtype_code;
                  v_equal_cch_taxcat_code := tax_matrix.then_cch_taxcat_code;
                  v_equal_cch_group_code := tax_matrix.then_cch_group_code;
                  -- v_equal_cch_item_code := tax_matrix.then_cch_item_code;
                  v_greater_hold_code_flag := tax_matrix.else_hold_code_flag;
                  v_greater_taxcode_detail_id := tax_matrix.else_taxcode_detail_id;
                  v_greater_taxcode_state_code := tax_matrix.else_taxcode_state_code;
                  v_greater_taxcode_type_code := tax_matrix.else_taxcode_type_code;
                  v_greater_taxcode_code := tax_matrix.else_taxcode_code;
                  v_greater_jurisdiction_id := tax_matrix.else_jurisdiction_id;
                  v_greater_measure_type_code := tax_matrix.else_measure_type_code;
                  v_greater_taxtype_code := tax_matrix.else_taxtype_code;
                  v_greater_cch_taxcat_code := tax_matrix.else_cch_taxcat_code;
                  v_greater_cch_group_code := tax_matrix.else_cch_group_code;
                  -- v_greater_cch_item_code := tax_matrix.else_cch_item_code;
               ELSIF tax_matrix.relation_sign = '>' THEN
                  v_less_hold_code_flag := tax_matrix.else_hold_code_flag;
                  v_less_taxcode_detail_id := tax_matrix.else_taxcode_detail_id;
                  v_less_taxcode_state_code := tax_matrix.else_taxcode_state_code;
                  v_less_taxcode_type_code := tax_matrix.else_taxcode_type_code;
                  v_less_taxcode_code := tax_matrix.else_taxcode_code;
                  v_less_jurisdiction_id := tax_matrix.else_jurisdiction_id;
                  v_less_measure_type_code := tax_matrix.else_measure_type_code;
                  v_less_taxtype_code := tax_matrix.else_taxtype_code;
                  v_less_cch_taxcat_code := tax_matrix.else_cch_taxcat_code;
                  v_less_cch_group_code := tax_matrix.else_cch_group_code;
                  -- v_less_cch_item_code := tax_matrix.else_cch_item_code;
                  v_equal_hold_code_flag := tax_matrix.else_hold_code_flag;
                  v_equal_taxcode_detail_id := tax_matrix.else_taxcode_detail_id;
                  v_equal_taxcode_state_code := tax_matrix.else_taxcode_state_code;
                  v_equal_taxcode_type_code := tax_matrix.else_taxcode_type_code;
                  v_equal_taxcode_code := tax_matrix.else_taxcode_code;
                  v_equal_jurisdiction_id := tax_matrix.else_jurisdiction_id;
                  v_equal_measure_type_code := tax_matrix.else_measure_type_code;
                  v_equal_taxtype_code := tax_matrix.else_taxtype_code;
                  v_equal_cch_taxcat_code := tax_matrix.else_cch_taxcat_code;
                  v_equal_cch_group_code := tax_matrix.else_cch_group_code;
                  -- v_equal_cch_item_code := tax_matrix.else_cch_item_code;
                  v_greater_hold_code_flag := tax_matrix.then_hold_code_flag;
                  v_greater_taxcode_detail_id := tax_matrix.then_taxcode_detail_id;
                  v_greater_taxcode_state_code := tax_matrix.then_taxcode_state_code;
                  v_greater_taxcode_type_code := tax_matrix.then_taxcode_type_code;
                  v_greater_taxcode_code := tax_matrix.then_taxcode_code;
                  v_greater_jurisdiction_id := tax_matrix.then_jurisdiction_id;
                  v_greater_measure_type_code := tax_matrix.then_measure_type_code;
                  v_greater_taxtype_code := tax_matrix.then_taxtype_code;
                  v_greater_cch_taxcat_code := tax_matrix.then_cch_taxcat_code;
                  v_greater_cch_group_code := tax_matrix.then_cch_group_code;
                  -- v_greater_cch_item_code := tax_matrix.then_cch_item_code;
               ELSIF tax_matrix.relation_sign = '>=' THEN
                  v_less_hold_code_flag := tax_matrix.else_hold_code_flag;
                  v_less_taxcode_detail_id := tax_matrix.else_taxcode_detail_id;
                  v_less_taxcode_state_code := tax_matrix.else_taxcode_state_code;
                  v_less_taxcode_type_code := tax_matrix.else_taxcode_type_code;
                  v_less_taxcode_code := tax_matrix.else_taxcode_code;
                  v_less_jurisdiction_id := tax_matrix.else_jurisdiction_id;
                  v_less_measure_type_code := tax_matrix.else_measure_type_code;
                  v_less_taxtype_code := tax_matrix.else_taxtype_code;
                  v_less_cch_taxcat_code := tax_matrix.else_cch_taxcat_code;
                  v_less_cch_group_code := tax_matrix.else_cch_group_code;
                  -- v_less_cch_item_code := tax_matrix.else_cch_item_code;
                  v_equal_hold_code_flag := tax_matrix.then_hold_code_flag;
                  v_equal_taxcode_detail_id := tax_matrix.then_taxcode_detail_id;
                  v_equal_taxcode_state_code := tax_matrix.then_taxcode_state_code;
                  v_equal_taxcode_type_code := tax_matrix.then_taxcode_type_code;
                  v_equal_taxcode_code := tax_matrix.then_taxcode_code;
                  v_equal_jurisdiction_id := tax_matrix.then_jurisdiction_id;
                  v_equal_measure_type_code := tax_matrix.then_measure_type_code;
                  v_equal_taxtype_code := tax_matrix.then_taxtype_code;
                  v_equal_cch_taxcat_code := tax_matrix.then_cch_taxcat_code;
                  v_equal_cch_group_code := tax_matrix.then_cch_group_code;
                  -- v_equal_cch_item_code := tax_matrix.then_cch_item_code;
                  v_greater_hold_code_flag := tax_matrix.then_hold_code_flag;
                  v_greater_taxcode_detail_id := tax_matrix.then_taxcode_detail_id;
                  v_greater_taxcode_state_code := tax_matrix.then_taxcode_state_code;
                  v_greater_taxcode_type_code := tax_matrix.then_taxcode_type_code;
                  v_greater_taxcode_code := tax_matrix.then_taxcode_code;
                  v_greater_jurisdiction_id := tax_matrix.then_jurisdiction_id;
                  v_greater_measure_type_code := tax_matrix.then_measure_type_code;
                  v_greater_taxtype_code := tax_matrix.then_taxtype_code;
                  v_greater_cch_taxcat_code := tax_matrix.then_cch_taxcat_code;
                  v_greater_cch_group_code := tax_matrix.then_cch_group_code;
                  -- v_greater_cch_item_code := tax_matrix.then_cch_item_code;
               ELSIF tax_matrix.relation_sign = '<>' THEN
                  v_less_hold_code_flag := tax_matrix.then_hold_code_flag;
                  v_less_taxcode_detail_id := tax_matrix.then_taxcode_detail_id;
                  v_less_taxcode_state_code := tax_matrix.then_taxcode_state_code;
                  v_less_taxcode_type_code := tax_matrix.then_taxcode_type_code;
                  v_less_taxcode_code := tax_matrix.then_taxcode_code;
                  v_less_jurisdiction_id := tax_matrix.then_jurisdiction_id;
                  v_less_measure_type_code := tax_matrix.then_measure_type_code;
                  v_less_taxtype_code := tax_matrix.then_taxtype_code;
                  v_less_cch_taxcat_code := tax_matrix.then_cch_taxcat_code;
                  v_less_cch_group_code := tax_matrix.then_cch_group_code;
                  -- v_less_cch_item_code := tax_matrix.then_cch_item_code;
                  v_equal_hold_code_flag := tax_matrix.else_hold_code_flag;
                  v_equal_taxcode_detail_id := tax_matrix.else_taxcode_detail_id;
                  v_equal_taxcode_state_code := tax_matrix.else_taxcode_state_code;
                  v_equal_taxcode_type_code := tax_matrix.else_taxcode_type_code;
                  v_equal_taxcode_code := tax_matrix.else_taxcode_code;
                  v_equal_jurisdiction_id := tax_matrix.else_jurisdiction_id;
                  v_equal_measure_type_code := tax_matrix.else_measure_type_code;
                  v_equal_taxtype_code := tax_matrix.else_taxtype_code;
                  v_equal_cch_taxcat_code := tax_matrix.else_cch_taxcat_code;
                  v_equal_cch_group_code := tax_matrix.else_cch_group_code;
                  -- v_equal_cch_item_code := tax_matrix.else_cch_item_code;
                  v_greater_hold_code_flag := tax_matrix.then_hold_code_flag;
                  v_greater_taxcode_detail_id := tax_matrix.then_taxcode_detail_id;
                  v_greater_taxcode_state_code := tax_matrix.then_taxcode_state_code;
                  v_greater_taxcode_type_code := tax_matrix.then_taxcode_type_code;
                  v_greater_taxcode_code := tax_matrix.then_taxcode_code;
                  v_greater_jurisdiction_id := tax_matrix.then_jurisdiction_id;
                  v_greater_measure_type_code := tax_matrix.then_measure_type_code;
                  v_greater_taxtype_code := tax_matrix.then_taxtype_code;
                  v_greater_cch_taxcat_code := tax_matrix.then_cch_taxcat_code;
                  v_greater_cch_group_code := tax_matrix.then_cch_group_code;
                  -- v_greater_cch_item_code := tax_matrix.then_cch_item_code;
               END IF;

               SELECT DECODE(SIGN(transaction_detail.gl_line_itm_dist_amt - tax_matrix.relation_amount), -1, v_less_hold_code_flag, 0, v_equal_hold_code_flag, 1, v_greater_hold_code_flag),
                      DECODE(SIGN(transaction_detail.gl_line_itm_dist_amt - tax_matrix.relation_amount), -1, v_less_taxcode_detail_id, 0, v_equal_taxcode_detail_id, 1, v_greater_taxcode_detail_id),
                      DECODE(SIGN(transaction_detail.gl_line_itm_dist_amt - tax_matrix.relation_amount), -1, v_less_taxcode_state_code, 0, v_equal_taxcode_state_code, 1, v_greater_taxcode_state_code),
                      DECODE(SIGN(transaction_detail.gl_line_itm_dist_amt - tax_matrix.relation_amount), -1, v_less_taxcode_type_code, 0, v_equal_taxcode_type_code, 1, v_greater_taxcode_type_code),
                      DECODE(SIGN(transaction_detail.gl_line_itm_dist_amt - tax_matrix.relation_amount), -1, v_less_taxcode_code, 0, v_equal_taxcode_code, 1, v_greater_taxcode_code),
                      DECODE(SIGN(transaction_detail.gl_line_itm_dist_amt - tax_matrix.relation_amount), -1, v_less_jurisdiction_id, 0, v_equal_jurisdiction_id, 1, v_greater_jurisdiction_id),
                      DECODE(SIGN(transaction_detail.gl_line_itm_dist_amt - tax_matrix.relation_amount), -1, v_less_measure_type_code, 0, v_equal_measure_type_code, 1, v_greater_measure_type_code),
                      DECODE(SIGN(transaction_detail.gl_line_itm_dist_amt - tax_matrix.relation_amount), -1, v_less_taxtype_code, 0, v_equal_taxtype_code, 1, v_greater_taxtype_code),
                      DECODE(SIGN(transaction_detail.gl_line_itm_dist_amt - tax_matrix.relation_amount), -1, v_less_cch_taxcat_code, 0, v_equal_cch_taxcat_code, 1, v_greater_cch_taxcat_code),
                      DECODE(SIGN(transaction_detail.gl_line_itm_dist_amt - tax_matrix.relation_amount), -1, v_less_cch_group_code, 0, v_equal_cch_group_code, 1, v_greater_cch_group_code)
                 INTO v_hold_code_flag, transaction_detail.taxcode_detail_id, transaction_detail.taxcode_state_code, transaction_detail.taxcode_type_code, transaction_detail.taxcode_code, v_override_jurisdiction_id, transaction_detail.measure_type_code, v_taxtype_code, transaction_detail.cch_taxcat_code, transaction_detail.cch_group_code
                 FROM DUAL;
            ELSE
            -- Tax Matrix Line NOT Found
               transaction_detail.transaction_ind := 'S';
               transaction_detail.suspend_ind := 'T';
            END IF;
         END IF;
      ELSE
         -- Get Tax Type Codes
         SELECT taxtype_code
           INTO v_taxtype_code
           FROM tb_taxcode
          WHERE taxcode_code = transaction_detail.taxcode_code
            AND taxcode_type_code = transaction_detail.taxcode_type_code;
      END IF;

      -- Continue if not Suspended
      IF transaction_detail.transaction_ind IS NULL OR transaction_detail.transaction_ind <> 'S' THEN
         -- ****** DETERMINE TYPE of TAXCODE ****** --
         -- The Tax Code is Taxable
         IF transaction_detail.taxcode_type_code = 'T' THEN
            -- If Override jurisdiction id is not blank or 0 then use it instead of location matrix
            IF v_override_jurisdiction_id IS NOT NULL AND v_override_jurisdiction_id <> 0 THEN
               transaction_detail.jurisdiction_id := v_override_jurisdiction_id;
               transaction_detail.manual_jurisdiction_ind := 'AO';
            END IF;

            -- Get Jurisdiction TaxRates and calculate Tax Amounts
            sp_getusetax(transaction_detail.jurisdiction_id,
                         transaction_detail.gl_date,
                         transaction_detail.measure_type_code,
                         transaction_detail.gl_line_itm_dist_amt,
                         v_taxtype_code,
                         l_tb_location_matrix.override_taxtype_code,
                         l_tb_location_matrix.state_flag,
                         l_tb_location_matrix.county_flag,
                         l_tb_location_matrix.county_local_flag,
                         l_tb_location_matrix.city_flag,
                         l_tb_location_matrix.city_local_flag,
                         transaction_detail.invoice_freight_amt,
                         transaction_detail.invoice_discount_amt,
                         transaction_detail.transaction_state_code,
-- future? --                  transaction_detail.import_definition_code,
                         'importdefn',
                         transaction_detail.taxcode_code,
                         transaction_detail.jurisdiction_taxrate_id,
                         transaction_detail.state_use_amount,
                         transaction_detail.state_use_tier2_amount,
                         transaction_detail.state_use_tier3_amount,
                         transaction_detail.county_use_amount,
                         transaction_detail.county_local_use_amount,
                         transaction_detail.city_use_amount,
                         transaction_detail.city_local_use_amount,
                         transaction_detail.state_use_rate,
                         transaction_detail.state_use_tier2_rate,
                         transaction_detail.state_use_tier3_rate,
                         transaction_detail.state_split_amount,
                         transaction_detail.state_tier2_min_amount,
                         transaction_detail.state_tier2_max_amount,
                         transaction_detail.state_maxtax_amount,
                         transaction_detail.county_use_rate,
                         transaction_detail.county_local_use_rate,
                         transaction_detail.county_split_amount,
                         transaction_detail.county_maxtax_amount,
                         transaction_detail.county_single_flag,
                         transaction_detail.county_default_flag,
                         transaction_detail.city_use_rate,
                         transaction_detail.city_local_use_rate,
                         transaction_detail.city_split_amount,
                         transaction_detail.city_split_use_rate,
                         transaction_detail.city_single_flag,
                         transaction_detail.city_default_flag,
                         transaction_detail.combined_use_rate,
                         transaction_detail.tb_calc_tax_amt,
-- future --               transaction_detail.taxable_amt,
-- Targa? --                 transaction_detail.user_number_10,
                         vn_taxable_amt,
                         v_taxtype_used );

            -- Suspend if tax rate id is null
            IF transaction_detail.jurisdiction_taxrate_id IS NULL OR transaction_detail.jurisdiction_taxrate_id = 0 THEN
               transaction_detail.transaction_ind := 'S';
               transaction_detail.suspend_ind := 'R';
            ELSE
               -- Check for Taxable Amount -- 3351
               IF vn_taxable_amt <> transaction_detail.gl_line_itm_dist_amt THEN
                  transaction_detail.gl_extract_amt := transaction_detail.gl_line_itm_dist_amt;
                  transaction_detail.gl_line_itm_dist_amt := vn_taxable_amt;
               END IF;
            END IF;

         -- The Tax Code is Exempt
         ELSIF transaction_detail.taxcode_type_code = 'E' THEN
            transaction_detail.transaction_ind := 'P';

         -- The Tax Code is a Question
         ELSIF transaction_detail.taxcode_type_code = 'Q' THEN
           transaction_detail.transaction_ind := 'Q';

         -- The Tax Code is Unrecognized - Suspend
         ELSE
            transaction_detail.transaction_ind := 'S';
            transaction_detail.suspend_ind := '?';
         END IF;
      END IF;

      -- Continue if not Suspended
      IF transaction_detail.transaction_ind IS NULL OR transaction_detail.transaction_ind <> 'S' THEN
         -- Check for matrix "H"old
         IF ( v_hold_code_flag = '1' ) AND
            ( transaction_detail.taxcode_type_code = 'T' OR transaction_detail.taxcode_type_code = 'E') THEN
               transaction_detail.transaction_ind := 'H';
         ELSE
             transaction_detail.transaction_ind := 'P';
         END IF;
      END IF;

      -- Update transaction detail row
      BEGIN
         -- 3411 - add taxable amount and tax type used.
         UPDATE tb_transaction_detail
            SET gl_line_itm_dist_amt = transaction_detail.gl_line_itm_dist_amt,
                tb_calc_tax_amt = transaction_detail.tb_calc_tax_amt,
                state_use_amount = transaction_detail.state_use_amount,
                state_use_tier2_amount = transaction_detail.state_use_tier2_amount,
                state_use_tier3_amount = transaction_detail.state_use_tier3_amount,
                county_use_amount = transaction_detail.county_use_amount,
                county_local_use_amount = transaction_detail.county_local_use_amount,
                city_use_amount = transaction_detail.city_use_amount,
                city_local_use_amount = transaction_detail.city_local_use_amount,
                transaction_state_code = transaction_detail.transaction_state_code,
                auto_transaction_state_code = transaction_detail.auto_transaction_state_code,
                transaction_ind = transaction_detail.transaction_ind,
                suspend_ind = transaction_detail.suspend_ind,
                taxcode_detail_id = transaction_detail.taxcode_detail_id,
                taxcode_state_code = transaction_detail.taxcode_state_code,
                taxcode_type_code = transaction_detail.taxcode_type_code,
                taxcode_code = transaction_detail.taxcode_code,
                cch_taxcat_code = transaction_detail.cch_taxcat_code,
                cch_group_code =  transaction_detail.cch_group_code,
                tax_matrix_id = transaction_detail.tax_matrix_id,
                location_matrix_id = transaction_detail.location_matrix_id,
                jurisdiction_id = transaction_detail.jurisdiction_id,
                jurisdiction_taxrate_id = transaction_detail.jurisdiction_taxrate_id,
                manual_jurisdiction_ind = transaction_detail.manual_jurisdiction_ind,
                measure_type_code = transaction_detail.measure_type_code,
                state_use_rate = transaction_detail.state_use_rate,
                state_use_tier2_rate = transaction_detail.state_use_tier2_rate,
                state_use_tier3_rate = transaction_detail.state_use_tier3_rate,
                state_split_amount = transaction_detail.state_split_amount,
                state_tier2_min_amount = transaction_detail.state_tier2_min_amount,
                state_tier2_max_amount = transaction_detail.state_tier2_max_amount,
                state_maxtax_amount = transaction_detail.state_maxtax_amount,
                county_use_rate = transaction_detail.county_use_rate,
                county_local_use_rate = transaction_detail.county_local_use_rate,
                county_split_amount = transaction_detail.county_split_amount,
                county_maxtax_amount = transaction_detail.county_maxtax_amount,
                county_single_flag = transaction_detail.county_single_flag,
                county_default_flag = transaction_detail.county_default_flag,
                city_use_rate = transaction_detail.city_use_rate,
                city_local_use_rate = transaction_detail.city_local_use_rate,
                city_split_amount = transaction_detail.city_split_amount,
                city_split_use_rate = transaction_detail.city_split_use_rate,
                city_single_flag = transaction_detail.city_single_flag,
                city_default_flag = transaction_detail.city_default_flag,
                combined_use_rate = transaction_detail.combined_use_rate,
                gl_extract_amt = transaction_detail.gl_extract_amt,
                update_user_id = transaction_detail.update_user_id,
                update_timestamp = transaction_detail.update_timestamp
          WHERE transaction_detail_id = transaction_detail.transaction_detail_id;
         -- Error Checking
--         IF SQLCODE != 0 THEN
--            RAISE e_badupdate;
--         END IF;
--      EXCEPTION
--         WHEN e_badupdate THEN
--            NULL;
      END;

   END LOOP;
   CLOSE transaction_detail_cursor;

END IF;

--EXCEPTION
--   WHEN OTHERS THEN
--      NULL;
END;
/

SHOW ERRORS;

CREATE OR REPLACE PROCEDURE sp_tb_taxcode_a_u (
	p_new_erp_taxcode IN VARCHAR2,
	p_new_taxtype_code IN VARCHAR2,
	p_new_measure_type_code IN VARCHAR2,
	p_new_pct_dist_amt IN VARCHAR2,
	p_new_taxcode_type_code IN VARCHAR2,
	p_new_taxcode_code IN VARCHAR2 )

AS
BEGIN
   UPDATE tb_taxcode_detail
      SET tb_taxcode_detail.erp_taxcode = p_new_erp_taxcode,
	  tb_taxcode_detail.taxtype_code = p_new_taxtype_code,
	  tb_taxcode_detail.measure_type_code = p_new_measure_type_code,
	  tb_taxcode_detail.pct_dist_amt = p_new_pct_dist_amt
    WHERE tb_taxcode_detail.taxcode_type_code = p_new_taxcode_type_code
      AND tb_taxcode_detail.taxcode_code = p_new_taxcode_code;
END;
/

SHOW ERRORS;

CREATE OR REPLACE PROCEDURE sp_tb_taxcode_detail_b_iu
	(  p_new_taxcode_type_code IN VARCHAR2,
	   p_new_taxcode_code IN VARCHAR2,
	   p_new_erp_taxcode IN OUT VARCHAR2,
	   p_new_taxtype_code IN OUT VARCHAR2,
	   p_new_measure_type_code IN OUT VARCHAR2,
	   p_new_pct_dist_amt IN OUT VARCHAR2
	)
AS
BEGIN
      SELECT a.erp_taxcode, a.taxtype_code, a.measure_type_code, a.pct_dist_amt
	INTO p_new_erp_taxcode, p_new_taxtype_code, p_new_measure_type_code, p_new_pct_dist_amt
	FROM tb_taxcode a
       WHERE ( a.taxcode_type_code = p_new_taxcode_type_code )
	 AND ( a.taxcode_code = p_new_taxcode_code);

END;
/

SHOW ERRORS;

CREATE OR REPLACE PROCEDURE sp_tb_taxcode_state_a_u(
  p_new_active_flag IN VARCHAR2,
  p_new_taxcode_state_code IN VARCHAR2
)
AS
BEGIN
   UPDATE tb_taxcode_detail dtl
      SET dtl.active_flag = p_new_active_flag
    WHERE dtl.taxcode_state_code = p_new_taxcode_state_code;
END;
/

SHOW ERRORS;

CREATE OR REPLACE PROCEDURE sp_tb_tax_matrix_a_i(p_tax_matrix_id IN tb_tax_matrix.tax_matrix_id%TYPE)
AS
/* ************************************************************************************************/
/* Object Type/Name: SP Trigger/After Insert - sp_tb_tax_matrix_a_i                               */
/* Author:           Michael B. Fuller                                                            */
/* Date:             a long time ago                                                              */
/* Description:      A Tax Matrix line has been added                                             */
/* Arguments:        None                                                                         */
/* Returns:          None                                                                         */
/* ************************************************************************************************/
/* Maintenance History:                                                                           */
/* Scan  Author       Date     Release    Description                                  Ticket #   */
/* ----- ---------- ---------- ---------- -------------------------------------------- ---------- */
/*       M. Fuller  06/02/2006 3.3.5.0    Fix processing an allocated line - Temporary 871        */
/*       M. Fuller  01/04/2007 3.3.5.1    Fix processing an allocated line - Permenant 871        */
/* MBF01 M. Fuller  08/13/2007 3.4.1.1    Add 5 Calc Tax Flags                         883        */
/* JJF   J.Franco   12/17/2008 x.x.x.x    Convert trigger to stored procedure          xxx        */
/* ************************************************************************************************/

-- Defire Row Type for tb_tax_matrix record
   l_tb_tax_matrix tb_tax_matrix%ROWTYPE;

-- Define Cursor Type
   TYPE cursor_type is REF CURSOR;

-- Transaction Detail Selection SQL
   vc_transaction_detail_select    VARCHAR2(2000) :=
      'SELECT tb_transaction_detail.* ' ||
        'FROM tb_tmp_tax_matrix, ' ||
             'tb_transaction_detail ' ||
       'WHERE ( tb_tmp_tax_matrix.tax_matrix_id = ' || p_tax_matrix_id || ') AND ' ||
             '(( tb_tmp_tax_matrix.matrix_state_code = ''*ALL'' ) OR ( tb_tmp_tax_matrix.matrix_state_code = tb_transaction_detail.transaction_state_code )) AND ' ||
             '( tb_tmp_tax_matrix.effective_date <= tb_transaction_detail.gl_date ) AND ( tb_tmp_tax_matrix.expiration_date >= tb_transaction_detail.gl_date ) AND ' ||
             '( tb_transaction_detail.transaction_ind = ''S'' ) AND ( tb_transaction_detail.suspend_ind = ''T'' ) ';
   vc_transaction_detail_where     VARCHAR2(5000) := '';
   vc_transaction_detail_update    VARCHAR2(100) := 'FOR UPDATE';
   vc_transaction_detail_stmt      VARCHAR2(7100);
   transaction_detail_cursor       cursor_type;
   transaction_detail              tb_transaction_detail%ROWTYPE;

-- Location Matrix Selection SQL
   vc_location_matrix_select       VARCHAR2(2000) :=
      'SELECT tb_location_matrix.location_matrix_id, ' ||
             'tb_location_matrix.jurisdiction_id, ' ||
             'tb_location_matrix.override_taxtype_code, ' ||
             'tb_location_matrix.state_flag,' ||
             'tb_location_matrix.county_flag,' ||
             'tb_location_matrix.county_local_flag,' ||
             'tb_location_matrix.city_flag,' ||
             'tb_location_matrix.city_local_flag,' ||
             'tb_jurisdiction.state, ' ||
             'tb_jurisdiction.county, ' ||
             'tb_jurisdiction.city, ' ||
             'tb_jurisdiction.zip, ' ||
             'tb_jurisdiction.in_out ' ||
        'FROM tb_location_matrix, ' ||
             'tb_jurisdiction, ' ||
             'tb_transaction_detail ' ||
      'WHERE ( tb_transaction_detail.transaction_detail_id = :v_transaction_detail_id ) AND ' ||
            '( tb_location_matrix.default_flag = ''0'' AND tb_location_matrix.binary_weight > 0 ) AND ' ||
            '( tb_location_matrix.effective_date <= tb_transaction_detail.gl_date ) AND ( tb_location_matrix.expiration_date >= tb_transaction_detail.gl_date ) AND ' ||
            '( tb_location_matrix.jurisdiction_id = tb_jurisdiction.jurisdiction_id ) ';
   vc_location_matrix_where        VARCHAR2(5000) := '';
   vc_location_matrix_orderby      VARCHAR2(1000) :=
      'ORDER BY tb_location_matrix.binary_weight DESC, tb_location_matrix.significant_digits DESC, tb_location_matrix.effective_date DESC';
   vc_location_matrix_stmt         VARCHAR2 (8000);
   location_matrix_cursor          cursor_type;

   vc_state_driver_flag            CHAR(1)                                           := NULL;

-- Location Matrix Record TYPE
   /*TYPE location_matrix_record is RECORD (
      location_matrix_id           tb_location_matrix.location_matrix_id%TYPE,
      jurisdiction_id              tb_location_matrix.jurisdiction_id%TYPE,
      override_taxtype_code        tb_location_matrix.override_taxtype_code%TYPE,
      state_flag                   tb_location_matrix.state_flag%TYPE,
      county_flag                  tb_location_matrix.county_flag%TYPE,
      county_local_flag            tb_location_matrix.county_local_flag%TYPE,
      city_flag                    tb_location_matrix.city_flag%TYPE,
      city_local_flag              tb_location_matrix.city_local_flag%TYPE,
      state                        tb_jurisdiction.state%TYPE,
      county                       tb_jurisdiction.county%TYPE,
      city                         tb_jurisdiction.city%TYPE,
      zip                          tb_jurisdiction.zip%TYPE,
      in_out                       tb_jurisdiction.in_out%TYPE );*/
   location_matrix                 matrix_record_pkg.location_matrix_record;

-- Table defined variables
   v_relation_amount               tb_tax_matrix.relation_amount%TYPE                := NULL;
   v_hold_code_flag                tb_tax_matrix.then_hold_code_flag%TYPE            := NULL;
   v_less_hold_code_flag           tb_tax_matrix.then_hold_code_flag%TYPE            := NULL;
   v_less_taxcode_detail_id        tb_tax_matrix.then_taxcode_detail_id%TYPE         := NULL;
   v_less_cch_taxcat_code          tb_tax_matrix.then_cch_taxcat_code%TYPE           := NULL;
   v_less_cch_group_code           tb_tax_matrix.then_cch_group_code%TYPE            := NULL;
   v_equal_hold_code_flag          tb_tax_matrix.then_hold_code_flag%TYPE            := NULL;
   v_equal_taxcode_detail_id       tb_tax_matrix.then_taxcode_detail_id%TYPE         := NULL;
   v_equal_cch_taxcat_code         tb_tax_matrix.then_cch_taxcat_code%TYPE           := NULL;
   v_equal_cch_group_code          tb_tax_matrix.then_cch_group_code%TYPE            := NULL;
   v_greater_hold_code_flag        tb_tax_matrix.then_hold_code_flag%TYPE            := NULL;
   v_greater_taxcode_detail_id     tb_tax_matrix.then_taxcode_detail_id%TYPE         := NULL;
   v_greater_cch_taxcat_code       tb_tax_matrix.then_cch_taxcat_code%TYPE           := NULL;
   v_greater_cch_group_code        tb_tax_matrix.then_cch_group_code%TYPE            := NULL;
   v_then_taxcode_state_code       tb_taxcode_detail.taxcode_state_code%TYPE         := NULL;
   v_then_taxcode_type_code        tb_taxcode_detail.taxcode_type_code%TYPE          := NULL;
   v_then_taxcode_code             tb_taxcode_detail.taxcode_code%TYPE               := NULL;
   v_then_jurisdiction_id          tb_taxcode_detail.jurisdiction_id%TYPE            := NULL;
   v_then_measure_type_code        tb_taxcode_detail.measure_type_code%TYPE          := NULL;
   v_then_taxtype_code             tb_taxcode_detail.taxtype_code%TYPE               := NULL;
   v_else_taxcode_state_code       tb_taxcode_detail.taxcode_state_code%TYPE         := NULL;
   v_else_taxcode_type_code        tb_taxcode_detail.taxcode_type_code%TYPE          := NULL;
   v_else_taxcode_code             tb_taxcode_detail.taxcode_code%TYPE               := NULL;
   v_else_jurisdiction_id          tb_taxcode_detail.jurisdiction_id%TYPE            := NULL;
   v_else_measure_type_code        tb_taxcode_detail.measure_type_code%TYPE          := NULL;
   v_else_taxtype_code             tb_taxcode_detail.taxtype_code%TYPE               := NULL;
   v_less_taxcode_state_code       tb_taxcode_detail.taxcode_state_code%TYPE         := NULL;
   v_less_taxcode_type_code        tb_taxcode_detail.taxcode_type_code%TYPE          := NULL;
   v_less_taxcode_code             tb_taxcode_detail.taxcode_code%TYPE               := NULL;
   v_less_jurisdiction_id          tb_taxcode_detail.jurisdiction_id%TYPE            := NULL;
   v_less_measure_type_code        tb_taxcode_detail.measure_type_code%TYPE          := NULL;
   v_less_taxtype_code             tb_taxcode_detail.taxtype_code%TYPE               := NULL;
   v_equal_taxcode_state_code      tb_taxcode_detail.taxcode_state_code%TYPE         := NULL;
   v_equal_taxcode_type_code       tb_taxcode_detail.taxcode_type_code%TYPE          := NULL;
   v_equal_taxcode_code            tb_taxcode_detail.taxcode_code%TYPE               := NULL;
   v_equal_jurisdiction_id         tb_taxcode_detail.jurisdiction_id%TYPE            := NULL;
   v_equal_measure_type_code       tb_taxcode_detail.measure_type_code%TYPE          := NULL;
   v_equal_taxtype_code            tb_taxcode_detail.taxtype_code%TYPE               := NULL;
   v_greater_taxcode_state_code    tb_taxcode_detail.taxcode_state_code%TYPE         := NULL;
   v_greater_taxcode_type_code     tb_taxcode_detail.taxcode_type_code%TYPE          := NULL;
   v_greater_taxcode_code          tb_taxcode_detail.taxcode_code%TYPE               := NULL;
   v_greater_jurisdiction_id       tb_taxcode_detail.jurisdiction_id%TYPE            := NULL;
   v_greater_measure_type_code     tb_taxcode_detail.measure_type_code%TYPE          := NULL;
   v_greater_taxtype_code          tb_taxcode_detail.taxtype_code%TYPE               := NULL;
   v_transaction_detail_id         tb_transaction_detail.transaction_detail_id%TYPE  := NULL;
   v_override_jurisdiction_id      tb_bcp_transactions.jurisdiction_id%TYPE          := NULL;
   v_sysdate                       tb_transaction_detail.load_timestamp%TYPE         := SYSDATE;
   v_taxtype_code                  tb_taxcode.taxtype_code%TYPE                      := NULL;
   v_taxtype_used                  tb_taxcode.taxtype_code%TYPE                      := NULL;

-- Program defined variables
   vn_hold_trans_amount            NUMBER                                            := 0;
   vn_fetch_rows                   NUMBER                                            := 0;
   vi_error_count                  INTEGER                                           := 0;
   vn_actual_rowno                 NUMBER                                            := 0;
-- temporary
   vn_taxable_amt                  NUMBER                                            := 0;

   vc_test1 varchar2(4000);
   vc_test2 varchar2(4000);

-- Define Location Driver Names Cursor (tb_driver_names)
   /*CURSOR location_driver_cursor
   IS
        SELECT driver.trans_dtl_column_name, driver.matrix_column_name, driver.null_driver_flag, driver.wildcard_flag, driver.range_flag, driver.to_number_flag,
               datadefcol.desc_column_name, datadefcol.data_type
          FROM tb_driver_names driver,
               (SELECT *
                  FROM tb_data_def_column
                 WHERE table_name = 'TB_TRANSACTION_DETAIL') datadefcol
         WHERE driver.driver_names_code = 'L' AND
               driver.trans_dtl_column_name = datadefcol.column_name
      ORDER BY driver.driver_id;
      location_driver              location_driver_cursor%ROWTYPE;*/

-- Define Tax Driver Names Cursor (tb_driver_names)
   /*CURSOR tax_driver_cursor
   IS
        SELECT driver.trans_dtl_column_name, driver.matrix_column_name, driver.null_driver_flag, driver.wildcard_flag, driver.range_flag, driver.to_number_flag,
               datadefcol.desc_column_name, datadefcol.data_type
          FROM tb_driver_names driver,
               (SELECT *
                  FROM tb_data_def_column
                 WHERE table_name = 'TB_TRANSACTION_DETAIL') datadefcol
         WHERE driver.driver_names_code = 'T' AND
               driver.trans_dtl_column_name = datadefcol.column_name
      ORDER BY driver.driver_id;
      tax_driver                   tax_driver_cursor%ROWTYPE;*/

-- Define Exceptions
   e_badread                       exception;
   e_badupdate                     exception;

-- ****** Program starts ****** --------------------------------------------------------------------
BEGIN

SELECT tb_tax_matrix.*
 INTO  l_tb_tax_matrix
 FROM  tb_tax_matrix
 WHERE tax_matrix_id = p_tax_matrix_id;

IF l_tb_tax_matrix.default_flag <> '1' AND l_tb_tax_matrix.binary_weight > 0 THEN

   -- *******************************************************************************************************************************************
   -- NEW BATCH PROCESS CODE --------------------------------------------------------------------------------------------------------------------
   -- ***** Create Dynamic WHERE Clause for Transaction Detail SQL ***** --
   -- Tax Matrix Drivers
   /*OPEN tax_driver_cursor;
   LOOP
      FETCH tax_driver_cursor INTO tax_driver;
      EXIT WHEN tax_driver_cursor%NOTFOUND;
      IF tax_driver.range_flag = '1' THEN
         IF tax_driver.to_number_flag = '1' THEN
            IF tax_driver.null_driver_flag = '1' THEN
               -- Range and ToNumber and NULL
               IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                  UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                  vc_transaction_detail_where := vc_transaction_detail_where || 'AND ( ' ||
                  '( tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || '_THRU = ''*NA'' AND ' ||
                     '( tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NULL AND ( tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*NULL'' )) OR ' ||
                     '( tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND ( tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR DECODE(isnumber(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || '),1,to_char(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ',''00000000000000000000.00000000000000000000''),UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ')) = DECODE(isnumber(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || '),1,to_char(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ',''00000000000000000000.00000000000000000000''),UPPER(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ') ))) OR ' ||
                  '( tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || '_THRU <> ''*NA'' AND ' ||
                     '( tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND (DECODE(isnumber(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || '),1,to_char(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ',''00000000000000000000.00000000000000000000''),UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ')) >= DECODE(isnumber(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || '),1,to_char(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ',''00000000000000000000.00000000000000000000''),UPPER(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ') ) AND ' ||
                                                                                            'DECODE(isnumber(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || '),1,to_char(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ',''00000000000000000000.00000000000000000000''),UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ')) <= DECODE(isnumber(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || '_THRU ),1,to_char(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || '_THRU,''00000000000000000000.00000000000000000000''),UPPER(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || '_THRU)) )))) ';
               END IF;
            ELSE
               -- Range and ToNumber and NOT NULL
               IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                  UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                  vc_transaction_detail_where := vc_transaction_detail_where || 'AND ( ' ||
                  '( tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || '_THRU = ''*NA'' AND (tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR DECODE(isnumber(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || '),1,to_char(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ',''00000000000000000000.00000000000000000000''),UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ')) = DECODE(isnumber(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || '),1,to_char(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ',''00000000000000000000.00000000000000000000''),UPPER(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ')))) OR ' ||
                  '( tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || '_THRU <> ''*NA'' AND (DECODE(isnumber(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || '),1,to_char(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ',''00000000000000000000.00000000000000000000''),UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ')) >= DECODE(isnumber(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || '),1,to_char(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ',''00000000000000000000.00000000000000000000''),UPPER(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ')) AND ' ||
                                                                                        'DECODE(isnumber(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || '),1,to_char(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ',''00000000000000000000.00000000000000000000''),UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ')) <= DECODE(isnumber(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || '_THRU ),1,to_char(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || '_THRU,''00000000000000000000.00000000000000000000''),UPPER(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || '_THRU)) )) ';
               END IF;
            END IF;
         ELSE
            IF tax_driver.null_driver_flag = '1' THEN
               IF tax_driver.wildcard_flag = '1' THEN
                  -- Range and NOT ToNumber and NULL and Wildcard
                  IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                     UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                     vc_transaction_detail_where := vc_transaction_detail_where || 'AND ( ' ||
                     '( tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || '_THRU = ''*NA'' AND ' ||
                        '( tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NULL AND (tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*NULL'' )) OR ' ||
                        '( tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND (tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') LIKE UPPER(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ')))) OR ' ||
                     '( tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || '_THRU <> ''*NA'' AND (tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND (UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') >= UPPER(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ') AND UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') <= UPPER(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || '_THRU) ))) ';
                  END IF;
               ELSE
                  -- Range and NOT ToNumber and NULL and NOT Wildcard
                  IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                     UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                     vc_transaction_detail_where := vc_transaction_detail_where || 'AND ( ' ||
                     '( tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || '_THRU = ''*NA'' AND ' ||
                        '( tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NULL AND (tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*NULL'' )) OR ' ||
                        '( tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND (tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') = UPPER(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ')))) OR ' ||
                     '( tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || '_THRU <> ''*NA'' AND (tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND (UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') >= UPPER(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ') AND UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') <= UPPER(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || '_THRU) ))) ';
                  END IF;
               END IF;
            ELSE
               IF tax_driver.wildcard_flag = '1' THEN
                  -- Range and NOT ToNumber and NOT NULL and Wildcard
                  IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                     UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                     vc_transaction_detail_where := vc_transaction_detail_where || 'AND ( ' ||
                     '( tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || '_THRU = ''*NA'' AND ' ||
                        '(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') LIKE UPPER(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || '))) OR ' ||
                     '( tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || '_THRU <> ''*NA'' AND (UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') >= UPPER(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ') AND UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') <= UPPER(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || '_THRU) )) ';
                  END IF;
               ELSE
                  -- Range and NOT ToNumber and NOT NULL and NOT Wildcard
                  IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                     UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                     vc_transaction_detail_where := vc_transaction_detail_where || 'AND ( ' ||
                     '( tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || '_THRU = ''*NA'' AND ' ||
                        '(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') = UPPER(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || '))) OR ' ||
                     '( tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || '_THRU <> ''*NA'' AND (UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') >= UPPER(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ') AND UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') <= UPPER(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || '_THRU) )) ';
                  END IF;
               END IF;
            END IF;
         END IF;
      ELSE
         IF tax_driver.null_driver_flag = '1' THEN
            IF tax_driver.wildcard_flag = '1' THEN
               -- NOT Range and NULL and Wildcard
               IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                  UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                     vc_transaction_detail_where := vc_transaction_detail_where || 'AND ( ' ||
                     '( tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NULL AND (tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*NULL'' )) OR ' ||
                     '( tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND (tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') LIKE UPPER(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ')) ) ';
               END IF;
            ELSE
               -- NOT Range and NULL and NOT Wildcard
               IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                  UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                     vc_transaction_detail_where := vc_transaction_detail_where || 'AND ( ' ||
                     '( tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NULL AND (tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*NULL'' )) OR ' ||
                     '( tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND (tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') = UPPER(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ')) ) ';
               END IF;
            END IF;
         ELSE
            IF tax_driver.wildcard_flag = '1' THEN
               -- NOT Range and NOT NULL and Wildcard
               IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                  UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                     vc_transaction_detail_where := vc_transaction_detail_where || 'AND ( ' ||
                     '( tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') LIKE UPPER(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ') ) ';
               END IF;
            ELSE
               -- NOT Range and NOT NULL and NOT Wildcard
               IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                  UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                  vc_transaction_detail_where := vc_transaction_detail_where || 'AND ( ' ||
                     '( tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') = UPPER(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ') ) ';
               END IF;
            END IF;
         END IF;
      END IF;
      vc_transaction_detail_where := vc_transaction_detail_where || ') ';
   END LOOP;
   CLOSE tax_driver_cursor;*/

   -- New call to sp_gen_tax_driver for MidTier Cleanup
   sp_gen_tax_driver (
	p_generate_driver_reference	=> 'N',
	p_an_batch_id			=> NULL,
	p_transaction_table_name	=> 'tb_transaction_detail',
	p_tax_table_name		=> 'tb_tmp_tax_matrix',
	p_vc_state_driver_flag		=> vc_state_driver_flag,
	p_vc_tax_matrix_where		=> vc_transaction_detail_where);

   -- NEW BATCH PROCESS CODE --------------------------------------------------------------------------------------------------------------------
   -- *******************************************************************************************************************************************

   -- If no drivers found raise error, else create transaction detail sql statement
   IF vc_transaction_detail_where IS NULL THEN
      NULL;
   ELSE
      vc_transaction_detail_stmt := vc_transaction_detail_select || vc_transaction_detail_where || vc_transaction_detail_update;
   END IF;

   -- ***** Create Dynamic WHERE Clause for Matrix SQLs ***** --
   -- Location Matrix Drivers
   /*OPEN location_driver_cursor;
   LOOP
      FETCH location_driver_cursor INTO location_driver;
      EXIT WHEN location_driver_cursor%NOTFOUND;
      IF location_driver.null_driver_flag = '1' THEN
         IF location_driver.wildcard_flag = '1' THEN
            IF UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'CHAR' OR
               UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'VARC' THEN
                  vc_location_matrix_where := vc_location_matrix_where || 'AND ' ||
                  '((tb_transaction_detail.' || location_driver.trans_dtl_column_name || ' IS NULL AND (tb_location_matrix.' || location_driver.matrix_column_name || ' = ''*ALL'' OR tb_location_matrix.' || location_driver.matrix_column_name || ' = ''*NULL'' )) OR ' ||
                  '(tb_transaction_detail.' || location_driver.trans_dtl_column_name || ' IS NOT NULL AND (tb_location_matrix.' || location_driver.matrix_column_name || ' = ''*ALL'' OR UPPER(tb_transaction_detail.' || location_driver.trans_dtl_column_name || ') LIKE UPPER(tb_location_matrix.' || location_driver.matrix_column_name || ') ))) ';
            END IF;
         ELSE
            IF UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'CHAR' OR
               UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'VARC' THEN
                  vc_location_matrix_where := vc_location_matrix_where || 'AND ' ||
                  '((tb_transaction_detail.' || location_driver.trans_dtl_column_name || ' IS NULL AND (tb_location_matrix.' || location_driver.matrix_column_name || ' = ''*ALL'' OR tb_location_matrix.' || location_driver.matrix_column_name || ' = ''*NULL'' )) OR ' ||
                  '(tb_transaction_detail.' || location_driver.trans_dtl_column_name || ' IS NOT NULL AND (tb_location_matrix.' || location_driver.matrix_column_name || ' = ''*ALL'' OR UPPER(tb_transaction_detail.' || location_driver.trans_dtl_column_name || ') = UPPER(tb_location_matrix.' || location_driver.matrix_column_name || ') ))) ';
            END IF;
         END IF;
      ELSE
         IF location_driver.wildcard_flag = '1' THEN
            IF UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'CHAR' OR
               UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'VARC' THEN
                  vc_location_matrix_where := vc_location_matrix_where || 'AND ' ||
                  '(tb_location_matrix.' || location_driver.matrix_column_name || ' = ''*ALL'' OR UPPER(tb_transaction_detail.' || location_driver.trans_dtl_column_name || ') LIKE UPPER(tb_location_matrix.' || location_driver.matrix_column_name || ') ) ';
            END IF;
         ELSE
            IF UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'CHAR' OR
               UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'VARC' THEN
                  vc_location_matrix_where := vc_location_matrix_where || 'AND ' ||
                  '(tb_location_matrix.' || location_driver.matrix_column_name || ' = ''*ALL'' OR UPPER(tb_transaction_detail.' || location_driver.trans_dtl_column_name || ') = UPPER(tb_location_matrix.' || location_driver.matrix_column_name || ') ) ';
            END IF;
         END IF;
      END IF;
   END LOOP;
   CLOSE location_driver_cursor;*/

   -- New call to sp_gen_location_driver for MidTier Cleanup
   sp_gen_location_driver (
	p_generate_driver_reference	=> 'N',
	p_an_batch_id			=> NULL,
	p_transaction_table_name	=> 'tb_transaction_detail',
	p_location_table_name		=> 'tb_location_matrix',
	p_vc_location_matrix_where	=> vc_location_matrix_where);

   -- If no drivers found raise error, else create transaction detail sql statement
   IF vc_location_matrix_where IS NULL THEN
      NULL;
   ELSE
      vc_location_matrix_stmt := vc_location_matrix_select || vc_location_matrix_where || vc_location_matrix_orderby;
   END IF;

   -- Insert New Tax Matrix columns into temporary table -------------------------------------------
   INSERT INTO tb_tmp_tax_matrix (
      tax_matrix_id, matrix_state_code, driver_global_flag,
      driver_01, driver_01_desc,
      driver_01_thru, driver_01_thru_desc,
      driver_02, driver_02_desc,
      driver_02_thru, driver_02_thru_desc,
      driver_03, driver_03_desc,
      driver_03_thru, driver_03_thru_desc,
      driver_04, driver_04_desc,
      driver_04_thru, driver_04_thru_desc,
      driver_05, driver_05_desc,
      driver_05_thru, driver_05_thru_desc,
      driver_06, driver_06_desc,
      driver_06_thru, driver_06_thru_desc,
      driver_07, driver_07_desc,
      driver_07_thru, driver_07_thru_desc,
      driver_08, driver_08_desc,
      driver_08_thru, driver_08_thru_desc,
      driver_09, driver_09_desc,
      driver_09_thru, driver_09_thru_desc,
      driver_10, driver_10_desc,
      driver_10_thru, driver_10_thru_desc,
      driver_11, driver_11_desc,
      driver_11_thru, driver_11_thru_desc,
      driver_12, driver_12_desc,
      driver_12_thru, driver_12_thru_desc,
      driver_13, driver_13_desc,
      driver_13_thru, driver_13_thru_desc,
      driver_14, driver_14_desc,
      driver_14_thru, driver_14_thru_desc,
      driver_15, driver_15_desc,
      driver_15_thru, driver_15_thru_desc,
      driver_16, driver_16_desc,
      driver_16_thru, driver_16_thru_desc,
      driver_17, driver_17_desc,
      driver_17_thru, driver_17_thru_desc,
      driver_18, driver_18_desc,
      driver_18_thru, driver_18_thru_desc,
      driver_19, driver_19_desc,
      driver_19_thru, driver_19_thru_desc,
      driver_20, driver_20_desc,
      driver_20_thru, driver_20_thru_desc,
      driver_21, driver_21_desc,
      driver_21_thru, driver_21_thru_desc,
      driver_22, driver_22_desc,
      driver_22_thru, driver_22_thru_desc,
      driver_23, driver_23_desc,
      driver_23_thru, driver_23_thru_desc,
      driver_24, driver_24_desc,
      driver_24_thru, driver_24_thru_desc,
      driver_25, driver_25_desc,
      driver_25_thru, driver_25_thru_desc,
      driver_26, driver_26_desc,
      driver_26_thru, driver_26_thru_desc,
      driver_27, driver_27_desc,
      driver_27_thru, driver_27_thru_desc,
      driver_28, driver_28_desc,
      driver_28_thru, driver_28_thru_desc,
      driver_29, driver_29_desc,
      driver_29_thru, driver_29_thru_desc,
      driver_30, driver_30_desc,
      driver_30_thru, driver_30_thru_desc,
      binary_weight, significant_digits,
      effective_date, expiration_date,
      relation_sign, relation_amount,
      then_hold_code_flag, then_taxcode_detail_id, then_cch_taxcat_code, then_cch_group_code, then_cch_item_code,
      else_hold_code_flag, else_taxcode_detail_id, else_cch_taxcat_code, else_cch_group_code, else_cch_item_code,
      comments,
      last_used_timestamp, update_user_id, update_timestamp )
   VALUES (
      l_tb_tax_matrix.tax_matrix_id, l_tb_tax_matrix.matrix_state_code, l_tb_tax_matrix.driver_global_flag,
      l_tb_tax_matrix.driver_01, l_tb_tax_matrix.driver_01_desc,
      l_tb_tax_matrix.driver_01_thru, l_tb_tax_matrix.driver_01_thru_desc,
      l_tb_tax_matrix.driver_02, l_tb_tax_matrix.driver_02_desc,
      l_tb_tax_matrix.driver_02_thru, l_tb_tax_matrix.driver_02_thru_desc,
      l_tb_tax_matrix.driver_03, l_tb_tax_matrix.driver_03_desc,
      l_tb_tax_matrix.driver_03_thru, l_tb_tax_matrix.driver_03_thru_desc,
      l_tb_tax_matrix.driver_04, l_tb_tax_matrix.driver_04_desc,
      l_tb_tax_matrix.driver_04_thru, l_tb_tax_matrix.driver_04_thru_desc,
      l_tb_tax_matrix.driver_05, l_tb_tax_matrix.driver_05_desc,
      l_tb_tax_matrix.driver_05_thru, l_tb_tax_matrix.driver_05_thru_desc,
      l_tb_tax_matrix.driver_06, l_tb_tax_matrix.driver_06_desc,
      l_tb_tax_matrix.driver_06_thru, l_tb_tax_matrix.driver_06_thru_desc,
      l_tb_tax_matrix.driver_07, l_tb_tax_matrix.driver_07_desc,
      l_tb_tax_matrix.driver_07_thru, l_tb_tax_matrix.driver_07_thru_desc,
      l_tb_tax_matrix.driver_08, l_tb_tax_matrix.driver_08_desc,
      l_tb_tax_matrix.driver_08_thru, l_tb_tax_matrix.driver_08_thru_desc,
      l_tb_tax_matrix.driver_09, l_tb_tax_matrix.driver_09_desc,
      l_tb_tax_matrix.driver_09_thru, l_tb_tax_matrix.driver_09_thru_desc,
      l_tb_tax_matrix.driver_10, l_tb_tax_matrix.driver_10_desc,
      l_tb_tax_matrix.driver_10_thru, l_tb_tax_matrix.driver_10_thru_desc,
      l_tb_tax_matrix.driver_11, l_tb_tax_matrix.driver_11_desc,
      l_tb_tax_matrix.driver_11_thru, l_tb_tax_matrix.driver_11_thru_desc,
      l_tb_tax_matrix.driver_12, l_tb_tax_matrix.driver_12_desc,
      l_tb_tax_matrix.driver_12_thru, l_tb_tax_matrix.driver_12_thru_desc,
      l_tb_tax_matrix.driver_13, l_tb_tax_matrix.driver_13_desc,
      l_tb_tax_matrix.driver_13_thru, l_tb_tax_matrix.driver_13_thru_desc,
      l_tb_tax_matrix.driver_14, l_tb_tax_matrix.driver_14_desc,
      l_tb_tax_matrix.driver_14_thru, l_tb_tax_matrix.driver_14_thru_desc,
      l_tb_tax_matrix.driver_15, l_tb_tax_matrix.driver_15_desc,
      l_tb_tax_matrix.driver_15_thru, l_tb_tax_matrix.driver_15_thru_desc,
      l_tb_tax_matrix.driver_16, l_tb_tax_matrix.driver_16_desc,
      l_tb_tax_matrix.driver_16_thru, l_tb_tax_matrix.driver_16_thru_desc,
      l_tb_tax_matrix.driver_17, l_tb_tax_matrix.driver_17_desc,
      l_tb_tax_matrix.driver_17_thru, l_tb_tax_matrix.driver_17_thru_desc,
      l_tb_tax_matrix.driver_18, l_tb_tax_matrix.driver_18_desc,
      l_tb_tax_matrix.driver_18_thru, l_tb_tax_matrix.driver_18_thru_desc,
      l_tb_tax_matrix.driver_19, l_tb_tax_matrix.driver_19_desc,
      l_tb_tax_matrix.driver_19_thru, l_tb_tax_matrix.driver_19_thru_desc,
      l_tb_tax_matrix.driver_20, l_tb_tax_matrix.driver_20_desc,
      l_tb_tax_matrix.driver_20_thru, l_tb_tax_matrix.driver_20_thru_desc,
      l_tb_tax_matrix.driver_21, l_tb_tax_matrix.driver_21_desc,
      l_tb_tax_matrix.driver_21_thru, l_tb_tax_matrix.driver_21_thru_desc,
      l_tb_tax_matrix.driver_22, l_tb_tax_matrix.driver_22_desc,
      l_tb_tax_matrix.driver_22_thru, l_tb_tax_matrix.driver_22_thru_desc,
      l_tb_tax_matrix.driver_23, l_tb_tax_matrix.driver_23_desc,
      l_tb_tax_matrix.driver_23_thru, l_tb_tax_matrix.driver_23_thru_desc,
      l_tb_tax_matrix.driver_24, l_tb_tax_matrix.driver_24_desc,
      l_tb_tax_matrix.driver_24_thru, l_tb_tax_matrix.driver_24_thru_desc,
      l_tb_tax_matrix.driver_25, l_tb_tax_matrix.driver_25_desc,
      l_tb_tax_matrix.driver_25_thru, l_tb_tax_matrix.driver_25_thru_desc,
      l_tb_tax_matrix.driver_26, l_tb_tax_matrix.driver_26_desc,
      l_tb_tax_matrix.driver_26_thru, l_tb_tax_matrix.driver_26_thru_desc,
      l_tb_tax_matrix.driver_27, l_tb_tax_matrix.driver_27_desc,
      l_tb_tax_matrix.driver_27_thru, l_tb_tax_matrix.driver_27_thru_desc,
      l_tb_tax_matrix.driver_28, l_tb_tax_matrix.driver_28_desc,
      l_tb_tax_matrix.driver_28_thru, l_tb_tax_matrix.driver_28_thru_desc,
      l_tb_tax_matrix.driver_29, l_tb_tax_matrix.driver_29_desc,
      l_tb_tax_matrix.driver_29_thru, l_tb_tax_matrix.driver_29_thru_desc,
      l_tb_tax_matrix.driver_30, l_tb_tax_matrix.driver_30_desc,
      l_tb_tax_matrix.driver_30_thru, l_tb_tax_matrix.driver_30_thru_desc,
      l_tb_tax_matrix.binary_weight, l_tb_tax_matrix.significant_digits,
      l_tb_tax_matrix.effective_date, l_tb_tax_matrix.expiration_date,
      l_tb_tax_matrix.relation_sign, l_tb_tax_matrix.relation_amount,
      l_tb_tax_matrix.then_hold_code_flag, l_tb_tax_matrix.then_taxcode_detail_id, l_tb_tax_matrix.then_cch_taxcat_code,  l_tb_tax_matrix.then_cch_group_code,  l_tb_tax_matrix.then_cch_item_code,
      l_tb_tax_matrix.else_hold_code_flag, l_tb_tax_matrix.else_taxcode_detail_id, l_tb_tax_matrix.else_cch_taxcat_code,  l_tb_tax_matrix.else_cch_group_code,  l_tb_tax_matrix.else_cch_item_code,
      l_tb_tax_matrix.comments,
      l_tb_tax_matrix.last_used_timestamp, l_tb_tax_matrix.update_user_id, l_tb_tax_matrix.update_timestamp );

   -- ****** Read and Process Transactions ****** -----------------------------------------
    OPEN transaction_detail_cursor
    FOR vc_transaction_detail_stmt;
    LOOP
      FETCH transaction_detail_cursor
       INTO transaction_detail;
       EXIT WHEN transaction_detail_cursor%NOTFOUND;

      -- Populate and/or Clear Transaction Detail working fields
      transaction_detail.transaction_ind := NULL;
      transaction_detail.suspend_ind := NULL;
      transaction_detail.tax_matrix_id := l_tb_tax_matrix.tax_matrix_id;
      transaction_detail.update_user_id := USER;
      transaction_detail.update_timestamp := v_sysdate;

      -- Get "Then" TaxCode Elements
      BEGIN
         SELECT taxcode_state_code,
                taxcode_type_code,
                taxcode_code,
                jurisdiction_id,
                measure_type_code,
                taxtype_code
           INTO v_then_taxcode_state_code,
                v_then_taxcode_type_code,
                v_then_taxcode_code,
                v_then_jurisdiction_id,
                v_then_measure_type_code,
                v_then_taxtype_code
           FROM tb_taxcode_detail
          WHERE taxcode_detail_id = l_tb_tax_matrix.then_taxcode_detail_id;
      EXCEPTION
         WHEN OTHERS THEN
            v_then_taxcode_state_code := NULL;
            v_then_taxcode_type_code := NULL;
            v_then_taxcode_code := NULL;
            v_then_jurisdiction_id := NULL;
            v_then_measure_type_code := NULL;
            v_then_taxtype_code := NULL;
      END;

      -- Get "Else" TaxCode Elements
      BEGIN
         SELECT taxcode_state_code,
                taxcode_type_code,
                taxcode_code,
                jurisdiction_id,
                measure_type_code,
                taxtype_code
           INTO v_else_taxcode_state_code,
                v_else_taxcode_type_code,
                v_else_taxcode_code,
                v_else_jurisdiction_id,
                v_else_measure_type_code,
                v_else_taxtype_code
           FROM tb_taxcode_detail
          WHERE taxcode_detail_id = l_tb_tax_matrix.else_taxcode_detail_id;
      EXCEPTION
         WHEN OTHERS THEN
            v_else_taxcode_state_code := NULL;
            v_else_taxcode_type_code := NULL;
            v_else_taxcode_code := NULL;
            v_else_jurisdiction_id := NULL;
            v_else_measure_type_code := NULL;
            v_else_taxtype_code := NULL;
      END;

      -- Continue if Elements Found
      IF ( v_then_taxcode_state_code IS NOT NULL ) AND
         (( l_tb_tax_matrix.else_taxcode_detail_id IS NULL OR l_tb_tax_matrix.else_taxcode_detail_id = 0 ) OR
          ( l_tb_tax_matrix.else_taxcode_detail_id IS NOT NULL AND l_tb_tax_matrix.else_taxcode_detail_id <> 0 AND v_else_taxcode_state_code IS NOT NULL )) THEN
         -- Determine "Then" or "Else" Results
         v_relation_amount := l_tb_tax_matrix.relation_amount;
         IF l_tb_tax_matrix.relation_sign = 'na' THEN
            v_relation_amount := 0;
            v_less_hold_code_flag := l_tb_tax_matrix.then_hold_code_flag;
            v_less_taxcode_detail_id := l_tb_tax_matrix.then_taxcode_detail_id;
            v_less_taxcode_state_code := v_then_taxcode_state_code;
            v_less_taxcode_type_code := v_then_taxcode_type_code;
            v_less_taxcode_code := v_then_taxcode_code;
            v_less_jurisdiction_id := v_then_jurisdiction_id;
            v_less_measure_type_code := v_then_measure_type_code;
            v_less_taxtype_code := v_then_taxtype_code;
            v_less_cch_taxcat_code := l_tb_tax_matrix.then_cch_taxcat_code;
            v_less_cch_group_code := l_tb_tax_matrix.then_cch_group_code;
            -- v_less_cch_item_code := l_tb_tax_matrix.then_cch_item_code;
            v_equal_hold_code_flag := l_tb_tax_matrix.then_hold_code_flag;
            v_equal_taxcode_detail_id := l_tb_tax_matrix.then_taxcode_detail_id;
            v_equal_taxcode_state_code := v_then_taxcode_state_code;
            v_equal_taxcode_type_code := v_then_taxcode_type_code;
            v_equal_taxcode_code := v_then_taxcode_code;
            v_equal_jurisdiction_id := v_then_jurisdiction_id;
            v_equal_measure_type_code := v_then_measure_type_code;
            v_equal_taxtype_code := v_then_taxtype_code;
            v_equal_cch_taxcat_code := l_tb_tax_matrix.then_cch_taxcat_code;
            v_equal_cch_group_code := l_tb_tax_matrix.then_cch_group_code;
            -- v_equal_cch_item_code := l_tb_tax_matrix.then_cch_item_code;
            v_greater_hold_code_flag := l_tb_tax_matrix.then_hold_code_flag;
            v_greater_taxcode_detail_id := l_tb_tax_matrix.then_taxcode_detail_id;
            v_greater_taxcode_state_code := v_then_taxcode_state_code;
            v_greater_taxcode_type_code := v_then_taxcode_type_code;
            v_greater_taxcode_code := v_then_taxcode_code;
            v_greater_jurisdiction_id := v_then_jurisdiction_id;
            v_greater_measure_type_code := v_then_measure_type_code;
            v_greater_taxtype_code := v_then_taxtype_code;
            v_greater_cch_taxcat_code := l_tb_tax_matrix.then_cch_taxcat_code;
            v_greater_cch_group_code := l_tb_tax_matrix.then_cch_group_code;
            -- v_greater_cch_item_code := l_tb_tax_matrix.then_cch_item_code;
         ELSIF l_tb_tax_matrix.relation_sign = '=' THEN
            v_less_hold_code_flag := l_tb_tax_matrix.else_hold_code_flag;
            v_less_taxcode_detail_id := l_tb_tax_matrix.else_taxcode_detail_id;
            v_less_taxcode_state_code := v_else_taxcode_state_code;
            v_less_taxcode_type_code := v_else_taxcode_type_code;
            v_less_taxcode_code := v_else_taxcode_code;
            v_less_jurisdiction_id := v_else_jurisdiction_id;
            v_less_measure_type_code := v_else_measure_type_code;
            v_less_taxtype_code := v_else_taxtype_code;
            v_less_cch_taxcat_code := l_tb_tax_matrix.else_cch_taxcat_code;
            v_less_cch_group_code := l_tb_tax_matrix.else_cch_group_code;
            -- v_less_cch_item_code := l_tb_tax_matrix.else_cch_item_code;
            v_equal_hold_code_flag := l_tb_tax_matrix.then_hold_code_flag;
            v_equal_taxcode_detail_id := l_tb_tax_matrix.then_taxcode_detail_id;
            v_equal_taxcode_state_code := v_then_taxcode_state_code;
            v_equal_taxcode_type_code := v_then_taxcode_type_code;
            v_equal_taxcode_code := v_then_taxcode_code;
            v_equal_jurisdiction_id := v_then_jurisdiction_id;
            v_equal_measure_type_code := v_then_measure_type_code;
            v_equal_taxtype_code := v_then_taxtype_code;
            v_equal_cch_taxcat_code := l_tb_tax_matrix.then_cch_taxcat_code;
            v_equal_cch_group_code := l_tb_tax_matrix.then_cch_group_code;
            -- v_equal_cch_item_code := l_tb_tax_matrix.then_cch_item_code;
            v_greater_hold_code_flag := l_tb_tax_matrix.else_hold_code_flag;
            v_greater_taxcode_detail_id := l_tb_tax_matrix.else_taxcode_detail_id;
            v_greater_taxcode_state_code := v_else_taxcode_state_code;
            v_greater_taxcode_type_code := v_else_taxcode_type_code;
            v_greater_taxcode_code := v_else_taxcode_code;
            v_greater_jurisdiction_id := v_else_jurisdiction_id;
            v_greater_measure_type_code := v_else_measure_type_code;
            v_greater_taxtype_code := v_else_taxtype_code;
            v_greater_cch_taxcat_code := l_tb_tax_matrix.else_cch_taxcat_code;
            v_greater_cch_group_code := l_tb_tax_matrix.else_cch_group_code;
            -- v_greater_cch_item_code := l_tb_tax_matrix.else_cch_item_code;
         ELSIF l_tb_tax_matrix.relation_sign = '<' THEN
            v_less_hold_code_flag := l_tb_tax_matrix.then_hold_code_flag;
            v_less_taxcode_detail_id := l_tb_tax_matrix.then_taxcode_detail_id;
            v_less_taxcode_state_code := v_then_taxcode_state_code;
            v_less_taxcode_type_code := v_then_taxcode_type_code;
            v_less_taxcode_code := v_then_taxcode_code;
            v_less_jurisdiction_id := v_then_jurisdiction_id;
            v_less_measure_type_code := v_then_measure_type_code;
            v_less_taxtype_code := v_then_taxtype_code;
            v_less_cch_taxcat_code := l_tb_tax_matrix.then_cch_taxcat_code;
            v_less_cch_group_code := l_tb_tax_matrix.then_cch_group_code;
            -- v_less_cch_item_code := l_tb_tax_matrix.then_cch_item_code;
            v_equal_hold_code_flag := l_tb_tax_matrix.else_hold_code_flag;
            v_equal_taxcode_detail_id := l_tb_tax_matrix.else_taxcode_detail_id;
            v_equal_taxcode_state_code := v_else_taxcode_state_code;
            v_equal_taxcode_type_code := v_else_taxcode_type_code;
            v_equal_taxcode_code := v_else_taxcode_code;
            v_equal_jurisdiction_id := v_else_jurisdiction_id;
            v_equal_measure_type_code := v_else_measure_type_code;
            v_equal_taxtype_code := v_else_taxtype_code;
            v_equal_cch_taxcat_code := l_tb_tax_matrix.else_cch_taxcat_code;
            v_equal_cch_group_code := l_tb_tax_matrix.else_cch_group_code;
            -- v_equal_cch_item_code := l_tb_tax_matrix.else_cch_item_code;
            v_greater_hold_code_flag := l_tb_tax_matrix.else_hold_code_flag;
            v_greater_taxcode_detail_id := l_tb_tax_matrix.else_taxcode_detail_id;
            v_greater_taxcode_state_code := v_else_taxcode_state_code;
            v_greater_taxcode_type_code := v_else_taxcode_type_code;
            v_greater_taxcode_code := v_else_taxcode_code;
            v_greater_jurisdiction_id := v_else_jurisdiction_id;
            v_greater_measure_type_code := v_else_measure_type_code;
            v_greater_taxtype_code := v_else_taxtype_code;
            v_greater_cch_taxcat_code := l_tb_tax_matrix.else_cch_taxcat_code;
            v_greater_cch_group_code := l_tb_tax_matrix.else_cch_group_code;
            -- v_greater_cch_item_code := l_tb_tax_matrix.else_cch_item_code;
         ELSIF l_tb_tax_matrix.relation_sign = '<=' THEN
            v_less_hold_code_flag := l_tb_tax_matrix.then_hold_code_flag;
            v_less_taxcode_detail_id := l_tb_tax_matrix.then_taxcode_detail_id;
            v_less_taxcode_state_code := v_then_taxcode_state_code;
            v_less_taxcode_type_code := v_then_taxcode_type_code;
            v_less_taxcode_code := v_then_taxcode_code;
            v_less_jurisdiction_id := v_then_jurisdiction_id;
            v_less_measure_type_code := v_then_measure_type_code;
            v_less_taxtype_code := v_then_taxtype_code;
            v_less_cch_taxcat_code := l_tb_tax_matrix.then_cch_taxcat_code;
            v_less_cch_group_code := l_tb_tax_matrix.then_cch_group_code;
            -- v_less_cch_item_code := l_tb_tax_matrix.then_cch_item_code;
            v_equal_hold_code_flag := l_tb_tax_matrix.then_hold_code_flag;
            v_equal_taxcode_detail_id := l_tb_tax_matrix.then_taxcode_detail_id;
            v_equal_taxcode_state_code := v_then_taxcode_state_code;
            v_equal_taxcode_type_code := v_then_taxcode_type_code;
            v_equal_taxcode_code := v_then_taxcode_code;
            v_equal_jurisdiction_id := v_then_jurisdiction_id;
            v_equal_measure_type_code := v_then_measure_type_code;
            v_equal_taxtype_code := v_then_taxtype_code;
            v_equal_cch_taxcat_code := l_tb_tax_matrix.then_cch_taxcat_code;
            v_equal_cch_group_code := l_tb_tax_matrix.then_cch_group_code;
            -- v_equal_cch_item_code := l_tb_tax_matrix.then_cch_item_code;
            v_greater_hold_code_flag := l_tb_tax_matrix.else_hold_code_flag;
            v_greater_taxcode_detail_id := l_tb_tax_matrix.else_taxcode_detail_id;
            v_greater_taxcode_state_code := v_else_taxcode_state_code;
            v_greater_taxcode_type_code := v_else_taxcode_type_code;
            v_greater_taxcode_code := v_else_taxcode_code;
            v_greater_jurisdiction_id := v_else_jurisdiction_id;
            v_greater_measure_type_code := v_else_measure_type_code;
            v_greater_taxtype_code := v_else_taxtype_code;
            v_greater_cch_taxcat_code := l_tb_tax_matrix.else_cch_taxcat_code;
            v_greater_cch_group_code := l_tb_tax_matrix.else_cch_group_code;
            -- v_greater_cch_item_code := l_tb_tax_matrix.else_cch_item_code;
         ELSIF l_tb_tax_matrix.relation_sign = '>' THEN
            v_less_hold_code_flag := l_tb_tax_matrix.else_hold_code_flag;
            v_less_taxcode_detail_id := l_tb_tax_matrix.else_taxcode_detail_id;
            v_less_taxcode_state_code := v_else_taxcode_state_code;
            v_less_taxcode_type_code := v_else_taxcode_type_code;
            v_less_taxcode_code := v_else_taxcode_code;
            v_less_jurisdiction_id := v_else_jurisdiction_id;
            v_less_measure_type_code := v_else_measure_type_code;
            v_less_taxtype_code := v_else_taxtype_code;
            v_less_cch_taxcat_code := l_tb_tax_matrix.else_cch_taxcat_code;
            v_less_cch_group_code := l_tb_tax_matrix.else_cch_group_code;
            -- v_less_cch_item_code := l_tb_tax_matrix.else_cch_item_code;
            v_equal_hold_code_flag := l_tb_tax_matrix.else_hold_code_flag;
            v_equal_taxcode_detail_id := l_tb_tax_matrix.else_taxcode_detail_id;
            v_equal_taxcode_state_code := v_else_taxcode_state_code;
            v_equal_taxcode_type_code := v_else_taxcode_type_code;
            v_equal_taxcode_code := v_else_taxcode_code;
            v_equal_jurisdiction_id := v_else_jurisdiction_id;
            v_equal_measure_type_code := v_else_measure_type_code;
            v_equal_taxtype_code := v_else_taxtype_code;
            v_equal_cch_taxcat_code := l_tb_tax_matrix.else_cch_taxcat_code;
            v_equal_cch_group_code := l_tb_tax_matrix.else_cch_group_code;
            -- v_equal_cch_item_code := l_tb_tax_matrix.else_cch_item_code;
            v_greater_hold_code_flag := l_tb_tax_matrix.then_hold_code_flag;
            v_greater_taxcode_detail_id := l_tb_tax_matrix.then_taxcode_detail_id;
            v_greater_taxcode_state_code := v_then_taxcode_state_code;
            v_greater_taxcode_type_code := v_then_taxcode_type_code;
            v_greater_taxcode_code := v_then_taxcode_code;
            v_greater_jurisdiction_id := v_then_jurisdiction_id;
            v_greater_measure_type_code := v_then_measure_type_code;
            v_greater_taxtype_code := v_then_taxtype_code;
            v_greater_cch_taxcat_code := l_tb_tax_matrix.then_cch_taxcat_code;
            v_greater_cch_group_code := l_tb_tax_matrix.then_cch_group_code;
            -- v_greater_cch_item_code := l_tb_tax_matrix.then_cch_item_code;
         ELSIF l_tb_tax_matrix.relation_sign = '>=' THEN
            v_less_hold_code_flag := l_tb_tax_matrix.else_hold_code_flag;
            v_less_taxcode_detail_id := l_tb_tax_matrix.else_taxcode_detail_id;
            v_less_taxcode_state_code := v_else_taxcode_state_code;
            v_less_taxcode_type_code := v_else_taxcode_type_code;
            v_less_taxcode_code := v_else_taxcode_code;
            v_less_jurisdiction_id := v_else_jurisdiction_id;
            v_less_measure_type_code := v_else_measure_type_code;
            v_less_taxtype_code := v_else_taxtype_code;
            v_less_cch_taxcat_code := l_tb_tax_matrix.else_cch_taxcat_code;
            v_less_cch_group_code := l_tb_tax_matrix.else_cch_group_code;
            -- v_less_cch_item_code := l_tb_tax_matrix.else_cch_item_code;
            v_equal_hold_code_flag := l_tb_tax_matrix.then_hold_code_flag;
            v_equal_taxcode_detail_id := l_tb_tax_matrix.then_taxcode_detail_id;
            v_equal_taxcode_state_code := v_then_taxcode_state_code;
            v_equal_taxcode_type_code := v_then_taxcode_type_code;
            v_equal_taxcode_code := v_then_taxcode_code;
            v_equal_jurisdiction_id := v_then_jurisdiction_id;
            v_equal_measure_type_code := v_then_measure_type_code;
            v_equal_taxtype_code := v_then_taxtype_code;
            v_equal_cch_taxcat_code := l_tb_tax_matrix.then_cch_taxcat_code;
            v_equal_cch_group_code := l_tb_tax_matrix.then_cch_group_code;
            -- v_equal_cch_item_code := l_tb_tax_matrix.then_cch_item_code;
            v_greater_hold_code_flag := l_tb_tax_matrix.then_hold_code_flag;
            v_greater_taxcode_detail_id := l_tb_tax_matrix.then_taxcode_detail_id;
            v_greater_taxcode_state_code := v_then_taxcode_state_code;
            v_greater_taxcode_type_code := v_then_taxcode_type_code;
            v_greater_taxcode_code := v_then_taxcode_code;
            v_greater_jurisdiction_id := v_then_jurisdiction_id;
            v_greater_measure_type_code := v_then_measure_type_code;
            v_greater_taxtype_code := v_then_taxtype_code;
            v_greater_cch_taxcat_code := l_tb_tax_matrix.then_cch_taxcat_code;
            v_greater_cch_group_code := l_tb_tax_matrix.then_cch_group_code;
            -- v_greater_cch_item_code := l_tb_tax_matrix.then_cch_item_code;
         ELSIF l_tb_tax_matrix.relation_sign = '<>' THEN
            v_less_hold_code_flag := l_tb_tax_matrix.then_hold_code_flag;
            v_less_taxcode_detail_id := l_tb_tax_matrix.then_taxcode_detail_id;
            v_less_taxcode_state_code := v_then_taxcode_state_code;
            v_less_taxcode_type_code := v_then_taxcode_type_code;
            v_less_taxcode_code := v_then_taxcode_code;
            v_less_jurisdiction_id := v_then_jurisdiction_id;
            v_less_measure_type_code := v_then_measure_type_code;
            v_less_taxtype_code := v_then_taxtype_code;
            v_less_cch_taxcat_code := l_tb_tax_matrix.then_cch_taxcat_code;
            v_less_cch_group_code := l_tb_tax_matrix.then_cch_group_code;
            -- v_less_cch_item_code := l_tb_tax_matrix.then_cch_item_code;
            v_equal_hold_code_flag := l_tb_tax_matrix.else_hold_code_flag;
            v_equal_taxcode_detail_id := l_tb_tax_matrix.else_taxcode_detail_id;
            v_equal_taxcode_state_code := v_else_taxcode_state_code;
            v_equal_taxcode_type_code := v_else_taxcode_type_code;
            v_equal_taxcode_code := v_else_taxcode_code;
            v_equal_jurisdiction_id := v_else_jurisdiction_id;
            v_equal_measure_type_code := v_else_measure_type_code;
            v_equal_taxtype_code := v_else_taxtype_code;
            v_equal_cch_taxcat_code := l_tb_tax_matrix.else_cch_taxcat_code;
            v_equal_cch_group_code := l_tb_tax_matrix.else_cch_group_code;
            -- v_equal_cch_item_code := l_tb_tax_matrix.else_cch_item_code;
            v_greater_hold_code_flag := l_tb_tax_matrix.then_hold_code_flag;
            v_greater_taxcode_detail_id := l_tb_tax_matrix.then_taxcode_detail_id;
            v_greater_taxcode_state_code := v_then_taxcode_state_code;
            v_greater_taxcode_type_code := v_then_taxcode_type_code;
            v_greater_taxcode_code := v_then_taxcode_code;
            v_greater_jurisdiction_id := v_then_jurisdiction_id;
            v_greater_measure_type_code := v_then_measure_type_code;
            v_greater_taxtype_code := v_then_taxtype_code;
            v_greater_cch_taxcat_code := l_tb_tax_matrix.then_cch_taxcat_code;
            v_greater_cch_group_code := l_tb_tax_matrix.then_cch_group_code;
            -- v_greater_cch_item_code := l_tb_tax_matrix.then_cch_item_code;
         END IF;

         SELECT DECODE(SIGN(transaction_detail.gl_line_itm_dist_amt - v_relation_amount), -1, v_less_hold_code_flag, 0, v_equal_hold_code_flag, 1, v_greater_hold_code_flag),
                DECODE(SIGN(transaction_detail.gl_line_itm_dist_amt - v_relation_amount), -1, v_less_taxcode_detail_id, 0, v_equal_taxcode_detail_id, 1, v_greater_taxcode_detail_id),
                DECODE(SIGN(transaction_detail.gl_line_itm_dist_amt - v_relation_amount), -1, v_less_taxcode_state_code, 0, v_equal_taxcode_state_code, 1, v_greater_taxcode_state_code),
                DECODE(SIGN(transaction_detail.gl_line_itm_dist_amt - v_relation_amount), -1, v_less_taxcode_type_code, 0, v_equal_taxcode_type_code, 1, v_greater_taxcode_type_code),
                DECODE(SIGN(transaction_detail.gl_line_itm_dist_amt - v_relation_amount), -1, v_less_taxcode_code, 0, v_equal_taxcode_code, 1, v_greater_taxcode_code),
                DECODE(SIGN(transaction_detail.gl_line_itm_dist_amt - v_relation_amount), -1, v_less_jurisdiction_id, 0, v_equal_jurisdiction_id, 1, v_greater_jurisdiction_id),
                DECODE(SIGN(transaction_detail.gl_line_itm_dist_amt - v_relation_amount), -1, v_less_measure_type_code, 0, v_equal_measure_type_code, 1, v_greater_measure_type_code),
                DECODE(SIGN(transaction_detail.gl_line_itm_dist_amt - v_relation_amount), -1, v_less_taxtype_code, 0, v_equal_taxtype_code, 1, v_greater_taxtype_code),
                DECODE(SIGN(transaction_detail.gl_line_itm_dist_amt - v_relation_amount), -1, v_less_cch_taxcat_code, 0, v_equal_cch_taxcat_code, 1, v_greater_cch_taxcat_code),
                DECODE(SIGN(transaction_detail.gl_line_itm_dist_amt - v_relation_amount), -1, v_less_cch_group_code, 0, v_equal_cch_group_code, 1, v_greater_cch_group_code)
           INTO v_hold_code_flag, transaction_detail.taxcode_detail_id, transaction_detail.taxcode_state_code, transaction_detail.taxcode_type_code, transaction_detail.taxcode_code, v_override_jurisdiction_id, transaction_detail.measure_type_code, v_taxtype_code, transaction_detail.cch_taxcat_code, transaction_detail.cch_group_code
           FROM DUAL;

         -- ****** DETERMINE TYPE of TAXCODE ****** --
         -- The Tax Code is Taxable
         IF transaction_detail.taxcode_type_code = 'T' THEN
            -- If Override jurisdiction id is not blank or 0 then use it instead of location matrix
            IF v_override_jurisdiction_id IS NOT NULL AND v_override_jurisdiction_id <> 0 THEN
               transaction_detail.jurisdiction_id := v_override_jurisdiction_id;
               transaction_detail.manual_jurisdiction_ind := 'AO';
               location_matrix.override_taxtype_code := '*NO';
               location_matrix.state_flag := '1';                             -- MBF01
               location_matrix.county_flag := '1';                            -- MBF01
               location_matrix.county_local_flag := '1';                      -- MBF01
               location_matrix.city_flag := '1';                              -- MBF01
               location_matrix.city_local_flag := '1';                        -- MBF01
            ELSE
               -- Search for Location Matrix if not already found AND not already manually applied!!
               IF transaction_detail.location_matrix_id IS NULL AND transaction_detail.jurisdiction_id IS NULL THEN
                  v_transaction_detail_id := transaction_detail.transaction_detail_id;
                  -- Search Location Matrix for matches
                  BEGIN
                      OPEN location_matrix_cursor
                       FOR vc_location_matrix_stmt
                     USING v_transaction_detail_id;
                     FETCH location_matrix_cursor
                      INTO location_matrix;
                     -- Rows found?
                     IF location_matrix_cursor%FOUND THEN
                        vn_fetch_rows := location_matrix_cursor%ROWCOUNT;
                     ELSE
                        vn_fetch_rows := 0;
                     END IF;
                     CLOSE location_matrix_cursor;
                     IF SQLCODE != 0 THEN
                        RAISE e_badread;
                     END IF;
                  EXCEPTION
                     WHEN e_badread THEN
                        vn_fetch_rows := 0;
                  END;
                  -- Location Matrix Line Found
                  IF vn_fetch_rows > 0 THEN
                     transaction_detail.location_matrix_id := location_matrix.location_matrix_id;
                     transaction_detail.jurisdiction_id := location_matrix.jurisdiction_id;
                  ELSE
                     transaction_detail.transaction_ind := 'S';
                     transaction_detail.suspend_ind := 'L';
                  END IF;
               -- Get Location Matrix if it exists AND not already manually applied!!
               ELSIF transaction_detail.location_matrix_id IS NOT NULL AND (v_override_jurisdiction_id IS NULL OR v_override_jurisdiction_id = 0) THEN
                  BEGIN
                     SELECT override_taxtype_code,
                            state_flag, county_flag, county_local_flag, city_flag, city_local_flag
                       INTO location_matrix.override_taxtype_code,
                            location_matrix.state_flag,
                            location_matrix.county_flag,
                            location_matrix.county_local_flag,
                            location_matrix.city_flag,
                            location_matrix.city_local_flag
                       FROM tb_location_matrix
                      WHERE tb_location_matrix.location_matrix_id = transaction_detail.location_matrix_id;
                  EXCEPTION
                     WHEN NO_DATA_FOUND THEN
                        location_matrix.override_taxtype_code := '*NO';
                        location_matrix.state_flag := '1';
                        location_matrix.county_flag := '1';
                        location_matrix.county_local_flag := '1';
                        location_matrix.city_flag := '1';
                        location_matrix.city_local_flag := '1';
                  END;
               ELSE
                  location_matrix.override_taxtype_code := '*NO';
                  location_matrix.state_flag := '1';
                  location_matrix.county_flag := '1';
                  location_matrix.county_local_flag := '1';
                  location_matrix.city_flag := '1';
                  location_matrix.city_local_flag := '1';
               END IF;
            END IF;

            -- Continue if not Suspended
            IF transaction_detail.transaction_ind IS NULL OR transaction_detail.transaction_ind <> 'S' THEN
               -- Get Jurisdiction TaxRates and calculate Tax Amounts
               sp_getusetax(transaction_detail.jurisdiction_id,
                            transaction_detail.gl_date,
                            transaction_detail.measure_type_code,
                            transaction_detail.gl_line_itm_dist_amt,
                            v_taxtype_code,
                            location_matrix.override_taxtype_code,
                            location_matrix.state_flag,
                            location_matrix.county_flag,
                            location_matrix.county_local_flag,
                            location_matrix.city_flag,
                            location_matrix.city_local_flag,
                            transaction_detail.invoice_freight_amt,
                            transaction_detail.invoice_discount_amt,
                            transaction_detail.transaction_state_code,
-- future? --                     transaction_detail.import_definition_code,
                            'importdefn',
                            transaction_detail.taxcode_code,
                            transaction_detail.jurisdiction_taxrate_id,
                            transaction_detail.state_use_amount,
                            transaction_detail.state_use_tier2_amount,
                            transaction_detail.state_use_tier3_amount,
                            transaction_detail.county_use_amount,
                            transaction_detail.county_local_use_amount,
                            transaction_detail.city_use_amount,
                            transaction_detail.city_local_use_amount,
                            transaction_detail.state_use_rate,
                            transaction_detail.state_use_tier2_rate,
                            transaction_detail.state_use_tier3_rate,
                            transaction_detail.state_split_amount,
                            transaction_detail.state_tier2_min_amount,
                            transaction_detail.state_tier2_max_amount,
                            transaction_detail.state_maxtax_amount,
                            transaction_detail.county_use_rate,
                            transaction_detail.county_local_use_rate,
                            transaction_detail.county_split_amount,
                            transaction_detail.county_maxtax_amount,
                            transaction_detail.county_single_flag,
                            transaction_detail.county_default_flag,
                            transaction_detail.city_use_rate,
                            transaction_detail.city_local_use_rate,
                            transaction_detail.city_split_amount,
                            transaction_detail.city_split_use_rate,
                            transaction_detail.city_single_flag,
                            transaction_detail.city_default_flag,
                            transaction_detail.combined_use_rate,
                            transaction_detail.tb_calc_tax_amt,
-- future? --                     transaction_detail.taxable_amt,
-- Targa? --                transaction_detail.user_number_10,
                            vn_taxable_amt,
                            v_taxtype_used );

               -- Suspend if tax rate id is null
               IF transaction_detail.jurisdiction_taxrate_id IS NULL OR transaction_detail.jurisdiction_taxrate_id = 0 THEN
                  transaction_detail.transaction_ind := 'S';
                  transaction_detail.suspend_ind := 'R';
               ELSE
                  -- Check for Taxable Amount -- 3351
                  IF vn_taxable_amt <> transaction_detail.gl_line_itm_dist_amt THEN
                     transaction_detail.gl_extract_amt := transaction_detail.gl_line_itm_dist_amt;
                     transaction_detail.gl_line_itm_dist_amt := vn_taxable_amt;
                  END IF;
               END IF;
            END IF;

         -- The Tax Code is Exempt
         ELSIF transaction_detail.taxcode_type_code = 'E' THEN
            transaction_detail.transaction_ind := 'P';

         -- The Tax Code is a Question
         ELSIF transaction_detail.taxcode_type_code = 'Q' THEN
           transaction_detail.transaction_ind := 'Q';

         -- The Tax Code is Unrecognized - Suspend
         ELSE
            transaction_detail.transaction_ind := 'S';
            transaction_detail.suspend_ind := '?';
         END IF;

         -- Continue if not Suspended
         IF transaction_detail.transaction_ind IS NULL OR transaction_detail.transaction_ind <> 'S' THEN
            -- Check for matrix "H"old
            IF ( v_hold_code_flag = '1' ) AND
               ( transaction_detail.taxcode_type_code = 'T' OR transaction_detail.taxcode_type_code = 'E') THEN
                  transaction_detail.transaction_ind := 'H';
            ELSE
                transaction_detail.transaction_ind := 'P';
            END IF;
         END IF;

      ELSE
         -- Elements NOT Found
         transaction_detail.transaction_ind := 'S';
         transaction_detail.suspend_ind := '?';
      END IF;

      -- Update transaction detail row
      BEGIN
         -- 3411 - add taxable amount and tax type used.
         UPDATE tb_transaction_detail
            SET gl_line_itm_dist_amt = transaction_detail.gl_line_itm_dist_amt,
                tb_calc_tax_amt = transaction_detail.tb_calc_tax_amt,
                state_use_amount = transaction_detail.state_use_amount,
                state_use_tier2_amount = transaction_detail.state_use_tier2_amount,
                state_use_tier3_amount = transaction_detail.state_use_tier3_amount,
                county_use_amount = transaction_detail.county_use_amount,
                county_local_use_amount = transaction_detail.county_local_use_amount,
                city_use_amount = transaction_detail.city_use_amount,
                city_local_use_amount = transaction_detail.city_local_use_amount,
                transaction_state_code = transaction_detail.transaction_state_code,
                transaction_ind = transaction_detail.transaction_ind,
                suspend_ind = transaction_detail.suspend_ind,
                taxcode_detail_id = transaction_detail.taxcode_detail_id,
                taxcode_state_code = transaction_detail.taxcode_state_code,
                taxcode_type_code = transaction_detail.taxcode_type_code,
                taxcode_code = transaction_detail.taxcode_code,
                cch_taxcat_code = transaction_detail.cch_taxcat_code,
                cch_group_code = transaction_detail.cch_group_code,
                tax_matrix_id = transaction_detail.tax_matrix_id,
                location_matrix_id = transaction_detail.location_matrix_id,
                jurisdiction_id = transaction_detail.jurisdiction_id,
                jurisdiction_taxrate_id = transaction_detail.jurisdiction_taxrate_id,
                manual_jurisdiction_ind = transaction_detail.manual_jurisdiction_ind,
                measure_type_code = transaction_detail.measure_type_code,
                state_use_rate = transaction_detail.state_use_rate,
                state_use_tier2_rate = transaction_detail.state_use_tier2_rate,
                state_use_tier3_rate = transaction_detail.state_use_tier3_rate,
                state_split_amount = transaction_detail.state_split_amount,
                state_tier2_min_amount = transaction_detail.state_tier2_min_amount,
                state_tier2_max_amount = transaction_detail.state_tier2_max_amount,
                state_maxtax_amount = transaction_detail.state_maxtax_amount,
                county_use_rate = transaction_detail.county_use_rate,
                county_local_use_rate = transaction_detail.county_local_use_rate,
                county_split_amount = transaction_detail.county_split_amount,
                county_maxtax_amount = transaction_detail.county_maxtax_amount,
                county_single_flag = transaction_detail.county_single_flag,
                county_default_flag = transaction_detail.county_default_flag,
                city_use_rate = transaction_detail.city_use_rate,
                city_local_use_rate = transaction_detail.city_local_use_rate,
                city_split_amount = transaction_detail.city_split_amount,
                city_split_use_rate = transaction_detail.city_split_use_rate,
                city_single_flag = transaction_detail.city_single_flag,
                city_default_flag = transaction_detail.city_default_flag,
                combined_use_rate = transaction_detail.combined_use_rate,
                gl_extract_amt = transaction_detail.gl_extract_amt,
                update_user_id = transaction_detail.update_user_id,
                update_timestamp = transaction_detail.update_timestamp
          WHERE transaction_detail_id = transaction_detail.transaction_detail_id;
         -- Error Checking
--         IF SQLCODE != 0 THEN
--            RAISE e_badupdate;
--         END IF;
--      EXCEPTION
--         WHEN e_badupdate THEN
--            NULL;
      END;
   END LOOP;
   CLOSE transaction_detail_cursor;

END IF;

--EXCEPTION
--  WHEN OTHERS THEN
--vc_test1 := substr(vc_transaction_detail_stmt,1,4000);
--vc_test2 := substr(vc_transaction_detail_stmt,4001,4000);
--insert into tb_debug(row_joe, row_bob) values(vc_test1, vc_test2);
--      NULL;
END;
/

SHOW ERRORS;

CREATE OR REPLACE PROCEDURE sp_tb_transaction_detail_a_u
		(
		 p_transaction_detail_id IN tb_transaction_detail_excp.transaction_detail_id%TYPE,
		 p_old_taxcode_detail_id IN tb_transaction_detail_excp.old_taxcode_detail_id%TYPE,
		 p_old_taxcode_state_code IN tb_transaction_detail_excp.old_taxcode_state_code%TYPE,
		 p_old_taxcode_type_code IN tb_transaction_detail_excp.old_taxcode_type_code%TYPE,
		 p_old_taxcode_code IN tb_transaction_detail_excp.old_taxcode_code%TYPE,
		 p_old_jurisdiction_id IN tb_transaction_detail_excp.old_jurisdiction_id%TYPE,
		 p_old_jurisdiction_taxrate_id IN tb_transaction_detail_excp.old_jurisdiction_taxrate_id%TYPE,
		 p_old_cch_taxcat_code IN tb_transaction_detail_excp.old_cch_taxcat_code%TYPE,
		 p_old_cch_group_code IN tb_transaction_detail_excp.old_cch_group_code%TYPE,
		 p_old_cch_item_code IN tb_transaction_detail_excp.old_cch_item_code%TYPE,
		 p_new_taxcode_detail_id IN tb_transaction_detail_excp.new_taxcode_detail_id%TYPE,
		 p_new_taxcode_state_code IN tb_transaction_detail_excp.new_taxcode_state_code%TYPE,
		 p_new_taxcode_type_code IN tb_transaction_detail_excp.new_taxcode_type_code%TYPE,
		 p_new_taxcode_code IN tb_transaction_detail_excp.new_taxcode_code%TYPE,
		 p_new_jurisdiction_id IN tb_transaction_detail_excp.new_jurisdiction_id%TYPE,
		 p_new_jurisdiction_taxrate_id IN tb_transaction_detail_excp.new_jurisdiction_taxrate_id%TYPE,
		 p_new_cch_taxcat_code IN tb_transaction_detail_excp.new_cch_taxcat_code%TYPE,
		 p_new_cch_group_code IN tb_transaction_detail_excp.new_cch_group_code%TYPE,
		 p_new_cch_item_code IN tb_transaction_detail_excp.new_cch_item_code%TYPE
		)

	AS
	BEGIN
	   IF (( p_old_taxcode_detail_id IS NOT NULL ) AND p_old_taxcode_detail_id <> p_new_taxcode_detail_id ) OR
	      (( p_old_jurisdiction_id IS NOT NULL ) AND p_old_jurisdiction_id <> p_new_jurisdiction_id ) OR
	      (( p_old_jurisdiction_taxrate_id IS NOT NULL ) AND p_old_jurisdiction_taxrate_id <> p_new_jurisdiction_taxrate_id ) THEN

	      INSERT INTO tb_transaction_detail_excp (
		 transaction_detail_excp_id,
		 transaction_detail_id,
		 old_taxcode_detail_id,
		 old_taxcode_state_code,
		 old_taxcode_type_code,
		 old_taxcode_code,
		 old_jurisdiction_id,
		 old_jurisdiction_taxrate_id,
		 old_cch_taxcat_code,
		 old_cch_group_code,
		 old_cch_item_code,
		 new_taxcode_detail_id,
		 new_taxcode_state_code,
		 new_taxcode_type_code,
		 new_taxcode_code,
		 new_jurisdiction_id,
		 new_jurisdiction_taxrate_id,
		 new_cch_taxcat_code,
		 new_cch_group_code,
		 new_cch_item_code,
		 update_user_id,
		 update_timestamp )
	      VALUES (
		 SQ_TB_TRANS_DETAIL_EXCP_ID.NEXTVAL,
		 p_transaction_detail_id,
		 p_old_taxcode_detail_id,
		 p_old_taxcode_state_code,
		 p_old_taxcode_type_code,
		 p_old_taxcode_code,
		 p_old_jurisdiction_id,
		 p_old_jurisdiction_taxrate_id,
		 p_old_cch_taxcat_code,
		 p_old_cch_group_code,
		 p_old_cch_item_code,
		 p_new_taxcode_detail_id,
		 p_new_taxcode_state_code,
		 p_new_taxcode_type_code,
		 p_new_taxcode_code,
		 p_new_jurisdiction_id,
		 p_new_jurisdiction_taxrate_id,
		 p_new_cch_taxcat_code,
		 p_new_cch_group_code,
		 p_new_cch_item_code,
		 USER,
		 SYSDATE );
	   END IF;
	END;
/

SHOW ERRORS;

CREATE OR REPLACE PROCEDURE sp_tb_transaction_detail_b_u(
	p_old_tb_transaction_detail IN     tb_transaction_detail%ROWTYPE,
	p_new_tb_transaction_detail IN OUT tb_transaction_detail%ROWTYPE
) AS
/* ************************************************************************************************/
/* Object Type/Name: Trigger/Before Update - tb_transaction_detail_b_u                            */
/* Author:           Michael B. Fuller                                                            */
/* Date:             a long time ago                                                              */
/* Description:      A transaction has been changed                                               */
/* Arguments:        None                                                                         */
/* Returns:          None                                                                         */
/* ************************************************************************************************/
/* Maintenance History:                                                                           */
/* Scan  Author       Date     Release    Description                                  Ticket #   */
/* ----- ---------- ---------- ---------- -------------------------------------------- ---------- */
/*       M. Fuller  06/02/2006 3.3.5.0    Fix processing an allocated line - Temporary 871        */
/*       M. Fuller  01/04/2007 3.3.5.1    Fix processing an allocated line - Permenant 871        */
/* MBF01 M. Fuller  02/09/2007 3.3.5.1    Correct allocated lines with no locn matrix id          */
/* MBF02 M. Fuller  08/13/2007 3.4.1.1    Add 5 Calc Tax Flags                         883        */
/* MBF03 M. Fuller  03/06/2008 3.4.2.1    Correct If/Then/Else's for "AO"              913        */
/* JFF   J. Franco  12/17/2008 x.x.x.x    Convert trigger to stored procedure          xxx        */
/* ************************************************************************************************/

-- Define Cursor Type
   TYPE cursor_type is REF CURSOR;

-- Location Matrix Selection SQL      // MBF02
   vc_location_matrix_select       VARCHAR2(1000) :=
      'SELECT tb_location_matrix.location_matrix_id, ' ||
             'tb_location_matrix.jurisdiction_id, ' ||
             'tb_location_matrix.override_taxtype_code, ' ||
             'tb_location_matrix.state_flag,' ||
             'tb_location_matrix.county_flag,' ||
             'tb_location_matrix.county_local_flag,' ||
             'tb_location_matrix.city_flag,' ||
             'tb_location_matrix.city_local_flag,' ||
             'tb_jurisdiction.state, ' ||
             'tb_jurisdiction.county, ' ||
             'tb_jurisdiction.city, ' ||
             'tb_jurisdiction.zip, ' ||
             'tb_jurisdiction.in_out ' ||
        'FROM tb_location_matrix, ' ||
             'tb_jurisdiction, ' ||
             'tb_tmp_transaction_detail ' ||
      'WHERE ( tb_tmp_transaction_detail.transaction_detail_id = :v_transaction_detail_id ) AND ' ||
            '( tb_location_matrix.default_flag = ''0'' AND tb_location_matrix.binary_weight > 0 ) AND ' ||
            '( tb_location_matrix.effective_date <= tb_tmp_transaction_detail.gl_date ) AND ( tb_location_matrix.expiration_date >= tb_tmp_transaction_detail.gl_date ) AND ' ||
            '( tb_location_matrix.jurisdiction_id = tb_jurisdiction.jurisdiction_id ) ';
   vc_location_matrix_where        VARCHAR2(3000) := '';
   vc_location_matrix_orderby      VARCHAR2(1000) :=
      'ORDER BY tb_location_matrix.binary_weight DESC, tb_location_matrix.significant_digits DESC, tb_location_matrix.effective_date DESC';
   vc_location_matrix_stmt         VARCHAR2 (5000);
   location_matrix_cursor          cursor_type;

-- Location Matrix Record TYPE
   /*TYPE location_matrix_record is RECORD (
      location_matrix_id           tb_location_matrix.location_matrix_id%TYPE,
      jurisdiction_id              tb_location_matrix.jurisdiction_id%TYPE,
      override_taxtype_code        tb_location_matrix.override_taxtype_code%TYPE,
      state_flag                   tb_location_matrix.state_flag%TYPE,
      county_flag                  tb_location_matrix.county_flag%TYPE,
      county_local_flag            tb_location_matrix.county_local_flag%TYPE,
      city_flag                    tb_location_matrix.city_flag%TYPE,
      city_local_flag              tb_location_matrix.city_local_flag%TYPE,
      state                        tb_jurisdiction.state%TYPE,
      county                       tb_jurisdiction.county%TYPE,
      city                         tb_jurisdiction.city%TYPE,
      zip                          tb_jurisdiction.zip%TYPE,
      in_out                       tb_jurisdiction.in_out%TYPE );*/
   location_matrix                 matrix_record_pkg.location_matrix_record;

-- Tax Matrix Selection SQL
   vc_tax_matrix_select            VARCHAR2(2000) :=
      'SELECT tb_tax_matrix.tax_matrix_id, tb_tax_matrix.relation_sign, tb_tax_matrix.relation_amount, ' ||
             'tb_tax_matrix.then_hold_code_flag, tb_tax_matrix.else_hold_code_flag, ' ||
             'tb_tax_matrix.then_taxcode_detail_id, tb_tax_matrix.else_taxcode_detail_id, ' ||
             'tb_tax_matrix.then_cch_taxcat_code, tb_tax_matrix.then_cch_group_code, tb_tax_matrix.then_cch_item_code, ' ||
             'tb_tax_matrix.else_cch_taxcat_code, tb_tax_matrix.else_cch_group_code, tb_tax_matrix.else_cch_item_code, ' ||
             'thentaxcddtl.taxcode_state_code, thentaxcddtl.taxcode_type_code, thentaxcddtl.taxcode_code, thentaxcddtl.jurisdiction_id, thentaxcddtl.measure_type_code, thentaxcddtl.taxtype_code, ' ||
             'elsetaxcddtl.taxcode_state_code, elsetaxcddtl.taxcode_type_code, elsetaxcddtl.taxcode_code, elsetaxcddtl.jurisdiction_id, elsetaxcddtl.measure_type_code, elsetaxcddtl.taxtype_code ' ||
       'FROM tb_tax_matrix, ' ||
            'tb_taxcode_detail thentaxcddtl, ' ||
            'tb_taxcode_detail elsetaxcddtl, ' ||
            'tb_tmp_transaction_detail ' ||
      'WHERE ( tb_tmp_transaction_detail.transaction_detail_id = :v_transaction_detail_id ) AND ' ||
            '( tb_tax_matrix.default_flag = ''0'' AND tb_tax_matrix.binary_weight > 0 ) AND ' ||
            '(( tb_tax_matrix.matrix_state_code = ''*ALL'' ) OR ( tb_tax_matrix.matrix_state_code = :v_transaction_state_code )) AND ' ||
            '( tb_tax_matrix.effective_date <= tb_tmp_transaction_detail.gl_date ) AND ( tb_tax_matrix.expiration_date >= tb_tmp_transaction_detail.gl_date ) AND ' ||
            '( tb_tax_matrix.then_taxcode_detail_id = thentaxcddtl.taxcode_detail_id (+)) AND ' ||
            '( tb_tax_matrix.else_taxcode_detail_id = elsetaxcddtl.taxcode_detail_id (+)) ';
   vc_tax_matrix_where             VARCHAR2(6000) := '';
   vc_tax_matrix_orderby           VARCHAR2(1000) :=
      'ORDER BY tb_tax_matrix.binary_weight DESC, tb_tax_matrix.significant_digits DESC, tb_tax_matrix.effective_date DESC';
   vc_tax_matrix_stmt              VARCHAR2 (9000);
   tax_matrix_cursor               cursor_type;

-- Tax Matrix Record TYPE
   /*TYPE tax_matrix_record is RECORD (
      tax_matrix_id                tb_tax_matrix.tax_matrix_id%TYPE,
      relation_sign                tb_tax_matrix.relation_sign%TYPE,
      relation_amount              tb_tax_matrix.relation_amount%TYPE,
      then_hold_code_flag          tb_tax_matrix.then_hold_code_flag%TYPE,
      else_hold_code_flag          tb_tax_matrix.else_hold_code_flag%TYPE,
      then_taxcode_detail_id       tb_bcp_transactions.taxcode_detail_id%TYPE,
      else_taxcode_detail_id       tb_bcp_transactions.taxcode_detail_id%TYPE,
      then_cch_taxcat_code         tb_tax_matrix.then_cch_taxcat_code%TYPE,
      then_cch_group_code          tb_tax_matrix.then_cch_group_code%TYPE,
      then_cch_item_code           tb_tax_matrix.then_cch_item_code%TYPE,
      else_cch_taxcat_code         tb_tax_matrix.else_cch_taxcat_code%TYPE,
      else_cch_group_code          tb_tax_matrix.else_cch_group_code%TYPE,
      else_cch_item_code           tb_tax_matrix.else_cch_item_code%TYPE,
      then_taxcode_state_code      tb_bcp_transactions.taxcode_state_code%TYPE,
      then_taxcode_type_code       tb_bcp_transactions.taxcode_type_code%TYPE,
      then_taxcode_code            tb_bcp_transactions.taxcode_code%TYPE,
      then_jurisdiction_id         tb_bcp_transactions.jurisdiction_id%TYPE,
      then_measure_type_code       tb_bcp_transactions.measure_type_code%TYPE,
      then_taxtype_code            tb_taxcode.taxtype_code%TYPE,
      else_taxcode_state_code      tb_bcp_transactions.taxcode_state_code%TYPE,
      else_taxcode_type_code       tb_bcp_transactions.taxcode_type_code%TYPE,
      else_taxcode_code            tb_bcp_transactions.taxcode_code%TYPE,
      else_jurisdiction_id         tb_bcp_transactions.jurisdiction_id%TYPE,
      else_measure_type_code       tb_bcp_transactions.measure_type_code%TYPE,
      else_taxtype_code            tb_taxcode.taxtype_code%TYPE );*/
   tax_matrix                      matrix_record_pkg.tax_matrix_record;

-- Table defined variables
   v_hold_code_flag                tb_tax_matrix.then_hold_code_flag%TYPE            := NULL;
   v_less_hold_code_flag           tb_tax_matrix.then_hold_code_flag%TYPE            := NULL;
   v_less_taxcode_detail_id        tb_tax_matrix.then_taxcode_detail_id%TYPE         := NULL;
   v_less_cch_taxcat_code          tb_tax_matrix.then_cch_taxcat_code%TYPE           := NULL;
   v_less_cch_group_code           tb_tax_matrix.then_cch_group_code%TYPE            := NULL;
   v_equal_hold_code_flag          tb_tax_matrix.then_hold_code_flag%TYPE            := NULL;
   v_equal_taxcode_detail_id       tb_tax_matrix.then_taxcode_detail_id%TYPE         := NULL;
   v_equal_cch_taxcat_code         tb_tax_matrix.then_cch_taxcat_code%TYPE           := NULL;
   v_equal_cch_group_code          tb_tax_matrix.then_cch_group_code%TYPE            := NULL;
   v_greater_hold_code_flag        tb_tax_matrix.then_hold_code_flag%TYPE            := NULL;
   v_greater_taxcode_detail_id     tb_tax_matrix.then_taxcode_detail_id%TYPE         := NULL;
   v_greater_cch_taxcat_code       tb_tax_matrix.then_cch_taxcat_code%TYPE           := NULL;
   v_greater_cch_group_code        tb_tax_matrix.then_cch_group_code%TYPE            := NULL;
   v_jurisdiction_id               tb_jurisdiction.jurisdiction_id%TYPE              := NULL;
   v_less_taxcode_state_code       tb_taxcode_detail.taxcode_state_code%TYPE         := NULL;
   v_less_taxcode_type_code        tb_taxcode_detail.taxcode_type_code%TYPE          := NULL;
   v_less_taxcode_code             tb_taxcode_detail.taxcode_code%TYPE               := NULL;
   v_less_jurisdiction_id          tb_taxcode_detail.jurisdiction_id%TYPE            := NULL;
   v_less_measure_type_code        tb_taxcode_detail.measure_type_code%TYPE          := NULL;
   v_less_taxtype_code             tb_taxcode_detail.taxtype_code%TYPE               := NULL;
   v_equal_taxcode_state_code      tb_taxcode_detail.taxcode_state_code%TYPE         := NULL;
   v_equal_taxcode_type_code       tb_taxcode_detail.taxcode_type_code%TYPE          := NULL;
   v_equal_taxcode_code            tb_taxcode_detail.taxcode_code%TYPE               := NULL;
   v_equal_jurisdiction_id         tb_taxcode_detail.jurisdiction_id%TYPE            := NULL;
   v_equal_measure_type_code       tb_taxcode_detail.measure_type_code%TYPE          := NULL;
   v_equal_taxtype_code            tb_taxcode_detail.taxtype_code%TYPE               := NULL;
   v_greater_taxcode_state_code    tb_taxcode_detail.taxcode_state_code%TYPE         := NULL;
   v_greater_taxcode_type_code     tb_taxcode_detail.taxcode_type_code%TYPE          := NULL;
   v_greater_taxcode_code          tb_taxcode_detail.taxcode_code%TYPE               := NULL;
   v_greater_jurisdiction_id       tb_taxcode_detail.jurisdiction_id%TYPE            := NULL;
   v_greater_measure_type_code     tb_taxcode_detail.measure_type_code%TYPE          := NULL;
   v_greater_taxtype_code          tb_taxcode_detail.taxtype_code%TYPE               := NULL;
   v_transaction_detail_id         tb_transaction_detail.transaction_detail_id%TYPE  := NULL;
   v_transaction_state_code        tb_transaction_detail.transaction_state_code%TYPE := NULL;
   v_override_jurisdiction_id      tb_transaction_detail.jurisdiction_id%TYPE        := NULL;
   v_sysdate                       tb_transaction_detail.load_timestamp%TYPE         := SYSDATE;
   v_sysdate_plus                  tb_transaction_detail.load_timestamp%TYPE         := v_sysdate + (1/86400);
   v_taxtype_code                  tb_taxcode.taxtype_code%TYPE                      := NULL;
   v_taxtype_used                  tb_taxcode.taxtype_code%TYPE                      := NULL;

-- Program defined variables
   vn_fetch_rows                   NUMBER                                            := 0;
   vc_state_driver_flag            CHAR(1)                                           := '0';
-- temporary
   vn_taxable_amt                  NUMBER                                            := 0;

-- Define Location Driver Names Cursor (tb_driver_names)
   /*CURSOR location_driver_cursor
   IS
        SELECT driver.trans_dtl_column_name, driver.matrix_column_name, driver.null_driver_flag, driver.wildcard_flag,
               datadefcol.desc_column_name, datadefcol.data_type
          FROM tb_driver_names driver,
               (SELECT *
                  FROM tb_data_def_column
                 WHERE table_name = 'TB_TRANSACTION_DETAIL') datadefcol
         WHERE driver.driver_names_code = 'L' AND
               driver.trans_dtl_column_name = datadefcol.column_name
      ORDER BY driver.driver_id;
      location_driver              location_driver_cursor%ROWTYPE;*/

-- Define Tax Driver Names Cursor (tb_driver_names)
   /*CURSOR tax_driver_cursor
   IS
        SELECT driver.trans_dtl_column_name, driver.matrix_column_name, driver.null_driver_flag, driver.wildcard_flag, driver.range_flag, to_number_flag,
               datadefcol.desc_column_name, datadefcol.data_type
          FROM tb_driver_names driver,
               (SELECT *
                  FROM tb_data_def_column
                 WHERE table_name = 'TB_TRANSACTION_DETAIL') datadefcol
         WHERE driver.driver_names_code = 'T' AND
               driver.trans_dtl_column_name = datadefcol.column_name
      ORDER BY driver.driver_id;
      tax_driver                   tax_driver_cursor%ROWTYPE;*/

-- Define Exceptions
   e_badread                       exception;

-- ****** Program starts ****** --------------------------------------------------------------------
BEGIN
   p_new_tb_transaction_detail.update_user_id   := USER;
   p_new_tb_transaction_detail.update_timestamp := v_sysdate;

   -- If was processed and now processed OR
   --    was tax suspended and now processed and now manual taxcode indicator not null OR
   --    was locn suspended and now processed and now manual jurisdiction indicator not null THEN
--   IF ( UPPER(p_old_tb_transaction_detail.transaction_ind) = 'P' AND UPPER(p_new_tb_transaction_detail.transaction_ind) = 'P' ) OR
--      ( UPPER(p_old_tb_transaction_detail.transaction_ind) = 'S' AND UPPER(p_old_tb_transaction_detail.suspend_ind) = 'T' AND p_new_tb_transaction_detail.manual_taxcode_ind IS NOT NULL ) OR
--      ( UPPER(p_old_tb_transaction_detail.transaction_ind) = 'S' AND UPPER(p_old_tb_transaction_detail.suspend_ind) = 'L' AND p_new_tb_transaction_detail.manual_jurisdiction_ind IS NOT NULL ) THEN
   IF (( NOT ( UPPER(p_old_tb_transaction_detail.transaction_ind) = 'P' AND UPPER(p_new_tb_transaction_detail.transaction_ind) = 'S' )) AND
      ((( p_old_tb_transaction_detail.taxcode_detail_id IS NOT NULL AND p_old_tb_transaction_detail.taxcode_detail_id <> p_new_tb_transaction_detail.taxcode_detail_id ) OR
        ( p_old_tb_transaction_detail.taxcode_detail_id IS NOT NULL AND p_new_tb_transaction_detail.taxcode_detail_id IS NULL ) OR
        ( p_old_tb_transaction_detail.taxcode_detail_id IS NULL AND p_new_tb_transaction_detail.taxcode_detail_id IS NOT NULL AND p_new_tb_transaction_detail.manual_taxcode_ind IS NOT NULL )) OR
       (( p_old_tb_transaction_detail.jurisdiction_id IS NOT NULL AND p_old_tb_transaction_detail.jurisdiction_id <> p_new_tb_transaction_detail.jurisdiction_id ) OR
        ( p_old_tb_transaction_detail.jurisdiction_id IS NOT NULL AND p_new_tb_transaction_detail.jurisdiction_id IS NULL ) OR
        ( p_old_tb_transaction_detail.jurisdiction_id IS NULL AND p_new_tb_transaction_detail.jurisdiction_id IS NOT NULL AND p_new_tb_transaction_detail.manual_jurisdiction_ind IS NOT NULL AND p_new_tb_transaction_detail.manual_jurisdiction_ind <> 'AO' )) OR
       (( (p_old_tb_transaction_detail.jurisdiction_taxrate_id IS NOT NULL AND p_old_tb_transaction_detail.jurisdiction_taxrate_id <> 0) AND p_old_tb_transaction_detail.jurisdiction_taxrate_id <> p_new_tb_transaction_detail.jurisdiction_taxrate_id ) OR
        ( (p_old_tb_transaction_detail.jurisdiction_taxrate_id IS NOT NULL AND p_old_tb_transaction_detail.jurisdiction_taxrate_id <> 0) AND (p_new_tb_transaction_detail.jurisdiction_taxrate_id IS NULL OR p_new_tb_transaction_detail.jurisdiction_taxrate_id = 0) ))))
   AND NOT (( UPPER(p_old_tb_transaction_detail.transaction_ind) = 'S' AND UPPER(p_new_tb_transaction_detail.transaction_ind) = 'S' ) AND
       (( p_old_tb_transaction_detail.taxcode_detail_id IS NOT NULL AND p_new_tb_transaction_detail.taxcode_detail_id IS NULL ) OR
        ( p_old_tb_transaction_detail.jurisdiction_id IS NOT NULL AND p_new_tb_transaction_detail.jurisdiction_id IS NULL ))) THEN
      -- Initialize Transaction Indicator and Suspended Indicator
      p_new_tb_transaction_detail.transaction_ind := NULL;
      p_new_tb_transaction_detail.suspend_ind := NULL;
      -- Insert New Tax Matrix columns into temporary table -------------------------------------------
      -- 3411 - add taxable amount and tax type used.
      INSERT INTO tb_tmp_transaction_detail (
         transaction_detail_id,
         source_transaction_id,
         process_batch_no,
         gl_extract_batch_no,
         archive_batch_no,
         allocation_matrix_id,
         allocation_subtrans_id,
         entered_date,
         transaction_status,
         gl_date,
         gl_company_nbr,
         gl_company_name,
         gl_division_nbr,
         gl_division_name,
         gl_cc_nbr_dept_id,
         gl_cc_nbr_dept_name,
         gl_local_acct_nbr,
         gl_local_acct_name,
         gl_local_sub_acct_nbr,
         gl_local_sub_acct_name,
         gl_full_acct_nbr,
         gl_full_acct_name,
         gl_line_itm_dist_amt,
         orig_gl_line_itm_dist_amt,
         vendor_nbr,
         vendor_name,
         vendor_address_line_1,
         vendor_address_line_2,
         vendor_address_line_3,
         vendor_address_line_4,
         vendor_address_city,
         vendor_address_county,
         vendor_address_state,
         vendor_address_zip,
         vendor_address_country,
         vendor_type,
         vendor_type_name,
         invoice_nbr,
         invoice_desc,
         invoice_date,
         invoice_freight_amt,
         invoice_discount_amt,
         invoice_tax_amt,
         invoice_total_amt,
         invoice_tax_flg,
         invoice_line_nbr,
         invoice_line_name,
         invoice_line_type,
         invoice_line_type_name,
         invoice_line_amt,
         invoice_line_tax,
         afe_project_nbr,
         afe_project_name,
         afe_category_nbr,
         afe_category_name,
         afe_sub_cat_nbr,
         afe_sub_cat_name,
         afe_use,
         afe_contract_type,
         afe_contract_structure,
         afe_property_cat,
         inventory_nbr,
         inventory_name,
         inventory_class,
         inventory_class_name,
         po_nbr,
         po_name,
         po_date,
         po_line_nbr,
         po_line_name,
         po_line_type,
         po_line_type_name,
         ship_to_location,
         ship_to_location_name,
         ship_to_address_line_1,
         ship_to_address_line_2,
         ship_to_address_line_3,
         ship_to_address_line_4,
         ship_to_address_city,
         ship_to_address_county,
         ship_to_address_state,
         ship_to_address_zip,
         ship_to_address_country,
         wo_nbr,
         wo_name,
         wo_date,
         wo_type,
         wo_type_desc,
         wo_class,
         wo_class_desc,
         wo_entity,
         wo_entity_desc,
         wo_line_nbr,
         wo_line_name,
         wo_line_type,
         wo_line_type_desc,
         wo_shut_down_cd,
         wo_shut_down_cd_desc,
         voucher_id,
         voucher_name,
         voucher_date,
         voucher_line_nbr,
         voucher_line_desc,
         check_nbr,
         check_no,
         check_date,
         check_amt,
         check_desc,
         user_text_01,
         user_text_02,
         user_text_03,
         user_text_04,
         user_text_05,
         user_text_06,
         user_text_07,
         user_text_08,
         user_text_09,
         user_text_10,
         user_text_11,
         user_text_12,
         user_text_13,
         user_text_14,
         user_text_15,
         user_text_16,
         user_text_17,
         user_text_18,
         user_text_19,
         user_text_20,
         user_text_21,
         user_text_22,
         user_text_23,
         user_text_24,
         user_text_25,
         user_text_26,
         user_text_27,
         user_text_28,
         user_text_29,
         user_text_30,
         user_number_01,
         user_number_02,
         user_number_03,
         user_number_04,
         user_number_05,
         user_number_06,
         user_number_07,
         user_number_08,
         user_number_09,
         user_number_10,
         user_date_01,
         user_date_02,
         user_date_03,
         user_date_04,
         user_date_05,
         user_date_06,
         user_date_07,
         user_date_08,
         user_date_09,
         user_date_10,
         comments,
         tb_calc_tax_amt,
         state_use_amount,
         state_use_tier2_amount,
         state_use_tier3_amount,
         county_use_amount,
         county_local_use_amount,
         city_use_amount,
         city_local_use_amount,
         transaction_state_code,
         transaction_ind,
         suspend_ind,
         taxcode_detail_id,
         taxcode_state_code,
         taxcode_type_code,
         taxcode_code,
         cch_taxcat_code,
         cch_group_code,
         cch_item_code,
         manual_taxcode_ind,
         tax_matrix_id,
         location_matrix_id,
         jurisdiction_id,
         jurisdiction_taxrate_id,
         manual_jurisdiction_ind,
         measure_type_code,
         state_use_rate,
         state_use_tier2_rate,
         state_use_tier3_rate,
         state_split_amount,
         state_tier2_min_amount,
         state_tier2_max_amount,
         state_maxtax_amount,
         county_use_rate,
         county_local_use_rate,
         county_split_amount,
         county_maxtax_amount,
         county_single_flag,
         county_default_flag,
         city_use_rate,
         city_local_use_rate,
         city_split_amount,
         city_split_use_rate,
         city_single_flag,
         city_default_flag,
         combined_use_rate,
         load_timestamp,
         gl_extract_updater,
         gl_extract_timestamp,
         gl_extract_flag,
         gl_log_flag,
         gl_extract_amt,
         audit_flag,
         audit_user_id,
         audit_timestamp,
         modify_user_id,
         modify_timestamp,
         update_user_id,
         update_timestamp )
      VALUES (
         p_new_tb_transaction_detail.transaction_detail_id,
         p_new_tb_transaction_detail.source_transaction_id,
         p_new_tb_transaction_detail.process_batch_no,
         p_new_tb_transaction_detail.gl_extract_batch_no,
         p_new_tb_transaction_detail.archive_batch_no,
         p_new_tb_transaction_detail.allocation_matrix_id,
         p_new_tb_transaction_detail.allocation_subtrans_id,
         p_new_tb_transaction_detail.entered_date,
         p_new_tb_transaction_detail.transaction_status,
         p_new_tb_transaction_detail.gl_date,
         p_new_tb_transaction_detail.gl_company_nbr,
         p_new_tb_transaction_detail.gl_company_name,
         p_new_tb_transaction_detail.gl_division_nbr,
         p_new_tb_transaction_detail.gl_division_name,
         p_new_tb_transaction_detail.gl_cc_nbr_dept_id,
         p_new_tb_transaction_detail.gl_cc_nbr_dept_name,
         p_new_tb_transaction_detail.gl_local_acct_nbr,
         p_new_tb_transaction_detail.gl_local_acct_name,
         p_new_tb_transaction_detail.gl_local_sub_acct_nbr,
         p_new_tb_transaction_detail.gl_local_sub_acct_name,
         p_new_tb_transaction_detail.gl_full_acct_nbr,
         p_new_tb_transaction_detail.gl_full_acct_name,
         p_new_tb_transaction_detail.gl_line_itm_dist_amt,
         p_new_tb_transaction_detail.orig_gl_line_itm_dist_amt,
         p_new_tb_transaction_detail.vendor_nbr,
         p_new_tb_transaction_detail.vendor_name,
         p_new_tb_transaction_detail.vendor_address_line_1,
         p_new_tb_transaction_detail.vendor_address_line_2,
         p_new_tb_transaction_detail.vendor_address_line_3,
         p_new_tb_transaction_detail.vendor_address_line_4,
         p_new_tb_transaction_detail.vendor_address_city,
         p_new_tb_transaction_detail.vendor_address_county,
         p_new_tb_transaction_detail.vendor_address_state,
         p_new_tb_transaction_detail.vendor_address_zip,
         p_new_tb_transaction_detail.vendor_address_country,
         p_new_tb_transaction_detail.vendor_type,
         p_new_tb_transaction_detail.vendor_type_name,
         p_new_tb_transaction_detail.invoice_nbr,
         p_new_tb_transaction_detail.invoice_desc,
         p_new_tb_transaction_detail.invoice_date,
         p_new_tb_transaction_detail.invoice_freight_amt,
         p_new_tb_transaction_detail.invoice_discount_amt,
         p_new_tb_transaction_detail.invoice_tax_amt,
         p_new_tb_transaction_detail.invoice_total_amt,
         p_new_tb_transaction_detail.invoice_tax_flg,
         p_new_tb_transaction_detail.invoice_line_nbr,
         p_new_tb_transaction_detail.invoice_line_name,
         p_new_tb_transaction_detail.invoice_line_type,
         p_new_tb_transaction_detail.invoice_line_type_name,
         p_new_tb_transaction_detail.invoice_line_amt,
         p_new_tb_transaction_detail.invoice_line_tax,
         p_new_tb_transaction_detail.afe_project_nbr,
         p_new_tb_transaction_detail.afe_project_name,
         p_new_tb_transaction_detail.afe_category_nbr,
         p_new_tb_transaction_detail.afe_category_name,
         p_new_tb_transaction_detail.afe_sub_cat_nbr,
         p_new_tb_transaction_detail.afe_sub_cat_name,
         p_new_tb_transaction_detail.afe_use,
         p_new_tb_transaction_detail.afe_contract_type,
         p_new_tb_transaction_detail.afe_contract_structure,
         p_new_tb_transaction_detail.afe_property_cat,
         p_new_tb_transaction_detail.inventory_nbr,
         p_new_tb_transaction_detail.inventory_name,
         p_new_tb_transaction_detail.inventory_class,
         p_new_tb_transaction_detail.inventory_class_name,
         p_new_tb_transaction_detail.po_nbr,
         p_new_tb_transaction_detail.po_name,
         p_new_tb_transaction_detail.po_date,
         p_new_tb_transaction_detail.po_line_nbr,
         p_new_tb_transaction_detail.po_line_name,
         p_new_tb_transaction_detail.po_line_type,
         p_new_tb_transaction_detail.po_line_type_name,
         p_new_tb_transaction_detail.ship_to_location,
         p_new_tb_transaction_detail.ship_to_location_name,
         p_new_tb_transaction_detail.ship_to_address_line_1,
         p_new_tb_transaction_detail.ship_to_address_line_2,
         p_new_tb_transaction_detail.ship_to_address_line_3,
         p_new_tb_transaction_detail.ship_to_address_line_4,
         p_new_tb_transaction_detail.ship_to_address_city,
         p_new_tb_transaction_detail.ship_to_address_county,
         p_new_tb_transaction_detail.ship_to_address_state,
         p_new_tb_transaction_detail.ship_to_address_zip,
         p_new_tb_transaction_detail.ship_to_address_country,
         p_new_tb_transaction_detail.wo_nbr,
         p_new_tb_transaction_detail.wo_name,
         p_new_tb_transaction_detail.wo_date,
         p_new_tb_transaction_detail.wo_type,
         p_new_tb_transaction_detail.wo_type_desc,
         p_new_tb_transaction_detail.wo_class,
         p_new_tb_transaction_detail.wo_class_desc,
         p_new_tb_transaction_detail.wo_entity,
         p_new_tb_transaction_detail.wo_entity_desc,
         p_new_tb_transaction_detail.wo_line_nbr,
         p_new_tb_transaction_detail.wo_line_name,
         p_new_tb_transaction_detail.wo_line_type,
         p_new_tb_transaction_detail.wo_line_type_desc,
         p_new_tb_transaction_detail.wo_shut_down_cd,
         p_new_tb_transaction_detail.wo_shut_down_cd_desc,
         p_new_tb_transaction_detail.voucher_id,
         p_new_tb_transaction_detail.voucher_name,
         p_new_tb_transaction_detail.voucher_date,
         p_new_tb_transaction_detail.voucher_line_nbr,
         p_new_tb_transaction_detail.voucher_line_desc,
         p_new_tb_transaction_detail.check_nbr,
         p_new_tb_transaction_detail.check_no,
         p_new_tb_transaction_detail.check_date,
         p_new_tb_transaction_detail.check_amt,
         p_new_tb_transaction_detail.check_desc,
         p_new_tb_transaction_detail.user_text_01,
         p_new_tb_transaction_detail.user_text_02,
         p_new_tb_transaction_detail.user_text_03,
         p_new_tb_transaction_detail.user_text_04,
         p_new_tb_transaction_detail.user_text_05,
         p_new_tb_transaction_detail.user_text_06,
         p_new_tb_transaction_detail.user_text_07,
         p_new_tb_transaction_detail.user_text_08,
         p_new_tb_transaction_detail.user_text_09,
         p_new_tb_transaction_detail.user_text_10,
         p_new_tb_transaction_detail.user_text_11,
         p_new_tb_transaction_detail.user_text_12,
         p_new_tb_transaction_detail.user_text_13,
         p_new_tb_transaction_detail.user_text_14,
         p_new_tb_transaction_detail.user_text_15,
         p_new_tb_transaction_detail.user_text_16,
         p_new_tb_transaction_detail.user_text_17,
         p_new_tb_transaction_detail.user_text_18,
         p_new_tb_transaction_detail.user_text_19,
         p_new_tb_transaction_detail.user_text_20,
         p_new_tb_transaction_detail.user_text_21,
         p_new_tb_transaction_detail.user_text_22,
         p_new_tb_transaction_detail.user_text_23,
         p_new_tb_transaction_detail.user_text_24,
         p_new_tb_transaction_detail.user_text_25,
         p_new_tb_transaction_detail.user_text_26,
         p_new_tb_transaction_detail.user_text_27,
         p_new_tb_transaction_detail.user_text_28,
         p_new_tb_transaction_detail.user_text_29,
         p_new_tb_transaction_detail.user_text_30,
         p_new_tb_transaction_detail.user_number_01,
         p_new_tb_transaction_detail.user_number_02,
         p_new_tb_transaction_detail.user_number_03,
         p_new_tb_transaction_detail.user_number_04,
         p_new_tb_transaction_detail.user_number_05,
         p_new_tb_transaction_detail.user_number_06,
         p_new_tb_transaction_detail.user_number_07,
         p_new_tb_transaction_detail.user_number_08,
         p_new_tb_transaction_detail.user_number_09,
         p_new_tb_transaction_detail.user_number_10,
         p_new_tb_transaction_detail.user_date_01,
         p_new_tb_transaction_detail.user_date_02,
         p_new_tb_transaction_detail.user_date_03,
         p_new_tb_transaction_detail.user_date_04,
         p_new_tb_transaction_detail.user_date_05,
         p_new_tb_transaction_detail.user_date_06,
         p_new_tb_transaction_detail.user_date_07,
         p_new_tb_transaction_detail.user_date_08,
         p_new_tb_transaction_detail.user_date_09,
         p_new_tb_transaction_detail.user_date_10,
         p_new_tb_transaction_detail.comments,
         p_new_tb_transaction_detail.tb_calc_tax_amt,
         p_new_tb_transaction_detail.state_use_amount,
         p_new_tb_transaction_detail.state_use_tier2_amount,
         p_new_tb_transaction_detail.state_use_tier3_amount,
         p_new_tb_transaction_detail.county_use_amount,
         p_new_tb_transaction_detail.county_local_use_amount,
         p_new_tb_transaction_detail.city_use_amount,
         p_new_tb_transaction_detail.city_local_use_amount,
         p_new_tb_transaction_detail.transaction_state_code,
         p_new_tb_transaction_detail.transaction_ind,
         p_new_tb_transaction_detail.suspend_ind,
         p_new_tb_transaction_detail.taxcode_detail_id,
         p_new_tb_transaction_detail.taxcode_state_code,
         p_new_tb_transaction_detail.taxcode_type_code,
         p_new_tb_transaction_detail.taxcode_code,
         p_new_tb_transaction_detail.cch_taxcat_code,
         p_new_tb_transaction_detail.cch_group_code,
         p_new_tb_transaction_detail.cch_item_code,
         p_new_tb_transaction_detail.manual_taxcode_ind,
         p_new_tb_transaction_detail.tax_matrix_id,
         p_new_tb_transaction_detail.location_matrix_id,
         p_new_tb_transaction_detail.jurisdiction_id,
         p_new_tb_transaction_detail.jurisdiction_taxrate_id,
         p_new_tb_transaction_detail.manual_jurisdiction_ind,
         p_new_tb_transaction_detail.measure_type_code,
         p_new_tb_transaction_detail.state_use_rate,
         p_new_tb_transaction_detail.state_use_tier2_rate,
         p_new_tb_transaction_detail.state_use_tier3_rate,
         p_new_tb_transaction_detail.state_split_amount,
         p_new_tb_transaction_detail.state_tier2_min_amount,
         p_new_tb_transaction_detail.state_tier2_max_amount,
         p_new_tb_transaction_detail.state_maxtax_amount,
         p_new_tb_transaction_detail.county_use_rate,
         p_new_tb_transaction_detail.county_local_use_rate,
         p_new_tb_transaction_detail.county_split_amount,
         p_new_tb_transaction_detail.county_maxtax_amount,
         p_new_tb_transaction_detail.county_single_flag,
         p_new_tb_transaction_detail.county_default_flag,
         p_new_tb_transaction_detail.city_use_rate,
         p_new_tb_transaction_detail.city_local_use_rate,
         p_new_tb_transaction_detail.city_split_amount,
         p_new_tb_transaction_detail.city_split_use_rate,
         p_new_tb_transaction_detail.city_single_flag,
         p_new_tb_transaction_detail.city_default_flag,
         p_new_tb_transaction_detail.combined_use_rate,
         p_new_tb_transaction_detail.load_timestamp,
         p_new_tb_transaction_detail.gl_extract_updater,
         p_new_tb_transaction_detail.gl_extract_timestamp,
         p_new_tb_transaction_detail.gl_extract_flag,
         p_new_tb_transaction_detail.gl_log_flag,
         p_new_tb_transaction_detail.gl_extract_amt,
         p_new_tb_transaction_detail.audit_flag,
         p_new_tb_transaction_detail.audit_user_id,
         p_new_tb_transaction_detail.audit_timestamp,
         p_new_tb_transaction_detail.modify_user_id,
         p_new_tb_transaction_detail.modify_timestamp,
         p_new_tb_transaction_detail.update_user_id,
         p_new_tb_transaction_detail.update_timestamp );

      -- If the new TaxCode Type is "T"axable then zero Tax Amounts
      IF UPPER(p_new_tb_transaction_detail.taxcode_type_code) = 'T' OR UPPER(p_old_tb_transaction_detail.taxcode_type_code) = 'T' THEN
         p_new_tb_transaction_detail.tb_calc_tax_amt := 0;
-- future?? --         p_new_tb_transaction_detail.taxable_amt := 0;
         p_new_tb_transaction_detail.state_use_amount := 0;
         p_new_tb_transaction_detail.state_use_tier2_amount := 0;
         p_new_tb_transaction_detail.state_use_tier3_amount := 0;
         p_new_tb_transaction_detail.county_use_amount := 0;
         p_new_tb_transaction_detail.county_local_use_amount := 0;
         p_new_tb_transaction_detail.city_use_amount := 0;
         p_new_tb_transaction_detail.city_local_use_amount := 0;
         p_new_tb_transaction_detail.state_use_rate := 0;
         p_new_tb_transaction_detail.state_use_tier2_rate := 0;
         p_new_tb_transaction_detail.state_use_tier3_rate := 0;
         p_new_tb_transaction_detail.state_split_amount := 0;
         p_new_tb_transaction_detail.state_tier2_min_amount := 0;
         p_new_tb_transaction_detail.state_tier2_max_amount := 0;
         p_new_tb_transaction_detail.state_maxtax_amount := 0;
         p_new_tb_transaction_detail.county_use_rate := 0;
         p_new_tb_transaction_detail.county_local_use_rate := 0;
         p_new_tb_transaction_detail.county_split_amount := 0;
         p_new_tb_transaction_detail.county_maxtax_amount := 0;
         p_new_tb_transaction_detail.county_single_flag := '0';
         p_new_tb_transaction_detail.county_default_flag := '0';
         p_new_tb_transaction_detail.city_use_rate := 0;
         p_new_tb_transaction_detail.city_local_use_rate := 0;
         p_new_tb_transaction_detail.city_split_amount := 0;
         p_new_tb_transaction_detail.city_split_use_rate := 0;
         p_new_tb_transaction_detail.city_single_flag := '0';
         p_new_tb_transaction_detail.city_default_flag := '0';
         p_new_tb_transaction_detail.combined_use_rate := 0;
         -- Replace original distribution amount if populated -- 3351
         IF p_new_tb_transaction_detail.gl_extract_amt IS NOT NULL AND p_new_tb_transaction_detail.gl_extract_amt <> 0 THEN
            p_new_tb_transaction_detail.gl_line_itm_dist_amt := p_new_tb_transaction_detail.gl_extract_amt;
         END IF;
         p_new_tb_transaction_detail.gl_extract_amt := 0;
      END IF;

      -- If the new TaxCode Detail ID is null then search tax matrix
      IF p_new_tb_transaction_detail.taxcode_detail_id IS NULL THEN
         v_transaction_detail_id := p_new_tb_transaction_detail.transaction_detail_id;
         v_transaction_state_code := p_new_tb_transaction_detail.transaction_state_code;
         -- ***** Create Dynamic WHERE Clause for Matrix SQLs ***** --
         -- Does Transaction Record have a valid State?
         IF v_transaction_state_code IS NULL THEN
            vn_fetch_rows := 0;
         ELSE
            SELECT count(*)
              INTO vn_fetch_rows
              FROM tb_taxcode_state
             WHERE taxcode_state_code = v_transaction_state_code;
         END IF;
         IF vn_fetch_rows = 0 THEN
            -- Get State from new Jurisdiction
            v_transaction_state_code := NULL;
            BEGIN
               SELECT state
                 INTO v_transaction_state_code
                 FROM tb_jurisdiction
                WHERE jurisdiction_id = p_new_tb_transaction_detail.jurisdiction_id;
               IF SQLCODE != 0 THEN
                  RAISE e_badread;
               END IF;
            EXCEPTION
               WHEN e_badread THEN
                  v_transaction_state_code := NULL;
               WHEN NO_DATA_FOUND THEN
                  v_transaction_state_code := NULL;
            END;
            -- Jurisdiction Line Found
            IF v_transaction_state_code IS NOT NULL THEN
               IF p_new_tb_transaction_detail.transaction_state_code IS NULL THEN
                  p_new_tb_transaction_detail.auto_transaction_state_code := '*NULL';
               ELSE
                  p_new_tb_transaction_detail.auto_transaction_state_code := p_new_tb_transaction_detail.transaction_state_code;
               END IF;
               p_new_tb_transaction_detail.transaction_state_code := v_transaction_state_code;
            ELSE
               p_new_tb_transaction_detail.transaction_ind := 'S';
               p_new_tb_transaction_detail.suspend_ind := 'L';
            END IF;
         END IF;

         -- Continue if not Suspended
         IF p_new_tb_transaction_detail.transaction_ind IS NULL OR p_new_tb_transaction_detail.transaction_ind <> 'S' THEN

   -- *******************************************************************************************************************************************
   -- NEW BATCH PROCESS CODE --------------------------------------------------------------------------------------------------------------------
   -- ***** Create Dynamic WHERE Clause for Transaction Detail SQL ***** --
   -- Tax Matrix Drivers
   /*OPEN tax_driver_cursor;
   LOOP
      FETCH tax_driver_cursor INTO tax_driver;
      EXIT WHEN tax_driver_cursor%NOTFOUND;
      IF tax_driver.range_flag = '1' THEN
         IF tax_driver.to_number_flag = '1' THEN
            IF tax_driver.null_driver_flag = '1' THEN
               -- Range and ToNumber and NULL
               IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                  UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                  vc_tax_matrix_where := vc_tax_matrix_where || 'AND ( ' ||
                  '( tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU = ''*NA'' AND ' ||
                     '( tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NULL AND ( tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*NULL'' )) OR ' ||
                     '( tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND ( tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR DECODE(isnumber(tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || '),1,to_char(tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ',''00000000000000000000.00000000000000000000''),UPPER(tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ')) = DECODE(isnumber(tb_tax_matrix.' || tax_driver.matrix_column_name || '),1,to_char(tb_tax_matrix.' || tax_driver.matrix_column_name || ',''00000000000000000000.00000000000000000000''),UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') ))) OR ' ||
                  '( tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU <> ''*NA'' AND ' ||
                     '( tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND (DECODE(isnumber(tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || '),1,to_char(tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ',''00000000000000000000.00000000000000000000''),UPPER(tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ')) >= DECODE(isnumber(tb_tax_matrix.' || tax_driver.matrix_column_name || '),1,to_char(tb_tax_matrix.' || tax_driver.matrix_column_name || ',''00000000000000000000.00000000000000000000''),UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') ) AND ' ||
                                                                                            'DECODE(isnumber(tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || '),1,to_char(tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ',''00000000000000000000.00000000000000000000''),UPPER(tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ')) <= DECODE(isnumber(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU ),1,to_char(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU,''00000000000000000000.00000000000000000000''),UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU)) )))) ';
               END IF;
            ELSE
               -- Range and ToNumber and NOT NULL
               IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                  UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                  vc_tax_matrix_where := vc_tax_matrix_where || 'AND ( ' ||
                  '( tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU = ''*NA'' AND (tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR DECODE(isnumber(tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || '),1,to_char(tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ',''00000000000000000000.00000000000000000000''),UPPER(tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ')) = DECODE(isnumber(tb_tax_matrix.' || tax_driver.matrix_column_name || '),1,to_char(tb_tax_matrix.' || tax_driver.matrix_column_name || ',''00000000000000000000.00000000000000000000''),UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ')))) OR ' ||
                  '( tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU <> ''*NA'' AND (DECODE(isnumber(tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || '),1,to_char(tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ',''00000000000000000000.00000000000000000000''),UPPER(tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ')) >= DECODE(isnumber(tb_tax_matrix.' || tax_driver.matrix_column_name || '),1,to_char(tb_tax_matrix.' || tax_driver.matrix_column_name || ',''00000000000000000000.00000000000000000000''),UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ')) AND ' ||
                                                                                        'DECODE(isnumber(tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || '),1,to_char(tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ',''00000000000000000000.00000000000000000000''),UPPER(tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ')) <= DECODE(isnumber(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU ),1,to_char(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU,''00000000000000000000.00000000000000000000''),UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU)) )) ';
               END IF;
            END IF;
         ELSE
            IF tax_driver.null_driver_flag = '1' THEN
               IF tax_driver.wildcard_flag = '1' THEN
                  -- Range and NOT ToNumber and NULL and Wildcard
                  IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                     UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                     vc_tax_matrix_where := vc_tax_matrix_where || 'AND ( ' ||
                     '( tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU = ''*NA'' AND ' ||
                        '( tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NULL AND (tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*NULL'' )) OR ' ||
                        '( tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND (tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR UPPER(tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ') LIKE UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ')))) OR ' ||
                     '( tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU <> ''*NA'' AND (tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND (UPPER(tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ') >= UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') AND UPPER(tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ') <= UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU) ))) ';
                  END IF;
               ELSE
                  -- Range and NOT ToNumber and NULL and NOT Wildcard
                  IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                     UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                     vc_tax_matrix_where := vc_tax_matrix_where || 'AND ( ' ||
                     '( tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU = ''*NA'' AND ' ||
                        '( tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NULL AND (tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*NULL'' )) OR ' ||
                        '( tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND (tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR UPPER(tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ') = UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ')))) OR ' ||
                     '( tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU <> ''*NA'' AND (tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND (UPPER(tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ') >= UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') AND UPPER(tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ') <= UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU) ))) ';
                  END IF;
               END IF;
            ELSE
               IF tax_driver.wildcard_flag = '1' THEN
                  -- Range and NOT ToNumber and NOT NULL and Wildcard
                  IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                     UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                     vc_tax_matrix_where := vc_tax_matrix_where || 'AND ( ' ||
                     '( tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU = ''*NA'' AND ' ||
                        '(tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR UPPER(tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ') LIKE UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '))) OR ' ||
                     '( tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU <> ''*NA'' AND (UPPER(tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ') >= UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') AND UPPER(tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ') <= UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU) )) ';
                  END IF;
               ELSE
                  -- Range and NOT ToNumber and NOT NULL and NOT Wildcard
                  IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                     UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                     vc_tax_matrix_where := vc_tax_matrix_where || 'AND ( ' ||
                     '( tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU = ''*NA'' AND ' ||
                        '(tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR UPPER(tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ') = UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '))) OR ' ||
                     '( tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU <> ''*NA'' AND (UPPER(tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ') >= UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') AND UPPER(tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ') <= UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU) )) ';
                  END IF;
               END IF;
            END IF;
         END IF;
      ELSE
         IF tax_driver.null_driver_flag = '1' THEN
            IF tax_driver.wildcard_flag = '1' THEN
               -- NOT Range and NULL and Wildcard
               IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                  UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                     vc_tax_matrix_where := vc_tax_matrix_where || 'AND ( ' ||
                     '( tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NULL AND (tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*NULL'' )) OR ' ||
                     '( tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND (tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR UPPER(tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ') LIKE UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ')) ) ';
               END IF;
            ELSE
               -- NOT Range and NULL and NOT Wildcard
               IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                  UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                     vc_tax_matrix_where := vc_tax_matrix_where || 'AND ( ' ||
                     '( tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NULL AND (tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*NULL'' )) OR ' ||
                     '( tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND (tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR UPPER(tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ') = UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ')) ) ';
               END IF;
            END IF;
         ELSE
            IF tax_driver.wildcard_flag = '1' THEN
               -- NOT Range and NOT NULL and Wildcard
               IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                  UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                     vc_tax_matrix_where := vc_tax_matrix_where || 'AND ( ' ||
                     '( tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR UPPER(tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ') LIKE UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') ) ';
               END IF;
            ELSE
               -- NOT Range and NOT NULL and NOT Wildcard
               IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                  UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                  IF tax_driver.trans_dtl_column_name = 'TRANSACTION_STATE_CODE' THEN
                     IF vc_state_driver_flag <> '1' THEN
                        vc_state_driver_flag := '1';
                     END IF;
                     vc_tax_matrix_where := vc_tax_matrix_where || 'AND ( ' ||
                        '( tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR UPPER(:v_transaction_state_code) = UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ')) ';
                  ELSE
                     vc_tax_matrix_where := vc_tax_matrix_where || 'AND ( ' ||
                        '( tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR UPPER(tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ') = UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') ) ';
                  END IF;
               END IF;
            END IF;
         END IF;
      END IF;
      vc_tax_matrix_where := vc_tax_matrix_where || ') ';
   END LOOP;
   CLOSE tax_driver_cursor;*/

   -- New call to sp_gen_tax_driver for MidTier Cleanup
   sp_gen_tax_driver (
	p_generate_driver_reference	=> 'N',
	p_an_batch_id			=> NULL,
	p_transaction_table_name	=> 'tb_tmp_transaction_detail',
	p_tax_table_name		=> 'tb_tax_matrix',
	p_vc_state_driver_flag		=> vc_state_driver_flag,
	p_vc_tax_matrix_where		=> vc_tax_matrix_where);

   -- NEW BATCH PROCESS CODE --------------------------------------------------------------------------------------------------------------------
   -- *******************************************************************************************************************************************

            -- If no drivers found raise error, else create location matrix sql statement
            IF vc_tax_matrix_where IS NULL THEN
               NULL;
            ELSE
               vc_tax_matrix_stmt := vc_tax_matrix_select || vc_tax_matrix_where || vc_tax_matrix_orderby;
            END IF;

            -- Search Tax Matrix for matches
            BEGIN
               IF vc_state_driver_flag = '1' THEN
                   OPEN tax_matrix_cursor
                    FOR vc_tax_matrix_stmt
                  USING v_transaction_detail_id, v_transaction_state_code, v_transaction_state_code;
                  FETCH tax_matrix_cursor
                   INTO tax_matrix;
               ELSE
                   OPEN tax_matrix_cursor
                    FOR vc_tax_matrix_stmt
                  USING v_transaction_detail_id, v_transaction_state_code;
                  FETCH tax_matrix_cursor
                   INTO tax_matrix;
               END IF;
               -- Rows found?
               IF tax_matrix_cursor%FOUND THEN
                  vn_fetch_rows := tax_matrix_cursor%ROWCOUNT;
               ELSE
                  vn_fetch_rows := 0;
               END IF;
               CLOSE tax_matrix_cursor;
               IF SQLCODE != 0 THEN
                  RAISE e_badread;
               END IF;
            EXCEPTION
               WHEN e_badread THEN
                  vn_fetch_rows := 0;
            END;

            -- Tax Matrix Line Found
            IF vn_fetch_rows > 0 THEN
               p_new_tb_transaction_detail.tax_matrix_id := tax_matrix.tax_matrix_id;
               -- Determine "Then" or "Else" Results
               IF tax_matrix.relation_sign = 'na' THEN
                  tax_matrix.relation_amount := 0;
                  v_less_hold_code_flag := tax_matrix.then_hold_code_flag;
                  v_less_taxcode_detail_id := tax_matrix.then_taxcode_detail_id;
                  v_less_taxcode_state_code := tax_matrix.then_taxcode_state_code;
                  v_less_taxcode_type_code := tax_matrix.then_taxcode_type_code;
                  v_less_taxcode_code := tax_matrix.then_taxcode_code;
                  v_less_jurisdiction_id := tax_matrix.then_jurisdiction_id;
                  v_less_measure_type_code := tax_matrix.then_measure_type_code;
                  v_less_taxtype_code := tax_matrix.then_taxtype_code;
                  v_less_cch_taxcat_code := tax_matrix.then_cch_taxcat_code;
                  v_less_cch_group_code := tax_matrix.then_cch_group_code;
                  -- v_less_cch_item_code := tax_matrix.then_cch_item_code;
                  v_equal_hold_code_flag := tax_matrix.then_hold_code_flag;
                  v_equal_taxcode_detail_id := tax_matrix.then_taxcode_detail_id;
                  v_equal_taxcode_state_code := tax_matrix.then_taxcode_state_code;
                  v_equal_taxcode_type_code := tax_matrix.then_taxcode_type_code;
                  v_equal_taxcode_code := tax_matrix.then_taxcode_code;
                  v_equal_jurisdiction_id := tax_matrix.then_jurisdiction_id;
                  v_equal_measure_type_code := tax_matrix.then_measure_type_code;
                  v_equal_taxtype_code := tax_matrix.then_taxtype_code;
                  v_equal_cch_taxcat_code := tax_matrix.then_cch_taxcat_code;
                  v_equal_cch_group_code := tax_matrix.then_cch_group_code;
                  -- v_equal_cch_item_code := tax_matrix.then_cch_item_code;
                  v_greater_hold_code_flag := tax_matrix.then_hold_code_flag;
                  v_greater_taxcode_detail_id := tax_matrix.then_taxcode_detail_id;
                  v_greater_taxcode_state_code := tax_matrix.then_taxcode_state_code;
                  v_greater_taxcode_type_code := tax_matrix.then_taxcode_type_code;
                  v_greater_taxcode_code := tax_matrix.then_taxcode_code;
                  v_greater_jurisdiction_id := tax_matrix.then_jurisdiction_id;
                  v_greater_measure_type_code := tax_matrix.then_measure_type_code;
                  v_greater_taxtype_code := tax_matrix.then_taxtype_code;
                  v_greater_cch_taxcat_code := tax_matrix.then_cch_taxcat_code;
                  v_greater_cch_group_code := tax_matrix.then_cch_group_code;
                  -- v_greater_cch_item_code := tax_matrix.then_cch_item_code;
               ELSIF tax_matrix.relation_sign = '=' THEN
                  v_less_hold_code_flag := tax_matrix.else_hold_code_flag;
                  v_less_taxcode_detail_id := tax_matrix.else_taxcode_detail_id;
                  v_less_taxcode_state_code := tax_matrix.else_taxcode_state_code;
                  v_less_taxcode_type_code := tax_matrix.else_taxcode_type_code;
                  v_less_taxcode_code := tax_matrix.else_taxcode_code;
                  v_less_jurisdiction_id := tax_matrix.else_jurisdiction_id;
                  v_less_measure_type_code := tax_matrix.else_measure_type_code;
                  v_less_taxtype_code := tax_matrix.else_taxtype_code;
                  v_less_cch_taxcat_code := tax_matrix.else_cch_taxcat_code;
                  v_less_cch_group_code := tax_matrix.else_cch_group_code;
                  -- v_less_cch_item_code := tax_matrix.else_cch_item_code;
                  v_equal_hold_code_flag := tax_matrix.then_hold_code_flag;
                  v_equal_taxcode_detail_id := tax_matrix.then_taxcode_detail_id;
                  v_equal_taxcode_state_code := tax_matrix.then_taxcode_state_code;
                  v_equal_taxcode_type_code := tax_matrix.then_taxcode_type_code;
                  v_equal_taxcode_code := tax_matrix.then_taxcode_code;
                  v_equal_jurisdiction_id := tax_matrix.then_jurisdiction_id;
                  v_equal_measure_type_code := tax_matrix.then_measure_type_code;
                  v_equal_taxtype_code := tax_matrix.then_taxtype_code;
                  v_equal_cch_taxcat_code := tax_matrix.then_cch_taxcat_code;
                  v_equal_cch_group_code := tax_matrix.then_cch_group_code;
                  -- v_equal_cch_item_code := tax_matrix.then_cch_item_code;
                  v_greater_hold_code_flag := tax_matrix.else_hold_code_flag;
                  v_greater_taxcode_detail_id := tax_matrix.else_taxcode_detail_id;
                  v_greater_taxcode_state_code := tax_matrix.else_taxcode_state_code;
                  v_greater_taxcode_type_code := tax_matrix.else_taxcode_type_code;
                  v_greater_taxcode_code := tax_matrix.else_taxcode_code;
                  v_greater_jurisdiction_id := tax_matrix.else_jurisdiction_id;
                  v_greater_measure_type_code := tax_matrix.else_measure_type_code;
                  v_greater_taxtype_code := tax_matrix.else_taxtype_code;
                  v_greater_cch_taxcat_code := tax_matrix.else_cch_taxcat_code;
                  v_greater_cch_group_code := tax_matrix.else_cch_group_code;
                  -- v_greater_cch_item_code := tax_matrix.else_cch_item_code;
               ELSIF tax_matrix.relation_sign = '<' THEN
                  v_less_hold_code_flag := tax_matrix.then_hold_code_flag;
                  v_less_taxcode_detail_id := tax_matrix.then_taxcode_detail_id;
                  v_less_taxcode_state_code := tax_matrix.then_taxcode_state_code;
                  v_less_taxcode_type_code := tax_matrix.then_taxcode_type_code;
                  v_less_taxcode_code := tax_matrix.then_taxcode_code;
                  v_less_jurisdiction_id := tax_matrix.then_jurisdiction_id;
                  v_less_measure_type_code := tax_matrix.then_measure_type_code;
                  v_less_taxtype_code := tax_matrix.then_taxtype_code;
                  v_less_cch_taxcat_code := tax_matrix.then_cch_taxcat_code;
                  v_less_cch_group_code := tax_matrix.then_cch_group_code;
                  -- v_less_cch_item_code := tax_matrix.then_cch_item_code;
                  v_equal_hold_code_flag := tax_matrix.else_hold_code_flag;
                  v_equal_taxcode_detail_id := tax_matrix.else_taxcode_detail_id;
                  v_equal_taxcode_state_code := tax_matrix.else_taxcode_state_code;
                  v_equal_taxcode_type_code := tax_matrix.else_taxcode_type_code;
                  v_equal_taxcode_code := tax_matrix.else_taxcode_code;
                  v_equal_jurisdiction_id := tax_matrix.else_jurisdiction_id;
                  v_equal_measure_type_code := tax_matrix.else_measure_type_code;
                  v_equal_taxtype_code := tax_matrix.else_taxtype_code;
                  v_equal_cch_taxcat_code := tax_matrix.else_cch_taxcat_code;
                  v_equal_cch_group_code := tax_matrix.else_cch_group_code;
                  -- v_equal_cch_item_code := tax_matrix.else_cch_item_code;
                  v_greater_hold_code_flag := tax_matrix.else_hold_code_flag;
                  v_greater_taxcode_detail_id := tax_matrix.else_taxcode_detail_id;
                  v_greater_taxcode_state_code := tax_matrix.else_taxcode_state_code;
                  v_greater_taxcode_type_code := tax_matrix.else_taxcode_type_code;
                  v_greater_taxcode_code := tax_matrix.else_taxcode_code;
                  v_greater_jurisdiction_id := tax_matrix.else_jurisdiction_id;
                  v_greater_measure_type_code := tax_matrix.else_measure_type_code;
                  v_greater_taxtype_code := tax_matrix.else_taxtype_code;
                  v_greater_cch_taxcat_code := tax_matrix.else_cch_taxcat_code;
                  v_greater_cch_group_code := tax_matrix.else_cch_group_code;
                  -- v_greater_cch_item_code := tax_matrix.else_cch_item_code;
               ELSIF tax_matrix.relation_sign = '<=' THEN
                  v_less_hold_code_flag := tax_matrix.then_hold_code_flag;
                  v_less_taxcode_detail_id := tax_matrix.then_taxcode_detail_id;
                  v_less_taxcode_state_code := tax_matrix.then_taxcode_state_code;
                  v_less_taxcode_type_code := tax_matrix.then_taxcode_type_code;
                  v_less_taxcode_code := tax_matrix.then_taxcode_code;
                  v_less_jurisdiction_id := tax_matrix.then_jurisdiction_id;
                  v_less_measure_type_code := tax_matrix.then_measure_type_code;
                  v_less_taxtype_code := tax_matrix.then_taxtype_code;
                  v_less_cch_taxcat_code := tax_matrix.then_cch_taxcat_code;
                  v_less_cch_group_code := tax_matrix.then_cch_group_code;
                  -- v_less_cch_item_code := tax_matrix.then_cch_item_code;
                  v_equal_hold_code_flag := tax_matrix.then_hold_code_flag;
                  v_equal_taxcode_detail_id := tax_matrix.then_taxcode_detail_id;
                  v_equal_taxcode_state_code := tax_matrix.then_taxcode_state_code;
                  v_equal_taxcode_type_code := tax_matrix.then_taxcode_type_code;
                  v_equal_taxcode_code := tax_matrix.then_taxcode_code;
                  v_equal_jurisdiction_id := tax_matrix.then_jurisdiction_id;
                  v_equal_measure_type_code := tax_matrix.then_measure_type_code;
                  v_equal_taxtype_code := tax_matrix.then_taxtype_code;
                  v_equal_cch_taxcat_code := tax_matrix.then_cch_taxcat_code;
                  v_equal_cch_group_code := tax_matrix.then_cch_group_code;
                  -- v_equal_cch_item_code := tax_matrix.then_cch_item_code;
                  v_greater_hold_code_flag := tax_matrix.else_hold_code_flag;
                  v_greater_taxcode_detail_id := tax_matrix.else_taxcode_detail_id;
                  v_greater_taxcode_state_code := tax_matrix.else_taxcode_state_code;
                  v_greater_taxcode_type_code := tax_matrix.else_taxcode_type_code;
                  v_greater_taxcode_code := tax_matrix.else_taxcode_code;
                  v_greater_jurisdiction_id := tax_matrix.else_jurisdiction_id;
                  v_greater_measure_type_code := tax_matrix.else_measure_type_code;
                  v_greater_taxtype_code := tax_matrix.else_taxtype_code;
                  v_greater_cch_taxcat_code := tax_matrix.else_cch_taxcat_code;
                  v_greater_cch_group_code := tax_matrix.else_cch_group_code;
                  -- v_greater_cch_item_code := tax_matrix.else_cch_item_code;
               ELSIF tax_matrix.relation_sign = '>' THEN
                  v_less_hold_code_flag := tax_matrix.else_hold_code_flag;
                  v_less_taxcode_detail_id := tax_matrix.else_taxcode_detail_id;
                  v_less_taxcode_state_code := tax_matrix.else_taxcode_state_code;
                  v_less_taxcode_type_code := tax_matrix.else_taxcode_type_code;
                  v_less_taxcode_code := tax_matrix.else_taxcode_code;
                  v_less_jurisdiction_id := tax_matrix.else_jurisdiction_id;
                  v_less_measure_type_code := tax_matrix.else_measure_type_code;
                  v_less_taxtype_code := tax_matrix.else_taxtype_code;
                  v_less_cch_taxcat_code := tax_matrix.else_cch_taxcat_code;
                  v_less_cch_group_code := tax_matrix.else_cch_group_code;
                  -- v_less_cch_item_code := tax_matrix.else_cch_item_code;
                  v_equal_hold_code_flag := tax_matrix.else_hold_code_flag;
                  v_equal_taxcode_detail_id := tax_matrix.else_taxcode_detail_id;
                  v_equal_taxcode_state_code := tax_matrix.else_taxcode_state_code;
                  v_equal_taxcode_type_code := tax_matrix.else_taxcode_type_code;
                  v_equal_taxcode_code := tax_matrix.else_taxcode_code;
                  v_equal_jurisdiction_id := tax_matrix.else_jurisdiction_id;
                  v_equal_measure_type_code := tax_matrix.else_measure_type_code;
                  v_equal_taxtype_code := tax_matrix.else_taxtype_code;
                  v_equal_cch_taxcat_code := tax_matrix.else_cch_taxcat_code;
                  v_equal_cch_group_code := tax_matrix.else_cch_group_code;
                  -- v_equal_cch_item_code := tax_matrix.else_cch_item_code;
                  v_greater_hold_code_flag := tax_matrix.then_hold_code_flag;
                  v_greater_taxcode_detail_id := tax_matrix.then_taxcode_detail_id;
                  v_greater_taxcode_state_code := tax_matrix.then_taxcode_state_code;
                  v_greater_taxcode_type_code := tax_matrix.then_taxcode_type_code;
                  v_greater_taxcode_code := tax_matrix.then_taxcode_code;
                  v_greater_jurisdiction_id := tax_matrix.then_jurisdiction_id;
                  v_greater_measure_type_code := tax_matrix.then_measure_type_code;
                  v_greater_taxtype_code := tax_matrix.then_taxtype_code;
                  v_greater_cch_taxcat_code := tax_matrix.then_cch_taxcat_code;
                  v_greater_cch_group_code := tax_matrix.then_cch_group_code;
                  -- v_greater_cch_item_code := tax_matrix.then_cch_item_code;
               ELSIF tax_matrix.relation_sign = '>=' THEN
                  v_less_hold_code_flag := tax_matrix.else_hold_code_flag;
                  v_less_taxcode_detail_id := tax_matrix.else_taxcode_detail_id;
                  v_less_taxcode_state_code := tax_matrix.else_taxcode_state_code;
                  v_less_taxcode_type_code := tax_matrix.else_taxcode_type_code;
                  v_less_taxcode_code := tax_matrix.else_taxcode_code;
                  v_less_jurisdiction_id := tax_matrix.else_jurisdiction_id;
                  v_less_measure_type_code := tax_matrix.else_measure_type_code;
                  v_less_taxtype_code := tax_matrix.else_taxtype_code;
                  v_less_cch_taxcat_code := tax_matrix.else_cch_taxcat_code;
                  v_less_cch_group_code := tax_matrix.else_cch_group_code;
                  -- v_less_cch_item_code := tax_matrix.else_cch_item_code;
                  v_equal_hold_code_flag := tax_matrix.then_hold_code_flag;
                  v_equal_taxcode_detail_id := tax_matrix.then_taxcode_detail_id;
                  v_equal_taxcode_state_code := tax_matrix.then_taxcode_state_code;
                  v_equal_taxcode_type_code := tax_matrix.then_taxcode_type_code;
                  v_equal_taxcode_code := tax_matrix.then_taxcode_code;
                  v_equal_jurisdiction_id := tax_matrix.then_jurisdiction_id;
                  v_equal_measure_type_code := tax_matrix.then_measure_type_code;
                  v_equal_taxtype_code := tax_matrix.then_taxtype_code;
                  v_equal_cch_taxcat_code := tax_matrix.then_cch_taxcat_code;
                  v_equal_cch_group_code := tax_matrix.then_cch_group_code;
                  -- v_equal_cch_item_code := tax_matrix.then_cch_item_code;
                  v_greater_hold_code_flag := tax_matrix.then_hold_code_flag;
                  v_greater_taxcode_detail_id := tax_matrix.then_taxcode_detail_id;
                  v_greater_taxcode_state_code := tax_matrix.then_taxcode_state_code;
                  v_greater_taxcode_type_code := tax_matrix.then_taxcode_type_code;
                  v_greater_taxcode_code := tax_matrix.then_taxcode_code;
                  v_greater_jurisdiction_id := tax_matrix.then_jurisdiction_id;
                  v_greater_measure_type_code := tax_matrix.then_measure_type_code;
                  v_greater_taxtype_code := tax_matrix.then_taxtype_code;
                  v_greater_cch_taxcat_code := tax_matrix.then_cch_taxcat_code;
                  v_greater_cch_group_code := tax_matrix.then_cch_group_code;
                  -- v_greater_cch_item_code := tax_matrix.then_cch_item_code;
               ELSIF tax_matrix.relation_sign = '<>' THEN
                  v_less_hold_code_flag := tax_matrix.then_hold_code_flag;
                  v_less_taxcode_detail_id := tax_matrix.then_taxcode_detail_id;
                  v_less_taxcode_state_code := tax_matrix.then_taxcode_state_code;
                  v_less_taxcode_type_code := tax_matrix.then_taxcode_type_code;
                  v_less_taxcode_code := tax_matrix.then_taxcode_code;
                  v_less_jurisdiction_id := tax_matrix.then_jurisdiction_id;
                  v_less_measure_type_code := tax_matrix.then_measure_type_code;
                  v_less_taxtype_code := tax_matrix.then_taxtype_code;
                  v_less_cch_taxcat_code := tax_matrix.then_cch_taxcat_code;
                  v_less_cch_group_code := tax_matrix.then_cch_group_code;
                  -- v_less_cch_item_code := tax_matrix.then_cch_item_code;
                  v_equal_hold_code_flag := tax_matrix.else_hold_code_flag;
                  v_equal_taxcode_detail_id := tax_matrix.else_taxcode_detail_id;
                  v_equal_taxcode_state_code := tax_matrix.else_taxcode_state_code;
                  v_equal_taxcode_type_code := tax_matrix.else_taxcode_type_code;
                  v_equal_taxcode_code := tax_matrix.else_taxcode_code;
                  v_equal_jurisdiction_id := tax_matrix.else_jurisdiction_id;
                  v_equal_measure_type_code := tax_matrix.else_measure_type_code;
                  v_equal_taxtype_code := tax_matrix.else_taxtype_code;
                  v_equal_cch_taxcat_code := tax_matrix.else_cch_taxcat_code;
                  v_equal_cch_group_code := tax_matrix.else_cch_group_code;
                  -- v_equal_cch_item_code := tax_matrix.else_cch_item_code;
                  v_greater_hold_code_flag := tax_matrix.then_hold_code_flag;
                  v_greater_taxcode_detail_id := tax_matrix.then_taxcode_detail_id;
                  v_greater_taxcode_state_code := tax_matrix.then_taxcode_state_code;
                  v_greater_taxcode_type_code := tax_matrix.then_taxcode_type_code;
                  v_greater_taxcode_code := tax_matrix.then_taxcode_code;
                  v_greater_jurisdiction_id := tax_matrix.then_jurisdiction_id;
                  v_greater_measure_type_code := tax_matrix.then_measure_type_code;
                  v_greater_taxtype_code := tax_matrix.then_taxtype_code;
                  v_greater_cch_taxcat_code := tax_matrix.then_cch_taxcat_code;
                  v_greater_cch_group_code := tax_matrix.then_cch_group_code;
                  -- v_greater_cch_item_code := tax_matrix.then_cch_item_code;
               END IF;

               SELECT DECODE(SIGN(p_new_tb_transaction_detail.gl_line_itm_dist_amt - tax_matrix.relation_amount), -1, v_less_hold_code_flag, 0, v_equal_hold_code_flag, 1, v_greater_hold_code_flag),
                      DECODE(SIGN(p_new_tb_transaction_detail.gl_line_itm_dist_amt - tax_matrix.relation_amount), -1, v_less_taxcode_detail_id, 0, v_equal_taxcode_detail_id, 1, v_greater_taxcode_detail_id),
                      DECODE(SIGN(p_new_tb_transaction_detail.gl_line_itm_dist_amt - tax_matrix.relation_amount), -1, v_less_taxcode_state_code, 0, v_equal_taxcode_state_code, 1, v_greater_taxcode_state_code),
                      DECODE(SIGN(p_new_tb_transaction_detail.gl_line_itm_dist_amt - tax_matrix.relation_amount), -1, v_less_taxcode_type_code, 0, v_equal_taxcode_type_code, 1, v_greater_taxcode_type_code),
                      DECODE(SIGN(p_new_tb_transaction_detail.gl_line_itm_dist_amt - tax_matrix.relation_amount), -1, v_less_taxcode_code, 0, v_equal_taxcode_code, 1, v_greater_taxcode_code),
                      DECODE(SIGN(p_new_tb_transaction_detail.gl_line_itm_dist_amt - tax_matrix.relation_amount), -1, v_less_jurisdiction_id, 0, v_equal_jurisdiction_id, 1, v_greater_jurisdiction_id),
                      DECODE(SIGN(p_new_tb_transaction_detail.gl_line_itm_dist_amt - tax_matrix.relation_amount), -1, v_less_measure_type_code, 0, v_equal_measure_type_code, 1, v_greater_measure_type_code),
                      DECODE(SIGN(p_new_tb_transaction_detail.gl_line_itm_dist_amt - tax_matrix.relation_amount), -1, v_less_taxtype_code, 0, v_equal_taxtype_code, 1, v_greater_taxtype_code),
                      DECODE(SIGN(p_new_tb_transaction_detail.gl_line_itm_dist_amt - tax_matrix.relation_amount), -1, v_less_cch_taxcat_code, 0, v_equal_cch_taxcat_code, 1, v_greater_cch_taxcat_code),
                      DECODE(SIGN(p_new_tb_transaction_detail.gl_line_itm_dist_amt - tax_matrix.relation_amount), -1, v_less_cch_group_code, 0, v_equal_cch_group_code, 1, v_greater_cch_group_code)
                 INTO v_hold_code_flag, p_new_tb_transaction_detail.taxcode_detail_id, p_new_tb_transaction_detail.taxcode_state_code, p_new_tb_transaction_detail.taxcode_type_code, p_new_tb_transaction_detail.taxcode_code, v_override_jurisdiction_id, p_new_tb_transaction_detail.measure_type_code, v_taxtype_code, p_new_tb_transaction_detail.cch_taxcat_code, p_new_tb_transaction_detail.cch_group_code
                 FROM DUAL;

               IF v_override_jurisdiction_id IS NOT NULL AND v_override_jurisdiction_id <> 0 THEN
                  p_new_tb_transaction_detail.jurisdiction_id := v_override_jurisdiction_id;
                  p_new_tb_transaction_detail.manual_jurisdiction_ind := 'AO';
               END IF;
            ELSE
               -- Tax Matrix Line NOT Found
               p_new_tb_transaction_detail.transaction_ind := 'S';
               p_new_tb_transaction_detail.suspend_ind := 'T';
            END IF;
         END IF;
      ELSE
         -- Get Tax Type Code
         SELECT taxtype_code
           INTO v_taxtype_code
           FROM tb_taxcode
          WHERE taxcode_code = p_new_tb_transaction_detail.taxcode_code
            AND taxcode_type_code = p_new_tb_transaction_detail.taxcode_type_code;
      END IF;

      -- Continue if not Suspended
      IF p_new_tb_transaction_detail.transaction_ind IS NULL OR p_new_tb_transaction_detail.transaction_ind <> 'S' THEN
         -- ****** DETERMINE TYPE of TAXCODE ****** --
         -- If the new TaxCode Type is "T"axable then recalc Tax Amounts
         IF UPPER(p_new_tb_transaction_detail.taxcode_type_code) = 'T' THEN
            -- If Override jurisdiction id is not blank or 0 then use it instead of location matrix
            IF p_new_tb_transaction_detail.jurisdiction_id IS NOT NULL AND p_new_tb_transaction_detail.jurisdiction_id <> 0 THEN
               -- Only if there is a Location Matrix ID -- 3351 -- mbf01
               IF p_new_tb_transaction_detail.location_matrix_id IS NOT NULL AND p_new_tb_transaction_detail.location_matrix_id <> 0 THEN
                  -- Get Override Tax Type Code -- 3351
                  -- Get 5 Tax Calc Flags -- 3411 -- MBF02
                  BEGIN
                     SELECT override_taxtype_code,
                            state_flag, county_flag, county_local_flag, city_flag, city_local_flag
                       INTO location_matrix.override_taxtype_code,
                            location_matrix.state_flag,
                            location_matrix.county_flag,
                            location_matrix.county_local_flag,
                            location_matrix.city_flag,
                            location_matrix.city_local_flag
                       FROM tb_location_matrix
                      WHERE tb_location_matrix.location_matrix_id = p_new_tb_transaction_detail.location_matrix_id;
                  EXCEPTION
                     WHEN NO_DATA_FOUND THEN
                        location_matrix.override_taxtype_code := '*NO';
                        location_matrix.state_flag := '1';
                        location_matrix.county_flag := '1';
                        location_matrix.county_local_flag := '1';
                        location_matrix.city_flag := '1';
                        location_matrix.city_local_flag := '1';
                  END;
               ELSE
                  location_matrix.override_taxtype_code := '*NO';
                  location_matrix.state_flag := '1';
                  location_matrix.county_flag := '1';
                  location_matrix.county_local_flag := '1';
                  location_matrix.city_flag := '1';
                  location_matrix.city_local_flag := '1';
               END IF;
            ELSE
            -- Search for Location Matrix if not already found
            IF p_new_tb_transaction_detail.location_matrix_id IS NULL  AND p_new_tb_transaction_detail.manual_jurisdiction_ind IS NULL THEN
               v_transaction_detail_id := p_new_tb_transaction_detail.transaction_detail_id;
               -- ***** Create Dynamic WHERE Clause for Matrix SQLs ***** --
               -- Location Matrix Drivers
               /*OPEN location_driver_cursor;
               LOOP
                  FETCH location_driver_cursor INTO location_driver;
                  EXIT WHEN location_driver_cursor%NOTFOUND;
                  IF location_driver.null_driver_flag = '1' THEN
                     IF location_driver.wildcard_flag = '1' THEN
                        IF UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'CHAR' OR
                           UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'VARC' THEN
                              vc_location_matrix_where := vc_location_matrix_where || 'AND ' ||
                              '((tb_tmp_transaction_detail.' || location_driver.trans_dtl_column_name || ' IS NULL AND (tb_location_matrix.' || location_driver.matrix_column_name || ' = ''*ALL'' OR tb_location_matrix.' || location_driver.matrix_column_name || ' = ''*NULL'' )) OR ' ||
                              '(tb_tmp_transaction_detail.' || location_driver.trans_dtl_column_name || ' IS NOT NULL AND (tb_location_matrix.' || location_driver.matrix_column_name || ' = ''*ALL'' OR UPPER(tb_tmp_transaction_detail.' || location_driver.trans_dtl_column_name || ') LIKE UPPER(tb_location_matrix.' || location_driver.matrix_column_name || ') ))) ';
                        END IF;
                     ELSE
                        IF UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'CHAR' OR
                           UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'VARC' THEN
                              vc_location_matrix_where := vc_location_matrix_where || 'AND ' ||
                              '((tb_tmp_transaction_detail.' || location_driver.trans_dtl_column_name || ' IS NULL AND (tb_location_matrix.' || location_driver.matrix_column_name || ' = ''*ALL'' OR tb_location_matrix.' || location_driver.matrix_column_name || ' = ''*NULL'' )) OR ' ||
                              '(tb_tmp_transaction_detail.' || location_driver.trans_dtl_column_name || ' IS NOT NULL AND (tb_location_matrix.' || location_driver.matrix_column_name || ' = ''*ALL'' OR UPPER(tb_tmp_transaction_detail.' || location_driver.trans_dtl_column_name || ') = UPPER(tb_location_matrix.' || location_driver.matrix_column_name || ') ))) ';
                        END IF;
                     END IF;
                  ELSE
                     IF location_driver.wildcard_flag = '1' THEN
                        IF UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'CHAR' OR
                           UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'VARC' THEN
                              vc_location_matrix_where := vc_location_matrix_where || 'AND ' ||
                              '(tb_location_matrix.' || location_driver.matrix_column_name || ' = ''*ALL'' OR UPPER(tb_tmp_transaction_detail.' || location_driver.trans_dtl_column_name || ') LIKE UPPER(tb_location_matrix.' || location_driver.matrix_column_name || ') ) ';
                        END IF;
                     ELSE
                        IF UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'CHAR' OR
                           UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'VARC' THEN
                              vc_location_matrix_where := vc_location_matrix_where || 'AND ' ||
                              '(tb_location_matrix.' || location_driver.matrix_column_name || ' = ''*ALL'' OR UPPER(tb_tmp_transaction_detail.' || location_driver.trans_dtl_column_name || ') = UPPER(tb_location_matrix.' || location_driver.matrix_column_name || ') ) ';
                        END IF;
                     END IF;
                  END IF;
               END LOOP;
               CLOSE location_driver_cursor;*/

	       -- New call to sp_gen_location_driver for MidTier Cleanup
	       sp_gen_location_driver (
			p_generate_driver_reference	=> 'N',
			p_an_batch_id			=> NULL,
			p_transaction_table_name	=> 'tb_tmp_transaction_detail',
			p_location_table_name		=> 'tb_location_matrix',
			p_vc_location_matrix_where	=> vc_location_matrix_where);

               -- If no drivers found raise error, else create transaction detail sql statement
               IF vc_location_matrix_where IS NULL THEN
                  NULL;
               ELSE
                  vc_location_matrix_stmt := vc_location_matrix_select || vc_location_matrix_where || vc_location_matrix_orderby;
               END IF;

               -- Search Location Matrix for matches
               BEGIN
                   OPEN location_matrix_cursor
                    FOR vc_location_matrix_stmt
                  USING v_transaction_detail_id;
                  FETCH location_matrix_cursor
                   INTO location_matrix;
                  -- Rows found?
                  IF location_matrix_cursor%FOUND THEN
                     vn_fetch_rows := location_matrix_cursor%ROWCOUNT;
                  ELSE
                     vn_fetch_rows := 0;
                  END IF;
                  CLOSE location_matrix_cursor;
                  IF SQLCODE != 0 THEN
                    RAISE e_badread;
                  END IF;
               EXCEPTION
                  WHEN e_badread THEN
                     vn_fetch_rows := 0;
               END;

               -- Location Matrix Line Found
               IF vn_fetch_rows > 0 THEN
                  p_new_tb_transaction_detail.location_matrix_id := location_matrix.location_matrix_id;
                  p_new_tb_transaction_detail.jurisdiction_id := location_matrix.jurisdiction_id;
               ELSE
                  p_new_tb_transaction_detail.transaction_ind := 'S';
                  p_new_tb_transaction_detail.suspend_ind := 'L';
               END IF;
            END IF;
            END IF;

            -- Continue if transaction not Suspended
            IF ( p_new_tb_transaction_detail.transaction_ind IS NULL OR p_new_tb_transaction_detail.transaction_ind <> 'S' ) THEN
               -- Get Jurisdiction TaxRates and calculate Tax Amounts
               sp_getusetax(p_new_tb_transaction_detail.jurisdiction_id,
                            p_new_tb_transaction_detail.gl_date,
                            p_new_tb_transaction_detail.measure_type_code,
                            p_new_tb_transaction_detail.gl_line_itm_dist_amt,
                            v_taxtype_code,
                            location_matrix.override_taxtype_code,
                            location_matrix.state_flag,
                            location_matrix.county_flag,
                            location_matrix.county_local_flag,
                            location_matrix.city_flag,
                            location_matrix.city_local_flag,
                            p_new_tb_transaction_detail.invoice_freight_amt,
                            p_new_tb_transaction_detail.invoice_discount_amt,
                            p_new_tb_transaction_detail.transaction_state_code,
-- future?? --                     p_new_tb_transaction_detail.import_definition_code,
                            'importdefn',
                            p_new_tb_transaction_detail.taxcode_code,
                            p_new_tb_transaction_detail.jurisdiction_taxrate_id,
                            p_new_tb_transaction_detail.state_use_amount,
                            p_new_tb_transaction_detail.state_use_tier2_amount,
                            p_new_tb_transaction_detail.state_use_tier3_amount,
                            p_new_tb_transaction_detail.county_use_amount,
                            p_new_tb_transaction_detail.county_local_use_amount,
                            p_new_tb_transaction_detail.city_use_amount,
                            p_new_tb_transaction_detail.city_local_use_amount,
                            p_new_tb_transaction_detail.state_use_rate,
                            p_new_tb_transaction_detail.state_use_tier2_rate,
                            p_new_tb_transaction_detail.state_use_tier3_rate,
                            p_new_tb_transaction_detail.state_split_amount,
                            p_new_tb_transaction_detail.state_tier2_min_amount,
                            p_new_tb_transaction_detail.state_tier2_max_amount,
                            p_new_tb_transaction_detail.state_maxtax_amount,
                            p_new_tb_transaction_detail.county_use_rate,
                            p_new_tb_transaction_detail.county_local_use_rate,
                            p_new_tb_transaction_detail.county_split_amount,
                            p_new_tb_transaction_detail.county_maxtax_amount,
                            p_new_tb_transaction_detail.county_single_flag,
                            p_new_tb_transaction_detail.county_default_flag,
                            p_new_tb_transaction_detail.city_use_rate,
                            p_new_tb_transaction_detail.city_local_use_rate,
                            p_new_tb_transaction_detail.city_split_amount,
                            p_new_tb_transaction_detail.city_split_use_rate,
                            p_new_tb_transaction_detail.city_single_flag,
                            p_new_tb_transaction_detail.city_default_flag,
                            p_new_tb_transaction_detail.combined_use_rate,
                            p_new_tb_transaction_detail.tb_calc_tax_amt,
-- future?? --                     p_new_tb_transaction_detail.taxable_amt,
-- Targa --                    p_new_tb_transaction_detail.user_number_10,
                            vn_taxable_amt,
                            v_taxtype_used );

               -- Suspend if tax rate id is null
               IF p_new_tb_transaction_detail.jurisdiction_taxrate_id IS NULL OR p_new_tb_transaction_detail.jurisdiction_taxrate_id = 0 THEN
                  p_new_tb_transaction_detail.transaction_ind := 'S';
                  p_new_tb_transaction_detail.suspend_ind := 'R';
               ELSE
                  -- Check for Taxable Amount -- 3351
                  IF vn_taxable_amt <> p_new_tb_transaction_detail.gl_line_itm_dist_amt THEN
                     p_new_tb_transaction_detail.gl_extract_amt := p_new_tb_transaction_detail.gl_line_itm_dist_amt;
                     p_new_tb_transaction_detail.gl_line_itm_dist_amt := vn_taxable_amt;
                  END IF;
               END IF;
            END IF;

         -- Else, If the new TaxCode Type is "E"xempt
         ELSIF p_new_tb_transaction_detail.taxcode_type_code = 'E' THEN
            p_new_tb_transaction_detail.transaction_ind := 'P';

         -- Else, If the new TaxCode Type is a "Q"uestion
         ELSIF p_new_tb_transaction_detail.taxcode_type_code = 'Q' THEN
           p_new_tb_transaction_detail.transaction_ind := 'Q';

         -- Else, the new TaxCode Type is Unrecognized - Suspend
         ELSE
            p_new_tb_transaction_detail.transaction_ind := 'S';
            p_new_tb_transaction_detail.suspend_ind := '?';
         END IF;
      END IF;

      -- Continue if not Suspended
      IF p_new_tb_transaction_detail.transaction_ind IS NULL OR p_new_tb_transaction_detail.transaction_ind <> 'S' THEN
         p_new_tb_transaction_detail.transaction_ind := 'P';
      END IF;
   END IF;

   -- ******  GL logging module  ****** ------------------------------------------------------------
   sp_gl_logging (p_old_tb_transaction_detail, p_new_tb_transaction_detail, v_sysdate, v_sysdate_plus);

END;
/

SHOW ERRORS;

CREATE OR REPLACE PROCEDURE sp_tb_trans_detail_excp_b_iu (
	p_new_trans_detail_excp_id IN OUT VARCHAR2
)
AS
BEGIN
      SELECT sq_tb_trans_detail_excp_id.NEXTVAL
	INTO p_new_trans_detail_excp_id
	FROM DUAL;
END;
/

SHOW ERRORS;

CREATE OR REPLACE PROCEDURE sp_tb_user_a_d(p_old_user_code IN VARCHAR2)
AS
BEGIN
   DELETE
     FROM tb_option
    WHERE user_code = p_old_user_code;
   --DELETE
   --FROM tb_dw_syntax
   --WHERE user_code = p_old_user_code;
END;
/

SHOW ERRORS;

DROp PROCEDURE SP_ACTIVE_LOCATION_MTRX_LINES;

CREATE OR REPLACE FUNCTION SP_ACTIVE_LOCATION_MTRX_LINES (
	p_where_clause_token	IN VARCHAR2,
	p_effective_date	IN DATE,
	p_expiration_date	IN DATE
) RETURN sys_refcursor
AS
	l_cursor sys_refcursor;
BEGIN
	OPEN l_cursor FOR
	'SELECT COUNT (*) AS active_matrix_lines FROM TB_LOCATION_MATRIX left outer join TB_JURISDICTION 
	ON (TB_LOCATION_MATRIX.JURISDICTION_ID = TB_JURISDICTION.JURISDICTION_ID)
	WHERE (TB_LOCATION_MATRIX.EFFECTIVE_DATE  <= SYSDATE) 
	AND (TB_LOCATION_MATRIX.EXPIRATION_DATE >= SYSDATE) '||p_where_clause_token;

	RETURN l_cursor;
END;
/

SHOW ERRORS;

DROp PROCEDURE SP_ACTIVE_MATRIX_LINES ;

CREATE OR REPLACE FUNCTION SP_ACTIVE_MATRIX_LINES (
	p_where_clause_token	IN VARCHAR2,
	p_effective_date	IN DATE,
	p_expiration_date	IN DATE
) RETURN sys_refcursor
AS
	l_cursor sys_refcursor;
BEGIN
	OPEN l_cursor FOR
	'SELECT	COUNT (*) AS active_matrix_lines 
	FROM TB_TAX_MATRIX 
	left outer join TB_TAXCODE_DETAIL TB_TAXCODE_DETAIL_A on (TB_TAX_MATRIX.then_taxcode_detail_id = tb_taxcode_detail_a.taxcode_detail_id)    
	left outer join TB_TAXCODE_DETAIL TB_TAXCODE_DETAIL_B on (TB_TAX_MATRIX.else_taxcode_detail_id = tb_taxcode_detail_b.taxcode_detail_id)  
	where (TB_TAX_MATRIX.EFFECTIVE_DATE <= SYSDATE) AND (TB_TAX_MATRIX.EXPIRATION_DATE >= SYSDATE) '||p_where_clause_token;

	RETURN l_cursor;
END;
/

SHOW ERRORS;

DROP PROCEDURE SP_ALLOCATION_MTRX_MASTR_COUNT;

CREATE OR REPLACE FUNCTION SP_ALLOCATION_MTRX_MASTR_COUNT (
	p_where_clause_token	IN VARCHAR2
) RETURN sys_refcursor
AS
	l_cursor sys_refcursor;
BEGIN
	OPEN l_cursor FOR
	'SELECT COUNT(*)  
	    FROM (       
	    SELECT DISTINCT           
		TB_ALLOCATION_MATRIX.DRIVER_01,TB_ALLOCATION_MATRIX.DRIVER_02,TB_ALLOCATION_MATRIX.DRIVER_03,TB_ALLOCATION_MATRIX.DRIVER_04,TB_ALLOCATION_MATRIX.DRIVER_05,  
		TB_ALLOCATION_MATRIX.DRIVER_06,TB_ALLOCATION_MATRIX.DRIVER_07,TB_ALLOCATION_MATRIX.DRIVER_08,TB_ALLOCATION_MATRIX.DRIVER_09,TB_ALLOCATION_MATRIX.DRIVER_10,  
		TB_ALLOCATION_MATRIX.DRIVER_11,TB_ALLOCATION_MATRIX.DRIVER_12,TB_ALLOCATION_MATRIX.DRIVER_13,TB_ALLOCATION_MATRIX.DRIVER_14,TB_ALLOCATION_MATRIX.DRIVER_15,  
		TB_ALLOCATION_MATRIX.DRIVER_16,TB_ALLOCATION_MATRIX.DRIVER_17,TB_ALLOCATION_MATRIX.DRIVER_18,TB_ALLOCATION_MATRIX.DRIVER_19,TB_ALLOCATION_MATRIX.DRIVER_20,  
		TB_ALLOCATION_MATRIX.DRIVER_21,TB_ALLOCATION_MATRIX.DRIVER_22,TB_ALLOCATION_MATRIX.DRIVER_23,TB_ALLOCATION_MATRIX.DRIVER_24,TB_ALLOCATION_MATRIX.DRIVER_25,  
		TB_ALLOCATION_MATRIX.DRIVER_26,TB_ALLOCATION_MATRIX.DRIVER_27,TB_ALLOCATION_MATRIX.DRIVER_28,TB_ALLOCATION_MATRIX.DRIVER_29,TB_ALLOCATION_MATRIX.DRIVER_30,  
		TB_ALLOCATION_MATRIX.BINARY_WEIGHT      
	FROM TB_ALLOCATION_MATRIX_DETAIL 
	left outer join TB_ALLOCATION_MATRIX on (TB_ALLOCATION_MATRIX_DETAIL.ALLOCATION_MATRIX_ID = TB_ALLOCATION_MATRIX.ALLOCATION_MATRIX_ID)        
	left outer join TB_JURISDICTION      on (TB_ALLOCATION_MATRIX_DETAIL.JURISDICTION_ID      = TB_JURISDICTION.JURISDICTION_ID)   
	WHERE 1=1 '||p_where_clause_token||' ) B';

	RETURN l_cursor;
END;
/

SHOW ERRORS;

DROP PROCEDURE SP_ALLOCATION_MTRX_MASTR_LIST;

CREATE OR REPLACE FUNCTION SP_ALLOCATION_MTRX_MASTR_LIST (
	p_where_clause_token	IN VARCHAR2,
	p_order_by_token	IN VARCHAR2
) RETURN sys_refcursor 
AS
	l_cursor sys_refcursor;
BEGIN
	OPEN l_cursor FOR
	'SELECT * FROM (SELECT DISTINCT         
	TB_ALLOCATION_MATRIX.DRIVER_01,TB_ALLOCATION_MATRIX.DRIVER_02,TB_ALLOCATION_MATRIX.DRIVER_03,TB_ALLOCATION_MATRIX.DRIVER_04,TB_ALLOCATION_MATRIX.DRIVER_05,  
	TB_ALLOCATION_MATRIX.DRIVER_06,TB_ALLOCATION_MATRIX.DRIVER_07,TB_ALLOCATION_MATRIX.DRIVER_08,TB_ALLOCATION_MATRIX.DRIVER_09,TB_ALLOCATION_MATRIX.DRIVER_10,  
	TB_ALLOCATION_MATRIX.DRIVER_11,TB_ALLOCATION_MATRIX.DRIVER_12,TB_ALLOCATION_MATRIX.DRIVER_13,TB_ALLOCATION_MATRIX.DRIVER_14,TB_ALLOCATION_MATRIX.DRIVER_15,  
	TB_ALLOCATION_MATRIX.DRIVER_16,TB_ALLOCATION_MATRIX.DRIVER_17,TB_ALLOCATION_MATRIX.DRIVER_18,TB_ALLOCATION_MATRIX.DRIVER_19,TB_ALLOCATION_MATRIX.DRIVER_20,  
	TB_ALLOCATION_MATRIX.DRIVER_21,TB_ALLOCATION_MATRIX.DRIVER_22,TB_ALLOCATION_MATRIX.DRIVER_23,TB_ALLOCATION_MATRIX.DRIVER_24,TB_ALLOCATION_MATRIX.DRIVER_25,  
	TB_ALLOCATION_MATRIX.DRIVER_26,TB_ALLOCATION_MATRIX.DRIVER_27,TB_ALLOCATION_MATRIX.DRIVER_28,TB_ALLOCATION_MATRIX.DRIVER_29,TB_ALLOCATION_MATRIX.DRIVER_30,  
	TB_ALLOCATION_MATRIX.BINARY_WEIGHT  
	FROM TB_ALLOCATION_MATRIX_DETAIL 
	left outer join TB_ALLOCATION_MATRIX on (TB_ALLOCATION_MATRIX_DETAIL.ALLOCATION_MATRIX_ID = TB_ALLOCATION_MATRIX.ALLOCATION_MATRIX_ID)     
	left outer join TB_JURISDICTION on      (TB_ALLOCATION_MATRIX_DETAIL.JURISDICTION_ID      = TB_JURISDICTION.JURISDICTION_ID)     
	WHERE 1=1 '||p_where_clause_token||' '||p_order_by_token||' ) B ';

	RETURN l_cursor;
END;
/

SHOW ERRORS;

DROP PROCEDURE sp_distinct_loc_mtrx_lines;
CREATE OR REPLACE FUNCTION sp_distinct_loc_mtrx_lines (
	p_where_clause_token IN VARCHAR2
) RETURN sys_refcursor
AS
	l_cursor sys_refcursor;
BEGIN
	OPEN l_cursor FOR
	'SELECT COUNT (*) AS distinct_matrix_lines 
	FROM (
		SELECT COUNT (*) 
		FROM TB_LOCATION_MATRIX left outer join TB_JURISDICTION 
		on (TB_LOCATION_MATRIX.JURISDICTION_ID = TB_JURISDICTION.JURISDICTION_ID)  
		WHERE 1=1 '||p_where_clause_token||'
		GROUP BY driver_01,driver_02,driver_03,driver_04,driver_05,driver_06,
		driver_07,driver_08,driver_09,driver_10) driver_group_by';

	RETURN l_cursor;
END;
/

SHOW ERRORS;
DROP PROCEDURE SP_LOCATION_MTRX_MASTR_COUNT;

CREATE OR REPLACE FUNCTION SP_LOCATION_MTRX_MASTR_COUNT (
	p_where_clause_token IN VARCHAR2
) RETURN sys_refcursor
AS
	l_cursor sys_refcursor;
BEGIN
	OPEN l_cursor FOR
	'SELECT COUNT(*) 
	FROM (
		SELECT DISTINCT  
			TB_LOCATION_MATRIX.DRIVER_01, TB_LOCATION_MATRIX.DRIVER_02, TB_LOCATION_MATRIX.DRIVER_03, TB_LOCATION_MATRIX.DRIVER_04, 
			TB_LOCATION_MATRIX.DRIVER_05, TB_LOCATION_MATRIX.DRIVER_06, TB_LOCATION_MATRIX.DRIVER_07, TB_LOCATION_MATRIX.DRIVER_08, 
			TB_LOCATION_MATRIX.DRIVER_09, TB_LOCATION_MATRIX.DRIVER_10, 
			TB_LOCATION_MATRIX.BINARY_WEIGHT, TB_LOCATION_MATRIX.DEFAULT_FLAG, TB_LOCATION_MATRIX.DEFAULT_BINARY_WEIGHT  
	FROM TB_LOCATION_MATRIX left outer join TB_JURISDICTION  
	on (TB_LOCATION_MATRIX.JURISDICTION_ID = TB_JURISDICTION.JURISDICTION_ID)        WHERE 1=1 '||p_where_clause_token||' ) B ';

	RETURN l_cursor;
END;
/

SHOW ERRORS;

DROP PROCEDURE sp_loc_mtrx_tot_mtrx_lines;

CREATE OR REPLACE FUNCTION sp_loc_mtrx_tot_mtrx_lines (
	p_where_clause_token	IN VARCHAR2
) RETURN sys_refcursor
AS
	l_cursor sys_refcursor;
BEGIN
	OPEN l_cursor FOR
	'SELECT COUNT (*) AS total_matrix_lines FROM TB_LOCATION_MATRIX 
	left outer join TB_JURISDICTION on (TB_LOCATION_MATRIX.JURISDICTION_ID = TB_JURISDICTION.JURISDICTION_ID) '||p_where_clause_token;

	RETURN l_cursor;
END;
/

SHOW ERRORS;
DROP PROCEDURE SP_TAXCODE_DETAIL_LIST;
CREATE OR REPLACE FUNCTION SP_TAXCODE_DETAIL_LIST (
	p_where_clause_token	IN VARCHAR2
) RETURN sys_refcursor
AS
	l_cursor sys_refcursor;
BEGIN
	OPEN l_cursor FOR
	'SELECT	TB_TAXCODE_DETAIL.TAXCODE_CODE, TB_TAXCODE_DETAIL.TAXCODE_STATE_CODE,
		TB_TAXCODE_STATE.NAME, TB_TAXCODE_DETAIL.TAXCODE_TYPE_CODE,
		TB_LIST_CODE.DESCRIPTION AS TAXCODE_TYPE_DESCRIPTION,
		TB_TAXCODE.DESCRIPTION AS TAXCODE_DESCRIPTION, TB_TAXCODE.COMMENTS,
		TB_TAXCODE.TAXTYPE_CODE, TB_TAXCODE.ERP_TAXCODE,
		TB_LIST_CODE1.DESCRIPTION AS TAX_TYPE_DESCRIPTION,
		TB_TAXCODE_DETAIL.TAXCODE_DETAIL_ID,
		TB_TAXCODE_DETAIL.JURISDICTION_ID, TB_JURISDICTION.GEOCODE,
		TB_JURISDICTION.CITY, TB_JURISDICTION.COUNTY, TB_JURISDICTION.STATE,
		TB_JURISDICTION.ZIP,TB_TAXCODE.MEASURE_TYPE_CODE,TB_TAXCODE.MEASURE_TYPE_CODE 
	FROM ((((TB_TAXCODE_DETAIL INNER JOIN TB_TAXCODE_STATE ON TB_TAXCODE_DETAIL.TAXCODE_STATE_CODE = TB_TAXCODE_STATE.TAXCODE_STATE_CODE) 
	INNER JOIN TB_TAXCODE ON TB_TAXCODE_DETAIL.TAXCODE_CODE = TB_TAXCODE.TAXCODE_CODE AND TB_TAXCODE_DETAIL.TAXCODE_TYPE_CODE = TB_TAXCODE.TAXCODE_TYPE_CODE) 
	INNER JOIN TB_LIST_CODE ON TB_TAXCODE_DETAIL.TAXCODE_TYPE_CODE = TB_LIST_CODE.CODE_CODE) 
	LEFT JOIN  TB_LIST_CODE TB_LIST_CODE1 ON TB_LIST_CODE1.CODE_TYPE_CODE = ''TAXTYPE'' AND TB_LIST_CODE1.CODE_CODE = TB_TAXCODE.TAXTYPE_CODE) 
	LEFT JOIN  TB_JURISDICTION ON TB_JURISDICTION.JURISDICTION_ID = TB_TAXCODE_DETAIL.JURISDICTION_ID 
	WHERE      TB_TAXCODE_DETAIL.ACTIVE_FLAG = ''1'' 
	AND        TB_LIST_CODE.CODE_TYPE_CODE = ''TCTYPE'' '||
	p_where_clause_token
	; 

	RETURN l_cursor;
END;
/

SHOW ERRORS;

DROP PROCEDURE sp_tax_mtrx_dstnct_mtrx_lines;

CREATE OR REPLACE FUNCTION sp_tax_mtrx_dstnct_mtrx_lines (
	 p_where_clause_token IN VARCHAR2
) RETURN sys_refcursor
AS
	 l_cursor sys_refcursor;
BEGIN
	OPEN l_cursor for
	'SELECT COUNT (*) AS distinct_matrix_lines 
		FROM (
		SELECT COUNT (*) FROM TB_TAX_MATRIX 
		left outer join TB_TAXCODE_DETAIL TB_TAXCODE_DETAIL_A on (TB_TAX_MATRIX.then_taxcode_detail_id = tb_taxcode_detail_a.taxcode_detail_id) 
		left outer join TB_TAXCODE_DETAIL TB_TAXCODE_DETAIL_B on (TB_TAX_MATRIX.else_taxcode_detail_id = tb_taxcode_detail_b.taxcode_detail_id) 
		WHERE 1=1 '||p_where_clause_token||'
		GROUP BY matrix_state_code,driver_01,driver_02,driver_03,driver_04,driver_05,
		driver_06,driver_07,driver_08,driver_09, driver_10,driver_11,
		driver_12,driver_13,driver_14,driver_15, driver_16,driver_17,
		driver_18,driver_19,driver_20,driver_21,driver_22,driver_23,
		driver_24,driver_25,driver_26,driver_27,driver_28,driver_29,
		driver_30) driver_group_by ';

	RETURN l_cursor;
END;
/

SHOW ERRORS;

DROp PROCEDURE SP_TAX_MTRX_TOTAL_MTRX_LINES;

CREATE OR REPLACE FUNCTION SP_TAX_MTRX_TOTAL_MTRX_LINES (
	p_where_clause_token IN VARCHAR2
) RETURN sys_refcursor
AS
	l_cursor sys_refcursor;
BEGIN
		OPEN l_cursor FOR 
		'SELECT COUNT (*) AS total_matrix_lines FROM TB_TAX_MATRIX 
		left outer join TB_TAXCODE_DETAIL TB_TAXCODE_DETAIL_A  on (TB_TAX_MATRIX.then_taxcode_detail_id = tb_taxcode_detail_a.taxcode_detail_id) 
		left outer join TB_TAXCODE_DETAIL TB_TAXCODE_DETAIL_B  on (TB_TAX_MATRIX.else_taxcode_detail_id = tb_taxcode_detail_b.taxcode_detail_id) '||p_where_clause_token;

	RETURN l_cursor;
END;
/

SHOW ERRORS;

DROP PROCEDURE SP_TB_LOCATION_MTRX_MASTR_LIST;

CREATE OR REPLACE FUNCTION SP_TB_LOCATION_MTRX_MASTR_LIST (
	p_where_clause_token	IN VARCHAR2,
	p_order_by_token	IN VARCHAR2,
	p_start_index		IN INTEGER,
	p_max_rows		IN INTEGER
) RETURN SYS_REFCURSOR
AS
	l_cursor sys_refcursor;
BEGIN
	OPEN l_cursor FOR
	'SELECT * FROM ( SELECT B.*, rownum rnum  
	FROM ( 
	    SELECT DISTINCT 
	    TB_LOCATION_MATRIX.DRIVER_01, TB_LOCATION_MATRIX.DRIVER_02, TB_LOCATION_MATRIX.DRIVER_03,TB_LOCATION_MATRIX.DRIVER_04, TB_LOCATION_MATRIX.DRIVER_05, TB_LOCATION_MATRIX.DRIVER_06,TB_LOCATION_MATRIX.DRIVER_07, TB_LOCATION_MATRIX.DRIVER_08, TB_LOCATION_MATRIX.DRIVER_09, TB_LOCATION_MATRIX.DRIVER_10, TB_LOCATION_MATRIX.BINARY_WEIGHT,TB_LOCATION_MATRIX.DEFAULT_FLAG, TB_LOCATION_MATRIX.DEFAULT_BINARY_WEIGHT
	    FROM TB_LOCATION_MATRIX left outer join TB_JURISDICTION
	     on (TB_LOCATION_MATRIX.JURISDICTION_ID = TB_JURISDICTION.JURISDICTION_ID)        
	WHERE 1=1 '||p_where_clause_token||' '||p_order_by_token||' ) B WHERE rownum <= '|| (p_start_index + p_max_rows) ||' ) WHERE rnum > '||p_start_index;

	RETURN l_cursor;

END;
/

SHOW ERRORS;

DROP PROCEDURE SP_TB_TAX_MATRIX_MASTER_COUNT;

CREATE OR REPLACE FUNCTION SP_TB_TAX_MATRIX_MASTER_COUNT (
	p_where_clause_token	IN VARCHAR2
) RETURN sys_refcursor
AS
	l_cursor sys_refcursor;
BEGIN

	OPEN l_cursor FOR
	'SELECT COUNT(*) FROM ( SELECT DISTINCT 
	     T.DEFAULT_FLAG,T.MATRIX_STATE_CODE,T.DRIVER_GLOBAL_FLAG,T.DRIVER_01,T.DRIVER_02,T.DRIVER_03,T.DRIVER_04,T.DRIVER_05,T.DRIVER_06, 
	     T.DRIVER_07,T.DRIVER_08,T.DRIVER_09,T.DRIVER_10,T.DRIVER_11,T.DRIVER_12,T.DRIVER_13,T.DRIVER_14,T.DRIVER_15,T.DRIVER_16,T.DRIVER_17,T.DRIVER_18, 
	     T.DRIVER_19,T.DRIVER_20,T.DRIVER_21,T.DRIVER_22,T.DRIVER_23,T.DRIVER_24,T.DRIVER_25,T.DRIVER_26,T.DRIVER_27,T.DRIVER_28,T.DRIVER_29,T.DRIVER_30, 
	     T.BINARY_WEIGHT,T.DEFAULT_BINARY_WEIGHT,T.DRIVER_01_THRU,T.DRIVER_02_THRU,T.DRIVER_03_THRU,T.DRIVER_04_THRU,T.DRIVER_05_THRU, 
	     T.DRIVER_06_THRU,T.DRIVER_07_THRU,T.DRIVER_08_THRU,T.DRIVER_09_THRU,T.DRIVER_10_THRU, 
	     T.DRIVER_11_THRU,T.DRIVER_12_THRU,T.DRIVER_13_THRU,T.DRIVER_14_THRU,T.DRIVER_15_THRU, 
	     T.DRIVER_16_THRU,T.DRIVER_17_THRU,T.DRIVER_18_THRU,T.DRIVER_19_THRU,T.DRIVER_20_THRU, 
	     T.DRIVER_21_THRU,T.DRIVER_22_THRU,T.DRIVER_23_THRU,T.DRIVER_24_THRU,T.DRIVER_25_THRU, 
	     T.DRIVER_26_THRU,T.DRIVER_27_THRU,T.DRIVER_28_THRU,T.DRIVER_29_THRU,T.DRIVER_30_THRU 
	FROM TB_TAX_MATRIX T 
	 left outer join TB_TAXCODE_DETAIL TB_TAXCODE_DETAIL_A on (T.THEN_TAXCODE_DETAIL_ID = TB_TAXCODE_DETAIL_A.TAXCODE_DETAIL_ID) 
	 left outer join TB_TAXCODE_DETAIL TB_TAXCODE_DETAIL_B on (T.ELSE_TAXCODE_DETAIL_ID = TB_TAXCODE_DETAIL_B.TAXCODE_DETAIL_ID) 
	WHERE 1=1 '|| p_where_clause_token ||' ) B '; 

	RETURN l_cursor;
 
END;
/

SHOW ERRORS;
DROP PROCEDURE SP_TB_TAX_MATRIX_MASTER_LIST;

CREATE OR REPLACE function SP_TB_TAX_MATRIX_MASTER_LIST (
	p_where_clause_token	IN VARCHAR2,
	p_order_by_token	IN VARCHAR2,
	p_start_index		IN INTEGER,
	p_max_rows		IN INTEGER
) RETURN sys_refcursor
AS
	l_cursor	sys_refcursor;
BEGIN
	OPEN l_cursor FOR
	'SELECT * FROM ( SELECT B.*, rownum rnum  
	  FROM (  
	  SELECT DISTINCT  
		T.DEFAULT_FLAG,T.MATRIX_STATE_CODE,T.DRIVER_GLOBAL_FLAG, 
		T.DRIVER_01,T.DRIVER_02,T.DRIVER_03,T.DRIVER_04,T.DRIVER_05,T.DRIVER_06,        
		T.DRIVER_07,T.DRIVER_08,T.DRIVER_09,T.DRIVER_10,T.DRIVER_11,T.DRIVER_12,             
		T.DRIVER_13,T.DRIVER_14,T.DRIVER_15,T.DRIVER_16,T.DRIVER_17,T.DRIVER_18,        
		T.DRIVER_19,T.DRIVER_20,T.DRIVER_21,T.DRIVER_22,T.DRIVER_23,T.DRIVER_24,            
		T.DRIVER_25,T.DRIVER_26,T.DRIVER_27,T.DRIVER_28,T.DRIVER_29,T.DRIVER_30,         
		T.BINARY_WEIGHT,T.DEFAULT_BINARY_WEIGHT, T.DRIVER_01_THRU,T.DRIVER_02_THRU,T.DRIVER_03_THRU,T.DRIVER_04_THRU,T.DRIVER_05_THRU,         
		T.DRIVER_06_THRU,T.DRIVER_07_THRU,T.DRIVER_08_THRU,T.DRIVER_09_THRU,T.DRIVER_10_THRU,         
		T.DRIVER_11_THRU,T.DRIVER_12_THRU,T.DRIVER_13_THRU,T.DRIVER_14_THRU,T.DRIVER_15_THRU,         
		T.DRIVER_16_THRU,T.DRIVER_17_THRU,T.DRIVER_18_THRU,T.DRIVER_19_THRU,T.DRIVER_20_THRU,         
		T.DRIVER_21_THRU,T.DRIVER_22_THRU,T.DRIVER_23_THRU,T.DRIVER_24_THRU,T.DRIVER_25_THRU,         
		T.DRIVER_26_THRU,T.DRIVER_27_THRU,T.DRIVER_28_THRU,T.DRIVER_29_THRU,T.DRIVER_30_THRU      
	FROM	TB_TAX_MATRIX T 
	left outer join TB_TAXCODE_DETAIL TB_TAXCODE_DETAIL_A on (T.THEN_TAXCODE_DETAIL_ID = TB_TAXCODE_DETAIL_A.TAXCODE_DETAIL_ID)         
	left outer join TB_TAXCODE_DETAIL TB_TAXCODE_DETAIL_B on (T.ELSE_TAXCODE_DETAIL_ID = TB_TAXCODE_DETAIL_B.TAXCODE_DETAIL_ID)        
	WHERE 1=1 '
	||p_where_clause_token||' '
	||p_order_by_token    ||' ) B WHERE rownum <= '|| (p_start_index + p_max_rows) ||' ) WHERE rnum > '||p_start_index;

	RETURN l_cursor;
END;
/

SHOW ERRORS;

DROP PROCEDURE sp_truncate_bcp_tables;

CREATE OR REPLACE FUNCTION sp_truncate_bcp_tables
   RETURN NUMBER
IS
    v_count		  		   			number :=0;
    e_halt                          exception;

-- ****** Program starts ****** --------------------------------------------------------------------
BEGIN
   -- *** Check for Users in STS? ***
   SELECT count(*)
     INTO v_count
     FROM v$session
    WHERE audsid <> 0
      AND audsid NOT IN
         (SELECT sys_context('USERENV','SESSIONID') FROM DUAL);
   IF v_count > 0 Then
      RAISE e_halt;
   END IF;
  Return 0;

   -- *** Import/Process Transactions ***
   -- Delete batches
   DELETE
     FROM tb_bcp_transactions
    WHERE tb_bcp_transactions.process_batch_no IN (
      SELECT DISTINCT tb_bcp_transactions.process_batch_no
        FROM tb_bcp_transactions, tb_batch
       WHERE tb_bcp_transactions.process_batch_no = tb_batch.batch_id
         AND tb_batch.batch_status_code IN ('ABI','ABP','D','P') );

   -- Truncate tb_bcp_transactions
   SELECT count(*)
   INTO v_count
   FROM tb_bcp_transactions;

   IF v_count IS NULL OR v_count = 0 THEN
      EXECUTE IMMEDIATE 'TRUNCATE TABLE tb_bcp_transactions';
   END IF;

   -- *** Tax Rate Updates - Tax Rates ***
   -- Delete batches
   DELETE
     FROM tb_bcp_jurisdiction_taxrate
    WHERE tb_bcp_jurisdiction_taxrate.batch_id IN (
      SELECT DISTINCT tb_bcp_jurisdiction_taxrate.batch_id
        FROM tb_bcp_jurisdiction_taxrate, tb_batch
       WHERE tb_bcp_jurisdiction_taxrate.batch_id = tb_batch.batch_id
         AND tb_batch.batch_status_code IN ('ABTR','D','TR') );

   -- Truncate tb_bcp_jurisdiction_taxrate
   SELECT count(*)
   INTO v_count
   FROM tb_bcp_jurisdiction_taxrate;

   IF v_count IS NULL OR v_count = 0 THEN
      EXECUTE IMMEDIATE 'TRUNCATE TABLE tb_bcp_jurisdiction_taxrate';
   END IF;

   -- *** Tax Rate Updates - CCH Tax Codes ***
   -- Delete batches
   /*DELETE
     FROM tb_bcp_cch_code
    WHERE tb_bcp_cch_code.batch_id IN (
      SELECT DISTINCT tb_bcp_cch_code.batch_id
        FROM tb_bcp_cch_code, tb_batch
       WHERE tb_bcp_cch_code.batch_id = tb_batch.batch_id
         AND tb_batch.batch_status_code IN ('ABTR','D','TR') );

   -- Truncate tb_bcp_cch_code
   SELECT count(*)
   INTO v_count
   FROM tb_bcp_cch_code;

   IF v_count IS NULL OR v_count = 0 THEN
      EXECUTE IMMEDIATE 'TRUNCATE TABLE tb_bcp_cch_code';
   END IF;

   -- *** Tax Rate Updates - CCH Tax Matrix ***
   -- Delete batches
   DELETE
     FROM tb_bcp_cch_txmatrix
    WHERE tb_bcp_cch_txmatrix.batch_id IN (
      SELECT DISTINCT tb_bcp_cch_txmatrix.batch_id
        FROM tb_bcp_cch_txmatrix, tb_batch
       WHERE tb_bcp_cch_txmatrix.batch_id = tb_batch.batch_id
         AND tb_batch.batch_status_code IN ('ABTR','D','TR') );

   -- Truncate tb_bcp_cch_code
   SELECT count(*)
   INTO v_count
   FROM tb_bcp_cch_txmatrix;

   IF v_count IS NULL OR v_count = 0 THEN
      EXECUTE IMMEDIATE 'TRUNCATE TABLE tb_bcp_cch_txmatrix';
   END IF;*/

   Return 0;
EXCEPTION
   WHEN e_halt THEN
      return 1;
END;
/

SHOW ERRORS;

CREATE OR REPLACE FORCE VIEW TB_VIEW_COLUMN_NAMES
(TABLE_NAME, COLUMN_NAME)
AS 
SELECT	ALL_TAB_COLUMNS.TABLE_NAME,
	ALL_TAB_COLUMNS.COLUMN_NAME
FROM	ALL_TAB_COLUMNS
WHERE (ALL_TAB_COLUMNS.OWNER = 'STSCORP');

CREATE OR REPLACE FORCE VIEW TB_VIEW_TABLE_NAMES
(TABLE_NAME)
AS 
SELECT	ALL_TABLES.TABLE_NAME
FROM	ALL_TABLES
WHERE (ALL_TABLES.OWNER = 'STSCORP');


CREATE OR REPLACE PROCEDURE sp_batch_process(an_batch_id number)
/* ************************************************************************************************/
/* Object Type/Name: Stored Procedure - sp_batch_process                                          */
/* Author:           Michael B. Fuller                                                            */
/* Date:             a long time ago                                                              */
/* Description:      process a batch of transactions                                              */
/* Arguments:        an_batch_id number                                                           */
/* Returns:          None                                                                         */
/* ************************************************************************************************/
/* Maintenance History:                                                                           */
/* Scan  Author       Date     Release    Description                                  Ticket #   */
/* ----- ---------- ---------- ---------- -------------------------------------------- ---------- */
/*       M. Fuller  06/02/2006 3.3.5.0    Fix processing an allocated line - Temporary 871        */
/*       M. Fuller  01/04/2007 3.3.5.1    Fix processing an allocated line - Permenant 871        */
/* MBF01 M. Fuller                        Add Kill Process Flag                                   */
/* MBF02 M. Fuller  01/25/2007 3.3.5.1    Add Allocations                                         */
/* MBF03 M. Fuller  05/18/2007 3.3.7a.x   Add User Exit to end of procedure            888        */
/* MBF04 M. Fuller  08/13/2007 3.4.1.1    Add 5 Calc Tax Flags                         883        */
/* ************************************************************************************************/
IS
-- Define Cursor Type
   TYPE cursor_type is REF CURSOR;

-- Location Matrix Selection SQL           // MBF04
   vc_location_matrix_select       VARCHAR2(1000) :=
      'SELECT tb_location_matrix.location_matrix_id, ' ||
             'tb_location_matrix.jurisdiction_id, ' ||
             'tb_location_matrix.override_taxtype_code, ' ||
             'tb_location_matrix.state_flag,' ||
             'tb_location_matrix.county_flag,' ||
             'tb_location_matrix.county_local_flag,' ||
             'tb_location_matrix.city_flag,' ||
             'tb_location_matrix.city_local_flag,' ||
             'tb_jurisdiction.state, ' ||
             'tb_jurisdiction.county, ' ||
             'tb_jurisdiction.city, ' ||
             'tb_jurisdiction.zip, ' ||
             'tb_jurisdiction.in_out ' ||
        'FROM tb_location_matrix, ' ||
             'tb_jurisdiction, ' ||
             'tb_bcp_transactions ' ||
      'WHERE ( tb_bcp_transactions.rowid = :v_rowid ) AND ' ||
            '( tb_location_matrix.default_flag = ''0'' AND tb_location_matrix.binary_weight > 0 ) AND ' ||
            '( tb_location_matrix.effective_date <= tb_bcp_transactions.gl_date ) AND ( tb_location_matrix.expiration_date >= tb_bcp_transactions.gl_date ) AND ' ||
            '( tb_location_matrix.jurisdiction_id = tb_jurisdiction.jurisdiction_id ) ';
   vc_location_matrix_where        VARCHAR2(4000) := '';
   vc_location_matrix_orderby      VARCHAR2(1000) :=
      'ORDER BY tb_location_matrix.binary_weight DESC, tb_location_matrix.significant_digits DESC, tb_location_matrix.effective_date DESC';
   vc_location_matrix_stmt         VARCHAR2 (6000);
   location_matrix_cursor          cursor_type;

-- Location Matrix Record TYPE
   /*TYPE location_matrix_record is RECORD (
      location_matrix_id           tb_location_matrix.location_matrix_id%TYPE,
      jurisdiction_id              tb_location_matrix.jurisdiction_id%TYPE,
      override_taxtype_code        tb_location_matrix.override_taxtype_code%TYPE,
      state_flag                   tb_location_matrix.state_flag%TYPE,
      county_flag                  tb_location_matrix.county_flag%TYPE,
      county_local_flag            tb_location_matrix.county_local_flag%TYPE,
      city_flag                    tb_location_matrix.city_flag%TYPE,
      city_local_flag              tb_location_matrix.city_local_flag%TYPE,
      state                        tb_jurisdiction.state%TYPE,
      county                       tb_jurisdiction.county%TYPE,
      city                         tb_jurisdiction.city%TYPE,
      zip                          tb_jurisdiction.zip%TYPE,
      in_out                       tb_jurisdiction.in_out%TYPE );*/
   location_matrix                 matrix_record_pkg.location_matrix_record;

-- Tax Matrix Selection SQL
   vc_tax_matrix_select            VARCHAR2(2000) :=
      'SELECT tb_tax_matrix.tax_matrix_id, tb_tax_matrix.relation_sign, tb_tax_matrix.relation_amount, ' ||
             'tb_tax_matrix.then_hold_code_flag, tb_tax_matrix.else_hold_code_flag, ' ||
             'tb_tax_matrix.then_taxcode_detail_id, tb_tax_matrix.else_taxcode_detail_id, ' ||
             'tb_tax_matrix.then_cch_taxcat_code, tb_tax_matrix.then_cch_group_code, tb_tax_matrix.then_cch_item_code, ' ||
             'tb_tax_matrix.else_cch_taxcat_code, tb_tax_matrix.else_cch_group_code, tb_tax_matrix.else_cch_item_code, ' ||
             'thentaxcddtl.taxcode_state_code, thentaxcddtl.taxcode_type_code, thentaxcddtl.taxcode_code, thentaxcddtl.jurisdiction_id, thentaxcddtl.measure_type_code, thentaxcddtl.taxtype_code, ' ||
             'elsetaxcddtl.taxcode_state_code, elsetaxcddtl.taxcode_type_code, elsetaxcddtl.taxcode_code, elsetaxcddtl.jurisdiction_id, elsetaxcddtl.measure_type_code, elsetaxcddtl.taxtype_code ' ||
       'FROM tb_tax_matrix, ' ||
            'tb_taxcode_detail thentaxcddtl, ' ||
            'tb_taxcode_detail elsetaxcddtl, ' ||
            'tb_bcp_transactions ' ||
      'WHERE ( tb_bcp_transactions.rowid = :v_rowid ) AND ' ||
            '( tb_tax_matrix.default_flag = ''0'' AND tb_tax_matrix.binary_weight > 0 ) AND ' ||
            '(( tb_tax_matrix.matrix_state_code = ''*ALL'' ) OR ( tb_tax_matrix.matrix_state_code = :v_transaction_state_code )) AND ' ||
            '( tb_tax_matrix.effective_date <= tb_bcp_transactions.gl_date ) AND ( tb_tax_matrix.expiration_date >= tb_bcp_transactions.gl_date ) AND ' ||
            '( tb_tax_matrix.then_taxcode_detail_id = thentaxcddtl.taxcode_detail_id (+)) AND ' ||
            '( tb_tax_matrix.else_taxcode_detail_id = elsetaxcddtl.taxcode_detail_id (+)) ';
   vc_tax_matrix_where             VARCHAR2(6000) := '';
   vc_tax_matrix_orderby           VARCHAR2(1000) :=
      'ORDER BY tb_tax_matrix.binary_weight DESC, tb_tax_matrix.significant_digits DESC, tb_tax_matrix.effective_date DESC';
   vc_tax_matrix_stmt              VARCHAR2 (9000);
   tax_matrix_cursor               cursor_type;

-- Tax Matrix Record TYPE
   /*TYPE tax_matrix_record is RECORD (
      tax_matrix_id                tb_tax_matrix.tax_matrix_id%TYPE,
      relation_sign                tb_tax_matrix.relation_sign%TYPE,
      relation_amount              tb_tax_matrix.relation_amount%TYPE,
      then_hold_code_flag          tb_tax_matrix.then_hold_code_flag%TYPE,
      else_hold_code_flag          tb_tax_matrix.else_hold_code_flag%TYPE,
      then_taxcode_detail_id       tb_bcp_transactions.taxcode_detail_id%TYPE,
      else_taxcode_detail_id       tb_bcp_transactions.taxcode_detail_id%TYPE,
      then_cch_taxcat_code         tb_tax_matrix.then_cch_taxcat_code%TYPE,
      then_cch_group_code          tb_tax_matrix.then_cch_group_code%TYPE,
      then_cch_item_code           tb_tax_matrix.then_cch_item_code%TYPE,
      else_cch_taxcat_code         tb_tax_matrix.else_cch_taxcat_code%TYPE,
      else_cch_group_code          tb_tax_matrix.else_cch_group_code%TYPE,
      else_cch_item_code           tb_tax_matrix.else_cch_item_code%TYPE,
      then_taxcode_state_code      tb_bcp_transactions.taxcode_state_code%TYPE,
      then_taxcode_type_code       tb_bcp_transactions.taxcode_type_code%TYPE,
      then_taxcode_code            tb_bcp_transactions.taxcode_code%TYPE,
      then_jurisdiction_id         tb_bcp_transactions.jurisdiction_id%TYPE,
      then_measure_type_code       tb_bcp_transactions.measure_type_code%TYPE,
      then_taxtype_code            tb_taxcode.taxtype_code%TYPE,
      else_taxcode_state_code      tb_bcp_transactions.taxcode_state_code%TYPE,
      else_taxcode_type_code       tb_bcp_transactions.taxcode_type_code%TYPE,
      else_taxcode_code            tb_bcp_transactions.taxcode_code%TYPE,
      else_jurisdiction_id         tb_bcp_transactions.jurisdiction_id%TYPE,
      else_measure_type_code       tb_bcp_transactions.measure_type_code%TYPE,
      else_taxtype_code            tb_taxcode.taxtype_code%TYPE );*/
   tax_matrix                      matrix_record_pkg.tax_matrix_record;

-- Driver Reference Insert SQL
   vc_driver_ref_insert_stmt       VARCHAR2(3000);

-- Table defined variables
   v_batch_status_code             tb_batch.batch_status_code%TYPE                   := 'P';
   v_starting_sequence             tb_batch.start_row%TYPE                           := 0;
   v_processed_rows                tb_batch.processed_rows%TYPE                      := 0;
   v_error_sev_code                tb_batch.error_sev_code%TYPE                      := ' ';
   v_severity_level                tb_list_code.severity_level%TYPE                  := ' ';
   v_write_import_line_flag        tb_list_code.write_import_line_flag%TYPE          := '1';
   v_abort_import_flag             tb_list_code.abort_import_flag%TYPE               := '0';
   v_location_abort_import_flag    tb_list_code.abort_import_flag%TYPE               := '0';
   v_tax_abort_import_flag         tb_list_code.abort_import_flag%TYPE               := '0';
   v_allocation_flag               tb_option.value%TYPE                              := NULL;
   v_delete_zero_amounts           tb_option.value%TYPE                              := NULL;
   v_hold_trans_flag               tb_option.value%TYPE                              := NULL;
   v_hold_trans_amount             tb_option.value%TYPE                              := NULL;
   v_autoproc_flag                 tb_option.value%TYPE                              := NULL;
   v_autoproc_amount               tb_option.value%TYPE                              := NULL;
   v_autoproc_id                   tb_option.value%TYPE                              := NULL;
   v_processuserexit_flag          tb_option.value%TYPE                              := NULL;
   v_autoproc_state_code           tb_taxcode_detail.taxcode_state_code%TYPE         := NULL;
   v_autoproc_type_code            tb_taxcode_detail.taxcode_type_code%TYPE          := NULL;
   v_autoproc_code                 tb_taxcode_detail.taxcode_code%TYPE               := NULL;
   v_hold_code_flag                tb_tax_matrix.then_hold_code_flag%TYPE            := NULL;
   v_less_hold_code_flag           tb_tax_matrix.then_hold_code_flag%TYPE            := NULL;
   v_less_taxcode_detail_id        tb_tax_matrix.then_taxcode_detail_id%TYPE         := NULL;
   v_less_cch_taxcat_code          tb_tax_matrix.then_cch_taxcat_code%TYPE           := NULL;
   v_less_cch_group_code           tb_tax_matrix.then_cch_group_code%TYPE            := NULL;
   v_equal_hold_code_flag          tb_tax_matrix.then_hold_code_flag%TYPE            := NULL;
   v_equal_taxcode_detail_id       tb_tax_matrix.then_taxcode_detail_id%TYPE         := NULL;
   v_equal_cch_taxcat_code         tb_tax_matrix.then_cch_taxcat_code%TYPE           := NULL;
   v_equal_cch_group_code          tb_tax_matrix.then_cch_group_code%TYPE            := NULL;
   v_greater_hold_code_flag        tb_tax_matrix.then_hold_code_flag%TYPE            := NULL;
   v_greater_taxcode_detail_id     tb_tax_matrix.then_taxcode_detail_id%TYPE         := NULL;
   v_greater_cch_taxcat_code       tb_tax_matrix.then_cch_taxcat_code%TYPE           := NULL;
   v_greater_cch_group_code        tb_tax_matrix.then_cch_group_code%TYPE            := NULL;
   v_jurisdiction_id               tb_jurisdiction.jurisdiction_id%TYPE              := NULL;
   v_less_taxcode_state_code       tb_taxcode_detail.taxcode_state_code%TYPE         := NULL;
   v_less_taxcode_type_code        tb_taxcode_detail.taxcode_type_code%TYPE          := NULL;
   v_less_taxcode_code             tb_taxcode_detail.taxcode_code%TYPE               := NULL;
   v_less_jurisdiction_id          tb_taxcode_detail.jurisdiction_id%TYPE            := NULL;
   v_less_measure_type_code        tb_taxcode_detail.measure_type_code%TYPE          := NULL;
   v_less_taxtype_code             tb_taxcode_detail.taxtype_code%TYPE               := NULL;
   v_equal_taxcode_state_code      tb_taxcode_detail.taxcode_state_code%TYPE         := NULL;
   v_equal_taxcode_type_code       tb_taxcode_detail.taxcode_type_code%TYPE          := NULL;
   v_equal_taxcode_code            tb_taxcode_detail.taxcode_code%TYPE               := NULL;
   v_equal_jurisdiction_id         tb_taxcode_detail.jurisdiction_id%TYPE            := NULL;
   v_equal_measure_type_code       tb_taxcode_detail.measure_type_code%TYPE          := NULL;
   v_equal_taxtype_code            tb_taxcode_detail.taxtype_code%TYPE               := NULL;
   v_greater_taxcode_state_code    tb_taxcode_detail.taxcode_state_code%TYPE         := NULL;
   v_greater_taxcode_type_code     tb_taxcode_detail.taxcode_type_code%TYPE          := NULL;
   v_greater_taxcode_code          tb_taxcode_detail.taxcode_code%TYPE               := NULL;
   v_greater_jurisdiction_id       tb_taxcode_detail.jurisdiction_id%TYPE            := NULL;
   v_greater_measure_type_code     tb_taxcode_detail.measure_type_code%TYPE          := NULL;
   v_greater_taxtype_code          tb_taxcode_detail.taxtype_code%TYPE               := NULL;
   v_gl_date                       tb_bcp_transactions.gl_date%TYPE                  := NULL;
   v_transaction_state_code        tb_bcp_transactions.transaction_state_code%TYPE   := NULL;
   v_override_jurisdiction_id      tb_bcp_transactions.jurisdiction_id%TYPE          := NULL;
   v_sysdate                       tb_bcp_transactions.load_timestamp%TYPE           := SYSDATE;
   v_transaction_amt               tb_bcp_transactions.gl_line_itm_dist_amt%TYPE     := 0;
   v_kill_proc_flag                tb_option.value%TYPE                              := NULL;
   v_kill_proc_count               tb_option.value%TYPE                              := NULL;
   v_taxtype_code                  tb_taxcode.taxtype_code%TYPE                      := NULL;
   v_taxtype_used                  tb_taxcode.taxtype_code%TYPE                      := NULL;

-- Program defined variables
   v_rowid                         ROWID                                             := NULL;
   --vi_cursor_id                    INTEGER                                           := 0;
   --vi_rows_processed               INTEGER                                           := 0;
   vn_hold_trans_amount            NUMBER                                            := 0;
   vn_autoproc_amount              NUMBER                                            := 0;
   vn_autoproc_id                  NUMBER                                            := 0;
   vn_fetch_rows                   NUMBER                                            := 0;
   vi_error_count                  INTEGER                                           := 0;
   vn_actual_rowno                 NUMBER                                            := 0;
   --vc_driver_ref_ins_stmt          VARCHAR2(5000)                                    := NULL;
   vc_state_driver_flag            CHAR(1)                                           := '0';
   vn_kill_proc_count              NUMBER                                            := 0;
   vn_kill_proc_counter            NUMBER                                            := 0;
   vc_batch_status_code            VARCHAR2(10)                                      := NULL;
   vn_trans_created                NUMBER                                            := 0;
   vn_return                       NUMBER                                            := 0;
-- temporary
   vn_taxable_amt                  NUMBER                                            := 0;

/*-- Define Location Driver Names Cursor (tb_driver_names)
   CURSOR location_driver_cursor
   IS
        SELECT tb_driver_names.trans_dtl_column_name, tb_driver_names.matrix_column_name, tb_driver_names.null_driver_flag, tb_driver_names.wildcard_flag,
               datadefcol.desc_column_name, datadefcol.data_type
          FROM tb_driver_names,
               (SELECT *
                  FROM tb_data_def_column
                 WHERE table_name = 'TB_TRANSACTION_DETAIL') datadefcol
         WHERE tb_driver_names.driver_names_code = 'L' AND
               tb_driver_names.trans_dtl_column_name = datadefcol.column_name
      ORDER BY tb_driver_names.driver_id;
      location_driver              location_driver_cursor%ROWTYPE;*/

/*-- Define Tax Driver Names Cursor (tb_driver_names)
   CURSOR tax_driver_cursor
   IS
        SELECT tb_driver_names.trans_dtl_column_name, tb_driver_names.matrix_column_name, tb_driver_names.null_driver_flag, tb_driver_names.wildcard_flag, tb_driver_names.range_flag, tb_driver_names.to_number_flag,
               datadefcol.desc_column_name, datadefcol.data_type
          FROM tb_driver_names,
               (SELECT *
                  FROM tb_data_def_column
                 WHERE table_name = 'TB_TRANSACTION_DETAIL') datadefcol
         WHERE tb_driver_names.driver_names_code = 'T' AND
               tb_driver_names.trans_dtl_column_name = datadefcol.column_name
      ORDER BY tb_driver_names.driver_id;
      tax_driver                   tax_driver_cursor%ROWTYPE;*/

-- Define BCP Transactions Cursor (tb_bcp_transactions)
   -- 3411 - add taxable amount and tax type used.
   CURSOR bcp_transactions_cursor
   IS
      SELECT rowid,
             transaction_detail_id,
             source_transaction_id,
             process_batch_no,
             gl_extract_batch_no,
             archive_batch_no,
             allocation_matrix_id,
             allocation_subtrans_id,
             entered_date,
             transaction_status,
             gl_date,
             gl_company_nbr,
             gl_company_name,
             gl_division_nbr,
             gl_division_name,
             gl_cc_nbr_dept_id,
             gl_cc_nbr_dept_name,
             gl_local_acct_nbr,
             gl_local_acct_name,
             gl_local_sub_acct_nbr,
             gl_local_sub_acct_name,
             gl_full_acct_nbr,
             gl_full_acct_name,
             gl_line_itm_dist_amt,
             orig_gl_line_itm_dist_amt,
             vendor_nbr,
             vendor_name,
             vendor_address_line_1,
             vendor_address_line_2,
             vendor_address_line_3,
             vendor_address_line_4,
             vendor_address_city,
             vendor_address_county,
             vendor_address_state,
             vendor_address_zip,
             vendor_address_country,
             vendor_type,
             vendor_type_name,
             invoice_nbr,
             invoice_desc,
             invoice_date,
             invoice_freight_amt,
             invoice_discount_amt,
             invoice_tax_amt,
             invoice_total_amt,
             invoice_tax_flg,
             invoice_line_nbr,
             invoice_line_name,
             invoice_line_type,
             invoice_line_type_name,
             invoice_line_amt,
             invoice_line_tax,
             afe_project_nbr,
             afe_project_name,
             afe_category_nbr,
             afe_category_name,
             afe_sub_cat_nbr,
             afe_sub_cat_name,
             afe_use,
             afe_contract_type,
             afe_contract_structure,
             afe_property_cat,
             inventory_nbr,
             inventory_name,
             inventory_class,
             inventory_class_name,
             po_nbr,
             po_name,
             po_date,
             po_line_nbr,
             po_line_name,
             po_line_type,
             po_line_type_name,
             ship_to_location,
             ship_to_location_name,
             ship_to_address_line_1,
             ship_to_address_line_2,
             ship_to_address_line_3,
             ship_to_address_line_4,
             ship_to_address_city,
             ship_to_address_county,
             ship_to_address_state,
             ship_to_address_zip,
             ship_to_address_country,
             wo_nbr,
             wo_name,
             wo_date,
             wo_type,
             wo_type_desc,
             wo_class,
             wo_class_desc,
             wo_entity,
             wo_entity_desc,
             wo_line_nbr,
             wo_line_name,
             wo_line_type,
             wo_line_type_desc,
             wo_shut_down_cd,
             wo_shut_down_cd_desc,
             voucher_id,
             voucher_name,
             voucher_date,
             voucher_line_nbr,
             voucher_line_desc,
             check_nbr,
             check_no,
             check_date,
             check_amt,
             check_desc,
             user_text_01,
             user_text_02,
             user_text_03,
             user_text_04,
             user_text_05,
             user_text_06,
             user_text_07,
             user_text_08,
             user_text_09,
             user_text_10,
             user_text_11,
             user_text_12,
             user_text_13,
             user_text_14,
             user_text_15,
             user_text_16,
             user_text_17,
             user_text_18,
             user_text_19,
             user_text_20,
             user_text_21,
             user_text_22,
             user_text_23,
             user_text_24,
             user_text_25,
             user_text_26,
             user_text_27,
             user_text_28,
             user_text_29,
             user_text_30,
             user_number_01,
             user_number_02,
             user_number_03,
             user_number_04,
             user_number_05,
             user_number_06,
             user_number_07,
             user_number_08,
             user_number_09,
             user_number_10,
             user_date_01,
             user_date_02,
             user_date_03,
             user_date_04,
             user_date_05,
             user_date_06,
             user_date_07,
             user_date_08,
             user_date_09,
             user_date_10,
             comments,
             tb_calc_tax_amt,
             state_use_amount,
             state_use_tier2_amount,
             state_use_tier3_amount,
             county_use_amount,
             county_local_use_amount,
             city_use_amount,
             city_local_use_amount,
             transaction_state_code,
             auto_transaction_state_code,
             transaction_ind,
             suspend_ind,
             taxcode_detail_id,
             taxcode_state_code,
             taxcode_type_code,
             taxcode_code,
             cch_taxcat_code,
             cch_group_code,
             cch_item_code,
             manual_taxcode_ind,
             tax_matrix_id,
             location_matrix_id,
             jurisdiction_id,
             jurisdiction_taxrate_id,
             manual_jurisdiction_ind,
             measure_type_code,
             state_use_rate,
             state_use_tier2_rate,
             state_use_tier3_rate,
             state_split_amount,
             state_tier2_min_amount,
             state_tier2_max_amount,
             state_maxtax_amount,
             county_use_rate,
             county_local_use_rate,
             county_split_amount,
             county_maxtax_amount,
             county_single_flag,
             county_default_flag,
             city_use_rate,
             city_local_use_rate,
             city_split_amount,
             city_split_use_rate,
             city_single_flag,
             city_default_flag,
             combined_use_rate,
             load_timestamp,
             gl_extract_updater,
             gl_extract_timestamp,
             gl_extract_flag,
             gl_log_flag,
             gl_extract_amt,
             audit_flag,
             audit_user_id,
             audit_timestamp,
             modify_user_id,
             modify_timestamp,
             update_user_id,
             update_timestamp
        FROM tb_bcp_transactions
       WHERE process_batch_no = an_batch_id
         AND (( transaction_detail_id is NULL ) OR ( transaction_detail_id <> -1 ));
      bcp_transactions             bcp_transactions_cursor%ROWTYPE;

-- Define Exceptions
   e_badread                       exception;
   e_badupdate                     exception;
   e_badwrite                      exception;
   e_wrongdata                     exception;
   e_abort                         exception;
   e_kill                          exception;

-- ****** Program starts ****** --------------------------------------------------------------------
BEGIN
   -- Confirm batch exists and is flagged for processing
   BEGIN
      SELECT batch_status_code
        INTO v_batch_status_code
        FROM tb_batch
       WHERE batch_id = an_batch_id;
      IF SQLCODE != 0 THEN
         RAISE e_badread;
      ELSIF v_batch_status_code != 'FP' THEN
         RAISE e_wrongdata;
      END IF;
   EXCEPTION
      WHEN NO_DATA_FOUND OR e_badread THEN
         vi_error_count := vi_error_count + 1;
         sp_geterrorcode('P8', an_batch_id, 'P', v_sysdate, v_severity_level, v_write_import_line_flag, v_abort_import_flag);
         IF v_severity_level > v_error_sev_code THEN
            v_error_sev_code := v_severity_level;
         END IF;
      WHEN e_wrongdata THEN
         vi_error_count := vi_error_count + 1;
         sp_geterrorcode('P9', an_batch_id, 'P', v_sysdate, v_severity_level, v_write_import_line_flag, v_abort_import_flag);
         IF v_severity_level > v_error_sev_code THEN
            v_error_sev_code := v_severity_level;
         END IF;
   END;
   -- Abort Procedure?
   IF v_abort_import_flag = '1' THEN
      v_batch_status_code := 'ABP';
      RAISE e_abort;
   END IF;

   -- RUN ALLOCATIONS??
   -- Determine Allocation Flag -- 01/25/2007 -- MBF02
   BEGIN
      SELECT LTRIM(RTRIM(value))
        INTO v_allocation_flag
        FROM tb_option
       WHERE option_code = 'ALLOCATIONSENABLED' AND
             option_type_code = 'ADMIN' AND
             user_code = 'ADMIN';
   EXCEPTION
      WHEN OTHERS THEN
         v_allocation_flag := '0';
   END;
   IF v_allocation_flag IS NULL THEN
      v_allocation_flag := '0';
   END IF;
   -- Determine Allocation Flag -- 01/25/2007 -- MBF02

   -- Perform Allocations -- 01/25/2007 -- MBF02
   IF v_allocation_flag = '1' THEN
      sp_import_allocation(an_batch_id);
   END IF;
   -- Perform Allocations -- 01/25/2007 -- MBF02

   -- Get starting sequence no.
   BEGIN
      SELECT last_number
        INTO v_starting_sequence
        FROM user_sequences
       WHERE sequence_name = 'SQ_TB_TRANSACTION_DETAIL_ID';
   EXCEPTION
      WHEN OTHERS THEN
         NULL;
   END;

   -- Update batch
   BEGIN
      UPDATE tb_batch
         SET batch_status_code = 'XP',
             error_sev_code = '',
             start_row = v_starting_sequence,
             actual_start_timestamp = SYSDATE
       WHERE batch_id = an_batch_id;
      IF SQLCODE != 0 THEN
         RAISE e_badupdate;
      END IF;
   EXCEPTION
      WHEN e_badupdate THEN
         vi_error_count := vi_error_count + 1;
         sp_geterrorcode('P1', an_batch_id, 'P', v_sysdate, v_severity_level, v_write_import_line_flag, v_abort_import_flag);
         IF v_severity_level > v_error_sev_code THEN
            v_error_sev_code := v_severity_level;
         END IF;
   END;
   -- Abort Procedure?
   IF v_abort_import_flag = '1' THEN
      v_batch_status_code := 'ABP';
      RAISE e_abort;
   END IF;

   -- Determine Global Delete Zero Amounts Flag
   BEGIN
      SELECT LTRIM(RTRIM(value))
        INTO v_delete_zero_amounts
        FROM tb_option
       WHERE option_code = 'DELETE0AMTFLAG' AND
             option_type_code = 'SYSTEM' AND
             user_code = 'SYSTEM';
   EXCEPTION
      WHEN OTHERS THEN
         v_delete_zero_amounts := '0';
   END;
   IF v_delete_zero_amounts IS NULL THEN
      v_delete_zero_amounts := '0';
   END IF;

   -- Determine Global Hold Transactions Flag and Amount
   BEGIN
      SELECT LTRIM(RTRIM(value))
        INTO v_hold_trans_flag
        FROM tb_option
       WHERE option_code = 'HOLDTRANSFLAG' AND
             option_type_code = 'SYSTEM' AND
             user_code = 'SYSTEM';
   EXCEPTION
      WHEN OTHERS THEN
         v_hold_trans_flag := '0';
   END;
   IF v_hold_trans_flag IS NULL THEN
      v_hold_trans_flag := '0';
   END IF;
   IF v_hold_trans_flag = '1' THEN
      BEGIN
         SELECT LTRIM(RTRIM(value))
           INTO v_hold_trans_amount
           FROM tb_option
          WHERE option_code = 'HOLDTRANSAMT' AND
                option_type_code = 'SYSTEM' AND
                user_code = 'SYSTEM';
      EXCEPTION
         WHEN OTHERS THEN
            v_hold_trans_amount := '0';
      END;
      IF v_hold_trans_amount IS NULL THEN
         v_hold_trans_amount := '0';
      END IF;
      vn_hold_trans_amount := TO_NUMBER(v_hold_trans_amount);
   END IF;

   -- Determine "auto-process" Flag, Amount, and TaxCode Detail ID
   BEGIN
      SELECT LTRIM(RTRIM(value))
        INTO v_autoproc_flag
        FROM tb_option
       WHERE option_code = 'AUTOPROCFLAG' AND
             option_type_code = 'SYSTEM' AND
             user_code = 'SYSTEM';
   EXCEPTION
      WHEN OTHERS THEN
         v_hold_trans_flag := '0';
   END;
   IF v_autoproc_flag IS NULL THEN
      v_autoproc_flag := '0';
   END IF;

   IF v_autoproc_flag = '1' THEN
      BEGIN
         SELECT LTRIM(RTRIM(value))
           INTO v_autoproc_amount
           FROM tb_option
          WHERE option_code = 'AUTOPROCAMT' AND
                option_type_code = 'SYSTEM' AND
                user_code = 'SYSTEM';
      EXCEPTION
         WHEN OTHERS THEN
            v_autoproc_amount := '0';
      END;
      IF v_autoproc_amount IS NULL THEN
         v_autoproc_amount := '0';
      END IF;
      vn_autoproc_amount := TO_NUMBER(v_autoproc_amount);

      BEGIN
         SELECT LTRIM(RTRIM(value))
           INTO v_autoproc_id
           FROM tb_option
          WHERE option_code = 'AUTOPROCID' AND
                option_type_code = 'SYSTEM' AND
                user_code = 'SYSTEM';
      EXCEPTION
         WHEN OTHERS THEN
            v_autoproc_id := '0';
      END;
      IF v_autoproc_id IS NULL THEN
         v_autoproc_id := '0';
      END IF;
      vn_autoproc_id := TO_NUMBER(v_autoproc_id);

      BEGIN
         SELECT taxcode_state_code, taxcode_type_code, taxcode_code
           INTO v_autoproc_state_code, v_autoproc_type_code, v_autoproc_code
           FROM tb_taxcode_detail
          WHERE taxcode_detail_id = vn_autoproc_id;
      EXCEPTION
         WHEN OTHERS THEN
            v_autoproc_flag := '0';
      END;
      IF v_autoproc_state_code IS NULL OR v_autoproc_type_code IS NULL OR v_autoproc_code IS NULL THEN
         v_autoproc_flag := '0';
      END IF;
   END IF;

   -- Determine Kill Process Flag/Count -- MBF01 -- begin
   BEGIN
      SELECT LTRIM(RTRIM(value))
        INTO v_kill_proc_flag
        FROM tb_option
       WHERE option_code = 'KILLPROCFLAG' AND
             option_type_code = 'SYSTEM' AND
             user_code = 'SYSTEM';
   EXCEPTION
      WHEN OTHERS THEN
         v_kill_proc_flag := '0';
   END;
   IF v_kill_proc_flag IS NULL THEN
      v_kill_proc_flag := '0';
   END IF;
   IF v_kill_proc_flag = '1' THEN
      BEGIN
         SELECT LTRIM(RTRIM(value))
           INTO v_kill_proc_count
           FROM tb_option
          WHERE option_code = 'KILLPROCCOUNT' AND
                option_type_code = 'SYSTEM' AND
                user_code = 'SYSTEM';
      EXCEPTION
         WHEN OTHERS THEN
            v_kill_proc_count := '5000';
      END;
      IF v_kill_proc_count IS NULL THEN
         v_kill_proc_count := '5000';
      END IF;
      vn_kill_proc_count := TO_NUMBER(v_kill_proc_count);
   END IF;
   -- Determine Kill Process Flag/Count -- MBF01 -- end

   -- ***** Create Dynamic WHERE Clause for Matrix SQLs ***** --
   -- Location Matrix Drivers
/* OPEN location_driver_cursor;
   LOOP
      FETCH location_driver_cursor INTO location_driver;
      EXIT WHEN location_driver_cursor%NOTFOUND;
      IF location_driver.null_driver_flag = '1' THEN
         IF location_driver.wildcard_flag = '1' THEN
            IF UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'CHAR' OR
               UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'VARC' THEN
                  vc_location_matrix_where := vc_location_matrix_where || 'AND ' ||
                  '((tb_bcp_transactions.' || location_driver.trans_dtl_column_name || ' IS NULL AND (tb_location_matrix.' || location_driver.matrix_column_name || ' = ''*ALL'' OR tb_location_matrix.' || location_driver.matrix_column_name || ' = ''*NULL'' )) OR ' ||
                  '(tb_bcp_transactions.' || location_driver.trans_dtl_column_name || ' IS NOT NULL AND (tb_location_matrix.' || location_driver.matrix_column_name || ' = ''*ALL'' OR tb_bcp_transactions.' || location_driver.trans_dtl_column_name || ' LIKE tb_location_matrix.' || location_driver.matrix_column_name || '))) ';
            END IF;
         ELSE
            IF UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'CHAR' OR
               UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'VARC' THEN
                  vc_location_matrix_where := vc_location_matrix_where || 'AND ' ||
                  '((tb_bcp_transactions.' || location_driver.trans_dtl_column_name || ' IS NULL AND (tb_location_matrix.' || location_driver.matrix_column_name || ' = ''*ALL'' OR tb_location_matrix.' || location_driver.matrix_column_name || ' = ''*NULL'' )) OR ' ||
                  '(tb_bcp_transactions.' || location_driver.trans_dtl_column_name || ' IS NOT NULL AND (tb_location_matrix.' || location_driver.matrix_column_name || ' = ''*ALL'' OR tb_bcp_transactions.' || location_driver.trans_dtl_column_name || ' = tb_location_matrix.' || location_driver.matrix_column_name || '))) ';
            END IF;
         END IF;
      ELSE
         IF location_driver.wildcard_flag = '1' THEN
            IF UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'CHAR' OR
               UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'VARC' THEN
                  vc_location_matrix_where := vc_location_matrix_where || 'AND ' ||
                  '(tb_location_matrix.' || location_driver.matrix_column_name || ' = ''*ALL'' OR tb_bcp_transactions.' || location_driver.trans_dtl_column_name || ' LIKE tb_location_matrix.' || location_driver.matrix_column_name || ') ';
            END IF;
         ELSE
            IF UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'CHAR' OR
               UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'VARC' THEN
                  vc_location_matrix_where := vc_location_matrix_where || 'AND ' ||
                  '(tb_location_matrix.' || location_driver.matrix_column_name || ' = ''*ALL'' OR tb_bcp_transactions.' || location_driver.trans_dtl_column_name || ' = tb_location_matrix.' || location_driver.matrix_column_name || ') ';
            END IF;
         END IF;
      END IF;
      -- Insert Driver Reference Values for this driver
      -- Create Insert SQL
      vc_driver_ref_ins_stmt :=
         'INSERT INTO tb_driver_reference (trans_dtl_column_name, driver_value, driver_description) ' ||
            'SELECT ''' || Upper(location_driver.trans_dtl_column_name) ||
                    ''', ' || location_driver.trans_dtl_column_name || ', ';
      IF location_driver.desc_column_name IS NOT NULL THEN
         vc_driver_ref_ins_stmt := vc_driver_ref_ins_stmt || 'min(' || location_driver.desc_column_name || ') ';
      ELSE
         vc_driver_ref_ins_stmt := vc_driver_ref_ins_stmt || 'NULL ';
      END IF;
      vc_driver_ref_ins_stmt := vc_driver_ref_ins_stmt ||
              'FROM tb_bcp_transactions ' ||
             'WHERE process_batch_no = ' || TO_CHAR(an_batch_id) || ' AND ' ||
                    location_driver.trans_dtl_column_name || ' IS NOT NULL AND ' ||
                    location_driver.trans_dtl_column_name || ' NOT IN ' ||
               '(SELECT driver_value ' ||
                  'FROM tb_driver_reference ' ||
                 'WHERE trans_dtl_column_name = ''' || Upper(location_driver.trans_dtl_column_name) || ''') ' ||
              'GROUP BY ''' || Upper(location_driver.trans_dtl_column_name) ||
                        ''', ' || location_driver.trans_dtl_column_name;
      -- Execute Insert SQL
      vi_cursor_id := DBMS_SQL.open_cursor;
      DBMS_SQL.parse (vi_cursor_id, vc_driver_ref_ins_stmt, DBMS_SQL.v7);
      vi_rows_processed := DBMS_SQL.execute (vi_cursor_id);
      DBMS_SQL.close_cursor (vi_cursor_id);
   END LOOP;
   CLOSE location_driver_cursor;*/

   -- New call to sp_gen_location_driver for MidTier Cleanup
   sp_gen_location_driver (
	p_generate_driver_reference	=> 'Y',
	p_an_batch_id			=> an_batch_id,
	p_transaction_table_name	=> 'tb_bcp_transactions',
	p_location_table_name		=> 'tb_location_matrix',
	p_vc_location_matrix_where	=> vc_location_matrix_where);

   -- If no drivers found raise error, else create location matrix sql statement
   IF vc_location_matrix_where IS NULL THEN
      vi_error_count := vi_error_count + 1;
      sp_geterrorcode('P2', an_batch_id, 'P', v_sysdate, v_severity_level, v_write_import_line_flag, v_abort_import_flag);
      IF v_severity_level > v_error_sev_code THEN
         v_error_sev_code := v_severity_level;
      END IF;
   ELSE
      vc_location_matrix_stmt := vc_location_matrix_select || vc_location_matrix_where || vc_location_matrix_orderby;
   END IF;

   -- Tax Matrix Drivers
   /*OPEN tax_driver_cursor;
   LOOP
      FETCH tax_driver_cursor INTO tax_driver;
      EXIT WHEN tax_driver_cursor%NOTFOUND;
      IF tax_driver.range_flag = '1' THEN
         IF tax_driver.to_number_flag = '1' THEN
            IF tax_driver.null_driver_flag = '1' THEN
               -- Range and ToNumber and NULL
               IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                  UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                  vc_tax_matrix_where := vc_tax_matrix_where || 'AND ( ' ||
                  '( UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU) = ''*NA'' AND ' ||
                     '( tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ' IS NULL AND ( UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') = ''*ALL'' OR UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') = ''*NULL'' )) OR ' ||
                     '( tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND ( UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') = ''*ALL'' OR DECODE(isnumber(tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || '),1,to_char(tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ',''00000000000000000000.00000000000000000000''),UPPER(tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ')) = DECODE(isnumber(UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ')),1,to_char(UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '),''00000000000000000000.00000000000000000000''),UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') ))) OR ' ||
                  '( UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU) <> ''*NA'' AND ' ||
                     '( tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND (DECODE(isnumber(tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || '),1,to_char(tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ',''00000000000000000000.00000000000000000000''),UPPER(tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ')) >= DECODE(isnumber(UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ')),1,to_char(UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '),''00000000000000000000.00000000000000000000''),UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') ) AND ' ||
                                                                                                       'DECODE(isnumber(tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || '),1,to_char(tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ',''00000000000000000000.00000000000000000000''),UPPER(tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ')) <= DECODE(isnumber(UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU)),1,to_char(UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU),''00000000000000000000.00000000000000000000''),UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU) ))))) ';
               END IF;
            ELSE
               -- Range and ToNumber and NOT NULL
               IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                  UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                  vc_tax_matrix_where := vc_tax_matrix_where || 'AND ( ' ||
                  '( UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU) = ''*NA'' AND (UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') = ''*ALL'' OR DECODE(isnumber(tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || '),1,to_char(tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ',''00000000000000000000.00000000000000000000''),UPPER(tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ')) = DECODE(isnumber(UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ')),1,to_char(UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '),''00000000000000000000.00000000000000000000''),UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ')))) OR ' ||
                  '( UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU) <> ''*NA'' AND (DECODE(isnumber(tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || '),1,to_char(tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ',''00000000000000000000.00000000000000000000''),UPPER(tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ')) >= DECODE(isnumber(UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ')),1,to_char(UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '),''00000000000000000000.00000000000000000000''),UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ')) AND ' ||
                                                                                        'DECODE(isnumber(tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || '),1,to_char(tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ',''00000000000000000000.00000000000000000000''),UPPER(tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ')) <= DECODE(isnumber(UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU )),1,to_char(UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU),''00000000000000000000.00000000000000000000''),UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU) ))) ';
               END IF;
            END IF;
         ELSE
            IF tax_driver.null_driver_flag = '1' THEN
               IF tax_driver.wildcard_flag = '1' THEN
                  -- Range and NOT ToNumber and NULL and Wildcard
                  IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                     UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                     vc_tax_matrix_where := vc_tax_matrix_where || 'AND ( ' ||
                     '( UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU) = ''*NA'' AND ' ||
                        '( tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ' IS NULL AND (UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') = ''*ALL'' OR UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') = ''*NULL'' )) OR ' ||
                        '( tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND (UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') = ''*ALL'' OR UPPER(tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ') LIKE UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ')))) OR ' ||
                     '( UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU) <> ''*NA'' AND (tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND (UPPER(tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ') >= UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') AND UPPER(tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ') <= UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU) ))) ';
                  END IF;
               ELSE
                  -- Range and NOT ToNumber and NULL and NOT Wildcard
                  IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                     UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                     vc_tax_matrix_where := vc_tax_matrix_where || 'AND ( ' ||
                     '( UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU) = ''*NA'' AND ' ||
                        '( tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ' IS NULL AND (UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') = ''*ALL'' OR UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') = ''*NULL'' )) OR ' ||
                        '( tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND (UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') = ''*ALL'' OR UPPER(tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ') = UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ')))) OR ' ||
                     '( UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU) <> ''*NA'' AND (tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND (UPPER(tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ') >= UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') AND UPPER(tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ') <= UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU) ))) ';
                  END IF;
               END IF;
            ELSE
               IF tax_driver.wildcard_flag = '1' THEN
                  -- Range and NOT ToNumber and NOT NULL and Wildcard
                  IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                     UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                     vc_tax_matrix_where := vc_tax_matrix_where || 'AND ( ' ||
                     '( UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU) = ''*NA'' AND ' ||
                        '(UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') = ''*ALL'' OR UPPER(tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ') LIKE UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '))) OR ' ||
                     '( UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU) <> ''*NA'' AND (UPPER(tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ') >= UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') AND UPPER(tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ') <= UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU) )) ';
                  END IF;
               ELSE
                  -- Range and NOT ToNumber and NOT NULL and NOT Wildcard
                  IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                     UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                     vc_tax_matrix_where := vc_tax_matrix_where || 'AND ( ' ||
                     '( UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU) = ''*NA'' AND ' ||
                        '(UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') = ''*ALL'' OR UPPER(tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ') = UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '))) OR ' ||
                     '( UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU) <> ''*NA'' AND (UPPER(tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ') >= UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') AND UPPER(tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ') <= UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU) )) ';
                  END IF;
               END IF;
            END IF;
         END IF;
      ELSE
         IF tax_driver.null_driver_flag = '1' THEN
            IF tax_driver.wildcard_flag = '1' THEN
               -- NOT Range and NULL and Wildcard
               IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                  UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                     vc_tax_matrix_where := vc_tax_matrix_where || 'AND ( ' ||
                     '(( tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ' IS NULL AND (UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') = ''*ALL'' OR UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') = ''*NULL'' )) OR ' ||
                     '( tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND (UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') = ''*ALL'' OR UPPER(tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ') LIKE UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ')))) ';
               END IF;
            ELSE
               -- NOT Range and NULL and NOT Wildcard
               IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                  UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                     vc_tax_matrix_where := vc_tax_matrix_where || 'AND ( ' ||
                     '(( tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ' IS NULL AND (UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') = ''*ALL'' OR UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') = ''*NULL'' )) OR ' ||
                     '( tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND (UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') = ''*ALL'' OR UPPER(tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ') = UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ')))) ';
               END IF;
            END IF;
         ELSE
            IF tax_driver.wildcard_flag = '1' THEN
               -- NOT Range and NOT NULL and Wildcard
               IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                  UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                     vc_tax_matrix_where := vc_tax_matrix_where || 'AND ( ' ||
                     '( UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') = ''*ALL'' OR UPPER(tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ') LIKE UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ')) ';
               END IF;
            ELSE
               -- NOT Range and NOT NULL and NOT Wildcard
               IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                  UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                  IF tax_driver.trans_dtl_column_name = 'TRANSACTION_STATE_CODE' THEN
                     IF vc_state_driver_flag <> '1' THEN
                        vc_state_driver_flag := '1';
                     END IF;
                     vc_tax_matrix_where := vc_tax_matrix_where || 'AND ( ' ||
                     '( UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') = ''*ALL'' OR UPPER(:v_transaction_state_code) = UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ')) ';
                  ELSE
                     vc_tax_matrix_where := vc_tax_matrix_where || 'AND ( ' ||
                     '( UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') = ''*ALL'' OR UPPER(tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ') = UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ')) ';
                  END IF;
               END IF;
            END IF;
         END IF;
      END IF;
      vc_tax_matrix_where := vc_tax_matrix_where || ') ';
      -- Insert Driver Reference Values for this driver
      -- Create Insert SQL
      vc_driver_ref_ins_stmt :=
         'INSERT INTO tb_driver_reference (trans_dtl_column_name, driver_value, driver_description) ' ||
            'SELECT ''' || Upper(tax_driver.trans_dtl_column_name) ||
                    ''', ' || tax_driver.trans_dtl_column_name || ', ';
      IF tax_driver.desc_column_name IS NOT NULL THEN
         vc_driver_ref_ins_stmt := vc_driver_ref_ins_stmt || 'min(' || tax_driver.desc_column_name || ') ';
      ELSE
         vc_driver_ref_ins_stmt := vc_driver_ref_ins_stmt || 'NULL ';
      END IF;
      vc_driver_ref_ins_stmt := vc_driver_ref_ins_stmt ||
              'FROM tb_bcp_transactions ' ||
             'WHERE process_batch_no = ' || TO_CHAR(an_batch_id) || ' AND ' ||
                    tax_driver.trans_dtl_column_name || ' IS NOT NULL AND ' ||
                    tax_driver.trans_dtl_column_name || ' NOT IN ' ||
               '(SELECT driver_value ' ||
                  'FROM tb_driver_reference ' ||
                 'WHERE trans_dtl_column_name = ''' || Upper(tax_driver.trans_dtl_column_name) || ''') ' ||
              'GROUP BY ''' || Upper(tax_driver.trans_dtl_column_name) ||
                        ''', ' || tax_driver.trans_dtl_column_name;
      -- Execute Insert SQL
      vi_cursor_id := DBMS_SQL.open_cursor;
      DBMS_SQL.parse (vi_cursor_id, vc_driver_ref_ins_stmt, DBMS_SQL.v7);
      vi_rows_processed := DBMS_SQL.execute (vi_cursor_id);
      DBMS_SQL.close_cursor (vi_cursor_id);
   END LOOP;
   CLOSE tax_driver_cursor;*/

   -- New call to sp_gen_tax_driver for MidTier Cleanup
   sp_gen_tax_driver (
	p_generate_driver_reference	=> 'Y',
	p_an_batch_id			=> an_batch_id,
	p_transaction_table_name	=> 'tb_bcp_transactions',
	p_tax_table_name		=> 'tb_tax_matrix',
	p_vc_state_driver_flag		=> vc_state_driver_flag,
	p_vc_tax_matrix_where		=> vc_tax_matrix_where);

   -- If no drivers found raise error, else create location matrix sql statement
   IF vc_tax_matrix_where IS NULL THEN
      vi_error_count := vi_error_count + 1;
      sp_geterrorcode('P3', an_batch_id, 'P', v_sysdate, v_severity_level, v_write_import_line_flag, v_abort_import_flag);
      IF v_severity_level > v_error_sev_code THEN
         v_error_sev_code := v_severity_level;
      END IF;
   ELSE
      vc_tax_matrix_stmt := vc_tax_matrix_select || vc_tax_matrix_where || vc_tax_matrix_orderby;
   END IF;
   -- Abort Procedure?
   IF v_location_abort_import_flag = '1' OR v_tax_abort_import_flag = '1' THEN
      v_batch_status_code := 'ABP';
      RAISE e_abort;
   END IF;

   -- Set large rollback segment
   COMMIT;

   -- ****** Read and Process Transactions ****** -----------------------------------------
   OPEN bcp_transactions_cursor;
   LOOP
      FETCH bcp_transactions_cursor INTO bcp_transactions;
      EXIT WHEN bcp_transactions_cursor%NOTFOUND;
      vn_actual_rowno := vn_actual_rowno + 1;
      v_write_import_line_flag := '1';

      -- Skip zero amounts?
      IF (v_delete_zero_amounts = '1' AND bcp_transactions.gl_line_itm_dist_amt <> 0) OR
         (v_delete_zero_amounts <> '1') THEN

         v_rowid := bcp_transactions.rowid;
         v_processed_rows := v_processed_rows + 1;
         v_transaction_amt := v_transaction_amt + bcp_transactions.gl_line_itm_dist_amt;

         -- Perform some error checking and manually set the default value
         IF bcp_transactions.gl_company_nbr IS NULL THEN
            bcp_transactions.gl_company_nbr := ' ';
         END IF;
         IF bcp_transactions.gl_date IS NULL THEN
            bcp_transactions.gl_date := v_sysdate;
         END IF;

         -- Populate and/or Clear working fields
         bcp_transactions.process_batch_no := an_batch_id;
         bcp_transactions.transaction_ind := NULL;
         bcp_transactions.suspend_ind := NULL;
         bcp_transactions.taxcode_detail_id := NULL;
         bcp_transactions.taxcode_state_code := NULL;
         bcp_transactions.taxcode_type_code := NULL;
         bcp_transactions.taxcode_code := NULL;
         bcp_transactions.tax_matrix_id := NULL;
         bcp_transactions.location_matrix_id := NULL;
-- Do not clear! Allocations may have put this in. ---> bcp_transactions.jurisdiction_id := NULL;
         bcp_transactions.jurisdiction_taxrate_id := NULL;
         bcp_transactions.measure_type_code := NULL;
         bcp_transactions.gl_extract_updater := NULL;
         bcp_transactions.gl_extract_timestamp := NULL;
         bcp_transactions.gl_extract_flag := NULL;
         bcp_transactions.gl_log_flag := NULL;
         bcp_transactions.gl_extract_amt := NULL;
         bcp_transactions.load_timestamp := v_sysdate;
         bcp_transactions.update_user_id := USER;
         bcp_transactions.update_timestamp := SYSDATE;

         -- Does Transaction Record have a valid State?
         IF bcp_transactions.transaction_state_code IS NULL THEN
            vn_fetch_rows := 0;
         ELSE
            SELECT count(*)
              INTO vn_fetch_rows
              FROM tb_taxcode_state
             WHERE taxcode_state_code = bcp_transactions.transaction_state_code;
         END IF;
         IF vn_fetch_rows = 0 THEN
            -- Search Location Matrix for matches
            BEGIN
                OPEN location_matrix_cursor
                 FOR vc_location_matrix_stmt
               USING v_rowid;
               FETCH location_matrix_cursor
                INTO location_matrix;
               -- Rows found?
               IF location_matrix_cursor%FOUND THEN
                  vn_fetch_rows := location_matrix_cursor%ROWCOUNT;
               ELSE
                  vn_fetch_rows := 0;
               END IF;
               CLOSE location_matrix_cursor;
               IF SQLCODE != 0 THEN
                  RAISE e_badread;
               END IF;
            EXCEPTION
               WHEN e_badread THEN
                  vn_fetch_rows := 0;
                  vi_error_count := vi_error_count + 1;
                  sp_geterrorcode('P4', an_batch_id, 'P', v_sysdate, v_severity_level, v_write_import_line_flag, v_abort_import_flag);
                  IF v_severity_level > v_error_sev_code THEN
                     v_error_sev_code := v_severity_level;
                  END IF;
            END;
            -- Exit if aborted
            IF v_abort_import_flag = '1' THEN
               v_batch_status_code := 'ABP';
               RAISE e_abort;
            END IF;
            -- Location Matrix Line Found
            IF vn_fetch_rows > 0 THEN
               IF bcp_transactions.transaction_state_code IS NULL THEN
                  bcp_transactions.auto_transaction_state_code := '*NULL';
               ELSE
                  bcp_transactions.auto_transaction_state_code := bcp_transactions.transaction_state_code;
               END IF;
               bcp_transactions.transaction_state_code := location_matrix.state;
               bcp_transactions.location_matrix_id := location_matrix.location_matrix_id;
               bcp_transactions.jurisdiction_id := location_matrix.jurisdiction_id;
            ELSE
               -- added--> the following stuff:
               IF bcp_transactions.transaction_state_code IS NULL THEN
                  bcp_transactions.auto_transaction_state_code := '*NULL';
               ELSE
                  bcp_transactions.auto_transaction_state_code := bcp_transactions.transaction_state_code;
               END IF;
               bcp_transactions.transaction_state_code := '*DEF';
               -- removed--> bcp_transactions.transaction_ind := 'S';
               -- removed--> bcp_transactions.suspend_ind := 'L';
            END IF;
         END IF;

         -- Continue if not Suspended
         IF bcp_transactions.transaction_ind IS NULL OR bcp_transactions.transaction_ind <> 'S' THEN
            v_transaction_state_code := bcp_transactions.transaction_state_code;
            -- Search Tax Matrix for matches
            BEGIN
               IF vc_state_driver_flag = '1' THEN
                   OPEN tax_matrix_cursor
                    FOR vc_tax_matrix_stmt
                  USING v_rowid, v_transaction_state_code, v_transaction_state_code;
                  FETCH tax_matrix_cursor
                   INTO tax_matrix;
               ELSE
                   OPEN tax_matrix_cursor
                    FOR vc_tax_matrix_stmt
                  USING v_rowid, v_transaction_state_code;
                  FETCH tax_matrix_cursor
                   INTO tax_matrix;
               END IF;
               -- Rows found?
               IF tax_matrix_cursor%FOUND THEN
                  vn_fetch_rows := tax_matrix_cursor%ROWCOUNT;
               ELSE
                  vn_fetch_rows := 0;
               END IF;
               CLOSE tax_matrix_cursor;
               IF SQLCODE != 0 THEN
                  RAISE e_badread;
               END IF;
            EXCEPTION
               WHEN e_badread THEN
                  vn_fetch_rows := 0;
                  vi_error_count := vi_error_count + 1;
                  sp_geterrorcode('P5', an_batch_id, 'P', v_sysdate, v_severity_level, v_write_import_line_flag, v_abort_import_flag);
                  IF v_severity_level > v_error_sev_code THEN
                     v_error_sev_code := v_severity_level;
                   END IF;
            END;
            -- Exit if aborted
            IF v_abort_import_flag = '1' THEN
               v_batch_status_code := 'ABP';
               RAISE e_abort;
            END IF;

            -- Tax Matrix Line Found
            IF vn_fetch_rows > 0 THEN
               bcp_transactions.tax_matrix_id := tax_matrix.tax_matrix_id;
               -- Determine "Then" or "Else" Results
               IF tax_matrix.relation_sign = 'na' THEN
                  tax_matrix.relation_amount := 0;
                  v_less_hold_code_flag := tax_matrix.then_hold_code_flag;
                  v_less_taxcode_detail_id := tax_matrix.then_taxcode_detail_id;
                  v_less_taxcode_state_code := tax_matrix.then_taxcode_state_code;
                  v_less_taxcode_type_code := tax_matrix.then_taxcode_type_code;
                  v_less_taxcode_code := tax_matrix.then_taxcode_code;
                  v_less_jurisdiction_id := tax_matrix.then_jurisdiction_id;
                  v_less_measure_type_code := tax_matrix.then_measure_type_code;
                  v_less_taxtype_code := tax_matrix.then_taxtype_code;
                  v_less_cch_taxcat_code := tax_matrix.then_cch_taxcat_code;
                  v_less_cch_group_code := tax_matrix.then_cch_group_code;
                  -- v_less_cch_item_code := tax_matrix.then_cch_item_code;
                  v_equal_hold_code_flag := tax_matrix.then_hold_code_flag;
                  v_equal_taxcode_detail_id := tax_matrix.then_taxcode_detail_id;
                  v_equal_taxcode_state_code := tax_matrix.then_taxcode_state_code;
                  v_equal_taxcode_type_code := tax_matrix.then_taxcode_type_code;
                  v_equal_taxcode_code := tax_matrix.then_taxcode_code;
                  v_equal_jurisdiction_id := tax_matrix.then_jurisdiction_id;
                  v_equal_measure_type_code := tax_matrix.then_measure_type_code;
                  v_equal_taxtype_code := tax_matrix.then_taxtype_code;
                  v_equal_cch_taxcat_code := tax_matrix.then_cch_taxcat_code;
                  v_equal_cch_group_code := tax_matrix.then_cch_group_code;
                  -- v_equal_cch_item_code := tax_matrix.then_cch_item_code;
                  v_greater_hold_code_flag := tax_matrix.then_hold_code_flag;
                  v_greater_taxcode_detail_id := tax_matrix.then_taxcode_detail_id;
                  v_greater_taxcode_state_code := tax_matrix.then_taxcode_state_code;
                  v_greater_taxcode_type_code := tax_matrix.then_taxcode_type_code;
                  v_greater_taxcode_code := tax_matrix.then_taxcode_code;
                  v_greater_jurisdiction_id := tax_matrix.then_jurisdiction_id;
                  v_greater_measure_type_code := tax_matrix.then_measure_type_code;
                  v_greater_taxtype_code := tax_matrix.then_taxtype_code;
                  v_greater_cch_taxcat_code := tax_matrix.then_cch_taxcat_code;
                  v_greater_cch_group_code := tax_matrix.then_cch_group_code;
                  -- v_greater_cch_item_code := tax_matrix.then_cch_item_code;
              ELSIF tax_matrix.relation_sign = '=' THEN
                  v_less_hold_code_flag := tax_matrix.else_hold_code_flag;
                  v_less_taxcode_detail_id := tax_matrix.else_taxcode_detail_id;
                  v_less_taxcode_state_code := tax_matrix.else_taxcode_state_code;
                  v_less_taxcode_type_code := tax_matrix.else_taxcode_type_code;
                  v_less_taxcode_code := tax_matrix.else_taxcode_code;
                  v_less_jurisdiction_id := tax_matrix.else_jurisdiction_id;
                  v_less_measure_type_code := tax_matrix.else_measure_type_code;
                  v_less_taxtype_code := tax_matrix.else_taxtype_code;
                  v_less_cch_taxcat_code := tax_matrix.else_cch_taxcat_code;
                  v_less_cch_group_code := tax_matrix.else_cch_group_code;
                  -- v_less_cch_item_code := tax_matrix.else_cch_item_code;
                  v_equal_hold_code_flag := tax_matrix.then_hold_code_flag;
                  v_equal_taxcode_detail_id := tax_matrix.then_taxcode_detail_id;
                  v_equal_taxcode_state_code := tax_matrix.then_taxcode_state_code;
                  v_equal_taxcode_type_code := tax_matrix.then_taxcode_type_code;
                  v_equal_taxcode_code := tax_matrix.then_taxcode_code;
                  v_equal_jurisdiction_id := tax_matrix.then_jurisdiction_id;
                  v_equal_measure_type_code := tax_matrix.then_measure_type_code;
                  v_equal_taxtype_code := tax_matrix.then_taxtype_code;
                  v_equal_cch_taxcat_code := tax_matrix.then_cch_taxcat_code;
                  v_equal_cch_group_code := tax_matrix.then_cch_group_code;
                  -- v_equal_cch_item_code := tax_matrix.then_cch_item_code;
                  v_greater_hold_code_flag := tax_matrix.else_hold_code_flag;
                  v_greater_taxcode_detail_id := tax_matrix.else_taxcode_detail_id;
                  v_greater_taxcode_state_code := tax_matrix.else_taxcode_state_code;
                  v_greater_taxcode_type_code := tax_matrix.else_taxcode_type_code;
                  v_greater_taxcode_code := tax_matrix.else_taxcode_code;
                  v_greater_jurisdiction_id := tax_matrix.else_jurisdiction_id;
                  v_greater_measure_type_code := tax_matrix.else_measure_type_code;
                  v_greater_taxtype_code := tax_matrix.else_taxtype_code;
                  v_greater_cch_taxcat_code := tax_matrix.else_cch_taxcat_code;
                  v_greater_cch_group_code := tax_matrix.else_cch_group_code;
                  -- v_greater_cch_item_code := tax_matrix.else_cch_item_code;
               ELSIF tax_matrix.relation_sign = '<' THEN
                  v_less_hold_code_flag := tax_matrix.then_hold_code_flag;
                  v_less_taxcode_detail_id := tax_matrix.then_taxcode_detail_id;
                  v_less_taxcode_state_code := tax_matrix.then_taxcode_state_code;
                  v_less_taxcode_type_code := tax_matrix.then_taxcode_type_code;
                  v_less_taxcode_code := tax_matrix.then_taxcode_code;
                  v_less_jurisdiction_id := tax_matrix.then_jurisdiction_id;
                  v_less_measure_type_code := tax_matrix.then_measure_type_code;
                  v_less_taxtype_code := tax_matrix.then_taxtype_code;
                  v_less_cch_taxcat_code := tax_matrix.then_cch_taxcat_code;
                  v_less_cch_group_code := tax_matrix.then_cch_group_code;
                  -- v_less_cch_item_code := tax_matrix.then_cch_item_code;
                  v_equal_hold_code_flag := tax_matrix.else_hold_code_flag;
                  v_equal_taxcode_detail_id := tax_matrix.else_taxcode_detail_id;
                  v_equal_taxcode_state_code := tax_matrix.else_taxcode_state_code;
                  v_equal_taxcode_type_code := tax_matrix.else_taxcode_type_code;
                  v_equal_taxcode_code := tax_matrix.else_taxcode_code;
                  v_equal_jurisdiction_id := tax_matrix.else_jurisdiction_id;
                  v_equal_measure_type_code := tax_matrix.else_measure_type_code;
                  v_equal_taxtype_code := tax_matrix.else_taxtype_code;
                  v_equal_cch_taxcat_code := tax_matrix.else_cch_taxcat_code;
                  v_equal_cch_group_code := tax_matrix.else_cch_group_code;
                  -- v_equal_cch_item_code := tax_matrix.else_cch_item_code;
                  v_greater_hold_code_flag := tax_matrix.else_hold_code_flag;
                  v_greater_taxcode_detail_id := tax_matrix.else_taxcode_detail_id;
                  v_greater_taxcode_state_code := tax_matrix.else_taxcode_state_code;
                  v_greater_taxcode_type_code := tax_matrix.else_taxcode_type_code;
                  v_greater_taxcode_code := tax_matrix.else_taxcode_code;
                  v_greater_jurisdiction_id := tax_matrix.else_jurisdiction_id;
                  v_greater_measure_type_code := tax_matrix.else_measure_type_code;
                  v_greater_taxtype_code := tax_matrix.else_taxtype_code;
                  v_greater_cch_taxcat_code := tax_matrix.else_cch_taxcat_code;
                  v_greater_cch_group_code := tax_matrix.else_cch_group_code;
                  -- v_greater_cch_item_code := tax_matrix.else_cch_item_code;
               ELSIF tax_matrix.relation_sign = '<=' THEN
                  v_less_hold_code_flag := tax_matrix.then_hold_code_flag;
                  v_less_taxcode_detail_id := tax_matrix.then_taxcode_detail_id;
                  v_less_taxcode_state_code := tax_matrix.then_taxcode_state_code;
                  v_less_taxcode_type_code := tax_matrix.then_taxcode_type_code;
                  v_less_taxcode_code := tax_matrix.then_taxcode_code;
                  v_less_jurisdiction_id := tax_matrix.then_jurisdiction_id;
                  v_less_measure_type_code := tax_matrix.then_measure_type_code;
                  v_less_taxtype_code := tax_matrix.then_taxtype_code;
                  v_less_cch_taxcat_code := tax_matrix.then_cch_taxcat_code;
                  v_less_cch_group_code := tax_matrix.then_cch_group_code;
                  -- v_less_cch_item_code := tax_matrix.then_cch_item_code;
                  v_equal_hold_code_flag := tax_matrix.then_hold_code_flag;
                  v_equal_taxcode_detail_id := tax_matrix.then_taxcode_detail_id;
                  v_equal_taxcode_state_code := tax_matrix.then_taxcode_state_code;
                  v_equal_taxcode_type_code := tax_matrix.then_taxcode_type_code;
                  v_equal_taxcode_code := tax_matrix.then_taxcode_code;
                  v_equal_jurisdiction_id := tax_matrix.then_jurisdiction_id;
                  v_equal_measure_type_code := tax_matrix.then_measure_type_code;
                  v_equal_taxtype_code := tax_matrix.then_taxtype_code;
                  v_equal_cch_taxcat_code := tax_matrix.then_cch_taxcat_code;
                  v_equal_cch_group_code := tax_matrix.then_cch_group_code;
                  -- v_equal_cch_item_code := tax_matrix.then_cch_item_code;
                  v_greater_hold_code_flag := tax_matrix.else_hold_code_flag;
                  v_greater_taxcode_detail_id := tax_matrix.else_taxcode_detail_id;
                  v_greater_taxcode_state_code := tax_matrix.else_taxcode_state_code;
                  v_greater_taxcode_type_code := tax_matrix.else_taxcode_type_code;
                  v_greater_taxcode_code := tax_matrix.else_taxcode_code;
                  v_greater_jurisdiction_id := tax_matrix.else_jurisdiction_id;
                  v_greater_measure_type_code := tax_matrix.else_measure_type_code;
                  v_greater_taxtype_code := tax_matrix.else_taxtype_code;
                  v_greater_cch_taxcat_code := tax_matrix.else_cch_taxcat_code;
                  v_greater_cch_group_code := tax_matrix.else_cch_group_code;
                  -- v_greater_cch_item_code := tax_matrix.else_cch_item_code;
               ELSIF tax_matrix.relation_sign = '>' THEN
                  v_less_hold_code_flag := tax_matrix.else_hold_code_flag;
                  v_less_taxcode_detail_id := tax_matrix.else_taxcode_detail_id;
                  v_less_taxcode_state_code := tax_matrix.else_taxcode_state_code;
                  v_less_taxcode_type_code := tax_matrix.else_taxcode_type_code;
                  v_less_taxcode_code := tax_matrix.else_taxcode_code;
                  v_less_jurisdiction_id := tax_matrix.else_jurisdiction_id;
                  v_less_measure_type_code := tax_matrix.else_measure_type_code;
                  v_less_taxtype_code := tax_matrix.else_taxtype_code;
                  v_less_cch_taxcat_code := tax_matrix.else_cch_taxcat_code;
                  v_less_cch_group_code := tax_matrix.else_cch_group_code;
                  -- v_less_cch_item_code := tax_matrix.else_cch_item_code;
                  v_equal_hold_code_flag := tax_matrix.else_hold_code_flag;
                  v_equal_taxcode_detail_id := tax_matrix.else_taxcode_detail_id;
                  v_equal_taxcode_state_code := tax_matrix.else_taxcode_state_code;
                  v_equal_taxcode_type_code := tax_matrix.else_taxcode_type_code;
                  v_equal_taxcode_code := tax_matrix.else_taxcode_code;
                  v_equal_jurisdiction_id := tax_matrix.else_jurisdiction_id;
                  v_equal_measure_type_code := tax_matrix.else_measure_type_code;
                  v_equal_taxtype_code := tax_matrix.else_taxtype_code;
                  v_equal_cch_taxcat_code := tax_matrix.else_cch_taxcat_code;
                  v_equal_cch_group_code := tax_matrix.else_cch_group_code;
                  -- v_equal_cch_item_code := tax_matrix.else_cch_item_code;
                  v_greater_hold_code_flag := tax_matrix.then_hold_code_flag;
                  v_greater_taxcode_detail_id := tax_matrix.then_taxcode_detail_id;
                  v_greater_taxcode_state_code := tax_matrix.then_taxcode_state_code;
                  v_greater_taxcode_type_code := tax_matrix.then_taxcode_type_code;
                  v_greater_taxcode_code := tax_matrix.then_taxcode_code;
                  v_greater_jurisdiction_id := tax_matrix.then_jurisdiction_id;
                  v_greater_measure_type_code := tax_matrix.then_measure_type_code;
                  v_greater_taxtype_code := tax_matrix.then_taxtype_code;
                  v_greater_cch_taxcat_code := tax_matrix.then_cch_taxcat_code;
                  v_greater_cch_group_code := tax_matrix.then_cch_group_code;
                  -- v_greater_cch_item_code := tax_matrix.then_cch_item_code;
               ELSIF tax_matrix.relation_sign = '>=' THEN
                  v_less_hold_code_flag := tax_matrix.else_hold_code_flag;
                  v_less_taxcode_detail_id := tax_matrix.else_taxcode_detail_id;
                  v_less_taxcode_state_code := tax_matrix.else_taxcode_state_code;
                  v_less_taxcode_type_code := tax_matrix.else_taxcode_type_code;
                  v_less_taxcode_code := tax_matrix.else_taxcode_code;
                  v_less_jurisdiction_id := tax_matrix.else_jurisdiction_id;
                  v_less_measure_type_code := tax_matrix.else_measure_type_code;
                  v_less_taxtype_code := tax_matrix.else_taxtype_code;
                  v_less_cch_taxcat_code := tax_matrix.else_cch_taxcat_code;
                  v_less_cch_group_code := tax_matrix.else_cch_group_code;
                  -- v_less_cch_item_code := tax_matrix.else_cch_item_code;
                  v_equal_hold_code_flag := tax_matrix.then_hold_code_flag;
                  v_equal_taxcode_detail_id := tax_matrix.then_taxcode_detail_id;
                  v_equal_taxcode_state_code := tax_matrix.then_taxcode_state_code;
                  v_equal_taxcode_type_code := tax_matrix.then_taxcode_type_code;
                  v_equal_taxcode_code := tax_matrix.then_taxcode_code;
                  v_equal_jurisdiction_id := tax_matrix.then_jurisdiction_id;
                  v_equal_measure_type_code := tax_matrix.then_measure_type_code;
                  v_equal_taxtype_code := tax_matrix.then_taxtype_code;
                  v_equal_cch_taxcat_code := tax_matrix.then_cch_taxcat_code;
                  v_equal_cch_group_code := tax_matrix.then_cch_group_code;
                  -- v_equal_cch_item_code := tax_matrix.then_cch_item_code;
                  v_greater_hold_code_flag := tax_matrix.then_hold_code_flag;
                  v_greater_taxcode_detail_id := tax_matrix.then_taxcode_detail_id;
                  v_greater_taxcode_state_code := tax_matrix.then_taxcode_state_code;
                  v_greater_taxcode_type_code := tax_matrix.then_taxcode_type_code;
                  v_greater_taxcode_code := tax_matrix.then_taxcode_code;
                  v_greater_jurisdiction_id := tax_matrix.then_jurisdiction_id;
                  v_greater_measure_type_code := tax_matrix.then_measure_type_code;
                  v_greater_taxtype_code := tax_matrix.then_taxtype_code;
                  v_greater_cch_taxcat_code := tax_matrix.then_cch_taxcat_code;
                  v_greater_cch_group_code := tax_matrix.then_cch_group_code;
                  -- v_greater_cch_item_code := tax_matrix.then_cch_item_code;
               ELSIF tax_matrix.relation_sign = '<>' THEN
                  v_less_hold_code_flag := tax_matrix.then_hold_code_flag;
                  v_less_taxcode_detail_id := tax_matrix.then_taxcode_detail_id;
                  v_less_taxcode_state_code := tax_matrix.then_taxcode_state_code;
                  v_less_taxcode_type_code := tax_matrix.then_taxcode_type_code;
                  v_less_taxcode_code := tax_matrix.then_taxcode_code;
                  v_less_jurisdiction_id := tax_matrix.then_jurisdiction_id;
                  v_less_measure_type_code := tax_matrix.then_measure_type_code;
                  v_less_taxtype_code := tax_matrix.then_taxtype_code;
                  v_less_cch_taxcat_code := tax_matrix.then_cch_taxcat_code;
                  v_less_cch_group_code := tax_matrix.then_cch_group_code;
                  -- v_less_cch_item_code := tax_matrix.then_cch_item_code;
                  v_equal_hold_code_flag := tax_matrix.else_hold_code_flag;
                  v_equal_taxcode_detail_id := tax_matrix.else_taxcode_detail_id;
                  v_equal_taxcode_state_code := tax_matrix.else_taxcode_state_code;
                  v_equal_taxcode_type_code := tax_matrix.else_taxcode_type_code;
                  v_equal_taxcode_code := tax_matrix.else_taxcode_code;
                  v_equal_jurisdiction_id := tax_matrix.else_jurisdiction_id;
                  v_equal_measure_type_code := tax_matrix.else_measure_type_code;
                  v_equal_taxtype_code := tax_matrix.else_taxtype_code;
                  v_equal_cch_taxcat_code := tax_matrix.else_cch_taxcat_code;
                  v_equal_cch_group_code := tax_matrix.else_cch_group_code;
                  -- v_equal_cch_item_code := tax_matrix.else_cch_item_code;
                  v_greater_hold_code_flag := tax_matrix.then_hold_code_flag;
                  v_greater_taxcode_detail_id := tax_matrix.then_taxcode_detail_id;
                  v_greater_taxcode_state_code := tax_matrix.then_taxcode_state_code;
                  v_greater_taxcode_type_code := tax_matrix.then_taxcode_type_code;
                  v_greater_taxcode_code := tax_matrix.then_taxcode_code;
                  v_greater_jurisdiction_id := tax_matrix.then_jurisdiction_id;
                  v_greater_measure_type_code := tax_matrix.then_measure_type_code;
                  v_greater_taxtype_code := tax_matrix.then_taxtype_code;
                  v_greater_cch_taxcat_code := tax_matrix.then_cch_taxcat_code;
                  v_greater_cch_group_code := tax_matrix.then_cch_group_code;
                  -- v_greater_cch_item_code := tax_matrix.then_cch_item_code;
               END IF;

               SELECT DECODE(SIGN(bcp_transactions.gl_line_itm_dist_amt - tax_matrix.relation_amount), -1, v_less_hold_code_flag, 0, v_equal_hold_code_flag, 1, v_greater_hold_code_flag),
                      DECODE(SIGN(bcp_transactions.gl_line_itm_dist_amt - tax_matrix.relation_amount), -1, v_less_taxcode_detail_id, 0, v_equal_taxcode_detail_id, 1, v_greater_taxcode_detail_id),
                      DECODE(SIGN(bcp_transactions.gl_line_itm_dist_amt - tax_matrix.relation_amount), -1, v_less_taxcode_state_code, 0, v_equal_taxcode_state_code, 1, v_greater_taxcode_state_code),
                      DECODE(SIGN(bcp_transactions.gl_line_itm_dist_amt - tax_matrix.relation_amount), -1, v_less_taxcode_type_code, 0, v_equal_taxcode_type_code, 1, v_greater_taxcode_type_code),
                      DECODE(SIGN(bcp_transactions.gl_line_itm_dist_amt - tax_matrix.relation_amount), -1, v_less_taxcode_code, 0, v_equal_taxcode_code, 1, v_greater_taxcode_code),
                      DECODE(SIGN(bcp_transactions.gl_line_itm_dist_amt - tax_matrix.relation_amount), -1, v_less_jurisdiction_id, 0, v_equal_jurisdiction_id, 1, v_greater_jurisdiction_id),
                      DECODE(SIGN(bcp_transactions.gl_line_itm_dist_amt - tax_matrix.relation_amount), -1, v_less_measure_type_code, 0, v_equal_measure_type_code, 1, v_greater_measure_type_code),
                      DECODE(SIGN(bcp_transactions.gl_line_itm_dist_amt - tax_matrix.relation_amount), -1, v_less_taxtype_code, 0, v_equal_taxtype_code, 1, v_greater_taxtype_code),
                      DECODE(SIGN(bcp_transactions.gl_line_itm_dist_amt - tax_matrix.relation_amount), -1, v_less_cch_taxcat_code, 0, v_equal_cch_taxcat_code, 1, v_greater_cch_taxcat_code),
                      DECODE(SIGN(bcp_transactions.gl_line_itm_dist_amt - tax_matrix.relation_amount), -1, v_less_cch_group_code, 0, v_equal_cch_group_code, 1, v_greater_cch_group_code)
                 INTO v_hold_code_flag, bcp_transactions.taxcode_detail_id, bcp_transactions.taxcode_state_code, bcp_transactions.taxcode_type_code, bcp_transactions.taxcode_code, v_override_jurisdiction_id, bcp_transactions.measure_type_code, v_taxtype_code, bcp_transactions.cch_taxcat_code, bcp_transactions.cch_group_code
                 FROM DUAL;

               -- ****** DETERMINE TYPE of TAXCODE ****** --
               -- The Tax Code is Taxable
               IF bcp_transactions.taxcode_type_code = 'T' THEN
                  -- If Override jurisdiction id is not blank or 0 then use it instead of location matrix
                  IF v_override_jurisdiction_id IS NOT NULL AND v_override_jurisdiction_id <> 0 THEN
                     bcp_transactions.jurisdiction_id := v_override_jurisdiction_id;
                     bcp_transactions.manual_jurisdiction_ind := 'AO';
                  ELSE
                     -- Search for Location Matrix if not already found
                     IF (bcp_transactions.location_matrix_id IS NULL OR bcp_transactions.location_matrix_id = 0) AND
                        (bcp_transactions.jurisdiction_id IS NULL OR bcp_transactions.jurisdiction_id = 0) THEN
                        -- Search Location Matrix for matches
                        BEGIN
                            OPEN location_matrix_cursor
                             FOR vc_location_matrix_stmt
                           USING v_rowid;
                           FETCH location_matrix_cursor
                            INTO location_matrix;
                           -- Rows found?
                           IF location_matrix_cursor%FOUND THEN
                              vn_fetch_rows := location_matrix_cursor%ROWCOUNT;
                           ELSE
                              vn_fetch_rows := 0;
                           END IF;
                           CLOSE location_matrix_cursor;
                           IF SQLCODE != 0 THEN
                              RAISE e_badread;
                           END IF;
                        EXCEPTION
                           WHEN e_badread THEN
                              vn_fetch_rows := 0;
                              vi_error_count := vi_error_count + 1;
                              sp_geterrorcode('P4', an_batch_id, 'P', v_sysdate, v_severity_level, v_write_import_line_flag, v_abort_import_flag);
                              IF v_severity_level > v_error_sev_code THEN
                                 v_error_sev_code := v_severity_level;
                              END IF;
                        END;
                        -- Exit if aborted
                        IF v_abort_import_flag = '1' THEN
                           v_batch_status_code := 'ABP';
                           RAISE e_abort;
                        END IF;

                        -- Location Matrix Line Found
                        IF vn_fetch_rows > 0 THEN
                           bcp_transactions.location_matrix_id := location_matrix.location_matrix_id;
                           bcp_transactions.jurisdiction_id := location_matrix.jurisdiction_id;
                        ELSE
                           bcp_transactions.transaction_ind := 'S';
                           bcp_transactions.suspend_ind := 'L';
                        END IF;
                     END IF;
                  END IF;

                  -- Continue if not Suspended
                  IF bcp_transactions.transaction_ind IS NULL OR bcp_transactions.transaction_ind <> 'S' THEN
                     -- Get Jurisdiction TaxRates and calculate Tax Amounts
                     sp_getusetax(bcp_transactions.jurisdiction_id,
                                  bcp_transactions.gl_date,
                                  bcp_transactions.measure_type_code,
                                  bcp_transactions.gl_line_itm_dist_amt,
                                  v_taxtype_code,
                                  location_matrix.override_taxtype_code,
                                  location_matrix.state_flag,
                                  location_matrix.county_flag,
                                  location_matrix.county_local_flag,
                                  location_matrix.city_flag,
                                  location_matrix.city_local_flag,
                                  bcp_transactions.invoice_freight_amt,
                                  bcp_transactions.invoice_discount_amt,
                                  bcp_transactions.transaction_state_code,
-- future?? --                          bcp_transactions.import_definition_code,
                                  'importdefn',
                                  bcp_transactions.taxcode_code,
                                  bcp_transactions.jurisdiction_taxrate_id,
                                  bcp_transactions.state_use_amount,
                                  bcp_transactions.state_use_tier2_amount,
                                  bcp_transactions.state_use_tier3_amount,
                                  bcp_transactions.county_use_amount,
                                  bcp_transactions.county_local_use_amount,
                                  bcp_transactions.city_use_amount,
                                  bcp_transactions.city_local_use_amount,
                                  bcp_transactions.state_use_rate,
                                  bcp_transactions.state_use_tier2_rate,
                                  bcp_transactions.state_use_tier3_rate,
                                  bcp_transactions.state_split_amount,
                                  bcp_transactions.state_tier2_min_amount,
                                  bcp_transactions.state_tier2_max_amount,
                                  bcp_transactions.state_maxtax_amount,
                                  bcp_transactions.county_use_rate,
                                  bcp_transactions.county_local_use_rate,
                                  bcp_transactions.county_split_amount,
                                  bcp_transactions.county_maxtax_amount,
                                  bcp_transactions.county_single_flag,
                                  bcp_transactions.county_default_flag,
                                  bcp_transactions.city_use_rate,
                                  bcp_transactions.city_local_use_rate,
                                  bcp_transactions.city_split_amount,
                                  bcp_transactions.city_split_use_rate,
                                  bcp_transactions.city_single_flag,
                                  bcp_transactions.city_default_flag,
                                  bcp_transactions.combined_use_rate,
                                  bcp_transactions.tb_calc_tax_amt,
-- future?? --                          bcp_transactions.taxable_amt,
-- Targa --                         bcp_transactions.user_number_10,
                                  vn_taxable_amt,
                                  v_taxtype_used );
                     IF bcp_transactions.jurisdiction_taxrate_id = 0 THEN
                         vn_fetch_rows := 0;
                     END IF;
                     IF bcp_transactions.jurisdiction_taxrate_id IS NULL THEN
                        vn_fetch_rows := 0;
                        vi_error_count := vi_error_count + 1;
                        sp_geterrorcode('P6', an_batch_id, 'P', v_sysdate, v_severity_level, v_write_import_line_flag, v_abort_import_flag);
                        IF v_severity_level > v_error_sev_code THEN
                           v_error_sev_code := v_severity_level;
                        END IF;
                     END IF;
                     -- Exit if aborted
                     IF v_abort_import_flag = '1' THEN
                        v_batch_status_code := 'ABP';
                        RAISE e_abort;
                     END IF;

                     -- Jurisdiction Tax Rate FOUND -- 3351 --
                     IF vn_fetch_rows > 0 THEN
                        IF vn_taxable_amt <> bcp_transactions.gl_line_itm_dist_amt THEN
                           bcp_transactions.gl_extract_amt := bcp_transactions.gl_line_itm_dist_amt;
                           bcp_transactions.gl_line_itm_dist_amt := vn_taxable_amt;
                        END IF;
                     -- Jurisdiciton Tax Rate NOT Found
                     ELSE
                     -- IF vn_fetch_rows = 0 THEN
                        bcp_transactions.transaction_ind := 'S';
                        bcp_transactions.suspend_ind := 'R';
                     END IF;
                  END IF;

               -- The Tax Code is Exempt
               ELSIF bcp_transactions.taxcode_type_code = 'E' THEN
                  bcp_transactions.transaction_ind := 'P';

               -- The Tax Code is a Question
               ELSIF bcp_transactions.taxcode_type_code = 'Q' THEN
                 bcp_transactions.transaction_ind := 'Q';

               -- The Tax Code is Unrecognized - Suspend
               ELSE
                  bcp_transactions.transaction_ind := 'S';
                  bcp_transactions.suspend_ind := '?';
               END IF;

               -- Continue if not Suspended
               IF bcp_transactions.transaction_ind IS NULL OR bcp_transactions.transaction_ind <> 'S' THEN
                  -- Check for matrix "H"old AND and global "H"old ELSE "P"rocessed
                  IF ( v_hold_code_flag = '1' ) AND
                     ( bcp_transactions.taxcode_type_code = 'T' OR bcp_transactions.taxcode_type_code = 'E') THEN
                        bcp_transactions.transaction_ind := 'H';
                  ELSIF ( v_hold_trans_flag = '1' ) AND
                        ( bcp_transactions.taxcode_type_code = 'T' OR bcp_transactions.taxcode_type_code = 'E') AND
                        ( ABS(bcp_transactions.gl_line_itm_dist_amt) >= v_hold_trans_amount ) THEN
                           bcp_transactions.transaction_ind := 'H';
                  ELSE
                     bcp_transactions.transaction_ind := 'P';
                  END IF;
               END IF;

            -- Tax Matrix Line NOT Found
            ELSE
               IF bcp_transactions.transaction_state_code = '*DEF' THEN
                  bcp_transactions.transaction_ind := 'S';
                  bcp_transactions.suspend_ind := 'L';
                  IF bcp_transactions.auto_transaction_state_code = '*NULL' THEN
                     bcp_transactions.transaction_state_code := NULL;
                  ELSE
                     bcp_transactions.transaction_state_code := bcp_transactions.auto_transaction_state_code;
                  END IF;
                  bcp_transactions.auto_transaction_state_code := NULL;
               ELSE
                  bcp_transactions.transaction_ind := 'S';
                  bcp_transactions.suspend_ind := 'T';
               END IF;
            END IF;
         END IF;

         -- Write line?
         IF v_write_import_line_flag = '1' THEN
            -- Check for Auto-Process Material Limit on Suspended Lines
            IF v_autoproc_flag = '1' THEN
               IF bcp_transactions.transaction_ind = 'S' AND bcp_transactions.suspend_ind = 'T' THEN
                  IF ABS(bcp_transactions.gl_line_itm_dist_amt) <= vn_autoproc_amount THEN
                     bcp_transactions.transaction_ind := 'P';
                     bcp_transactions.suspend_ind := '';
                     bcp_transactions.manual_taxcode_ind := 'AP';
                     bcp_transactions.taxcode_detail_id := vn_autoproc_id;
                     bcp_transactions.taxcode_state_code := v_autoproc_state_code;
                     bcp_transactions.taxcode_type_code := v_autoproc_type_code;
                     bcp_transactions.taxcode_code := v_autoproc_code;
                  END IF;
               END IF;
            END IF;

            -- // MBF?? - Add Allocations - top
-- 3411? --            IF bcp_transactions.transaction_detail_id = -1 THEN
-- 3411? --               IF v_allocation_flag = '1' THEN
-- 3411? --                  IF NOT (bcp_transactions.transaction_ind = 'P' AND
-- 3411? --                          bcp_transactions.taxcode_state_code = '*ALL' AND
-- 3411? --                          bcp_transactions.taxcode_type_code = 'E') THEN
-- 3411? --                     sp_batch_allocation(v_rowid,
-- 3411? --                                         an_batch_id,
-- 3411? --                                         vn_trans_created);
-- 3411? --                     IF vn_trans_created > 0 THEN
-- 3411? --                        v_processed_rows := v_processed_rows + vn_trans_created - 1;
-- 3411? --                        v_write_import_line_flag := '0';
-- 3411? --                     END IF;
-- 3411? --                  END IF;
-- 3411? --               END IF;
-- 3411? --            END IF;

-- 3411? --            If v_write_import_line_flag = '1' THEN
            -- // MBF?? - Add Allocations - bottom

            -- Get next transaction ID for the new transaction detail record
            SELECT sq_tb_transaction_detail_id.NEXTVAL
              INTO bcp_transactions.transaction_detail_id
              FROM DUAL;

            -- Insert transaction detail row
            BEGIN
               -- 3411 - add taxable amount and tax type used.
               INSERT INTO tb_transaction_detail (
                  transaction_detail_id,
                  source_transaction_id,
                  process_batch_no,
                  gl_extract_batch_no,
                  archive_batch_no,
                  allocation_matrix_id,
                  allocation_subtrans_id,
                  entered_date,
                  transaction_status,
                  gl_date,
                  gl_company_nbr,
                  gl_company_name,
                  gl_division_nbr,
                  gl_division_name,
                  gl_cc_nbr_dept_id,
                  gl_cc_nbr_dept_name,
                  gl_local_acct_nbr,
                  gl_local_acct_name,
                  gl_local_sub_acct_nbr,
                  gl_local_sub_acct_name,
                  gl_full_acct_nbr,
                  gl_full_acct_name,
                  gl_line_itm_dist_amt,
                  orig_gl_line_itm_dist_amt,
                  vendor_nbr,
                  vendor_name,
                  vendor_address_line_1,
                  vendor_address_line_2,
                  vendor_address_line_3,
                  vendor_address_line_4,
                  vendor_address_city,
                  vendor_address_county,
                  vendor_address_state,
                  vendor_address_zip,
                  vendor_address_country,
                  vendor_type,
                  vendor_type_name,
                  invoice_nbr,
                  invoice_desc,
                  invoice_date,
                  invoice_freight_amt,
                  invoice_discount_amt,
                  invoice_tax_amt,
                  invoice_total_amt,
                  invoice_tax_flg,
                  invoice_line_nbr,
                  invoice_line_name,
                  invoice_line_type,
                  invoice_line_type_name,
                  invoice_line_amt,
                  invoice_line_tax,
                  afe_project_nbr,
                  afe_project_name,
                  afe_category_nbr,
                  afe_category_name,
                  afe_sub_cat_nbr,
                  afe_sub_cat_name,
                  afe_use,
                  afe_contract_type,
                  afe_contract_structure,
                  afe_property_cat,
                  inventory_nbr,
                  inventory_name,
                  inventory_class,
                  inventory_class_name,
                  po_nbr,
                  po_name,
                  po_date,
                  po_line_nbr,
                  po_line_name,
                  po_line_type,
                  po_line_type_name,
                  ship_to_location,
                  ship_to_location_name,
                  ship_to_address_line_1,
                  ship_to_address_line_2,
                  ship_to_address_line_3,
                  ship_to_address_line_4,
                  ship_to_address_city,
                  ship_to_address_county,
                  ship_to_address_state,
                  ship_to_address_zip,
                  ship_to_address_country,
                  wo_nbr,
                  wo_name,
                  wo_date,
                  wo_type,
                  wo_type_desc,
                  wo_class,
                  wo_class_desc,
                  wo_entity,
                  wo_entity_desc,
                  wo_line_nbr,
                  wo_line_name,
                  wo_line_type,
                  wo_line_type_desc,
                  wo_shut_down_cd,
                  wo_shut_down_cd_desc,
                  voucher_id,
                  voucher_name,
                  voucher_date,
                  voucher_line_nbr,
                  voucher_line_desc,
                  check_nbr,
                  check_no,
                  check_date,
                  check_amt,
                  check_desc,
                  user_text_01,
                  user_text_02,
                  user_text_03,
                  user_text_04,
                  user_text_05,
                  user_text_06,
                  user_text_07,
                  user_text_08,
                  user_text_09,
                  user_text_10,
                  user_text_11,
                  user_text_12,
                  user_text_13,
                  user_text_14,
                  user_text_15,
                  user_text_16,
                  user_text_17,
                  user_text_18,
                  user_text_19,
                  user_text_20,
                  user_text_21,
                  user_text_22,
                  user_text_23,
                  user_text_24,
                  user_text_25,
                  user_text_26,
                  user_text_27,
                  user_text_28,
                  user_text_29,
                  user_text_30,
                  user_number_01,
                  user_number_02,
                  user_number_03,
                  user_number_04,
                  user_number_05,
                  user_number_06,
                  user_number_07,
                  user_number_08,
                  user_number_09,
                  user_number_10,
                  user_date_01,
                  user_date_02,
                  user_date_03,
                  user_date_04,
                  user_date_05,
                  user_date_06,
                  user_date_07,
                  user_date_08,
                  user_date_09,
                  user_date_10,
                  comments,
                  tb_calc_tax_amt,
                  state_use_amount,
                  state_use_tier2_amount,
                  state_use_tier3_amount,
                  county_use_amount,
                  county_local_use_amount,
                  city_use_amount,
                  city_local_use_amount,
                  transaction_state_code,
                  auto_transaction_state_code,
                  transaction_ind,
                  suspend_ind,
                  taxcode_detail_id,
                  taxcode_state_code,
                  taxcode_type_code,
                  taxcode_code,
                  cch_taxcat_code,
                  cch_group_code,
                  cch_item_code,
                  manual_taxcode_ind,
                  tax_matrix_id,
                  location_matrix_id,
                  jurisdiction_id,
                  jurisdiction_taxrate_id,
                  manual_jurisdiction_ind,
                  measure_type_code,
                  state_use_rate,
                  state_use_tier2_rate,
                  state_use_tier3_rate,
                  state_split_amount,
                  state_tier2_min_amount,
                  state_tier2_max_amount,
                  state_maxtax_amount,
                  county_use_rate,
                  county_local_use_rate,
                  county_split_amount,
                  county_maxtax_amount,
                  county_single_flag,
                  county_default_flag,
                  city_use_rate,
                  city_local_use_rate,
                  city_split_amount,
                  city_split_use_rate,
                  city_single_flag,
                  city_default_flag,
                  combined_use_rate,
                  load_timestamp,
                  gl_extract_updater,
                  gl_extract_timestamp,
                  gl_extract_flag,
                  gl_log_flag,
                  gl_extract_amt,
                  audit_flag,
                  audit_user_id,
                  audit_timestamp,
                  modify_user_id,
                  modify_timestamp,
                  update_user_id,
                  update_timestamp )
               VALUES (
                  bcp_transactions.transaction_detail_id,
                  bcp_transactions.source_transaction_id,
                  bcp_transactions.process_batch_no,
                  bcp_transactions.gl_extract_batch_no,
                  bcp_transactions.archive_batch_no,
                  bcp_transactions.allocation_matrix_id,
                  bcp_transactions.allocation_subtrans_id,
                  bcp_transactions.entered_date,
                  bcp_transactions.transaction_status,
                  bcp_transactions.gl_date,
                  bcp_transactions.gl_company_nbr,
                  bcp_transactions.gl_company_name,
                  bcp_transactions.gl_division_nbr,
                  bcp_transactions.gl_division_name,
                  bcp_transactions.gl_cc_nbr_dept_id,
                  bcp_transactions.gl_cc_nbr_dept_name,
                  bcp_transactions.gl_local_acct_nbr,
                  bcp_transactions.gl_local_acct_name,
                  bcp_transactions.gl_local_sub_acct_nbr,
                  bcp_transactions.gl_local_sub_acct_name,
                  bcp_transactions.gl_full_acct_nbr,
                  bcp_transactions.gl_full_acct_name,
                  bcp_transactions.gl_line_itm_dist_amt,
                  bcp_transactions.orig_gl_line_itm_dist_amt,
                  bcp_transactions.vendor_nbr,
                  bcp_transactions.vendor_name,
                  bcp_transactions.vendor_address_line_1,
                  bcp_transactions.vendor_address_line_2,
                  bcp_transactions.vendor_address_line_3,
                  bcp_transactions.vendor_address_line_4,
                  bcp_transactions.vendor_address_city,
                  bcp_transactions.vendor_address_county,
                  bcp_transactions.vendor_address_state,
                  bcp_transactions.vendor_address_zip,
                  bcp_transactions.vendor_address_country,
                  bcp_transactions.vendor_type,
                  bcp_transactions.vendor_type_name,
                  bcp_transactions.invoice_nbr,
                  bcp_transactions.invoice_desc,
                  bcp_transactions.invoice_date,
                  bcp_transactions.invoice_freight_amt,
                  bcp_transactions.invoice_discount_amt,
                  bcp_transactions.invoice_tax_amt,
                  bcp_transactions.invoice_total_amt,
                  bcp_transactions.invoice_tax_flg,
                  bcp_transactions.invoice_line_nbr,
                  bcp_transactions.invoice_line_name,
                  bcp_transactions.invoice_line_type,
                  bcp_transactions.invoice_line_type_name,
                  bcp_transactions.invoice_line_amt,
                  bcp_transactions.invoice_line_tax,
                  bcp_transactions.afe_project_nbr,
                  bcp_transactions.afe_project_name,
                  bcp_transactions.afe_category_nbr,
                  bcp_transactions.afe_category_name,
                  bcp_transactions.afe_sub_cat_nbr,
                  bcp_transactions.afe_sub_cat_name,
                  bcp_transactions.afe_use,
                  bcp_transactions.afe_contract_type,
                  bcp_transactions.afe_contract_structure,
                  bcp_transactions.afe_property_cat,
                  bcp_transactions.inventory_nbr,
                  bcp_transactions.inventory_name,
                  bcp_transactions.inventory_class,
                  bcp_transactions.inventory_class_name,
                  bcp_transactions.po_nbr,
                  bcp_transactions.po_name,
                  bcp_transactions.po_date,
                  bcp_transactions.po_line_nbr,
                  bcp_transactions.po_line_name,
                  bcp_transactions.po_line_type,
                  bcp_transactions.po_line_type_name,
                  bcp_transactions.ship_to_location,
                  bcp_transactions.ship_to_location_name,
                  bcp_transactions.ship_to_address_line_1,
                  bcp_transactions.ship_to_address_line_2,
                  bcp_transactions.ship_to_address_line_3,
                  bcp_transactions.ship_to_address_line_4,
                  bcp_transactions.ship_to_address_city,
                  bcp_transactions.ship_to_address_county,
                  bcp_transactions.ship_to_address_state,
                  bcp_transactions.ship_to_address_zip,
                  bcp_transactions.ship_to_address_country,
                  bcp_transactions.wo_nbr,
                  bcp_transactions.wo_name,
                  bcp_transactions.wo_date,
                  bcp_transactions.wo_type,
                  bcp_transactions.wo_type_desc,
                  bcp_transactions.wo_class,
                  bcp_transactions.wo_class_desc,
                  bcp_transactions.wo_entity,
                  bcp_transactions.wo_entity_desc,
                  bcp_transactions.wo_line_nbr,
                  bcp_transactions.wo_line_name,
                  bcp_transactions.wo_line_type,
                  bcp_transactions.wo_line_type_desc,
                  bcp_transactions.wo_shut_down_cd,
                  bcp_transactions.wo_shut_down_cd_desc,
                  bcp_transactions.voucher_id,
                  bcp_transactions.voucher_name,
                  bcp_transactions.voucher_date,
                  bcp_transactions.voucher_line_nbr,
                  bcp_transactions.voucher_line_desc,
                  bcp_transactions.check_nbr,
                  bcp_transactions.check_no,
                  bcp_transactions.check_date,
                  bcp_transactions.check_amt,
                  bcp_transactions.check_desc,
                  bcp_transactions.user_text_01,
                  bcp_transactions.user_text_02,
                  bcp_transactions.user_text_03,
                  bcp_transactions.user_text_04,
                  bcp_transactions.user_text_05,
                  bcp_transactions.user_text_06,
                  bcp_transactions.user_text_07,
                  bcp_transactions.user_text_08,
                  bcp_transactions.user_text_09,
                  bcp_transactions.user_text_10,
                  bcp_transactions.user_text_11,
                  bcp_transactions.user_text_12,
                  bcp_transactions.user_text_13,
                  bcp_transactions.user_text_14,
                  bcp_transactions.user_text_15,
                  bcp_transactions.user_text_16,
                  bcp_transactions.user_text_17,
                  bcp_transactions.user_text_18,
                  bcp_transactions.user_text_19,
                  bcp_transactions.user_text_20,
                  bcp_transactions.user_text_21,
                  bcp_transactions.user_text_22,
                  bcp_transactions.user_text_23,
                  bcp_transactions.user_text_24,
                  bcp_transactions.user_text_25,
                  bcp_transactions.user_text_26,
                  bcp_transactions.user_text_27,
                  bcp_transactions.user_text_28,
                  bcp_transactions.user_text_29,
                  bcp_transactions.user_text_30,
                  bcp_transactions.user_number_01,
                  bcp_transactions.user_number_02,
                  bcp_transactions.user_number_03,
                  bcp_transactions.user_number_04,
                  bcp_transactions.user_number_05,
                  bcp_transactions.user_number_06,
                  bcp_transactions.user_number_07,
                  bcp_transactions.user_number_08,
                  bcp_transactions.user_number_09,
                  bcp_transactions.user_number_10,
                  bcp_transactions.user_date_01,
                  bcp_transactions.user_date_02,
                  bcp_transactions.user_date_03,
                  bcp_transactions.user_date_04,
                  bcp_transactions.user_date_05,
                  bcp_transactions.user_date_06,
                  bcp_transactions.user_date_07,
                  bcp_transactions.user_date_08,
                  bcp_transactions.user_date_09,
                  bcp_transactions.user_date_10,
                  bcp_transactions.comments,
                  bcp_transactions.tb_calc_tax_amt,
                  bcp_transactions.state_use_amount,
                  bcp_transactions.state_use_tier2_amount,
                  bcp_transactions.state_use_tier3_amount,
                  bcp_transactions.county_use_amount,
                  bcp_transactions.county_local_use_amount,
                  bcp_transactions.city_use_amount,
                  bcp_transactions.city_local_use_amount,
                  bcp_transactions.transaction_state_code,
                  bcp_transactions.auto_transaction_state_code,
                  bcp_transactions.transaction_ind,
                  bcp_transactions.suspend_ind,
                  bcp_transactions.taxcode_detail_id,
                  bcp_transactions.taxcode_state_code,
                  bcp_transactions.taxcode_type_code,
                  bcp_transactions.taxcode_code,
                  bcp_transactions.cch_taxcat_code,
                  bcp_transactions.cch_group_code,
                  bcp_transactions.cch_item_code,
                  bcp_transactions.manual_taxcode_ind,
                  bcp_transactions.tax_matrix_id,
                  bcp_transactions.location_matrix_id,
                  bcp_transactions.jurisdiction_id,
                  bcp_transactions.jurisdiction_taxrate_id,
                  bcp_transactions.manual_jurisdiction_ind,
                  bcp_transactions.measure_type_code,
                  bcp_transactions.state_use_rate,
                  bcp_transactions.state_use_tier2_rate,
                  bcp_transactions.state_use_tier3_rate,
                  bcp_transactions.state_split_amount,
                  bcp_transactions.state_tier2_min_amount,
                  bcp_transactions.state_tier2_max_amount,
                  bcp_transactions.state_maxtax_amount,
                  bcp_transactions.county_use_rate,
                  bcp_transactions.county_local_use_rate,
                  bcp_transactions.county_split_amount,
                  bcp_transactions.county_maxtax_amount,
                  bcp_transactions.county_single_flag,
                  bcp_transactions.county_default_flag,
                  bcp_transactions.city_use_rate,
                  bcp_transactions.city_local_use_rate,
                  bcp_transactions.city_split_amount,
                  bcp_transactions.city_split_use_rate,
                  bcp_transactions.city_single_flag,
                  bcp_transactions.city_default_flag,
                  bcp_transactions.combined_use_rate,
                  bcp_transactions.load_timestamp,
                  bcp_transactions.gl_extract_updater,
                  bcp_transactions.gl_extract_timestamp,
                  bcp_transactions.gl_extract_flag,
                  bcp_transactions.gl_log_flag,
                  bcp_transactions.gl_extract_amt,
                  bcp_transactions.audit_flag,
                  bcp_transactions.audit_user_id,
                  bcp_transactions.audit_timestamp,
                  bcp_transactions.modify_user_id,
                  bcp_transactions.modify_timestamp,
                  bcp_transactions.update_user_id,
                  bcp_transactions.update_timestamp );
               -- Error Checking
               IF SQLCODE != 0 THEN
                  RAISE e_badwrite;
               END IF;
            EXCEPTION
               WHEN e_badwrite THEN
                  vi_error_count := vi_error_count + 1;
                  sp_geterrorcode('P7', an_batch_id, 'P', v_sysdate, v_severity_level, v_write_import_line_flag, v_abort_import_flag);
                  IF v_severity_level > v_error_sev_code THEN
                     v_error_sev_code := v_severity_level;
                  END IF;
            END;
            -- Exit if aborted
            IF v_abort_import_flag = '1' THEN
               v_batch_status_code := 'ABP';
               RAISE e_abort;
            END IF;
-- 3411 --            END IF;
         END IF;
      END IF;

      -- Check Kill Process Flag -- MBF01 -- begin
      IF v_kill_proc_flag = '1' THEN
         vn_kill_proc_counter := vn_kill_proc_counter + 1;
         IF vn_kill_proc_counter >= vn_kill_proc_count THEN
            SELECT batch_status_code
              INTO vc_batch_status_code
              FROM tb_batch
             WHERE batch_id = an_batch_id;
            IF vc_batch_status_code = 'KILLP' THEN
               v_batch_status_code := 'ABP';
               RAISE e_kill;
            END IF;
            vn_kill_proc_counter := 0;
         END IF;
      END IF;
      -- Check Kill Process Flag -- MBF01 -- end

   END LOOP;
   CLOSE bcp_transactions_cursor;
   v_batch_status_code := 'P';

   --- Remove Batch from BCP Transactions Table
   DELETE
     FROM tb_bcp_transactions
    WHERE process_batch_no = an_batch_id;

   -- MBF03 -- Run User Exit?
   BEGIN
      SELECT LTRIM(RTRIM(value))
        INTO v_processuserexit_flag
        FROM tb_option
       WHERE option_code = 'PROCESSUSEREXIT' AND
             option_type_code = 'ADMIN' AND
             user_code = 'ADMIN';
   EXCEPTION
      WHEN OTHERS THEN
         v_processuserexit_flag := '0';
   END;
   IF v_processuserexit_flag IS NULL THEN
      v_processuserexit_flag := '0';
   END IF;
   -- MBF03 - Run User Exit if selected
   IF v_processuserexit_flag = '1' THEN
      vn_return := f_process_userexit(an_batch_id);
      IF vn_return < 0 THEN
         vi_error_count := vi_error_count + 1;
         sp_geterrorcode('UE4', an_batch_id, 'UE', v_sysdate, v_severity_level, v_write_import_line_flag, v_abort_import_flag);
         IF v_severity_level > v_error_sev_code THEN
            v_error_sev_code := v_severity_level;
         END IF;
      END IF;
   END IF;
   -- MBF03 - User Exit

   -- Update batch with final totals
   UPDATE tb_batch
      SET batch_status_code = v_batch_status_code,
          error_sev_code = v_error_sev_code,
          processed_rows = v_processed_rows,
          actual_end_timestamp = SYSDATE,
          nu02 = vn_actual_rowno,
          nu03 = v_transaction_amt
    WHERE batch_id = an_batch_id;

    COMMIT;

EXCEPTION
   WHEN e_abort THEN
      Rollback;
      -- Update batch with error codes
      UPDATE tb_batch
         SET batch_status_code = v_batch_status_code,
             error_sev_code = v_error_sev_code
       WHERE batch_id = an_batch_id;
      COMMIT;

   -- Check Kill Process Flag -- MBF01 -- begin
   WHEN e_kill THEN
      Rollback;
      -- Write error to errorlog
      sp_geterrorcode('P66', an_batch_id, 'P', v_sysdate, v_severity_level, v_write_import_line_flag, v_abort_import_flag);
      IF v_severity_level > v_error_sev_code THEN
         v_error_sev_code := v_severity_level;
      END IF;
      -- Update batch with error codes
      UPDATE tb_batch
         SET batch_status_code = v_batch_status_code,
             error_sev_code = v_error_sev_code
       WHERE batch_id = an_batch_id;
      COMMIT;
   -- Check Kill Process Flag -- MBF01 -- end
END;
/

SHOW ERRORS;

CREATE OR REPLACE PROCEDURE sp_batch_taxrate_update(an_batch_id number)
IS
-- Table defined variables
   v_batch_status_code             tb_batch.batch_status_code%TYPE                   := NULL;
   v_error_sev_code                tb_batch.error_sev_code%TYPE                      := ' ';
   v_starting_sequence             tb_batch.start_row%TYPE                           := 0;
   v_process_count                 tb_batch.start_row%TYPE                           := 0;
   v_processed_rows                tb_batch.start_row%TYPE                           := 0;
   v_severity_level                tb_list_code.severity_level%TYPE                  := ' ';
   v_write_import_line_flag        tb_list_code.write_import_line_flag%TYPE          := '1';
   v_abort_import_flag             tb_list_code.abort_import_flag%TYPE               := '0';
   v_jurisdiction_id               tb_jurisdiction.jurisdiction_id%TYPE              := NULL;
   v_jur_custom_flag               tb_jurisdiction.custom_flag%TYPE                  := NULL;
   v_jurisdiction_taxrate_id       tb_jurisdiction_taxrate.jurisdiction_taxrate_id%TYPE := NULL;
   v_rate_custom_flag              tb_jurisdiction_taxrate.custom_flag%TYPE          := NULL;
   v_rate_modified_flag            tb_jurisdiction_taxrate.modified_flag%TYPE        := NULL;
   v_sysdate                       tb_bcp_transactions.load_timestamp%TYPE           := SYSDATE;

-- Program defined variables
   vc_taxrate_update_type          VARCHAR(10)                                       := NULL;
   vi_release_number               INTEGER                                           := 0;
   vi_sessions                     INTEGER                                           := 0;
   vd_update_date                  DATE                                              := NULL;
   vi_cursor_id                    INTEGER                                           := 0;
   vi_error_count                  INTEGER                                           := 0;

-- Define BCP Jurisdiction TaxRate Updates Cursor (tb_bcp_jurisdiction_taxrate)
   CURSOR bcp_juris_taxrate_cursor
   IS
      SELECT
         batch_id,
         geocode,
         zip,
         state,
         county,
         city,
         zipplus4,
         in_out,
         state_sales_rate,
         state_use_rate,
         county_sales_rate,
         county_use_rate,
         county_local_sales_rate,
         county_local_use_rate,
         county_split_amount,
         county_maxtax_amount,
         county_single_flag,
         county_default_flag,
         city_sales_rate,
         city_use_rate,
         city_local_sales_rate,
         city_local_use_rate,
         city_split_amount,
         city_split_sales_rate,
         city_split_use_rate,
         city_single_flag,
         city_default_flag,
         effective_date
    FROM tb_bcp_jurisdiction_taxrate
   WHERE batch_id = an_batch_id;
   bcp                             bcp_juris_taxrate_cursor%ROWTYPE;

-- Define Exceptions
   e_badread                       exception;
   e_badupdate                     exception;
   e_badwrite                      exception;
   e_wrongdata                     exception;
   e_abort                         exception;
   e_halt                          exception;

-- ****** Program starts ****** --------------------------------------------------------------------
BEGIN
   -- Confirm batch exists and is flagged for processing
   BEGIN
      SELECT batch_status_code
        INTO v_batch_status_code
        FROM tb_batch
       WHERE batch_id = an_batch_id;
      IF SQLCODE != 0 THEN
         RAISE e_badread;
      ELSIF v_batch_status_code != 'FTR' THEN
         RAISE e_wrongdata;
      END IF;
   EXCEPTION
      WHEN NO_DATA_FOUND OR e_badread THEN
         vi_error_count := vi_error_count + 1;
         sp_geterrorcode('TR8', an_batch_id, 'TR', v_sysdate, v_severity_level, v_write_import_line_flag, v_abort_import_flag);
         IF v_severity_level > v_error_sev_code THEN
            v_error_sev_code := v_severity_level;
         END IF;
      WHEN e_wrongdata THEN
         vi_error_count := vi_error_count + 1;
         sp_geterrorcode('TR9', an_batch_id, 'TR', v_sysdate, v_severity_level, v_write_import_line_flag, v_abort_import_flag);
         IF v_severity_level > v_error_sev_code THEN
            v_error_sev_code := v_severity_level;
         END IF;
   END;
   -- Abort Procedure?
   IF v_abort_import_flag = '1' THEN
      v_batch_status_code := 'ABTR';
      RAISE e_abort;
   END IF;

   -- Check for Users in STS??
   -- Take this out for now -- 3.3.4.1
   /* SELECT count(*)
     INTO vi_sessions
     FROM v$session
    WHERE audsid <> 0
      AND audsid NOT IN
         (SELECT sys_context('USERENV','SESSIONID') FROM DUAL);
   IF vi_sessions > 0 Then
      RAISE e_halt;
   END IF; */

   -- Disable triggers
   execute immediate 'ALTER TRIGGER tb_jurisdiction_taxrate_a_i DISABLE';

   -- Get starting sequence no.
   BEGIN
      SELECT last_number
        INTO v_starting_sequence
        FROM user_sequences
       WHERE sequence_name = 'SQ_TB_PROCESS_COUNT';
   EXCEPTION
      WHEN OTHERS THEN
         NULL;
   END;

   -- Get batch information
   SELECT UPPER(VC02),
          NU01,
          TS01
     INTO vc_taxrate_update_type,
          vi_release_number,
          vd_update_date
     FROM tb_batch
    WHERE batch_id = an_batch_id;

   -- Update batch as processing
   BEGIN
      UPDATE tb_batch
         SET batch_status_code = 'XTR',
             error_sev_code = '',
             start_row = v_starting_sequence,
             actual_start_timestamp = v_sysdate
       WHERE batch_id = an_batch_id;
      IF SQLCODE != 0 THEN
         RAISE e_badupdate;
      END IF;
   EXCEPTION
      WHEN e_badupdate THEN
         vi_error_count := vi_error_count + 1;
         sp_geterrorcode('TR1', an_batch_id, 'TR', v_sysdate, v_severity_level, v_write_import_line_flag, v_abort_import_flag);
         IF v_severity_level > v_error_sev_code THEN
            v_error_sev_code := v_severity_level;
         END IF;
   END;
   -- Abort Procedure?
   IF v_abort_import_flag = '1' THEN
      v_batch_status_code := 'ABTR';
      RAISE e_abort;
   END IF;

   COMMIT;

   -- ****** Read and Process Transactions ****** -----------------------------------------
   OPEN bcp_juris_taxrate_cursor;
   LOOP
      FETCH bcp_juris_taxrate_cursor INTO bcp;
      EXIT WHEN bcp_juris_taxrate_cursor%NOTFOUND;

      -- Increment Process Count
      SELECT sq_tb_process_count.NEXTVAL
        INTO v_process_count
        FROM DUAL;
      v_processed_rows := v_processed_rows + 1;

      -- Find jurisdiction
      BEGIN
         SELECT jur.jurisdiction_id,
                jur.custom_flag
           INTO v_jurisdiction_id,
                v_jur_custom_flag
           FROM tb_jurisdiction jur
          WHERE ( jur.geocode = bcp.geocode ) AND
                ( jur.state = bcp.state ) AND
                ( jur.county = bcp.county ) AND
                ( jur.city = bcp.city ) AND
                ( jur.zip = bcp.zip );
      EXCEPTION
         WHEN OTHERS THEN
            v_jurisdiction_id := 0;
            v_jur_custom_flag := '0';
      END;

      -- If it does not exist, add it
      IF v_jurisdiction_id IS NULL or v_jurisdiction_id = 0 THEN
         -- Insert new jurisdiction
         INSERT INTO tb_jurisdiction(
            jurisdiction_id,
            geocode,
            state,
            county,
            city,
            zip,
            zipplus4,
            in_out,
            custom_flag,
            description )
         VALUES(
            0,
            bcp.geocode,
            bcp.state,
            bcp.county,
            bcp.city,
            bcp.zip,
            bcp.zipplus4,
            bcp.in_out,
            '0',
            'CCH Supplied Jurisdiction/Tax Rate' );
         -- Get last inserted jurisdiction id
         SELECT MAX(jurisdiction_id)
           INTO v_jurisdiction_id
           FROM tb_jurisdiction;
      ELSE
         -- If it does exist and is custom then write errog log warning
         IF v_jur_custom_flag = '1' THEN
            v_error_sev_code := '10';
            INSERT INTO tb_batch_error_log (
               batch_error_log_id,
               batch_id,
               process_type,
               process_timestamp,
               row_no,
               column_no,
               error_def_code,
               import_header_column,
               import_column_value,
               trans_dtl_column_name,
               trans_dtl_datatype,
               import_row )
            VALUES (
               0,
               bcp.batch_id,
               'TR',
               v_sysdate,
               rownum,
               0,
               'TR1',
               'GeoCode|State|County|City|Zip',
               bcp.geocode || '|' || bcp.state || '|' || bcp.county || '|' || bcp.city || '|' || bcp.zip,
               'n/a',
               'n/a',
               'n/a' );
         END IF;
      END IF;

      -- Find jurisdiction taxrate
      BEGIN
         SELECT rate.jurisdiction_taxrate_id,
                rate.custom_flag,
                rate.modified_flag
           INTO v_jurisdiction_taxrate_id,
                v_rate_custom_flag,
                v_rate_modified_flag
           FROM tb_jurisdiction_taxrate rate
          WHERE ( rate.jurisdiction_id = v_jurisdiction_id ) AND
                ( rate.effective_date = bcp.effective_date ) AND
                ( rate.measure_type_code = '0' );
      EXCEPTION
         WHEN OTHERS THEN
            v_jurisdiction_taxrate_id := 0;
            v_rate_custom_flag := '0';
            v_rate_modified_flag := '0';
      END;

      -- If it does not exist, add it
      IF v_jurisdiction_taxrate_id IS NULL or v_jurisdiction_taxrate_id = 0 THEN
         INSERT INTO tb_jurisdiction_taxrate(
            jurisdiction_taxrate_id,
            jurisdiction_id,
            measure_type_code,
            effective_date,
            expiration_date,
            custom_flag,
            modified_flag,
            state_sales_rate,
            state_use_rate,
            county_sales_rate,
            county_use_rate,
            county_local_sales_rate,
            county_local_use_rate,
            county_split_amount,
            county_maxtax_amount,
            county_single_flag,
            county_default_flag,
            city_sales_rate,
            city_use_rate,
            city_local_sales_rate,
            city_local_use_rate,
            city_split_amount,
            city_split_sales_rate,
            city_split_use_rate,
            city_single_flag,
            city_default_flag )
         VALUES(
            0,
            v_jurisdiction_id,
            '0',
            bcp.effective_date,
            to_date('12/31/9999', 'mm/dd/yyyy'),
            '0',
            '0',
            bcp.state_sales_rate,
            bcp.state_use_rate,
            bcp.county_sales_rate,
            bcp.county_use_rate,
            bcp.county_local_sales_rate,
            bcp.county_local_use_rate,
            bcp.county_split_amount,
            bcp.county_maxtax_amount,
            bcp.county_single_flag,
            bcp.county_default_flag,
            bcp.city_sales_rate,
            bcp.city_use_rate,
            bcp.city_local_sales_rate,
            bcp.city_local_use_rate,
            bcp.city_split_amount,
            bcp.city_split_sales_rate,
            bcp.city_split_use_rate,
            bcp.city_single_flag,
            bcp.city_default_flag );
      ELSE
         -- If it does exist and jurisdiction is not custom but rate is custom (modified or not) then write errog log warning
         IF (( v_jur_custom_flag IS NULL ) OR ( v_jur_custom_flag = '0' )) AND
            ( v_rate_custom_flag = '1' ) THEN
            v_error_sev_code := '10';
            INSERT INTO tb_batch_error_log (
               batch_error_log_id,
               batch_id,
               process_type,
               process_timestamp,
               row_no,
               column_no,
               error_def_code,
               import_header_column,
               import_column_value,
               trans_dtl_column_name,
               trans_dtl_datatype,
               import_row )
            VALUES (
               0,
               bcp.batch_id,
               'TR',
               v_sysdate,
               rownum,
               0,
               'TR2',
               'GeoCode|State|County|City|Zip|Effective Date',
               bcp.geocode || '|' || bcp.state || '|' || bcp.county || '|' || bcp.city || '|' || bcp.zip || '|' || to_char(bcp.effective_date, 'mm/dd/yyyy'),
               'n/a',
               'n/a',
               'n/a' );
         ELSE
            -- If it does exist and jurisdiction is not custom and rate is not custom but rate is modified then write error log warning
            IF (( v_jur_custom_flag IS NULL ) OR ( v_jur_custom_flag = '0' )) AND
               (( v_rate_custom_flag IS NULL ) OR ( v_rate_custom_flag = '0' )) AND
               ( v_rate_modified_flag = '1' ) THEN
               v_error_sev_code := '10';
               INSERT INTO tb_batch_error_log (
                  batch_error_log_id,
                  batch_id,
                  process_type,
                  process_timestamp,
                  row_no,
                  column_no,
                  error_def_code,
                  import_header_column,
                  import_column_value,
                  trans_dtl_column_name,
                  trans_dtl_datatype,
                  import_row )
               VALUES (
                  0,
                  bcp.batch_id,
                  'TR',
                  v_sysdate,
                  rownum,
                  0,
                  'TR3',
                  'GeoCode|State|County|City|Zip|Effective Date',
                  bcp.geocode || '|' || bcp.state || '|' || bcp.county || '|' || bcp.city || '|' || bcp.zip || '|' || to_char(bcp.effective_date, 'mm/dd/yyyy'),
                  'n/a',
                  'n/a',
                  'n/a' );
            END IF;
         END IF;
         -- Update TaxRate
         UPDATE tb_jurisdiction_taxrate rate SET
            rate.modified_flag = 0,
            rate.state_sales_rate = bcp.state_sales_rate,
            rate.state_use_rate =  bcp.state_use_rate,
            rate.county_sales_rate = bcp.county_sales_rate,
            rate.county_use_rate = bcp.county_use_rate,
            rate.county_local_sales_rate = bcp.county_local_sales_rate,
            rate.county_local_use_rate = bcp.county_local_use_rate,
            rate.county_split_amount = bcp.county_split_amount,
            rate.county_maxtax_amount = bcp.county_maxtax_amount,
            rate.county_single_flag = bcp.county_single_flag,
            rate.county_default_flag = bcp.county_default_flag,
            rate.city_sales_rate = bcp.city_sales_rate,
            rate.city_use_rate = bcp.city_use_rate,
            rate.city_local_sales_rate = bcp.city_local_sales_rate,
            rate.city_local_use_rate = bcp.city_local_use_rate,
            rate.city_split_amount = bcp.city_split_amount,
            rate.city_split_sales_rate = bcp.city_split_sales_rate,
            rate.city_split_use_rate = bcp.city_split_use_rate,
            rate.city_single_flag = bcp.city_single_flag,
            rate.city_default_flag = bcp.city_default_flag
         WHERE rate.jurisdiction_taxrate_id = v_jurisdiction_taxrate_id;
      END IF;
   END LOOP;
   CLOSE bcp_juris_taxrate_cursor;

   -- Remove Batch from BCP Transactions Table
   DELETE
     FROM tb_bcp_jurisdiction_taxrate
    WHERE batch_id = an_batch_id;

   --  Update Options table with last month/year updated
   UPDATE tb_option
      SET value = to_char(vd_update_date, 'yyyy_mm')
    WHERE option_code = 'LASTTAXRATEDATE' || vc_taxrate_update_type AND
          option_type_code = 'SYSTEM' AND
          user_code = 'SYSTEM';

   -- Update Options table with last release number
   UPDATE tb_option
      SET value = vi_release_number
    WHERE option_code = 'LASTTAXRATEREL' || vc_taxrate_update_type AND
          option_type_code = 'SYSTEM' AND
          user_code = 'SYSTEM';

   -- Update batch with final totals
   UPDATE tb_batch
      SET batch_status_code = 'TR',
          error_sev_code = v_error_sev_code,
          processed_rows = v_processed_rows,
          actual_end_timestamp = SYSDATE
    WHERE batch_id = an_batch_id;

   -- Update CCH Code table
   DELETE FROM tb_cch_code WHERE custom_flag <> '1';

   /*INSERT INTO tb_cch_code (
      filename,
      fieldname,
      fieldpos,
      fieldlen,
      fieldno,
      code,
      description,
      custom_flag,
      modified_flag )
   SELECT
      filename,
      fieldname,
      fieldpos,
      fieldlen,
      fieldno,
      code,
      description,
      '0',
      '0'
   FROM tb_bcp_cch_code
   WHERE batch_id = an_batch_id;*/

   /*DELETE
     FROM tb_bcp_cch_code
    WHERE batch_id = an_batch_id;*/

   -- Update CCH TxMatrix table
   DELETE FROM tb_cch_txmatrix WHERE custom_flag <> '1';

   /*INSERT INTO tb_cch_txmatrix (
      city,
      county,
      state,
      local,
      geocode,
      groupcode,
      groupdesc,
      item,
      itemdesc,
      provider,
      customer,
      taxtypet,
      taxcatt,
      taxable,
      taxtype,
      taxcat,
      effdate,
      rectype,
      custom_flag,
      modified_flag )
   SELECT
      city,
      county,
      state,
      local,
      geocode,
      groupcode,
      groupdesc,
      item,
      itemdesc,
      provider,
      customer,
      taxtypet,
      taxcatt,
      taxable,
      taxtype,
      taxcat,
      to_date(effdate,'yyyymmdd'),
      rectype,
     '0',
     '0'
   FROM tb_bcp_cch_txmatrix
   WHERE batch_id = an_batch_id;*/

   /*DELETE
     FROM tb_bcp_cch_txmatrix
    WHERE batch_id = an_batch_id;*/

   -- commit all updates
   COMMIT;

   -- Enable triggers
   execute immediate 'ALTER TRIGGER tb_jurisdiction_taxrate_a_i ENABLE';

EXCEPTION
   WHEN e_abort THEN
      -- Update batch with error codes
      UPDATE tb_batch
         SET batch_status_code = v_batch_status_code,
             error_sev_code = v_error_sev_code,
             actual_end_timestamp = SYSDATE
       WHERE batch_id = an_batch_id;
      COMMIT;
      -- Enable triggers
      execute immediate 'ALTER TRIGGER tb_jurisdiction_taxrate_a_i ENABLE';
   WHEN e_halt THEN
      NULL;
END;
/

SHOW ERRORS;

CREATE OR REPLACE PROCEDURE sp_invoke_tb_trans_detail_b_u(p_transaction_detail_id IN tb_transaction_detail.transaction_detail_id%TYPE)
AS
	l_old_tb_transaction_detail tb_transaction_detail%ROWTYPE;
	l_new_tb_transaction_detail tb_transaction_detail%ROWTYPE;
BEGIN
	SELECT * INTO l_old_tb_transaction_detail FROM tb_transaction_detail		WHERE transaction_detail_id = p_transaction_detail_id;
	SELECT * INTO l_new_tb_transaction_detail FROM tb_tmp_transaction_detail_b_u	WHERE transaction_detail_id = p_transaction_detail_id;

	sp_tb_transaction_detail_b_u(l_old_tb_transaction_detail, l_new_tb_transaction_detail);
END;
/

SHOW ERRORS;

CREATE OR REPLACE PROCEDURE sp_max_all
IS
-- Program starts **********************************************************************************
BEGIN
   sp_max_tb_allocation_matrix_id();
   sp_max_gl_extract_batch_id();
   sp_max_tb_batch_error_log_id();
   sp_max_tb_batch_id();
   sp_max_tb_entity_id();
   sp_max_tb_jurisdiction_id();
   sp_max_tb_juris_taxrate_id();
   sp_max_tb_location_matrix_id();
 --sp_max_tb_questans_log_id();
   sp_max_tb_taxcode_detail_id();
   sp_max_tb_tax_matrix_id();
   sp_max_tb_trans_detail_id();
   sp_max_tb_trans_detail_excp_id();
   sp_max_tb_bcp_juris_taxrate_id();
END;
/

SHOW ERRORS;


CREATE OR REPLACE PACKAGE matrix_record_pkg AS

   -- Location Matrix Record TYPE
   TYPE location_matrix_record is RECORD (
      location_matrix_id           tb_location_matrix.location_matrix_id%TYPE,
      jurisdiction_id              tb_location_matrix.jurisdiction_id%TYPE,
      override_taxtype_code        tb_location_matrix.override_taxtype_code%TYPE,
      state_flag                   tb_location_matrix.state_flag%TYPE,
      county_flag                  tb_location_matrix.county_flag%TYPE,
      county_local_flag            tb_location_matrix.county_local_flag%TYPE,
      city_flag                    tb_location_matrix.city_flag%TYPE,
      city_local_flag              tb_location_matrix.city_local_flag%TYPE,
      state                        tb_jurisdiction.state%TYPE,
      county                       tb_jurisdiction.county%TYPE,
      city                         tb_jurisdiction.city%TYPE,
      zip                          tb_jurisdiction.zip%TYPE,
      in_out                       tb_jurisdiction.in_out%TYPE );

   -- Tax Matrix Record TYPE
   TYPE tax_matrix_record is RECORD (
      tax_matrix_id                tb_tax_matrix.tax_matrix_id%TYPE,
      relation_sign                tb_tax_matrix.relation_sign%TYPE,
      relation_amount              tb_tax_matrix.relation_amount%TYPE,
      then_hold_code_flag          tb_tax_matrix.then_hold_code_flag%TYPE,
      else_hold_code_flag          tb_tax_matrix.else_hold_code_flag%TYPE,
      then_taxcode_detail_id       tb_bcp_transactions.taxcode_detail_id%TYPE,
      else_taxcode_detail_id       tb_bcp_transactions.taxcode_detail_id%TYPE,
      then_cch_taxcat_code         tb_tax_matrix.then_cch_taxcat_code%TYPE,
      then_cch_group_code          tb_tax_matrix.then_cch_group_code%TYPE,
      then_cch_item_code           tb_tax_matrix.then_cch_item_code%TYPE,
      else_cch_taxcat_code         tb_tax_matrix.else_cch_taxcat_code%TYPE,
      else_cch_group_code          tb_tax_matrix.else_cch_group_code%TYPE,
      else_cch_item_code           tb_tax_matrix.else_cch_item_code%TYPE,
      then_taxcode_state_code      tb_bcp_transactions.taxcode_state_code%TYPE,
      then_taxcode_type_code       tb_bcp_transactions.taxcode_type_code%TYPE,
      then_taxcode_code            tb_bcp_transactions.taxcode_code%TYPE,
      then_jurisdiction_id         tb_bcp_transactions.jurisdiction_id%TYPE,
      then_measure_type_code       tb_bcp_transactions.measure_type_code%TYPE,
      then_taxtype_code            tb_taxcode.taxtype_code%TYPE,
      else_taxcode_state_code      tb_bcp_transactions.taxcode_state_code%TYPE,
      else_taxcode_type_code       tb_bcp_transactions.taxcode_type_code%TYPE,
      else_taxcode_code            tb_bcp_transactions.taxcode_code%TYPE,
      else_jurisdiction_id         tb_bcp_transactions.jurisdiction_id%TYPE,
      else_measure_type_code       tb_bcp_transactions.measure_type_code%TYPE,
      else_taxtype_code            tb_taxcode.taxtype_code%TYPE );

END;
/

SHOW ERRORS;

ALTER PACKAGE STS COMPILE BODY;
/
CREATE INDEX IDX_TB_TRANS_DTL_DRIVER_01 ON TB_TRANSACTION_DETAIL
(VENDOR_TYPE)
NOLOGGING
NOPARALLEL
;
 
CREATE INDEX IDX_TB_TRANS_DTL_DRIVER_01_UP ON TB_TRANSACTION_DETAIL
(UPPER("VENDOR_TYPE"))
NOLOGGING
NOPARALLEL
;
 
CREATE INDEX IDX_TB_TRANS_DTL_DRIVER_04 ON TB_TRANSACTION_DETAIL
(VENDOR_NBR)
NOLOGGING
NOPARALLEL
;
 
CREATE INDEX IDX_TB_TRANS_DTL_DRIVER_04_UP ON TB_TRANSACTION_DETAIL
(UPPER("VENDOR_NBR"))
NOLOGGING
NOPARALLEL
;
 
CREATE INDEX IDX_TB_TRANS_DTL_DRIVER_06 ON TB_TRANSACTION_DETAIL
(GL_CC_NBR_DEPT_ID)
NOLOGGING
NOPARALLEL
;
 
CREATE INDEX IDX_TB_TRANS_DTL_DRIVER_06_UP ON TB_TRANSACTION_DETAIL
(UPPER("GL_CC_NBR_DEPT_ID"))
NOLOGGING
NOPARALLEL
;
 
CREATE INDEX IDX_TB_TRANS_DTL_DRIVER_09 ON TB_TRANSACTION_DETAIL
(SHIP_TO_LOCATION)
NOLOGGING
NOPARALLEL
;
 
CREATE INDEX IDX_TB_TRANS_DTL_DRIVER_09_UP ON TB_TRANSACTION_DETAIL
(UPPER("SHIP_TO_LOCATION"))
NOLOGGING
NOPARALLEL
;
 
CREATE TABLE TB_MAIN_MENU_ORIG AS SELECT * FROM TB_MAIN_MENU;

TRUNCATE TABLE TB_MAIN_MENU;

COMMIT;

REM INSERTING into TB_MENU
Insert into TB_MENU (MENU_CODE,MAIN_MENU_CODE,MENU_SEQUENCE,OPTION_NAME,COMMAND_TYPE,COMMAND_LINE,KEY_VALUES,PANEL_CLASS_NAME,UPDATE_USER_ID,UPDATE_TIMESTAMP) values ('m_driverreference','m_maintenance',6,'Driver Reference','OPEN','w_driver_reference_maintenance','microhelp=Maintain Driver References;','com.ryanco.sts.client.driverref.swing.DriverReferenceTreeViewPanel','STSCORP',to_timestamp(SYSDATE,'DD-MON-RR HH.MI.SSXFF AM'));
Insert into TB_MENU (MENU_CODE,MAIN_MENU_CODE,MENU_SEQUENCE,OPTION_NAME,COMMAND_TYPE,COMMAND_LINE,KEY_VALUES,PANEL_CLASS_NAME,UPDATE_USER_ID,UPDATE_TIMESTAMP) values ('m_glextractmap','m_maintenance',7,'G/L Extract Map','OPEN','w_gl_extract_map_maintenance','microhelp=Maintain G/L Extract Map','com.ryanco.sts.client.glextractmap.swing.GLExtractMapMaintenancePanel','STSCORP',to_timestamp(SYSDATE,'DD-MON-RR HH.MI.SSXFF AM'));
Insert into TB_MENU (MENU_CODE,MAIN_MENU_CODE,MENU_SEQUENCE,OPTION_NAME,COMMAND_TYPE,COMMAND_LINE,KEY_VALUES,PANEL_CLASS_NAME,UPDATE_USER_ID,UPDATE_TIMESTAMP) values ('m_reweightmatrixes','m_utilities',5,'Reweight Matrices','OPEN','w_reweight_matrixes','microhelp=Reweight Tax/Location Matrices;',null,'STSCORP',to_timestamp(SYSDATE,'DD-MON-RR HH.MI.SSXFF AM'));
Insert into TB_MENU (MENU_CODE,MAIN_MENU_CODE,MENU_SEQUENCE,OPTION_NAME,COMMAND_TYPE,COMMAND_LINE,KEY_VALUES,PANEL_CLASS_NAME,UPDATE_USER_ID,UPDATE_TIMESTAMP) values ('m_dbstatisticslines','m_setup',6,'DB Statistics Lines','OPEN','w_db_statistics_maintenance','microhelp=Maintain DB Statistics Lines;','com.ryanco.sts.client.setup.swing.DBStatisticsLinesPanel','STSCORP',to_timestamp(SYSDATE,'DD-MON-RR HH.MI.SSXFF AM'));
Insert into TB_MENU (MENU_CODE,MAIN_MENU_CODE,MENU_SEQUENCE,OPTION_NAME,COMMAND_TYPE,COMMAND_LINE,KEY_VALUES,PANEL_CLASS_NAME,UPDATE_USER_ID,UPDATE_TIMESTAMP) values ('m_rebuilddriverreferences','m_utilities',6,'Rebuild Driver References','OPEN','w_rebuild_driver_references','microhelp=Clear and Rebuild Driver References',null,'STSCORP',to_timestamp(SYSDATE,'DD-MON-RR HH.MI.SSXFF AM'));
Insert into TB_MENU (MENU_CODE,MAIN_MENU_CODE,MENU_SEQUENCE,OPTION_NAME,COMMAND_TYPE,COMMAND_LINE,KEY_VALUES,PANEL_CLASS_NAME,UPDATE_USER_ID,UPDATE_TIMESTAMP) values ('m_truncatebcptables','m_utilities',7,'Truncate BCP Tables','OPEN','w_truncate_bcp_tables','microhelp=Truncate BCP Tables',null,'STSCORP',to_timestamp(SYSDATE,'DD-MON-RR HH.MI.SSXFF AM'));
Insert into TB_MENU (MENU_CODE,MAIN_MENU_CODE,MENU_SEQUENCE,OPTION_NAME,COMMAND_TYPE,COMMAND_LINE,KEY_VALUES,PANEL_CLASS_NAME,UPDATE_USER_ID,UPDATE_TIMESTAMP) values ('m_transactions','m_process',1,'Transactions','OPEN','w_transaction_maintenance','microhelp=Maintain Processed & Unprocessed Transactions;','com.ryanco.sts.client.transactions.swing.TransactionsPanel','STSCORP',to_timestamp(SYSDATE,'DD-MON-RR HH.MI.SSXFF AM'));
Insert into TB_MENU (MENU_CODE,MAIN_MENU_CODE,MENU_SEQUENCE,OPTION_NAME,COMMAND_TYPE,COMMAND_LINE,KEY_VALUES,PANEL_CLASS_NAME,UPDATE_USER_ID,UPDATE_TIMESTAMP) values ('m_taxabilitycodes','m_maintenance',4,'Taxability Codes','OPEN','w_taxcode_maintenance','microhelp=Maintain Tax Codes;','com.ryanco.sts.client.taxabilitycodes.swing.CodeMaintenancePanel','STSCORP',to_timestamp(SYSDATE,'DD-MON-RR HH.MI.SSXFF AM'));
Insert into TB_MENU (MENU_CODE,MAIN_MENU_CODE,MENU_SEQUENCE,OPTION_NAME,COMMAND_TYPE,COMMAND_LINE,KEY_VALUES,PANEL_CLASS_NAME,UPDATE_USER_ID,UPDATE_TIMESTAMP) values ('m_taxrateupdate','m_datautility',4,'Tax Rate Updates','PARM','w_batch_workwith','microhelp=Import Monthly Tax Rate Updates and Apply;parm=TR;','com.ryanco.sts.client.datautility.swing.TaxRateBatchesPanel','STSCORP',to_timestamp(SYSDATE,'DD-MON-RR HH.MI.SSXFF AM'));
Insert into TB_MENU (MENU_CODE,MAIN_MENU_CODE,MENU_SEQUENCE,OPTION_NAME,COMMAND_TYPE,COMMAND_LINE,KEY_VALUES,PANEL_CLASS_NAME,UPDATE_USER_ID,UPDATE_TIMESTAMP) values ('m_databasesetup','m_setup',1,'Database Setup','OPEN','w_database_setup','microhelp=Database Setup','com.ryanco.sts.client.setup.swing.DatabaseSetupPanel','STSCORP',to_timestamp(SYSDATE,'DD-MON-RR HH.MI.SSXFF AM'));
Insert into TB_MENU (MENU_CODE,MAIN_MENU_CODE,MENU_SEQUENCE,OPTION_NAME,COMMAND_TYPE,COMMAND_LINE,KEY_VALUES,PANEL_CLASS_NAME,UPDATE_USER_ID,UPDATE_TIMESTAMP) values ('m_properties','m_file',1,'Preferences/Options','OPEN','w_preference_maintenance','microhelp=Set Options and Preferences','com.ryanco.sts.client.preferences.swing.PreferencesPanel','STSCORP',to_timestamp(SYSDATE,'DD-MON-RR HH.MI.SSXFF AM'));
Insert into TB_MENU (MENU_CODE,MAIN_MENU_CODE,MENU_SEQUENCE,OPTION_NAME,COMMAND_TYPE,COMMAND_LINE,KEY_VALUES,PANEL_CLASS_NAME,UPDATE_USER_ID,UPDATE_TIMESTAMP) values ('m_lists','m_setup',5,'Lists','OPEN','w_list_code_maintenance','microhelp=Maintain All Code Lists;','com.ryanco.sts.client.setup.swing.ListCodeMaintenancePanel','STSCORP',to_timestamp(SYSDATE,'DD-MON-RR HH.MI.SSXFF AM'));
Insert into TB_MENU (MENU_CODE,MAIN_MENU_CODE,MENU_SEQUENCE,OPTION_NAME,COMMAND_TYPE,COMMAND_LINE,KEY_VALUES,PANEL_CLASS_NAME,UPDATE_USER_ID,UPDATE_TIMESTAMP) values ('m_batchesmaintstatus','m_datautility',1,'Batch Maint./Status','OPEN','w_batch_maintenance','microhelp=Maintain and Check Status of Batches','com.ryanco.sts.client.datautility.swing.BatchMaintenanceListPanel','STSCORP',to_timestamp(SYSDATE,'DD-MON-RR HH.MI.SSXFF AM'));
Insert into TB_MENU (MENU_CODE,MAIN_MENU_CODE,MENU_SEQUENCE,OPTION_NAME,COMMAND_TYPE,COMMAND_LINE,KEY_VALUES,PANEL_CLASS_NAME,UPDATE_USER_ID,UPDATE_TIMESTAMP) values ('m_glextract','m_datautility',3,'G/L Extract','PARM','w_batch_workwith','microhelp=G/L Extract Procedure;parm=GE','com.ryanco.sts.client.datautility.swing.GLExtractBatchPanel','STSCORP',to_timestamp(SYSDATE,'DD-MON-RR HH.MI.SSXFF AM'));
Insert into TB_MENU (MENU_CODE,MAIN_MENU_CODE,MENU_SEQUENCE,OPTION_NAME,COMMAND_TYPE,COMMAND_LINE,KEY_VALUES,PANEL_CLASS_NAME,UPDATE_USER_ID,UPDATE_TIMESTAMP) values ('m_allocationmatrix','m_maintenance',3,'Allocation Matrix','OPEN','w_alloc_matrix_maintenance','microhelp=Maintain Allocation Matrix;','com.ryanco.sts.client.allocationmatrix.swing.AllocationMaintenanceListPanel','STSCORP',to_timestamp(SYSDATE,'DD-MON-RR HH.MI.SSXFF AM'));
Insert into TB_MENU (MENU_CODE,MAIN_MENU_CODE,MENU_SEQUENCE,OPTION_NAME,COMMAND_TYPE,COMMAND_LINE,KEY_VALUES,PANEL_CLASS_NAME,UPDATE_USER_ID,UPDATE_TIMESTAMP) values ('m_taxpartnerextract','m_datautility',6,'Tax Partners Extract','PARM','w_batch_workwith','microhelp=Tax Partners Extract;parm=TE;','com.ryanco.sts.client.datautility.swing.TaxPartnersExtractBatchPanel','STSCORP',to_timestamp(SYSDATE,'DD-MON-RR HH.MI.SSXFF AM'));
Insert into TB_MENU (MENU_CODE,MAIN_MENU_CODE,MENU_SEQUENCE,OPTION_NAME,COMMAND_TYPE,COMMAND_LINE,KEY_VALUES,PANEL_CLASS_NAME,UPDATE_USER_ID,UPDATE_TIMESTAMP) values ('m_taxabilitymatrix','m_maintenance',1,'Taxability Matrix','OPEN','w_tax_matrix_maintenance','microhelp=Maintain Tax Matrix;','com.ryanco.sts.client.taxmatrix.swing.TaxMatrixPanel','STSCORP',to_timestamp(SYSDATE,'DD-MON-RR HH.MI.SSXFF AM'));
Insert into TB_MENU (MENU_CODE,MAIN_MENU_CODE,MENU_SEQUENCE,OPTION_NAME,COMMAND_TYPE,COMMAND_LINE,KEY_VALUES,PANEL_CLASS_NAME,UPDATE_USER_ID,UPDATE_TIMESTAMP) values ('m_locationmatrix','m_maintenance',2,'Location Matrix','OPEN','w_locn_matrix_maintenance','microhelp=Maintain Location Matrix;','com.ryanco.sts.client.locationmatrix.swing.LocationMatrixPanel','STSCORP',to_timestamp(SYSDATE,'DD-MON-RR HH.MI.SSXFF AM'));
Insert into TB_MENU (MENU_CODE,MAIN_MENU_CODE,MENU_SEQUENCE,OPTION_NAME,COMMAND_TYPE,COMMAND_LINE,KEY_VALUES,PANEL_CLASS_NAME,UPDATE_USER_ID,UPDATE_TIMESTAMP) values ('m_archive','m_datautility',5,'Archive','PARM','w_batch_workwith','microhelp=Archive/Restore Transactions;parm=A;','com.ryanco.sts.client.datautility.swing.ArchiveBatchPanel','STSCORP',to_timestamp(SYSDATE,'DD-MON-RR HH.MI.SSXFF AM'));
Insert into TB_MENU (MENU_CODE,MAIN_MENU_CODE,MENU_SEQUENCE,OPTION_NAME,COMMAND_TYPE,COMMAND_LINE,KEY_VALUES,PANEL_CLASS_NAME,UPDATE_USER_ID,UPDATE_TIMESTAMP) values ('m_buildindexes','m_utilities',4,'Build Indexes','OPEN','w_build_indexes','microhelp=Build Indexes',null,'STSCORP',to_timestamp(SYSDATE,'DD-MON-RR HH.MI.SSXFF AM'));
Insert into TB_MENU (MENU_CODE,MAIN_MENU_CODE,MENU_SEQUENCE,OPTION_NAME,COMMAND_TYPE,COMMAND_LINE,KEY_VALUES,PANEL_CLASS_NAME,UPDATE_USER_ID,UPDATE_TIMESTAMP) values ('m_taxrates','m_maintenance',5,'Tax Rates','OPEN','w_tax_jur_rate_maintenance','microhelp=Maintain Tax Rates;','com.ryanco.sts.client.taxrate.swing.TaxRateMaintenanceListPanel','STSCORP',to_timestamp(SYSDATE,'DD-MON-RR HH.MI.SSXFF AM'));
Insert into TB_MENU (MENU_CODE,MAIN_MENU_CODE,MENU_SEQUENCE,OPTION_NAME,COMMAND_TYPE,COMMAND_LINE,KEY_VALUES,PANEL_CLASS_NAME,UPDATE_USER_ID,UPDATE_TIMESTAMP) values ('m_resetsequences','m_utilities',1,'Reset All Sequences','OPEN','w_reset_all_sequences','microhelp=Reset All Sequences',null,'STSCORP',to_timestamp(SYSDATE,'DD-MON-RR HH.MI.SSXFF AM'));
Insert into TB_MENU (MENU_CODE,MAIN_MENU_CODE,MENU_SEQUENCE,OPTION_NAME,COMMAND_TYPE,COMMAND_LINE,KEY_VALUES,PANEL_CLASS_NAME,UPDATE_USER_ID,UPDATE_TIMESTAMP) values ('m_crystalreports','m_utilities',21,'Crystal Reports','RUN','crw32.exe','microhelp=Launch Crystal Reports;userdir=USERDIRCRYSTAL;defaultdir=DEFAULTDIRCRYSTAL;',null,'STSCORP',to_timestamp(SYSDATE,'DD-MON-RR HH.MI.SSXFF AM'));
Insert into TB_MENU (MENU_CODE,MAIN_MENU_CODE,MENU_SEQUENCE,OPTION_NAME,COMMAND_TYPE,COMMAND_LINE,KEY_VALUES,PANEL_CLASS_NAME,UPDATE_USER_ID,UPDATE_TIMESTAMP) values ('m_changepassword','m_security',2,'Change Password','OPEN','w_change_password','microhelp=Change Your Password','com.ryanco.sts.client.security.swing.ChangePasswordPanel','STSCORP',to_timestamp(SYSDATE,'DD-MON-RR HH.MI.SSXFF AM'));
Insert into TB_MENU (MENU_CODE,MAIN_MENU_CODE,MENU_SEQUENCE,OPTION_NAME,COMMAND_TYPE,COMMAND_LINE,KEY_VALUES,PANEL_CLASS_NAME,UPDATE_USER_ID,UPDATE_TIMESTAMP) values ('m_datadefinitions','m_setup',2,'Data Definitions','OPEN','w_data_def_maintenance','microhelp=Maintain Data Definitions;','com.ryanco.sts.client.setup.swing.DataDefinitionsMaintenancePanel','STSCORP',to_timestamp(SYSDATE,'DD-MON-RR HH.MI.SSXFF AM'));
Insert into TB_MENU (MENU_CODE,MAIN_MENU_CODE,MENU_SEQUENCE,OPTION_NAME,COMMAND_TYPE,COMMAND_LINE,KEY_VALUES,PANEL_CLASS_NAME,UPDATE_USER_ID,UPDATE_TIMESTAMP) values ('m_runmcp','m_utilities',2,'Run Master Control Program','OPEN','w_run_mcp','microhelp=Reset the Master Control Program (the Bus)',null,'STSCORP',to_timestamp(SYSDATE,'DD-MON-RR HH.MI.SSXFF AM'));
Insert into TB_MENU (MENU_CODE,MAIN_MENU_CODE,MENU_SEQUENCE,OPTION_NAME,COMMAND_TYPE,COMMAND_LINE,KEY_VALUES,PANEL_CLASS_NAME,UPDATE_USER_ID,UPDATE_TIMESTAMP) values ('m_resetdwsyntax','m_utilities',3,'Reset DataWindow Syntax','OPEN','w_reset_dw_syntax','microhelp=Reset the Transaction Grid Columns',null,'STSCORP',to_timestamp(SYSDATE,'DD-MON-RR HH.MI.SSXFF AM'));
Insert into TB_MENU (MENU_CODE,MAIN_MENU_CODE,MENU_SEQUENCE,OPTION_NAME,COMMAND_TYPE,COMMAND_LINE,KEY_VALUES,PANEL_CLASS_NAME,UPDATE_USER_ID,UPDATE_TIMESTAMP) values ('m_matrixdrivers','m_setup',3,'Matrix Drivers','OPEN','w_matrix_drivers_maintenance','microhelp=Maintain Matrix Drivers','com.ryanco.sts.client.setup.swing.MatrixDriverMaintenancePanel','STSCORP',to_timestamp(SYSDATE,'DD-MON-RR HH.MI.SSXFF AM'));
Insert into TB_MENU (MENU_CODE,MAIN_MENU_CODE,MENU_SEQUENCE,OPTION_NAME,COMMAND_TYPE,COMMAND_LINE,KEY_VALUES,PANEL_CLASS_NAME,UPDATE_USER_ID,UPDATE_TIMESTAMP) values ('m_importdefinitionsandspecs','m_setup',4,'Import Definitions and Specs','OPEN','w_import_defn_spec_maintenance','microhelp=Maintain Import Definitions and Specs','com.ryanco.sts.client.setup.swing.ImportDefSpecsMaintenancePanel','STSCORP',to_timestamp(SYSDATE,'DD-MON-RR HH.MI.SSXFF AM'));
Insert into TB_MENU (MENU_CODE,MAIN_MENU_CODE,MENU_SEQUENCE,OPTION_NAME,COMMAND_TYPE,COMMAND_LINE,KEY_VALUES,PANEL_CLASS_NAME,UPDATE_USER_ID,UPDATE_TIMESTAMP) values ('m_importprocess','m_datautility',2,'Import/Map and Process','PARM','w_batch_workwith','microhelp=Import/Map/Process Transactions;parm=IP;','com.ryanco.sts.client.datautility.swing.ImportMapProcessBatchPanel','STSCORP',to_timestamp(SYSDATE,'DD-MON-RR HH.MI.SSXFF AM'));
Insert into TB_MENU (MENU_CODE,MAIN_MENU_CODE,MENU_SEQUENCE,OPTION_NAME,COMMAND_TYPE,COMMAND_LINE,KEY_VALUES,PANEL_CLASS_NAME,UPDATE_USER_ID,UPDATE_TIMESTAMP) values ('m_statistics','m_process',2,'Database Statistics','OPEN','w_database_statistics','microhelp=Display Database Statistics','com.ryanco.sts.client.process.swing.DatabaseStatisticsPanel','STSCORP',to_timestamp(SYSDATE,'DD-MON-RR HH.MI.SSXFF AM'));
Insert into TB_MENU (MENU_CODE,MAIN_MENU_CODE,MENU_SEQUENCE,OPTION_NAME,COMMAND_TYPE,COMMAND_LINE,KEY_VALUES,PANEL_CLASS_NAME,UPDATE_USER_ID,UPDATE_TIMESTAMP) values ('m_windowsexplorer','m_utilities',22,'Windows Explorer','RUN','explorer','microhelp=Launch Windows Explorer',null,'STSCORP',to_timestamp(SYSDATE,'DD-MON-RR HH.MI.SSXFF AM'));
Insert into TB_MENU (MENU_CODE,MAIN_MENU_CODE,MENU_SEQUENCE,OPTION_NAME,COMMAND_TYPE,COMMAND_LINE,KEY_VALUES,PANEL_CLASS_NAME,UPDATE_USER_ID,UPDATE_TIMESTAMP) values ('m_logonprofiles','m_security',1,'Logon Profiles','OPEN','w_logon_profile_maintenance','microhelp=Maintain Logon Profiles;',null,'STSCORP',to_timestamp(SYSDATE,'DD-MON-RR HH.MI.SSXFF AM'));
Insert into TB_MENU (MENU_CODE,MAIN_MENU_CODE,MENU_SEQUENCE,OPTION_NAME,COMMAND_TYPE,COMMAND_LINE,KEY_VALUES,PANEL_CLASS_NAME,UPDATE_USER_ID,UPDATE_TIMESTAMP) values ('m_securitymodules','m_security',3,'Security Modules','OPEN','w_security_maintenance','microhelp=Maintain Security Modules;','com.ryanco.sts.client.security.swing.SecurityModulesPanel','STSCORP',to_timestamp(SYSDATE,'DD-MON-RR HH.MI.SSXFF AM'));
Insert into TB_MENU (MENU_CODE,MAIN_MENU_CODE,MENU_SEQUENCE,OPTION_NAME,COMMAND_TYPE,COMMAND_LINE,KEY_VALUES,PANEL_CLASS_NAME,UPDATE_USER_ID,UPDATE_TIMESTAMP) values ('m_entitystructurelevels','m_security',4,'Entity Structure Levels','OPEN','w_entity_level_maintenance','microhelp=Maintain Entity Structure Levels;','com.ryanco.sts.client.security.swing.EntityLevelMaintenancePanel','STSCORP',to_timestamp(SYSDATE,'DD-MON-RR HH.MI.SSXFF AM'));
Insert into TB_MENU (MENU_CODE,MAIN_MENU_CODE,MENU_SEQUENCE,OPTION_NAME,COMMAND_TYPE,COMMAND_LINE,KEY_VALUES,PANEL_CLASS_NAME,UPDATE_USER_ID,UPDATE_TIMESTAMP) values ('m_rulesandsoftcodes','m_maintenance',6,'Rules and Softcodes','OPEN','w_rule_softcode_maintenance','microhelp=Maintain Rules and Softcodes;',null,'STSCORP',to_timestamp(SYSDATE,'DD-MON-RR HH.MI.SSXFF AM'));
REM INSERTING into TB_USER_ENTITY
Insert into TB_USER_ENTITY (USER_CODE,ENTITY_ID,ROLE_CODE,LAST_USED_TIMESTAMP,UPDATE_USER_ID,UPDATE_TIMESTAMP) values ('STSCORP',0,'ADMIN',null,'STSCORP',to_timestamp(SYSDATE,'DD-MON-RR HH.MI.SSXFF AM'));
COMMIT;
UPDATE tb_tax_matrix SET else_taxcode_detail_id = NULL WHERE else_taxcode_detail_id = 0;
COMMIT;



