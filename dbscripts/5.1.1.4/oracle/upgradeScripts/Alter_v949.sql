set define off
spool c:\alter_v949.log
set echo on

--For build 949 --

CREATE OR REPLACE PROCEDURE Sp_Batch
IS
   vi_running                      INTEGER                                           := 0;
   vi_batches                      INTEGER                                           := 0;
   vc_cursor_open                  CHAR(1)                                           := 'N';
   vdt_sysdate                     DATE                                              := SYS_EXTRACT_UTC(SYSTIMESTAMP);

CURSOR batch_cursor
IS
   SELECT batch_id,
          batch_type_code,
          batch_status_code
    FROM TB_BATCH
   WHERE ( batch_status_code LIKE 'F%' ) AND
         ( held_flag IS NULL OR held_flag <> '1' ) AND
         ( sched_start_timestamp IS NULL OR sched_start_timestamp <= vdt_sysdate )
ORDER BY sched_start_timestamp, batch_id;
batch_record                       batch_cursor%ROWTYPE;

-- ****** Program starts ****** --------------------------------------------------------------------
BEGIN
   SELECT COUNT(*)
     INTO vi_running
     FROM TB_BATCH
    WHERE batch_status_code LIKE 'X%';

   IF vi_running = 0 THEN
      LOOP
         IF vc_cursor_open = 'Y' THEN
            CLOSE batch_cursor;
            vc_cursor_open := 'N';
         END IF;

         OPEN batch_cursor;
         vc_cursor_open := 'Y';
         FETCH batch_cursor INTO batch_record;
         EXIT WHEN batch_cursor%NOTFOUND;

         IF batch_record.batch_status_code = 'FI' THEN
--            sp_batch_import(batch_record.batch_id);
            NULL;
         ELSIF batch_record.batch_status_code = 'FP' THEN
            Sp_Batch_Process(batch_record.batch_id);
         ELSIF batch_record.batch_status_code = 'FD' THEN
            Sp_Batch_Delete(batch_record.batch_id);
         ELSIF batch_record.batch_status_code = 'FA' THEN
            Sp_Batch_Archive(batch_record.batch_id);
         ELSIF batch_record.batch_status_code = 'FAR' THEN
            Sp_Batch_Archive_Restore(batch_record.batch_id);
         ELSIF batch_record.batch_status_code = 'FADR' THEN
            Sp_Batch_Archive_Delete(batch_record.batch_id);
         ELSIF batch_record.batch_status_code = 'FE' THEN
--            sp_batch_extract(batch_record.batch_id);
            NULL;
         ELSIF batch_record.batch_status_code = 'FTR' THEN
            Sp_Batch_Taxrate_Update(batch_record.batch_id);
         ELSE
            UPDATE TB_BATCH
               SET batch_status_code = 'ERR'
             WHERE batch_id = batch_record.batch_id;
            COMMIT;
         END IF;
      END LOOP;
      CLOSE batch_cursor;
   END IF;
END;
/

CREATE OR REPLACE PROCEDURE Sp_Batch_Archive(an_batch_id NUMBER)
IS
-- Define Cursor Type
   TYPE cursor_type IS REF CURSOR;

-- Update Transaction Detail SQL
   vc_trans_dtl_update             VARCHAR2(1000)                                    :=
      'UPDATE tb_transaction_detail ' ||
         'SET archive_batch_no = :an_batch_id ';
   vc_trans_dtl_where              VARCHAR2(2000)                                    :=
      'WHERE archive_batch_no IS NULL AND gl_extract_flag = 1';
   vc_trans_dtl_stmt               VARCHAR2(3000)                                    := NULL;
   trans_dtl_cursor                cursor_type;

-- Table defined variables
   v_batch_status_code             TB_BATCH.batch_status_code%TYPE                   := NULL;
   v_error_sev_code                TB_BATCH.error_sev_code%TYPE                      := ' ';
   v_severity_level                TB_LIST_CODE.severity_level%TYPE                  := ' ';
   v_write_import_line_flag        TB_LIST_CODE.write_import_line_flag%TYPE          := '1';
   v_abort_import_flag             TB_LIST_CODE.abort_import_flag%TYPE               := '0';
   v_business_unit                 TB_BATCH.vc01%TYPE                                := NULL;
   v_trans_detail_column_name      TB_BATCH.vc02%TYPE                                := NULL;
   v_from_timestamp                TB_BATCH.ts01%TYPE                                := NULL;
   v_to_timestamp                  TB_BATCH.ts02%TYPE                                := NULL;
   v_sysdate                       TB_BATCH.actual_start_timestamp%TYPE              := SYS_EXTRACT_UTC(SYSTIMESTAMP);

-- Program defined variables
   vi_error_count                  INTEGER                                           := 0;
   vi_cursor_id                    INTEGER                                           := 0;
   vi_rows_processed               INTEGER                                           := 0;

-- Define Exceptions
   e_badread                       EXCEPTION;
   e_badupdate                     EXCEPTION;
   e_badwrite                      EXCEPTION;
   e_wrongdata                     EXCEPTION;
   e_abort                         EXCEPTION;

-- Program starts ******************************************************************************************
BEGIN
   -- Confirm batch exists and is flagged for processing
   BEGIN
      SELECT batch_status_code
        INTO v_batch_status_code
        FROM TB_BATCH
       WHERE batch_id = an_batch_id;
      IF SQLCODE != 0 THEN
         RAISE e_badread;
      ELSIF v_batch_status_code != 'FA' THEN
         RAISE e_wrongdata;
      END IF;
   EXCEPTION
      WHEN NO_DATA_FOUND OR e_badread THEN
         vi_error_count := vi_error_count + 1;
         Sp_Geterrorcode('A8', an_batch_id, 'A', v_sysdate, v_severity_level, v_write_import_line_flag, v_abort_import_flag);
         IF v_severity_level > v_error_sev_code THEN
            v_error_sev_code := v_severity_level;
         END IF;
      WHEN e_wrongdata THEN
         vi_error_count := vi_error_count + 1;
         Sp_Geterrorcode('A9', an_batch_id, 'A', v_sysdate, v_severity_level, v_write_import_line_flag, v_abort_import_flag);
         IF v_severity_level > v_error_sev_code THEN
            v_error_sev_code := v_severity_level;
         END IF;
   END;
   -- Abort Procedure?
   IF v_abort_import_flag = '1' THEN
      v_batch_status_code := 'ABA';
      RAISE e_abort;
   END IF;

   -- Get Batch information
   SELECT VC01,
          VC02,
          TS01,
          TS02
     INTO v_trans_detail_column_name,
          v_business_unit,
          v_from_timestamp,
          v_to_timestamp
     FROM TB_BATCH
    WHERE batch_id = an_batch_id;

   -- Update batch as processing
   BEGIN
      UPDATE TB_BATCH
         SET batch_status_code = 'XA',
             error_sev_code = '',
             actual_start_timestamp = SYS_EXTRACT_UTC(SYSTIMESTAMP)
       WHERE batch_id = an_batch_id;
      IF SQLCODE != 0 THEN
         RAISE e_badupdate;
      END IF;
   EXCEPTION
      WHEN e_badupdate THEN
         vi_error_count := vi_error_count + 1;
         Sp_Geterrorcode('A1', an_batch_id, 'A', v_sysdate, v_severity_level, v_write_import_line_flag, v_abort_import_flag);
         IF v_severity_level > v_error_sev_code THEN
            v_error_sev_code := v_severity_level;
         END IF;
   END;
   -- Abort Procedure?
   IF v_abort_import_flag = '1' THEN
      v_batch_status_code := 'ABA';
      RAISE e_abort;
   END IF;

   COMMIT;

   /**  Create Where Clause and construct Dynamic SQL Statement  **********************************/
   -- Business Unit
   IF v_business_unit IS NOT NULL AND v_trans_detail_column_name IS NOT NULL THEN
      IF vc_trans_dtl_where IS NULL THEN
         vc_trans_dtl_where := 'WHERE ';
      ELSE
         vc_trans_dtl_where := vc_trans_dtl_where || ' AND ';
      END IF;
      vc_trans_dtl_where := vc_trans_dtl_where || v_trans_detail_column_name || ' = ''' || v_business_unit || '''';
   END IF;
   -- From G/L Date
   IF v_from_timestamp IS NOT NULL THEN
      IF vc_trans_dtl_where IS NULL THEN
         vc_trans_dtl_where := 'WHERE ';
      ELSE
         vc_trans_dtl_where := vc_trans_dtl_where || ' AND ';
      END IF;
      vc_trans_dtl_where := vc_trans_dtl_where || 'gl_date >= to_date(''' || TO_CHAR(v_from_timestamp,'mm/dd/yyyy') || ''',''mm/dd/yyyy'')';
   END IF;
   -- To G/L Date
   IF v_to_timestamp IS NOT NULL THEN
      IF vc_trans_dtl_where IS NULL THEN
         vc_trans_dtl_where := 'WHERE ';
      ELSE
         vc_trans_dtl_where := vc_trans_dtl_where || ' AND ';
      END IF;
      vc_trans_dtl_where := vc_trans_dtl_where || 'gl_date <= to_date(''' || TO_CHAR(v_to_timestamp,'mm/dd/yyyy') || ''',''mm/dd/yyyy'')';
   END IF;
   -- Build Statement
   vc_trans_dtl_stmt := vc_trans_dtl_update || vc_trans_dtl_where;

--insert into tb_debug(row_joe) values(vc_trans_dtl_stmt);
--commit;

   /**  Update Archived Rows with Archive Batch No.  **********************************************/
   vi_cursor_id := DBMS_SQL.open_cursor;
   DBMS_SQL.parse (vi_cursor_id, vc_trans_dtl_stmt, DBMS_SQL.v7);
   DBMS_SQL.bind_variable (vi_cursor_id, ':an_batch_id', an_batch_id);
   vi_rows_processed := DBMS_SQL.EXECUTE (vi_cursor_id);
   DBMS_SQL.close_cursor (vi_cursor_id);
   COMMIT;

   /**  Copy Archive Batch from Transaction Detail table to Archive Detail table  *****************/
   INSERT INTO TB_ARCHIVE_TRANSACTION_DTL
      SELECT *
        FROM TB_TRANSACTION_DETAIL
       WHERE archive_batch_no = an_batch_id;

   /**  Delete Archive Batch from Transaction Detail table  ***************************************/
   DELETE FROM TB_TRANSACTION_DETAIL
    WHERE archive_batch_no = an_batch_id;

   /**  Update Batch file  ************************************************************************/
   UPDATE TB_BATCH
      SET batch_status_code = 'A',
          total_rows = vi_rows_processed,
          processed_rows = vi_rows_processed,
          actual_end_timestamp = SYS_EXTRACT_UTC(SYSTIMESTAMP)
    WHERE batch_id = an_batch_id;

    COMMIT;

EXCEPTION
   WHEN e_abort THEN
      -- Update batch with error codes
      UPDATE TB_BATCH
         SET batch_status_code = v_batch_status_code,
             error_sev_code = v_error_sev_code
       WHERE batch_id = an_batch_id;
      COMMIT;
   WHEN OTHERS THEN
      -- Update batch with something else
      UPDATE TB_BATCH
         SET batch_status_code = 'A?',
             error_sev_code = 99
       WHERE batch_id = an_batch_id;
      COMMIT;
END;
/

CREATE OR REPLACE PROCEDURE Sp_Batch_Archive_Delete(an_batch_id NUMBER)
IS
-- Table defined variables
   v_batch_status_code             TB_BATCH.batch_status_code%TYPE                   := NULL;
   v_error_sev_code                TB_BATCH.error_sev_code%TYPE                      := ' ';
   v_severity_level                TB_LIST_CODE.severity_level%TYPE                  := ' ';
   v_write_import_line_flag        TB_LIST_CODE.write_import_line_flag%TYPE          := '1';
   v_abort_import_flag             TB_LIST_CODE.abort_import_flag%TYPE               := '0';
   v_sysdate                       TB_BATCH.actual_start_timestamp%TYPE              := SYS_EXTRACT_UTC(SYSTIMESTAMP);

-- Program defined variables
   vi_error_count                  INTEGER                                           := 0;
   vi_cursor_id                    INTEGER                                           := 0;
   vi_rows_processed               INTEGER                                           := 0;

-- Define Exceptions
   e_badread                       EXCEPTION;
   e_badupdate                     EXCEPTION;
   e_badwrite                      EXCEPTION;
   e_wrongdata                     EXCEPTION;
   e_abort                         EXCEPTION;

-- Program starts ******************************************************************************************
BEGIN
   -- Confirm batch exists and is flagged for processing
   BEGIN
      SELECT batch_status_code
        INTO v_batch_status_code
        FROM TB_BATCH
       WHERE batch_id = an_batch_id;
      IF SQLCODE != 0 THEN
         RAISE e_badread;
      ELSIF v_batch_status_code != 'FADR' THEN
         RAISE e_wrongdata;
      END IF;
   EXCEPTION
      WHEN NO_DATA_FOUND OR e_badread THEN
         vi_error_count := vi_error_count + 1;
         Sp_Geterrorcode('A8', an_batch_id, 'ADR', v_sysdate, v_severity_level, v_write_import_line_flag, v_abort_import_flag);
         IF v_severity_level > v_error_sev_code THEN
            v_error_sev_code := v_severity_level;
         END IF;
      WHEN e_wrongdata THEN
         vi_error_count := vi_error_count + 1;
         Sp_Geterrorcode('A9', an_batch_id, 'ADR', v_sysdate, v_severity_level, v_write_import_line_flag, v_abort_import_flag);
         IF v_severity_level > v_error_sev_code THEN
            v_error_sev_code := v_severity_level;
         END IF;
   END;
   -- Abort Procedure?
   IF v_abort_import_flag = '1' THEN
      v_batch_status_code := 'ABADR';
      RAISE e_abort;
   END IF;

   -- Update batch as processing
   BEGIN
      UPDATE TB_BATCH
         SET batch_status_code = 'XADR',
             error_sev_code = '',
             actual_start_timestamp = SYS_EXTRACT_UTC(SYSTIMESTAMP)
       WHERE batch_id = an_batch_id;
      IF SQLCODE != 0 THEN
         RAISE e_badupdate;
      END IF;
   EXCEPTION
      WHEN e_badupdate THEN
         vi_error_count := vi_error_count + 1;
         Sp_Geterrorcode('A1', an_batch_id, 'ADR', v_sysdate, v_severity_level, v_write_import_line_flag, v_abort_import_flag);
         IF v_severity_level > v_error_sev_code THEN
            v_error_sev_code := v_severity_level;
         END IF;
   END;
   -- Abort Procedure?
   IF v_abort_import_flag = '1' THEN
      v_batch_status_code := 'ABADR';
      RAISE e_abort;
   END IF;

   COMMIT;

   /**  Delete Archive Batch from Restore Transaction Detail table  *******************************/
   DELETE FROM TB_RESTORE_TRANSACTION_DTL
    WHERE archive_batch_no = an_batch_id;

   /**  Update Batch file  ************************************************************************/
   UPDATE TB_BATCH
      SET batch_status_code = 'A',
          actual_end_timestamp = SYS_EXTRACT_UTC(SYSTIMESTAMP)
    WHERE batch_id = an_batch_id;

    COMMIT;

EXCEPTION
   WHEN e_abort THEN
      -- Update batch with error codes
      UPDATE TB_BATCH
         SET batch_status_code = v_batch_status_code,
             error_sev_code = v_error_sev_code
       WHERE batch_id = an_batch_id;
      COMMIT;
END;
/

CREATE OR REPLACE PROCEDURE Sp_Batch_Archive_Restore(an_batch_id NUMBER)
IS
-- Table defined variables
   v_batch_status_code             TB_BATCH.batch_status_code%TYPE                   := NULL;
   v_error_sev_code                TB_BATCH.error_sev_code%TYPE                      := ' ';
   v_severity_level                TB_LIST_CODE.severity_level%TYPE                  := ' ';
   v_write_import_line_flag        TB_LIST_CODE.write_import_line_flag%TYPE          := '1';
   v_abort_import_flag             TB_LIST_CODE.abort_import_flag%TYPE               := '0';
   v_sysdate                       TB_BATCH.actual_start_timestamp%TYPE              := SYS_EXTRACT_UTC(SYSTIMESTAMP);

-- Program defined variables
   vi_error_count                  INTEGER                                           := 0;
   vi_cursor_id                    INTEGER                                           := 0;
   vi_rows_processed               INTEGER                                           := 0;

-- Define Exceptions
   e_badread                       EXCEPTION;
   e_badupdate                     EXCEPTION;
   e_badwrite                      EXCEPTION;
   e_wrongdata                     EXCEPTION;
   e_abort                         EXCEPTION;

-- Program starts ******************************************************************************************
BEGIN
   -- Confirm batch exists and is flagged for processing
   BEGIN
      SELECT batch_status_code
        INTO v_batch_status_code
        FROM TB_BATCH
       WHERE batch_id = an_batch_id;
      IF SQLCODE != 0 THEN
         RAISE e_badread;
      ELSIF v_batch_status_code != 'FAR' THEN
         RAISE e_wrongdata;
      END IF;
   EXCEPTION
      WHEN NO_DATA_FOUND OR e_badread THEN
         vi_error_count := vi_error_count + 1;
         Sp_Geterrorcode('A8', an_batch_id, 'AR', v_sysdate, v_severity_level, v_write_import_line_flag, v_abort_import_flag);
         IF v_severity_level > v_error_sev_code THEN
            v_error_sev_code := v_severity_level;
         END IF;
      WHEN e_wrongdata THEN
         vi_error_count := vi_error_count + 1;
         Sp_Geterrorcode('A9', an_batch_id, 'AR', v_sysdate, v_severity_level, v_write_import_line_flag, v_abort_import_flag);
         IF v_severity_level > v_error_sev_code THEN
            v_error_sev_code := v_severity_level;
         END IF;
   END;
   -- Abort Procedure?
   IF v_abort_import_flag = '1' THEN
      v_batch_status_code := 'ABAR';
      RAISE e_abort;
   END IF;

   -- Update batch as processing
   BEGIN
      UPDATE TB_BATCH
         SET batch_status_code = 'XAR',
             error_sev_code = '',
             actual_start_timestamp = SYS_EXTRACT_UTC(SYSTIMESTAMP)
       WHERE batch_id = an_batch_id;
      IF SQLCODE != 0 THEN
         RAISE e_badupdate;
      END IF;
   EXCEPTION
      WHEN e_badupdate THEN
         vi_error_count := vi_error_count + 1;
         Sp_Geterrorcode('A1', an_batch_id, 'AR', v_sysdate, v_severity_level, v_write_import_line_flag, v_abort_import_flag);
         IF v_severity_level > v_error_sev_code THEN
            v_error_sev_code := v_severity_level;
         END IF;
   END;
   -- Abort Procedure?
   IF v_abort_import_flag = '1' THEN
      v_batch_status_code := 'ABAR';
      RAISE e_abort;
   END IF;

   COMMIT;

   /**  Copy Archive Batch from Archive Trans. Detail table to Restored Trans. Detail table  ******/
   INSERT INTO TB_RESTORE_TRANSACTION_DTL
      SELECT *
        FROM TB_ARCHIVE_TRANSACTION_DTL
       WHERE archive_batch_no = an_batch_id;

   /**  Update Batch file  ************************************************************************/
   UPDATE TB_BATCH
      SET batch_status_code = 'AR',
          actual_end_timestamp = SYS_EXTRACT_UTC(SYSTIMESTAMP)
    WHERE batch_id = an_batch_id;

    COMMIT;

EXCEPTION
   WHEN e_abort THEN
      -- Update batch with error codes
      UPDATE TB_BATCH
         SET batch_status_code = v_batch_status_code,
             error_sev_code = v_error_sev_code
       WHERE batch_id = an_batch_id;
      COMMIT;
END;
/

CREATE OR REPLACE PROCEDURE Sp_Batch_Process(an_batch_id NUMBER)
/* ************************************************************************************************/
/* Object Type/Name: Stored Procedure - sp_batch_process                                          */
/* Author:           Michael B. Fuller                                                            */
/* Date:             a long time ago                                                              */
/* Description:      process a batch of transactions                                              */
/* Arguments:        an_batch_id number                                                           */
/* Returns:          None                                                                         */
/* ************************************************************************************************/
/* Maintenance History:                                                                           */
/* Scan  Author       Date     Release    Description                                  Ticket #   */
/* ----- ---------- ---------- ---------- -------------------------------------------- ---------- */
/*       M. Fuller  06/02/2006 3.3.5.0    Fix processing an allocated line - Temporary 871        */
/*       M. Fuller  01/04/2007 3.3.5.1    Fix processing an allocated line - Permenant 871        */
/* MBF01 M. Fuller                        Add Kill Process Flag                                   */
/* MBF02 M. Fuller  01/25/2007 3.3.5.1    Add Allocations                                         */
/* MBF03 M. Fuller  05/18/2007 3.3.7a.x   Add User Exit to end of procedure            888        */
/* MBF04 M. Fuller  08/13/2007 3.4.1.1    Add 5 Calc Tax Flags                         883        */
/* ************************************************************************************************/
IS
-- Define Cursor Type
   TYPE cursor_type IS REF CURSOR;

-- Location Matrix Selection SQL           // MBF04
   vc_location_matrix_select       VARCHAR2(1000) :=
      'SELECT tb_location_matrix.location_matrix_id, ' ||
             'tb_location_matrix.jurisdiction_id, ' ||
             'tb_location_matrix.override_taxtype_code, ' ||
             'tb_location_matrix.state_flag,' ||
             'tb_location_matrix.county_flag,' ||
             'tb_location_matrix.county_local_flag,' ||
             'tb_location_matrix.city_flag,' ||
             'tb_location_matrix.city_local_flag,' ||
             'tb_jurisdiction.state, ' ||
             'tb_jurisdiction.county, ' ||
             'tb_jurisdiction.city, ' ||
             'tb_jurisdiction.zip, ' ||
             'tb_jurisdiction.in_out ' ||
        'FROM tb_location_matrix, ' ||
             'tb_jurisdiction, ' ||
             'tb_bcp_transactions ' ||
      'WHERE ( tb_bcp_transactions.rowid = :v_rowid ) AND ' ||
            '( tb_location_matrix.default_flag = ''0'' AND tb_location_matrix.binary_weight > 0 ) AND ' ||
            '( tb_location_matrix.effective_date <= tb_bcp_transactions.gl_date ) AND ( tb_location_matrix.expiration_date >= tb_bcp_transactions.gl_date ) AND ' ||
            '( tb_location_matrix.jurisdiction_id = tb_jurisdiction.jurisdiction_id ) ';
   vc_location_matrix_where        VARCHAR2(4000) := '';
   vc_location_matrix_orderby      VARCHAR2(1000) :=
      'ORDER BY tb_location_matrix.binary_weight DESC, tb_location_matrix.significant_digits DESC, tb_location_matrix.effective_date DESC';
   vc_location_matrix_stmt         VARCHAR2 (6000);
   location_matrix_cursor          cursor_type;

-- Location Matrix Record TYPE
   /*TYPE location_matrix_record is RECORD (
      location_matrix_id           tb_location_matrix.location_matrix_id%TYPE,
      jurisdiction_id              tb_location_matrix.jurisdiction_id%TYPE,
      override_taxtype_code        tb_location_matrix.override_taxtype_code%TYPE,
      state_flag                   tb_location_matrix.state_flag%TYPE,
      county_flag                  tb_location_matrix.county_flag%TYPE,
      county_local_flag            tb_location_matrix.county_local_flag%TYPE,
      city_flag                    tb_location_matrix.city_flag%TYPE,
      city_local_flag              tb_location_matrix.city_local_flag%TYPE,
      state                        tb_jurisdiction.state%TYPE,
      county                       tb_jurisdiction.county%TYPE,
      city                         tb_jurisdiction.city%TYPE,
      zip                          tb_jurisdiction.zip%TYPE,
      in_out                       tb_jurisdiction.in_out%TYPE );*/
   location_matrix                 Matrix_Record_Pkg.location_matrix_record;

-- Tax Matrix Selection SQL
   vc_tax_matrix_select            VARCHAR2(2000) :=
      'SELECT tb_tax_matrix.tax_matrix_id, tb_tax_matrix.relation_sign, tb_tax_matrix.relation_amount, ' ||
             'tb_tax_matrix.then_hold_code_flag, tb_tax_matrix.else_hold_code_flag, ' ||
             'tb_tax_matrix.then_taxcode_detail_id, tb_tax_matrix.else_taxcode_detail_id, ' ||
             'tb_tax_matrix.then_cch_taxcat_code, tb_tax_matrix.then_cch_group_code, tb_tax_matrix.then_cch_item_code, ' ||
             'tb_tax_matrix.else_cch_taxcat_code, tb_tax_matrix.else_cch_group_code, tb_tax_matrix.else_cch_item_code, ' ||
             'thentaxcddtl.taxcode_state_code, thentaxcddtl.taxcode_type_code, thentaxcddtl.taxcode_code, thentaxcddtl.jurisdiction_id, thentaxcddtl.measure_type_code, thentaxcddtl.taxtype_code, ' ||
             'elsetaxcddtl.taxcode_state_code, elsetaxcddtl.taxcode_type_code, elsetaxcddtl.taxcode_code, elsetaxcddtl.jurisdiction_id, elsetaxcddtl.measure_type_code, elsetaxcddtl.taxtype_code ' ||
       'FROM tb_tax_matrix, ' ||
            'tb_taxcode_detail thentaxcddtl, ' ||
            'tb_taxcode_detail elsetaxcddtl, ' ||
            'tb_bcp_transactions ' ||
      'WHERE ( tb_bcp_transactions.rowid = :v_rowid ) AND ' ||
            '( tb_tax_matrix.default_flag = ''0'' AND tb_tax_matrix.binary_weight > 0 ) AND ' ||
            '(( tb_tax_matrix.matrix_state_code = ''*ALL'' ) OR ( tb_tax_matrix.matrix_state_code = :v_transaction_state_code )) AND ' ||
            '( tb_tax_matrix.effective_date <= tb_bcp_transactions.gl_date ) AND ( tb_tax_matrix.expiration_date >= tb_bcp_transactions.gl_date ) AND ' ||
            '( tb_tax_matrix.then_taxcode_detail_id = thentaxcddtl.taxcode_detail_id (+)) AND ' ||
            '( tb_tax_matrix.else_taxcode_detail_id = elsetaxcddtl.taxcode_detail_id (+)) ';
   vc_tax_matrix_where             VARCHAR2(6000) := '';
   vc_tax_matrix_orderby           VARCHAR2(1000) :=
      'ORDER BY tb_tax_matrix.binary_weight DESC, tb_tax_matrix.significant_digits DESC, tb_tax_matrix.effective_date DESC';
   vc_tax_matrix_stmt              VARCHAR2 (9000);
   tax_matrix_cursor               cursor_type;

-- Tax Matrix Record TYPE
   /*TYPE tax_matrix_record is RECORD (
      tax_matrix_id                tb_tax_matrix.tax_matrix_id%TYPE,
      relation_sign                tb_tax_matrix.relation_sign%TYPE,
      relation_amount              tb_tax_matrix.relation_amount%TYPE,
      then_hold_code_flag          tb_tax_matrix.then_hold_code_flag%TYPE,
      else_hold_code_flag          tb_tax_matrix.else_hold_code_flag%TYPE,
      then_taxcode_detail_id       tb_bcp_transactions.taxcode_detail_id%TYPE,
      else_taxcode_detail_id       tb_bcp_transactions.taxcode_detail_id%TYPE,
      then_cch_taxcat_code         tb_tax_matrix.then_cch_taxcat_code%TYPE,
      then_cch_group_code          tb_tax_matrix.then_cch_group_code%TYPE,
      then_cch_item_code           tb_tax_matrix.then_cch_item_code%TYPE,
      else_cch_taxcat_code         tb_tax_matrix.else_cch_taxcat_code%TYPE,
      else_cch_group_code          tb_tax_matrix.else_cch_group_code%TYPE,
      else_cch_item_code           tb_tax_matrix.else_cch_item_code%TYPE,
      then_taxcode_state_code      tb_bcp_transactions.taxcode_state_code%TYPE,
      then_taxcode_type_code       tb_bcp_transactions.taxcode_type_code%TYPE,
      then_taxcode_code            tb_bcp_transactions.taxcode_code%TYPE,
      then_jurisdiction_id         tb_bcp_transactions.jurisdiction_id%TYPE,
      then_measure_type_code       tb_bcp_transactions.measure_type_code%TYPE,
      then_taxtype_code            tb_taxcode.taxtype_code%TYPE,
      else_taxcode_state_code      tb_bcp_transactions.taxcode_state_code%TYPE,
      else_taxcode_type_code       tb_bcp_transactions.taxcode_type_code%TYPE,
      else_taxcode_code            tb_bcp_transactions.taxcode_code%TYPE,
      else_jurisdiction_id         tb_bcp_transactions.jurisdiction_id%TYPE,
      else_measure_type_code       tb_bcp_transactions.measure_type_code%TYPE,
      else_taxtype_code            tb_taxcode.taxtype_code%TYPE );*/
   tax_matrix                      Matrix_Record_Pkg.tax_matrix_record;

-- Driver Reference Insert SQL
   vc_driver_ref_insert_stmt       VARCHAR2(3000);

-- Table defined variables
   v_batch_status_code             TB_BATCH.batch_status_code%TYPE                   := 'P';
   v_starting_sequence             TB_BATCH.start_row%TYPE                           := 0;
   v_processed_rows                TB_BATCH.processed_rows%TYPE                      := 0;
   v_error_sev_code                TB_BATCH.error_sev_code%TYPE                      := ' ';
   v_severity_level                TB_LIST_CODE.severity_level%TYPE                  := ' ';
   v_write_import_line_flag        TB_LIST_CODE.write_import_line_flag%TYPE          := '1';
   v_abort_import_flag             TB_LIST_CODE.abort_import_flag%TYPE               := '0';
   v_location_abort_import_flag    TB_LIST_CODE.abort_import_flag%TYPE               := '0';
   v_tax_abort_import_flag         TB_LIST_CODE.abort_import_flag%TYPE               := '0';
   v_allocation_flag               TB_OPTION.value%TYPE                              := NULL;
   v_delete_zero_amounts           TB_OPTION.value%TYPE                              := NULL;
   v_hold_trans_flag               TB_OPTION.value%TYPE                              := NULL;
   v_hold_trans_amount             TB_OPTION.value%TYPE                              := NULL;
   v_autoproc_flag                 TB_OPTION.value%TYPE                              := NULL;
   v_autoproc_amount               TB_OPTION.value%TYPE                              := NULL;
   v_autoproc_id                   TB_OPTION.value%TYPE                              := NULL;
   v_processuserexit_flag          TB_OPTION.value%TYPE                              := NULL;
   v_autoproc_state_code           TB_TAXCODE_DETAIL.taxcode_state_code%TYPE         := NULL;
   v_autoproc_type_code            TB_TAXCODE_DETAIL.taxcode_type_code%TYPE          := NULL;
   v_autoproc_code                 TB_TAXCODE_DETAIL.taxcode_code%TYPE               := NULL;
   v_hold_code_flag                TB_TAX_MATRIX.then_hold_code_flag%TYPE            := NULL;
   v_less_hold_code_flag           TB_TAX_MATRIX.then_hold_code_flag%TYPE            := NULL;
   v_less_taxcode_detail_id        TB_TAX_MATRIX.then_taxcode_detail_id%TYPE         := NULL;
   v_less_cch_taxcat_code          TB_TAX_MATRIX.then_cch_taxcat_code%TYPE           := NULL;
   v_less_cch_group_code           TB_TAX_MATRIX.then_cch_group_code%TYPE            := NULL;
   v_equal_hold_code_flag          TB_TAX_MATRIX.then_hold_code_flag%TYPE            := NULL;
   v_equal_taxcode_detail_id       TB_TAX_MATRIX.then_taxcode_detail_id%TYPE         := NULL;
   v_equal_cch_taxcat_code         TB_TAX_MATRIX.then_cch_taxcat_code%TYPE           := NULL;
   v_equal_cch_group_code          TB_TAX_MATRIX.then_cch_group_code%TYPE            := NULL;
   v_greater_hold_code_flag        TB_TAX_MATRIX.then_hold_code_flag%TYPE            := NULL;
   v_greater_taxcode_detail_id     TB_TAX_MATRIX.then_taxcode_detail_id%TYPE         := NULL;
   v_greater_cch_taxcat_code       TB_TAX_MATRIX.then_cch_taxcat_code%TYPE           := NULL;
   v_greater_cch_group_code        TB_TAX_MATRIX.then_cch_group_code%TYPE            := NULL;
   v_jurisdiction_id               TB_JURISDICTION.jurisdiction_id%TYPE              := NULL;
   v_less_taxcode_state_code       TB_TAXCODE_DETAIL.taxcode_state_code%TYPE         := NULL;
   v_less_taxcode_type_code        TB_TAXCODE_DETAIL.taxcode_type_code%TYPE          := NULL;
   v_less_taxcode_code             TB_TAXCODE_DETAIL.taxcode_code%TYPE               := NULL;
   v_less_jurisdiction_id          TB_TAXCODE_DETAIL.jurisdiction_id%TYPE            := NULL;
   v_less_measure_type_code        TB_TAXCODE_DETAIL.measure_type_code%TYPE          := NULL;
   v_less_taxtype_code             TB_TAXCODE_DETAIL.taxtype_code%TYPE               := NULL;
   v_equal_taxcode_state_code      TB_TAXCODE_DETAIL.taxcode_state_code%TYPE         := NULL;
   v_equal_taxcode_type_code       TB_TAXCODE_DETAIL.taxcode_type_code%TYPE          := NULL;
   v_equal_taxcode_code            TB_TAXCODE_DETAIL.taxcode_code%TYPE               := NULL;
   v_equal_jurisdiction_id         TB_TAXCODE_DETAIL.jurisdiction_id%TYPE            := NULL;
   v_equal_measure_type_code       TB_TAXCODE_DETAIL.measure_type_code%TYPE          := NULL;
   v_equal_taxtype_code            TB_TAXCODE_DETAIL.taxtype_code%TYPE               := NULL;
   v_greater_taxcode_state_code    TB_TAXCODE_DETAIL.taxcode_state_code%TYPE         := NULL;
   v_greater_taxcode_type_code     TB_TAXCODE_DETAIL.taxcode_type_code%TYPE          := NULL;
   v_greater_taxcode_code          TB_TAXCODE_DETAIL.taxcode_code%TYPE               := NULL;
   v_greater_jurisdiction_id       TB_TAXCODE_DETAIL.jurisdiction_id%TYPE            := NULL;
   v_greater_measure_type_code     TB_TAXCODE_DETAIL.measure_type_code%TYPE          := NULL;
   v_greater_taxtype_code          TB_TAXCODE_DETAIL.taxtype_code%TYPE               := NULL;
   v_gl_date                       TB_BCP_TRANSACTIONS.gl_date%TYPE                  := NULL;
   v_transaction_state_code        TB_BCP_TRANSACTIONS.transaction_state_code%TYPE   := NULL;
   v_override_jurisdiction_id      TB_BCP_TRANSACTIONS.jurisdiction_id%TYPE          := NULL;
   v_sysdate                       TB_BCP_TRANSACTIONS.load_timestamp%TYPE           := SYS_EXTRACT_UTC(SYSTIMESTAMP);
   v_transaction_amt               TB_BCP_TRANSACTIONS.gl_line_itm_dist_amt%TYPE     := 0;
   v_kill_proc_flag                TB_OPTION.value%TYPE                              := NULL;
   v_kill_proc_count               TB_OPTION.value%TYPE                              := NULL;
   v_taxtype_code                  TB_TAXCODE.taxtype_code%TYPE                      := NULL;
   v_taxtype_used                  TB_TAXCODE.taxtype_code%TYPE                      := NULL;

-- Program defined variables
   v_rowid                         ROWID                                             := NULL;
   --vi_cursor_id                    INTEGER                                           := 0;
   --vi_rows_processed               INTEGER                                           := 0;
   vn_hold_trans_amount            NUMBER                                            := 0;
   vn_autoproc_amount              NUMBER                                            := 0;
   vn_autoproc_id                  NUMBER                                            := 0;
   vn_fetch_rows                   NUMBER                                            := 0;
   vi_error_count                  INTEGER                                           := 0;
   vn_actual_rowno                 NUMBER                                            := 0;
   --vc_driver_ref_ins_stmt          VARCHAR2(5000)                                    := NULL;
   vc_state_driver_flag            CHAR(1)                                           := '0';
   vn_kill_proc_count              NUMBER                                            := 0;
   vn_kill_proc_counter            NUMBER                                            := 0;
   vc_batch_status_code            VARCHAR2(10)                                      := NULL;
   vn_trans_created                NUMBER                                            := 0;
   vn_return                       NUMBER                                            := 0;
-- temporary
   vn_taxable_amt                  NUMBER                                            := 0;

/*-- Define Location Driver Names Cursor (tb_driver_names)
   CURSOR location_driver_cursor
   IS
        SELECT tb_driver_names.trans_dtl_column_name, tb_driver_names.matrix_column_name, tb_driver_names.null_driver_flag, tb_driver_names.wildcard_flag,
               datadefcol.desc_column_name, datadefcol.data_type
          FROM tb_driver_names,
               (SELECT *
                  FROM tb_data_def_column
                 WHERE table_name = 'TB_TRANSACTION_DETAIL') datadefcol
         WHERE tb_driver_names.driver_names_code = 'L' AND
               tb_driver_names.trans_dtl_column_name = datadefcol.column_name
      ORDER BY tb_driver_names.driver_id;
      location_driver              location_driver_cursor%ROWTYPE;*/

/*-- Define Tax Driver Names Cursor (tb_driver_names)
   CURSOR tax_driver_cursor
   IS
        SELECT tb_driver_names.trans_dtl_column_name, tb_driver_names.matrix_column_name, tb_driver_names.null_driver_flag, tb_driver_names.wildcard_flag, tb_driver_names.range_flag, tb_driver_names.to_number_flag,
               datadefcol.desc_column_name, datadefcol.data_type
          FROM tb_driver_names,
               (SELECT *
                  FROM tb_data_def_column
                 WHERE table_name = 'TB_TRANSACTION_DETAIL') datadefcol
         WHERE tb_driver_names.driver_names_code = 'T' AND
               tb_driver_names.trans_dtl_column_name = datadefcol.column_name
      ORDER BY tb_driver_names.driver_id;
      tax_driver                   tax_driver_cursor%ROWTYPE;*/

-- Define BCP Transactions Cursor (tb_bcp_transactions)
   -- 3411 - add taxable amount and tax type used.
   CURSOR bcp_transactions_cursor
   IS
      SELECT ROWID,
             transaction_detail_id,
             source_transaction_id,
             process_batch_no,
             gl_extract_batch_no,
             archive_batch_no,
             allocation_matrix_id,
             allocation_subtrans_id,
             entered_date,
             transaction_status,
             gl_date,
             gl_company_nbr,
             gl_company_name,
             gl_division_nbr,
             gl_division_name,
             gl_cc_nbr_dept_id,
             gl_cc_nbr_dept_name,
             gl_local_acct_nbr,
             gl_local_acct_name,
             gl_local_sub_acct_nbr,
             gl_local_sub_acct_name,
             gl_full_acct_nbr,
             gl_full_acct_name,
             gl_line_itm_dist_amt,
             orig_gl_line_itm_dist_amt,
             vendor_nbr,
             vendor_name,
             vendor_address_line_1,
             vendor_address_line_2,
             vendor_address_line_3,
             vendor_address_line_4,
             vendor_address_city,
             vendor_address_county,
             vendor_address_state,
             vendor_address_zip,
             vendor_address_country,
             vendor_type,
             vendor_type_name,
             invoice_nbr,
             invoice_desc,
             invoice_date,
             invoice_freight_amt,
             invoice_discount_amt,
             invoice_tax_amt,
             invoice_total_amt,
             invoice_tax_flg,
             invoice_line_nbr,
             invoice_line_name,
             invoice_line_type,
             invoice_line_type_name,
             invoice_line_amt,
             invoice_line_tax,
             afe_project_nbr,
             afe_project_name,
             afe_category_nbr,
             afe_category_name,
             afe_sub_cat_nbr,
             afe_sub_cat_name,
             afe_use,
             afe_contract_type,
             afe_contract_structure,
             afe_property_cat,
             inventory_nbr,
             inventory_name,
             inventory_class,
             inventory_class_name,
             po_nbr,
             po_name,
             po_date,
             po_line_nbr,
             po_line_name,
             po_line_type,
             po_line_type_name,
             ship_to_location,
             ship_to_location_name,
             ship_to_address_line_1,
             ship_to_address_line_2,
             ship_to_address_line_3,
             ship_to_address_line_4,
             ship_to_address_city,
             ship_to_address_county,
             ship_to_address_state,
             ship_to_address_zip,
             ship_to_address_country,
             wo_nbr,
             wo_name,
             wo_date,
             wo_type,
             wo_type_desc,
             wo_class,
             wo_class_desc,
             wo_entity,
             wo_entity_desc,
             wo_line_nbr,
             wo_line_name,
             wo_line_type,
             wo_line_type_desc,
             wo_shut_down_cd,
             wo_shut_down_cd_desc,
             voucher_id,
             voucher_name,
             voucher_date,
             voucher_line_nbr,
             voucher_line_desc,
             check_nbr,
             check_no,
             check_date,
             check_amt,
             check_desc,
             user_text_01,
             user_text_02,
             user_text_03,
             user_text_04,
             user_text_05,
             user_text_06,
             user_text_07,
             user_text_08,
             user_text_09,
             user_text_10,
             user_text_11,
             user_text_12,
             user_text_13,
             user_text_14,
             user_text_15,
             user_text_16,
             user_text_17,
             user_text_18,
             user_text_19,
             user_text_20,
             user_text_21,
             user_text_22,
             user_text_23,
             user_text_24,
             user_text_25,
             user_text_26,
             user_text_27,
             user_text_28,
             user_text_29,
             user_text_30,
             user_number_01,
             user_number_02,
             user_number_03,
             user_number_04,
             user_number_05,
             user_number_06,
             user_number_07,
             user_number_08,
             user_number_09,
             user_number_10,
             user_date_01,
             user_date_02,
             user_date_03,
             user_date_04,
             user_date_05,
             user_date_06,
             user_date_07,
             user_date_08,
             user_date_09,
             user_date_10,
             comments,
             tb_calc_tax_amt,
             state_use_amount,
             state_use_tier2_amount,
             state_use_tier3_amount,
             county_use_amount,
             county_local_use_amount,
             city_use_amount,
             city_local_use_amount,
             transaction_state_code,
             auto_transaction_state_code,
             transaction_ind,
             suspend_ind,
             taxcode_detail_id,
             taxcode_state_code,
             taxcode_type_code,
             taxcode_code,
             cch_taxcat_code,
             cch_group_code,
             cch_item_code,
             manual_taxcode_ind,
             tax_matrix_id,
             location_matrix_id,
             jurisdiction_id,
             jurisdiction_taxrate_id,
             manual_jurisdiction_ind,
             measure_type_code,
             state_use_rate,
             state_use_tier2_rate,
             state_use_tier3_rate,
             state_split_amount,
             state_tier2_min_amount,
             state_tier2_max_amount,
             state_maxtax_amount,
             county_use_rate,
             county_local_use_rate,
             county_split_amount,
             county_maxtax_amount,
             county_single_flag,
             county_default_flag,
             city_use_rate,
             city_local_use_rate,
             city_split_amount,
             city_split_use_rate,
             city_single_flag,
             city_default_flag,
             combined_use_rate,
             load_timestamp,
             gl_extract_updater,
             gl_extract_timestamp,
             gl_extract_flag,
             gl_log_flag,
             gl_extract_amt,
             audit_flag,
             audit_user_id,
             audit_timestamp,
             modify_user_id,
             modify_timestamp,
             update_user_id,
             update_timestamp
        FROM TB_BCP_TRANSACTIONS
       WHERE process_batch_no = an_batch_id
         AND (( transaction_detail_id IS NULL ) OR ( transaction_detail_id <> -1 ));
      bcp_transactions             bcp_transactions_cursor%ROWTYPE;

-- Define Exceptions
   e_badread                       EXCEPTION;
   e_badupdate                     EXCEPTION;
   e_badwrite                      EXCEPTION;
   e_wrongdata                     EXCEPTION;
   e_abort                         EXCEPTION;
   e_kill                          EXCEPTION;

-- ****** Program starts ****** --------------------------------------------------------------------
BEGIN
   -- Confirm batch exists and is flagged for processing
   BEGIN
      SELECT batch_status_code
        INTO v_batch_status_code
        FROM TB_BATCH
       WHERE batch_id = an_batch_id;
      IF SQLCODE != 0 THEN
         RAISE e_badread;
      ELSIF v_batch_status_code != 'FP' THEN
         RAISE e_wrongdata;
      END IF;
   EXCEPTION
      WHEN NO_DATA_FOUND OR e_badread THEN
         vi_error_count := vi_error_count + 1;
         Sp_Geterrorcode('P8', an_batch_id, 'P', v_sysdate, v_severity_level, v_write_import_line_flag, v_abort_import_flag);
         IF v_severity_level > v_error_sev_code THEN
            v_error_sev_code := v_severity_level;
         END IF;
      WHEN e_wrongdata THEN
         vi_error_count := vi_error_count + 1;
         Sp_Geterrorcode('P9', an_batch_id, 'P', v_sysdate, v_severity_level, v_write_import_line_flag, v_abort_import_flag);
         IF v_severity_level > v_error_sev_code THEN
            v_error_sev_code := v_severity_level;
         END IF;
   END;
   -- Abort Procedure?
   IF v_abort_import_flag = '1' THEN
      v_batch_status_code := 'ABP';
      RAISE e_abort;
   END IF;

   -- RUN ALLOCATIONS??
   -- Determine Allocation Flag -- 01/25/2007 -- MBF02
   BEGIN
      SELECT LTRIM(RTRIM(value))
        INTO v_allocation_flag
        FROM TB_OPTION
       WHERE option_code = 'ALLOCATIONSENABLED' AND
             option_type_code = 'ADMIN' AND
             user_code = 'ADMIN';
   EXCEPTION
      WHEN OTHERS THEN
         v_allocation_flag := '0';
   END;
   IF v_allocation_flag IS NULL THEN
      v_allocation_flag := '0';
   END IF;
   -- Determine Allocation Flag -- 01/25/2007 -- MBF02

   -- Perform Allocations -- 01/25/2007 -- MBF02
   IF v_allocation_flag = '1' THEN
      Sp_Import_Allocation(an_batch_id);
   END IF;
   -- Perform Allocations -- 01/25/2007 -- MBF02

   -- Get starting sequence no.
   BEGIN
      SELECT last_number
        INTO v_starting_sequence
        FROM user_sequences
       WHERE sequence_name = 'SQ_TB_TRANSACTION_DETAIL_ID';
   EXCEPTION
      WHEN OTHERS THEN
         NULL;
   END;

   -- Update batch
   BEGIN
      UPDATE TB_BATCH
         SET batch_status_code = 'XP',
             error_sev_code = '',
             start_row = v_starting_sequence,
             actual_start_timestamp = SYS_EXTRACT_UTC(SYSTIMESTAMP)
       WHERE batch_id = an_batch_id;
      IF SQLCODE != 0 THEN
         RAISE e_badupdate;
      END IF;
   EXCEPTION
      WHEN e_badupdate THEN
         vi_error_count := vi_error_count + 1;
         Sp_Geterrorcode('P1', an_batch_id, 'P', v_sysdate, v_severity_level, v_write_import_line_flag, v_abort_import_flag);
         IF v_severity_level > v_error_sev_code THEN
            v_error_sev_code := v_severity_level;
         END IF;
   END;
   -- Abort Procedure?
   IF v_abort_import_flag = '1' THEN
      v_batch_status_code := 'ABP';
      RAISE e_abort;
   END IF;

   -- Determine Global Delete Zero Amounts Flag
   BEGIN
      SELECT LTRIM(RTRIM(value))
        INTO v_delete_zero_amounts
        FROM TB_OPTION
       WHERE option_code = 'DELETE0AMTFLAG' AND
             option_type_code = 'SYSTEM' AND
             user_code = 'SYSTEM';
   EXCEPTION
      WHEN OTHERS THEN
         v_delete_zero_amounts := '0';
   END;
   IF v_delete_zero_amounts IS NULL THEN
      v_delete_zero_amounts := '0';
   END IF;

   -- Determine Global Hold Transactions Flag and Amount
   BEGIN
      SELECT LTRIM(RTRIM(value))
        INTO v_hold_trans_flag
        FROM TB_OPTION
       WHERE option_code = 'HOLDTRANSFLAG' AND
             option_type_code = 'SYSTEM' AND
             user_code = 'SYSTEM';
   EXCEPTION
      WHEN OTHERS THEN
         v_hold_trans_flag := '0';
   END;
   IF v_hold_trans_flag IS NULL THEN
      v_hold_trans_flag := '0';
   END IF;
   IF v_hold_trans_flag = '1' THEN
      BEGIN
         SELECT LTRIM(RTRIM(value))
           INTO v_hold_trans_amount
           FROM TB_OPTION
          WHERE option_code = 'HOLDTRANSAMT' AND
                option_type_code = 'SYSTEM' AND
                user_code = 'SYSTEM';
      EXCEPTION
         WHEN OTHERS THEN
            v_hold_trans_amount := '0';
      END;
      IF v_hold_trans_amount IS NULL THEN
         v_hold_trans_amount := '0';
      END IF;
      vn_hold_trans_amount := TO_NUMBER(v_hold_trans_amount);
   END IF;

   -- Determine "auto-process" Flag, Amount, and TaxCode Detail ID
   BEGIN
      SELECT LTRIM(RTRIM(value))
        INTO v_autoproc_flag
        FROM TB_OPTION
       WHERE option_code = 'AUTOPROCFLAG' AND
             option_type_code = 'SYSTEM' AND
             user_code = 'SYSTEM';
   EXCEPTION
      WHEN OTHERS THEN
         v_hold_trans_flag := '0';
   END;
   IF v_autoproc_flag IS NULL THEN
      v_autoproc_flag := '0';
   END IF;

   IF v_autoproc_flag = '1' THEN
      BEGIN
         SELECT LTRIM(RTRIM(value))
           INTO v_autoproc_amount
           FROM TB_OPTION
          WHERE option_code = 'AUTOPROCAMT' AND
                option_type_code = 'SYSTEM' AND
                user_code = 'SYSTEM';
      EXCEPTION
         WHEN OTHERS THEN
            v_autoproc_amount := '0';
      END;
      IF v_autoproc_amount IS NULL THEN
         v_autoproc_amount := '0';
      END IF;
      vn_autoproc_amount := TO_NUMBER(v_autoproc_amount);

      BEGIN
         SELECT LTRIM(RTRIM(value))
           INTO v_autoproc_id
           FROM TB_OPTION
          WHERE option_code = 'AUTOPROCID' AND
                option_type_code = 'SYSTEM' AND
                user_code = 'SYSTEM';
      EXCEPTION
         WHEN OTHERS THEN
            v_autoproc_id := '0';
      END;
      IF v_autoproc_id IS NULL THEN
         v_autoproc_id := '0';
      END IF;
      vn_autoproc_id := TO_NUMBER(v_autoproc_id);

      BEGIN
         SELECT taxcode_state_code, taxcode_type_code, taxcode_code
           INTO v_autoproc_state_code, v_autoproc_type_code, v_autoproc_code
           FROM TB_TAXCODE_DETAIL
          WHERE taxcode_detail_id = vn_autoproc_id;
      EXCEPTION
         WHEN OTHERS THEN
            v_autoproc_flag := '0';
      END;
      IF v_autoproc_state_code IS NULL OR v_autoproc_type_code IS NULL OR v_autoproc_code IS NULL THEN
         v_autoproc_flag := '0';
      END IF;
   END IF;

   -- Determine Kill Process Flag/Count -- MBF01 -- begin
   BEGIN
      SELECT LTRIM(RTRIM(value))
        INTO v_kill_proc_flag
        FROM TB_OPTION
       WHERE option_code = 'KILLPROCFLAG' AND
             option_type_code = 'SYSTEM' AND
             user_code = 'SYSTEM';
   EXCEPTION
      WHEN OTHERS THEN
         v_kill_proc_flag := '0';
   END;
   IF v_kill_proc_flag IS NULL THEN
      v_kill_proc_flag := '0';
   END IF;
   IF v_kill_proc_flag = '1' THEN
      BEGIN
         SELECT LTRIM(RTRIM(value))
           INTO v_kill_proc_count
           FROM TB_OPTION
          WHERE option_code = 'KILLPROCCOUNT' AND
                option_type_code = 'SYSTEM' AND
                user_code = 'SYSTEM';
      EXCEPTION
         WHEN OTHERS THEN
            v_kill_proc_count := '5000';
      END;
      IF v_kill_proc_count IS NULL THEN
         v_kill_proc_count := '5000';
      END IF;
      vn_kill_proc_count := TO_NUMBER(v_kill_proc_count);
   END IF;
   -- Determine Kill Process Flag/Count -- MBF01 -- end

   -- ***** Create Dynamic WHERE Clause for Matrix SQLs ***** --
   -- Location Matrix Drivers
/* OPEN location_driver_cursor;
   LOOP
      FETCH location_driver_cursor INTO location_driver;
      EXIT WHEN location_driver_cursor%NOTFOUND;
      IF location_driver.null_driver_flag = '1' THEN
         IF location_driver.wildcard_flag = '1' THEN
            IF UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'CHAR' OR
               UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'VARC' THEN
                  vc_location_matrix_where := vc_location_matrix_where || 'AND ' ||
                  '((tb_bcp_transactions.' || location_driver.trans_dtl_column_name || ' IS NULL AND (tb_location_matrix.' || location_driver.matrix_column_name || ' = ''*ALL'' OR tb_location_matrix.' || location_driver.matrix_column_name || ' = ''*NULL'' )) OR ' ||
                  '(tb_bcp_transactions.' || location_driver.trans_dtl_column_name || ' IS NOT NULL AND (tb_location_matrix.' || location_driver.matrix_column_name || ' = ''*ALL'' OR tb_bcp_transactions.' || location_driver.trans_dtl_column_name || ' LIKE tb_location_matrix.' || location_driver.matrix_column_name || '))) ';
            END IF;
         ELSE
            IF UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'CHAR' OR
               UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'VARC' THEN
                  vc_location_matrix_where := vc_location_matrix_where || 'AND ' ||
                  '((tb_bcp_transactions.' || location_driver.trans_dtl_column_name || ' IS NULL AND (tb_location_matrix.' || location_driver.matrix_column_name || ' = ''*ALL'' OR tb_location_matrix.' || location_driver.matrix_column_name || ' = ''*NULL'' )) OR ' ||
                  '(tb_bcp_transactions.' || location_driver.trans_dtl_column_name || ' IS NOT NULL AND (tb_location_matrix.' || location_driver.matrix_column_name || ' = ''*ALL'' OR tb_bcp_transactions.' || location_driver.trans_dtl_column_name || ' = tb_location_matrix.' || location_driver.matrix_column_name || '))) ';
            END IF;
         END IF;
      ELSE
         IF location_driver.wildcard_flag = '1' THEN
            IF UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'CHAR' OR
               UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'VARC' THEN
                  vc_location_matrix_where := vc_location_matrix_where || 'AND ' ||
                  '(tb_location_matrix.' || location_driver.matrix_column_name || ' = ''*ALL'' OR tb_bcp_transactions.' || location_driver.trans_dtl_column_name || ' LIKE tb_location_matrix.' || location_driver.matrix_column_name || ') ';
            END IF;
         ELSE
            IF UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'CHAR' OR
               UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'VARC' THEN
                  vc_location_matrix_where := vc_location_matrix_where || 'AND ' ||
                  '(tb_location_matrix.' || location_driver.matrix_column_name || ' = ''*ALL'' OR tb_bcp_transactions.' || location_driver.trans_dtl_column_name || ' = tb_location_matrix.' || location_driver.matrix_column_name || ') ';
            END IF;
         END IF;
      END IF;
      -- Insert Driver Reference Values for this driver
      -- Create Insert SQL
      vc_driver_ref_ins_stmt :=
         'INSERT INTO tb_driver_reference (trans_dtl_column_name, driver_value, driver_description) ' ||
            'SELECT ''' || Upper(location_driver.trans_dtl_column_name) ||
                    ''', ' || location_driver.trans_dtl_column_name || ', ';
      IF location_driver.desc_column_name IS NOT NULL THEN
         vc_driver_ref_ins_stmt := vc_driver_ref_ins_stmt || 'min(' || location_driver.desc_column_name || ') ';
      ELSE
         vc_driver_ref_ins_stmt := vc_driver_ref_ins_stmt || 'NULL ';
      END IF;
      vc_driver_ref_ins_stmt := vc_driver_ref_ins_stmt ||
              'FROM tb_bcp_transactions ' ||
             'WHERE process_batch_no = ' || TO_CHAR(an_batch_id) || ' AND ' ||
                    location_driver.trans_dtl_column_name || ' IS NOT NULL AND ' ||
                    location_driver.trans_dtl_column_name || ' NOT IN ' ||
               '(SELECT driver_value ' ||
                  'FROM tb_driver_reference ' ||
                 'WHERE trans_dtl_column_name = ''' || Upper(location_driver.trans_dtl_column_name) || ''') ' ||
              'GROUP BY ''' || Upper(location_driver.trans_dtl_column_name) ||
                        ''', ' || location_driver.trans_dtl_column_name;
      -- Execute Insert SQL
      vi_cursor_id := DBMS_SQL.open_cursor;
      DBMS_SQL.parse (vi_cursor_id, vc_driver_ref_ins_stmt, DBMS_SQL.v7);
      vi_rows_processed := DBMS_SQL.execute (vi_cursor_id);
      DBMS_SQL.close_cursor (vi_cursor_id);
   END LOOP;
   CLOSE location_driver_cursor;*/

   -- New call to sp_gen_location_driver for MidTier Cleanup
   Sp_Gen_Location_Driver (
	p_generate_driver_reference	=> 'Y',
	p_an_batch_id			=> an_batch_id,
	p_transaction_table_name	=> 'tb_bcp_transactions',
	p_location_table_name		=> 'tb_location_matrix',
	p_vc_location_matrix_where	=> vc_location_matrix_where);

   -- If no drivers found raise error, else create location matrix sql statement
   IF vc_location_matrix_where IS NULL THEN
      vi_error_count := vi_error_count + 1;
      Sp_Geterrorcode('P2', an_batch_id, 'P', v_sysdate, v_severity_level, v_write_import_line_flag, v_abort_import_flag);
      IF v_severity_level > v_error_sev_code THEN
         v_error_sev_code := v_severity_level;
      END IF;
   ELSE
      vc_location_matrix_stmt := vc_location_matrix_select || vc_location_matrix_where || vc_location_matrix_orderby;
   END IF;

   -- Tax Matrix Drivers
   /*OPEN tax_driver_cursor;
   LOOP
      FETCH tax_driver_cursor INTO tax_driver;
      EXIT WHEN tax_driver_cursor%NOTFOUND;
      IF tax_driver.range_flag = '1' THEN
         IF tax_driver.to_number_flag = '1' THEN
            IF tax_driver.null_driver_flag = '1' THEN
               -- Range and ToNumber and NULL
               IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                  UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                  vc_tax_matrix_where := vc_tax_matrix_where || 'AND ( ' ||
                  '( UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU) = ''*NA'' AND ' ||
                     '( tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ' IS NULL AND ( UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') = ''*ALL'' OR UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') = ''*NULL'' )) OR ' ||
                     '( tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND ( UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') = ''*ALL'' OR DECODE(isnumber(tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || '),1,to_char(tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ',''00000000000000000000.00000000000000000000''),UPPER(tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ')) = DECODE(isnumber(UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ')),1,to_char(UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '),''00000000000000000000.00000000000000000000''),UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') ))) OR ' ||
                  '( UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU) <> ''*NA'' AND ' ||
                     '( tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND (DECODE(isnumber(tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || '),1,to_char(tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ',''00000000000000000000.00000000000000000000''),UPPER(tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ')) >= DECODE(isnumber(UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ')),1,to_char(UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '),''00000000000000000000.00000000000000000000''),UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') ) AND ' ||
                                                                                                       'DECODE(isnumber(tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || '),1,to_char(tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ',''00000000000000000000.00000000000000000000''),UPPER(tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ')) <= DECODE(isnumber(UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU)),1,to_char(UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU),''00000000000000000000.00000000000000000000''),UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU) ))))) ';
               END IF;
            ELSE
               -- Range and ToNumber and NOT NULL
               IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                  UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                  vc_tax_matrix_where := vc_tax_matrix_where || 'AND ( ' ||
                  '( UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU) = ''*NA'' AND (UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') = ''*ALL'' OR DECODE(isnumber(tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || '),1,to_char(tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ',''00000000000000000000.00000000000000000000''),UPPER(tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ')) = DECODE(isnumber(UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ')),1,to_char(UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '),''00000000000000000000.00000000000000000000''),UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ')))) OR ' ||
                  '( UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU) <> ''*NA'' AND (DECODE(isnumber(tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || '),1,to_char(tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ',''00000000000000000000.00000000000000000000''),UPPER(tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ')) >= DECODE(isnumber(UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ')),1,to_char(UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '),''00000000000000000000.00000000000000000000''),UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ')) AND ' ||
                                                                                        'DECODE(isnumber(tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || '),1,to_char(tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ',''00000000000000000000.00000000000000000000''),UPPER(tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ')) <= DECODE(isnumber(UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU )),1,to_char(UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU),''00000000000000000000.00000000000000000000''),UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU) ))) ';
               END IF;
            END IF;
         ELSE
            IF tax_driver.null_driver_flag = '1' THEN
               IF tax_driver.wildcard_flag = '1' THEN
                  -- Range and NOT ToNumber and NULL and Wildcard
                  IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                     UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                     vc_tax_matrix_where := vc_tax_matrix_where || 'AND ( ' ||
                     '( UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU) = ''*NA'' AND ' ||
                        '( tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ' IS NULL AND (UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') = ''*ALL'' OR UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') = ''*NULL'' )) OR ' ||
                        '( tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND (UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') = ''*ALL'' OR UPPER(tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ') LIKE UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ')))) OR ' ||
                     '( UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU) <> ''*NA'' AND (tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND (UPPER(tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ') >= UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') AND UPPER(tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ') <= UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU) ))) ';
                  END IF;
               ELSE
                  -- Range and NOT ToNumber and NULL and NOT Wildcard
                  IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                     UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                     vc_tax_matrix_where := vc_tax_matrix_where || 'AND ( ' ||
                     '( UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU) = ''*NA'' AND ' ||
                        '( tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ' IS NULL AND (UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') = ''*ALL'' OR UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') = ''*NULL'' )) OR ' ||
                        '( tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND (UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') = ''*ALL'' OR UPPER(tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ') = UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ')))) OR ' ||
                     '( UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU) <> ''*NA'' AND (tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND (UPPER(tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ') >= UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') AND UPPER(tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ') <= UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU) ))) ';
                  END IF;
               END IF;
            ELSE
               IF tax_driver.wildcard_flag = '1' THEN
                  -- Range and NOT ToNumber and NOT NULL and Wildcard
                  IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                     UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                     vc_tax_matrix_where := vc_tax_matrix_where || 'AND ( ' ||
                     '( UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU) = ''*NA'' AND ' ||
                        '(UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') = ''*ALL'' OR UPPER(tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ') LIKE UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '))) OR ' ||
                     '( UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU) <> ''*NA'' AND (UPPER(tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ') >= UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') AND UPPER(tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ') <= UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU) )) ';
                  END IF;
               ELSE
                  -- Range and NOT ToNumber and NOT NULL and NOT Wildcard
                  IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                     UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                     vc_tax_matrix_where := vc_tax_matrix_where || 'AND ( ' ||
                     '( UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU) = ''*NA'' AND ' ||
                        '(UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') = ''*ALL'' OR UPPER(tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ') = UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '))) OR ' ||
                     '( UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU) <> ''*NA'' AND (UPPER(tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ') >= UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') AND UPPER(tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ') <= UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU) )) ';
                  END IF;
               END IF;
            END IF;
         END IF;
      ELSE
         IF tax_driver.null_driver_flag = '1' THEN
            IF tax_driver.wildcard_flag = '1' THEN
               -- NOT Range and NULL and Wildcard
               IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                  UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                     vc_tax_matrix_where := vc_tax_matrix_where || 'AND ( ' ||
                     '(( tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ' IS NULL AND (UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') = ''*ALL'' OR UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') = ''*NULL'' )) OR ' ||
                     '( tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND (UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') = ''*ALL'' OR UPPER(tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ') LIKE UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ')))) ';
               END IF;
            ELSE
               -- NOT Range and NULL and NOT Wildcard
               IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                  UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                     vc_tax_matrix_where := vc_tax_matrix_where || 'AND ( ' ||
                     '(( tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ' IS NULL AND (UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') = ''*ALL'' OR UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') = ''*NULL'' )) OR ' ||
                     '( tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND (UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') = ''*ALL'' OR UPPER(tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ') = UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ')))) ';
               END IF;
            END IF;
         ELSE
            IF tax_driver.wildcard_flag = '1' THEN
               -- NOT Range and NOT NULL and Wildcard
               IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                  UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                     vc_tax_matrix_where := vc_tax_matrix_where || 'AND ( ' ||
                     '( UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') = ''*ALL'' OR UPPER(tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ') LIKE UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ')) ';
               END IF;
            ELSE
               -- NOT Range and NOT NULL and NOT Wildcard
               IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                  UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                  IF tax_driver.trans_dtl_column_name = 'TRANSACTION_STATE_CODE' THEN
                     IF vc_state_driver_flag <> '1' THEN
                        vc_state_driver_flag := '1';
                     END IF;
                     vc_tax_matrix_where := vc_tax_matrix_where || 'AND ( ' ||
                     '( UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') = ''*ALL'' OR UPPER(:v_transaction_state_code) = UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ')) ';
                  ELSE
                     vc_tax_matrix_where := vc_tax_matrix_where || 'AND ( ' ||
                     '( UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') = ''*ALL'' OR UPPER(tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ') = UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ')) ';
                  END IF;
               END IF;
            END IF;
         END IF;
      END IF;
      vc_tax_matrix_where := vc_tax_matrix_where || ') ';
      -- Insert Driver Reference Values for this driver
      -- Create Insert SQL
      vc_driver_ref_ins_stmt :=
         'INSERT INTO tb_driver_reference (trans_dtl_column_name, driver_value, driver_description) ' ||
            'SELECT ''' || Upper(tax_driver.trans_dtl_column_name) ||
                    ''', ' || tax_driver.trans_dtl_column_name || ', ';
      IF tax_driver.desc_column_name IS NOT NULL THEN
         vc_driver_ref_ins_stmt := vc_driver_ref_ins_stmt || 'min(' || tax_driver.desc_column_name || ') ';
      ELSE
         vc_driver_ref_ins_stmt := vc_driver_ref_ins_stmt || 'NULL ';
      END IF;
      vc_driver_ref_ins_stmt := vc_driver_ref_ins_stmt ||
              'FROM tb_bcp_transactions ' ||
             'WHERE process_batch_no = ' || TO_CHAR(an_batch_id) || ' AND ' ||
                    tax_driver.trans_dtl_column_name || ' IS NOT NULL AND ' ||
                    tax_driver.trans_dtl_column_name || ' NOT IN ' ||
               '(SELECT driver_value ' ||
                  'FROM tb_driver_reference ' ||
                 'WHERE trans_dtl_column_name = ''' || Upper(tax_driver.trans_dtl_column_name) || ''') ' ||
              'GROUP BY ''' || Upper(tax_driver.trans_dtl_column_name) ||
                        ''', ' || tax_driver.trans_dtl_column_name;
      -- Execute Insert SQL
      vi_cursor_id := DBMS_SQL.open_cursor;
      DBMS_SQL.parse (vi_cursor_id, vc_driver_ref_ins_stmt, DBMS_SQL.v7);
      vi_rows_processed := DBMS_SQL.execute (vi_cursor_id);
      DBMS_SQL.close_cursor (vi_cursor_id);
   END LOOP;
   CLOSE tax_driver_cursor;*/

   -- New call to sp_gen_tax_driver for MidTier Cleanup
   Sp_Gen_Tax_Driver (
	p_generate_driver_reference	=> 'Y',
	p_an_batch_id			=> an_batch_id,
	p_transaction_table_name	=> 'tb_bcp_transactions',
	p_tax_table_name		=> 'tb_tax_matrix',
	p_vc_state_driver_flag		=> vc_state_driver_flag,
	p_vc_tax_matrix_where		=> vc_tax_matrix_where);

   -- If no drivers found raise error, else create location matrix sql statement
   IF vc_tax_matrix_where IS NULL THEN
      vi_error_count := vi_error_count + 1;
      Sp_Geterrorcode('P3', an_batch_id, 'P', v_sysdate, v_severity_level, v_write_import_line_flag, v_abort_import_flag);
      IF v_severity_level > v_error_sev_code THEN
         v_error_sev_code := v_severity_level;
      END IF;
   ELSE
      vc_tax_matrix_stmt := vc_tax_matrix_select || vc_tax_matrix_where || vc_tax_matrix_orderby;
   END IF;
   -- Abort Procedure?
   IF v_location_abort_import_flag = '1' OR v_tax_abort_import_flag = '1' THEN
      v_batch_status_code := 'ABP';
      RAISE e_abort;
   END IF;

   -- Set large rollback segment
   COMMIT;

   -- ****** Read and Process Transactions ****** -----------------------------------------
   OPEN bcp_transactions_cursor;
   LOOP
      FETCH bcp_transactions_cursor INTO bcp_transactions;
      EXIT WHEN bcp_transactions_cursor%NOTFOUND;
      vn_actual_rowno := vn_actual_rowno + 1;
      v_write_import_line_flag := '1';

      -- Skip zero amounts?
      IF (v_delete_zero_amounts = '1' AND bcp_transactions.gl_line_itm_dist_amt <> 0) OR
         (v_delete_zero_amounts <> '1') THEN

         v_rowid := bcp_transactions.ROWID;
         v_processed_rows := v_processed_rows + 1;
         v_transaction_amt := v_transaction_amt + bcp_transactions.gl_line_itm_dist_amt;

         -- Perform some error checking and manually set the default value
         IF bcp_transactions.gl_company_nbr IS NULL THEN
            bcp_transactions.gl_company_nbr := ' ';
         END IF;
         IF bcp_transactions.gl_date IS NULL THEN
            bcp_transactions.gl_date := SYSDATE;
         END IF;

         -- Populate and/or Clear working fields
         bcp_transactions.process_batch_no := an_batch_id;
         bcp_transactions.transaction_ind := NULL;
         bcp_transactions.suspend_ind := NULL;
         bcp_transactions.taxcode_detail_id := NULL;
         bcp_transactions.taxcode_state_code := NULL;
         bcp_transactions.taxcode_type_code := NULL;
         bcp_transactions.taxcode_code := NULL;
         bcp_transactions.tax_matrix_id := NULL;
         bcp_transactions.location_matrix_id := NULL;
-- Do not clear! Allocations may have put this in. ---> bcp_transactions.jurisdiction_id := NULL;
         bcp_transactions.jurisdiction_taxrate_id := NULL;
         bcp_transactions.measure_type_code := NULL;
         bcp_transactions.gl_extract_updater := NULL;
         bcp_transactions.gl_extract_timestamp := NULL;
         bcp_transactions.gl_extract_flag := NULL;
         bcp_transactions.gl_log_flag := NULL;
         bcp_transactions.gl_extract_amt := NULL;
         bcp_transactions.load_timestamp := v_sysdate;
         bcp_transactions.update_user_id := USER;
         bcp_transactions.update_timestamp := SYS_EXTRACT_UTC(SYSTIMESTAMP);

         -- Does Transaction Record have a valid State?
         IF bcp_transactions.transaction_state_code IS NULL THEN
            vn_fetch_rows := 0;
         ELSE
            SELECT COUNT(*)
              INTO vn_fetch_rows
              FROM TB_TAXCODE_STATE
             WHERE taxcode_state_code = bcp_transactions.transaction_state_code;
         END IF;
         IF vn_fetch_rows = 0 THEN
            -- Search Location Matrix for matches
            BEGIN
                OPEN location_matrix_cursor
                 FOR vc_location_matrix_stmt
               USING v_rowid;
               FETCH location_matrix_cursor
                INTO location_matrix;
               -- Rows found?
               IF location_matrix_cursor%FOUND THEN
                  vn_fetch_rows := location_matrix_cursor%ROWCOUNT;
               ELSE
                  vn_fetch_rows := 0;
               END IF;
               CLOSE location_matrix_cursor;
               IF SQLCODE != 0 THEN
                  RAISE e_badread;
               END IF;
            EXCEPTION
               WHEN e_badread THEN
                  vn_fetch_rows := 0;
                  vi_error_count := vi_error_count + 1;
                  Sp_Geterrorcode('P4', an_batch_id, 'P', v_sysdate, v_severity_level, v_write_import_line_flag, v_abort_import_flag);
                  IF v_severity_level > v_error_sev_code THEN
                     v_error_sev_code := v_severity_level;
                  END IF;
            END;
            -- Exit if aborted
            IF v_abort_import_flag = '1' THEN
               v_batch_status_code := 'ABP';
               RAISE e_abort;
            END IF;
            -- Location Matrix Line Found
            IF vn_fetch_rows > 0 THEN
               IF bcp_transactions.transaction_state_code IS NULL THEN
                  bcp_transactions.auto_transaction_state_code := '*NULL';
               ELSE
                  bcp_transactions.auto_transaction_state_code := bcp_transactions.transaction_state_code;
               END IF;
               bcp_transactions.transaction_state_code := location_matrix.state;
               bcp_transactions.location_matrix_id := location_matrix.location_matrix_id;
               bcp_transactions.jurisdiction_id := location_matrix.jurisdiction_id;
            ELSE
               -- added--> the following stuff:
               IF bcp_transactions.transaction_state_code IS NULL THEN
                  bcp_transactions.auto_transaction_state_code := '*NULL';
               ELSE
                  bcp_transactions.auto_transaction_state_code := bcp_transactions.transaction_state_code;
               END IF;
               bcp_transactions.transaction_state_code := '*DEF';
               -- removed--> bcp_transactions.transaction_ind := 'S';
               -- removed--> bcp_transactions.suspend_ind := 'L';
            END IF;
         END IF;

         -- Continue if not Suspended
         IF bcp_transactions.transaction_ind IS NULL OR bcp_transactions.transaction_ind <> 'S' THEN
            v_transaction_state_code := bcp_transactions.transaction_state_code;
            -- Search Tax Matrix for matches
            BEGIN
               IF vc_state_driver_flag = '1' THEN
                   OPEN tax_matrix_cursor
                    FOR vc_tax_matrix_stmt
                  USING v_rowid, v_transaction_state_code, v_transaction_state_code;
                  FETCH tax_matrix_cursor
                   INTO tax_matrix;
               ELSE
                   OPEN tax_matrix_cursor
                    FOR vc_tax_matrix_stmt
                  USING v_rowid, v_transaction_state_code;
                  FETCH tax_matrix_cursor
                   INTO tax_matrix;
               END IF;
               -- Rows found?
               IF tax_matrix_cursor%FOUND THEN
                  vn_fetch_rows := tax_matrix_cursor%ROWCOUNT;
               ELSE
                  vn_fetch_rows := 0;
               END IF;
               CLOSE tax_matrix_cursor;
               IF SQLCODE != 0 THEN
                  RAISE e_badread;
               END IF;
            EXCEPTION
               WHEN e_badread THEN
                  vn_fetch_rows := 0;
                  vi_error_count := vi_error_count + 1;
                  Sp_Geterrorcode('P5', an_batch_id, 'P', v_sysdate, v_severity_level, v_write_import_line_flag, v_abort_import_flag);
                  IF v_severity_level > v_error_sev_code THEN
                     v_error_sev_code := v_severity_level;
                   END IF;
            END;
            -- Exit if aborted
            IF v_abort_import_flag = '1' THEN
               v_batch_status_code := 'ABP';
               RAISE e_abort;
            END IF;

            -- Tax Matrix Line Found
            IF vn_fetch_rows > 0 THEN
               bcp_transactions.tax_matrix_id := tax_matrix.tax_matrix_id;
               -- Determine "Then" or "Else" Results
               IF tax_matrix.relation_sign = 'na' THEN
                  tax_matrix.relation_amount := 0;
                  v_less_hold_code_flag := tax_matrix.then_hold_code_flag;
                  v_less_taxcode_detail_id := tax_matrix.then_taxcode_detail_id;
                  v_less_taxcode_state_code := tax_matrix.then_taxcode_state_code;
                  v_less_taxcode_type_code := tax_matrix.then_taxcode_type_code;
                  v_less_taxcode_code := tax_matrix.then_taxcode_code;
                  v_less_jurisdiction_id := tax_matrix.then_jurisdiction_id;
                  v_less_measure_type_code := tax_matrix.then_measure_type_code;
                  v_less_taxtype_code := tax_matrix.then_taxtype_code;
                  v_less_cch_taxcat_code := tax_matrix.then_cch_taxcat_code;
                  v_less_cch_group_code := tax_matrix.then_cch_group_code;
                  -- v_less_cch_item_code := tax_matrix.then_cch_item_code;
                  v_equal_hold_code_flag := tax_matrix.then_hold_code_flag;
                  v_equal_taxcode_detail_id := tax_matrix.then_taxcode_detail_id;
                  v_equal_taxcode_state_code := tax_matrix.then_taxcode_state_code;
                  v_equal_taxcode_type_code := tax_matrix.then_taxcode_type_code;
                  v_equal_taxcode_code := tax_matrix.then_taxcode_code;
                  v_equal_jurisdiction_id := tax_matrix.then_jurisdiction_id;
                  v_equal_measure_type_code := tax_matrix.then_measure_type_code;
                  v_equal_taxtype_code := tax_matrix.then_taxtype_code;
                  v_equal_cch_taxcat_code := tax_matrix.then_cch_taxcat_code;
                  v_equal_cch_group_code := tax_matrix.then_cch_group_code;
                  -- v_equal_cch_item_code := tax_matrix.then_cch_item_code;
                  v_greater_hold_code_flag := tax_matrix.then_hold_code_flag;
                  v_greater_taxcode_detail_id := tax_matrix.then_taxcode_detail_id;
                  v_greater_taxcode_state_code := tax_matrix.then_taxcode_state_code;
                  v_greater_taxcode_type_code := tax_matrix.then_taxcode_type_code;
                  v_greater_taxcode_code := tax_matrix.then_taxcode_code;
                  v_greater_jurisdiction_id := tax_matrix.then_jurisdiction_id;
                  v_greater_measure_type_code := tax_matrix.then_measure_type_code;
                  v_greater_taxtype_code := tax_matrix.then_taxtype_code;
                  v_greater_cch_taxcat_code := tax_matrix.then_cch_taxcat_code;
                  v_greater_cch_group_code := tax_matrix.then_cch_group_code;
                  -- v_greater_cch_item_code := tax_matrix.then_cch_item_code;
              ELSIF tax_matrix.relation_sign = '=' THEN
                  v_less_hold_code_flag := tax_matrix.else_hold_code_flag;
                  v_less_taxcode_detail_id := tax_matrix.else_taxcode_detail_id;
                  v_less_taxcode_state_code := tax_matrix.else_taxcode_state_code;
                  v_less_taxcode_type_code := tax_matrix.else_taxcode_type_code;
                  v_less_taxcode_code := tax_matrix.else_taxcode_code;
                  v_less_jurisdiction_id := tax_matrix.else_jurisdiction_id;
                  v_less_measure_type_code := tax_matrix.else_measure_type_code;
                  v_less_taxtype_code := tax_matrix.else_taxtype_code;
                  v_less_cch_taxcat_code := tax_matrix.else_cch_taxcat_code;
                  v_less_cch_group_code := tax_matrix.else_cch_group_code;
                  -- v_less_cch_item_code := tax_matrix.else_cch_item_code;
                  v_equal_hold_code_flag := tax_matrix.then_hold_code_flag;
                  v_equal_taxcode_detail_id := tax_matrix.then_taxcode_detail_id;
                  v_equal_taxcode_state_code := tax_matrix.then_taxcode_state_code;
                  v_equal_taxcode_type_code := tax_matrix.then_taxcode_type_code;
                  v_equal_taxcode_code := tax_matrix.then_taxcode_code;
                  v_equal_jurisdiction_id := tax_matrix.then_jurisdiction_id;
                  v_equal_measure_type_code := tax_matrix.then_measure_type_code;
                  v_equal_taxtype_code := tax_matrix.then_taxtype_code;
                  v_equal_cch_taxcat_code := tax_matrix.then_cch_taxcat_code;
                  v_equal_cch_group_code := tax_matrix.then_cch_group_code;
                  -- v_equal_cch_item_code := tax_matrix.then_cch_item_code;
                  v_greater_hold_code_flag := tax_matrix.else_hold_code_flag;
                  v_greater_taxcode_detail_id := tax_matrix.else_taxcode_detail_id;
                  v_greater_taxcode_state_code := tax_matrix.else_taxcode_state_code;
                  v_greater_taxcode_type_code := tax_matrix.else_taxcode_type_code;
                  v_greater_taxcode_code := tax_matrix.else_taxcode_code;
                  v_greater_jurisdiction_id := tax_matrix.else_jurisdiction_id;
                  v_greater_measure_type_code := tax_matrix.else_measure_type_code;
                  v_greater_taxtype_code := tax_matrix.else_taxtype_code;
                  v_greater_cch_taxcat_code := tax_matrix.else_cch_taxcat_code;
                  v_greater_cch_group_code := tax_matrix.else_cch_group_code;
                  -- v_greater_cch_item_code := tax_matrix.else_cch_item_code;
               ELSIF tax_matrix.relation_sign = '<' THEN
                  v_less_hold_code_flag := tax_matrix.then_hold_code_flag;
                  v_less_taxcode_detail_id := tax_matrix.then_taxcode_detail_id;
                  v_less_taxcode_state_code := tax_matrix.then_taxcode_state_code;
                  v_less_taxcode_type_code := tax_matrix.then_taxcode_type_code;
                  v_less_taxcode_code := tax_matrix.then_taxcode_code;
                  v_less_jurisdiction_id := tax_matrix.then_jurisdiction_id;
                  v_less_measure_type_code := tax_matrix.then_measure_type_code;
                  v_less_taxtype_code := tax_matrix.then_taxtype_code;
                  v_less_cch_taxcat_code := tax_matrix.then_cch_taxcat_code;
                  v_less_cch_group_code := tax_matrix.then_cch_group_code;
                  -- v_less_cch_item_code := tax_matrix.then_cch_item_code;
                  v_equal_hold_code_flag := tax_matrix.else_hold_code_flag;
                  v_equal_taxcode_detail_id := tax_matrix.else_taxcode_detail_id;
                  v_equal_taxcode_state_code := tax_matrix.else_taxcode_state_code;
                  v_equal_taxcode_type_code := tax_matrix.else_taxcode_type_code;
                  v_equal_taxcode_code := tax_matrix.else_taxcode_code;
                  v_equal_jurisdiction_id := tax_matrix.else_jurisdiction_id;
                  v_equal_measure_type_code := tax_matrix.else_measure_type_code;
                  v_equal_taxtype_code := tax_matrix.else_taxtype_code;
                  v_equal_cch_taxcat_code := tax_matrix.else_cch_taxcat_code;
                  v_equal_cch_group_code := tax_matrix.else_cch_group_code;
                  -- v_equal_cch_item_code := tax_matrix.else_cch_item_code;
                  v_greater_hold_code_flag := tax_matrix.else_hold_code_flag;
                  v_greater_taxcode_detail_id := tax_matrix.else_taxcode_detail_id;
                  v_greater_taxcode_state_code := tax_matrix.else_taxcode_state_code;
                  v_greater_taxcode_type_code := tax_matrix.else_taxcode_type_code;
                  v_greater_taxcode_code := tax_matrix.else_taxcode_code;
                  v_greater_jurisdiction_id := tax_matrix.else_jurisdiction_id;
                  v_greater_measure_type_code := tax_matrix.else_measure_type_code;
                  v_greater_taxtype_code := tax_matrix.else_taxtype_code;
                  v_greater_cch_taxcat_code := tax_matrix.else_cch_taxcat_code;
                  v_greater_cch_group_code := tax_matrix.else_cch_group_code;
                  -- v_greater_cch_item_code := tax_matrix.else_cch_item_code;
               ELSIF tax_matrix.relation_sign = '<=' THEN
                  v_less_hold_code_flag := tax_matrix.then_hold_code_flag;
                  v_less_taxcode_detail_id := tax_matrix.then_taxcode_detail_id;
                  v_less_taxcode_state_code := tax_matrix.then_taxcode_state_code;
                  v_less_taxcode_type_code := tax_matrix.then_taxcode_type_code;
                  v_less_taxcode_code := tax_matrix.then_taxcode_code;
                  v_less_jurisdiction_id := tax_matrix.then_jurisdiction_id;
                  v_less_measure_type_code := tax_matrix.then_measure_type_code;
                  v_less_taxtype_code := tax_matrix.then_taxtype_code;
                  v_less_cch_taxcat_code := tax_matrix.then_cch_taxcat_code;
                  v_less_cch_group_code := tax_matrix.then_cch_group_code;
                  -- v_less_cch_item_code := tax_matrix.then_cch_item_code;
                  v_equal_hold_code_flag := tax_matrix.then_hold_code_flag;
                  v_equal_taxcode_detail_id := tax_matrix.then_taxcode_detail_id;
                  v_equal_taxcode_state_code := tax_matrix.then_taxcode_state_code;
                  v_equal_taxcode_type_code := tax_matrix.then_taxcode_type_code;
                  v_equal_taxcode_code := tax_matrix.then_taxcode_code;
                  v_equal_jurisdiction_id := tax_matrix.then_jurisdiction_id;
                  v_equal_measure_type_code := tax_matrix.then_measure_type_code;
                  v_equal_taxtype_code := tax_matrix.then_taxtype_code;
                  v_equal_cch_taxcat_code := tax_matrix.then_cch_taxcat_code;
                  v_equal_cch_group_code := tax_matrix.then_cch_group_code;
                  -- v_equal_cch_item_code := tax_matrix.then_cch_item_code;
                  v_greater_hold_code_flag := tax_matrix.else_hold_code_flag;
                  v_greater_taxcode_detail_id := tax_matrix.else_taxcode_detail_id;
                  v_greater_taxcode_state_code := tax_matrix.else_taxcode_state_code;
                  v_greater_taxcode_type_code := tax_matrix.else_taxcode_type_code;
                  v_greater_taxcode_code := tax_matrix.else_taxcode_code;
                  v_greater_jurisdiction_id := tax_matrix.else_jurisdiction_id;
                  v_greater_measure_type_code := tax_matrix.else_measure_type_code;
                  v_greater_taxtype_code := tax_matrix.else_taxtype_code;
                  v_greater_cch_taxcat_code := tax_matrix.else_cch_taxcat_code;
                  v_greater_cch_group_code := tax_matrix.else_cch_group_code;
                  -- v_greater_cch_item_code := tax_matrix.else_cch_item_code;
               ELSIF tax_matrix.relation_sign = '>' THEN
                  v_less_hold_code_flag := tax_matrix.else_hold_code_flag;
                  v_less_taxcode_detail_id := tax_matrix.else_taxcode_detail_id;
                  v_less_taxcode_state_code := tax_matrix.else_taxcode_state_code;
                  v_less_taxcode_type_code := tax_matrix.else_taxcode_type_code;
                  v_less_taxcode_code := tax_matrix.else_taxcode_code;
                  v_less_jurisdiction_id := tax_matrix.else_jurisdiction_id;
                  v_less_measure_type_code := tax_matrix.else_measure_type_code;
                  v_less_taxtype_code := tax_matrix.else_taxtype_code;
                  v_less_cch_taxcat_code := tax_matrix.else_cch_taxcat_code;
                  v_less_cch_group_code := tax_matrix.else_cch_group_code;
                  -- v_less_cch_item_code := tax_matrix.else_cch_item_code;
                  v_equal_hold_code_flag := tax_matrix.else_hold_code_flag;
                  v_equal_taxcode_detail_id := tax_matrix.else_taxcode_detail_id;
                  v_equal_taxcode_state_code := tax_matrix.else_taxcode_state_code;
                  v_equal_taxcode_type_code := tax_matrix.else_taxcode_type_code;
                  v_equal_taxcode_code := tax_matrix.else_taxcode_code;
                  v_equal_jurisdiction_id := tax_matrix.else_jurisdiction_id;
                  v_equal_measure_type_code := tax_matrix.else_measure_type_code;
                  v_equal_taxtype_code := tax_matrix.else_taxtype_code;
                  v_equal_cch_taxcat_code := tax_matrix.else_cch_taxcat_code;
                  v_equal_cch_group_code := tax_matrix.else_cch_group_code;
                  -- v_equal_cch_item_code := tax_matrix.else_cch_item_code;
                  v_greater_hold_code_flag := tax_matrix.then_hold_code_flag;
                  v_greater_taxcode_detail_id := tax_matrix.then_taxcode_detail_id;
                  v_greater_taxcode_state_code := tax_matrix.then_taxcode_state_code;
                  v_greater_taxcode_type_code := tax_matrix.then_taxcode_type_code;
                  v_greater_taxcode_code := tax_matrix.then_taxcode_code;
                  v_greater_jurisdiction_id := tax_matrix.then_jurisdiction_id;
                  v_greater_measure_type_code := tax_matrix.then_measure_type_code;
                  v_greater_taxtype_code := tax_matrix.then_taxtype_code;
                  v_greater_cch_taxcat_code := tax_matrix.then_cch_taxcat_code;
                  v_greater_cch_group_code := tax_matrix.then_cch_group_code;
                  -- v_greater_cch_item_code := tax_matrix.then_cch_item_code;
               ELSIF tax_matrix.relation_sign = '>=' THEN
                  v_less_hold_code_flag := tax_matrix.else_hold_code_flag;
                  v_less_taxcode_detail_id := tax_matrix.else_taxcode_detail_id;
                  v_less_taxcode_state_code := tax_matrix.else_taxcode_state_code;
                  v_less_taxcode_type_code := tax_matrix.else_taxcode_type_code;
                  v_less_taxcode_code := tax_matrix.else_taxcode_code;
                  v_less_jurisdiction_id := tax_matrix.else_jurisdiction_id;
                  v_less_measure_type_code := tax_matrix.else_measure_type_code;
                  v_less_taxtype_code := tax_matrix.else_taxtype_code;
                  v_less_cch_taxcat_code := tax_matrix.else_cch_taxcat_code;
                  v_less_cch_group_code := tax_matrix.else_cch_group_code;
                  -- v_less_cch_item_code := tax_matrix.else_cch_item_code;
                  v_equal_hold_code_flag := tax_matrix.then_hold_code_flag;
                  v_equal_taxcode_detail_id := tax_matrix.then_taxcode_detail_id;
                  v_equal_taxcode_state_code := tax_matrix.then_taxcode_state_code;
                  v_equal_taxcode_type_code := tax_matrix.then_taxcode_type_code;
                  v_equal_taxcode_code := tax_matrix.then_taxcode_code;
                  v_equal_jurisdiction_id := tax_matrix.then_jurisdiction_id;
                  v_equal_measure_type_code := tax_matrix.then_measure_type_code;
                  v_equal_taxtype_code := tax_matrix.then_taxtype_code;
                  v_equal_cch_taxcat_code := tax_matrix.then_cch_taxcat_code;
                  v_equal_cch_group_code := tax_matrix.then_cch_group_code;
                  -- v_equal_cch_item_code := tax_matrix.then_cch_item_code;
                  v_greater_hold_code_flag := tax_matrix.then_hold_code_flag;
                  v_greater_taxcode_detail_id := tax_matrix.then_taxcode_detail_id;
                  v_greater_taxcode_state_code := tax_matrix.then_taxcode_state_code;
                  v_greater_taxcode_type_code := tax_matrix.then_taxcode_type_code;
                  v_greater_taxcode_code := tax_matrix.then_taxcode_code;
                  v_greater_jurisdiction_id := tax_matrix.then_jurisdiction_id;
                  v_greater_measure_type_code := tax_matrix.then_measure_type_code;
                  v_greater_taxtype_code := tax_matrix.then_taxtype_code;
                  v_greater_cch_taxcat_code := tax_matrix.then_cch_taxcat_code;
                  v_greater_cch_group_code := tax_matrix.then_cch_group_code;
                  -- v_greater_cch_item_code := tax_matrix.then_cch_item_code;
               ELSIF tax_matrix.relation_sign = '<>' THEN
                  v_less_hold_code_flag := tax_matrix.then_hold_code_flag;
                  v_less_taxcode_detail_id := tax_matrix.then_taxcode_detail_id;
                  v_less_taxcode_state_code := tax_matrix.then_taxcode_state_code;
                  v_less_taxcode_type_code := tax_matrix.then_taxcode_type_code;
                  v_less_taxcode_code := tax_matrix.then_taxcode_code;
                  v_less_jurisdiction_id := tax_matrix.then_jurisdiction_id;
                  v_less_measure_type_code := tax_matrix.then_measure_type_code;
                  v_less_taxtype_code := tax_matrix.then_taxtype_code;
                  v_less_cch_taxcat_code := tax_matrix.then_cch_taxcat_code;
                  v_less_cch_group_code := tax_matrix.then_cch_group_code;
                  -- v_less_cch_item_code := tax_matrix.then_cch_item_code;
                  v_equal_hold_code_flag := tax_matrix.else_hold_code_flag;
                  v_equal_taxcode_detail_id := tax_matrix.else_taxcode_detail_id;
                  v_equal_taxcode_state_code := tax_matrix.else_taxcode_state_code;
                  v_equal_taxcode_type_code := tax_matrix.else_taxcode_type_code;
                  v_equal_taxcode_code := tax_matrix.else_taxcode_code;
                  v_equal_jurisdiction_id := tax_matrix.else_jurisdiction_id;
                  v_equal_measure_type_code := tax_matrix.else_measure_type_code;
                  v_equal_taxtype_code := tax_matrix.else_taxtype_code;
                  v_equal_cch_taxcat_code := tax_matrix.else_cch_taxcat_code;
                  v_equal_cch_group_code := tax_matrix.else_cch_group_code;
                  -- v_equal_cch_item_code := tax_matrix.else_cch_item_code;
                  v_greater_hold_code_flag := tax_matrix.then_hold_code_flag;
                  v_greater_taxcode_detail_id := tax_matrix.then_taxcode_detail_id;
                  v_greater_taxcode_state_code := tax_matrix.then_taxcode_state_code;
                  v_greater_taxcode_type_code := tax_matrix.then_taxcode_type_code;
                  v_greater_taxcode_code := tax_matrix.then_taxcode_code;
                  v_greater_jurisdiction_id := tax_matrix.then_jurisdiction_id;
                  v_greater_measure_type_code := tax_matrix.then_measure_type_code;
                  v_greater_taxtype_code := tax_matrix.then_taxtype_code;
                  v_greater_cch_taxcat_code := tax_matrix.then_cch_taxcat_code;
                  v_greater_cch_group_code := tax_matrix.then_cch_group_code;
                  -- v_greater_cch_item_code := tax_matrix.then_cch_item_code;
               END IF;

               SELECT DECODE(SIGN(bcp_transactions.gl_line_itm_dist_amt - tax_matrix.relation_amount), -1, v_less_hold_code_flag, 0, v_equal_hold_code_flag, 1, v_greater_hold_code_flag),
                      DECODE(SIGN(bcp_transactions.gl_line_itm_dist_amt - tax_matrix.relation_amount), -1, v_less_taxcode_detail_id, 0, v_equal_taxcode_detail_id, 1, v_greater_taxcode_detail_id),
                      DECODE(SIGN(bcp_transactions.gl_line_itm_dist_amt - tax_matrix.relation_amount), -1, v_less_taxcode_state_code, 0, v_equal_taxcode_state_code, 1, v_greater_taxcode_state_code),
                      DECODE(SIGN(bcp_transactions.gl_line_itm_dist_amt - tax_matrix.relation_amount), -1, v_less_taxcode_type_code, 0, v_equal_taxcode_type_code, 1, v_greater_taxcode_type_code),
                      DECODE(SIGN(bcp_transactions.gl_line_itm_dist_amt - tax_matrix.relation_amount), -1, v_less_taxcode_code, 0, v_equal_taxcode_code, 1, v_greater_taxcode_code),
                      DECODE(SIGN(bcp_transactions.gl_line_itm_dist_amt - tax_matrix.relation_amount), -1, v_less_jurisdiction_id, 0, v_equal_jurisdiction_id, 1, v_greater_jurisdiction_id),
                      DECODE(SIGN(bcp_transactions.gl_line_itm_dist_amt - tax_matrix.relation_amount), -1, v_less_measure_type_code, 0, v_equal_measure_type_code, 1, v_greater_measure_type_code),
                      DECODE(SIGN(bcp_transactions.gl_line_itm_dist_amt - tax_matrix.relation_amount), -1, v_less_taxtype_code, 0, v_equal_taxtype_code, 1, v_greater_taxtype_code),
                      DECODE(SIGN(bcp_transactions.gl_line_itm_dist_amt - tax_matrix.relation_amount), -1, v_less_cch_taxcat_code, 0, v_equal_cch_taxcat_code, 1, v_greater_cch_taxcat_code),
                      DECODE(SIGN(bcp_transactions.gl_line_itm_dist_amt - tax_matrix.relation_amount), -1, v_less_cch_group_code, 0, v_equal_cch_group_code, 1, v_greater_cch_group_code)
                 INTO v_hold_code_flag, bcp_transactions.taxcode_detail_id, bcp_transactions.taxcode_state_code, bcp_transactions.taxcode_type_code, bcp_transactions.taxcode_code, v_override_jurisdiction_id, bcp_transactions.measure_type_code, v_taxtype_code, bcp_transactions.cch_taxcat_code, bcp_transactions.cch_group_code
                 FROM DUAL;

               -- ****** DETERMINE TYPE of TAXCODE ****** --
               -- The Tax Code is Taxable
               IF bcp_transactions.taxcode_type_code = 'T' THEN
                  -- If Override jurisdiction id is not blank or 0 then use it instead of location matrix
                  IF v_override_jurisdiction_id IS NOT NULL AND v_override_jurisdiction_id <> 0 THEN
                     bcp_transactions.jurisdiction_id := v_override_jurisdiction_id;
                     bcp_transactions.manual_jurisdiction_ind := 'AO';
                  ELSE
                     -- Search for Location Matrix if not already found
                     IF (bcp_transactions.location_matrix_id IS NULL OR bcp_transactions.location_matrix_id = 0) AND
                        (bcp_transactions.jurisdiction_id IS NULL OR bcp_transactions.jurisdiction_id = 0) THEN
                        -- Search Location Matrix for matches
                        BEGIN
                            OPEN location_matrix_cursor
                             FOR vc_location_matrix_stmt
                           USING v_rowid;
                           FETCH location_matrix_cursor
                            INTO location_matrix;
                           -- Rows found?
                           IF location_matrix_cursor%FOUND THEN
                              vn_fetch_rows := location_matrix_cursor%ROWCOUNT;
                           ELSE
                              vn_fetch_rows := 0;
                           END IF;
                           CLOSE location_matrix_cursor;
                           IF SQLCODE != 0 THEN
                              RAISE e_badread;
                           END IF;
                        EXCEPTION
                           WHEN e_badread THEN
                              vn_fetch_rows := 0;
                              vi_error_count := vi_error_count + 1;
                              Sp_Geterrorcode('P4', an_batch_id, 'P', v_sysdate, v_severity_level, v_write_import_line_flag, v_abort_import_flag);
                              IF v_severity_level > v_error_sev_code THEN
                                 v_error_sev_code := v_severity_level;
                              END IF;
                        END;
                        -- Exit if aborted
                        IF v_abort_import_flag = '1' THEN
                           v_batch_status_code := 'ABP';
                           RAISE e_abort;
                        END IF;

                        -- Location Matrix Line Found
                        IF vn_fetch_rows > 0 THEN
                           bcp_transactions.location_matrix_id := location_matrix.location_matrix_id;
                           bcp_transactions.jurisdiction_id := location_matrix.jurisdiction_id;
                        ELSE
                           bcp_transactions.transaction_ind := 'S';
                           bcp_transactions.suspend_ind := 'L';
                        END IF;
                     END IF;
                  END IF;

                  -- Continue if not Suspended
                  IF bcp_transactions.transaction_ind IS NULL OR bcp_transactions.transaction_ind <> 'S' THEN
                     -- Get Jurisdiction TaxRates and calculate Tax Amounts
                     Sp_Getusetax(bcp_transactions.jurisdiction_id,
                                  bcp_transactions.gl_date,
                                  bcp_transactions.measure_type_code,
                                  bcp_transactions.gl_line_itm_dist_amt,
                                  v_taxtype_code,
                                  location_matrix.override_taxtype_code,
                                  location_matrix.state_flag,
                                  location_matrix.county_flag,
                                  location_matrix.county_local_flag,
                                  location_matrix.city_flag,
                                  location_matrix.city_local_flag,
                                  bcp_transactions.invoice_freight_amt,
                                  bcp_transactions.invoice_discount_amt,
                                  bcp_transactions.transaction_state_code,
-- future?? --                          bcp_transactions.import_definition_code,
                                  'importdefn',
                                  bcp_transactions.taxcode_code,
                                  bcp_transactions.jurisdiction_taxrate_id,
                                  bcp_transactions.state_use_amount,
                                  bcp_transactions.state_use_tier2_amount,
                                  bcp_transactions.state_use_tier3_amount,
                                  bcp_transactions.county_use_amount,
                                  bcp_transactions.county_local_use_amount,
                                  bcp_transactions.city_use_amount,
                                  bcp_transactions.city_local_use_amount,
                                  bcp_transactions.state_use_rate,
                                  bcp_transactions.state_use_tier2_rate,
                                  bcp_transactions.state_use_tier3_rate,
                                  bcp_transactions.state_split_amount,
                                  bcp_transactions.state_tier2_min_amount,
                                  bcp_transactions.state_tier2_max_amount,
                                  bcp_transactions.state_maxtax_amount,
                                  bcp_transactions.county_use_rate,
                                  bcp_transactions.county_local_use_rate,
                                  bcp_transactions.county_split_amount,
                                  bcp_transactions.county_maxtax_amount,
                                  bcp_transactions.county_single_flag,
                                  bcp_transactions.county_default_flag,
                                  bcp_transactions.city_use_rate,
                                  bcp_transactions.city_local_use_rate,
                                  bcp_transactions.city_split_amount,
                                  bcp_transactions.city_split_use_rate,
                                  bcp_transactions.city_single_flag,
                                  bcp_transactions.city_default_flag,
                                  bcp_transactions.combined_use_rate,
                                  bcp_transactions.tb_calc_tax_amt,
-- future?? --                          bcp_transactions.taxable_amt,
-- Targa --                         bcp_transactions.user_number_10,
                                  vn_taxable_amt,
                                  v_taxtype_used );
                     IF bcp_transactions.jurisdiction_taxrate_id = 0 THEN
                         vn_fetch_rows := 0;
                     END IF;
                     IF bcp_transactions.jurisdiction_taxrate_id IS NULL THEN
                        vn_fetch_rows := 0;
                        vi_error_count := vi_error_count + 1;
                        Sp_Geterrorcode('P6', an_batch_id, 'P', v_sysdate, v_severity_level, v_write_import_line_flag, v_abort_import_flag);
                        IF v_severity_level > v_error_sev_code THEN
                           v_error_sev_code := v_severity_level;
                        END IF;
                     END IF;
                     -- Exit if aborted
                     IF v_abort_import_flag = '1' THEN
                        v_batch_status_code := 'ABP';
                        RAISE e_abort;
                     END IF;

                     -- Jurisdiction Tax Rate FOUND -- 3351 --
                     IF vn_fetch_rows > 0 THEN
                        IF vn_taxable_amt <> bcp_transactions.gl_line_itm_dist_amt THEN
                           bcp_transactions.gl_extract_amt := bcp_transactions.gl_line_itm_dist_amt;
                           bcp_transactions.gl_line_itm_dist_amt := vn_taxable_amt;
                        END IF;
                     -- Jurisdiciton Tax Rate NOT Found
                     ELSE
                     -- IF vn_fetch_rows = 0 THEN
                        bcp_transactions.transaction_ind := 'S';
                        bcp_transactions.suspend_ind := 'R';
                     END IF;
                  END IF;

               -- The Tax Code is Exempt
               ELSIF bcp_transactions.taxcode_type_code = 'E' THEN
                  bcp_transactions.transaction_ind := 'P';

               -- The Tax Code is a Question
               ELSIF bcp_transactions.taxcode_type_code = 'Q' THEN
                 bcp_transactions.transaction_ind := 'Q';

               -- The Tax Code is Unrecognized - Suspend
               ELSE
                  bcp_transactions.transaction_ind := 'S';
                  bcp_transactions.suspend_ind := '?';
               END IF;

               -- Continue if not Suspended
               IF bcp_transactions.transaction_ind IS NULL OR bcp_transactions.transaction_ind <> 'S' THEN
                  -- Check for matrix "H"old AND and global "H"old ELSE "P"rocessed
                  IF ( v_hold_code_flag = '1' ) AND
                     ( bcp_transactions.taxcode_type_code = 'T' OR bcp_transactions.taxcode_type_code = 'E') THEN
                        bcp_transactions.transaction_ind := 'H';
                  ELSIF ( v_hold_trans_flag = '1' ) AND
                        ( bcp_transactions.taxcode_type_code = 'T' OR bcp_transactions.taxcode_type_code = 'E') AND
                        ( ABS(bcp_transactions.gl_line_itm_dist_amt) >= v_hold_trans_amount ) THEN
                           bcp_transactions.transaction_ind := 'H';
                  ELSE
                     bcp_transactions.transaction_ind := 'P';
                  END IF;
               END IF;

            -- Tax Matrix Line NOT Found
            ELSE
               IF bcp_transactions.transaction_state_code = '*DEF' THEN
                  bcp_transactions.transaction_ind := 'S';
                  bcp_transactions.suspend_ind := 'L';
                  IF bcp_transactions.auto_transaction_state_code = '*NULL' THEN
                     bcp_transactions.transaction_state_code := NULL;
                  ELSE
                     bcp_transactions.transaction_state_code := bcp_transactions.auto_transaction_state_code;
                  END IF;
                  bcp_transactions.auto_transaction_state_code := NULL;
               ELSE
                  bcp_transactions.transaction_ind := 'S';
                  bcp_transactions.suspend_ind := 'T';
               END IF;
            END IF;
         END IF;

         -- Write line?
         IF v_write_import_line_flag = '1' THEN
            -- Check for Auto-Process Material Limit on Suspended Lines
            IF v_autoproc_flag = '1' THEN
               IF bcp_transactions.transaction_ind = 'S' AND bcp_transactions.suspend_ind = 'T' THEN
                  IF ABS(bcp_transactions.gl_line_itm_dist_amt) <= vn_autoproc_amount THEN
                     bcp_transactions.transaction_ind := 'P';
                     bcp_transactions.suspend_ind := '';
                     bcp_transactions.manual_taxcode_ind := 'AP';
                     bcp_transactions.taxcode_detail_id := vn_autoproc_id;
                     bcp_transactions.taxcode_state_code := v_autoproc_state_code;
                     bcp_transactions.taxcode_type_code := v_autoproc_type_code;
                     bcp_transactions.taxcode_code := v_autoproc_code;
                  END IF;
               END IF;
            END IF;

            -- // MBF?? - Add Allocations - top
-- 3411? --            IF bcp_transactions.transaction_detail_id = -1 THEN
-- 3411? --               IF v_allocation_flag = '1' THEN
-- 3411? --                  IF NOT (bcp_transactions.transaction_ind = 'P' AND
-- 3411? --                          bcp_transactions.taxcode_state_code = '*ALL' AND
-- 3411? --                          bcp_transactions.taxcode_type_code = 'E') THEN
-- 3411? --                     sp_batch_allocation(v_rowid,
-- 3411? --                                         an_batch_id,
-- 3411? --                                         vn_trans_created);
-- 3411? --                     IF vn_trans_created > 0 THEN
-- 3411? --                        v_processed_rows := v_processed_rows + vn_trans_created - 1;
-- 3411? --                        v_write_import_line_flag := '0';
-- 3411? --                     END IF;
-- 3411? --                  END IF;
-- 3411? --               END IF;
-- 3411? --            END IF;

-- 3411? --            If v_write_import_line_flag = '1' THEN
            -- // MBF?? - Add Allocations - bottom

            -- Get next transaction ID for the new transaction detail record
            SELECT sq_tb_transaction_detail_id.NEXTVAL
              INTO bcp_transactions.transaction_detail_id
              FROM DUAL;

            -- Insert transaction detail row
            BEGIN
               -- 3411 - add taxable amount and tax type used.
               INSERT INTO TB_TRANSACTION_DETAIL (
                  transaction_detail_id,
                  source_transaction_id,
                  process_batch_no,
                  gl_extract_batch_no,
                  archive_batch_no,
                  allocation_matrix_id,
                  allocation_subtrans_id,
                  entered_date,
                  transaction_status,
                  gl_date,
                  gl_company_nbr,
                  gl_company_name,
                  gl_division_nbr,
                  gl_division_name,
                  gl_cc_nbr_dept_id,
                  gl_cc_nbr_dept_name,
                  gl_local_acct_nbr,
                  gl_local_acct_name,
                  gl_local_sub_acct_nbr,
                  gl_local_sub_acct_name,
                  gl_full_acct_nbr,
                  gl_full_acct_name,
                  gl_line_itm_dist_amt,
                  orig_gl_line_itm_dist_amt,
                  vendor_nbr,
                  vendor_name,
                  vendor_address_line_1,
                  vendor_address_line_2,
                  vendor_address_line_3,
                  vendor_address_line_4,
                  vendor_address_city,
                  vendor_address_county,
                  vendor_address_state,
                  vendor_address_zip,
                  vendor_address_country,
                  vendor_type,
                  vendor_type_name,
                  invoice_nbr,
                  invoice_desc,
                  invoice_date,
                  invoice_freight_amt,
                  invoice_discount_amt,
                  invoice_tax_amt,
                  invoice_total_amt,
                  invoice_tax_flg,
                  invoice_line_nbr,
                  invoice_line_name,
                  invoice_line_type,
                  invoice_line_type_name,
                  invoice_line_amt,
                  invoice_line_tax,
                  afe_project_nbr,
                  afe_project_name,
                  afe_category_nbr,
                  afe_category_name,
                  afe_sub_cat_nbr,
                  afe_sub_cat_name,
                  afe_use,
                  afe_contract_type,
                  afe_contract_structure,
                  afe_property_cat,
                  inventory_nbr,
                  inventory_name,
                  inventory_class,
                  inventory_class_name,
                  po_nbr,
                  po_name,
                  po_date,
                  po_line_nbr,
                  po_line_name,
                  po_line_type,
                  po_line_type_name,
                  ship_to_location,
                  ship_to_location_name,
                  ship_to_address_line_1,
                  ship_to_address_line_2,
                  ship_to_address_line_3,
                  ship_to_address_line_4,
                  ship_to_address_city,
                  ship_to_address_county,
                  ship_to_address_state,
                  ship_to_address_zip,
                  ship_to_address_country,
                  wo_nbr,
                  wo_name,
                  wo_date,
                  wo_type,
                  wo_type_desc,
                  wo_class,
                  wo_class_desc,
                  wo_entity,
                  wo_entity_desc,
                  wo_line_nbr,
                  wo_line_name,
                  wo_line_type,
                  wo_line_type_desc,
                  wo_shut_down_cd,
                  wo_shut_down_cd_desc,
                  voucher_id,
                  voucher_name,
                  voucher_date,
                  voucher_line_nbr,
                  voucher_line_desc,
                  check_nbr,
                  check_no,
                  check_date,
                  check_amt,
                  check_desc,
                  user_text_01,
                  user_text_02,
                  user_text_03,
                  user_text_04,
                  user_text_05,
                  user_text_06,
                  user_text_07,
                  user_text_08,
                  user_text_09,
                  user_text_10,
                  user_text_11,
                  user_text_12,
                  user_text_13,
                  user_text_14,
                  user_text_15,
                  user_text_16,
                  user_text_17,
                  user_text_18,
                  user_text_19,
                  user_text_20,
                  user_text_21,
                  user_text_22,
                  user_text_23,
                  user_text_24,
                  user_text_25,
                  user_text_26,
                  user_text_27,
                  user_text_28,
                  user_text_29,
                  user_text_30,
                  user_number_01,
                  user_number_02,
                  user_number_03,
                  user_number_04,
                  user_number_05,
                  user_number_06,
                  user_number_07,
                  user_number_08,
                  user_number_09,
                  user_number_10,
                  user_date_01,
                  user_date_02,
                  user_date_03,
                  user_date_04,
                  user_date_05,
                  user_date_06,
                  user_date_07,
                  user_date_08,
                  user_date_09,
                  user_date_10,
                  comments,
                  tb_calc_tax_amt,
                  state_use_amount,
                  state_use_tier2_amount,
                  state_use_tier3_amount,
                  county_use_amount,
                  county_local_use_amount,
                  city_use_amount,
                  city_local_use_amount,
                  transaction_state_code,
                  auto_transaction_state_code,
                  transaction_ind,
                  suspend_ind,
                  taxcode_detail_id,
                  taxcode_state_code,
                  taxcode_type_code,
                  taxcode_code,
                  cch_taxcat_code,
                  cch_group_code,
                  cch_item_code,
                  manual_taxcode_ind,
                  tax_matrix_id,
                  location_matrix_id,
                  jurisdiction_id,
                  jurisdiction_taxrate_id,
                  manual_jurisdiction_ind,
                  measure_type_code,
                  state_use_rate,
                  state_use_tier2_rate,
                  state_use_tier3_rate,
                  state_split_amount,
                  state_tier2_min_amount,
                  state_tier2_max_amount,
                  state_maxtax_amount,
                  county_use_rate,
                  county_local_use_rate,
                  county_split_amount,
                  county_maxtax_amount,
                  county_single_flag,
                  county_default_flag,
                  city_use_rate,
                  city_local_use_rate,
                  city_split_amount,
                  city_split_use_rate,
                  city_single_flag,
                  city_default_flag,
                  combined_use_rate,
                  load_timestamp,
                  gl_extract_updater,
                  gl_extract_timestamp,
                  gl_extract_flag,
                  gl_log_flag,
                  gl_extract_amt,
                  audit_flag,
                  audit_user_id,
                  audit_timestamp,
                  modify_user_id,
                  modify_timestamp,
                  update_user_id,
                  update_timestamp )
               VALUES (
                  bcp_transactions.transaction_detail_id,
                  bcp_transactions.source_transaction_id,
                  bcp_transactions.process_batch_no,
                  bcp_transactions.gl_extract_batch_no,
                  bcp_transactions.archive_batch_no,
                  bcp_transactions.allocation_matrix_id,
                  bcp_transactions.allocation_subtrans_id,
                  bcp_transactions.entered_date,
                  bcp_transactions.transaction_status,
                  bcp_transactions.gl_date,
                  bcp_transactions.gl_company_nbr,
                  bcp_transactions.gl_company_name,
                  bcp_transactions.gl_division_nbr,
                  bcp_transactions.gl_division_name,
                  bcp_transactions.gl_cc_nbr_dept_id,
                  bcp_transactions.gl_cc_nbr_dept_name,
                  bcp_transactions.gl_local_acct_nbr,
                  bcp_transactions.gl_local_acct_name,
                  bcp_transactions.gl_local_sub_acct_nbr,
                  bcp_transactions.gl_local_sub_acct_name,
                  bcp_transactions.gl_full_acct_nbr,
                  bcp_transactions.gl_full_acct_name,
                  bcp_transactions.gl_line_itm_dist_amt,
                  bcp_transactions.orig_gl_line_itm_dist_amt,
                  bcp_transactions.vendor_nbr,
                  bcp_transactions.vendor_name,
                  bcp_transactions.vendor_address_line_1,
                  bcp_transactions.vendor_address_line_2,
                  bcp_transactions.vendor_address_line_3,
                  bcp_transactions.vendor_address_line_4,
                  bcp_transactions.vendor_address_city,
                  bcp_transactions.vendor_address_county,
                  bcp_transactions.vendor_address_state,
                  bcp_transactions.vendor_address_zip,
                  bcp_transactions.vendor_address_country,
                  bcp_transactions.vendor_type,
                  bcp_transactions.vendor_type_name,
                  bcp_transactions.invoice_nbr,
                  bcp_transactions.invoice_desc,
                  bcp_transactions.invoice_date,
                  bcp_transactions.invoice_freight_amt,
                  bcp_transactions.invoice_discount_amt,
                  bcp_transactions.invoice_tax_amt,
                  bcp_transactions.invoice_total_amt,
                  bcp_transactions.invoice_tax_flg,
                  bcp_transactions.invoice_line_nbr,
                  bcp_transactions.invoice_line_name,
                  bcp_transactions.invoice_line_type,
                  bcp_transactions.invoice_line_type_name,
                  bcp_transactions.invoice_line_amt,
                  bcp_transactions.invoice_line_tax,
                  bcp_transactions.afe_project_nbr,
                  bcp_transactions.afe_project_name,
                  bcp_transactions.afe_category_nbr,
                  bcp_transactions.afe_category_name,
                  bcp_transactions.afe_sub_cat_nbr,
                  bcp_transactions.afe_sub_cat_name,
                  bcp_transactions.afe_use,
                  bcp_transactions.afe_contract_type,
                  bcp_transactions.afe_contract_structure,
                  bcp_transactions.afe_property_cat,
                  bcp_transactions.inventory_nbr,
                  bcp_transactions.inventory_name,
                  bcp_transactions.inventory_class,
                  bcp_transactions.inventory_class_name,
                  bcp_transactions.po_nbr,
                  bcp_transactions.po_name,
                  bcp_transactions.po_date,
                  bcp_transactions.po_line_nbr,
                  bcp_transactions.po_line_name,
                  bcp_transactions.po_line_type,
                  bcp_transactions.po_line_type_name,
                  bcp_transactions.ship_to_location,
                  bcp_transactions.ship_to_location_name,
                  bcp_transactions.ship_to_address_line_1,
                  bcp_transactions.ship_to_address_line_2,
                  bcp_transactions.ship_to_address_line_3,
                  bcp_transactions.ship_to_address_line_4,
                  bcp_transactions.ship_to_address_city,
                  bcp_transactions.ship_to_address_county,
                  bcp_transactions.ship_to_address_state,
                  bcp_transactions.ship_to_address_zip,
                  bcp_transactions.ship_to_address_country,
                  bcp_transactions.wo_nbr,
                  bcp_transactions.wo_name,
                  bcp_transactions.wo_date,
                  bcp_transactions.wo_type,
                  bcp_transactions.wo_type_desc,
                  bcp_transactions.wo_class,
                  bcp_transactions.wo_class_desc,
                  bcp_transactions.wo_entity,
                  bcp_transactions.wo_entity_desc,
                  bcp_transactions.wo_line_nbr,
                  bcp_transactions.wo_line_name,
                  bcp_transactions.wo_line_type,
                  bcp_transactions.wo_line_type_desc,
                  bcp_transactions.wo_shut_down_cd,
                  bcp_transactions.wo_shut_down_cd_desc,
                  bcp_transactions.voucher_id,
                  bcp_transactions.voucher_name,
                  bcp_transactions.voucher_date,
                  bcp_transactions.voucher_line_nbr,
                  bcp_transactions.voucher_line_desc,
                  bcp_transactions.check_nbr,
                  bcp_transactions.check_no,
                  bcp_transactions.check_date,
                  bcp_transactions.check_amt,
                  bcp_transactions.check_desc,
                  bcp_transactions.user_text_01,
                  bcp_transactions.user_text_02,
                  bcp_transactions.user_text_03,
                  bcp_transactions.user_text_04,
                  bcp_transactions.user_text_05,
                  bcp_transactions.user_text_06,
                  bcp_transactions.user_text_07,
                  bcp_transactions.user_text_08,
                  bcp_transactions.user_text_09,
                  bcp_transactions.user_text_10,
                  bcp_transactions.user_text_11,
                  bcp_transactions.user_text_12,
                  bcp_transactions.user_text_13,
                  bcp_transactions.user_text_14,
                  bcp_transactions.user_text_15,
                  bcp_transactions.user_text_16,
                  bcp_transactions.user_text_17,
                  bcp_transactions.user_text_18,
                  bcp_transactions.user_text_19,
                  bcp_transactions.user_text_20,
                  bcp_transactions.user_text_21,
                  bcp_transactions.user_text_22,
                  bcp_transactions.user_text_23,
                  bcp_transactions.user_text_24,
                  bcp_transactions.user_text_25,
                  bcp_transactions.user_text_26,
                  bcp_transactions.user_text_27,
                  bcp_transactions.user_text_28,
                  bcp_transactions.user_text_29,
                  bcp_transactions.user_text_30,
                  bcp_transactions.user_number_01,
                  bcp_transactions.user_number_02,
                  bcp_transactions.user_number_03,
                  bcp_transactions.user_number_04,
                  bcp_transactions.user_number_05,
                  bcp_transactions.user_number_06,
                  bcp_transactions.user_number_07,
                  bcp_transactions.user_number_08,
                  bcp_transactions.user_number_09,
                  bcp_transactions.user_number_10,
                  bcp_transactions.user_date_01,
                  bcp_transactions.user_date_02,
                  bcp_transactions.user_date_03,
                  bcp_transactions.user_date_04,
                  bcp_transactions.user_date_05,
                  bcp_transactions.user_date_06,
                  bcp_transactions.user_date_07,
                  bcp_transactions.user_date_08,
                  bcp_transactions.user_date_09,
                  bcp_transactions.user_date_10,
                  bcp_transactions.comments,
                  bcp_transactions.tb_calc_tax_amt,
                  bcp_transactions.state_use_amount,
                  bcp_transactions.state_use_tier2_amount,
                  bcp_transactions.state_use_tier3_amount,
                  bcp_transactions.county_use_amount,
                  bcp_transactions.county_local_use_amount,
                  bcp_transactions.city_use_amount,
                  bcp_transactions.city_local_use_amount,
                  bcp_transactions.transaction_state_code,
                  bcp_transactions.auto_transaction_state_code,
                  bcp_transactions.transaction_ind,
                  bcp_transactions.suspend_ind,
                  bcp_transactions.taxcode_detail_id,
                  bcp_transactions.taxcode_state_code,
                  bcp_transactions.taxcode_type_code,
                  bcp_transactions.taxcode_code,
                  bcp_transactions.cch_taxcat_code,
                  bcp_transactions.cch_group_code,
                  bcp_transactions.cch_item_code,
                  bcp_transactions.manual_taxcode_ind,
                  bcp_transactions.tax_matrix_id,
                  bcp_transactions.location_matrix_id,
                  bcp_transactions.jurisdiction_id,
                  bcp_transactions.jurisdiction_taxrate_id,
                  bcp_transactions.manual_jurisdiction_ind,
                  bcp_transactions.measure_type_code,
                  bcp_transactions.state_use_rate,
                  bcp_transactions.state_use_tier2_rate,
                  bcp_transactions.state_use_tier3_rate,
                  bcp_transactions.state_split_amount,
                  bcp_transactions.state_tier2_min_amount,
                  bcp_transactions.state_tier2_max_amount,
                  bcp_transactions.state_maxtax_amount,
                  bcp_transactions.county_use_rate,
                  bcp_transactions.county_local_use_rate,
                  bcp_transactions.county_split_amount,
                  bcp_transactions.county_maxtax_amount,
                  bcp_transactions.county_single_flag,
                  bcp_transactions.county_default_flag,
                  bcp_transactions.city_use_rate,
                  bcp_transactions.city_local_use_rate,
                  bcp_transactions.city_split_amount,
                  bcp_transactions.city_split_use_rate,
                  bcp_transactions.city_single_flag,
                  bcp_transactions.city_default_flag,
                  bcp_transactions.combined_use_rate,
                  bcp_transactions.load_timestamp,
                  bcp_transactions.gl_extract_updater,
                  bcp_transactions.gl_extract_timestamp,
                  bcp_transactions.gl_extract_flag,
                  bcp_transactions.gl_log_flag,
                  bcp_transactions.gl_extract_amt,
                  bcp_transactions.audit_flag,
                  bcp_transactions.audit_user_id,
                  bcp_transactions.audit_timestamp,
                  bcp_transactions.modify_user_id,
                  bcp_transactions.modify_timestamp,
                  bcp_transactions.update_user_id,
                  bcp_transactions.update_timestamp );
               -- Error Checking
               IF SQLCODE != 0 THEN
                  RAISE e_badwrite;
               END IF;
            EXCEPTION
               WHEN e_badwrite THEN
                  vi_error_count := vi_error_count + 1;
                  Sp_Geterrorcode('P7', an_batch_id, 'P', v_sysdate, v_severity_level, v_write_import_line_flag, v_abort_import_flag);
                  IF v_severity_level > v_error_sev_code THEN
                     v_error_sev_code := v_severity_level;
                  END IF;
            END;
            -- Exit if aborted
            IF v_abort_import_flag = '1' THEN
               v_batch_status_code := 'ABP';
               RAISE e_abort;
            END IF;
-- 3411 --            END IF;
         END IF;
      END IF;

      -- Check Kill Process Flag -- MBF01 -- begin
      IF v_kill_proc_flag = '1' THEN
         vn_kill_proc_counter := vn_kill_proc_counter + 1;
         IF vn_kill_proc_counter >= vn_kill_proc_count THEN
            SELECT batch_status_code
              INTO vc_batch_status_code
              FROM TB_BATCH
             WHERE batch_id = an_batch_id;
            IF vc_batch_status_code = 'KILLP' THEN
               v_batch_status_code := 'ABP';
               RAISE e_kill;
            END IF;
            vn_kill_proc_counter := 0;
         END IF;
      END IF;
      -- Check Kill Process Flag -- MBF01 -- end

   END LOOP;
   CLOSE bcp_transactions_cursor;
   v_batch_status_code := 'P';

   --- Remove Batch from BCP Transactions Table
   DELETE
     FROM TB_BCP_TRANSACTIONS
    WHERE process_batch_no = an_batch_id;

   -- MBF03 -- Run User Exit?
   BEGIN
      SELECT LTRIM(RTRIM(value))
        INTO v_processuserexit_flag
        FROM TB_OPTION
       WHERE option_code = 'PROCESSUSEREXIT' AND
             option_type_code = 'ADMIN' AND
             user_code = 'ADMIN';
   EXCEPTION
      WHEN OTHERS THEN
         v_processuserexit_flag := '0';
   END;
   IF v_processuserexit_flag IS NULL THEN
      v_processuserexit_flag := '0';
   END IF;
   -- MBF03 - Run User Exit if selected
   IF v_processuserexit_flag = '1' THEN
      vn_return := F_Process_Userexit(an_batch_id);
      IF vn_return < 0 THEN
         vi_error_count := vi_error_count + 1;
         Sp_Geterrorcode('UE4', an_batch_id, 'UE', v_sysdate, v_severity_level, v_write_import_line_flag, v_abort_import_flag);
         IF v_severity_level > v_error_sev_code THEN
            v_error_sev_code := v_severity_level;
         END IF;
      END IF;
   END IF;
   -- MBF03 - User Exit

   -- Update batch with final totals
   UPDATE TB_BATCH
      SET batch_status_code = v_batch_status_code,
          error_sev_code = v_error_sev_code,
          processed_rows = v_processed_rows,
          actual_end_timestamp = SYS_EXTRACT_UTC(SYSTIMESTAMP),
          nu02 = vn_actual_rowno,
          nu03 = v_transaction_amt
    WHERE batch_id = an_batch_id;

    COMMIT;

EXCEPTION
   WHEN e_abort THEN
      ROLLBACK;
      -- Update batch with error codes
      UPDATE TB_BATCH
         SET batch_status_code = v_batch_status_code,
             error_sev_code = v_error_sev_code
       WHERE batch_id = an_batch_id;
      COMMIT;

   -- Check Kill Process Flag -- MBF01 -- begin
   WHEN e_kill THEN
      ROLLBACK;
      -- Write error to errorlog
      Sp_Geterrorcode('P66', an_batch_id, 'P', v_sysdate, v_severity_level, v_write_import_line_flag, v_abort_import_flag);
      IF v_severity_level > v_error_sev_code THEN
         v_error_sev_code := v_severity_level;
      END IF;
      -- Update batch with error codes
      UPDATE TB_BATCH
         SET batch_status_code = v_batch_status_code,
             error_sev_code = v_error_sev_code
       WHERE batch_id = an_batch_id;
      COMMIT;
   -- Check Kill Process Flag -- MBF01 -- end
END;
/

CREATE OR REPLACE PROCEDURE Sp_Batch_Taxrate_Update(an_batch_id NUMBER)
/* ************************************************************************************************/
/* Object Type/Name: Stored Procedure - sp_batch_taxrate_update                                   */
/* Author:           Michael B. Fuller                                                            */
/* Date:             a long time ago                                                              */
/* Description:      process a batch of tax rate updates                                          */
/* Arguments:        an_batch_id number                                                           */
/* Returns:          None                                                                         */
/* ************************************************************************************************/
/* Maintenance History:                                                                           */
/* Scan  Author       Date     Release    Description                                  Ticket #   */
/* ----- ---------- ---------- ---------- -------------------------------------------- ---------- */
/* MBF01 M. Fuller  01/07/2010 5.1.1.1    Add code to get next sequences               4597       */
/* ************************************************************************************************/
IS
-- Table defined variables
   v_batch_status_code             TB_BATCH.batch_status_code%TYPE                   := NULL;
   v_error_sev_code                TB_BATCH.error_sev_code%TYPE                      := ' ';
   v_starting_sequence             TB_BATCH.start_row%TYPE                           := 0;
   v_process_count                 TB_BATCH.start_row%TYPE                           := 0;
   v_processed_rows                TB_BATCH.start_row%TYPE                           := 0;
   v_batch_error_log_id            TB_BATCH_ERROR_LOG.batch_error_log_id%TYPE        := 0;
   v_severity_level                TB_LIST_CODE.severity_level%TYPE                  := ' ';
   v_write_import_line_flag        TB_LIST_CODE.write_import_line_flag%TYPE          := '1';
   v_abort_import_flag             TB_LIST_CODE.abort_import_flag%TYPE               := '0';
   v_jurisdiction_id               TB_JURISDICTION.jurisdiction_id%TYPE              := NULL;
   v_jur_custom_flag               TB_JURISDICTION.custom_flag%TYPE                  := NULL;
   v_jurisdiction_taxrate_id       TB_JURISDICTION_TAXRATE.jurisdiction_taxrate_id%TYPE := NULL;
   v_rate_custom_flag              TB_JURISDICTION_TAXRATE.custom_flag%TYPE          := NULL;
   v_rate_modified_flag            TB_JURISDICTION_TAXRATE.modified_flag%TYPE        := NULL;
   v_sysdate                       TB_BCP_TRANSACTIONS.load_timestamp%TYPE           := SYS_EXTRACT_UTC(SYSTIMESTAMP);

-- Program defined variables
   vc_taxrate_update_type          VARCHAR(10)                                       := NULL;
   vi_release_number               INTEGER                                           := 0;
   vi_sessions                     INTEGER                                           := 0;
   vd_update_date                  DATE                                              := NULL;
   vi_cursor_id                    INTEGER                                           := 0;
   vi_error_count                  INTEGER                                           := 0;

-- Define BCP Jurisdiction TaxRate Updates Cursor (tb_bcp_jurisdiction_taxrate)
   CURSOR bcp_juris_taxrate_cursor
   IS
      SELECT
         batch_id,
         geocode,
         zip,
         state,
         county,
         city,
         zipplus4,
         in_out,
         state_sales_rate,
         state_use_rate,
         county_sales_rate,
         county_use_rate,
         county_local_sales_rate,
         county_local_use_rate,
         county_split_amount,
         county_maxtax_amount,
         county_single_flag,
         county_default_flag,
         city_sales_rate,
         city_use_rate,
         city_local_sales_rate,
         city_local_use_rate,
         city_split_amount,
         city_split_sales_rate,
         city_split_use_rate,
         city_single_flag,
         city_default_flag,
         effective_date
    FROM TB_BCP_JURISDICTION_TAXRATE
   WHERE batch_id = an_batch_id;
   bcp                             bcp_juris_taxrate_cursor%ROWTYPE;

-- Define Exceptions
   e_badread                       EXCEPTION;
   e_badupdate                     EXCEPTION;
   e_badwrite                      EXCEPTION;
   e_wrongdata                     EXCEPTION;
   e_abort                         EXCEPTION;
   e_halt                          EXCEPTION;

-- ****** Program starts ****** --------------------------------------------------------------------
BEGIN
   -- Confirm batch exists and is flagged for processing
   BEGIN
      SELECT batch_status_code
        INTO v_batch_status_code
        FROM TB_BATCH
       WHERE batch_id = an_batch_id;
      IF SQLCODE != 0 THEN
         RAISE e_badread;
      ELSIF v_batch_status_code != 'FTR' THEN
         RAISE e_wrongdata;
      END IF;
   EXCEPTION
      WHEN NO_DATA_FOUND OR e_badread THEN
         vi_error_count := vi_error_count + 1;
         Sp_Geterrorcode('TR8', an_batch_id, 'TR', v_sysdate, v_severity_level, v_write_import_line_flag, v_abort_import_flag);
         IF v_severity_level > v_error_sev_code THEN
            v_error_sev_code := v_severity_level;
         END IF;
      WHEN e_wrongdata THEN
         vi_error_count := vi_error_count + 1;
         Sp_Geterrorcode('TR9', an_batch_id, 'TR', v_sysdate, v_severity_level, v_write_import_line_flag, v_abort_import_flag);
         IF v_severity_level > v_error_sev_code THEN
            v_error_sev_code := v_severity_level;
         END IF;
   END;
   -- Abort Procedure?
   IF v_abort_import_flag = '1' THEN
      v_batch_status_code := 'ABTR';
      RAISE e_abort;
   END IF;

   -- Check for Users in STS??
   -- Take this out for now -- 3.3.4.1
   /* SELECT count(*)
     INTO vi_sessions
     FROM v$session
    WHERE audsid <> 0
      AND audsid NOT IN
         (SELECT sys_context('USERENV','SESSIONID') FROM DUAL);
   IF vi_sessions > 0 Then
      RAISE e_halt;
   END IF; */

   -- Disable triggers
   --EXECUTE IMMEDIATE 'ALTER TRIGGER tb_jurisdiction_taxrate_a_i DISABLE';

   -- Get starting sequence no.
   BEGIN
      SELECT last_number
        INTO v_starting_sequence
        FROM user_sequences
       WHERE sequence_name = 'SQ_TB_PROCESS_COUNT';
   EXCEPTION
      WHEN OTHERS THEN
         NULL;
   END;

   -- Get batch information
   SELECT UPPER(VC02),
          NU01,
          TS01
     INTO vc_taxrate_update_type,
          vi_release_number,
          vd_update_date
     FROM TB_BATCH
    WHERE batch_id = an_batch_id;

   -- Update batch as processing
   BEGIN
      UPDATE TB_BATCH
         SET batch_status_code = 'XTR',
             error_sev_code = '',
             start_row = v_starting_sequence,
             actual_start_timestamp = v_sysdate
       WHERE batch_id = an_batch_id;
      IF SQLCODE != 0 THEN
         RAISE e_badupdate;
      END IF;
   EXCEPTION
      WHEN e_badupdate THEN
         vi_error_count := vi_error_count + 1;
         Sp_Geterrorcode('TR1', an_batch_id, 'TR', v_sysdate, v_severity_level, v_write_import_line_flag, v_abort_import_flag);
         IF v_severity_level > v_error_sev_code THEN
            v_error_sev_code := v_severity_level;
         END IF;
   END;
   -- Abort Procedure?
   IF v_abort_import_flag = '1' THEN
      v_batch_status_code := 'ABTR';
      RAISE e_abort;
   END IF;

   COMMIT;

   -- ****** Read and Process Transactions ****** -----------------------------------------
   OPEN bcp_juris_taxrate_cursor;
   LOOP
      FETCH bcp_juris_taxrate_cursor INTO bcp;
      EXIT WHEN bcp_juris_taxrate_cursor%NOTFOUND;

      -- Increment Process Count
      SELECT sq_tb_process_count.NEXTVAL
        INTO v_process_count
        FROM DUAL;
      v_processed_rows := v_processed_rows + 1;

      -- Find jurisdiction
      BEGIN
         SELECT jur.jurisdiction_id,
                jur.custom_flag
           INTO v_jurisdiction_id,
                v_jur_custom_flag
           FROM TB_JURISDICTION jur
          WHERE ( jur.geocode = bcp.geocode ) AND
                ( jur.state = bcp.state ) AND
                ( jur.county = bcp.county ) AND
                ( jur.city = bcp.city ) AND
                ( jur.zip = bcp.zip );
      EXCEPTION
         WHEN OTHERS THEN
            v_jurisdiction_id := 0;
            v_jur_custom_flag := '0';
      END;

      -- If it does not exist, add it
      IF v_jurisdiction_id IS NULL OR v_jurisdiction_id = 0 THEN
         -- Insert new jurisdiction
         SELECT sq_tb_jurisdiction_id.NEXTVAL
           INTO v_jurisdiction_id
           FROM DUAL;

         INSERT INTO TB_JURISDICTION(
            jurisdiction_id,
            geocode,
            state,
            county,
            city,
            zip,
            zipplus4,
            in_out,
            custom_flag,
            description )
         VALUES(
            v_jurisdiction_id,
            bcp.geocode,
            bcp.state,
            bcp.county,
            bcp.city,
            bcp.zip,
            bcp.zipplus4,
            bcp.in_out,
            '0',
            'Second Decimal Supplied Jurisdiction/Tax Rate' );
         -- Get last inserted jurisdiction id
         --SELECT MAX(jurisdiction_id)
         --  INTO v_jurisdiction_id
         --  FROM TB_JURISDICTION;
      ELSE
         -- If it does exist and is custom then write errog log warning
         IF v_jur_custom_flag = '1' THEN
            v_error_sev_code := '10';
            SELECT sq_tb_batch_error_log_id.NEXTVAL
              INTO v_batch_error_log_id
              FROM DUAL;

            INSERT INTO TB_BATCH_ERROR_LOG (
               batch_error_log_id,
               batch_id,
               process_type,
               process_timestamp,
               row_no,
               column_no,
               error_def_code,
               import_header_column,
               import_column_value,
               trans_dtl_column_name,
               trans_dtl_datatype,
               import_row )
            VALUES (
               v_batch_error_log_id,
               bcp.batch_id,
               'TR',
               v_sysdate,
               ROWNUM,
               0,
               'TR1',
               'GeoCode|State|County|City|Zip',
               bcp.geocode || '|' || bcp.state || '|' || bcp.county || '|' || bcp.city || '|' || bcp.zip,
               'n/a',
               'n/a',
               'n/a' );
         END IF;
      END IF;

      -- Find jurisdiction taxrate
      BEGIN
         SELECT rate.jurisdiction_taxrate_id,
                rate.custom_flag,
                rate.modified_flag
           INTO v_jurisdiction_taxrate_id,
                v_rate_custom_flag,
                v_rate_modified_flag
           FROM TB_JURISDICTION_TAXRATE rate
          WHERE ( rate.jurisdiction_id = v_jurisdiction_id ) AND
                ( rate.effective_date = bcp.effective_date ) AND
                ( rate.measure_type_code = '0' );
      EXCEPTION
         WHEN OTHERS THEN
            v_jurisdiction_taxrate_id := 0;
            v_rate_custom_flag := '0';
            v_rate_modified_flag := '0';
      END;

      -- If it does not exist, add it
      IF v_jurisdiction_taxrate_id IS NULL OR v_jurisdiction_taxrate_id = 0 THEN
         SELECT sq_tb_jurisdiction_taxrate_id.NEXTVAL
           INTO v_jurisdiction_taxrate_id
           FROM DUAL;

         INSERT INTO TB_JURISDICTION_TAXRATE(
            jurisdiction_taxrate_id,
            jurisdiction_id,
            measure_type_code,
            effective_date,
            expiration_date,
            custom_flag,
            modified_flag,
            state_sales_rate,
            state_use_rate,
            county_sales_rate,
            county_use_rate,
            county_local_sales_rate,
            county_local_use_rate,
            county_split_amount,
            county_maxtax_amount,
            county_single_flag,
            county_default_flag,
            city_sales_rate,
            city_use_rate,
            city_local_sales_rate,
            city_local_use_rate,
            city_split_amount,
            city_split_sales_rate,
            city_split_use_rate,
            city_single_flag,
            city_default_flag )
         VALUES(
            v_jurisdiction_taxrate_id,
            v_jurisdiction_id,
            '0',
            bcp.effective_date,
            TO_DATE('12/31/9999', 'mm/dd/yyyy'),
            '0',
            '0',
            bcp.state_sales_rate,
            bcp.state_use_rate,
            bcp.county_sales_rate,
            bcp.county_use_rate,
            bcp.county_local_sales_rate,
            bcp.county_local_use_rate,
            bcp.county_split_amount,
            bcp.county_maxtax_amount,
            bcp.county_single_flag,
            bcp.county_default_flag,
            bcp.city_sales_rate,
            bcp.city_use_rate,
            bcp.city_local_sales_rate,
            bcp.city_local_use_rate,
            bcp.city_split_amount,
            bcp.city_split_sales_rate,
            bcp.city_split_use_rate,
            bcp.city_single_flag,
            bcp.city_default_flag );
      ELSE
         -- If it does exist and jurisdiction is not custom but rate is custom (modified or not) then write errog log warning
         IF (( v_jur_custom_flag IS NULL ) OR ( v_jur_custom_flag = '0' )) AND
            ( v_rate_custom_flag = '1' ) THEN
            v_error_sev_code := '10';
            SELECT sq_tb_batch_error_log_id.NEXTVAL
              INTO v_batch_error_log_id
              FROM DUAL;

            INSERT INTO TB_BATCH_ERROR_LOG (
               batch_error_log_id,
               batch_id,
               process_type,
               process_timestamp,
               row_no,
               column_no,
               error_def_code,
               import_header_column,
               import_column_value,
               trans_dtl_column_name,
               trans_dtl_datatype,
               import_row )
            VALUES (
               v_batch_error_log_id,
               bcp.batch_id,
               'TR',
               v_sysdate,
               ROWNUM,
               0,
               'TR2',
               'GeoCode|State|County|City|Zip|Effective Date',
               bcp.geocode || '|' || bcp.state || '|' || bcp.county || '|' || bcp.city || '|' || bcp.zip || '|' || TO_CHAR(bcp.effective_date, 'mm/dd/yyyy'),
               'n/a',
               'n/a',
               'n/a' );
         ELSE
            -- If it does exist and jurisdiction is not custom and rate is not custom but rate is modified then write error log warning
            IF (( v_jur_custom_flag IS NULL ) OR ( v_jur_custom_flag = '0' )) AND
               (( v_rate_custom_flag IS NULL ) OR ( v_rate_custom_flag = '0' )) AND
               ( v_rate_modified_flag = '1' ) THEN
               v_error_sev_code := '10';
               SELECT sq_tb_batch_error_log_id.NEXTVAL
                 INTO v_batch_error_log_id
                 FROM DUAL;

               INSERT INTO TB_BATCH_ERROR_LOG (
                  batch_error_log_id,
                  batch_id,
                  process_type,
                  process_timestamp,
                  row_no,
                  column_no,
                  error_def_code,
                  import_header_column,
                  import_column_value,
                  trans_dtl_column_name,
                  trans_dtl_datatype,
                  import_row )
               VALUES (
                  v_batch_error_log_id,
                  bcp.batch_id,
                  'TR',
                  v_sysdate,
                  ROWNUM,
                  0,
                  'TR3',
                  'GeoCode|State|County|City|Zip|Effective Date',
                  bcp.geocode || '|' || bcp.state || '|' || bcp.county || '|' || bcp.city || '|' || bcp.zip || '|' || TO_CHAR(bcp.effective_date, 'mm/dd/yyyy'),
                  'n/a',
                  'n/a',
                  'n/a' );
            END IF;
         END IF;
         -- Update TaxRate
         UPDATE TB_JURISDICTION_TAXRATE rate SET
            rate.modified_flag = 0,
            rate.state_sales_rate = bcp.state_sales_rate,
            rate.state_use_rate =  bcp.state_use_rate,
            rate.county_sales_rate = bcp.county_sales_rate,
            rate.county_use_rate = bcp.county_use_rate,
            rate.county_local_sales_rate = bcp.county_local_sales_rate,
            rate.county_local_use_rate = bcp.county_local_use_rate,
            rate.county_split_amount = bcp.county_split_amount,
            rate.county_maxtax_amount = bcp.county_maxtax_amount,
            rate.county_single_flag = bcp.county_single_flag,
            rate.county_default_flag = bcp.county_default_flag,
            rate.city_sales_rate = bcp.city_sales_rate,
            rate.city_use_rate = bcp.city_use_rate,
            rate.city_local_sales_rate = bcp.city_local_sales_rate,
            rate.city_local_use_rate = bcp.city_local_use_rate,
            rate.city_split_amount = bcp.city_split_amount,
            rate.city_split_sales_rate = bcp.city_split_sales_rate,
            rate.city_split_use_rate = bcp.city_split_use_rate,
            rate.city_single_flag = bcp.city_single_flag,
            rate.city_default_flag = bcp.city_default_flag
         WHERE rate.jurisdiction_taxrate_id = v_jurisdiction_taxrate_id;
      END IF;
   END LOOP;
   CLOSE bcp_juris_taxrate_cursor;

   -- Remove Batch from BCP Transactions Table
   DELETE
     FROM TB_BCP_JURISDICTION_TAXRATE
    WHERE batch_id = an_batch_id;

   --  Update Options table with last month/year updated
   UPDATE TB_OPTION
      SET value = TO_CHAR(vd_update_date, 'yyyy_mm')
    WHERE option_code = 'LASTTAXRATEDATEUPDATE' AND
          option_type_code = 'SYSTEM' AND
          user_code = 'SYSTEM';

   -- Update Options table with last release number
   UPDATE TB_OPTION
      SET value = vi_release_number
    WHERE option_code = 'LASTTAXRATERELUPDATE' AND
          option_type_code = 'SYSTEM' AND
          user_code = 'SYSTEM';

   -- Update batch with final totals
   UPDATE TB_BATCH
      SET batch_status_code = 'TR',
          error_sev_code = v_error_sev_code,
          processed_rows = v_processed_rows,
          actual_end_timestamp = SYS_EXTRACT_UTC(SYSTIMESTAMP)
    WHERE batch_id = an_batch_id;

   -- Update CCH Code table
   --DELETE FROM TB_CCH_CODE WHERE custom_flag <> '1';

   /*INSERT INTO tb_cch_code (
      filename,
      fieldname,
      fieldpos,
      fieldlen,
      fieldno,
      code,
      description,
      custom_flag,
      modified_flag )
   SELECT
      filename,
      fieldname,
      fieldpos,
      fieldlen,
      fieldno,
      code,
      description,
      '0',
      '0'
   FROM tb_bcp_cch_code
   WHERE batch_id = an_batch_id;*/

   /*DELETE
     FROM tb_bcp_cch_code
    WHERE batch_id = an_batch_id;*/

   -- Update CCH TxMatrix table
   --DELETE FROM TB_CCH_TXMATRIX WHERE custom_flag <> '1';

   /*INSERT INTO tb_cch_txmatrix (
      city,
      county,
      state,
      local,
      geocode,
      groupcode,
      groupdesc,
      item,
      itemdesc,
      provider,
      customer,
      taxtypet,
      taxcatt,
      taxable,
      taxtype,
      taxcat,
      effdate,
      rectype,
      custom_flag,
      modified_flag )
   SELECT
      city,
      county,
      state,
      local,
      geocode,
      groupcode,
      groupdesc,
      item,
      itemdesc,
      provider,
      customer,
      taxtypet,
      taxcatt,
      taxable,
      taxtype,
      taxcat,
      to_date(effdate,'yyyymmdd'),
      rectype,
     '0',
     '0'
   FROM tb_bcp_cch_txmatrix
   WHERE batch_id = an_batch_id;*/

   /*DELETE
     FROM tb_bcp_cch_txmatrix
    WHERE batch_id = an_batch_id;*/

   -- commit all updates
   COMMIT;

   -- Enable triggers
   --EXECUTE IMMEDIATE 'ALTER TRIGGER tb_jurisdiction_taxrate_a_i ENABLE';

EXCEPTION
   WHEN e_abort THEN
      -- Update batch with error codes
      UPDATE TB_BATCH
         SET batch_status_code = v_batch_status_code,
             error_sev_code = v_error_sev_code,
             actual_end_timestamp = SYS_EXTRACT_UTC(SYSTIMESTAMP)
       WHERE batch_id = an_batch_id;
      COMMIT;
      -- Enable triggers
      --EXECUTE IMMEDIATE 'ALTER TRIGGER tb_jurisdiction_taxrate_a_i ENABLE';
   WHEN e_halt THEN
      NULL;
END;
/

CREATE OR REPLACE PROCEDURE Sp_Import_Allocation(an_batch_id NUMBER)
IS
-- Moved Allocations to the tb_batch_process stored procedure -- 01/25/2007 -- MBF01
-- Corrected Rounded Correction -- 02/09/2007 -- MBF02
-- Add Allocation Rows to Total Imported Rows -- 02/13/2007 -- MBF03
-- Define Cursor Type
   TYPE cursor_type IS REF CURSOR;

-- Allocation Matrix Selection SQL
   vc_allocation_matrix_select       VARCHAR2(4000) :=
      'SELECT tb_allocation_matrix.allocation_matrix_id ' ||
       'FROM tb_allocation_matrix, ' ||
            'tb_bcp_transactions ' ||
      'WHERE ( tb_bcp_transactions.rowid = :v_rowid ) AND ' ||
            '( tb_allocation_matrix.effective_date <= tb_bcp_transactions.gl_date ) AND ( tb_allocation_matrix.expiration_date >= tb_bcp_transactions.gl_date ) ';
   vc_allocation_matrix_where        VARCHAR2(4000) := '';
   vc_allocation_matrix_orderby      VARCHAR2(1000) :=
      'ORDER BY tb_allocation_matrix.binary_weight DESC, tb_allocation_matrix.significant_digits DESC, tb_allocation_matrix.effective_date DESC';
   vc_allocation_matrix_stmt         VARCHAR2 (9000);
   allocation_matrix_cursor          cursor_type;

-- Location Matrix Record TYPE
   TYPE allocation_matrix_record IS RECORD (
      allocation_matrix_id         TB_ALLOCATION_MATRIX.allocation_matrix_id%TYPE );
   allocation_matrix               allocation_matrix_record;

-- Table defined variables
   v_value                         TB_OPTION.value%TYPE                              := NULL;
   v_batch_status_code             TB_BATCH.batch_status_code%TYPE                   := 'P';
   v_starting_sequence             TB_BATCH.start_row%TYPE                           := 0;
   v_error_sev_code                TB_BATCH.error_sev_code%TYPE                      := ' ';
   v_total_rows                    TB_BATCH.total_rows%TYPE                          := 0;
   v_severity_level                TB_LIST_CODE.severity_level%TYPE                  := ' ';
   v_write_import_line_flag        TB_LIST_CODE.write_import_line_flag%TYPE          := '1';
   v_abort_import_flag             TB_LIST_CODE.abort_import_flag%TYPE               := '0';
   v_allocation_matrix_id          TB_ALLOCATION_MATRIX.allocation_matrix_id%TYPE    := 0;
   v_transaction_detail_id         TB_BCP_TRANSACTIONS.transaction_detail_id%TYPE    := 0;
   v_sysdate                       TB_BCP_TRANSACTIONS.load_timestamp%TYPE           := SYS_EXTRACT_UTC(SYSTIMESTAMP);
   v_accum_allocation              TB_BCP_TRANSACTIONS.gl_line_itm_dist_amt%TYPE     := 0;
   v_gl_line_itm_dist_amt          TB_BCP_TRANSACTIONS.gl_line_itm_dist_amt%TYPE     := 0;
   v_orig_gl_line_itm_dist_amt     TB_BCP_TRANSACTIONS.gl_line_itm_dist_amt%TYPE     := 0;
   v_transaction_state_code        TB_BCP_TRANSACTIONS.transaction_state_code%TYPE   := NULL;
   v_delta                         TB_BCP_TRANSACTIONS.gl_line_itm_dist_amt%TYPE     := 0;

-- Program defined variables
   v_rowid                         ROWID                                             := NULL;
   vn_fetch_rows                   NUMBER                                            := 0;
   vn_allocation_total_rows        NUMBER                                            := 0;
   vi_error_count                  INTEGER                                           := 0;

-- Define Tax Driver Names Cursor (tb_driver_names)
   CURSOR tax_driver_cursor
   IS
        SELECT driver.trans_dtl_column_name, driver.matrix_column_name, driver.null_driver_flag, driver.wildcard_flag,
               datadefcol.desc_column_name, datadefcol.data_type
          FROM TB_DRIVER_NAMES driver,
               (SELECT *
                  FROM TB_DATA_DEF_COLUMN
                 WHERE table_name = 'TB_TRANSACTION_DETAIL') datadefcol

         WHERE driver.driver_names_code = 'T' AND
               driver.trans_dtl_column_name = datadefcol.column_name
      ORDER BY driver.driver_id;
      tax_driver                   tax_driver_cursor%ROWTYPE;

-- Define Allocation Matrix Detail / Jurisdiction Cursor (tb_allocation_matrix_detail, tb_jurisdiction)
   CURSOR allocation_detail_cursor
   IS
        SELECT TB_ALLOCATION_MATRIX_DETAIL.jurisdiction_id, TB_ALLOCATION_MATRIX_DETAIL.allocation_percent,
               TB_JURISDICTION.geocode, TB_JURISDICTION.state, TB_JURISDICTION.county, TB_JURISDICTION.city, TB_JURISDICTION.zip
          FROM TB_ALLOCATION_MATRIX_DETAIL,
	       TB_JURISDICTION
         WHERE ( TB_ALLOCATION_MATRIX_DETAIL.jurisdiction_id = TB_JURISDICTION.jurisdiction_id (+))
           AND ( TB_ALLOCATION_MATRIX_DETAIL.allocation_matrix_id = v_allocation_matrix_id );
      allocation_detail                   allocation_detail_cursor%ROWTYPE;

-- Define BCP Transactions Cursor (tb_bcp_transactions)
   CURSOR bcp_transactions_cursor
   IS
      SELECT ROWID,
             transaction_detail_id,
             source_transaction_id,
             process_batch_no,
             gl_extract_batch_no,
             archive_batch_no,
             allocation_matrix_id,
             allocation_subtrans_id,
             entered_date,
             transaction_status,
             gl_date,
             gl_company_nbr,
             gl_company_name,
             gl_division_nbr,
             gl_division_name,
             gl_cc_nbr_dept_id,
             gl_cc_nbr_dept_name,
             gl_local_acct_nbr,
             gl_local_acct_name,
             gl_local_sub_acct_nbr,
             gl_local_sub_acct_name,
             gl_full_acct_nbr,
             gl_full_acct_name,
             gl_line_itm_dist_amt,
             orig_gl_line_itm_dist_amt,
             vendor_nbr,
             vendor_name,
             vendor_address_line_1,
             vendor_address_line_2,
             vendor_address_line_3,
             vendor_address_line_4,
             vendor_address_city,
             vendor_address_county,
             vendor_address_state,
             vendor_address_zip,
             vendor_address_country,
             vendor_type,
             vendor_type_name,
             invoice_nbr,
             invoice_desc,
             invoice_date,
             invoice_freight_amt,
             invoice_discount_amt,
             invoice_tax_amt,
             invoice_total_amt,
             invoice_tax_flg,
             invoice_line_nbr,
             invoice_line_name,
             invoice_line_type,
             invoice_line_type_name,
             invoice_line_amt,
             invoice_line_tax,
             afe_project_nbr,
             afe_project_name,
             afe_category_nbr,
             afe_category_name,
             afe_sub_cat_nbr,
             afe_sub_cat_name,
             afe_use,
             afe_contract_type,
             afe_contract_structure,
             afe_property_cat,
             inventory_nbr,
             inventory_name,
             inventory_class,
             inventory_class_name,
             po_nbr,
             po_name,
             po_date,
             po_line_nbr,
             po_line_name,
             po_line_type,
             po_line_type_name,
             ship_to_location,
             ship_to_location_name,
             ship_to_address_line_1,
             ship_to_address_line_2,
             ship_to_address_line_3,
             ship_to_address_line_4,
             ship_to_address_city,
             ship_to_address_county,
             ship_to_address_state,
             ship_to_address_zip,
             ship_to_address_country,
             wo_nbr,
             wo_name,
             wo_date,
             wo_type,
             wo_type_desc,
             wo_class,
             wo_class_desc,
             wo_entity,
             wo_entity_desc,
             wo_line_nbr,
             wo_line_name,
             wo_line_type,
             wo_line_type_desc,
             wo_shut_down_cd,
             wo_shut_down_cd_desc,
             voucher_id,
             voucher_name,
             voucher_date,
             voucher_line_nbr,
             voucher_line_desc,
             check_nbr,
             check_no,
             check_date,
             check_amt,
             check_desc,
             user_text_01,
             user_text_02,
             user_text_03,
             user_text_04,
             user_text_05,
             user_text_06,
             user_text_07,
             user_text_08,
             user_text_09,
             user_text_10,
             user_text_11,
             user_text_12,
             user_text_13,
             user_text_14,
             user_text_15,
             user_text_16,
             user_text_17,
             user_text_18,
             user_text_19,
             user_text_20,
             user_text_21,
             user_text_22,
             user_text_23,
             user_text_24,
             user_text_25,
             user_text_26,
             user_text_27,
             user_text_28,
             user_text_29,
             user_text_30,
             user_number_01,
             user_number_02,
             user_number_03,
             user_number_04,
             user_number_05,
             user_number_06,
             user_number_07,
             user_number_08,
             user_number_09,
             user_number_10,
             user_date_01,
             user_date_02,
             user_date_03,
             user_date_04,
             user_date_05,
             user_date_06,
             user_date_07,
             user_date_08,
             user_date_09,
             user_date_10,
             comments,
             tb_calc_tax_amt,
             state_use_amount,
             state_use_tier2_amount,
             state_use_tier3_amount,
             county_use_amount,
             county_local_use_amount,
             city_use_amount,
             city_local_use_amount,
             transaction_state_code,
             auto_transaction_state_code,
             transaction_ind,
             suspend_ind,
             taxcode_detail_id,
             taxcode_state_code,
             taxcode_type_code,
             taxcode_code,
             cch_taxcat_code,
             cch_group_code,
             cch_item_code,
             manual_taxcode_ind,
             tax_matrix_id,
             location_matrix_id,
             jurisdiction_id,
             jurisdiction_taxrate_id,
             manual_jurisdiction_ind,
             measure_type_code,
             state_use_rate,
             state_use_tier2_rate,
             state_use_tier3_rate,
             state_split_amount,
             state_tier2_min_amount,
             state_tier2_max_amount,
             state_maxtax_amount,
             county_use_rate,
             county_local_use_rate,
             county_split_amount,
             county_maxtax_amount,
             county_single_flag,
             county_default_flag,
             city_use_rate,
             city_local_use_rate,
             city_split_amount,
             city_split_use_rate,
             city_single_flag,
             city_default_flag,
             combined_use_rate,
             load_timestamp,
             gl_extract_updater,
             gl_extract_timestamp,
             gl_extract_flag,
             gl_log_flag,
             gl_extract_amt,
             audit_flag,
             audit_user_id,
             audit_timestamp,
             modify_user_id,
             modify_timestamp,
             update_user_id,
             update_timestamp
        FROM TB_BCP_TRANSACTIONS
       WHERE process_batch_no = an_batch_id;
      bcp_transactions             bcp_transactions_cursor%ROWTYPE;

-- Define Exceptions
   e_badread                       EXCEPTION;
   e_badupdate                     EXCEPTION;
   e_badwrite                      EXCEPTION;
   e_wrongdata                     EXCEPTION;
   e_abort                         EXCEPTION;

-- ****** Program starts ****** --------------------------------------------------------------------
BEGIN
   -- Confirm allocations are active
   BEGIN
      SELECT value
        INTO v_value
        FROM TB_OPTION
       WHERE option_code = 'ALLOCATIONSENABLED'
         AND option_type_code = 'ADMIN'
         AND user_code = 'ADMIN';
      IF SQLCODE != 0 THEN
         RAISE e_badread;
      ELSIF v_value <> '1' THEN
         RAISE e_wrongdata;
      END IF;
   EXCEPTION
      WHEN NO_DATA_FOUND OR e_badread OR e_wrongdata THEN
         --insert into tb_debug(row_joe) values('STOP');
         RAISE e_abort;
   END;

   -- Confirm batch exists and is being imported
   -- Confirm batch exists and is flagged for process -- 01/25/2007 -- MBF01
   BEGIN
      SELECT batch_status_code, total_rows
        INTO v_batch_status_code, v_total_rows
        FROM TB_BATCH
       WHERE batch_id = an_batch_id;
      IF SQLCODE != 0 THEN
         RAISE e_badread;
      ELSIF v_batch_status_code != 'FP' THEN
         RAISE e_wrongdata;
      END IF;
   EXCEPTION
      WHEN NO_DATA_FOUND OR e_badread THEN
         vi_error_count := vi_error_count + 1;
         Sp_Geterrorcode('AL1', an_batch_id, 'AL', v_sysdate, v_severity_level, v_write_import_line_flag, v_abort_import_flag);
         IF v_severity_level > v_error_sev_code THEN
            v_error_sev_code := v_severity_level;
         END IF;
      WHEN e_wrongdata THEN
         vi_error_count := vi_error_count + 1;
         Sp_Geterrorcode('AL2', an_batch_id, 'AL', v_sysdate, v_severity_level, v_write_import_line_flag, v_abort_import_flag);
         IF v_severity_level > v_error_sev_code THEN
            v_error_sev_code := v_severity_level;
         END IF;
   END;
   -- Abort Procedure?
   IF v_abort_import_flag = '1' THEN
      v_batch_status_code := 'ABAL';
      RAISE e_abort;
   END IF;

   -- Get starting sequence no.
   BEGIN
      SELECT last_number
        INTO v_starting_sequence
        FROM user_sequences
       WHERE sequence_name = 'SQ_TB_ALLOCATION_MATRIX_ID';
   EXCEPTION
      WHEN OTHERS THEN
         NULL;
   END;

   -- Update batch
   BEGIN
      UPDATE TB_BATCH
         SET batch_status_code = 'XAL',
             error_sev_code = '',
             nu04 = v_total_rows
       WHERE batch_id = an_batch_id;
      IF SQLCODE != 0 THEN
         RAISE e_badupdate;
      END IF;
   EXCEPTION
      WHEN e_badupdate THEN
         vi_error_count := vi_error_count + 1;
         Sp_Geterrorcode('AL3', an_batch_id, 'P', v_sysdate, v_severity_level, v_write_import_line_flag, v_abort_import_flag);
         IF v_severity_level > v_error_sev_code THEN
            v_error_sev_code := v_severity_level;
         END IF;
   END;
   -- Abort Procedure?
   IF v_abort_import_flag = '1' THEN
      v_batch_status_code := 'ABAL';
      RAISE e_abort;
   END IF;

   -- ***** Create Dynamic WHERE Clause for Matrix SQLs ***** --
   -- Allocation Matrix Drivers
   OPEN tax_driver_cursor;
   LOOP
      FETCH tax_driver_cursor INTO tax_driver;
      EXIT WHEN tax_driver_cursor%NOTFOUND;
      IF tax_driver.null_driver_flag = '1' THEN
         IF tax_driver.wildcard_flag = '1' THEN
            IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
               UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                  vc_allocation_matrix_where := vc_allocation_matrix_where || 'AND ' ||
                  '((tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ' IS NULL AND (tb_allocation_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR tb_allocation_matrix.' || tax_driver.matrix_column_name || ' = ''*NULL'' )) OR ' ||
                  '(tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND (tb_allocation_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ' LIKE tb_allocation_matrix.' || tax_driver.matrix_column_name || '))) ';
            END IF;
         ELSE
            IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
               UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                  vc_allocation_matrix_where := vc_allocation_matrix_where || 'AND ' ||
                  '((tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ' IS NULL AND (tb_allocation_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR tb_allocation_matrix.' || tax_driver.matrix_column_name || ' = ''*NULL'' )) OR ' ||
                  '(tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND (tb_allocation_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ' = tb_allocation_matrix.' || tax_driver.matrix_column_name || '))) ';
            END IF;
         END IF;
      ELSE
         IF tax_driver.wildcard_flag = '1' THEN
            IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
               UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                  vc_allocation_matrix_where := vc_allocation_matrix_where || 'AND ' ||
                  '(tb_allocation_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ' LIKE tb_allocation_matrix.' || tax_driver.matrix_column_name || ') ';
            END IF;
         ELSE
            IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
               UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                  vc_allocation_matrix_where := vc_allocation_matrix_where || 'AND ' ||
                  '(tb_allocation_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR tb_bcp_transactions.' || tax_driver.trans_dtl_column_name || ' = tb_allocation_matrix.' || tax_driver.matrix_column_name || ') ';
            END IF;
         END IF;
      END IF;
   END LOOP;
   CLOSE tax_driver_cursor;
   -- If no drivers found raise error, else create location matrix sql statement
   IF vc_allocation_matrix_where IS NULL THEN
      vi_error_count := vi_error_count + 1;
      Sp_Geterrorcode('AL3', an_batch_id, 'AL', v_sysdate, v_severity_level, v_write_import_line_flag, v_abort_import_flag);
      IF v_severity_level > v_error_sev_code THEN
         v_error_sev_code := v_severity_level;
      END IF;
   ELSE
      vc_allocation_matrix_stmt := vc_allocation_matrix_select || vc_allocation_matrix_where || vc_allocation_matrix_orderby;
   END IF;
   -- Abort Procedure?
   IF v_abort_import_flag = '1' THEN
      v_batch_status_code := 'ABAL';
      RAISE e_abort;
   END IF;

   -- Clear large rollback segment
   COMMIT;

   -- ****** Read and Process Transactions ****** -----------------------------------------
   OPEN bcp_transactions_cursor;
   LOOP
      FETCH bcp_transactions_cursor INTO bcp_transactions;
      EXIT WHEN bcp_transactions_cursor%NOTFOUND;
      v_write_import_line_flag := '1';
      v_rowid := bcp_transactions.ROWID;

      -- Search Allocation Matrix for matches
      BEGIN
          OPEN allocation_matrix_cursor
           FOR vc_allocation_matrix_stmt
         USING v_rowid;
         FETCH allocation_matrix_cursor
          INTO allocation_matrix;
         -- Rows found?
         IF allocation_matrix_cursor%FOUND THEN
            vn_fetch_rows := allocation_matrix_cursor%ROWCOUNT;
         ELSE
            vn_fetch_rows := 0;
         END IF;
         CLOSE allocation_matrix_cursor;
         IF SQLCODE != 0 THEN
            RAISE e_badread;
         END IF;
      EXCEPTION
         WHEN e_badread THEN
            vn_fetch_rows := 0;
            vi_error_count := vi_error_count + 1;
            Sp_Geterrorcode('AL4', an_batch_id, 'AL', v_sysdate, v_severity_level, v_write_import_line_flag, v_abort_import_flag);
            IF v_severity_level > v_error_sev_code THEN
               v_error_sev_code := v_severity_level;
             END IF;
      END;
      -- Exit if aborted
      IF v_abort_import_flag = '1' THEN
         v_batch_status_code := 'ABAL';
         RAISE e_abort;
      END IF;

      -- Allocation Matrix Line Found
      IF vn_fetch_rows > 0 THEN
         -- Get and create allocations --
         v_transaction_detail_id := bcp_transactions.transaction_detail_id;
         v_allocation_matrix_id := allocation_matrix.allocation_matrix_id;
         v_transaction_state_code := bcp_transactions.transaction_state_code;
         IF v_transaction_state_code IS NULL THEN
            v_transaction_state_code := '*NULL';
         END IF;
         v_orig_gl_line_itm_dist_amt := bcp_transactions.gl_line_itm_dist_amt;
         v_accum_allocation := 0;
         v_delta := 0;

         -- Write lines?
         IF v_write_import_line_flag = '1' THEN
            bcp_transactions.allocation_matrix_id := v_allocation_matrix_id;

            -- Get next allocation sub-trans ID for the new transaction detail records
            SELECT sq_tb_allocation_subtrans_id.NEXTVAL
              INTO bcp_transactions.allocation_subtrans_id
              FROM DUAL;

            -- Get number of Allocation Detail lines
            vn_allocation_total_rows := 0;
            SELECT COUNT(*)
              INTO vn_allocation_total_rows
              FROM TB_ALLOCATION_MATRIX_DETAIL
             WHERE TB_ALLOCATION_MATRIX_DETAIL.allocation_matrix_id = v_allocation_matrix_id;

            IF vn_allocation_total_rows > 0 THEN
               -- Execute Cursor to get allocations
               OPEN allocation_detail_cursor;
               LOOP
                  FETCH allocation_detail_cursor INTO allocation_detail;
                  EXIT WHEN allocation_detail_cursor%NOTFOUND;

                  -- Add allocation rows to total imported rows -- 3361 -- 02/13/2007 -- MBF03
                  v_total_rows := v_total_rows + 1;

                  -- Fill other fields from allocation matrix detail
                  bcp_transactions.auto_transaction_state_code := v_transaction_state_code;
                  bcp_transactions.jurisdiction_id := allocation_detail.jurisdiction_id;
                  bcp_transactions.transaction_state_code := allocation_detail.state;

                  v_gl_line_itm_dist_amt := ROUND(v_orig_gl_line_itm_dist_amt * allocation_detail.allocation_percent,2);
                  v_accum_allocation := v_accum_allocation + v_gl_line_itm_dist_amt;

                  bcp_transactions.orig_gl_line_itm_dist_amt := v_orig_gl_line_itm_dist_amt;
                  bcp_transactions.gl_line_itm_dist_amt := v_gl_line_itm_dist_amt;

                  -- Check for last allocation and rounding -- 3361 -- 02/09/2007 -- MBF02
                  IF allocation_detail_cursor%ROWCOUNT = vn_allocation_total_rows THEN
                     -- Check rounding
                     v_delta := v_orig_gl_line_itm_dist_amt - v_accum_allocation;
                     IF v_delta <> 0 THEN
                        bcp_transactions.gl_line_itm_dist_amt := bcp_transactions.gl_line_itm_dist_amt + v_delta;
                     END IF;
                  END IF;

                  -- Insert transaction detail row
                  BEGIN
                     INSERT INTO TB_BCP_TRANSACTIONS (
                        transaction_detail_id,
                        source_transaction_id,
                        process_batch_no,
                        gl_extract_batch_no,
                        archive_batch_no,
                        allocation_matrix_id,
                        allocation_subtrans_id,
                        entered_date,
                        transaction_status,
                        gl_date,
                        gl_company_nbr,
                        gl_company_name,
                        gl_division_nbr,
                        gl_division_name,
                        gl_cc_nbr_dept_id,
                        gl_cc_nbr_dept_name,
                        gl_local_acct_nbr,
                        gl_local_acct_name,
                        gl_local_sub_acct_nbr,
                        gl_local_sub_acct_name,
                        gl_full_acct_nbr,
                        gl_full_acct_name,
                        gl_line_itm_dist_amt,
                        orig_gl_line_itm_dist_amt,
                        vendor_nbr,
                        vendor_name,
                        vendor_address_line_1,
                        vendor_address_line_2,
                        vendor_address_line_3,
                        vendor_address_line_4,
                        vendor_address_city,
                        vendor_address_county,
                        vendor_address_state,
                        vendor_address_zip,
                        vendor_address_country,
                        vendor_type,
                        vendor_type_name,
                        invoice_nbr,
                        invoice_desc,
                        invoice_date,
                        invoice_freight_amt,
                        invoice_discount_amt,
                        invoice_tax_amt,
                        invoice_total_amt,
                        invoice_tax_flg,
                        invoice_line_nbr,
                        invoice_line_name,
                        invoice_line_type,
                        invoice_line_type_name,
                        invoice_line_amt,
                        invoice_line_tax,
                        afe_project_nbr,
                        afe_project_name,
                        afe_category_nbr,
                        afe_category_name,
                        afe_sub_cat_nbr,
                        afe_sub_cat_name,
                        afe_use,
                        afe_contract_type,
                        afe_contract_structure,
                        afe_property_cat,
                        inventory_nbr,
                        inventory_name,
                        inventory_class,
                        inventory_class_name,
                        po_nbr,
                        po_name,
                        po_date,
                        po_line_nbr,
                        po_line_name,
                        po_line_type,
                        po_line_type_name,
                        ship_to_location,
                        ship_to_location_name,
                        ship_to_address_line_1,
                        ship_to_address_line_2,
                        ship_to_address_line_3,
                        ship_to_address_line_4,
                        ship_to_address_city,
                        ship_to_address_county,
                        ship_to_address_state,
                        ship_to_address_zip,
                        ship_to_address_country,
                        wo_nbr,
                        wo_name,
                        wo_date,
                        wo_type,
                        wo_type_desc,
                        wo_class,
                        wo_class_desc,
                        wo_entity,
                        wo_entity_desc,
                        wo_line_nbr,
                        wo_line_name,
                        wo_line_type,
                        wo_line_type_desc,
                        wo_shut_down_cd,
                        wo_shut_down_cd_desc,
                        voucher_id,
                        voucher_name,
                        voucher_date,
                        voucher_line_nbr,
                        voucher_line_desc,
                        check_nbr,
                        check_no,
                        check_date,
                        check_amt,
                        check_desc,
                        user_text_01,
                        user_text_02,
                        user_text_03,
                        user_text_04,
                        user_text_05,
                        user_text_06,
                        user_text_07,
                        user_text_08,
                        user_text_09,
                        user_text_10,
                        user_text_11,
                        user_text_12,
                        user_text_13,
                        user_text_14,
                        user_text_15,
                        user_text_16,
                        user_text_17,
                        user_text_18,
                        user_text_19,
                        user_text_20,
                        user_text_21,
                        user_text_22,
                        user_text_23,
                        user_text_24,
                        user_text_25,
                        user_text_26,
                        user_text_27,
                        user_text_28,
                        user_text_29,
                        user_text_30,
                        user_number_01,
                        user_number_02,
                        user_number_03,
                        user_number_04,
                        user_number_05,
                        user_number_06,
                        user_number_07,
                        user_number_08,
                        user_number_09,
                        user_number_10,
                        user_date_01,
                        user_date_02,
                        user_date_03,
                        user_date_04,
                        user_date_05,
                        user_date_06,
                        user_date_07,
                        user_date_08,
                        user_date_09,
                        user_date_10,
                        comments,
                        tb_calc_tax_amt,
                        state_use_amount,
                        state_use_tier2_amount,
                        state_use_tier3_amount,
                        county_use_amount,
                        county_local_use_amount,
                        city_use_amount,
                        city_local_use_amount,
                        transaction_state_code,
                        auto_transaction_state_code,
                        transaction_ind,
                        suspend_ind,
                        taxcode_detail_id,
                        taxcode_state_code,
                        taxcode_type_code,
                        taxcode_code,
                        cch_taxcat_code,
                        cch_group_code,
                        cch_item_code,
                        manual_taxcode_ind,
                        tax_matrix_id,
                        location_matrix_id,
                        jurisdiction_id,
                        jurisdiction_taxrate_id,
                        manual_jurisdiction_ind,
                        measure_type_code,
                        state_use_rate,
                        state_use_tier2_rate,
                        state_use_tier3_rate,
                        state_split_amount,
                        state_tier2_min_amount,
                        state_tier2_max_amount,
                        state_maxtax_amount,
                        county_use_rate,
                        county_local_use_rate,
                        county_split_amount,
                        county_maxtax_amount,
                        county_single_flag,
                        county_default_flag,
                        city_use_rate,
                        city_local_use_rate,
                        city_split_amount,
                        city_split_use_rate,
                        city_single_flag,
                        city_default_flag,
                        combined_use_rate,
                        load_timestamp,
                        gl_extract_updater,
                        gl_extract_timestamp,
                        gl_extract_flag,
                        gl_log_flag,
                        gl_extract_amt,
                        audit_flag,
                        audit_user_id,
                        audit_timestamp,
                        modify_user_id,
                        modify_timestamp,
                        update_user_id,
                        update_timestamp )
                     VALUES (
                        bcp_transactions.transaction_detail_id,
                        bcp_transactions.source_transaction_id,
                        bcp_transactions.process_batch_no,
                        bcp_transactions.gl_extract_batch_no,
                        bcp_transactions.archive_batch_no,
                        bcp_transactions.allocation_matrix_id,
                        bcp_transactions.allocation_subtrans_id,
                        bcp_transactions.entered_date,
                        bcp_transactions.transaction_status,
                        bcp_transactions.gl_date,
                        bcp_transactions.gl_company_nbr,
                        bcp_transactions.gl_company_name,
                        bcp_transactions.gl_division_nbr,
                        bcp_transactions.gl_division_name,
                        bcp_transactions.gl_cc_nbr_dept_id,
                        bcp_transactions.gl_cc_nbr_dept_name,
                        bcp_transactions.gl_local_acct_nbr,
                        bcp_transactions.gl_local_acct_name,
                        bcp_transactions.gl_local_sub_acct_nbr,
                        bcp_transactions.gl_local_sub_acct_name,
                        bcp_transactions.gl_full_acct_nbr,
                        bcp_transactions.gl_full_acct_name,
                        bcp_transactions.gl_line_itm_dist_amt,
                        bcp_transactions.orig_gl_line_itm_dist_amt,
                        bcp_transactions.vendor_nbr,
                        bcp_transactions.vendor_name,
                        bcp_transactions.vendor_address_line_1,
                        bcp_transactions.vendor_address_line_2,
                        bcp_transactions.vendor_address_line_3,
                        bcp_transactions.vendor_address_line_4,
                        bcp_transactions.vendor_address_city,
                        bcp_transactions.vendor_address_county,
                        bcp_transactions.vendor_address_state,
                        bcp_transactions.vendor_address_zip,
                        bcp_transactions.vendor_address_country,
                        bcp_transactions.vendor_type,
                        bcp_transactions.vendor_type_name,
                        bcp_transactions.invoice_nbr,
                        bcp_transactions.invoice_desc,
                        bcp_transactions.invoice_date,
                        bcp_transactions.invoice_freight_amt,
                        bcp_transactions.invoice_discount_amt,
                        bcp_transactions.invoice_tax_amt,
                        bcp_transactions.invoice_total_amt,
                        bcp_transactions.invoice_tax_flg,
                        bcp_transactions.invoice_line_nbr,
                        bcp_transactions.invoice_line_name,
                        bcp_transactions.invoice_line_type,
                        bcp_transactions.invoice_line_type_name,
                        bcp_transactions.invoice_line_amt,
                        bcp_transactions.invoice_line_tax,
                        bcp_transactions.afe_project_nbr,
                        bcp_transactions.afe_project_name,
                        bcp_transactions.afe_category_nbr,
                        bcp_transactions.afe_category_name,
                        bcp_transactions.afe_sub_cat_nbr,
                        bcp_transactions.afe_sub_cat_name,
                        bcp_transactions.afe_use,
                        bcp_transactions.afe_contract_type,
                        bcp_transactions.afe_contract_structure,
                        bcp_transactions.afe_property_cat,
                        bcp_transactions.inventory_nbr,
                        bcp_transactions.inventory_name,
                        bcp_transactions.inventory_class,
                        bcp_transactions.inventory_class_name,
                        bcp_transactions.po_nbr,
                        bcp_transactions.po_name,
                        bcp_transactions.po_date,
                        bcp_transactions.po_line_nbr,
                        bcp_transactions.po_line_name,
                        bcp_transactions.po_line_type,
                        bcp_transactions.po_line_type_name,
                        bcp_transactions.ship_to_location,
                        bcp_transactions.ship_to_location_name,
                        bcp_transactions.ship_to_address_line_1,
                        bcp_transactions.ship_to_address_line_2,
                        bcp_transactions.ship_to_address_line_3,
                        bcp_transactions.ship_to_address_line_4,
                        bcp_transactions.ship_to_address_city,
                        bcp_transactions.ship_to_address_county,
                        bcp_transactions.ship_to_address_state,
                        bcp_transactions.ship_to_address_zip,
                        bcp_transactions.ship_to_address_country,
                        bcp_transactions.wo_nbr,
                        bcp_transactions.wo_name,
                        bcp_transactions.wo_date,
                        bcp_transactions.wo_type,
                        bcp_transactions.wo_type_desc,
                        bcp_transactions.wo_class,
                        bcp_transactions.wo_class_desc,
                        bcp_transactions.wo_entity,
                        bcp_transactions.wo_entity_desc,
                        bcp_transactions.wo_line_nbr,
                        bcp_transactions.wo_line_name,
                        bcp_transactions.wo_line_type,
                        bcp_transactions.wo_line_type_desc,
                        bcp_transactions.wo_shut_down_cd,
                        bcp_transactions.wo_shut_down_cd_desc,
                        bcp_transactions.voucher_id,
                        bcp_transactions.voucher_name,
                        bcp_transactions.voucher_date,
                        bcp_transactions.voucher_line_nbr,
                        bcp_transactions.voucher_line_desc,
                        bcp_transactions.check_nbr,
                        bcp_transactions.check_no,
                        bcp_transactions.check_date,
                        bcp_transactions.check_amt,
                        bcp_transactions.check_desc,
                        bcp_transactions.user_text_01,
                        bcp_transactions.user_text_02,
                        bcp_transactions.user_text_03,
                        bcp_transactions.user_text_04,
                        bcp_transactions.user_text_05,
                        bcp_transactions.user_text_06,
                        bcp_transactions.user_text_07,
                        bcp_transactions.user_text_08,
                        bcp_transactions.user_text_09,
                        bcp_transactions.user_text_10,
                        bcp_transactions.user_text_11,
                        bcp_transactions.user_text_12,
                        bcp_transactions.user_text_13,
                        bcp_transactions.user_text_14,
                        bcp_transactions.user_text_15,
                        bcp_transactions.user_text_16,
                        bcp_transactions.user_text_17,
                        bcp_transactions.user_text_18,
                        bcp_transactions.user_text_19,
                        bcp_transactions.user_text_20,
                        bcp_transactions.user_text_21,
                        bcp_transactions.user_text_22,
                        bcp_transactions.user_text_23,
                        bcp_transactions.user_text_24,
                        bcp_transactions.user_text_25,
                        bcp_transactions.user_text_26,
                        bcp_transactions.user_text_27,
                        bcp_transactions.user_text_28,
                        bcp_transactions.user_text_29,
                        bcp_transactions.user_text_30,
                        bcp_transactions.user_number_01,
                        bcp_transactions.user_number_02,
                        bcp_transactions.user_number_03,
                        bcp_transactions.user_number_04,
                        bcp_transactions.user_number_05,
                        bcp_transactions.user_number_06,
                        bcp_transactions.user_number_07,
                        bcp_transactions.user_number_08,
                        bcp_transactions.user_number_09,
                        bcp_transactions.user_number_10,
                        bcp_transactions.user_date_01,
                        bcp_transactions.user_date_02,
                        bcp_transactions.user_date_03,
                        bcp_transactions.user_date_04,
                        bcp_transactions.user_date_05,
                        bcp_transactions.user_date_06,
                        bcp_transactions.user_date_07,
                        bcp_transactions.user_date_08,
                        bcp_transactions.user_date_09,
                        bcp_transactions.user_date_10,
                        bcp_transactions.comments,
                        bcp_transactions.tb_calc_tax_amt,
                        bcp_transactions.state_use_amount,
                        bcp_transactions.state_use_tier2_amount,
                        bcp_transactions.state_use_tier3_amount,
                        bcp_transactions.county_use_amount,
                        bcp_transactions.county_local_use_amount,
                        bcp_transactions.city_use_amount,
                        bcp_transactions.city_local_use_amount,
                        bcp_transactions.transaction_state_code,
                        bcp_transactions.auto_transaction_state_code,
                        bcp_transactions.transaction_ind,
                        bcp_transactions.suspend_ind,
                        bcp_transactions.taxcode_detail_id,
                        bcp_transactions.taxcode_state_code,
                        bcp_transactions.taxcode_type_code,
                        bcp_transactions.taxcode_code,
                        bcp_transactions.cch_taxcat_code,
                        bcp_transactions.cch_group_code,
                        bcp_transactions.cch_item_code,
                        bcp_transactions.manual_taxcode_ind,
                        bcp_transactions.tax_matrix_id,
                        bcp_transactions.location_matrix_id,
                        bcp_transactions.jurisdiction_id,
                        bcp_transactions.jurisdiction_taxrate_id,
                        bcp_transactions.manual_jurisdiction_ind,
                        bcp_transactions.measure_type_code,
                        bcp_transactions.state_use_rate,
                        bcp_transactions.state_use_tier2_rate,
                        bcp_transactions.state_use_tier3_rate,
                        bcp_transactions.state_split_amount,
                        bcp_transactions.state_tier2_min_amount,
                        bcp_transactions.state_tier2_max_amount,
                        bcp_transactions.state_maxtax_amount,
                        bcp_transactions.county_use_rate,
                        bcp_transactions.county_local_use_rate,
                        bcp_transactions.county_split_amount,
                        bcp_transactions.county_maxtax_amount,
                        bcp_transactions.county_single_flag,
                        bcp_transactions.county_default_flag,
                        bcp_transactions.city_use_rate,
                        bcp_transactions.city_local_use_rate,
                        bcp_transactions.city_split_amount,
                        bcp_transactions.city_split_use_rate,
                        bcp_transactions.city_single_flag,
                        bcp_transactions.city_default_flag,
                        bcp_transactions.combined_use_rate,
                        bcp_transactions.load_timestamp,
                        bcp_transactions.gl_extract_updater,
                        bcp_transactions.gl_extract_timestamp,
                        bcp_transactions.gl_extract_flag,
                        bcp_transactions.gl_log_flag,
                        bcp_transactions.gl_extract_amt,
                        bcp_transactions.audit_flag,
                        bcp_transactions.audit_user_id,
                        bcp_transactions.audit_timestamp,
                        bcp_transactions.modify_user_id,
                        bcp_transactions.modify_timestamp,
                        bcp_transactions.update_user_id,
                        bcp_transactions.update_timestamp );
                     -- Error Checking
                     IF SQLCODE != 0 THEN
                        RAISE e_badwrite;
                     END IF;
                  EXCEPTION
                     WHEN e_badwrite THEN
                        vi_error_count := vi_error_count + 1;
                        Sp_Geterrorcode('AL5', an_batch_id, 'AL', v_sysdate, v_severity_level, v_write_import_line_flag, v_abort_import_flag);
                        IF v_severity_level > v_error_sev_code THEN
                           v_error_sev_code := v_severity_level;
                        END IF;
                  END;
                  -- Exit if aborted
                  IF v_abort_import_flag = '1' THEN
                     v_batch_status_code := 'ABAL';
                     RAISE e_abort;
                  END IF;
               END LOOP;
            END IF;
            CLOSE allocation_detail_cursor;

-- MBF02      -- Check rounding
--            v_delta := v_orig_gl_line_itm_dist_amt - v_accum_allocation;
--            IF v_delta <> 0 THEN
--               UPDATE tb_bcp_transactions
--                  SET gl_line_itm_dist_amt = gl_line_itm_dist_amt + v_delta
--                WHERE transaction_detail_id = bcp_transactions.transaction_detail_id;
--            END IF;

            -- Update Allocated Transaction
            v_total_rows := v_total_rows - 1;
            UPDATE TB_BCP_TRANSACTIONS
               SET transaction_detail_id = -1
             WHERE ROWID = v_rowid;
         END IF;
      END IF;
   END LOOP;
   CLOSE bcp_transactions_cursor;
   v_batch_status_code := 'AL';

   -- Update batch with final totals
   UPDATE TB_BATCH
      SET batch_status_code = v_batch_status_code,
          error_sev_code = v_error_sev_code,
          total_rows = v_total_rows
    WHERE batch_id = an_batch_id;

   COMMIT;

EXCEPTION
   WHEN e_abort THEN
      -- Update batch with error codes
      UPDATE TB_BATCH
         SET batch_status_code = v_batch_status_code,
             error_sev_code = v_error_sev_code
       WHERE batch_id = an_batch_id;
      COMMIT;
END;
/

CREATE OR REPLACE PROCEDURE Sp_Tb_Jurisdiction_Taxrate_A_I(
p_jurisdiction_taxrate_id IN NUMBER
--p_tb_jurisdiction_taxrate_type IN tb_jurisdiction_taxrate%ROWTYPE
)
AS
   -- Table defined variables
   v_sysdate                       TB_TRANSACTION_DETAIL.load_timestamp%TYPE         := SYS_EXTRACT_UTC(SYSTIMESTAMP);
   v_taxtype_code                  TB_TAXCODE.taxtype_code%TYPE                      := NULL;
   v_override_taxtype_code         TB_TAXCODE.taxtype_code%TYPE                      := NULL;
   v_taxtype_used                  TB_TAXCODE.taxtype_code%TYPE                      := NULL;
   v_state_flag                    TB_LOCATION_MATRIX.state_flag%TYPE                := '0';
   v_county_flag                   TB_LOCATION_MATRIX.county_flag%TYPE               := '0';
   v_county_local_flag             TB_LOCATION_MATRIX.county_local_flag%TYPE         := '0';
   v_city_flag                     TB_LOCATION_MATRIX.city_flag%TYPE                 := '0';
   v_city_local_flag               TB_LOCATION_MATRIX.city_local_flag%TYPE           := '0';

   -- temporary
   vn_taxable_amt                  NUMBER                                            := 0;

   l_tb_jurisdiction_taxrate_type TB_JURISDICTION_TAXRATE%ROWTYPE;

   -- Define Exceptions
   --e_badupdate                     exception;

-- ****** Program starts ****** --------------------------------------------------------------------
BEGIN

   SELECT	*
		INTO l_tb_jurisdiction_taxrate_type
   FROM		TB_JURISDICTION_TAXRATE
   WHERE	jurisdiction_taxrate_id = p_jurisdiction_taxrate_id;

   DECLARE
	   -- Define Transaction Detail Cursor (tb_transaction_detail)
	   CURSOR transaction_detail_cursor
	   IS
	      SELECT TB_TRANSACTION_DETAIL.*
		FROM TB_TRANSACTION_DETAIL
	       WHERE ( TB_TRANSACTION_DETAIL.jurisdiction_id = l_tb_jurisdiction_taxrate_type.jurisdiction_id ) AND
		     ( l_tb_jurisdiction_taxrate_type.effective_date <= TB_TRANSACTION_DETAIL.gl_date ) AND ( l_tb_jurisdiction_taxrate_type.expiration_date >= TB_TRANSACTION_DETAIL.gl_date ) AND
		     ( l_tb_jurisdiction_taxrate_type.measure_type_code = TB_TRANSACTION_DETAIL.measure_type_code ) AND
		     ( TB_TRANSACTION_DETAIL.transaction_ind = 'S' ) AND ( TB_TRANSACTION_DETAIL.suspend_ind = 'R' )
	      FOR UPDATE;
	      transaction_detail           transaction_detail_cursor%ROWTYPE;
   BEGIN
	   -- ****** Read and Process Transactions ****** -----------------------------------------
	   OPEN transaction_detail_cursor;
	   LOOP
	      FETCH transaction_detail_cursor
	       INTO transaction_detail;
	       EXIT WHEN transaction_detail_cursor%NOTFOUND;

	      -- Get Tax Type Codes
	      SELECT taxtype_code
		INTO v_taxtype_code
		FROM TB_TAXCODE
	       WHERE taxcode_code = transaction_detail.taxcode_code
		 AND taxcode_type_code = transaction_detail.taxcode_type_code;

	      -- Get Location Matrix Codes if NOT applied or overriden          -- MBF01
	      IF (transaction_detail.location_matrix_id IS NOT NULL AND transaction_detail.location_matrix_id <> 0) AND
		 (transaction_detail.manual_jurisdiction_ind IS NULL) THEN
		 -- Get Override Tax Type Code -- 3351
		 -- Get 5 Tax Calc Flags -- 3411 -- MBF02
		 SELECT override_taxtype_code,
			state_flag, county_flag, county_local_flag, city_flag, city_local_flag
		   INTO v_override_taxtype_code,
			v_state_flag,
			v_county_flag,
			v_county_local_flag,
			v_city_flag,
			v_city_local_flag
		   FROM TB_LOCATION_MATRIX
		  WHERE TB_LOCATION_MATRIX.location_matrix_id = transaction_detail.location_matrix_id;
	      ELSE
		 v_override_taxtype_code := '*NO';
		 v_state_flag := '1';                           -- MBF01
		 v_county_flag := '1';                          -- MBF01
		 v_county_local_flag := '1';                    -- MBF01
		 v_city_flag := '1';                            -- MBF01
		 v_city_local_flag := '1';                      -- MBF01
	      END IF;

	      -- Determine Tax Type - Use or Sales
	      v_taxtype_used := v_override_taxtype_code;
	      IF v_taxtype_used IS NULL OR TRIM(v_taxtype_used) = '' OR TRIM(v_taxtype_used) = '*NO' THEN
		 v_taxtype_used := SUBSTR(TRIM(v_taxtype_code),1,1);
		 IF v_taxtype_used IS NULL OR Trim(v_taxtype_used) = '' THEN
		    v_taxtype_used := 'U';
		 END IF;
	      END IF;

	      -- Populate and/or Clear Transaction Detail working fields
	      transaction_detail.state_use_amount := 0;
	      transaction_detail.state_use_tier2_amount := 0;
	      transaction_detail.state_use_tier3_amount := 0;
	      transaction_detail.county_use_amount := 0;
	      transaction_detail.county_local_use_amount := 0;
	      transaction_detail.city_use_amount := 0;
	      transaction_detail.city_local_use_amount := 0;
	      transaction_detail.tb_calc_tax_amt := 0;
	      transaction_detail.gl_extract_amt := 0;
	-- future? --      transaction_detail.taxable_amt := 0;
	-- future? --      transactoin_detail.taxtype_used := v_taxtype_used;
	      transaction_detail.update_user_id := USER;
	      transaction_detail.update_timestamp := v_sysdate;

	      -- Move Rates to parameters
	      transaction_detail.transaction_ind := 'P';
	      transaction_detail.suspend_ind := NULL;
	      transaction_detail.jurisdiction_id := l_tb_jurisdiction_taxrate_type.jurisdiction_id;
	      transaction_detail.jurisdiction_taxrate_id := l_tb_jurisdiction_taxrate_type.jurisdiction_taxrate_id;
	      transaction_detail.county_split_amount := l_tb_jurisdiction_taxrate_type.county_split_amount;
	      transaction_detail.county_maxtax_amount := l_tb_jurisdiction_taxrate_type.county_maxtax_amount;
	      transaction_detail.county_single_flag := l_tb_jurisdiction_taxrate_type.county_single_flag;
	      transaction_detail.county_default_flag := l_tb_jurisdiction_taxrate_type.county_default_flag;
	      transaction_detail.city_split_amount := l_tb_jurisdiction_taxrate_type.city_split_amount;
	      transaction_detail.city_single_flag := l_tb_jurisdiction_taxrate_type.city_single_flag;
	      transaction_detail.city_default_flag := l_tb_jurisdiction_taxrate_type.city_default_flag;

	      IF v_taxtype_used = 'U' THEN
		 -- Use Tax Rates
		 transaction_detail.state_use_rate := l_tb_jurisdiction_taxrate_type.state_use_rate;
		 transaction_detail.state_use_tier2_rate := l_tb_jurisdiction_taxrate_type.state_use_tier2_rate;
		 transaction_detail.state_use_tier3_rate := l_tb_jurisdiction_taxrate_type.state_use_tier3_rate;
		 transaction_detail.state_split_amount := l_tb_jurisdiction_taxrate_type.state_split_amount;
		 transaction_detail.state_tier2_min_amount := l_tb_jurisdiction_taxrate_type.state_tier2_min_amount;
		 transaction_detail.state_tier2_max_amount := l_tb_jurisdiction_taxrate_type.state_tier2_max_amount;
		 transaction_detail.state_maxtax_amount := l_tb_jurisdiction_taxrate_type.state_maxtax_amount;
		 transaction_detail.county_use_rate := l_tb_jurisdiction_taxrate_type.county_use_rate;
		 transaction_detail.county_local_use_rate := l_tb_jurisdiction_taxrate_type.county_local_use_rate;
		 transaction_detail.city_use_rate := l_tb_jurisdiction_taxrate_type.city_use_rate;
		 transaction_detail.city_local_use_rate := l_tb_jurisdiction_taxrate_type.city_local_use_rate;
		 transaction_detail.city_split_use_rate := l_tb_jurisdiction_taxrate_type.city_split_use_rate;
	      ELSIF v_taxtype_used = 'S' THEN
		 -- Sales Tax Rates
		 transaction_detail.state_use_rate := l_tb_jurisdiction_taxrate_type.state_sales_rate;
		 transaction_detail.state_use_tier2_rate := 0;
		 transaction_detail.state_use_tier3_rate := 0;
		 transaction_detail.state_split_amount := 0;
		 transaction_detail.state_tier2_min_amount := 0;
		 transaction_detail.state_tier2_max_amount := 999999999;
		 transaction_detail.state_maxtax_amount := 999999999;
		 transaction_detail.county_use_rate := l_tb_jurisdiction_taxrate_type.county_sales_rate;
		 transaction_detail.county_local_use_rate := l_tb_jurisdiction_taxrate_type.county_local_sales_rate;
		 transaction_detail.city_use_rate := l_tb_jurisdiction_taxrate_type.city_sales_rate;
		 transaction_detail.city_local_use_rate := l_tb_jurisdiction_taxrate_type.city_local_sales_rate;
		 transaction_detail.city_split_use_rate := l_tb_jurisdiction_taxrate_type.city_split_sales_rate;
	      ELSE
		 -- Unknown!
		 transaction_detail.state_use_rate := 0;
		 transaction_detail.state_use_tier2_rate := 0;
		 transaction_detail.state_use_tier3_rate := 0;
		 transaction_detail.state_split_amount := 0;
		 transaction_detail.state_tier2_min_amount := 0;
		 transaction_detail.state_tier2_max_amount := 999999999;
		 transaction_detail.state_maxtax_amount := 999999999;
		 transaction_detail.county_use_rate := 0;
		 transaction_detail.county_local_use_rate := 0;
		 transaction_detail.city_use_rate := 0;
		 transaction_detail.city_local_use_rate := 0;
		 transaction_detail.city_split_use_rate := 0;
	      END IF;

	      -- Get Jurisdiction TaxRates and calculate Tax Amounts
	      Sp_Calcusetax(transaction_detail.gl_line_itm_dist_amt,
			    transaction_detail.invoice_freight_amt,
			    transaction_detail.invoice_discount_amt,
			    transaction_detail.transaction_state_code,
	-- future? --             transaction_detail.import_definition_code,
			    'importdefn',
			    transaction_detail.taxcode_code,
			    v_state_flag,
			    v_county_flag,
			    v_county_local_flag,
			    v_city_flag,
			    v_city_local_flag,
			    transaction_detail.state_use_rate,
			    transaction_detail.state_use_tier2_rate,
			    transaction_detail.state_use_tier3_rate,
			    transaction_detail.state_split_amount,
			    transaction_detail.state_tier2_min_amount,
			    transaction_detail.state_tier2_max_amount,
			    transaction_detail.state_maxtax_amount,
			    transaction_detail.county_use_rate,
			    transaction_detail.county_local_use_rate,
			    transaction_detail.county_split_amount,
			    transaction_detail.county_maxtax_amount,
			    transaction_detail.county_single_flag,
			    transaction_detail.county_default_flag,
			    transaction_detail.city_use_rate,
			    transaction_detail.city_local_use_rate,
			    transaction_detail.city_split_amount,
			    transaction_detail.city_split_use_rate,
			    transaction_detail.city_single_flag,
			    transaction_detail.city_default_flag,
			    transaction_detail.state_use_amount,
			    transaction_detail.state_use_tier2_amount,
			    transaction_detail.state_use_tier3_amount,
			    transaction_detail.county_use_amount,
			    transaction_detail.county_local_use_amount,
			    transaction_detail.city_use_amount,
			    transaction_detail.city_local_use_amount,
			    transaction_detail.tb_calc_tax_amt,
	-- future? --             transaction_detail.taxable_amt);
	-- Targa? --            transaction_detail.user_number_10 );
			    vn_taxable_amt );

	      transaction_detail.combined_use_rate := transaction_detail.state_use_rate +
						      transaction_detail.county_use_rate +
						      transaction_detail.county_local_use_rate +
						      transaction_detail.city_use_rate +
						      transaction_detail.city_local_use_rate;

	      -- Check for Taxable Amount -- 3351
	      IF vn_taxable_amt  <> transaction_detail.gl_line_itm_dist_amt THEN
		 transaction_detail.gl_extract_amt := transaction_detail.gl_line_itm_dist_amt;
		 transaction_detail.gl_line_itm_dist_amt := vn_taxable_amt;
	      END IF;

	      -- Update transaction detail row
	      BEGIN
		 -- 3411 - add taxable amount and tax type used.
		 UPDATE TB_TRANSACTION_DETAIL
		    SET gl_line_itm_dist_amt = transaction_detail.gl_line_itm_dist_amt,
			tb_calc_tax_amt = transaction_detail.tb_calc_tax_amt,
			state_use_amount = transaction_detail.state_use_amount,
			state_use_tier2_amount = transaction_detail.state_use_tier2_amount,
			state_use_tier3_amount = transaction_detail.state_use_tier3_amount,
			county_use_amount = transaction_detail.county_use_amount,
			county_local_use_amount = transaction_detail.county_local_use_amount,
			city_use_amount = transaction_detail.city_use_amount,
			city_local_use_amount = transaction_detail.city_local_use_amount,
			transaction_ind = transaction_detail.transaction_ind,
			suspend_ind = transaction_detail.suspend_ind,
			jurisdiction_id = transaction_detail.jurisdiction_id,
			jurisdiction_taxrate_id = transaction_detail.jurisdiction_taxrate_id,
			state_use_rate = transaction_detail.state_use_rate,
			state_use_tier2_rate = transaction_detail.state_use_tier2_rate,
			state_use_tier3_rate = transaction_detail.state_use_tier3_rate,
			state_split_amount = transaction_detail.state_split_amount,
			state_tier2_min_amount = transaction_detail.state_tier2_min_amount,
			state_tier2_max_amount = transaction_detail.state_tier2_max_amount,
			state_maxtax_amount = transaction_detail.state_maxtax_amount,
			county_use_rate = transaction_detail.county_use_rate,
			county_local_use_rate = transaction_detail.county_local_use_rate,
			county_split_amount = transaction_detail.county_split_amount,
			county_maxtax_amount = transaction_detail.county_maxtax_amount,
			county_single_flag = transaction_detail.county_single_flag,
			county_default_flag = transaction_detail.county_default_flag,
			city_use_rate = transaction_detail.city_use_rate,
			city_local_use_rate = transaction_detail.city_local_use_rate,
			city_split_amount = transaction_detail.city_split_amount,
			city_split_use_rate = transaction_detail.city_split_use_rate,
			city_single_flag = transaction_detail.city_single_flag,
			city_default_flag = transaction_detail.city_default_flag,
			combined_use_rate = transaction_detail.combined_use_rate,
			gl_extract_amt = transaction_detail.gl_extract_amt,
			update_user_id = transaction_detail.update_user_id,
			update_timestamp = transaction_detail.update_timestamp
		  WHERE transaction_detail_id = transaction_detail.transaction_detail_id;
		 -- Error Checking
	--         IF SQLCODE != 0 THEN
	--            RAISE e_badupdate;
	--         END IF;
	--      EXCEPTION
	--         WHEN e_badupdate THEN
	--            NULL;
	      END;
	   END LOOP;
	   CLOSE transaction_detail_cursor;
END;

--EXCEPTION
--   WHEN OTHERS THEN
--      NULL;
END;
/

CREATE OR REPLACE PROCEDURE Sp_Tb_Location_Matrix_A_I(p_location_matrix_id IN TB_LOCATION_MATRIX.location_matrix_id%TYPE)
AS
/* ************************************************************************************************/
/* Object Type/Name: SP Trigger/After Insert - sp_tb_location_matrix_a_i                          */
/* Author:           Michael B. Fuller                                                            */
/* Date:             a long time ago                                                              */
/* Description:      A Location Matrix line has been added                                        */
/* Arguments:        None                                                                         */
/* Returns:          None                                                                         */
/* ************************************************************************************************/
/* Maintenance History:                                                                           */
/* Scan  Author       Date     Release    Description                                  Ticket #   */
/* ----- ---------- ---------- ---------- -------------------------------------------- ---------- */
/*       M. Fuller  06/02/2006 3.3.5.0    Fix processing an allocated line - Temporary 871        */
/*       M. Fuller  01/04/2007 3.3.5.1    Fix processing an allocated line - Permenant 871        */
/* MBF01 M. Fuller  08/14/2007 3.4.1.1    Add 5 Calc Tax Flags                         883        */
/* MBF01 J. Franco  12/16/2008 x.x.x.x    Convert Trigger to SP                        xxx        */
/* ************************************************************************************************/

-- Define Row Type
   l_tb_location_matrix TB_LOCATION_MATRIX%ROWTYPE;

-- Define Cursor Type
   TYPE cursor_type IS REF CURSOR;

-- Transaction Detail Selection SQL
   vc_transaction_detail_select    VARCHAR2(2000) :=
      'SELECT tb_transaction_detail.* ' ||
        'FROM tb_tmp_location_matrix, ' ||
             'tb_transaction_detail '   ||
       'WHERE ( tb_tmp_location_matrix.location_matrix_id = ' || p_location_matrix_id || ') AND ' ||
             '( tb_tmp_location_matrix.effective_date <= tb_transaction_detail.gl_date ) AND ( tb_tmp_location_matrix.expiration_date >= tb_transaction_detail.gl_date ) AND ' ||
             '( tb_transaction_detail.transaction_ind = ''S'' ) AND ( tb_transaction_detail.suspend_ind = ''L'' ) ';
   vc_transaction_detail_where     VARCHAR2(5000) := '';
   vc_transaction_detail_update    VARCHAR2(100) := 'FOR UPDATE';
   vc_transaction_detail_stmt      VARCHAR2(7100);
   transaction_detail_cursor       cursor_type;
   transaction_detail              TB_TRANSACTION_DETAIL%ROWTYPE;

-- Tax Matrix Selection SQL
   vc_tax_matrix_select            VARCHAR2(2000) :=
      'SELECT tb_tax_matrix.tax_matrix_id, tb_tax_matrix.relation_sign, tb_tax_matrix.relation_amount, ' ||
             'tb_tax_matrix.then_hold_code_flag, tb_tax_matrix.else_hold_code_flag, ' ||
             'tb_tax_matrix.then_taxcode_detail_id, tb_tax_matrix.else_taxcode_detail_id, ' ||
             'tb_tax_matrix.then_cch_taxcat_code, tb_tax_matrix.then_cch_group_code, tb_tax_matrix.then_cch_item_code, ' ||
             'tb_tax_matrix.else_cch_taxcat_code, tb_tax_matrix.else_cch_group_code, tb_tax_matrix.else_cch_item_code, ' ||
             'thentaxcddtl.taxcode_state_code, thentaxcddtl.taxcode_type_code, thentaxcddtl.taxcode_code, thentaxcddtl.jurisdiction_id, thentaxcddtl.measure_type_code, thentaxcddtl.taxtype_code, ' ||
             'elsetaxcddtl.taxcode_state_code, elsetaxcddtl.taxcode_type_code, elsetaxcddtl.taxcode_code, elsetaxcddtl.jurisdiction_id, elsetaxcddtl.measure_type_code, elsetaxcddtl.taxtype_code ' ||
       'FROM tb_tax_matrix, ' ||
            'tb_taxcode_detail thentaxcddtl, ' ||
            'tb_taxcode_detail elsetaxcddtl, ' ||
            'tb_transaction_detail ' ||
      'WHERE ( tb_transaction_detail.transaction_detail_id = :v_transaction_detail_id ) AND ' ||
            '( tb_tax_matrix.default_flag = ''0'' AND tb_tax_matrix.binary_weight > 0 ) AND ' ||
            '(( tb_tax_matrix.matrix_state_code = ''*ALL'' ) OR ( tb_tax_matrix.matrix_state_code = :v_transaction_state_code )) AND ' ||
            '( tb_tax_matrix.effective_date <= tb_transaction_detail.gl_date ) AND ( tb_tax_matrix.expiration_date >= tb_transaction_detail.gl_date ) AND ' ||
            '( tb_tax_matrix.then_taxcode_detail_id = thentaxcddtl.taxcode_detail_id (+)) AND ' ||
            '( tb_tax_matrix.else_taxcode_detail_id = elsetaxcddtl.taxcode_detail_id (+)) ';
   vc_tax_matrix_where             VARCHAR2(6000) := '';
   vc_tax_matrix_orderby           VARCHAR2(1000) :=
      'ORDER BY tb_tax_matrix.binary_weight DESC, tb_tax_matrix.significant_digits DESC, tb_tax_matrix.effective_date DESC';
   vc_tax_matrix_stmt              VARCHAR2 (9000);
   tax_matrix_cursor               cursor_type;

-- Tax Matrix Record TYPE
   /*TYPE tax_matrix_record is RECORD (
      tax_matrix_id                tb_tax_matrix.tax_matrix_id%TYPE,
      relation_sign                tb_tax_matrix.relation_sign%TYPE,
      relation_amount              tb_tax_matrix.relation_amount%TYPE,
      then_hold_code_flag          tb_tax_matrix.then_hold_code_flag%TYPE,
      else_hold_code_flag          tb_tax_matrix.else_hold_code_flag%TYPE,
      then_taxcode_detail_id       tb_bcp_transactions.taxcode_detail_id%TYPE,
      else_taxcode_detail_id       tb_bcp_transactions.taxcode_detail_id%TYPE,
      then_cch_taxcat_code         tb_tax_matrix.then_cch_taxcat_code%TYPE,
      then_cch_group_code          tb_tax_matrix.then_cch_group_code%TYPE,
      then_cch_item_code           tb_tax_matrix.then_cch_item_code%TYPE,
      else_cch_taxcat_code         tb_tax_matrix.else_cch_taxcat_code%TYPE,
      else_cch_group_code          tb_tax_matrix.else_cch_group_code%TYPE,
      else_cch_item_code           tb_tax_matrix.else_cch_item_code%TYPE,
      then_taxcode_state_code      tb_bcp_transactions.taxcode_state_code%TYPE,
      then_taxcode_type_code       tb_bcp_transactions.taxcode_type_code%TYPE,
      then_taxcode_code            tb_bcp_transactions.taxcode_code%TYPE,
      then_jurisdiction_id         tb_bcp_transactions.jurisdiction_id%TYPE,
      then_measure_type_code       tb_bcp_transactions.measure_type_code%TYPE,
      then_taxtype_code            tb_taxcode.taxtype_code%TYPE,
      else_taxcode_state_code      tb_bcp_transactions.taxcode_state_code%TYPE,
      else_taxcode_type_code       tb_bcp_transactions.taxcode_type_code%TYPE,
      else_taxcode_code            tb_bcp_transactions.taxcode_code%TYPE,
      else_jurisdiction_id         tb_bcp_transactions.jurisdiction_id%TYPE,
      else_measure_type_code       tb_bcp_transactions.measure_type_code%TYPE,
      else_taxtype_code            tb_taxcode.taxtype_code%TYPE );*/
   tax_matrix                      Matrix_Record_Pkg.tax_matrix_record;

-- Table defined variables
   v_hold_code_flag                TB_TAX_MATRIX.then_hold_code_flag%TYPE            := NULL;
   v_less_hold_code_flag           TB_TAX_MATRIX.then_hold_code_flag%TYPE            := NULL;
   v_less_taxcode_detail_id        TB_TAX_MATRIX.then_taxcode_detail_id%TYPE         := NULL;
   v_less_cch_taxcat_code          TB_TAX_MATRIX.then_cch_taxcat_code%TYPE           := NULL;
   v_less_cch_group_code           TB_TAX_MATRIX.then_cch_group_code%TYPE            := NULL;
   v_equal_hold_code_flag          TB_TAX_MATRIX.then_hold_code_flag%TYPE            := NULL;
   v_equal_taxcode_detail_id       TB_TAX_MATRIX.then_taxcode_detail_id%TYPE         := NULL;
   v_equal_cch_taxcat_code         TB_TAX_MATRIX.then_cch_taxcat_code%TYPE           := NULL;
   v_equal_cch_group_code          TB_TAX_MATRIX.then_cch_group_code%TYPE            := NULL;
   v_greater_hold_code_flag        TB_TAX_MATRIX.then_hold_code_flag%TYPE            := NULL;
   v_greater_taxcode_detail_id     TB_TAX_MATRIX.then_taxcode_detail_id%TYPE         := NULL;
   v_greater_cch_taxcat_code       TB_TAX_MATRIX.then_cch_taxcat_code%TYPE           := NULL;
   v_greater_cch_group_code        TB_TAX_MATRIX.then_cch_group_code%TYPE            := NULL;
   v_jurisdiction_id               TB_JURISDICTION.jurisdiction_id%TYPE              := NULL;
   v_less_taxcode_state_code       TB_TAXCODE_DETAIL.taxcode_state_code%TYPE         := NULL;
   v_less_taxcode_type_code        TB_TAXCODE_DETAIL.taxcode_type_code%TYPE          := NULL;
   v_less_taxcode_code             TB_TAXCODE_DETAIL.taxcode_code%TYPE               := NULL;
   v_less_jurisdiction_id          TB_TAXCODE_DETAIL.jurisdiction_id%TYPE            := NULL;
   v_less_measure_type_code        TB_TAXCODE_DETAIL.measure_type_code%TYPE          := NULL;
   v_less_taxtype_code             TB_TAXCODE_DETAIL.taxtype_code%TYPE               := NULL;
   v_equal_taxcode_state_code      TB_TAXCODE_DETAIL.taxcode_state_code%TYPE         := NULL;
   v_equal_taxcode_type_code       TB_TAXCODE_DETAIL.taxcode_type_code%TYPE          := NULL;
   v_equal_taxcode_code            TB_TAXCODE_DETAIL.taxcode_code%TYPE               := NULL;
   v_equal_jurisdiction_id         TB_TAXCODE_DETAIL.jurisdiction_id%TYPE            := NULL;
   v_equal_measure_type_code       TB_TAXCODE_DETAIL.measure_type_code%TYPE          := NULL;
   v_equal_taxtype_code            TB_TAXCODE_DETAIL.taxtype_code%TYPE               := NULL;
   v_greater_taxcode_state_code    TB_TAXCODE_DETAIL.taxcode_state_code%TYPE         := NULL;
   v_greater_taxcode_type_code     TB_TAXCODE_DETAIL.taxcode_type_code%TYPE          := NULL;
   v_greater_taxcode_code          TB_TAXCODE_DETAIL.taxcode_code%TYPE               := NULL;
   v_greater_jurisdiction_id       TB_TAXCODE_DETAIL.jurisdiction_id%TYPE            := NULL;
   v_greater_measure_type_code     TB_TAXCODE_DETAIL.measure_type_code%TYPE          := NULL;
   v_greater_taxtype_code          TB_TAXCODE_DETAIL.taxtype_code%TYPE               := NULL;
   v_transaction_detail_id         TB_TRANSACTION_DETAIL.transaction_detail_id%TYPE  := NULL;
   v_transaction_state_code        TB_TRANSACTION_DETAIL.transaction_state_code%TYPE := NULL;
   v_override_jurisdiction_id      TB_TRANSACTION_DETAIL.jurisdiction_id%TYPE        := NULL;
   v_sysdate                       TB_TRANSACTION_DETAIL.load_timestamp%TYPE         := SYS_EXTRACT_UTC(SYSTIMESTAMP);
   v_taxtype_code                  TB_TAXCODE.taxtype_code%TYPE                      := NULL;
   v_taxtype_used                  TB_TAXCODE.taxtype_code%TYPE                      := NULL;

-- Program defined variables
   vn_hold_trans_amount            NUMBER                                            := 0;
   vn_fetch_rows                   NUMBER                                            := 0;
   vi_error_count                  INTEGER                                           := 0;
   vn_actual_rowno                 NUMBER                                            := 0;
   vc_state_driver_flag            CHAR(1)                                           := '0';
-- temporary
   vn_taxable_amt                  NUMBER                                            := 0;

-- Define Location Driver Names Cursor (tb_driver_names)
   /*CURSOR location_driver_cursor
   IS
        SELECT driver.trans_dtl_column_name, driver.matrix_column_name, driver.null_driver_flag, driver.wildcard_flag,
               datadefcol.desc_column_name, datadefcol.data_type
          FROM tb_driver_names driver,
               (SELECT *
                  FROM tb_data_def_column
                 WHERE table_name = 'TB_TRANSACTION_DETAIL') datadefcol
         WHERE driver.driver_names_code = 'L' AND
               driver.trans_dtl_column_name = datadefcol.column_name
      ORDER BY driver.driver_id;
      location_driver              location_driver_cursor%ROWTYPE;*/

-- Define Tax Driver Names Cursor (tb_driver_names)
   /*CURSOR tax_driver_cursor
   IS
        SELECT driver.trans_dtl_column_name, driver.matrix_column_name, driver.null_driver_flag, driver.wildcard_flag, driver.range_flag, to_number_flag,
               datadefcol.desc_column_name, datadefcol.data_type
          FROM tb_driver_names driver,
               (SELECT *
                  FROM tb_data_def_column
                 WHERE table_name = 'TB_TRANSACTION_DETAIL') datadefcol
         WHERE driver.driver_names_code = 'T' AND
               driver.trans_dtl_column_name = datadefcol.column_name
      ORDER BY driver.driver_id;
      tax_driver                   tax_driver_cursor%ROWTYPE;*/

-- Define Exceptions
   e_badread                       EXCEPTION;
   e_badupdate                     EXCEPTION;

-- ****** Program starts ****** --------------------------------------------------------------------
BEGIN

-- Populate row type with location matrix record
SELECT TB_LOCATION_MATRIX.*
 INTO  l_tb_location_matrix
 FROM  TB_LOCATION_MATRIX
 WHERE location_matrix_id = p_location_matrix_id;

IF l_tb_location_matrix.default_flag <> '1' AND l_tb_location_matrix.binary_weight > 0 THEN

   -- ***** Create Dynamic WHERE Clause for Transaction Detail SQL ***** --
   -- Location Matrix Drivers
   /*OPEN location_driver_cursor;
   LOOP
      FETCH location_driver_cursor INTO location_driver;
      EXIT WHEN location_driver_cursor%NOTFOUND;
      IF location_driver.null_driver_flag = '1' THEN
         IF location_driver.wildcard_flag = '1' THEN
            IF UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'CHAR' OR
               UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'VARC' THEN
                  vc_transaction_detail_where := vc_transaction_detail_where || 'AND ' ||
                  '((tb_transaction_detail.' || location_driver.trans_dtl_column_name || ' IS NULL AND ( tb_tmp_location_matrix.' || location_driver.matrix_column_name || ' = ''*ALL'' OR tb_tmp_location_matrix.' || location_driver.matrix_column_name || ' = ''*NULL'' )) OR ' ||
                  '(tb_transaction_detail.' || location_driver.trans_dtl_column_name || ' IS NOT NULL AND ( tb_tmp_location_matrix.' || location_driver.matrix_column_name || ' = ''*ALL'' OR UPPER(tb_transaction_detail.' || location_driver.trans_dtl_column_name || ') LIKE UPPER(tb_tmp_location_matrix.' || location_driver.matrix_column_name || ') ))) ';
            END IF;
         ELSE
            IF UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'CHAR' OR
               UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'VARC' THEN
                  vc_transaction_detail_where := vc_transaction_detail_where || 'AND ' ||
                  '((tb_transaction_detail.' || location_driver.trans_dtl_column_name || ' IS NULL AND ( tb_tmp_location_matrix.' || location_driver.matrix_column_name || ' = ''*ALL'' OR tb_tmp_location_matrix.' || location_driver.matrix_column_name || ' = ''*NULL'' )) OR ' ||
                  '(tb_transaction_detail.' || location_driver.trans_dtl_column_name || ' IS NOT NULL AND ( tb_tmp_location_matrix.' || location_driver.matrix_column_name || ' = ''*ALL'' OR UPPER(tb_transaction_detail.' || location_driver.trans_dtl_column_name || ') = UPPER(tb_tmp_location_matrix.' || location_driver.matrix_column_name || ') ))) ';
            END IF;
         END IF;
      ELSE
         IF location_driver.wildcard_flag = '1' THEN
            IF UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'CHAR' OR
               UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'VARC' THEN
                  vc_transaction_detail_where := vc_transaction_detail_where || 'AND ' ||
                  '( tb_tmp_location_matrix.' || location_driver.matrix_column_name || ' = ''*ALL'' OR UPPER(tb_transaction_detail.' || location_driver.trans_dtl_column_name || ') LIKE UPPER(tb_tmp_location_matrix.' || location_driver.matrix_column_name || ') ) ';
            END IF;
         ELSE
            IF UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'CHAR' OR
               UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'VARC' THEN
                  vc_transaction_detail_where := vc_transaction_detail_where || 'AND ' ||
                  '( tb_tmp_location_matrix.' || location_driver.matrix_column_name || ' = ''*ALL'' OR UPPER(tb_transaction_detail.' || location_driver.trans_dtl_column_name || ') = UPPER(tb_tmp_location_matrix.' || location_driver.matrix_column_name || ') ) ';
            END IF;
         END IF;
      END IF;
   END LOOP;
   CLOSE location_driver_cursor;*/

   -- New call to sp_gen_location_driver for MidTier Cleanup
   Sp_Gen_Location_Driver (
	p_generate_driver_reference	=> 'N',
	p_an_batch_id			=> NULL,
	p_transaction_table_name	=> 'tb_transaction_detail',
	p_location_table_name		=> 'tb_tmp_location_matrix',
	p_vc_location_matrix_where	=> vc_transaction_detail_where);

   -- If no drivers found raise error, else create transaction detail sql statement
   IF vc_transaction_detail_where IS NULL THEN
      NULL;
   ELSE
      vc_transaction_detail_stmt := vc_transaction_detail_select || vc_transaction_detail_where || vc_transaction_detail_update;
   END IF;

   -- *******************************************************************************************************************************************
   -- NEW BATCH PROCESS CODE --------------------------------------------------------------------------------------------------------------------
   -- ***** Create Dynamic WHERE Clause for Transaction Detail SQL ***** --
   -- Tax Matrix Drivers
   /*OPEN tax_driver_cursor;
   LOOP
      FETCH tax_driver_cursor INTO tax_driver;
      EXIT WHEN tax_driver_cursor%NOTFOUND;
      IF tax_driver.range_flag = '1' THEN
         IF tax_driver.to_number_flag = '1' THEN
            IF tax_driver.null_driver_flag = '1' THEN
               -- Range and ToNumber and NULL
               IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                  UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                  vc_tax_matrix_where := vc_tax_matrix_where || 'AND ( ' ||
                  '( tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU = ''*NA'' AND ' ||
                     '( tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NULL AND ( tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*NULL'' )) OR ' ||
                     '( tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND ( tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR DECODE(isnumber(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || '),1,to_char(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ',''00000000000000000000.00000000000000000000''),UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ')) = DECODE(isnumber(tb_tax_matrix.' || tax_driver.matrix_column_name || '),1,to_char(tb_tax_matrix.' || tax_driver.matrix_column_name || ',''00000000000000000000.00000000000000000000''),UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') ))) OR ' ||
                  '( tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU <> ''*NA'' AND ' ||
                     '( tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND (DECODE(isnumber(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || '),1,to_char(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ',''00000000000000000000.00000000000000000000''),UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ')) >= DECODE(isnumber(tb_tax_matrix.' || tax_driver.matrix_column_name || '),1,to_char(tb_tax_matrix.' || tax_driver.matrix_column_name || ',''00000000000000000000.00000000000000000000''),UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') ) AND ' ||
                                                                                            'DECODE(isnumber(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || '),1,to_char(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ',''00000000000000000000.00000000000000000000''),UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ')) <= DECODE(isnumber(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU ),1,to_char(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU,''00000000000000000000.00000000000000000000''),UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU)) )))) ';
               END IF;
            ELSE
               -- Range and ToNumber and NOT NULL
               IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                  UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                  vc_tax_matrix_where := vc_tax_matrix_where || 'AND ( ' ||
                  '( tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU = ''*NA'' AND (tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR DECODE(isnumber(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || '),1,to_char(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ',''00000000000000000000.00000000000000000000''),UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ')) = DECODE(isnumber(tb_tax_matrix.' || tax_driver.matrix_column_name || '),1,to_char(tb_tax_matrix.' || tax_driver.matrix_column_name || ',''00000000000000000000.00000000000000000000''),UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ')))) OR ' ||
                  '( tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU <> ''*NA'' AND (DECODE(isnumber(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || '),1,to_char(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ',''00000000000000000000.00000000000000000000''),UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ')) >= DECODE(isnumber(tb_tax_matrix.' || tax_driver.matrix_column_name || '),1,to_char(tb_tax_matrix.' || tax_driver.matrix_column_name || ',''00000000000000000000.00000000000000000000''),UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ')) AND ' ||
                                                                                        'DECODE(isnumber(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || '),1,to_char(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ',''00000000000000000000.00000000000000000000''),UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ')) <= DECODE(isnumber(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU ),1,to_char(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU,''00000000000000000000.00000000000000000000''),UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU)) )) ';
               END IF;
            END IF;
         ELSE
            IF tax_driver.null_driver_flag = '1' THEN
               IF tax_driver.wildcard_flag = '1' THEN
                  -- Range and NOT ToNumber and NULL and Wildcard
                  IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                     UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                     vc_tax_matrix_where := vc_tax_matrix_where || 'AND ( ' ||
                     '( tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU = ''*NA'' AND ' ||
                        '( tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NULL AND (tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*NULL'' )) OR ' ||
                        '( tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND (tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') LIKE UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ')))) OR ' ||
                     '( tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU <> ''*NA'' AND (tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND (UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') >= UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') AND UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') <= UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU) ))) ';
                  END IF;
               ELSE
                  -- Range and NOT ToNumber and NULL and NOT Wildcard
                  IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                     UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                     vc_tax_matrix_where := vc_tax_matrix_where || 'AND ( ' ||
                     '( tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU = ''*NA'' AND ' ||
                        '( tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NULL AND (tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*NULL'' )) OR ' ||
                        '( tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND (tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') = UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ')))) OR ' ||
                     '( tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU <> ''*NA'' AND (tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND (UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') >= UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') AND UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') <= UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU) ))) ';
                  END IF;
               END IF;
            ELSE
               IF tax_driver.wildcard_flag = '1' THEN
                  -- Range and NOT ToNumber and NOT NULL and Wildcard
                  IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                     UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                     vc_tax_matrix_where := vc_tax_matrix_where || 'AND ( ' ||
                     '( tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU = ''*NA'' AND ' ||
                        '(tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') LIKE UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '))) OR ' ||
                     '( tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU <> ''*NA'' AND (UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') >= UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') AND UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') <= UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU) )) ';
                  END IF;
               ELSE
                  -- Range and NOT ToNumber and NOT NULL and NOT Wildcard
                  IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                     UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                     vc_tax_matrix_where := vc_tax_matrix_where || 'AND ( ' ||
                     '( tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU = ''*NA'' AND ' ||
                        '(tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') = UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '))) OR ' ||
                     '( tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU <> ''*NA'' AND (UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') >= UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') AND UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') <= UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU) )) ';
                  END IF;
               END IF;
            END IF;
         END IF;
      ELSE
         IF tax_driver.null_driver_flag = '1' THEN
            IF tax_driver.wildcard_flag = '1' THEN
               -- NOT Range and NULL and Wildcard
               IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                  UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                     vc_tax_matrix_where := vc_tax_matrix_where || 'AND ( ' ||
                     '( tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NULL AND (tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*NULL'' )) OR ' ||
                     '( tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND (tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') LIKE UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ')) ) ';
               END IF;
            ELSE
               -- NOT Range and NULL and NOT Wildcard
               IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                  UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                     vc_tax_matrix_where := vc_tax_matrix_where || 'AND ( ' ||
                     '( tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NULL AND (tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*NULL'' )) OR ' ||
                     '( tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND (tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') = UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ')) ) ';
               END IF;
            END IF;
         ELSE
            IF tax_driver.wildcard_flag = '1' THEN
               -- NOT Range and NOT NULL and Wildcard
               IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                  UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                     vc_tax_matrix_where := vc_tax_matrix_where || 'AND ( ' ||
                     '( tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') LIKE UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') ) ';
               END IF;
            ELSE
               -- NOT Range and NOT NULL and NOT Wildcard
               IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                  UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                  IF tax_driver.trans_dtl_column_name = 'TRANSACTION_STATE_CODE' THEN
                     IF vc_state_driver_flag <> '1' THEN
                        vc_state_driver_flag := '1';
                     END IF;
                     vc_tax_matrix_where := vc_tax_matrix_where || 'AND ( ' ||
                        '( tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR UPPER(:v_transaction_state_code) = UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ')) ';
                  ELSE
                     vc_tax_matrix_where := vc_tax_matrix_where || 'AND ( ' ||
                        '( tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') = UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') ) ';
                  END IF;
               END IF;
            END IF;
         END IF;
      END IF;
      vc_tax_matrix_where := vc_tax_matrix_where || ') ';
   END LOOP;
   CLOSE tax_driver_cursor;*/

   -- New call to sp_gen_tax_driver for MidTier Cleanup
   Sp_Gen_Tax_Driver (
	p_generate_driver_reference	=> 'N',
	p_an_batch_id			=> NULL,
	p_transaction_table_name	=> 'tb_transaction_detail',
	p_tax_table_name		=> 'tb_tax_matrix',
	p_vc_state_driver_flag		=> vc_state_driver_flag,
	p_vc_tax_matrix_where		=> vc_tax_matrix_where);

   -- NEW BATCH PROCESS CODE --------------------------------------------------------------------------------------------------------------------
   -- *******************************************************************************************************************************************

   -- If no drivers found raise error, else create location matrix sql statement
   IF vc_tax_matrix_where IS NULL THEN
      NULL;
   ELSE
      vc_tax_matrix_stmt := vc_tax_matrix_select || vc_tax_matrix_where || vc_tax_matrix_orderby;
   END IF;

   -- Insert New Location Matrix columns into temporary table --------------------------------------
   INSERT INTO TB_TMP_LOCATION_MATRIX (
      location_matrix_id,
      driver_01, driver_01_desc,
      driver_02, driver_02_desc,
      driver_03, driver_03_desc,
      driver_04, driver_04_desc,
      driver_05, driver_05_desc,
      driver_06, driver_06_desc,
      driver_07, driver_07_desc,
      driver_08, driver_08_desc,
      driver_09, driver_09_desc,
      driver_10, driver_10_desc,
      binary_weight, significant_digits,
      default_flag, default_binary_weight, default_significant_digits,
      effective_date, expiration_date,
      jurisdiction_id, state, comments,
      last_used_timestamp, update_user_id, update_timestamp )
   VALUES (
      l_tb_location_matrix.location_matrix_id,
      l_tb_location_matrix.driver_01, l_tb_location_matrix.driver_01_desc,
      l_tb_location_matrix.driver_02, l_tb_location_matrix.driver_02_desc,
      l_tb_location_matrix.driver_03, l_tb_location_matrix.driver_03_desc,
      l_tb_location_matrix.driver_04, l_tb_location_matrix.driver_04_desc,
      l_tb_location_matrix.driver_05, l_tb_location_matrix.driver_05_desc,
      l_tb_location_matrix.driver_06, l_tb_location_matrix.driver_06_desc,
      l_tb_location_matrix.driver_07, l_tb_location_matrix.driver_07_desc,
      l_tb_location_matrix.driver_08, l_tb_location_matrix.driver_08_desc,
      l_tb_location_matrix.driver_09, l_tb_location_matrix.driver_09_desc,
      l_tb_location_matrix.driver_10, l_tb_location_matrix.driver_10_desc,
      l_tb_location_matrix.binary_weight, l_tb_location_matrix.significant_digits,
      l_tb_location_matrix.default_flag, l_tb_location_matrix.default_binary_weight, l_tb_location_matrix.default_significant_digits,
      l_tb_location_matrix.effective_date, l_tb_location_matrix.expiration_date,
      l_tb_location_matrix.jurisdiction_id, l_tb_location_matrix.state, l_tb_location_matrix.comments,
      l_tb_location_matrix.last_used_timestamp, l_tb_location_matrix.update_user_id, l_tb_location_matrix.update_timestamp );

   -- ****** Read and Process Transactions ****** -----------------------------------------
   OPEN transaction_detail_cursor
    FOR vc_transaction_detail_stmt;
   LOOP
      FETCH transaction_detail_cursor
       INTO transaction_detail;
       EXIT WHEN transaction_detail_cursor%NOTFOUND;

      -- Populate and/or Transaction Detail Clear working fields
      transaction_detail.transaction_ind := NULL;
      transaction_detail.suspend_ind := NULL;
      transaction_detail.location_matrix_id := l_tb_location_matrix.location_matrix_id;
      transaction_detail.jurisdiction_id := l_tb_location_matrix.jurisdiction_id;
      transaction_detail.update_user_id := USER;
      transaction_detail.update_timestamp := v_sysdate;

      -- Continue if no Matrix ID
      IF transaction_detail.tax_matrix_id IS NULL OR transaction_detail.tax_matrix_id = 0 THEN
         v_transaction_detail_id := transaction_detail.transaction_detail_id;
         v_transaction_state_code := transaction_detail.transaction_state_code;

         -- Does Transaction Record have a valid State?
         IF v_transaction_state_code IS NULL THEN
            vn_fetch_rows := 0;
         ELSE
            SELECT COUNT(*)
              INTO vn_fetch_rows
              FROM TB_TAXCODE_STATE
             WHERE taxcode_state_code = v_transaction_state_code;
         END IF;
         IF vn_fetch_rows = 0 THEN
            -- Get State from new Jurisdiction
            v_transaction_state_code := NULL;
            BEGIN
               SELECT state
                 INTO v_transaction_state_code
                 FROM TB_JURISDICTION
                WHERE jurisdiction_id = l_tb_location_matrix.jurisdiction_id;
               IF SQLCODE != 0 THEN
                  RAISE e_badread;
               END IF;
            EXCEPTION
               WHEN e_badread THEN
                  v_transaction_state_code := NULL;
               WHEN NO_DATA_FOUND THEN
                  v_transaction_state_code := NULL;
            END;
            -- Jurisdiction Line Found
            IF v_transaction_state_code IS NOT NULL THEN
               IF transaction_detail.transaction_state_code IS NULL THEN
                  transaction_detail.auto_transaction_state_code := '*NULL';
               ELSE
                  transaction_detail.auto_transaction_state_code := transaction_detail.transaction_state_code;
               END IF;
               transaction_detail.transaction_state_code := v_transaction_state_code;
            ELSE
               transaction_detail.transaction_ind := 'S';
               transaction_detail.suspend_ind := 'L';
            END IF;
         END IF;

         -- Continue if not Suspended
         IF transaction_detail.transaction_ind IS NULL OR transaction_detail.transaction_ind <> 'S' THEN
            -- Search Tax Matrix for matches
            BEGIN
               IF vc_state_driver_flag = '1' THEN
                   OPEN tax_matrix_cursor
                    FOR vc_tax_matrix_stmt
                  USING v_transaction_detail_id, v_transaction_state_code, v_transaction_state_code;
                  FETCH tax_matrix_cursor
                   INTO tax_matrix;
               ELSE
                   OPEN tax_matrix_cursor
                    FOR vc_tax_matrix_stmt
                  USING v_transaction_detail_id, v_transaction_state_code;
                  FETCH tax_matrix_cursor
                   INTO tax_matrix;
               END IF;

               -- Rows found?
               IF tax_matrix_cursor%FOUND THEN
                  vn_fetch_rows := tax_matrix_cursor%ROWCOUNT;
               ELSE
                  vn_fetch_rows := 0;
               END IF;
               CLOSE tax_matrix_cursor;
               IF SQLCODE != 0 THEN
                  RAISE e_badread;
               END IF;
            EXCEPTION
               WHEN e_badread THEN
                  vn_fetch_rows := 0;
            END;

            -- Tax Matrix Line Found
            IF vn_fetch_rows > 0 THEN
               transaction_detail.tax_matrix_id := tax_matrix.tax_matrix_id;
               -- Determine "Then" or "Else" Results
               IF tax_matrix.relation_sign = 'na' THEN
                  tax_matrix.relation_amount := 0;
                  v_less_hold_code_flag := tax_matrix.then_hold_code_flag;
                  v_less_taxcode_detail_id := tax_matrix.then_taxcode_detail_id;
                  v_less_taxcode_state_code := tax_matrix.then_taxcode_state_code;
                  v_less_taxcode_type_code := tax_matrix.then_taxcode_type_code;
                  v_less_taxcode_code := tax_matrix.then_taxcode_code;
                  v_less_jurisdiction_id := tax_matrix.then_jurisdiction_id;
                  v_less_measure_type_code := tax_matrix.then_measure_type_code;
                  v_less_taxtype_code := tax_matrix.then_taxtype_code;
                  v_less_cch_taxcat_code := tax_matrix.then_cch_taxcat_code;
                  v_less_cch_group_code := tax_matrix.then_cch_group_code;
                  -- v_less_cch_item_code := tax_matrix.then_cch_item_code;
                  v_equal_hold_code_flag := tax_matrix.then_hold_code_flag;
                  v_equal_taxcode_detail_id := tax_matrix.then_taxcode_detail_id;
                  v_equal_taxcode_state_code := tax_matrix.then_taxcode_state_code;
                  v_equal_taxcode_type_code := tax_matrix.then_taxcode_type_code;
                  v_equal_taxcode_code := tax_matrix.then_taxcode_code;
                  v_equal_jurisdiction_id := tax_matrix.then_jurisdiction_id;
                  v_equal_measure_type_code := tax_matrix.then_measure_type_code;
                  v_equal_taxtype_code := tax_matrix.then_taxtype_code;
                  v_equal_cch_taxcat_code := tax_matrix.then_cch_taxcat_code;
                  v_equal_cch_group_code := tax_matrix.then_cch_group_code;
                  -- v_equal_cch_item_code := tax_matrix.then_cch_item_code;
                  v_greater_hold_code_flag := tax_matrix.then_hold_code_flag;
                  v_greater_taxcode_detail_id := tax_matrix.then_taxcode_detail_id;
                  v_greater_taxcode_state_code := tax_matrix.then_taxcode_state_code;
                  v_greater_taxcode_type_code := tax_matrix.then_taxcode_type_code;
                  v_greater_taxcode_code := tax_matrix.then_taxcode_code;
                  v_greater_jurisdiction_id := tax_matrix.then_jurisdiction_id;
                  v_greater_measure_type_code := tax_matrix.then_measure_type_code;
                  v_greater_taxtype_code := tax_matrix.then_taxtype_code;
                  v_greater_cch_taxcat_code := tax_matrix.then_cch_taxcat_code;
                  v_greater_cch_group_code := tax_matrix.then_cch_group_code;
                  -- v_greater_cch_item_code := tax_matrix.then_cch_item_code;
              ELSIF tax_matrix.relation_sign = '=' THEN
                  v_less_hold_code_flag := tax_matrix.else_hold_code_flag;
                  v_less_taxcode_detail_id := tax_matrix.else_taxcode_detail_id;
                  v_less_taxcode_state_code := tax_matrix.else_taxcode_state_code;
                  v_less_taxcode_type_code := tax_matrix.else_taxcode_type_code;
                  v_less_taxcode_code := tax_matrix.else_taxcode_code;
                  v_less_jurisdiction_id := tax_matrix.else_jurisdiction_id;
                  v_less_measure_type_code := tax_matrix.else_measure_type_code;
                  v_less_taxtype_code := tax_matrix.else_taxtype_code;
                  v_less_cch_taxcat_code := tax_matrix.else_cch_taxcat_code;
                  v_less_cch_group_code := tax_matrix.else_cch_group_code;
                  -- v_less_cch_item_code := tax_matrix.else_cch_item_code;
                  v_equal_hold_code_flag := tax_matrix.then_hold_code_flag;
                  v_equal_taxcode_detail_id := tax_matrix.then_taxcode_detail_id;
                  v_equal_taxcode_state_code := tax_matrix.then_taxcode_state_code;
                  v_equal_taxcode_type_code := tax_matrix.then_taxcode_type_code;
                  v_equal_taxcode_code := tax_matrix.then_taxcode_code;
                  v_equal_jurisdiction_id := tax_matrix.then_jurisdiction_id;
                  v_equal_measure_type_code := tax_matrix.then_measure_type_code;
                  v_equal_taxtype_code := tax_matrix.then_taxtype_code;
                  v_equal_cch_taxcat_code := tax_matrix.then_cch_taxcat_code;
                  v_equal_cch_group_code := tax_matrix.then_cch_group_code;
                  -- v_equal_cch_item_code := tax_matrix.then_cch_item_code;
                  v_greater_hold_code_flag := tax_matrix.else_hold_code_flag;
                  v_greater_taxcode_detail_id := tax_matrix.else_taxcode_detail_id;
                  v_greater_taxcode_state_code := tax_matrix.else_taxcode_state_code;
                  v_greater_taxcode_type_code := tax_matrix.else_taxcode_type_code;
                  v_greater_taxcode_code := tax_matrix.else_taxcode_code;
                  v_greater_jurisdiction_id := tax_matrix.else_jurisdiction_id;
                  v_greater_measure_type_code := tax_matrix.else_measure_type_code;
                  v_greater_taxtype_code := tax_matrix.else_taxtype_code;
                  v_greater_cch_taxcat_code := tax_matrix.else_cch_taxcat_code;
                  v_greater_cch_group_code := tax_matrix.else_cch_group_code;
                  -- v_greater_cch_item_code := tax_matrix.else_cch_item_code;
               ELSIF tax_matrix.relation_sign = '<' THEN
                  v_less_hold_code_flag := tax_matrix.then_hold_code_flag;
                  v_less_taxcode_detail_id := tax_matrix.then_taxcode_detail_id;
                  v_less_taxcode_state_code := tax_matrix.then_taxcode_state_code;
                  v_less_taxcode_type_code := tax_matrix.then_taxcode_type_code;
                  v_less_taxcode_code := tax_matrix.then_taxcode_code;
                  v_less_jurisdiction_id := tax_matrix.then_jurisdiction_id;
                  v_less_measure_type_code := tax_matrix.then_measure_type_code;
                  v_less_taxtype_code := tax_matrix.then_taxtype_code;
                  v_less_cch_taxcat_code := tax_matrix.then_cch_taxcat_code;
                  v_less_cch_group_code := tax_matrix.then_cch_group_code;
                  -- v_less_cch_item_code := tax_matrix.then_cch_item_code;
                  v_equal_hold_code_flag := tax_matrix.else_hold_code_flag;
                  v_equal_taxcode_detail_id := tax_matrix.else_taxcode_detail_id;
                  v_equal_taxcode_state_code := tax_matrix.else_taxcode_state_code;
                  v_equal_taxcode_type_code := tax_matrix.else_taxcode_type_code;
                  v_equal_taxcode_code := tax_matrix.else_taxcode_code;
                  v_equal_jurisdiction_id := tax_matrix.else_jurisdiction_id;
                  v_equal_measure_type_code := tax_matrix.else_measure_type_code;
                  v_equal_taxtype_code := tax_matrix.else_taxtype_code;
                  v_equal_cch_taxcat_code := tax_matrix.else_cch_taxcat_code;
                  v_equal_cch_group_code := tax_matrix.else_cch_group_code;
                  -- v_equal_cch_item_code := tax_matrix.else_cch_item_code;
                  v_greater_hold_code_flag := tax_matrix.else_hold_code_flag;
                  v_greater_taxcode_detail_id := tax_matrix.else_taxcode_detail_id;
                  v_greater_taxcode_state_code := tax_matrix.else_taxcode_state_code;
                  v_greater_taxcode_type_code := tax_matrix.else_taxcode_type_code;
                  v_greater_taxcode_code := tax_matrix.else_taxcode_code;
                  v_greater_jurisdiction_id := tax_matrix.else_jurisdiction_id;
                  v_greater_measure_type_code := tax_matrix.else_measure_type_code;
                  v_greater_taxtype_code := tax_matrix.else_taxtype_code;
                  v_greater_cch_taxcat_code := tax_matrix.else_cch_taxcat_code;
                  v_greater_cch_group_code := tax_matrix.else_cch_group_code;
                  -- v_greater_cch_item_code := tax_matrix.else_cch_item_code;
               ELSIF tax_matrix.relation_sign = '<=' THEN
                  v_less_hold_code_flag := tax_matrix.then_hold_code_flag;
                  v_less_taxcode_detail_id := tax_matrix.then_taxcode_detail_id;
                  v_less_taxcode_state_code := tax_matrix.then_taxcode_state_code;
                  v_less_taxcode_type_code := tax_matrix.then_taxcode_type_code;
                  v_less_taxcode_code := tax_matrix.then_taxcode_code;
                  v_less_jurisdiction_id := tax_matrix.then_jurisdiction_id;
                  v_less_measure_type_code := tax_matrix.then_measure_type_code;
                  v_less_taxtype_code := tax_matrix.then_taxtype_code;
                  v_less_cch_taxcat_code := tax_matrix.then_cch_taxcat_code;
                  v_less_cch_group_code := tax_matrix.then_cch_group_code;
                  -- v_less_cch_item_code := tax_matrix.then_cch_item_code;
                  v_equal_hold_code_flag := tax_matrix.then_hold_code_flag;
                  v_equal_taxcode_detail_id := tax_matrix.then_taxcode_detail_id;
                  v_equal_taxcode_state_code := tax_matrix.then_taxcode_state_code;
                  v_equal_taxcode_type_code := tax_matrix.then_taxcode_type_code;
                  v_equal_taxcode_code := tax_matrix.then_taxcode_code;
                  v_equal_jurisdiction_id := tax_matrix.then_jurisdiction_id;
                  v_equal_measure_type_code := tax_matrix.then_measure_type_code;
                  v_equal_taxtype_code := tax_matrix.then_taxtype_code;
                  v_equal_cch_taxcat_code := tax_matrix.then_cch_taxcat_code;
                  v_equal_cch_group_code := tax_matrix.then_cch_group_code;
                  -- v_equal_cch_item_code := tax_matrix.then_cch_item_code;
                  v_greater_hold_code_flag := tax_matrix.else_hold_code_flag;
                  v_greater_taxcode_detail_id := tax_matrix.else_taxcode_detail_id;
                  v_greater_taxcode_state_code := tax_matrix.else_taxcode_state_code;
                  v_greater_taxcode_type_code := tax_matrix.else_taxcode_type_code;
                  v_greater_taxcode_code := tax_matrix.else_taxcode_code;
                  v_greater_jurisdiction_id := tax_matrix.else_jurisdiction_id;
                  v_greater_measure_type_code := tax_matrix.else_measure_type_code;
                  v_greater_taxtype_code := tax_matrix.else_taxtype_code;
                  v_greater_cch_taxcat_code := tax_matrix.else_cch_taxcat_code;
                  v_greater_cch_group_code := tax_matrix.else_cch_group_code;
                  -- v_greater_cch_item_code := tax_matrix.else_cch_item_code;
               ELSIF tax_matrix.relation_sign = '>' THEN
                  v_less_hold_code_flag := tax_matrix.else_hold_code_flag;
                  v_less_taxcode_detail_id := tax_matrix.else_taxcode_detail_id;
                  v_less_taxcode_state_code := tax_matrix.else_taxcode_state_code;
                  v_less_taxcode_type_code := tax_matrix.else_taxcode_type_code;
                  v_less_taxcode_code := tax_matrix.else_taxcode_code;
                  v_less_jurisdiction_id := tax_matrix.else_jurisdiction_id;
                  v_less_measure_type_code := tax_matrix.else_measure_type_code;
                  v_less_taxtype_code := tax_matrix.else_taxtype_code;
                  v_less_cch_taxcat_code := tax_matrix.else_cch_taxcat_code;
                  v_less_cch_group_code := tax_matrix.else_cch_group_code;
                  -- v_less_cch_item_code := tax_matrix.else_cch_item_code;
                  v_equal_hold_code_flag := tax_matrix.else_hold_code_flag;
                  v_equal_taxcode_detail_id := tax_matrix.else_taxcode_detail_id;
                  v_equal_taxcode_state_code := tax_matrix.else_taxcode_state_code;
                  v_equal_taxcode_type_code := tax_matrix.else_taxcode_type_code;
                  v_equal_taxcode_code := tax_matrix.else_taxcode_code;
                  v_equal_jurisdiction_id := tax_matrix.else_jurisdiction_id;
                  v_equal_measure_type_code := tax_matrix.else_measure_type_code;
                  v_equal_taxtype_code := tax_matrix.else_taxtype_code;
                  v_equal_cch_taxcat_code := tax_matrix.else_cch_taxcat_code;
                  v_equal_cch_group_code := tax_matrix.else_cch_group_code;
                  -- v_equal_cch_item_code := tax_matrix.else_cch_item_code;
                  v_greater_hold_code_flag := tax_matrix.then_hold_code_flag;
                  v_greater_taxcode_detail_id := tax_matrix.then_taxcode_detail_id;
                  v_greater_taxcode_state_code := tax_matrix.then_taxcode_state_code;
                  v_greater_taxcode_type_code := tax_matrix.then_taxcode_type_code;
                  v_greater_taxcode_code := tax_matrix.then_taxcode_code;
                  v_greater_jurisdiction_id := tax_matrix.then_jurisdiction_id;
                  v_greater_measure_type_code := tax_matrix.then_measure_type_code;
                  v_greater_taxtype_code := tax_matrix.then_taxtype_code;
                  v_greater_cch_taxcat_code := tax_matrix.then_cch_taxcat_code;
                  v_greater_cch_group_code := tax_matrix.then_cch_group_code;
                  -- v_greater_cch_item_code := tax_matrix.then_cch_item_code;
               ELSIF tax_matrix.relation_sign = '>=' THEN
                  v_less_hold_code_flag := tax_matrix.else_hold_code_flag;
                  v_less_taxcode_detail_id := tax_matrix.else_taxcode_detail_id;
                  v_less_taxcode_state_code := tax_matrix.else_taxcode_state_code;
                  v_less_taxcode_type_code := tax_matrix.else_taxcode_type_code;
                  v_less_taxcode_code := tax_matrix.else_taxcode_code;
                  v_less_jurisdiction_id := tax_matrix.else_jurisdiction_id;
                  v_less_measure_type_code := tax_matrix.else_measure_type_code;
                  v_less_taxtype_code := tax_matrix.else_taxtype_code;
                  v_less_cch_taxcat_code := tax_matrix.else_cch_taxcat_code;
                  v_less_cch_group_code := tax_matrix.else_cch_group_code;
                  -- v_less_cch_item_code := tax_matrix.else_cch_item_code;
                  v_equal_hold_code_flag := tax_matrix.then_hold_code_flag;
                  v_equal_taxcode_detail_id := tax_matrix.then_taxcode_detail_id;
                  v_equal_taxcode_state_code := tax_matrix.then_taxcode_state_code;
                  v_equal_taxcode_type_code := tax_matrix.then_taxcode_type_code;
                  v_equal_taxcode_code := tax_matrix.then_taxcode_code;
                  v_equal_jurisdiction_id := tax_matrix.then_jurisdiction_id;
                  v_equal_measure_type_code := tax_matrix.then_measure_type_code;
                  v_equal_taxtype_code := tax_matrix.then_taxtype_code;
                  v_equal_cch_taxcat_code := tax_matrix.then_cch_taxcat_code;
                  v_equal_cch_group_code := tax_matrix.then_cch_group_code;
                  -- v_equal_cch_item_code := tax_matrix.then_cch_item_code;
                  v_greater_hold_code_flag := tax_matrix.then_hold_code_flag;
                  v_greater_taxcode_detail_id := tax_matrix.then_taxcode_detail_id;
                  v_greater_taxcode_state_code := tax_matrix.then_taxcode_state_code;
                  v_greater_taxcode_type_code := tax_matrix.then_taxcode_type_code;
                  v_greater_taxcode_code := tax_matrix.then_taxcode_code;
                  v_greater_jurisdiction_id := tax_matrix.then_jurisdiction_id;
                  v_greater_measure_type_code := tax_matrix.then_measure_type_code;
                  v_greater_taxtype_code := tax_matrix.then_taxtype_code;
                  v_greater_cch_taxcat_code := tax_matrix.then_cch_taxcat_code;
                  v_greater_cch_group_code := tax_matrix.then_cch_group_code;
                  -- v_greater_cch_item_code := tax_matrix.then_cch_item_code;
               ELSIF tax_matrix.relation_sign = '<>' THEN
                  v_less_hold_code_flag := tax_matrix.then_hold_code_flag;
                  v_less_taxcode_detail_id := tax_matrix.then_taxcode_detail_id;
                  v_less_taxcode_state_code := tax_matrix.then_taxcode_state_code;
                  v_less_taxcode_type_code := tax_matrix.then_taxcode_type_code;
                  v_less_taxcode_code := tax_matrix.then_taxcode_code;
                  v_less_jurisdiction_id := tax_matrix.then_jurisdiction_id;
                  v_less_measure_type_code := tax_matrix.then_measure_type_code;
                  v_less_taxtype_code := tax_matrix.then_taxtype_code;
                  v_less_cch_taxcat_code := tax_matrix.then_cch_taxcat_code;
                  v_less_cch_group_code := tax_matrix.then_cch_group_code;
                  -- v_less_cch_item_code := tax_matrix.then_cch_item_code;
                  v_equal_hold_code_flag := tax_matrix.else_hold_code_flag;
                  v_equal_taxcode_detail_id := tax_matrix.else_taxcode_detail_id;
                  v_equal_taxcode_state_code := tax_matrix.else_taxcode_state_code;
                  v_equal_taxcode_type_code := tax_matrix.else_taxcode_type_code;
                  v_equal_taxcode_code := tax_matrix.else_taxcode_code;
                  v_equal_jurisdiction_id := tax_matrix.else_jurisdiction_id;
                  v_equal_measure_type_code := tax_matrix.else_measure_type_code;
                  v_equal_taxtype_code := tax_matrix.else_taxtype_code;
                  v_equal_cch_taxcat_code := tax_matrix.else_cch_taxcat_code;
                  v_equal_cch_group_code := tax_matrix.else_cch_group_code;
                  -- v_equal_cch_item_code := tax_matrix.else_cch_item_code;
                  v_greater_hold_code_flag := tax_matrix.then_hold_code_flag;
                  v_greater_taxcode_detail_id := tax_matrix.then_taxcode_detail_id;
                  v_greater_taxcode_state_code := tax_matrix.then_taxcode_state_code;
                  v_greater_taxcode_type_code := tax_matrix.then_taxcode_type_code;
                  v_greater_taxcode_code := tax_matrix.then_taxcode_code;
                  v_greater_jurisdiction_id := tax_matrix.then_jurisdiction_id;
                  v_greater_measure_type_code := tax_matrix.then_measure_type_code;
                  v_greater_taxtype_code := tax_matrix.then_taxtype_code;
                  v_greater_cch_taxcat_code := tax_matrix.then_cch_taxcat_code;
                  v_greater_cch_group_code := tax_matrix.then_cch_group_code;
                  -- v_greater_cch_item_code := tax_matrix.then_cch_item_code;
               END IF;

               SELECT DECODE(SIGN(transaction_detail.gl_line_itm_dist_amt - tax_matrix.relation_amount), -1, v_less_hold_code_flag, 0, v_equal_hold_code_flag, 1, v_greater_hold_code_flag),
                      DECODE(SIGN(transaction_detail.gl_line_itm_dist_amt - tax_matrix.relation_amount), -1, v_less_taxcode_detail_id, 0, v_equal_taxcode_detail_id, 1, v_greater_taxcode_detail_id),
                      DECODE(SIGN(transaction_detail.gl_line_itm_dist_amt - tax_matrix.relation_amount), -1, v_less_taxcode_state_code, 0, v_equal_taxcode_state_code, 1, v_greater_taxcode_state_code),
                      DECODE(SIGN(transaction_detail.gl_line_itm_dist_amt - tax_matrix.relation_amount), -1, v_less_taxcode_type_code, 0, v_equal_taxcode_type_code, 1, v_greater_taxcode_type_code),
                      DECODE(SIGN(transaction_detail.gl_line_itm_dist_amt - tax_matrix.relation_amount), -1, v_less_taxcode_code, 0, v_equal_taxcode_code, 1, v_greater_taxcode_code),
                      DECODE(SIGN(transaction_detail.gl_line_itm_dist_amt - tax_matrix.relation_amount), -1, v_less_jurisdiction_id, 0, v_equal_jurisdiction_id, 1, v_greater_jurisdiction_id),
                      DECODE(SIGN(transaction_detail.gl_line_itm_dist_amt - tax_matrix.relation_amount), -1, v_less_measure_type_code, 0, v_equal_measure_type_code, 1, v_greater_measure_type_code),
                      DECODE(SIGN(transaction_detail.gl_line_itm_dist_amt - tax_matrix.relation_amount), -1, v_less_taxtype_code, 0, v_equal_taxtype_code, 1, v_greater_taxtype_code),
                      DECODE(SIGN(transaction_detail.gl_line_itm_dist_amt - tax_matrix.relation_amount), -1, v_less_cch_taxcat_code, 0, v_equal_cch_taxcat_code, 1, v_greater_cch_taxcat_code),
                      DECODE(SIGN(transaction_detail.gl_line_itm_dist_amt - tax_matrix.relation_amount), -1, v_less_cch_group_code, 0, v_equal_cch_group_code, 1, v_greater_cch_group_code)
                 INTO v_hold_code_flag, transaction_detail.taxcode_detail_id, transaction_detail.taxcode_state_code, transaction_detail.taxcode_type_code, transaction_detail.taxcode_code, v_override_jurisdiction_id, transaction_detail.measure_type_code, v_taxtype_code, transaction_detail.cch_taxcat_code, transaction_detail.cch_group_code
                 FROM DUAL;
            ELSE
            -- Tax Matrix Line NOT Found
               transaction_detail.transaction_ind := 'S';
               transaction_detail.suspend_ind := 'T';
            END IF;
         END IF;
      ELSE
         -- Get Tax Type Codes
         BEGIN
           SELECT taxtype_code
           INTO v_taxtype_code
           FROM tb_taxcode
           WHERE taxcode_code = transaction_detail.taxcode_code
             AND taxcode_type_code = transaction_detail.taxcode_type_code;
          EXCEPTION
            WHEN OTHERS THEN
              v_taxtype_code := 'U';
          END;
      END IF;

      -- Continue if not Suspended
      IF transaction_detail.transaction_ind IS NULL OR transaction_detail.transaction_ind <> 'S' THEN
         -- ****** DETERMINE TYPE of TAXCODE ****** --
         -- The Tax Code is Taxable
         IF transaction_detail.taxcode_type_code = 'T' THEN
            -- If Override jurisdiction id is not blank or 0 then use it instead of location matrix
            IF v_override_jurisdiction_id IS NOT NULL AND v_override_jurisdiction_id <> 0 THEN
               transaction_detail.jurisdiction_id := v_override_jurisdiction_id;
               transaction_detail.manual_jurisdiction_ind := 'AO';
            END IF;

            -- Get Jurisdiction TaxRates and calculate Tax Amounts
            Sp_Getusetax(transaction_detail.jurisdiction_id,
                         transaction_detail.gl_date,
                         transaction_detail.measure_type_code,
                         transaction_detail.gl_line_itm_dist_amt,
                         v_taxtype_code,
                         l_tb_location_matrix.override_taxtype_code,
                         l_tb_location_matrix.state_flag,
                         l_tb_location_matrix.county_flag,
                         l_tb_location_matrix.county_local_flag,
                         l_tb_location_matrix.city_flag,
                         l_tb_location_matrix.city_local_flag,
                         transaction_detail.invoice_freight_amt,
                         transaction_detail.invoice_discount_amt,
                         transaction_detail.transaction_state_code,
-- future? --                  transaction_detail.import_definition_code,
                         'importdefn',
                         transaction_detail.taxcode_code,
                         transaction_detail.jurisdiction_taxrate_id,
                         transaction_detail.state_use_amount,
                         transaction_detail.state_use_tier2_amount,
                         transaction_detail.state_use_tier3_amount,
                         transaction_detail.county_use_amount,
                         transaction_detail.county_local_use_amount,
                         transaction_detail.city_use_amount,
                         transaction_detail.city_local_use_amount,
                         transaction_detail.state_use_rate,
                         transaction_detail.state_use_tier2_rate,
                         transaction_detail.state_use_tier3_rate,
                         transaction_detail.state_split_amount,
                         transaction_detail.state_tier2_min_amount,
                         transaction_detail.state_tier2_max_amount,
                         transaction_detail.state_maxtax_amount,
                         transaction_detail.county_use_rate,
                         transaction_detail.county_local_use_rate,
                         transaction_detail.county_split_amount,
                         transaction_detail.county_maxtax_amount,
                         transaction_detail.county_single_flag,
                         transaction_detail.county_default_flag,
                         transaction_detail.city_use_rate,
                         transaction_detail.city_local_use_rate,
                         transaction_detail.city_split_amount,
                         transaction_detail.city_split_use_rate,
                         transaction_detail.city_single_flag,
                         transaction_detail.city_default_flag,
                         transaction_detail.combined_use_rate,
                         transaction_detail.tb_calc_tax_amt,
-- future --               transaction_detail.taxable_amt,
-- Targa? --                 transaction_detail.user_number_10,
                         vn_taxable_amt,
                         v_taxtype_used );

            -- Suspend if tax rate id is null
            IF transaction_detail.jurisdiction_taxrate_id IS NULL OR transaction_detail.jurisdiction_taxrate_id = 0 THEN
               transaction_detail.transaction_ind := 'S';
               transaction_detail.suspend_ind := 'R';
            ELSE
               -- Check for Taxable Amount -- 3351
               IF vn_taxable_amt <> transaction_detail.gl_line_itm_dist_amt THEN
                  transaction_detail.gl_extract_amt := transaction_detail.gl_line_itm_dist_amt;
                  transaction_detail.gl_line_itm_dist_amt := vn_taxable_amt;
               END IF;
            END IF;

         -- The Tax Code is Exempt
         ELSIF transaction_detail.taxcode_type_code = 'E' THEN
            transaction_detail.transaction_ind := 'P';

         -- The Tax Code is a Question
         ELSIF transaction_detail.taxcode_type_code = 'Q' THEN
           transaction_detail.transaction_ind := 'Q';

         -- The Tax Code is Unrecognized - Suspend
         ELSE
            transaction_detail.transaction_ind := 'S';
            transaction_detail.suspend_ind := '?';
         END IF;
      END IF;

      -- Continue if not Suspended
      IF transaction_detail.transaction_ind IS NULL OR transaction_detail.transaction_ind <> 'S' THEN
         -- Check for matrix "H"old
         IF ( v_hold_code_flag = '1' ) AND
            ( transaction_detail.taxcode_type_code = 'T' OR transaction_detail.taxcode_type_code = 'E') THEN
               transaction_detail.transaction_ind := 'H';
         ELSE
             transaction_detail.transaction_ind := 'P';
         END IF;
      END IF;

      -- Update transaction detail row
      BEGIN
         -- 3411 - add taxable amount and tax type used.
         UPDATE TB_TRANSACTION_DETAIL
            SET gl_line_itm_dist_amt = transaction_detail.gl_line_itm_dist_amt,
                tb_calc_tax_amt = transaction_detail.tb_calc_tax_amt,
                state_use_amount = transaction_detail.state_use_amount,
                state_use_tier2_amount = transaction_detail.state_use_tier2_amount,
                state_use_tier3_amount = transaction_detail.state_use_tier3_amount,
                county_use_amount = transaction_detail.county_use_amount,
                county_local_use_amount = transaction_detail.county_local_use_amount,
                city_use_amount = transaction_detail.city_use_amount,
                city_local_use_amount = transaction_detail.city_local_use_amount,
                transaction_state_code = transaction_detail.transaction_state_code,
                auto_transaction_state_code = transaction_detail.auto_transaction_state_code,
                transaction_ind = transaction_detail.transaction_ind,
                suspend_ind = transaction_detail.suspend_ind,
                taxcode_detail_id = transaction_detail.taxcode_detail_id,
                taxcode_state_code = transaction_detail.taxcode_state_code,
                taxcode_type_code = transaction_detail.taxcode_type_code,
                taxcode_code = transaction_detail.taxcode_code,
                cch_taxcat_code = transaction_detail.cch_taxcat_code,
                cch_group_code =  transaction_detail.cch_group_code,
                tax_matrix_id = transaction_detail.tax_matrix_id,
                location_matrix_id = transaction_detail.location_matrix_id,
                jurisdiction_id = transaction_detail.jurisdiction_id,
                jurisdiction_taxrate_id = transaction_detail.jurisdiction_taxrate_id,
                manual_jurisdiction_ind = transaction_detail.manual_jurisdiction_ind,
                measure_type_code = transaction_detail.measure_type_code,
                state_use_rate = transaction_detail.state_use_rate,
                state_use_tier2_rate = transaction_detail.state_use_tier2_rate,
                state_use_tier3_rate = transaction_detail.state_use_tier3_rate,
                state_split_amount = transaction_detail.state_split_amount,
                state_tier2_min_amount = transaction_detail.state_tier2_min_amount,
                state_tier2_max_amount = transaction_detail.state_tier2_max_amount,
                state_maxtax_amount = transaction_detail.state_maxtax_amount,
                county_use_rate = transaction_detail.county_use_rate,
                county_local_use_rate = transaction_detail.county_local_use_rate,
                county_split_amount = transaction_detail.county_split_amount,
                county_maxtax_amount = transaction_detail.county_maxtax_amount,
                county_single_flag = transaction_detail.county_single_flag,
                county_default_flag = transaction_detail.county_default_flag,
                city_use_rate = transaction_detail.city_use_rate,
                city_local_use_rate = transaction_detail.city_local_use_rate,
                city_split_amount = transaction_detail.city_split_amount,
                city_split_use_rate = transaction_detail.city_split_use_rate,
                city_single_flag = transaction_detail.city_single_flag,
                city_default_flag = transaction_detail.city_default_flag,
                combined_use_rate = transaction_detail.combined_use_rate,
                gl_extract_amt = transaction_detail.gl_extract_amt,
                update_user_id = transaction_detail.update_user_id,
                update_timestamp = transaction_detail.update_timestamp
          WHERE transaction_detail_id = transaction_detail.transaction_detail_id;
         -- Error Checking
--         IF SQLCODE != 0 THEN
--            RAISE e_badupdate;
--         END IF;
--      EXCEPTION
--         WHEN e_badupdate THEN
--            NULL;
      END;

   END LOOP;
   CLOSE transaction_detail_cursor;

END IF;

--EXCEPTION
--   WHEN OTHERS THEN
--      NULL;
END;
/

CREATE OR REPLACE PROCEDURE Sp_Tb_Tax_Matrix_A_I(p_tax_matrix_id IN TB_TAX_MATRIX.tax_matrix_id%TYPE)
AS
/* ************************************************************************************************/
/* Object Type/Name: SP Trigger/After Insert - sp_tb_tax_matrix_a_i                               */
/* Author:           Michael B. Fuller                                                            */
/* Date:             a long time ago                                                              */
/* Description:      A Tax Matrix line has been added                                             */
/* Arguments:        None                                                                         */
/* Returns:          None                                                                         */
/* ************************************************************************************************/
/* Maintenance History:                                                                           */
/* Scan  Author       Date     Release    Description                                  Ticket #   */
/* ----- ---------- ---------- ---------- -------------------------------------------- ---------- */
/*       M. Fuller  06/02/2006 3.3.5.0    Fix processing an allocated line - Temporary 871        */
/*       M. Fuller  01/04/2007 3.3.5.1    Fix processing an allocated line - Permenant 871        */
/* MBF01 M. Fuller  08/13/2007 3.4.1.1    Add 5 Calc Tax Flags                         883        */
/* JJF   J.Franco   12/17/2008 x.x.x.x    Convert trigger to stored procedure          xxx        */
/* ************************************************************************************************/

-- Defire Row Type for tb_tax_matrix record
   l_tb_tax_matrix TB_TAX_MATRIX%ROWTYPE;

-- Define Cursor Type
   TYPE cursor_type IS REF CURSOR;

-- Transaction Detail Selection SQL
   vc_transaction_detail_select    VARCHAR2(2000) :=
      'SELECT tb_transaction_detail.* ' ||
        'FROM tb_tmp_tax_matrix, ' ||
             'tb_transaction_detail ' ||
       'WHERE ( tb_tmp_tax_matrix.tax_matrix_id = ' || p_tax_matrix_id || ') AND ' ||
             '(( tb_tmp_tax_matrix.matrix_state_code = ''*ALL'' ) OR ( tb_tmp_tax_matrix.matrix_state_code = tb_transaction_detail.transaction_state_code )) AND ' ||
             '( tb_tmp_tax_matrix.effective_date <= tb_transaction_detail.gl_date ) AND ( tb_tmp_tax_matrix.expiration_date >= tb_transaction_detail.gl_date ) AND ' ||
             '( tb_transaction_detail.transaction_ind = ''S'' ) AND ( tb_transaction_detail.suspend_ind = ''T'' ) ';
   vc_transaction_detail_where     VARCHAR2(5000) := '';
   vc_transaction_detail_update    VARCHAR2(100) := 'FOR UPDATE';
   vc_transaction_detail_stmt      VARCHAR2(7100);
   transaction_detail_cursor       cursor_type;
   transaction_detail              TB_TRANSACTION_DETAIL%ROWTYPE;

-- Location Matrix Selection SQL
   vc_location_matrix_select       VARCHAR2(2000) :=
      'SELECT tb_location_matrix.location_matrix_id, ' ||
             'tb_location_matrix.jurisdiction_id, ' ||
             'tb_location_matrix.override_taxtype_code, ' ||
             'tb_location_matrix.state_flag,' ||
             'tb_location_matrix.county_flag,' ||
             'tb_location_matrix.county_local_flag,' ||
             'tb_location_matrix.city_flag,' ||
             'tb_location_matrix.city_local_flag,' ||
             'tb_jurisdiction.state, ' ||
             'tb_jurisdiction.county, ' ||
             'tb_jurisdiction.city, ' ||
             'tb_jurisdiction.zip, ' ||
             'tb_jurisdiction.in_out ' ||
        'FROM tb_location_matrix, ' ||
             'tb_jurisdiction, ' ||
             'tb_transaction_detail ' ||
      'WHERE ( tb_transaction_detail.transaction_detail_id = :v_transaction_detail_id ) AND ' ||
            '( tb_location_matrix.default_flag = ''0'' AND tb_location_matrix.binary_weight > 0 ) AND ' ||
            '( tb_location_matrix.effective_date <= tb_transaction_detail.gl_date ) AND ( tb_location_matrix.expiration_date >= tb_transaction_detail.gl_date ) AND ' ||
            '( tb_location_matrix.jurisdiction_id = tb_jurisdiction.jurisdiction_id ) ';
   vc_location_matrix_where        VARCHAR2(5000) := '';
   vc_location_matrix_orderby      VARCHAR2(1000) :=
      'ORDER BY tb_location_matrix.binary_weight DESC, tb_location_matrix.significant_digits DESC, tb_location_matrix.effective_date DESC';
   vc_location_matrix_stmt         VARCHAR2 (8000);
   location_matrix_cursor          cursor_type;

   vc_state_driver_flag            CHAR(1)                                           := NULL;

-- Location Matrix Record TYPE
   /*TYPE location_matrix_record is RECORD (
      location_matrix_id           tb_location_matrix.location_matrix_id%TYPE,
      jurisdiction_id              tb_location_matrix.jurisdiction_id%TYPE,
      override_taxtype_code        tb_location_matrix.override_taxtype_code%TYPE,
      state_flag                   tb_location_matrix.state_flag%TYPE,
      county_flag                  tb_location_matrix.county_flag%TYPE,
      county_local_flag            tb_location_matrix.county_local_flag%TYPE,
      city_flag                    tb_location_matrix.city_flag%TYPE,
      city_local_flag              tb_location_matrix.city_local_flag%TYPE,
      state                        tb_jurisdiction.state%TYPE,
      county                       tb_jurisdiction.county%TYPE,
      city                         tb_jurisdiction.city%TYPE,
      zip                          tb_jurisdiction.zip%TYPE,
      in_out                       tb_jurisdiction.in_out%TYPE );*/
   location_matrix                 Matrix_Record_Pkg.location_matrix_record;

-- Table defined variables
   v_relation_amount               TB_TAX_MATRIX.relation_amount%TYPE                := NULL;
   v_hold_code_flag                TB_TAX_MATRIX.then_hold_code_flag%TYPE            := NULL;
   v_less_hold_code_flag           TB_TAX_MATRIX.then_hold_code_flag%TYPE            := NULL;
   v_less_taxcode_detail_id        TB_TAX_MATRIX.then_taxcode_detail_id%TYPE         := NULL;
   v_less_cch_taxcat_code          TB_TAX_MATRIX.then_cch_taxcat_code%TYPE           := NULL;
   v_less_cch_group_code           TB_TAX_MATRIX.then_cch_group_code%TYPE            := NULL;
   v_equal_hold_code_flag          TB_TAX_MATRIX.then_hold_code_flag%TYPE            := NULL;
   v_equal_taxcode_detail_id       TB_TAX_MATRIX.then_taxcode_detail_id%TYPE         := NULL;
   v_equal_cch_taxcat_code         TB_TAX_MATRIX.then_cch_taxcat_code%TYPE           := NULL;
   v_equal_cch_group_code          TB_TAX_MATRIX.then_cch_group_code%TYPE            := NULL;
   v_greater_hold_code_flag        TB_TAX_MATRIX.then_hold_code_flag%TYPE            := NULL;
   v_greater_taxcode_detail_id     TB_TAX_MATRIX.then_taxcode_detail_id%TYPE         := NULL;
   v_greater_cch_taxcat_code       TB_TAX_MATRIX.then_cch_taxcat_code%TYPE           := NULL;
   v_greater_cch_group_code        TB_TAX_MATRIX.then_cch_group_code%TYPE            := NULL;
   v_then_taxcode_state_code       TB_TAXCODE_DETAIL.taxcode_state_code%TYPE         := NULL;
   v_then_taxcode_type_code        TB_TAXCODE_DETAIL.taxcode_type_code%TYPE          := NULL;
   v_then_taxcode_code             TB_TAXCODE_DETAIL.taxcode_code%TYPE               := NULL;
   v_then_jurisdiction_id          TB_TAXCODE_DETAIL.jurisdiction_id%TYPE            := NULL;
   v_then_measure_type_code        TB_TAXCODE_DETAIL.measure_type_code%TYPE          := NULL;
   v_then_taxtype_code             TB_TAXCODE_DETAIL.taxtype_code%TYPE               := NULL;
   v_else_taxcode_state_code       TB_TAXCODE_DETAIL.taxcode_state_code%TYPE         := NULL;
   v_else_taxcode_type_code        TB_TAXCODE_DETAIL.taxcode_type_code%TYPE          := NULL;
   v_else_taxcode_code             TB_TAXCODE_DETAIL.taxcode_code%TYPE               := NULL;
   v_else_jurisdiction_id          TB_TAXCODE_DETAIL.jurisdiction_id%TYPE            := NULL;
   v_else_measure_type_code        TB_TAXCODE_DETAIL.measure_type_code%TYPE          := NULL;
   v_else_taxtype_code             TB_TAXCODE_DETAIL.taxtype_code%TYPE               := NULL;
   v_less_taxcode_state_code       TB_TAXCODE_DETAIL.taxcode_state_code%TYPE         := NULL;
   v_less_taxcode_type_code        TB_TAXCODE_DETAIL.taxcode_type_code%TYPE          := NULL;
   v_less_taxcode_code             TB_TAXCODE_DETAIL.taxcode_code%TYPE               := NULL;
   v_less_jurisdiction_id          TB_TAXCODE_DETAIL.jurisdiction_id%TYPE            := NULL;
   v_less_measure_type_code        TB_TAXCODE_DETAIL.measure_type_code%TYPE          := NULL;
   v_less_taxtype_code             TB_TAXCODE_DETAIL.taxtype_code%TYPE               := NULL;
   v_equal_taxcode_state_code      TB_TAXCODE_DETAIL.taxcode_state_code%TYPE         := NULL;
   v_equal_taxcode_type_code       TB_TAXCODE_DETAIL.taxcode_type_code%TYPE          := NULL;
   v_equal_taxcode_code            TB_TAXCODE_DETAIL.taxcode_code%TYPE               := NULL;
   v_equal_jurisdiction_id         TB_TAXCODE_DETAIL.jurisdiction_id%TYPE            := NULL;
   v_equal_measure_type_code       TB_TAXCODE_DETAIL.measure_type_code%TYPE          := NULL;
   v_equal_taxtype_code            TB_TAXCODE_DETAIL.taxtype_code%TYPE               := NULL;
   v_greater_taxcode_state_code    TB_TAXCODE_DETAIL.taxcode_state_code%TYPE         := NULL;
   v_greater_taxcode_type_code     TB_TAXCODE_DETAIL.taxcode_type_code%TYPE          := NULL;
   v_greater_taxcode_code          TB_TAXCODE_DETAIL.taxcode_code%TYPE               := NULL;
   v_greater_jurisdiction_id       TB_TAXCODE_DETAIL.jurisdiction_id%TYPE            := NULL;
   v_greater_measure_type_code     TB_TAXCODE_DETAIL.measure_type_code%TYPE          := NULL;
   v_greater_taxtype_code          TB_TAXCODE_DETAIL.taxtype_code%TYPE               := NULL;
   v_transaction_detail_id         TB_TRANSACTION_DETAIL.transaction_detail_id%TYPE  := NULL;
   v_override_jurisdiction_id      TB_BCP_TRANSACTIONS.jurisdiction_id%TYPE          := NULL;
   v_sysdate                       TB_TRANSACTION_DETAIL.load_timestamp%TYPE         := SYS_EXTRACT_UTC(SYSTIMESTAMP);
   v_taxtype_code                  TB_TAXCODE.taxtype_code%TYPE                      := NULL;
   v_taxtype_used                  TB_TAXCODE.taxtype_code%TYPE                      := NULL;

-- Program defined variables
   vn_hold_trans_amount            NUMBER                                            := 0;
   vn_fetch_rows                   NUMBER                                            := 0;
   vi_error_count                  INTEGER                                           := 0;
   vn_actual_rowno                 NUMBER                                            := 0;
-- temporary
   vn_taxable_amt                  NUMBER                                            := 0;

   vc_test1 VARCHAR2(4000);
   vc_test2 VARCHAR2(4000);

-- Define Location Driver Names Cursor (tb_driver_names)
   /*CURSOR location_driver_cursor
   IS
        SELECT driver.trans_dtl_column_name, driver.matrix_column_name, driver.null_driver_flag, driver.wildcard_flag, driver.range_flag, driver.to_number_flag,
               datadefcol.desc_column_name, datadefcol.data_type
          FROM tb_driver_names driver,
               (SELECT *
                  FROM tb_data_def_column
                 WHERE table_name = 'TB_TRANSACTION_DETAIL') datadefcol
         WHERE driver.driver_names_code = 'L' AND
               driver.trans_dtl_column_name = datadefcol.column_name
      ORDER BY driver.driver_id;
      location_driver              location_driver_cursor%ROWTYPE;*/

-- Define Tax Driver Names Cursor (tb_driver_names)
   /*CURSOR tax_driver_cursor
   IS
        SELECT driver.trans_dtl_column_name, driver.matrix_column_name, driver.null_driver_flag, driver.wildcard_flag, driver.range_flag, driver.to_number_flag,
               datadefcol.desc_column_name, datadefcol.data_type
          FROM tb_driver_names driver,
               (SELECT *
                  FROM tb_data_def_column
                 WHERE table_name = 'TB_TRANSACTION_DETAIL') datadefcol
         WHERE driver.driver_names_code = 'T' AND
               driver.trans_dtl_column_name = datadefcol.column_name
      ORDER BY driver.driver_id;
      tax_driver                   tax_driver_cursor%ROWTYPE;*/

-- Define Exceptions
   e_badread                       EXCEPTION;
   e_badupdate                     EXCEPTION;

-- ****** Program starts ****** --------------------------------------------------------------------
BEGIN

SELECT TB_TAX_MATRIX.*
 INTO  l_tb_tax_matrix
 FROM  TB_TAX_MATRIX
 WHERE tax_matrix_id = p_tax_matrix_id;

IF l_tb_tax_matrix.default_flag <> '1' AND l_tb_tax_matrix.binary_weight > 0 THEN

   -- *******************************************************************************************************************************************
   -- NEW BATCH PROCESS CODE --------------------------------------------------------------------------------------------------------------------
   -- ***** Create Dynamic WHERE Clause for Transaction Detail SQL ***** --
   -- Tax Matrix Drivers
   /*OPEN tax_driver_cursor;
   LOOP
      FETCH tax_driver_cursor INTO tax_driver;
      EXIT WHEN tax_driver_cursor%NOTFOUND;
      IF tax_driver.range_flag = '1' THEN
         IF tax_driver.to_number_flag = '1' THEN
            IF tax_driver.null_driver_flag = '1' THEN
               -- Range and ToNumber and NULL
               IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                  UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                  vc_transaction_detail_where := vc_transaction_detail_where || 'AND ( ' ||
                  '( tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || '_THRU = ''*NA'' AND ' ||
                     '( tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NULL AND ( tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*NULL'' )) OR ' ||
                     '( tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND ( tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR DECODE(isnumber(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || '),1,to_char(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ',''00000000000000000000.00000000000000000000''),UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ')) = DECODE(isnumber(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || '),1,to_char(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ',''00000000000000000000.00000000000000000000''),UPPER(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ') ))) OR ' ||
                  '( tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || '_THRU <> ''*NA'' AND ' ||
                     '( tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND (DECODE(isnumber(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || '),1,to_char(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ',''00000000000000000000.00000000000000000000''),UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ')) >= DECODE(isnumber(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || '),1,to_char(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ',''00000000000000000000.00000000000000000000''),UPPER(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ') ) AND ' ||
                                                                                            'DECODE(isnumber(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || '),1,to_char(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ',''00000000000000000000.00000000000000000000''),UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ')) <= DECODE(isnumber(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || '_THRU ),1,to_char(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || '_THRU,''00000000000000000000.00000000000000000000''),UPPER(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || '_THRU)) )))) ';
               END IF;
            ELSE
               -- Range and ToNumber and NOT NULL
               IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                  UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                  vc_transaction_detail_where := vc_transaction_detail_where || 'AND ( ' ||
                  '( tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || '_THRU = ''*NA'' AND (tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR DECODE(isnumber(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || '),1,to_char(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ',''00000000000000000000.00000000000000000000''),UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ')) = DECODE(isnumber(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || '),1,to_char(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ',''00000000000000000000.00000000000000000000''),UPPER(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ')))) OR ' ||
                  '( tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || '_THRU <> ''*NA'' AND (DECODE(isnumber(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || '),1,to_char(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ',''00000000000000000000.00000000000000000000''),UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ')) >= DECODE(isnumber(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || '),1,to_char(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ',''00000000000000000000.00000000000000000000''),UPPER(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ')) AND ' ||
                                                                                        'DECODE(isnumber(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || '),1,to_char(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ',''00000000000000000000.00000000000000000000''),UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ')) <= DECODE(isnumber(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || '_THRU ),1,to_char(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || '_THRU,''00000000000000000000.00000000000000000000''),UPPER(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || '_THRU)) )) ';
               END IF;
            END IF;
         ELSE
            IF tax_driver.null_driver_flag = '1' THEN
               IF tax_driver.wildcard_flag = '1' THEN
                  -- Range and NOT ToNumber and NULL and Wildcard
                  IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                     UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                     vc_transaction_detail_where := vc_transaction_detail_where || 'AND ( ' ||
                     '( tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || '_THRU = ''*NA'' AND ' ||
                        '( tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NULL AND (tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*NULL'' )) OR ' ||
                        '( tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND (tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') LIKE UPPER(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ')))) OR ' ||
                     '( tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || '_THRU <> ''*NA'' AND (tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND (UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') >= UPPER(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ') AND UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') <= UPPER(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || '_THRU) ))) ';
                  END IF;
               ELSE
                  -- Range and NOT ToNumber and NULL and NOT Wildcard
                  IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                     UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                     vc_transaction_detail_where := vc_transaction_detail_where || 'AND ( ' ||
                     '( tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || '_THRU = ''*NA'' AND ' ||
                        '( tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NULL AND (tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*NULL'' )) OR ' ||
                        '( tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND (tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') = UPPER(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ')))) OR ' ||
                     '( tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || '_THRU <> ''*NA'' AND (tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND (UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') >= UPPER(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ') AND UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') <= UPPER(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || '_THRU) ))) ';
                  END IF;
               END IF;
            ELSE
               IF tax_driver.wildcard_flag = '1' THEN
                  -- Range and NOT ToNumber and NOT NULL and Wildcard
                  IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                     UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                     vc_transaction_detail_where := vc_transaction_detail_where || 'AND ( ' ||
                     '( tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || '_THRU = ''*NA'' AND ' ||
                        '(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') LIKE UPPER(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || '))) OR ' ||
                     '( tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || '_THRU <> ''*NA'' AND (UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') >= UPPER(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ') AND UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') <= UPPER(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || '_THRU) )) ';
                  END IF;
               ELSE
                  -- Range and NOT ToNumber and NOT NULL and NOT Wildcard
                  IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                     UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                     vc_transaction_detail_where := vc_transaction_detail_where || 'AND ( ' ||
                     '( tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || '_THRU = ''*NA'' AND ' ||
                        '(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') = UPPER(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || '))) OR ' ||
                     '( tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || '_THRU <> ''*NA'' AND (UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') >= UPPER(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ') AND UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') <= UPPER(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || '_THRU) )) ';
                  END IF;
               END IF;
            END IF;
         END IF;
      ELSE
         IF tax_driver.null_driver_flag = '1' THEN
            IF tax_driver.wildcard_flag = '1' THEN
               -- NOT Range and NULL and Wildcard
               IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                  UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                     vc_transaction_detail_where := vc_transaction_detail_where || 'AND ( ' ||
                     '( tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NULL AND (tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*NULL'' )) OR ' ||
                     '( tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND (tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') LIKE UPPER(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ')) ) ';
               END IF;
            ELSE
               -- NOT Range and NULL and NOT Wildcard
               IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                  UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                     vc_transaction_detail_where := vc_transaction_detail_where || 'AND ( ' ||
                     '( tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NULL AND (tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*NULL'' )) OR ' ||
                     '( tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND (tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') = UPPER(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ')) ) ';
               END IF;
            END IF;
         ELSE
            IF tax_driver.wildcard_flag = '1' THEN
               -- NOT Range and NOT NULL and Wildcard
               IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                  UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                     vc_transaction_detail_where := vc_transaction_detail_where || 'AND ( ' ||
                     '( tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') LIKE UPPER(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ') ) ';
               END IF;
            ELSE
               -- NOT Range and NOT NULL and NOT Wildcard
               IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                  UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                  vc_transaction_detail_where := vc_transaction_detail_where || 'AND ( ' ||
                     '( tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') = UPPER(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ') ) ';
               END IF;
            END IF;
         END IF;
      END IF;
      vc_transaction_detail_where := vc_transaction_detail_where || ') ';
   END LOOP;
   CLOSE tax_driver_cursor;*/

   -- New call to sp_gen_tax_driver for MidTier Cleanup
   Sp_Gen_Tax_Driver (
	p_generate_driver_reference	=> 'N',
	p_an_batch_id			=> NULL,
	p_transaction_table_name	=> 'tb_transaction_detail',
	p_tax_table_name		=> 'tb_tmp_tax_matrix',
	p_vc_state_driver_flag		=> vc_state_driver_flag,
	p_vc_tax_matrix_where		=> vc_transaction_detail_where);

   -- NEW BATCH PROCESS CODE --------------------------------------------------------------------------------------------------------------------
   -- *******************************************************************************************************************************************

   -- If no drivers found raise error, else create transaction detail sql statement
   IF vc_transaction_detail_where IS NULL THEN
      NULL;
   ELSE
      vc_transaction_detail_stmt := vc_transaction_detail_select || vc_transaction_detail_where || vc_transaction_detail_update;
   END IF;

   -- ***** Create Dynamic WHERE Clause for Matrix SQLs ***** --
   -- Location Matrix Drivers
   /*OPEN location_driver_cursor;
   LOOP
      FETCH location_driver_cursor INTO location_driver;
      EXIT WHEN location_driver_cursor%NOTFOUND;
      IF location_driver.null_driver_flag = '1' THEN
         IF location_driver.wildcard_flag = '1' THEN
            IF UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'CHAR' OR
               UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'VARC' THEN
                  vc_location_matrix_where := vc_location_matrix_where || 'AND ' ||
                  '((tb_transaction_detail.' || location_driver.trans_dtl_column_name || ' IS NULL AND (tb_location_matrix.' || location_driver.matrix_column_name || ' = ''*ALL'' OR tb_location_matrix.' || location_driver.matrix_column_name || ' = ''*NULL'' )) OR ' ||
                  '(tb_transaction_detail.' || location_driver.trans_dtl_column_name || ' IS NOT NULL AND (tb_location_matrix.' || location_driver.matrix_column_name || ' = ''*ALL'' OR UPPER(tb_transaction_detail.' || location_driver.trans_dtl_column_name || ') LIKE UPPER(tb_location_matrix.' || location_driver.matrix_column_name || ') ))) ';
            END IF;
         ELSE
            IF UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'CHAR' OR
               UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'VARC' THEN
                  vc_location_matrix_where := vc_location_matrix_where || 'AND ' ||
                  '((tb_transaction_detail.' || location_driver.trans_dtl_column_name || ' IS NULL AND (tb_location_matrix.' || location_driver.matrix_column_name || ' = ''*ALL'' OR tb_location_matrix.' || location_driver.matrix_column_name || ' = ''*NULL'' )) OR ' ||
                  '(tb_transaction_detail.' || location_driver.trans_dtl_column_name || ' IS NOT NULL AND (tb_location_matrix.' || location_driver.matrix_column_name || ' = ''*ALL'' OR UPPER(tb_transaction_detail.' || location_driver.trans_dtl_column_name || ') = UPPER(tb_location_matrix.' || location_driver.matrix_column_name || ') ))) ';
            END IF;
         END IF;
      ELSE
         IF location_driver.wildcard_flag = '1' THEN
            IF UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'CHAR' OR
               UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'VARC' THEN
                  vc_location_matrix_where := vc_location_matrix_where || 'AND ' ||
                  '(tb_location_matrix.' || location_driver.matrix_column_name || ' = ''*ALL'' OR UPPER(tb_transaction_detail.' || location_driver.trans_dtl_column_name || ') LIKE UPPER(tb_location_matrix.' || location_driver.matrix_column_name || ') ) ';
            END IF;
         ELSE
            IF UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'CHAR' OR
               UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'VARC' THEN
                  vc_location_matrix_where := vc_location_matrix_where || 'AND ' ||
                  '(tb_location_matrix.' || location_driver.matrix_column_name || ' = ''*ALL'' OR UPPER(tb_transaction_detail.' || location_driver.trans_dtl_column_name || ') = UPPER(tb_location_matrix.' || location_driver.matrix_column_name || ') ) ';
            END IF;
         END IF;
      END IF;
   END LOOP;
   CLOSE location_driver_cursor;*/

   -- New call to sp_gen_location_driver for MidTier Cleanup
   Sp_Gen_Location_Driver (
	p_generate_driver_reference	=> 'N',
	p_an_batch_id			=> NULL,
	p_transaction_table_name	=> 'tb_transaction_detail',
	p_location_table_name		=> 'tb_location_matrix',
	p_vc_location_matrix_where	=> vc_location_matrix_where);

   -- If no drivers found raise error, else create transaction detail sql statement
   IF vc_location_matrix_where IS NULL THEN
      NULL;
   ELSE
      vc_location_matrix_stmt := vc_location_matrix_select || vc_location_matrix_where || vc_location_matrix_orderby;
   END IF;

   -- Insert New Tax Matrix columns into temporary table -------------------------------------------
   INSERT INTO TB_TMP_TAX_MATRIX (
      tax_matrix_id, matrix_state_code, driver_global_flag,
      driver_01, driver_01_desc,
      driver_01_thru, driver_01_thru_desc,
      driver_02, driver_02_desc,
      driver_02_thru, driver_02_thru_desc,
      driver_03, driver_03_desc,
      driver_03_thru, driver_03_thru_desc,
      driver_04, driver_04_desc,
      driver_04_thru, driver_04_thru_desc,
      driver_05, driver_05_desc,
      driver_05_thru, driver_05_thru_desc,
      driver_06, driver_06_desc,
      driver_06_thru, driver_06_thru_desc,
      driver_07, driver_07_desc,
      driver_07_thru, driver_07_thru_desc,
      driver_08, driver_08_desc,
      driver_08_thru, driver_08_thru_desc,
      driver_09, driver_09_desc,
      driver_09_thru, driver_09_thru_desc,
      driver_10, driver_10_desc,
      driver_10_thru, driver_10_thru_desc,
      driver_11, driver_11_desc,
      driver_11_thru, driver_11_thru_desc,
      driver_12, driver_12_desc,
      driver_12_thru, driver_12_thru_desc,
      driver_13, driver_13_desc,
      driver_13_thru, driver_13_thru_desc,
      driver_14, driver_14_desc,
      driver_14_thru, driver_14_thru_desc,
      driver_15, driver_15_desc,
      driver_15_thru, driver_15_thru_desc,
      driver_16, driver_16_desc,
      driver_16_thru, driver_16_thru_desc,
      driver_17, driver_17_desc,
      driver_17_thru, driver_17_thru_desc,
      driver_18, driver_18_desc,
      driver_18_thru, driver_18_thru_desc,
      driver_19, driver_19_desc,
      driver_19_thru, driver_19_thru_desc,
      driver_20, driver_20_desc,
      driver_20_thru, driver_20_thru_desc,
      driver_21, driver_21_desc,
      driver_21_thru, driver_21_thru_desc,
      driver_22, driver_22_desc,
      driver_22_thru, driver_22_thru_desc,
      driver_23, driver_23_desc,
      driver_23_thru, driver_23_thru_desc,
      driver_24, driver_24_desc,
      driver_24_thru, driver_24_thru_desc,
      driver_25, driver_25_desc,
      driver_25_thru, driver_25_thru_desc,
      driver_26, driver_26_desc,
      driver_26_thru, driver_26_thru_desc,
      driver_27, driver_27_desc,
      driver_27_thru, driver_27_thru_desc,
      driver_28, driver_28_desc,
      driver_28_thru, driver_28_thru_desc,
      driver_29, driver_29_desc,
      driver_29_thru, driver_29_thru_desc,
      driver_30, driver_30_desc,
      driver_30_thru, driver_30_thru_desc,
      binary_weight, significant_digits,
      effective_date, expiration_date,
      relation_sign, relation_amount,
      then_hold_code_flag, then_taxcode_detail_id, then_cch_taxcat_code, then_cch_group_code, then_cch_item_code,
      else_hold_code_flag, else_taxcode_detail_id, else_cch_taxcat_code, else_cch_group_code, else_cch_item_code,
      comments,
      last_used_timestamp, update_user_id, update_timestamp )
   VALUES (
      l_tb_tax_matrix.tax_matrix_id, l_tb_tax_matrix.matrix_state_code, l_tb_tax_matrix.driver_global_flag,
      l_tb_tax_matrix.driver_01, l_tb_tax_matrix.driver_01_desc,
      l_tb_tax_matrix.driver_01_thru, l_tb_tax_matrix.driver_01_thru_desc,
      l_tb_tax_matrix.driver_02, l_tb_tax_matrix.driver_02_desc,
      l_tb_tax_matrix.driver_02_thru, l_tb_tax_matrix.driver_02_thru_desc,
      l_tb_tax_matrix.driver_03, l_tb_tax_matrix.driver_03_desc,
      l_tb_tax_matrix.driver_03_thru, l_tb_tax_matrix.driver_03_thru_desc,
      l_tb_tax_matrix.driver_04, l_tb_tax_matrix.driver_04_desc,
      l_tb_tax_matrix.driver_04_thru, l_tb_tax_matrix.driver_04_thru_desc,
      l_tb_tax_matrix.driver_05, l_tb_tax_matrix.driver_05_desc,
      l_tb_tax_matrix.driver_05_thru, l_tb_tax_matrix.driver_05_thru_desc,
      l_tb_tax_matrix.driver_06, l_tb_tax_matrix.driver_06_desc,
      l_tb_tax_matrix.driver_06_thru, l_tb_tax_matrix.driver_06_thru_desc,
      l_tb_tax_matrix.driver_07, l_tb_tax_matrix.driver_07_desc,
      l_tb_tax_matrix.driver_07_thru, l_tb_tax_matrix.driver_07_thru_desc,
      l_tb_tax_matrix.driver_08, l_tb_tax_matrix.driver_08_desc,
      l_tb_tax_matrix.driver_08_thru, l_tb_tax_matrix.driver_08_thru_desc,
      l_tb_tax_matrix.driver_09, l_tb_tax_matrix.driver_09_desc,
      l_tb_tax_matrix.driver_09_thru, l_tb_tax_matrix.driver_09_thru_desc,
      l_tb_tax_matrix.driver_10, l_tb_tax_matrix.driver_10_desc,
      l_tb_tax_matrix.driver_10_thru, l_tb_tax_matrix.driver_10_thru_desc,
      l_tb_tax_matrix.driver_11, l_tb_tax_matrix.driver_11_desc,
      l_tb_tax_matrix.driver_11_thru, l_tb_tax_matrix.driver_11_thru_desc,
      l_tb_tax_matrix.driver_12, l_tb_tax_matrix.driver_12_desc,
      l_tb_tax_matrix.driver_12_thru, l_tb_tax_matrix.driver_12_thru_desc,
      l_tb_tax_matrix.driver_13, l_tb_tax_matrix.driver_13_desc,
      l_tb_tax_matrix.driver_13_thru, l_tb_tax_matrix.driver_13_thru_desc,
      l_tb_tax_matrix.driver_14, l_tb_tax_matrix.driver_14_desc,
      l_tb_tax_matrix.driver_14_thru, l_tb_tax_matrix.driver_14_thru_desc,
      l_tb_tax_matrix.driver_15, l_tb_tax_matrix.driver_15_desc,
      l_tb_tax_matrix.driver_15_thru, l_tb_tax_matrix.driver_15_thru_desc,
      l_tb_tax_matrix.driver_16, l_tb_tax_matrix.driver_16_desc,
      l_tb_tax_matrix.driver_16_thru, l_tb_tax_matrix.driver_16_thru_desc,
      l_tb_tax_matrix.driver_17, l_tb_tax_matrix.driver_17_desc,
      l_tb_tax_matrix.driver_17_thru, l_tb_tax_matrix.driver_17_thru_desc,
      l_tb_tax_matrix.driver_18, l_tb_tax_matrix.driver_18_desc,
      l_tb_tax_matrix.driver_18_thru, l_tb_tax_matrix.driver_18_thru_desc,
      l_tb_tax_matrix.driver_19, l_tb_tax_matrix.driver_19_desc,
      l_tb_tax_matrix.driver_19_thru, l_tb_tax_matrix.driver_19_thru_desc,
      l_tb_tax_matrix.driver_20, l_tb_tax_matrix.driver_20_desc,
      l_tb_tax_matrix.driver_20_thru, l_tb_tax_matrix.driver_20_thru_desc,
      l_tb_tax_matrix.driver_21, l_tb_tax_matrix.driver_21_desc,
      l_tb_tax_matrix.driver_21_thru, l_tb_tax_matrix.driver_21_thru_desc,
      l_tb_tax_matrix.driver_22, l_tb_tax_matrix.driver_22_desc,
      l_tb_tax_matrix.driver_22_thru, l_tb_tax_matrix.driver_22_thru_desc,
      l_tb_tax_matrix.driver_23, l_tb_tax_matrix.driver_23_desc,
      l_tb_tax_matrix.driver_23_thru, l_tb_tax_matrix.driver_23_thru_desc,
      l_tb_tax_matrix.driver_24, l_tb_tax_matrix.driver_24_desc,
      l_tb_tax_matrix.driver_24_thru, l_tb_tax_matrix.driver_24_thru_desc,
      l_tb_tax_matrix.driver_25, l_tb_tax_matrix.driver_25_desc,
      l_tb_tax_matrix.driver_25_thru, l_tb_tax_matrix.driver_25_thru_desc,
      l_tb_tax_matrix.driver_26, l_tb_tax_matrix.driver_26_desc,
      l_tb_tax_matrix.driver_26_thru, l_tb_tax_matrix.driver_26_thru_desc,
      l_tb_tax_matrix.driver_27, l_tb_tax_matrix.driver_27_desc,
      l_tb_tax_matrix.driver_27_thru, l_tb_tax_matrix.driver_27_thru_desc,
      l_tb_tax_matrix.driver_28, l_tb_tax_matrix.driver_28_desc,
      l_tb_tax_matrix.driver_28_thru, l_tb_tax_matrix.driver_28_thru_desc,
      l_tb_tax_matrix.driver_29, l_tb_tax_matrix.driver_29_desc,
      l_tb_tax_matrix.driver_29_thru, l_tb_tax_matrix.driver_29_thru_desc,
      l_tb_tax_matrix.driver_30, l_tb_tax_matrix.driver_30_desc,
      l_tb_tax_matrix.driver_30_thru, l_tb_tax_matrix.driver_30_thru_desc,
      l_tb_tax_matrix.binary_weight, l_tb_tax_matrix.significant_digits,
      l_tb_tax_matrix.effective_date, l_tb_tax_matrix.expiration_date,
      l_tb_tax_matrix.relation_sign, l_tb_tax_matrix.relation_amount,
      l_tb_tax_matrix.then_hold_code_flag, l_tb_tax_matrix.then_taxcode_detail_id, l_tb_tax_matrix.then_cch_taxcat_code,  l_tb_tax_matrix.then_cch_group_code,  l_tb_tax_matrix.then_cch_item_code,
      l_tb_tax_matrix.else_hold_code_flag, l_tb_tax_matrix.else_taxcode_detail_id, l_tb_tax_matrix.else_cch_taxcat_code,  l_tb_tax_matrix.else_cch_group_code,  l_tb_tax_matrix.else_cch_item_code,
      l_tb_tax_matrix.comments,
      l_tb_tax_matrix.last_used_timestamp, l_tb_tax_matrix.update_user_id, l_tb_tax_matrix.update_timestamp );

   -- ****** Read and Process Transactions ****** -----------------------------------------
    OPEN transaction_detail_cursor
    FOR vc_transaction_detail_stmt;
    LOOP
      FETCH transaction_detail_cursor
       INTO transaction_detail;
       EXIT WHEN transaction_detail_cursor%NOTFOUND;

      -- Populate and/or Clear Transaction Detail working fields
      transaction_detail.transaction_ind := NULL;
      transaction_detail.suspend_ind := NULL;
      transaction_detail.tax_matrix_id := l_tb_tax_matrix.tax_matrix_id;
      transaction_detail.update_user_id := USER;
      transaction_detail.update_timestamp := v_sysdate;

      -- Get "Then" TaxCode Elements
      BEGIN
         SELECT taxcode_state_code,
                taxcode_type_code,
                taxcode_code,
                jurisdiction_id,
                measure_type_code,
                taxtype_code
           INTO v_then_taxcode_state_code,
                v_then_taxcode_type_code,
                v_then_taxcode_code,
                v_then_jurisdiction_id,
                v_then_measure_type_code,
                v_then_taxtype_code
           FROM TB_TAXCODE_DETAIL
          WHERE taxcode_detail_id = l_tb_tax_matrix.then_taxcode_detail_id;
      EXCEPTION
         WHEN OTHERS THEN
            v_then_taxcode_state_code := NULL;
            v_then_taxcode_type_code := NULL;
            v_then_taxcode_code := NULL;
            v_then_jurisdiction_id := NULL;
            v_then_measure_type_code := NULL;
            v_then_taxtype_code := NULL;
      END;

      -- Get "Else" TaxCode Elements
      BEGIN
         SELECT taxcode_state_code,
                taxcode_type_code,
                taxcode_code,
                jurisdiction_id,
                measure_type_code,
                taxtype_code
           INTO v_else_taxcode_state_code,
                v_else_taxcode_type_code,
                v_else_taxcode_code,
                v_else_jurisdiction_id,
                v_else_measure_type_code,
                v_else_taxtype_code
           FROM TB_TAXCODE_DETAIL
          WHERE taxcode_detail_id = l_tb_tax_matrix.else_taxcode_detail_id;
      EXCEPTION
         WHEN OTHERS THEN
            v_else_taxcode_state_code := NULL;
            v_else_taxcode_type_code := NULL;
            v_else_taxcode_code := NULL;
            v_else_jurisdiction_id := NULL;
            v_else_measure_type_code := NULL;
            v_else_taxtype_code := NULL;
      END;

      -- Continue if Elements Found
      IF ( v_then_taxcode_state_code IS NOT NULL ) AND
         (( l_tb_tax_matrix.else_taxcode_detail_id IS NULL OR l_tb_tax_matrix.else_taxcode_detail_id = 0 ) OR
          ( l_tb_tax_matrix.else_taxcode_detail_id IS NOT NULL AND l_tb_tax_matrix.else_taxcode_detail_id <> 0 AND v_else_taxcode_state_code IS NOT NULL )) THEN
         -- Determine "Then" or "Else" Results
         v_relation_amount := l_tb_tax_matrix.relation_amount;
         IF l_tb_tax_matrix.relation_sign = 'na' THEN
            v_relation_amount := 0;
            v_less_hold_code_flag := l_tb_tax_matrix.then_hold_code_flag;
            v_less_taxcode_detail_id := l_tb_tax_matrix.then_taxcode_detail_id;
            v_less_taxcode_state_code := v_then_taxcode_state_code;
            v_less_taxcode_type_code := v_then_taxcode_type_code;
            v_less_taxcode_code := v_then_taxcode_code;
            v_less_jurisdiction_id := v_then_jurisdiction_id;
            v_less_measure_type_code := v_then_measure_type_code;
            v_less_taxtype_code := v_then_taxtype_code;
            v_less_cch_taxcat_code := l_tb_tax_matrix.then_cch_taxcat_code;
            v_less_cch_group_code := l_tb_tax_matrix.then_cch_group_code;
            -- v_less_cch_item_code := l_tb_tax_matrix.then_cch_item_code;
            v_equal_hold_code_flag := l_tb_tax_matrix.then_hold_code_flag;
            v_equal_taxcode_detail_id := l_tb_tax_matrix.then_taxcode_detail_id;
            v_equal_taxcode_state_code := v_then_taxcode_state_code;
            v_equal_taxcode_type_code := v_then_taxcode_type_code;
            v_equal_taxcode_code := v_then_taxcode_code;
            v_equal_jurisdiction_id := v_then_jurisdiction_id;
            v_equal_measure_type_code := v_then_measure_type_code;
            v_equal_taxtype_code := v_then_taxtype_code;
            v_equal_cch_taxcat_code := l_tb_tax_matrix.then_cch_taxcat_code;
            v_equal_cch_group_code := l_tb_tax_matrix.then_cch_group_code;
            -- v_equal_cch_item_code := l_tb_tax_matrix.then_cch_item_code;
            v_greater_hold_code_flag := l_tb_tax_matrix.then_hold_code_flag;
            v_greater_taxcode_detail_id := l_tb_tax_matrix.then_taxcode_detail_id;
            v_greater_taxcode_state_code := v_then_taxcode_state_code;
            v_greater_taxcode_type_code := v_then_taxcode_type_code;
            v_greater_taxcode_code := v_then_taxcode_code;
            v_greater_jurisdiction_id := v_then_jurisdiction_id;
            v_greater_measure_type_code := v_then_measure_type_code;
            v_greater_taxtype_code := v_then_taxtype_code;
            v_greater_cch_taxcat_code := l_tb_tax_matrix.then_cch_taxcat_code;
            v_greater_cch_group_code := l_tb_tax_matrix.then_cch_group_code;
            -- v_greater_cch_item_code := l_tb_tax_matrix.then_cch_item_code;
         ELSIF l_tb_tax_matrix.relation_sign = '=' THEN
            v_less_hold_code_flag := l_tb_tax_matrix.else_hold_code_flag;
            v_less_taxcode_detail_id := l_tb_tax_matrix.else_taxcode_detail_id;
            v_less_taxcode_state_code := v_else_taxcode_state_code;
            v_less_taxcode_type_code := v_else_taxcode_type_code;
            v_less_taxcode_code := v_else_taxcode_code;
            v_less_jurisdiction_id := v_else_jurisdiction_id;
            v_less_measure_type_code := v_else_measure_type_code;
            v_less_taxtype_code := v_else_taxtype_code;
            v_less_cch_taxcat_code := l_tb_tax_matrix.else_cch_taxcat_code;
            v_less_cch_group_code := l_tb_tax_matrix.else_cch_group_code;
            -- v_less_cch_item_code := l_tb_tax_matrix.else_cch_item_code;
            v_equal_hold_code_flag := l_tb_tax_matrix.then_hold_code_flag;
            v_equal_taxcode_detail_id := l_tb_tax_matrix.then_taxcode_detail_id;
            v_equal_taxcode_state_code := v_then_taxcode_state_code;
            v_equal_taxcode_type_code := v_then_taxcode_type_code;
            v_equal_taxcode_code := v_then_taxcode_code;
            v_equal_jurisdiction_id := v_then_jurisdiction_id;
            v_equal_measure_type_code := v_then_measure_type_code;
            v_equal_taxtype_code := v_then_taxtype_code;
            v_equal_cch_taxcat_code := l_tb_tax_matrix.then_cch_taxcat_code;
            v_equal_cch_group_code := l_tb_tax_matrix.then_cch_group_code;
            -- v_equal_cch_item_code := l_tb_tax_matrix.then_cch_item_code;
            v_greater_hold_code_flag := l_tb_tax_matrix.else_hold_code_flag;
            v_greater_taxcode_detail_id := l_tb_tax_matrix.else_taxcode_detail_id;
            v_greater_taxcode_state_code := v_else_taxcode_state_code;
            v_greater_taxcode_type_code := v_else_taxcode_type_code;
            v_greater_taxcode_code := v_else_taxcode_code;
            v_greater_jurisdiction_id := v_else_jurisdiction_id;
            v_greater_measure_type_code := v_else_measure_type_code;
            v_greater_taxtype_code := v_else_taxtype_code;
            v_greater_cch_taxcat_code := l_tb_tax_matrix.else_cch_taxcat_code;
            v_greater_cch_group_code := l_tb_tax_matrix.else_cch_group_code;
            -- v_greater_cch_item_code := l_tb_tax_matrix.else_cch_item_code;
         ELSIF l_tb_tax_matrix.relation_sign = '<' THEN
            v_less_hold_code_flag := l_tb_tax_matrix.then_hold_code_flag;
            v_less_taxcode_detail_id := l_tb_tax_matrix.then_taxcode_detail_id;
            v_less_taxcode_state_code := v_then_taxcode_state_code;
            v_less_taxcode_type_code := v_then_taxcode_type_code;
            v_less_taxcode_code := v_then_taxcode_code;
            v_less_jurisdiction_id := v_then_jurisdiction_id;
            v_less_measure_type_code := v_then_measure_type_code;
            v_less_taxtype_code := v_then_taxtype_code;
            v_less_cch_taxcat_code := l_tb_tax_matrix.then_cch_taxcat_code;
            v_less_cch_group_code := l_tb_tax_matrix.then_cch_group_code;
            -- v_less_cch_item_code := l_tb_tax_matrix.then_cch_item_code;
            v_equal_hold_code_flag := l_tb_tax_matrix.else_hold_code_flag;
            v_equal_taxcode_detail_id := l_tb_tax_matrix.else_taxcode_detail_id;
            v_equal_taxcode_state_code := v_else_taxcode_state_code;
            v_equal_taxcode_type_code := v_else_taxcode_type_code;
            v_equal_taxcode_code := v_else_taxcode_code;
            v_equal_jurisdiction_id := v_else_jurisdiction_id;
            v_equal_measure_type_code := v_else_measure_type_code;
            v_equal_taxtype_code := v_else_taxtype_code;
            v_equal_cch_taxcat_code := l_tb_tax_matrix.else_cch_taxcat_code;
            v_equal_cch_group_code := l_tb_tax_matrix.else_cch_group_code;
            -- v_equal_cch_item_code := l_tb_tax_matrix.else_cch_item_code;
            v_greater_hold_code_flag := l_tb_tax_matrix.else_hold_code_flag;
            v_greater_taxcode_detail_id := l_tb_tax_matrix.else_taxcode_detail_id;
            v_greater_taxcode_state_code := v_else_taxcode_state_code;
            v_greater_taxcode_type_code := v_else_taxcode_type_code;
            v_greater_taxcode_code := v_else_taxcode_code;
            v_greater_jurisdiction_id := v_else_jurisdiction_id;
            v_greater_measure_type_code := v_else_measure_type_code;
            v_greater_taxtype_code := v_else_taxtype_code;
            v_greater_cch_taxcat_code := l_tb_tax_matrix.else_cch_taxcat_code;
            v_greater_cch_group_code := l_tb_tax_matrix.else_cch_group_code;
            -- v_greater_cch_item_code := l_tb_tax_matrix.else_cch_item_code;
         ELSIF l_tb_tax_matrix.relation_sign = '<=' THEN
            v_less_hold_code_flag := l_tb_tax_matrix.then_hold_code_flag;
            v_less_taxcode_detail_id := l_tb_tax_matrix.then_taxcode_detail_id;
            v_less_taxcode_state_code := v_then_taxcode_state_code;
            v_less_taxcode_type_code := v_then_taxcode_type_code;
            v_less_taxcode_code := v_then_taxcode_code;
            v_less_jurisdiction_id := v_then_jurisdiction_id;
            v_less_measure_type_code := v_then_measure_type_code;
            v_less_taxtype_code := v_then_taxtype_code;
            v_less_cch_taxcat_code := l_tb_tax_matrix.then_cch_taxcat_code;
            v_less_cch_group_code := l_tb_tax_matrix.then_cch_group_code;
            -- v_less_cch_item_code := l_tb_tax_matrix.then_cch_item_code;
            v_equal_hold_code_flag := l_tb_tax_matrix.then_hold_code_flag;
            v_equal_taxcode_detail_id := l_tb_tax_matrix.then_taxcode_detail_id;
            v_equal_taxcode_state_code := v_then_taxcode_state_code;
            v_equal_taxcode_type_code := v_then_taxcode_type_code;
            v_equal_taxcode_code := v_then_taxcode_code;
            v_equal_jurisdiction_id := v_then_jurisdiction_id;
            v_equal_measure_type_code := v_then_measure_type_code;
            v_equal_taxtype_code := v_then_taxtype_code;
            v_equal_cch_taxcat_code := l_tb_tax_matrix.then_cch_taxcat_code;
            v_equal_cch_group_code := l_tb_tax_matrix.then_cch_group_code;
            -- v_equal_cch_item_code := l_tb_tax_matrix.then_cch_item_code;
            v_greater_hold_code_flag := l_tb_tax_matrix.else_hold_code_flag;
            v_greater_taxcode_detail_id := l_tb_tax_matrix.else_taxcode_detail_id;
            v_greater_taxcode_state_code := v_else_taxcode_state_code;
            v_greater_taxcode_type_code := v_else_taxcode_type_code;
            v_greater_taxcode_code := v_else_taxcode_code;
            v_greater_jurisdiction_id := v_else_jurisdiction_id;
            v_greater_measure_type_code := v_else_measure_type_code;
            v_greater_taxtype_code := v_else_taxtype_code;
            v_greater_cch_taxcat_code := l_tb_tax_matrix.else_cch_taxcat_code;
            v_greater_cch_group_code := l_tb_tax_matrix.else_cch_group_code;
            -- v_greater_cch_item_code := l_tb_tax_matrix.else_cch_item_code;
         ELSIF l_tb_tax_matrix.relation_sign = '>' THEN
            v_less_hold_code_flag := l_tb_tax_matrix.else_hold_code_flag;
            v_less_taxcode_detail_id := l_tb_tax_matrix.else_taxcode_detail_id;
            v_less_taxcode_state_code := v_else_taxcode_state_code;
            v_less_taxcode_type_code := v_else_taxcode_type_code;
            v_less_taxcode_code := v_else_taxcode_code;
            v_less_jurisdiction_id := v_else_jurisdiction_id;
            v_less_measure_type_code := v_else_measure_type_code;
            v_less_taxtype_code := v_else_taxtype_code;
            v_less_cch_taxcat_code := l_tb_tax_matrix.else_cch_taxcat_code;
            v_less_cch_group_code := l_tb_tax_matrix.else_cch_group_code;
            -- v_less_cch_item_code := l_tb_tax_matrix.else_cch_item_code;
            v_equal_hold_code_flag := l_tb_tax_matrix.else_hold_code_flag;
            v_equal_taxcode_detail_id := l_tb_tax_matrix.else_taxcode_detail_id;
            v_equal_taxcode_state_code := v_else_taxcode_state_code;
            v_equal_taxcode_type_code := v_else_taxcode_type_code;
            v_equal_taxcode_code := v_else_taxcode_code;
            v_equal_jurisdiction_id := v_else_jurisdiction_id;
            v_equal_measure_type_code := v_else_measure_type_code;
            v_equal_taxtype_code := v_else_taxtype_code;
            v_equal_cch_taxcat_code := l_tb_tax_matrix.else_cch_taxcat_code;
            v_equal_cch_group_code := l_tb_tax_matrix.else_cch_group_code;
            -- v_equal_cch_item_code := l_tb_tax_matrix.else_cch_item_code;
            v_greater_hold_code_flag := l_tb_tax_matrix.then_hold_code_flag;
            v_greater_taxcode_detail_id := l_tb_tax_matrix.then_taxcode_detail_id;
            v_greater_taxcode_state_code := v_then_taxcode_state_code;
            v_greater_taxcode_type_code := v_then_taxcode_type_code;
            v_greater_taxcode_code := v_then_taxcode_code;
            v_greater_jurisdiction_id := v_then_jurisdiction_id;
            v_greater_measure_type_code := v_then_measure_type_code;
            v_greater_taxtype_code := v_then_taxtype_code;
            v_greater_cch_taxcat_code := l_tb_tax_matrix.then_cch_taxcat_code;
            v_greater_cch_group_code := l_tb_tax_matrix.then_cch_group_code;
            -- v_greater_cch_item_code := l_tb_tax_matrix.then_cch_item_code;
         ELSIF l_tb_tax_matrix.relation_sign = '>=' THEN
            v_less_hold_code_flag := l_tb_tax_matrix.else_hold_code_flag;
            v_less_taxcode_detail_id := l_tb_tax_matrix.else_taxcode_detail_id;
            v_less_taxcode_state_code := v_else_taxcode_state_code;
            v_less_taxcode_type_code := v_else_taxcode_type_code;
            v_less_taxcode_code := v_else_taxcode_code;
            v_less_jurisdiction_id := v_else_jurisdiction_id;
            v_less_measure_type_code := v_else_measure_type_code;
            v_less_taxtype_code := v_else_taxtype_code;
            v_less_cch_taxcat_code := l_tb_tax_matrix.else_cch_taxcat_code;
            v_less_cch_group_code := l_tb_tax_matrix.else_cch_group_code;
            -- v_less_cch_item_code := l_tb_tax_matrix.else_cch_item_code;
            v_equal_hold_code_flag := l_tb_tax_matrix.then_hold_code_flag;
            v_equal_taxcode_detail_id := l_tb_tax_matrix.then_taxcode_detail_id;
            v_equal_taxcode_state_code := v_then_taxcode_state_code;
            v_equal_taxcode_type_code := v_then_taxcode_type_code;
            v_equal_taxcode_code := v_then_taxcode_code;
            v_equal_jurisdiction_id := v_then_jurisdiction_id;
            v_equal_measure_type_code := v_then_measure_type_code;
            v_equal_taxtype_code := v_then_taxtype_code;
            v_equal_cch_taxcat_code := l_tb_tax_matrix.then_cch_taxcat_code;
            v_equal_cch_group_code := l_tb_tax_matrix.then_cch_group_code;
            -- v_equal_cch_item_code := l_tb_tax_matrix.then_cch_item_code;
            v_greater_hold_code_flag := l_tb_tax_matrix.then_hold_code_flag;
            v_greater_taxcode_detail_id := l_tb_tax_matrix.then_taxcode_detail_id;
            v_greater_taxcode_state_code := v_then_taxcode_state_code;
            v_greater_taxcode_type_code := v_then_taxcode_type_code;
            v_greater_taxcode_code := v_then_taxcode_code;
            v_greater_jurisdiction_id := v_then_jurisdiction_id;
            v_greater_measure_type_code := v_then_measure_type_code;
            v_greater_taxtype_code := v_then_taxtype_code;
            v_greater_cch_taxcat_code := l_tb_tax_matrix.then_cch_taxcat_code;
            v_greater_cch_group_code := l_tb_tax_matrix.then_cch_group_code;
            -- v_greater_cch_item_code := l_tb_tax_matrix.then_cch_item_code;
         ELSIF l_tb_tax_matrix.relation_sign = '<>' THEN
            v_less_hold_code_flag := l_tb_tax_matrix.then_hold_code_flag;
            v_less_taxcode_detail_id := l_tb_tax_matrix.then_taxcode_detail_id;
            v_less_taxcode_state_code := v_then_taxcode_state_code;
            v_less_taxcode_type_code := v_then_taxcode_type_code;
            v_less_taxcode_code := v_then_taxcode_code;
            v_less_jurisdiction_id := v_then_jurisdiction_id;
            v_less_measure_type_code := v_then_measure_type_code;
            v_less_taxtype_code := v_then_taxtype_code;
            v_less_cch_taxcat_code := l_tb_tax_matrix.then_cch_taxcat_code;
            v_less_cch_group_code := l_tb_tax_matrix.then_cch_group_code;
            -- v_less_cch_item_code := l_tb_tax_matrix.then_cch_item_code;
            v_equal_hold_code_flag := l_tb_tax_matrix.else_hold_code_flag;
            v_equal_taxcode_detail_id := l_tb_tax_matrix.else_taxcode_detail_id;
            v_equal_taxcode_state_code := v_else_taxcode_state_code;
            v_equal_taxcode_type_code := v_else_taxcode_type_code;
            v_equal_taxcode_code := v_else_taxcode_code;
            v_equal_jurisdiction_id := v_else_jurisdiction_id;
            v_equal_measure_type_code := v_else_measure_type_code;
            v_equal_taxtype_code := v_else_taxtype_code;
            v_equal_cch_taxcat_code := l_tb_tax_matrix.else_cch_taxcat_code;
            v_equal_cch_group_code := l_tb_tax_matrix.else_cch_group_code;
            -- v_equal_cch_item_code := l_tb_tax_matrix.else_cch_item_code;
            v_greater_hold_code_flag := l_tb_tax_matrix.then_hold_code_flag;
            v_greater_taxcode_detail_id := l_tb_tax_matrix.then_taxcode_detail_id;
            v_greater_taxcode_state_code := v_then_taxcode_state_code;
            v_greater_taxcode_type_code := v_then_taxcode_type_code;
            v_greater_taxcode_code := v_then_taxcode_code;
            v_greater_jurisdiction_id := v_then_jurisdiction_id;
            v_greater_measure_type_code := v_then_measure_type_code;
            v_greater_taxtype_code := v_then_taxtype_code;
            v_greater_cch_taxcat_code := l_tb_tax_matrix.then_cch_taxcat_code;
            v_greater_cch_group_code := l_tb_tax_matrix.then_cch_group_code;
            -- v_greater_cch_item_code := l_tb_tax_matrix.then_cch_item_code;
         END IF;

         SELECT DECODE(SIGN(transaction_detail.gl_line_itm_dist_amt - v_relation_amount), -1, v_less_hold_code_flag, 0, v_equal_hold_code_flag, 1, v_greater_hold_code_flag),
                DECODE(SIGN(transaction_detail.gl_line_itm_dist_amt - v_relation_amount), -1, v_less_taxcode_detail_id, 0, v_equal_taxcode_detail_id, 1, v_greater_taxcode_detail_id),
                DECODE(SIGN(transaction_detail.gl_line_itm_dist_amt - v_relation_amount), -1, v_less_taxcode_state_code, 0, v_equal_taxcode_state_code, 1, v_greater_taxcode_state_code),
                DECODE(SIGN(transaction_detail.gl_line_itm_dist_amt - v_relation_amount), -1, v_less_taxcode_type_code, 0, v_equal_taxcode_type_code, 1, v_greater_taxcode_type_code),
                DECODE(SIGN(transaction_detail.gl_line_itm_dist_amt - v_relation_amount), -1, v_less_taxcode_code, 0, v_equal_taxcode_code, 1, v_greater_taxcode_code),
                DECODE(SIGN(transaction_detail.gl_line_itm_dist_amt - v_relation_amount), -1, v_less_jurisdiction_id, 0, v_equal_jurisdiction_id, 1, v_greater_jurisdiction_id),
                DECODE(SIGN(transaction_detail.gl_line_itm_dist_amt - v_relation_amount), -1, v_less_measure_type_code, 0, v_equal_measure_type_code, 1, v_greater_measure_type_code),
                DECODE(SIGN(transaction_detail.gl_line_itm_dist_amt - v_relation_amount), -1, v_less_taxtype_code, 0, v_equal_taxtype_code, 1, v_greater_taxtype_code),
                DECODE(SIGN(transaction_detail.gl_line_itm_dist_amt - v_relation_amount), -1, v_less_cch_taxcat_code, 0, v_equal_cch_taxcat_code, 1, v_greater_cch_taxcat_code),
                DECODE(SIGN(transaction_detail.gl_line_itm_dist_amt - v_relation_amount), -1, v_less_cch_group_code, 0, v_equal_cch_group_code, 1, v_greater_cch_group_code)
           INTO v_hold_code_flag, transaction_detail.taxcode_detail_id, transaction_detail.taxcode_state_code, transaction_detail.taxcode_type_code, transaction_detail.taxcode_code, v_override_jurisdiction_id, transaction_detail.measure_type_code, v_taxtype_code, transaction_detail.cch_taxcat_code, transaction_detail.cch_group_code
           FROM DUAL;

         -- ****** DETERMINE TYPE of TAXCODE ****** --
         -- The Tax Code is Taxable
         IF transaction_detail.taxcode_type_code = 'T' THEN
            -- If Override jurisdiction id is not blank or 0 then use it instead of location matrix
            IF v_override_jurisdiction_id IS NOT NULL AND v_override_jurisdiction_id <> 0 THEN
               transaction_detail.jurisdiction_id := v_override_jurisdiction_id;
               transaction_detail.manual_jurisdiction_ind := 'AO';
               location_matrix.override_taxtype_code := '*NO';
               location_matrix.state_flag := '1';                             -- MBF01
               location_matrix.county_flag := '1';                            -- MBF01
               location_matrix.county_local_flag := '1';                      -- MBF01
               location_matrix.city_flag := '1';                              -- MBF01
               location_matrix.city_local_flag := '1';                        -- MBF01
            ELSE
               -- Search for Location Matrix if not already found AND not already manually applied!!
               IF transaction_detail.location_matrix_id IS NULL AND transaction_detail.jurisdiction_id IS NULL THEN
                  v_transaction_detail_id := transaction_detail.transaction_detail_id;
                  -- Search Location Matrix for matches
                  BEGIN
                      OPEN location_matrix_cursor
                       FOR vc_location_matrix_stmt
                     USING v_transaction_detail_id;
                     FETCH location_matrix_cursor
                      INTO location_matrix;
                     -- Rows found?
                     IF location_matrix_cursor%FOUND THEN
                        vn_fetch_rows := location_matrix_cursor%ROWCOUNT;
                     ELSE
                        vn_fetch_rows := 0;
                     END IF;
                     CLOSE location_matrix_cursor;
                     IF SQLCODE != 0 THEN
                        RAISE e_badread;
                     END IF;
                  EXCEPTION
                     WHEN e_badread THEN
                        vn_fetch_rows := 0;
                  END;
                  -- Location Matrix Line Found
                  IF vn_fetch_rows > 0 THEN
                     transaction_detail.location_matrix_id := location_matrix.location_matrix_id;
                     transaction_detail.jurisdiction_id := location_matrix.jurisdiction_id;
                  ELSE
                     transaction_detail.transaction_ind := 'S';
                     transaction_detail.suspend_ind := 'L';
                  END IF;
               -- Get Location Matrix if it exists AND not already manually applied!!
               ELSIF transaction_detail.location_matrix_id IS NOT NULL AND (v_override_jurisdiction_id IS NULL OR v_override_jurisdiction_id = 0) THEN
                  BEGIN
                     SELECT override_taxtype_code,
                            state_flag, county_flag, county_local_flag, city_flag, city_local_flag
                       INTO location_matrix.override_taxtype_code,
                            location_matrix.state_flag,
                            location_matrix.county_flag,
                            location_matrix.county_local_flag,
                            location_matrix.city_flag,
                            location_matrix.city_local_flag
                       FROM TB_LOCATION_MATRIX
                      WHERE TB_LOCATION_MATRIX.location_matrix_id = transaction_detail.location_matrix_id;
                  EXCEPTION
                     WHEN NO_DATA_FOUND THEN
                        location_matrix.override_taxtype_code := '*NO';
                        location_matrix.state_flag := '1';
                        location_matrix.county_flag := '1';
                        location_matrix.county_local_flag := '1';
                        location_matrix.city_flag := '1';
                        location_matrix.city_local_flag := '1';
                  END;
               ELSE
                  location_matrix.override_taxtype_code := '*NO';
                  location_matrix.state_flag := '1';
                  location_matrix.county_flag := '1';
                  location_matrix.county_local_flag := '1';
                  location_matrix.city_flag := '1';
                  location_matrix.city_local_flag := '1';
               END IF;
            END IF;

            -- Continue if not Suspended
            IF transaction_detail.transaction_ind IS NULL OR transaction_detail.transaction_ind <> 'S' THEN
               -- Get Jurisdiction TaxRates and calculate Tax Amounts
               Sp_Getusetax(transaction_detail.jurisdiction_id,
                            transaction_detail.gl_date,
                            transaction_detail.measure_type_code,
                            transaction_detail.gl_line_itm_dist_amt,
                            v_taxtype_code,
                            location_matrix.override_taxtype_code,
                            location_matrix.state_flag,
                            location_matrix.county_flag,
                            location_matrix.county_local_flag,
                            location_matrix.city_flag,
                            location_matrix.city_local_flag,
                            transaction_detail.invoice_freight_amt,
                            transaction_detail.invoice_discount_amt,
                            transaction_detail.transaction_state_code,
-- future? --                     transaction_detail.import_definition_code,
                            'importdefn',
                            transaction_detail.taxcode_code,
                            transaction_detail.jurisdiction_taxrate_id,
                            transaction_detail.state_use_amount,
                            transaction_detail.state_use_tier2_amount,
                            transaction_detail.state_use_tier3_amount,
                            transaction_detail.county_use_amount,
                            transaction_detail.county_local_use_amount,
                            transaction_detail.city_use_amount,
                            transaction_detail.city_local_use_amount,
                            transaction_detail.state_use_rate,
                            transaction_detail.state_use_tier2_rate,
                            transaction_detail.state_use_tier3_rate,
                            transaction_detail.state_split_amount,
                            transaction_detail.state_tier2_min_amount,
                            transaction_detail.state_tier2_max_amount,
                            transaction_detail.state_maxtax_amount,
                            transaction_detail.county_use_rate,
                            transaction_detail.county_local_use_rate,
                            transaction_detail.county_split_amount,
                            transaction_detail.county_maxtax_amount,
                            transaction_detail.county_single_flag,
                            transaction_detail.county_default_flag,
                            transaction_detail.city_use_rate,
                            transaction_detail.city_local_use_rate,
                            transaction_detail.city_split_amount,
                            transaction_detail.city_split_use_rate,
                            transaction_detail.city_single_flag,
                            transaction_detail.city_default_flag,
                            transaction_detail.combined_use_rate,
                            transaction_detail.tb_calc_tax_amt,
-- future? --                     transaction_detail.taxable_amt,
-- Targa? --                transaction_detail.user_number_10,
                            vn_taxable_amt,
                            v_taxtype_used );

               -- Suspend if tax rate id is null
               IF transaction_detail.jurisdiction_taxrate_id IS NULL OR transaction_detail.jurisdiction_taxrate_id = 0 THEN
                  transaction_detail.transaction_ind := 'S';
                  transaction_detail.suspend_ind := 'R';
               ELSE
                  -- Check for Taxable Amount -- 3351
                  IF vn_taxable_amt <> transaction_detail.gl_line_itm_dist_amt THEN
                     transaction_detail.gl_extract_amt := transaction_detail.gl_line_itm_dist_amt;
                     transaction_detail.gl_line_itm_dist_amt := vn_taxable_amt;
                  END IF;
               END IF;
            END IF;

         -- The Tax Code is Exempt
         ELSIF transaction_detail.taxcode_type_code = 'E' THEN
            transaction_detail.transaction_ind := 'P';

         -- The Tax Code is a Question
         ELSIF transaction_detail.taxcode_type_code = 'Q' THEN
           transaction_detail.transaction_ind := 'Q';

         -- The Tax Code is Unrecognized - Suspend
         ELSE
            transaction_detail.transaction_ind := 'S';
            transaction_detail.suspend_ind := '?';
         END IF;

         -- Continue if not Suspended
         IF transaction_detail.transaction_ind IS NULL OR transaction_detail.transaction_ind <> 'S' THEN
            -- Check for matrix "H"old
            IF ( v_hold_code_flag = '1' ) AND
               ( transaction_detail.taxcode_type_code = 'T' OR transaction_detail.taxcode_type_code = 'E') THEN
                  transaction_detail.transaction_ind := 'H';
            ELSE
                transaction_detail.transaction_ind := 'P';
            END IF;
         END IF;

      ELSE
         -- Elements NOT Found
         transaction_detail.transaction_ind := 'S';
         transaction_detail.suspend_ind := '?';
      END IF;

      -- Update transaction detail row
      BEGIN
         -- 3411 - add taxable amount and tax type used.
         UPDATE TB_TRANSACTION_DETAIL
            SET gl_line_itm_dist_amt = transaction_detail.gl_line_itm_dist_amt,
                tb_calc_tax_amt = transaction_detail.tb_calc_tax_amt,
                state_use_amount = transaction_detail.state_use_amount,
                state_use_tier2_amount = transaction_detail.state_use_tier2_amount,
                state_use_tier3_amount = transaction_detail.state_use_tier3_amount,
                county_use_amount = transaction_detail.county_use_amount,
                county_local_use_amount = transaction_detail.county_local_use_amount,
                city_use_amount = transaction_detail.city_use_amount,
                city_local_use_amount = transaction_detail.city_local_use_amount,
                transaction_state_code = transaction_detail.transaction_state_code,
                transaction_ind = transaction_detail.transaction_ind,
                suspend_ind = transaction_detail.suspend_ind,
                taxcode_detail_id = transaction_detail.taxcode_detail_id,
                taxcode_state_code = transaction_detail.taxcode_state_code,
                taxcode_type_code = transaction_detail.taxcode_type_code,
                taxcode_code = transaction_detail.taxcode_code,
                cch_taxcat_code = transaction_detail.cch_taxcat_code,
                cch_group_code = transaction_detail.cch_group_code,
                tax_matrix_id = transaction_detail.tax_matrix_id,
                location_matrix_id = transaction_detail.location_matrix_id,
                jurisdiction_id = transaction_detail.jurisdiction_id,
                jurisdiction_taxrate_id = transaction_detail.jurisdiction_taxrate_id,
                manual_jurisdiction_ind = transaction_detail.manual_jurisdiction_ind,
                measure_type_code = transaction_detail.measure_type_code,
                state_use_rate = transaction_detail.state_use_rate,
                state_use_tier2_rate = transaction_detail.state_use_tier2_rate,
                state_use_tier3_rate = transaction_detail.state_use_tier3_rate,
                state_split_amount = transaction_detail.state_split_amount,
                state_tier2_min_amount = transaction_detail.state_tier2_min_amount,
                state_tier2_max_amount = transaction_detail.state_tier2_max_amount,
                state_maxtax_amount = transaction_detail.state_maxtax_amount,
                county_use_rate = transaction_detail.county_use_rate,
                county_local_use_rate = transaction_detail.county_local_use_rate,
                county_split_amount = transaction_detail.county_split_amount,
                county_maxtax_amount = transaction_detail.county_maxtax_amount,
                county_single_flag = transaction_detail.county_single_flag,
                county_default_flag = transaction_detail.county_default_flag,
                city_use_rate = transaction_detail.city_use_rate,
                city_local_use_rate = transaction_detail.city_local_use_rate,
                city_split_amount = transaction_detail.city_split_amount,
                city_split_use_rate = transaction_detail.city_split_use_rate,
                city_single_flag = transaction_detail.city_single_flag,
                city_default_flag = transaction_detail.city_default_flag,
                combined_use_rate = transaction_detail.combined_use_rate,
                gl_extract_amt = transaction_detail.gl_extract_amt,
                update_user_id = transaction_detail.update_user_id,
                update_timestamp = transaction_detail.update_timestamp
          WHERE transaction_detail_id = transaction_detail.transaction_detail_id;
         -- Error Checking
--         IF SQLCODE != 0 THEN
--            RAISE e_badupdate;
--         END IF;
--      EXCEPTION
--         WHEN e_badupdate THEN
--            NULL;
      END;
   END LOOP;
   CLOSE transaction_detail_cursor;

END IF;

--EXCEPTION
--  WHEN OTHERS THEN
--vc_test1 := substr(vc_transaction_detail_stmt,1,4000);
--vc_test2 := substr(vc_transaction_detail_stmt,4001,4000);
--insert into tb_debug(row_joe, row_bob) values(vc_test1, vc_test2);
--      NULL;
END;
/

CREATE OR REPLACE PROCEDURE Sp_Tb_Transaction_Detail_A_U
		(
		 p_transaction_detail_id IN TB_TRANSACTION_DETAIL_EXCP.transaction_detail_id%TYPE,
		 p_old_taxcode_detail_id IN TB_TRANSACTION_DETAIL_EXCP.old_taxcode_detail_id%TYPE,
		 p_old_taxcode_state_code IN TB_TRANSACTION_DETAIL_EXCP.old_taxcode_state_code%TYPE,
		 p_old_taxcode_type_code IN TB_TRANSACTION_DETAIL_EXCP.old_taxcode_type_code%TYPE,
		 p_old_taxcode_code IN TB_TRANSACTION_DETAIL_EXCP.old_taxcode_code%TYPE,
		 p_old_jurisdiction_id IN TB_TRANSACTION_DETAIL_EXCP.old_jurisdiction_id%TYPE,
		 p_old_jurisdiction_taxrate_id IN TB_TRANSACTION_DETAIL_EXCP.old_jurisdiction_taxrate_id%TYPE,
		 p_old_cch_taxcat_code IN TB_TRANSACTION_DETAIL_EXCP.old_cch_taxcat_code%TYPE,
		 p_old_cch_group_code IN TB_TRANSACTION_DETAIL_EXCP.old_cch_group_code%TYPE,
		 p_old_cch_item_code IN TB_TRANSACTION_DETAIL_EXCP.old_cch_item_code%TYPE,
		 p_new_taxcode_detail_id IN TB_TRANSACTION_DETAIL_EXCP.new_taxcode_detail_id%TYPE,
		 p_new_taxcode_state_code IN TB_TRANSACTION_DETAIL_EXCP.new_taxcode_state_code%TYPE,
		 p_new_taxcode_type_code IN TB_TRANSACTION_DETAIL_EXCP.new_taxcode_type_code%TYPE,
		 p_new_taxcode_code IN TB_TRANSACTION_DETAIL_EXCP.new_taxcode_code%TYPE,
		 p_new_jurisdiction_id IN TB_TRANSACTION_DETAIL_EXCP.new_jurisdiction_id%TYPE,
		 p_new_jurisdiction_taxrate_id IN TB_TRANSACTION_DETAIL_EXCP.new_jurisdiction_taxrate_id%TYPE,
		 p_new_cch_taxcat_code IN TB_TRANSACTION_DETAIL_EXCP.new_cch_taxcat_code%TYPE,
		 p_new_cch_group_code IN TB_TRANSACTION_DETAIL_EXCP.new_cch_group_code%TYPE,
		 p_new_cch_item_code IN TB_TRANSACTION_DETAIL_EXCP.new_cch_item_code%TYPE
		)

	AS
	BEGIN
	   IF (( p_old_taxcode_detail_id IS NOT NULL ) AND p_old_taxcode_detail_id <> p_new_taxcode_detail_id ) OR
	      (( p_old_jurisdiction_id IS NOT NULL ) AND p_old_jurisdiction_id <> p_new_jurisdiction_id ) OR
	      (( p_old_jurisdiction_taxrate_id IS NOT NULL ) AND p_old_jurisdiction_taxrate_id <> p_new_jurisdiction_taxrate_id ) THEN

	      INSERT INTO TB_TRANSACTION_DETAIL_EXCP (
		 transaction_detail_excp_id,
		 transaction_detail_id,
		 old_taxcode_detail_id,
		 old_taxcode_state_code,
		 old_taxcode_type_code,
		 old_taxcode_code,
		 old_jurisdiction_id,
		 old_jurisdiction_taxrate_id,
		 old_cch_taxcat_code,
		 old_cch_group_code,
		 old_cch_item_code,
		 new_taxcode_detail_id,
		 new_taxcode_state_code,
		 new_taxcode_type_code,
		 new_taxcode_code,
		 new_jurisdiction_id,
		 new_jurisdiction_taxrate_id,
		 new_cch_taxcat_code,
		 new_cch_group_code,
		 new_cch_item_code,
		 update_user_id,
		 update_timestamp )
	      VALUES (
		 SQ_TB_TRANS_DETAIL_EXCP_ID.NEXTVAL,
		 p_transaction_detail_id,
		 p_old_taxcode_detail_id,
		 p_old_taxcode_state_code,
		 p_old_taxcode_type_code,
		 p_old_taxcode_code,
		 p_old_jurisdiction_id,
		 p_old_jurisdiction_taxrate_id,
		 p_old_cch_taxcat_code,
		 p_old_cch_group_code,
		 p_old_cch_item_code,
		 p_new_taxcode_detail_id,
		 p_new_taxcode_state_code,
		 p_new_taxcode_type_code,
		 p_new_taxcode_code,
		 p_new_jurisdiction_id,
		 p_new_jurisdiction_taxrate_id,
		 p_new_cch_taxcat_code,
		 p_new_cch_group_code,
		 p_new_cch_item_code,
		 USER,
		 SYS_EXTRACT_UTC(SYSTIMESTAMP) );
	   END IF;
	END;
/

CREATE OR REPLACE PROCEDURE Sp_Tb_Transaction_Detail_B_U(
	p_old_tb_transaction_detail IN     TB_TRANSACTION_DETAIL%ROWTYPE,
	p_new_tb_transaction_detail IN OUT TB_TRANSACTION_DETAIL%ROWTYPE
) AS
/* ************************************************************************************************/
/* Object Type/Name: Trigger/Before Update - tb_transaction_detail_b_u                            */
/* Author:           Michael B. Fuller                                                            */
/* Date:             a long time ago                                                              */
/* Description:      A transaction has been changed                                               */
/* Arguments:        None                                                                         */
/* Returns:          None                                                                         */
/* ************************************************************************************************/
/* Maintenance History:                                                                           */
/* Scan  Author       Date     Release    Description                                  Ticket #   */
/* ----- ---------- ---------- ---------- -------------------------------------------- ---------- */
/*       M. Fuller  06/02/2006 3.3.5.0    Fix processing an allocated line - Temporary 871        */
/*       M. Fuller  01/04/2007 3.3.5.1    Fix processing an allocated line - Permenant 871        */
/* MBF01 M. Fuller  02/09/2007 3.3.5.1    Correct allocated lines with no locn matrix id          */
/* MBF02 M. Fuller  08/13/2007 3.4.1.1    Add 5 Calc Tax Flags                         883        */
/* MBF03 M. Fuller  03/06/2008 3.4.2.1    Correct If/Then/Else's for "AO"              913        */
/* JFF   J. Franco  12/17/2008 x.x.x.x    Convert trigger to stored procedure          xxx        */
/* ************************************************************************************************/

-- Define Cursor Type
   TYPE cursor_type IS REF CURSOR;

-- Location Matrix Selection SQL      // MBF02
   vc_location_matrix_select       VARCHAR2(1000) :=
      'SELECT tb_location_matrix.location_matrix_id, ' ||
             'tb_location_matrix.jurisdiction_id, ' ||
             'tb_location_matrix.override_taxtype_code, ' ||
             'tb_location_matrix.state_flag,' ||
             'tb_location_matrix.county_flag,' ||
             'tb_location_matrix.county_local_flag,' ||
             'tb_location_matrix.city_flag,' ||
             'tb_location_matrix.city_local_flag,' ||
             'tb_jurisdiction.state, ' ||
             'tb_jurisdiction.county, ' ||
             'tb_jurisdiction.city, ' ||
             'tb_jurisdiction.zip, ' ||
             'tb_jurisdiction.in_out ' ||
        'FROM tb_location_matrix, ' ||
             'tb_jurisdiction, ' ||
             'tb_tmp_transaction_detail ' ||
      'WHERE ( tb_tmp_transaction_detail.transaction_detail_id = :v_transaction_detail_id ) AND ' ||
            '( tb_location_matrix.default_flag = ''0'' AND tb_location_matrix.binary_weight > 0 ) AND ' ||
            '( tb_location_matrix.effective_date <= tb_tmp_transaction_detail.gl_date ) AND ( tb_location_matrix.expiration_date >= tb_tmp_transaction_detail.gl_date ) AND ' ||
            '( tb_location_matrix.jurisdiction_id = tb_jurisdiction.jurisdiction_id ) ';
   vc_location_matrix_where        VARCHAR2(3000) := '';
   vc_location_matrix_orderby      VARCHAR2(1000) :=
      'ORDER BY tb_location_matrix.binary_weight DESC, tb_location_matrix.significant_digits DESC, tb_location_matrix.effective_date DESC';
   vc_location_matrix_stmt         VARCHAR2 (5000);
   location_matrix_cursor          cursor_type;

-- Location Matrix Record TYPE
   /*TYPE location_matrix_record is RECORD (
      location_matrix_id           tb_location_matrix.location_matrix_id%TYPE,
      jurisdiction_id              tb_location_matrix.jurisdiction_id%TYPE,
      override_taxtype_code        tb_location_matrix.override_taxtype_code%TYPE,
      state_flag                   tb_location_matrix.state_flag%TYPE,
      county_flag                  tb_location_matrix.county_flag%TYPE,
      county_local_flag            tb_location_matrix.county_local_flag%TYPE,
      city_flag                    tb_location_matrix.city_flag%TYPE,
      city_local_flag              tb_location_matrix.city_local_flag%TYPE,
      state                        tb_jurisdiction.state%TYPE,
      county                       tb_jurisdiction.county%TYPE,
      city                         tb_jurisdiction.city%TYPE,
      zip                          tb_jurisdiction.zip%TYPE,
      in_out                       tb_jurisdiction.in_out%TYPE );*/
   location_matrix                 Matrix_Record_Pkg.location_matrix_record;

-- Tax Matrix Selection SQL
   vc_tax_matrix_select            VARCHAR2(2000) :=
      'SELECT tb_tax_matrix.tax_matrix_id, tb_tax_matrix.relation_sign, tb_tax_matrix.relation_amount, ' ||
             'tb_tax_matrix.then_hold_code_flag, tb_tax_matrix.else_hold_code_flag, ' ||
             'tb_tax_matrix.then_taxcode_detail_id, tb_tax_matrix.else_taxcode_detail_id, ' ||
             'tb_tax_matrix.then_cch_taxcat_code, tb_tax_matrix.then_cch_group_code, tb_tax_matrix.then_cch_item_code, ' ||
             'tb_tax_matrix.else_cch_taxcat_code, tb_tax_matrix.else_cch_group_code, tb_tax_matrix.else_cch_item_code, ' ||
             'thentaxcddtl.taxcode_state_code, thentaxcddtl.taxcode_type_code, thentaxcddtl.taxcode_code, thentaxcddtl.jurisdiction_id, thentaxcddtl.measure_type_code, thentaxcddtl.taxtype_code, ' ||
             'elsetaxcddtl.taxcode_state_code, elsetaxcddtl.taxcode_type_code, elsetaxcddtl.taxcode_code, elsetaxcddtl.jurisdiction_id, elsetaxcddtl.measure_type_code, elsetaxcddtl.taxtype_code ' ||
       'FROM tb_tax_matrix, ' ||
            'tb_taxcode_detail thentaxcddtl, ' ||
            'tb_taxcode_detail elsetaxcddtl, ' ||
            'tb_tmp_transaction_detail ' ||
      'WHERE ( tb_tmp_transaction_detail.transaction_detail_id = :v_transaction_detail_id ) AND ' ||
            '( tb_tax_matrix.default_flag = ''0'' AND tb_tax_matrix.binary_weight > 0 ) AND ' ||
            '(( tb_tax_matrix.matrix_state_code = ''*ALL'' ) OR ( tb_tax_matrix.matrix_state_code = :v_transaction_state_code )) AND ' ||
            '( tb_tax_matrix.effective_date <= tb_tmp_transaction_detail.gl_date ) AND ( tb_tax_matrix.expiration_date >= tb_tmp_transaction_detail.gl_date ) AND ' ||
            '( tb_tax_matrix.then_taxcode_detail_id = thentaxcddtl.taxcode_detail_id (+)) AND ' ||
            '( tb_tax_matrix.else_taxcode_detail_id = elsetaxcddtl.taxcode_detail_id (+)) ';
   vc_tax_matrix_where             VARCHAR2(6000) := '';
   vc_tax_matrix_orderby           VARCHAR2(1000) :=
      'ORDER BY tb_tax_matrix.binary_weight DESC, tb_tax_matrix.significant_digits DESC, tb_tax_matrix.effective_date DESC';
   vc_tax_matrix_stmt              VARCHAR2 (9000);
   tax_matrix_cursor               cursor_type;

-- Tax Matrix Record TYPE
   /*TYPE tax_matrix_record is RECORD (
      tax_matrix_id                tb_tax_matrix.tax_matrix_id%TYPE,
      relation_sign                tb_tax_matrix.relation_sign%TYPE,
      relation_amount              tb_tax_matrix.relation_amount%TYPE,
      then_hold_code_flag          tb_tax_matrix.then_hold_code_flag%TYPE,
      else_hold_code_flag          tb_tax_matrix.else_hold_code_flag%TYPE,
      then_taxcode_detail_id       tb_bcp_transactions.taxcode_detail_id%TYPE,
      else_taxcode_detail_id       tb_bcp_transactions.taxcode_detail_id%TYPE,
      then_cch_taxcat_code         tb_tax_matrix.then_cch_taxcat_code%TYPE,
      then_cch_group_code          tb_tax_matrix.then_cch_group_code%TYPE,
      then_cch_item_code           tb_tax_matrix.then_cch_item_code%TYPE,
      else_cch_taxcat_code         tb_tax_matrix.else_cch_taxcat_code%TYPE,
      else_cch_group_code          tb_tax_matrix.else_cch_group_code%TYPE,
      else_cch_item_code           tb_tax_matrix.else_cch_item_code%TYPE,
      then_taxcode_state_code      tb_bcp_transactions.taxcode_state_code%TYPE,
      then_taxcode_type_code       tb_bcp_transactions.taxcode_type_code%TYPE,
      then_taxcode_code            tb_bcp_transactions.taxcode_code%TYPE,
      then_jurisdiction_id         tb_bcp_transactions.jurisdiction_id%TYPE,
      then_measure_type_code       tb_bcp_transactions.measure_type_code%TYPE,
      then_taxtype_code            tb_taxcode.taxtype_code%TYPE,
      else_taxcode_state_code      tb_bcp_transactions.taxcode_state_code%TYPE,
      else_taxcode_type_code       tb_bcp_transactions.taxcode_type_code%TYPE,
      else_taxcode_code            tb_bcp_transactions.taxcode_code%TYPE,
      else_jurisdiction_id         tb_bcp_transactions.jurisdiction_id%TYPE,
      else_measure_type_code       tb_bcp_transactions.measure_type_code%TYPE,
      else_taxtype_code            tb_taxcode.taxtype_code%TYPE );*/
   tax_matrix                      Matrix_Record_Pkg.tax_matrix_record;

-- Table defined variables
   v_hold_code_flag                TB_TAX_MATRIX.then_hold_code_flag%TYPE            := NULL;
   v_less_hold_code_flag           TB_TAX_MATRIX.then_hold_code_flag%TYPE            := NULL;
   v_less_taxcode_detail_id        TB_TAX_MATRIX.then_taxcode_detail_id%TYPE         := NULL;
   v_less_cch_taxcat_code          TB_TAX_MATRIX.then_cch_taxcat_code%TYPE           := NULL;
   v_less_cch_group_code           TB_TAX_MATRIX.then_cch_group_code%TYPE            := NULL;
   v_equal_hold_code_flag          TB_TAX_MATRIX.then_hold_code_flag%TYPE            := NULL;
   v_equal_taxcode_detail_id       TB_TAX_MATRIX.then_taxcode_detail_id%TYPE         := NULL;
   v_equal_cch_taxcat_code         TB_TAX_MATRIX.then_cch_taxcat_code%TYPE           := NULL;
   v_equal_cch_group_code          TB_TAX_MATRIX.then_cch_group_code%TYPE            := NULL;
   v_greater_hold_code_flag        TB_TAX_MATRIX.then_hold_code_flag%TYPE            := NULL;
   v_greater_taxcode_detail_id     TB_TAX_MATRIX.then_taxcode_detail_id%TYPE         := NULL;
   v_greater_cch_taxcat_code       TB_TAX_MATRIX.then_cch_taxcat_code%TYPE           := NULL;
   v_greater_cch_group_code        TB_TAX_MATRIX.then_cch_group_code%TYPE            := NULL;
   v_jurisdiction_id               TB_JURISDICTION.jurisdiction_id%TYPE              := NULL;
   v_less_taxcode_state_code       TB_TAXCODE_DETAIL.taxcode_state_code%TYPE         := NULL;
   v_less_taxcode_type_code        TB_TAXCODE_DETAIL.taxcode_type_code%TYPE          := NULL;
   v_less_taxcode_code             TB_TAXCODE_DETAIL.taxcode_code%TYPE               := NULL;
   v_less_jurisdiction_id          TB_TAXCODE_DETAIL.jurisdiction_id%TYPE            := NULL;
   v_less_measure_type_code        TB_TAXCODE_DETAIL.measure_type_code%TYPE          := NULL;
   v_less_taxtype_code             TB_TAXCODE_DETAIL.taxtype_code%TYPE               := NULL;
   v_equal_taxcode_state_code      TB_TAXCODE_DETAIL.taxcode_state_code%TYPE         := NULL;
   v_equal_taxcode_type_code       TB_TAXCODE_DETAIL.taxcode_type_code%TYPE          := NULL;
   v_equal_taxcode_code            TB_TAXCODE_DETAIL.taxcode_code%TYPE               := NULL;
   v_equal_jurisdiction_id         TB_TAXCODE_DETAIL.jurisdiction_id%TYPE            := NULL;
   v_equal_measure_type_code       TB_TAXCODE_DETAIL.measure_type_code%TYPE          := NULL;
   v_equal_taxtype_code            TB_TAXCODE_DETAIL.taxtype_code%TYPE               := NULL;
   v_greater_taxcode_state_code    TB_TAXCODE_DETAIL.taxcode_state_code%TYPE         := NULL;
   v_greater_taxcode_type_code     TB_TAXCODE_DETAIL.taxcode_type_code%TYPE          := NULL;
   v_greater_taxcode_code          TB_TAXCODE_DETAIL.taxcode_code%TYPE               := NULL;
   v_greater_jurisdiction_id       TB_TAXCODE_DETAIL.jurisdiction_id%TYPE            := NULL;
   v_greater_measure_type_code     TB_TAXCODE_DETAIL.measure_type_code%TYPE          := NULL;
   v_greater_taxtype_code          TB_TAXCODE_DETAIL.taxtype_code%TYPE               := NULL;
   v_transaction_detail_id         TB_TRANSACTION_DETAIL.transaction_detail_id%TYPE  := NULL;
   v_transaction_state_code        TB_TRANSACTION_DETAIL.transaction_state_code%TYPE := NULL;
   v_override_jurisdiction_id      TB_TRANSACTION_DETAIL.jurisdiction_id%TYPE        := NULL;
   v_sysdate                       TB_TRANSACTION_DETAIL.load_timestamp%TYPE         := SYS_EXTRACT_UTC(SYSTIMESTAMP);
   v_sysdate_plus                  TB_TRANSACTION_DETAIL.load_timestamp%TYPE         := v_sysdate + (1/86400);
   v_taxtype_code                  TB_TAXCODE.taxtype_code%TYPE                      := NULL;
   v_taxtype_used                  TB_TAXCODE.taxtype_code%TYPE                      := NULL;

-- Program defined variables
   vn_fetch_rows                   NUMBER                                            := 0;
   vc_state_driver_flag            CHAR(1)                                           := '0';
-- temporary
   vn_taxable_amt                  NUMBER                                            := 0;

-- Define Location Driver Names Cursor (tb_driver_names)
   /*CURSOR location_driver_cursor
   IS
        SELECT driver.trans_dtl_column_name, driver.matrix_column_name, driver.null_driver_flag, driver.wildcard_flag,
               datadefcol.desc_column_name, datadefcol.data_type
          FROM tb_driver_names driver,
               (SELECT *
                  FROM tb_data_def_column
                 WHERE table_name = 'TB_TRANSACTION_DETAIL') datadefcol
         WHERE driver.driver_names_code = 'L' AND
               driver.trans_dtl_column_name = datadefcol.column_name
      ORDER BY driver.driver_id;
      location_driver              location_driver_cursor%ROWTYPE;*/

-- Define Tax Driver Names Cursor (tb_driver_names)
   /*CURSOR tax_driver_cursor
   IS
        SELECT driver.trans_dtl_column_name, driver.matrix_column_name, driver.null_driver_flag, driver.wildcard_flag, driver.range_flag, to_number_flag,
               datadefcol.desc_column_name, datadefcol.data_type
          FROM tb_driver_names driver,
               (SELECT *
                  FROM tb_data_def_column
                 WHERE table_name = 'TB_TRANSACTION_DETAIL') datadefcol
         WHERE driver.driver_names_code = 'T' AND
               driver.trans_dtl_column_name = datadefcol.column_name
      ORDER BY driver.driver_id;
      tax_driver                   tax_driver_cursor%ROWTYPE;*/

-- Define Exceptions
   e_badread                       EXCEPTION;

-- ****** Program starts ****** --------------------------------------------------------------------
BEGIN
   p_new_tb_transaction_detail.update_user_id   := USER;
   p_new_tb_transaction_detail.update_timestamp := v_sysdate;

   -- If was processed and now processed OR
   --    was tax suspended and now processed and now manual taxcode indicator not null OR
   --    was locn suspended and now processed and now manual jurisdiction indicator not null THEN
--   IF ( UPPER(p_old_tb_transaction_detail.transaction_ind) = 'P' AND UPPER(p_new_tb_transaction_detail.transaction_ind) = 'P' ) OR
--      ( UPPER(p_old_tb_transaction_detail.transaction_ind) = 'S' AND UPPER(p_old_tb_transaction_detail.suspend_ind) = 'T' AND p_new_tb_transaction_detail.manual_taxcode_ind IS NOT NULL ) OR
--      ( UPPER(p_old_tb_transaction_detail.transaction_ind) = 'S' AND UPPER(p_old_tb_transaction_detail.suspend_ind) = 'L' AND p_new_tb_transaction_detail.manual_jurisdiction_ind IS NOT NULL ) THEN
   IF (( NOT ( UPPER(p_old_tb_transaction_detail.transaction_ind) = 'P' AND UPPER(p_new_tb_transaction_detail.transaction_ind) = 'S' )) AND
      ((( p_old_tb_transaction_detail.taxcode_detail_id IS NOT NULL AND p_old_tb_transaction_detail.taxcode_detail_id <> p_new_tb_transaction_detail.taxcode_detail_id ) OR
        ( p_old_tb_transaction_detail.taxcode_detail_id IS NOT NULL AND p_new_tb_transaction_detail.taxcode_detail_id IS NULL ) OR
        ( p_old_tb_transaction_detail.taxcode_detail_id IS NULL AND p_new_tb_transaction_detail.taxcode_detail_id IS NOT NULL AND p_new_tb_transaction_detail.manual_taxcode_ind IS NOT NULL )) OR
       (( p_old_tb_transaction_detail.jurisdiction_id IS NOT NULL AND p_old_tb_transaction_detail.jurisdiction_id <> p_new_tb_transaction_detail.jurisdiction_id ) OR
        ( p_old_tb_transaction_detail.jurisdiction_id IS NOT NULL AND p_new_tb_transaction_detail.jurisdiction_id IS NULL ) OR
        ( p_old_tb_transaction_detail.jurisdiction_id IS NULL AND p_new_tb_transaction_detail.jurisdiction_id IS NOT NULL AND p_new_tb_transaction_detail.manual_jurisdiction_ind IS NOT NULL AND p_new_tb_transaction_detail.manual_jurisdiction_ind <> 'AO' )) OR
       (( (p_old_tb_transaction_detail.jurisdiction_taxrate_id IS NOT NULL AND p_old_tb_transaction_detail.jurisdiction_taxrate_id <> 0) AND p_old_tb_transaction_detail.jurisdiction_taxrate_id <> p_new_tb_transaction_detail.jurisdiction_taxrate_id ) OR
        ( (p_old_tb_transaction_detail.jurisdiction_taxrate_id IS NOT NULL AND p_old_tb_transaction_detail.jurisdiction_taxrate_id <> 0) AND (p_new_tb_transaction_detail.jurisdiction_taxrate_id IS NULL OR p_new_tb_transaction_detail.jurisdiction_taxrate_id = 0) ))))
   AND NOT (( UPPER(p_old_tb_transaction_detail.transaction_ind) = 'S' AND UPPER(p_new_tb_transaction_detail.transaction_ind) = 'S' ) AND
       (( p_old_tb_transaction_detail.taxcode_detail_id IS NOT NULL AND p_new_tb_transaction_detail.taxcode_detail_id IS NULL ) OR
        ( p_old_tb_transaction_detail.jurisdiction_id IS NOT NULL AND p_new_tb_transaction_detail.jurisdiction_id IS NULL ))) THEN
      -- Initialize Transaction Indicator and Suspended Indicator
      p_new_tb_transaction_detail.transaction_ind := NULL;
      p_new_tb_transaction_detail.suspend_ind := NULL;
      -- Insert New Tax Matrix columns into temporary table -------------------------------------------
      -- 3411 - add taxable amount and tax type used.
      INSERT INTO TB_TMP_TRANSACTION_DETAIL (
         transaction_detail_id,
         source_transaction_id,
         process_batch_no,
         gl_extract_batch_no,
         archive_batch_no,
         allocation_matrix_id,
         allocation_subtrans_id,
         entered_date,
         transaction_status,
         gl_date,
         gl_company_nbr,
         gl_company_name,
         gl_division_nbr,
         gl_division_name,
         gl_cc_nbr_dept_id,
         gl_cc_nbr_dept_name,
         gl_local_acct_nbr,
         gl_local_acct_name,
         gl_local_sub_acct_nbr,
         gl_local_sub_acct_name,
         gl_full_acct_nbr,
         gl_full_acct_name,
         gl_line_itm_dist_amt,
         orig_gl_line_itm_dist_amt,
         vendor_nbr,
         vendor_name,
         vendor_address_line_1,
         vendor_address_line_2,
         vendor_address_line_3,
         vendor_address_line_4,
         vendor_address_city,
         vendor_address_county,
         vendor_address_state,
         vendor_address_zip,
         vendor_address_country,
         vendor_type,
         vendor_type_name,
         invoice_nbr,
         invoice_desc,
         invoice_date,
         invoice_freight_amt,
         invoice_discount_amt,
         invoice_tax_amt,
         invoice_total_amt,
         invoice_tax_flg,
         invoice_line_nbr,
         invoice_line_name,
         invoice_line_type,
         invoice_line_type_name,
         invoice_line_amt,
         invoice_line_tax,
         afe_project_nbr,
         afe_project_name,
         afe_category_nbr,
         afe_category_name,
         afe_sub_cat_nbr,
         afe_sub_cat_name,
         afe_use,
         afe_contract_type,
         afe_contract_structure,
         afe_property_cat,
         inventory_nbr,
         inventory_name,
         inventory_class,
         inventory_class_name,
         po_nbr,
         po_name,
         po_date,
         po_line_nbr,
         po_line_name,
         po_line_type,
         po_line_type_name,
         ship_to_location,
         ship_to_location_name,
         ship_to_address_line_1,
         ship_to_address_line_2,
         ship_to_address_line_3,
         ship_to_address_line_4,
         ship_to_address_city,
         ship_to_address_county,
         ship_to_address_state,
         ship_to_address_zip,
         ship_to_address_country,
         wo_nbr,
         wo_name,
         wo_date,
         wo_type,
         wo_type_desc,
         wo_class,
         wo_class_desc,
         wo_entity,
         wo_entity_desc,
         wo_line_nbr,
         wo_line_name,
         wo_line_type,
         wo_line_type_desc,
         wo_shut_down_cd,
         wo_shut_down_cd_desc,
         voucher_id,
         voucher_name,
         voucher_date,
         voucher_line_nbr,
         voucher_line_desc,
         check_nbr,
         check_no,
         check_date,
         check_amt,
         check_desc,
         user_text_01,
         user_text_02,
         user_text_03,
         user_text_04,
         user_text_05,
         user_text_06,
         user_text_07,
         user_text_08,
         user_text_09,
         user_text_10,
         user_text_11,
         user_text_12,
         user_text_13,
         user_text_14,
         user_text_15,
         user_text_16,
         user_text_17,
         user_text_18,
         user_text_19,
         user_text_20,
         user_text_21,
         user_text_22,
         user_text_23,
         user_text_24,
         user_text_25,
         user_text_26,
         user_text_27,
         user_text_28,
         user_text_29,
         user_text_30,
         user_number_01,
         user_number_02,
         user_number_03,
         user_number_04,
         user_number_05,
         user_number_06,
         user_number_07,
         user_number_08,
         user_number_09,
         user_number_10,
         user_date_01,
         user_date_02,
         user_date_03,
         user_date_04,
         user_date_05,
         user_date_06,
         user_date_07,
         user_date_08,
         user_date_09,
         user_date_10,
         comments,
         tb_calc_tax_amt,
         state_use_amount,
         state_use_tier2_amount,
         state_use_tier3_amount,
         county_use_amount,
         county_local_use_amount,
         city_use_amount,
         city_local_use_amount,
         transaction_state_code,
         transaction_ind,
         suspend_ind,
         taxcode_detail_id,
         taxcode_state_code,
         taxcode_type_code,
         taxcode_code,
         cch_taxcat_code,
         cch_group_code,
         cch_item_code,
         manual_taxcode_ind,
         tax_matrix_id,
         location_matrix_id,
         jurisdiction_id,
         jurisdiction_taxrate_id,
         manual_jurisdiction_ind,
         measure_type_code,
         state_use_rate,
         state_use_tier2_rate,
         state_use_tier3_rate,
         state_split_amount,
         state_tier2_min_amount,
         state_tier2_max_amount,
         state_maxtax_amount,
         county_use_rate,
         county_local_use_rate,
         county_split_amount,
         county_maxtax_amount,
         county_single_flag,
         county_default_flag,
         city_use_rate,
         city_local_use_rate,
         city_split_amount,
         city_split_use_rate,
         city_single_flag,
         city_default_flag,
         combined_use_rate,
         load_timestamp,
         gl_extract_updater,
         gl_extract_timestamp,
         gl_extract_flag,
         gl_log_flag,
         gl_extract_amt,
         audit_flag,
         audit_user_id,
         audit_timestamp,
         modify_user_id,
         modify_timestamp,
         update_user_id,
         update_timestamp )
      VALUES (
         p_new_tb_transaction_detail.transaction_detail_id,
         p_new_tb_transaction_detail.source_transaction_id,
         p_new_tb_transaction_detail.process_batch_no,
         p_new_tb_transaction_detail.gl_extract_batch_no,
         p_new_tb_transaction_detail.archive_batch_no,
         p_new_tb_transaction_detail.allocation_matrix_id,
         p_new_tb_transaction_detail.allocation_subtrans_id,
         p_new_tb_transaction_detail.entered_date,
         p_new_tb_transaction_detail.transaction_status,
         p_new_tb_transaction_detail.gl_date,
         p_new_tb_transaction_detail.gl_company_nbr,
         p_new_tb_transaction_detail.gl_company_name,
         p_new_tb_transaction_detail.gl_division_nbr,
         p_new_tb_transaction_detail.gl_division_name,
         p_new_tb_transaction_detail.gl_cc_nbr_dept_id,
         p_new_tb_transaction_detail.gl_cc_nbr_dept_name,
         p_new_tb_transaction_detail.gl_local_acct_nbr,
         p_new_tb_transaction_detail.gl_local_acct_name,
         p_new_tb_transaction_detail.gl_local_sub_acct_nbr,
         p_new_tb_transaction_detail.gl_local_sub_acct_name,
         p_new_tb_transaction_detail.gl_full_acct_nbr,
         p_new_tb_transaction_detail.gl_full_acct_name,
         p_new_tb_transaction_detail.gl_line_itm_dist_amt,
         p_new_tb_transaction_detail.orig_gl_line_itm_dist_amt,
         p_new_tb_transaction_detail.vendor_nbr,
         p_new_tb_transaction_detail.vendor_name,
         p_new_tb_transaction_detail.vendor_address_line_1,
         p_new_tb_transaction_detail.vendor_address_line_2,
         p_new_tb_transaction_detail.vendor_address_line_3,
         p_new_tb_transaction_detail.vendor_address_line_4,
         p_new_tb_transaction_detail.vendor_address_city,
         p_new_tb_transaction_detail.vendor_address_county,
         p_new_tb_transaction_detail.vendor_address_state,
         p_new_tb_transaction_detail.vendor_address_zip,
         p_new_tb_transaction_detail.vendor_address_country,
         p_new_tb_transaction_detail.vendor_type,
         p_new_tb_transaction_detail.vendor_type_name,
         p_new_tb_transaction_detail.invoice_nbr,
         p_new_tb_transaction_detail.invoice_desc,
         p_new_tb_transaction_detail.invoice_date,
         p_new_tb_transaction_detail.invoice_freight_amt,
         p_new_tb_transaction_detail.invoice_discount_amt,
         p_new_tb_transaction_detail.invoice_tax_amt,
         p_new_tb_transaction_detail.invoice_total_amt,
         p_new_tb_transaction_detail.invoice_tax_flg,
         p_new_tb_transaction_detail.invoice_line_nbr,
         p_new_tb_transaction_detail.invoice_line_name,
         p_new_tb_transaction_detail.invoice_line_type,
         p_new_tb_transaction_detail.invoice_line_type_name,
         p_new_tb_transaction_detail.invoice_line_amt,
         p_new_tb_transaction_detail.invoice_line_tax,
         p_new_tb_transaction_detail.afe_project_nbr,
         p_new_tb_transaction_detail.afe_project_name,
         p_new_tb_transaction_detail.afe_category_nbr,
         p_new_tb_transaction_detail.afe_category_name,
         p_new_tb_transaction_detail.afe_sub_cat_nbr,
         p_new_tb_transaction_detail.afe_sub_cat_name,
         p_new_tb_transaction_detail.afe_use,
         p_new_tb_transaction_detail.afe_contract_type,
         p_new_tb_transaction_detail.afe_contract_structure,
         p_new_tb_transaction_detail.afe_property_cat,
         p_new_tb_transaction_detail.inventory_nbr,
         p_new_tb_transaction_detail.inventory_name,
         p_new_tb_transaction_detail.inventory_class,
         p_new_tb_transaction_detail.inventory_class_name,
         p_new_tb_transaction_detail.po_nbr,
         p_new_tb_transaction_detail.po_name,
         p_new_tb_transaction_detail.po_date,
         p_new_tb_transaction_detail.po_line_nbr,
         p_new_tb_transaction_detail.po_line_name,
         p_new_tb_transaction_detail.po_line_type,
         p_new_tb_transaction_detail.po_line_type_name,
         p_new_tb_transaction_detail.ship_to_location,
         p_new_tb_transaction_detail.ship_to_location_name,
         p_new_tb_transaction_detail.ship_to_address_line_1,
         p_new_tb_transaction_detail.ship_to_address_line_2,
         p_new_tb_transaction_detail.ship_to_address_line_3,
         p_new_tb_transaction_detail.ship_to_address_line_4,
         p_new_tb_transaction_detail.ship_to_address_city,
         p_new_tb_transaction_detail.ship_to_address_county,
         p_new_tb_transaction_detail.ship_to_address_state,
         p_new_tb_transaction_detail.ship_to_address_zip,
         p_new_tb_transaction_detail.ship_to_address_country,
         p_new_tb_transaction_detail.wo_nbr,
         p_new_tb_transaction_detail.wo_name,
         p_new_tb_transaction_detail.wo_date,
         p_new_tb_transaction_detail.wo_type,
         p_new_tb_transaction_detail.wo_type_desc,
         p_new_tb_transaction_detail.wo_class,
         p_new_tb_transaction_detail.wo_class_desc,
         p_new_tb_transaction_detail.wo_entity,
         p_new_tb_transaction_detail.wo_entity_desc,
         p_new_tb_transaction_detail.wo_line_nbr,
         p_new_tb_transaction_detail.wo_line_name,
         p_new_tb_transaction_detail.wo_line_type,
         p_new_tb_transaction_detail.wo_line_type_desc,
         p_new_tb_transaction_detail.wo_shut_down_cd,
         p_new_tb_transaction_detail.wo_shut_down_cd_desc,
         p_new_tb_transaction_detail.voucher_id,
         p_new_tb_transaction_detail.voucher_name,
         p_new_tb_transaction_detail.voucher_date,
         p_new_tb_transaction_detail.voucher_line_nbr,
         p_new_tb_transaction_detail.voucher_line_desc,
         p_new_tb_transaction_detail.check_nbr,
         p_new_tb_transaction_detail.check_no,
         p_new_tb_transaction_detail.check_date,
         p_new_tb_transaction_detail.check_amt,
         p_new_tb_transaction_detail.check_desc,
         p_new_tb_transaction_detail.user_text_01,
         p_new_tb_transaction_detail.user_text_02,
         p_new_tb_transaction_detail.user_text_03,
         p_new_tb_transaction_detail.user_text_04,
         p_new_tb_transaction_detail.user_text_05,
         p_new_tb_transaction_detail.user_text_06,
         p_new_tb_transaction_detail.user_text_07,
         p_new_tb_transaction_detail.user_text_08,
         p_new_tb_transaction_detail.user_text_09,
         p_new_tb_transaction_detail.user_text_10,
         p_new_tb_transaction_detail.user_text_11,
         p_new_tb_transaction_detail.user_text_12,
         p_new_tb_transaction_detail.user_text_13,
         p_new_tb_transaction_detail.user_text_14,
         p_new_tb_transaction_detail.user_text_15,
         p_new_tb_transaction_detail.user_text_16,
         p_new_tb_transaction_detail.user_text_17,
         p_new_tb_transaction_detail.user_text_18,
         p_new_tb_transaction_detail.user_text_19,
         p_new_tb_transaction_detail.user_text_20,
         p_new_tb_transaction_detail.user_text_21,
         p_new_tb_transaction_detail.user_text_22,
         p_new_tb_transaction_detail.user_text_23,
         p_new_tb_transaction_detail.user_text_24,
         p_new_tb_transaction_detail.user_text_25,
         p_new_tb_transaction_detail.user_text_26,
         p_new_tb_transaction_detail.user_text_27,
         p_new_tb_transaction_detail.user_text_28,
         p_new_tb_transaction_detail.user_text_29,
         p_new_tb_transaction_detail.user_text_30,
         p_new_tb_transaction_detail.user_number_01,
         p_new_tb_transaction_detail.user_number_02,
         p_new_tb_transaction_detail.user_number_03,
         p_new_tb_transaction_detail.user_number_04,
         p_new_tb_transaction_detail.user_number_05,
         p_new_tb_transaction_detail.user_number_06,
         p_new_tb_transaction_detail.user_number_07,
         p_new_tb_transaction_detail.user_number_08,
         p_new_tb_transaction_detail.user_number_09,
         p_new_tb_transaction_detail.user_number_10,
         p_new_tb_transaction_detail.user_date_01,
         p_new_tb_transaction_detail.user_date_02,
         p_new_tb_transaction_detail.user_date_03,
         p_new_tb_transaction_detail.user_date_04,
         p_new_tb_transaction_detail.user_date_05,
         p_new_tb_transaction_detail.user_date_06,
         p_new_tb_transaction_detail.user_date_07,
         p_new_tb_transaction_detail.user_date_08,
         p_new_tb_transaction_detail.user_date_09,
         p_new_tb_transaction_detail.user_date_10,
         p_new_tb_transaction_detail.comments,
         p_new_tb_transaction_detail.tb_calc_tax_amt,
         p_new_tb_transaction_detail.state_use_amount,
         p_new_tb_transaction_detail.state_use_tier2_amount,
         p_new_tb_transaction_detail.state_use_tier3_amount,
         p_new_tb_transaction_detail.county_use_amount,
         p_new_tb_transaction_detail.county_local_use_amount,
         p_new_tb_transaction_detail.city_use_amount,
         p_new_tb_transaction_detail.city_local_use_amount,
         p_new_tb_transaction_detail.transaction_state_code,
         p_new_tb_transaction_detail.transaction_ind,
         p_new_tb_transaction_detail.suspend_ind,
         p_new_tb_transaction_detail.taxcode_detail_id,
         p_new_tb_transaction_detail.taxcode_state_code,
         p_new_tb_transaction_detail.taxcode_type_code,
         p_new_tb_transaction_detail.taxcode_code,
         p_new_tb_transaction_detail.cch_taxcat_code,
         p_new_tb_transaction_detail.cch_group_code,
         p_new_tb_transaction_detail.cch_item_code,
         p_new_tb_transaction_detail.manual_taxcode_ind,
         p_new_tb_transaction_detail.tax_matrix_id,
         p_new_tb_transaction_detail.location_matrix_id,
         p_new_tb_transaction_detail.jurisdiction_id,
         p_new_tb_transaction_detail.jurisdiction_taxrate_id,
         p_new_tb_transaction_detail.manual_jurisdiction_ind,
         p_new_tb_transaction_detail.measure_type_code,
         p_new_tb_transaction_detail.state_use_rate,
         p_new_tb_transaction_detail.state_use_tier2_rate,
         p_new_tb_transaction_detail.state_use_tier3_rate,
         p_new_tb_transaction_detail.state_split_amount,
         p_new_tb_transaction_detail.state_tier2_min_amount,
         p_new_tb_transaction_detail.state_tier2_max_amount,
         p_new_tb_transaction_detail.state_maxtax_amount,
         p_new_tb_transaction_detail.county_use_rate,
         p_new_tb_transaction_detail.county_local_use_rate,
         p_new_tb_transaction_detail.county_split_amount,
         p_new_tb_transaction_detail.county_maxtax_amount,
         p_new_tb_transaction_detail.county_single_flag,
         p_new_tb_transaction_detail.county_default_flag,
         p_new_tb_transaction_detail.city_use_rate,
         p_new_tb_transaction_detail.city_local_use_rate,
         p_new_tb_transaction_detail.city_split_amount,
         p_new_tb_transaction_detail.city_split_use_rate,
         p_new_tb_transaction_detail.city_single_flag,
         p_new_tb_transaction_detail.city_default_flag,
         p_new_tb_transaction_detail.combined_use_rate,
         p_new_tb_transaction_detail.load_timestamp,
         p_new_tb_transaction_detail.gl_extract_updater,
         p_new_tb_transaction_detail.gl_extract_timestamp,
         p_new_tb_transaction_detail.gl_extract_flag,
         p_new_tb_transaction_detail.gl_log_flag,
         p_new_tb_transaction_detail.gl_extract_amt,
         p_new_tb_transaction_detail.audit_flag,
         p_new_tb_transaction_detail.audit_user_id,
         p_new_tb_transaction_detail.audit_timestamp,
         p_new_tb_transaction_detail.modify_user_id,
         p_new_tb_transaction_detail.modify_timestamp,
         p_new_tb_transaction_detail.update_user_id,
         p_new_tb_transaction_detail.update_timestamp );

      -- If the new TaxCode Type is "T"axable then zero Tax Amounts
      IF UPPER(p_new_tb_transaction_detail.taxcode_type_code) = 'T' OR UPPER(p_old_tb_transaction_detail.taxcode_type_code) = 'T' THEN
         p_new_tb_transaction_detail.tb_calc_tax_amt := 0;
-- future?? --         p_new_tb_transaction_detail.taxable_amt := 0;
         p_new_tb_transaction_detail.state_use_amount := 0;
         p_new_tb_transaction_detail.state_use_tier2_amount := 0;
         p_new_tb_transaction_detail.state_use_tier3_amount := 0;
         p_new_tb_transaction_detail.county_use_amount := 0;
         p_new_tb_transaction_detail.county_local_use_amount := 0;
         p_new_tb_transaction_detail.city_use_amount := 0;
         p_new_tb_transaction_detail.city_local_use_amount := 0;
         p_new_tb_transaction_detail.state_use_rate := 0;
         p_new_tb_transaction_detail.state_use_tier2_rate := 0;
         p_new_tb_transaction_detail.state_use_tier3_rate := 0;
         p_new_tb_transaction_detail.state_split_amount := 0;
         p_new_tb_transaction_detail.state_tier2_min_amount := 0;
         p_new_tb_transaction_detail.state_tier2_max_amount := 0;
         p_new_tb_transaction_detail.state_maxtax_amount := 0;
         p_new_tb_transaction_detail.county_use_rate := 0;
         p_new_tb_transaction_detail.county_local_use_rate := 0;
         p_new_tb_transaction_detail.county_split_amount := 0;
         p_new_tb_transaction_detail.county_maxtax_amount := 0;
         p_new_tb_transaction_detail.county_single_flag := '0';
         p_new_tb_transaction_detail.county_default_flag := '0';
         p_new_tb_transaction_detail.city_use_rate := 0;
         p_new_tb_transaction_detail.city_local_use_rate := 0;
         p_new_tb_transaction_detail.city_split_amount := 0;
         p_new_tb_transaction_detail.city_split_use_rate := 0;
         p_new_tb_transaction_detail.city_single_flag := '0';
         p_new_tb_transaction_detail.city_default_flag := '0';
         p_new_tb_transaction_detail.combined_use_rate := 0;
         -- Replace original distribution amount if populated -- 3351
         IF p_new_tb_transaction_detail.gl_extract_amt IS NOT NULL AND p_new_tb_transaction_detail.gl_extract_amt <> 0 THEN
            p_new_tb_transaction_detail.gl_line_itm_dist_amt := p_new_tb_transaction_detail.gl_extract_amt;
         END IF;
         p_new_tb_transaction_detail.gl_extract_amt := 0;
      END IF;

      -- If the new TaxCode Detail ID is null then search tax matrix
      IF p_new_tb_transaction_detail.taxcode_detail_id IS NULL THEN
         v_transaction_detail_id := p_new_tb_transaction_detail.transaction_detail_id;
         v_transaction_state_code := p_new_tb_transaction_detail.transaction_state_code;
         -- ***** Create Dynamic WHERE Clause for Matrix SQLs ***** --
         -- Does Transaction Record have a valid State?
         IF v_transaction_state_code IS NULL THEN
            vn_fetch_rows := 0;
         ELSE
            SELECT COUNT(*)
              INTO vn_fetch_rows
              FROM TB_TAXCODE_STATE
             WHERE taxcode_state_code = v_transaction_state_code;
         END IF;
         IF vn_fetch_rows = 0 THEN
            -- Get State from new Jurisdiction
            v_transaction_state_code := NULL;
            BEGIN
               SELECT state
                 INTO v_transaction_state_code
                 FROM TB_JURISDICTION
                WHERE jurisdiction_id = p_new_tb_transaction_detail.jurisdiction_id;
               IF SQLCODE != 0 THEN
                  RAISE e_badread;
               END IF;
            EXCEPTION
               WHEN e_badread THEN
                  v_transaction_state_code := NULL;
               WHEN NO_DATA_FOUND THEN
                  v_transaction_state_code := NULL;
            END;
            -- Jurisdiction Line Found
            IF v_transaction_state_code IS NOT NULL THEN
               IF p_new_tb_transaction_detail.transaction_state_code IS NULL THEN
                  p_new_tb_transaction_detail.auto_transaction_state_code := '*NULL';
               ELSE
                  p_new_tb_transaction_detail.auto_transaction_state_code := p_new_tb_transaction_detail.transaction_state_code;
               END IF;
               p_new_tb_transaction_detail.transaction_state_code := v_transaction_state_code;
            ELSE
               p_new_tb_transaction_detail.transaction_ind := 'S';
               p_new_tb_transaction_detail.suspend_ind := 'L';
            END IF;
         END IF;

         -- Continue if not Suspended
         IF p_new_tb_transaction_detail.transaction_ind IS NULL OR p_new_tb_transaction_detail.transaction_ind <> 'S' THEN

   -- *******************************************************************************************************************************************
   -- NEW BATCH PROCESS CODE --------------------------------------------------------------------------------------------------------------------
   -- ***** Create Dynamic WHERE Clause for Transaction Detail SQL ***** --
   -- Tax Matrix Drivers
   /*OPEN tax_driver_cursor;
   LOOP
      FETCH tax_driver_cursor INTO tax_driver;
      EXIT WHEN tax_driver_cursor%NOTFOUND;
      IF tax_driver.range_flag = '1' THEN
         IF tax_driver.to_number_flag = '1' THEN
            IF tax_driver.null_driver_flag = '1' THEN
               -- Range and ToNumber and NULL
               IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                  UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                  vc_tax_matrix_where := vc_tax_matrix_where || 'AND ( ' ||
                  '( tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU = ''*NA'' AND ' ||
                     '( tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NULL AND ( tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*NULL'' )) OR ' ||
                     '( tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND ( tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR DECODE(isnumber(tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || '),1,to_char(tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ',''00000000000000000000.00000000000000000000''),UPPER(tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ')) = DECODE(isnumber(tb_tax_matrix.' || tax_driver.matrix_column_name || '),1,to_char(tb_tax_matrix.' || tax_driver.matrix_column_name || ',''00000000000000000000.00000000000000000000''),UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') ))) OR ' ||
                  '( tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU <> ''*NA'' AND ' ||
                     '( tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND (DECODE(isnumber(tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || '),1,to_char(tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ',''00000000000000000000.00000000000000000000''),UPPER(tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ')) >= DECODE(isnumber(tb_tax_matrix.' || tax_driver.matrix_column_name || '),1,to_char(tb_tax_matrix.' || tax_driver.matrix_column_name || ',''00000000000000000000.00000000000000000000''),UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') ) AND ' ||
                                                                                            'DECODE(isnumber(tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || '),1,to_char(tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ',''00000000000000000000.00000000000000000000''),UPPER(tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ')) <= DECODE(isnumber(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU ),1,to_char(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU,''00000000000000000000.00000000000000000000''),UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU)) )))) ';
               END IF;
            ELSE
               -- Range and ToNumber and NOT NULL
               IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                  UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                  vc_tax_matrix_where := vc_tax_matrix_where || 'AND ( ' ||
                  '( tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU = ''*NA'' AND (tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR DECODE(isnumber(tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || '),1,to_char(tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ',''00000000000000000000.00000000000000000000''),UPPER(tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ')) = DECODE(isnumber(tb_tax_matrix.' || tax_driver.matrix_column_name || '),1,to_char(tb_tax_matrix.' || tax_driver.matrix_column_name || ',''00000000000000000000.00000000000000000000''),UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ')))) OR ' ||
                  '( tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU <> ''*NA'' AND (DECODE(isnumber(tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || '),1,to_char(tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ',''00000000000000000000.00000000000000000000''),UPPER(tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ')) >= DECODE(isnumber(tb_tax_matrix.' || tax_driver.matrix_column_name || '),1,to_char(tb_tax_matrix.' || tax_driver.matrix_column_name || ',''00000000000000000000.00000000000000000000''),UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ')) AND ' ||
                                                                                        'DECODE(isnumber(tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || '),1,to_char(tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ',''00000000000000000000.00000000000000000000''),UPPER(tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ')) <= DECODE(isnumber(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU ),1,to_char(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU,''00000000000000000000.00000000000000000000''),UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU)) )) ';
               END IF;
            END IF;
         ELSE
            IF tax_driver.null_driver_flag = '1' THEN
               IF tax_driver.wildcard_flag = '1' THEN
                  -- Range and NOT ToNumber and NULL and Wildcard
                  IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                     UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                     vc_tax_matrix_where := vc_tax_matrix_where || 'AND ( ' ||
                     '( tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU = ''*NA'' AND ' ||
                        '( tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NULL AND (tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*NULL'' )) OR ' ||
                        '( tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND (tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR UPPER(tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ') LIKE UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ')))) OR ' ||
                     '( tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU <> ''*NA'' AND (tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND (UPPER(tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ') >= UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') AND UPPER(tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ') <= UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU) ))) ';
                  END IF;
               ELSE
                  -- Range and NOT ToNumber and NULL and NOT Wildcard
                  IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                     UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                     vc_tax_matrix_where := vc_tax_matrix_where || 'AND ( ' ||
                     '( tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU = ''*NA'' AND ' ||
                        '( tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NULL AND (tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*NULL'' )) OR ' ||
                        '( tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND (tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR UPPER(tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ') = UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ')))) OR ' ||
                     '( tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU <> ''*NA'' AND (tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND (UPPER(tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ') >= UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') AND UPPER(tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ') <= UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU) ))) ';
                  END IF;
               END IF;
            ELSE
               IF tax_driver.wildcard_flag = '1' THEN
                  -- Range and NOT ToNumber and NOT NULL and Wildcard
                  IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                     UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                     vc_tax_matrix_where := vc_tax_matrix_where || 'AND ( ' ||
                     '( tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU = ''*NA'' AND ' ||
                        '(tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR UPPER(tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ') LIKE UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '))) OR ' ||
                     '( tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU <> ''*NA'' AND (UPPER(tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ') >= UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') AND UPPER(tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ') <= UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU) )) ';
                  END IF;
               ELSE
                  -- Range and NOT ToNumber and NOT NULL and NOT Wildcard
                  IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                     UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                     vc_tax_matrix_where := vc_tax_matrix_where || 'AND ( ' ||
                     '( tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU = ''*NA'' AND ' ||
                        '(tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR UPPER(tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ') = UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '))) OR ' ||
                     '( tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU <> ''*NA'' AND (UPPER(tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ') >= UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') AND UPPER(tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ') <= UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU) )) ';
                  END IF;
               END IF;
            END IF;
         END IF;
      ELSE
         IF tax_driver.null_driver_flag = '1' THEN
            IF tax_driver.wildcard_flag = '1' THEN
               -- NOT Range and NULL and Wildcard
               IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                  UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                     vc_tax_matrix_where := vc_tax_matrix_where || 'AND ( ' ||
                     '( tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NULL AND (tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*NULL'' )) OR ' ||
                     '( tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND (tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR UPPER(tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ') LIKE UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ')) ) ';
               END IF;
            ELSE
               -- NOT Range and NULL and NOT Wildcard
               IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                  UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                     vc_tax_matrix_where := vc_tax_matrix_where || 'AND ( ' ||
                     '( tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NULL AND (tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*NULL'' )) OR ' ||
                     '( tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND (tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR UPPER(tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ') = UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ')) ) ';
               END IF;
            END IF;
         ELSE
            IF tax_driver.wildcard_flag = '1' THEN
               -- NOT Range and NOT NULL and Wildcard
               IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                  UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                     vc_tax_matrix_where := vc_tax_matrix_where || 'AND ( ' ||
                     '( tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR UPPER(tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ') LIKE UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') ) ';
               END IF;
            ELSE
               -- NOT Range and NOT NULL and NOT Wildcard
               IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                  UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                  IF tax_driver.trans_dtl_column_name = 'TRANSACTION_STATE_CODE' THEN
                     IF vc_state_driver_flag <> '1' THEN
                        vc_state_driver_flag := '1';
                     END IF;
                     vc_tax_matrix_where := vc_tax_matrix_where || 'AND ( ' ||
                        '( tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR UPPER(:v_transaction_state_code) = UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ')) ';
                  ELSE
                     vc_tax_matrix_where := vc_tax_matrix_where || 'AND ( ' ||
                        '( tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR UPPER(tb_tmp_transaction_detail.' || tax_driver.trans_dtl_column_name || ') = UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') ) ';
                  END IF;
               END IF;
            END IF;
         END IF;
      END IF;
      vc_tax_matrix_where := vc_tax_matrix_where || ') ';
   END LOOP;
   CLOSE tax_driver_cursor;*/

   -- New call to sp_gen_tax_driver for MidTier Cleanup
   Sp_Gen_Tax_Driver (
	p_generate_driver_reference	=> 'N',
	p_an_batch_id			=> NULL,
	p_transaction_table_name	=> 'tb_tmp_transaction_detail',
	p_tax_table_name		=> 'tb_tax_matrix',
	p_vc_state_driver_flag		=> vc_state_driver_flag,
	p_vc_tax_matrix_where		=> vc_tax_matrix_where);

   -- NEW BATCH PROCESS CODE --------------------------------------------------------------------------------------------------------------------
   -- *******************************************************************************************************************************************

            -- If no drivers found raise error, else create location matrix sql statement
            IF vc_tax_matrix_where IS NULL THEN
               NULL;
            ELSE
               vc_tax_matrix_stmt := vc_tax_matrix_select || vc_tax_matrix_where || vc_tax_matrix_orderby;
            END IF;

            -- Search Tax Matrix for matches
            BEGIN
               IF vc_state_driver_flag = '1' THEN
                   OPEN tax_matrix_cursor
                    FOR vc_tax_matrix_stmt
                  USING v_transaction_detail_id, v_transaction_state_code, v_transaction_state_code;
                  FETCH tax_matrix_cursor
                   INTO tax_matrix;
               ELSE
                   OPEN tax_matrix_cursor
                    FOR vc_tax_matrix_stmt
                  USING v_transaction_detail_id, v_transaction_state_code;
                  FETCH tax_matrix_cursor
                   INTO tax_matrix;
               END IF;
               -- Rows found?
               IF tax_matrix_cursor%FOUND THEN
                  vn_fetch_rows := tax_matrix_cursor%ROWCOUNT;
               ELSE
                  vn_fetch_rows := 0;
               END IF;
               CLOSE tax_matrix_cursor;
               IF SQLCODE != 0 THEN
                  RAISE e_badread;
               END IF;
            EXCEPTION
               WHEN e_badread THEN
                  vn_fetch_rows := 0;
            END;

            -- Tax Matrix Line Found
            IF vn_fetch_rows > 0 THEN
               p_new_tb_transaction_detail.tax_matrix_id := tax_matrix.tax_matrix_id;
               -- Determine "Then" or "Else" Results
               IF tax_matrix.relation_sign = 'na' THEN
                  tax_matrix.relation_amount := 0;
                  v_less_hold_code_flag := tax_matrix.then_hold_code_flag;
                  v_less_taxcode_detail_id := tax_matrix.then_taxcode_detail_id;
                  v_less_taxcode_state_code := tax_matrix.then_taxcode_state_code;
                  v_less_taxcode_type_code := tax_matrix.then_taxcode_type_code;
                  v_less_taxcode_code := tax_matrix.then_taxcode_code;
                  v_less_jurisdiction_id := tax_matrix.then_jurisdiction_id;
                  v_less_measure_type_code := tax_matrix.then_measure_type_code;
                  v_less_taxtype_code := tax_matrix.then_taxtype_code;
                  v_less_cch_taxcat_code := tax_matrix.then_cch_taxcat_code;
                  v_less_cch_group_code := tax_matrix.then_cch_group_code;
                  -- v_less_cch_item_code := tax_matrix.then_cch_item_code;
                  v_equal_hold_code_flag := tax_matrix.then_hold_code_flag;
                  v_equal_taxcode_detail_id := tax_matrix.then_taxcode_detail_id;
                  v_equal_taxcode_state_code := tax_matrix.then_taxcode_state_code;
                  v_equal_taxcode_type_code := tax_matrix.then_taxcode_type_code;
                  v_equal_taxcode_code := tax_matrix.then_taxcode_code;
                  v_equal_jurisdiction_id := tax_matrix.then_jurisdiction_id;
                  v_equal_measure_type_code := tax_matrix.then_measure_type_code;
                  v_equal_taxtype_code := tax_matrix.then_taxtype_code;
                  v_equal_cch_taxcat_code := tax_matrix.then_cch_taxcat_code;
                  v_equal_cch_group_code := tax_matrix.then_cch_group_code;
                  -- v_equal_cch_item_code := tax_matrix.then_cch_item_code;
                  v_greater_hold_code_flag := tax_matrix.then_hold_code_flag;
                  v_greater_taxcode_detail_id := tax_matrix.then_taxcode_detail_id;
                  v_greater_taxcode_state_code := tax_matrix.then_taxcode_state_code;
                  v_greater_taxcode_type_code := tax_matrix.then_taxcode_type_code;
                  v_greater_taxcode_code := tax_matrix.then_taxcode_code;
                  v_greater_jurisdiction_id := tax_matrix.then_jurisdiction_id;
                  v_greater_measure_type_code := tax_matrix.then_measure_type_code;
                  v_greater_taxtype_code := tax_matrix.then_taxtype_code;
                  v_greater_cch_taxcat_code := tax_matrix.then_cch_taxcat_code;
                  v_greater_cch_group_code := tax_matrix.then_cch_group_code;
                  -- v_greater_cch_item_code := tax_matrix.then_cch_item_code;
               ELSIF tax_matrix.relation_sign = '=' THEN
                  v_less_hold_code_flag := tax_matrix.else_hold_code_flag;
                  v_less_taxcode_detail_id := tax_matrix.else_taxcode_detail_id;
                  v_less_taxcode_state_code := tax_matrix.else_taxcode_state_code;
                  v_less_taxcode_type_code := tax_matrix.else_taxcode_type_code;
                  v_less_taxcode_code := tax_matrix.else_taxcode_code;
                  v_less_jurisdiction_id := tax_matrix.else_jurisdiction_id;
                  v_less_measure_type_code := tax_matrix.else_measure_type_code;
                  v_less_taxtype_code := tax_matrix.else_taxtype_code;
                  v_less_cch_taxcat_code := tax_matrix.else_cch_taxcat_code;
                  v_less_cch_group_code := tax_matrix.else_cch_group_code;
                  -- v_less_cch_item_code := tax_matrix.else_cch_item_code;
                  v_equal_hold_code_flag := tax_matrix.then_hold_code_flag;
                  v_equal_taxcode_detail_id := tax_matrix.then_taxcode_detail_id;
                  v_equal_taxcode_state_code := tax_matrix.then_taxcode_state_code;
                  v_equal_taxcode_type_code := tax_matrix.then_taxcode_type_code;
                  v_equal_taxcode_code := tax_matrix.then_taxcode_code;
                  v_equal_jurisdiction_id := tax_matrix.then_jurisdiction_id;
                  v_equal_measure_type_code := tax_matrix.then_measure_type_code;
                  v_equal_taxtype_code := tax_matrix.then_taxtype_code;
                  v_equal_cch_taxcat_code := tax_matrix.then_cch_taxcat_code;
                  v_equal_cch_group_code := tax_matrix.then_cch_group_code;
                  -- v_equal_cch_item_code := tax_matrix.then_cch_item_code;
                  v_greater_hold_code_flag := tax_matrix.else_hold_code_flag;
                  v_greater_taxcode_detail_id := tax_matrix.else_taxcode_detail_id;
                  v_greater_taxcode_state_code := tax_matrix.else_taxcode_state_code;
                  v_greater_taxcode_type_code := tax_matrix.else_taxcode_type_code;
                  v_greater_taxcode_code := tax_matrix.else_taxcode_code;
                  v_greater_jurisdiction_id := tax_matrix.else_jurisdiction_id;
                  v_greater_measure_type_code := tax_matrix.else_measure_type_code;
                  v_greater_taxtype_code := tax_matrix.else_taxtype_code;
                  v_greater_cch_taxcat_code := tax_matrix.else_cch_taxcat_code;
                  v_greater_cch_group_code := tax_matrix.else_cch_group_code;
                  -- v_greater_cch_item_code := tax_matrix.else_cch_item_code;
               ELSIF tax_matrix.relation_sign = '<' THEN
                  v_less_hold_code_flag := tax_matrix.then_hold_code_flag;
                  v_less_taxcode_detail_id := tax_matrix.then_taxcode_detail_id;
                  v_less_taxcode_state_code := tax_matrix.then_taxcode_state_code;
                  v_less_taxcode_type_code := tax_matrix.then_taxcode_type_code;
                  v_less_taxcode_code := tax_matrix.then_taxcode_code;
                  v_less_jurisdiction_id := tax_matrix.then_jurisdiction_id;
                  v_less_measure_type_code := tax_matrix.then_measure_type_code;
                  v_less_taxtype_code := tax_matrix.then_taxtype_code;
                  v_less_cch_taxcat_code := tax_matrix.then_cch_taxcat_code;
                  v_less_cch_group_code := tax_matrix.then_cch_group_code;
                  -- v_less_cch_item_code := tax_matrix.then_cch_item_code;
                  v_equal_hold_code_flag := tax_matrix.else_hold_code_flag;
                  v_equal_taxcode_detail_id := tax_matrix.else_taxcode_detail_id;
                  v_equal_taxcode_state_code := tax_matrix.else_taxcode_state_code;
                  v_equal_taxcode_type_code := tax_matrix.else_taxcode_type_code;
                  v_equal_taxcode_code := tax_matrix.else_taxcode_code;
                  v_equal_jurisdiction_id := tax_matrix.else_jurisdiction_id;
                  v_equal_measure_type_code := tax_matrix.else_measure_type_code;
                  v_equal_taxtype_code := tax_matrix.else_taxtype_code;
                  v_equal_cch_taxcat_code := tax_matrix.else_cch_taxcat_code;
                  v_equal_cch_group_code := tax_matrix.else_cch_group_code;
                  -- v_equal_cch_item_code := tax_matrix.else_cch_item_code;
                  v_greater_hold_code_flag := tax_matrix.else_hold_code_flag;
                  v_greater_taxcode_detail_id := tax_matrix.else_taxcode_detail_id;
                  v_greater_taxcode_state_code := tax_matrix.else_taxcode_state_code;
                  v_greater_taxcode_type_code := tax_matrix.else_taxcode_type_code;
                  v_greater_taxcode_code := tax_matrix.else_taxcode_code;
                  v_greater_jurisdiction_id := tax_matrix.else_jurisdiction_id;
                  v_greater_measure_type_code := tax_matrix.else_measure_type_code;
                  v_greater_taxtype_code := tax_matrix.else_taxtype_code;
                  v_greater_cch_taxcat_code := tax_matrix.else_cch_taxcat_code;
                  v_greater_cch_group_code := tax_matrix.else_cch_group_code;
                  -- v_greater_cch_item_code := tax_matrix.else_cch_item_code;
               ELSIF tax_matrix.relation_sign = '<=' THEN
                  v_less_hold_code_flag := tax_matrix.then_hold_code_flag;
                  v_less_taxcode_detail_id := tax_matrix.then_taxcode_detail_id;
                  v_less_taxcode_state_code := tax_matrix.then_taxcode_state_code;
                  v_less_taxcode_type_code := tax_matrix.then_taxcode_type_code;
                  v_less_taxcode_code := tax_matrix.then_taxcode_code;
                  v_less_jurisdiction_id := tax_matrix.then_jurisdiction_id;
                  v_less_measure_type_code := tax_matrix.then_measure_type_code;
                  v_less_taxtype_code := tax_matrix.then_taxtype_code;
                  v_less_cch_taxcat_code := tax_matrix.then_cch_taxcat_code;
                  v_less_cch_group_code := tax_matrix.then_cch_group_code;
                  -- v_less_cch_item_code := tax_matrix.then_cch_item_code;
                  v_equal_hold_code_flag := tax_matrix.then_hold_code_flag;
                  v_equal_taxcode_detail_id := tax_matrix.then_taxcode_detail_id;
                  v_equal_taxcode_state_code := tax_matrix.then_taxcode_state_code;
                  v_equal_taxcode_type_code := tax_matrix.then_taxcode_type_code;
                  v_equal_taxcode_code := tax_matrix.then_taxcode_code;
                  v_equal_jurisdiction_id := tax_matrix.then_jurisdiction_id;
                  v_equal_measure_type_code := tax_matrix.then_measure_type_code;
                  v_equal_taxtype_code := tax_matrix.then_taxtype_code;
                  v_equal_cch_taxcat_code := tax_matrix.then_cch_taxcat_code;
                  v_equal_cch_group_code := tax_matrix.then_cch_group_code;
                  -- v_equal_cch_item_code := tax_matrix.then_cch_item_code;
                  v_greater_hold_code_flag := tax_matrix.else_hold_code_flag;
                  v_greater_taxcode_detail_id := tax_matrix.else_taxcode_detail_id;
                  v_greater_taxcode_state_code := tax_matrix.else_taxcode_state_code;
                  v_greater_taxcode_type_code := tax_matrix.else_taxcode_type_code;
                  v_greater_taxcode_code := tax_matrix.else_taxcode_code;
                  v_greater_jurisdiction_id := tax_matrix.else_jurisdiction_id;
                  v_greater_measure_type_code := tax_matrix.else_measure_type_code;
                  v_greater_taxtype_code := tax_matrix.else_taxtype_code;
                  v_greater_cch_taxcat_code := tax_matrix.else_cch_taxcat_code;
                  v_greater_cch_group_code := tax_matrix.else_cch_group_code;
                  -- v_greater_cch_item_code := tax_matrix.else_cch_item_code;
               ELSIF tax_matrix.relation_sign = '>' THEN
                  v_less_hold_code_flag := tax_matrix.else_hold_code_flag;
                  v_less_taxcode_detail_id := tax_matrix.else_taxcode_detail_id;
                  v_less_taxcode_state_code := tax_matrix.else_taxcode_state_code;
                  v_less_taxcode_type_code := tax_matrix.else_taxcode_type_code;
                  v_less_taxcode_code := tax_matrix.else_taxcode_code;
                  v_less_jurisdiction_id := tax_matrix.else_jurisdiction_id;
                  v_less_measure_type_code := tax_matrix.else_measure_type_code;
                  v_less_taxtype_code := tax_matrix.else_taxtype_code;
                  v_less_cch_taxcat_code := tax_matrix.else_cch_taxcat_code;
                  v_less_cch_group_code := tax_matrix.else_cch_group_code;
                  -- v_less_cch_item_code := tax_matrix.else_cch_item_code;
                  v_equal_hold_code_flag := tax_matrix.else_hold_code_flag;
                  v_equal_taxcode_detail_id := tax_matrix.else_taxcode_detail_id;
                  v_equal_taxcode_state_code := tax_matrix.else_taxcode_state_code;
                  v_equal_taxcode_type_code := tax_matrix.else_taxcode_type_code;
                  v_equal_taxcode_code := tax_matrix.else_taxcode_code;
                  v_equal_jurisdiction_id := tax_matrix.else_jurisdiction_id;
                  v_equal_measure_type_code := tax_matrix.else_measure_type_code;
                  v_equal_taxtype_code := tax_matrix.else_taxtype_code;
                  v_equal_cch_taxcat_code := tax_matrix.else_cch_taxcat_code;
                  v_equal_cch_group_code := tax_matrix.else_cch_group_code;
                  -- v_equal_cch_item_code := tax_matrix.else_cch_item_code;
                  v_greater_hold_code_flag := tax_matrix.then_hold_code_flag;
                  v_greater_taxcode_detail_id := tax_matrix.then_taxcode_detail_id;
                  v_greater_taxcode_state_code := tax_matrix.then_taxcode_state_code;
                  v_greater_taxcode_type_code := tax_matrix.then_taxcode_type_code;
                  v_greater_taxcode_code := tax_matrix.then_taxcode_code;
                  v_greater_jurisdiction_id := tax_matrix.then_jurisdiction_id;
                  v_greater_measure_type_code := tax_matrix.then_measure_type_code;
                  v_greater_taxtype_code := tax_matrix.then_taxtype_code;
                  v_greater_cch_taxcat_code := tax_matrix.then_cch_taxcat_code;
                  v_greater_cch_group_code := tax_matrix.then_cch_group_code;
                  -- v_greater_cch_item_code := tax_matrix.then_cch_item_code;
               ELSIF tax_matrix.relation_sign = '>=' THEN
                  v_less_hold_code_flag := tax_matrix.else_hold_code_flag;
                  v_less_taxcode_detail_id := tax_matrix.else_taxcode_detail_id;
                  v_less_taxcode_state_code := tax_matrix.else_taxcode_state_code;
                  v_less_taxcode_type_code := tax_matrix.else_taxcode_type_code;
                  v_less_taxcode_code := tax_matrix.else_taxcode_code;
                  v_less_jurisdiction_id := tax_matrix.else_jurisdiction_id;
                  v_less_measure_type_code := tax_matrix.else_measure_type_code;
                  v_less_taxtype_code := tax_matrix.else_taxtype_code;
                  v_less_cch_taxcat_code := tax_matrix.else_cch_taxcat_code;
                  v_less_cch_group_code := tax_matrix.else_cch_group_code;
                  -- v_less_cch_item_code := tax_matrix.else_cch_item_code;
                  v_equal_hold_code_flag := tax_matrix.then_hold_code_flag;
                  v_equal_taxcode_detail_id := tax_matrix.then_taxcode_detail_id;
                  v_equal_taxcode_state_code := tax_matrix.then_taxcode_state_code;
                  v_equal_taxcode_type_code := tax_matrix.then_taxcode_type_code;
                  v_equal_taxcode_code := tax_matrix.then_taxcode_code;
                  v_equal_jurisdiction_id := tax_matrix.then_jurisdiction_id;
                  v_equal_measure_type_code := tax_matrix.then_measure_type_code;
                  v_equal_taxtype_code := tax_matrix.then_taxtype_code;
                  v_equal_cch_taxcat_code := tax_matrix.then_cch_taxcat_code;
                  v_equal_cch_group_code := tax_matrix.then_cch_group_code;
                  -- v_equal_cch_item_code := tax_matrix.then_cch_item_code;
                  v_greater_hold_code_flag := tax_matrix.then_hold_code_flag;
                  v_greater_taxcode_detail_id := tax_matrix.then_taxcode_detail_id;
                  v_greater_taxcode_state_code := tax_matrix.then_taxcode_state_code;
                  v_greater_taxcode_type_code := tax_matrix.then_taxcode_type_code;
                  v_greater_taxcode_code := tax_matrix.then_taxcode_code;
                  v_greater_jurisdiction_id := tax_matrix.then_jurisdiction_id;
                  v_greater_measure_type_code := tax_matrix.then_measure_type_code;
                  v_greater_taxtype_code := tax_matrix.then_taxtype_code;
                  v_greater_cch_taxcat_code := tax_matrix.then_cch_taxcat_code;
                  v_greater_cch_group_code := tax_matrix.then_cch_group_code;
                  -- v_greater_cch_item_code := tax_matrix.then_cch_item_code;
               ELSIF tax_matrix.relation_sign = '<>' THEN
                  v_less_hold_code_flag := tax_matrix.then_hold_code_flag;
                  v_less_taxcode_detail_id := tax_matrix.then_taxcode_detail_id;
                  v_less_taxcode_state_code := tax_matrix.then_taxcode_state_code;
                  v_less_taxcode_type_code := tax_matrix.then_taxcode_type_code;
                  v_less_taxcode_code := tax_matrix.then_taxcode_code;
                  v_less_jurisdiction_id := tax_matrix.then_jurisdiction_id;
                  v_less_measure_type_code := tax_matrix.then_measure_type_code;
                  v_less_taxtype_code := tax_matrix.then_taxtype_code;
                  v_less_cch_taxcat_code := tax_matrix.then_cch_taxcat_code;
                  v_less_cch_group_code := tax_matrix.then_cch_group_code;
                  -- v_less_cch_item_code := tax_matrix.then_cch_item_code;
                  v_equal_hold_code_flag := tax_matrix.else_hold_code_flag;
                  v_equal_taxcode_detail_id := tax_matrix.else_taxcode_detail_id;
                  v_equal_taxcode_state_code := tax_matrix.else_taxcode_state_code;
                  v_equal_taxcode_type_code := tax_matrix.else_taxcode_type_code;
                  v_equal_taxcode_code := tax_matrix.else_taxcode_code;
                  v_equal_jurisdiction_id := tax_matrix.else_jurisdiction_id;
                  v_equal_measure_type_code := tax_matrix.else_measure_type_code;
                  v_equal_taxtype_code := tax_matrix.else_taxtype_code;
                  v_equal_cch_taxcat_code := tax_matrix.else_cch_taxcat_code;
                  v_equal_cch_group_code := tax_matrix.else_cch_group_code;
                  -- v_equal_cch_item_code := tax_matrix.else_cch_item_code;
                  v_greater_hold_code_flag := tax_matrix.then_hold_code_flag;
                  v_greater_taxcode_detail_id := tax_matrix.then_taxcode_detail_id;
                  v_greater_taxcode_state_code := tax_matrix.then_taxcode_state_code;
                  v_greater_taxcode_type_code := tax_matrix.then_taxcode_type_code;
                  v_greater_taxcode_code := tax_matrix.then_taxcode_code;
                  v_greater_jurisdiction_id := tax_matrix.then_jurisdiction_id;
                  v_greater_measure_type_code := tax_matrix.then_measure_type_code;
                  v_greater_taxtype_code := tax_matrix.then_taxtype_code;
                  v_greater_cch_taxcat_code := tax_matrix.then_cch_taxcat_code;
                  v_greater_cch_group_code := tax_matrix.then_cch_group_code;
                  -- v_greater_cch_item_code := tax_matrix.then_cch_item_code;
               END IF;

               SELECT DECODE(SIGN(p_new_tb_transaction_detail.gl_line_itm_dist_amt - tax_matrix.relation_amount), -1, v_less_hold_code_flag, 0, v_equal_hold_code_flag, 1, v_greater_hold_code_flag),
                      DECODE(SIGN(p_new_tb_transaction_detail.gl_line_itm_dist_amt - tax_matrix.relation_amount), -1, v_less_taxcode_detail_id, 0, v_equal_taxcode_detail_id, 1, v_greater_taxcode_detail_id),
                      DECODE(SIGN(p_new_tb_transaction_detail.gl_line_itm_dist_amt - tax_matrix.relation_amount), -1, v_less_taxcode_state_code, 0, v_equal_taxcode_state_code, 1, v_greater_taxcode_state_code),
                      DECODE(SIGN(p_new_tb_transaction_detail.gl_line_itm_dist_amt - tax_matrix.relation_amount), -1, v_less_taxcode_type_code, 0, v_equal_taxcode_type_code, 1, v_greater_taxcode_type_code),
                      DECODE(SIGN(p_new_tb_transaction_detail.gl_line_itm_dist_amt - tax_matrix.relation_amount), -1, v_less_taxcode_code, 0, v_equal_taxcode_code, 1, v_greater_taxcode_code),
                      DECODE(SIGN(p_new_tb_transaction_detail.gl_line_itm_dist_amt - tax_matrix.relation_amount), -1, v_less_jurisdiction_id, 0, v_equal_jurisdiction_id, 1, v_greater_jurisdiction_id),
                      DECODE(SIGN(p_new_tb_transaction_detail.gl_line_itm_dist_amt - tax_matrix.relation_amount), -1, v_less_measure_type_code, 0, v_equal_measure_type_code, 1, v_greater_measure_type_code),
                      DECODE(SIGN(p_new_tb_transaction_detail.gl_line_itm_dist_amt - tax_matrix.relation_amount), -1, v_less_taxtype_code, 0, v_equal_taxtype_code, 1, v_greater_taxtype_code),
                      DECODE(SIGN(p_new_tb_transaction_detail.gl_line_itm_dist_amt - tax_matrix.relation_amount), -1, v_less_cch_taxcat_code, 0, v_equal_cch_taxcat_code, 1, v_greater_cch_taxcat_code),
                      DECODE(SIGN(p_new_tb_transaction_detail.gl_line_itm_dist_amt - tax_matrix.relation_amount), -1, v_less_cch_group_code, 0, v_equal_cch_group_code, 1, v_greater_cch_group_code)
                 INTO v_hold_code_flag, p_new_tb_transaction_detail.taxcode_detail_id, p_new_tb_transaction_detail.taxcode_state_code, p_new_tb_transaction_detail.taxcode_type_code, p_new_tb_transaction_detail.taxcode_code, v_override_jurisdiction_id, p_new_tb_transaction_detail.measure_type_code, v_taxtype_code, p_new_tb_transaction_detail.cch_taxcat_code, p_new_tb_transaction_detail.cch_group_code
                 FROM DUAL;

               IF v_override_jurisdiction_id IS NOT NULL AND v_override_jurisdiction_id <> 0 THEN
                  p_new_tb_transaction_detail.jurisdiction_id := v_override_jurisdiction_id;
                  p_new_tb_transaction_detail.manual_jurisdiction_ind := 'AO';
               END IF;
            ELSE
               -- Tax Matrix Line NOT Found
               p_new_tb_transaction_detail.transaction_ind := 'S';
               p_new_tb_transaction_detail.suspend_ind := 'T';
            END IF;
         END IF;
      ELSE
         -- Get Tax Type Code
         BEGIN
           SELECT taxtype_code
             INTO v_taxtype_code
             FROM tb_taxcode
            WHERE taxcode_code = p_new_tb_transaction_detail.taxcode_code
              AND taxcode_type_code = p_new_tb_transaction_detail.taxcode_type_code;
          EXCEPTION
            WHEN OTHERS THEN
              v_taxtype_code := 'U';
         END;
      END IF;

      -- Continue if not Suspended
      IF p_new_tb_transaction_detail.transaction_ind IS NULL OR p_new_tb_transaction_detail.transaction_ind <> 'S' THEN
         -- ****** DETERMINE TYPE of TAXCODE ****** --
         -- If the new TaxCode Type is "T"axable then recalc Tax Amounts
         IF UPPER(p_new_tb_transaction_detail.taxcode_type_code) = 'T' THEN
            -- If Override jurisdiction id is not blank or 0 then use it instead of location matrix
            IF p_new_tb_transaction_detail.jurisdiction_id IS NOT NULL AND p_new_tb_transaction_detail.jurisdiction_id <> 0 THEN
               -- Only if there is a Location Matrix ID -- 3351 -- mbf01
               IF p_new_tb_transaction_detail.location_matrix_id IS NOT NULL AND p_new_tb_transaction_detail.location_matrix_id <> 0 THEN
                  -- Get Override Tax Type Code -- 3351
                  -- Get 5 Tax Calc Flags -- 3411 -- MBF02
                  BEGIN
                     SELECT override_taxtype_code,
                            state_flag, county_flag, county_local_flag, city_flag, city_local_flag
                       INTO location_matrix.override_taxtype_code,
                            location_matrix.state_flag,
                            location_matrix.county_flag,
                            location_matrix.county_local_flag,
                            location_matrix.city_flag,
                            location_matrix.city_local_flag
                       FROM TB_LOCATION_MATRIX
                      WHERE TB_LOCATION_MATRIX.location_matrix_id = p_new_tb_transaction_detail.location_matrix_id;
                  EXCEPTION
                     WHEN NO_DATA_FOUND THEN
                        location_matrix.override_taxtype_code := '*NO';
                        location_matrix.state_flag := '1';
                        location_matrix.county_flag := '1';
                        location_matrix.county_local_flag := '1';
                        location_matrix.city_flag := '1';
                        location_matrix.city_local_flag := '1';
                  END;
               ELSE
                  location_matrix.override_taxtype_code := '*NO';
                  location_matrix.state_flag := '1';
                  location_matrix.county_flag := '1';
                  location_matrix.county_local_flag := '1';
                  location_matrix.city_flag := '1';
                  location_matrix.city_local_flag := '1';
               END IF;
            ELSE
            -- Search for Location Matrix if not already found
            IF p_new_tb_transaction_detail.location_matrix_id IS NULL  AND p_new_tb_transaction_detail.manual_jurisdiction_ind IS NULL THEN
               v_transaction_detail_id := p_new_tb_transaction_detail.transaction_detail_id;
               -- ***** Create Dynamic WHERE Clause for Matrix SQLs ***** --
               -- Location Matrix Drivers
               /*OPEN location_driver_cursor;
               LOOP
                  FETCH location_driver_cursor INTO location_driver;
                  EXIT WHEN location_driver_cursor%NOTFOUND;
                  IF location_driver.null_driver_flag = '1' THEN
                     IF location_driver.wildcard_flag = '1' THEN
                        IF UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'CHAR' OR
                           UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'VARC' THEN
                              vc_location_matrix_where := vc_location_matrix_where || 'AND ' ||
                              '((tb_tmp_transaction_detail.' || location_driver.trans_dtl_column_name || ' IS NULL AND (tb_location_matrix.' || location_driver.matrix_column_name || ' = ''*ALL'' OR tb_location_matrix.' || location_driver.matrix_column_name || ' = ''*NULL'' )) OR ' ||
                              '(tb_tmp_transaction_detail.' || location_driver.trans_dtl_column_name || ' IS NOT NULL AND (tb_location_matrix.' || location_driver.matrix_column_name || ' = ''*ALL'' OR UPPER(tb_tmp_transaction_detail.' || location_driver.trans_dtl_column_name || ') LIKE UPPER(tb_location_matrix.' || location_driver.matrix_column_name || ') ))) ';
                        END IF;
                     ELSE
                        IF UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'CHAR' OR
                           UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'VARC' THEN
                              vc_location_matrix_where := vc_location_matrix_where || 'AND ' ||
                              '((tb_tmp_transaction_detail.' || location_driver.trans_dtl_column_name || ' IS NULL AND (tb_location_matrix.' || location_driver.matrix_column_name || ' = ''*ALL'' OR tb_location_matrix.' || location_driver.matrix_column_name || ' = ''*NULL'' )) OR ' ||
                              '(tb_tmp_transaction_detail.' || location_driver.trans_dtl_column_name || ' IS NOT NULL AND (tb_location_matrix.' || location_driver.matrix_column_name || ' = ''*ALL'' OR UPPER(tb_tmp_transaction_detail.' || location_driver.trans_dtl_column_name || ') = UPPER(tb_location_matrix.' || location_driver.matrix_column_name || ') ))) ';
                        END IF;
                     END IF;
                  ELSE
                     IF location_driver.wildcard_flag = '1' THEN
                        IF UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'CHAR' OR
                           UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'VARC' THEN
                              vc_location_matrix_where := vc_location_matrix_where || 'AND ' ||
                              '(tb_location_matrix.' || location_driver.matrix_column_name || ' = ''*ALL'' OR UPPER(tb_tmp_transaction_detail.' || location_driver.trans_dtl_column_name || ') LIKE UPPER(tb_location_matrix.' || location_driver.matrix_column_name || ') ) ';
                        END IF;
                     ELSE
                        IF UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'CHAR' OR
                           UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'VARC' THEN
                              vc_location_matrix_where := vc_location_matrix_where || 'AND ' ||
                              '(tb_location_matrix.' || location_driver.matrix_column_name || ' = ''*ALL'' OR UPPER(tb_tmp_transaction_detail.' || location_driver.trans_dtl_column_name || ') = UPPER(tb_location_matrix.' || location_driver.matrix_column_name || ') ) ';
                        END IF;
                     END IF;
                  END IF;
               END LOOP;
               CLOSE location_driver_cursor;*/

	       -- New call to sp_gen_location_driver for MidTier Cleanup
	       Sp_Gen_Location_Driver (
			p_generate_driver_reference	=> 'N',
			p_an_batch_id			=> NULL,
			p_transaction_table_name	=> 'tb_tmp_transaction_detail',
			p_location_table_name		=> 'tb_location_matrix',
			p_vc_location_matrix_where	=> vc_location_matrix_where);

               -- If no drivers found raise error, else create transaction detail sql statement
               IF vc_location_matrix_where IS NULL THEN
                  NULL;
               ELSE
                  vc_location_matrix_stmt := vc_location_matrix_select || vc_location_matrix_where || vc_location_matrix_orderby;
               END IF;

               -- Search Location Matrix for matches
               BEGIN
                   OPEN location_matrix_cursor
                    FOR vc_location_matrix_stmt
                  USING v_transaction_detail_id;
                  FETCH location_matrix_cursor
                   INTO location_matrix;
                  -- Rows found?
                  IF location_matrix_cursor%FOUND THEN
                     vn_fetch_rows := location_matrix_cursor%ROWCOUNT;
                  ELSE
                     vn_fetch_rows := 0;
                  END IF;
                  CLOSE location_matrix_cursor;
                  IF SQLCODE != 0 THEN
                    RAISE e_badread;
                  END IF;
               EXCEPTION
                  WHEN e_badread THEN
                     vn_fetch_rows := 0;
               END;

               -- Location Matrix Line Found
               IF vn_fetch_rows > 0 THEN
                  p_new_tb_transaction_detail.location_matrix_id := location_matrix.location_matrix_id;
                  p_new_tb_transaction_detail.jurisdiction_id := location_matrix.jurisdiction_id;
               ELSE
                  p_new_tb_transaction_detail.transaction_ind := 'S';
                  p_new_tb_transaction_detail.suspend_ind := 'L';
               END IF;
            END IF;
            END IF;

            -- Continue if transaction not Suspended
            IF ( p_new_tb_transaction_detail.transaction_ind IS NULL OR p_new_tb_transaction_detail.transaction_ind <> 'S' ) THEN
               -- Get Jurisdiction TaxRates and calculate Tax Amounts
               Sp_Getusetax(p_new_tb_transaction_detail.jurisdiction_id,
                            p_new_tb_transaction_detail.gl_date,
                            p_new_tb_transaction_detail.measure_type_code,
                            p_new_tb_transaction_detail.gl_line_itm_dist_amt,
                            v_taxtype_code,
                            location_matrix.override_taxtype_code,
                            location_matrix.state_flag,
                            location_matrix.county_flag,
                            location_matrix.county_local_flag,
                            location_matrix.city_flag,
                            location_matrix.city_local_flag,
                            p_new_tb_transaction_detail.invoice_freight_amt,
                            p_new_tb_transaction_detail.invoice_discount_amt,
                            p_new_tb_transaction_detail.transaction_state_code,
-- future?? --                     p_new_tb_transaction_detail.import_definition_code,
                            'importdefn',
                            p_new_tb_transaction_detail.taxcode_code,
                            p_new_tb_transaction_detail.jurisdiction_taxrate_id,
                            p_new_tb_transaction_detail.state_use_amount,
                            p_new_tb_transaction_detail.state_use_tier2_amount,
                            p_new_tb_transaction_detail.state_use_tier3_amount,
                            p_new_tb_transaction_detail.county_use_amount,
                            p_new_tb_transaction_detail.county_local_use_amount,
                            p_new_tb_transaction_detail.city_use_amount,
                            p_new_tb_transaction_detail.city_local_use_amount,
                            p_new_tb_transaction_detail.state_use_rate,
                            p_new_tb_transaction_detail.state_use_tier2_rate,
                            p_new_tb_transaction_detail.state_use_tier3_rate,
                            p_new_tb_transaction_detail.state_split_amount,
                            p_new_tb_transaction_detail.state_tier2_min_amount,
                            p_new_tb_transaction_detail.state_tier2_max_amount,
                            p_new_tb_transaction_detail.state_maxtax_amount,
                            p_new_tb_transaction_detail.county_use_rate,
                            p_new_tb_transaction_detail.county_local_use_rate,
                            p_new_tb_transaction_detail.county_split_amount,
                            p_new_tb_transaction_detail.county_maxtax_amount,
                            p_new_tb_transaction_detail.county_single_flag,
                            p_new_tb_transaction_detail.county_default_flag,
                            p_new_tb_transaction_detail.city_use_rate,
                            p_new_tb_transaction_detail.city_local_use_rate,
                            p_new_tb_transaction_detail.city_split_amount,
                            p_new_tb_transaction_detail.city_split_use_rate,
                            p_new_tb_transaction_detail.city_single_flag,
                            p_new_tb_transaction_detail.city_default_flag,
                            p_new_tb_transaction_detail.combined_use_rate,
                            p_new_tb_transaction_detail.tb_calc_tax_amt,
-- future?? --                     p_new_tb_transaction_detail.taxable_amt,
-- Targa --                    p_new_tb_transaction_detail.user_number_10,
                            vn_taxable_amt,
                            v_taxtype_used );

               -- Suspend if tax rate id is null
               IF p_new_tb_transaction_detail.jurisdiction_taxrate_id IS NULL OR p_new_tb_transaction_detail.jurisdiction_taxrate_id = 0 THEN
                  p_new_tb_transaction_detail.transaction_ind := 'S';
                  p_new_tb_transaction_detail.suspend_ind := 'R';
               ELSE
                  -- Check for Taxable Amount -- 3351
                  IF vn_taxable_amt <> p_new_tb_transaction_detail.gl_line_itm_dist_amt THEN
                     p_new_tb_transaction_detail.gl_extract_amt := p_new_tb_transaction_detail.gl_line_itm_dist_amt;
                     p_new_tb_transaction_detail.gl_line_itm_dist_amt := vn_taxable_amt;
                  END IF;
               END IF;
            END IF;

         -- Else, If the new TaxCode Type is "E"xempt
         ELSIF p_new_tb_transaction_detail.taxcode_type_code = 'E' THEN
            p_new_tb_transaction_detail.transaction_ind := 'P';

         -- Else, If the new TaxCode Type is a "Q"uestion
         ELSIF p_new_tb_transaction_detail.taxcode_type_code = 'Q' THEN
           p_new_tb_transaction_detail.transaction_ind := 'Q';

         -- Else, the new TaxCode Type is Unrecognized - Suspend
         ELSE
            p_new_tb_transaction_detail.transaction_ind := 'S';
            p_new_tb_transaction_detail.suspend_ind := '?';
         END IF;
      END IF;

      -- Continue if not Suspended
      IF p_new_tb_transaction_detail.transaction_ind IS NULL OR p_new_tb_transaction_detail.transaction_ind <> 'S' THEN
         p_new_tb_transaction_detail.transaction_ind := 'P';
      END IF;
   END IF;

   -- ******  GL logging module  ****** ------------------------------------------------------------
   Sp_Gl_Logging (p_old_tb_transaction_detail, p_new_tb_transaction_detail, v_sysdate, v_sysdate_plus);

END;
/

UPDATE  TB_OPTION SET  VALUE='EST'  WHERE OPTION_CODE='DEFUSERTIMEZONE' AND VALUE='EASTERN';
UPDATE  TB_OPTION SET  VALUE='AKST'  WHERE OPTION_CODE='DEFUSERTIMEZONE' AND VALUE='ALASKA';
UPDATE  TB_OPTION SET  VALUE='HAST'  WHERE OPTION_CODE='DEFUSERTIMEZONE' AND VALUE='HAWAII';
UPDATE  TB_OPTION SET  VALUE='MST'  WHERE OPTION_CODE='DEFUSERTIMEZONE' AND VALUE='MOUNTAIN';
UPDATE  TB_OPTION SET  VALUE='CST'  WHERE OPTION_CODE='DEFUSERTIMEZONE' AND VALUE='CENTRAL';
UPDATE  TB_OPTION SET  VALUE='PST'  WHERE OPTION_CODE='DEFUSERTIMEZONE' AND VALUE='PACIFIC';

UPDATE  TB_OPTION SET  VALUE='EST'  WHERE OPTION_CODE='USERTIMEZONE' AND VALUE='EASTERN';
UPDATE  TB_OPTION SET  VALUE='AKST'  WHERE OPTION_CODE='USERTIMEZONE' AND VALUE='ALASKA';
UPDATE  TB_OPTION SET  VALUE='HAST'  WHERE OPTION_CODE='USERTIMEZONE' AND VALUE='HAWAII';
UPDATE  TB_OPTION SET  VALUE='MST'  WHERE OPTION_CODE='USERTIMEZONE' AND VALUE='MOUNTAIN';
UPDATE  TB_OPTION SET  VALUE='CST'  WHERE OPTION_CODE='USERTIMEZONE' AND VALUE='CENTRAL';
UPDATE  TB_OPTION SET  VALUE='PST'  WHERE OPTION_CODE='USERTIMEZONE' AND VALUE='PACIFIC';

--System Timezone
UPDATE  TB_OPTION SET  VALUE='UTC'  WHERE OPTION_CODE='SYSTEMTIMEZONE';
--STSCorporate Timezone
UPDATE  TB_OPTION SET  VALUE='UTC'  WHERE OPTION_CODE='STSCORPTIMEZONE';

UPDATE  TB_LIST_CODE  SET  CODE_CODE='EST'  WHERE CODE_TYPE_CODE='TIMEZONE' AND CODE_CODE='EASTERN';  
UPDATE  TB_LIST_CODE  SET  CODE_CODE='AKST'  WHERE CODE_TYPE_CODE='TIMEZONE' AND CODE_CODE='ALASKA'; 
UPDATE  TB_LIST_CODE  SET  CODE_CODE='HAST'  WHERE CODE_TYPE_CODE='TIMEZONE' AND CODE_CODE='HAWAII';  
UPDATE  TB_LIST_CODE  SET  CODE_CODE='MST'  WHERE CODE_TYPE_CODE='TIMEZONE' AND CODE_CODE='MOUNTAIN';  
UPDATE  TB_LIST_CODE  SET  CODE_CODE='CST'  WHERE CODE_TYPE_CODE='TIMEZONE' AND CODE_CODE='CENTRAL';
UPDATE  TB_LIST_CODE  SET  CODE_CODE='PST'  WHERE CODE_TYPE_CODE='TIMEZONE' AND CODE_CODE='PACIFIC'; 
INSERT INTO TB_LIST_CODE (CODE_TYPE_CODE, CODE_CODE, DESCRIPTION, MESSAGE) VALUES ('TIMEZONE', 'UTC', 'Universal Time', '-00:00');



COMMIT;

spool off