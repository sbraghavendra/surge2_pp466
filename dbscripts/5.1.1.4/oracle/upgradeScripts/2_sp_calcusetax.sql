/*  Calculate Use Tax  ****************************************************************************/
CREATE OR REPLACE PROCEDURE sp_calcusetax(
/* ************************************************************************************************/
/* Object Type/Name: Stored Procedure - sp_calcusetax                                             */
/* Author:           Michael B. Fuller                                                            */
/* Date:             a long time ago                                                              */
/* Description:      calculate use (or sales by override) taxes                                   */
/* Arguments:        many - see "IN"s below                                                       */
/* Returns:          many = see "OUT"s below                                                      */
/* ************************************************************************************************/
/* Maintenance History:                                                                           */
/* Scan  Author       Date     Release    Description                                  Ticket #   */
/* ----- ---------- ---------- ---------- -------------------------------------------- ---------- */
/*       M. Fuller  06/02/2006 3.3.5.0    Fix processing an allocated line - Temporary 871        */
/*       M. Fuller  01/04/2007 3.3.5.1    Fix processing an allocated line - Permenant 871        */
/* MBF01 M. Fuller  04/12/2007 3.3.7.x    Change check for 'TARGA' to be in the                   */
/*                                        tb_options table so it does not have to be              */
/*                                        hard-coded.                                             */
/* MBF02 M. Fuller  10/12/2007 3.3.8.x    Correct tax calcs for negative dist. amt.s   904        */
/* MBF03 M. Fuller  08/13/2007 3.4.1.1    Add checkboxes to toggle calc of 5 rates     883        */
/* MBF04 M. Fuller  05/04/2011            Add Country for Federal Tax Level            24         */
/* ************************************************************************************************/
   an_gl_line_itm_dist_amt in number,
   an_invoice_freight_amt in number,
   an_invoice_discount_amt in number,
   av_transaction_country_code in varchar2,
   av_transaction_state_code in varchar2,
   av_import_definition_code in varchar2,
   av_taxcode_code in varchar2,
   ac_country_flag in char,
   ac_state_flag in char,
   ac_county_flag in char,
   ac_county_local_flag in char,
   ac_city_flag in char,
   ac_city_local_flag in char,
   an_country_rate in number,
   an_state_rate in number,
   an_state_use_tier2_rate in number,
   an_state_use_tier3_rate in number,
   an_state_split_amount in number,
   an_state_tier2_min_amount in number,
   an_state_tier2_max_amount in number,
   an_state_maxtax_amount in number,
   an_county_rate in number,
   an_county_local_rate in number,
   an_county_split_amount in number,
   an_county_maxtax_amount in number,
   ac_county_single_flag in char,
   ac_county_default_flag in char,
   an_city_rate in number,
   an_city_local_rate in number,
   an_city_split_amount in number,
   an_city_split_rate in number,
   ac_city_single_flag in char,
   ac_city_default_flag in char,
   an_country_amount out number,
   an_state_amount out number,
   an_state_use_tier2_amount out number,
   an_state_use_tier3_amount out number,
   an_county_amount out number,
   an_county_local_amount out number,
   an_city_amount out number,
   an_city_local_amount out number,
   an_tb_calc_tax_amt out number,
   an_taxable_amt out number )
IS
-- future --  v_sub_tax_flags                 tb_taxcode_state.sub_tax_flags%TYPE               := NULL;
-- future --  v_sub_sep_flags                 tb_import_definition.sub_sep_flags%TYPE           := NULL;
   v_taxable_amt                   tb_transaction_detail.gl_line_itm_dist_amt%TYPE   := 0;
   v_sub_amt                       tb_transaction_detail.gl_line_itm_dist_amt%TYPE   := 0;
   v_pct_dist_amt                  tb_taxcode.pct_dist_amt%TYPE                      := 0.00;
   v_state_use_tier1_rate          tb_jurisdiction_taxrate.state_use_rate%TYPE       := 0;
   v_state_use_tier2_rate          tb_jurisdiction_taxrate.state_use_tier2_rate%TYPE := 0;
   v_state_use_tier3_rate          tb_jurisdiction_taxrate.state_use_tier3_rate%TYPE := 0;
   v_state_tier1_dist_amt          tb_transaction_detail.gl_line_itm_dist_amt%TYPE   := 0;
   v_state_tier2_dist_amt          tb_transaction_detail.gl_line_itm_dist_amt%TYPE   := 0;
   v_state_tier3_dist_amt          tb_transaction_detail.gl_line_itm_dist_amt%TYPE   := 0;
   v_state_tier1_max_amount        tb_jurisdiction_taxrate.state_tier2_min_amount%TYPE := 0;
   v_state_tier2_min_amount        tb_jurisdiction_taxrate.state_tier2_min_amount%TYPE := 0;
   v_state_tier2_max_amount        tb_jurisdiction_taxrate.state_tier2_max_amount%TYPE := 0;
   v_state_tier3_min_amount        tb_jurisdiction_taxrate.state_tier2_max_amount%TYPE := 0;
   v_state_maxtax_amount           tb_jurisdiction_taxrate.state_maxtax_amount%TYPE  := 0;

   -- TEMPORARY
   v_sub_tax_flags varchar(100);
   v_sub_sep_flags varchar(100);
   vc_value varchar(100);

-- ****** Program starts ****** --------------------------------------------------------------------
BEGIN
   -- Get Subsidiary Taxable Flags from TaxCode State - Freight/Discount
-- future --   BEGIN
-- future --     SELECT sub_tax_flags
-- future --       INTO v_sub_tax_flags
-- future --       FROM tb_taxcode_state
-- future --      WHERE taxcode_state_code = av_transaction_state_code;
-- future --  EXCEPTION
-- future --     WHEN OTHERS THEN
-- future --        v_sub_tax_flags := 'TE';
-- future --  END;
	  
   -- Get Subsidiary Separate Flag from Import Definition - Freight/Discount
-- future --  BEGIN
-- future --     SELECT sub_sep_flags
-- future --       INTO v_sub_sep_flags
-- future --       FROM tb_import_definition
-- future --      WHERE import_definition_code = av_import_definition_code;
-- future --  EXCEPTION
-- future --     WHEN OTHERS THEN
-- future --        v_sub_sep_flags := 'EI';
-- future --  END;

   -- TEMPORARY -- Hard Code Flags
 BEGIN
   SELECT value
     INTO vc_value
     FROM tb_option
    WHERE UPPER(option_code) = 'PWDENYMSG'
      AND UPPER(option_type_code) = 'SYSTEM'
      AND UPPER(user_code) = 'SYSTEM';
 EXCEPTION
  WHEN OTHERS THEN
   vc_value := 'NoThInG';
 END;
--   IF av_import_definition_code = 'TARGA' THEN
   IF SUBSTR(UPPER(TRIM(vc_value)),1,5) = 'TARGA' THEN
      SELECT DECODE(av_transaction_state_code,'LA','EE','TE')
        INTO v_sub_tax_flags
        FROM DUAL;
      v_sub_sep_flags := 'EI';
   ELSE
      v_sub_tax_flags := 'XX';
      v_sub_sep_flags := 'XX';
   END IF;

   -- Get Percent of Distribution Amount for Tax Basis
   SELECT pct_dist_amt
     INTO v_pct_dist_amt
     FROM tb_taxcode
    WHERE tb_taxcode.taxcode_code = av_taxcode_code
      AND tb_taxcode.taxcode_type_code = 'T';

   -- Calculate Taxable Amount --
   -- Add/Subtract Freight/Discount
   SELECT an_gl_line_itm_dist_amt +
         (DECODE(SUBSTR(v_sub_sep_flags,1,1)||SUBSTR(v_sub_tax_flags,1,1),'IE',NVL(an_invoice_freight_amt,0) * -1,'ET',NVL(an_invoice_freight_amt,0),0)) +
         (DECODE(SUBSTR(v_sub_sep_flags,2,1)||SUBSTR(v_sub_tax_flags,2,1),'IE',NVL(an_invoice_discount_amt,0) * -1,'ET',NVL(an_invoice_discount_amt,0),0))
     INTO an_taxable_amt
     FROM DUAL;
   -- Apply Percent of Distribution Amount
   IF v_pct_dist_amt IS NOT NULL AND v_pct_dist_amt <> 1.00 AND v_pct_dist_amt <> 0.00 THEN
      an_taxable_amt := an_taxable_amt * v_pct_dist_amt;
   END IF;

   -- Calculate State Use Tax Amounts
-- OLD CODE - REMOVED TO IMPLEMENT TIERS
--   --an_state_use_amount := an_gl_line_itm_dist_amt * an_state_use_rate;
--   IF an_state_split_amount > 0 THEN
--      IF an_state_split_amount < an_gl_line_itm_dist_amt THEN
--         an_state_use_amount := an_state_split_amount * an_state_use_rate;
--      ELSE
--         an_state_use_amount := an_gl_line_itm_dist_amt * an_state_use_rate;
--      END IF;
--   ELSE
--      an_state_use_amount := an_gl_line_itm_dist_amt * an_state_use_rate;
--   END IF;
-- OLD CODE - REMOVED TO IMPLEMENT TIERS

   /*  Determine State Tiers and initialize variables  ************************/
   -- Tier 1 Variables --
   v_state_use_tier1_rate := NVL(an_state_rate,0);
   v_state_tier1_max_amount := NVL(an_state_split_amount,999999999);
   IF v_state_tier1_max_amount = 0 THEN
      v_state_tier1_max_amount := 999999999;
   END IF;
   v_state_tier1_dist_amt := NVL(an_taxable_amt,0);
   an_state_amount := 0;

   -- Tier 2 Variables --
   v_state_use_tier2_rate := NVL(an_state_use_tier2_rate,0);
   IF v_state_tier1_max_amount < 999999999 THEN
      v_state_tier2_min_amount := v_state_tier1_max_amount;
   ELSE
      v_state_tier2_min_amount := NVL(an_state_tier2_min_amount,0);
   END IF;
   v_state_tier2_max_amount := NVL(an_state_tier2_max_amount,999999999);
   IF v_state_tier2_max_amount = 0 THEN
      v_state_tier2_max_amount := 999999999;
   END IF;
   IF v_state_tier2_max_amount < 999999999 THEN
      v_state_tier2_max_amount := v_state_tier2_max_amount - v_state_tier2_min_amount;
   END IF;
   --v_state_tier2_dist_amt := v_state_tier1_dist_amt - v_state_tier2_min_amount;
   v_state_tier2_dist_amt := GREATEST((ABS(v_state_tier1_dist_amt) - v_state_tier2_min_amount),0) * SIGN(v_state_tier1_dist_amt);
   an_state_use_tier2_amount := 0;

   -- Tier 3 Variables --
   v_state_use_tier3_rate := NVL(an_state_use_tier3_rate,0);
   v_state_tier3_min_amount := NVL(an_state_tier2_max_amount,999999999);
   --v_state_tier3_dist_amt := v_state_tier1_dist_amt - v_state_tier3_min_amount;
   v_state_tier3_dist_amt := GREATEST((ABS(v_state_tier1_dist_amt) - v_state_tier3_min_amount),0) * SIGN(v_state_tier1_dist_amt);
   an_state_use_tier3_amount := 0;

   -- MaxTax Amount --
   v_state_maxtax_amount := NVL(an_state_maxtax_amount,999999999);
   IF v_state_maxtax_amount = 0 THEN
      v_state_maxtax_amount := 999999999;
   END IF;
   an_tb_calc_tax_amt := 0;
   an_country_amount := 0;
   an_county_amount := 0;
   an_county_local_amount := 0;
   an_city_amount := 0;
   an_city_local_amount := 0;

   /*  Calculate Country (Federal) Taxes  *************************************/    -- MBF04
   IF ac_country_flag = '1' THEN
      -- Calculate Country Sales/Use Tax Amount
      an_country_amount := an_taxable_amt * an_country_rate;
   END IF;

   /*  Calculate State Taxes  *************************************************/
   IF ac_state_flag = '1' THEN
      -- Tier 1
      IF v_state_use_tier1_rate > 0 THEN
         --an_state_amount := LEAST(v_state_tier1_dist_amt,v_state_tier1_max_amount) * v_state_use_tier1_rate;
         IF v_state_tier1_max_amount < ABS(v_state_tier1_dist_amt) THEN
            an_state_amount := (v_state_tier1_max_amount * v_state_use_tier1_rate) * SIGN(v_state_tier1_dist_amt);
         ELSE
            an_state_amount := v_state_tier1_dist_amt * v_state_use_tier1_rate;
         END IF;
      END IF;
      -- Tier 2
      IF v_state_use_tier2_rate > 0 THEN
         --an_state_use_tier2_amount := GREATEST(LEAST(v_state_tier2_dist_amt,v_state_tier2_max_amount) * v_state_use_tier2_rate, 0);
         IF v_state_tier2_max_amount < ABS(v_state_tier2_dist_amt) THEN
            an_state_use_tier2_amount := (v_state_tier2_max_amount * v_state_use_tier2_rate) * SIGN(v_state_tier2_dist_amt);
         ELSE
            an_state_use_tier2_amount := v_state_tier2_dist_amt * v_state_use_tier2_rate;
         END IF;
      END IF;
      -- Tier 3
      IF v_state_use_tier3_rate > 0 THEN
         --an_state_use_tier3_amount := GREATEST(v_state_tier3_dist_amt * v_state_use_tier3_rate,0);
         an_state_use_tier3_amount := v_state_tier3_dist_amt * v_state_use_tier3_rate;
      END IF;

      -- Determine MAXTAX Amount --
      IF v_state_tier1_max_amount = 999999999 AND v_state_use_tier2_rate = 0 AND v_state_use_tier3_rate = 0 THEN
         --an_state_amount := LEAST(an_state_amount, v_state_maxtax_amount);
         IF v_state_maxtax_amount > 0 THEN
            IF v_state_maxtax_amount < ABS(an_state_amount) THEN
               an_state_amount := v_state_maxtax_amount * SIGN(an_state_amount);
            END IF;
         END IF;
      END IF;
   END IF;
--DBMS_OUTPUT.Put_Line('State Tier 1 = ' || to_char(an_state_use_amount));
--DBMS_OUTPUT.Put_Line('State Tier 2 = ' || to_char(an_state_use_tier2_amount));
--DBMS_OUTPUT.Put_Line('State Tier 3 = ' || to_char(an_state_use_tier3_amount));

   /*  Calculate County Taxes  ************************************************/
   IF ac_county_flag = '1' THEN
      -- Calculate County Sales/Use Tax Amount
      IF an_county_split_amount > 0 THEN
         IF an_county_split_amount < ABS(an_taxable_amt) THEN
            an_county_amount := (an_county_split_amount * an_county_rate) * SIGN(an_taxable_amt);
         ELSE
            an_county_amount := an_taxable_amt * an_county_rate;
         END IF;
      ELSE
         an_county_amount := an_taxable_amt * an_county_rate;
      END IF;

      -- Determine MAXTAX Amount --
      IF an_county_maxtax_amount > 0 THEN
         IF an_county_maxtax_amount < ABS(an_county_amount) THEN
            an_county_amount := an_county_maxtax_amount * SIGN(an_county_amount);
         END IF;
      END IF;
   END IF;

   /*  Calculate County Local Taxes  ******************************************/
   IF ac_county_local_flag = '1' THEN
      -- Calculate County Local Sales/Use Tax Amount
      an_county_local_amount := an_taxable_amt * an_county_local_rate;
   END IF;

   /*  Calculate City Taxes  **************************************************/
   IF ac_city_flag = '1' THEN
      -- Calculate City Sales/Use Tax Amount
      IF an_city_split_amount > 0 THEN
         IF an_city_split_amount < ABS(an_taxable_amt) THEN
            IF an_city_split_rate > 0 THEN
               an_city_amount :=
                  ROUND((an_city_split_amount * an_city_rate) +
                     ((ABS(an_taxable_amt) - an_city_split_amount) * an_city_split_rate),2) * SIGN(an_taxable_amt);
            ELSE
               an_city_amount := (an_city_split_amount * an_city_rate) * SIGN(an_taxable_amt);
            END IF;
         ELSE
            an_city_amount := an_taxable_amt * an_city_rate;
         END IF;
      ELSE
         an_city_amount := an_taxable_amt * an_city_rate;
      END IF;
   END IF;

   /*  Calculate City Local Taxes  ********************************************/
   IF ac_city_local_flag = '1' THEN
      -- Calculate City Local Sales/Use Tax Amount
      an_city_local_amount := an_taxable_amt * an_city_local_rate;
   END IF;

   /*  Calculate Total Taxes  *************************************************/
   -- Calculate Total Tax Amount
   an_tb_calc_tax_amt := an_country_amount +
                         an_state_amount +
                         an_state_use_tier2_amount +
                         an_state_use_tier3_amount +
                         an_county_amount +
                         an_county_local_amount +
                         an_city_amount +
                         an_city_local_amount;
END;
/
