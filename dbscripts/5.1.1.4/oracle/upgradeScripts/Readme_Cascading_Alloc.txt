------------------------------------------------------
-- Upgrade Scripts for Federal Tax and Cascading Allocation
------------------------------------------------------

Alter_1165_tb_driver_names.sql
Create_1165_SQ_TB_TAX_ALLOC_MATRIX_DTL_ID.sql
Create_1165_SQ_TB_TAX_ALLOC_MATRIX_ID.sql
Create_1165_tb_jur_alloc_matrix_detail.sql
Create_1165_TB_TAX_ALLOC_MATRIX.sql
Create_1165_SQ_TB_SPLIT_SUBTRANS_ID.sql
Create_Split_List_Codes.sql
Create_1165_TB_TAX_ALLOC_MATRIX_DETAIL.SQL
Alter_1165_tb_bcp_transactions.sql
Alter_1165_tb_transaction_detail.sql
Alter_1165_TB_ARCHIVE_TRANSACTION_DTL.sql
Alter_1165_TB_RESTORE_TRANSACTION_DTL.sql
Update_TB_DATA_DEF_COLUMN_1359.sql

1_matrix_record_pkg.sql
2_sp_calcusetax.sql
3_sp_getusetax.sql
4_sp_batch_process.sql
5_sp_batch_taxrate_update.sql
6_sp_import_allocation.sql
7_sp_tb_jurisdiction_taxrate_a_i.sql
8_sp_tb_location_matrix_a_i.sql
9_sp_tb_tax_matrix_a_i.sql
10_sp_batch.sql
11_sp_batch_juris_allocation.sql
12_sp_batch_tax_allocation.sql
13_sp_transaction_juris_alloc.sql
14_sp_transaction_process.sql