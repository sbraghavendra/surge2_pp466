create or replace
PROCEDURE sp_max_all
/* ************************************************************************************************/
/* Object Type/Name: Stored Procedure - sp_max_sequence                                           */
/* Author:           Michael B. Fuller                                                            */
/* Date:             08/31/2011                                                                   */
/* Description:      Reset all Sequences                                                          */
/* Arguments:        None                                                                         */
/* Returns:          None                                                                         */
/* ************************************************************************************************/
/* Maintenance History:                                                                           */
/* Scan  Author       Date     Release    Description                                  Ticket #   */
/* ----- ---------- ---------- ---------- -------------------------------------------- ---------- */
/* ************************************************************************************************/
IS
   vn_max_id                       NUMBER                                            := 0;
   vn_count                        NUMBER                                            := 0;
BEGIN
   -- sq_gl_extract_batch_id --> tb_gl_report_log
   SELECT MAX(gl_extract_batch_no)
     INTO vn_max_id
     FROM tb_gl_report_log;
   IF vn_max_id IS NULL THEN
      vn_max_id := 1;
   ELSE
      vn_max_id := vn_max_id + 1;
   END IF;
   SELECT count(*)
     INTO vn_count
     FROM all_sequences
    WHERE sequence_name = 'SQ_GL_EXTRACT_BATCH_ID'
      AND sequence_owner = 'STSCORP';
   IF vn_count > 0 THEN
      EXECUTE IMMEDIATE 'DROP SEQUENCE STSCORP.sq_gl_extract_batch_id';
   END IF;
   EXECUTE IMMEDIATE 'CREATE SEQUENCE STSCORP.sq_gl_extract_batch_id INCREMENT BY 1 START WITH ' ||
      vn_max_id || ' MAXVALUE 1.0E27 MINVALUE 1 NOCYCLE NOCACHE NOORDER';
   EXECUTE IMMEDIATE 'GRANT SELECT ON STSCORP.sq_gl_extract_batch_id TO STSUSER';

   -- sq_tb_allocation_matrix_id --> tb_allocation_matrix
   SELECT MAX(allocation_matrix_id)
     INTO vn_max_id
     FROM tb_allocation_matrix;
   IF vn_max_id IS NULL THEN
      vn_max_id := 1;
   ELSE
      vn_max_id := vn_max_id + 1;
   END IF;
   SELECT count(*)
     INTO vn_count
     FROM all_sequences
    WHERE sequence_name = 'SQ_TB_ALLOCATION_MATRIX_ID'
      AND sequence_owner = 'STSCORP';
   IF vn_count > 0 THEN
      EXECUTE IMMEDIATE 'DROP SEQUENCE STSCORP.sq_tb_allocation_matrix_id';
   END IF;
   EXECUTE IMMEDIATE 'CREATE SEQUENCE STSCORP.sq_tb_allocation_matrix_id INCREMENT BY 1 START WITH ' ||
      vn_max_id || ' MAXVALUE 1.0E27 MINVALUE 1 NOCYCLE NOCACHE NOORDER';
   EXECUTE IMMEDIATE 'GRANT SELECT ON STSCORP.sq_tb_allocation_matrix_id TO STSUSER';

   -- sq_tb_allocation_subtrans_id --> tb_transaction_detail
   SELECT MAX(allocation_subtrans_id)
     INTO vn_max_id
     FROM tb_transaction_detail;
   IF vn_max_id IS NULL THEN
      vn_max_id := 1;
   ELSE
      vn_max_id := vn_max_id + 1;
   END IF;
   SELECT count(*)
     INTO vn_count
     FROM all_sequences
    WHERE sequence_name = 'SQ_TB_ALLOCATION_SUBTRANS_ID'
      AND sequence_owner = 'STSCORP';
   IF vn_count > 0 THEN
      EXECUTE IMMEDIATE 'DROP SEQUENCE STSCORP.sq_tb_allocation_subtrans_id';
   END IF;
   EXECUTE IMMEDIATE 'CREATE SEQUENCE STSCORP.sq_tb_allocation_subtrans_id INCREMENT BY 1 START WITH ' ||
      vn_max_id || ' MAXVALUE 1.0E27 MINVALUE 1 NOCYCLE NOCACHE NOORDER';
   EXECUTE IMMEDIATE 'GRANT SELECT ON STSCORP.sq_tb_allocation_subtrans_id TO STSUSER';

   -- sq_tb_batch_error_log_id --> tb_batch_error_log
   SELECT MAX(batch_error_log_id)
     INTO vn_max_id
     FROM tb_batch_error_log;
   IF vn_max_id IS NULL THEN
      vn_max_id := 1;
   ELSE
      vn_max_id := vn_max_id + 1;
   END IF;
   SELECT count(*)
     INTO vn_count
     FROM all_sequences
    WHERE sequence_name = 'SQ_TB_BATCH_ERROR_LOG_ID'
      AND sequence_owner = 'STSCORP';
   IF vn_count > 0 THEN
      EXECUTE IMMEDIATE 'DROP SEQUENCE STSCORP.sq_tb_batch_error_log_id';
   END IF;
   EXECUTE IMMEDIATE 'CREATE SEQUENCE STSCORP.sq_tb_batch_error_log_id INCREMENT BY 1 START WITH ' ||
      vn_max_id || ' MAXVALUE 1.0E27 MINVALUE 1 NOCYCLE NOCACHE NOORDER';
   EXECUTE IMMEDIATE 'GRANT SELECT ON STSCORP.sq_tb_batch_error_log_id TO STSUSER';

   -- sq_tb_batch_id --> tb_batch
   SELECT MAX(batch_id)
     INTO vn_max_id
     FROM tb_batch;
   IF vn_max_id IS NULL THEN
      vn_max_id := 1;
   ELSE
      vn_max_id := vn_max_id + 1;
   END IF;
   SELECT count(*)
     INTO vn_count
     FROM all_sequences
    WHERE sequence_name = 'SQ_TB_BATCH_ID'
      AND sequence_owner = 'STSCORP';
   IF vn_count > 0 THEN
      EXECUTE IMMEDIATE 'DROP SEQUENCE STSCORP.sq_tb_batch_id';
   END IF;
   EXECUTE IMMEDIATE 'CREATE SEQUENCE STSCORP.sq_tb_batch_id INCREMENT BY 1 START WITH ' ||
      vn_max_id || ' MAXVALUE 1.0E27 MINVALUE 1 NOCYCLE NOCACHE NOORDER';
   EXECUTE IMMEDIATE 'GRANT SELECT ON STSCORP.sq_tb_batch_id TO STSUSER';	

   -- sq_tb_bcp_juris_taxrate_id --> tb_bcp_jurisdiction_taxrate
   SELECT MAX(bcp_jurisdiction_taxrate_id)
     INTO vn_max_id
     FROM tb_bcp_jurisdiction_taxrate;
   IF vn_max_id IS NULL THEN
      vn_max_id := 1;
   ELSE
      vn_max_id := vn_max_id + 1;
   END IF;
   SELECT count(*)
     INTO vn_count
     FROM all_sequences
    WHERE sequence_name = 'SQ_TB_BCP_JURIS_TAXRATE_ID'
      AND sequence_owner = 'STSCORP';
   IF vn_count > 0 THEN
      EXECUTE IMMEDIATE 'DROP SEQUENCE STSCORP.sq_tb_bcp_juris_taxrate_id';
   END IF;
   EXECUTE IMMEDIATE 'CREATE SEQUENCE STSCORP.sq_tb_bcp_juris_taxrate_id INCREMENT BY 1 START WITH ' ||
      vn_max_id || ' MAXVALUE 1.0E27 MINVALUE 1 NOCYCLE NOCACHE NOORDER';
   EXECUTE IMMEDIATE 'GRANT SELECT ON STSCORP.sq_tb_bcp_juris_taxrate_id TO STSUSER';

   -- sq_tb_bcp_taxcode_rule_id --> tb_bcp_taxcode_rule
   SELECT MAX(bcp_taxcode_rule_id)
     INTO vn_max_id
     FROM tb_bcp_taxcode_rules;
   IF vn_max_id IS NULL THEN
      vn_max_id := 1;
   ELSE
      vn_max_id := vn_max_id + 1;
   END IF;
   SELECT count(*)
     INTO vn_count
     FROM all_sequences
    WHERE sequence_name = 'SQ_TB_BCP_TAXCODE_RULE_ID'
      AND sequence_owner = 'STSCORP';
   IF vn_count > 0 THEN
      EXECUTE IMMEDIATE 'DROP SEQUENCE STSCORP.sq_tb_bcp_taxcode_rule_id';
   END IF;
   EXECUTE IMMEDIATE 'CREATE SEQUENCE STSCORP.sq_tb_bcp_taxcode_rule_id INCREMENT BY 1 START WITH ' ||
      vn_max_id || ' MAXVALUE 1.0E27 MINVALUE 1 NOCYCLE NOCACHE NOORDER';
   EXECUTE IMMEDIATE 'GRANT SELECT ON STSCORP.sq_tb_bcp_taxcode_rule_id TO STSUSER';

   -- sq_tb_bcp_transactions --> tb_bcp_transactions
   SELECT MAX(bcp_transaction_id)
     INTO vn_max_id
     FROM tb_bcp_transactions;
   IF vn_max_id IS NULL THEN
      vn_max_id := 1;
   ELSE
      vn_max_id := vn_max_id + 1;
   END IF;
   SELECT count(*)
     INTO vn_count
     FROM all_sequences
    WHERE sequence_name = 'SQ_TB_BCP_TRANSACTIONS'
      AND sequence_owner = 'STSCORP';
   IF vn_count > 0 THEN
      EXECUTE IMMEDIATE 'DROP SEQUENCE STSCORP.sq_tb_bcp_transactions';
   END IF;
   EXECUTE IMMEDIATE 'CREATE SEQUENCE STSCORP.sq_tb_bcp_transactions INCREMENT BY 1 START WITH ' ||
      vn_max_id || ' MAXVALUE 1.0E27 MINVALUE 1 NOCYCLE NOCACHE NOORDER';
   EXECUTE IMMEDIATE 'GRANT SELECT ON STSCORP.sq_tb_bcp_transactions TO STSUSER';

   -- sq_tb_entity_id --> tb_entity
   SELECT MAX(entity_id)
     INTO vn_max_id
     FROM tb_entity;
   IF vn_max_id IS NULL THEN
      vn_max_id := 1;
   ELSE
      vn_max_id := vn_max_id + 1;
   END IF;
   SELECT count(*)
     INTO vn_count
     FROM all_sequences
    WHERE sequence_name = 'SQ_TB_ENTITY_ID'
      AND sequence_owner = 'STSCORP';
   IF vn_count > 0 THEN
      EXECUTE IMMEDIATE 'DROP SEQUENCE STSCORP.sq_tb_entity_id';
   END IF;
   EXECUTE IMMEDIATE 'CREATE SEQUENCE STSCORP.sq_tb_entity_id INCREMENT BY 1 START WITH ' ||
      vn_max_id || ' MAXVALUE 1.0E27 MINVALUE 1 NOCYCLE NOCACHE NOORDER';
   EXECUTE IMMEDIATE 'GRANT SELECT ON STSCORP.sq_tb_entity_id TO STSUSER';

   -- sq_tb_gl_extract_map_id --> tb_gl_extract_map
   SELECT MAX(gl_extract_map_id)
     INTO vn_max_id
     FROM tb_gl_extract_map;
   IF vn_max_id IS NULL THEN
      vn_max_id := 1;
   ELSE
      vn_max_id := vn_max_id + 1;
   END IF;
   SELECT count(*)
     INTO vn_count
     FROM all_sequences
    WHERE sequence_name = 'SQ_TB_GL_EXTRACT_MAP_ID'
      AND sequence_owner = 'STSCORP';
   IF vn_count > 0 THEN
      EXECUTE IMMEDIATE 'DROP SEQUENCE STSCORP.sq_tb_gl_extract_map_id';
   END IF;
   EXECUTE IMMEDIATE 'CREATE SEQUENCE STSCORP.sq_tb_gl_extract_map_id INCREMENT BY 1 START WITH ' ||
      vn_max_id || ' MAXVALUE 1.0E27 MINVALUE 1 NOCYCLE NOCACHE NOORDER';
   EXECUTE IMMEDIATE 'GRANT SELECT ON STSCORP.sq_tb_gl_extract_map_id TO STSUSER';

   -- sq_tb_jurisdiction_id --> tb_jurisdiction
   SELECT MAX(jurisdiction_id)
     INTO vn_max_id
     FROM tb_jurisdiction;
   IF vn_max_id IS NULL THEN
      vn_max_id := 1;
   ELSE
      vn_max_id := vn_max_id + 1;
   END IF;
   SELECT count(*)
     INTO vn_count
     FROM all_sequences
    WHERE sequence_name = 'SQ_TB_JURISDICTION_ID'
      AND sequence_owner = 'STSCORP';
   IF vn_count > 0 THEN
      EXECUTE IMMEDIATE 'DROP SEQUENCE STSCORP.sq_tb_jurisdiction_id';
   END IF;
   EXECUTE IMMEDIATE 'CREATE SEQUENCE STSCORP.sq_tb_jurisdiction_id INCREMENT BY 1 START WITH ' ||
      vn_max_id || ' MAXVALUE 1.0E27 MINVALUE 1 NOCYCLE NOCACHE NOORDER';
   EXECUTE IMMEDIATE 'GRANT SELECT ON STSCORP.sq_tb_jurisdiction_id TO STSUSER';

   -- sq_tb_jurisdiction_taxrate_id --> tb_jurisdiction_taxrate
   SELECT MAX(jurisdiction_taxrate_id)
     INTO vn_max_id
     FROM tb_jurisdiction_taxrate;
   IF vn_max_id IS NULL THEN
      vn_max_id := 1;
   ELSE
      vn_max_id := vn_max_id + 1;
   END IF;
   SELECT count(*)
     INTO vn_count
     FROM all_sequences
    WHERE sequence_name = 'SQ_TB_JURISDICTION_TAXRATE_ID'
      AND sequence_owner = 'STSCORP';
   IF vn_count > 0 THEN
      EXECUTE IMMEDIATE 'DROP SEQUENCE STSCORP.sq_tb_jurisdiction_taxrate_id';
   END IF;
   EXECUTE IMMEDIATE 'CREATE SEQUENCE STSCORP.sq_tb_jurisdiction_taxrate_id INCREMENT BY 1 START WITH ' ||
      vn_max_id || ' MAXVALUE 1.0E27 MINVALUE 1 NOCYCLE NOCACHE NOORDER';
   EXECUTE IMMEDIATE 'GRANT SELECT ON STSCORP.sq_tb_jurisdiction_taxrate_id TO STSUSER';

   -- sq_tb_location_matrix_id --> tb_location_matrix
   SELECT MAX( location_matrix_id )
     INTO vn_max_id
     FROM tb_location_matrix;
   IF vn_max_id IS NULL THEN
      vn_max_id := 1;
   ELSE
      vn_max_id := vn_max_id + 1;
   END IF;
   SELECT count(*)
     INTO vn_count
     FROM all_sequences
    WHERE sequence_name = 'SQ_TB_LOCATION_MATRIX_ID'
      AND sequence_owner = 'STSCORP';
   IF vn_count > 0 THEN
      EXECUTE IMMEDIATE 'DROP SEQUENCE STSCORP.sq_tb_location_matrix_id';
   END IF;
   EXECUTE IMMEDIATE 'CREATE SEQUENCE STSCORP.sq_tb_location_matrix_id INCREMENT BY 1 START WITH ' ||
      vn_max_id || ' MAXVALUE 1.0E27 MINVALUE 1 NOCYCLE NOCACHE NOORDER';
   EXECUTE IMMEDIATE 'GRANT SELECT ON STSCORP.sq_tb_location_matrix_id TO STSUSER';

   -- sq_tb_split_subtrans_id --> tb_transaction_detail
   SELECT MAX(split_subtrans_id)
     INTO vn_max_id
     FROM tb_transaction_detail;
   IF vn_max_id IS NULL THEN
      vn_max_id := 1;
   ELSE
      vn_max_id := vn_max_id + 1;
   END IF;
   SELECT count(*)
     INTO vn_count
     FROM all_sequences
    WHERE sequence_name = 'SQ_TB_SPLIT_SUBTRANS_ID'
      AND sequence_owner = 'STSCORP';
   IF vn_count > 0 THEN
      EXECUTE IMMEDIATE 'DROP SEQUENCE STSCORP.sq_tb_split_subtrans_id';
   END IF;
   EXECUTE IMMEDIATE 'CREATE SEQUENCE STSCORP.sq_tb_split_subtrans_id INCREMENT BY 1 START WITH ' ||
      vn_max_id || ' MAXVALUE 1.0E27 MINVALUE 1 NOCYCLE NOCACHE NOORDER';
   EXECUTE IMMEDIATE 'GRANT SELECT ON STSCORP.sq_tb_split_subtrans_id TO STSUSER';

   -- sq_tb_tax_alloc_matrix_dtl_id --> tb_tax_alloc_matrix_detail
   SELECT MAX(tax_alloc_matrix_dtl_id)
     INTO vn_max_id
     FROM tb_tax_alloc_matrix_detail;
   IF vn_max_id IS NULL THEN
      vn_max_id := 1;
   ELSE
      vn_max_id := vn_max_id + 1;
   END IF;
   SELECT count(*)
     INTO vn_count
     FROM all_sequences
    WHERE sequence_name = 'SQ_TB_TAX_ALLOC_MATRIX_DTL_ID'
      AND sequence_owner = 'STSCORP';
   IF vn_count > 0 THEN
      EXECUTE IMMEDIATE 'DROP SEQUENCE STSCORP.sq_tb_tax_alloc_matrix_dtl_id';
   END IF;
   EXECUTE IMMEDIATE 'CREATE SEQUENCE STSCORP.sq_tb_tax_alloc_matrix_dtl_id INCREMENT BY 1 START WITH ' ||
      vn_max_id || ' MAXVALUE 1.0E27 MINVALUE 1 NOCYCLE NOCACHE NOORDER';
   EXECUTE IMMEDIATE 'GRANT SELECT ON STSCORP.sq_tb_tax_alloc_matrix_dtl_id TO STSUSER';

   -- sq_tb_tax_alloc_matrix_id --> tb_tax_alloc_matrix
   SELECT MAX(tax_alloc_matrix_id)
     INTO vn_max_id
     FROM tb_tax_alloc_matrix;
   IF vn_max_id IS NULL THEN
      vn_max_id := 1;
   ELSE
      vn_max_id := vn_max_id + 1;
   END IF;
   SELECT count(*)
     INTO vn_count
     FROM all_sequences
    WHERE sequence_name = 'SQ_TB_TAX_ALLOC_MATRIX_ID'
      AND sequence_owner = 'STSCORP';
   IF vn_count > 0 THEN
      EXECUTE IMMEDIATE 'DROP SEQUENCE STSCORP.sq_tb_tax_alloc_matrix_id';
   END IF;
   EXECUTE IMMEDIATE 'CREATE SEQUENCE STSCORP.sq_tb_tax_alloc_matrix_id INCREMENT BY 1 START WITH ' ||
      vn_max_id || ' MAXVALUE 1.0E27 MINVALUE 1 NOCYCLE NOCACHE NOORDER';
   EXECUTE IMMEDIATE 'GRANT SELECT ON STSCORP.sq_tb_tax_alloc_matrix_id TO STSUSER';

   -- sq_tb_tax_matrix_id --> tb_tax_matrix
   SELECT MAX(tax_matrix_id)
     INTO vn_max_id
     FROM tb_tax_matrix;
   IF vn_max_id IS NULL THEN
      vn_max_id := 1;
   ELSE
      vn_max_id := vn_max_id + 1;
   END IF;
   SELECT count(*)
     INTO vn_count
     FROM all_sequences
    WHERE sequence_name = 'SQ_TB_TAX_MATRIX_ID'
      AND sequence_owner = 'STSCORP';
   IF vn_count > 0 THEN
      EXECUTE IMMEDIATE 'DROP SEQUENCE STSCORP.sq_tb_tax_matrix_id';
   END IF;
   EXECUTE IMMEDIATE 'CREATE SEQUENCE STSCORP.sq_tb_tax_matrix_id INCREMENT BY 1 START WITH ' ||
      vn_max_id || ' MAXVALUE 1.0E27 MINVALUE 1 NOCYCLE NOCACHE NOORDER';
   EXECUTE IMMEDIATE 'GRANT SELECT ON STSCORP.sq_tb_tax_matrix_id TO STSUSER';

   -- sq_tb_taxcode_detail_id --> tb_taxcode_detail
   SELECT MAX(taxcode_detail_id)
     INTO vn_max_id
     FROM tb_taxcode_detail;
   IF vn_max_id IS NULL THEN
      vn_max_id := 1;
   ELSE
      vn_max_id := vn_max_id + 1;
   END IF;
   SELECT count(*)
     INTO vn_count
     FROM all_sequences
    WHERE sequence_name = 'SQ_TB_TAXCODE_DETAIL_ID'
      AND sequence_owner = 'STSCORP';
   IF vn_count > 0 THEN
      EXECUTE IMMEDIATE 'DROP SEQUENCE STSCORP.sq_tb_taxcode_detail_id';
   END IF;
   EXECUTE IMMEDIATE 'CREATE SEQUENCE STSCORP.sq_tb_taxcode_detail_id INCREMENT BY 1 START WITH ' ||
      vn_max_id || ' MAXVALUE 1.0E27 MINVALUE 1 NOCYCLE NOCACHE NOORDER';
   EXECUTE IMMEDIATE 'GRANT SELECT ON STSCORP.sq_tb_taxcode_detail_id TO STSUSER';

   -- sq_tb_trans_detail_excp_id --> tb_transaction_detail_excp
   SELECT MAX(transaction_detail_excp_id)
     INTO vn_max_id
     FROM tb_transaction_detail_excp;
   IF vn_max_id IS NULL THEN
      vn_max_id := 1;
   ELSE
      vn_max_id := vn_max_id + 1;
   END IF;
   SELECT count(*)
     INTO vn_count
     FROM all_sequences
    WHERE sequence_name = 'SQ_TB_TRANS_DETAIL_EXCP_ID'
      AND sequence_owner = 'STSCORP';
   IF vn_count > 0 THEN
      EXECUTE IMMEDIATE 'DROP SEQUENCE STSCORP.sq_tb_trans_detail_excp_id';
   END IF;
   EXECUTE IMMEDIATE 'CREATE SEQUENCE STSCORP.sq_tb_trans_detail_excp_id INCREMENT BY 1 START WITH ' ||
      vn_max_id || ' MAXVALUE 1.0E27 MINVALUE 1 NOCYCLE CACHE 1000 NOORDER';
   EXECUTE IMMEDIATE 'GRANT SELECT ON STSCORP.sq_tb_trans_detail_excp_id TO STSUSER';

   -- sq_tb_transaction_detail_id --> tb_transaction_detail
   SELECT MAX(transaction_detail_id)
     INTO vn_max_id
     FROM tb_transaction_detail;
   IF vn_max_id IS NULL THEN
      vn_max_id := 1;
   ELSE
      vn_max_id := vn_max_id + 1;
   END IF;
   SELECT count(*)
     INTO vn_count
     FROM all_sequences
    WHERE sequence_name = 'SQ_TB_TRANSACTION_DETAIL_ID'
      AND sequence_owner = 'STSCORP';
   IF vn_count > 0 THEN
      EXECUTE IMMEDIATE 'DROP SEQUENCE STSCORP.sq_tb_transaction_detail_id';
   END IF;
   EXECUTE IMMEDIATE 'CREATE SEQUENCE STSCORP.sq_tb_transaction_detail_id INCREMENT BY 1 START WITH ' ||
      vn_max_id || ' MAXVALUE 1.0E27 MINVALUE 1 NOCYCLE CACHE 1000 NOORDER';
   EXECUTE IMMEDIATE 'GRANT SELECT ON STSCORP.sq_tb_transaction_detail_id TO STSUSER';	
	
	END;
