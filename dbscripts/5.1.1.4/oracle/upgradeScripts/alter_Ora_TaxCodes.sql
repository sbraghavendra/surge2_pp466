/* ***  Alter v5.1.1.1 to v5.2.1.1  *** */

set define off
SPOOL alter_taxcodes.log

-- Drop tables
DROP TABLE tb_cch_code;
DROP TABLE tb_cch_txmatrix;
DROP TABLE tb_tmp_transaction_detail_b_u;

-- Drop constraints
ALTER TABLE tb_gl_extract_map DROP CONSTRAINT tb_gl_extract_map_fk01;
ALTER TABLE tb_gl_extract_map DROP CONSTRAINT tb_gl_extract_map_fk02;
ALTER TABLE tb_jurisdiction DROP CONSTRAINT tb_jurisdiction_fk01;
ALTER TABLE tb_taxcode_state DROP PRIMARY KEY CASCADE;
ALTER TABLE tb_taxcode DROP PRIMARY KEY CASCADE;
ALTER TABLE tb_taxcode_detail DROP PRIMARY KEY CASCADE;
ALTER TABLE tb_taxcode_detail DROP CONSTRAINT tb_taxcode_detail_fk01;
ALTER TABLE tb_taxcode_detail DROP CONSTRAINT tb_taxcode_detail_fk02;
ALTER TABLE tb_taxcode_detail DROP CONSTRAINT tb_taxcode_detail_fk03;
ALTER TABLE tb_tax_matrix DROP CONSTRAINT pk_tb_tax_matrix;
ALTER TABLE tb_tax_matrix DROP CONSTRAINT tb_tax_matrix_fk01;
ALTER TABLE tb_tax_matrix DROP CONSTRAINT tb_tax_matrix_fk02;
ALTER TABLE tb_tax_matrix DROP CONSTRAINT tb_tax_matrix_fk03;

-- Drop indexes
DROP INDEX pk_tb_taxcode_state;
DROP INDEX pk_tb_taxcode;
DROP INDEX pk_tb_taxcode_detail;
DROP INDEX pk_tb_tax_matrix;

-- Drop unused procedures
DROP PROCEDURE sp_batch_archive;
DROP PROCEDURE sp_batch_archive_delete;
DROP PROCEDURE sp_batch_archive_restore;
DROP PROCEDURE sp_cch_txmatrix_a_d;
DROP PROCEDURE sp_tb_taxcode_detail_b_iu;
DROP PROCEDURE sp_tb_trans_detail_excp_b_iu;
DROP PROCEDURE sp_maxgl_extract_batch_id;
DROP PROCEDURE sp_max_tb_allocation_matrix_id;
DROP PROCEDURE sp_max_tb_batch_error_log_id;
DROP PROCEDURE sp_max_tb_batch_id;
DROP PROCEDURE sp_max_tb_bcp_juris_taxrate_id;
DROP PROCEDURE sp_max_tb_entity_id;
DROP PROCEDURE sp_max_tb_gl_extract_map_id;
DROP PROCEDURE sp_max_tb_juris_taxrate_id;
DROP PROCEDURE sp_max_tb_;jurisdiction_id;
DROP PROCEDURE sp_max_tb_location_matrix_id;
DROP PROCEDURE sp_max_tb_tax_matrix_id;
DROP PROCEDURE sp_max_tb_taxcode_detail_id;
DROP PROCEDURE sp_max_tb_trans_detail_excp_id;
DROP PROCEDURE sp_max_tb_trans_detail_id;

-- tb_taxcode_state
RENAME tb_taxcode_state TO v511_taxcode_state;
CREATE TABLE tb_taxcode_state (
   taxcode_country_code            VARCHAR2(10)            NOT NULL,
   taxcode_state_code              VARCHAR2(10)            NOT NULL,
   geocode                         VARCHAR2(12),
   name                            VARCHAR2(50),
   local_taxability_code           VARCHAR2(10),
   active_flag                     CHAR(1),
   custom_flag                     CHAR(1),
   modified_flag                   CHAR(1),
   update_user_id                  VARCHAR2(40)            DEFAULT USER,
   update_timestamp                DATE                    DEFAULT SYS_EXTRACT_UTC(SYSTIMESTAMP),
   CONSTRAINT pk_tb_taxcode_state PRIMARY KEY (taxcode_country_code, taxcode_state_code));

INSERT INTO tb_taxcode_state (
   taxcode_country_code,
   taxcode_state_code,
   geocode,
   name,
   local_taxability_code,
   active_flag,
   custom_flag,
   modified_flag,
   update_user_id,
   update_timestamp)
SELECT
   DECODE(upper(country),'USA','US','CANADA','CA',substr(country,1,10)),
   taxcode_state_code,
   geocode,
   name,
   'S',
   active_flag,
   '0',
   '0',
   update_user_id,
   update_timestamp
  FROM v511_taxcode_state;


-- tb_taxcode
RENAME tb_taxcode TO v511_taxcode;
CREATE TABLE tb_taxcode (
   taxcode_code                    VARCHAR2(40)            NOT NULL,
   description                     VARCHAR2(120),
   comments                        VARCHAR2(255),
   erp_taxcode                     VARCHAR2(40),
   active_flag                     CHAR(1),
   custom_flag                     CHAR(1),
   modified_flag                   CHAR(1),
   update_user_id                  VARCHAR2(40)            DEFAULT USER,
   update_timestamp                DATE                    DEFAULT SYS_EXTRACT_UTC(SYSTIMESTAMP),
   CONSTRAINT pk_tb_taxcode PRIMARY KEY (taxcode_code));

INSERT INTO tb_taxcode (
   taxcode_code,
   description,
   comments,
   erp_taxcode,
   active_flag,
   custom_flag,
   modified_flag,
   update_user_id,
   update_timestamp)
SELECT
   DECODE((SELECT count(*) FROM v511_taxcode B WHERE B.taxcode_code=A.taxcode_code),1,taxcode_code,taxcode_type_code || substr(taxcode_code,1,39)),
   description,
   comments,
   erp_taxcode,
   '1',
   '0',
   '0',
   update_user_id,
   update_timestamp
  FROM v511_taxcode A;

-- tb_taxcode_detail
RENAME tb_taxcode_detail TO v511_taxcode_detail;
CREATE TABLE tb_taxcode_detail (
   taxcode_detail_id               NUMBER                  NOT NULL,
   taxcode_code                    VARCHAR2(40),
   taxcode_country_code            VARCHAR2(10),
   taxcode_state_code              VARCHAR2(10),
   taxcode_county                  VARCHAR2(50),
   taxcode_city                    VARCHAR2(50),
   effective_date                  DATE,
   expiration_date                 DATE,
   taxcode_type_code               VARCHAR2(10),
   override_taxtype_code           VARCHAR2(10),
   ratetype_code                   VARCHAR2(10),
   taxable_threshold_amt           NUMBER,
   tax_limitation_amt              NUMBER,
   cap_amt                         NUMBER,
   base_change_pct                 NUMBER,
   special_rate                    NUMBER,
   active_flag                     CHAR(1),
   custom_flag                     CHAR(1),
   modified_flag                   CHAR(1),
   jurisdiction_id                 NUMBER,
   update_user_id                  VARCHAR2(40)            DEFAULT USER,
   update_timestamp                DATE                    DEFAULT SYS_EXTRACT_UTC(SYSTIMESTAMP),
   CONSTRAINT pk_tb_taxcode_detail PRIMARY KEY (taxcode_detail_id));

INSERT INTO tb_taxcode_detail (
   taxcode_detail_id,
   taxcode_code,
   taxcode_country_code,
   taxcode_state_code,
   taxcode_county,
   taxcode_city,
   effective_date,
   expiration_date,
   taxcode_type_code,
   override_taxtype_code,
   ratetype_code,
   taxable_threshold_amt,
   tax_limitation_amt,
   cap_amt,
   base_change_pct,
   special_rate,
   active_flag,
   custom_flag,
   modified_flag,
   jurisdiction_id,
   update_user_id,
   update_timestamp)
SELECT
   taxcode_detail_id,
   DECODE((SELECT count(*) FROM v511_taxcode B WHERE B.taxcode_code=A.taxcode_code),1,taxcode_code,taxcode_type_code || substr(taxcode_code,1,39)) taxcode_code,
   (SELECT taxcode_country_code FROM tb_taxcode_state B WHERE A.taxcode_state_code=B.taxcode_state_code) taxcode_country_code,
   --DECODE(taxcode_state_code,'CN','CA','US') taxcode_country_code,
   taxcode_state_code,
   '*ALL' taxcode_county,
   '*ALL' taxcode_city,
   to_date('01/01/1970','mm/dd/yyyy') effective_date,
   to_date('12/31/9999','mm/dd/yyyy') expiration_date,
   taxcode_type_code,
   DECODE(taxcode_type_code,'T',taxtype_code,NULL) override_taxtype_code,
   DECODE(taxcode_type_code,'T',measure_type_code,NULL) ratetype_code,
   DECODE(taxcode_type_code,'T',0,NULL) taxable_threshold_amt,
   DECODE(taxcode_type_code,'T',0,NULL) tax_limitation_amt,
   DECODE(taxcode_type_code,'T',0,NULL) cap_amt,
   DECODE(taxcode_type_code,'T',pct_dist_amt,NULL) base_change_pct,
   DECODE(taxcode_type_code,'T',0,NULL) special_rate,
   '1' active_flag,
   '0' custom_flag,
   '0' modified_flag,
   jurisdiction_id,
   update_user_id,
   update_timestamp
  FROM v511_taxcode_detail A;


CREATE TABLE kill_me AS
   SELECT taxcode_detail_id, min(gl_date) effective_date
     FROM tb_transaction_detail
    WHERE taxcode_detail_id IS NOT NULL
      AND taxcode_detail_id>0
 GROUP BY taxcode_detail_id;

UPDATE tb_taxcode_detail
   SET effective_date = NVL((SELECT effective_date FROM kill_me where tb_taxcode_detail.taxcode_detail_id=kill_me.taxcode_detail_id),to_date('01/01/1970','mm/dd/yyyy'));

DROP TABLE kill_me;

-- Build constraints
-- ALTER TABLE tb_gl_extract_map
--  ADD (CONSTRAINT tb_gl_extract_map_fk01 FOREIGN KEY(taxcode_state_code) REFERENCES tb_taxcode_state(taxcode_state_code),
--       CONSTRAINT tb_gl_extract_map_fk02 FOREIGN KEY(taxcode_code) REFERENCES tb_taxcode(taxcode_code));

ALTER TABLE tb_jurisdiction
  ADD (CONSTRAINT tb_jurisdiction_fk01 FOREIGN KEY(country, state) REFERENCES tb_taxcode_state(taxcode_country_code, taxcode_state_code));

ALTER TABLE tb_taxcode_detail
  ADD (CONSTRAINT tb_taxcode_detail_fk01 FOREIGN KEY(taxcode_country_code, taxcode_state_code) REFERENCES tb_taxcode_state(taxcode_country_code, taxcode_state_code),
       CONSTRAINT tb_taxcode_detail_fk02 FOREIGN KEY(taxcode_code) REFERENCES tb_taxcode(taxcode_code),
       CONSTRAINT tb_taxcode_detail_fk03 FOREIGN KEY(jurisdiction_id) REFERENCES tb_jurisdiction(jurisdiction_id));

-- tb_tax_matrix
RENAME tb_tax_matrix TO v511_tax_matrix;
CREATE TABLE tb_tax_matrix (
   tax_matrix_id                   NUMBER                  NOT NULL,
   driver_global_flag              CHAR(1),
   driver_01                       VARCHAR2(100),
   driver_01_desc                  VARCHAR2(100),
   driver_01_thru                  VARCHAR2(100),
   driver_01_thru_desc             VARCHAR2(100),
   driver_02                       VARCHAR2(100),
   driver_02_desc                  VARCHAR2(100),
   driver_02_thru                  VARCHAR2(100),
   driver_02_thru_desc             VARCHAR2(100),
   driver_03                       VARCHAR2(100),
   driver_03_desc                  VARCHAR2(100),
   driver_03_thru                  VARCHAR2(100),
   driver_03_thru_desc             VARCHAR2(100),
   driver_04                       VARCHAR2(100),
   driver_04_desc                  VARCHAR2(100),
   driver_04_thru                  VARCHAR2(100),
   driver_04_thru_desc             VARCHAR2(100),
   driver_05                       VARCHAR2(100),
   driver_05_desc                  VARCHAR2(100),
   driver_05_thru                  VARCHAR2(100),
   driver_05_thru_desc             VARCHAR2(100),
   driver_06                       VARCHAR2(100),
   driver_06_desc                  VARCHAR2(100),
   driver_06_thru                  VARCHAR2(100),
   driver_06_thru_desc             VARCHAR2(100),
   driver_07                       VARCHAR2(100),
   driver_07_desc                  VARCHAR2(100),
   driver_07_thru                  VARCHAR2(100),
   driver_07_thru_desc             VARCHAR2(100),
   driver_08                       VARCHAR2(100),
   driver_08_desc                  VARCHAR2(100),
   driver_08_thru                  VARCHAR2(100),
   driver_08_thru_desc             VARCHAR2(100),
   driver_09                       VARCHAR2(100),
   driver_09_desc                  VARCHAR2(100),
   driver_09_thru                  VARCHAR2(100),
   driver_09_thru_desc             VARCHAR2(100),
   driver_10                       VARCHAR2(100),
   driver_10_desc                  VARCHAR2(100),
   driver_10_thru                  VARCHAR2(100),
   driver_10_thru_desc             VARCHAR2(100),
   driver_11                       VARCHAR2(100),
   driver_11_desc                  VARCHAR2(100),
   driver_11_thru                  VARCHAR2(100),
   driver_11_thru_desc             VARCHAR2(100),
   driver_12                       VARCHAR2(100),
   driver_12_desc                  VARCHAR2(100),
   driver_12_thru                  VARCHAR2(100),
   driver_12_thru_desc             VARCHAR2(100),
   driver_13                       VARCHAR2(100),
   driver_13_desc                  VARCHAR2(100),
   driver_13_thru                  VARCHAR2(100),
   driver_13_thru_desc             VARCHAR2(100),
   driver_14                       VARCHAR2(100),
   driver_14_desc                  VARCHAR2(100),
   driver_14_thru                  VARCHAR2(100),
   driver_14_thru_desc             VARCHAR2(100),
   driver_15                       VARCHAR2(100),
   driver_15_desc                  VARCHAR2(100),
   driver_15_thru                  VARCHAR2(100),
   driver_15_thru_desc             VARCHAR2(100),
   driver_16                       VARCHAR2(100),
   driver_16_desc                  VARCHAR2(100),
   driver_16_thru                  VARCHAR2(100),
   driver_16_thru_desc             VARCHAR2(100),
   driver_17                       VARCHAR2(100),
   driver_17_desc                  VARCHAR2(100),
   driver_17_thru                  VARCHAR2(100),
   driver_17_thru_desc             VARCHAR2(100),
   driver_18                       VARCHAR2(100),
   driver_18_desc                  VARCHAR2(100),
   driver_18_thru                  VARCHAR2(100),
   driver_18_thru_desc             VARCHAR2(100),
   driver_19                       VARCHAR2(100),
   driver_19_desc                  VARCHAR2(100),
   driver_19_thru                  VARCHAR2(100),
   driver_19_thru_desc             VARCHAR2(100),
   driver_20                       VARCHAR2(100),
   driver_20_desc                  VARCHAR2(100),
   driver_20_thru                  VARCHAR2(100),
   driver_20_thru_desc             VARCHAR2(100),
   driver_21                       VARCHAR2(100),
   driver_21_desc                  VARCHAR2(100),
   driver_21_thru                  VARCHAR2(100),
   driver_21_thru_desc             VARCHAR2(100),
   driver_22                       VARCHAR2(100),
   driver_22_desc                  VARCHAR2(100),
   driver_22_thru                  VARCHAR2(100),
   driver_22_thru_desc             VARCHAR2(100),
   driver_23                       VARCHAR2(100),
   driver_23_desc                  VARCHAR2(100),
   driver_23_thru                  VARCHAR2(100),
   driver_23_thru_desc             VARCHAR2(100),
   driver_24                       VARCHAR2(100),
   driver_24_desc                  VARCHAR2(100),
   driver_24_thru                  VARCHAR2(100),
   driver_24_thru_desc             VARCHAR2(100),
   driver_25                       VARCHAR2(100),
   driver_25_desc                  VARCHAR2(100),
   driver_25_thru                  VARCHAR2(100),
   driver_25_thru_desc             VARCHAR2(100),
   driver_26                       VARCHAR2(100),
   driver_26_desc                  VARCHAR2(100),
   driver_26_thru                  VARCHAR2(100),
   driver_26_thru_desc             VARCHAR2(100),
   driver_27                       VARCHAR2(100),
   driver_27_desc                  VARCHAR2(100),
   driver_27_thru                  VARCHAR2(100),
   driver_27_thru_desc             VARCHAR2(100),
   driver_28                       VARCHAR2(100),
   driver_28_desc                  VARCHAR2(100),
   driver_28_thru                  VARCHAR2(100),
   driver_28_thru_desc             VARCHAR2(100),
   driver_29                       VARCHAR2(100),
   driver_29_desc                  VARCHAR2(100),
   driver_29_thru                  VARCHAR2(100),
   driver_29_thru_desc             VARCHAR2(100),
   driver_30                       VARCHAR2(100),
   driver_30_desc                  VARCHAR2(100),
   driver_30_thru                  VARCHAR2(100),
   driver_30_thru_desc             VARCHAR2(100),
   binary_weight                   NUMBER,
   significant_digits              VARCHAR2(119),
   default_flag                    CHAR(1),
   default_binary_weight           NUMBER,
   default_significant_digits      VARCHAR2(119),
   effective_date                  DATE,
   expiration_date                 DATE,
   relation_sign                   VARCHAR2(3),
   relation_amount                 NUMBER,
   then_hold_code_flag             CHAR(1),
   then_taxcode_code               VARCHAR2(40),
   else_hold_code_flag             CHAR(1),
   else_taxcode_code               VARCHAR2(40),
   comments                        VARCHAR2(255),
   active_flag                     CHAR(1),
   update_user_id                  VARCHAR2(40)            DEFAULT USER,
   update_timestamp                DATE                    DEFAULT SYS_EXTRACT_UTC(SYSTIMESTAMP),
   CONSTRAINT pk_tb_tax_matrix PRIMARY KEY (tax_matrix_id));

ALTER TABLE tb_tax_matrix
   ADD (CONSTRAINT tb_tax_matrix_fk01 FOREIGN KEY(then_taxcode_code) REFERENCES tb_taxcode(taxcode_code),
        CONSTRAINT tb_tax_matrix_fk02 FOREIGN KEY(else_taxcode_code) REFERENCES tb_taxcode(taxcode_code));

-- TEMPORARY -- this will be replaced
INSERT INTO tb_tax_matrix
SELECT
   tm.tax_matrix_id,
   tm.driver_global_flag,
   tm.driver_01,
   tm.driver_01_desc, 
   tm.driver_01_thru,
   tm.driver_01_thru_desc,
   tm.driver_02,
   tm.driver_02_desc, 
   tm.driver_02_thru,
   tm.driver_02_thru_desc,
   tm.driver_03,
   tm.driver_03_desc, 
   tm.driver_03_thru,
   tm.driver_03_thru_desc,
   tm.driver_04,
   tm.driver_04_desc, 
   tm.driver_04_thru,
   tm.driver_04_thru_desc,
   tm.driver_05,
   tm.driver_05_desc, 
   tm.driver_05_thru,
   tm.driver_05_thru_desc,
   tm.driver_06,
   tm.driver_06_desc, 
   tm.driver_06_thru,
   tm.driver_06_thru_desc,
   tm.driver_07,
   tm.driver_07_desc, 
   tm.driver_07_thru,
   tm.driver_07_thru_desc,
   tm.driver_08,
   tm.driver_08_desc, 
   tm.driver_08_thru,
   tm.driver_08_thru_desc,
   tm.driver_09,
   tm.driver_09_desc, 
   tm.driver_09_thru,
   tm.driver_09_thru_desc,
   tm.driver_10,
   tm.driver_10_desc, 
   tm.driver_10_thru,
   tm.driver_10_thru_desc,
   tm.driver_11,
   tm.driver_11_desc, 
   tm.driver_11_thru,
   tm.driver_11_thru_desc,
   tm.driver_12,
   tm.driver_12_desc, 
   tm.driver_12_thru,
   tm.driver_12_thru_desc,
   tm.driver_13,
   tm.driver_13_desc, 
   tm.driver_13_thru,
   tm.driver_13_thru_desc,
   tm.driver_14,
   tm.driver_14_desc, 
   tm.driver_14_thru,
   tm.driver_14_thru_desc,
   tm.driver_15,
   tm.driver_15_desc, 
   tm.driver_15_thru,
   tm.driver_15_thru_desc,
   tm.driver_16,
   tm.driver_16_desc, 
   tm.driver_16_thru,
   tm.driver_16_thru_desc,
   tm.driver_17,
   tm.driver_17_desc, 
   tm.driver_17_thru,
   tm.driver_17_thru_desc,
   tm.driver_18,
   tm.driver_18_desc, 
   tm.driver_18_thru,
   tm.driver_18_thru_desc,
   tm.driver_19,
   tm.driver_19_desc, 
   tm.driver_19_thru,
   tm.driver_19_thru_desc,
   tm.driver_20,
   tm.driver_20_desc, 
   tm.driver_20_thru,
   tm.driver_20_thru_desc,
   tm.driver_21,
   tm.driver_21_desc, 
   tm.driver_21_thru,
   tm.driver_21_thru_desc,
   tm.driver_22,
   tm.driver_22_desc, 
   tm.driver_22_thru,
   tm.driver_22_thru_desc,
   tm.driver_23,
   tm.driver_23_desc, 
   tm.driver_23_thru,
   tm.driver_23_thru_desc,
   tm.driver_24,
   tm.driver_24_desc, 
   tm.driver_24_thru,
   tm.driver_24_thru_desc,
   tm.driver_25,
   tm.driver_25_desc, 
   tm.driver_25_thru,
   tm.driver_25_thru_desc,
   tm.driver_26,
   tm.driver_26_desc, 
   tm.driver_26_thru,
   tm.driver_26_thru_desc,
   tm.driver_27,
   tm.driver_27_desc, 
   tm.driver_27_thru,
   tm.driver_27_thru_desc,
   tm.driver_28,
   tm.driver_28_desc, 
   tm.driver_28_thru,
   tm.driver_28_thru_desc,
   tm.driver_29,
   tm.driver_29_desc, 
   tm.driver_29_thru,
   tm.driver_29_thru_desc,
   tm.driver_30,
   tm.driver_30_desc, 
   tm.driver_30_thru,
   tm.driver_30_thru_desc,
   tm.binary_weight,
   tm.significant_digits,
   tm.default_flag,
   tm.default_binary_weight,
   tm.default_significant_digits,
   tm.effective_date,
   tm.expiration_date,
   tm.relation_sign,
   tm.relation_amount,
   tm.then_hold_code_flag,
   tcd_t.taxcode_code,
   tm.else_hold_code_flag,
   tcd_e.taxcode_code,
   tm.comments,
   '1' active_flag,
   tm.update_user_id,
   tm.update_timestamp
  FROM v511_tax_matrix tm, tb_taxcode_detail tcd_t, tb_taxcode_detail tcd_e
 WHERE tm.then_taxcode_detail_id = tcd_t.taxcode_detail_id (+)
   AND tm.else_taxcode_detail_id = tcd_e.taxcode_detail_id (+);

-- tb_tmp_tax_matrix
DROP TABLE tb_tmp_tax_matrix;
CREATE GLOBAL TEMPORARY TABLE tb_tmp_tax_matrix (
   tax_matrix_id                   NUMBER,
   driver_global_flag              CHAR(1),
   driver_01                       VARCHAR2(100),
   driver_01_desc                  VARCHAR2(100),
   driver_01_thru                  VARCHAR2(100),
   driver_01_thru_desc             VARCHAR2(100),
   driver_02                       VARCHAR2(100),
   driver_02_desc                  VARCHAR2(100),
   driver_02_thru                  VARCHAR2(100),
   driver_02_thru_desc             VARCHAR2(100),
   driver_03                       VARCHAR2(100),
   driver_03_desc                  VARCHAR2(100),
   driver_03_thru                  VARCHAR2(100),
   driver_03_thru_desc             VARCHAR2(100),
   driver_04                       VARCHAR2(100),
   driver_04_desc                  VARCHAR2(100),
   driver_04_thru                  VARCHAR2(100),
   driver_04_thru_desc             VARCHAR2(100),
   driver_05                       VARCHAR2(100),
   driver_05_desc                  VARCHAR2(100),
   driver_05_thru                  VARCHAR2(100),
   driver_05_thru_desc             VARCHAR2(100),
   driver_06                       VARCHAR2(100),
   driver_06_desc                  VARCHAR2(100),
   driver_06_thru                  VARCHAR2(100),
   driver_06_thru_desc             VARCHAR2(100),
   driver_07                       VARCHAR2(100),
   driver_07_desc                  VARCHAR2(100),
   driver_07_thru                  VARCHAR2(100),
   driver_07_thru_desc             VARCHAR2(100),
   driver_08                       VARCHAR2(100),
   driver_08_desc                  VARCHAR2(100),
   driver_08_thru                  VARCHAR2(100),
   driver_08_thru_desc             VARCHAR2(100),
   driver_09                       VARCHAR2(100),
   driver_09_desc                  VARCHAR2(100),
   driver_09_thru                  VARCHAR2(100),
   driver_09_thru_desc             VARCHAR2(100),
   driver_10                       VARCHAR2(100),
   driver_10_desc                  VARCHAR2(100),
   driver_10_thru                  VARCHAR2(100),
   driver_10_thru_desc             VARCHAR2(100),
   driver_11                       VARCHAR2(100),
   driver_11_desc                  VARCHAR2(100),
   driver_11_thru                  VARCHAR2(100),
   driver_11_thru_desc             VARCHAR2(100),
   driver_12                       VARCHAR2(100),
   driver_12_desc                  VARCHAR2(100),
   driver_12_thru                  VARCHAR2(100),
   driver_12_thru_desc             VARCHAR2(100),
   driver_13                       VARCHAR2(100),
   driver_13_desc                  VARCHAR2(100),
   driver_13_thru                  VARCHAR2(100),
   driver_13_thru_desc             VARCHAR2(100),
   driver_14                       VARCHAR2(100),
   driver_14_desc                  VARCHAR2(100),
   driver_14_thru                  VARCHAR2(100),
   driver_14_thru_desc             VARCHAR2(100),
   driver_15                       VARCHAR2(100),
   driver_15_desc                  VARCHAR2(100),
   driver_15_thru                  VARCHAR2(100),
   driver_15_thru_desc             VARCHAR2(100),
   driver_16                       VARCHAR2(100),
   driver_16_desc                  VARCHAR2(100),
   driver_16_thru                  VARCHAR2(100),
   driver_16_thru_desc             VARCHAR2(100),
   driver_17                       VARCHAR2(100),
   driver_17_desc                  VARCHAR2(100),
   driver_17_thru                  VARCHAR2(100),
   driver_17_thru_desc             VARCHAR2(100),
   driver_18                       VARCHAR2(100),
   driver_18_desc                  VARCHAR2(100),
   driver_18_thru                  VARCHAR2(100),
   driver_18_thru_desc             VARCHAR2(100),
   driver_19                       VARCHAR2(100),
   driver_19_desc                  VARCHAR2(100),
   driver_19_thru                  VARCHAR2(100),
   driver_19_thru_desc             VARCHAR2(100),
   driver_20                       VARCHAR2(100),
   driver_20_desc                  VARCHAR2(100),
   driver_20_thru                  VARCHAR2(100),
   driver_20_thru_desc             VARCHAR2(100),
   driver_21                       VARCHAR2(100),
   driver_21_desc                  VARCHAR2(100),
   driver_21_thru                  VARCHAR2(100),
   driver_21_thru_desc             VARCHAR2(100),
   driver_22                       VARCHAR2(100),
   driver_22_desc                  VARCHAR2(100),
   driver_22_thru                  VARCHAR2(100),
   driver_22_thru_desc             VARCHAR2(100),
   driver_23                       VARCHAR2(100),
   driver_23_desc                  VARCHAR2(100),
   driver_23_thru                  VARCHAR2(100),
   driver_23_thru_desc             VARCHAR2(100),
   driver_24                       VARCHAR2(100),
   driver_24_desc                  VARCHAR2(100),
   driver_24_thru                  VARCHAR2(100),
   driver_24_thru_desc             VARCHAR2(100),
   driver_25                       VARCHAR2(100),
   driver_25_desc                  VARCHAR2(100),
   driver_25_thru                  VARCHAR2(100),
   driver_25_thru_desc             VARCHAR2(100),
   driver_26                       VARCHAR2(100),
   driver_26_desc                  VARCHAR2(100),
   driver_26_thru                  VARCHAR2(100),
   driver_26_thru_desc             VARCHAR2(100),
   driver_27                       VARCHAR2(100),
   driver_27_desc                  VARCHAR2(100),
   driver_27_thru                  VARCHAR2(100),
   driver_27_thru_desc             VARCHAR2(100),
   driver_28                       VARCHAR2(100),
   driver_28_desc                  VARCHAR2(100),
   driver_28_thru                  VARCHAR2(100),
   driver_28_thru_desc             VARCHAR2(100),
   driver_29                       VARCHAR2(100),
   driver_29_desc                  VARCHAR2(100),
   driver_29_thru                  VARCHAR2(100),
   driver_29_thru_desc             VARCHAR2(100),
   driver_30                       VARCHAR2(100),
   driver_30_desc                  VARCHAR2(100),
   driver_30_thru                  VARCHAR2(100),
   driver_30_thru_desc             VARCHAR2(100),
   binary_weight                   NUMBER,
   significant_digits              VARCHAR2(119),
   default_flag                    CHAR(1),
   default_binary_weight           NUMBER,
   default_significant_digits      VARCHAR2(119),
   effective_date                  DATE,
   expiration_date                 DATE,
   relation_sign                   VARCHAR2(3),
   relation_amount                 NUMBER,
   then_hold_code_flag             CHAR(1),
   then_taxcode_code               VARCHAR2(40),
   else_hold_code_flag             CHAR(1),
   else_taxcode_code               VARCHAR2(40),
   comments                        VARCHAR2(255),
   active_flag                     CHAR(1),
   update_user_id                  VARCHAR2(40),
   update_timestamp                DATE)
 ON COMMIT DELETE ROWS ;

-- tb_tax_holiday
CREATE TABLE stscorp.tb_tax_holiday (
   tax_holiday_code                VARCHAR2(40)            NOT NULL,
   taxcode_country_code            VARCHAR2(10),
   taxcode_state_code              VARCHAR2(10),
   description                     VARCHAR2(50),
   notes                           VARCHAR2(255),
   effective_date                  DATE,
   expiration_date                 DATE,
   taxcode_type_code               VARCHAR2(10),
   override_taxtype_code           VARCHAR2(10),
   ratetype_code                   VARCHAR2(10),
   taxable_threshold_amt           NUMBER,
   tax_limitation_amt              NUMBER,
   cap_amt                         NUMBER,
   base_change_pct                 NUMBER,
   special_rate                    NUMBER,
   execute_flag                    CHAR(1),
   update_user_id                  VARCHAR2(40)            DEFAULT USER,
   update_timestamp                DATE                    DEFAULT SYS_EXTRACT_UTC(SYSTIMESTAMP),
   CONSTRAINT pk_tb_tax_holiday PRIMARY KEY (tax_holiday_code) ENABLE VALIDATE);

-- tax_holiday_datail
CREATE TABLE stscorp.tb_tax_holiday_detail (
   tax_holiday_code                VARCHAR2(40)            NOT NULL,
   taxcode_code                    VARCHAR2(40)            NOT NULL,
   taxcode_county                  VARCHAR2(50)            NOT NULL,
   taxcode_city                    VARCHAR2(50)            NOT NULL,
   CONSTRAINT pk_tb_tax_holiday_detail PRIMARY KEY (tax_holiday_code, taxcode_code, taxcode_county, taxcode_city) ENABLE VALIDATE);

-- tb_bcp_taxcode_rules
CREATE TABLE tb_bcp_taxcode_rules (
   bcp_taxcode_rule_id             NUMBER                  NOT NULL,
   batch_id                        NUMBER,
   taxcode_country_code            VARCHAR2(10),
   lc_description                  VARCHAR2(50),
   taxcode_state_code              VARCHAR2(10),
   ts_geocode                      VARCHAR2(12),
   ts_name                         VARCHAR2(50),
   ts_local_taxability_code        VARCHAR2(10),
   ts_active_flag                  CHAR(1),
   taxcode_code                    VARCHAR2(40),
   tc_description                  VARCHAR2(120),
   tc_comments                     VARCHAR2(255),
   tc_active_flag                  CHAR(1),
   td_taxcode_county               VARCHAR2(50),
   td_taxcode_city                 VARCHAR2(50),
   td_effective_date               DATE,
   td_expiration_date              DATE,
   td_taxcode_type_code            VARCHAR2(10),
   td_override_taxtype_code        VARCHAR2(10),
   td_ratetype_code                VARCHAR2(10),
   td_taxable_threshold_amt        NUMBER,
   td_tax_limitation_amt           NUMBER,
   td_cap_amt                      NUMBER,
   td_base_change_pct              NUMBER,
   td_special_rate                 NUMBER,
   td_active_flag                  CHAR(1),
   update_code                     VARCHAR2(40),
   CONSTRAINT pk_tb_bcp_taxcode_rules PRIMARY KEY (bcp_taxcode_rule_id));

CREATE SEQUENCE stscorp.sq_tb_bcp_taxcode_rule_id INCREMENT BY 1 START WITH 1 MAXVALUE 1.0E27 MINVALUE 1 NOCYCLE NOCACHE NOORDER;

-- tb_tmp_taxcode_detail
CREATE GLOBAL TEMPORARY TABLE tb_tmp_taxcode_detail (
   taxcode_detail_id               NUMBER,
   taxcode_code                    VARCHAR2(40),
   taxcode_country_code            VARCHAR2(10),
   taxcode_state_code              VARCHAR2(10),
   taxcode_county                  VARCHAR2(50),
   taxcode_city                    VARCHAR2(50),
   effective_date                  DATE,
   expiration_date                 DATE,
   taxcode_type_code               VARCHAR2(10),
   override_taxtype_code           VARCHAR2(10),
   ratetype_code                   VARCHAR2(10),
   taxable_threshold_amt           NUMBER,
   tax_limitation_amt              NUMBER,
   cap_amt                         NUMBER,
   base_change_pct                 NUMBER,
   special_rate                    NUMBER,
   active_flag                     CHAR(1),
   custom_flag                     CHAR(1),
   modified_flag                   CHAR(1),
   jurisdiction_id                 NUMBER,
   update_user_id                  VARCHAR2(40),
   update_timestamp                DATE)
 ON COMMIT DELETE ROWS ;

-- tb_jurisdiction  -- this change already done in fed tax level
--ALTER TABLE tb_jurisdiction
--  ADD (country VARCHAR2(50));
--UPDATE tb_jurisdiction SET country=(SELECT taxcode_country_code FROM tb_taxcode_state WHERE taxcode_state_code=state);

-- tb_bcp_taxcode_rules_text
CREATE TABLE tb_bcp_taxcode_rules_text ( 
   batch_id NUMBER NOT NULL,
   line NUMBER NOT NULL,
   text VARCHAR2(4000) NULL,
   PRIMARY KEY(batch_id,line));

-- tb_transaction_detail
ALTER TABLE tb_transaction_detail
   ADD (state_taxcode_detail_id NUMBER)
   ADD (county_taxcode_detail_id NUMBER)
   ADD (city_taxcode_detail_id NUMBER)
   ADD (taxtype_used_code VARCHAR2(10))
   ADD (state_taxable_amt NUMBER)
   ADD (county_taxable_amt NUMBER)
   ADD (city_taxable_amt NUMBER)
   ADD (state_taxcode_type_code VARCHAR2(10))
   ADD (county_taxcode_type_code VARCHAR2(10))
   ADD (city_taxcode_type_code VARCHAR2(10));

ALTER TABLE tb_transaction_detail
   DROP (cch_taxcat_code, cch_group_code, cch_item_code);

-- tb_tmp_transaction_detail
ALTER TABLE tb_tmp_transaction_detail
   ADD (state_taxcode_detail_id NUMBER)
   ADD (county_taxcode_detail_id NUMBER)
   ADD (city_taxcode_detail_id NUMBER)
   ADD (taxtype_used_code VARCHAR2(10))
   ADD (state_taxable_amt NUMBER)
   ADD (county_taxable_amt NUMBER)
   ADD (city_taxable_amt NUMBER)
   ADD (state_taxcode_type_code VARCHAR2(10))
   ADD (county_taxcode_type_code VARCHAR2(10))
   ADD (city_taxcode_type_code VARCHAR2(10));

ALTER TABLE tb_tmp_transaction_detail
   DROP (cch_taxcat_code, cch_group_code, cch_item_code);

-- tb_gl_export_log
ALTER TABLE tb_gl_export_log
   ADD (state_taxcode_detail_id NUMBER)
   ADD (county_taxcode_detail_id NUMBER)
   ADD (city_taxcode_detail_id NUMBER)
   ADD (taxtype_used_code VARCHAR2(10))
   ADD (state_taxable_amt NUMBER)
   ADD (county_taxable_amt NUMBER)
   ADD (city_taxable_amt NUMBER)
   ADD (state_taxcode_type_code VARCHAR2(10))
   ADD (county_taxcode_type_code VARCHAR2(10))
   ADD (city_taxcode_type_code VARCHAR2(10));

ALTER TABLE tb_gl_export_log
   DROP (cch_taxcat_code, cch_group_code, cch_item_code);

-- tb_gl_report_log
ALTER TABLE tb_gl_report_log
   ADD (state_taxcode_detail_id NUMBER)
   ADD (county_taxcode_detail_id NUMBER)
   ADD (city_taxcode_detail_id NUMBER)
   ADD (taxtype_used_code VARCHAR2(10))
   ADD (state_taxable_amt NUMBER)
   ADD (county_taxable_amt NUMBER)
   ADD (city_taxable_amt NUMBER)
   ADD (state_taxcode_type_code VARCHAR2(10))
   ADD (county_taxcode_type_code VARCHAR2(10))
   ADD (city_taxcode_type_code VARCHAR2(10));

ALTER TABLE tb_gl_report_log
   DROP (cch_taxcat_code, cch_group_code, cch_item_code);

-- tb_bcp_transactions
ALTER TABLE tb_bcp_transactions
   ADD (state_taxcode_detail_id NUMBER)
   ADD (county_taxcode_detail_id NUMBER)
   ADD (city_taxcode_detail_id NUMBER)
   ADD (taxtype_used_code VARCHAR2(10))
   ADD (state_taxable_amt NUMBER)
   ADD (county_taxable_amt NUMBER)
   ADD (city_taxable_amt NUMBER)
   ADD (state_taxcode_type_code VARCHAR2(10))
   ADD (county_taxcode_type_code VARCHAR2(10))
   ADD (city_taxcode_type_code VARCHAR2(10));

ALTER TABLE tb_bcp_transactions
   DROP (cch_taxcat_code, cch_group_code, cch_item_code);

-- tb_batch_metadata
INSERT INTO TB_BATCH_METADATA (BATCH_TYPE_CODE, VC01_DESC, VC02_DESC, NU01_DESC, NU02_DESC, TS01_DESC, UPDATE_USER_ID, UPDATE_TIMESTAMP)
VALUES ('TCR', 'Rules Update File Name', 'File Type', 'Update Release', 'Special Rate Total', 'Update Date', USER, SYSDATE);

-- tb_list_code
INSERT INTO TB_LIST_CODE (CODE_TYPE_CODE, CODE_CODE, DESCRIPTION, MESSAGE, EXPLANATION, SEVERITY_LEVEL, WRITE_IMPORT_LINE_FLAG, ABORT_IMPORT_FLAG, UPDATE_USER_ID, UPDATE_TIMESTAMP)
VALUES ('*DEF', 'RATETYPE', 'Rate Types', NULL, NULL, NULL, NULL, NULL, 'STSCORP', SYSDATE); 
INSERT INTO TB_LIST_CODE (CODE_TYPE_CODE, CODE_CODE, DESCRIPTION, MESSAGE, EXPLANATION, SEVERITY_LEVEL, WRITE_IMPORT_LINE_FLAG, ABORT_IMPORT_FLAG, UPDATE_USER_ID, UPDATE_TIMESTAMP)
VALUES ('*DEF', 'LOCALTAX', 'Local Taxability Codes', NULL, NULL, NULL, NULL, NULL, 'STSCORP', SYSDATE); 
INSERT INTO TB_LIST_CODE (CODE_TYPE_CODE, CODE_CODE, DESCRIPTION, MESSAGE, EXPLANATION, SEVERITY_LEVEL, WRITE_IMPORT_LINE_FLAG, ABORT_IMPORT_FLAG, UPDATE_USER_ID, UPDATE_TIMESTAMP)
VALUES ('BATCHTYPE', 'TCR', 'TaxCode Rules', '0', '0', '0', '0', '0', 'STSCORP', SYSDATE); 
INSERT INTO TB_LIST_CODE (CODE_TYPE_CODE, CODE_CODE, DESCRIPTION, MESSAGE, EXPLANATION, SEVERITY_LEVEL, WRITE_IMPORT_LINE_FLAG, ABORT_IMPORT_FLAG, UPDATE_USER_ID, UPDATE_TIMESTAMP)
VALUES ('RATETYPE', '0', 'Tangible Personal Property', NULL, NULL, NULL, NULL, NULL, 'STSCORP', SYSDATE); 
INSERT INTO TB_LIST_CODE (CODE_TYPE_CODE, CODE_CODE, DESCRIPTION, MESSAGE, EXPLANATION, SEVERITY_LEVEL, WRITE_IMPORT_LINE_FLAG, ABORT_IMPORT_FLAG, UPDATE_USER_ID, UPDATE_TIMESTAMP)
VALUES ('RATETYPE', '1', 'Services', NULL, NULL, NULL, NULL, NULL, 'STSCORP', SYSDATE); 
INSERT INTO TB_LIST_CODE (CODE_TYPE_CODE, CODE_CODE, DESCRIPTION, MESSAGE, EXPLANATION, SEVERITY_LEVEL, WRITE_IMPORT_LINE_FLAG, ABORT_IMPORT_FLAG, UPDATE_USER_ID, UPDATE_TIMESTAMP)
VALUES ('LOCALTAX', 'S', 'State Only', NULL, NULL, NULL, NULL, NULL, 'STSCORP', SYSDATE); 
INSERT INTO TB_LIST_CODE (CODE_TYPE_CODE, CODE_CODE, DESCRIPTION, MESSAGE, EXPLANATION, SEVERITY_LEVEL, WRITE_IMPORT_LINE_FLAG, ABORT_IMPORT_FLAG, UPDATE_USER_ID, UPDATE_TIMESTAMP)
VALUES ('LOCALTAX', 'L', 'State & Local', NULL, NULL, NULL, NULL, NULL, 'STSCORP', SYSDATE); 
INSERT INTO TB_LIST_CODE (CODE_TYPE_CODE,CODE_CODE,DESCRIPTION,MESSAGE,EXPLANATION,SEVERITY_LEVEL,WRITE_IMPORT_LINE_FLAG,ABORT_IMPORT_FLAG,UPDATE_USER_ID,UPDATE_TIMESTAMP)
VALUES ('ERRORCODE','TCR1','Custom TaxCode',null,'Updating a Custom TaxCode','10','1','0',USER,SYS_EXTRACT_UTC(SYSTIMESTAMP));
INSERT INTO TB_LIST_CODE (CODE_TYPE_CODE,CODE_CODE,DESCRIPTION,MESSAGE,EXPLANATION,SEVERITY_LEVEL,WRITE_IMPORT_LINE_FLAG,ABORT_IMPORT_FLAG,UPDATE_USER_ID,UPDATE_TIMESTAMP)
VALUES ('ERRORCODE','TCR2','Modified TaxCode',null,'Updating a Modified TaxCode','10','1','0',USER,SYS_EXTRACT_UTC(SYSTIMESTAMP));
INSERT INTO TB_LIST_CODE (CODE_TYPE_CODE,CODE_CODE,DESCRIPTION,MESSAGE,EXPLANATION,SEVERITY_LEVEL,WRITE_IMPORT_LINE_FLAG,ABORT_IMPORT_FLAG,UPDATE_USER_ID,UPDATE_TIMESTAMP)
VALUES ('ERRORCODE','TCR3','Custom TaxCode Detail',null,'Updating a Custom TaxCode Detail','10','1','0',USER,SYS_EXTRACT_UTC(SYSTIMESTAMP));
INSERT INTO TB_LIST_CODE (CODE_TYPE_CODE,CODE_CODE,DESCRIPTION,MESSAGE,EXPLANATION,SEVERITY_LEVEL,WRITE_IMPORT_LINE_FLAG,ABORT_IMPORT_FLAG,UPDATE_USER_ID,UPDATE_TIMESTAMP)
VALUES ('ERRORCODE','TCR4','Modified TaxCode Detail',null,'Updating a Modified TaxCode Detail','10','1','0',USER,SYS_EXTRACT_UTC(SYSTIMESTAMP));
INSERT INTO TB_LIST_CODE (CODE_TYPE_CODE,CODE_CODE,DESCRIPTION,MESSAGE,EXPLANATION,SEVERITY_LEVEL,WRITE_IMPORT_LINE_FLAG,ABORT_IMPORT_FLAG,UPDATE_USER_ID,UPDATE_TIMESTAMP)
VALUES ('ERRORCODE','TCR5','Custom TaxCode State',null,'Updating a Custom TaxCode State','10','1','0',USER,SYS_EXTRACT_UTC(SYSTIMESTAMP));
INSERT INTO TB_LIST_CODE (CODE_TYPE_CODE,CODE_CODE,DESCRIPTION,MESSAGE,EXPLANATION,SEVERITY_LEVEL,WRITE_IMPORT_LINE_FLAG,ABORT_IMPORT_FLAG,UPDATE_USER_ID,UPDATE_TIMESTAMP)
VALUES ('ERRORCODE','TCR6','Modified TaxCode State',null,'Updating a Modified TaxCode State','10','1','0',USER,SYS_EXTRACT_UTC(SYSTIMESTAMP));
INSERT INTO TB_LIST_CODE (CODE_TYPE_CODE,CODE_CODE,DESCRIPTION,MESSAGE,EXPLANATION,SEVERITY_LEVEL,WRITE_IMPORT_LINE_FLAG,ABORT_IMPORT_FLAG,UPDATE_USER_ID,UPDATE_TIMESTAMP)
VALUES ('ERRORCODE','TCR7','Bad Return on Batch Update',null,null,'30','1','1',USER,SYS_EXTRACT_UTC(SYSTIMESTAMP));
INSERT INTO TB_LIST_CODE (CODE_TYPE_CODE,CODE_CODE,DESCRIPTION,MESSAGE,EXPLANATION,SEVERITY_LEVEL,WRITE_IMPORT_LINE_FLAG,ABORT_IMPORT_FLAG,UPDATE_USER_ID,UPDATE_TIMESTAMP)
VALUES ('ERRORCODE','TCR8','Batch not found or Bad Return on Batch Read',null,null,'30','1','1',USER,SYS_EXTRACT_UTC(SYSTIMESTAMP));
INSERT INTO TB_LIST_CODE (CODE_TYPE_CODE,CODE_CODE,DESCRIPTION,MESSAGE,EXPLANATION,SEVERITY_LEVEL,WRITE_IMPORT_LINE_FLAG,ABORT_IMPORT_FLAG,UPDATE_USER_ID,UPDATE_TIMESTAMP)
VALUES ('ERRORCODE','TCR9','Batch not Flagged for Processing',null,null,'30','1','1',USER,SYS_EXTRACT_UTC(SYSTIMESTAMP));
INSERT INTO TB_LIST_CODE (CODE_TYPE_CODE,CODE_CODE,DESCRIPTION,MESSAGE,EXPLANATION,SEVERITY_LEVEL,WRITE_IMPORT_LINE_FLAG,ABORT_IMPORT_FLAG,UPDATE_USER_ID,UPDATE_TIMESTAMP)
VALUES ('ERRORCODE','TCR10','TaxCode Used in Matrix',null,'Cannot delete a used TaxCode','10','1','0',USER,SYS_EXTRACT_UTC(SYSTIMESTAMP));
INSERT INTO TB_LIST_CODE (CODE_TYPE_CODE,CODE_CODE,DESCRIPTION,MESSAGE,EXPLANATION,SEVERITY_LEVEL,WRITE_IMPORT_LINE_FLAG,ABORT_IMPORT_FLAG,UPDATE_USER_ID,UPDATE_TIMESTAMP)
VALUES ('ERRORCODE','TCR11','TaxCode Not Found',null,'Cannot delete non-existing TaxCode','10','1','0',USER,SYS_EXTRACT_UTC(SYSTIMESTAMP));
INSERT INTO TB_LIST_CODE (CODE_TYPE_CODE,CODE_CODE,DESCRIPTION,MESSAGE,EXPLANATION,SEVERITY_LEVEL,WRITE_IMPORT_LINE_FLAG,ABORT_IMPORT_FLAG,UPDATE_USER_ID,UPDATE_TIMESTAMP)
VALUES ('ERRORCODE','TCR12','TaxCode Detail Used in Transactions',null,'Cannot delete a used TaxCode Detail','10','1','0',USER,SYS_EXTRACT_UTC(SYSTIMESTAMP));
INSERT INTO TB_LIST_CODE (CODE_TYPE_CODE,CODE_CODE,DESCRIPTION,MESSAGE,EXPLANATION,SEVERITY_LEVEL,WRITE_IMPORT_LINE_FLAG,ABORT_IMPORT_FLAG,UPDATE_USER_ID,UPDATE_TIMESTAMP)
VALUES ('ERRORCODE','TCR13','TaxCode Detail Not Found',null,'Cannot delete non-existing TaxCode Detail','10','1','0',USER,SYS_EXTRACT_UTC(SYSTIMESTAMP));
INSERT INTO TB_LIST_CODE (CODE_TYPE_CODE,CODE_CODE,DESCRIPTION,MESSAGE,EXPLANATION,SEVERITY_LEVEL,WRITE_IMPORT_LINE_FLAG,ABORT_IMPORT_FLAG,UPDATE_USER_ID,UPDATE_TIMESTAMP)
VALUES ('ERRORCODE','TCR22','Bad Update Code',null,'Update Code not recognized','10','1','0',USER,SYS_EXTRACT_UTC(SYSTIMESTAMP));
INSERT INTO TB_LIST_CODE(CODE_TYPE_CODE, CODE_CODE, DESCRIPTION, SEVERITY_LEVEL, WRITE_IMPORT_LINE_FLAG, ABORT_IMPORT_FLAG, UPDATE_USER_ID, UPDATE_TIMESTAMP)
VALUES ('ERRORCODE', 'TCR21', 'Error on Import to Taxcode Rules File', 10, 0, 0, USER, SYSDATE);
INSERT INTO TB_LIST_CODE(CODE_TYPE_CODE, CODE_CODE, DESCRIPTION, SEVERITY_LEVEL, WRITE_IMPORT_LINE_FLAG, ABORT_IMPORT_FLAG, UPDATE_USER_ID, UPDATE_TIMESTAMP)
VALUES ('ERRORCODE', 'TCR14', 'Invalid Batch Count', 20, 0, 0, USER, SYSDATE);
INSERT INTO TB_LIST_CODE(CODE_TYPE_CODE, CODE_CODE, DESCRIPTION, SEVERITY_LEVEL, WRITE_IMPORT_LINE_FLAG, ABORT_IMPORT_FLAG, UPDATE_USER_ID, UPDATE_TIMESTAMP)
VALUES ('ERRORCODE', 'TCR15', 'Invalid Special Rate Total', 20, 0, 0, USER, SYSDATE);
INSERT INTO TB_LIST_CODE(CODE_TYPE_CODE, CODE_CODE, DESCRIPTION, SEVERITY_LEVEL, WRITE_IMPORT_LINE_FLAG, ABORT_IMPORT_FLAG, UPDATE_USER_ID, UPDATE_TIMESTAMP)
VALUES ('ERRORCODE', 'TCR16', 'Bad or Missing End-of-File Marker', 20, 0, 0, USER, SYSDATE);
INSERT INTO TB_LIST_CODE(CODE_TYPE_CODE, CODE_CODE, DESCRIPTION, SEVERITY_LEVEL, WRITE_IMPORT_LINE_FLAG, ABORT_IMPORT_FLAG, UPDATE_USER_ID, UPDATE_TIMESTAMP)
VALUES ('ERRORCODE', 'TCR23', 'Invalid Column Value', 10, 0, 0, USER, SYSDATE);
INSERT INTO TB_LIST_CODE(CODE_TYPE_CODE, CODE_CODE, DESCRIPTION, MESSAGE, EXPLANATION, SEVERITY_LEVEL, WRITE_IMPORT_LINE_FLAG, ABORT_IMPORT_FLAG, UPDATE_USER_ID, UPDATE_TIMESTAMP)
VALUES('BATCHSTAT', 'ABTCR', 'TaxCode Rules Aborted', 'TCR', '', '0', '1', '0', USER, SYSDATE);
INSERT INTO TB_LIST_CODE(CODE_TYPE_CODE, CODE_CODE, DESCRIPTION, MESSAGE, EXPLANATION, SEVERITY_LEVEL, WRITE_IMPORT_LINE_FLAG, ABORT_IMPORT_FLAG, UPDATE_USER_ID, UPDATE_TIMESTAMP) 
VALUES('BATCHSTAT', 'FTCR', 'Flagged for TaxCode Rules Update', 'TCR', '', '', '1', '', USER, SYSDATE);
INSERT INTO TB_LIST_CODE(CODE_TYPE_CODE, CODE_CODE, DESCRIPTION, MESSAGE, EXPLANATION, SEVERITY_LEVEL, WRITE_IMPORT_LINE_FLAG, ABORT_IMPORT_FLAG, UPDATE_USER_ID, UPDATE_TIMESTAMP) 
VALUES('BATCHSTAT', 'ITCR', 'Imported TaxCode Rules Update', 'TCR', '', '', '1', '', USER, SYSDATE);
INSERT INTO TB_LIST_CODE(CODE_TYPE_CODE, CODE_CODE, DESCRIPTION, MESSAGE, EXPLANATION, SEVERITY_LEVEL, WRITE_IMPORT_LINE_FLAG, ABORT_IMPORT_FLAG, UPDATE_USER_ID, UPDATE_TIMESTAMP) 
VALUES('BATCHSTAT', 'TCR', 'TaxCode Rules Updated', 'TCR', '', '', '1', '', USER, SYSDATE);
INSERT INTO TB_LIST_CODE(CODE_TYPE_CODE, CODE_CODE, DESCRIPTION, MESSAGE, EXPLANATION, SEVERITY_LEVEL, WRITE_IMPORT_LINE_FLAG, ABORT_IMPORT_FLAG, UPDATE_USER_ID, UPDATE_TIMESTAMP) 
VALUES('BATCHSTAT', 'XITCR', 'TaxCode Rules Update Import Running', 'TCR', '', '', '1', '', USER, SYSDATE);
INSERT INTO TB_LIST_CODE(CODE_TYPE_CODE, CODE_CODE, DESCRIPTION, MESSAGE, EXPLANATION, SEVERITY_LEVEL, WRITE_IMPORT_LINE_FLAG, ABORT_IMPORT_FLAG, UPDATE_USER_ID, UPDATE_TIMESTAMP) 
VALUES('BATCHSTAT', 'XTCR', 'TaxCode Rules Updating', 'TCR', '', '', '1', '', USER, SYSDATE);
INSERT INTO TB_LIST_CODE(CODE_TYPE_CODE, CODE_CODE, DESCRIPTION, UPDATE_USER_ID, UPDATE_TIMESTAMP)
VALUES('PROCTYPE', 'TCR', 'TaxCode Rules Update', USER, SYSDATE);

INSERT INTO tb_list_code (code_type_code, code_code, description, message, explanation, severity_level, write_import_line_flag, abort_import_flag, update_user_id, update_timestamp)
VALUES ('SUSPENDIND', 'D', 'TaxCode Detail', NULL, NULL, NULL, NULL, NULL, USER, SYS_EXTRACT_UTC(SYSTIMESTAMP)); 

-- tb_menu
INSERT INTO TB_MENU(MENU_CODE, MAIN_MENU_CODE, MENU_SEQUENCE, OPTION_NAME, COMMAND_TYPE, COMMAND_LINE, KEY_VALUES, PANEL_CLASS_NAME, UPDATE_USER_ID, UPDATE_TIMESTAMP)
VALUES('m_taxcoderules', 'm_datautility', 5, 'TaxCode Rules', 'OPEN', '', '', '', USER, SYSDATE);
INSERT INTO TB_MENU(MENU_CODE, MAIN_MENU_CODE, MENU_SEQUENCE, OPTION_NAME, COMMAND_TYPE, COMMAND_LINE, KEY_VALUES, PANEL_CLASS_NAME, UPDATE_USER_ID, UPDATE_TIMESTAMP)
VALUES('m_supportingcodes', 'm_setup', 7, 'Supporting Codes', 'OPEN', '', '', '', USER, SYSDATE);
INSERT INTO TB_MENU(MENU_CODE, MAIN_MENU_CODE, MENU_SEQUENCE, OPTION_NAME, COMMAND_TYPE, COMMAND_LINE, KEY_VALUES, PANEL_CLASS_NAME, UPDATE_USER_ID, UPDATE_TIMESTAMP)
VALUES('m_taxallocationmatrix', 'm_maintenance', 4, 'Tax Allocation', 'OPEN', '', '', '', USER, SYSDATE);
INSERT INTO TB_MENU(MENU_CODE, MAIN_MENU_CODE, MENU_SEQUENCE, OPTION_NAME, COMMAND_TYPE, COMMAND_LINE, KEY_VALUES, PANEL_CLASS_NAME, UPDATE_USER_ID, UPDATE_TIMESTAMP)
VALUES ('m_taxholiday', 'm_maintenance', 8, 'Tax Holiday', 'OPEN', '', '', '', USER, SYSDATE);

DROP PROCEDURE sp_tb_taxcode_state_a_u;
DROP PROCEDURE sp_tb_taxcode_a_u;

COMMIT;

SHOW ERRORS
SPOOL OFF 
