CREATE OR REPLACE FUNCTION get_tax_matrix(p_transaction_detail_id numeric)
  RETURNS character varying AS
$BODY$
DECLARE
/* ************************************************************************************************/
/* Object Type/Name: get_tax_matrix					                            */
/* Author:           Michael B. Fuller                                                            */
/* Date:             a long time ago                                                              */
/* Description:      A transaction has been changed                                               */
/* Arguments:        None                                                                         */
/* Returns:          None                                                                         */
/* ************************************************************************************************/
/* Maintenance History:                                                                           */
/* Scan  Author       Date     Release    Description                                  Ticket #   */
/* ----- ---------- ---------- ---------- -------------------------------------------- ---------- */
/*  SRINI            01/26/2011            Initial Version                              955       */
/* ************************************************************************************************/
   vc_tax_matrix_where             VARCHAR(4000) := '';
   tmp                             VARCHAR(4000) := '';
   vc_state_driver_flag            VARCHAR(10) := '0';
begin

select  sp_gen_tax_driver (
	'N',
	NULL,
	'trn',
	'mat',
	vc_state_driver_flag,
	vc_tax_matrix_where) 
  into vc_tax_matrix_where; 
   
  IF vc_tax_matrix_where IS NULL THEN
    vc_tax_matrix_where := '';
  END IF;

  return vc_tax_matrix_where;
END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
ALTER FUNCTION get_tax_matrix(numeric) OWNER TO stscorp;
