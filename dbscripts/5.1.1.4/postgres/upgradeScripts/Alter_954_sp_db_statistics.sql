-- Function: sp_db_statistics(character varying)

-- DROP FUNCTION sp_db_statistics(character varying);

CREATE OR REPLACE FUNCTION sp_db_statistics(avc_where character varying)
  RETURNS void AS
$BODY$
/* ************************************************************************************************/
/* Object Type/Name: Stored Procedure - sp_batch_process                                          */
/* Author:           Michael B. Fuller                                                            */
/* Date:             a long time ago                                                              */
/* Description:      process a batch of transactions                                              */
/* Arguments:        an_batch_id number                                                           */
/* Returns:          None                                                                         */
/* ************************************************************************************************/
/* Maintenance History:                                                                           */
/* Scan  Author       Date     Release    Description                                  Ticket #   */
/* ----- ---------- ---------- ---------- -------------------------------------------- ---------- */
/*       M. Fuller  06/02/2006 3.3.5.0    Fix processing an allocated line - Temporary 871        */
/*       M. Fuller  01/04/2007 3.3.5.1    Fix processing an allocated line - Permenant 871        */
/* MBF01 M. Fuller                        Add Kill Process Flag                                   */
/* MBF02 M. Fuller  01/25/2007 3.3.5.1    Add Allocations                                         */
/* MBF03 M. Fuller  05/18/2007 3.3.7a.x   Add User Exit to end of procedure            888        */
/* MBF04 M. Fuller  08/13/2007 3.4.1.1    Add 5 Calc Tax Flags                         883        */
/* MBF05 M. Fuller  10/10/2008 5.1.1.1    Change SQL to ANSI standard                  3464       */
/* SRINI M. Fuller  01/23/2011 5.1.1.1    Change VARCHAR size                           954       */
/* ************************************************************************************************/
DECLARE
-- Statistics Selection SQL
-- MBF -- Removed hints -- 5.1.1.1
   vc_stat_total                   VARCHAR (1000) :=
      'SELECT count(*), sum(gl_line_itm_dist_amt), sum(tb_calc_tax_amt) ' ||
      'FROM tb_transaction_detail LEFT JOIN tb_jurisdiction ON (tb_transaction_detail.jurisdiction_id = tb_jurisdiction.jurisdiction_id) ';
   vc_stat_select                  VARCHAR (1000) :=
      'SELECT count(*), sum(gl_line_itm_dist_amt), sum(tb_calc_tax_amt), max(gl_line_itm_dist_amt), avg(gl_line_itm_dist_amt) ' ||
      'FROM tb_transaction_detail LEFT JOIN tb_jurisdiction ON (tb_transaction_detail.jurisdiction_id = tb_jurisdiction.jurisdiction_id) ';
   vc_stat_where                   VARCHAR (28000) := ''; /***954***SRINI***/
   vc_stat_stmt                    VARCHAR (30000);       /***954***SRINI***/

   stat_cursor                     REFCURSOR;
   stat_cursor_record		   RECORD;

-- Driver Reference Insert SQL
   vc_driver_ref_ins_stmt          VARCHAR (30000);      /***954***SRINI***/
   vn_status                       NUMERIC := NULL;

-- Table defined variables
   v_count_total                   tb_tmp_statistics.count%TYPE              := 0;
   v_sum_total                     tb_tmp_statistics.sum%TYPE                := 0;
   v_sum_tax                       tb_tmp_statistics.sum%TYPE                := 0;
   v_count                         tb_tmp_statistics.count%TYPE              := 0;
   v_count_pct                     tb_tmp_statistics.count_pct%TYPE          := 0;
   v_sum                           tb_tmp_statistics.sum%TYPE                := 0;
   v_sum_pct                       tb_tmp_statistics.sum_pct%TYPE            := 0;
   v_tax_amt                       tb_tmp_statistics.tax_amt%TYPE            := 0;
   v_max                           tb_tmp_statistics.max%TYPE                := 0;
   v_avg                           tb_tmp_statistics.avg%TYPE                := 0;

-- Define Statistics Cursor  (tb_db_statistics)
   db_statistics_cursor CURSOR 
   FOR
      SELECT row_title, row_where, group_by_on_drilldown
      FROM tb_db_statistics
      ORDER BY db_statistics_id;
   
   db_statistics                 RECORD;


-- Program starts **********************************************************************************
BEGIN

   -- Clean up tb_tmp_statistis table
   EXECUTE 'TRUNCATE TABLE tb_tmp_statistics';

   -- Get Totals for Percents-of-totals ------------------------------------------------------------
   -- Initialize fields
   vc_stat_where := '';
   vc_stat_stmt := '';

   -- Check input parameter for where clause and build SQL statement
   IF avc_where IS NULL THEN
      vc_stat_stmt := vc_stat_total;
   ELSE
	   vc_stat_stmt := vc_stat_total || avc_where;
   END IF;

	-- Debug Statements
   --insert into tb_debug(row_joe) values(vc_stat_stmt);
   --commit;

	-- Get Totals
   BEGIN
      
      OPEN stat_cursor
       FOR EXECUTE vc_stat_stmt;

      FETCH stat_cursor
       INTO stat_cursor_record;
       --INTO v_count_total, v_sum_total, v_sum_tax;

      IF (FOUND) THEN
	vn_status := 1;

	v_count_total	:= stat_cursor_record.cnt;
	v_sum_total	:= stat_cursor_record.sum_gl_line_itm_dist_amt;
	v_sum_tax	:= stat_cursor_record.sum_tb_calc_tax_amt;

      ELSE
         vn_status := 0;
      END IF;
      CLOSE stat_cursor;
   EXCEPTION
     WHEN OTHERS THEN
       vn_status := 0;
   END;


   -- Read and Process Each Statistics Line  -------------------------------------------------------
   OPEN db_statistics_cursor;
   LOOP
      
      FETCH db_statistics_cursor INTO db_statistics;
      
      IF (NOT FOUND) THEN
	EXIT;
      END IF;


      -- Initialize fields
      vc_stat_where := '';
      vc_stat_stmt := '';

      -- Check db_statistics table and input parameter and create where clause
      IF db_statistics.row_where IS NULL THEN
         IF avc_where IS NULL THEN
            vc_stat_where := NULL;
         ELSE
            vc_stat_where := avc_where;
         END IF;
      ELSE
         IF avc_where IS NULL THEN
            vc_stat_where := ' WHERE ' || db_statistics.row_where;
         ELSE
            vc_stat_where := avc_where || ' AND ' || db_statistics.row_where;
         END IF;
      END IF;

		-- Build SQL statement
		IF vc_stat_where IS NULL OR vc_stat_where = '' THEN
		   vc_stat_stmt := vc_stat_select;
		ELSE
		   vc_stat_stmt := vc_stat_select || vc_stat_where;
		END IF;

      -- Debug Statements
		--insert into tb_debug(row_joe) values(vc_stat_where);
      --insert into tb_debug(row_joe) values(vc_stat_stmt);
      --commit;

      -- Get Transactions
      BEGIN
         OPEN stat_cursor
          FOR EXECUTE vc_stat_stmt;

         FETCH stat_cursor
          INTO stat_cursor_record;
	  
         IF (FOUND) THEN
	    vn_status := 1;

	    v_count	:= stat_cursor_record.cnt;
	    v_sum	:= stat_cursor_record.sum_gl_line_itm_dist_amt;
	    v_tax_amt	:= stat_cursor_record.sum_tb_calc_tax_amt;
	    v_max	:= stat_cursor_record.max_gl_line_itm_dist_amt;
	    v_avg	:= stat_cursor_record.avg_gl_line_itm_dist_amt;
         ELSE
            vn_status := 0;
         END IF;
         CLOSE stat_cursor;
      EXCEPTION
         WHEN OTHERS THEN
           vn_status := 0;
      END;

      -- Calculate percents-of-totals
	  IF v_count_total <> 0 THEN
         v_count_pct := (v_count/v_count_total)*100;
      ELSE
         v_count_pct := 0;
      END IF;

      IF v_sum_total <> 0 THEN
         v_sum_pct := (v_sum/v_sum_total)*100;
      ELSE
         v_sum_pct := 0;
      END IF;

      -- Insert tmp statistics detail row
      INSERT INTO tb_tmp_statistics (
         title,
         count,
         count_pct,
         sum,
         sum_pct,
         tax_amt,
         max,
         avg,
         where_clause,
         group_by_on_drilldown )
      VALUES (
         db_statistics.row_title,
         v_count,
         v_count_pct,
         v_sum,
         v_sum_pct,
         v_tax_amt,
         v_max,
         v_avg,
         vc_stat_where,
         db_statistics.group_by_on_drilldown );

      -- Error checking
--    IF SQLCODE != 0 THEN
--       vc_status_message := 'ERROR Inserting into Transaction Detail: ' || TO_CHAR (SQLCODE);
--       INSERT INTO tb_stscdu_messages (run_date, message)
--           VALUES (v_load_timestamp, vc_status_message);
--           vi_error_count := vi_error_count + 1;
--    END IF;

   END LOOP;

   CLOSE db_statistics_cursor;

   --COMMIT;

END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
ALTER FUNCTION sp_db_statistics(character varying) OWNER TO stscorp;
