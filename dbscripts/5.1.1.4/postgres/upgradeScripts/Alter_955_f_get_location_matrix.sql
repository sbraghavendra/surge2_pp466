-- Function: f_get_location_matrix(numeric)

-- DROP FUNCTION f_get_location_matrix(numeric);

CREATE OR REPLACE FUNCTION f_get_location_matrix(p_transaction_detail_id numeric)
  RETURNS character varying AS
$BODY$
DECLARE
/* ************************************************************************************************/
/* Object Type/Name: f_get_location_matrix					                            */
/* Author:           Michael B. Fuller                                                            */
/* Date:             a long time ago                                                              */
/* Description:                                                                                   */
/* Arguments:        None                                                                         */
/* Returns:          None                                                                         */
/* ************************************************************************************************/
/* Maintenance History:                                                                           */
/* Scan  Author       Date     Release    Description                                  Ticket #   */
/* ----- ---------- ---------- ---------- -------------------------------------------- ---------- */
/*  SRINI            01/26/2011            Initial Version                              955       */
/* ************************************************************************************************/
    vn_location_matrix_id INTEGER := 0;
   vc_location_matrix_select       VARCHAR(1000) :=
      'SELECT tb_location_matrix.location_matrix_id, ' ||
             'tb_location_matrix.jurisdiction_id, ' ||
             'tb_location_matrix.override_taxtype_code, ' ||
             'tb_location_matrix.state_flag,' ||
             'tb_location_matrix.county_flag,' ||
             'tb_location_matrix.county_local_flag,' ||
             'tb_location_matrix.city_flag,' ||
             'tb_location_matrix.city_local_flag,' ||
             'tb_jurisdiction.state, ' ||
             'tb_jurisdiction.county, ' ||
             'tb_jurisdiction.city, ' ||
             'tb_jurisdiction.zip, ' ||
             'tb_jurisdiction.in_out ' ||
        'FROM tb_location_matrix, ' ||
             'tb_jurisdiction, ' ||
             'tb_tmp_transaction_detail_b_u ' ||
      'WHERE ( tb_tmp_transaction_detail_b_u.transaction_detail_id = :v_transaction_detail_id ) AND ' ||
            '( tb_location_matrix.default_flag = ''0'' AND tb_location_matrix.binary_weight > 0 ) AND ' ||
            '( tb_location_matrix.effective_date <= tb_tmp_transaction_detail_b_u.gl_date ) AND ( tb_location_matrix.expiration_date >= tb_tmp_transaction_detail_b_u.gl_date ) AND ' ||
            '( tb_location_matrix.jurisdiction_id = tb_jurisdiction.jurisdiction_id ) ';
   vc_location_matrix_where        VARCHAR(3000) := '';
   vc_location_matrix_orderby      VARCHAR(1000) :=
      'ORDER BY tb_location_matrix.binary_weight DESC, tb_location_matrix.significant_digits DESC, tb_location_matrix.effective_date DESC';
   vc_location_matrix_stmt         VARCHAR(5000);
   p_new_tb_transaction_detail     tb_tmp_transaction_detail_b_u%ROWTYPE;
begin
    SELECT * INTO p_new_tb_transaction_detail FROM tb_tmp_transaction_detail_b_u
    WHERE transaction_detail_id = p_transaction_detail_id;
select  sp_gen_location_driver(
	'N',
	NULL,
	'tb_tmp_transaction_detail_b_u',
	'tb_location_matrix',
	vc_location_matrix_where) 
  into vc_location_matrix_where; 
   
  -- If no drivers found raise error, else create transaction detail sql statement
  IF vc_location_matrix_where IS NULL THEN
    vc_location_matrix_stmt := '';
  ELSE
    vc_location_matrix_stmt := vc_location_matrix_select || vc_location_matrix_where || vc_location_matrix_orderby;
  END IF;
  return vc_location_matrix_stmt;
END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
ALTER FUNCTION f_get_location_matrix(numeric) OWNER TO stscorp;
