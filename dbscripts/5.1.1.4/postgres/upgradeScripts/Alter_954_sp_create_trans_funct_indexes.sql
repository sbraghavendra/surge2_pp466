-- Function: sp_create_trans_funct_indexes()

-- DROP FUNCTION sp_create_trans_funct_indexes();

CREATE OR REPLACE FUNCTION sp_create_trans_funct_indexes()
  RETURNS void AS
$BODY$
DECLARE
/* ************************************************************************************************/
/* Object Type/Name: Stored Procedure - sp_create_trans_funct_indexes                                  */
/* Author:           Michael B. Fuller                                                            */
/* Date:             a long time ago                                                              */
/* Description:      Create indexes .                                                             */
/* Arguments:                                                                                     */
/* Returns:          None                                                                         */
/* ************************************************************************************************/
/* Maintenance History:                                                                           */
/* Scan  Author       Date     Release    Description                                  Ticket #   */
/* ----- ---------- ---------- ---------- -------------------------------------------- ---------- */
/* SRINI M. Fuller  01/21/2011            Change the size of VARCHAR                   954       */
/* ************************************************************************************************/
-- Program defined variables
   vc_statement                    varchar(30000)                            := ''; /***954***SRINI***/
   vc_index_tablespace             varchar(1000)                              := '';/***954***SRINI***/
   vi_rows_processed               integer                                  := 0;
   vi_cursor_id                    integer                                  := 0;

   tax_driver                      RECORD;

-- Program starts **********************************************************************************
BEGIN
   -- Get Index Tablespace name from preferences/options table
   BEGIN
      SELECT value
        INTO STRICT vc_index_tablespace
        FROM tb_option
       WHERE option_code = 'INDEX_TABLESPACE'
         AND option_type_code = 'ADMIN'
         AND user_code = 'ADMIN';
   EXCEPTION
      WHEN NO_DATA_FOUND THEN
         vc_index_tablespace := 'indx';
   END;
   
   vc_index_tablespace := TRIM(vc_index_tablespace);
   IF vc_index_tablespace IS NULL OR vc_index_tablespace = '' THEN
      vc_index_tablespace := 'indx';
   END IF;

   -- Create and execute sql to build index for each driver   
   FOR tax_driver IN
	SELECT driver.trans_dtl_column_name, driver.matrix_column_name
         FROM tb_driver_names driver
         WHERE driver.driver_names_code = 'T'
	 ORDER BY driver.driver_id
   LOOP
      -- Create dynamic SQL
      vc_statement := 'CREATE INDEX idx_tb_trans_dtl_' || tax_driver.matrix_column_name ||
                      '_UP ON tb_transaction_detail (UPPER(' || tax_driver.trans_dtl_column_name ||
                      '))';
                      --CM tablespace ' || vc_index_tablespace;

      -- Execute dynamic SQL
      EXECUTE vc_statement;
   END LOOP;

  --CM COMMIT;
END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
ALTER FUNCTION sp_create_trans_funct_indexes() OWNER TO stscorp;
