CREATE OR REPLACE FUNCTION stscorp.sp_get_location_matrix(p_transaction_detail_id numeric)
  RETURNS REFCURSOR AS
$BODY$
DECLARE
/* ************************************************************************************************/
/* Object Type/Name: sp_get_location_matrix					                            */
/* Author:           Michael B. Fuller                                                            */
/* Date:             a long time ago                                                              */
/* Description:                                                                                   */
/* Arguments:        None                                                                         */
/* Returns:          None                                                                         */
/* ************************************************************************************************/
/* Maintenance History:                                                                           */
/* Scan  Author       Date     Release    Description                                  Ticket #   */
/* ----- ---------- ---------- ---------- -------------------------------------------- ---------- */
/*  SRINI            02/04/2011            Initial Version                              956       */
/* ************************************************************************************************/
    vn_location_matrix_id INTEGER := 0;
   vc_location_matrix_select       VARCHAR(2000) :=
      'SELECT tb_location_matrix.location_matrix_id, ' ||
             'tb_location_matrix.jurisdiction_id, ' ||
             'tb_location_matrix.override_taxtype_code, ' ||
             'tb_location_matrix.state_flag,' ||
             'tb_location_matrix.county_flag,' ||
             'tb_location_matrix.county_local_flag,' ||
             'tb_location_matrix.city_flag,' ||
             'tb_location_matrix.city_local_flag,' ||
             'tb_jurisdiction.state, ' ||
             'tb_jurisdiction.county, ' ||
             'tb_jurisdiction.city, ' ||
             'tb_jurisdiction.zip, ' ||
             'tb_jurisdiction.in_out ' ||
        'FROM tb_location_matrix, ' ||
             'tb_jurisdiction, ' ||
             'tb_tmp_transaction_detail_b_u ' ||
      'WHERE ( tb_tmp_transaction_detail_b_u.transaction_detail_id = ( SELECT CAST (param_value AS NUMERIC) FROM sp_gen_tax_driver_tmp WHERE param_name = ''v_transaction_detail_id'' ) ) AND '  ||
            '( tb_location_matrix.default_flag = ''0'' AND tb_location_matrix.binary_weight > 0 ) AND ' ||
            '( tb_location_matrix.effective_date <= tb_tmp_transaction_detail_b_u.gl_date ) AND ( tb_location_matrix.expiration_date >= tb_tmp_transaction_detail_b_u.gl_date ) AND ' ||
            '( tb_location_matrix.jurisdiction_id = tb_jurisdiction.jurisdiction_id ) ';
   vc_location_matrix_where        VARCHAR(28000) := '';
   vc_location_matrix_orderby      VARCHAR(1000) :=
      'ORDER BY tb_location_matrix.binary_weight DESC, tb_location_matrix.significant_digits DESC, tb_location_matrix.effective_date DESC';
   vc_location_matrix_stmt         VARCHAR(31000);
   p_new_tb_transaction_detail     tb_tmp_transaction_detail_b_u%ROWTYPE;
    v_transaction_detail_id         tb_tmp_transaction_detail_b_u.transaction_detail_id%TYPE  := NULL;
   vn_fetch_rows                   NUMERIC                                            := 0;
    location_matrix_cursor       REFCURSOR;
    location_matrix		 RECORD;
    res				 REFCURSOR;
begin
    SELECT * INTO p_new_tb_transaction_detail FROM tb_tmp_transaction_detail_b_u
    WHERE transaction_detail_id = p_transaction_detail_id;

     IF p_new_tb_transaction_detail.transaction_ind IS NULL OR p_new_tb_transaction_detail.transaction_ind <> 'S' THEN
     IF UPPER(p_new_tb_transaction_detail.taxcode_type_code) = 'T' THEN
        IF p_new_tb_transaction_detail.jurisdiction_id IS NULL OR p_new_tb_transaction_detail.jurisdiction_id = 0 THEN
           IF p_new_tb_transaction_detail.location_matrix_id IS NULL AND p_new_tb_transaction_detail.manual_jurisdiction_ind IS NULL THEN
               v_transaction_detail_id := p_new_tb_transaction_detail.transaction_detail_id;
select  sp_gen_location_driver(
	'N',
	NULL,
	'tb_tmp_transaction_detail_b_u',
	'tb_location_matrix',
	vc_location_matrix_where) 
  into vc_location_matrix_where; 
   -- If no drivers found raise error, else create transaction detail sql statement
               IF vc_location_matrix_where IS NULL THEN
                  NULL;
               ELSE
                  vc_location_matrix_stmt := vc_location_matrix_select || vc_location_matrix_where || vc_location_matrix_orderby;
               END IF;

               -- Search Location Matrix for matches
               BEGIN
                    -- ****** CREATE Temporary Table to hold "Bind" Values ****** -----------------------------------------
		     BEGIN
		       EXECUTE 'CREATE TEMPORARY TABLE sp_gen_location_driver_tmp (param_name varchar(30), param_value varchar(30))';
		     EXCEPTION 
		       -- Table may already exist if using Connection/Session Pooling, Ignore
		       WHEN OTHERS THEN NULL;
		     END;

		     -- Prep TMP table with values
		     DELETE FROM sp_gen_location_driver_tmp;
		     INSERT INTO sp_gen_location_driver_tmp(param_name, param_value) VALUES ('v_transaction_detail_id',  v_transaction_detail_id);
                   OPEN location_matrix_cursor
                    FOR EXECUTE vc_location_matrix_stmt;
                  --USING v_transaction_detail_id;
                  FETCH location_matrix_cursor
                   INTO location_matrix;
                  -- Rows found?
                 IF (NOT FOUND) THEN
                     vn_fetch_rows := location_matrix_cursor%ROWCOUNT;
                  ELSE
                     vn_fetch_rows := 0;
                  END IF;
                  CLOSE location_matrix_cursor;
                          
		     BEGIN
		       EXECUTE 'DROP TABLE sp_gen_location_driver_tmp ';
		     EXCEPTION 
		       WHEN OTHERS THEN NULL;
		     END;
                --  IF SQLCODE != 0 THEN
                --    RAISE e_badread;
                --  END IF;
               EXCEPTION
                  WHEN OTHERS THEN
                     vn_fetch_rows := 0;
               END;

               -- Location Matrix Line Found
               IF vn_fetch_rows > 0 THEN
                  p_new_tb_transaction_detail.location_matrix_id := location_matrix.location_matrix_id;
                  p_new_tb_transaction_detail.jurisdiction_id := location_matrix.jurisdiction_id;

                  update tb_tmp_transaction_detail_b_u set
                     taxcode_detail_id = p_new_tb_transaction_detail.taxcode_detail_id,
                     taxcode_state_code = p_new_tb_transaction_detail.taxcode_state_code,
                     taxcode_type_code = p_new_tb_transaction_detail.taxcode_type_code,
                     taxcode_code = p_new_tb_transaction_detail.taxcode_code,
                     measure_type_code = p_new_tb_transaction_detail.measure_type_code,
                     cch_taxcat_code = p_new_tb_transaction_detail.cch_taxcat_code,
                     cch_group_code = p_new_tb_transaction_detail.cch_group_code
                     where transaction_detail_id = p_new_tb_transaction_detail.transaction_detail_id;
               ELSE
                  p_new_tb_transaction_detail.transaction_detail_id := 0;
               END IF;
            END IF;
        END IF;
     END IF;
   END IF;
   OPEN res
     FOR select * from tb_tmp_transaction_detail_b_u where transaction_detail_id = CAST(p_new_tb_transaction_detail.transaction_detail_id AS CHAR(30));
END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
ALTER FUNCTION stscorp.sp_get_location_matrix(numeric) OWNER TO stscorp;
