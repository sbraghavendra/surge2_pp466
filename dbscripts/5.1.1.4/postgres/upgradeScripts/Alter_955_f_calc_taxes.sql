-- Function: f_calc_taxes(numeric, date, character varying, numeric, character varying, character varying, character, character, character, character, character, numeric, numeric, character varying, character varying, character varying)

-- DROP FUNCTION f_calc_taxes(numeric, date, character varying, numeric, character varying, character varying, character, character, character, character, character, numeric, numeric, character varying, character varying, character varying);

CREATE OR REPLACE FUNCTION f_calc_taxes(an_jurisdiction_id numeric, ad_gl_date date, av_measure_type_code character varying, an_gl_line_itm_dist_amt numeric, av_taxtype_code character varying, av_override_taxtype_code character varying, ac_state_flag character, ac_county_flag character, ac_county_local_flag character, ac_city_flag character, ac_city_local_flag character, an_invoice_freight_amt numeric, an_invoice_discount_amt numeric, av_transaction_state_code character varying, av_import_definition_code character varying, av_taxcode_code character varying)
  RETURNS character varying AS
$BODY$
DECLARE
/* ************************************************************************************************/
/* Object Type/Name: f_calc_taxes					                            */
/* Author:           Michael B. Fuller                                                            */
/* Date:             a long time ago                                                              */
/* Description:                                                                                   */
/* Arguments:        None                                                                         */
/* Returns:          None                                                                         */
/* ************************************************************************************************/
/* Maintenance History:                                                                           */
/* Scan  Author       Date     Release    Description                                  Ticket #   */
/* ----- ---------- ---------- ---------- -------------------------------------------- ---------- */
/*  SRINI            02/03/2011            Initial Version                              955       */
/* ************************************************************************************************/
-- Program defined variables
   vn_fetch_rows numeric := 0;

-- Define Jurisdiction Tax Rates Cursor Record (tb_jurisdiction_taxrate)
   jurisdiction_taxrate         RECORD;
   jurisdiction_taxrate_cursor  REFCURSOR;      

 an_jurisdiction_taxrate_id NUMERIC;
   an_state_amount NUMERIC;
   an_state_use_tier2_amount NUMERIC;
   an_state_use_tier3_amount NUMERIC;
   an_county_amount NUMERIC;
   an_county_local_amount NUMERIC;
   an_city_amount NUMERIC;
   an_city_local_amount NUMERIC;
   an_state_rate NUMERIC;
   an_state_use_tier2_rate NUMERIC;
   an_state_use_tier3_rate NUMERIC;
   an_state_split_amount NUMERIC;
   an_state_tier2_min_amount NUMERIC;
   an_state_tier2_max_amount NUMERIC;
   an_state_maxtax_amount NUMERIC;
   an_county_rate NUMERIC;
   an_county_local_rate NUMERIC;
   an_county_split_amount NUMERIC;
   an_county_maxtax_amount NUMERIC;
   ac_county_single_flag CHAR;
   ac_county_default_flag CHAR;
   an_city_rate NUMERIC;
   an_city_local_rate NUMERIC;
   an_city_split_amount NUMERIC;
   an_city_split_rate NUMERIC;
   ac_city_single_flag CHAR;
   ac_city_default_flag CHAR;
   an_combined_rate NUMERIC;
   an_tb_calc_tax_amt NUMERIC;
   an_taxable_amt NUMERIC;
   av_taxtype_used VARCHAR(10);
   av_return_vals VARCHAR(4000) := '';
-- ****** Program starts ****** --------------------------------------------------------------------
BEGIN
   -- Search for Jurisdiction Tax Rates
   OPEN jurisdiction_taxrate_cursor FOR
        SELECT /*+ INDEX (TB_JURISDICTION_TAXRATE)*/
               jurisdiction_taxrate_id, state_sales_rate, state_use_rate, state_use_tier2_rate, state_use_tier3_rate,
               state_split_amount, state_tier2_min_amount, state_tier2_max_amount, state_maxtax_amount,
               county_sales_rate, county_use_rate, county_local_sales_rate, county_local_use_rate,
               county_split_amount, county_maxtax_amount, county_single_flag, county_default_flag,
               city_sales_rate, city_use_rate, city_local_sales_rate, city_local_use_rate,
               city_split_amount, city_split_sales_rate, city_split_use_rate, city_single_flag, city_default_flag
          FROM tb_jurisdiction_taxrate
         WHERE jurisdiction_id = an_jurisdiction_id AND
               effective_date <= ad_gl_date AND
               expiration_date >= ad_gl_date AND
               measure_type_code = av_measure_type_code
        ORDER BY effective_date DESC;
   
   FETCH jurisdiction_taxrate_cursor
    INTO jurisdiction_taxrate;
   
   -- If rows found, set to 1 to indicate success
   IF (FOUND) THEN
      vn_fetch_rows := 1;
   ELSE
      vn_fetch_rows := 0;
   END IF;
   
   CLOSE jurisdiction_taxrate_cursor;
   
   IF vn_fetch_rows > 0 THEN
      -- Determine Tax Type - Use or Sales
      av_taxtype_used := av_override_taxtype_code;
      IF av_taxtype_used is NULL OR TRIM(av_taxtype_used) = '' OR TRIM(av_taxtype_used) = '*NO' THEN
         av_taxtype_used := SUBSTR(TRIM(av_taxtype_code),1,1);
         IF av_taxtype_used is NULL or Trim(av_taxtype_used) = '' THEN
            av_taxtype_used := 'U';
         END IF;
      END IF;

      -- Move Rates to parameters
      an_jurisdiction_taxrate_id := jurisdiction_taxrate.jurisdiction_taxrate_id;
      an_county_split_amount := jurisdiction_taxrate.county_split_amount;
      an_county_maxtax_amount := jurisdiction_taxrate.county_maxtax_amount;
      ac_county_single_flag := jurisdiction_taxrate.county_single_flag;
      ac_county_default_flag := jurisdiction_taxrate.county_default_flag;
      an_city_split_amount := jurisdiction_taxrate.city_split_amount;
      ac_city_single_flag := jurisdiction_taxrate.city_single_flag;
      ac_city_default_flag := jurisdiction_taxrate.city_default_flag;

      IF av_taxtype_used = 'U' THEN
         -- Use Tax Rates
         an_state_rate := jurisdiction_taxrate.state_use_rate;
         an_state_use_tier2_rate := jurisdiction_taxrate.state_use_tier2_rate;
         an_state_use_tier3_rate := jurisdiction_taxrate.state_use_tier3_rate;
         an_state_split_amount := jurisdiction_taxrate.state_split_amount;
         an_state_tier2_min_amount := jurisdiction_taxrate.state_tier2_min_amount;
         an_state_tier2_max_amount := jurisdiction_taxrate.state_tier2_max_amount;
         an_state_maxtax_amount := jurisdiction_taxrate.state_maxtax_amount;
         an_county_rate := jurisdiction_taxrate.county_use_rate;
         an_county_local_rate := jurisdiction_taxrate.county_local_use_rate;
         an_city_rate := jurisdiction_taxrate.city_use_rate;
         an_city_local_rate := jurisdiction_taxrate.city_local_use_rate;
         an_city_split_rate := jurisdiction_taxrate.city_split_use_rate;
      ELSIF av_taxtype_used = 'S' THEN
         -- Sales Tax Rates
         an_state_rate := jurisdiction_taxrate.state_sales_rate;
         an_state_use_tier2_rate := 0;
         an_state_use_tier3_rate := 0;
         an_state_split_amount := 0;
         an_state_tier2_min_amount := 0;
         an_state_tier2_max_amount := 999999999;
         an_state_maxtax_amount := 999999999;
         an_county_rate := jurisdiction_taxrate.county_sales_rate;
         an_county_local_rate := jurisdiction_taxrate.county_local_sales_rate;
         an_city_rate := jurisdiction_taxrate.city_sales_rate;
         an_city_local_rate := jurisdiction_taxrate.city_local_sales_rate;
         an_city_split_rate := jurisdiction_taxrate.city_split_sales_rate;
      ELSE
         -- Unknown!
         an_state_rate := 0;
         an_state_use_tier2_rate := 0;
         an_state_use_tier3_rate := 0;
         an_state_split_amount := 0;
         an_state_tier2_min_amount := 0;
         an_state_tier2_max_amount := 999999999;
         an_state_maxtax_amount := 999999999;
         an_county_rate := 0;
         an_county_local_rate := 0;
         an_city_rate := 0;
         an_city_local_rate := 0;
         an_city_split_rate := 0;
      END IF;

      -- Calculate Sales/Use Tax Amounts
      EXECUTE sp_calcusetax(an_gl_line_itm_dist_amt,
                    an_invoice_freight_amt,
                    an_invoice_discount_amt,
                    av_transaction_state_code,
                    av_import_definition_code,
                    av_taxcode_code,
		    ac_state_flag,
                    ac_county_flag,
                    ac_county_local_flag,
                    ac_city_flag,
                    ac_city_local_flag,
		    an_state_rate,
                    an_state_use_tier2_rate,
                    an_state_use_tier3_rate,
                    an_state_split_amount,
                    an_state_tier2_min_amount,
                    an_state_tier2_max_amount,
                    an_state_maxtax_amount,
                    an_county_rate,
                    an_county_local_rate,
                    an_county_split_amount,
                    an_county_maxtax_amount,
                    ac_county_single_flag,
                    ac_county_default_flag,
                    an_city_rate,
                    an_city_local_rate,
                    an_city_split_amount,
                    an_city_split_rate,
                    ac_city_single_flag,
                    ac_city_default_flag,
                    an_state_amount,
                    an_state_use_tier2_amount,
                    an_state_use_tier3_amount,
                    an_county_amount,
                    an_county_local_amount,
                    an_city_amount,
                    an_city_local_amount,
                    an_tb_calc_tax_amt,
                    an_taxable_amt );

      -- Calculate Combined Sales/Use Rate
      an_combined_rate := an_state_rate +
                          an_county_rate +
                          an_county_local_rate +
                          an_city_rate +
                          an_city_local_rate;
   ELSE
      an_jurisdiction_taxrate_id := 0;
   END IF;

   EXCEPTION
      WHEN OTHERS THEN
         an_jurisdiction_taxrate_id := 0;
         av_return_vals := TO_CHAR(an_jurisdiction_taxrate_id) || ',' ||
                    TO_CHAR(an_state_amount) || ',' ||
                    TO_CHAR(an_state_use_tier2_amount) || ',' ||
                    TO_CHAR(an_state_use_tier3_amount) || ',' ||
                    TO_CHAR(an_county_amount) || ',' ||
                    TO_CHAR(an_county_local_amount) || ',' ||
                    TO_CHAR(an_city_amount) || ',' ||
                    TO_CHAR(an_city_local_amount) || ',' ||
                    TO_CHAR(an_state_rate) || ',' ||
                    TO_CHAR(an_state_use_tier2_rate) || ',' ||
                    TO_CHAR(an_state_use_tier3_rate) || ',' ||
                    TO_CHAR(an_state_split_amount) || ',' ||
                    TO_CHAR(an_state_tier2_min_amount) || ',' ||
                    TO_CHAR(an_state_tier2_max_amount) || ',' ||
                    TO_CHAR(an_state_maxtax_amount) || ',' ||
                    TO_CHAR(an_county_rate) || ',' ||
                    TO_CHAR(an_county_local_rate) || ',' ||
                    TO_CHAR(an_county_split_amount) || ',' ||
                    TO_CHAR(an_county_maxtax_amount) || ',' ||
                    TO_CHAR(ac_county_single_flag) || ',' ||
                    TO_CHAR(ac_county_default_flag) || ',' ||
                    TO_CHAR(an_city_rate) || ',' ||
                    TO_CHAR(an_city_local_rate) || ',' ||
                    TO_CHAR(an_city_split_amount) || ',' ||
                    TO_CHAR(an_city_split_rate) || ',' ||
                    TO_CHAR(ac_city_single_flag) || ',' ||
                    TO_CHAR(ac_city_default_flag) || ',' ||
                    TO_CHAR(an_combined_rate) || ',' ||
                    TO_CHAR(an_tb_calc_tax_amt) || ',' ||
                    TO_CHAR(an_taxable_amt) || ',' ||
                    TO_CHAR(av_taxtype_used);
   RETURN av_return_vals;  
         
END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
ALTER FUNCTION f_calc_taxes(numeric, date, character varying, numeric, character varying, character varying, character, character, character, character, character, numeric, numeric, character varying, character varying, character varying) OWNER TO stscorp;
