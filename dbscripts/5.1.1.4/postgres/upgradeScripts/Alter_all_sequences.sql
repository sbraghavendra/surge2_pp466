CREATE OR REPLACE FUNCTION all_sequences(IN sequence_name character varying)
  RETURNS numeric AS
$BODY$
/* ************************************************************************************************/
/* Object Type/Name: Stored Procedure - sp_all_sequences                                          */
/* Author:           Srini D                                                                     */
/* Date:             02/14/2011                                                                  */
/* Description:      Get the last value of sequence                                              */
/* Arguments:        None                                                                         */
/* Returns:          None                                                                         */
/* ************************************************************************************************/
/* Maintenance History:                                                                           */
/* Scan  Author       Date     Release    Description                                  Ticket #   */
/* Srini	      02/14/2011         Inial Version						  */
/* ----- ---------- ---------- ---------- -------------------------------------------- ---------- */
DECLARE
	

		v_SQ_TB_JURISDICTION_TAXRATE_ID    numeric;
		v_SQ_TB_LOCATION_MATRIX_ID    numeric;
		v_SQ_GL_EXTRACT_BATCH_ID    numeric;
		v_SQ_TB_ALLOCATION_MATRIX_ID    numeric;
		v_SQ_TB_ALLOCATION_SUBTRANS_ID    numeric;
		v_SQ_TB_BATCH_ERROR_LOG_ID    numeric;
		v_SQ_TB_BATCH_ID    numeric;
		v_SQ_TB_ENTITY_ID    numeric;
		v_SQ_TB_GL_EXTRACT_MAP_ID    numeric;
		v_SQ_TB_JURISDICTION_ID    numeric;
		v_SQ_TB_PROCESS_COUNT    numeric;
		v_SQ_TB_QUESTIONANSWER_LOG_ID    numeric;
		v_SQ_TB_TAX_MATRIX_ID    numeric;
		v_SQ_TB_TAXCODE_DETAIL_ID    numeric;
		v_SQ_TB_TRANS_DETAIL_EXCP_ID    numeric;
		v_SQ_TB_TRANSACTION_DETAIL_ID    numeric;
		v_SQ_TB_BCP_JURIS_TAXRATE_ID    numeric;
		v_SQ_TB_BCP_TRANSACTIONS    numeric;
		v_last_number		    numeric;

BEGIN
		IF UPPER(sequence_name)='SQ_TB_JURISDICTION_TAXRATE_ID' THEN
			select coalesce(max(JURISDICTION_TAXRATE_ID),0)
			INTO STRICT v_last_number
                        from TB_JURISDICTION_TAXRATE;
                ELSIF UPPER(sequence_name)='SQ_TB_LOCATION_MATRIX_ID' THEN
			 select coalesce(max(LOCATION_MATRIX_ID),0)
                        INTO STRICT v_last_number
                        from TB_LOCATION_MATRIX;
                ELSIF UPPER(sequence_name)='SQ_GL_EXTRACT_BATCH_ID' THEN
			select coalesce(max(GL_EXTRACT_MAP_ID),0)
			INTO STRICT v_last_number
			from TB_GL_EXTRACT_map;                 
		ELSIF UPPER(sequence_name)='SQ_TB_ALLOCATION_MATRIX_ID' THEN
			select coalesce(max(ALLOCATION_MATRIX_ID),0)
                        INTO STRICT v_last_number
			from TB_ALLOCATION_MATRIX;
                ELSIF UPPER(sequence_name)='SQ_TB_BATCH_ERROR_LOG_ID' THEN    
			select coalesce(max(BATCH_ERROR_LOG_ID),0)
			INTO STRICT v_last_number
			from TB_BATCH_ERROR_LOG;            
                ELSIF UPPER(sequence_name)='SQ_TB_BATCH_ID' THEN 
			select coalesce(max(BATCH_ID),0)
                        INTO STRICT v_last_number
                        from TB_BATCH;
		ELSIF UPPER(sequence_name)='SQ_TB_ENTITY_ID' THEN 
                     select coalesce(max(ENTITY_ID),0)
                         INTO STRICT v_last_number
                         from TB_ENTITY;
                ELSIF UPPER(sequence_name)='SQ_TB_GL_EXTRACT_MAP_ID' THEN           
			select coalesce(max(GL_EXTRACT_MAP_ID),0)
                         INTO STRICT v_last_number
                         from TB_GL_EXTRACT_MAP;
		ELSIF UPPER(sequence_name)='SQ_TB_JURISDICTION_ID' THEN    
			 select coalesce(max(JURISDICTION_ID),0) 
                         INTO STRICT v_last_number
                         from TB_JURISDICTION;
                ELSIF UPPER(sequence_name)='SQ_TB_TRANS_DETAIL_EXCP_ID' THEN
			select coalesce(max(TRANSACTION_DETAIL_EXCP_ID),0)
                       INTO STRICT v_last_number
                       from TB_TRANSACTION_DETAIL_EXCP;              
                ELSIF UPPER(sequence_name)='SQ_TB_TAX_MATRIX_ID' THEN    
			 select coalesce(max(TAX_MATRIX_ID),0)
                         INTO STRICT v_last_number
                         from TB_TAX_MATRIX;
                ELSIF UPPER(sequence_name)='SQ_TB_TAXCODE_DETAIL_ID' THEN    
                         select coalesce(max(TAXCODE_DETAIL_ID),0)
                        INTO STRICT  v_last_number
                         from TB_TAXCODE_DETAIL;
                ELSIF UPPER(sequence_name)='SQ_TB_TRANS_DETAIL_EXCP_ID' THEN
                        select coalesce(max(TRANSACTION_DETAIL_EXCP_ID),0) 
                       INTO STRICT v_last_number
                       from TB_TRANSACTION_DETAIL_EXCP;
               ELSIF UPPER(sequence_name)='SQ_TB_TRANSACTION_DETAIL_ID' THEN
			select coalesce(max(TRANSACTION_DETAIL_ID),0) 
                        INTO STRICT v_last_number
                        from TB_TRANSACTION_DETAIL; 
		ELSIF UPPER(sequence_name)='SQ_TB_BCP_JURIS_TAXRATE_ID' THEN
			select coalesce(max(BCP_JURISDICTION_TAXRATE_ID),0)
			INTO STRICT v_last_number
			from TB_BCP_JURISDICTION_TAXRATE;                       
                 ELSIF UPPER(sequence_name)='SQ_TB_BCP_TRANSACTIONS' THEN   
                      select coalesce(max(BCP_TRANSACTION_ID),0)
                      INTO STRICT v_last_number
                       from TB_BCP_TRANSACTIONS;
                ELSE
                   v_last_number :=-1;
                END IF;
               RETURN   v_last_number;    
                 
	
EXCEPTION
  WHEN OTHERS THEN
	RAISE WARNING 'EXCEPTION ';
END ;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;

