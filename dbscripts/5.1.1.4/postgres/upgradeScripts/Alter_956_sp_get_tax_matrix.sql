CREATE OR REPLACE FUNCTION sp_get_tax_matrix(p_transaction_detail_id numeric)
  RETURNS REFCURSOR AS
$BODY$
DECLARE
/* ************************************************************************************************/
/* Object Type/Name: sp_get_tax_matrix					                            */
/* Author:           Michael B. Fuller                                                            */
/* Date:             a long time ago                                                              */
/* Description:      A transaction has been changed                                               */
/* Arguments:        None                                                                         */
/* Returns:          None                                                                         */
/* ************************************************************************************************/
/* Maintenance History:                                                                           */
/* Scan  Author       Date     Release    Description                                  Ticket #   */
/* ----- ---------- ---------- ---------- -------------------------------------------- ---------- */
/*  SRINI            02/05/2011            Initial Version                              956      */
/* ************************************************************************************************/
-- Tax Matrix Selection SQL
   vc_tax_matrix_select            VARCHAR(2000) :=
      'SELECT tb_tax_matrix.tax_matrix_id, tb_tax_matrix.relation_sign, tb_tax_matrix.relation_amount, ' ||
             'tb_tax_matrix.then_hold_code_flag, tb_tax_matrix.else_hold_code_flag, ' ||
             'tb_tax_matrix.then_taxcode_detail_id, tb_tax_matrix.else_taxcode_detail_id, ' ||
             'tb_tax_matrix.then_cch_taxcat_code, tb_tax_matrix.then_cch_group_code, tb_tax_matrix.then_cch_item_code, ' ||
             'tb_tax_matrix.else_cch_taxcat_code, tb_tax_matrix.else_cch_group_code, tb_tax_matrix.else_cch_item_code, ' ||
             'thentaxcddtl.taxcode_state_code, thentaxcddtl.taxcode_type_code, thentaxcddtl.taxcode_code, thentaxcddtl.jurisdiction_id, thentaxcddtl.measure_type_code, thentaxcddtl.taxtype_code, ' ||
             'elsetaxcddtl.taxcode_state_code, elsetaxcddtl.taxcode_type_code, elsetaxcddtl.taxcode_code, elsetaxcddtl.jurisdiction_id, elsetaxcddtl.measure_type_code, elsetaxcddtl.taxtype_code ' ||
       'FROM tb_tax_matrix, ' ||
            'tb_taxcode_detail thentaxcddtl, ' ||
            'tb_taxcode_detail elsetaxcddtl, ' ||
            'tb_tmp_transaction_detail ' ||
      'WHERE ( tb_tmp_transaction_detail.transaction_detail_id = ( SELECT CAST (param_value AS NUMERIC) FROM sp_gen_tax_driver_tmp WHERE param_name = ''v_transaction_detail_id'' ) ) AND '  ||
            '( tb_tax_matrix.default_flag = ''0'' AND tb_tax_matrix.binary_weight > 0 ) AND ' ||
            '(( tb_tax_matrix.matrix_state_code = ''*ALL'' ) OR ( tb_tax_matrix.matrix_state_code = ( SELECT param_value FROM sp_gen_tax_driver_tmp WHERE param_name = ''v_transaction_state_code'' ) )) AND '  ||
            '( tb_tax_matrix.effective_date <= tb_tmp_transaction_detail.gl_date ) AND ( tb_tax_matrix.expiration_date >= tb_tmp_transaction_detail.gl_date ) AND ' ||
            '( tb_tax_matrix.then_taxcode_detail_id = thentaxcddtl.taxcode_detail_id (+)) AND ' ||
            '( tb_tax_matrix.else_taxcode_detail_id = elsetaxcddtl.taxcode_detail_id (+)) ';
   vc_tax_matrix_where             VARCHAR(28000) := '';
   vc_tax_matrix_orderby           VARCHAR(1000) :=
      'ORDER BY tb_tax_matrix.binary_weight DESC, tb_tax_matrix.significant_digits DESC, tb_tax_matrix.effective_date DESC';
   vc_tax_matrix_stmt              VARCHAR (31000);
   tax_matrix_cursor               refcursor;
   tax_matrix                      record;
   p_new_tb_transaction_detail     tb_tmp_transaction_detail_b_u%ROWTYPE;
   v_transaction_detail_id         tb_transaction_detail.transaction_detail_id%TYPE  := NULL;
   v_transaction_state_code        tb_transaction_detail.transaction_state_code%TYPE := NULL;

-- Program defined variables
   vn_fetch_rows                   NUMERIC                                            := 0;
   vc_state_driver_flag            CHAR(1)                                           := '0';
   vn_taxable_amt                  NUMERIC                                            := 0;
  -- e_badread                       exception;
BEGIN
  SELECT * INTO STRICT p_new_tb_transaction_detail FROM tb_tmp_transaction_detail_b_u
    WHERE transaction_detail_id = p_transaction_detail_id;
         v_transaction_detail_id := p_new_tb_transaction_detail.transaction_detail_id;
         v_transaction_state_code := p_new_tb_transaction_detail.transaction_state_code;
         IF v_transaction_state_code IS NULL THEN
            vn_fetch_rows := 0;
         ELSE
            SELECT count(*)
              INTO STRICT vn_fetch_rows
              FROM tb_taxcode_state
             WHERE taxcode_state_code = v_transaction_state_code;
         END IF;
         IF vn_fetch_rows = 0 THEN
            v_transaction_state_code := NULL;
            BEGIN
               SELECT state
                 INTO STRICT v_transaction_state_code
                 FROM tb_jurisdiction
                WHERE jurisdiction_id = p_new_tb_transaction_detail.jurisdiction_id;
               --IF SQLCODE != 0 THEN
               --   RAISE e_badread;
               --END IF;
            EXCEPTION
               WHEN OTHERS THEN
                  v_transaction_state_code := NULL;
              -- WHEN NO_DATA_FOUND THEN
                --  v_transaction_state_code := NULL;
            END;
            IF v_transaction_state_code IS NOT NULL THEN
               IF p_new_tb_transaction_detail.transaction_state_code IS NULL THEN
                  p_new_tb_transaction_detail.auto_transaction_state_code := '*NULL';
               ELSE
                  p_new_tb_transaction_detail.auto_transaction_state_code := p_new_tb_transaction_detail.transaction_state_code;
               END IF;
               p_new_tb_transaction_detail.transaction_state_code := v_transaction_state_code;
            ELSE
               p_new_tb_transaction_detail.transaction_ind := 'S';
               p_new_tb_transaction_detail.suspend_ind := 'L';
            END IF;
         END IF;
         IF p_new_tb_transaction_detail.transaction_ind IS NULL OR p_new_tb_transaction_detail.transaction_ind <> 'S' THEN

		select  sp_gen_tax_driver (
						'N',
						NULL,
						'tb_tmp_transaction_detail_b_u',
						'tb_tax_matrix',
						vc_state_driver_flag,
						vc_tax_matrix_where) 
						into vc_tax_matrix_where; 
   
   IF vc_tax_matrix_where IS NULL THEN
               NULL;
            ELSE
               vc_tax_matrix_stmt := vc_tax_matrix_select || vc_tax_matrix_where || vc_tax_matrix_orderby;
            END IF;
          
            BEGIN
                 -- ****** CREATE Temporary Table to hold "Bind" Values ****** --
	       BEGIN
		   EXECUTE 'CREATE TEMPORARY TABLE sp_gen_tax_driver_tmp (param_name varchar(30), param_value varchar(30))';
	       EXCEPTION 
		   -- Table may already exist if using Connection/Session Pooling, Ignore
		   WHEN OTHERS THEN NULL;
	       END;

	       -- Prep TMP table with values
	       DELETE FROM sp_gen_tax_driver_tmp;
	       INSERT INTO sp_gen_tax_driver_tmp(param_name, param_value) VALUES ('v_transaction_detail_id',  v_transaction_detail_id);
	       INSERT INTO sp_gen_tax_driver_tmp(param_name, param_value) VALUES ('v_transaction_state_code', v_transaction_state_code);

               
               IF  vc_state_driver_flag = '1' THEN
              
                 OPEN tax_matrix_cursor 
                 FOR EXECUTE  vc_tax_matrix_stmt;
                  --USING v_transaction_detail_id, v_transaction_state_code, v_transaction_state_code;
                FETCH tax_matrix_cursor
                INTO tax_matrix;
                         
               ELSE
            
                  OPEN tax_matrix_cursor
                   FOR EXECUTE  vc_tax_matrix_stmt;
                  --USING v_transaction_detail_id, v_transaction_state_code;
                 FETCH tax_matrix_cursor
                 INTO tax_matrix;
               END IF;
               --*/
               -- Rows found?
               IF (FOUND) THEN
                  vn_fetch_rows := tax_matrix_cursor%ROWCOUNT;
               ELSE
                  vn_fetch_rows := 0;
               END IF;
               CLOSE tax_matrix_cursor;
               BEGIN
		   EXECUTE 'DROP TABLE sp_gen_tax_driver_tmp ';
	       EXCEPTION 
		   WHEN OTHERS THEN NULL;
	       END;

               --IF SQLCODE != 0 THEN
               --   RAISE e_badread;
               --END IF;
            EXCEPTION
               WHEN OTHERS THEN
                  vn_fetch_rows := 0;
            END;
      ELSE
        OPEN tax_matrix_cursor
          FOR select * from tb_tax_matrix where tax_matrix_id < 1;
          FETCH tax_matrix_cursor
          INTO tax_matrix;
      END IF;
END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
ALTER FUNCTION sp_get_tax_matrix(numeric) OWNER TO stscorp;
