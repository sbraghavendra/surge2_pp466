-- Function: sp_tb_location_matrix_a_i(numeric)

-- DROP FUNCTION sp_tb_location_matrix_a_i(numeric);

CREATE OR REPLACE FUNCTION sp_tb_location_matrix_a_i(p_location_matrix_id numeric)
  RETURNS void AS
$BODY$
DECLARE
/* ************************************************************************************************/
/* Object Type/Name: SP Trigger/After Insert - sp_tb_location_matrix_a_i                          */
/* Author:           Michael B. Fuller                                                            */
/* Date:             a long time ago                                                              */
/* Description:      A Location Matrix line has been added                                        */
/* Arguments:        None                                                                         */
/* Returns:          None                                                                         */
/* ************************************************************************************************/
/* Maintenance History:                                                                           */
/* Scan  Author       Date     Release    Description                                  Ticket #   */
/* ----- ---------- ---------- ---------- -------------------------------------------- ---------- */
/*       M. Fuller  06/02/2006 3.3.5.0    Fix processing an allocated line - Temporary 871        */
/*       M. Fuller  01/04/2007 3.3.5.1    Fix processing an allocated line - Permenant 871        */
/* MBF01 M. Fuller  08/14/2007 3.4.1.1    Add 5 Calc Tax Flags                         883        */
/* MBF01 J. Franco  12/16/2008 x.x.x.x    Convert Trigger to SP                        xxx        */
/* SRINI J. Franco  01/17/2011 x.x.x.x    Add UTC timetsamp  CHG#949                   949        */
/* SRINI J. Franco  01/25/2011 x.x.x.x    Set effective date transaction gl_date       952        */
/* SRINI J. Franco  01/25/2011 x.x.x.x    Remove effective date transaction gl_date    953        */
/* ************************************************************************************************/

-- Define Row Type
   l_tb_location_matrix tb_location_matrix%ROWTYPE;

-- Transaction Detail Selection SQL
   vc_transaction_detail_select    VARCHAR(2000) :=
      'SELECT tb_transaction_detail.* ' ||
        'FROM tb_tmp_location_matrix, ' ||
             'tb_transaction_detail '   ||
       'WHERE ( tb_tmp_location_matrix.location_matrix_id = ' || p_location_matrix_id || ') AND ' ||
             '( tb_tmp_location_matrix.effective_date <= tb_transaction_detail.gl_date ) AND ( tb_tmp_location_matrix.expiration_date >= tb_transaction_detail.gl_date ) AND ' ||
             '( tb_transaction_detail.transaction_ind = ''S'' ) AND ( tb_transaction_detail.suspend_ind = ''L'' ) ';
   
   vc_transaction_detail_where     VARCHAR(5000) := '';
   vc_transaction_detail_update    VARCHAR(100) := 'FOR UPDATE';
   vc_transaction_detail_stmt      VARCHAR(7100);
   
   transaction_detail_cursor       REFCURSOR;
   transaction_detail              RECORD;

-- Tax Matrix Selection SQL
   vc_tax_matrix_select            VARCHAR(2000) :=
	'SELECT	tb_tax_matrix.tax_matrix_id,  '||
	'	tb_tax_matrix.relation_sign,  '||
	'	tb_tax_matrix.relation_amount, '||
	'	tb_tax_matrix.then_hold_code_flag,  '||
	'	tb_tax_matrix.else_hold_code_flag, '||
	'	tb_tax_matrix.then_taxcode_detail_id,  '||
	'	tb_tax_matrix.else_taxcode_detail_id, '||
	'	tb_tax_matrix.then_cch_taxcat_code,  '||
	'	tb_tax_matrix.then_cch_group_code,  '||
	'	tb_tax_matrix.then_cch_item_code, '||
	'	tb_tax_matrix.else_cch_taxcat_code,  '||
	'	tb_tax_matrix.else_cch_group_code,  '||
	'	tb_tax_matrix.else_cch_item_code, '||
	'	thentaxcddtl.taxcode_state_code,  '||
	'	thentaxcddtl.taxcode_type_code,  '||
	'	thentaxcddtl.taxcode_code,  '||
	'	thentaxcddtl.jurisdiction_id,  '||
	'	thentaxcddtl.measure_type_code,  '||
	'	thentaxcddtl.taxtype_code, '||
	'	elsetaxcddtl.taxcode_state_code,  '||
	'	elsetaxcddtl.taxcode_type_code,  '||
	'	elsetaxcddtl.taxcode_code,  '||
	'	elsetaxcddtl.jurisdiction_id,  '||
	'	elsetaxcddtl.measure_type_code,  '||
	'	elsetaxcddtl.taxtype_code '||
	'FROM	tb_transaction_detail  '||
	'JOIN	tb_tax_matrix ON ( '||
	'		tb_transaction_detail.transaction_detail_id = (SELECT CAST (param_value AS NUMERIC) FROM sp_gen_tax_driver_tmp WHERE param_name = ''v_transaction_detail_id'')  '||
	'	AND	tb_tax_matrix.default_flag  = ''0''  '||
	'	AND	tb_tax_matrix.binary_weight >  0  '||
	'	AND	( '||
	'		tb_tax_matrix.matrix_state_code = ''*ALL'' OR  '||
	'		tb_tax_matrix.matrix_state_code = (SELECT param_value FROM sp_gen_tax_driver_tmp WHERE param_name = ''v_transaction_state_code'')  '||
	'		)  '||
	'	AND	tb_tax_matrix.effective_date  <= tb_transaction_detail.gl_date '||
	'	AND	tb_tax_matrix.expiration_date >= tb_transaction_detail.gl_date ) '||
	'LEFT OUTER JOIN tb_taxcode_detail thentaxcddtl ON (tb_tax_matrix.then_taxcode_detail_id = thentaxcddtl.taxcode_detail_id) '||
	'LEFT OUTER JOIN tb_taxcode_detail elsetaxcddtl ON (tb_tax_matrix.else_taxcode_detail_id = elsetaxcddtl.taxcode_detail_id) ';

   vc_tax_matrix_where             VARCHAR(6000) := '';
   vc_tax_matrix_orderby           VARCHAR(1000) :=
      'ORDER BY tb_tax_matrix.binary_weight DESC, tb_tax_matrix.significant_digits DESC, tb_tax_matrix.effective_date DESC';
   
   vc_tax_matrix_stmt              VARCHAR (9000);
   tax_matrix_cursor               REFCURSOR;

-- Tax Matrix Record TYPE
   /*TYPE tax_matrix_record is RECORD (
      tax_matrix_id                tb_tax_matrix.tax_matrix_id%TYPE,
      relation_sign                tb_tax_matrix.relation_sign%TYPE,
      relation_amount              tb_tax_matrix.relation_amount%TYPE,
      then_hold_code_flag          tb_tax_matrix.then_hold_code_flag%TYPE,
      else_hold_code_flag          tb_tax_matrix.else_hold_code_flag%TYPE,
      then_taxcode_detail_id       tb_bcp_transactions.taxcode_detail_id%TYPE,
      else_taxcode_detail_id       tb_bcp_transactions.taxcode_detail_id%TYPE,
      then_cch_taxcat_code         tb_tax_matrix.then_cch_taxcat_code%TYPE,
      then_cch_group_code          tb_tax_matrix.then_cch_group_code%TYPE,
      then_cch_item_code           tb_tax_matrix.then_cch_item_code%TYPE,
      else_cch_taxcat_code         tb_tax_matrix.else_cch_taxcat_code%TYPE,
      else_cch_group_code          tb_tax_matrix.else_cch_group_code%TYPE,
      else_cch_item_code           tb_tax_matrix.else_cch_item_code%TYPE,
      then_taxcode_state_code      tb_bcp_transactions.taxcode_state_code%TYPE,
      then_taxcode_type_code       tb_bcp_transactions.taxcode_type_code%TYPE,
      then_taxcode_code            tb_bcp_transactions.taxcode_code%TYPE,
      then_jurisdiction_id         tb_bcp_transactions.jurisdiction_id%TYPE,
      then_measure_type_code       tb_bcp_transactions.measure_type_code%TYPE,
      then_taxtype_code            tb_taxcode.taxtype_code%TYPE,
      else_taxcode_state_code      tb_bcp_transactions.taxcode_state_code%TYPE,
      else_taxcode_type_code       tb_bcp_transactions.taxcode_type_code%TYPE,
      else_taxcode_code            tb_bcp_transactions.taxcode_code%TYPE,
      else_jurisdiction_id         tb_bcp_transactions.jurisdiction_id%TYPE,
      else_measure_type_code       tb_bcp_transactions.measure_type_code%TYPE,
      else_taxtype_code            tb_taxcode.taxtype_code%TYPE );*/
   tax_matrix                      RECORD;

-- Table defined variables
   v_hold_code_flag                tb_tax_matrix.then_hold_code_flag%TYPE            := NULL;
   v_less_hold_code_flag           tb_tax_matrix.then_hold_code_flag%TYPE            := NULL;
   v_less_taxcode_detail_id        tb_tax_matrix.then_taxcode_detail_id%TYPE         := NULL;
   v_less_cch_taxcat_code          tb_tax_matrix.then_cch_taxcat_code%TYPE           := NULL;
   v_less_cch_group_code           tb_tax_matrix.then_cch_group_code%TYPE            := NULL;
   v_equal_hold_code_flag          tb_tax_matrix.then_hold_code_flag%TYPE            := NULL;
   v_equal_taxcode_detail_id       tb_tax_matrix.then_taxcode_detail_id%TYPE         := NULL;
   v_equal_cch_taxcat_code         tb_tax_matrix.then_cch_taxcat_code%TYPE           := NULL;
   v_equal_cch_group_code          tb_tax_matrix.then_cch_group_code%TYPE            := NULL;
   v_greater_hold_code_flag        tb_tax_matrix.then_hold_code_flag%TYPE            := NULL;
   v_greater_taxcode_detail_id     tb_tax_matrix.then_taxcode_detail_id%TYPE         := NULL;
   v_greater_cch_taxcat_code       tb_tax_matrix.then_cch_taxcat_code%TYPE           := NULL;
   v_greater_cch_group_code        tb_tax_matrix.then_cch_group_code%TYPE            := NULL;
   v_jurisdiction_id               tb_jurisdiction.jurisdiction_id%TYPE              := NULL;
   v_less_taxcode_state_code       tb_taxcode_detail.taxcode_state_code%TYPE         := NULL;
   v_less_taxcode_type_code        tb_taxcode_detail.taxcode_type_code%TYPE          := NULL;
   v_less_taxcode_code             tb_taxcode_detail.taxcode_code%TYPE               := NULL;
   v_less_jurisdiction_id          tb_taxcode_detail.jurisdiction_id%TYPE            := NULL;
   v_less_measure_type_code        tb_taxcode_detail.measure_type_code%TYPE          := NULL;
   v_less_taxtype_code             tb_taxcode_detail.taxtype_code%TYPE               := NULL;
   v_equal_taxcode_state_code      tb_taxcode_detail.taxcode_state_code%TYPE         := NULL;
   v_equal_taxcode_type_code       tb_taxcode_detail.taxcode_type_code%TYPE          := NULL;
   v_equal_taxcode_code            tb_taxcode_detail.taxcode_code%TYPE               := NULL;
   v_equal_jurisdiction_id         tb_taxcode_detail.jurisdiction_id%TYPE            := NULL;
   v_equal_measure_type_code       tb_taxcode_detail.measure_type_code%TYPE          := NULL;
   v_equal_taxtype_code            tb_taxcode_detail.taxtype_code%TYPE               := NULL;
   v_greater_taxcode_state_code    tb_taxcode_detail.taxcode_state_code%TYPE         := NULL;
   v_greater_taxcode_type_code     tb_taxcode_detail.taxcode_type_code%TYPE          := NULL;
   v_greater_taxcode_code          tb_taxcode_detail.taxcode_code%TYPE               := NULL;
   v_greater_jurisdiction_id       tb_taxcode_detail.jurisdiction_id%TYPE            := NULL;
   v_greater_measure_type_code     tb_taxcode_detail.measure_type_code%TYPE          := NULL;
   v_greater_taxtype_code          tb_taxcode_detail.taxtype_code%TYPE               := NULL;
   v_transaction_detail_id         tb_transaction_detail.transaction_detail_id%TYPE  := NULL;
   v_transaction_state_code        tb_transaction_detail.transaction_state_code%TYPE := NULL;
   v_override_jurisdiction_id      tb_transaction_detail.jurisdiction_id%TYPE        := NULL;
   v_sysdate                       tb_transaction_detail.load_timestamp%TYPE         := SYS_EXTRACT_UTC(CURRENT_TIMESTAMP);/****UTC Timestamp ##SRINI###*****/
   v_taxtype_code                  tb_taxcode.taxtype_code%TYPE                      := NULL;
   v_taxtype_used                  tb_taxcode.taxtype_code%TYPE                      := NULL;

-- Program defined variables
   vn_hold_trans_amount            NUMERIC                                            := 0;
   vn_fetch_rows                   NUMERIC                                            := 0;
   vi_error_count                  INTEGER                                           := 0;
   vn_actual_rowno                 NUMERIC                                            := 0;
   vc_state_driver_flag            CHAR(1)                                           := '0';
-- temporary
   vn_taxable_amt                  NUMERIC                                            := 0;

-- Define Location Driver Names Cursor (tb_driver_names)
   /*CURSOR location_driver_cursor
   IS
        SELECT driver.trans_dtl_column_name, driver.matrix_column_name, driver.null_driver_flag, driver.wildcard_flag,
               datadefcol.desc_column_name, datadefcol.data_type
          FROM tb_driver_names driver,
               (SELECT *
                  FROM tb_data_def_column
                 WHERE table_name = 'TB_TRANSACTION_DETAIL') datadefcol
         WHERE driver.driver_names_code = 'L' AND
               driver.trans_dtl_column_name = datadefcol.column_name
      ORDER BY driver.driver_id;
      location_driver              location_driver_cursor%ROWTYPE;*/

-- Define Tax Driver Names Cursor (tb_driver_names)
   /*CURSOR tax_driver_cursor
   IS
        SELECT driver.trans_dtl_column_name, driver.matrix_column_name, driver.null_driver_flag, driver.wildcard_flag, driver.range_flag, to_number_flag,
               datadefcol.desc_column_name, datadefcol.data_type
          FROM tb_driver_names driver,
               (SELECT *
                  FROM tb_data_def_column
                 WHERE table_name = 'TB_TRANSACTION_DETAIL') datadefcol
         WHERE driver.driver_names_code = 'T' AND
               driver.trans_dtl_column_name = datadefcol.column_name
      ORDER BY driver.driver_id;
      tax_driver                   tax_driver_cursor%ROWTYPE;*/

-- Define Exceptions
   --e_badread                       exception;
   --e_badupdate                     exception;

-- ****** Program starts ****** --------------------------------------------------------------------
BEGIN

-- Populate row type with location matrix record
SELECT tb_location_matrix.* 
 INTO  STRICT l_tb_location_matrix 
 FROM  tb_location_matrix
 WHERE location_matrix_id = p_location_matrix_id;

IF l_tb_location_matrix.default_flag <> '1' AND l_tb_location_matrix.binary_weight > 0 THEN

   -- ***** Create Dynamic WHERE Clause for Transaction Detail SQL ***** --
   -- Location Matrix Drivers
   /*OPEN location_driver_cursor;
   LOOP
      FETCH location_driver_cursor INTO location_driver;
      EXIT WHEN location_driver_cursor%NOTFOUND;
      IF location_driver.null_driver_flag = '1' THEN
         IF location_driver.wildcard_flag = '1' THEN
            IF UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'CHAR' OR
               UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'VARC' THEN
                  vc_transaction_detail_where := vc_transaction_detail_where || 'AND ' ||
                  '((tb_transaction_detail.' || location_driver.trans_dtl_column_name || ' IS NULL AND ( tb_tmp_location_matrix.' || location_driver.matrix_column_name || ' = ''*ALL'' OR tb_tmp_location_matrix.' || location_driver.matrix_column_name || ' = ''*NULL'' )) OR ' ||
                  '(tb_transaction_detail.' || location_driver.trans_dtl_column_name || ' IS NOT NULL AND ( tb_tmp_location_matrix.' || location_driver.matrix_column_name || ' = ''*ALL'' OR UPPER(tb_transaction_detail.' || location_driver.trans_dtl_column_name || ') LIKE UPPER(tb_tmp_location_matrix.' || location_driver.matrix_column_name || ') ))) ';
            END IF;
         ELSE
            IF UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'CHAR' OR
               UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'VARC' THEN
                  vc_transaction_detail_where := vc_transaction_detail_where || 'AND ' ||
                  '((tb_transaction_detail.' || location_driver.trans_dtl_column_name || ' IS NULL AND ( tb_tmp_location_matrix.' || location_driver.matrix_column_name || ' = ''*ALL'' OR tb_tmp_location_matrix.' || location_driver.matrix_column_name || ' = ''*NULL'' )) OR ' ||
                  '(tb_transaction_detail.' || location_driver.trans_dtl_column_name || ' IS NOT NULL AND ( tb_tmp_location_matrix.' || location_driver.matrix_column_name || ' = ''*ALL'' OR UPPER(tb_transaction_detail.' || location_driver.trans_dtl_column_name || ') = UPPER(tb_tmp_location_matrix.' || location_driver.matrix_column_name || ') ))) ';
            END IF;
         END IF;
      ELSE
         IF location_driver.wildcard_flag = '1' THEN
            IF UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'CHAR' OR
               UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'VARC' THEN
                  vc_transaction_detail_where := vc_transaction_detail_where || 'AND ' ||
                  '( tb_tmp_location_matrix.' || location_driver.matrix_column_name || ' = ''*ALL'' OR UPPER(tb_transaction_detail.' || location_driver.trans_dtl_column_name || ') LIKE UPPER(tb_tmp_location_matrix.' || location_driver.matrix_column_name || ') ) ';
            END IF;
         ELSE
            IF UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'CHAR' OR
               UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'VARC' THEN
                  vc_transaction_detail_where := vc_transaction_detail_where || 'AND ' ||
                  '( tb_tmp_location_matrix.' || location_driver.matrix_column_name || ' = ''*ALL'' OR UPPER(tb_transaction_detail.' || location_driver.trans_dtl_column_name || ') = UPPER(tb_tmp_location_matrix.' || location_driver.matrix_column_name || ') ) ';
            END IF;
         END IF;
      END IF;
   END LOOP;
   CLOSE location_driver_cursor;*/
   
   -- New call to sp_gen_location_driver for MidTier Cleanup
 /*  EXECUTE sp_gen_location_driver (
	p_generate_driver_reference	=> 'N',
	p_an_batch_id			=> NULL,
	p_transaction_table_name	=> 'tb_transaction_detail',
	p_location_table_name		=> 'tb_tmp_location_matrix',
	p_vc_location_matrix_where	=> vc_transaction_detail_where);
*/
    select 
     sp_gen_location_driver (
	'N',
	NULL,
	'tb_transaction_detail',
	'tb_tmp_location_matrix',
	vc_transaction_detail_where)
    into vc_transaction_detail_where;	

    
   -- If no drivers found raise error, else create transaction detail sql statement
   IF vc_transaction_detail_where IS NULL THEN
      NULL;
   ELSE
      vc_transaction_detail_stmt := vc_transaction_detail_select || vc_transaction_detail_where || vc_transaction_detail_update;
   END IF;

   -- *******************************************************************************************************************************************
   -- NEW BATCH PROCESS CODE --------------------------------------------------------------------------------------------------------------------
   -- ***** Create Dynamic WHERE Clause for Transaction Detail SQL ***** --
   -- Tax Matrix Drivers
   /*OPEN tax_driver_cursor;
   LOOP
      FETCH tax_driver_cursor INTO tax_driver;
      EXIT WHEN tax_driver_cursor%NOTFOUND;
      IF tax_driver.range_flag = '1' THEN
         IF tax_driver.to_number_flag = '1' THEN
            IF tax_driver.null_driver_flag = '1' THEN
               -- Range and ToNumber and NULL
               IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                  UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                  vc_tax_matrix_where := vc_tax_matrix_where || 'AND ( ' ||
                  '( tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU = ''*NA'' AND ' ||
                     '( tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NULL AND ( tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*NULL'' )) OR ' ||
                     '( tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND ( tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR DECODE(isnumber(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || '),1,to_char(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ',''00000000000000000000.00000000000000000000''),UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ')) = DECODE(isnumber(tb_tax_matrix.' || tax_driver.matrix_column_name || '),1,to_char(tb_tax_matrix.' || tax_driver.matrix_column_name || ',''00000000000000000000.00000000000000000000''),UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') ))) OR ' ||
                  '( tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU <> ''*NA'' AND ' ||
                     '( tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND (DECODE(isnumber(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || '),1,to_char(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ',''00000000000000000000.00000000000000000000''),UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ')) >= DECODE(isnumber(tb_tax_matrix.' || tax_driver.matrix_column_name || '),1,to_char(tb_tax_matrix.' || tax_driver.matrix_column_name || ',''00000000000000000000.00000000000000000000''),UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') ) AND ' ||
                                                                                            'DECODE(isnumber(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || '),1,to_char(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ',''00000000000000000000.00000000000000000000''),UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ')) <= DECODE(isnumber(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU ),1,to_char(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU,''00000000000000000000.00000000000000000000''),UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU)) )))) ';
               END IF;
            ELSE
               -- Range and ToNumber and NOT NULL
               IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                  UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                  vc_tax_matrix_where := vc_tax_matrix_where || 'AND ( ' ||
                  '( tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU = ''*NA'' AND (tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR DECODE(isnumber(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || '),1,to_char(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ',''00000000000000000000.00000000000000000000''),UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ')) = DECODE(isnumber(tb_tax_matrix.' || tax_driver.matrix_column_name || '),1,to_char(tb_tax_matrix.' || tax_driver.matrix_column_name || ',''00000000000000000000.00000000000000000000''),UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ')))) OR ' ||
                  '( tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU <> ''*NA'' AND (DECODE(isnumber(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || '),1,to_char(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ',''00000000000000000000.00000000000000000000''),UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ')) >= DECODE(isnumber(tb_tax_matrix.' || tax_driver.matrix_column_name || '),1,to_char(tb_tax_matrix.' || tax_driver.matrix_column_name || ',''00000000000000000000.00000000000000000000''),UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ')) AND ' ||
                                                                                        'DECODE(isnumber(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || '),1,to_char(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ',''00000000000000000000.00000000000000000000''),UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ')) <= DECODE(isnumber(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU ),1,to_char(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU,''00000000000000000000.00000000000000000000''),UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU)) )) ';
               END IF;
            END IF;
         ELSE
            IF tax_driver.null_driver_flag = '1' THEN
               IF tax_driver.wildcard_flag = '1' THEN
                  -- Range and NOT ToNumber and NULL and Wildcard
                  IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                     UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                     vc_tax_matrix_where := vc_tax_matrix_where || 'AND ( ' ||
                     '( tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU = ''*NA'' AND ' ||
                        '( tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NULL AND (tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*NULL'' )) OR ' ||
                        '( tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND (tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') LIKE UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ')))) OR ' ||
                     '( tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU <> ''*NA'' AND (tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND (UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') >= UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') AND UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') <= UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU) ))) ';
                  END IF;
               ELSE
                  -- Range and NOT ToNumber and NULL and NOT Wildcard
                  IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                     UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                     vc_tax_matrix_where := vc_tax_matrix_where || 'AND ( ' ||
                     '( tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU = ''*NA'' AND ' ||
                        '( tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NULL AND (tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*NULL'' )) OR ' ||
                        '( tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND (tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') = UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ')))) OR ' ||
                     '( tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU <> ''*NA'' AND (tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND (UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') >= UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') AND UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') <= UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU) ))) ';
                  END IF;
               END IF;
            ELSE
               IF tax_driver.wildcard_flag = '1' THEN
                  -- Range and NOT ToNumber and NOT NULL and Wildcard
                  IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                     UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                     vc_tax_matrix_where := vc_tax_matrix_where || 'AND ( ' ||
                     '( tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU = ''*NA'' AND ' ||
                        '(tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') LIKE UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '))) OR ' ||
                     '( tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU <> ''*NA'' AND (UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') >= UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') AND UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') <= UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU) )) ';
                  END IF;
               ELSE
                  -- Range and NOT ToNumber and NOT NULL and NOT Wildcard
                  IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                     UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                     vc_tax_matrix_where := vc_tax_matrix_where || 'AND ( ' ||
                     '( tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU = ''*NA'' AND ' ||
                        '(tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') = UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '))) OR ' ||
                     '( tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU <> ''*NA'' AND (UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') >= UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') AND UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') <= UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || '_THRU) )) ';
                  END IF;
               END IF;
            END IF;
         END IF;
      ELSE
         IF tax_driver.null_driver_flag = '1' THEN
            IF tax_driver.wildcard_flag = '1' THEN
               -- NOT Range and NULL and Wildcard
               IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                  UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                     vc_tax_matrix_where := vc_tax_matrix_where || 'AND ( ' ||
                     '( tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NULL AND (tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*NULL'' )) OR ' ||
                     '( tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND (tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') LIKE UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ')) ) ';
               END IF;
            ELSE
               -- NOT Range and NULL and NOT Wildcard
               IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                  UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                     vc_tax_matrix_where := vc_tax_matrix_where || 'AND ( ' ||
                     '( tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NULL AND (tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*NULL'' )) OR ' ||
                     '( tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND (tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') = UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ')) ) ';
               END IF;
            END IF;
         ELSE
            IF tax_driver.wildcard_flag = '1' THEN
               -- NOT Range and NOT NULL and Wildcard
               IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                  UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                     vc_tax_matrix_where := vc_tax_matrix_where || 'AND ( ' ||
                     '( tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') LIKE UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') ) ';
               END IF;
            ELSE
               -- NOT Range and NOT NULL and NOT Wildcard
               IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                  UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                  IF tax_driver.trans_dtl_column_name = 'TRANSACTION_STATE_CODE' THEN
                     IF vc_state_driver_flag <> '1' THEN
                        vc_state_driver_flag := '1';
                     END IF;
                     vc_tax_matrix_where := vc_tax_matrix_where || 'AND ( ' ||
                        '( tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR UPPER(:v_transaction_state_code) = UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ')) ';
                  ELSE
                     vc_tax_matrix_where := vc_tax_matrix_where || 'AND ( ' ||
                        '( tb_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') = UPPER(tb_tax_matrix.' || tax_driver.matrix_column_name || ') ) ';
                  END IF;
               END IF;
            END IF;
         END IF;
      END IF;
      vc_tax_matrix_where := vc_tax_matrix_where || ') ';
   END LOOP;
   CLOSE tax_driver_cursor;*/

   -- New call to sp_gen_tax_driver for MidTier Cleanup
/*CM   EXECUTE sp_gen_tax_driver (
	p_generate_driver_reference	=> 'N',
	p_an_batch_id			=> NULL,
	p_transaction_table_name	=> 'tb_transaction_detail',
	p_tax_table_name		=> 'tb_tax_matrix',
	p_vc_state_driver_flag		=> vc_state_driver_flag,
	p_vc_tax_matrix_where		=> vc_tax_matrix_where);   
*/
 select  sp_gen_tax_driver (
	'N',
	NULL,
	'tb_transaction_detail',
	'tb_tax_matrix',
	vc_state_driver_flag,
	vc_tax_matrix_where) 
  into vc_tax_matrix_where; 
RAISE NOTICE 'Calling vc_tax_matrix_where(%)', vc_tax_matrix_where; 
vc_tax_matrix_where := replace(vc_tax_matrix_where, '(,"' ,'');
vc_tax_matrix_where := replace(vc_tax_matrix_where, '")' ,'');

   -- NEW BATCH PROCESS CODE --------------------------------------------------------------------------------------------------------------------
   -- *******************************************************************************************************************************************

   -- If no drivers found raise error, else create location matrix sql statement
   IF vc_tax_matrix_where IS NULL THEN
      NULL;
   ELSE
      vc_tax_matrix_stmt := vc_tax_matrix_select || vc_tax_matrix_where || vc_tax_matrix_orderby;
   END IF;

   -- Insert New Location Matrix columns into temporary table --------------------------------------
   INSERT INTO tb_tmp_location_matrix (
      location_matrix_id,
      driver_01, driver_01_desc,
      driver_02, driver_02_desc,
      driver_03, driver_03_desc,
      driver_04, driver_04_desc,
      driver_05, driver_05_desc,
      driver_06, driver_06_desc,
      driver_07, driver_07_desc,
      driver_08, driver_08_desc,
      driver_09, driver_09_desc,
      driver_10, driver_10_desc,
      binary_weight, significant_digits,
      default_flag, default_binary_weight, default_significant_digits,
      effective_date, expiration_date,
      jurisdiction_id, state, comments,
      last_used_timestamp, update_user_id, update_timestamp )
   VALUES (
      l_tb_location_matrix.location_matrix_id,
      l_tb_location_matrix.driver_01, l_tb_location_matrix.driver_01_desc,
      l_tb_location_matrix.driver_02, l_tb_location_matrix.driver_02_desc,
      l_tb_location_matrix.driver_03, l_tb_location_matrix.driver_03_desc,
      l_tb_location_matrix.driver_04, l_tb_location_matrix.driver_04_desc,
      l_tb_location_matrix.driver_05, l_tb_location_matrix.driver_05_desc,
      l_tb_location_matrix.driver_06, l_tb_location_matrix.driver_06_desc,
      l_tb_location_matrix.driver_07, l_tb_location_matrix.driver_07_desc,
      l_tb_location_matrix.driver_08, l_tb_location_matrix.driver_08_desc,
      l_tb_location_matrix.driver_09, l_tb_location_matrix.driver_09_desc,
      l_tb_location_matrix.driver_10, l_tb_location_matrix.driver_10_desc,
      l_tb_location_matrix.binary_weight, l_tb_location_matrix.significant_digits,
      l_tb_location_matrix.default_flag, l_tb_location_matrix.default_binary_weight, l_tb_location_matrix.default_significant_digits,
      l_tb_location_matrix.effective_date, l_tb_location_matrix.expiration_date,
      l_tb_location_matrix.jurisdiction_id, l_tb_location_matrix.state, l_tb_location_matrix.comments,
      l_tb_location_matrix.last_used_timestamp, l_tb_location_matrix.update_user_id, l_tb_location_matrix.update_timestamp );

   -- ****** Read and Process Transactions ****** -----------------------------------------
   OPEN transaction_detail_cursor
    FOR EXECUTE vc_transaction_detail_stmt;
    -- Initialize effective date to current date     /***#952***SRINI****/  
   --l_tb_location_matrix.effective_date := v_sysdate; /***#953***SRINI****/ 
   LOOP
      FETCH transaction_detail_cursor
       INTO transaction_detail;
       
      IF (NOT FOUND) THEN 
	EXIT;
      END IF;

      -- Populate and/or Transaction Detail Clear working fields
      transaction_detail.transaction_ind := NULL;
      transaction_detail.suspend_ind := NULL;
      transaction_detail.location_matrix_id := l_tb_location_matrix.location_matrix_id;
      transaction_detail.jurisdiction_id := l_tb_location_matrix.jurisdiction_id;
      transaction_detail.update_user_id := USER;
      transaction_detail.update_timestamp := v_sysdate;

      -- Continue if no Matrix ID
      IF transaction_detail.tax_matrix_id IS NULL OR transaction_detail.tax_matrix_id = 0 THEN
         v_transaction_detail_id := transaction_detail.transaction_detail_id;
         v_transaction_state_code := transaction_detail.transaction_state_code;

         -- Does Transaction Record have a valid State?
         IF v_transaction_state_code IS NULL THEN
            vn_fetch_rows := 0;
         ELSE
            SELECT count(*)
              INTO STRICT vn_fetch_rows
              FROM tb_taxcode_state
             WHERE taxcode_state_code = v_transaction_state_code;
         END IF;
         IF vn_fetch_rows = 0 THEN
            -- Get State from new Jurisdiction
            v_transaction_state_code := NULL;
            BEGIN
               SELECT state
                 INTO STRICT v_transaction_state_code
                 FROM tb_jurisdiction
                WHERE jurisdiction_id = l_tb_location_matrix.jurisdiction_id;
            EXCEPTION
               WHEN OTHERS THEN
                  v_transaction_state_code := NULL;
            END;
            -- Jurisdiction Line Found
            IF v_transaction_state_code IS NOT NULL THEN
               IF transaction_detail.transaction_state_code IS NULL THEN
                  transaction_detail.auto_transaction_state_code := '*NULL';
               ELSE
                  transaction_detail.auto_transaction_state_code := transaction_detail.transaction_state_code;
               END IF;
               transaction_detail.transaction_state_code := v_transaction_state_code;
            ELSE
               transaction_detail.transaction_ind := 'S';
               transaction_detail.suspend_ind := 'L';
            END IF;
         END IF;

         -- Continue if not Suspended
         IF transaction_detail.transaction_ind IS NULL OR transaction_detail.transaction_ind <> 'S' THEN
            -- Search Tax Matrix for matches
            BEGIN
                -- ****** CREATE Temporary Table to hold "Bind" Values ****** --
	       BEGIN
		   EXECUTE 'CREATE TEMPORARY TABLE sp_gen_tax_driver_tmp (param_name varchar(30), param_value varchar(30))';
	       EXCEPTION 
		   -- Table may already exist if using Connection/Session Pooling, Ignore
		   WHEN OTHERS THEN NULL;
	       END;

	       -- Prep TMP table with values
	       DELETE FROM sp_gen_tax_driver_tmp;
	       INSERT INTO sp_gen_tax_driver_tmp(param_name, param_value) VALUES ('v_transaction_detail_id',  v_transaction_detail_id);
	       INSERT INTO sp_gen_tax_driver_tmp(param_name, param_value) VALUES ('v_transaction_state_code', v_transaction_state_code);

               OPEN tax_matrix_cursor
                 FOR EXECUTE vc_tax_matrix_stmt;
               
               FETCH tax_matrix_cursor
                INTO tax_matrix;
               
	       -- Rows found?
               IF (FOUND) THEN
                  vn_fetch_rows := 1;
               ELSE
                  vn_fetch_rows := 0;
               END IF;
               CLOSE tax_matrix_cursor;

	       BEGIN
		   EXECUTE 'DROP TABLE sp_gen_tax_driver_tmp ';
	       EXCEPTION 
		   WHEN OTHERS THEN NULL;
	       END;

            EXCEPTION
               WHEN OTHERS THEN
                  vn_fetch_rows := 0;
            END;

            -- Tax Matrix Line Found
            IF vn_fetch_rows > 0 THEN
               transaction_detail.tax_matrix_id := tax_matrix.tax_matrix_id;
               -- Determine "Then" or "Else" Results
               IF tax_matrix.relation_sign = 'na' THEN
                  tax_matrix.relation_amount := 0;
                  v_less_hold_code_flag := tax_matrix.then_hold_code_flag;
                  v_less_taxcode_detail_id := tax_matrix.then_taxcode_detail_id;
                  v_less_taxcode_state_code := tax_matrix.then_taxcode_state_code;
                  v_less_taxcode_type_code := tax_matrix.then_taxcode_type_code;
                  v_less_taxcode_code := tax_matrix.then_taxcode_code;
                  v_less_jurisdiction_id := tax_matrix.then_jurisdiction_id;
                  v_less_measure_type_code := tax_matrix.then_measure_type_code;
                  v_less_taxtype_code := tax_matrix.then_taxtype_code;
                  v_less_cch_taxcat_code := tax_matrix.then_cch_taxcat_code;
                  v_less_cch_group_code := tax_matrix.then_cch_group_code;
                  -- v_less_cch_item_code := tax_matrix.then_cch_item_code;
                  v_equal_hold_code_flag := tax_matrix.then_hold_code_flag;
                  v_equal_taxcode_detail_id := tax_matrix.then_taxcode_detail_id;
                  v_equal_taxcode_state_code := tax_matrix.then_taxcode_state_code;
                  v_equal_taxcode_type_code := tax_matrix.then_taxcode_type_code;
                  v_equal_taxcode_code := tax_matrix.then_taxcode_code;
                  v_equal_jurisdiction_id := tax_matrix.then_jurisdiction_id;
                  v_equal_measure_type_code := tax_matrix.then_measure_type_code;
                  v_equal_taxtype_code := tax_matrix.then_taxtype_code;
                  v_equal_cch_taxcat_code := tax_matrix.then_cch_taxcat_code;
                  v_equal_cch_group_code := tax_matrix.then_cch_group_code;
                  -- v_equal_cch_item_code := tax_matrix.then_cch_item_code;
                  v_greater_hold_code_flag := tax_matrix.then_hold_code_flag;
                  v_greater_taxcode_detail_id := tax_matrix.then_taxcode_detail_id;
                  v_greater_taxcode_state_code := tax_matrix.then_taxcode_state_code;
                  v_greater_taxcode_type_code := tax_matrix.then_taxcode_type_code;
                  v_greater_taxcode_code := tax_matrix.then_taxcode_code;
                  v_greater_jurisdiction_id := tax_matrix.then_jurisdiction_id;
                  v_greater_measure_type_code := tax_matrix.then_measure_type_code;
                  v_greater_taxtype_code := tax_matrix.then_taxtype_code;
                  v_greater_cch_taxcat_code := tax_matrix.then_cch_taxcat_code;
                  v_greater_cch_group_code := tax_matrix.then_cch_group_code;
                  -- v_greater_cch_item_code := tax_matrix.then_cch_item_code;
              ELSIF tax_matrix.relation_sign = '=' THEN
                  v_less_hold_code_flag := tax_matrix.else_hold_code_flag;
                  v_less_taxcode_detail_id := tax_matrix.else_taxcode_detail_id;
                  v_less_taxcode_state_code := tax_matrix.else_taxcode_state_code;
                  v_less_taxcode_type_code := tax_matrix.else_taxcode_type_code;
                  v_less_taxcode_code := tax_matrix.else_taxcode_code;
                  v_less_jurisdiction_id := tax_matrix.else_jurisdiction_id;
                  v_less_measure_type_code := tax_matrix.else_measure_type_code;
                  v_less_taxtype_code := tax_matrix.else_taxtype_code;
                  v_less_cch_taxcat_code := tax_matrix.else_cch_taxcat_code;
                  v_less_cch_group_code := tax_matrix.else_cch_group_code;
                  -- v_less_cch_item_code := tax_matrix.else_cch_item_code;
                  v_equal_hold_code_flag := tax_matrix.then_hold_code_flag;
                  v_equal_taxcode_detail_id := tax_matrix.then_taxcode_detail_id;
                  v_equal_taxcode_state_code := tax_matrix.then_taxcode_state_code;
                  v_equal_taxcode_type_code := tax_matrix.then_taxcode_type_code;
                  v_equal_taxcode_code := tax_matrix.then_taxcode_code;
                  v_equal_jurisdiction_id := tax_matrix.then_jurisdiction_id;
                  v_equal_measure_type_code := tax_matrix.then_measure_type_code;
                  v_equal_taxtype_code := tax_matrix.then_taxtype_code;
                  v_equal_cch_taxcat_code := tax_matrix.then_cch_taxcat_code;
                  v_equal_cch_group_code := tax_matrix.then_cch_group_code;
                  -- v_equal_cch_item_code := tax_matrix.then_cch_item_code;
                  v_greater_hold_code_flag := tax_matrix.else_hold_code_flag;
                  v_greater_taxcode_detail_id := tax_matrix.else_taxcode_detail_id;
                  v_greater_taxcode_state_code := tax_matrix.else_taxcode_state_code;
                  v_greater_taxcode_type_code := tax_matrix.else_taxcode_type_code;
                  v_greater_taxcode_code := tax_matrix.else_taxcode_code;
                  v_greater_jurisdiction_id := tax_matrix.else_jurisdiction_id;
                  v_greater_measure_type_code := tax_matrix.else_measure_type_code;
                  v_greater_taxtype_code := tax_matrix.else_taxtype_code;
                  v_greater_cch_taxcat_code := tax_matrix.else_cch_taxcat_code;
                  v_greater_cch_group_code := tax_matrix.else_cch_group_code;
                  -- v_greater_cch_item_code := tax_matrix.else_cch_item_code;
               ELSIF tax_matrix.relation_sign = '<' THEN
                  v_less_hold_code_flag := tax_matrix.then_hold_code_flag;
                  v_less_taxcode_detail_id := tax_matrix.then_taxcode_detail_id;
                  v_less_taxcode_state_code := tax_matrix.then_taxcode_state_code;
                  v_less_taxcode_type_code := tax_matrix.then_taxcode_type_code;
                  v_less_taxcode_code := tax_matrix.then_taxcode_code;
                  v_less_jurisdiction_id := tax_matrix.then_jurisdiction_id;
                  v_less_measure_type_code := tax_matrix.then_measure_type_code;
                  v_less_taxtype_code := tax_matrix.then_taxtype_code;
                  v_less_cch_taxcat_code := tax_matrix.then_cch_taxcat_code;
                  v_less_cch_group_code := tax_matrix.then_cch_group_code;
                  -- v_less_cch_item_code := tax_matrix.then_cch_item_code;
                  v_equal_hold_code_flag := tax_matrix.else_hold_code_flag;
                  v_equal_taxcode_detail_id := tax_matrix.else_taxcode_detail_id;
                  v_equal_taxcode_state_code := tax_matrix.else_taxcode_state_code;
                  v_equal_taxcode_type_code := tax_matrix.else_taxcode_type_code;
                  v_equal_taxcode_code := tax_matrix.else_taxcode_code;
                  v_equal_jurisdiction_id := tax_matrix.else_jurisdiction_id;
                  v_equal_measure_type_code := tax_matrix.else_measure_type_code;
                  v_equal_taxtype_code := tax_matrix.else_taxtype_code;
                  v_equal_cch_taxcat_code := tax_matrix.else_cch_taxcat_code;
                  v_equal_cch_group_code := tax_matrix.else_cch_group_code;
                  -- v_equal_cch_item_code := tax_matrix.else_cch_item_code;
                  v_greater_hold_code_flag := tax_matrix.else_hold_code_flag;
                  v_greater_taxcode_detail_id := tax_matrix.else_taxcode_detail_id;
                  v_greater_taxcode_state_code := tax_matrix.else_taxcode_state_code;
                  v_greater_taxcode_type_code := tax_matrix.else_taxcode_type_code;
                  v_greater_taxcode_code := tax_matrix.else_taxcode_code;
                  v_greater_jurisdiction_id := tax_matrix.else_jurisdiction_id;
                  v_greater_measure_type_code := tax_matrix.else_measure_type_code;
                  v_greater_taxtype_code := tax_matrix.else_taxtype_code;
                  v_greater_cch_taxcat_code := tax_matrix.else_cch_taxcat_code;
                  v_greater_cch_group_code := tax_matrix.else_cch_group_code;
                  -- v_greater_cch_item_code := tax_matrix.else_cch_item_code;
               ELSIF tax_matrix.relation_sign = '<=' THEN
                  v_less_hold_code_flag := tax_matrix.then_hold_code_flag;
                  v_less_taxcode_detail_id := tax_matrix.then_taxcode_detail_id;
                  v_less_taxcode_state_code := tax_matrix.then_taxcode_state_code;
                  v_less_taxcode_type_code := tax_matrix.then_taxcode_type_code;
                  v_less_taxcode_code := tax_matrix.then_taxcode_code;
                  v_less_jurisdiction_id := tax_matrix.then_jurisdiction_id;
                  v_less_measure_type_code := tax_matrix.then_measure_type_code;
                  v_less_taxtype_code := tax_matrix.then_taxtype_code;
                  v_less_cch_taxcat_code := tax_matrix.then_cch_taxcat_code;
                  v_less_cch_group_code := tax_matrix.then_cch_group_code;
                  -- v_less_cch_item_code := tax_matrix.then_cch_item_code;
                  v_equal_hold_code_flag := tax_matrix.then_hold_code_flag;
                  v_equal_taxcode_detail_id := tax_matrix.then_taxcode_detail_id;
                  v_equal_taxcode_state_code := tax_matrix.then_taxcode_state_code;
                  v_equal_taxcode_type_code := tax_matrix.then_taxcode_type_code;
                  v_equal_taxcode_code := tax_matrix.then_taxcode_code;
                  v_equal_jurisdiction_id := tax_matrix.then_jurisdiction_id;
                  v_equal_measure_type_code := tax_matrix.then_measure_type_code;
                  v_equal_taxtype_code := tax_matrix.then_taxtype_code;
                  v_equal_cch_taxcat_code := tax_matrix.then_cch_taxcat_code;
                  v_equal_cch_group_code := tax_matrix.then_cch_group_code;
                  -- v_equal_cch_item_code := tax_matrix.then_cch_item_code;
                  v_greater_hold_code_flag := tax_matrix.else_hold_code_flag;
                  v_greater_taxcode_detail_id := tax_matrix.else_taxcode_detail_id;
                  v_greater_taxcode_state_code := tax_matrix.else_taxcode_state_code;
                  v_greater_taxcode_type_code := tax_matrix.else_taxcode_type_code;
                  v_greater_taxcode_code := tax_matrix.else_taxcode_code;
                  v_greater_jurisdiction_id := tax_matrix.else_jurisdiction_id;
                  v_greater_measure_type_code := tax_matrix.else_measure_type_code;
                  v_greater_taxtype_code := tax_matrix.else_taxtype_code;
                  v_greater_cch_taxcat_code := tax_matrix.else_cch_taxcat_code;
                  v_greater_cch_group_code := tax_matrix.else_cch_group_code;
                  -- v_greater_cch_item_code := tax_matrix.else_cch_item_code;
               ELSIF tax_matrix.relation_sign = '>' THEN
                  v_less_hold_code_flag := tax_matrix.else_hold_code_flag;
                  v_less_taxcode_detail_id := tax_matrix.else_taxcode_detail_id;
                  v_less_taxcode_state_code := tax_matrix.else_taxcode_state_code;
                  v_less_taxcode_type_code := tax_matrix.else_taxcode_type_code;
                  v_less_taxcode_code := tax_matrix.else_taxcode_code;
                  v_less_jurisdiction_id := tax_matrix.else_jurisdiction_id;
                  v_less_measure_type_code := tax_matrix.else_measure_type_code;
                  v_less_taxtype_code := tax_matrix.else_taxtype_code;
                  v_less_cch_taxcat_code := tax_matrix.else_cch_taxcat_code;
                  v_less_cch_group_code := tax_matrix.else_cch_group_code;
                  -- v_less_cch_item_code := tax_matrix.else_cch_item_code;
                  v_equal_hold_code_flag := tax_matrix.else_hold_code_flag;
                  v_equal_taxcode_detail_id := tax_matrix.else_taxcode_detail_id;
                  v_equal_taxcode_state_code := tax_matrix.else_taxcode_state_code;
                  v_equal_taxcode_type_code := tax_matrix.else_taxcode_type_code;
                  v_equal_taxcode_code := tax_matrix.else_taxcode_code;
                  v_equal_jurisdiction_id := tax_matrix.else_jurisdiction_id;
                  v_equal_measure_type_code := tax_matrix.else_measure_type_code;
                  v_equal_taxtype_code := tax_matrix.else_taxtype_code;
                  v_equal_cch_taxcat_code := tax_matrix.else_cch_taxcat_code;
                  v_equal_cch_group_code := tax_matrix.else_cch_group_code;
                  -- v_equal_cch_item_code := tax_matrix.else_cch_item_code;
                  v_greater_hold_code_flag := tax_matrix.then_hold_code_flag;
                  v_greater_taxcode_detail_id := tax_matrix.then_taxcode_detail_id;
                  v_greater_taxcode_state_code := tax_matrix.then_taxcode_state_code;
                  v_greater_taxcode_type_code := tax_matrix.then_taxcode_type_code;
                  v_greater_taxcode_code := tax_matrix.then_taxcode_code;
                  v_greater_jurisdiction_id := tax_matrix.then_jurisdiction_id;
                  v_greater_measure_type_code := tax_matrix.then_measure_type_code;
                  v_greater_taxtype_code := tax_matrix.then_taxtype_code;
                  v_greater_cch_taxcat_code := tax_matrix.then_cch_taxcat_code;
                  v_greater_cch_group_code := tax_matrix.then_cch_group_code;
                  -- v_greater_cch_item_code := tax_matrix.then_cch_item_code;
               ELSIF tax_matrix.relation_sign = '>=' THEN
                  v_less_hold_code_flag := tax_matrix.else_hold_code_flag;
                  v_less_taxcode_detail_id := tax_matrix.else_taxcode_detail_id;
                  v_less_taxcode_state_code := tax_matrix.else_taxcode_state_code;
                  v_less_taxcode_type_code := tax_matrix.else_taxcode_type_code;
                  v_less_taxcode_code := tax_matrix.else_taxcode_code;
                  v_less_jurisdiction_id := tax_matrix.else_jurisdiction_id;
                  v_less_measure_type_code := tax_matrix.else_measure_type_code;
                  v_less_taxtype_code := tax_matrix.else_taxtype_code;
                  v_less_cch_taxcat_code := tax_matrix.else_cch_taxcat_code;
                  v_less_cch_group_code := tax_matrix.else_cch_group_code;
                  -- v_less_cch_item_code := tax_matrix.else_cch_item_code;
                  v_equal_hold_code_flag := tax_matrix.then_hold_code_flag;
                  v_equal_taxcode_detail_id := tax_matrix.then_taxcode_detail_id;
                  v_equal_taxcode_state_code := tax_matrix.then_taxcode_state_code;
                  v_equal_taxcode_type_code := tax_matrix.then_taxcode_type_code;
                  v_equal_taxcode_code := tax_matrix.then_taxcode_code;
                  v_equal_jurisdiction_id := tax_matrix.then_jurisdiction_id;
                  v_equal_measure_type_code := tax_matrix.then_measure_type_code;
                  v_equal_taxtype_code := tax_matrix.then_taxtype_code;
                  v_equal_cch_taxcat_code := tax_matrix.then_cch_taxcat_code;
                  v_equal_cch_group_code := tax_matrix.then_cch_group_code;
                  -- v_equal_cch_item_code := tax_matrix.then_cch_item_code;
                  v_greater_hold_code_flag := tax_matrix.then_hold_code_flag;
                  v_greater_taxcode_detail_id := tax_matrix.then_taxcode_detail_id;
                  v_greater_taxcode_state_code := tax_matrix.then_taxcode_state_code;
                  v_greater_taxcode_type_code := tax_matrix.then_taxcode_type_code;
                  v_greater_taxcode_code := tax_matrix.then_taxcode_code;
                  v_greater_jurisdiction_id := tax_matrix.then_jurisdiction_id;
                  v_greater_measure_type_code := tax_matrix.then_measure_type_code;
                  v_greater_taxtype_code := tax_matrix.then_taxtype_code;
                  v_greater_cch_taxcat_code := tax_matrix.then_cch_taxcat_code;
                  v_greater_cch_group_code := tax_matrix.then_cch_group_code;
                  -- v_greater_cch_item_code := tax_matrix.then_cch_item_code;
               ELSIF tax_matrix.relation_sign = '<>' THEN
                  v_less_hold_code_flag := tax_matrix.then_hold_code_flag;
                  v_less_taxcode_detail_id := tax_matrix.then_taxcode_detail_id;
                  v_less_taxcode_state_code := tax_matrix.then_taxcode_state_code;
                  v_less_taxcode_type_code := tax_matrix.then_taxcode_type_code;
                  v_less_taxcode_code := tax_matrix.then_taxcode_code;
                  v_less_jurisdiction_id := tax_matrix.then_jurisdiction_id;
                  v_less_measure_type_code := tax_matrix.then_measure_type_code;
                  v_less_taxtype_code := tax_matrix.then_taxtype_code;
                  v_less_cch_taxcat_code := tax_matrix.then_cch_taxcat_code;
                  v_less_cch_group_code := tax_matrix.then_cch_group_code;
                  -- v_less_cch_item_code := tax_matrix.then_cch_item_code;
                  v_equal_hold_code_flag := tax_matrix.else_hold_code_flag;
                  v_equal_taxcode_detail_id := tax_matrix.else_taxcode_detail_id;
                  v_equal_taxcode_state_code := tax_matrix.else_taxcode_state_code;
                  v_equal_taxcode_type_code := tax_matrix.else_taxcode_type_code;
                  v_equal_taxcode_code := tax_matrix.else_taxcode_code;
                  v_equal_jurisdiction_id := tax_matrix.else_jurisdiction_id;
                  v_equal_measure_type_code := tax_matrix.else_measure_type_code;
                  v_equal_taxtype_code := tax_matrix.else_taxtype_code;
                  v_equal_cch_taxcat_code := tax_matrix.else_cch_taxcat_code;
                  v_equal_cch_group_code := tax_matrix.else_cch_group_code;
                  -- v_equal_cch_item_code := tax_matrix.else_cch_item_code;
                  v_greater_hold_code_flag := tax_matrix.then_hold_code_flag;
                  v_greater_taxcode_detail_id := tax_matrix.then_taxcode_detail_id;
                  v_greater_taxcode_state_code := tax_matrix.then_taxcode_state_code;
                  v_greater_taxcode_type_code := tax_matrix.then_taxcode_type_code;
                  v_greater_taxcode_code := tax_matrix.then_taxcode_code;
                  v_greater_jurisdiction_id := tax_matrix.then_jurisdiction_id;
                  v_greater_measure_type_code := tax_matrix.then_measure_type_code;
                  v_greater_taxtype_code := tax_matrix.then_taxtype_code;
                  v_greater_cch_taxcat_code := tax_matrix.then_cch_taxcat_code;
                  v_greater_cch_group_code := tax_matrix.then_cch_group_code;
                  -- v_greater_cch_item_code := tax_matrix.then_cch_item_code;
               END IF;

               SELECT CASE SIGN(transaction_detail.gl_line_itm_dist_amt - tax_matrix.relation_amount) WHEN -1 THEN v_less_hold_code_flag WHEN 0		THEN v_equal_hold_code_flag WHEN 1	THEN v_greater_hold_code_flag END,
                      CASE SIGN(transaction_detail.gl_line_itm_dist_amt - tax_matrix.relation_amount) WHEN -1 THEN v_less_taxcode_detail_id WHEN 0	THEN v_equal_taxcode_detail_id WHEN 1	THEN v_greater_taxcode_detail_id END,
                      CASE SIGN(transaction_detail.gl_line_itm_dist_amt - tax_matrix.relation_amount) WHEN -1 THEN v_less_taxcode_state_code WHEN 0	THEN v_equal_taxcode_state_code WHEN 1	THEN v_greater_taxcode_state_code END,
                      CASE SIGN(transaction_detail.gl_line_itm_dist_amt - tax_matrix.relation_amount) WHEN -1 THEN v_less_taxcode_type_code WHEN 0	THEN v_equal_taxcode_type_code WHEN 1	THEN v_greater_taxcode_type_code END,
                      CASE SIGN(transaction_detail.gl_line_itm_dist_amt - tax_matrix.relation_amount) WHEN -1 THEN v_less_taxcode_code WHEN 0		THEN v_equal_taxcode_code WHEN 1	THEN v_greater_taxcode_code END,
                      CASE SIGN(transaction_detail.gl_line_itm_dist_amt - tax_matrix.relation_amount) WHEN -1 THEN v_less_jurisdiction_id WHEN 0	THEN v_equal_jurisdiction_id WHEN 1	THEN v_greater_jurisdiction_id END,
                      CASE SIGN(transaction_detail.gl_line_itm_dist_amt - tax_matrix.relation_amount) WHEN -1 THEN v_less_measure_type_code WHEN 0	THEN v_equal_measure_type_code WHEN 1	THEN v_greater_measure_type_code END,
                      CASE SIGN(transaction_detail.gl_line_itm_dist_amt - tax_matrix.relation_amount) WHEN -1 THEN v_less_taxtype_code WHEN 0		THEN v_equal_taxtype_code WHEN 1	THEN v_greater_taxtype_code END,
                      CASE SIGN(transaction_detail.gl_line_itm_dist_amt - tax_matrix.relation_amount) WHEN -1 THEN v_less_cch_taxcat_code WHEN 0	THEN v_equal_cch_taxcat_code WHEN 1	THEN v_greater_cch_taxcat_code END,
                      CASE SIGN(transaction_detail.gl_line_itm_dist_amt - tax_matrix.relation_amount) WHEN -1 THEN v_less_cch_group_code WHEN 0		THEN v_equal_cch_group_code WHEN 1	THEN v_greater_cch_group_code END
                 INTO STRICT v_hold_code_flag, transaction_detail.taxcode_detail_id, transaction_detail.taxcode_state_code, transaction_detail.taxcode_type_code, transaction_detail.taxcode_code, v_override_jurisdiction_id, transaction_detail.measure_type_code, v_taxtype_code, transaction_detail.cch_taxcat_code, transaction_detail.cch_group_code;
            ELSE
            -- Tax Matrix Line NOT Found
               transaction_detail.transaction_ind := 'S';
               transaction_detail.suspend_ind := 'T';
            END IF;
         END IF;
      ELSE
         -- Get Tax Type Codes
         SELECT taxtype_code
           INTO STRICT v_taxtype_code
           FROM tb_taxcode
          WHERE taxcode_code = transaction_detail.taxcode_code
            AND taxcode_type_code = transaction_detail.taxcode_type_code;
      END IF;

      -- Continue if not Suspended
      IF transaction_detail.transaction_ind IS NULL OR transaction_detail.transaction_ind <> 'S' THEN
         -- ****** DETERMINE TYPE of TAXCODE ****** --
         -- The Tax Code is Taxable
         IF transaction_detail.taxcode_type_code = 'T' THEN
            -- If Override jurisdiction id is not blank or 0 then use it instead of location matrix
            IF v_override_jurisdiction_id IS NOT NULL AND v_override_jurisdiction_id <> 0 THEN
               transaction_detail.jurisdiction_id := v_override_jurisdiction_id;
               transaction_detail.manual_jurisdiction_ind := 'AO';
            END IF;

            -- Get Jurisdiction TaxRates and calculate Tax Amounts
            EXECUTE sp_getusetax(transaction_detail.jurisdiction_id,
                         transaction_detail.gl_date,
                         transaction_detail.measure_type_code,
                         transaction_detail.gl_line_itm_dist_amt,
                         v_taxtype_code,
                         l_tb_location_matrix.override_taxtype_code,
                         l_tb_location_matrix.state_flag,
                         l_tb_location_matrix.county_flag,
                         l_tb_location_matrix.county_local_flag,
                         l_tb_location_matrix.city_flag,
                         l_tb_location_matrix.city_local_flag,
                         transaction_detail.invoice_freight_amt,
                         transaction_detail.invoice_discount_amt,
                         transaction_detail.transaction_state_code,
-- future? --                  transaction_detail.import_definition_code,
                         'importdefn',
                         transaction_detail.taxcode_code,
                         transaction_detail.jurisdiction_taxrate_id,
                         transaction_detail.state_use_amount,
                         transaction_detail.state_use_tier2_amount,
                         transaction_detail.state_use_tier3_amount,
                         transaction_detail.county_use_amount,
                         transaction_detail.county_local_use_amount,
                         transaction_detail.city_use_amount,
                         transaction_detail.city_local_use_amount,
                         transaction_detail.state_use_rate,
                         transaction_detail.state_use_tier2_rate,
                         transaction_detail.state_use_tier3_rate,
                         transaction_detail.state_split_amount,
                         transaction_detail.state_tier2_min_amount,
                         transaction_detail.state_tier2_max_amount,
                         transaction_detail.state_maxtax_amount,
                         transaction_detail.county_use_rate,
                         transaction_detail.county_local_use_rate,
                         transaction_detail.county_split_amount,
                         transaction_detail.county_maxtax_amount,
                         transaction_detail.county_single_flag,
                         transaction_detail.county_default_flag,
                         transaction_detail.city_use_rate,
                         transaction_detail.city_local_use_rate,
                         transaction_detail.city_split_amount,
                         transaction_detail.city_split_use_rate,
                         transaction_detail.city_single_flag,
                         transaction_detail.city_default_flag,
                         transaction_detail.combined_use_rate,
                         transaction_detail.tb_calc_tax_amt,
-- future --               transaction_detail.taxable_amt,
-- Targa? --                 transaction_detail.user_number_10,
                         vn_taxable_amt,
                         v_taxtype_used );

            -- Suspend if tax rate id is null
            IF transaction_detail.jurisdiction_taxrate_id IS NULL OR transaction_detail.jurisdiction_taxrate_id = 0 THEN
               transaction_detail.transaction_ind := 'S';
               transaction_detail.suspend_ind := 'R';
            ELSE
               -- Check for Taxable Amount -- 3351
               IF vn_taxable_amt <> transaction_detail.gl_line_itm_dist_amt THEN
                  transaction_detail.gl_extract_amt := transaction_detail.gl_line_itm_dist_amt;
                  transaction_detail.gl_line_itm_dist_amt := vn_taxable_amt;
               END IF;
            END IF;

         -- The Tax Code is Exempt
         ELSIF transaction_detail.taxcode_type_code = 'E' THEN
            transaction_detail.transaction_ind := 'P';

         -- The Tax Code is a Question
         ELSIF transaction_detail.taxcode_type_code = 'Q' THEN
           transaction_detail.transaction_ind := 'Q';

         -- The Tax Code is Unrecognized - Suspend
         ELSE
            transaction_detail.transaction_ind := 'S';
            transaction_detail.suspend_ind := '?';
         END IF;
      END IF;

      -- Continue if not Suspended
      IF transaction_detail.transaction_ind IS NULL OR transaction_detail.transaction_ind <> 'S' THEN
         -- Check for matrix "H"old
         IF ( v_hold_code_flag = '1' ) AND
            ( transaction_detail.taxcode_type_code = 'T' OR transaction_detail.taxcode_type_code = 'E') THEN
               transaction_detail.transaction_ind := 'H';
         ELSE
             transaction_detail.transaction_ind := 'P';
         END IF;
      END IF;

 -- Set effective date value to earliest transaction gl_date  /***#952***SRINI***/
      --IF transaction_detail.gl_date <= l_tb_location_matrix.effective_date THEN /***#953***SRINI****/ 
      --  l_tb_location_matrix.effective_date := transaction_detail.gl_date; /***#953***SRINI****/ 
      --END IF; /***#953***SRINI****/ 

      -- Update transaction detail row
      BEGIN
         -- 3411 - add taxable amount and tax type used.
         UPDATE tb_transaction_detail
            SET gl_line_itm_dist_amt = transaction_detail.gl_line_itm_dist_amt,
                tb_calc_tax_amt = transaction_detail.tb_calc_tax_amt,
                state_use_amount = transaction_detail.state_use_amount,
                state_use_tier2_amount = transaction_detail.state_use_tier2_amount,
                state_use_tier3_amount = transaction_detail.state_use_tier3_amount,
                county_use_amount = transaction_detail.county_use_amount,
                county_local_use_amount = transaction_detail.county_local_use_amount,
                city_use_amount = transaction_detail.city_use_amount,
                city_local_use_amount = transaction_detail.city_local_use_amount,
                transaction_state_code = transaction_detail.transaction_state_code,
                auto_transaction_state_code = transaction_detail.auto_transaction_state_code,
                transaction_ind = transaction_detail.transaction_ind,
                suspend_ind = transaction_detail.suspend_ind,
                taxcode_detail_id = transaction_detail.taxcode_detail_id,
                taxcode_state_code = transaction_detail.taxcode_state_code,
                taxcode_type_code = transaction_detail.taxcode_type_code,
                taxcode_code = transaction_detail.taxcode_code,
                cch_taxcat_code = transaction_detail.cch_taxcat_code,
                cch_group_code =  transaction_detail.cch_group_code,
                tax_matrix_id = transaction_detail.tax_matrix_id,
                location_matrix_id = transaction_detail.location_matrix_id,
                jurisdiction_id = transaction_detail.jurisdiction_id,
                jurisdiction_taxrate_id = transaction_detail.jurisdiction_taxrate_id,
                manual_jurisdiction_ind = transaction_detail.manual_jurisdiction_ind,
                measure_type_code = transaction_detail.measure_type_code,
                state_use_rate = transaction_detail.state_use_rate,
                state_use_tier2_rate = transaction_detail.state_use_tier2_rate,
                state_use_tier3_rate = transaction_detail.state_use_tier3_rate,
                state_split_amount = transaction_detail.state_split_amount,
                state_tier2_min_amount = transaction_detail.state_tier2_min_amount,
                state_tier2_max_amount = transaction_detail.state_tier2_max_amount,
                state_maxtax_amount = transaction_detail.state_maxtax_amount,
                county_use_rate = transaction_detail.county_use_rate,
                county_local_use_rate = transaction_detail.county_local_use_rate,
                county_split_amount = transaction_detail.county_split_amount,
                county_maxtax_amount = transaction_detail.county_maxtax_amount,
                county_single_flag = transaction_detail.county_single_flag,
                county_default_flag = transaction_detail.county_default_flag,
                city_use_rate = transaction_detail.city_use_rate,
                city_local_use_rate = transaction_detail.city_local_use_rate,
                city_split_amount = transaction_detail.city_split_amount,
                city_split_use_rate = transaction_detail.city_split_use_rate,
                city_single_flag = transaction_detail.city_single_flag,
                city_default_flag = transaction_detail.city_default_flag,
                combined_use_rate = transaction_detail.combined_use_rate,
                gl_extract_amt = transaction_detail.gl_extract_amt,
                update_user_id = transaction_detail.update_user_id,
                update_timestamp = transaction_detail.update_timestamp
          WHERE transaction_detail_id = transaction_detail.transaction_detail_id;
         -- Error Checking
--         IF SQLCODE != 0 THEN
--            RAISE e_badupdate;
--         END IF;
--      EXCEPTION
--         WHEN e_badupdate THEN
--            NULL;
      END;

   END LOOP;
   CLOSE transaction_detail_cursor;
-- Set effective date value to earliest transaction gl_date  /***#952***SRINI***/
	--UPDATE tb_location_matrix set effective_date = l_tb_location_matrix.effective_date  /***#953***SRINI****/ 
     	--WHERE location_matrix_id = p_location_matrix_id;  /***#953***SRINI****/ 
 
END IF;

--EXCEPTION
--   WHEN OTHERS THEN
--      NULL;
END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
ALTER FUNCTION sp_tb_location_matrix_a_i(numeric) OWNER TO stscorp;
