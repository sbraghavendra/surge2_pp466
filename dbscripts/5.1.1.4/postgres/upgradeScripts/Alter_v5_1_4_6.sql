ALTER TABLE stscorp.tb_bcp_jurisdiction_taxrate ALTER geocode TYPE character varying(12);
ALTER TABLE stscorp.tb_jurisdiction ALTER geocode TYPE character varying(12);
ALTER TABLE stscorp.tb_jurisdiction ALTER client_geocode TYPE character varying(12);
ALTER TABLE stscorp.tb_jurisdiction ALTER comp_geocode TYPE character varying(12);
