

CREATE OR REPLACE FUNCTION stscorp.sp_db_statistics (in avc_where varchar) RETURNS void AS
$BODY$
DECLARE
/* ************************************************************************************************/
/* Object Type/Name: Stored Procedure - sp_db_statistics                                              */
/* Author:           Michael Fuller                                                               */
/* Date:             Long Time Ago                                                                  */
/* Description:                                                                                   */
/* Arguments:        None                                                                         */
/* Returns:          None                                                                         */
/* ************************************************************************************************/
/* Maintenance History:                                                                           */
/* Scan  Author       Date     Release    Description                                  Ticket #   */
/* Srini	      02/21/2011          The dynamic sqls were missing the column alias   1118       */
/* ----- ---------- ---------- ---------- -------------------------------------------- ---------- */
   vc_stat_total                   VARCHAR (1000) :=
      'SELECT count(*) AS cnt, sum(gl_line_itm_dist_amt) AS sum_gl_line_itm_dist_amt, sum(tb_calc_tax_amt) AS sum_tb_calc_tax_amt ' ||
      'FROM tb_transaction_detail LEFT JOIN tb_jurisdiction ON (tb_transaction_detail.jurisdiction_id = tb_jurisdiction.jurisdiction_id) ';  /**CHG1118**SRINI**/
   vc_stat_select                  VARCHAR (1000) :=
      'SELECT count(*) AS cnt, sum(gl_line_itm_dist_amt) AS sum_gl_line_itm_dist_amt, sum(tb_calc_tax_amt) AS sum_tb_calc_tax_amt, max(gl_line_itm_dist_amt) AS max_gl_line_itm_dist_amt, avg(gl_line_itm_dist_amt) AS avg_gl_line_itm_dist_amt ' ||
      'FROM tb_transaction_detail LEFT JOIN tb_jurisdiction ON (tb_transaction_detail.jurisdiction_id = tb_jurisdiction.jurisdiction_id) '; /**CHG1118**SRINI**/
   vc_stat_where                   VARCHAR (28000) := ''; 
   vc_stat_stmt                    VARCHAR (30000);       

   stat_cursor                     REFCURSOR;
   stat_cursor_record		   RECORD;

   vc_driver_ref_ins_stmt          VARCHAR (30000);      
   vn_status                       NUMERIC := NULL;

   v_count_total                   tb_tmp_statistics.count%TYPE              := 0;
   v_sum_total                     tb_tmp_statistics.sum%TYPE                := 0;
   v_sum_tax                       tb_tmp_statistics.sum%TYPE                := 0;
   v_count                         tb_tmp_statistics.count%TYPE              := 0;
   v_count_pct                     tb_tmp_statistics.count_pct%TYPE          := 0;
   v_sum                           tb_tmp_statistics.sum%TYPE                := 0;
   v_sum_pct                       tb_tmp_statistics.sum_pct%TYPE            := 0;
   v_tax_amt                       tb_tmp_statistics.tax_amt%TYPE            := 0;
   v_max                           tb_tmp_statistics.max%TYPE                := 0;
   v_avg                           tb_tmp_statistics.avg%TYPE                := 0;

   db_statistics_cursor CURSOR 
   FOR
      SELECT row_title, row_where, group_by_on_drilldown
      FROM tb_db_statistics
      ORDER BY db_statistics_id;
   
   db_statistics                 RECORD;


BEGIN

      EXECUTE 'TRUNCATE TABLE tb_tmp_statistics';

         vc_stat_where := '';
   vc_stat_stmt := '';
  insert into tb_debug(row_joe) values(vc_stat_stmt);
      IF avc_where IS NULL THEN
      vc_stat_stmt := vc_stat_total;
   ELSE
	   vc_stat_stmt := vc_stat_total || avc_where;
   END IF;
--debug
    --EXECUTE 'TRUNCATE TABLE tb_debug';
    --insert into tb_debug(row_joe) values(current_timestamp); 
	--insert into tb_debug(row_joe) values(vc_stat_stmt);      
	   BEGIN
      
      OPEN stat_cursor
       FOR EXECUTE vc_stat_stmt;

      FETCH stat_cursor
       INTO stat_cursor_record;
       
      IF (FOUND) THEN
	vn_status := 1;

	v_count_total	:= stat_cursor_record.cnt;
	v_sum_total	:= stat_cursor_record.sum_gl_line_itm_dist_amt;
	v_sum_tax	:= stat_cursor_record.sum_tb_calc_tax_amt;

      ELSE
         vn_status := 0;
      END IF;
      CLOSE stat_cursor;
   EXCEPTION
     WHEN OTHERS THEN
       vn_status := 0;
   END;


      OPEN db_statistics_cursor;
   LOOP
      
      FETCH db_statistics_cursor INTO db_statistics;
      
      IF (NOT FOUND) THEN
	EXIT;
      END IF;


            vc_stat_where := '';
      vc_stat_stmt := '';

            IF db_statistics.row_where IS NULL THEN
         IF avc_where IS NULL THEN
            vc_stat_where := NULL;
         ELSE
            vc_stat_where := avc_where;
         END IF;
      ELSE
         IF avc_where IS NULL THEN
            vc_stat_where := ' WHERE ' || db_statistics.row_where;
         ELSE
            vc_stat_where := avc_where || ' AND ' || db_statistics.row_where;
         END IF;
      END IF;

--debug
    --insert into tb_debug(row_joe) values(vc_stat_stmt);
    --insert into tb_debug(row_joe) values(vc_stat_where);
				IF vc_stat_where IS NULL OR vc_stat_where = '' THEN
		   vc_stat_stmt := vc_stat_select;
		ELSE
		   vc_stat_stmt := vc_stat_select || vc_stat_where;
		END IF;

      		                  
            BEGIN
         OPEN stat_cursor
          FOR EXECUTE vc_stat_stmt;

         FETCH stat_cursor
          INTO stat_cursor_record;
	  
         IF (FOUND) THEN
	    vn_status := 1;

	    v_count	:= stat_cursor_record.cnt;
	    v_sum	:= stat_cursor_record.sum_gl_line_itm_dist_amt;
	    v_tax_amt	:= stat_cursor_record.sum_tb_calc_tax_amt;
	    v_max	:= stat_cursor_record.max_gl_line_itm_dist_amt;
	    v_avg	:= stat_cursor_record.avg_gl_line_itm_dist_amt;
         ELSE
            vn_status := 0;
         END IF;
         CLOSE stat_cursor;
      EXCEPTION
         WHEN OTHERS THEN
           vn_status := 0;
      END;

      	  IF v_count_total <> 0 THEN
         v_count_pct := (v_count/v_count_total)*100;
      ELSE
         v_count_pct := 0;
      END IF;

      IF v_sum_total <> 0 THEN
         v_sum_pct := (v_sum/v_sum_total)*100;
      ELSE
         v_sum_pct := 0;
      END IF;
--debug
    --insert into tb_debug(row_joe) values(vc_stat_stmt);
    --insert into tb_debug(row_joe) values(vc_stat_where);   
      INSERT INTO tb_tmp_statistics (
         title,
         count,
         count_pct,
         sum,
         sum_pct,
         tax_amt,
         max,
         avg,
         where_clause,
         group_by_on_drilldown )
      VALUES (
         db_statistics.row_title,
         v_count,
         v_count_pct,
         v_sum,
         v_sum_pct,
         v_tax_amt,
         v_max,
         v_avg,
         vc_stat_where,
         db_statistics.group_by_on_drilldown );
 
      
   END LOOP;

   CLOSE db_statistics_cursor;

   
END;
$BODY$
LANGUAGE 'plpgsql'

