-- Function: sp_driver_reference_maint()

-- DROP FUNCTION sp_driver_reference_maint();

CREATE OR REPLACE FUNCTION sp_driver_reference_maint()
  RETURNS void AS
$BODY$
DECLARE
/* ************************************************************************************************/
/* Object Type/Name: Stored Procedure - sp_driver_reference_maint                                 */
/* Author:           Michael B. Fuller                                                            */
/* Date:             a long time ago                                                              */
/* Description:      Clear and re-populate the Driver References table                            */
/* Arguments:        None                                                                         */
/* Returns:          None                                                                         */
/* ************************************************************************************************/
/* Maintenance History:                                                                           */
/* Scan  Author       Date     Release    Description                                  Ticket #   */
/* ----- ---------- ---------- ---------- -------------------------------------------- ---------- */
/* SRINI MFULLER      01/24/2011          Change VARCHAR Size                            954      */        
/* ************************************************************************************************/
-- Program defined variables
   vc_driver_ref_ins_stmt VARCHAR (30000) := NULL;  /***954***SRINI***/

-- Define Driver Names Cursor  (tb_driver_names)
   driver_names RECORD;

-- Program starts ******************************************************************************************
BEGIN
   EXECUTE 'TRUNCATE TABLE tb_driver_reference';

   -- Read Driver information and create Matrix Selection SQL ---------------------------------------------------
   FOR driver_names IN
	SELECT	DISTINCT 
		a.trans_dtl_column_name,
		b.desc_column_name
	FROM	tb_driver_names a
	LEFT OUTER JOIN
	(
		SELECT	column_name, 
			desc_column_name
		FROM	tb_data_def_column
		WHERE	table_name = 'TB_TRANSACTION_DETAIL'
	) b
	ON	( a.trans_dtl_column_name = b.column_name )
	WHERE	( a.driver_names_code = 'T' 
	OR	  a.driver_names_code = 'L' )
   LOOP
      -- Insert Driver Reference Values for this driver
      -- Create Insert SQL
      vc_driver_ref_ins_stmt :=
         'INSERT INTO tb_driver_reference (trans_dtl_column_name, driver_value, driver_description) ' ||
            'SELECT ''' || upper(driver_names.trans_dtl_column_name)||
                    ''', ' || driver_names.trans_dtl_column_name || ', ';

      IF driver_names.desc_column_name IS NOT NULL THEN
         vc_driver_ref_ins_stmt := vc_driver_ref_ins_stmt || 'min(' || driver_names.desc_column_name || ') ';
      ELSE
         vc_driver_ref_ins_stmt := vc_driver_ref_ins_stmt || 'NULL ';
      END IF;

      vc_driver_ref_ins_stmt := vc_driver_ref_ins_stmt ||
              'FROM tb_transaction_detail ' ||
             'WHERE ' || driver_names.trans_dtl_column_name || ' IS NOT NULL AND ' ||
                        driver_names.trans_dtl_column_name || ' NOT IN ' ||
               '(SELECT driver_value ' ||
                  'FROM tb_driver_reference ' ||
                 'WHERE trans_dtl_column_name = ''' || upper(driver_names.trans_dtl_column_name)|| ''') ' ||
              'GROUP BY ' || upper(driver_names.trans_dtl_column_name) ||
                        ',' || driver_names.trans_dtl_column_name ;

      -- Execute Insert SQL
      EXECUTE vc_driver_ref_ins_stmt;

   END LOOP;

   --COMMIT;

END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
ALTER FUNCTION sp_driver_reference_maint() OWNER TO stscorp;
