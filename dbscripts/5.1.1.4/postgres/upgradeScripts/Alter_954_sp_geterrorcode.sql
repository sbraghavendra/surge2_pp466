-- Function: sp_geterrorcode(character varying, numeric, character varying, date)

-- DROP FUNCTION sp_geterrorcode(character varying, numeric, character varying, date);

CREATE OR REPLACE FUNCTION sp_geterrorcode(IN av_error_code character varying, IN an_batch_id numeric, IN av_process_type character varying, IN ad_process_timestamp date, OUT av_severity_level character varying, OUT av_write_import_line_flag character varying, OUT av_abort_import_flag character varying)
  RETURNS record AS
$BODY$
DECLARE
/* ************************************************************************************************/
/* Object Type/Name: Stored Procedure - sp_geterrorcode                                  */
/* Author:           Michael B. Fuller                                                            */
/* Date:             a long time ago                                                              */
/* Description:                                                                  */
/* Arguments:                                                                                     */
/* Returns:          None                                                                         */
/* ************************************************************************************************/
/* Maintenance History:                                                                           */
/* Scan  Author       Date     Release    Description                                  Ticket #   */
/* ----- ---------- ---------- ---------- -------------------------------------------- ---------- */
/* SRINI M. Fuller  01/21/2011            Add Sequence                                  954       */
/* ************************************************************************************************/
	v_sq_tb_batch_error_log_id	tb_batch_error_log.batch_error_log_id%TYPE;   /***954***SRINI***/
	
-- ****** Program starts ****** --------------------------------------------------------------------
BEGIN
   -- Fetch error condition information
   BEGIN
      SELECT ERRORCODE.severity_level,
             ERRORSEV.write_import_line_flag,
             ERRORSEV.abort_import_flag
        INTO STRICT 
	     av_severity_level,
             av_write_import_line_flag,
             av_abort_import_flag
        FROM ( SELECT *
                 FROM tb_list_code
                WHERE code_type_code = 'ERRORCODE' ) ERRORCODE,
             ( SELECT *
                 FROM tb_list_code
                WHERE code_type_code = 'ERRORSEV' ) ERRORSEV
       WHERE ERRORCODE.severity_level = ERRORSEV.code_code AND
             ERRORCODE.code_code = av_error_code;
      EXCEPTION WHEN OTHERS THEN
         av_severity_level := '90';
         av_write_import_line_flag := '0';
         av_abort_import_flag := '1';
   END;

   -- Write error to log
   SELECT NEXTVAL('sq_tb_batch_error_log_id') INTO v_sq_tb_batch_error_log_id;  /***954***SRINI***/
   INSERT INTO tb_batch_error_log (
   batch_error_log_id,     /***954***SRINI***/
      batch_id,
      process_type,
      process_timestamp,
      row_no,
      column_no,
      error_def_code )
   VALUES (
   v_sq_tb_batch_error_log_id,  /***954***SRINI***/
      an_batch_id, 
      av_process_type,
      ad_process_timestamp,
      0,
      0,
      av_error_code );
END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
ALTER FUNCTION sp_geterrorcode(character varying, numeric, character varying, date) OWNER TO stscorp;
