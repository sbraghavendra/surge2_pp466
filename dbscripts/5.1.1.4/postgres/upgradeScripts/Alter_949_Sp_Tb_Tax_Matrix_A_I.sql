-- Function: sp_tb_tax_matrix_a_i(numeric)

DROP FUNCTION sp_tb_tax_matrix_a_i(numeric);

CREATE OR REPLACE FUNCTION sp_tb_tax_matrix_a_i(p_tax_matrix_id numeric)
  RETURNS void AS
$BODY$
DECLARE
/* ************************************************************************************************/
/* Object Type/Name: SP Trigger/After Insert - sp_tb_tax_matrix_a_i                               */
/* Author:           Michael B. Fuller                                                            */
/* Date:             a long time ago                                                              */
/* Description:      A Tax Matrix line has been added                                             */
/* Arguments:        None                                                                         */
/* Returns:          None                                                                         */
/* ************************************************************************************************/
/* Maintenance History:                                                                           */
/* Scan  Author       Date     Release    Description                                  Ticket #   */
/* ----- ---------- ---------- ---------- -------------------------------------------- ---------- */
/*       M. Fuller  06/02/2006 3.3.5.0    Fix processing an allocated line - Temporary 871        */
/*       M. Fuller  01/04/2007 3.3.5.1    Fix processing an allocated line - Permenant 871        */
/* MBF01 M. Fuller  08/13/2007 3.4.1.1    Add 5 Calc Tax Flags                         883        */
/* JJF   J.Franco   12/17/2008 x.x.x.x    Convert trigger to stored procedure          xxx        */
/* ************************************************************************************************/

-- Defire Row Type for tb_tax_matrix record
   l_tb_tax_matrix tb_tax_matrix%ROWTYPE;

-- Transaction Detail Selection SQL
   vc_transaction_detail_select    VARCHAR(2000) :=
      'SELECT tb_transaction_detail.* ' ||
        'FROM tb_tmp_tax_matrix, ' ||
             'tb_transaction_detail ' ||
       'WHERE ( tb_tmp_tax_matrix.tax_matrix_id = ' || p_tax_matrix_id || ') AND ' ||
             '(( tb_tmp_tax_matrix.matrix_state_code = ''*ALL'' ) OR ( tb_tmp_tax_matrix.matrix_state_code = tb_transaction_detail.transaction_state_code )) AND ' ||
             '( tb_tmp_tax_matrix.effective_date <= tb_transaction_detail.gl_date ) AND ( tb_tmp_tax_matrix.expiration_date >= tb_transaction_detail.gl_date ) AND ' ||
             '( tb_transaction_detail.transaction_ind = ''S'' ) AND ( tb_transaction_detail.suspend_ind = ''T'' ) ';
  vc_transaction_detail_where     VARCHAR(5000) := '';
 --  vc_transaction_detail_where     VARCHAR(5000);
   vc_transaction_detail_update    VARCHAR(100) := 'FOR UPDATE';
   vc_transaction_detail_stmt      VARCHAR(7100);

   transaction_detail_cursor       REFCURSOR;
   transaction_detail              RECORD;

-- Location Matrix Selection SQL
   vc_location_matrix_select       VARCHAR(2000) :=
      'SELECT tb_location_matrix.location_matrix_id, ' ||
             'tb_location_matrix.jurisdiction_id, ' ||
             'tb_location_matrix.override_taxtype_code, ' ||
             'tb_location_matrix.state_flag,' ||
             'tb_location_matrix.county_flag,' ||
             'tb_location_matrix.county_local_flag,' ||
             'tb_location_matrix.city_flag,' ||
             'tb_location_matrix.city_local_flag,' ||
             'tb_jurisdiction.state, ' ||
             'tb_jurisdiction.county, ' ||
             'tb_jurisdiction.city, ' ||
             'tb_jurisdiction.zip, ' ||
             'tb_jurisdiction.in_out ' ||
        'FROM tb_location_matrix, ' ||
             'tb_jurisdiction, ' ||
             'tb_transaction_detail ' ||
      'WHERE ( tb_transaction_detail.transaction_detail_id = (SELECT CAST (param_value AS NUMERIC) FROM sp_gen_location_driver_tmp WHERE param_name = ''v_transaction_detail_id'') ) AND ' ||
            '( tb_location_matrix.default_flag = ''0'' AND tb_location_matrix.binary_weight > 0 ) AND ' ||
            '( tb_location_matrix.effective_date <= tb_transaction_detail.gl_date ) AND ( tb_location_matrix.expiration_date >= tb_transaction_detail.gl_date ) AND ' ||
            '( tb_location_matrix.jurisdiction_id = tb_jurisdiction.jurisdiction_id ) ';
   vc_location_matrix_where        VARCHAR(5000) := '';
   vc_location_matrix_orderby      VARCHAR(1000) :=
      'ORDER BY tb_location_matrix.binary_weight DESC, tb_location_matrix.significant_digits DESC, tb_location_matrix.effective_date DESC';
   vc_location_matrix_stmt         VARCHAR (8000);
   
   location_matrix_cursor          REFCURSOR;

   vc_state_driver_flag            CHAR(1)                                           := NULL;

-- Location Matrix Record TYPE
   /*TYPE location_matrix_record is RECORD (
      location_matrix_id           tb_location_matrix.location_matrix_id%TYPE,
      jurisdiction_id              tb_location_matrix.jurisdiction_id%TYPE,
      override_taxtype_code        tb_location_matrix.override_taxtype_code%TYPE,
      state_flag                   tb_location_matrix.state_flag%TYPE,
      county_flag                  tb_location_matrix.county_flag%TYPE,
      county_local_flag            tb_location_matrix.county_local_flag%TYPE,
      city_flag                    tb_location_matrix.city_flag%TYPE,
      city_local_flag              tb_location_matrix.city_local_flag%TYPE,
      state                        tb_jurisdiction.state%TYPE,
      county                       tb_jurisdiction.county%TYPE,
      city                         tb_jurisdiction.city%TYPE,
      zip                          tb_jurisdiction.zip%TYPE,
      in_out                       tb_jurisdiction.in_out%TYPE );*/
   location_matrix                 RECORD;

-- Table defined variables
   v_relation_amount               tb_tax_matrix.relation_amount%TYPE                := NULL;
   v_hold_code_flag                tb_tax_matrix.then_hold_code_flag%TYPE            := NULL;
   v_less_hold_code_flag           tb_tax_matrix.then_hold_code_flag%TYPE            := NULL;
   v_less_taxcode_detail_id        tb_tax_matrix.then_taxcode_detail_id%TYPE         := NULL;
   v_less_cch_taxcat_code          tb_tax_matrix.then_cch_taxcat_code%TYPE           := NULL;
   v_less_cch_group_code           tb_tax_matrix.then_cch_group_code%TYPE            := NULL;
   v_equal_hold_code_flag          tb_tax_matrix.then_hold_code_flag%TYPE            := NULL;
   v_equal_taxcode_detail_id       tb_tax_matrix.then_taxcode_detail_id%TYPE         := NULL;
   v_equal_cch_taxcat_code         tb_tax_matrix.then_cch_taxcat_code%TYPE           := NULL;
   v_equal_cch_group_code          tb_tax_matrix.then_cch_group_code%TYPE            := NULL;
   v_greater_hold_code_flag        tb_tax_matrix.then_hold_code_flag%TYPE            := NULL;
   v_greater_taxcode_detail_id     tb_tax_matrix.then_taxcode_detail_id%TYPE         := NULL;
   v_greater_cch_taxcat_code       tb_tax_matrix.then_cch_taxcat_code%TYPE           := NULL;
   v_greater_cch_group_code        tb_tax_matrix.then_cch_group_code%TYPE            := NULL;
   v_then_taxcode_state_code       tb_taxcode_detail.taxcode_state_code%TYPE         := NULL;
   v_then_taxcode_type_code        tb_taxcode_detail.taxcode_type_code%TYPE          := NULL;
   v_then_taxcode_code             tb_taxcode_detail.taxcode_code%TYPE               := NULL;
   v_then_jurisdiction_id          tb_taxcode_detail.jurisdiction_id%TYPE            := NULL;
   v_then_measure_type_code        tb_taxcode_detail.measure_type_code%TYPE          := NULL;
   v_then_taxtype_code             tb_taxcode_detail.taxtype_code%TYPE               := NULL;
   v_else_taxcode_state_code       tb_taxcode_detail.taxcode_state_code%TYPE         := NULL;
   v_else_taxcode_type_code        tb_taxcode_detail.taxcode_type_code%TYPE          := NULL;
   v_else_taxcode_code             tb_taxcode_detail.taxcode_code%TYPE               := NULL;
   v_else_jurisdiction_id          tb_taxcode_detail.jurisdiction_id%TYPE            := NULL;
   v_else_measure_type_code        tb_taxcode_detail.measure_type_code%TYPE          := NULL;
   v_else_taxtype_code             tb_taxcode_detail.taxtype_code%TYPE               := NULL;
   v_less_taxcode_state_code       tb_taxcode_detail.taxcode_state_code%TYPE         := NULL;
   v_less_taxcode_type_code        tb_taxcode_detail.taxcode_type_code%TYPE          := NULL;
   v_less_taxcode_code             tb_taxcode_detail.taxcode_code%TYPE               := NULL;
   v_less_jurisdiction_id          tb_taxcode_detail.jurisdiction_id%TYPE            := NULL;
   v_less_measure_type_code        tb_taxcode_detail.measure_type_code%TYPE          := NULL;
   v_less_taxtype_code             tb_taxcode_detail.taxtype_code%TYPE               := NULL;
   v_equal_taxcode_state_code      tb_taxcode_detail.taxcode_state_code%TYPE         := NULL;
   v_equal_taxcode_type_code       tb_taxcode_detail.taxcode_type_code%TYPE          := NULL;
   v_equal_taxcode_code            tb_taxcode_detail.taxcode_code%TYPE               := NULL;
   v_equal_jurisdiction_id         tb_taxcode_detail.jurisdiction_id%TYPE            := NULL;
   v_equal_measure_type_code       tb_taxcode_detail.measure_type_code%TYPE          := NULL;
   v_equal_taxtype_code            tb_taxcode_detail.taxtype_code%TYPE               := NULL;
   v_greater_taxcode_state_code    tb_taxcode_detail.taxcode_state_code%TYPE         := NULL;
   v_greater_taxcode_type_code     tb_taxcode_detail.taxcode_type_code%TYPE          := NULL;
   v_greater_taxcode_code          tb_taxcode_detail.taxcode_code%TYPE               := NULL;
   v_greater_jurisdiction_id       tb_taxcode_detail.jurisdiction_id%TYPE            := NULL;
   v_greater_measure_type_code     tb_taxcode_detail.measure_type_code%TYPE          := NULL;
   v_greater_taxtype_code          tb_taxcode_detail.taxtype_code%TYPE               := NULL;
   v_transaction_detail_id         tb_transaction_detail.transaction_detail_id%TYPE  := NULL;
   v_override_jurisdiction_id      tb_bcp_transactions.jurisdiction_id%TYPE          := NULL;
   v_sysdate                       tb_transaction_detail.load_timestamp%TYPE         := SYS_EXTRACT_UTC(CURRENT_TIMESTAMP);/****UTC Timestamp ##SRINI###*****/
   v_taxtype_code                  tb_taxcode.taxtype_code%TYPE                      := NULL;
   v_taxtype_used                  tb_taxcode.taxtype_code%TYPE                      := NULL;

-- Program defined variables
   vn_hold_trans_amount            NUMERIC                                            := 0;
   vn_fetch_rows                   NUMERIC                                            := 0;
   vi_error_count                  INTEGER                                           := 0;
   vn_actual_rowno                 NUMERIC                                            := 0;
-- temporary
   vn_taxable_amt                  NUMERIC                                            := 0;

   vc_test1 VARCHAR(4000);
   vc_test2 VARCHAR(4000);

-- Define Location Driver Names Cursor (tb_driver_names)
   /*CURSOR location_driver_cursor
   IS
        SELECT driver.trans_dtl_column_name, driver.matrix_column_name, driver.null_driver_flag, driver.wildcard_flag, driver.range_flag, driver.to_number_flag,
               datadefcol.desc_column_name, datadefcol.data_type
          FROM tb_driver_names driver,
               (SELECT *
                  FROM tb_data_def_column
                 WHERE table_name = 'TB_TRANSACTION_DETAIL') datadefcol
         WHERE driver.driver_names_code = 'L' AND
               driver.trans_dtl_column_name = datadefcol.column_name
      ORDER BY driver.driver_id;
      location_driver              location_driver_cursor%ROWTYPE;*/

-- Define Tax Driver Names Cursor (tb_driver_names)
   /*CURSOR tax_driver_cursor
   IS
        SELECT driver.trans_dtl_column_name, driver.matrix_column_name, driver.null_driver_flag, driver.wildcard_flag, driver.range_flag, driver.to_number_flag,
               datadefcol.desc_column_name, datadefcol.data_type
          FROM tb_driver_names driver,
               (SELECT *
                  FROM tb_data_def_column
                 WHERE table_name = 'TB_TRANSACTION_DETAIL') datadefcol
         WHERE driver.driver_names_code = 'T' AND
               driver.trans_dtl_column_name = datadefcol.column_name
      ORDER BY driver.driver_id;
      tax_driver                   tax_driver_cursor%ROWTYPE;*/

-- Define Exceptions
   --e_badread                       exception;
   --e_badupdate                     exception;

-- ****** Program starts ****** --------------------------------------------------------------------
BEGIN

SELECT tb_tax_matrix.* 
 INTO  STRICT l_tb_tax_matrix
 FROM  tb_tax_matrix
 WHERE tax_matrix_id = p_tax_matrix_id;

IF l_tb_tax_matrix.default_flag <> '1' AND l_tb_tax_matrix.binary_weight > 0 THEN

   -- *******************************************************************************************************************************************
   -- NEW BATCH PROCESS CODE --------------------------------------------------------------------------------------------------------------------
   -- ***** Create Dynamic WHERE Clause for Transaction Detail SQL ***** --
   -- Tax Matrix Drivers
   /*OPEN tax_driver_cursor;
   LOOP
      FETCH tax_driver_cursor INTO tax_driver;
      EXIT WHEN tax_driver_cursor%NOTFOUND;
      IF tax_driver.range_flag = '1' THEN
         IF tax_driver.to_number_flag = '1' THEN
            IF tax_driver.null_driver_flag = '1' THEN
               -- Range and ToNumber and NULL
               IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                  UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                  vc_transaction_detail_where := vc_transaction_detail_where || 'AND ( ' ||
                  '( tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || '_THRU = ''*NA'' AND ' ||
                     '( tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NULL AND ( tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*NULL'' )) OR ' ||
                     '( tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND ( tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR DECODE(isnumber(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || '),1,to_char(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ',''00000000000000000000.00000000000000000000''),UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ')) = DECODE(isnumber(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || '),1,to_char(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ',''00000000000000000000.00000000000000000000''),UPPER(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ') ))) OR ' ||
                  '( tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || '_THRU <> ''*NA'' AND ' ||
                     '( tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND (DECODE(isnumber(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || '),1,to_char(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ',''00000000000000000000.00000000000000000000''),UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ')) >= DECODE(isnumber(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || '),1,to_char(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ',''00000000000000000000.00000000000000000000''),UPPER(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ') ) AND ' ||
                                                                                            'DECODE(isnumber(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || '),1,to_char(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ',''00000000000000000000.00000000000000000000''),UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ')) <= DECODE(isnumber(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || '_THRU ),1,to_char(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || '_THRU,''00000000000000000000.00000000000000000000''),UPPER(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || '_THRU)) )))) ';
               END IF;
            ELSE
               -- Range and ToNumber and NOT NULL
               IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                  UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                  vc_transaction_detail_where := vc_transaction_detail_where || 'AND ( ' ||
                  '( tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || '_THRU = ''*NA'' AND (tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR DECODE(isnumber(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || '),1,to_char(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ',''00000000000000000000.00000000000000000000''),UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ')) = DECODE(isnumber(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || '),1,to_char(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ',''00000000000000000000.00000000000000000000''),UPPER(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ')))) OR ' ||
                  '( tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || '_THRU <> ''*NA'' AND (DECODE(isnumber(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || '),1,to_char(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ',''00000000000000000000.00000000000000000000''),UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ')) >= DECODE(isnumber(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || '),1,to_char(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ',''00000000000000000000.00000000000000000000''),UPPER(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ')) AND ' ||
                                                                                        'DECODE(isnumber(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || '),1,to_char(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ',''00000000000000000000.00000000000000000000''),UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ')) <= DECODE(isnumber(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || '_THRU ),1,to_char(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || '_THRU,''00000000000000000000.00000000000000000000''),UPPER(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || '_THRU)) )) ';
               END IF;
            END IF;
         ELSE
            IF tax_driver.null_driver_flag = '1' THEN
               IF tax_driver.wildcard_flag = '1' THEN
                  -- Range and NOT ToNumber and NULL and Wildcard
                  IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                     UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                     vc_transaction_detail_where := vc_transaction_detail_where || 'AND ( ' ||
                     '( tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || '_THRU = ''*NA'' AND ' ||
                        '( tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NULL AND (tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*NULL'' )) OR ' ||
                        '( tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND (tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') LIKE UPPER(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ')))) OR ' ||
                     '( tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || '_THRU <> ''*NA'' AND (tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND (UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') >= UPPER(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ') AND UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') <= UPPER(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || '_THRU) ))) ';
                  END IF;
               ELSE
                  -- Range and NOT ToNumber and NULL and NOT Wildcard
                  IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                     UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                     vc_transaction_detail_where := vc_transaction_detail_where || 'AND ( ' ||
                     '( tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || '_THRU = ''*NA'' AND ' ||
                        '( tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NULL AND (tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*NULL'' )) OR ' ||
                        '( tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND (tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') = UPPER(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ')))) OR ' ||
                     '( tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || '_THRU <> ''*NA'' AND (tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND (UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') >= UPPER(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ') AND UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') <= UPPER(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || '_THRU) ))) ';
                  END IF;
               END IF;
            ELSE
               IF tax_driver.wildcard_flag = '1' THEN
                  -- Range and NOT ToNumber and NOT NULL and Wildcard
                  IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                     UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                     vc_transaction_detail_where := vc_transaction_detail_where || 'AND ( ' ||
                     '( tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || '_THRU = ''*NA'' AND ' ||
                        '(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') LIKE UPPER(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || '))) OR ' ||
                     '( tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || '_THRU <> ''*NA'' AND (UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') >= UPPER(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ') AND UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') <= UPPER(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || '_THRU) )) ';
                  END IF;
               ELSE
                  -- Range and NOT ToNumber and NOT NULL and NOT Wildcard
                  IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                     UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                     vc_transaction_detail_where := vc_transaction_detail_where || 'AND ( ' ||
                     '( tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || '_THRU = ''*NA'' AND ' ||
                        '(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') = UPPER(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || '))) OR ' ||
                     '( tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || '_THRU <> ''*NA'' AND (UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') >= UPPER(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ') AND UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') <= UPPER(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || '_THRU) )) ';
                  END IF;
               END IF;
            END IF;
         END IF;
      ELSE
         IF tax_driver.null_driver_flag = '1' THEN
            IF tax_driver.wildcard_flag = '1' THEN
               -- NOT Range and NULL and Wildcard
               IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                  UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                     vc_transaction_detail_where := vc_transaction_detail_where || 'AND ( ' ||
                     '( tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NULL AND (tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*NULL'' )) OR ' ||
                     '( tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND (tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') LIKE UPPER(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ')) ) ';
               END IF;
            ELSE
               -- NOT Range and NULL and NOT Wildcard
               IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                  UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                     vc_transaction_detail_where := vc_transaction_detail_where || 'AND ( ' ||
                     '( tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NULL AND (tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*NULL'' )) OR ' ||
                     '( tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ' IS NOT NULL AND (tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') = UPPER(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ')) ) ';
               END IF;
            END IF;
         ELSE
            IF tax_driver.wildcard_flag = '1' THEN
               -- NOT Range and NOT NULL and Wildcard
               IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                  UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                     vc_transaction_detail_where := vc_transaction_detail_where || 'AND ( ' ||
                     '( tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') LIKE UPPER(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ') ) ';
               END IF;
            ELSE
               -- NOT Range and NOT NULL and NOT Wildcard
               IF UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'CHAR' OR
                  UPPER(SUBSTR(LTRIM(RTRIM(tax_driver.data_type)), 1, 4)) = 'VARC' THEN
                  vc_transaction_detail_where := vc_transaction_detail_where || 'AND ( ' ||
                     '( tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ' = ''*ALL'' OR UPPER(tb_transaction_detail.' || tax_driver.trans_dtl_column_name || ') = UPPER(tb_tmp_tax_matrix.' || tax_driver.matrix_column_name || ') ) ';
               END IF;
            END IF;
         END IF;
      END IF;
      vc_transaction_detail_where := vc_transaction_detail_where || ') ';
   END LOOP;
   CLOSE tax_driver_cursor;*/

   -- New call to sp_gen_tax_driver for MidTier Cleanup

/*Chandra Commented the below call and added another call
   EXECUTE sp_gen_tax_driver (
	'N',
	NULL,
	'tb_transaction_detail',
	'tb_tmp_tax_matrix',
	vc_state_driver_flag,
	vc_transaction_detail_where);   */

 select  sp_gen_tax_driver (
	'N',
	NULL,
	'tb_transaction_detail',
	'tb_tmp_tax_matrix',
	vc_state_driver_flag,
	vc_transaction_detail_where) 
  into vc_transaction_detail_where; 
RAISE NOTICE 'Calling vc_transaction_detail_where(%)', vc_transaction_detail_where; 
vc_transaction_detail_where := replace(vc_transaction_detail_where, '(,"' ,'');
vc_transaction_detail_where := replace(vc_transaction_detail_where, '")' ,'');

	

   -- NEW BATCH PROCESS CODE --------------------------------------------------------------------------------------------------------------------
   -- *******************************************************************************************************************************************
--RAISE NOTICE 'Calling vc_transaction_detail_where(%)', vc_transaction_detail_where;  
   -- If no drivers found raise error, else create transaction detail sql statement
   IF vc_transaction_detail_where IS NULL THEN
      NULL;
   ELSE
      vc_transaction_detail_stmt := vc_transaction_detail_select || vc_transaction_detail_where || vc_transaction_detail_update;
   END IF;

   -- ***** Create Dynamic WHERE Clause for Matrix SQLs ***** --
   -- Location Matrix Drivers
   /*OPEN location_driver_cursor;
   LOOP
      FETCH location_driver_cursor INTO location_driver;
      EXIT WHEN location_driver_cursor%NOTFOUND;
      IF location_driver.null_driver_flag = '1' THEN
         IF location_driver.wildcard_flag = '1' THEN
            IF UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'CHAR' OR
               UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'VARC' THEN
                  vc_location_matrix_where := vc_location_matrix_where || 'AND ' ||
                  '((tb_transaction_detail.' || location_driver.trans_dtl_column_name || ' IS NULL AND (tb_location_matrix.' || location_driver.matrix_column_name || ' = ''*ALL'' OR tb_location_matrix.' || location_driver.matrix_column_name || ' = ''*NULL'' )) OR ' ||
                  '(tb_transaction_detail.' || location_driver.trans_dtl_column_name || ' IS NOT NULL AND (tb_location_matrix.' || location_driver.matrix_column_name || ' = ''*ALL'' OR UPPER(tb_transaction_detail.' || location_driver.trans_dtl_column_name || ') LIKE UPPER(tb_location_matrix.' || location_driver.matrix_column_name || ') ))) ';
            END IF;
         ELSE
            IF UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'CHAR' OR
               UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'VARC' THEN
                  vc_location_matrix_where := vc_location_matrix_where || 'AND ' ||
                  '((tb_transaction_detail.' || location_driver.trans_dtl_column_name || ' IS NULL AND (tb_location_matrix.' || location_driver.matrix_column_name || ' = ''*ALL'' OR tb_location_matrix.' || location_driver.matrix_column_name || ' = ''*NULL'' )) OR ' ||
                  '(tb_transaction_detail.' || location_driver.trans_dtl_column_name || ' IS NOT NULL AND (tb_location_matrix.' || location_driver.matrix_column_name || ' = ''*ALL'' OR UPPER(tb_transaction_detail.' || location_driver.trans_dtl_column_name || ') = UPPER(tb_location_matrix.' || location_driver.matrix_column_name || ') ))) ';
            END IF;
         END IF;
      ELSE
         IF location_driver.wildcard_flag = '1' THEN
            IF UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'CHAR' OR
               UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'VARC' THEN
                  vc_location_matrix_where := vc_location_matrix_where || 'AND ' ||
                  '(tb_location_matrix.' || location_driver.matrix_column_name || ' = ''*ALL'' OR UPPER(tb_transaction_detail.' || location_driver.trans_dtl_column_name || ') LIKE UPPER(tb_location_matrix.' || location_driver.matrix_column_name || ') ) ';
            END IF;
         ELSE
            IF UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'CHAR' OR
               UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'VARC' THEN
                  vc_location_matrix_where := vc_location_matrix_where || 'AND ' ||
                  '(tb_location_matrix.' || location_driver.matrix_column_name || ' = ''*ALL'' OR UPPER(tb_transaction_detail.' || location_driver.trans_dtl_column_name || ') = UPPER(tb_location_matrix.' || location_driver.matrix_column_name || ') ) ';
            END IF;
         END IF;
      END IF;
   END LOOP;
   CLOSE location_driver_cursor;*/

   -- New call to sp_gen_location_driver for MidTier Cleanup
/*Chandra Commented the below call and added another call
   EXECUTE sp_gen_location_driver (
	'N',
	NULL,
	'tb_transaction_detail',
	'tb_location_matrix',
	vc_location_matrix_where); */

    select 
     sp_gen_location_driver (
	'N',
	NULL,
	'tb_transaction_detail',
	'tb_location_matrix',
	vc_location_matrix_where)
    into vc_location_matrix_where;	

   -- If no drivers found raise error, else create transaction detail sql statement
   IF vc_location_matrix_where IS NULL THEN
      NULL;
   ELSE
      vc_location_matrix_stmt := vc_location_matrix_select || vc_location_matrix_where || vc_location_matrix_orderby;
   END IF;

   -- Insert New Tax Matrix columns into temporary table -------------------------------------------
   INSERT INTO tb_tmp_tax_matrix (
      tax_matrix_id, matrix_state_code, driver_global_flag,
      driver_01, driver_01_desc,
      driver_01_thru, driver_01_thru_desc,
      driver_02, driver_02_desc,
      driver_02_thru, driver_02_thru_desc,
      driver_03, driver_03_desc,
      driver_03_thru, driver_03_thru_desc,
      driver_04, driver_04_desc,
      driver_04_thru, driver_04_thru_desc,
      driver_05, driver_05_desc,
      driver_05_thru, driver_05_thru_desc,
      driver_06, driver_06_desc,
      driver_06_thru, driver_06_thru_desc,
      driver_07, driver_07_desc,
      driver_07_thru, driver_07_thru_desc,
      driver_08, driver_08_desc,
      driver_08_thru, driver_08_thru_desc,
      driver_09, driver_09_desc,
      driver_09_thru, driver_09_thru_desc,
      driver_10, driver_10_desc,
      driver_10_thru, driver_10_thru_desc,
      driver_11, driver_11_desc,
      driver_11_thru, driver_11_thru_desc,
      driver_12, driver_12_desc,
      driver_12_thru, driver_12_thru_desc,
      driver_13, driver_13_desc,
      driver_13_thru, driver_13_thru_desc,
      driver_14, driver_14_desc,
      driver_14_thru, driver_14_thru_desc,
      driver_15, driver_15_desc,
      driver_15_thru, driver_15_thru_desc,
      driver_16, driver_16_desc,
      driver_16_thru, driver_16_thru_desc,
      driver_17, driver_17_desc,
      driver_17_thru, driver_17_thru_desc,
      driver_18, driver_18_desc,
      driver_18_thru, driver_18_thru_desc,
      driver_19, driver_19_desc,
      driver_19_thru, driver_19_thru_desc,
      driver_20, driver_20_desc,
      driver_20_thru, driver_20_thru_desc,
      driver_21, driver_21_desc,
      driver_21_thru, driver_21_thru_desc,
      driver_22, driver_22_desc,
      driver_22_thru, driver_22_thru_desc,
      driver_23, driver_23_desc,
      driver_23_thru, driver_23_thru_desc,
      driver_24, driver_24_desc,
      driver_24_thru, driver_24_thru_desc,
      driver_25, driver_25_desc,
      driver_25_thru, driver_25_thru_desc,
      driver_26, driver_26_desc,
      driver_26_thru, driver_26_thru_desc,
      driver_27, driver_27_desc,
      driver_27_thru, driver_27_thru_desc,
      driver_28, driver_28_desc,
      driver_28_thru, driver_28_thru_desc,
      driver_29, driver_29_desc,
      driver_29_thru, driver_29_thru_desc,
      driver_30, driver_30_desc,
      driver_30_thru, driver_30_thru_desc,
      binary_weight, significant_digits,
      effective_date, expiration_date,
      relation_sign, relation_amount,
      then_hold_code_flag, then_taxcode_detail_id, then_cch_taxcat_code, then_cch_group_code, then_cch_item_code,
      else_hold_code_flag, else_taxcode_detail_id, else_cch_taxcat_code, else_cch_group_code, else_cch_item_code,
      comments,
      last_used_timestamp, update_user_id, update_timestamp )
   VALUES (
      l_tb_tax_matrix.tax_matrix_id, l_tb_tax_matrix.matrix_state_code, l_tb_tax_matrix.driver_global_flag,
      l_tb_tax_matrix.driver_01, l_tb_tax_matrix.driver_01_desc,
      l_tb_tax_matrix.driver_01_thru, l_tb_tax_matrix.driver_01_thru_desc,
      l_tb_tax_matrix.driver_02, l_tb_tax_matrix.driver_02_desc,
      l_tb_tax_matrix.driver_02_thru, l_tb_tax_matrix.driver_02_thru_desc,
      l_tb_tax_matrix.driver_03, l_tb_tax_matrix.driver_03_desc,
      l_tb_tax_matrix.driver_03_thru, l_tb_tax_matrix.driver_03_thru_desc,
      l_tb_tax_matrix.driver_04, l_tb_tax_matrix.driver_04_desc,
      l_tb_tax_matrix.driver_04_thru, l_tb_tax_matrix.driver_04_thru_desc,
      l_tb_tax_matrix.driver_05, l_tb_tax_matrix.driver_05_desc,
      l_tb_tax_matrix.driver_05_thru, l_tb_tax_matrix.driver_05_thru_desc,
      l_tb_tax_matrix.driver_06, l_tb_tax_matrix.driver_06_desc,
      l_tb_tax_matrix.driver_06_thru, l_tb_tax_matrix.driver_06_thru_desc,
      l_tb_tax_matrix.driver_07, l_tb_tax_matrix.driver_07_desc,
      l_tb_tax_matrix.driver_07_thru, l_tb_tax_matrix.driver_07_thru_desc,
      l_tb_tax_matrix.driver_08, l_tb_tax_matrix.driver_08_desc,
      l_tb_tax_matrix.driver_08_thru, l_tb_tax_matrix.driver_08_thru_desc,
      l_tb_tax_matrix.driver_09, l_tb_tax_matrix.driver_09_desc,
      l_tb_tax_matrix.driver_09_thru, l_tb_tax_matrix.driver_09_thru_desc,
      l_tb_tax_matrix.driver_10, l_tb_tax_matrix.driver_10_desc,
      l_tb_tax_matrix.driver_10_thru, l_tb_tax_matrix.driver_10_thru_desc,
      l_tb_tax_matrix.driver_11, l_tb_tax_matrix.driver_11_desc,
      l_tb_tax_matrix.driver_11_thru, l_tb_tax_matrix.driver_11_thru_desc,
      l_tb_tax_matrix.driver_12, l_tb_tax_matrix.driver_12_desc,
      l_tb_tax_matrix.driver_12_thru, l_tb_tax_matrix.driver_12_thru_desc,
      l_tb_tax_matrix.driver_13, l_tb_tax_matrix.driver_13_desc,
      l_tb_tax_matrix.driver_13_thru, l_tb_tax_matrix.driver_13_thru_desc,
      l_tb_tax_matrix.driver_14, l_tb_tax_matrix.driver_14_desc,
      l_tb_tax_matrix.driver_14_thru, l_tb_tax_matrix.driver_14_thru_desc,
      l_tb_tax_matrix.driver_15, l_tb_tax_matrix.driver_15_desc,
      l_tb_tax_matrix.driver_15_thru, l_tb_tax_matrix.driver_15_thru_desc,
      l_tb_tax_matrix.driver_16, l_tb_tax_matrix.driver_16_desc,
      l_tb_tax_matrix.driver_16_thru, l_tb_tax_matrix.driver_16_thru_desc,
      l_tb_tax_matrix.driver_17, l_tb_tax_matrix.driver_17_desc,
      l_tb_tax_matrix.driver_17_thru, l_tb_tax_matrix.driver_17_thru_desc,
      l_tb_tax_matrix.driver_18, l_tb_tax_matrix.driver_18_desc,
      l_tb_tax_matrix.driver_18_thru, l_tb_tax_matrix.driver_18_thru_desc,
      l_tb_tax_matrix.driver_19, l_tb_tax_matrix.driver_19_desc,
      l_tb_tax_matrix.driver_19_thru, l_tb_tax_matrix.driver_19_thru_desc,
      l_tb_tax_matrix.driver_20, l_tb_tax_matrix.driver_20_desc,
      l_tb_tax_matrix.driver_20_thru, l_tb_tax_matrix.driver_20_thru_desc,
      l_tb_tax_matrix.driver_21, l_tb_tax_matrix.driver_21_desc,
      l_tb_tax_matrix.driver_21_thru, l_tb_tax_matrix.driver_21_thru_desc,
      l_tb_tax_matrix.driver_22, l_tb_tax_matrix.driver_22_desc,
      l_tb_tax_matrix.driver_22_thru, l_tb_tax_matrix.driver_22_thru_desc,
      l_tb_tax_matrix.driver_23, l_tb_tax_matrix.driver_23_desc,
      l_tb_tax_matrix.driver_23_thru, l_tb_tax_matrix.driver_23_thru_desc,
      l_tb_tax_matrix.driver_24, l_tb_tax_matrix.driver_24_desc,
      l_tb_tax_matrix.driver_24_thru, l_tb_tax_matrix.driver_24_thru_desc,
      l_tb_tax_matrix.driver_25, l_tb_tax_matrix.driver_25_desc,
      l_tb_tax_matrix.driver_25_thru, l_tb_tax_matrix.driver_25_thru_desc,
      l_tb_tax_matrix.driver_26, l_tb_tax_matrix.driver_26_desc,
      l_tb_tax_matrix.driver_26_thru, l_tb_tax_matrix.driver_26_thru_desc,
      l_tb_tax_matrix.driver_27, l_tb_tax_matrix.driver_27_desc,
      l_tb_tax_matrix.driver_27_thru, l_tb_tax_matrix.driver_27_thru_desc,
      l_tb_tax_matrix.driver_28, l_tb_tax_matrix.driver_28_desc,
      l_tb_tax_matrix.driver_28_thru, l_tb_tax_matrix.driver_28_thru_desc,
      l_tb_tax_matrix.driver_29, l_tb_tax_matrix.driver_29_desc,
      l_tb_tax_matrix.driver_29_thru, l_tb_tax_matrix.driver_29_thru_desc,
      l_tb_tax_matrix.driver_30, l_tb_tax_matrix.driver_30_desc,
      l_tb_tax_matrix.driver_30_thru, l_tb_tax_matrix.driver_30_thru_desc,
      l_tb_tax_matrix.binary_weight, l_tb_tax_matrix.significant_digits,
      l_tb_tax_matrix.effective_date, l_tb_tax_matrix.expiration_date,
      l_tb_tax_matrix.relation_sign, l_tb_tax_matrix.relation_amount,
      l_tb_tax_matrix.then_hold_code_flag, l_tb_tax_matrix.then_taxcode_detail_id, l_tb_tax_matrix.then_cch_taxcat_code,  l_tb_tax_matrix.then_cch_group_code,  l_tb_tax_matrix.then_cch_item_code,
      l_tb_tax_matrix.else_hold_code_flag, l_tb_tax_matrix.else_taxcode_detail_id, l_tb_tax_matrix.else_cch_taxcat_code,  l_tb_tax_matrix.else_cch_group_code,  l_tb_tax_matrix.else_cch_item_code,
      l_tb_tax_matrix.comments,
      l_tb_tax_matrix.last_used_timestamp, l_tb_tax_matrix.update_user_id, l_tb_tax_matrix.update_timestamp );

   -- ****** Read and Process Transactions ****** -----------------------------------------
    OPEN transaction_detail_cursor
    FOR EXECUTE vc_transaction_detail_stmt;
    LOOP
      FETCH transaction_detail_cursor
       INTO transaction_detail;
       
      IF (NOT FOUND) THEN 
	EXIT;
      END IF;

      -- Populate and/or Clear Transaction Detail working fields
      transaction_detail.transaction_ind := NULL;
      transaction_detail.suspend_ind := NULL;
      transaction_detail.tax_matrix_id := l_tb_tax_matrix.tax_matrix_id;
      transaction_detail.update_user_id := USER;
      transaction_detail.update_timestamp := v_sysdate;

      -- Get "Then" TaxCode Elements
      BEGIN
         SELECT taxcode_state_code,
                taxcode_type_code,
                taxcode_code,
                jurisdiction_id,
                measure_type_code,
                taxtype_code
           INTO STRICT 
		v_then_taxcode_state_code,
                v_then_taxcode_type_code,
                v_then_taxcode_code,
                v_then_jurisdiction_id,
                v_then_measure_type_code,
                v_then_taxtype_code
           FROM tb_taxcode_detail
          WHERE taxcode_detail_id = l_tb_tax_matrix.then_taxcode_detail_id;
      EXCEPTION
         WHEN OTHERS THEN
            v_then_taxcode_state_code := NULL;
            v_then_taxcode_type_code := NULL;
            v_then_taxcode_code := NULL;
            v_then_jurisdiction_id := NULL;
            v_then_measure_type_code := NULL;
            v_then_taxtype_code := NULL;
      END;

      -- Get "Else" TaxCode Elements
      BEGIN
         SELECT taxcode_state_code,
                taxcode_type_code,
                taxcode_code,
                jurisdiction_id,
                measure_type_code,
                taxtype_code
           INTO STRICT
		v_else_taxcode_state_code,
                v_else_taxcode_type_code,
                v_else_taxcode_code,
                v_else_jurisdiction_id,
                v_else_measure_type_code,
                v_else_taxtype_code
           FROM tb_taxcode_detail
          WHERE taxcode_detail_id = l_tb_tax_matrix.else_taxcode_detail_id;
      EXCEPTION
         WHEN OTHERS THEN
            v_else_taxcode_state_code := NULL;
            v_else_taxcode_type_code := NULL;
            v_else_taxcode_code := NULL;
            v_else_jurisdiction_id := NULL;
            v_else_measure_type_code := NULL;
            v_else_taxtype_code := NULL;
      END;

      -- Continue if Elements Found
      IF ( v_then_taxcode_state_code IS NOT NULL ) AND
         (( l_tb_tax_matrix.else_taxcode_detail_id IS NULL OR l_tb_tax_matrix.else_taxcode_detail_id = 0 ) OR
          ( l_tb_tax_matrix.else_taxcode_detail_id IS NOT NULL AND l_tb_tax_matrix.else_taxcode_detail_id <> 0 AND v_else_taxcode_state_code IS NOT NULL )) THEN
         -- Determine "Then" or "Else" Results
         v_relation_amount := l_tb_tax_matrix.relation_amount;
         IF l_tb_tax_matrix.relation_sign = 'na' THEN
            v_relation_amount := 0;
            v_less_hold_code_flag := l_tb_tax_matrix.then_hold_code_flag;
            v_less_taxcode_detail_id := l_tb_tax_matrix.then_taxcode_detail_id;
            v_less_taxcode_state_code := v_then_taxcode_state_code;
            v_less_taxcode_type_code := v_then_taxcode_type_code;
            v_less_taxcode_code := v_then_taxcode_code;
            v_less_jurisdiction_id := v_then_jurisdiction_id;
            v_less_measure_type_code := v_then_measure_type_code;
            v_less_taxtype_code := v_then_taxtype_code;
            v_less_cch_taxcat_code := l_tb_tax_matrix.then_cch_taxcat_code;
            v_less_cch_group_code := l_tb_tax_matrix.then_cch_group_code;
            -- v_less_cch_item_code := l_tb_tax_matrix.then_cch_item_code;
            v_equal_hold_code_flag := l_tb_tax_matrix.then_hold_code_flag;
            v_equal_taxcode_detail_id := l_tb_tax_matrix.then_taxcode_detail_id;
            v_equal_taxcode_state_code := v_then_taxcode_state_code;
            v_equal_taxcode_type_code := v_then_taxcode_type_code;
            v_equal_taxcode_code := v_then_taxcode_code;
            v_equal_jurisdiction_id := v_then_jurisdiction_id;
            v_equal_measure_type_code := v_then_measure_type_code;
            v_equal_taxtype_code := v_then_taxtype_code;
            v_equal_cch_taxcat_code := l_tb_tax_matrix.then_cch_taxcat_code;
            v_equal_cch_group_code := l_tb_tax_matrix.then_cch_group_code;
            -- v_equal_cch_item_code := l_tb_tax_matrix.then_cch_item_code;
            v_greater_hold_code_flag := l_tb_tax_matrix.then_hold_code_flag;
            v_greater_taxcode_detail_id := l_tb_tax_matrix.then_taxcode_detail_id;
            v_greater_taxcode_state_code := v_then_taxcode_state_code;
            v_greater_taxcode_type_code := v_then_taxcode_type_code;
            v_greater_taxcode_code := v_then_taxcode_code;
            v_greater_jurisdiction_id := v_then_jurisdiction_id;
            v_greater_measure_type_code := v_then_measure_type_code;
            v_greater_taxtype_code := v_then_taxtype_code;
            v_greater_cch_taxcat_code := l_tb_tax_matrix.then_cch_taxcat_code;
            v_greater_cch_group_code := l_tb_tax_matrix.then_cch_group_code;
            -- v_greater_cch_item_code := l_tb_tax_matrix.then_cch_item_code;
         ELSIF l_tb_tax_matrix.relation_sign = '=' THEN
            v_less_hold_code_flag := l_tb_tax_matrix.else_hold_code_flag;
            v_less_taxcode_detail_id := l_tb_tax_matrix.else_taxcode_detail_id;
            v_less_taxcode_state_code := v_else_taxcode_state_code;
            v_less_taxcode_type_code := v_else_taxcode_type_code;
            v_less_taxcode_code := v_else_taxcode_code;
            v_less_jurisdiction_id := v_else_jurisdiction_id;
            v_less_measure_type_code := v_else_measure_type_code;
            v_less_taxtype_code := v_else_taxtype_code;
            v_less_cch_taxcat_code := l_tb_tax_matrix.else_cch_taxcat_code;
            v_less_cch_group_code := l_tb_tax_matrix.else_cch_group_code;
            -- v_less_cch_item_code := l_tb_tax_matrix.else_cch_item_code;
            v_equal_hold_code_flag := l_tb_tax_matrix.then_hold_code_flag;
            v_equal_taxcode_detail_id := l_tb_tax_matrix.then_taxcode_detail_id;
            v_equal_taxcode_state_code := v_then_taxcode_state_code;
            v_equal_taxcode_type_code := v_then_taxcode_type_code;
            v_equal_taxcode_code := v_then_taxcode_code;
            v_equal_jurisdiction_id := v_then_jurisdiction_id;
            v_equal_measure_type_code := v_then_measure_type_code;
            v_equal_taxtype_code := v_then_taxtype_code;
            v_equal_cch_taxcat_code := l_tb_tax_matrix.then_cch_taxcat_code;
            v_equal_cch_group_code := l_tb_tax_matrix.then_cch_group_code;
            -- v_equal_cch_item_code := l_tb_tax_matrix.then_cch_item_code;
            v_greater_hold_code_flag := l_tb_tax_matrix.else_hold_code_flag;
            v_greater_taxcode_detail_id := l_tb_tax_matrix.else_taxcode_detail_id;
            v_greater_taxcode_state_code := v_else_taxcode_state_code;
            v_greater_taxcode_type_code := v_else_taxcode_type_code;
            v_greater_taxcode_code := v_else_taxcode_code;
            v_greater_jurisdiction_id := v_else_jurisdiction_id;
            v_greater_measure_type_code := v_else_measure_type_code;
            v_greater_taxtype_code := v_else_taxtype_code;
            v_greater_cch_taxcat_code := l_tb_tax_matrix.else_cch_taxcat_code;
            v_greater_cch_group_code := l_tb_tax_matrix.else_cch_group_code;
            -- v_greater_cch_item_code := l_tb_tax_matrix.else_cch_item_code;
         ELSIF l_tb_tax_matrix.relation_sign = '<' THEN
            v_less_hold_code_flag := l_tb_tax_matrix.then_hold_code_flag;
            v_less_taxcode_detail_id := l_tb_tax_matrix.then_taxcode_detail_id;
            v_less_taxcode_state_code := v_then_taxcode_state_code;
            v_less_taxcode_type_code := v_then_taxcode_type_code;
            v_less_taxcode_code := v_then_taxcode_code;
            v_less_jurisdiction_id := v_then_jurisdiction_id;
            v_less_measure_type_code := v_then_measure_type_code;
            v_less_taxtype_code := v_then_taxtype_code;
            v_less_cch_taxcat_code := l_tb_tax_matrix.then_cch_taxcat_code;
            v_less_cch_group_code := l_tb_tax_matrix.then_cch_group_code;
            -- v_less_cch_item_code := l_tb_tax_matrix.then_cch_item_code;
            v_equal_hold_code_flag := l_tb_tax_matrix.else_hold_code_flag;
            v_equal_taxcode_detail_id := l_tb_tax_matrix.else_taxcode_detail_id;
            v_equal_taxcode_state_code := v_else_taxcode_state_code;
            v_equal_taxcode_type_code := v_else_taxcode_type_code;
            v_equal_taxcode_code := v_else_taxcode_code;
            v_equal_jurisdiction_id := v_else_jurisdiction_id;
            v_equal_measure_type_code := v_else_measure_type_code;
            v_equal_taxtype_code := v_else_taxtype_code;
            v_equal_cch_taxcat_code := l_tb_tax_matrix.else_cch_taxcat_code;
            v_equal_cch_group_code := l_tb_tax_matrix.else_cch_group_code;
            -- v_equal_cch_item_code := l_tb_tax_matrix.else_cch_item_code;
            v_greater_hold_code_flag := l_tb_tax_matrix.else_hold_code_flag;
            v_greater_taxcode_detail_id := l_tb_tax_matrix.else_taxcode_detail_id;
            v_greater_taxcode_state_code := v_else_taxcode_state_code;
            v_greater_taxcode_type_code := v_else_taxcode_type_code;
            v_greater_taxcode_code := v_else_taxcode_code;
            v_greater_jurisdiction_id := v_else_jurisdiction_id;
            v_greater_measure_type_code := v_else_measure_type_code;
            v_greater_taxtype_code := v_else_taxtype_code;
            v_greater_cch_taxcat_code := l_tb_tax_matrix.else_cch_taxcat_code;
            v_greater_cch_group_code := l_tb_tax_matrix.else_cch_group_code;
            -- v_greater_cch_item_code := l_tb_tax_matrix.else_cch_item_code;
         ELSIF l_tb_tax_matrix.relation_sign = '<=' THEN
            v_less_hold_code_flag := l_tb_tax_matrix.then_hold_code_flag;
            v_less_taxcode_detail_id := l_tb_tax_matrix.then_taxcode_detail_id;
            v_less_taxcode_state_code := v_then_taxcode_state_code;
            v_less_taxcode_type_code := v_then_taxcode_type_code;
            v_less_taxcode_code := v_then_taxcode_code;
            v_less_jurisdiction_id := v_then_jurisdiction_id;
            v_less_measure_type_code := v_then_measure_type_code;
            v_less_taxtype_code := v_then_taxtype_code;
            v_less_cch_taxcat_code := l_tb_tax_matrix.then_cch_taxcat_code;
            v_less_cch_group_code := l_tb_tax_matrix.then_cch_group_code;
            -- v_less_cch_item_code := l_tb_tax_matrix.then_cch_item_code;
            v_equal_hold_code_flag := l_tb_tax_matrix.then_hold_code_flag;
            v_equal_taxcode_detail_id := l_tb_tax_matrix.then_taxcode_detail_id;
            v_equal_taxcode_state_code := v_then_taxcode_state_code;
            v_equal_taxcode_type_code := v_then_taxcode_type_code;
            v_equal_taxcode_code := v_then_taxcode_code;
            v_equal_jurisdiction_id := v_then_jurisdiction_id;
            v_equal_measure_type_code := v_then_measure_type_code;
            v_equal_taxtype_code := v_then_taxtype_code;
            v_equal_cch_taxcat_code := l_tb_tax_matrix.then_cch_taxcat_code;
            v_equal_cch_group_code := l_tb_tax_matrix.then_cch_group_code;
            -- v_equal_cch_item_code := l_tb_tax_matrix.then_cch_item_code;
            v_greater_hold_code_flag := l_tb_tax_matrix.else_hold_code_flag;
            v_greater_taxcode_detail_id := l_tb_tax_matrix.else_taxcode_detail_id;
            v_greater_taxcode_state_code := v_else_taxcode_state_code;
            v_greater_taxcode_type_code := v_else_taxcode_type_code;
            v_greater_taxcode_code := v_else_taxcode_code;
            v_greater_jurisdiction_id := v_else_jurisdiction_id;
            v_greater_measure_type_code := v_else_measure_type_code;
            v_greater_taxtype_code := v_else_taxtype_code;
            v_greater_cch_taxcat_code := l_tb_tax_matrix.else_cch_taxcat_code;
            v_greater_cch_group_code := l_tb_tax_matrix.else_cch_group_code;
            -- v_greater_cch_item_code := l_tb_tax_matrix.else_cch_item_code;
         ELSIF l_tb_tax_matrix.relation_sign = '>' THEN
            v_less_hold_code_flag := l_tb_tax_matrix.else_hold_code_flag;
            v_less_taxcode_detail_id := l_tb_tax_matrix.else_taxcode_detail_id;
            v_less_taxcode_state_code := v_else_taxcode_state_code;
            v_less_taxcode_type_code := v_else_taxcode_type_code;
            v_less_taxcode_code := v_else_taxcode_code;
            v_less_jurisdiction_id := v_else_jurisdiction_id;
            v_less_measure_type_code := v_else_measure_type_code;
            v_less_taxtype_code := v_else_taxtype_code;
            v_less_cch_taxcat_code := l_tb_tax_matrix.else_cch_taxcat_code;
            v_less_cch_group_code := l_tb_tax_matrix.else_cch_group_code;
            -- v_less_cch_item_code := l_tb_tax_matrix.else_cch_item_code;
            v_equal_hold_code_flag := l_tb_tax_matrix.else_hold_code_flag;
            v_equal_taxcode_detail_id := l_tb_tax_matrix.else_taxcode_detail_id;
            v_equal_taxcode_state_code := v_else_taxcode_state_code;
            v_equal_taxcode_type_code := v_else_taxcode_type_code;
            v_equal_taxcode_code := v_else_taxcode_code;
            v_equal_jurisdiction_id := v_else_jurisdiction_id;
            v_equal_measure_type_code := v_else_measure_type_code;
            v_equal_taxtype_code := v_else_taxtype_code;
            v_equal_cch_taxcat_code := l_tb_tax_matrix.else_cch_taxcat_code;
            v_equal_cch_group_code := l_tb_tax_matrix.else_cch_group_code;
            -- v_equal_cch_item_code := l_tb_tax_matrix.else_cch_item_code;
            v_greater_hold_code_flag := l_tb_tax_matrix.then_hold_code_flag;
            v_greater_taxcode_detail_id := l_tb_tax_matrix.then_taxcode_detail_id;
            v_greater_taxcode_state_code := v_then_taxcode_state_code;
            v_greater_taxcode_type_code := v_then_taxcode_type_code;
            v_greater_taxcode_code := v_then_taxcode_code;
            v_greater_jurisdiction_id := v_then_jurisdiction_id;
            v_greater_measure_type_code := v_then_measure_type_code;
            v_greater_taxtype_code := v_then_taxtype_code;
            v_greater_cch_taxcat_code := l_tb_tax_matrix.then_cch_taxcat_code;
            v_greater_cch_group_code := l_tb_tax_matrix.then_cch_group_code;
            -- v_greater_cch_item_code := l_tb_tax_matrix.then_cch_item_code;
         ELSIF l_tb_tax_matrix.relation_sign = '>=' THEN
            v_less_hold_code_flag := l_tb_tax_matrix.else_hold_code_flag;
            v_less_taxcode_detail_id := l_tb_tax_matrix.else_taxcode_detail_id;
            v_less_taxcode_state_code := v_else_taxcode_state_code;
            v_less_taxcode_type_code := v_else_taxcode_type_code;
            v_less_taxcode_code := v_else_taxcode_code;
            v_less_jurisdiction_id := v_else_jurisdiction_id;
            v_less_measure_type_code := v_else_measure_type_code;
            v_less_taxtype_code := v_else_taxtype_code;
            v_less_cch_taxcat_code := l_tb_tax_matrix.else_cch_taxcat_code;
            v_less_cch_group_code := l_tb_tax_matrix.else_cch_group_code;
            -- v_less_cch_item_code := l_tb_tax_matrix.else_cch_item_code;
            v_equal_hold_code_flag := l_tb_tax_matrix.then_hold_code_flag;
            v_equal_taxcode_detail_id := l_tb_tax_matrix.then_taxcode_detail_id;
            v_equal_taxcode_state_code := v_then_taxcode_state_code;
            v_equal_taxcode_type_code := v_then_taxcode_type_code;
            v_equal_taxcode_code := v_then_taxcode_code;
            v_equal_jurisdiction_id := v_then_jurisdiction_id;
            v_equal_measure_type_code := v_then_measure_type_code;
            v_equal_taxtype_code := v_then_taxtype_code;
            v_equal_cch_taxcat_code := l_tb_tax_matrix.then_cch_taxcat_code;
            v_equal_cch_group_code := l_tb_tax_matrix.then_cch_group_code;
            -- v_equal_cch_item_code := l_tb_tax_matrix.then_cch_item_code;
            v_greater_hold_code_flag := l_tb_tax_matrix.then_hold_code_flag;
            v_greater_taxcode_detail_id := l_tb_tax_matrix.then_taxcode_detail_id;
            v_greater_taxcode_state_code := v_then_taxcode_state_code;
            v_greater_taxcode_type_code := v_then_taxcode_type_code;
            v_greater_taxcode_code := v_then_taxcode_code;
            v_greater_jurisdiction_id := v_then_jurisdiction_id;
            v_greater_measure_type_code := v_then_measure_type_code;
            v_greater_taxtype_code := v_then_taxtype_code;
            v_greater_cch_taxcat_code := l_tb_tax_matrix.then_cch_taxcat_code;
            v_greater_cch_group_code := l_tb_tax_matrix.then_cch_group_code;
            -- v_greater_cch_item_code := l_tb_tax_matrix.then_cch_item_code;
         ELSIF l_tb_tax_matrix.relation_sign = '<>' THEN
            v_less_hold_code_flag := l_tb_tax_matrix.then_hold_code_flag;
            v_less_taxcode_detail_id := l_tb_tax_matrix.then_taxcode_detail_id;
            v_less_taxcode_state_code := v_then_taxcode_state_code;
            v_less_taxcode_type_code := v_then_taxcode_type_code;
            v_less_taxcode_code := v_then_taxcode_code;
            v_less_jurisdiction_id := v_then_jurisdiction_id;
            v_less_measure_type_code := v_then_measure_type_code;
            v_less_taxtype_code := v_then_taxtype_code;
            v_less_cch_taxcat_code := l_tb_tax_matrix.then_cch_taxcat_code;
            v_less_cch_group_code := l_tb_tax_matrix.then_cch_group_code;
            -- v_less_cch_item_code := l_tb_tax_matrix.then_cch_item_code;
            v_equal_hold_code_flag := l_tb_tax_matrix.else_hold_code_flag;
            v_equal_taxcode_detail_id := l_tb_tax_matrix.else_taxcode_detail_id;
            v_equal_taxcode_state_code := v_else_taxcode_state_code;
            v_equal_taxcode_type_code := v_else_taxcode_type_code;
            v_equal_taxcode_code := v_else_taxcode_code;
            v_equal_jurisdiction_id := v_else_jurisdiction_id;
            v_equal_measure_type_code := v_else_measure_type_code;
            v_equal_taxtype_code := v_else_taxtype_code;
            v_equal_cch_taxcat_code := l_tb_tax_matrix.else_cch_taxcat_code;
            v_equal_cch_group_code := l_tb_tax_matrix.else_cch_group_code;
            -- v_equal_cch_item_code := l_tb_tax_matrix.else_cch_item_code;
            v_greater_hold_code_flag := l_tb_tax_matrix.then_hold_code_flag;
            v_greater_taxcode_detail_id := l_tb_tax_matrix.then_taxcode_detail_id;
            v_greater_taxcode_state_code := v_then_taxcode_state_code;
            v_greater_taxcode_type_code := v_then_taxcode_type_code;
            v_greater_taxcode_code := v_then_taxcode_code;
            v_greater_jurisdiction_id := v_then_jurisdiction_id;
            v_greater_measure_type_code := v_then_measure_type_code;
            v_greater_taxtype_code := v_then_taxtype_code;
            v_greater_cch_taxcat_code := l_tb_tax_matrix.then_cch_taxcat_code;
            v_greater_cch_group_code := l_tb_tax_matrix.then_cch_group_code;
            -- v_greater_cch_item_code := l_tb_tax_matrix.then_cch_item_code;
         END IF;

         SELECT CASE SIGN(transaction_detail.gl_line_itm_dist_amt - v_relation_amount) WHEN -1 THEN v_less_hold_code_flag WHEN 0 THEN v_equal_hold_code_flag WHEN 1 THEN v_greater_hold_code_flag END,
                CASE SIGN(transaction_detail.gl_line_itm_dist_amt - v_relation_amount) WHEN -1 THEN v_less_taxcode_detail_id WHEN 0 THEN v_equal_taxcode_detail_id WHEN 1 THEN v_greater_taxcode_detail_id END,
                CASE SIGN(transaction_detail.gl_line_itm_dist_amt - v_relation_amount) WHEN -1 THEN v_less_taxcode_state_code WHEN 0 THEN v_equal_taxcode_state_code WHEN 1 THEN v_greater_taxcode_state_code END,
                CASE SIGN(transaction_detail.gl_line_itm_dist_amt - v_relation_amount) WHEN -1 THEN v_less_taxcode_type_code WHEN 0 THEN v_equal_taxcode_type_code WHEN 1 THEN v_greater_taxcode_type_code END,
                CASE SIGN(transaction_detail.gl_line_itm_dist_amt - v_relation_amount) WHEN -1 THEN v_less_taxcode_code WHEN 0 THEN v_equal_taxcode_code WHEN 1 THEN v_greater_taxcode_code END,
                CASE SIGN(transaction_detail.gl_line_itm_dist_amt - v_relation_amount) WHEN -1 THEN v_less_jurisdiction_id WHEN 0 THEN v_equal_jurisdiction_id WHEN 1 THEN v_greater_jurisdiction_id END,
                CASE SIGN(transaction_detail.gl_line_itm_dist_amt - v_relation_amount) WHEN -1 THEN v_less_measure_type_code WHEN 0 THEN v_equal_measure_type_code WHEN 1 THEN v_greater_measure_type_code END,
                CASE SIGN(transaction_detail.gl_line_itm_dist_amt - v_relation_amount) WHEN -1 THEN v_less_taxtype_code WHEN 0 THEN v_equal_taxtype_code WHEN 1 THEN v_greater_taxtype_code END,
                CASE SIGN(transaction_detail.gl_line_itm_dist_amt - v_relation_amount) WHEN -1 THEN v_less_cch_taxcat_code WHEN 0 THEN v_equal_cch_taxcat_code WHEN 1 THEN v_greater_cch_taxcat_code END,
                CASE SIGN(transaction_detail.gl_line_itm_dist_amt - v_relation_amount) WHEN -1 THEN v_less_cch_group_code WHEN 0 THEN v_equal_cch_group_code WHEN 1 THEN v_greater_cch_group_code END
           INTO STRICT v_hold_code_flag, transaction_detail.taxcode_detail_id, transaction_detail.taxcode_state_code, transaction_detail.taxcode_type_code, transaction_detail.taxcode_code, v_override_jurisdiction_id, transaction_detail.measure_type_code, v_taxtype_code, transaction_detail.cch_taxcat_code, transaction_detail.cch_group_code;

         -- ****** DETERMINE TYPE of TAXCODE ****** --
         -- The Tax Code is Taxable
         IF transaction_detail.taxcode_type_code = 'T' THEN
            -- If Override jurisdiction id is not blank or 0 then use it instead of location matrix
            IF v_override_jurisdiction_id IS NOT NULL AND v_override_jurisdiction_id <> 0 THEN
               transaction_detail.jurisdiction_id := v_override_jurisdiction_id;
               transaction_detail.manual_jurisdiction_ind := 'AO';
               location_matrix.override_taxtype_code := '*NO';
               location_matrix.state_flag := '1';                             -- MBF01
               location_matrix.county_flag := '1';                            -- MBF01
               location_matrix.county_local_flag := '1';                      -- MBF01
               location_matrix.city_flag := '1';                              -- MBF01
               location_matrix.city_local_flag := '1';                        -- MBF01
            ELSE
               -- Search for Location Matrix if not already found AND not already manually applied!!
               IF transaction_detail.location_matrix_id IS NULL AND transaction_detail.jurisdiction_id IS NULL THEN
                  v_transaction_detail_id := transaction_detail.transaction_detail_id;
                  -- Search Location Matrix for matches
                  BEGIN
		     -- ****** CREATE Temporary Table to hold "Bind" Values ****** -----------------------------------------
		     BEGIN
		       EXECUTE 'CREATE TEMPORARY TABLE sp_gen_location_driver_tmp (param_name varchar(30), param_value varchar(30))';
		     EXCEPTION 
		       -- Table may already exist if using Connection/Session Pooling, Ignore
		       WHEN OTHERS THEN NULL;
		     END;
		      
		     -- Prep TMP table with values
		     DELETE FROM sp_gen_location_driver_tmp;
		     INSERT INTO sp_gen_location_driver_tmp(param_name, param_value) VALUES ('v_transaction_detail_id',  v_transaction_detail_id);

		     OPEN location_matrix_cursor
                       FOR EXECUTE vc_location_matrix_stmt;

                     FETCH location_matrix_cursor
                      INTO location_matrix;

                     -- Rows found?
                     IF (FOUND) THEN
                        vn_fetch_rows := 1;
                     ELSE
                        vn_fetch_rows := 0;
                     END IF;

                     CLOSE location_matrix_cursor;

		     BEGIN
		       EXECUTE 'DROP TABLE sp_gen_location_driver_tmp ';
		     EXCEPTION 
		       WHEN OTHERS THEN NULL;
		     END;

                  EXCEPTION
                     WHEN OTHERS THEN
                        vn_fetch_rows := 0;
                  END;

                  -- Location Matrix Line Found
                  IF vn_fetch_rows > 0 THEN
                     transaction_detail.location_matrix_id := location_matrix.location_matrix_id;
                     transaction_detail.jurisdiction_id := location_matrix.jurisdiction_id;
                  ELSE
                     transaction_detail.transaction_ind := 'S';
                     transaction_detail.suspend_ind := 'L';
                  END IF;
               -- Get Location Matrix if it exists AND not already manually applied!!
               ELSIF transaction_detail.location_matrix_id IS NOT NULL AND (v_override_jurisdiction_id IS NULL OR v_override_jurisdiction_id = 0) THEN
                  BEGIN
                     SELECT override_taxtype_code,
                            state_flag, county_flag, county_local_flag, city_flag, city_local_flag
                       INTO STRICT 
			    location_matrix.override_taxtype_code,
                            location_matrix.state_flag,
                            location_matrix.county_flag,
                            location_matrix.county_local_flag,
                            location_matrix.city_flag,
                            location_matrix.city_local_flag
                       FROM tb_location_matrix
                      WHERE tb_location_matrix.location_matrix_id = transaction_detail.location_matrix_id;
                  EXCEPTION
                     WHEN NO_DATA_FOUND THEN
                        location_matrix.override_taxtype_code := '*NO';
                        location_matrix.state_flag := '1';
                        location_matrix.county_flag := '1';
                        location_matrix.county_local_flag := '1';
                        location_matrix.city_flag := '1';
                        location_matrix.city_local_flag := '1';
                  END;
               ELSE
                  location_matrix.override_taxtype_code := '*NO';
                  location_matrix.state_flag := '1';
                  location_matrix.county_flag := '1';
                  location_matrix.county_local_flag := '1';
                  location_matrix.city_flag := '1';
                  location_matrix.city_local_flag := '1';
               END IF;
            END IF;

            -- Continue if not Suspended
            IF transaction_detail.transaction_ind IS NULL OR transaction_detail.transaction_ind <> 'S' THEN
               -- Get Jurisdiction TaxRates and calculate Tax Amounts
               EXECUTE sp_getusetax(transaction_detail.jurisdiction_id,
                            transaction_detail.gl_date,
                            transaction_detail.measure_type_code,
                            transaction_detail.gl_line_itm_dist_amt,
                            v_taxtype_code,
                            location_matrix.override_taxtype_code,
                            location_matrix.state_flag,
                            location_matrix.county_flag,
                            location_matrix.county_local_flag,
                            location_matrix.city_flag,
                            location_matrix.city_local_flag,
                            transaction_detail.invoice_freight_amt,
                            transaction_detail.invoice_discount_amt,
                            transaction_detail.transaction_state_code,
-- future? --                     transaction_detail.import_definition_code,
                            'importdefn',
                            transaction_detail.taxcode_code,
                            transaction_detail.jurisdiction_taxrate_id,
                            transaction_detail.state_use_amount,
                            transaction_detail.state_use_tier2_amount,
                            transaction_detail.state_use_tier3_amount,
                            transaction_detail.county_use_amount,
                            transaction_detail.county_local_use_amount,
                            transaction_detail.city_use_amount,
                            transaction_detail.city_local_use_amount,
                            transaction_detail.state_use_rate,
                            transaction_detail.state_use_tier2_rate,
                            transaction_detail.state_use_tier3_rate,
                            transaction_detail.state_split_amount,
                            transaction_detail.state_tier2_min_amount,
                            transaction_detail.state_tier2_max_amount,
                            transaction_detail.state_maxtax_amount,
                            transaction_detail.county_use_rate,
                            transaction_detail.county_local_use_rate,
                            transaction_detail.county_split_amount,
                            transaction_detail.county_maxtax_amount,
                            transaction_detail.county_single_flag,
                            transaction_detail.county_default_flag,
                            transaction_detail.city_use_rate,
                            transaction_detail.city_local_use_rate,
                            transaction_detail.city_split_amount,
                            transaction_detail.city_split_use_rate,
                            transaction_detail.city_single_flag,
                            transaction_detail.city_default_flag,
                            transaction_detail.combined_use_rate,
                            transaction_detail.tb_calc_tax_amt,
-- future? --                     transaction_detail.taxable_amt,
-- Targa? --                transaction_detail.user_number_10,
                            vn_taxable_amt,
                            v_taxtype_used );

               -- Suspend if tax rate id is null
               IF transaction_detail.jurisdiction_taxrate_id IS NULL OR transaction_detail.jurisdiction_taxrate_id = 0 THEN
                  transaction_detail.transaction_ind := 'S';
                  transaction_detail.suspend_ind := 'R';
               ELSE
                  -- Check for Taxable Amount -- 3351
                  IF vn_taxable_amt <> transaction_detail.gl_line_itm_dist_amt THEN
                     transaction_detail.gl_extract_amt := transaction_detail.gl_line_itm_dist_amt;
                     transaction_detail.gl_line_itm_dist_amt := vn_taxable_amt;
                  END IF;
               END IF;
            END IF;

         -- The Tax Code is Exempt
         ELSIF transaction_detail.taxcode_type_code = 'E' THEN
            transaction_detail.transaction_ind := 'P';

         -- The Tax Code is a Question
         ELSIF transaction_detail.taxcode_type_code = 'Q' THEN
           transaction_detail.transaction_ind := 'Q';

         -- The Tax Code is Unrecognized - Suspend
         ELSE
            transaction_detail.transaction_ind := 'S';
            transaction_detail.suspend_ind := '?';
         END IF;

         -- Continue if not Suspended
         IF transaction_detail.transaction_ind IS NULL OR transaction_detail.transaction_ind <> 'S' THEN
            -- Check for matrix "H"old
            IF ( v_hold_code_flag = '1' ) AND
               ( transaction_detail.taxcode_type_code = 'T' OR transaction_detail.taxcode_type_code = 'E') THEN
                  transaction_detail.transaction_ind := 'H';
            ELSE
                transaction_detail.transaction_ind := 'P';
            END IF;
         END IF;

      ELSE
         -- Elements NOT Found
         transaction_detail.transaction_ind := 'S';
         transaction_detail.suspend_ind := '?';
      END IF;

      -- Update transaction detail row
      BEGIN
         -- 3411 - add taxable amount and tax type used.
         UPDATE tb_transaction_detail
            SET gl_line_itm_dist_amt = transaction_detail.gl_line_itm_dist_amt,
                tb_calc_tax_amt = transaction_detail.tb_calc_tax_amt,
                state_use_amount = transaction_detail.state_use_amount,
                state_use_tier2_amount = transaction_detail.state_use_tier2_amount,
                state_use_tier3_amount = transaction_detail.state_use_tier3_amount,
                county_use_amount = transaction_detail.county_use_amount,
                county_local_use_amount = transaction_detail.county_local_use_amount,
                city_use_amount = transaction_detail.city_use_amount,
                city_local_use_amount = transaction_detail.city_local_use_amount,
                transaction_state_code = transaction_detail.transaction_state_code,
                transaction_ind = transaction_detail.transaction_ind,
                suspend_ind = transaction_detail.suspend_ind,
                taxcode_detail_id = transaction_detail.taxcode_detail_id,
                taxcode_state_code = transaction_detail.taxcode_state_code,
                taxcode_type_code = transaction_detail.taxcode_type_code,
                taxcode_code = transaction_detail.taxcode_code,
                cch_taxcat_code = transaction_detail.cch_taxcat_code,
                cch_group_code = transaction_detail.cch_group_code,
                tax_matrix_id = transaction_detail.tax_matrix_id,
                location_matrix_id = transaction_detail.location_matrix_id,
                jurisdiction_id = transaction_detail.jurisdiction_id,
                jurisdiction_taxrate_id = transaction_detail.jurisdiction_taxrate_id,
                manual_jurisdiction_ind = transaction_detail.manual_jurisdiction_ind,
                measure_type_code = transaction_detail.measure_type_code,
                state_use_rate = transaction_detail.state_use_rate,
                state_use_tier2_rate = transaction_detail.state_use_tier2_rate,
                state_use_tier3_rate = transaction_detail.state_use_tier3_rate,
                state_split_amount = transaction_detail.state_split_amount,
                state_tier2_min_amount = transaction_detail.state_tier2_min_amount,
                state_tier2_max_amount = transaction_detail.state_tier2_max_amount,
                state_maxtax_amount = transaction_detail.state_maxtax_amount,
                county_use_rate = transaction_detail.county_use_rate,
                county_local_use_rate = transaction_detail.county_local_use_rate,
                county_split_amount = transaction_detail.county_split_amount,
                county_maxtax_amount = transaction_detail.county_maxtax_amount,
                county_single_flag = transaction_detail.county_single_flag,
                county_default_flag = transaction_detail.county_default_flag,
                city_use_rate = transaction_detail.city_use_rate,
                city_local_use_rate = transaction_detail.city_local_use_rate,
                city_split_amount = transaction_detail.city_split_amount,
                city_split_use_rate = transaction_detail.city_split_use_rate,
                city_single_flag = transaction_detail.city_single_flag,
                city_default_flag = transaction_detail.city_default_flag,
                combined_use_rate = transaction_detail.combined_use_rate,
                gl_extract_amt = transaction_detail.gl_extract_amt,
                update_user_id = transaction_detail.update_user_id,
                update_timestamp = transaction_detail.update_timestamp
          WHERE transaction_detail_id = transaction_detail.transaction_detail_id;
        
      END;
   END LOOP;
   CLOSE transaction_detail_cursor;

END IF;


END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
ALTER FUNCTION sp_tb_tax_matrix_a_i(numeric) OWNER TO stscorp;
