-- Function: sp_tb_transaction_detail_a_u(numeric, numeric, character varying, character varying, character varying, numeric, numeric, character varying, character varying, character varying, numeric, character varying, character varying, character varying, numeric, numeric, character varying, character varying, character varying)

DROP FUNCTION sp_tb_transaction_detail_a_u(numeric, numeric, character varying, character varying, character varying, numeric, numeric, character varying, character varying, character varying, numeric, character varying, character varying, character varying, numeric, numeric, character varying, character varying, character varying);

CREATE OR REPLACE FUNCTION sp_tb_transaction_detail_a_u(p_transaction_detail_id numeric, p_old_taxcode_detail_id numeric, p_old_taxcode_state_code character varying, p_old_taxcode_type_code character varying, p_old_taxcode_code character varying, p_old_jurisdiction_id numeric, p_old_jurisdiction_taxrate_id numeric, p_old_cch_taxcat_code character varying, p_old_cch_group_code character varying, p_old_cch_item_code character varying, p_new_taxcode_detail_id numeric, p_new_taxcode_state_code character varying, p_new_taxcode_type_code character varying, p_new_taxcode_code character varying, p_new_jurisdiction_id numeric, p_new_jurisdiction_taxrate_id numeric, p_new_cch_taxcat_code character varying, p_new_cch_group_code character varying, p_new_cch_item_code character varying)
  RETURNS void AS
$BODY$
	BEGIN
	  
	      INSERT INTO tb_transaction_detail_excp (
		 transaction_detail_excp_id,
		 transaction_detail_id,
		 old_taxcode_detail_id,
		 old_taxcode_state_code,
		 old_taxcode_type_code,
		 old_taxcode_code,
		 old_jurisdiction_id,
		 old_jurisdiction_taxrate_id,
		 old_cch_taxcat_code,
		 old_cch_group_code,
		 old_cch_item_code,
		 new_taxcode_detail_id,
		 new_taxcode_state_code,
		 new_taxcode_type_code,
		 new_taxcode_code,
		 new_jurisdiction_id,
		 new_jurisdiction_taxrate_id,
		 new_cch_taxcat_code,
		 new_cch_group_code,
		 new_cch_item_code,
		 update_user_id,
		 update_timestamp )
	      VALUES (
		 NEXTVAL('SQ_TB_TRANS_DETAIL_EXCP_ID'),
		 p_transaction_detail_id,
		 p_old_taxcode_detail_id,
		 p_old_taxcode_state_code,
		 p_old_taxcode_type_code,
		 p_old_taxcode_code,
		 p_old_jurisdiction_id,
		 p_old_jurisdiction_taxrate_id,
		 p_old_cch_taxcat_code,
		 p_old_cch_group_code,
		 p_old_cch_item_code,
		 p_new_taxcode_detail_id,
		 p_new_taxcode_state_code,
		 p_new_taxcode_type_code,
		 p_new_taxcode_code,
		 p_new_jurisdiction_id,
		 p_new_jurisdiction_taxrate_id,
		 p_new_cch_taxcat_code,
		 p_new_cch_group_code,
		 p_new_cch_item_code,
		 CURRENT_USER,
		 SYS_EXTRACT_UTC(CURRENT_TIMESTAMP));/****UTC Timestamp ##SRINI###*****/

	END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
ALTER FUNCTION sp_tb_transaction_detail_a_u(numeric, numeric, character varying, character varying, character varying, numeric, numeric, character varying, character varying, character varying, numeric, character varying, character varying, character varying, numeric, numeric, character varying, character varying, character varying) OWNER TO stscorp;
