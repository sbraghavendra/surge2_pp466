CREATE  TABLE TB_TMP_LOCATION_MATRIX
(
  LOCATION_MATRIX_ID          NUMERIC,
  DRIVER_01                   VARCHAR(100),
  DRIVER_01_DESC              VARCHAR(100),
  DRIVER_02                   VARCHAR(100),
  DRIVER_02_DESC              VARCHAR(100),
  DRIVER_03                   VARCHAR(100),
  DRIVER_03_DESC              VARCHAR(100),
  DRIVER_04                   VARCHAR(100),
  DRIVER_04_DESC              VARCHAR(100),
  DRIVER_05                   VARCHAR(100),
  DRIVER_05_DESC              VARCHAR(100),
  DRIVER_06                   VARCHAR(100),
  DRIVER_06_DESC              VARCHAR(100),
  DRIVER_07                   VARCHAR(100),
  DRIVER_07_DESC              VARCHAR(100),
  DRIVER_08                   VARCHAR(100),
  DRIVER_08_DESC              VARCHAR(100),
  DRIVER_09                   VARCHAR(100),
  DRIVER_09_DESC              VARCHAR(100),
  DRIVER_10                   VARCHAR(100),
  DRIVER_10_DESC              VARCHAR(100),
  BINARY_WEIGHT               NUMERIC,
  SIGNIFICANT_DIGITS          VARCHAR(39),
  DEFAULT_FLAG                CHAR(1),
  DEFAULT_BINARY_WEIGHT       NUMERIC,
  DEFAULT_SIGNIFICANT_DIGITS  VARCHAR(39),
  EFFECTIVE_DATE              DATE,
  EXPIRATION_DATE             DATE,
  JURISDICTION_ID             NUMERIC,
  STATE                       VARCHAR(10),
  COMMENTS                    VARCHAR(255),
  LAST_USED_TIMESTAMP         DATE,
  UPDATE_USER_ID              VARCHAR(40),
  UPDATE_TIMESTAMP            DATE
);


CREATE  TABLE TB_TMP_COMPUTE_DISTAMT_1
(
  COMPUTE_COUNT       NUMERIC,
  COMPUTE_DISTAMT     NUMERIC,
  COMPUTE_TAXAMT      NUMERIC,
  COMPUTE_EFFTAXRATE  NUMERIC,
  COMPUTE_MAXDISTAMT  NUMERIC,
  COMPUTE_AVGDISTAMT  NUMERIC
);
CREATE  TABLE TB_TMP_COMPUTE_DISTAMT_2
(
  COMPUTE_COUNT       NUMERIC,
  COMPUTE_DISTAMT     NUMERIC,
  COMPUTE_TAXAMT      NUMERIC,
  COMPUTE_EFFTAXRATE  NUMERIC,
  COMPUTE_MAXDISTAMT  NUMERIC,
  COMPUTE_AVGDISTAMT  NUMERIC
);


CREATE  TABLE TB_TMP_GL_EXPORT
(
  TRANSACTION_DETAIL_ID        NUMERIC,
  SOURCE_TRANSACTION_ID        VARCHAR(100),
  PROCESS_BATCH_NO             NUMERIC,
  GL_EXTRACT_BATCH_NO          NUMERIC,
  ARCHIVE_BATCH_NO             NUMERIC,
  ALLOCATION_MATRIX_ID         NUMERIC,
  ALLOCATION_SUBTRANS_ID       NUMERIC,
  ENTERED_DATE                 DATE,
  TRANSACTION_STATUS           VARCHAR(10),
  GL_DATE                      DATE,
  GL_COMPANY_NBR               VARCHAR(100),
  GL_COMPANY_NAME              VARCHAR(100),
  GL_DIVISION_NBR              VARCHAR(100),
  GL_DIVISION_NAME             VARCHAR(100),
  GL_CC_NBR_DEPT_ID            VARCHAR(100),
  GL_CC_NBR_DEPT_NAME          VARCHAR(100),
  GL_LOCAL_ACCT_NBR            VARCHAR(100),
  GL_LOCAL_ACCT_NAME           VARCHAR(100),
  GL_LOCAL_SUB_ACCT_NBR        VARCHAR(100),
  GL_LOCAL_SUB_ACCT_NAME       VARCHAR(100),
  GL_FULL_ACCT_NBR             VARCHAR(100),
  GL_FULL_ACCT_NAME            VARCHAR(100),
  GL_LINE_ITM_DIST_AMT         NUMERIC,
  ORIG_GL_LINE_ITM_DIST_AMT    NUMERIC,
  VENDOR_NBR                   VARCHAR(100),
  VENDOR_NAME                  VARCHAR(100),
  VENDOR_ADDRESS_LINE_1        VARCHAR(100),
  VENDOR_ADDRESS_LINE_2        VARCHAR(100),
  VENDOR_ADDRESS_LINE_3        VARCHAR(100),
  VENDOR_ADDRESS_LINE_4        VARCHAR(100),
  VENDOR_ADDRESS_CITY          VARCHAR(100),
  VENDOR_ADDRESS_COUNTY        VARCHAR(100),
  VENDOR_ADDRESS_STATE         VARCHAR(100),
  VENDOR_ADDRESS_ZIP           VARCHAR(100),
  VENDOR_ADDRESS_COUNTRY       VARCHAR(100),
  VENDOR_TYPE                  VARCHAR(100),
  VENDOR_TYPE_NAME             VARCHAR(100),
  INVOICE_NBR                  VARCHAR(100),
  INVOICE_DESC                 VARCHAR(100),
  INVOICE_DATE                 DATE,
  INVOICE_FREIGHT_AMT          NUMERIC,
  INVOICE_DISCOUNT_AMT         NUMERIC,
  INVOICE_TAX_AMT              NUMERIC,
  INVOICE_TOTAL_AMT            NUMERIC,
  INVOICE_TAX_FLG              VARCHAR(100),
  INVOICE_LINE_NBR             VARCHAR(100),
  INVOICE_LINE_NAME            VARCHAR(100),
  INVOICE_LINE_TYPE            VARCHAR(100),
  INVOICE_LINE_TYPE_NAME       VARCHAR(100),
  INVOICE_LINE_AMT             NUMERIC,
  INVOICE_LINE_TAX             NUMERIC,
  AFE_PROJECT_NBR              VARCHAR(100),
  AFE_PROJECT_NAME             VARCHAR(100),
  AFE_CATEGORY_NBR             VARCHAR(100),
  AFE_CATEGORY_NAME            VARCHAR(100),
  AFE_SUB_CAT_NBR              VARCHAR(100),
  AFE_SUB_CAT_NAME             VARCHAR(100),
  AFE_USE                      VARCHAR(100),
  AFE_CONTRACT_TYPE            VARCHAR(100),
  AFE_CONTRACT_STRUCTURE       VARCHAR(100),
  AFE_PROPERTY_CAT             VARCHAR(100),
  INVENTORY_NBR                VARCHAR(100),
  INVENTORY_NAME               VARCHAR(100),
  INVENTORY_CLASS              VARCHAR(100),
  INVENTORY_CLASS_NAME         VARCHAR(100),
  PO_NBR                       VARCHAR(100),
  PO_NAME                      VARCHAR(100),
  PO_DATE                      DATE,
  PO_LINE_NBR                  VARCHAR(100),
  PO_LINE_NAME                 VARCHAR(100),
  PO_LINE_TYPE                 VARCHAR(100),
  PO_LINE_TYPE_NAME            VARCHAR(100),
  SHIP_TO_LOCATION             VARCHAR(100),
  SHIP_TO_LOCATION_NAME        VARCHAR(100),
  SHIP_TO_ADDRESS_LINE_1       VARCHAR(100),
  SHIP_TO_ADDRESS_LINE_2       VARCHAR(100),
  SHIP_TO_ADDRESS_LINE_3       VARCHAR(100),
  SHIP_TO_ADDRESS_LINE_4       VARCHAR(100),
  SHIP_TO_ADDRESS_CITY         VARCHAR(100),
  SHIP_TO_ADDRESS_COUNTY       VARCHAR(100),
  SHIP_TO_ADDRESS_STATE        VARCHAR(100),
  SHIP_TO_ADDRESS_ZIP          VARCHAR(100),
  SHIP_TO_ADDRESS_COUNTRY      VARCHAR(100),
  WO_NBR                       VARCHAR(100),
  WO_NAME                      VARCHAR(100),
  WO_DATE                      DATE,
  WO_TYPE                      VARCHAR(100),
  WO_TYPE_DESC                 VARCHAR(100),
  WO_CLASS                     VARCHAR(100),
  WO_CLASS_DESC                VARCHAR(100),
  WO_ENTITY                    VARCHAR(100),
  WO_ENTITY_DESC               VARCHAR(100),
  WO_LINE_NBR                  VARCHAR(100),
  WO_LINE_NAME                 VARCHAR(100),
  WO_LINE_TYPE                 VARCHAR(100),
  WO_LINE_TYPE_DESC            VARCHAR(100),
  WO_SHUT_DOWN_CD              VARCHAR(100),
  WO_SHUT_DOWN_CD_DESC         VARCHAR(100),
  VOUCHER_ID                   VARCHAR(100),
  VOUCHER_NAME                 VARCHAR(100),
  VOUCHER_DATE                 DATE,
  VOUCHER_LINE_NBR             VARCHAR(100),
  VOUCHER_LINE_DESC            VARCHAR(100),
  CHECK_NBR                    VARCHAR(100),
  CHECK_NO                     NUMERIC,
  CHECK_DATE                   DATE,
  CHECK_AMT                    NUMERIC,
  CHECK_DESC                   VARCHAR(100),
  USER_TEXT_01                 VARCHAR(100),
  USER_TEXT_02                 VARCHAR(100),
  USER_TEXT_03                 VARCHAR(100),
  USER_TEXT_04                 VARCHAR(100),
  USER_TEXT_05                 VARCHAR(100),
  USER_TEXT_06                 VARCHAR(100),
  USER_TEXT_07                 VARCHAR(100),
  USER_TEXT_08                 VARCHAR(100),
  USER_TEXT_09                 VARCHAR(100),
  USER_TEXT_10                 VARCHAR(100),
  USER_TEXT_11                 VARCHAR(100),
  USER_TEXT_12                 VARCHAR(100),
  USER_TEXT_13                 VARCHAR(100),
  USER_TEXT_14                 VARCHAR(100),
  USER_TEXT_15                 VARCHAR(100),
  USER_TEXT_16                 VARCHAR(100),
  USER_TEXT_17                 VARCHAR(100),
  USER_TEXT_18                 VARCHAR(100),
  USER_TEXT_19                 VARCHAR(100),
  USER_TEXT_20                 VARCHAR(100),
  USER_TEXT_21                 VARCHAR(100),
  USER_TEXT_22                 VARCHAR(100),
  USER_TEXT_23                 VARCHAR(100),
  USER_TEXT_24                 VARCHAR(100),
  USER_TEXT_25                 VARCHAR(100),
  USER_TEXT_26                 VARCHAR(100),
  USER_TEXT_27                 VARCHAR(100),
  USER_TEXT_28                 VARCHAR(100),
  USER_TEXT_29                 VARCHAR(100),
  USER_TEXT_30                 VARCHAR(100),
  USER_NUMBER_01               NUMERIC,
  USER_NUMBER_02               NUMERIC,
  USER_NUMBER_03               NUMERIC,
  USER_NUMBER_04               NUMERIC,
  USER_NUMBER_05               NUMERIC,
  USER_NUMBER_06               NUMERIC,
  USER_NUMBER_07               NUMERIC,
  USER_NUMBER_08               NUMERIC,
  USER_NUMBER_09               NUMERIC,
  USER_NUMBER_10               NUMERIC,
  USER_DATE_01                 DATE,
  USER_DATE_02                 DATE,
  USER_DATE_03                 DATE,
  USER_DATE_04                 DATE,
  USER_DATE_05                 DATE,
  USER_DATE_06                 DATE,
  USER_DATE_07                 DATE,
  USER_DATE_08                 DATE,
  USER_DATE_09                 DATE,
  USER_DATE_10                 DATE,
  COMMENTS                     VARCHAR(4000),
  TB_CALC_TAX_AMT              NUMERIC,
  STATE_USE_AMOUNT             NUMERIC,
  STATE_USE_TIER2_AMOUNT       NUMERIC,
  STATE_USE_TIER3_AMOUNT       NUMERIC,
  COUNTY_USE_AMOUNT            NUMERIC,
  COUNTY_LOCAL_USE_AMOUNT      NUMERIC,
  CITY_USE_AMOUNT              NUMERIC,
  CITY_LOCAL_USE_AMOUNT        NUMERIC,
  TRANSACTION_STATE_CODE       VARCHAR(10),
  AUTO_TRANSACTION_STATE_CODE  VARCHAR(10),
  TRANSACTION_IND              VARCHAR(10),
  SUSPEND_IND                  VARCHAR(10),
  TAXCODE_DETAIL_ID            NUMERIC,
  TAXCODE_STATE_CODE           VARCHAR(10),
  TAXCODE_TYPE_CODE            VARCHAR(10),
  TAXCODE_CODE                 VARCHAR(40),
  CCH_TAXCAT_CODE              VARCHAR(2),
  CCH_GROUP_CODE               VARCHAR(4),
  CCH_ITEM_CODE                VARCHAR(3),
  MANUAL_TAXCODE_IND           VARCHAR(10),
  TAX_MATRIX_ID                NUMERIC,
  LOCATION_MATRIX_ID           NUMERIC,
  JURISDICTION_ID              NUMERIC,
  JURISDICTION_TAXRATE_ID      NUMERIC,
  MANUAL_JURISDICTION_IND      VARCHAR(10),
  MEASURE_TYPE_CODE            VARCHAR(10),
  STATE_USE_RATE               NUMERIC,
  STATE_USE_TIER2_RATE         NUMERIC,
  STATE_USE_TIER3_RATE         NUMERIC,
  STATE_SPLIT_AMOUNT           NUMERIC,
  STATE_TIER2_MIN_AMOUNT       NUMERIC,
  STATE_TIER2_MAX_AMOUNT       NUMERIC,
  STATE_MAXTAX_AMOUNT          NUMERIC,
  COUNTY_USE_RATE              NUMERIC,
  COUNTY_LOCAL_USE_RATE        NUMERIC,
  COUNTY_SPLIT_AMOUNT          NUMERIC,
  COUNTY_MAXTAX_AMOUNT         NUMERIC,
  COUNTY_SINGLE_FLAG           CHAR(1),
  COUNTY_DEFAULT_FLAG          CHAR(1),
  CITY_USE_RATE                NUMERIC,
  CITY_LOCAL_USE_RATE          NUMERIC,
  CITY_SPLIT_AMOUNT            NUMERIC,
  CITY_SPLIT_USE_RATE          NUMERIC,
  CITY_SINGLE_FLAG             CHAR(1),
  CITY_DEFAULT_FLAG            CHAR(1),
  COMBINED_USE_RATE            NUMERIC,
  LOAD_TIMESTAMP               DATE,
  GL_EXTRACT_UPDATER           VARCHAR(40),
  GL_EXTRACT_TIMESTAMP         DATE,
  GL_EXTRACT_FLAG              NUMERIC,
  GL_LOG_FLAG                  NUMERIC,
  GL_EXTRACT_AMT               NUMERIC,
  AUDIT_FLAG                   CHAR(1),
  AUDIT_USER_ID                VARCHAR(40),
  AUDIT_TIMESTAMP              DATE,
  MODIFY_USER_ID               VARCHAR(40),
  MODIFY_TIMESTAMP             DATE,
  UPDATE_USER_ID               VARCHAR(40),
  UPDATE_TIMESTAMP             DATE
);

CREATE  TABLE TB_TMP_LOC_MTRX_DRIVERS_COUNT
(
  DRIVER_01  NUMERIC,
  DRIVER_02  NUMERIC,
  DRIVER_03  NUMERIC,
  DRIVER_04  NUMERIC,
  DRIVER_05  NUMERIC,
  DRIVER_06  NUMERIC,
  DRIVER_07  NUMERIC,
  DRIVER_08  NUMERIC,
  DRIVER_09  NUMERIC,
  DRIVER_10  NUMERIC
);

CREATE  TABLE TB_TMP_TAX_MTRX_DRIVERS_COUNT
(
  DRIVER_01  NUMERIC,
  DRIVER_02  NUMERIC,
  DRIVER_03  NUMERIC,
  DRIVER_04  NUMERIC,
  DRIVER_05  NUMERIC,
  DRIVER_06  NUMERIC,
  DRIVER_07  NUMERIC,
  DRIVER_08  NUMERIC,
  DRIVER_09  NUMERIC,
  DRIVER_10  NUMERIC,
  DRIVER_11  NUMERIC,
  DRIVER_12  NUMERIC,
  DRIVER_13  NUMERIC,
  DRIVER_14  NUMERIC,
  DRIVER_15  NUMERIC,
  DRIVER_16  NUMERIC,
  DRIVER_17  NUMERIC,
  DRIVER_18  NUMERIC,
  DRIVER_19  NUMERIC,
  DRIVER_20  NUMERIC,
  DRIVER_21  NUMERIC,
  DRIVER_22  NUMERIC,
  DRIVER_23  NUMERIC,
  DRIVER_24  NUMERIC,
  DRIVER_25  NUMERIC,
  DRIVER_26  NUMERIC,
  DRIVER_27  NUMERIC,
  DRIVER_28  NUMERIC,
  DRIVER_29  NUMERIC,
  DRIVER_30  NUMERIC
);
