-- Function: sp_gen_location_driver(character varying, numeric, character varying, character varying, character varying)

-- DROP FUNCTION sp_gen_location_driver(character varying, numeric, character varying, character varying, character varying);

CREATE OR REPLACE FUNCTION sp_gen_location_driver(IN p_generate_driver_reference character varying, IN p_an_batch_id numeric, IN p_transaction_table_name character varying, IN p_location_table_name character varying, INOUT p_vc_location_matrix_where character varying)
  RETURNS character varying AS
$BODY$
DECLARE
/* ************************************************************************************************/
/* Object Type/Name: Stored Procedure - sp_gen_location_driver                                 */
/* Author:           Michael B. Fuller                                                            */
/* Date:             a long time ago                                                              */
/* Description:                                 */
/* Arguments:        None                                                                         */
/* Returns:          None                                                                         */
/* ************************************************************************************************/
/* Maintenance History:                                                                           */
/* Scan  Author       Date     Release    Description                                  Ticket #   */
/* ----- ---------- ---------- ---------- -------------------------------------------- ---------- */
/* SRINI MFULLER      01/24/2011          Change VARCHAR Size                            954      */        
/* ************************************************************************************************/
	-- Define Location Driver Names Cursor (tb_driver_names)
	location_driver_cursor CURSOR
	   FOR
		SELECT tb_driver_names.trans_dtl_column_name, tb_driver_names.matrix_column_name, tb_driver_names.null_driver_flag, tb_driver_names.wildcard_flag,
		       datadefcol.desc_column_name, datadefcol.data_type
		  FROM tb_driver_names,
		       (SELECT *
			  FROM tb_data_def_column
			 WHERE table_name = 'TB_TRANSACTION_DETAIL') datadefcol
		 WHERE tb_driver_names.driver_names_code = 'L' AND
		       tb_driver_names.trans_dtl_column_name = datadefcol.column_name
	      ORDER BY tb_driver_names.driver_id;

	location_driver		RECORD;

	vi_rows_processed	INTEGER		:= 0;

	vc_driver_ref_ins_stmt          VARCHAR(30000)              := NULL; /***954***SRINI***/
	vi_cursor_id                    INTEGER                    := 0;
BEGIN
   
   -- ***** Create Dynamic WHERE Clause for Matrix SQLs ***** --
   -- Location Matrix Drivers
   OPEN location_driver_cursor;
   LOOP

                 FETCH location_driver_cursor INTO location_driver;

                 IF (NOT FOUND) THEN
		   EXIT;
		 END IF;

		  IF location_driver.null_driver_flag = '1' THEN
                     IF location_driver.wildcard_flag = '1' THEN
                        IF UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'CHAR' OR
                           UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'VARC' THEN
                              p_vc_location_matrix_where := p_vc_location_matrix_where || 'AND ' ||
                              '(('|| p_transaction_table_name ||'.' || location_driver.trans_dtl_column_name || ' IS NULL AND ('|| p_location_table_name ||'.' || location_driver.matrix_column_name || ' = ''*ALL'' OR '|| p_location_table_name ||'.' || location_driver.matrix_column_name || ' = ''*NULL'' )) OR ' ||
                              '('|| p_transaction_table_name ||'.' || location_driver.trans_dtl_column_name || ' IS NOT NULL AND ('|| p_location_table_name ||'.' || location_driver.matrix_column_name || ' = ''*ALL'' OR UPPER('|| p_transaction_table_name ||'.' || location_driver.trans_dtl_column_name || ') LIKE UPPER('|| p_location_table_name ||'.' || location_driver.matrix_column_name || ') ))) ';
                        END IF;
                     ELSE
                        IF UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'CHAR' OR
                           UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'VARC' THEN
                              p_vc_location_matrix_where := p_vc_location_matrix_where || 'AND ' ||
                              '(('|| p_transaction_table_name ||'.' || location_driver.trans_dtl_column_name || ' IS NULL AND ('|| p_location_table_name ||'.' || location_driver.matrix_column_name || ' = ''*ALL'' OR '|| p_location_table_name ||'.' || location_driver.matrix_column_name || ' = ''*NULL'' )) OR ' ||
                              '('|| p_transaction_table_name ||'.' || location_driver.trans_dtl_column_name || ' IS NOT NULL AND ('|| p_location_table_name ||'.' || location_driver.matrix_column_name || ' = ''*ALL'' OR UPPER('|| p_transaction_table_name ||'.' || location_driver.trans_dtl_column_name || ') = UPPER('|| p_location_table_name ||'.' || location_driver.matrix_column_name || ') ))) ';
                        END IF;
                     END IF;
                  ELSE
                     IF location_driver.wildcard_flag = '1' THEN
                        IF UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'CHAR' OR
                           UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'VARC' THEN
                              p_vc_location_matrix_where := p_vc_location_matrix_where || 'AND ' ||
                              '('|| p_location_table_name ||'.' || location_driver.matrix_column_name || ' = ''*ALL'' OR UPPER('|| p_transaction_table_name ||'.' || location_driver.trans_dtl_column_name || ') LIKE UPPER('|| p_location_table_name ||'.' || location_driver.matrix_column_name || ') ) ';
                        END IF;
                     ELSE
                        IF UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'CHAR' OR
                           UPPER(SUBSTR(LTRIM(RTRIM(location_driver.data_type)), 1, 4)) = 'VARC' THEN
                              p_vc_location_matrix_where := p_vc_location_matrix_where || 'AND ' ||
                              '('|| p_location_table_name ||'.' || location_driver.matrix_column_name || ' = ''*ALL'' OR UPPER('|| p_transaction_table_name ||'.' || location_driver.trans_dtl_column_name || ') = UPPER('|| p_location_table_name ||'.' || location_driver.matrix_column_name || ') ) ';
                        END IF;
                     END IF;
                  END IF;

		-- Check to confirm whether we're running driver reference portion. 
		-- NOTE: This is done ONLY for Batch Processing.
		IF (p_generate_driver_reference = 'Y') THEN
		      -- Insert Driver Reference Values for this driver
		      -- Create Insert SQL
		      vc_driver_ref_ins_stmt :=
			 'INSERT INTO tb_driver_reference (trans_dtl_column_name, driver_value, driver_description) ' ||
			    'SELECT ''' || Upper(location_driver.trans_dtl_column_name) ||
				    ''', ' || location_driver.trans_dtl_column_name || ', ';
		      IF location_driver.desc_column_name IS NOT NULL THEN
			 vc_driver_ref_ins_stmt := vc_driver_ref_ins_stmt || 'min(' || location_driver.desc_column_name || ') ';
		      ELSE
			 vc_driver_ref_ins_stmt := vc_driver_ref_ins_stmt || 'NULL ';
		      END IF;
		      vc_driver_ref_ins_stmt := vc_driver_ref_ins_stmt ||
			      'FROM tb_bcp_transactions ' ||
			     'WHERE process_batch_no = ' || TO_CHAR(p_an_batch_id) || ' AND ' ||
				    location_driver.trans_dtl_column_name || ' IS NOT NULL AND ' ||
				    location_driver.trans_dtl_column_name || ' NOT IN ' ||
			       '(SELECT driver_value ' ||
				  'FROM tb_driver_reference ' ||
				 'WHERE trans_dtl_column_name = ''' || Upper(location_driver.trans_dtl_column_name) || ''') ' ||
			      'GROUP BY ''' || Upper(location_driver.trans_dtl_column_name) ||
					''', ' || location_driver.trans_dtl_column_name;
		      -- Execute Insert SQL
		      EXECUTE vc_driver_ref_ins_stmt;
		END IF;

   END LOOP;
   CLOSE location_driver_cursor;

END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
ALTER FUNCTION sp_gen_location_driver(character varying, numeric, character varying, character varying, character varying) OWNER TO stscorp;
