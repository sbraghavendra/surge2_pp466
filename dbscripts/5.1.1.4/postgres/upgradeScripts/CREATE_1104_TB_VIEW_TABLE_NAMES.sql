--RECREATE  VIEW with TABLE NAMES as UPPER CASE


Drop view tb_view_table_names;
CREATE OR REPLACE VIEW tb_view_table_names AS 
 SELECT UPPER(tables.table_name) AS table_name
   FROM information_schema.tables
  WHERE tables.table_schema::text = 'stscorp'::text;